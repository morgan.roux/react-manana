#!/bin/bash

API_URL='http://localhost:4000'
routing_keys='roles;avatars;items;traitements;users;couleurs;titulaires;titulaireaffectations;pharmacies;ppersonnels;groupements;personnels;personnelaffectations;activiteusers;projects;todoetiquettes;todosections;actions;aides;comments;partages;informationliaisons;groupeamises;groupeamisdetails;ideeoubonnepratiques;tickets;ticketchangementcibles;partenaires;laboratoires;partenairerepresentants;partenairerepresentantaffectations'

IFS=';'
read -ra splitted_routing_keys <<< "$routing_keys"
IFS=' '

len=${#splitted_routing_keys[@]}
if [ $len -eq 0 ]; then
    echo "Please configure routing_keys.";
else

    for routing_key in "${splitted_routing_keys[@]}"
        do
            echo "\nMapping $routing_key"
            curl "$API_URL/graphql" \
                -H "authority: $API_URL" \
                -H 'accept: */*' \
                -H 'content-type: application/json' \
                -H "origin: $API_URL" \
                -H 'sec-fetch-site: same-origin' \
                -H 'sec-fetch-mode: cors' \
                -H 'sec-fetch-dest: empty' \
                -H "referer: $API_URL/graphql" \
                --data-binary '{"operationName":"mappingIndex","variables":{},"query":"query mappingIndex {\n  mappingIndex(routingKey: \"'"$routing_key"'\")\n}\n"}' \
                --compressed
            echo "\nReindex $routing_key"
            curl "$API_URL/graphql" \
                -H "authority: $API_URL" \
                -H 'accept: */*' \
                -H 'content-type: application/json' \
                -H "origin: $API_URL" \
                -H 'sec-fetch-site: same-origin' \
                -H 'sec-fetch-mode: cors' \
                -H 'sec-fetch-dest: empty' \
                -H "referer: $API_URL/graphql" \
                --data-binary '{"operationName":"reindex","variables":{},"query":"query reindex {\n  reindex(routingKey: \"'"$routing_key"'\")\n}\n"}' \
                --compressed
        done
fi

#!/bin/bash

API_URL='http://localhost:4000'
routing_keys='roles;avatars;items;traitements;users;couleurs;titulaires;titulaireaffectations;pharmacies;ppersonnels;groupements;personnels;personnelaffectations;activiteusers;projects;todoetiquettes;todosections;actions;aides;comments;partages;informationliaisons;groupeamises;groupeamisdetails;ideeoubonnepratiques;tickets;ticketchangementcibles;partenaires;laboratoires;partenairerepresentants;partenairerepresentantaffectations'

IFS=';'
read -ra splitted_routing_keys <<< "$routing_keys"
IFS=' '

len=${#splitted_routing_keys[@]}
if [ $len -eq 0 ]; then
    echo "Please configure routing_keys.";
else

    for routing_key in "${splitted_routing_keys[@]}"
        do
            echo "\nMapping $routing_key"
            curl "$API_URL/graphql" \
                -H "authority: $API_URL" \
                -H 'accept: */*' \
                -H 'content-type: application/json' \
                -H "origin: $API_URL" \
                -H 'sec-fetch-site: same-origin' \
                -H 'sec-fetch-mode: cors' \
                -H 'sec-fetch-dest: empty' \
                -H "referer: $API_URL/graphql" \
                --data-binary '{"operationName":"mappingIndex","variables":{},"query":"query mappingIndex {\n  mappingIndex(routingKey: \"'"$routing_key"'\")\n}\n"}' \
                --compressed
            echo "\nReindex $routing_key"
            curl "$API_URL/graphql" \
                -H "authority: $API_URL" \
                -H 'accept: */*' \
                -H 'content-type: application/json' \
                -H "origin: $API_URL" \
                -H 'sec-fetch-site: same-origin' \
                -H 'sec-fetch-mode: cors' \
                -H 'sec-fetch-dest: empty' \
                -H "referer: $API_URL/graphql" \
                --data-binary '{"operationName":"reindex","variables":{},"query":"query reindex {\n  reindex(routingKey: \"'"$routing_key"'\")\n}\n"}' \
                --compressed
        done
fi

