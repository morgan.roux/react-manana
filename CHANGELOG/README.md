## Fichiers changelog

Les fichiers changelog contiennent toutes les modifications effectuées sur le projet et les instructions à suivre pour mettre à jour les environment d'éxecution.

### Format

DATE-NUMERO_VERSION-changelog.md

### Exemple 

20201127-V1.0.0-changelog.md

## Fichiers migration

Les fichiers migrations contiennent les requêtes SQL qui nous permettent de mettre à jour la base de données.

### Format

DATE-NUMERO_VERSION-migration.sql

### Exemple

20201127-V1.0.0-migration.sql
