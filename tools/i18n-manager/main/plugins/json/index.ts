export const fileExtensions = ['.json', '.arb'];

export const parse = (content: string): Promise<any> => {
  try {
    return JSON.parse(content);
  } catch (e) {
    return Promise.resolve(undefined);
  }
};

const deepSortObjectByKeys = (unordered: any, sortArrays = false): any => {
  if (!unordered || typeof unordered !== 'object') {
    return unordered;
  }

  if (Array.isArray(unordered)) {
    const newArr = unordered.map((item) => deepSortObjectByKeys(item, sortArrays));
    if (sortArrays) {
      newArr.sort();
    }
    return newArr;
  }

  const ordered: any = {};
  Object.keys(unordered)
    .sort()
    .forEach((key) => {
      ordered[key] = deepSortObjectByKeys(unordered[key], sortArrays);
    });
  return ordered;
};

export const serialize = async (data: object): Promise<string | undefined> => {
  try {
    return JSON.stringify(deepSortObjectByKeys(data), undefined, 2); //JSON.stringify(data, Object.keys(data).sort(), 2);
  } catch (e) {
    return undefined;
  }
};
