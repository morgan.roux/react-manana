import {
  getCurrentWindow,
  sendSave,
} from '../windowManager';
import { onPreferencesClick } from './shared';

const saveDirectory = () => {
  const currentWindow = getCurrentWindow();
  if (currentWindow) {
    sendSave(currentWindow);
  }
};

const fileMenu: Electron.MenuItemConstructorOptions = {
  label: 'File',
  submenu: [
    {
      label: 'Save',
      click: saveDirectory,
      accelerator: 'CommandOrControl+S',
    },
  ],
};

if (process.platform === 'linux') {
  (fileMenu.submenu as any).push(
    { type: 'separator' },
    {
      label: 'Preferences',
      click: onPreferencesClick,
      accelerator: 'CommandOrControl+,',
    },
  );
}

export default fileMenu;
