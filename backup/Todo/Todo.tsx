import { ApolloQueryResult } from '@apollo/client';
import { isMd, isMobile, useApplicationContext, useDisplayNotification, useValueParameterAsBoolean } from '@lib/common';
import {
  Bulk_CountQuery,
  Bulk_CountQueryVariables,
  Search_Todo_ActionDocument,
  useBulk_CountLazyQuery,
  useBulk_CountQuery,
  useGet_UrgencesQuery,
  useImportancesQuery,
  useSearch_Todo_ActionLazyQuery,
  useSearch_Todo_ActionQuery,
} from '@lib/common/src/federation';
import {
  ParamCategory,
  Search_ActionDocument,
  useParameters_Groupes_CategoriesQuery,
  useSearch_Custom_Content_ProjectQuery,
  useSearch_Todo_SectionLazyQuery,
} from '@lib/common/src/graphql';
import { CustomModal } from '@lib/ui-kit';
import { ListItem, ListItemText, Radio } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import { useTheme } from '@material-ui/core';
import { Search as SearchIcon } from '@material-ui/icons';
import React, { FC, useEffect, useState } from 'react';
import { useHistory, useLocation, useParams } from 'react-router';
import FilterAlt from '../../assets/icons/todo/filter_alt.svg';
//import ConcernedParticipant from '../InteligenceCollective/Modals/ConcernedParticipant';
import { ImportanceInterface } from './Common/ImportanceFilter/ImportanceFilterList';
import { UrgenceInterface } from './Common/UrgenceFilter/UrgenceFilter';
import MobileFilters from './Filters/MobileFilters';
import Search from './Filters/Search';
import { useTodoURLParams } from './hooks/useTodoURLParams';
import MainContent from './MainContent/MainContent';
import Sider from './NavBar/Sider';
import useStyles from './styles';

interface TodoProps {
  window?: () => Window;
  match: {
    params: {
      filter: string;
      filterId: string;
    };
  };
}

export interface CountTodosProps {
  refetchCountTodos?: (variables?: Bulk_CountQueryVariables | undefined) => Promise<ApolloQueryResult<Bulk_CountQuery>>;
  countTodos?: any;
}

export const defaultDateTerm = {
  range: { dateDebut: { gt: 'now/d' } },
};

const Todo: FC<TodoProps> = (props) => {
  const { pathname } = useLocation();
  const { push } = useHistory();
  const { filter, filterId }: any = useParams();

  const classes = useStyles({});
  const theme = useTheme();
  const enableMatriceResponsabilite = useValueParameterAsBoolean('0501');
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [fetchingMore, setFetchingMore] = useState<boolean>(false);
  const [triage, setTriage] = useState<any[]>([{ priorite: { order: 'asc' } }]);
  const [taskListVariables, setTaskListVariables] = useState({});
  const [showMainContent, setShowMainContent] = useState<boolean>(pathname === '/todo/aujourdui' ? false : true);
  const displayNotification = useDisplayNotification();
  const [skip, setSkip] = useState<number>(0);
  const [todaySkip, setTodaySkip] = useState<number>(0);
  const isOnTeam = pathname.includes('equipe');

  const [urgence, setUrgence] = useState<UrgenceInterface[] | null | undefined>();
  const [importances, setImportances] = useState<ImportanceInterface[] | null | undefined>([]);
  const { user, graphql, federation } = useApplicationContext();
  const loadingImportances = useImportancesQuery({ client: federation });
  const loadingUrgences = useGet_UrgencesQuery({ client: federation });
  const { params, redirectTo, getActionsQuery } = useTodoURLParams();
  const [menuFilters, setMenuFilters] = useState<any>({
    equipe: params.assignee?.includes('team') ? true : false,
  });
  const [showMobileFilters, setShowMobileFilters] = useState<boolean>(!params.date ? true : false);
  const importancesData = loadingImportances?.data?.importances.nodes || [];
  const urgencesData = loadingUrgences?.data?.urgences.nodes || [];
  const idProjet = params.projet && params.projet.length > 0 ? params.projet[0] : undefined;

  useEffect(() => {
    if (!loadingUrgences.loading && urgencesData) {
      setUrgence((urgencesData || []) as any);
    }
  }, [loadingUrgences.loading, urgencesData]);

  useEffect(() => {
    if (!loadingImportances.loading && importancesData) {
      setImportances((importancesData || []) as any);
    }
  }, [loadingImportances.loading, importancesData]);

  const [filters, setFilters] = useState<any>(null);

  useEffect(() => {
    if (pathname === '/todo/aujourdui') {
      setShowMainContent(false);
    }
  }, [pathname]);

  const { data: dataParameters } = useParameters_Groupes_CategoriesQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
    variables: {
      categories: [ParamCategory.GroupementSuperadmin, ParamCategory.Groupement, ParamCategory.User],
      groupes: ['TODO'],
    },
  });

  const isOnNextWeek = filter === 'sept-jours';
  const isOnNextWeekTeam = filter === 'sept-jours-equipe';

  const isInbox = pathname.includes('recu') && !pathname.includes('recu-equipe');
  const isInboxTeam = pathname.includes('recu-equipe');
  const isProject = pathname.includes('projet');
  const termActive = filters && filters.taskStatus ? filters.taskStatus : 'ACTIVE';

  const [actionListFilters, setActionListFilters] = useState(isOnNextWeek || isOnNextWeekTeam ? defaultDateTerm : {});

  const termologie = enableMatriceResponsabilite ? 'fonction' : 'projet';

  const typeProjectFilters =
    filters?.typeProject?.length > 0 ? filters.typeProject : ['GROUPEMENT', 'PERSONNEL', 'PROFESSIONNEL'];

  const loadCount = useBulk_CountQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
    variables: {
      queries: {
        pastMe: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['me'], date: 'past', projet: undefined }),
        },
        todayMe: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['me'], date: 'today', projet: undefined }),
        },
        nextMe: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['me'], date: 'next', projet: undefined }),
        },
        pastTeam: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['team'], date: 'past', projet: undefined }),
        },
        todayTeam: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['team'], date: 'today', projet: undefined }),
        },
        nextTeam: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['team'], date: 'next', projet: undefined }),
        },
        active: {
          index: 'todoaction',
          ...getActionsQuery({ type: ['ACTIVE'], assignee: ['me'], date: undefined, projet: undefined }),
        },
        activeTeam: {
          index: 'todoaction',
          ...getActionsQuery({ type: ['ACTIVE'], assignee: ['team'], date: undefined, projet: undefined }),
        },
        done: {
          index: 'todoaction',
          ...getActionsQuery({ type: ['DONE'], assignee: ['me'], date: undefined, projet: undefined }),
        },
        doneTeam: {
          index: 'todoaction',
          ...getActionsQuery({ type: ['DONE'], assignee: ['team'], date: undefined, projet: undefined }),
        },
        totalMe: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['me'], date: undefined, projet: undefined }),
        },
        totalTeam: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['team'], date: undefined, projet: undefined }),
        },
      },
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const { data: countTodosData, refetch: refetchCountTodos } = loadCount;

  const countTodos = countTodosData?.bulkCount;

  const defaultMust = [
    {
      term: { isRemoved: false },
    },
    { terms: { typeProject: [...typeProjectFilters, 'PERSONNEL'] } },
    {
      bool: enableMatriceResponsabilite
        ? {
            should: [
              {
                bool: {
                  must: [
                    { term: { typeProject: 'PERSONNEL' } },
                    {
                      term: { 'user.id': user.id },
                    },
                  ],
                },
              },
              {
                bool: {
                  must: [{ term: { typeProject: 'PROFESSIONNEL' } }, { term: { 'participants.id': user.id } }],
                },
              },
            ],
          }
        : {
            should: [
              {
                term: { 'user.id': user.id },
              },
              { term: { 'participants.id': user.id } },
            ],
            minimum_should_match: 1,
          },
    },
  ];

  const projectList = useSearch_Custom_Content_ProjectQuery({
    client: graphql,
    variables: {
      type: ['project'],
      query: {
        query: { bool: { must: defaultMust } },
      },
      sortBy: [{ ordre: { order: 'asc' } }],
      actionStatus: termActive,
    },
  });

  const loadActions = useSearch_Todo_ActionQuery({
    client: federation,
    variables: {
      index: ['todoaction'],
      query: params.date === 'today' ? { ...getActionsQuery({ date: 'past' }) } : { ...getActionsQuery() },
      sortBy: triage,
      take: params.date !== 'next' ? 12 : undefined,
      skip: params.date !== 'next' ? skip : undefined,
    },
    skip: !params || loadingImportances.loading || projectList.loading || loadingUrgences.loading,
  });

  const {
    data: dataActions,
    loading: loadingTask,
    variables,
    refetch: refetchTask,
    fetchMore: fetchMoreTask,
  } = loadActions;

  const loadTodayActions = useSearch_Todo_ActionQuery({
    client: federation,
    variables: {
      index: ['todoaction'],
      query: { ...getActionsQuery() },
      sortBy: triage,
      take: 12,
      skip: todaySkip,
    },
    skip:
      !params.date ||
      params.date !== 'today' ||
      loadingImportances.loading ||
      projectList.loading ||
      loadingUrgences.loading,
  });

  const {
    data: dataTodayActions,
    loading: loadingTodayTask,
    variables: variablesToday,
    refetch: refetchTodayTask,
    fetchMore: fetchMoreTodayTask,
  } = loadTodayActions;

  useEffect(() => {
    if (variables) setTaskListVariables(variables);
  }, [variables]);

  const sectionQueryVariables = (id: string) =>
    id && isProject
      ? {
          query: {
            bool: {
              must: [{ term: { 'project.id': id } }, { term: { isRemoved: false } }],
            },
          },
        }
      : isInboxTeam
      ? {
          query: {
            bool: {
              must: [{ term: { isRemoved: false } }, { term: { isInInboxTeam: true } }],
              must_not: [
                {
                  exists: {
                    field: 'project',
                  },
                },
              ],
            },
          },
        }
      : isInbox
      ? {
          query: {
            bool: {
              must: [{ term: { isRemoved: false } }, { term: { isInInbox: true } }],
              must_not: [
                {
                  exists: {
                    field: 'project',
                  },
                },
              ],
            },
          },
        }
      : {
          query: {
            bool: {
              must: [
                { term: { isRemoved: false } },
                { term: { isInInbox: false } },
                { term: { isInInboxTeam: false } },
              ],
              must_not: [
                {
                  exists: {
                    field: 'project',
                  },
                },
              ],
            },
          },
        };

  const [loadSections, { data: sectionQuery, refetch: refetchSection }] = useSearch_Todo_SectionLazyQuery({
    client: graphql,
  });

  useEffect(() => {
    if (!loadingImportances.loading && !projectList.loading && !loadingUrgences.loading) {
      // loadActions({
      //   variables: {
      //     index: ['todoaction'],
      //     query:
      //       params.date === 'today'
      //         ? { ...getActionsQuery({ ...params, date: 'past' }) }
      //         : { ...getActionsQuery({ ...params }) },
      //     sortBy: triage,
      //     take: !isOnSevenDays ? 12 : undefined,
      //     skip: !isOnSevenDays ? skip : undefined,
      //   },
      // });
      // if (params.date === 'today')
      //   loadTodayActions({
      //     variables: {
      //       index: ['todoaction'],
      //       query: { ...getActionsQuery(params) },
      //       sortBy: triage,
      //       take: 12,
      //       skip: todaySkip,
      //     },
      //   });

      if (idProjet) {
        loadSections({
          variables: {
            type: ['todosection'],
            query: sectionQueryVariables(idProjet),
          },
        });
      }
    }
  }, [loadingImportances.loading, projectList.loading, loadingUrgences.loading, urgence, importances, triage]);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const parameters =
    dataParameters && dataParameters.parametersGroupesCategories && dataParameters.parametersGroupesCategories.length
      ? dataParameters.parametersGroupesCategories.map((groupe) => {
          return {
            code: groupe?.code,
            value: groupe && groupe.value && groupe.value.id ? groupe.value.value : 'false',
          };
        })
      : [];

  const refetchAll = () => {
    if (refetchCountTodos) refetchCountTodos();
    if (refetchSection) refetchSection();
    handleRefetchTask();
    if (projectList && projectList.refetch) projectList.refetch();
    if (loadingImportances && loadingImportances.refetch) loadingImportances.refetch();
  };

  const handleRefetchTask = () => {
    refetchTask && refetchTask();
    refetchTodayTask && refetchTodayTask();
  };

  const handleFetchMoreActions = () => {
    setFetchingMore(true);
    fetchMoreTask &&
      fetchMoreTask({
        query: Search_Todo_ActionDocument,
        updateQuery: (prev: any, { fetchMoreResult }) => {
          if (prev?.search?.data && fetchMoreResult?.search?.data) {
            setFetchingMore(false);
            return {
              ...prev,
              search: {
                ...prev.search,
                data: [...prev.search.data, ...fetchMoreResult.search.data],
                total: fetchMoreResult.search.total,
              },
            };
          }
          return prev;
        },
        variables: {
          index: ['todoaction'],
          query: params.date === 'today' ? { ...getActionsQuery({ date: 'past' }) } : { ...getActionsQuery() },
          sortBy: triage,
          take: 12,
          skip: skip + 12,
        },
      }).finally(() => {
        setFetchingMore(false);
        setSkip(skip + 12);
      });
  };

  const handleFetchMoreTodayActions = () => {
    setFetchingMore(true);
    fetchMoreTodayTask &&
      fetchMoreTodayTask({
        query: Search_Todo_ActionDocument,
        updateQuery: (prev: any, { fetchMoreResult }) => {
          if (
            prev &&
            prev.search &&
            prev.search.data &&
            fetchMoreResult &&
            fetchMoreResult.search &&
            fetchMoreResult.search.data
          ) {
            const { data: currentData } = prev.search;
            return {
              ...prev,
              search: {
                ...prev.search,
                data: [...currentData, ...fetchMoreResult.search.data],
                total: fetchMoreResult.search.total,
              },
            };
          }
          return prev;
        },
        variables: {
          index: ['todoaction'],
          query: { ...getActionsQuery({ assignee: [isOnTeam ? 'team' : 'me'], date: 'today' }) },
          sortBy: triage,
          take: 12,
          skip: todaySkip + 12,
        },
      }).finally(() => {
        setFetchingMore(false);
        setTodaySkip(todaySkip + 12);
      });
  };

  const handleShowMobileFiltersClick = () => {
    setShowMobileFilters(!showMobileFilters);
  };

  const handleMobileFilterClose = (value: any) => {
    if (typeof value === 'boolean') {
      setShowMobileFilters(value);
    } else {
      setShowMobileFilters(false);
      push(value);
      setShowMainContent(true);
    }
  };

  const handleEquipeFilterClick = () => {
    setMenuFilters((prevState: any) => ({ ...prevState, equipe: !prevState.equipe }));
    redirectTo({ ...params, assignee: params.assignee?.includes('me') ? ['team'] : ['me'], projet: undefined });
  };

  const header = (
    <>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <ListItem role={undefined} dense={true} button={true} onClick={handleEquipeFilterClick}>
          <Box display="flex" alignItems="center">
            <Radio checked={params.assignee?.includes('me') ? true : false} tabIndex={-1} disableRipple={true} />
            <ListItemText primary="Ma To-Do" />
          </Box>
        </ListItem>
        <ListItem role={undefined} dense={true} button={true} onClick={handleEquipeFilterClick}>
          <Box display="flex" alignItems="center">
            <Radio checked={params.assignee?.includes('team') ? true : false} tabIndex={-1} disableRipple={true} />
            <ListItemText primary="Equipe" />
          </Box>
        </ListItem>
        <IconButton
          className={classes.iconAction}
          color="inherit"
          aria-label="settings"
          edge="start"
          onClick={handleShowMobileFiltersClick}
        >
          <img src={FilterAlt} />
        </IconButton>
      </Box>
      {!showMobileFilters && (
        <Search placeholder={'Rechercher des tâches'} refetchAll={refetchAll} popupIcon={<SearchIcon />} />
      )}
    </>
  );

  return (
    <Box className={classes.root}>
      {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
      <Hidden smDown={true} implementation="css">
        <nav className={classes.drawerWeb} aria-label="mailbox folders">
          <Box className={classes.drawerContentRoot}>
            {header}
            <Box style={{ display: showMobileFilters ? 'unset' : 'none' }}>
              <MobileFilters
                countTodos={countTodos}
                refetchCountTodos={refetchCountTodos}
                onClose={handleMobileFilterClose}
              />
            </Box>
            {!showMobileFilters && (
              <Sider
                projectList={projectList}
                setActionListFilters={setActionListFilters}
                refetchCountTodos={refetchCountTodos}
                countTodos={countTodos}
                parameters={parameters}
                setShowMainContent={setShowMainContent}
                menuFilters={menuFilters}
                termologie={termologie}
                useMatriceResponsabilite={enableMatriceResponsabilite}
                urgence={urgence}
                setUrgence={setUrgence}
                importances={importances}
                setImportances={setImportances}
                setTriage={setTriage}
              />
            )}
          </Box>
        </nav>
      </Hidden>
      <Hidden mdUp={true} implementation="css">
        {!showMainContent && (
          <nav className={classes.drawer} aria-label="mailbox folders">
            <Box className={classes.drawerContentRoot}>
              {header}
              <Sider
                projectList={projectList}
                setActionListFilters={setActionListFilters}
                refetchCountTodos={refetchCountTodos}
                countTodos={countTodos}
                parameters={parameters}
                setShowMainContent={setShowMainContent}
                menuFilters={menuFilters}
                termologie={termologie}
                useMatriceResponsabilite={enableMatriceResponsabilite}
                urgence={urgence}
                setUrgence={setUrgence}
                importances={importances}
                setImportances={setImportances}
                setTriage={setTriage}
              />
            </Box>
          </nav>
        )}
      </Hidden>

      {isMd() && (
        <Drawer
          container={window.document.body}
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <nav className={classes.drawerWeb} aria-label="mailbox folders">
            <Box className={classes.drawerContentRoot}>
              {header}
              {showMobileFilters ? (
                <MobileFilters
                  countTodos={countTodos}
                  refetchCountTodos={refetchCountTodos}
                  onClose={handleMobileFilterClose}
                />
              ) : (
                <Sider
                  projectList={projectList}
                  setActionListFilters={setActionListFilters}
                  refetchCountTodos={refetchCountTodos}
                  countTodos={countTodos}
                  parameters={parameters}
                  setShowMainContent={setShowMainContent}
                  menuFilters={menuFilters}
                  termologie={termologie}
                  useMatriceResponsabilite={enableMatriceResponsabilite}
                  urgence={urgence}
                  setUrgence={setUrgence}
                  importances={importances}
                  setImportances={setImportances}
                  setTriage={setTriage}
                />
              )}
            </Box>
          </nav>
        </Drawer>
      )}

      {(!isMobile() || showMainContent) && (
        <main className={classes.content}>
          <MainContent
            filters={filters}
            refetchCountTodos={refetchCountTodos}
            countTodos={countTodos}
            parameters={parameters}
            actions={dataActions}
            todayActions={dataTodayActions}
            actionListFilters={actionListFilters}
            setTriage={setTriage}
            triage={triage}
            setTaskListVariables={setTaskListVariables}
            taskListVariables={taskListVariables}
            loadingTask={fetchingMore || loadingTask || loadingTodayTask}
            refetchTask={handleRefetchTask}
            projectList={projectList}
            sectionQuery={sectionQuery}
            refetchSection={refetchSection}
            refetchAll={refetchAll}
            handleDrawerToggle={handleDrawerToggle}
            useMatriceResponsabilite={enableMatriceResponsabilite}
            fetchingMoreActions={fetchingMore}
            fetchMoreActions={handleFetchMoreActions}
            fetchMoreTodayActions={handleFetchMoreTodayActions}
          />
        </main>
      )}

      {isMobile() && (
        <CustomModal
          open={showMobileFilters}
          setOpen={setShowMobileFilters}
          fullScreen={true}
          closeIcon={true}
          withBtnsActions={false}
          noDialogContent={true}
        >
          <MobileFilters
            countTodos={countTodos}
            refetchCountTodos={refetchCountTodos}
            onClose={handleMobileFilterClose}
          />
        </CustomModal>
      )}

      {/* <ConcernedParticipant
        isOpen={openConcernedParticipantModal}
        setOpen={setOpenConcernedParticipantModal}
        title="Collègue(s) Concernée(s)"
        isOnEquip={true}
      /> */}
    </Box>
  );
};

export default Todo;
