import { Box, Icon, List, ListItem, Typography } from '@material-ui/core';
import React, { ReactNode } from 'react';
import useStyles from './styles';

interface FavorisList {
  icon: ReactNode;
  title: string;
  count: number;
  path: string;
  isActive: boolean;
}

interface FarovisHeaderProps {
  headerList: FavorisList[];
  handleFavorisClick: (favoris: any) => void;
  setShowMainContent?: (value: boolean) => void;
}

const FavorisHeader: React.FC<FarovisHeaderProps> = ({
  headerList,
  handleFavorisClick,
  setShowMainContent,
}) => {
  const classes = useStyles({});

  const handleClick = (favoris: any) => {
    handleFavorisClick(favoris);
    if (setShowMainContent) setShowMainContent(true);
  };

  return (
    <List className={classes.root}>
      {headerList.map((favoris, index) => (
        <ListItem
          button={true}
          key={index}
          onClick={() => handleClick(favoris)}
          selected={favoris.isActive}
        >
          <Box width="100%" display="flex" alignItems="center" justifyContent="space-between">
            <Box display="flex" alignItems="center">
              <Icon color="secondary">{favoris.icon}</Icon>
              <Typography className={classes.title}>{favoris.title}</Typography>
            </Box>
            <Box>
              <Typography className={classes.count}>{favoris.count}</Typography>
            </Box>
          </Box>
        </ListItem>
      ))}
    </List>
  );
};

export default FavorisHeader;
