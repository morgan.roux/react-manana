import { capitalizeFirstLetter, useApplicationContext } from '@lib/common';
import { TypeProject } from '@lib/common/src/graphql';
import { useNiveauMatriceFonctions } from '@lib/hooks';
import { Backdrop, ConfirmDialog } from '@lib/ui-kit';
import { DateRange, FiberManualRecord, LocalOffer, SupervisedUserCircle, Today } from '@material-ui/icons';
import _, { concat, filter } from 'lodash';
import React, { useState } from 'react';
import { useHistory, useParams } from 'react-router';
import { useTodoURLParams } from '../../hooks/useTodoURLParams';
import DropdownDraggable from '../../Common/DropdownDraggable/';
import ImportanceFilter from '../../Common/ImportanceFilter/ImportanceFilter';
import { ImportanceInterface } from '../../Common/ImportanceFilter/ImportanceFilterList';
import UrgenceFilter from '../../Common/UrgenceFilter';
import { UrgenceInterface } from '../../Common/UrgenceFilter/UrgenceFilter';
import { useUpdateProject } from '../../Common/utils/useProject';
import AjoutParticipant from '../../Modals/AjoutParticipants';
import { CountTodosProps } from '../../Todo';
import FavorisHeader from '../FavorisHeader';
import { filterSubProjects } from './../../util';
import ActionButton from './SiderActionButton';
import { useFavorisList } from './utils/useFavoris';
import { useProjectList } from './utils/useProject';

const computeProjetTypeLabel = (project: any, useMatriceResponsabilite: boolean): string => {
  return useMatriceResponsabilite
    ? ''
    : project && project.typeProject === TypeProject.Groupement
    ? 'Groupe'
    : project.typeProject === TypeProject.Professionnel
    ? 'Pro'
    : project.typeProject === TypeProject.Personnel
    ? 'Perso'
    : '';
};

interface SidebarProps {
  projectList: any;
  setActionListFilters: (value: any) => void;
  parameters: any[];
  setShowMainContent?: (value: boolean) => void;
  menuFilters?: any;
  termologie: string;
  useMatriceResponsabilite: boolean;
  urgence: UrgenceInterface[] | null | undefined;
  setUrgence: (urgence: UrgenceInterface[] | null | undefined) => void;
  importances: ImportanceInterface[] | null | undefined;
  setImportances: (importances: ImportanceInterface[] | null | undefined) => void;
  setTriage: (triage: any) => void;
}

const Sider: React.FC<SidebarProps & CountTodosProps> = ({
  setActionListFilters,
  refetchCountTodos,
  countTodos,
  projectList,
  parameters,
  setShowMainContent,
  menuFilters,
  termologie,
  useMatriceResponsabilite,
  urgence,
  setUrgence,
  importances,
  setImportances,
  setTriage,
}) => {
  const [openAddProject, setOpenAddProject] = useState<boolean>(false);
  const handleAddProject = () => {
    setOpenAddProject(!openAddProject);
  };
  const { params, redirectTo } = useTodoURLParams();

  const { filterId }: any = useParams();
  const { push } = useHistory();
  const { user } = useApplicationContext();

  const handleFavorisClick = (favoris: any) => {
    if (favoris.onClick) {
      favoris.onClick();
    }
  };

  const handleProjectClick = (project: any) => {
    redirectTo({ projet: [project.id], assignee: undefined, date: undefined, type: undefined });
  };

  // state of modals (open or not)
  const [openAddToFavorite, setOpenAddToFavorite] = useState<boolean>(false);
  const [openShare, setOpenShare] = useState<boolean>(false);

  const [selectedUsersIds, setselectedUsersIds] = useState<string[]>([]);
  // Modals state handler
  const addToFavoriteclick = () => {
    setOpenAddToFavorite(!openAddToFavorite);
  };

  const [clickedRow, setClickedRow] = useState<any>();

  const [switchProjectFavs, switchProjectFavsLoading] = useUpdateProject(setOpenAddToFavorite, clickedRow);

  const [favorisMenuItems] = useFavorisList(addToFavoriteclick);

  const switchFavs = (clickedRow: any) => {
    if (clickedRow && clickedRow.type === 'project') {
      return switchProjectFavs(clickedRow);
    }
  };

  const [projectMenuItems] = useProjectList(addToFavoriteclick, clickedRow);

  const isTodayOnly = params.assignee?.includes('me') && params.date === 'today' ? true : false;
  const isOnAujourhduiEquipe: boolean = params.assignee?.includes('team') && params.date === 'today' ? true : false;
  const isOnSeptJours: boolean = params.assignee?.includes('me') && params.date === 'next' ? true : false;
  const isOnSeptJoursEquipe: boolean = params.assignee?.includes('team') && params.date === 'next' ? true : false;
  const niveauMatriceFonctions = useNiveauMatriceFonctions();
  const statusLabel = params.type?.includes('DONE')
    ? '(terminée(s))'
    : params.type?.includes('ACTIVE')
    ? '(à faire)'
    : '';

  const favorisList =
    menuFilters && menuFilters.equipe
      ? [
          {
            icon: <Today />,
            title: `Aujourd’hui de l’équipe ${statusLabel}`,
            count: countTodos?.todayTeam + countTodos?.pastTeam || 0,
            path: 'aujourdhui-equipe',
            isActive: isOnAujourhduiEquipe,
            onClick: () => () => redirectTo({ assignee: ['team'], date: 'today', projet: undefined }),
          },
          {
            icon: <DateRange />,
            title: `Prochains jours de l’équipe ${statusLabel}`,
            count: countTodos?.nextTeam || 0,
            path: 'sept-jours-equipe',
            isActive: isOnSeptJoursEquipe,
            onClick: () => redirectTo({ assignee: ['team'], date: 'next', projet: undefined }),
          },
        ]
      : [
          {
            icon: <Today />,
            title: `Aujourd’hui ${statusLabel}`,
            count: countTodos?.todayMe + countTodos?.pastMe || 0,
            path: 'aujourdhui',
            isActive: isTodayOnly,
            onClick: () => redirectTo({ assignee: ['me'], date: 'today', projet: undefined }),
          },
          {
            icon: <DateRange />,
            title: `Prochains jours ${statusLabel}`,
            count: countTodos?.nextMe || 0,
            path: 'sept-jours',
            isActive: isOnSeptJours,
            onClick: () => redirectTo({ assignee: ['me'], date: 'next', projet: undefined }),
          },
        ];

  const projectResult = projectList?.data?.search?.data || [];

  const filteredProject = filter(projectResult, (project) => project.isArchived === false);

  const projectParents = _.filter(filteredProject || [], (project) => project.projetParent === null);
  const projectParentIds = projectParents.map((parent) => parent.id);

  let listFilterProjects = (niveauMatriceFonctions === 1
    ? projectParents
    : [
        ...projectParents,
        ...filteredProject.filter(
          (project) =>
            project.projetParent && project.projetParent.id && !projectParentIds.includes(project.projetParent.id)
        ),
      ]
  ).sort((p1, p2) => p1.ordre - p2.ordre);

  if (niveauMatriceFonctions === 1) {
    listFilterProjects = listFilterProjects.sort((p1, p2) => p2.typeProject.localeCompare(p1.typeProject));
  }

  const allFavoris = concat(filter(filteredProject, (project) => project.isInFavoris === true));

  const favorisDropDown = {
    title: 'Favoris',
    element:
      allFavoris &&
      allFavoris.map((favoris) => ({
        id: favoris && favoris.id,
        actionButton: (
          <ActionButton
            row={favoris}
            menuItems={favorisMenuItems}
            setClickedRow={setClickedRow}
            refetchCountTodos={refetchCountTodos}
          />
        ),
        title: favoris && favoris._name,
        typeName: favoris && favoris.__typename,
        count: favoris && favoris.nbAction,
        type: computeProjetTypeLabel(favoris, useMatriceResponsabilite),
        icon:
          favoris && favoris.isShared ? (
            <SupervisedUserCircle htmlColor={(favoris && favoris.couleur && favoris.couleur.code) || '#A3B1B0'} />
          ) : !favoris.typeProject ? (
            <LocalOffer htmlColor={(favoris && favoris.couleur && favoris.couleur.code) || '#DF0A6E'} />
          ) : (
            <FiberManualRecord htmlColor={(favoris && favoris.couleur && favoris.couleur.code) || '#A3B1B0'} />
          ),
        subProjects:
          favoris.__typename === 'Project'
            ? filterSubProjects(favoris, useMatriceResponsabilite, user, true)
                .sort((p1: any, p2: any) => p1.ordre - p2.ordre)
                .map((subProject: any) => ({
                  id: subProject && subProject.id,
                  typeName: 'Project',
                  actionButton: (
                    <ActionButton
                      row={subProject}
                      menuItems={projectMenuItems}
                      setClickedRow={setClickedRow}
                      refetchCountTodos={refetchCountTodos}
                    />
                  ),
                  title: subProject && subProject._name,
                  count: subProject && subProject.nbAction,
                  type: computeProjetTypeLabel(subProject, useMatriceResponsabilite),
                  isActive: filterId === subProject && subProject.id,
                  icon:
                    subProject && subProject.isShared ? (
                      <SupervisedUserCircle
                        htmlColor={(subProject && subProject.couleur && subProject.couleur.code) || '#A3B1B0'}
                      />
                    ) : (
                      <FiberManualRecord
                        htmlColor={(subProject && subProject.couleur && subProject.couleur.code) || '#A3B1B0'}
                      />
                    ),
                }))
            : null,
      })),
    withAddAction: false,
  };

  const projectDropDown = {
    title: `${capitalizeFirstLetter(termologie)}s`,
    element: listFilterProjects.map((project) => ({
      id: project && project.id,
      typeName: 'Project',
      actionButton: (
        <ActionButton
          row={project}
          menuItems={projectMenuItems}
          setClickedRow={setClickedRow}
          refetchCountTodos={refetchCountTodos}
        />
      ),
      title: project && project._name,
      count: project && project.nbAction,
      type: computeProjetTypeLabel(project, useMatriceResponsabilite),
      isActive: filterId === project && project.id,
      icon:
        project && project.isShared ? (
          <SupervisedUserCircle htmlColor={(project && project.couleur && project.couleur.code) || '#A3B1B0'} />
        ) : (
          <FiberManualRecord htmlColor={(project && project.couleur && project.couleur.code) || '#A3B1B0'} />
        ),
      subProjects:
        niveauMatriceFonctions === 1
          ? []
          : filterSubProjects(project, useMatriceResponsabilite, user, true)
              .sort((p1: any, p2: any) => p1.ordre - p2.ordre)
              .map((subProject: any) => ({
                id: subProject && subProject.id,
                typeName: 'Project',
                actionButton: (
                  <ActionButton
                    row={subProject}
                    menuItems={projectMenuItems}
                    setClickedRow={setClickedRow}
                    refetchCountTodos={refetchCountTodos}
                  />
                ),
                title: subProject && subProject._name,
                count: subProject && subProject.nbAction,
                type: computeProjetTypeLabel(subProject, useMatriceResponsabilite),
                isActive: filterId === subProject && subProject.id,
                icon:
                  subProject && subProject.isShared ? (
                    <SupervisedUserCircle
                      htmlColor={(subProject && subProject.couleur && subProject.couleur.code) || '#A3B1B0'}
                    />
                  ) : (
                    <FiberManualRecord
                      htmlColor={(subProject && subProject.couleur && subProject.couleur.code) || '#A3B1B0'}
                    />
                  ),
              })),
    })),
    addAction: handleAddProject,
    withAddAction: !useMatriceResponsabilite,
  };

  return (
    <>
      <FavorisHeader
        headerList={favorisList}
        handleFavorisClick={handleFavorisClick}
        setShowMainContent={setShowMainContent}
      />
      <DropdownDraggable
        list={favorisDropDown}
        clickable={false}
        niveauMatriceFonctions={niveauMatriceFonctions}
        defaultExpanded={true}
        handleProjectClick={handleProjectClick}
        handleSubProjectClick={handleProjectClick}
        setShowMainContent={setShowMainContent}
      />

      <DropdownDraggable
        disabled={useMatriceResponsabilite && niveauMatriceFonctions === 2}
        niveauMatriceFonctions={niveauMatriceFonctions}
        list={projectDropDown}
        handleProjectClick={handleProjectClick}
        handleSubProjectClick={handleProjectClick}
        clickable={false}
        setShowMainContent={setShowMainContent}
      />

      <UrgenceFilter withDropdown={true} urgence={urgence} setUrgence={setUrgence} />
      <ImportanceFilter importances={importances} setImportances={setImportances} />

      <ConfirmDialog
        open={openAddToFavorite}
        message={
          clickedRow && clickedRow.isInFavoris
            ? 'Êtes-vous sûr de vouloir retirer des favoris ?'
            : 'Êtes-vous sûr de vouloir ajouter au favoris ?'
        }
        handleValidate={() => switchFavs(clickedRow)}
        handleClose={addToFavoriteclick}
        loading={switchProjectFavsLoading}
      />
      <AjoutParticipant
        openModalParticipant={openShare}
        setOpenParticipant={setOpenShare}
        userIds={selectedUsersIds}
        setUserIds={setselectedUsersIds}
        idProjectTask={(clickedRow && clickedRow.id) || ''}
      />
      <Backdrop open={projectList && projectList.loading} />
    </>
  );
};
export default Sider;
