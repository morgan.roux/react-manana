import React from 'react';
import RetirerFavoris from '../../../../../assets/icons/todo/retirer_favoris.svg';
import { ActionButtonMenu } from '../../../Common/ActionButton';
export const useFavorisList = (removeFavoriteClick: (row: any) => void) => {
  const favorisMenuItems: ActionButtonMenu[] = [
    {
      label: 'Retirer des favoris',
      icon: <img src={RetirerFavoris} />,
      onClick: removeFavoriteClick,
      disabled: false,
    },
  ];
  return [favorisMenuItems];
};
