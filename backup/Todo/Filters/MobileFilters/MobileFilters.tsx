import { Box, Hidden, IconButton, ListItem, ListItemText, Typography } from '@material-ui/core';
import { Close, Inbox } from '@material-ui/icons';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { useTodoURLParams } from '../../hooks/useTodoURLParams';
import { CountTodosProps } from '../../Todo';
import Content from '../Content';
import useStyles from './styles';

interface MobileFiltersProps {
  onClose: (close: any) => void;
}

const MobileFilters: FC<MobileFiltersProps & RouteComponentProps & CountTodosProps> = (props) => {
  const classes = useStyles({});

  const { countTodos, onClose } = props;
  const { params, redirectTo } = useTodoURLParams();

  const taskStatus = [
    {
      nom: 'A faire',
      checked: params.type?.includes('ACTIVE'),
      value: 'ACTIVE',
      count: params.assignee?.includes('me') ? countTodos?.active : countTodos?.activeTeam,
    },
    {
      nom: 'Terminée(s)',
      checked: params.type?.includes('DONE'),
      value: 'DONE',
      count: params.assignee?.includes('me') ? countTodos?.done : countTodos?.doneTeam,
    },
    {
      nom: 'Tout',
      checked: params.type?.length === 0 || !params.type,
      value: 'ALL',
      count: params.assignee?.includes('me')
        ? countTodos?.active + countTodos?.done
        : countTodos?.activeTeam + countTodos?.doneTeam,
    },
  ];

  const title = 'Filtre To-Do';

  const handleClose = () => {
    onClose(false);
  };

  const handleStatus = (values: any) => {
    const checked = values.find((el: any) => el.checked);

    redirectTo(
      params.assignee?.includes('me')
        ? { type: checked.value === 'ALL' ? undefined : [checked.value], assignee: ['me'] }
        : { type: checked.value === 'ALL' ? undefined : [checked.value], assignee: ['team'] }
    );
  };

  const handleClickAll = () => {
    redirectTo({ type: undefined, date: undefined });
  };

  return (
    <Box display="flex" flexDirection="column" width="100%">
      <Hidden mdUp={true} implementation="css">
        <Box className={classes.topBar}>
          <Typography color="inherit">{title}</Typography>
          <IconButton color="inherit" onClick={handleClose}>
            <Close />
          </IconButton>
        </Box>
      </Hidden>
      <Content title="Status tâches" datas={taskStatus} updateDatas={handleStatus} useRadios={true} />
      <Box borderBottom="1px solid rgb(227, 227, 227)" marginBottom="16px"></Box>

      <ListItem className={classes.listItem} role={undefined} dense={true} button={true} onClick={handleClickAll}>
        <Box width="100%" display="flex" alignItems="center" justifyContent="space-between">
          <Box display="flex" alignItems="center">
            <Inbox color="secondary" />
            <ListItemText
              primary={
                params.assignee?.includes('me') ? 'Voir toutes mes tâches' : "Voir toutes les tâches de l'équipe"
              }
            />
          </Box>
          {/* <Typography className={classes.count}>
            {params.assignee?.includes('me' ? countTodos?.totalMe : countTodos?.totalTeam)}
          </Typography> */}
        </Box>
      </ListItem>
    </Box>
  );
};

export default withRouter(MobileFilters);
