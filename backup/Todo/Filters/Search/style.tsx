import { createStyles, fade, Theme, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    search: {
      display: 'flex',
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: theme.palette.common.white,
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.97),
      },
      width: 'auto',
      '& .MuiInputBase-root': {
        paddingTop: '0!important',
        paddingBottom: '0!important',
      },
    },
    searchIcon: {
      width: 36,
      height: '100%',
      position: 'absolute',
      right: '0',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputInput: {
      padding: '15px 36px 15px 8px',
      width: '100%',
      fontSize: '0.75rem!important',
      '&::placeholder': {
        fontSize: '0.75rem!important',
      },
    },
    inputRoot: {
      color: 'inherit',
      width: '100%',
    },
    option: {
      fontSize: 15,
      '& > span': {
        marginRight: 10,
        fontSize: 18,
      },
      padding: 5,
    },
    taskDetailsModal: {
      '& .MuiDialog-paperWidthMd': {
        maxWidth: '762px !important',
        minHeight: '700px !important',
      },
      '& .MuiListItemIcon-root': {
        minWidth: '35px !important',
      },
    },
    popupIndicatorOpen: {
      transform: 'rotate(0deg)',
    },
  })
);
