import { isMobile, useApplicationContext, useDisplayNotification } from '@lib/common';
import { useUpdate_Item_Scoring_For_ItemLazyQuery } from '@lib/common/src/federation';
import { useSearch_ActionLazyQuery, useSoft_Delete_ActionsMutation } from '@lib/common/src/graphql';
import { Autocomplete, CustomModal } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import { debounce } from 'lodash';
import React, { FC, ReactNode, useRef, useState } from 'react';
import AjoutTaches from '../../Modals/AjoutTache';
import TaskDetails from '../../Task/TaskDetails';
import { useStyles } from './style';

interface SearchContentProps {
  placeholder: string;
  refetchAll?: () => void;
  popupIcon?: ReactNode;
}

const SearchContent: FC<SearchContentProps> = ({ placeholder, refetchAll, popupIcon }) => {
  const classes = useStyles({});
  const { graphql, federation, currentGroupement: groupement } = useApplicationContext();

  const [todos, setTodos] = React.useState<any[]>([]);
  const displayNotification = useDisplayNotification();

  const [openModalTache, setOpenModalTache] = useState<boolean>(false);

  const [open, setOpen] = React.useState<boolean>(false);
  const [clickedTask, setClickedTask] = React.useState<any | null>(null);

  const [doSearchAction, resultActions] = useSearch_ActionLazyQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
  });

  React.useEffect(() => {
    if (
      resultActions &&
      resultActions.data &&
      resultActions.data.search &&
      resultActions.data.search.data &&
      resultActions.data.search.data.length
    ) {
      // Execute query get message
      setTodos(resultActions.data.search.data);
    }
  }, [resultActions]);

  const debouncedSearchUser = useRef(
    debounce((text: string) => {
      if (!text) setTodos([]);
      else
        doSearchAction({
          variables: {
            type: ['action'],
            query: {
              query: {
                bool: {
                  must: [
                    {
                      query_string: {
                        query: text.startsWith('*') || text.endsWith('*') ? text : `*${text}*`,
                        analyze_wildcard: true,
                      },
                    },
                    {
                      term: { isRemoved: false },
                    },
                  ],
                  should: [
                    { term: { 'assignedUsers.idGroupement': groupement && groupement.id } },

                    { term: { 'userCreation.idGroupement': groupement && groupement.id } },
                  ],
                  minimum_should_match: 1,
                },
              },
            },
          },
        });
    }, 1000)
  );

  const handleChangeAutocomplete = (name: string) => (e: React.ChangeEvent<any>, value: any) => {
    debouncedSearchUser.current(value);
  };

  const handleChange = (name: string) => (e: React.ChangeEvent<any>, value: any) => {
    if (value) {
      setClickedTask(value);
      if (isMobile()) {
        setOpenModalTache(true);
      } else {
        setOpen(true);
      }
    }
  };

  const [updateItemScoring, updatingItemScoring] = useUpdate_Item_Scoring_For_ItemLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  // Mutation delete tasks
  const [doSoftDeleteActions, doingSoftDeleteActions] = useSoft_Delete_ActionsMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.softDeleteActions) {
        updateItemScoring({
          variables: {
            idItemAssocie: data.softDeleteActions[0].id,
            codeItem: 'L',
          },
        });
        displayNotification({
          type: 'success',
          message: 'Suppression effectuée avec succès !!!',
        });
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const handleRefetchAll = () => {
    if (refetchAll) refetchAll();
    setOpen(false);
  };

  return (
    <>
      <Box className={classes.search}>
        <Autocomplete
          value={''}
          name="todo"
          multiple={false}
          variant="outlined"
          handleChange={handleChange}
          handleInputChange={handleChangeAutocomplete}
          options={todos}
          optionLabel="description"
          placeholder={placeholder}
          style={{ width: '100%' }}
          noOptionsText={'Aucune tâche correspondant'}
          popupIcon={popupIcon}
          popupIndicatorOpen={classes.popupIndicatorOpen}
        />
      </Box>
      <AjoutTaches
        openModalTache={openModalTache}
        setOpenTache={setOpenModalTache}
        task={clickedTask}
        operationName="EDIT"
        refetchAll={refetchAll}
      />
      {open && (
        <CustomModal
          open={open}
          setOpen={setOpen}
          title="Détail de la tâche"
          withBtnsActions={false}
          closeIcon={true}
          headerWithBgColor={true}
          maxWidth="md"
          fullWidth={true}
          className={classes.taskDetailsModal}
        >
          {clickedTask && clickedTask.id && (
            <TaskDetails task={clickedTask} doSoftDeleteActions={doSoftDeleteActions} refetchAll={handleRefetchAll} />
          )}
        </CustomModal>
      )}
    </>
  );
};

export default SearchContent;
