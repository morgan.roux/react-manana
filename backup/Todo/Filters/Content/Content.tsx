import { Box, Checkbox, ListItem, ListItemText, Radio, Typography } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import useStyles from './styles';

interface ContentProps {
  title: string;
  datas: any;
  updateDatas: (list: any) => void;
  useRadios?: boolean;
}
const Content: FC<ContentProps & RouteComponentProps<any, any, any>> = ({ title, datas, updateDatas, useRadios }) => {
  const classes = useStyles({});

  const onGetValue = (itemIndex: number) => {
    const newList = datas.map((item: any, index: number) => {
      if (itemIndex === index) {
        return { ...item, checked: !item.checked };
      }
      return item;
    });

    updateDatas(newList);
  };

  const onGetRadioValue = (itemIndex: number) => {
    const newList = datas.map((item: any, index: number) => {
      if (itemIndex === index) {
        item.checked = true;
        return item;
      } else {
        item.checked = false;
        return item;
      }
    });
    updateDatas(newList);
  };

  return (
    <Box borderTop="1px solid #E3E3E3" padding="16px 0" overflow="auto">
      {useRadios ? (
        <Box className={classes.noStyle}>
          {datas.map(
            (item: any, index: number) =>
              item && (
                <ListItem
                  role={undefined}
                  dense={true}
                  button={true}
                  key={`${index}-${item.nom}`}
                  onClick={() => onGetRadioValue(index)}
                  disableGutters={true}
                  style={{ paddingLeft: 0 }}
                >
                  <Box display="flex" alignItems="center" width="100%">
                    <Radio checked={item.checked} tabIndex={-1} disableRipple={true} />
                    <ListItemText primary={item.nom} />
                  </Box>
                  {
                    <Box>
                      <Typography className={classes.nbrProduits}>{item.count}</Typography>
                    </Box>
                  }
                </ListItem>
              )
          )}
        </Box>
      ) : (
        <Box className={classes.noStyle}>
          {datas.map(
            (item: any, index: number) =>
              item &&
              !item.hidden && (
                <ListItem
                  role={undefined}
                  dense={true}
                  button={true}
                  onClick={() => (!item.disable ? onGetValue(index) : {})}
                  key={`${index}-${item.nom}`}
                  disabled={!item.disable ? false : true}
                  style={{ paddingLeft: 0 }}
                >
                  <Box className={classes.checkBoxLeftName} display="flex" alignItems="center">
                    <Checkbox
                      tabIndex={-1}
                      checked={item.checked}
                      disableRipple={true}
                      disabled={!item.disable ? false : true}
                    />
                    <ListItemText className={classes.nom} primary={item.nom} />
                  </Box>

                  {
                    <Box>
                      <Typography className={classes.nbrProduits}>{item.count}</Typography>
                    </Box>
                  }
                </ListItem>
              )
          )}
        </Box>
      )}
    </Box>
  );
};
export default withRouter(Content);
