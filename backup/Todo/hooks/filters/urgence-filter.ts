import { MeUserInfoFragment, UrgenceInfoFragment } from '@lib/common/src/federation';
import { Filter } from '../util/types';
import { TodoURLParams } from '../useTodoURLParams';

const isCoupleUrgence = (urgences: UrgenceInfoFragment[], code1: string, code2: string): boolean => {
  if (urgences.length === 2) {
    return urgences.some(({ code }) => code === code1) && urgences.some(({ code }) => code === code2);
  }

  return false;
};

export const buildUrgenceFilter = (
  user: MeUserInfoFragment,
  params: TodoURLParams,
  urgences: UrgenceInfoFragment[]
): Filter => {
  if (params.urgence && urgences.length > 0 && params.urgence.length > 0 && params.urgence.length < urgences.length) {
    if (params.urgence.length === 1) {
      const activeUrgence = urgences.find(({ id }) => id === (params as any).urgence[0]);
      if (activeUrgence) {
        if (activeUrgence.code === 'B') {
          return {
            must: [{ range: { dateDebut: { gte: 'now+1d/d' } } }, { range: { dateDebut: { lt: 'now+7d/d' } } }],
          };
        }
        if (activeUrgence.code === 'C') {
          return {
            must: [{ range: { dateDebut: { gte: 'now+13d/d' } } }],
          };
        }

        return {
          // A
          must: [{ range: { dateDebut: { lte: 'now/d' } } }],
        };
      }
    } // End one filter
    else {
      const activeUrgences = urgences.find(({ id }) => params.urgence?.includes(id)) as any;

      // 2 filter
      if (isCoupleUrgence(activeUrgences, 'A', 'B')) {
        // Journée-semaine
        return {
          must: [{ range: { dateDebut: { lte: 'now+7d/d' } } }],
        };
      }

      if (isCoupleUrgence(activeUrgences, 'A', 'C')) {
        // Journée-mois
        return {
          must: [{ range: { dateDebut: { gte: 'now/d' } } }],
        };
      }

      if (isCoupleUrgence(activeUrgences, 'B', 'C')) {
        // semaine-mois
        return {
          must: [{ range: { dateDebut: { gte: 'now+1d/d' } } }],
        };
      }
    }
  }

  return {};
};
