import { endTime, startTime } from '@lib/common';
import moment from 'moment';
import { TodoURLParams, TodoURLParamsOptions } from '../useTodoURLParams';
import { Filter } from '../util/types';

export const buildDateFilter = (params: TodoURLParams, options?: TodoURLParamsOptions): Filter => {
  if (params.date === 'past') {
    return {
      must: [{ range: { dateDebut: { lt: 'now/d' } } }, { exists: { field: 'dateDebut' } }],
    };
  } else if (params.date === 'today') {
    if (options?.includePast) {
      return {
        must: [{ range: { dateDebut: { lt: 'now+1d/d' } } }, { exists: { field: 'dateDebut' } }],
      };
    }

    return {
      must: [
        { range: { dateDebut: { gte: 'now/d' } } },
        { range: { dateDebut: { lt: 'now+1d/d' } } },
        { exists: { field: 'dateDebut' } },
      ],
    };
  } else if (params.date === 'next') {
    return {
      must: [{ range: { dateDebut: { gt: 'now/d' } } }, { exists: { field: 'dateDebut' } }],
    };
  } else if (params.date) {
    const dateDebut = moment(params.date, 'YYYY-MM-DD');

    const should = [
      {
        bool: {
          must: [
            {
              exists: { field: 'dateFin' },
            },
            {
              range: {
                dateDebut: {
                  gte: startTime(dateDebut),
                },
              },
            },
            {
              range: {
                dateFin: {
                  lte: endTime(dateDebut),
                },
              },
            },
          ],
        },
      },

      {
        bool: {
          must_not: [
            {
              exists: { field: 'dateFin' },
            },
          ],
          must: [
            {
              range: {
                dateDebut: {
                  gte: startTime(dateDebut),
                  lte: endTime(dateDebut),
                },
              },
            },
          ],
        },
      },
    ];

    return {
      should,
    };
  }

  return {};
};
