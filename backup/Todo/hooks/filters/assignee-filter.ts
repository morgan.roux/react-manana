import { MeUserInfoFragment } from '@lib/common/src/federation';
import { Filter } from '../util/types';
import { TodoURLParams } from '../useTodoURLParams';

export const buildAssigneeFilter = (user: MeUserInfoFragment, params: TodoURLParams): Filter => {
  if (!params.assignee) {
    return {
      should: [
        {
          term: {
            teamIdUsers: user.id,
          },
        },
        {
          term: {
            todoIdUsers: user.id,
          },
        },
      ],
    };
  }
  if (params.assignee.includes('me')) {
    return {
      must: [
        {
          term: {
            todoIdUsers: user.id,
          },
        },
      ],
    };
  }

  if (params.assignee.includes('team')) {
    return {
      must: [
        {
          term: {
            teamIdUsers: user.id,
          },
        },
      ],
    };
  }

  return {
    must: [
      {
        terms: {
          todoIdUsers: params.assignee,
        },
      },
    ],
  };
};
