import { useApplicationContext, useURLSearchParams } from '@lib/common';
import { useGet_UrgencesQuery } from '@lib/common/src/federation';
import { UrgenceInfoFragment } from '@lib/common/src/graphql';
import { useCallback, useMemo } from 'react';
import { useHistory } from 'react-router';
import { buildTodoActionsQuery } from './util/query';

export interface TodoURLParams {
  date?: 'today' | 'past' | 'next';
  assignee?: ('me' | 'team' | string)[];
  type?: ('ACTIVE' | 'DONE')[];
  projet?: string[];
  urgence?: string[];
  importance?: string[];
  sorting?: Record<string, { order: 'asc' | 'desc' }>[];
}

export interface TodoURLParamsOptions {
  enableSort?: boolean;
  includePast?: boolean;
}
export interface UseTodoURLParamsResult {
  params: TodoURLParams;
  redirectTo: (params?: TodoURLParams) => void;
  getActionsQuery: (changedParams?: TodoURLParams, options?: TodoURLParamsOptions) => any;
}
const encodeParam = (param: any, isObjectArray?: boolean): string | undefined => {
  if (Array.isArray(param) && param.length > 0) {
    return isObjectArray
      ? encodeURIComponent(param.map((el) => JSON.stringify(el)).join(','))
      : encodeURIComponent(param.join(','));
  } else if (param) {
    return encodeURIComponent(param);
  }

  return undefined;
};

const decodeParam = (
  params: URLSearchParams,
  name: keyof TodoURLParams,
  isArray: boolean,
  isObjectArray?: boolean
): any | undefined => {
  const value = params.get(name);
  if (value) {
    const decodedValue = decodeURIComponent(value);
    if (isObjectArray) {
      return JSON.parse(decodedValue);
    }
    if (isArray) {
      return decodedValue.split(',');
    }
    return decodedValue;
  }

  return undefined;
};

const basePath = '/todo';
export const useTodoURLParams = (): UseTodoURLParamsResult => {
  const query = useURLSearchParams();
  const history = useHistory();
  const { user, federation } = useApplicationContext();
  const loadingUrgences = useGet_UrgencesQuery({ client: federation });

  const urgences: UrgenceInfoFragment[] = loadingUrgences.data?.urgences.nodes || [];

  const params: TodoURLParams = useMemo(
    () => ({
      assignee: decodeParam(query, 'assignee', true),
      type: decodeParam(query, 'type', true),
      projet: decodeParam(query, 'projet', true),
      urgence: decodeParam(query, 'urgence', true),
      importance: decodeParam(query, 'importance', true),
      sorting: decodeParam(query, 'sorting', true, true),
      date: decodeParam(query, 'date', false),
    }),
    [query]
  );

  const redirectTo = useCallback(
    (changedParams?: TodoURLParams) => {
      const newParams: any = {
        ...params,
        ...(changedParams || {}),
      };

      const objectArrayParams = ['sorting'];

      const newUrl = Object.keys(newParams).reduce((url, paramName: any) => {
        const newValue = newParams[paramName];
        if (!newValue) {
          return url;
        }
        const encodedValue = encodeParam(newValue, objectArrayParams.includes(paramName) ? true : false);
        if (!encodedValue) {
          return url;
        }

        const keyValue = `${paramName}=${encodedValue}`;

        const toAppend = !url.includes('?') ? `?${keyValue}` : `&${keyValue}`;

        return `${url}${toAppend}`;
      }, `${basePath}`);

      history.push(newUrl);
    },
    [params]
  );

  const getActionsQuery = useCallback(
    (changedParams?: TodoURLParams, options?: TodoURLParamsOptions) => {
      return buildTodoActionsQuery({
        options,
        urgences: urgences as any,
        user,
        params: changedParams ? { ...params, ...changedParams } : params,
      });
    },
    [params]
  );

  return {
    params,
    getActionsQuery,
    redirectTo,
  };
};
