import { MeUserInfoFragment } from '@lib/common/src/federation';
import { TodoURLParams } from '../useTodoURLParams';

export const buildSort = (user: MeUserInfoFragment, params: TodoURLParams): any | undefined => {
  if (params.sorting) {
    return params.sorting;
  }
  return undefined;
};
