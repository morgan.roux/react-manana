import { MeUserInfoFragment, UrgenceInfoFragment } from '@lib/common/src/federation';
import { buildAssigneeFilter } from '../filters/assignee-filter';
import { buildDateFilter } from '../filters/date-filter';
import { buildImportanceFilter } from '../filters/importance-filter';
import { buildProjetFilter } from '../filters/projet-filter';
import { buildTypeFilter } from '../filters/type-filter';
import { buildUrgenceFilter } from '../filters/urgence-filter';
import { buildSort } from '../sorting/sort';
import { TodoURLParams, TodoURLParamsOptions } from '../useTodoURLParams';
import { commonTerms } from './constants';
import { mergeFilters } from './merge-filter';
import { Filter } from './types';

export const buildQueryFilters = ({ user, params, urgences, options }: BuildTodoActionsQueryParams): Filter => {
  return mergeFilters([
    { must: commonTerms },
    buildDateFilter(params, options),
    buildAssigneeFilter(user, params),
    buildImportanceFilter(user, params),
    buildProjetFilter(user, params),
    buildTypeFilter(user, params),
    buildUrgenceFilter(user, params, urgences),
  ]);
};

interface BuildTodoActionsQueryParams {
  urgences: UrgenceInfoFragment[];
  user: MeUserInfoFragment;
  params: TodoURLParams;
  options?: TodoURLParamsOptions;
}

export const buildTodoActionsQuery = ({ user, params, urgences, options }: BuildTodoActionsQueryParams) => {
  const { should, must } = buildQueryFilters({ user, params, urgences, options });
  const sortingQuery = buildSort(user, params);
  const sort = options?.enableSort && sortingQuery ? sortingQuery : undefined;

  return {
    sort,
    query: {
      bool: {
        should,
        minimum_should_match: (should?.length || 0) > 1 ? 1 : undefined,
        must,
      },
    },
  };
};
