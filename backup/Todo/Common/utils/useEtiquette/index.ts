import { useDeleteEtiquette } from './useDeleteEtiquette';
import { useUpdateEtiquette } from './useUpdateEtiquette';
export { useUpdateEtiquette, useDeleteEtiquette };
