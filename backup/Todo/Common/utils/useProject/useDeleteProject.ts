import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useDelete_ProjectMutation } from '@lib/common/src/graphql';

export const useDeleteProject = (
  queryVariable: any,
  setOpenDeleteDialog: (value: boolean) => any,
  refetch: any,
  refetchCountTodos?: (variables?: any) => void
) => {
  const { graphql } = useApplicationContext();
  const displayNotification = useDisplayNotification();
  const [removeMutation, { loading: deleteProjectLoading, error: deleteProjectError }] = useDelete_ProjectMutation({
    client: graphql,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
    onCompleted: (data) => {
      if (data && data.softDeleteProjects) {
        if (refetch) refetch();
        if (refetchCountTodos) refetchCountTodos();
        displayNotification({
          type: 'success',
          message: 'Suppression réussie',
        });
      }
      setOpenDeleteDialog(false);
    },
  });

  const removeProject = (row: any) => {
    removeMutation({
      variables: {
        ids: [row.id],
      },
    });
  };
  return [removeProject];
};
