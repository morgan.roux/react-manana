import { useApplicationContext, useDisplayNotification } from '@lib/common';
import {
  Search_ActionDocument,
  Search_Custom_Content_ProjectDocument,
  useCreate_ProjectMutation,
  useDelete_ActionsMutation,
  useDelete_ProjectMutation,
} from '@lib/common/src/graphql';
import { Backdrop, ConfirmDeleteDialog, ConfirmDialog } from '@lib/ui-kit';
import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Add, Delete, Edit, Favorite, MoreHoriz, Person } from '@material-ui/icons';
import ArchiveIcon from '@material-ui/icons/Archive';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import EventIcon from '@material-ui/icons/Event';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import FlagIcon from '@material-ui/icons/Flag';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import SortByAlphaIcon from '@material-ui/icons/SortByAlpha';
import ViewDayIcon from '@material-ui/icons/ViewDay';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import { createStyles, makeStyles } from '@material-ui/styles';
import { differenceBy } from 'lodash';
import React, { FC, ReactNode, useState } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import AjouterDessous from '../../../assets/icons/todo/ajouter_dessous.svg';
import AjouterDessus from '../../../assets/icons/todo/ajouter_dessus.svg';
import Archiver from '../../../assets/icons/todo/archiver.svg';
import RetirerFavoris from '../../../assets/icons/todo/retirer_favoris.svg';
import AjoutParticipant from '../Modals/AjoutParticipants';
import AjoutSectionModal from '../Modals/AjoutSection';
import AjoutTaches from '../Modals/AjoutTache/AjoutTaches';
import { CountTodosProps } from '../Todo';
import { useCreateUpdateSection, useSection } from './utils/useSection';

const useStyles = makeStyles(() =>
  createStyles({
    itemList: {
      fontSize: '0.875rem !important',
    },
  })
);
export interface ActionButtonMenu {
  label: any;
  icon: ReactNode;
  disabled: boolean;
  onClick: (event: any) => any;
  setState?: (row: any) => void;
  setOperationName?: (operationName: string) => void;
  hide?: boolean;
}

export interface ActionButtonProps {
  task?: any;
  row?: any; // this will remain optional till we get the shared etiquette
  actionMenu?: string;
  queryVariable?: any;
  variableTache?: any;
  refetch?: any;
  setTriage?: (triage: any[]) => void;
  refetchSection?: any;
  setCacheTacheAchevE?: (cacheTacheAchevE: boolean) => void;
  cacheTacheAchevE?: boolean;
  statutMainHeader?: boolean;
  parameters?: any;
  refetchAll?: () => void;
}

const Actionbutton: FC<ActionButtonProps & CountTodosProps> = ({
  actionMenu,
  row,
  statutMainHeader,
  queryVariable,
  variableTache,
  refetch,
  task,
  setTriage,
  refetchSection,
  setCacheTacheAchevE,
  cacheTacheAchevE,
  refetchCountTodos,
  parameters,
  refetchAll,
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const { filter, filterId }: any = useParams();
  const { pathname } = useLocation();
  const { user: currentUser, graphql, currentPharmacie } = useApplicationContext();
  const open = anchorEl ? true : false;
  const [openAddSection, setOpenAddSection] = React.useState(false);
  const [openModalTache, setOpenModalTache] = useState<boolean>(false);
  const [openDeleteDialogTask, setOpenDeleteDialogTask] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const displayNotification = useDisplayNotification();

  const [sectionValues, setSectionValues, handleSectionChange] = useSection();
  const [doCreateUpdateSection] = useCreateUpdateSection(
    sectionValues,
    refetchSection,
    filter,
    filterId,
    setSectionValues,
    setOpenAddSection
  );

  const [openShare, setOpenShare] = useState<boolean>(false);
  const [selectedUsersIds, setselectedUsersIds] = useState<string[]>([]);

  const [openAddToFavorite, setOpenAddToFavorite] = useState<boolean>(false);
  const [openArchived, setOpenArchived] = useState<boolean>(false);
  const [openProject, setOpenProject] = useState<boolean>(false);

  const handleTacheAchevE = () => {
    if (setCacheTacheAchevE) setCacheTacheAchevE(!cacheTacheAchevE);
  };

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    }
  };

  const handlesetOpenDialogDelete = () => {
    setOpenDeleteDialogTask(!openDeleteDialogTask);
  };

  const [order, setorder] = useState('asc');

  const handleFiltre = (e: any) => {
    setorder(order === 'asc' ? 'desc' : 'asc');
    if (setTriage) {
      switch (e) {
        case 1:
          setTriage([{ dateCreation: { order: order === 'asc' ? 'desc' : 'asc' } }]);
          break;
        case 2:
          setTriage([{ dateDebut: { order: order === 'asc' ? 'desc' : 'asc' } }]);
          break;
        case 3:
          setTriage([{ priority: { order: order === 'asc' ? 'desc' : 'asc' } }]);
          break;
        case 4:
          setTriage([{ description: { order: order === 'asc' ? 'desc' : 'asc' } }]);
          break;
        case 5:
          setTriage([{ isAssigned: { order: order === 'asc' ? 'desc' : 'asc' } }]);
          break;
        default:
          setTriage([{ dateCreation: { order: order === 'asc' ? 'desc' : 'asc' } }]);
          break;
      }
    }
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOpenModalTacheAction = () => {
    setOpenModalTache(!openModalTache);
  };

  const handleClickDeleteProject = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setOpenDeleteDialog(!openDeleteDialog);
  };

  const handleclickOpenEditProject = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setOpenProject(!openProject);
  };

  const handleOpenAddSection = (event: React.MouseEvent<HTMLElement>) => {
    setOpenAddSection(!openAddSection);
    handleClick(event);
  };

  const handleFavorite = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setOpenAddToFavorite(!openAddToFavorite);
  };

  const handleArchived = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setOpenArchived(!openArchived);
  };

  const myId = currentUser.id;

  const [switchProject] = useCreate_ProjectMutation({
    client: graphql,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
    onCompleted: (data) => {
      if (data && data.createUpdateProject) {
        displayNotification({
          type: 'success',
          message: 'Opération réussie',
        });
      }
      setOpenAddToFavorite(false);
      setOpenArchived(false);
    },
  });

  const projectInputVariables = {
    id: row && row.id,
    name: row && row._name,
    typeProject: row && row.typeProject,
    idUser: currentUser && currentUser.id,
    idPharmacie: currentPharmacie.id,
  };

  const switchFavs = (row: any) => {
    if (row && row.typeProject) {
      switchProject({
        variables: {
          input: {
            ...projectInputVariables,
            isFavoris: !(row && row.isInFavoris),
          },
        },
      });
    }
  };

  const switchArchived = (row: any) => {
    switchProject({
      variables: {
        input: {
          ...projectInputVariables,
          isArchived: !(row && row.isArchived),
        },
      },
    });
  };

  const [removeProject, { loading: deleteProjectLoading, error: deleteProjectError }] = useDelete_ProjectMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.softDeleteProjects) {
        if (queryVariable && queryVariable.projectVariable) {
          const req: any = cache.readQuery({
            query: Search_Custom_Content_ProjectDocument,
            variables: queryVariable && queryVariable.projectVariable,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.softDeleteProjects;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: Search_Custom_Content_ProjectDocument,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables: queryVariable && queryVariable.projectVariable,
            });
          }
        }
      }
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
    onCompleted: (data) => {
      if (data && data.softDeleteProjects) {
        if (refetchCountTodos) refetchCountTodos();
        displayNotification({
          type: 'success',
          message: 'Suppression réussie',
        });
      }
      setOpenDeleteDialog(false);
    },
  });

  const handleRemove = (row: any) => {
    if (row && row.type === 'project') {
      removeProject({
        variables: {
          ids: [row && row.id],
        },
      });
    }
  };

  const [removeTache, { loading: loadingDelete }] = useDelete_ActionsMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.softDeleteActions) {
        if (variableTache) {
          const req: any = cache.readQuery({
            query: Search_ActionDocument,
            variables: variableTache,
          });
          if (req && req.search && req.search.data) {
            const result = data.softDeleteActions;
            cache.writeQuery({
              query: Search_ActionDocument,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: result,
                  },
                },
              },
              variables: variableTache,
            });
          }
        }
      }
    },
    onCompleted: (data) => {
      displayNotification({
        type: 'success',
        message: 'Suppression effectuée avec succès',
      });
      if (refetch) refetch();
      if (refetchCountTodos) refetchCountTodos();
      // setdeletedId(null);
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const onClickConfirmDeleteTask = () => {
    removeTache({
      variables: {
        ids: task && task.id,
      },
    });
    setOpenDeleteDialogTask(!openDeleteDialogTask);
  };

  const handleOpenAjoutSectionDialog = () => {
    setOpenAddSection(true);
  };

  // FAVORIS: project + etiquette
  const favorisMenuItems: ActionButtonMenu[] = [
    { label: 'Modifier', icon: <Edit />, onClick: handleClick, disabled: false },
    {
      label: 'Retirer des favoris',
      icon: <img src={RetirerFavoris} />,
      onClick: handleFavorite,
      disabled: false,
    },
  ];

  // PROJECT
  const projectMenuItems: ActionButtonMenu[] = [
    {
      label: 'Créer un sous-projet',
      icon: <Add />,
      onClick: handleClick,
      disabled: false,
      hide:
        row && row.projetParent
          ? true
          : parameters &&
            parameters.length &&
            parameters.filter((parameter: any) => parameter.code === '0120' && parameter.value === 'true').length
          ? false
          : true,
    },
    {
      label: 'Ajouter un projet au-dessus',
      icon: <img src={AjouterDessus} />,
      onClick: handleClick,
      disabled: false,
      hide:
        row && row.projetParent
          ? true
          : parameters &&
            parameters.length &&
            parameters.filter((parameter: any) => parameter.code === '0120' && parameter.value === 'true').length
          ? false
          : true,
    },
    {
      label: 'Ajouter un projet au-dessous',
      icon: <img src={AjouterDessous} />,
      onClick: handleClick,
      disabled: false,
      hide:
        row && row.projetParent
          ? true
          : parameters &&
            parameters.length &&
            parameters.filter((parameter: any) => parameter.code === '0120' && parameter.value === 'true').length
          ? false
          : true,
    },
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: handleclickOpenEditProject,
      disabled: false,
    },
    {
      label: 'Ajouter au favoris',
      icon: <Favorite />,
      onClick: handleFavorite,
      disabled: row && row.isInFavoris === true,
    },
    {
      label: 'Archiver',
      icon: <img src={Archiver} />,
      onClick: handleArchived,
      disabled:
        (row && row.isArchived === true) ||
        (parameters &&
        parameters.length &&
        parameters.filter((parameter: any) => parameter.code === '0121' && parameter.value === 'true').length
          ? false
          : true),
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handleClickDeleteProject,
      disabled: false,
    },
  ];

  // PROJET ARCHIVEE
  const archivedMenuItems: ActionButtonMenu[] = [
    {
      label: 'Désarchiver',
      icon: <img src={Archiver} />,
      onClick: handleArchived,
      disabled:
        parameters &&
        parameters.length &&
        parameters.filter((parameter: any) => parameter.code === '0121' && parameter.value === 'true').length
          ? false
          : true,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handleClickDeleteProject,
      disabled: false,
    },
  ];

  let defaultContent: ActionButtonMenu[] = [
    {
      label: 'Retirer des favoris',
      icon: <img src={RetirerFavoris} />,
      onClick: handleClick,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handlesetOpenDialogDelete,
      disabled: false,
    },
  ];

  // MENUTASKLIST
  const MenuTaskList: ActionButtonMenu[] = [
    /*{
      label: 'Créer une sous-tâche',
      icon: <AddIcon />,
      onClick: () => handleClick,
      disabled: false,
    },*/
    {
      label: 'Ajouter une tâche au-dessus',
      icon: <PlaylistAddIcon />,
      onClick: () => handleClick,
      disabled: false,
    },
    {
      label: 'Ajouter une tâche en-dessous',
      icon: <PlaylistAddIcon />,
      onClick: () => handleClick,
      disabled: false,
    },
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: () => handleOpenModalTacheAction(),
      disabled: false,
    },
    {
      label: 'Déplacer',
      icon: <ExitToAppIcon />,
      onClick: handleClick,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handlesetOpenDialogDelete,
      disabled: false,
    },
  ];

  const isOnAllSee = filter === 'tout-voir';
  const isOnProject = filter === 'projet';
  const isInbox = pathname.includes('recu') && !pathname.includes('recu-equipe');
  const isInboxTeam = pathname.includes('recu-equipe');

  const hide =
    parameters &&
    parameters.length &&
    parameters.filter((parameter: any) => parameter.code === '0120' && parameter.value === 'true').length
      ? isOnProject && row && row.user && row.user.id === myId
        ? false
        : isOnAllSee || isInbox || isInboxTeam
        ? false
        : true
      : true;

  // MainHeader statutMainHeader
  const headerMenuItems: ActionButtonMenu[] = statutMainHeader
    ? [
        {
          label: cacheTacheAchevE ? 'Affichez tous les taches achevées' : 'Masquez tous les taches achevées',
          icon: cacheTacheAchevE ? <VisibilityIcon /> : <VisibilityOffIcon />,
          onClick: handleTacheAchevE,
          disabled: false,
        },
        { label: 'Modifier', icon: <Edit />, onClick: handleclickOpenEditProject, disabled: false },
        // {
        //   label: 'Ajouter une section',
        //   icon: <ViewDayIcon />,
        //   onClick: handleOpenAjoutSectionDialog,
        //   disabled: false,
        //   hide,
        // },
        {
          label: 'Ajouter au favoris',
          icon: <Favorite />,
          onClick: handleFavorite,
          disabled: row && row.isInFavoris === true,
        },
        {
          label: 'Trier par date de création',
          icon: <CalendarTodayIcon />,
          onClick: () => handleFiltre(1),
          disabled: false,
        },
        {
          label: "Trier par date d'echéance",
          icon: <EventIcon />,
          onClick: () => handleFiltre(2),
          disabled: false,
        },
        {
          label: 'Trier par priorité',
          icon: <FlagIcon />,
          onClick: () => handleFiltre(3),
          disabled: false,
        },
        {
          label: 'Trier par nom',
          icon: <SortByAlphaIcon />,
          onClick: () => handleFiltre(4),
          disabled: false,
        },
        {
          label: 'Trier par responsable',
          icon: <Person />,
          onClick: () => handleFiltre(5),
          disabled: false,
        },
        {
          label: 'Archiver',
          icon: <ArchiveIcon />,
          onClick: handleArchived,
          disabled:
            (row && row.isArchived === true) ||
            (parameters &&
              parameters.length &&
              parameters.filter((parameter: any) => parameter.code === '0121' && parameter.value === 'true').length)
              ? false
              : true,
        },
      ]
    : [
        {
          label: 'Ajouter une section',
          icon: <ViewDayIcon />,
          onClick: handleOpenAjoutSectionDialog,
          disabled: false,
          hide,
        },
        {
          label: 'Trier par date de création',
          icon: <CalendarTodayIcon />,
          onClick: () => handleFiltre(1),
          disabled: false,
        },
        {
          label: "Trier par date d'echéance",
          icon: <EventIcon />,
          onClick: () => handleFiltre(2),
          disabled: false,
        },
        {
          label: 'Trier par priorité',
          icon: <FlagIcon />,
          onClick: () => handleFiltre(3),
          disabled: false,
        },
        {
          label: 'Trier par nom',
          icon: <SortByAlphaIcon />,
          onClick: () => handleFiltre(4),
          disabled: false,
        },
        {
          label: 'Trier par responsable',
          icon: <Person />,
          onClick: () => handleFiltre(5),
          disabled: false,
        },
      ];

  switch (actionMenu) {
    case 'FAVORITE':
      defaultContent = favorisMenuItems;
      break;
    case 'PROJECT':
      defaultContent = projectMenuItems;
      break;
    case 'ARCHIVED':
      defaultContent = archivedMenuItems;
      break;
    case 'MENU_TASK':
      defaultContent = MenuTaskList;
      break;
    case 'HEADER_MENU':
      defaultContent = headerMenuItems; // A modifier
      break;
    default:
      defaultContent = defaultContent;
      break;
  }

  const DeleteDialogContent = () => {
    return <span>Êtes-vous sur de vouloir supprimer ?</span>;
  };

  if (actionMenu === 'PROJECT') console.log('*********', defaultContent);

  return (
    <>
      {loadingDelete && <Backdrop value="Suppression en cours, veuillez patienter..." />}
      <ConfirmDeleteDialog
        open={openDeleteDialogTask}
        setOpen={handlesetOpenDialogDelete}
        onClickConfirm={onClickConfirmDeleteTask}
      />

      <AjoutSectionModal
        openModal={openAddSection}
        setOpen={handleOpenAddSection}
        handleChange={handleSectionChange}
        values={sectionValues}
        handleSubmit={doCreateUpdateSection}
      />
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        // tslint:disable-next-line: jsx-no-lambda
        onClickConfirm={() => handleRemove(row)}
      />
      <ConfirmDialog
        open={openAddToFavorite}
        message={
          row && row.isInFavoris
            ? 'Êtes-vous sur de vouloir retirer des favoris ?'
            : 'Êtes-vous sur de vouloir ajouter au favoris ?'
        }
        handleValidate={() => switchFavs(row)}
        handleClose={handleFavorite}
      />
      <ConfirmDialog
        open={openArchived}
        message={
          row && row.isArchived ? 'Êtes-vous sur de vouloir désarchiver ?' : 'Êtes-vous sur de vouloir archiver ?'
        }
        handleValidate={() => switchArchived(row)}
        handleClose={handleArchived}
      />
      <AjoutTaches
        openModalTache={openModalTache}
        setOpenTache={setOpenModalTache}
        task={task}
        refetchCountTodos={refetchCountTodos}
        refetchTasks={refetch}
        refetchAll={refetchAll}
      />
      <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
        <MoreHoriz />
      </IconButton>
      <Menu
        // tslint:disable-next-line: jsx-no-lambda
        onClick={(event) => {
          event.preventDefault();
          event.stopPropagation();
        }}
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {defaultContent &&
          defaultContent.length > 0 &&
          defaultContent.map(
            (i: ActionButtonMenu, index: number) =>
              !i.hide && (
                <MenuItem
                  // tslint:disable-next-line: jsx-no-lambda
                  onClick={(event) => {
                    event.preventDefault();
                    event.stopPropagation();
                    handleClose();
                    i.onClick(event);
                  }}
                  key={`table_menu_item_${index}`}
                  disabled={i.disabled}
                  className={classes.itemList}
                >
                  <ListItemIcon>{i.icon}</ListItemIcon>
                  <Typography variant="inherit">{i.label} </Typography>
                </MenuItem>
              )
          )}
      </Menu>

      <AjoutParticipant
        openModalParticipant={openShare}
        setOpenParticipant={setOpenShare}
        userIds={selectedUsersIds}
        setUserIds={setselectedUsersIds}
        idProjectTask={(row && row.id) || ''}
      />
    </>
  );
};

export default Actionbutton;
