import { isMobile } from '@lib/common';
import { AppBar, Box, CssBaseline, Hidden, IconButton, Tab, Tabs, Typography } from '@material-ui/core';
import { Add, KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, ReactNode, useState } from 'react';
import { useParams } from 'react-router';
import { useTodoURLParams } from '../../hooks/useTodoURLParams';
import AjoutParticipant from '../../Modals/AjoutParticipants';
import { CountTodosProps } from '../../Todo';
import ActionButton from '../ActionButton';
import { countActionsAfterOffsetFromNow, filterActionsByOffsetFromNow } from '../utils/actions';
import useStyles from './styles';

const daysRequired = 7;

interface MainHeaderProps {
  withDate?: boolean;
  title: string;
  actionIconButton?: ReactNode;
  handleSetOpenModalTache: (dateDebut: any) => void;
  onChangeOffsetFromNow: (offset: number) => void;
  actions?: any;
  participants?: number | null | undefined;
  setTriage?: (triage: any[]) => void;
  refetchSection: any;
  setCacheTacheAchevE?: (cacheTacheAchevE: boolean) => void;
  cacheTacheAchevE?: boolean;
  statutMainHeader?: boolean;
  row?: any;
  parameters: any;
  refetchAll?: () => void;
}

const MainHeader: FC<MainHeaderProps & CountTodosProps> = ({
  title,
  actions,
  withDate,
  actionIconButton,
  statutMainHeader,
  row,
  handleSetOpenModalTache,
  participants,
  onChangeOffsetFromNow,
  setTriage,
  refetchSection,
  setCacheTacheAchevE,
  cacheTacheAchevE,
  refetchCountTodos,
  parameters,
  refetchAll,
}) => {
  const { filter }: any = useParams();
  const handleClickIcon = () => {};
  const date = moment().format('dddd Do MMMM YYYY');
  const classes = useStyles({});
  const [openModal, setOpenModal] = React.useState(false);
  const [selectedUsersIds, setselectedUsersIds] = useState<string[]>([]);
  const [daysOffset, setDaysOffset] = useState<number>(1);
  const [daysTabs, setDaysTabs] = useState<any>([]);
  const [totalTacheRestantes, setTotalTacheRestantes] = useState<number>(0);
  const { params, redirectTo } = useTodoURLParams();
  const isOnNextWeek = filter === 'sept-jours';
  const isOnNextWeekTeam = filter === 'sept-jours-equipe';
  const isOnProject = params.projet && params.projet.length > 0 ? true : false;

  const [currentTab, setCurrentTab] = useState<number>(0);

  const handleTabsChange = (event: React.ChangeEvent<{}>, value: any) => {
    setCurrentTab(value);
    const activeDate = daysTabs[value];
    if (onChangeOffsetFromNow) {
      onChangeOffsetFromNow(activeDate.offset);
    }
  };

  React.useEffect(() => {
    const days: any[] = [];

    const monday = moment().startOf('week').isoWeekday(0);
    const diffDays = moment().diff(monday, 'days');

    for (let i = daysOffset - diffDays; i <= daysRequired + daysOffset - diffDays; i++) {
      days.push({
        id: `tab-${i}`,
        label: moment().add(i, 'days').format('DD'),
        date: moment().add(i, 'days').format('ddd'),

        value: moment().add(i, 'days').format('YYYY-MM-DD'),

        offset: i,
      });
    }

    setDaysTabs(days);
  }, [daysOffset]);

  React.useEffect(() => {
    if (daysTabs.length > 0) {
      const minDayTabIndex = daysTabs.findIndex((dayTab: any) => dayTab.offset > 0);

      setCurrentTab(minDayTabIndex);
      if (onChangeOffsetFromNow) {
        onChangeOffsetFromNow(daysTabs[minDayTabIndex].offset);
      }
    }
  }, [daysTabs, filter]);

  React.useEffect(() => {
    if (daysTabs.length > 0) {
      setTotalTacheRestantes(
        daysTabs[daysTabs.length - 1]?.offset
          ? countActionsAfterOffsetFromNow(actions, daysTabs[daysTabs.length - 1].offset)
          : 0
      );
    }
  }, [daysTabs, actions]);

  const handleScrollNext = () => {
    setDaysOffset((prev) => prev + daysRequired);
  };

  const handleScrollPrev = () => {
    if (daysOffset > 1) {
      setDaysOffset((prev) => prev - daysRequired);
    }
  };

  const onTaskCalendar = params.date === 'next';
  return (
    <Box className={classes.root}>
      <Box display="flex" flexDirection="column" width="100%">
        <Box display="flex" alignItems="flex-end">
          <CssBaseline />
          <Hidden mdDown={true}>
            <Typography className={classes.title}>
              {onTaskCalendar ? `${moment().add(daysOffset, 'days').format('MMM YYYY')}` : title}
            </Typography>
          </Hidden>

          {withDate && <Typography className={classes.date}>{date}</Typography>}
        </Box>
        <Box className={classes.nextWeekContainer}>
          {onTaskCalendar && (
            <AppBar className={classes.appBarCalendar} position="static">
              <Tabs
                value={currentTab}
                onChange={handleTabsChange}
                variant="scrollable"
                scrollButtons="on"
                aria-label="simple tabs example"
                ScrollButtonComponent={(item) => {
                  if (item.direction === 'left' && daysOffset === 1) {
                    return null;
                  }
                  return (
                    <IconButton
                      onClick={item.direction === 'right' ? handleScrollNext : handleScrollPrev}
                      style={{ borderRadius: 0 }}
                    >
                      {item.direction === 'right' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                      {item.direction === 'right' && totalTacheRestantes > 0 ? (
                        <span style={{ fontSize: 12, marginRight: 2 }}>{totalTacheRestantes} </span>
                      ) : null}
                    </IconButton>
                  );
                }}
              >
                {onTaskCalendar &&
                  daysTabs.map((day: any) => {
                    const count = filterActionsByOffsetFromNow(actions, day.offset).length;

                    return (
                      <Tab
                        disabled={day.offset <= 0}
                        key={day.id}
                        label={
                          <Box height={75}>
                            <Typography className={classes.jourName}>{day.date}</Typography>
                            <Typography className={classes.jourNombre}>{day.label}</Typography>
                            {count > 0 && <Typography>({count})</Typography>}
                          </Box>
                        }
                      />
                    );
                  })}
              </Tabs>
            </AppBar>
          )}
        </Box>
        {isOnProject && (
          <Box display="flex" alignItems="center">
            <Typography className={classes.labelParticipant}>Participant(s)</Typography>
            <Typography className={classes.count}>{participants}</Typography>
          </Box>
        )}

        {!isMobile() ? (
          <Typography
            className={classes.addTaskButton}
            color="primary"
            onClick={() =>
              handleSetOpenModalTache(
                onTaskCalendar ? moment().add(daysTabs[currentTab].offset, 'days').toDate() : moment().toDate()
              )
            }
          >
            <Add />
            Ajouter une tâche
          </Typography>
        ) : (
          isOnProject && <Typography className={classes.title}>Titre de la fonction</Typography>
        )}
      </Box>
      <Box display="flex" alignItems="center">
        <IconButton onClick={handleClickIcon}>{actionIconButton}</IconButton>
        {!onTaskCalendar && (
          <Box display="flex">
            <CssBaseline />
            <Hidden mdDown={true}>
              <ActionButton
                actionMenu="HEADER_MENU"
                setTriage={setTriage}
                refetchSection={refetchSection}
                setCacheTacheAchevE={setCacheTacheAchevE}
                cacheTacheAchevE={cacheTacheAchevE}
                statutMainHeader={statutMainHeader}
                refetchCountTodos={refetchCountTodos}
                row={row}
                parameters={parameters}
                refetchAll={refetchAll}
              />
            </Hidden>

            <AjoutParticipant
              openModalParticipant={openModal}
              setOpenParticipant={setOpenModal}
              userIds={selectedUsersIds}
              setUserIds={setselectedUsersIds}
              idProjectTask={(row && row.id) || ''}
            />
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default MainHeader;
