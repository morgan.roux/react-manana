import { MobileTopBar, useApplicationContext } from '@lib/common';
import { useProjectQuery } from '@lib/common/src/graphql';
import Box from '@material-ui/core/Box';
import React, { useState } from 'react';
import { useHistory, useLocation, useParams } from 'react-router';
import ActionButton from '../Common/ActionButton';
import MainHeader from '../Common/MainHeader';
import { filterActionsByOffsetFromNow } from '../Common/utils/actions';
import { useTodoURLParams } from '../hooks/useTodoURLParams';
import AjoutTaches from '../Modals/AjoutTache/AjoutTaches';
import { CountTodosProps } from '../Todo';
import Section from './Section';
import useStyles from './styles';

interface MainContentProps {
  window?: () => Window;
  parameters: any[];
  filters: any;
  actions: any;
  todayActions: any;
  actionListFilters?: any;
  setTriage?: (triage: any[]) => void;
  triage?: any;
  setTaskListVariables?: (values: any) => void;
  taskListVariables?: any;
  refetchTask?: any;
  loadingTask?: any;
  etiquetteList?: any;
  projectList?: any;
  sectionQuery?: any;
  refetchSection?: any;
  refetchAll?: () => void;
  handleDrawerToggle: () => void;
  useMatriceResponsabilite: boolean;
  fetchMoreActions?: any;
  fetchingMoreActions: boolean;
  fetchMoreTodayActions?: any;
}

export const defaultDateTerm = {
  range: { dateDebut: { gt: 'now/d' } },
};

export const operationVariable = {
  edit: 'EDIT',
  addSubs: 'ADD_SUBS',
  addUpper: 'ADD_UPPER',
  addDown: 'ADD_DOWN',
};

const MainContent: React.FC<MainContentProps & CountTodosProps> = (props) => {
  const {
    parameters,
    filters,
    refetchCountTodos,
    actions,
    todayActions,
    actionListFilters,
    setTriage,
    triage,
    taskListVariables,
    loadingTask,
    refetchTask,
    etiquetteList,
    projectList,
    sectionQuery,
    refetchSection,
    refetchAll,
    handleDrawerToggle,
    useMatriceResponsabilite,
    fetchMoreActions,
    fetchingMoreActions,
    fetchMoreTodayActions,
    countTodos,
  } = props;
  const { filter, filterId }: any = useParams();
  const { pathname } = useLocation();
  const { push } = useHistory();
  const [cacheTacheAchevE, setCacheTacheAchevE] = useState<boolean>(true);
  const [statutMainHeader, setStatutMainHeader] = useState<boolean>(false);
  const [refetchTasks, setRefetchTasks] = useState<any>(null);
  const [nextTaskOffsetFromNow, setNextTaskOffsetFromNow] = useState<number>(1);
  const [defaultValues, setDefaultValues] = useState<any>();
  const { params, redirectTo } = useTodoURLParams();
  const isOnProject = pathname.includes('projet');
  const isOnAujourdhui: boolean = pathname.includes('/aujourdhui');
  const isOnAujourhduiEquipe: boolean = pathname.includes('aujourdhui-equipe');
  const isOnSeptJours: boolean = pathname.includes('/sept-jours') && !pathname.includes('sept-jours-equipe');
  const withoutDate: boolean = pathname.includes('sans-date');
  const isAllSee: boolean = pathname.includes('/tout-voir');
  const isOnSeptJoursEquipe: boolean = pathname.includes('/sept-jours-equipe');
  const { graphql } = useApplicationContext();

  const classes = useStyles(isAllSee ? 'hidden' : 'auto')();

  const refetchEtiquette = etiquetteList && etiquetteList.refetch;
  const refetchProject = projectList && projectList.refetch;
  const sectionResult = sectionQuery && sectionQuery.search && sectionQuery.search.data;

  const [openModalTache, setOpenModalTache] = useState(false);
  const [currentProjectId, setCurrentProjectId] = useState();

  const handleSetOpenModalTache = (dateDebut: any) => {
    setCurrentProjectId(filterId);
    setDefaultValues((prev: any) => ({ ...prev, dateDebut }));
    setOpenModalTache(!openModalTache);
  };

  const getProject = useProjectQuery({
    client: graphql,
    variables: { id: filterId },
    skip: !pathname.includes('todo/projet/'),
  });

  const projectInfo = getProject && getProject.data && getProject.data.project;

  const pathnameIncludes = (includes: string) => {
    return pathname === `/todo${includes}`;
  };

  const title = pathnameIncludes('/aujourdhui')
    ? "Mes Tâches d'Aujourd'hui"
    : pathnameIncludes('/sept-jours')
    ? 'Mes Tâches des Prochains Jours'
    : pathnameIncludes('/aujourdhui-equipe')
    ? //capitalizeFirstLetter(moment().format('MMMM YYYY'))
      "Tâches de l'Equipe d'Aujourd'hui"
    : pathnameIncludes('/sept-jours-equipe')
    ? //? `${capitalizeFirstLetter(moment().format('MMMM YYYY'))} (Equipe)`
      "Tâches de l'Equipe des Prochains Jours"
    : isOnProject
    ? projectInfo?._name || ''
    : withoutDate
    ? 'Sans date'
    : isAllSee
    ? 'Tout voir'
    : '';

  const optionBtn = (
    <ActionButton
      actionMenu="HEADER_MENU"
      setTriage={setTriage}
      refetchSection={refetchSection}
      setCacheTacheAchevE={setCacheTacheAchevE}
      cacheTacheAchevE={cacheTacheAchevE}
      statutMainHeader={statutMainHeader}
      refetchCountTodos={refetchCountTodos}
      parameters={parameters}
      refetchAll={refetchAll}
    />
  );

  const handleClickBack = () => {
    if (pathname !== '/todo') {
      push('/todo/aujourdui');
    } else {
      push('/');
    }
  };

  return (
    <Box className={classes.root}>
      <MobileTopBar
        title={title}
        withBackBtn={true}
        handleDrawerToggle={handleDrawerToggle}
        optionBtn={[optionBtn]}
        onClickBack={handleClickBack}
      />
      <main className={classes.content}>
        <MainHeader
          actions={actions?.search?.data}
          title={title}
          row={projectInfo}
          withDate={isOnAujourdhui ? true : isOnAujourhduiEquipe ? true : false}
          handleSetOpenModalTache={handleSetOpenModalTache}
          onChangeOffsetFromNow={setNextTaskOffsetFromNow}
          participants={projectInfo && projectInfo.participants ? projectInfo.participants.length : 0}
          setTriage={setTriage}
          refetchSection={refetchSection}
          setCacheTacheAchevE={setCacheTacheAchevE}
          cacheTacheAchevE={cacheTacheAchevE}
          statutMainHeader={statutMainHeader}
          refetchCountTodos={refetchCountTodos}
          parameters={parameters}
          refetchAll={refetchAll}
        />
        <Section
          filters={filters}
          actionListFilters={actionListFilters}
          triage={triage}
          sections={sectionResult}
          refetchSection={refetchSection}
          setStatutMainHeader={setStatutMainHeader}
          refetchCountTodos={refetchCountTodos}
          refetchProject={refetchProject}
          parameters={parameters}
          data={
            params.date === 'next'
              ? {
                  ...(actions || {}),
                  search: {
                    data: filterActionsByOffsetFromNow(actions?.search?.data, nextTaskOffsetFromNow),
                  },
                }
              : actions
          }
          todayData={todayActions}
          loadingTask={loadingTask}
          refetchTask={refetchTask}
          refetchAll={refetchAll}
          fetchingMoreActions={fetchingMoreActions}
          fetchMoreActions={fetchMoreActions}
          fetchMoreTodayActions={fetchMoreTodayActions}
          countTodos={countTodos}
        />

        <AjoutTaches
          defaultValues={defaultValues}
          queryVariables={taskListVariables}
          openModalTache={openModalTache}
          setOpenTache={handleSetOpenModalTache}
          refetchCountTodos={refetchCountTodos}
          currentProjectId={currentProjectId}
          refetchTasks={refetchTasks}
          refetchEtiquette={refetchEtiquette}
          refetchProject={refetchProject}
          refetchAll={refetchAll}
        />
      </main>
    </Box>
  );
};

export default MainContent;
