import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useUpdate_Item_Scoring_For_ItemLazyQuery } from '@lib/common/src/federation';
import { useDelete_ActionsMutation } from '@lib/common/src/graphql';
import { Backdrop, ConfirmDeleteDialog } from '@lib/ui-kit';
import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Delete, Edit, MoreHoriz } from '@material-ui/icons';
import React, { useState } from 'react';
import { ActionButtonMenu } from '../../../Common/ActionButton';
import AjoutTaches from '../../../Modals/AjoutTache';
import { CountTodosProps } from '../../../Todo';
import { isManuelAction } from '../../../util';
import { operationVariable } from '../../MainContent';

interface TaskListActionButtonProps {
  task: any;
  refetch: any;
  refetchProject: any;
  refetchAll?: () => void;
}
const TaskListActionButton: React.FC<TaskListActionButtonProps & CountTodosProps> = (props) => {
  const { task, refetch, refetchCountTodos, refetchProject, refetchAll } = props;

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = anchorEl ? true : false;
  const [openModalTache, setOpenModalTache] = useState<boolean>(false);
  const [openDeleteDialogTask, setOpenDeleteDialogTask] = useState<boolean>(false);
  const [operationName, setOperationName] = useState<string>('');

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    }
  };

  const { user, federation, graphql } = useApplicationContext();
  const displayNotification = useDisplayNotification();

  const assigns: any[] =
    task && task.participants && task.participants.length ? task.participants.map((user: any) => user.id) : [];
  const idCreation = task && task.createdBy ? task.createdBy.id : '';

  const handlesetOpenDialogDelete = () => {
    setOpenDeleteDialogTask(!openDeleteDialogTask);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const { addDown, addSubs, edit, addUpper } = operationVariable;
  const handleOpenModalTacheAction = () => {
    setOpenModalTache(!openModalTache);
    setOperationName(edit);
  };

  const [updateItemScoring, updatingItemScoring] = useUpdate_Item_Scoring_For_ItemLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  const [removeTache, { loading: loadingDelete }] = useDelete_ActionsMutation({
    client: graphql,
    onCompleted: (data) => {
      updateItemScoring({
        variables: {
          idItemAssocie: data.softDeleteActions[0].id,
          codeItem: 'L',
        },
      });
      displayNotification({
        type: 'success',
        message: 'Tâche supprimée avec succés.',
      });
      refetch();
      if (refetchCountTodos) {
        refetchCountTodos();
      }
      if (refetchProject) {
        refetchProject();
      }
    },
  });

  const onClickConfirmDeleteTask = () => {
    removeTache({
      variables: {
        ids: task && task.id,
      },
    });
    setOpenDeleteDialogTask(!openDeleteDialogTask);
  };

  const tasklistMenuItems: ActionButtonMenu[] = [
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: () => handleOpenModalTacheAction(),
      disabled: false,
      hide: !isManuelAction(task),
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handlesetOpenDialogDelete,
      disabled: user && user.id && (assigns.includes(user.id) || idCreation === user.id) ? false : true,
      hide: !isManuelAction(task),
    },
  ];

  return (
    <>
      {loadingDelete && <Backdrop value="Suppression en cours, veuillez patienter..." />}
      <ConfirmDeleteDialog
        open={openDeleteDialogTask}
        setOpen={handlesetOpenDialogDelete}
        onClickConfirm={onClickConfirmDeleteTask}
        centeredBtns
      />
      <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <MoreHoriz />
      </IconButton>
      <Menu
        // tslint:disable-next-line: jsx-no-lambda
        onClick={(event) => {
          event.preventDefault();
          event.stopPropagation();
        }}
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {tasklistMenuItems &&
          tasklistMenuItems.length > 0 &&
          tasklistMenuItems.map(
            (i: ActionButtonMenu, index: number) =>
              !i.hide && (
                <MenuItem
                  // tslint:disable-next-line: jsx-no-lambda
                  onClick={(event) => {
                    event.preventDefault();
                    event.stopPropagation();
                    handleClose();
                    i.onClick(event);
                  }}
                  key={`table_menu_item_${index}`}
                  disabled={i.disabled}
                >
                  <ListItemIcon>{i.icon}</ListItemIcon>
                  <Typography variant="inherit">{i.label}</Typography>
                </MenuItem>
              )
          )}
      </Menu>
      <AjoutTaches
        openModalTache={openModalTache}
        setOpenTache={setOpenModalTache}
        task={task}
        operationName={operationName}
        refetchCountTodos={refetchCountTodos}
        refetchTasks={refetch}
        refetchProject={refetchProject}
        refetchAll={refetchAll}
      />
    </>
  );
};

export default TaskListActionButton;
