import { RouteComponentProps, withRouter } from 'react-router';
import { Box, Popover } from '@material-ui/core';
import React, { useState } from 'react';
import moment from 'moment';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useCreate_ActionMutation } from '@lib/common/src/graphql';
import { CustomDatePickerHiddenInput } from '@lib/ui-kit';

interface TaskContentPopoverProps {
  open: boolean;
  id: string | undefined;
  anchorEl: any | null;
  section: string | undefined;
  task: any;
  setOpen: (value: boolean) => void;
  refetchAll?: any;
}

const TaskContentPopover: React.FC<TaskContentPopoverProps & RouteComponentProps> = ({
  id,
  open,
  anchorEl,
  section,
  task,
  setOpen,
  refetchAll,
}) => {
  const { user, graphql } = useApplicationContext();

  const displayNotification = useDisplayNotification();

  const [values, setValues] = useState<any>(task);

  const [createAndUpdateAction, { loading: addTaskLoading }] = useCreate_ActionMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createUpdateAction) {
        // ! IMPORTANT
        /* if (refetchTasks) refetchTasks(); */
        if (refetchAll) refetchAll();
        displayNotification({
          type: 'success',
          message: values.id ? 'Modification effectuée avec succès' : ' Création effectuée avec succès',
        });
      }
    },

    onError: (error) => {
      console.log('error :>> ', error);
      displayNotification({ type: 'error', message: error.message });
    },
  });

  const onChange = (event: any) => {
    event.stopPropagation();
    const { value, name } = event.target;
    const { idProject, dateDebut, id, idSection } = values;
    setValues((prevState: any) => ({ ...prevState, [name]: value }));

    createAndUpdateAction({
      variables: {
        input: {
          ...values,
          idProject: idProject ? (idProject === 'INBOXTEAM' || idProject === 'INBOX' ? '' : idProject) : '',
          id,
          idSection,
          isInInbox: idProject && idProject === 'INBOX' ? true : false,
          isInInboxTeam: idProject && idProject === 'INBOXTEAM' ? true : false,
          dateDebut: dateDebut,
          codeItem: 'TODO',
        },
      },
    });
    setOpen(false);
  };

  const onDateChange = (date: MaterialUiPickersDate, value?: string | null | undefined) => {
    setValues((prevState: any) => ({ ...prevState, dateDebut: moment(date).format() }));
  };

  return (
    <Popover id={id} open={open} anchorEl={anchorEl}>
      {section === 'DATE' && (
        <Box>
          <CustomDatePickerHiddenInput
            //open={openDatePicker}
            name="dateDebut"
            variant="static"
            onChange={onDateChange}
            value={values.dateDebut}
            autoOk={true}
            TextFieldComponent={() => null}
            autoFocus={true}
          />
        </Box>
      )}
    </Popover>
  );
};

export default withRouter(TaskContentPopover);
