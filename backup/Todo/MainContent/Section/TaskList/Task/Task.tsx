import { nl2br, useApplicationContext, useValueParameterAsBoolean } from '@lib/common';
import { UrgenceImportanceBadge } from '@lib/common/src/components';
import { ActionStatus } from '@lib/common/src/graphql';
import { CustomAvatarGroup } from '@lib/ui-kit';
import { Box, Icon, IconButton, Typography } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import { ChatBubble, Dashboard, EventNote, Repeat, Today } from '@material-ui/icons';
import BlockIcon from '@material-ui/icons/Block';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import { useTheme } from '@material-ui/styles';
import classnames from 'classnames';
import moment from 'moment';
import React, { useState } from 'react';
import { useLocation } from 'react-router';
import PrivateIcon from '../../../../../../assets/icons/todo/private.svg';
import { CountTodosProps } from '../../../../Todo';
import { isManuelAction, isReunionQualiteAction, isTraitementAutomatiqueAction } from '../../../../util';
import { computeCodeFromdateDebut } from './../../../../Common/UrgenceLabel/UrgenceLabel';
import ActionButton from './../TaskListActionButton';
import useStyles from './styles';
import TaskContentPopover from './TaskContentPopover';

interface TaskListProps {
  task: any;
  onClickCheckbox: (value: any) => (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  handleOpenModal?: (task: any) => (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  refetch?: any;
  refetchProject: any;
  isSubTask?: boolean;
  refetchAll?: () => void;
  setOpenTaskDetail?: (openTaskDetail: boolean) => void;
}

const Task: React.FC<TaskListProps & CountTodosProps> = ({
  task,
  onClickCheckbox,
  handleOpenModal,
  refetch,
  refetchCountTodos,
  refetchProject,
  isSubTask,
  refetchAll,
  setOpenTaskDetail,
}) => {
  const classes = useStyles();
  const { pathname } = useLocation();
  const { user: me } = useApplicationContext();

  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const isTM = task?.item?.code === 'PLAN_MARKETING_TYPE';

  const [openPopover, setOpenPopover] = useState<boolean>(false);

  const [popoverItem, setPopoverItem] = useState<string>('');

  const [popoverSection, setPopoverSection] = useState<string>();

  const popoverId = openPopover ? popoverItem : undefined;

  const useMatriceResponsabilite = useValueParameterAsBoolean('0501');

  const disabled =
    (task.participants && !task.participants.length && task.createdBy && me && task.createdBy.id === me.id) ||
    (task.participants &&
      task.participants.length &&
      me &&
      task.participants.map((assign: any) => assign.id).includes(me.id))
      ? false
      : !useMatriceResponsabilite;

  const handleDateClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    //event.stopPropagation();
    setAnchorEl(event.currentTarget);
    setPopoverSection('DATE');
    //setOpenPopover(true);
  };

  const handleCommentClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
    if (setOpenTaskDetail) setOpenTaskDetail(true);
    event.stopPropagation();
  };

  const bottomItems = (
    <Box className={classes.contentLabels}>
      {task.dateDebut && (
        <Box className={classes.labels} onClick={handleDateClick}>
          <Icon color="secondary">
            <Today />
          </Icon>
          <Typography className={classnames(classes.labelText, classes.date)} color="primary">
            {task.dateFin
              ? `du ${moment.utc(task.dateDebut).format('DD MMM')} au ${moment.utc(task.dateFin).format('DD MMM')}`
              : moment.utc(task.dateDebut).format('DD MMM')}
          </Typography>
        </Box>
      )}
      <Box className={classes.labels}>
        <Icon onClick={handleCommentClick}>
          <ChatBubble />
        </Icon>
        <Typography className={classnames(classes.labelText)}>{task.nbComment}</Typography>
      </Box>
      <Box className={classes.labels}>
        <UrgenceImportanceBadge
          importanceOrdre={task?.importance?.ordre || 2}
          urgenceCode={computeCodeFromdateDebut(task.dateDebut)}
        />
      </Box>
      {task.project && task.project.name && (
        <Box className={classes.labels}>
          <Typography className={classnames(classes.labelOrigine)}>Fonction</Typography>:
          <Typography className={classnames(classes.labelText)}>{task.project.name}</Typography>
        </Box>
      )}
      {task.isPrivate && (
        <Box className={classes.labels}>
          <img src={PrivateIcon} style={{ width: 28 }} />
        </Box>
      )}
      {isTM && (
        <Box className={classes.labels}>
          <Dashboard />
        </Box>
      )}
      {isTraitementAutomatiqueAction(task) ? (
        <Box className={classes.labels}>
          <Repeat />
        </Box>
      ) : isReunionQualiteAction(task) ? (
        <Box className={classes.labels}>
          <EventNote />
        </Box>
      ) : null}
    </Box>
  );

  return (
    task && (
      <>
        <Box className={!isSubTask ? classes.tasks : classes.subTasks}>
          <Box display="flex" justifyContent="space-between" alignItems="center" width="100%">
            <Box display="flex" alignItems="center">
              <Checkbox
                icon={disabled ? <BlockIcon fontSize="large" /> : <CircleUnchecked fontSize="large" />}
                checkedIcon={<CircleChecked fontSize="large" />}
                onClick={onClickCheckbox(task)}
                checked={task.status === ActionStatus.Done}
                disabled={disabled}
                color="primary"
                style={{ color: '#757575' }}
                classes={{ checked: classes.checkedBox }}
              />
              <Typography
                className={classes.taskTitle}
                dangerouslySetInnerHTML={{ __html: nl2br(task.description) } as any}
              />
            </Box>

            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              // tslint:disable-next-line: jsx-no-lambda
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
              }}
            >
              <Box
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                }}
                display="flex"
              >
                {
                  <IconButton onClick={isManuelAction(task) && handleOpenModal ? handleOpenModal(task) : undefined}>
                    {task.participants && task.participants.length > 0 ? (
                      <CustomAvatarGroup users={task.participants} max={5} />
                    ) : isManuelAction(task) ? (
                      <PersonAddIcon />
                    ) : null}
                  </IconButton>
                }

                <ActionButton
                  task={task}
                  refetch={refetch}
                  refetchCountTodos={refetchCountTodos}
                  refetchProject={refetchProject}
                  refetchAll={refetchAll}
                />
              </Box>
            </Box>
          </Box>
          {bottomItems}
        </Box>
        <TaskContentPopover
          id={popoverId}
          anchorEl={anchorEl}
          open={openPopover}
          setOpen={setOpenPopover}
          section={popoverSection}
          task={task}
        />
      </>
    )
  );
};

export default Task;
