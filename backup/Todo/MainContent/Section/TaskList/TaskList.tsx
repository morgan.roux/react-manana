import { useApplicationContext, useDisplayNotification } from '@lib/common';
import {
  useUpdate_Item_Scoring_For_ItemLazyQuery,
  useUpdate_Item_Scoring_For_UserLazyQuery,
} from '@lib/common/src/federation';
import {
  ActionStatus,
  useAssign_Task_To_UsersMutation,
  useSoft_Delete_ActionsMutation,
  useUnassign_Task_To_UsersMutation,
  useUpdate_Action_StatusMutation,
} from '@lib/common/src/graphql';
import { Backdrop, CustomModal } from '@lib/ui-kit';
import { Box, CircularProgress, List, ListItem } from '@material-ui/core';
import _ from 'lodash';
import React, { Fragment, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import AjoutTaches from '../../../Modals/AjoutTache';
import TaskDetails from '../../../Task/TaskDetails';
import { CountTodosProps } from '../../../Todo';
import useStyles from '../styles';
import Task from './Task/Task';
import { CustomSelectUser } from '@lib/common/src/components/CustomSelectUser';

interface TaskListProps {
  refetchProject: any;
  parameters: any;
  tasks: any;
  refetch?: any;
  refetchAll?: () => void;
  loading?: boolean;
}

const TaskList: React.FC<TaskListProps & RouteComponentProps & CountTodosProps> = ({
  tasks,
  refetch,
  refetchCountTodos,
  refetchProject,
  parameters,
  refetchAll,
  loading,
}) => {
  const classes = useStyles()();

  const [open, setOpen] = React.useState<boolean>(false);
  const [clickedTask, setClickedTask] = React.useState<any | null>(null);
  const { user, federation, graphql } = useApplicationContext();
  const displayNotification = useDisplayNotification();
  const [openModalTache, setOpenModalTache] = useState<boolean>(false);
  const [openTaskDetail, setOpenTaskDetail] = useState<boolean>(false);

  const taskParents = (tasks || []).filter((task: any) => task.idParent === null);
  const taskParentIds = (tasks || []).filter((task: any) => task.idParent === null).map((parent: any) => parent.id);
  const taskChildIds = tasks.filter((task: any) => task && task.idParent).map((child: any) => child && child.id);

  const listTasks = [
    ...taskParents,
    ...tasks.filter((task: any) => task.idParent && !taskParentIds.includes(task.idParent)),
  ];

  const onClickTask = (task: any) => (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
    event.preventDefault();
    event.stopPropagation();
    setClickedTask(task);
    setOpenModalTache(true);
  };

  const [updateItemScoring, updatingItemScoring] = useUpdate_Item_Scoring_For_ItemLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  const [updateItemScoringByUser, updatingItemScoringByUser] = useUpdate_Item_Scoring_For_UserLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  const [doUpdateActionStatus, { loading: loadingChangeStatus }] = useUpdate_Action_StatusMutation({
    client: graphql,
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
    onCompleted: (data) => {
      if (data && data.updateActionStatus) {
        updateItemScoring({
          variables: {
            idItemAssocie: data.updateActionStatus.id,
            codeItem: 'L',
          },
        });
        if (refetch) {
          refetch();
        }
        if (refetchCountTodos) refetchCountTodos();
        if (refetchProject) refetchProject();
      }
    },
  });

  const [assignParticipant, setAssignParticipant] = React.useState(false);
  const [taskToAssign, setTaskToAssign] = React.useState<any>(null);

  const [assignTaskToParticipant, { loading: loadingAssign }] = useAssign_Task_To_UsersMutation({
    client: graphql,
    onCompleted: (data) => {
      updateItemScoringByUser({
        variables: {
          idUser: user.id,
          codeItem: 'L',
        },
      });
      displayNotification({
        type: 'success',
        message: 'Tâche assignée avec succès',
      });
      if (data && data.assignActionUsers) {
        setTaskToAssign(data.assignActionUsers);
      }
      if (refetchAll) refetchAll();
      else {
        if (refetch) refetch();
        if (refetchCountTodos) refetchCountTodos();
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  // désassignation
  const [unAssignTaskToParticipant, { loading: loadingUnAssign }] = useUnassign_Task_To_UsersMutation({
    client: graphql,
    onCompleted: (data) => {
      updateItemScoringByUser({
        variables: {
          idUser: user.id,
          codeItem: 'L',
        },
      });
      displayNotification({
        type: 'success',
        message: 'Tâche désassignée avec succès',
      });
      if (data && data.unAssignActionUsers) {
        setTaskToAssign(data.unAssignActionUsers);
      }
      if (refetchAll) refetchAll();
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const onClickCheckbox = (task: any) => (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    event.stopPropagation();
    if (task) {
      if (task.status === ActionStatus.Done) {
        doUpdateActionStatus({
          variables: { input: { idAction: task.id, status: ActionStatus.Active } },
        });
      } else if (task.status === ActionStatus.Active) {
        doUpdateActionStatus({
          variables: { input: { idAction: task.id, status: ActionStatus.Done } },
        });
      }
    }
  };

  const handleValidateParticipants = (newParticipants: any) => {
    const newAssignUsers = newParticipants.filter(
      ({ id }: any) => !taskToAssign.assignedUsers.some((assignment: any) => assignment.id === id)
    );
    if (newAssignUsers.length > 0) {
      assignTaskToParticipant({
        variables: {
          idAction: taskToAssign.id,
          idUserDestinations: newAssignUsers.map(({ id }: any) => id),
        },
      });
    }

    const oldAssignUsers = taskToAssign.assignedUsers.filter(
      ({ id }: any) => !newParticipants.some((participant: any) => participant.id === id)
    );
    if (oldAssignUsers.length > 0) {
      unAssignTaskToParticipant({
        variables: {
          idAction: taskToAssign.id,
          idUserDestinations: oldAssignUsers.map(({ id }: any) => id),
        },
      });
    }
  };
  const handleOpenModal = (task: any) => (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    event.stopPropagation();
    setAssignParticipant(true);
    setTaskToAssign(task);
  };

  // Update task on details after edit
  React.useMemo(() => {
    if (tasks.length > 0 && clickedTask) {
      const newClickedTask = tasks.find((t: any) => t.id === clickedTask.id);
      if (newClickedTask) setClickedTask(newClickedTask);
    }
  }, [clickedTask, tasks]);

  // Mutation delete tasks
  const [doSoftDeleteActions, { loading: deleteLoading, called }] = useSoft_Delete_ActionsMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.softDeleteActions) {
        displayNotification({
          type: 'success',
          message: 'Suppression effectuée avec succès',
        });
        if (refetch) {
          refetch();
        }
        if (refetchCountTodos) {
          refetchCountTodos();
        }
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  // Close modal details on delete
  React.useMemo(() => {
    if (called) setOpen(false);
  }, [called]);

  // useEffect(() => {
  //   if (!openModalTache) {
  //     setScrollToCommentArea(false);
  //   }
  // }, [openModalTache]);

  /* const locationAccepted = ['/todo/projet', '/todo/aujourdhui-equipe']; */

  return (
    <Fragment>
      {deleteLoading && <Backdrop value="Suppression en cours, veuillez patienter ..." />}
      {loadingChangeStatus && <Backdrop value="Changement de status en cours, veuillez patienter ..." />}
      {(loadingAssign || loadingUnAssign) && <Backdrop />}
      <List className={classes.list}>
        {listTasks &&
          listTasks.length > 0 &&
          listTasks.map((task) => (
            <Box key={task.id} zIndex={1}>
              <ListItem button={true} className={classes.list} key={task.id} onClick={onClickTask(task)}>
                <Task
                  handleOpenModal={handleOpenModal}
                  onClickCheckbox={onClickCheckbox}
                  refetchProject={refetchProject}
                  task={task}
                  refetch={refetch}
                  refetchCountTodos={refetchCountTodos}
                  refetchAll={refetchAll}
                  setOpenTaskDetail={setOpenTaskDetail}
                />
              </ListItem>
              {task &&
                task.subActions &&
                task.subActions.length > 0 &&
                task.subActions.map(
                  (subAction: any) =>
                    subAction &&
                    subAction.id &&
                    taskChildIds.includes(subAction.id) && (
                      <ListItem
                        button={true}
                        className={classes.list}
                        key={subAction.id}
                        onClick={onClickTask(subAction)}
                      >
                        <Task
                          handleOpenModal={handleOpenModal}
                          onClickCheckbox={onClickCheckbox}
                          refetchProject={refetchProject}
                          task={subAction}
                          refetch={refetch}
                          refetchCountTodos={refetchCountTodos}
                          isSubTask={true}
                          refetchAll={refetchAll}
                        />
                      </ListItem>
                    )
                )}
            </Box>
          ))}
      </List>
      {loading && (
        <Box width="100%" display="flex" justifyContent="center">
          <CircularProgress size={35} />
        </Box>
      )}

      <CustomSelectUser
        openModal={assignParticipant}
        setOpenModal={setAssignParticipant}
        idParticipants={(taskToAssign?.project?.participants || []).map(({ id }: any) => id)}
        selected={taskToAssign?.assignedUsers || []}
        setSelected={(participants: any) => handleValidateParticipants(participants)}
      />
      {openModalTache && clickedTask && !openTaskDetail && (
        <AjoutTaches
          openModalTache={openModalTache}
          setOpenTache={setOpenModalTache}
          task={clickedTask}
          operationName="EDIT"
          refetchCountTodos={refetchCountTodos}
          refetchTasks={refetch}
          refetchProject={refetchProject}
          refetchAll={refetchAll}
        />
      )}
      {openTaskDetail && clickedTask && (
        <CustomModal open={openTaskDetail} setOpen={setOpenTaskDetail} withBtnsActions={false}>
          <TaskDetails
            task={clickedTask}
            doSoftDeleteActions={doSoftDeleteActions}
            refetchAll={refetchAll}
            scrollToCommentArea={true}
          />
        </CustomModal>
      )}
    </Fragment>
  );
};

export default withRouter(TaskList);
