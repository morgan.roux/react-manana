// ! IMPORTANT : ATTENTION AU REBASE

import { useApolloClient, useMutation } from '@apollo/client';
import DateFnsUtils from '@date-io/date-fns';
import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useUpdate_Action_Date_Debut_FinsMutation } from '@lib/common/src/graphql';
import { Backdrop, NoItemContentImage } from '@lib/ui-kit';
import { Box, CircularProgress, Typography } from '@material-ui/core';
import Accordion from '@material-ui/core/Accordion';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import localization from 'date-fns/locale/fr';
import moment from 'moment';
import React, { FC, Fragment } from 'react';
import { RouteComponentProps, useLocation, useParams, withRouter } from 'react-router';
import { useTodoURLParams } from '../../hooks/useTodoURLParams';
import InboxImg from '../../../../assets/img/Todo/alerte.svg';
import TodayEmptyImg from '../../../../assets/img/Todo/aujourdhui.svg';
import TodayTeamEmptyImg from '../../../../assets/img/Todo/aujourdhui_equipe.svg';
import InboxTeamImg from '../../../../assets/img/Todo/autres.svg';
import { CountTodosProps } from '../../Todo';
import SectionActionButton from './SectionActionButton';
import useStyles from './styles';
import TaskList from './TaskList/TaskList';

interface DateInterface {
  dateDebut: Date | null;
  dateFin: Date | null;
}

interface SectionProps {
  triage?: any[];
  actionListFilters: any;
  filters: any;
  sections: any;
  refetchSection: any;
  setStatutMainHeader?: (test: boolean) => void;
  refetchProject?: any;
  parameters: any;
  data: any;
  todayData: any;
  refetchTask?: any;
  loadingTask?: any;
  refetchAll?: () => void;
  fetchingMoreActions: boolean;
  fetchMoreActions?: any;
  fetchMoreTodayActions?: any;
}

const Section: FC<SectionProps & CountTodosProps> = ({
  sections,
  refetchSection,
  setStatutMainHeader,
  refetchCountTodos,
  refetchProject,
  parameters,
  data,
  todayData,
  loadingTask,
  refetchTask,
  refetchAll,
  fetchingMoreActions,
  fetchMoreActions,
  fetchMoreTodayActions,
  countTodos,
}) => {
  const { pathname } = useLocation();
  const isToday = pathname.includes('aujourdhui');
  const { params } = useTodoURLParams();
  const classes = useStyles(isToday ? undefined : 'auto')();
  const [openDate, setOpenDate] = React.useState<boolean>(false);
  const [date, setDate] = React.useState<DateInterface>({ dateDebut: null, dateFin: null });
  moment.locale('fr');

  const { graphql } = useApplicationContext();
  const displayNotification = useDisplayNotification();

  const isInbox = pathname.includes('recu') && !pathname.includes('recu-equipe');
  const isInboxTeam = pathname.includes('recu-equipe');

  const isTodayOnly = pathname.includes('aujourdhui') && !pathname.includes('aujourdhui-equipe');
  const isTodayTeam = pathname.includes('aujourdhui-equipe');
  const isProject = pathname.includes('projet');

  if (setStatutMainHeader) {
    if (isProject) {
      setStatutMainHeader(true);
    } else {
      setStatutMainHeader(false);
    }
  }

  const sectionIds = sections && sections.length ? sections.map((section: any) => section.id) : [];

  const getSectionTask = (section: any) => {
    return (
      (data &&
        data.search &&
        data.search.data &&
        data.search.data.filter((task: any) => task && task.section && task.section.id === section.id)) ||
      []
    );
  };

  const getTaskWithoutSection = () => {
    return (
      (data &&
        data.search &&
        data.search.data &&
        data.search.data.filter(
          (task: any) => task && (!task.section || (task.section && !sectionIds.includes(task.section.id)))
        )) ||
      []
    );
  };

  const tasks = data?.search?.data || [];
  const todayTasks = todayData?.search?.data || [];

  const Boxision = [
    {
      id: 1,
      sectionTitle: 'En retard',
      fetchMore: fetchMoreActions,
      total: data?.search?.total || 0,
      count: params.assignee?.includes('me') ? countTodos?.pastMe : countTodos?.pastTeam,
      tasks: tasks,
    },
    {
      id: 2,
      sectionTitle: "Aujourd'hui",
      fetchMore: fetchMoreTodayActions,
      total: todayData?.search?.total || 0,
      count: params.assignee?.includes('me') ? countTodos?.todayMe : countTodos?.todayTeam,
      tasks: todayTasks,
    },
  ];

  // Mutation update tasks dates
  const [doUpdateActionDateDebutFins, { loading: uLoading }] = useUpdate_Action_Date_Debut_FinsMutation({
    client: graphql,
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
    onCompleted: (data) => {
      if (data && data.updateActionDateDebutFins && refetchTask) {
        refetchTask();
      }
    },
  });

  const handleOpenDate = () => {
    setOpenDate(true);
  };

  const handleCloseDate = () => {
    setOpenDate(false);
  };

  const handleDateChange = (date: Date | null, name: string) => {
    setDate((prevState) => ({ ...prevState, [name]: date }));
  };

  // Execute update many tasks
  React.useMemo(() => {
    const dateDebut = date.dateDebut;
    const dateFin = date.dateFin;
    if (dateDebut && dateFin && tasks.length > 0) {
      // Update overdueTasks
      const inputs: any[] = [];
      tasks.map((i: any) => {
        const input: any = { idAction: i.id, dateDebut, dateFin };
        inputs.push(input);
      });
      doUpdateActionDateDebutFins({ variables: { inputs } });
    }
  }, [date]);

  const todayEmptyTxt = 'Un aperçu de la journée qui vous attend';
  const todayEmptyTxtSub = "Toutes les tâches dues aujourd'hui s'afficheront ici.";

  const todayTeamEmptyTxt = 'Un aperçu de la journée qui attend vos collègues';
  const todayTeamEmptyTxtSub = "Toutes les tâches dues aujourd'hui s'afficheront ici.";

  const inboxEmptyTxt = 'Terminé !';
  const inboxEmptyTxtSub = 'Il semble que tout soit organisé au bon endroit.';

  const inboxTeamEmptyTxt = 'Gardez vos tâches organisées par projet';
  const inboxTeamEmptyTxtSub =
    'Regroupez vos tâches par objectif et domaine. Glissez-dépossez les tâches pour les réorganiser ou créer des sous-tâches.';

  const emptyTitle = isTodayOnly
    ? todayEmptyTxt
    : isTodayTeam
    ? todayTeamEmptyTxt
    : isInbox
    ? inboxEmptyTxt
    : inboxTeamEmptyTxt;

  const emptySubTitle = isTodayOnly
    ? todayEmptyTxtSub
    : isTodayTeam
    ? todayTeamEmptyTxtSub
    : isInbox
    ? inboxEmptyTxtSub
    : inboxTeamEmptyTxtSub;

  const emptyImg = isTodayOnly ? TodayEmptyImg : isTodayTeam ? TodayTeamEmptyImg : isInbox ? InboxImg : InboxTeamImg;

  const handleScroll = (e: any) => {
    const bottom = e.target.scrollHeight - e.target.scrollTop - 10 <= e.target.clientHeight; // 10 : marge

    const total = data?.search?.total || 0;
    if (!fetchingMoreActions && bottom && fetchMoreActions && tasks.length < total && params.date !== 'today') {
      fetchMoreActions();
    }
  };

  // if (loadingTask)
  //   return (
  //     <div className={classes.loading}>
  //       <CircularProgress size={35} />
  //     </div>
  //   );

  return (
    <Box className={classes.root} onScroll={handleScroll}>
      {uLoading && <Backdrop value={'Mise à jour en cours...'} />}

      <Fragment>
        {params.date === 'today' && (
          <Fragment>
            {Boxision.map((section) => (
              <Accordion defaultExpanded={true}>
                <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                  <Typography>{`${section.sectionTitle} ${section.count ? `(${section.count})` : ''}`}</Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Box width="100%">
                    <TaskList
                      tasks={section.tasks}
                      refetch={refetchTask}
                      refetchCountTodos={refetchCountTodos}
                      refetchProject={refetchProject}
                      parameters={parameters}
                      refetchAll={refetchAll}
                      loading={loadingTask}
                    />
                    {section.tasks.length > 0 && section.tasks.length < section.total && (
                      <Typography onClick={section.fetchMore} className={classes.afficherPlus}>
                        Afficher plus
                      </Typography>
                    )}
                  </Box>
                </AccordionDetails>
              </Accordion>
            ))}
          </Fragment>
        )}
        {sections &&
          sections.map((section: any) => (
            <Accordion defaultExpanded={true}>
              <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                <Box display="flex" alignItems="center">
                  <Typography className={classes.sectionLibelle}>{section.libelle}</Typography>
                  <Typography className={classes.nbrProduits}>{section.nbAction}</Typography>
                </Box>
                <SectionActionButton section={section} refetchSection={refetchSection} refetchAll={refetchAll} />
              </AccordionSummary>
              <AccordionDetails>
                <TaskList
                  tasks={getSectionTask(section)}
                  refetch={refetchTask}
                  refetchCountTodos={refetchCountTodos}
                  refetchProject={refetchProject}
                  parameters={parameters}
                  refetchAll={refetchAll}
                  loading={loadingTask}
                />
              </AccordionDetails>
            </Accordion>
          ))}
        {params.date !== 'today' && (
          <TaskList
            tasks={getTaskWithoutSection()}
            refetch={refetchTask}
            refetchCountTodos={refetchCountTodos}
            refetchProject={refetchProject}
            parameters={parameters}
            refetchAll={refetchAll}
          />
        )}
      </Fragment>

      {(isTodayOnly || isTodayTeam || isInbox || isInboxTeam) && tasks.length === 0 && !loadingTask && (
        <div className={classes.emptyContainer}>
          <NoItemContentImage src={emptyImg} title={emptyTitle} subtitle={emptySubTitle} />
        </div>
      )}
      {fetchingMoreActions && (
        <Box width="100%" display="flex" justifyContent="center">
          <CircularProgress size={35} />
        </Box>
      )}
      <MuiPickersUtilsProvider utils={DateFnsUtils} libInstance={moment} locale={localization}>
        <KeyboardDatePicker
          margin="normal"
          id="date-picker-dialog"
          label="Date picker dialog"
          format="dd/MM/yyyy"
          value={date.dateDebut}
          onChange={(date) => handleDateChange(date, 'dateDebut')}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
          autoOk={false}
          disableToolbar={true}
          open={openDate}
          onOpen={handleOpenDate}
          onClose={handleCloseDate}
          okLabel="Valider"
          cancelLabel="Annuler"
          disablePast={true}
        />
      </MuiPickersUtilsProvider>
      <MuiPickersUtilsProvider utils={DateFnsUtils} libInstance={moment} locale={localization}>
        <KeyboardDatePicker
          margin="normal"
          id="date-picker-dialog"
          label="Date picker dialog"
          format="dd/MM/yyyy"
          value={date.dateFin}
          onChange={(date) => handleDateChange(date, 'dateFin')}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
          autoOk={false}
          disableToolbar={true}
          open={openDate}
          onOpen={handleOpenDate}
          onClose={handleCloseDate}
          okLabel="Valider"
          cancelLabel="Annuler"
          disablePast={true}
        />
      </MuiPickersUtilsProvider>
    </Box>
  );
};

export default Section;
