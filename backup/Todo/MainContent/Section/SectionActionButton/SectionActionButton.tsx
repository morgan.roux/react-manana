import { ConfirmDeleteDialog } from '@lib/ui-kit';
import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Add, Delete, Edit, MoreHoriz } from '@material-ui/icons';
import React, { FC, useState } from 'react';
import { useParams } from 'react-router';
import { ActionButtonMenu } from '../../../Common/ActionButton';
import { useCreateUpdateSection, useSection } from '../../../Common/utils';
import { useDeleteSection } from '../../../Common/utils/useSection';
import AjoutSection from '../../../Modals/AjoutSection';
import AjoutTaches from '../../../Modals/AjoutTache';
import useStyles from './styles';
interface SectionActionButtonProps {
  section: any;
  refetchSection: any;
  refetchAll?: () => void;
}

const SectionActionButton: FC<SectionActionButtonProps> = ({ section, refetchSection, refetchAll }) => {
  const classes = useStyles({});
  const { filter, filterId }: any = useParams();
  const [sectionAnchorEl, setSectionAnchorEl] = useState<null | HTMLElement>(null);
  const [openModalTache, setOpenModalTache] = useState<boolean>(false);
  const [openEditSection, setOpenEditSection] = React.useState(false);
  const open = sectionAnchorEl ? true : false;
  const [sectionValues, setSectionValues, handleSectionChange] = useSection();
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [doCreateUpdateSection] = useCreateUpdateSection(
    sectionValues,
    refetchSection,
    filter,
    filterId,
    setSectionValues,
    setOpenEditSection
  );

  const [doDeleteTodoSection] = useDeleteSection(section.id, refetchSection, setOpenDeleteDialog);

  const handleOpenModalTache = (event: React.MouseEvent<HTMLElement>) => {
    setOpenModalTache(true);
  };

  const handleOpenEditSection = (event: React.MouseEvent<HTMLElement>) => {
    setSectionValues((prevState: any) => ({ ...prevState, id: section.id, libelle: section.libelle }));
    setOpenEditSection(true);
  };

  const handleOptionClick = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setSectionAnchorEl(event.currentTarget);
  };

  const handleConfirmDeleteSection = () => {
    doDeleteTodoSection();
  };

  const handleOpenDeleteDialog = () => {
    setOpenDeleteDialog(true);
  };

  const handleCloseMenu = () => {
    setSectionAnchorEl(null);
  };

  const handleCloseAjoutSection = () => {
    setOpenEditSection(!openEditSection);
  };

  let defaultContent: ActionButtonMenu[] = [
    {
      label: 'Ajouter une tâche',
      icon: <Add />,
      onClick: handleOpenModalTache,
      disabled: false,
    },
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: handleOpenEditSection,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handleOpenDeleteDialog,
      disabled: false,
    },
  ];

  return (
    <div>
      <IconButton aria-controls="section-menu" aria-haspopup="true" onClick={handleOptionClick}>
        <MoreHoriz />
      </IconButton>
      <Menu
        id="section-menu"
        onClick={(e) => e.stopPropagation()}
        anchorEl={sectionAnchorEl}
        keepMounted={false}
        open={open}
        TransitionComponent={Fade}
        onClose={handleCloseMenu}
      >
        {defaultContent &&
          defaultContent.length > 0 &&
          defaultContent.map((i: ActionButtonMenu, index: number) => (
            <MenuItem
              // tslint:disable-next-line: jsx-no-lambda
              onClick={(event) => {
                i.onClick(event);
                event.preventDefault();
                event.stopPropagation();
                setSectionAnchorEl(null);
              }}
              key={`table_menu_item_${index}`}
              disabled={i.disabled}
            >
              <ListItemIcon>{i.icon}</ListItemIcon>
              <Typography variant="inherit">{i.label}</Typography>
            </MenuItem>
          ))}
      </Menu>
      <AjoutTaches
        openModalTache={openModalTache}
        setOpenTache={setOpenModalTache}
        idSection={section && section.id}
        refetchAll={refetchAll}
      />
      <AjoutSection
        openModal={openEditSection}
        setOpen={handleCloseAjoutSection}
        handleChange={handleSectionChange}
        values={sectionValues}
        handleSubmit={doCreateUpdateSection}
      />
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDeleteSection}
      />
    </div>
  );
};

export default SectionActionButton;
