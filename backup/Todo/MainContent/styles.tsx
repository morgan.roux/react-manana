import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';
const drawerWidth = 380;

const useStyles = (overflow?: string) =>
  makeStyles((theme: Theme) =>
    createStyles({
      root: {
        display: 'flex',
        height: '100%',
        [theme.breakpoints.down('md')]: {
          flexDirection: 'column',
        },
      },
      drawer: {
        [theme.breakpoints.up('md')]: {
          width: drawerWidth,
          flexShrink: 0,
        },
        borderRight: '1px solid rgba(0, 0, 0, 0.12)',
        height: '100%',
      },

      menuButton: {
        marginRight: theme.spacing(2),
        position: 'absolute',
        top: 156,
        left: 12,
        background: '#373740',
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        borderTopRightRadius: 50,
        borderBottomRightRadius: 50,
        [theme.breakpoints.up('md')]: {
          display: 'none',
        },
        '&:hover': {
          backgroundColor: '#373740',
          opacity: 0.98,
        },
        zIndex: 2,
      },
      // necessary for content to be below app bar
      toolbar: theme.mixins.toolbar,
      drawerPaper: {
        width: drawerWidth,
        top: 86,
        left: 280,
        boxShadow: 'none !important',
        [theme.breakpoints.down('md')]: {
          left: 0,
          top: 150,
        },

        [theme.breakpoints.down('sm')]: {
          left: 0,
        },
      },
      content: {
        flexGrow: 1,
        zIndex: 0,
        //padding: theme.spacing(3.125, 4),
        height: '100%',
        display: 'flex',
        overflow,
        flexDirection: 'column',
        [theme.breakpoints.down('md')]: {
          marginBottom: 70,
        },
      },
      contentNavFavoris: {
        padding: theme.spacing(1, 3, 12),
        '& .MuiIconButton-root': {
          color: '#000000',
        },
      },
      mobileTopBar: {
        height: 70,
        background: theme.palette.secondary.main,
      },
    })
  );

export default useStyles;
