import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { Comment, CommentListNew, CommonFieldsForm, ReactQuillLabel } from '@lib/common/src/components';
import { useUpdate_Item_Scoring_For_ItemLazyQuery } from '@lib/common/src/federation';
import { ActionInput, ActionStatus, useCreate_ActionMutation } from '@lib/common/src/graphql';
import { Backdrop, CustomAlertDonneesMedicales, CustomDatePicker, CustomModal, CustomTextarea } from '@lib/ui-kit';
import { AppBar, Box, Tab, Tabs } from '@material-ui/core';
import { InsertComment, ShowChart } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import ReactQuill from 'react-quill';
import { useLocation, useParams } from 'react-router';
import { computeCodeFromdateDebut } from '../../Common/UrgenceLabel/UrgenceLabel';
import { operationVariable } from '../../MainContent/MainContent';
import TaskActivities from '../../Task/TaskActivity';
import { CountTodosProps } from '../../Todo';
import { isManuelAction, isReunionQualiteAction, isTraitementAutomatiqueAction } from '../../util';
import { useStyles } from './styles';

interface UsersModalProps {
  openModalTache: boolean;
  operationName?: string;
  task?: any;
  queryVariables?: any;
  setOpenTache: (value: boolean) => void;
  idSection?: string;
  tacheRapide?: boolean;
  currentProjectId?: string;
  refetchTasks?: any;
  refetchEtiquette?: any;
  refetchProject?: any;
  refetchAll?: () => void;
  section?: string;

  defaultValues?: any;
}

interface UrgenceInterface {
  value: string;
  label: string;
}

const dispatcherSaveCommentWrapper: any = [];

const AddTask: FC<UsersModalProps & CountTodosProps> = (props) => {
  const classes = useStyles({});

  // data to fetch from back
  const {
    openModalTache,
    setOpenTache,
    task,
    tacheRapide,
    idSection,
    currentProjectId,
    operationName,
    refetchAll,
    section,
    defaultValues,
  } = props;

  const { pathname } = useLocation();
  const { filterId }: any = useParams();
  const isInbox = pathname.includes('recu') && !pathname.includes('recu-equipe');
  const isInboxTeam = pathname.includes('recu-equipe');
  const [desabledPlanif, setdesabledPlanif] = useState<boolean>(false);
  const { federation, graphql, isMobile } = useApplicationContext();

  const [activeTab, setActiveTab] = useState<number>(0);
  const [assignedUsers, setAssingedUsers] = useState<any[]>([]);

  const displayNotification = useDisplayNotification();

  const urgenceList: UrgenceInterface[] = [
    {
      value: 'today',
      label: 'A : Journée',
    },
    { value: 'week', label: 'B : Semaine' },
    { value: 'month', label: 'C : Mois' },
  ];

  const [urgence, setUrgence] = useState<string>(urgenceList[0].value);
  const taskValueInitialState: ActionInput = {
    id: '',
    description: '',
    idProject: currentProjectId || '',
    idsEtiquettes: [],
    idOrigine: '',
    idActionType: '',
    codeMaj: '',
    dateDebut: task?.dateDebut || moment().add(7, 'days').toDate(),
    dateFin: null,
    isInInbox: false,
    isInInboxTeam: false,
    isPrivate: false,
    codeItem: 'TODO',
    idActionParent: '',
    idItemAssocie: '',
    idSection: '',
    status: ActionStatus.Active,
    ordre: null,
    isRemoved: false,
    idsUsersDestinations: [],
    commentaire: {
      id: '',
      content: '',
    },
  };

  const [taskValue, setTaskValue] = useState<ActionInput>(taskValueInitialState);

  let title = task ? 'Modification de tâche' : tacheRapide ? 'Ajout de tâche Rapide' : `Ajout de tâche`;

  switch (operationName) {
    case operationVariable.edit:
      title = 'Modification de tâche';
      break;
    case operationVariable.addSubs:
      title = 'Ajout de sous-tâche';
      break;
    default:
      title = `Ajout de tâche`;
      break;
  }
  const nomBoutton = taskValue.id ? 'Modifier' : 'Ajouter';

  const [updateItemScoring, updateItemScoringLoading] = useUpdate_Item_Scoring_For_ItemLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  const [createAndUpdateAction, { loading: addTaskLoading }] = useCreate_ActionMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createUpdateAction) {
        updateItemScoring({
          variables: { codeItem: 'L', idItemAssocie: data.createUpdateAction.id },
        });
        if (refetchAll) refetchAll();

        displayNotification({
          type: 'success',
          message: taskValue.id ? 'Modification effectuée avec succès' : ' Création effectuée avec succès',
        });
        setOpenTache(false);
        setTaskValue(taskValueInitialState);
        handleUserDestinationChanges([]);
      }
    },

    onError: (error) => {
      console.log('error :>> ', error);
      displayNotification({ type: 'error', message: error.message });
    },
  });

  const createUpdateAction = () => {
    if (dispatcherSaveCommentWrapper[0]) {
      dispatcherSaveCommentWrapper[0](); //TODO : Find better solution
    }

    const { idProject, dateDebut, dateFin } = taskValue;
    createAndUpdateAction({
      variables: {
        input: {
          ...taskValue,
          idProject: idProject ? (idProject === 'INBOXTEAM' || idProject === 'INBOX' ? '' : idProject) : '',
          id: operationName === operationVariable.edit ? task.id : '',
          idSection: taskValue.idSection,
          isInInbox: idProject && idProject === 'INBOX' ? true : false,
          isInInboxTeam: idProject && idProject === 'INBOXTEAM' ? true : false,
          idImportance: taskValue.idImportance,
          dateDebut,
          dateFin,
          codeItem: 'TODO',
          idsUsersDestinations: taskValue.idsUsersDestinations,
        },
      },
    });
  };

  React.useEffect(() => {
    if (currentProjectId) {
      setTaskValue((prev: any) => ({ ...prev, idProject: currentProjectId }));
    }
  }, [currentProjectId]);

  // On edit
  React.useEffect(() => {
    if (task && operationName) {
      if (operationName === operationVariable.edit) {
        const idsEtiquettes = (task.etiquettes && task.etiquettes.map((i: any) => i.id)) || [];
        const idsUsersDestinations = (task.assignedUsers && task.assignedUsers.map((i: any) => i.id)) || [];
        setAssingedUsers((task && task.assignedUsers) || []);
        const isInInbox = task.isInInbox;
        const isInInboxTeam = task.isInInboxTeam;
        const dateDebut = task.dateDebut || null;
        const dateFin = task.dateFin || null;
        const isPrivate = task.isPrivate ? true : false;

        const isToday: boolean = dateDebut && moment(dateDebut).format() === moment().format();

        const idProject = task.project ? task.project.id : '';

        const newTaskValue: ActionInput = {
          id: task.id,
          description: task.description,
          idProject: idProject ? idProject : isInboxTeam ? 'INBOXTEAM' : isInbox ? 'INBOX' : 'INBOX',
          idsEtiquettes: idsEtiquettes && idsEtiquettes[0],
          idOrigine: (task.origine && task.origine.id) || '',
          idActionType: (task.actionType && task.actionType.id) || '',
          dateDebut,
          dateFin,
          isInInbox,
          isInInboxTeam,
          isPrivate,
          commentaire: {
            id: (task.firstComment && task.firstComment.id) || '',
            content: (task.firstComment && task.firstComment.content) || '',
          },
          idActionParent: (task.actionParent && task.actionParent.id) || '',
          status: task.status || ActionStatus.Active,
          idItemAssocie: task.idItemAssocie || task.id,
          idSection: (task.section && task.section.id) || '',
          idsUsersDestinations,
          ordre: task.ordre,
          isRemoved: task.isRemoved,
          idImportance: task.importance?.id,
        };
        setTaskValue(newTaskValue);
        setUrgence(computeCodeFromdateDebut(task.dateDebut));
      }
      if (operationName === operationVariable.addSubs) {
        const newTaskValue: ActionInput = {
          ...taskValueInitialState,
          idProject: task.project ? task.project.id : '',
          dateDebut: new Date(Date.now()),
          dateFin: null,
          idActionParent: task.id,
        };
        setTaskValue(newTaskValue);
      }
      if (operationName === operationVariable.addUpper) {
        const newTaskValue: ActionInput = {
          ...taskValueInitialState,
          ordre: task.ordre === 1 ? 1 : task.ordre - 1,
        };
        setTaskValue(newTaskValue);
      }
      if (operationName === operationVariable.addDown) {
        const newTaskValue: ActionInput = {
          ...taskValueInitialState,
          ordre: task.ordre + 1,
        };
        setTaskValue(newTaskValue);
      }
    }
  }, [task, operationName]);

  useEffect(() => {
    if (!task) {
      //setAssingedUsers([]);
    }
  }, [task]);

  React.useEffect(() => {
    if (!openModalTache && !task) {
      setTaskValue({ ...taskValueInitialState });
    } else if (openModalTache && !task) {
      setTaskValue({
        ...taskValueInitialState,
        dateDebut: new Date(Date.now()),
        dateFin: null,
        idSection: idSection || '',
        idProject: filterId ? filterId : isInboxTeam ? 'INBOXTEAM' : isInbox ? 'INBOX' : 'INBOX',
      });
    }
  }, [openModalTache, task]);

  const formChange = (event: any) => {
    const { value, name } = event.target;

    if (name === 'idProject') {
      /*searchSection({
        variables: {
          type: ['todosection'],
          query: sectionQueryVariables(value || ''),
        },
      });
      */
      setTaskValue((prevState: any) => ({ ...prevState, idSection: '', [name]: value || '' }));
      setdesabledPlanif(false);

      return;
    }

    if (name === 'commentaire') {
      setTaskValue((prevState: any) => ({
        ...prevState,
        commentaire: { ...prevState.commentaire, content: value },
      }));
      return;
    }

    setTaskValue((prevState: any) => ({ ...prevState, [name]: value }));
  };

  const descriptionChange = (content: string) => {
    setTaskValue({ ...taskValue, description: content });
  };

  const handleChangeDate = (name: string) => (date: any) => {
    if (name) {
      setTaskValue((prevState: any) => ({ ...prevState, [name]: date || null }));
    }
  };

  const handleTabsChange = (event: React.ChangeEvent<{}>, value: any) => {
    setActiveTab(value);
  };

  const handleUserDestinationChanges = (selected: any) => {
    const idsUsersDestinations = selected && selected.map((selected: any) => selected.id);
    setAssingedUsers(selected);
    setTaskValue((prevState: any) => ({ ...prevState, idsUsersDestinations }));
  };

  const handleChangeProjet = (idProjet: string) => {
    formChange({ target: { name: 'idProject', value: idProjet } });
  };

  useEffect(() => {
    handleChangeDateDebut(taskValue.dateDebut);
  }, [taskValue.dateDebut]);

  React.useEffect(() => {
    if (defaultValues) {
      setTaskValue((prev: any) => ({ ...prev, ...defaultValues }));
    }
  }, [defaultValues]);

  const handleChangeDateDebut = (date: any) => {
    const today = moment();

    const dateDebut = moment(taskValue.dateDebut);

    const nextSunday = moment().endOf('isoWeek').add(1, 'week');

    if (dateDebut.day() === today.day()) {
      setUrgence('A');
    }
    if (dateDebut > today && dateDebut <= nextSunday) {
      setUrgence('B');
    }
    if (dateDebut > nextSunday) {
      setUrgence('C');
    }

    setTaskValue((prevState: any) => ({ ...prevState, dateDebut: date }));
  };

  const handleUrgenceChange = (event: any) => {
    const { value } = event.target;

    const today = moment();

    const dateDebut = moment(taskValue.dateDebut);

    const nextDay = moment().add(1, 'day');

    const nextSunday = moment().endOf('isoWeek').add(1, 'week');

    const nextMonth = moment().startOf('isoWeek').add(2, 'week');

    switch (value) {
      case 'A':
        if (dateDebut.day() !== today.day()) {
          setTaskValue((prevState: any) => ({ ...prevState, dateDebut: moment().format() }));
        }
        break;

      case 'B':
        if (dateDebut < nextDay || dateDebut > nextSunday) {
          setTaskValue((prevState: any) => ({ ...prevState, dateDebut: nextDay.format() }));
        }
        break;

      case 'C':
        if (dateDebut < nextMonth) {
          setTaskValue((prevState: any) => ({ ...prevState, dateDebut: nextMonth.format() }));
        }
        break;

      default:
        break;
    }
    setUrgence(value);
  };

  const [elFocus, setElFocus] = useState<ReactQuill | null>(null);

  if (section && section === 'DATE') {
    return (
      <CustomModal
        // tslint:disable-next-line: jsx-no-lambda
        onClick={(event: any) => event.stopPropagation()}
        open={openModalTache}
        setOpen={setOpenTache}
        title={title}
        withBtnsActions={true}
        withCancelButton={!isMobile}
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="sm"
        fullWidth={true}
        className={classes.usersModalRoot}
        onClickConfirm={createUpdateAction}
        actionButton={nomBoutton}
        disabledButton={!taskValue.description ? true : false}
        disableBackdropClick={true}
      >
        <Box className={classes.tableContainer}>
          <Box className={classes.row}>
            <Box
              className={classes.sectionDatePicker}
              // tslint:disable-next-line: jsx-no-lambda
              onClick={(event) => {
                event.preventDefault();
                event.stopPropagation();
              }}
            >
              <CustomDatePicker
                label="Date Début"
                placeholder="Date début"
                onChange={handleChangeDate('dateDebut')}
                name="dateDebut"
                value={taskValue.dateDebut}
                disabled={desabledPlanif}
              />
            </Box>
            <Box
              className={classes.sectionDatePicker}
              // tslint:disable-next-line: jsx-no-lambda
              onClick={(event) => {
                event.preventDefault();
                event.stopPropagation();
              }}
            >
              <CustomDatePicker
                label="Date Fin"
                placeholder="Date fin"
                onChange={handleChangeDate('dateFin')}
                name="dateFin"
                value={taskValue.dateFin}
                disabled={desabledPlanif}
              />
            </Box>
          </Box>
        </Box>
        <Backdrop open={addTaskLoading} value={`${task ? 'Modification' : 'Création'} en cours...`} />
      </CustomModal>
    );
  }

  const isGeneratedFromTA = !isManuelAction(task);
  const alertMessage = isTraitementAutomatiqueAction(task)
    ? 'Vous ne pouvez pas modifier cette tâche qui a été générée à partir de traitement automatique'
    : isReunionQualiteAction(task)
    ? 'Vous ne pouvez pas modifier cette tâche qui a été générée à partir de la réunion qualité'
    : '';

  return (
    <CustomModal
      // tslint:disable-next-line: jsx-no-lambda
      onClick={(event: any) => event.stopPropagation()}
      open={openModalTache}
      setOpen={setOpenTache}
      title={isGeneratedFromTA ? 'Détail de tâche' : title}
      withBtnsActions={true}
      withCancelButton={!isMobile}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="sm"
      fullWidth={true}
      className={classes.usersModalRoot}
      onClickConfirm={createUpdateAction}
      actionButton={nomBoutton}
      disableBackdropClick={true}
      centerBtns
      //cancelButtonTitle={isGeneratedFromTA ? 'Fermer' : undefined}
      //withConfirmButton={!isGeneratedFromTA}
      disabledButton={!taskValue.description || !taskValue.idImportance ? true : false}
    >
      <Box className={classes.tableContainer}>
        {isGeneratedFromTA && (
          <Box style={{ paddingBottom: 10 }}>
            <CustomAlertDonneesMedicales message={alertMessage} />
          </Box>
        )}
        <Box className={classes.section}>
          <ReactQuillLabel label="Description" required={true} />
          <ReactQuill
            readOnly={isGeneratedFromTA}
            className="customized-title"
            theme="snow"
            value={taskValue.description}
            onChange={descriptionChange}
            ref={(el) => {
              setElFocus(el);
            }}
          />
        </Box>

        <CommonFieldsForm
          disabled={isGeneratedFromTA}
          style={{ padding: 0 }}
          selectedUsers={assignedUsers}
          urgence={urgence}
          isPrivate={taskValue.isPrivate ? true : false}
          urgenceProps={{
            useCode: true,
          }}
          usersModalProps={{
            withNotAssigned: true,
            searchPlaceholder: 'Rechercher...',
            withAssignTeam: false,
            singleSelect: false,
          }}
          onlyMatriceFonctions={false}
          projet={taskValue.idProject}
          importance={taskValue.idImportance ? taskValue.idImportance : undefined}
          onChangeUsersSelection={handleUserDestinationChanges}
          onChangeUrgence={(urgence: any) =>
            handleUrgenceChange({ target: { name: 'idUrgence', value: urgence.code } })
          }
          onChangeImportance={(importance: any) =>
            formChange({ target: { name: 'idImportance', value: importance.id } })
          }
          onChangeProjet={(projet: any) => {
            handleChangeProjet(projet?.id);
          }}
          onChangePrivate={(isPrivate: any) => setTaskValue((prevState: any) => ({ ...prevState, isPrivate }))}
        />

        <Box
          className={classes.sectionDatePicker}
          // tslint:disable-next-line: jsx-no-lambda
          onClick={(event) => {
            event.preventDefault();
            event.stopPropagation();
          }}
        >
          <CustomDatePicker
            label="Date Début"
            placeholder="Date début"
            onChange={/*handleChangeDate('dateDebut')*/ handleChangeDateDebut}
            name="dateDebut"
            value={taskValue.dateDebut}
            disabled={isGeneratedFromTA || desabledPlanif}
            disablePast={false}
          />
        </Box>

        <Box
          className={classes.sectionDatePicker}
          // tslint:disable-next-line: jsx-no-lambda
          onClick={(event) => {
            event.preventDefault();
            event.stopPropagation();
          }}
        >
          <CustomDatePicker
            label="Date Fin"
            placeholder="Date fin"
            onChange={handleChangeDate('dateFin')}
            name="dateFin"
            value={taskValue.dateFin}
            disabled={isGeneratedFromTA || desabledPlanif}
            disablePast={false}
          />
        </Box>

        {task ? (
          <div id="testId" className={classes.tabPaper}>
            <AppBar position="static" className={classes.tabs} elevation={0}>
              <Tabs
                centered={true}
                value={activeTab}
                onChange={handleTabsChange}
                indicatorColor="secondary"
                variant="scrollable"
                scrollButtons="auto"
                aria-label="add task modal tabs"
              >
                {/*<Tab icon={<LibraryBooks />} label="Sous-Tâche" />*/}
                <Tab icon={<InsertComment />} label="Commentaires" />
                <Tab icon={<ShowChart />} label="Activités" />
              </Tabs>
            </AppBar>
            {/*activeTab === 0 && <Box></Box>*/}
            {activeTab === 0 && (
              <Box>
                <CommentListNew codeItem="TODO" idItemAssocie={task.id} />
                <Comment
                  codeItem="TODO"
                  idSource={task.id}
                  withAttachement={true}
                  dispatchSubmitCb={(dispatcher: any) => {
                    dispatcherSaveCommentWrapper[0] = dispatcher;
                  }}
                />
              </Box>
            )}

            {activeTab === 1 && (
              <Box padding="15px">
                <TaskActivities idTask={task.id} />
              </Box>
            )}
          </div>
        ) : (
          <div className={classes.section}>
            <CustomTextarea
              label="Commentaire"
              value={(taskValue.commentaire && taskValue.commentaire.content) || ''}
              name="commentaire"
              onChangeTextarea={formChange}
              rows={3}
            />
          </div>
        )}
      </Box>
      <Backdrop open={addTaskLoading} value={`${task ? 'Modification' : 'Création'} en cours...`} />
    </CustomModal>
  );
};

export default AddTask;
