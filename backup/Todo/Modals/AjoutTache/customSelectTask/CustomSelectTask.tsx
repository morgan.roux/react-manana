import { Box, Checkbox, Chip } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { createStyles, Theme, withStyles } from '@material-ui/core';
import Error from '@material-ui/icons/Error';
import classnames from 'classnames';
import React, { FC, useState } from 'react';

const styles = (theme: Theme) =>
  createStyles({
    formControl: {
      width: '100%',
      '& .MuiSelect-selectMenu': {
        minHeight: '30px !important',
        display: 'flex',
        alignItems: 'center',
      },
      '& label': {
        '& .MuiFormLabel-asterisk': {
          color: 'red',
        },
      },
      '&:hover label, & label.Mui-focused': {
        color: theme.palette.primary.main,
      },
      '&.Mui-focused fieldset': {
        border: `2px solid ${theme.palette.primary.main} !important`,
      },
      '& input:invalid + fieldset': {
        borderColor: 'red',
      },
      '& .Mui-focused .MuiIconButton-label svg': {
        color: `${theme.palette.primary.main} !important`,
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: theme.palette.common.white,
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          // borderColor: theme.palette.common.black,
        },
        '&:hover fieldset': {
          borderColor: theme.palette.primary.main,
        },
        '&.Mui-focused fieldset': {
          borderColor: theme.palette.primary.main,
        },
      },
      transition:
        'padding-left 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,border-color 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,border-width 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms',
    },
    label: {
      background: theme.palette.common.white,
      // color: theme.palette.common.black,
      padding: '0 4px',
    },
    noLabel: {
      '& fieldset > legend > span': {
        display: 'none',
      },
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: 2,
    },
    ItemEtiquette: {
      fontSize: '0.875rem',
      fontWeight: 500,
      '& .MuiCheckbox-root': {
        padding: 4,
      },
    },
    noMarginBottom: {
      marginBottom: 0,
    },
    withMarginBottom: {
      marginBottom: 16,
    },
  });

const CustomSelect: FC<any> = ({ ...props }) => {
  const {
    classes,
    label,
    NoMarginBottom,
    index,
    listId,
    list,
    error,
    variant,
    shrink,
    required,
    withNoneValue,
    placeholder,
    withPlaceholder,
    disabled,
    checkeds,
    setCheckeds,
    multiple = true,
  } = props;
  const [openSelect, setOpenSelect] = useState<boolean>(false);
  /* onClick={ !multiple?undefined: (e) => {
          setOpenSelect((prevState) => !prevState);
        }}*/

  const handleChange = (event: any, id: string) => {
    let newChekeds: string[] = [];
    event.stopPropagation();
    event.preventDefault();

    if (!multiple) {
      setCheckeds([id]);
      return;
    }

    if (checkeds.includes(id)) {
      newChekeds = checkeds.filter((i) => i !== id);
      setCheckeds(newChekeds);
    } else {
      setCheckeds([id]);
    }

    setOpenSelect(false);
  };

  return (
    <FormControl
      required={required}
      className={
        !NoMarginBottom
          ? !label
            ? classnames(classes.formControl, classes.noLabel, classes.withMarginBottom)
            : classnames(classes.formControl, classes.withMarginBottom)
          : !label
          ? classnames(classes.formControl, classes.noLabel)
          : classes.formControl
      }
      variant={variant}
      error={error}
      disabled={disabled}
    >
      <InputLabel shrink={shrink} htmlFor="name" className={classes.label}>
        {label}
      </InputLabel>
      <Select
        {...props}
        className={classes.select}
        multiple={multiple}
        MenuProps={{
          getContentAnchorEl: null,
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'left',
          },
          //     open: !multiple? undefined: openSelect,
        }}
        // tslint:disable-next-line: jsx-no-lambda
        renderValue={
          !multiple
            ? undefined
            : (selected: any) => {
                return (
                  <div className={classes.chips}>
                    {selected.map((value: any) => (
                      <Box display="flex" alignItems="center">
                        <Error
                          htmlColor={
                            list && list.length !== 0 && list.find((item: any) => item.id === value).couleur.code
                          }
                          style={{
                            marginRight: '13px',
                            marginLeft: '10px',
                          }}
                        />
                        <Chip
                          key={value}
                          label={list && list.length !== 0 && list.find((item) => item.id === value).nom}
                          className={classes.chip}
                        />
                      </Box>
                    ))}
                  </div>
                );
              }
        }
      >
        {list &&
          list.length > 0 &&
          list.map((item) => (
            <MenuItem
              key={item.id}
              className={classes.ItemEtiquette}
              value={item.id}
              onClick={(e) => {
                handleChange(e, item.id);
                e.stopPropagation();
              }}
            >
              {multiple && <Checkbox checked={checkeds.includes(item.id)} name={item.id} />}
              <Error
                htmlColor={item && item.couleur && item.couleur.code ? item.couleur.code : 'black'}
                style={{
                  marginRight: '13px',
                  marginLeft: '10px',
                }}
              />
              {item.nom}
            </MenuItem>
          ))}
      </Select>
    </FormControl>
  );
};

export default withStyles(styles, { withTheme: true })(CustomSelect);

CustomSelect.defaultProps = {
  list: [],
  label: '',
  variant: 'outlined',
  error: false,
  shrink: true,
  required: false,
  withNoneValue: false,
  withPlaceholder: false,
  NoMarginBottom: false,
};
