import React, { useEffect, useState } from 'react';
import { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useStyles } from './participantStyle';
import { CustomModal } from '../../../../../Common/CustomModal';
import { Avatar, Fab, Grid, Box, Tooltip } from '@material-ui/core';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import { DO_SEARCH_PARTICIPANT, DO_GET_PARTICIPANT_LIST } from '../../../../../../graphql/Todo/query';
import { useQuery } from '@apollo/client';
import { PARTICIPANT_LIST, PARTICIPANT_LISTVariables } from '../../../../../../graphql/Todo/types/PARTICIPANT_LIST';
import WithSearch from '../../../../../Common/newWithSearch/withSearch';
import ParticipantList from './ParticipantListChildren/ParticipantList';
import SearchInput from '../../../../../Common/newCustomContent/SearchInput';
import AjoutParticipant from '../AjoutParticipants/AjoutParticipant';
import CustomSelectTask from '../../Common/Task/CustomSelectTask';

interface UsersModalProps {
  openModal: boolean;
  setOpenModal: any;
  match: {
    params: {
      filterId: string | undefined;
    };
  };
}

const ParticipantListModal: FC<UsersModalProps & RouteComponentProps> = ({
  openModal,
  match: {
    params: { filterId },
  },
  setOpenModal,
}) => {
  //request elastic search
  const particpantList = {};
  /* const { loading: isLoading, data: particpantList, error: onError } = useQuery<any, any>(
    DO_SEARCH_PARTICIPANT,
    {
      fetchPolicy: 'cache-and-network',
      variables: {
        type: ['project'],
        query: {
          query: {
            bool: { must: [{ term: { _id: filterId } }] },
          },
        },
      },
    },
  ); */
  // simple request to get default user
  const { loading, data, error } = useQuery<PARTICIPANT_LIST, PARTICIPANT_LISTVariables>(DO_GET_PARTICIPANT_LIST, {
    variables: { id: filterId || '' },
  });

  const projectList = [
    { label: 'Tous les types', value: 'Tous' },
    { label: 'Pharmacie', value: 'pharmacie' },
    { label: 'Présidents de régions', value: 'presidentregion' },
    { label: 'Partenaires de services', value: 'partenaire' },
    { label: 'Laboratoire partenaire', value: 'laboratoire' },
  ];
  const title = `Liste des participants`;
  const [open, setOpen] = useState<boolean>(false);

  useEffect(() => {
    setOpen(openModal);
  }, [openModal]);

  const classes = useStyles({});

  const defaultUser = data && data.project && data.project.userCreation && (
    <Box className={classes.section}>
      <Grid container spacing={3}>
        <Grid item xs={1}>
          <Avatar alt="user1" src={data.project.userCreation.id} className={classes.large} />
        </Grid>
        <Grid item xs={10} className={classes.description}>
          <Grid item xs={12} className={classes.title}>
            {data.project.userCreation.userName}
          </Grid>
          <Grid item xs={12} className={classes.content}>
            {data &&
              data.project &&
              data.project.userCreation &&
              data.project.userCreation.role &&
              data.project.userCreation.role.nom}
          </Grid>
          <Grid item xs={12} className={classes.content}>
            {data.project.userCreation.email}
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
  const [participantsIDs, setParticipantIds] = useState<string[]>([]);

  /* useEffect(() => {
    if (
      particpantList &&
      particpantList.search &&
      particpantList.search.data &&
      particpantList.search.data[0] &&
      particpantList.search.data[0].participants
    ) {
      setParticipantIds(['']);
      setParticipantIds(
        particpantList.search.data[0].participants.map((participant) => participant.id),
      );
    }
  }, [particpantList]); */

  const [openModalParticipantAdd, setOpenModalParticipantAdd] = useState<boolean>(false);

  const openParticipantModal = () => {
    setOpenModalParticipantAdd(true);
  };

  //handleChange
  interface DATAFORM {
    searchValue: string;
    sortBy: string;
  }
  const initialFormValue: DATAFORM = {
    searchValue: '',
    sortBy: 'Tous',
  };
  const [dataParticipantList, setDataParticipantList] = useState(initialFormValue);
  const handleChange = (event) => {
    const { name, value } = event.target;
    setDataParticipantList({ ...dataParticipantList, [name]: value });
  };
  // open add participant
  const [addParticipant, setAddParticipant] = useState(false);

  const openAddParticipant = (event) => {
    setAddParticipant(!addParticipant);
    event.stopPropagation();
  };

  const optionalMust =
    dataParticipantList.sortBy === 'Tous'
      ? [{ term: { _id: filterId || '' } }]
      : [
          { term: { _id: filterId || '' } },
          {
            exists: {
              field: dataParticipantList.sortBy,
            },
          },
        ];

  // fin handle change
  return (
    <CustomModal
      open={open}
      setOpen={setOpenModal}
      title={title}
      withBtnsActions={false}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="md"
      fullWidth={true}
      className={classes.usersModalRoot}
    >
      <Box className={classes.tableContainer}>
        <Box className={classes.section}>
          <Grid container spacing={3}>
            <Grid item xs={8}>
              <SearchInput searchPlaceholder="Rechercher un participant..." />
            </Grid>
            <Grid item xs={4}>
              <CustomSelectTask
                label="Type de contact"
                list={projectList}
                name="sortBy"
                className={classes.espaceHaut}
                onChange={handleChange}
              />
            </Grid>
          </Grid>
        </Box>
        <Box className={classes.section}>
          <Grid container spacing={3}>
            <Grid item xs={1}>
              <Tooltip title="Ajouter des personnes" aria-label="add" className={classes.iconCustom}>
                <Fab onClick={openParticipantModal}>
                  <GroupAddIcon />
                </Fab>
              </Tooltip>
              <AjoutParticipant
                openModalParticipant={openModalParticipantAdd}
                setOpenParticipant={setOpenModalParticipantAdd}
                userIds={participantsIDs}
                setUserIds={setParticipantIds}
                idProjectTask={filterId}
              />
            </Grid>
            <Grid item xs={5} className={classes.label}>
              Ajouter des personnes
            </Grid>
          </Grid>
        </Box>
        {defaultUser}
        {/* {participantDisplay} */}
        <Box className={classes.section}>
          <WithSearch
            type="project"
            WrappedComponent={ParticipantList}
            searchQuery={DO_SEARCH_PARTICIPANT}
            optionalMust={optionalMust}
          />
        </Box>
      </Box>
    </CustomModal>
  );
};
export default withRouter(ParticipantListModal);
