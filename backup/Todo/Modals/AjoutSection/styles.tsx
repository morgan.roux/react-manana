import { createStyles, Theme } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    usersModalRoot: {
      '& .MuiDialogActions-root .MuiButtonBase-root': {
        width: 219,
        height: 50,
      },
      '& .MuiDialogContent-root': {
        padding: '8px 48px',
      },
      '& .MuiDialogActions-root': {
        padding: '0px 48px 48px',
      },
    },
    tableContainer: {
      width: '100%',
    },
    espaceHaut: {
      margin: '50px 0 40px',
    },
    espacedroite: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
  })
);
