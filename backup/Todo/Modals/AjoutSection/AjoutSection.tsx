import { CustomFormTextField, CustomModal } from '@lib/ui-kit';
import Box from '@material-ui/core/Box';
import React, { FC } from 'react';
import { useStyles } from './styles';

interface UsersModalProps {
  openModal: boolean;
  setOpen: any;
  handleChange: (value: any) => void;
  handleSubmit: any;
  values: any;
}

const AjoutSection: FC<UsersModalProps> = (props) => {
  const { openModal, setOpen, handleChange, values, handleSubmit } = props;
  const { libelle } = values;
  const title = `Ajout de section`;
  const classes = useStyles({});

  return (
    <CustomModal
      open={openModal}
      setOpen={setOpen}
      title={title}
      withBtnsActions={true}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="sm"
      fullWidth={true}
      className={classes.usersModalRoot}
      centerBtns={true}
      actionButton={values.id ? 'Modifier' : 'Ajouter'}
      onClickConfirm={handleSubmit}
      disabledButton={!libelle ? true : false}
    >
      <Box className={classes.tableContainer}>
        <CustomFormTextField
          label="Libellé"
          name="libelle"
          placeholder="Nom de la section"
          required={true}
          className={classes.espaceHaut}
          value={libelle || ''}
          onChange={handleChange}
          autoFocus={true}
        />
      </Box>
    </CustomModal>
  );
};
export default AjoutSection;
