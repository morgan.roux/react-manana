import { createStyles, Theme } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    usersModalRoot: {
      '& .MuiDialogContent-root': {
        padding: '0px 0px 0px 24px',
      },
      '& .MuiDialogActions-root': {
        boxShadow: '0px -3px 6px rgb(0, 0, 0, 0.16)',
        padding: '20px 24px',
        '& .MuiButton-root': {
          margin: '0 8px',
          padding: '9px 24px',
        },
        [theme.breakpoints.down('md')]: {
          width: '100vw',
          padding: 8,
          justifyContent: 'left',
        },
      },
      [theme.breakpoints.down('md')]: {
        '& .MuiDialog-scrollPaper': {
          justifyContent: 'left',
        },
        '& .MuiDialog-paperFullWidth': {
          width: '100%',
          maxWidth: '100%',
          minHeight: '100%',
        },
        '& .MuiDialog-paper': {
          margin: 0,
        },
        '& .MuiDialogContent-root': {
          padding: 0,
        },
        '& .MuiDialogTitle-root': {
          background: theme.palette.secondary.main,
        },
      },
    },

    paddinleft: {
      paddingLeft: -50,
    },
    searchInputBox: {
      padding: '20px 0px 0px 20px',
      display: 'flex',
    },
    container: {
      width: '100%',
    },
    listUserRoot: {
      '& .MuiTableRow-head': {
        '& > th:nth-child(6), & > th:nth-child(7)': {
          minWidth: 116,
        },
        '& > th:nth-child(10), & > th:nth-child(11)': {
          minWidth: 128,
        },
      },
    },
  })
);
