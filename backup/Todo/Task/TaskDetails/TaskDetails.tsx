import {
  Comment,
  CommentListNew,
  nl2br,
  useApplicationContext,
  useDisplayNotification,
  useValueParameterAsBoolean,
} from '@lib/common';
import { useUpdate_Item_Scoring_For_ItemLazyQuery } from '@lib/common/src/federation';
import { ActionStatus, useUpdate_Action_StatusMutation } from '@lib/common/src/graphql';
import { ConfirmDeleteDialog, CustomTabs, TabInterface } from '@lib/ui-kit';
import { Box, Icon, IconButton, ListItemIcon, Menu, MenuItem, Radio, Typography } from '@material-ui/core';
import { CheckCircleOutline, Delete, Edit, MoreHoriz, Today } from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import React, { useRef } from 'react';
import useCommonStyles from '../../MainContent/Section/styles';
import AjoutTaches from '../../Modals/AjoutTache/AjoutTaches';
import { CountTodosProps } from '../../Todo';
import TaskActivities from '../TaskActivity';
import useStyles from './styles';

interface TaskDetailsProps {
  task: any;
  refetchTasks?: any;
  disabledActions?: boolean;
  doSoftDeleteActions?: Function;
  refetchAll?: () => void;
  scrollToCommentArea?: boolean;
}

const TaskDetails: React.FC<TaskDetailsProps & CountTodosProps> = ({
  task,
  doSoftDeleteActions,
  refetchTasks,
  refetchCountTodos,
  disabledActions = false,
  refetchAll,
  scrollToCommentArea,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles()();
  const useMatriceResponsabilite = useValueParameterAsBoolean('0501');
  const commentAreaRef = useRef<HTMLDivElement>(null);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [openFormModal, setOpenFormModal] = React.useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = React.useState(false);
  const { user, federation } = useApplicationContext();
  const displayNotification = useDisplayNotification();

  const tabs: TabInterface[] = [
    {
      id: 0,
      label: 'Commentaires',
      content: (
        <Box display="flex" flexDirection="column">
          <CommentListNew codeItem="TODO" idItemAssocie={task.id} />
          {!disabledActions && <Comment codeItem="TODO" idSource={task.id} withAttachement={true} />}
        </Box>
      ),
    },
    {
      id: 1,
      label: 'Activités',
      content: <TaskActivities idTask={task.id} />,
    },
  ];
  const assigns: any[] =
    task && task.assignedUsers && task.assignedUsers.length ? task.assignedUsers.map((user: any) => user.id) : [];
  const idCreation = task && task.userCreation ? task.userCreation.id : '';

  const [updateItemScoring] = useUpdate_Item_Scoring_For_ItemLazyQuery({
    client: federation,
  });

  const [doUpdateActionStatus] = useUpdate_Action_StatusMutation({
    client: federation,
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
    onCompleted: (data) => {
      if (data && data.updateActionStatus) {
        updateItemScoring({
          variables: {
            idItemAssocie: data.updateActionStatus.id,
            codeItem: 'L',
          },
        });
        task.status = data.updateActionStatus.status;
        refetchTasks();
        if (refetchCountTodos) refetchCountTodos();
      }
    },
  });
  const handleChange = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    event.stopPropagation();
    if (task) {
      if (task.status === ActionStatus.Done) {
        doUpdateActionStatus({
          variables: { input: { idAction: task.id, status: ActionStatus.Active } },
        });
      } else if (task.status === ActionStatus.Active) {
        doUpdateActionStatus({
          variables: { input: { idAction: task.id, status: ActionStatus.Done } },
        });
      }
    }
  };

  const onClickMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => setAnchorEl(null);

  const onClickEdit = () => {
    handleCloseMenu();
    setOpenFormModal(true);
  };

  const onClickDelete = () => {
    handleCloseMenu();
    setOpenDeleteDialog(true);
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (doSoftDeleteActions) {
      doSoftDeleteActions({ variables: { ids: [task.id] } });
    }
  };

  if (scrollToCommentArea)
    commentAreaRef?.current?.scrollIntoView({
      behavior: 'smooth',
    });

  return (
    <div className={classes.taskDetailsRoot}>
      <div className={classes.taskContainer}>
        <div className={classes.taskTitleContainer}>
          <div className={classes.radioContainer}>
            <Radio
              className={classes.radio}
              checked={task.status === ActionStatus.Done}
              onClick={handleChange}
              checkedIcon={<CheckCircleOutline />}
              disabled={
                (task.assignedUsers &&
                  !task.assignedUsers.length &&
                  task.userCreation &&
                  user &&
                  task.userCreation.id === user.id) ||
                (task.assignedUsers &&
                  task.assignedUsers.length &&
                  user &&
                  task.assignedUsers.map((assign: any) => assign.id).includes(user.id))
                  ? false
                  : !useMatriceResponsabilite
              }
            />
            <Typography
              className={classes.taskTitle}
              dangerouslySetInnerHTML={{ __html: nl2br(task.description) } as any}
            />
          </div>
          <IconButton onClick={onClickMenu} disabled={disabledActions}>
            <MoreHoriz />
          </IconButton>
          <Menu
            id="task-details-menu"
            anchorEl={anchorEl}
            keepMounted={true}
            open={Boolean(anchorEl)}
            onClose={handleCloseMenu}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          >
            <MenuItem onClick={onClickEdit}>
              <ListItemIcon>
                <Edit fontSize="small" />
              </ListItemIcon>
              <Typography variant="inherit">Modifier</Typography>
            </MenuItem>
            <MenuItem
              onClick={onClickDelete}
              disabled={user && user.id && (assigns.includes(user.id) || idCreation === user.id) ? false : true}
            >
              <ListItemIcon>
                <Delete fontSize="small" />
              </ListItemIcon>
              <Typography variant="inherit">Supprimer</Typography>
            </MenuItem>
          </Menu>
        </div>
        <Box className={classnames(commonClasses.contentLabels, classes.taskInfoContainer)}>
          {task.dateDebut && (
            <Box className={commonClasses.labels}>
              <Icon color="secondary">
                <Today />
              </Icon>
              <Typography className={classnames(commonClasses.labelText, commonClasses.date)}>
                {moment(task.dateDebut).format('DD MMM YYYY')}
              </Typography>
            </Box>
          )}
          {task.dateFin && (
            <Box className={commonClasses.labels}>
              <Icon color="secondary">
                <Today />
              </Icon>
              <Typography className={classnames(commonClasses.labelText, commonClasses.date)}>
                {moment(task.dateFin).format('DD MMM YYYY')}
              </Typography>
            </Box>
          )}
        </Box>
      </div>
      <div ref={commentAreaRef}>
        <CustomTabs tabs={tabs} hideArrow={true} />
      </div>

      <AjoutTaches
        openModalTache={openFormModal}
        setOpenTache={setOpenFormModal}
        task={task}
        refetchCountTodos={refetchCountTodos}
        refetchTasks={refetchTasks}
        operationName={'EDIT'}
        refetchAll={refetchAll}
      />
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content="Êtes-vous sur de vouloir supprimer ?"
        onClickConfirm={onClickConfirmDelete}
      />
    </div>
  );
};

export default TaskDetails;
