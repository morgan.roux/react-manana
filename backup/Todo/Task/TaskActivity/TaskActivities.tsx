import { stringToAvatar, useApplicationContext, useDisplayNotification } from '@lib/common';
import { useTask_ActivitesQuery } from '@lib/common/src/graphql';
import { LoaderSmall } from '@lib/ui-kit';
import { Avatar, Typography } from '@material-ui/core';
import { ChatBubble, Edit, Error, Group, Today } from '@material-ui/icons';
import moment from 'moment';
import React, { FC } from 'react';
import useStyles from './styles';
import TaskActivitiesEmpty from './TaskActivitiesEmpty';

export interface TaskActivitiesProps {
  idTask: string;
}

const TaskActivities: FC<TaskActivitiesProps> = ({ idTask }) => {
  const classes = useStyles({});
  const { graphql } = useApplicationContext();
  const displayNotification = useDisplayNotification();

  const { data, loading } = useTask_ActivitesQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
    variables: { idTask },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  // console.log('data :>> ', data);

  const actionActivites = (data && data.actionActivites) || [];

  const activiteIcon = (activite: any) => {
    switch (activite.activiteType) {
      case 'EDIT':
        return {
          icon: <Edit className={classes.activiteIcon} />,
          color1: '#F11957',
          color2: '#E3464A',
          message: 'a modifié la tâche.',
        };
      case 'CHANGE_DATE_DEBUT':
        return {
          icon: <Today className={classes.activiteIcon} />,
          color1: '#F11957',
          color2: '#E3464A',
          message: `a changé la date de début${
            activite.newDateDebut ? ` à ${moment(activite.newDateDebut).format('DD/MM/YYYY HH:mm')}.` : '.'
          } `,
        };
      case 'CHANGE_DATE_FIN':
        return {
          icon: <Today className={classes.activiteIcon} />,
          color1: '#F11957',
          color2: '#E3464A',
          message: `a changé la date de fin${
            activite.newDateFin ? ` à ${moment(activite.newDateFin).format('DD/MM/YYYY HH:mm')}.` : '.'
          } `,
        };
      case 'COMMENT':
        return {
          icon: <ChatBubble className={classes.activiteIcon} />,
          color1: '#D9E021',
          color2: '#8CC63F',
          message: 'a commenté la tâche.',
        };
      case 'ASSIGN_USER':
        return {
          icon: <Group className={classes.activiteIcon} />,
          color1: '#F6D945',
          color2: '#FDA509',
          message: 'a ajouté de nouveau participant',
        };
      case 'UNASSIGN_USER':
        return {
          icon: <Group className={classes.activiteIcon} />,
          color1: '#F6D945',
          color2: '#FDA509',
          message: 'a retiré de participant',
        };
      case 'CHANGE_STATUS':
        return {
          icon: <Error className={classes.activiteIcon} />,
          color1: '#F6D945',
          color2: '#FDA509',
          message: `a changé le statut de la tâche${activite.newStatus ? ` à ${activite.newStatus}.` : '.'} `,
        };
      default:
        break;
    }
    return null;
  };

  return (
    <div className={classes.taskActivitiesRoot}>
      {loading && <LoaderSmall />}
      {actionActivites.length > 0 &&
        actionActivites.map((activite) => {
          const userPhotoUrl: string =
            (activite.userSource.userPhoto &&
              activite.userSource.userPhoto.fichier &&
              activite.userSource.userPhoto.fichier.publicUrl) ||
            '';
          const userName = activite.userSource.userName || '';

          const icon = activiteIcon(activite);

          return (
            <div className={classes.activiteRow} key={activite.id}>
              <div className={classes.imageContainer}>
                {userPhotoUrl ? (
                  <img className={classes.img} src={userPhotoUrl} alt="User photo" />
                ) : (
                  <Avatar className={classes.img}>{stringToAvatar(userName)}</Avatar>
                )}
                <div
                  className={classes.activiteIconContainer}
                  style={{
                    background: `transparent linear-gradient(31deg, ${icon && icon.color1} 0%, ${
                      icon && icon.color2
                    } 100%) 0% 0% no-repeat padding-box`,
                  }}
                >
                  {icon && icon.icon}
                </div>
              </div>
              <div className={classes.activiteInfos}>
                <Typography className={classes.username}>
                  {activite.userSource.userName} <span>{(icon && icon.message) || ''}</span>
                </Typography>
                <Typography className={classes.date}>{moment(activite.dateCreation).fromNow()}</Typography>
              </div>
            </div>
          );
        })}
      {actionActivites.length === 0 && !loading && <TaskActivitiesEmpty />}
    </div>
  );
};

export default TaskActivities;
