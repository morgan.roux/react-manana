import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    taskActivitiesRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    activiteRow: {
      display: 'flex',
      flexDirection: 'row',
      [theme.breakpoints.down('md')]: {
        marginBottom: 25,
      },
    },
    imageContainer: {
      position: 'relative',
    },
    img: {
      width: 65,
      height: 65,
      borderRadius: '50%',
      [theme.breakpoints.down('md')]: {
        width: 24,
        height: 24,
      },
    },
    activiteInfos: {
      display: 'flex',
      flexDirection: 'column',
      marginLeft: 30,
      '& .MuiTypography-root': {
        [theme.breakpoints.down('md')]: {
          fontSize: 12,
        },
      },
    },
    username: {
      fontWeight: 'bold',
      fontSize: 14,
      '& > span': {
        fontWeight: 'normal',
      },
    },
    date: {
      color: '#878787',
      fontSize: 12,
    },
    activiteIconContainer: {
      position: 'absolute',
      top: 45,
      left: 48,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: 23,
      height: 23,
      borderRadius: '50%',
      boxShadow: '0px 0px 6px #00000029',
      [theme.breakpoints.down('md')]: {
        top: 15,
        left: 20,
        width: 20,
        height: 20,
      },
    },
    activiteIcon: {
      width: 16,
      height: 16,
      color: '#fff',
      background: 'transparent',
    },
  })
);

export default useStyles;
