import { Box, Typography } from '@material-ui/core';
import React, { FC, useEffect } from 'react';
import useStyles from './style';

export interface DetailsProps {
  currentItem?: any;
  setCurrentItem?: (value: any) => void;
  //url? : string;
  listResult: any;
}

const Details: FC<DetailsProps> = ({ currentItem, setCurrentItem, listResult }) => {
  const classes = useStyles();

  useEffect(() => {
    //const data = listResult && listResult.data && listResult.data.search && listResult.data.search.data;
    const search = listResult.find((element: any) => element.id === currentItem.id);
    search && setCurrentItem && setCurrentItem(search);
  }, []);

  return (
    <Box className={classes.root}>
      <Box className={classes.fixedAppBarTab}>
        <Box padding="30px 60px" display="flex" flexDirection="column">
          <Typography className={classes.libelle}>{`${currentItem.ordre}.${currentItem.libelle}`}</Typography>
          <AttachedFiles downloadable={true} file={currentItem.fichier} />
        </Box>
      </Box>
    </Box>
  );
};

export default Details;
