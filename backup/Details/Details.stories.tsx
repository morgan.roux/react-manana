import React from 'react';

import Details from './Details'

export default {
    title: 'Design system|Atoms/Details',
    parameters: {
        info: { inline: true }
    }
};

  const listResult : any [] = [
    {
      id : 1,
      title : 'A',
      createDate : '15/8/20',
      updateDate : '15/8/20',
      file : {
        id : 1,
        nomOriginal : 'XX',
        chemin : 'TTT',
        publicUrl : 'https://img.maxisciences.com/s3/frgsd/300/photographie/default_2020-01-02_41f960d5-236c-4a94-9222-a339e0ddd365.jpeg'
      }
    },
    {
      id : 2,
      title : 'B',
      createDate : '15/8/20',
      updateDate : '15/8/20',
      file : {
        id : 1,
        nomOriginal : 'XX',
        chemin : 'TTT',
        publicUrl : 'https://img.maxisciences.com/s3/frgsd/300/photographie/default_2020-01-02_41f960d5-236c-4a94-9222-a339e0ddd365.jpeg'
      }
    },
    {
      id : 3,
      title : 'C',
      createDate : '15/8/20',
      updateDate : '15/8/20',
      file : {
        id : 1,
        nomOriginal : 'XX',
        chemin : 'TTT',
        publicUrl : 'https://img.maxisciences.com/s3/frgsd/300/photographie/default_2020-01-02_41f960d5-236c-4a94-9222-a339e0ddd365.jpeg'
      }
    },
    {
      id : 4,
      title : 'D',
      createDate : '15/8/20',
      updateDate : '15/8/20',
      file : {
        id : 1,
        nomOriginal : 'XX',
        chemin : 'TTT',
        publicUrl : 'https://img.maxisciences.com/s3/frgsd/300/photographie/default_2020-01-02_41f960d5-236c-4a94-9222-a339e0ddd365.jpeg'
      }
    },
  ];

  const currentItem = 
  {
    id : 3,
    title : 'C',
    createDate : '15/8/20',
    updateDate : '15/8/20',
    file : {
      id : 1,
      nomOriginal : 'XX',
      chemin : 'TTT',
      publicUrl : 'https://pharmacie.s3.eu-west-3.amazonaws.com/files/20201012105540tenamenprotectionurinairemasculinepdf.pdf'
    }
  };


export const CommentStory = () => (
    <Details listResult={listResult} currentItem={currentItem} />
);
