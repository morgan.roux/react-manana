import { MouseEvent } from 'react';
export interface MenuActionProps {
  anchorEl: Element | ((element: Element) => Element) | null | undefined;
  open: boolean;
  handleClose: () => void;
  handleEdit: () => void;
  handleShowMenu: (modeleEcritureAutomatique: any, event: MouseEvent<HTMLElement>) => void;
  modeleEcritureAutomatique: any;
  setOpenDeleteDialog: (boolean: true) => void;
}
