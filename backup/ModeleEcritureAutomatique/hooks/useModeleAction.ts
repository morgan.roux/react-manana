import { useState } from 'react';
import { useApplicationContext } from '@lib/common';
import {
  useCreateEfModeleEcritureMutation,
  useDeleteEfModeleEcritureMutation,
  useUpdateEfModeleEcritureMutation,
} from '@lib/common/src/federation';
import { ModeleEcritureSave } from '../form/ModeleEcritureAutomatiqueForm';

export const useModeleAction = (refetch?: () => void) => {
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const { notify } = useApplicationContext();

  const [createModeleEcriture] = useCreateEfModeleEcritureMutation({
    onCompleted: () => {
      setSaved(true);
      setSaving(false);
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `erreur: ${err.message}`,
      });
      setSaving(false);
    },
  });

  const [updateModeleEcriture] = useUpdateEfModeleEcritureMutation({
    onCompleted: () => {
      setSaved(true);
      setSaving(false);
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `erreur: ${err.message}`,
      });
      setSaving(false);
    },
  });

  const [deleteModeleEcriture] = useDeleteEfModeleEcritureMutation({
    onCompleted: () => {
      refetch && refetch();
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `erreur: ${err.message}`,
      });
    },
  });

  const handleSave = ({
    id,
    titre,
    description,
    idJournalType,
    isControleTva,
    isActif,
    lignes,
  }: ModeleEcritureSave) => {
    setSaving(true);
    setSaved(false);

    if (id) {
      updateModeleEcriture({
        variables: {
          id,
          input: {
            titre,
            description,
            idJournalType,
            isControleTva,
            isActif,
            lignes: lignes?.map(({ isContrepartie, guideSaisie, sens, compteComptable }) => ({
              isContrepartie,
              guideSaisie,
              sens,
              idCompteComptable: compteComptable?.id || '',
            })),
          },
        },
      });
    } else {
      createModeleEcriture({
        variables: {
          input: {
            titre,
            description,
            idJournalType,
            isControleTva,
            isActif,
            lignes: lignes?.map(({ isContrepartie, guideSaisie, sens, compteComptable }) => ({
              isContrepartie,
              guideSaisie,
              sens,
              idCompteComptable: compteComptable?.id || '',
            })),
          },
        },
      });
    }
  };

  const handleDelete = (id?: string) => {
    if (id) {
      deleteModeleEcriture({
        variables: {
          input: {
            id,
          },
        },
      });
    }
  };

  return {
    handleSave,
    handleDelete,
    saving,
    saved,
    setSaved,
  };
};
