import { useApplicationContext } from '@lib/common';
import { EfModeleEcritureFilter, useEfModeleEcrituresLazyQuery } from '@lib/common/src/federation';

export const useModeleEcriture = () => {
  const { currentPharmacie } = useApplicationContext();

  const [loadModeleEcriture, loadingModeleEcriture] = useEfModeleEcrituresLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const handleSearch = ({ skip, take, searchText, filterJournal }: any) => {
    const filterAnd: EfModeleEcritureFilter[] = [
      {
        idPharmacie: {
          eq: currentPharmacie.id,
        },
      },
    ];
    if (searchText) {
      filterAnd.push({
        titre: {
          iLike: `%${searchText}%`,
        },
      });
    }
    if (filterJournal && filterJournal !== 'Tous') {
      filterAnd.push({
        idJournalType: {
          eq: filterJournal
        }
      })
    }

    loadModeleEcriture({
      variables: {
        filter: {
          and: filterAnd,
        },
        paging: {
          offset: skip,
          limit: take,
        },
      },
    });
  };

  return {
    handleSearch,
    loadingModeleEcriture
  };
};
