import theme from '@lib/common/src/components/EspaceDocumentaire/theme';
import { createStyles, lighten, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
    },
    header: {
      position: 'sticky',
      top: 0,
      zIndex: 999,
      width: '100%',
      height: 70,
      display: 'flex',
      padding: '0 24px',
      marginBottom: '16px',
      justifyContent: 'space-between',
      alignItems: 'center',
      color: theme.palette.common.white,
      background: lighten(theme.palette.primary.main, 0.1),
    },
    editor: {
      '& .ql-container.ql-snow': {
        minHeight: '140px !important',
      },
    },
    body: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      margin: 'auto',
      minWidth: '990px',
      padding: '10px',
    },
    formulaire: {},
    tableCell: {
      padding: 0,
      color: '#fff',
      border: 'none',
    },
    tableCellAction: {
      padding: 0,
      color: '#000',
      border: 'none',
    },
    tableCellTitle: {
      color: '#000',
      fontSize: '16px',
      fontFamily: 'Roboto',
      fontWeight: 600,
      padding: 0,
      border: 'none',
      opacity: 0.8,
    },
    customFormText: {
      width: '170px',
    },
    customFormTextSens: {
      width: '110px',
    },
    customFormTextTva: {
      width: '100px',
      paddingLeft: 16,
    },
    actionDelete: {
      display: 'flex',
      cursor: 'pointer',
      '& .main-MuiSvgIcon-root': {
        color: '#000',
      },
      '& .main-MuiTypography-root': {
        textTransform: 'uppercase',
        marginLeft: '8px',
        color: '#000',
      },
    },
    actionAdd: {
      display: 'flex',
      cursor: 'pointer',
      float: 'right',
      '& .main-MuiSvgIcon-root': {
        color: theme.palette.primary.main,
      },
      '& .main-MuiTypography-root': {
        textTransform: 'uppercase',
        marginLeft: '8px',
        color: theme.palette.primary.main,
      },
    },
    sensContainer: {
      paddingTop: 4,
    },
    compteContainer: {
      minWidth: 110,
    },
  })
);
