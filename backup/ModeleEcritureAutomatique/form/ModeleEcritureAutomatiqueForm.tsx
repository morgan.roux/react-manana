import { CustomEditorText, CustomFormTextField, FormButtons, MontantInputField } from '@lib/ui-kit';
import CustomCheckbox from '@lib/ui-kit/src/components/atoms/CustomCheckBox/CustomCheckbox';
import {
  Box,
  FormControl,
  IconButton,
  Radio,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import React, { useState, ChangeEvent, useEffect } from 'react';
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './styles';
import { JournalTypeInput, SensInput } from '../../../../components/comptabilite';
import { EfCompteComptableInfoFragment, useEfModeleEcritureLazyQuery } from '@lib/common/src/federation';
import { useModeleAction } from '../hooks/useModeleAction';
import { useHistory, useLocation } from 'react-router';
import { CompteComptableInput } from '@lib/common';

export interface ModeleEcritureLigne {
  id?: string;
  isContrepartie?: boolean;
  sens?: string;
  guideSaisie?: string;
  compteComptable?: EfCompteComptableInfoFragment;
}

export interface ModeleEcritureSave {
  id?: string;
  titre: string;
  description?: string;
  idJournalType: string;
  isActif?: boolean;
  isControleTva?: boolean;
  lignes?: ModeleEcritureLigne[];
}

const initialValue: ModeleEcritureSave = {
  titre: '',
  description: '',
  idJournalType: '',
  isActif: false,
  isControleTva: false,
  lignes: [{}],
};

const initialLigne: ModeleEcritureLigne = {
  isContrepartie: false,
  sens: 'DEBIT',
  compteComptable: undefined,
  guideSaisie: '',
};

const ModeleEcritureAutomatiqueForm = () => {
  const [value, setValue] = useState<ModeleEcritureSave>(initialValue);

  const { handleSave, saving, saved, setSaved } = useModeleAction();
  const history = useHistory();
  const { search } = useLocation();
  const id = new URLSearchParams(search).get('id');
  const idComptes = (value.lignes || []).map((ligne) => ligne.compteComptable?.id || '');

  useEffect(() => {
    if (saved) {
      setValue(initialValue);
      setSaved(false);
      handleClose();
    }
  }, [saved]);

  useEffect(() => {
    if (id) {
      loadModeleEcriture({
        variables: {
          id,
        },
      });
    }
  }, [id]);

  const [loadModeleEcriture] = useEfModeleEcritureLazyQuery({
    fetchPolicy: 'cache-and-network',
    onCompleted: (result) => {
      if (result.eFModeleEcriture) {
        const { id, titre, description, idJournalType, isActif, isControleTva, lignes } = result.eFModeleEcriture;
        setValue({
          id,
          titre,
          description: description || '',
          idJournalType: idJournalType || '',
          isActif: isActif || false,
          isControleTva: isControleTva || false,
          lignes: lignes?.length
            ? lignes?.map(({ sens, compteComptable, isContrepartie, guideSaisie }) => ({
                sens: sens || '',
                compteComptable: compteComptable || undefined,
                isContrepartie: isContrepartie || false,
                guideSaisie: guideSaisie || '',
              }))
            : [],
        });
      }
    },
  });

  const handleChange = (name: string, value?: any) => {
    setValue((prev) => ({ ...prev, [name]: value }));
  };
  const handleChangeEvent = (e: ChangeEvent<HTMLInputElement>) => handleChange(e.target.name, e.target.value);

  console.log('modele', value);

  const handleRadioChange = (index: number) => {
    setValue((prev) => ({
      ...prev,
      lignes: prev.lignes?.map((ligne, idx) => {
        if (index === idx) {
          return { ...ligne, isContrepartie: !ligne.isContrepartie };
        }
        return ligne;
      }),
    }));
  };

  const handleChangeLigne = (index: number, name: string, value?: any) => {
    setValue((prev) => ({
      ...prev,
      lignes: prev.lignes?.map((ligne, idx) => {
        if (index === idx) {
          return { ...ligne, [name]: value };
        }
        return ligne;
      }),
    }));
  };

  const handleChangeCompte = (index: number, compte?: EfCompteComptableInfoFragment) => {
    handleChangeLigne(index, 'compteComptable', compte);
    handleChangeLigne(index, 'sens', compte?.sens);
    handleChangeLigne(index, 'guideSaisie', compte?.libelle);
  };

  const handleAddRow = () => {
    setValue((prev) => ({ ...prev, lignes: [...(prev.lignes || []), initialLigne] }));
  };
  const handleRemoveRow = (idx: any) => {
    setValue((prev) => ({ ...prev, lignes: prev.lignes?.filter((_, index) => index !== idx) }));
  };

  const classes = useStyles();

  const isValid = () => {
    return (
      value.titre &&
      value.idJournalType &&
      (!value.lignes?.length ||
        value.lignes.reduce((accumulator: boolean, ligne) => {
          return accumulator && !!ligne.compteComptable?.id;
        }, true))
    );
  };
  const handleClose = () => {
    history.goBack();
  };

  return (
    <div className={classes.root}>
      <Box className={classes.header}>
        <Typography style={{ margin: 'auto', fontSize: '24px', fontFamily: 'Roboto', fontWeight: 600 }}>
          {id ? 'Modification de modèle' : 'Nouveau Modèle'}
        </Typography>
        <IconButton color="inherit" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </Box>
      <Box className={classes.body}>
        <Box>
          <CustomFormTextField
            label="Titre du modèle"
            name="titre"
            placeholder="Titre du modèle"
            required
            type="text"
            value={value.titre}
            onChange={handleChangeEvent}
            className={classes.formulaire}
          />
        </Box>
        <Box>
          <CustomEditorText
            value={value.description || ''}
            placeholder="Description"
            onChange={(text: string) => handleChange('description', text)}
            className={classes.editor}
          />
        </Box>
        <Box style={{ marginTop: '16px' }}>
          <JournalTypeInput idType={value.idJournalType} onChange={handleChangeEvent} name="idJournalType" />
        </Box>
        <Box>
          <Typography style={{ fontSize: '18px', fontFamily: 'Roboto', fontWeight: 600 }}>Paramètres</Typography>
        </Box>
        <Box py={2} display="flex" flexDirection="row" justifyContent="space-between">
          <FormControl>
            <CustomCheckbox
              name="isActif"
              checked={value.isActif}
              onChange={(e: ChangeEvent<HTMLInputElement>) => handleChange(e.target.name, e.target.checked)}
              label="Actif"
            />
          </FormControl>
          <FormControl>
            <CustomCheckbox
              name="isControleTva"
              checked={value.isControleTva}
              onChange={(e: ChangeEvent<HTMLInputElement>) => handleChange(e.target.name, e.target.checked)}
              label="Contrôle TVA"
            />
          </FormControl>
          <div></div>
        </Box>
        <Box>
          <Typography style={{ fontSize: '18px', fontFamily: 'Roboto', fontWeight: 600 }}>Ventilation</Typography>
        </Box>
        <Box style={{ marginTop: '16px' }}>
          <Table className="table table-bordered table-hover" id="tab_logic">
            <TableHead>
              <TableRow>
                <TableCell className={classes.tableCellTitle}> Contrepartie </TableCell>
                <TableCell className={classes.tableCellTitle} align="center">
                  {' '}
                  Sens{' '}
                </TableCell>
                <TableCell className={classes.tableCellTitle} align="center">
                  {' '}
                  Compte{' '}
                </TableCell>
                <TableCell className={classes.tableCellTitle} align="center">
                  {' '}
                  Guide de saisie{' '}
                </TableCell>
                <TableCell className={classes.tableCellTitle} align="center">
                  {' '}
                  TVA{' '}
                </TableCell>
                <TableCell className={classes.tableCellTitle} />
              </TableRow>
            </TableHead>
            <TableBody>
              {value.lignes &&
                value.lignes.map((modeleLigne, idx) => (
                  <TableRow id="addr0" key={idx}>
                    <TableCell className={classes.tableCell} align="center">
                      <Radio
                        checked={modeleLigne.isContrepartie}
                        onClick={() => handleRadioChange(idx)}
                        inputProps={{ 'aria-label': 'A' }}
                      />
                    </TableCell>
                    <TableCell className={classes.tableCell} align="center">
                      <Box className={classes.sensContainer}>
                        <SensInput
                          onChange={(e) => handleChangeLigne(idx, 'sens', e.target.value)}
                          sens={modeleLigne.sens}
                          label=""
                          required={false}
                        />
                      </Box>
                    </TableCell>
                    <TableCell className={classes.tableCell} align="center">
                      <Box pl={2} className={classes.compteContainer}>
                        <CompteComptableInput
                          value={modeleLigne.compteComptable}
                          onChange={(compte) => handleChangeCompte(idx, compte)}
                          isTva={idx !== 0}
                          idComptes={idComptes}
                        />
                      </Box>
                    </TableCell>
                    <TableCell className={classes.tableCell} align="center">
                      <Box pl={2}>
                        <CustomFormTextField
                          type="text"
                          onChange={(e) => handleChangeLigne(idx, 'guideSaisie', e.target.value)}
                          value={modeleLigne.guideSaisie}
                          fullWidth
                        />
                      </Box>
                    </TableCell>
                    <TableCell className={classes.tableCell} align="center">
                      <Box className={classes.customFormTextTva}>
                        <MontantInputField
                          value={modeleLigne.compteComptable?.tva?.taux}
                          onChangeValue={() => {}}
                          suffix="%"
                          disabled
                        />
                      </Box>
                    </TableCell>
                    <TableCell className={classes.tableCell} align="center">
                      {idx !== 0 && (
                        <Box
                          className={classes.actionDelete}
                          onClick={() => {
                            handleRemoveRow(idx);
                          }}
                        >
                          <Box>
                            <CloseIcon />
                          </Box>
                          <Typography>Supprimer</Typography>
                        </Box>
                      ) }
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
          <Box onClick={handleAddRow} className={classes.actionAdd}>
            <Box>
              <AddIcon />
            </Box>
            <Typography>Ajouter une ligne</Typography>
          </Box>
        </Box>
        <Box pt={1}>
          <FormButtons
            onClickCancel={handleClose}
            onClickConfirm={() => handleSave(value)}
            disableConfirm={!isValid() || saving}
          />
        </Box>
      </Box>
    </div>
  );
};

export default ModeleEcritureAutomatiqueForm;
