import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Delete, Edit, MoreHoriz } from '@material-ui/icons';
import React from 'react';
import { MenuActionProps } from './../interface';

export const menuAction = ({
  anchorEl,
  open,
  handleClose,
  handleEdit,
  handleShowMenu,
  modeleEcritureAutomatique,
  setOpenDeleteDialog,
}: MenuActionProps) => {
  return (
    <div key={`row-${modeleEcritureAutomatique.id}`}>
      <IconButton
        aria-controls="simple-menu"
        aria-haspopup="true"
        // eslint-disable-next-line react/jsx-no-bind
        onClick={handleShowMenu.bind(null, modeleEcritureAutomatique)}
      >
        <MoreHoriz />
      </IconButton>
      <Menu
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open /*&& row.id === analyseToEdit?.id*/}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        <MenuItem
          onClick={(event) => {
            event.stopPropagation();
            handleClose();
            handleEdit(modeleEcritureAutomatique);
          }}
        >
          <ListItemIcon>
            <Edit />
          </ListItemIcon>
          <Typography variant="inherit">Modifier</Typography>
        </MenuItem>

        <MenuItem
          onClick={(event) => {
            event.stopPropagation();
            handleClose();
            setOpenDeleteDialog(true);
          }}
        >
          <ListItemIcon>
            <Delete />
          </ListItemIcon>
          <Typography variant="inherit">Supprimer</Typography>
        </MenuItem>
      </Menu>
    </div>
  );
};
