import { Column } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React from 'react';
import { EfModeleEcritureInfoFragment } from '@lib/common/src/federation';
import { MenuActionProps } from './../interface';
import { menuAction } from './menuAction';

export const useModeleEcritureColumn = ({
  anchorEl,
  open,
  handleClose,
  handleEdit,
  handleShowMenu,
  modeleEcritureAutomatique,
  setOpenDeleteDialog,
}: MenuActionProps) => {
  const columns: Column[] = [
    {
      name: 'journal',
      label: 'Journal',
      renderer: (row: EfModeleEcritureInfoFragment) => {
        return <Box>{row.journalType?.libelle}</Box>;
      },
    },
    {
      name: 'titre',
      label: 'Titre du journal',
      renderer: (row: EfModeleEcritureInfoFragment) => {
        return <Box>{row.titre}</Box>;
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: EfModeleEcritureInfoFragment) => {
        return menuAction({
          anchorEl,
          open,
          handleClose,
          handleEdit: () => handleEdit(),
          handleShowMenu: (_, e) => {
            handleShowMenu(row, e);
          },
          modeleEcritureAutomatique,
          setOpenDeleteDialog,
        });
      },
    },
  ];
  return columns;
};
