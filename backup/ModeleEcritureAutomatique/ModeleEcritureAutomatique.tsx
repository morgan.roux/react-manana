import { ConfirmDeleteDialog, Table, TableChange, TopBar } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC, useState, MouseEvent } from 'react';
import { useStyles } from './style';
import { useHistory } from 'react-router';
import { useModeleEcriture } from './hooks/useModeleEcriture';
import { useModeleEcritureColumn } from './useModeleEcriture/useModeleEcritureColumn';
import { useModeleAction } from './hooks/useModeleAction';
import { JournalTypeFilter } from '../../../components/comptabilite/JournalTypeFilter';

const ModeleEcritureAutomatique: FC<{}> = () => {
  const modeleEcritureAutomatique = {};
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [modeleEcritureSelected, setModeleEcritureSelected] = useState<any>();
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(10);
  const [filterJournal, setFilterJournal] = useState<string>();
  const open = Boolean(anchorEl);

  const { handleSearch: onSearch, loadingModeleEcriture } = useModeleEcriture();
  const { handleDelete: onDelete } = useModeleAction(loadingModeleEcriture.refetch);

  const history = useHistory();
  const styles = useStyles();

  const handleSearch = ({ skip, take, searchText: search }: TableChange) => {
    onSearch({ skip, take, filterJournal, searchText: search });
    setSkip(skip);
    setTake(take);
  };

  const handleSortTable = (column: string, direction: 'DESC' | 'ASC') => {
    // TODO
  };

  const handleFilter = (id?: string) => {
    onSearch({ skip, take, filterJournal: id });
    setFilterJournal(id);
  };

  const handleOpenAdd = () => {
    history.push('/db/modele-ecriture-automatique/form');
  };

  const handleEdit = () => {
    history.push({
      pathname: `/db/modele-ecriture-automatique/form`,
      search: `?id=${modeleEcritureSelected?.id}`,
    });
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const handleShowMenu = (modeleEcriture: any, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
    setModeleEcritureSelected(modeleEcriture);
  };

  const handleConfirmDelete = () => {
    onDelete(modeleEcritureSelected?.id);
    setOpenDeleteDialog(false);
  };

  const columns = useModeleEcritureColumn({
    anchorEl,
    open,
    handleClose,
    handleEdit,
    handleShowMenu,
    modeleEcritureAutomatique,
    setOpenDeleteDialog,
  });

  return (
    <Box>
      <Table
        loading={false}
        columns={columns}
        data={loadingModeleEcriture.data?.eFModeleEcritures?.nodes || []}
        search={false}
        selectable={false}
        rowsTotal={loadingModeleEcriture.data?.eFModeleEcritures?.totalCount}
        onRunSearch={handleSearch}
        onSortColumn={handleSortTable}
        topBarComponent={(searchComponent) => (
          <TopBar
            handleOpenAdd={handleOpenAdd}
            searchComponent={searchComponent}
            titleButton="AJOUTER MODELE"
            titleTopBar="Liste des Modèles d'Ecriture Automatique"
            withFilter
            searchContainerClassName={styles.searchTopBar}
            style={'width: auto'}
            className={styles.content}
            filterComponent={<JournalTypeFilter onFilter={handleFilter} />}
            titleTopBarClassName={styles.titleTobarProps}
          />
        )}
      />
      <ConfirmDeleteDialog
        title="Suppression du modèle"
        content="Voulez-vous supprimez ce modèle"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        confirmBtnLabel="Supprimer"
        onClickConfirm={handleConfirmDelete}
      />
    </Box>
  );
};

export default ModeleEcritureAutomatique;
