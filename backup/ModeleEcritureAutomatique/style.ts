import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      width: 'auto !important',
    },
    searchTopBar: {
      marginLeft: 0,
    },
    buttonJournal: {
      '& .main-MuiButtonBase-root': {
        background: '#FAFAFA'
      },
    },
    titleTobarProps: {
      fontSize: '18px !important',
      fontFamily: 'Roboto !important',
      fontWeight: 600,
    },
  })
);
