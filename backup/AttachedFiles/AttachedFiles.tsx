import { Box } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { NewCustomButton } from '../CustomButton';
import GetAppIcon from '@material-ui/icons/GetApp';
import ReactIframe from 'react-iframe';
import useStyles from './style';
import { FileViewer } from '../../molecules/FileViewer';
//@ts-ignore

export const downloadFile = (link: string): void => {
  alert(`Lien : ${link}`);
};

export interface IFichier {
  id: any;
  nomOriginal: string;
  type: string;
  chemin: string;
  publicUrl: string;
}

export interface AttachedFilesProps {
  file: IFichier;
  downloadable?: boolean;
  onCompleteDownload?: (fichier: IFichier) => void;
}

const AttachedFiles: FC<AttachedFilesProps> = ({ file, onCompleteDownload, downloadable }) => {
  const classes = useStyles();
  const [downloading, setDownloading] = useState<boolean>(false);

  const download = async () => {
    const { publicUrl, nomOriginal } = file || {};
    if (publicUrl && nomOriginal) {
      setDownloading(true);
      fetch(publicUrl)
        .then((response) => response.blob())
        .then((blob) => {
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob([blob]));
          downloadLink.download = nomOriginal;
          downloadLink.click();

          if (onCompleteDownload) {
            onCompleteDownload(file);
          }
          setDownloading(false);
        });
    }
  };

  return (
    <Box mt={2}>
      {downloadable && (
        <NewCustomButton
          disabled={downloading}
          startIcon={<GetAppIcon />}
          children="Télécharger"
          style={{ marginBottom: 40 }}
          onClick={download}
        />
      )}
      <FileViewer className={classes.seeContentPdf} key={file.id} fileUrl={file.publicUrl} />
      {/*      <ReactIframe
        id={file.id}
        key={file.id}
        className={classes.seeContentPdf}
        width="100%"
        url={`${file.publicUrl}`}
/>*/}
    </Box>
  );
};

export default AttachedFiles;
