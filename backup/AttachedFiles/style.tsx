import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    buttonContainer: {
      display: 'flex',
      flexDirection: 'row',
      marginBottom: 25,
    },

    btnPasserCommande: {
      textTransform: 'none',
      marginRight: 8,
    },
    seeContentPdf: {
      height: 'calc(100vh - 351px)',
    },
  })
);

export default useStyles;
