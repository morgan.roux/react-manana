version: '3'
services:
  prisma:
    image: prismagraphql/prisma:1.34
    ports:
      - '${PRISMA_PORT:?PRISMA_PORT not set}:4466'
    restart: always
    environment:
      PRISMA_CONFIG: |
        port: ${PRISMA_PORT:?PRISMA_PORT not set}
        # uncomment the next line and provide the env var PRISMA_MANAGEMENT_API_SECRET=my-secret to activate cluster security
        managementApiSecret: ${PRISMA_MANAGEMENT_API_SECRET:?PRISMA_MANAGEMENT_API_SECRET not set}
        databases:
          default:
            connector: postgres
            host: postgres
            database: prisma
            schema: public
            port: ${POSTGRES_PORT:?POSTGRES_PORT not set}
            user: ${POSTGRES_USER}
            password: ${POSTGRES_PASSWORD}
            connectionLimit: 60
            rawAccess: true

  basis-service:
    build:
      context: services/
      dockerfile: basis-service.Dockerfile
    environment:
      SEQUELIZE_HOST: ${SEQUELIZE_HOST:?SEQUELIZE_HOST not set}
      SEQUELIZE_PORT: ${SEQUELIZE_PORT:?SEQUELIZE_PORT not set}
      SEQUELIZE_USERNAME: ${SEQUELIZE_USERNAME:?SEQUELIZE_USERNAME not set}
      SEQUELIZE_PASSWORD: ${SEQUELIZE_PASSWORD:?SEQUELIZE_PASSWORD not set}
      SEQUELIZE_DATABASE: ${SEQUELIZE_DATABASE:?SEQUELIZE_DATABASE not set}
      SEQUELIZE_SCHEMA: ${SEQUELIZE_SCHEMA:?SEQUELIZE_SCHEMA not set}
      GRAPHQL_DEBUG: ${GRAPHQL_DEBUG:?GRAPHQL_DEBUG not set}
      GRAPHQL_PLAYGROUND: ${GRAPHQL_PLAYGROUND:?GRAPHQL_PLAYGROUND not set}
      API_URL: ${API_URL:?API_URL not set}
    depends_on:
      - postgres
      - graphql
      - prisma
    ports:
      - ${BASIS_SERVICE_PORT}:4001

  basis-query-service:
    build:
      context: services/
      dockerfile: basis-query-service.Dockerfile
    environment:
      SEQUELIZE_HOST: ${SEQUELIZE_HOST:?SEQUELIZE_HOST not set}
      SEQUELIZE_PORT: ${SEQUELIZE_PORT:?SEQUELIZE_PORT not set}
      SEQUELIZE_USERNAME: ${SEQUELIZE_USERNAME:?SEQUELIZE_USERNAME not set}
      SEQUELIZE_PASSWORD: ${SEQUELIZE_PASSWORD:?SEQUELIZE_PASSWORD not set}
      SEQUELIZE_DATABASE: ${SEQUELIZE_DATABASE:?SEQUELIZE_DATABASE not set}
      SEQUELIZE_SCHEMA: ${SEQUELIZE_SCHEMA:?SEQUELIZE_SCHEMA not set}
      GRAPHQL_DEBUG: ${GRAPHQL_DEBUG:?GRAPHQL_DEBUG not set}
      GRAPHQL_PLAYGROUND: ${GRAPHQL_PLAYGROUND:?GRAPHQL_PLAYGROUND not set}
    depends_on:
      - postgres
    ports:
      - ${BASIS_QUERY_SERVICE_PORT}:4008

  ged-service:
    build:
      context: services/
      dockerfile: ged-service.Dockerfile
    environment:
      SEQUELIZE_HOST: ${SEQUELIZE_HOST:?SEQUELIZE_HOST not set}
      SEQUELIZE_PORT: ${SEQUELIZE_PORT:?SEQUELIZE_PORT not set}
      SEQUELIZE_USERNAME: ${SEQUELIZE_USERNAME:?SEQUELIZE_USERNAME not set}
      SEQUELIZE_PASSWORD: ${SEQUELIZE_PASSWORD:?SEQUELIZE_PASSWORD not set}
      SEQUELIZE_DATABASE: ${SEQUELIZE_DATABASE:?SEQUELIZE_DATABASE not set}
      SEQUELIZE_SCHEMA: ${SEQUELIZE_SCHEMA:?SEQUELIZE_SCHEMA not set}
      GRAPHQL_DEBUG: ${GRAPHQL_DEBUG:?GRAPHQL_DEBUG not set}
      GRAPHQL_PLAYGROUND: ${GRAPHQL_PLAYGROUND:?GRAPHQL_PLAYGROUND not set}
    depends_on:
      - postgres
    ports:
      - ${GED_SERVICE_PORT}:4007

  iam-service:
    build:
      context: services/
      dockerfile: iam-service.Dockerfile
    environment:
      SEQUELIZE_HOST: ${SEQUELIZE_HOST:?SEQUELIZE_HOST not set}
      SEQUELIZE_PORT: ${SEQUELIZE_PORT:?SEQUELIZE_PORT not set}
      SEQUELIZE_USERNAME: ${SEQUELIZE_USERNAME:?SEQUELIZE_USERNAME not set}
      SEQUELIZE_PASSWORD: ${SEQUELIZE_PASSWORD:?SEQUELIZE_PASSWORD not set}
      SEQUELIZE_DATABASE: ${SEQUELIZE_DATABASE:?SEQUELIZE_DATABASE not set}
      SEQUELIZE_SCHEMA: ${SEQUELIZE_SCHEMA:?SEQUELIZE_SCHEMA not set}
      GRAPHQL_DEBUG: ${GRAPHQL_DEBUG:?GRAPHQL_DEBUG not set}
      GRAPHQL_PLAYGROUND: ${GRAPHQL_PLAYGROUND:?GRAPHQL_PLAYGROUND not set}
      API_URL: ${API_URL:?API_URL not set}
      REDIS_HOST: ${REDIS_HOST:?REDIS_HOST not set}
      REDIS_PORT: ${REDIS_PORT:? REDIS_PORT not set}
    depends_on:
      - postgres
      - graphql
      - prisma
    ports:
      - ${IAM_SERVICE_PORT}:4002

  storage-service:
    build:
      context: services/
      dockerfile: storage-service.Dockerfile
    environment:
      SEQUELIZE_HOST: ${SEQUELIZE_HOST:?SEQUELIZE_HOST not set}
      SEQUELIZE_PORT: ${SEQUELIZE_PORT:?SEQUELIZE_PORT not set}
      SEQUELIZE_USERNAME: ${SEQUELIZE_USERNAME:?SEQUELIZE_USERNAME not set}
      SEQUELIZE_PASSWORD: ${SEQUELIZE_PASSWORD:?SEQUELIZE_PASSWORD not set}
      SEQUELIZE_DATABASE: ${SEQUELIZE_DATABASE:?SEQUELIZE_DATABASE not set}
      SEQUELIZE_SCHEMA: ${SEQUELIZE_SCHEMA:?SEQUELIZE_SCHEMA not set}
      GRAPHQL_DEBUG: ${GRAPHQL_DEBUG:?GRAPHQL_DEBUG not set}
      GRAPHQL_PLAYGROUND: ${GRAPHQL_PLAYGROUND:?GRAPHQL_PLAYGROUND not set}
      AWS_S3_BUCKET: ${AWS_S3_BUCKET:?AWS_S3_BUCKET not set}
      AWS_S3_ACCESS_KEY: ${AWS_S3_ACCESS_KEY:?AWS_S3_ACCESS_KEY not set}
      AWS_S3_EXPIRES: ${AWS_S3_EXPIRES:? AWS_S3_EXPIRES not set}
      AWS_S3_SECRET_KEY: ${AWS_S3_SECRET_KEY:? AWS_S3_SECRET_KEY not set}
      AWS_S3_REGION: ${AWS_S3_REGION:? AWS_S3_REGION not set}
      AWS_S3_ENDPOINT: ${AWS_S3_ENDPOINT:? AWS_S3_ENDPOINT not set}
    depends_on:
      - postgres
    ports:
      - ${STORAGE_SERVICE_PORT}:4005

  demarche-qualite-service:
    build:
      context: services/
      dockerfile: demarche-qualite-service.Dockerfile
    environment:
      SEQUELIZE_HOST: ${SEQUELIZE_HOST:?SEQUELIZE_HOST not set}
      SEQUELIZE_PORT: ${SEQUELIZE_PORT:?SEQUELIZE_PORT not set}
      SEQUELIZE_USERNAME: ${SEQUELIZE_USERNAME:?SEQUELIZE_USERNAME not set}
      SEQUELIZE_PASSWORD: ${SEQUELIZE_PASSWORD:?SEQUELIZE_PASSWORD not set}
      SEQUELIZE_DATABASE: ${SEQUELIZE_DATABASE:?SEQUELIZE_DATABASE not set}
      SEQUELIZE_SCHEMA: ${SEQUELIZE_SCHEMA:?SEQUELIZE_SCHEMA not set}
      GRAPHQL_DEBUG: ${GRAPHQL_DEBUG:?GRAPHQL_DEBUG not set}
      GRAPHQL_PLAYGROUND: ${GRAPHQL_PLAYGROUND:?GRAPHQL_PLAYGROUND not set}
    depends_on:
      - postgres
    ports:
      - ${DEMARCHE_QUALITE_SERVICE_PORT}:4004

  demarche-qualite-query-service:
    build:
      context: services/
      dockerfile: demarche-qualite-query-service.Dockerfile
    environment:
      SEQUELIZE_HOST: ${SEQUELIZE_HOST:?SEQUELIZE_HOST not set}
      SEQUELIZE_PORT: ${SEQUELIZE_PORT:?SEQUELIZE_PORT not set}
      SEQUELIZE_USERNAME: ${SEQUELIZE_USERNAME:?SEQUELIZE_USERNAME not set}
      SEQUELIZE_PASSWORD: ${SEQUELIZE_PASSWORD:?SEQUELIZE_PASSWORD not set}
      SEQUELIZE_DATABASE: ${SEQUELIZE_DATABASE:?SEQUELIZE_DATABASE not set}
      SEQUELIZE_SCHEMA: ${SEQUELIZE_SCHEMA:?SEQUELIZE_SCHEMA not set}
      GRAPHQL_DEBUG: ${GRAPHQL_DEBUG:?GRAPHQL_DEBUG not set}
      GRAPHQL_PLAYGROUND: ${GRAPHQL_PLAYGROUND:?GRAPHQL_PLAYGROUND not set}
    depends_on:
      - postgres
    ports:
      - ${DEMARCHE_QUALITE_QUERY_SERVICE_PORT}:4006

  search-service:
    build:
      context: services/
      dockerfile: search-service.Dockerfile
    environment:
      ELASTIC_SEARCH_NODE: ${ELASTIC_SEARCH_NODE:?ELASTIC_SEARCH_NODE not set}
      GRAPHQL_DEBUG: ${GRAPHQL_DEBUG:?GRAPHQL_DEBUG not set}
      GRAPHQL_PLAYGROUND: ${GRAPHQL_PLAYGROUND:?GRAPHQL_PLAYGROUND not set}
    depends_on:
      - elasticsearch
    ports:
      - ${SEARCH_SERVICE_PORT}:4003

  gateway-federation:
    build:
      context: services/
      dockerfile: gateway-federation.Dockerfile
    environment:
      GATEWAY_SERVER_CORS: ${GATEWAY_SERVER_CORS:?GATEWAY_SERVER_CORS not set}
      SEARCH_SERVICE_URL: ${SEARCH_SERVICE_URL:?SEARCH_SERVICE_URL not set}
      IAM_SERVICE_URL: ${IAM_SERVICE_URL:?IAM_SERVICE_URL not set}
      DEMARCHE_QUALITE_SERVICE_URL: ${DEMARCHE_QUALITE_SERVICE_URL:?DEMARCHE_QUALITE_SERVICE_URL not set}
      BASIS_SERVICE_URL: ${BASIS_SERVICE_URL:?BASIS_SERVICE_URL not set}
      STORAGE_SERVICE_URL: ${STORAGE_SERVICE_URL:?STORAGE_SERVICE_URL not set}
    depends_on:
      - search-service
      - iam-service
      - demarche-qualite-service
      - demarche-qualite-query-service
      - basis-service
      - storage-service
    ports:
      - ${GATEWAY_FEDERATION_PORT}:5000
  # client:
  #   build:
  #     context: client/web
  #     dockerfile: Dockerfile
  #   environment:
  #     REACT_APP_WS_URL: ws://localhost:4000
  #     REACT_APP_API_URL: http://localhost:4000
  #     REACT_APP_SAML_SSO_ENDPOINT: http://localhost:4001
  #     RREACT_APP_CHROME_EXTENSION_ID: jndnlkihngfelgknepkcdhnbpgonobdp
  #     REACT_APP_FIREFOX_EXTENSION_LINK: https://addons.mozilla.org/addon/gcr-pharma/
  #   volumes:
  #     - ./client/web/src:/app/src
  #   ports:
  #     - '3000:80'

  #indexing:
  #  build:
  #    context: servers/indexing
  #    dockerfile: dev.Dockerfile
  #  volumes:
  #    - ./servers/indexing/src:/indexing/src
  #    - ./servers/indexing/package.json:/indexing/package.json
  #  depends_on:
  #    - rabbitmq
  #    - elasticsearch
  #  environment:
  #    ELASTIC_SEARCH_NODE: ${ELASTIC_SEARCH_NODE:?ELASTIC_SEARCH_NODE not set}
  #    ELASTIC_SEARCH_LOG_LEVEL: ${ELASTIC_SEARCH_LOG_LEVEL:?ELASTIC_SEARCH_LOG_LEVEL not set}
  #    ELASTIC_SEARCH_VERSION: ${ELASTIC_SEARCH_VERSION:?ELASTIC_SEARCH_VERSION not set}
  #    RABBITMQ_HOST: ${RABBITMQ_HOST:?RABBITMQ_HOST not set}
  #    RABBITMQ_EXCHANGE: ${RABBITMQ_EXCHANGE:?RABBITMQ_EXCHANGE not set}
  #    RABBITMQ_EXCHANGE_TYPE: ${RABBITMQ_EXCHANGE_TYPE:?RABBITMQ_EXCHANGE_TYPE not set}
  #    API_URL: ${API_URL:?API_URL not set}
  #    API_CLIENT_ID: ${API_CLIENT_ID:?API_CLIENT_ID not set}
  #    API_CLIENT_SECRET: ${API_CLIENT_SECRET:?API_CLIENT_SECRET not set}

  graphql:
    build:
      context: servers/graphql
      dockerfile: dev.Dockerfile
    expose:
      - ${API_PORT}
    ports:
      - '${API_PORT}:${API_PORT}'
      - '9229:9229'
    volumes:
      - ./servers/graphql:/graphql
    depends_on:
      - redis
      - prisma
      - postgres
      - phppgadmin
      - elasticsearch
    #  - indexing
    environment:
      SPARKPOST_API_KEY: ${SPARKPOST_API_KEY:?SPARKPOST_API_KEY not set}
      CLIENT_URL: ${CLIENT_URL:?CLIENT_URL not set}
      REDIS_HOST: ${REDIS_HOST:-redis}
      REDIS_PORT: ${REDIS_PORT:-6379}
      REDIS_TLS: ${REDIS_TLS:-0}
      REDIS_EXPIRE: ${REDIS_EXPIRE:?REDIS_EXPIRE not set}
      REGISTRATION_TOKEN_EXPIRATION: ${REGISTRATION_TOKEN_EXPIRATION-259200}
      SPARKPOST_FROM_EMAIL: ${SPARKPOST_FROM_EMAIL:?SPARKPOST_FROM_EMAIL not set}
      PRISMA_SERVICE_SECRET: ${PRISMA_SERVICE_SECRET:?PRISMA_SERVICE_SECRET not set}
      PRISMA_MANAGEMENT_API_SECRET: ${PRISMA_MANAGEMENT_API_SECRET:?PRISMA_MANAGEMENT_API_SECRET not set}
      ELASTIC_SEARCH_NODE: ${ELASTIC_SEARCH_NODE:?ELASTIC_SEARCH_NODE not set}
      ELASTIC_SEARCH_LOG_LEVEL: ${ELASTIC_SEARCH_LOG_LEVEL:?ELASTIC_SEARCH_LOG_LEVEL not set}
      ELASTIC_SEARCH_VERSION: ${ELASTIC_SEARCH_VERSION:?ELASTIC_SEARCH_VERSION not set}
      RABBITMQ_HOST: ${RABBITMQ_HOST:?RABBITMQ_HOST not set}
      RABBITMQ_EXCHANGE: ${RABBITMQ_EXCHANGE:?RABBITMQ_EXCHANGE not set}
      RABBITMQ_EXCHANGE_TYPE: ${RABBITMQ_EXCHANGE_TYPE:?RABBITMQ_EXCHANGE_TYPE not set}
      PRISMA_END_POINT: ${PRISMA_END_POINT:?PRISMA_END_POINT not set}
      ENABLE_REINDEX_ALL: ${ENABLE_REINDEX_ALL:?ENABLE_REINDEX_ALL not set}
      SUPER_ADMIN_PWD: ${SUPER_ADMIN_PWD}
      SUPER_ADMIN_EMAIL: ${SUPER_ADMIN_EMAIL}
      SENDGRID_API_KEY: ${SENDGRID_API_KEY:?SENDGRID_API_KEY not set}
      USE_SENDGRID: ${USE_SENDGRID:?USE_SENDGRID not set }
      EMAIL_FROM: ${EMAIL_FROM:?EMAIL_FROM not set }
      AWS_S3_BUCKET: ${AWS_S3_BUCKET:?AWS_S3_BUCKET not set }
      AWS_S3_ACCESS_KEY: ${AWS_S3_ACCESS_KEY:?AWS_S3_ACCESS_KEY not set }
      AWS_S3_SECRET_KEY: ${AWS_S3_SECRET_KEY:?AWS_S3_SECRET_KEY not set }
      AWS_S3_REGION: ${AWS_S3_REGION:?AWS_S3_REGION not set }
      AWS_S3_ENDPOINT: ${AWS_S3_ENDPOINT:?AWS_S3_ENDPOINT not set }
      FORGOT_PASSWORD_TEMPLATE_ID: ${FORGOT_PASSWORD_TEMPLATE_ID:?FORGOT_PASSWORD_TEMPLATE_ID not set}
      CREATE_USER_TEMPLATE_ID: ${CREATE_USER_TEMPLATE_ID:?CREATE_USER_TEMPLATE_ID not set}
      CONFIRM_IP_TEMPLATE_ID: ${CONFIRM_IP_TEMPLATE_ID:?CONFIRM_IP_TEMPLATE_ID not set}
      API_URL: ${API_URL:?API_URL not set}
      API_CLIENT_ID: ${API_CLIENT_ID:?API_CLIENT_ID not set}
      API_CLIENT_SECRET: ${API_CLIENT_SECRET:?API_CLIENT_SECRET not set}
      INDEXING_AUTHORIZED_OPEN_CLOSE: ${INDEXING_AUTHORIZED_OPEN_CLOSE:?INDEXING_AUTHORIZED_OPEN_CLOSE not set}
      REINDEXING_PROD: ${REINDEXING_PROD:?REINDEXING_PROD not set}
      JWT_SECRET: ${JWT_SECRET:?JWT_SECRET not set}
      JWT_SIGN_OPTIONS_EXPIRES_IN_SECONDS: ${JWT_SIGN_OPTIONS_EXPIRES_IN_SECONDS:?JWT_SIGN_OPTIONS_EXPIRES_IN_SECONDS not set}
      SEQUELIZE_PORT: ${SEQUELIZE_PORT:-5432}
      SEQUELIZE_HOST: ${SEQUELIZE_HOST:?SEQUELIZE_HOST not set}
      SEQUELIZE_USERNAME: ${SEQUELIZE_USERNAME:?SEQUELIZE_USERNAME not set}
      SEQUELIZE_PASSWORD: ${SEQUELIZE_PASSWORD:?SEQUELIZE_PASSWORD not set}
      SEQUELIZE_DATABASE: ${SEQUELIZE_DATABASE:?SEQUELIZE_DATABASE not set}
      SEQUELIZE_SCHEMA: ${SEQUELIZE_SCHEMA:?SEQUELIZE_SCHEMA not set}
  #      GATEWAY_FEDERATION_URL: ${GATEWAY_FEDERATION_URL:?GATEWAY_FEDERATION_URL not set}
  redis:
    image: redis:5.0
    volumes:
      - ./VOLUMES/redis/data:/data
    ports:
      - ${REDIS_PORT:?REDIS_PORT not set}:6379

  postgres:
    image: postgres:12.0
    environment:
      POSTGRES_DB: ${POSTGRES_DB:?POSTGRES_DB not set}
      POSTGRES_USER: ${POSTGRES_USER:?POSTGRES_USER not set}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD:?POSTGRES_PASSWORD not set}
    volumes:
      - ./VOLUMES/postgres/data:/data/db
    ports:
      - ${POSTGRES_PORT:?POSTGRES_PORT not set}:5432

  elasticsearch:
    image: elasticsearch:7.4.0
    environment:
      - discovery.type=single-node
      - bootstrap.memory_lock=true
      - 'ES_JAVA_OPTS=-Xms1g -Xmx1g'
    ulimits:
      memlock:
        soft: -1
        hard: -1
    volumes:
      - ./VOLUMES/elasticsearch/data:/data/elasticsearch
    ports:
      - ${ELASTIC_SEARCH_PORT:?ELASTIC_SEARCH_PORT not set}:9200

  #rabbitmq:
  #  image: rabbitmq:3.8
  #  environment:
  #    RABBITMQ_ERLANG_COOKIE: ${RABBITMQ_ERLANG_COOKIE:?RABBITMQ_ERLANG_COOKIE not set}
  #  volumes:
  #    - ./VOLUMES/rabbitmq/data:/data/rabbitmq
  #  ports:
  #   - ${RABBITMQ_PORT:?RABBITMQ_PORT not set}:5672

  phppgadmin:
    image: dpage/pgadmin4:2019-11-12-2
    ports:
      - 8088:80
    environment:
      PGADMIN_DEFAULT_PASSWORD: ${PGADMIN_DEFAULT_PASSWORD}
      PGADMIN_DEFAULT_EMAIL: ${PGADMIN_DEFAULT_EMAIL}
      PHP_PG_ADMIN_SERVER_HOST: postgres
      PHP_PG_ADMIN_SERVER_PORT: ${POSTGRES_PORT}

  # metabase:
  #   image: metabase/metabase
  #   restart: always
  #   ports:
  #     - 3001:3000
  #   volumes:
  #     # declare your mount volume /host/dir:/container/dir
  #     - ./VOLUMES/metabase-data:/metabase-data
  #   environment:
  #     MB_DB_TYPE: ${MB_DB_TYPE}
  #     MB_DB_DBNAME: ${MB_DB_DBNAME}
  #     MB_DB_PORT: ${MB_DB_PORT}
  #     MB_DB_USER: ${MB_DB_USER}
  #     MB_DB_PASS: ${MB_DB_PASS}
  #     MB_DB_HOST: ${MB_DB_HOST}
  #   depends_on:
  #     - postgres
  #   links:
  #     - postgres

  sso:
    build:
      context: servers/saml-sso
      dockerfile: dev.Dockerfile
    ports:
      - ${SAML_SSO_PORT:?SAML_SSO_PORT not set}:4001
    volumes:
      - ./servers/saml-sso/src:/saml-sso/src
    environment:
      SAML_SSO_ENDPOINT: ${SAML_SSO_ENDPOINT}
      SAML_SSO_PORT: ${SAML_SSO_PORT}
      API_URL: ${API_URL}
      CLIENT_URL: ${CLIENT_URL}
      HELLOID_REDIRECT_URL: ${HELLOID_REDIRECT_URL}
      HELLOID_URL: ${HELLOID_URL}
    restart: always
