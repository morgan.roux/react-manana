version: '3'

networks:
  traefik_gateway:
    external: true

services:
  client:
    build:
      context: client/web
      dockerfile: Dockerfile
      args:
        REACT_APP_API_URL: ${API_URL:?API_URL not set}
    labels:
      - traefik.frontend.rule=Host:${BASE_HOSTNAME?Variable BASE_HOSTNAME not set}
      - traefik.enable=true
      - traefik.port=80
    networks:
      - traefik_gateway

  graphql:
    build:
      context: servers/graphql-mongo-prisma
      dockerfile: Dockerfile
      args:
        PRISMA_SERVICE_SECRET: ${PRISMA_SERVICE_SECRET:?PRISMA_SERVICE_SECRET not set}
    depends_on:
      - mongo-prisma
      - redis
    environment:
      SPARKPOST_API_KEY: ${SPARKPOST_API_KEY:?SPARKPOST_API_KEY not set}
      CLIENT_URL: ${CLIENT_URL:?CLIENT_URL not set}
      REDIS_HOST: ${REDIS_HOST:-redis}
      REDIS_PORT: ${REDIS_PORT:-6379}
      REDIS_TLS: ${REDIS_TLS:-0}
      REGISTRATION_TOKEN_EXPIRATION: ${REGISTRATION_TOKEN_EXPIRATION-259200}
      SPARKPOST_FROM_EMAIL: ${SPARKPOST_FROM_EMAIL:?SPARKPOST_FROM_EMAIL not set}
      PRISMA_SERVICE_SECRET: ${PRISMA_SERVICE_SECRET:?PRISMA_SERVICE_SECRET not set}
    labels:
      - traefik.frontend.rule=Host:${BASE_HOSTNAME?Variable BASE_HOSTNAME not set};PathPrefixStrip:/api;PathPrefix:/api
      - traefik.enable=true
      - traefik.port=4000
    networks:
      - traefik_gateway

  redis:
    image: redis:5.0
    restart: always
    volumes:
      - ./VOLUMES/redis/data:/data
    networks:
      - traefik_gateway
    labels:
      - traefik.enable=false

  mongo-prisma:
    image: mongo:3.6
    restart: always
    environment:
      MONGO_INITDB_ROOT_USERNAME: prisma
      MONGO_INITDB_ROOT_PASSWORD: prisma
    volumes:
      - ./VOLUMES/mongo/data:/data/db
    networks:
      - traefik_gateway
    labels:
      - traefik.enable=false

  prisma:
    image: prismagraphql/prisma:1.34
    environment:
      PRISMA_CONFIG: |
        port: 4466
        # uncomment the next line and provide the env var PRISMA_MANAGEMENT_API_SECRET=my-secret to activate cluster security
        managementApiSecret: ${PRISMA_MANAGEMENT_API_SECRET}
        databases:
          default:
            connector: mongo
            uri: 'mongodb://prisma:prisma@mongo-prisma'
      PRISMA_SERVICE_SECRET: ${PRISMA_SERVICE_SECRET:?PRISMA_SERVICE_SECRET not set}
    networks:
      - traefik_gateway
    ports:
      - 4466:4466
    labels:
      - traefik.enable=false
