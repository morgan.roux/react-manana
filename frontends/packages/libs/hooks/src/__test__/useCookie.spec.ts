import { renderHook, act } from '@testing-library/react-hooks';
import { useCookie } from '../index';
import faker from 'faker';

describe('useCookie', () => {
  it('should return cookie value and setter', () => {
    const cookieName = faker.random.word();
    const cookieValue = faker.random.word();

    const { result } = renderHook(() => useCookie(cookieName, cookieValue));
    expect(result.current[0]).toBe(cookieValue);

    const newValue = faker.random.word();
    act(() => result.current[1](newValue));
    expect(result.current[0]).toBe(newValue);
  });
});
