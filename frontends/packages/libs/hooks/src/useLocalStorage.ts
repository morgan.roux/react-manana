import { useState, useEffect } from 'react';

const useLocalStorage = <T extends string>(
  key: string,
  initialValue: T
): [T, (newValue: T | ((previous: T) => T)) => void] => {
  const [storedValue, setStoredValue] = useState<T>(initialValue);

  const setValue = (value: T | ((previous: T) => T)): void => {
    try {
      const valueToStore = value instanceof Function ? value(storedValue) : value;
      setStoredValue(valueToStore);
      window.localStorage.setItem(key, valueToStore);
    } catch (error) {
      // A more advanced implementation would handle the error case
      console.log(error);
    }
  };

  useEffect(() => {
    const storedValueInLocalStorage = window.localStorage.getItem(key) as T;
    if (storedValueInLocalStorage) setStoredValue(storedValueInLocalStorage);
  }, []);

  return [storedValue, setValue];
};

export default useLocalStorage;
