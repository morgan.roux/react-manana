import { useState, useEffect } from 'react';
import { getCookie, setCookie } from '@lib/helpers';

const useCookie = <T extends string>(
  name: string,
  initialValue: T,
  maxAge?: number
): [T, (newValue: T | ((previous: T) => T)) => void] => {
  const [storedValue, setStoredValue] = useState<T>(initialValue);
  const setValue = (value: T | ((previous: T) => T)): void => {
    const valueToStore = value instanceof Function ? value(storedValue) : value;

    setCookie(name, valueToStore, maxAge);
    setStoredValue(valueToStore);
  };

  useEffect(() => {
    const storedValueInCookie = getCookie(name) as T;
    if (storedValueInCookie) setStoredValue(storedValueInCookie);
  }, []);

  return [storedValue, setValue];
};

export default useCookie;
