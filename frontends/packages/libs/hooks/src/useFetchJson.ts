import { useState, useEffect } from 'react';

const useFetchJson = (path: string): { loading?: boolean; data?: Record<string, any>; error?: Error } => {
  const [loading, setIsLoading] = useState<boolean>(true);
  const [data, setData] = useState<Record<string, any>>();
  const [error, setError] = useState<Error>();

  const fetchData = () => {
    fetch(path, {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      cache: 'no-store',
    })
      .then((res) => res.json())
      .then((json) => {
        setData(json);
        setIsLoading(false);
      })
      .catch(setError);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return { data, loading, error };
};

export default useFetchJson;
