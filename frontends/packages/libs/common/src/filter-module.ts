import {
  ActivationParameter,
  Authorization,
  Feature,
  FeatureDevice,
  FilterableByParameter,
  ModuleDefinition,
} from './module';
import { ParameterInfoFragment } from './graphql';
import { User } from './federation';
import { ApplicationContextOptions } from './context';
import { isParameterValueEnabled, parseParameterValueAsBoolean } from './shared/operations/parametres/util';

export interface ModuleFilter {
  modules: ModuleDefinition[];
  parameters: ParameterInfoFragment[];
  user: User;
  device?: FeatureDevice;
}

export const filterUserEnabledModules = (filter: ModuleFilter): ModuleDefinition[] => {
  return filter.modules.map((module) => {
    let newModule = module;
    if (newModule.features) {
      newModule = {
        ...newModule,
        features: sortFeaturesByPreferredOrder(
          newModule.features.filter((feature) => {
            // Filter by device
            if (!isActiveOnDevice(feature, filter.device)) {
              return false;
            }
            // Filter by active when
            /*if (!isFeatureActiveWhen(feature, filter)) {
            return false;
          }*/

            // Filter by activation parameters
            if (!isActiveByParameters(feature, filter)) {
              return false;
            }

            // Filter by authorization
            if (!isActiveByAuthorization(feature, filter)) {
              return false;
            }

            return true;
          })
        ),
      };
    }

    if (newModule.routes) {
      newModule = {
        ...newModule,
        routes: newModule.routes.filter((route) => {
          // Filter by device
          if (!isActiveOnDevice(route, filter.device)) {
            return false;
          }
          // Filter by activation parameters
          if (!isActiveByParameters(route, filter)) {
            return false;
          }

          // Filter by authorization
          if (!isActiveByAuthorization(route, filter)) {
            return false;
          }

          return true;
        }),
      };
    }

    return newModule;
  });
};

interface FilterableByDevice {
  device?: FeatureDevice | FeatureDevice[];
}

const isActiveOnDevice = (filterable: FilterableByDevice, device?: FeatureDevice): boolean => {
  if (!filterable.device || !device || filterable.device === 'ALL') {
    return true;
  }
  return Array.isArray(filterable.device) ? filterable.device.includes(device) : filterable.device === device;
};

export const isFeatureActiveWhen = (feature: Feature, path: string, context: ApplicationContextOptions): boolean => {
  if (!feature.activeWhen) {
    return true;
  }

  if (typeof feature.activeWhen === 'string' || Array.isArray(feature.activeWhen)) {
    const paths = typeof feature.activeWhen === 'string' ? [feature.activeWhen] : feature.activeWhen;
    if (!path) {
      return false;
    }

    return paths.includes(path);
  }

  //@ts-ignore
  return feature.activeWhen(context);
};

const isActiveByParameters = (filterable: FilterableByParameter, filter: ModuleFilter): boolean => {
  if (!filterable.activationParameters) {
    return true;
  }

  if (Array.isArray(filterable.activationParameters)) {
    return filterable.activationParameters.reduce((active, activationParameter) => {
      return active && _doIsActiveByParameter(activationParameter, filter);
    }, true as boolean);
  }

  return _doIsActiveByParameter(filterable.activationParameters as any, filter);
};

const _doIsActiveByParameter = (activationParameter: ActivationParameter, filter: ModuleFilter): boolean => {
  if (typeof activationParameter === 'object') {
    if (!activationParameter.groupement || !activationParameter.pharmacie) {
      return false;
    }

    return isParameterValueEnabled(
      filter.parameters,
      activationParameter.groupement as any,
      activationParameter.pharmacie as any,
      filter.device === 'MOBILE'
    );
  }

  return parseParameterValueAsBoolean(filter.parameters, activationParameter);
};

interface FilterableByAuthorization {
  authorize?: Authorization;
}

const isActiveByAuthorization = (filterable: FilterableByAuthorization, filter: ModuleFilter): boolean => {
  if (filterable.authorize?.rolesDenied) {
    const nonAuthorizedRoles =
      typeof filterable.authorize.roles === 'string'
        ? [filterable.authorize.rolesDenied]
        : filterable.authorize.rolesDenied;

    if (nonAuthorizedRoles.includes(filter.user.role?.code as any)) {
      return false;
    }
  }

  if (filterable.authorize?.roles) {
    const authorizedRoles =
      typeof filterable.authorize.roles === 'string' ? [filterable.authorize.roles] : filterable.authorize.roles;
    if (!authorizedRoles.includes(filter.user.role?.code as any)) {
      return false;
    }
  }

  if (filterable.authorize?.permissions) {
    const requiredPermissions =
      typeof filterable.authorize.permissions === 'string'
        ? [filterable.authorize.permissions]
        : filterable.authorize.permissions;

    for (const permission of requiredPermissions) {
      if (!filter.user.codeTraitements?.includes(permission)) {
        return false;
      }
    }
  }

  return true;
};

const sortFeaturesByPreferredOrder = (features: Feature[]): Feature[] => {
  return features.sort((a, b) => (b.preferredOrder || 0) - (a.preferredOrder || 0));
};
