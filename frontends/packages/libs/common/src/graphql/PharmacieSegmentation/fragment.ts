import gql from 'graphql-tag';
import { TYPOLOGIE_INFO_FRAGMENT } from '../Typologie/fragment';
import { TRANCHE_CA_INFO_FRAGMENT } from '../TrancheCA/fragment';
import { QUALITE_INFO_FRAGMENT } from '../Qualite/fragment';
import { CONTRAT_INFO_FRAGMENT } from '../Contrat/fragment';

export const PHARMACIE_SEGMENTATION_INFO_FRAGMENT = gql`
  fragment PharmacieSegmentationInfo on PharmacieSegmentation {
    id
    dateSignature
    typologie {
      ...TypologieInfo
    }
    trancheCA {
      ...TrancheCAInfo
    }
    qualite {
      ...QualiteInfo
    }
    contrat {
      ...ContratInfo
    }
  }
  ${TYPOLOGIE_INFO_FRAGMENT}
  ${TRANCHE_CA_INFO_FRAGMENT}
  ${QUALITE_INFO_FRAGMENT}
  ${CONTRAT_INFO_FRAGMENT}
`;
