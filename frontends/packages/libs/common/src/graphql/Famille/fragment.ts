import gql from 'graphql-tag';

export const FAMILLE_INFO = gql`
  fragment FamilleInfo on Famille {
    id
    codeFamille
    libelleFamille
    countCanalArticles
    parent {
      id
      codeFamille
      countCanalArticles
      libelleFamille
    }
  }
`;
