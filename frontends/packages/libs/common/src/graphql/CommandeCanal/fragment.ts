import gql from 'graphql-tag';

export const COMMANDE_CANAL_INFO_FRAGMENT = gql`
  fragment CommandeCanalInfo on CommandeCanal {
    type
    id
    libelle
    code
    countOperationsCommercials
    countCanalArticles
  }
`;

export const SEARCH_COMMANDE_CANAL_INFO = gql`
  fragment SearchCommandeCanalInfo on CommandeCanal {
    type
    id
    libelle
    code
    countOperationsCommercials
    countCanalArticles
  }
`;
