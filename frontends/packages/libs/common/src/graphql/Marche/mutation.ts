import gql from 'graphql-tag';
import { MARCHE_INFO_FRAGMENT, MARCHE_INFO_FRAGMENT_FOR_EDIT } from './fragment';

export const DO_CREATE_UPDATE_MARCHE = gql`
  mutation CREATE_UPDATE_MARCHE(
    $id: ID
    $nom: String!
    $dateDebut: String!
    $dateFin: String!
    $marcheType: MarcheType!
    $avecPalier: Boolean!
    $status: MarcheStatus!
    $codeCanal: String!
    $laboratoires: [MarcheLaboratoireInput]
    $idsCanalArticle: [ID]
    $groupeClients: [GroupeClienRemisetInput!]!
  ) {
    createUpdateMarche(
      id: $id
      nom: $nom
      dateDebut: $dateDebut
      dateFin: $dateFin
      marcheType: $marcheType
      avecPalier: $avecPalier
      status: $status
      codeCanal: $codeCanal
      laboratoires: $laboratoires
      idsCanalArticle: $idsCanalArticle
      groupeClients: $groupeClients
    ) {
      ...MarcheInfoForEdit
    }
  }
  ${MARCHE_INFO_FRAGMENT_FOR_EDIT}
`;

export const DO_DELETE_MARCHE = gql`
  mutation DELETE_MARCHE($id: ID!) {
    softDeleteMarche(id: $id) {
      ...MarcheInfo
    }
  }
  ${MARCHE_INFO_FRAGMENT}
`;
