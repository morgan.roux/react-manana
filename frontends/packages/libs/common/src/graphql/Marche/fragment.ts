import gql from 'graphql-tag';
import { GROUPE_CLIENT_INFO_FRAGMENT } from '../GroupeClient/fragment';

export const MARCHE_LABORATOIRE_INFO_FRAGMENT = gql`
  fragment MarcheLaboratoireInfo on MarcheLaboratoire {
    id
    numOrdre
    obligatoire
    laboratoire {
      id
      nomLabo
    }
  }
`;

export const MARCHE_GROUPE_CLIENT_INFO_FRAGMENT = gql`
  fragment MarcheGroupeClientInfo on MarcheGroupeClient {
    id
    dateEngagement
    commentaire
    codeMaj
    dateCreation
    dateModification
    groupeClient {
      ...GroupeClientInfo
    }
  }
  ${GROUPE_CLIENT_INFO_FRAGMENT}
`;

export const MARCHE_INFO_FRAGMENT = gql`
  fragment MarcheInfo on Marche {
    type
    id
    nom
    dateDebut
    dateFin
    marcheType
    avecPalier
    status
    nbAdhesion
    nbPalier
    dateCreation
    dateModification
    commandeCanal {
      id
      code
      libelle
    }
    userCreated {
      id
      userName
    }
  }
`;

export const MARCHE_INFO_FRAGMENT_FOR_EDIT = gql`
  fragment MarcheInfoForEdit on Marche {
    type
    id
    nom
    dateDebut
    dateFin
    marcheType
    avecPalier
    status
    nbAdhesion
    nbPalier
    dateCreation
    dateModification
    commandeCanal {
      id
      code
      libelle
    }
    canalArticles {
      id
      produit {
        id
        libelle
        libelle2
        produitCode {
          id
          code
          typeCode
          referent
        }
      }
    }
    laboratoires {
      ...MarcheLaboratoireInfo
    }
    groupeClients {
      id
      groupeClient {
        id
        codeGroupe
      }
      remise {
        id
        remiseDetails {
          id
          quantiteMin
          quantiteMax
          pourcentageRemise
          remiseSupplementaire
        }
      }
    }
  }
  ${MARCHE_LABORATOIRE_INFO_FRAGMENT}
`;
