import gql from 'graphql-tag';
import { PUBLICITE_INFO_FRAGMENT } from './fragement';

export const DO_CREATE_UPDATE_PUBLICITE = gql`
  mutation CREATE_UPDATE_PUBLICITE(
    $id: ID
    $libelle: String
    $description: String
    $typeEspace: TypeEspace!
    $origine: OriginePublicite!
    $url: String
    $ordre: Int
    $codeItem: String
    $idItemSource: String
    $image: FichierInput
    $dateDebut: String
    $dateFin: String
  ) {
    createUpdatePublicite(
      id: $id
      libelle: $libelle
      description: $description
      typeEspace: $typeEspace
      origine: $origine
      url: $url
      ordre: $ordre
      codeItem: $codeItem
      idItemSource: $idItemSource
      image: $image
      dateDebut: $dateDebut
      dateFin: $dateFin
    ) {
      ...PubliciteInfo
    }
  }
  ${PUBLICITE_INFO_FRAGMENT}
`;

export const DO_DELETE_PUBLICITE = gql`
  mutation DELETE_PUBLICITE($id: ID!) {
    deletePublicite(id: $id) {
      ...PubliciteInfo
    }
  }
  ${PUBLICITE_INFO_FRAGMENT}
`;

export const DO_CREATE_SUIVI_PUBLICITAIRE = gql`
  mutation CREATE_SUIVI_PUBLICITAIRE($idPublicite: ID!) {
    createSuiviPublicitaire(idPublicite: $idPublicite) {
      id
      dateClick
      user {
        id
        userName
      }
      publicite {
        ...PubliciteInfo
      }
    }
  }
  ${PUBLICITE_INFO_FRAGMENT}
`;

export const DO_UPDATE_ORDRE_PUBLICITE = gql`
  mutation UPDATE_ORDRE_PUBLICITE($id: ID!, $ordre: Int) {
    updateOrdrePublicite(id: $id, ordre: $ordre) {
      ...PubliciteInfo
    }
  }
  ${PUBLICITE_INFO_FRAGMENT}
`;
