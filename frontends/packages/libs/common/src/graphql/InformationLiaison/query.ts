import gql from 'graphql-tag';

export const DO_SEARCH_INFORMATION_LIAISON = gql`
  query SEARCH_INFORMATION_LIAISON(
    $type: [String]
    $query: JSON
    $filterBy: JSON
    $sortBy: JSON
    $skip: Int
    $take: Int
  ) {
    search(
      type: $type
      query: $query
      filterBy: $filterBy
      sortBy: $sortBy
      skip: $skip
      take: $take
    ) {
      total
      data {
        ... on InformationLiaison {
          id
          idOrigine
          idOrigineAssocie
          origineAssocie{
            ...on Laboratoire{
              id
              type  
              nomLabo
            }
            ...on Service{
              type
              id
              code
              nom
            }
            ...on Partenaire{
              type
              id
              nom
            }

            ...on GroupeAmis{
              type
              id
              nom
            }
            ... on User{
              type
              id
              userName
            }
          }
          idType
          idItem
          idItemAssocie
          titre
          description
          bloquant
          statut
          idTache
          idFonction
          nbComment
          nbCollegue
          nbLue
          coupleStatutDeclarant
          colleguesConcernees {
            id
            statut
            informationLiaison {
              id
            }
            userConcernee {
              id
              userName
            }
          }
        }
      }
    }
  }
`;

export const DO_COUNT_INFORMATION_LIAISONS = gql`
  query COUNT_INFORMATION_LIAISONS($input: InformationLiaisonFilter!) {
    countInformationLiaisons(input: $input) {
      statuts {
        statut
        count
      }
      urgences {
        id
        count
      }
      importances {
        id
        count
      }
    }
  }
`;
