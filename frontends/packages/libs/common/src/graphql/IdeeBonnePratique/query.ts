import gql from 'graphql-tag';
import { IDEE_BONNE_PRATIQUE_FRAGMENT } from './fragment';

export const DO_SEARCH_IDEE_BONNE_PRATIQUE = gql`
  query SEARCH_IDEE_BONNE_PRATIQUE(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on IdeeOuBonnePratique {
          ...IdeeOuBonnePratiqueInfo
        }
      }
    }
  }
  ${IDEE_BONNE_PRATIQUE_FRAGMENT}
`;

export const GET_IDEE_BONNE_PRATIQUE = gql`
  query IDEE_BONNE_PRATIQUE($id: ID!) {
    ideeOuBonnePratique(id: $id) {
      ...IdeeOuBonnePratiqueInfo
    }
  }
  ${IDEE_BONNE_PRATIQUE_FRAGMENT}
`;

export const GET_IDEE_CLASSIFICATIONS = gql`
  query GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS($isRemoved: Boolean) {
    ideeOuBonnePratiqueClassifications(isRemoved: $isRemoved) {
      id
      nom
      parent {
        id
        nom
      }
      childs {
        id
        nom
      }
    }
  }
`;
