import gql from 'graphql-tag';
import { PERSONNEL_AFFECTATION_INFO_FRAGMENT } from './fragment';

export const GET_SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION = gql`
  query SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on PersonnelAffectation {
          ...PersonnelAffectationInfo
        }
      }
    }
  }
  ${PERSONNEL_AFFECTATION_INFO_FRAGMENT}
`;

export const GET_SEARCH_AFFECTATION_PERSONNEL_LIST = gql`
  query SEARCH_AFFECTATION_PERSONNEL_LIST($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Personnel {
          id
          fullName
        }
      }
    }
  }
`;
