import gql from 'graphql-tag';
import { PERSONNEL_AFFECTATION_INFO_FRAGMENT } from './fragment';

export const DO_AFFECT_PERSONNEL = gql`
  mutation AFFECT_PERSONNEL($inputs: AffectationInput) {
    createUpdatePersonnelAffectation(inputs: $inputs) {
      ...PersonnelAffectationInfo
    }
  }
  ${PERSONNEL_AFFECTATION_INFO_FRAGMENT}
`;
