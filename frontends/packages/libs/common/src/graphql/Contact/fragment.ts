import gql from 'graphql-tag';

export const CONTACT_INFO_FRAGMENT = gql`
  fragment ContactInfo on Contact {
    id
    cp
    ville
    pays
    adresse1
    adresse2
    faxProf
    faxPerso
    telProf
    telMobProf
    telPerso
    telMobPerso
    mailProf
    mailPerso
    siteProf
    sitePerso
    whatsAppMobProf
    whatsappMobPerso
    compteSkypeProf
    compteSkypePerso
    urlLinkedinProf
    urlLinkedinPerso
    urlTwitterProf
    urlTwitterPerso
    urlFacebookProf
    urlFacebookPerso
    codeMaj
    dateCreation
    dateModification
  }
`;

export const MIN_CONTACT_INFO_FRAGMENT = gql`
  fragment MinContactInfo on Contact {
    id
    cp
    ville
    pays
    adresse1
    adresse2
    faxProf
    faxPerso
    telProf
    telMobProf
    telPerso
    telMobPerso
    mailProf
    mailPerso
    siteProf
    sitePerso
  }
`;
