import gql from 'graphql-tag';
import { PRESIGNED_URL_INFO_FRAGEMENT } from './fragment';

export const DO_CREATE_PUT_PESIGNED_URL = gql`
  mutation CREATE_PUT_PESIGNED_URL($filePaths: [String]) {
    createPutPresignedUrls(filePaths: $filePaths) {
      ...PresignedUrlInfo
    }
  }
  ${PRESIGNED_URL_INFO_FRAGEMENT}
`;

export const DO_CREATE_GET_PRESIGNED_URL = gql`
  mutation CREATE_GET_PRESIGNED_URL($filePaths: [String]) {
    createGetPresignedUrls(filePaths: $filePaths) {
      ...PresignedUrlInfo
    }
  }
  ${PRESIGNED_URL_INFO_FRAGEMENT}
`;
