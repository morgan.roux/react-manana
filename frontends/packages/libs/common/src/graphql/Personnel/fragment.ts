import gql from 'graphql-tag';

export const PERSONNEL_INFO_FRAGMENT = gql`
  fragment PersonnelInfo on Personnel {
    type
    id
    service {
      id
      nom
    }
    respHierarch {
      id
    }
    sortie
    civilite
    nom
    prenom
    sexe
    fullName
    commentaire
    dateSortie
    idGroupement
    role {
      id
      code
      nom
    }
    _user: user {
      id
      status
      dateCreation
      dateModification
    }
    contact {
      id
      cp
      ville
      pays
      adresse1
      adresse2
      faxProf
      faxPerso
      telProf
      telMobProf
      telPerso
      telMobPerso
      mailProf
      mailPerso
      siteProf
      sitePerso
    }
  }
`;

export const SEARCH_PERSONNEL_FRAGMENT = gql`
  fragment SearchPersonnelInfo on Personnel {
    type
    id
    sortie
    civilite
    nom
    prenom
    fullName
    commentaire
    dateSortie
    idGroupement
    _user: user {
      id
      email
      login
      status
      userPhoto {
        id
        fichier {
          id
          nomOriginal
          publicUrl
          urlPresigned
        }
      }
    }
    role {
      id
      code
      nom
    }
    service {
      id
      nom
    }
  }
`;
