import gql from 'graphql-tag';
import { MIN_CONTACT_INFO_FRAGMENT } from '../Contact/fragment';

export const GET_PERSONNEL = gql`
  query PERSONNEL($id: ID!) {
    personnel(id: $id) {
      type
      id
      sortie
      civilite
      nom
      prenom
      sexe
      commentaire
      dateSortie
      idGroupement
      user {
        id
        email
        login
        status
        jourNaissance
        moisNaissance
        anneeNaissance
        codeTraitements
        userPhoto {
          id
          fichier {
            id
            chemin
            nomOriginal
            publicUrl
          }
        }
      }
      role {
        code
        nom
      }
      service {
        id
        code
      }
      contact {
        ...MinContactInfo
      }
    }
  }
  ${MIN_CONTACT_INFO_FRAGMENT}
`;

export const GET_SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT = gql`
  query SEARCH_CUSTOM_CONTENT_PERSONNEL_GROUPEMENT(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Personnel {
          id
          sortie
          civilite
          nom
          prenom
          fullName
          commentaire
          dateSortie
          idGroupement
          dateCreation
          dateModification
          user {
            id
            email
            login
            status
            userPhoto {
              id
              fichier {
                id
                nomOriginal
                publicUrl
                urlPresigned
              }
            }
          }
          contact {
            ...MinContactInfo
          }
          role {
            id
            code
            nom
          }
          service {
            id
            code
            nom
          }
        }
      }
    }
  }
  ${MIN_CONTACT_INFO_FRAGMENT}
`;
