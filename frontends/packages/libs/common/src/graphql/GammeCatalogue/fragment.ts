import gql from 'graphql-tag';

export const GAMME_CATALOGUE_INFO = gql`
  fragment GammeCatalogueInfo on GammeCatalogue {
    id
    codeGamme
    libelle
    numOrdre
    estDefaut
  }
`;

export const SEARCH_GAMME_CATALOGUE_INFO = gql`
  fragment SearchGammeCatalogueInfo on GammeCatalogue {
    id
    codeGamme
    libelle
    numOrdre
    estDefaut
    sousGammes {
      id
      codeSousGamme
      libelle
      numOrdre
      estDefaut
    }
  }
`;
