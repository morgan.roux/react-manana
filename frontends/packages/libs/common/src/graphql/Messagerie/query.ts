import gql from 'graphql-tag';
import { MESSAGERIE_INFO_FRAGMENT, MESSAGERIE_RESULT_INFO_FRAGMENT } from './fragment';

export const GET_MESSAGERIE = gql`
  query MESSAGERIE($id: ID!) {
    messagerie(id: $id) {
      ...MessagerieInfo
    }
  }
  ${MESSAGERIE_INFO_FRAGMENT}
`;

export const GET_MESSAGERIES = gql`
  query MESSAGERIES(
    $take: Int
    $skip: Int
    $typeMessagerie: MessagerieType
    $category: MessagerieCategory
    $year: Int
    $isRemoved: Boolean
    $lu: Boolean
    $orderBy: String
    $typeFilter: String
  ) {
    messageries(
      take: $take
      skip: $skip
      typeMessagerie: $typeMessagerie
      category: $category
      year: $year
      isRemoved: $isRemoved
      lu: $lu
      orderBy: $orderBy
      typeFilter: $typeFilter
    ) {
      ...MessagerieResultInfo
    }
  }
  ${MESSAGERIE_RESULT_INFO_FRAGMENT}
`;

export const GET_TOTAL_MESSAGERIE_NON_LUS = gql`
  query TOTAL_MESSAGERIE_NON_LUS {
    messagerieNonLus {
      total
    }
  }
`;

export const GET_TOTAL_MESSAGERIES = gql`
  query TOTAL_MESSAGERIES($typeMessagerie: MessagerieType, $isRemoved: Boolean, $lu: Boolean) {
    messageries(typeMessagerie: $typeMessagerie, isRemoved: $isRemoved, lu: $lu) {
      total
    }
  }
`;

export const GET_FILTER_COUNT_MESSAGERIES = gql`
  query FILTER_COUNT_MESSAGERIES($typeMessagerie: MessagerieType) {
    filterMessageriesCount(typeMessagerie: $typeMessagerie) {
      libelle
      code
      count
    }
  }
`;
