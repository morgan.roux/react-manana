import gql from 'graphql-tag';
import { PANIER_FRAGMENT, ARTICLE_SAME_PANACHEES_FRAGMENT } from './fragment';

export const GET_MY_PANIER = gql`
  query myPanier(
    $idPharmacie: ID!
    $codeCanal: String
    $skip: Int
    $take: Int
    $idRemiseOperation: ID
  ) {
    myPanier(idPharmacie: $idPharmacie, codeCanal: $codeCanal) {
      ...Panier
    }
  }
  ${PANIER_FRAGMENT}
`;

export const GET_MY_PANIERS = gql`
  query myPaniers($idPharmacie: ID!, $skip: Int, $take: Int, $idRemiseOperation: ID) {
    myPaniers(idPharmacie: $idPharmacie) {
      ...Panier
    }
  }
  ${PANIER_FRAGMENT}
`;

export const GET_PANIER_PRICE_BY_CANAL = gql`
  query PANIER_PRICE_BY_CANAL($idPanier: ID!, $codeCanalCommande: String!) {
    panierPriceByCanal(idPanier: $idPanier, codeCanalCommande: $codeCanalCommande) {
      nbrRef
      prixBaseTotalHT
      valeurRemiseTotal
      prixNetTotalHT
      remiseGlobale
      quantite
      uniteGratuite
    }
  }
`;

export const GET_PHARMACIE_PANIER = gql`
  query getPharmaciePanier($idPharmacie: ID!, $skip: Int, $take: Int, $idRemiseOperation: ID) {
    pharmaciePanier(idPharmacie: $idPharmacie) {
      ...Panier
    }
  }
  ${PANIER_FRAGMENT}
`;

export const GET_ARTICLE_SAME_PANACHEES = gql`
  query articleSamePanachees($id: ID!, $idPharmacie: ID, $idRemiseOperation: ID) {
    produitCanal(id: $id) {
      id
      ...Article_Same_Panachees
    }
  }
  ${ARTICLE_SAME_PANACHEES_FRAGMENT}
`;
