import gql from 'graphql-tag';
import { PANIER_FRAGMENT } from './fragment';
import { COMMANDE_FRAGMENT } from './fragment';

export const ADD_TO_PANIER = gql`
  mutation addToPanier(
    $idCanalArticle: ID!
    $idPharmacie: ID!
    $codeCanal: String!
    $optimiser: Int
    $quantite: Int
    $skip: Int
    $take: Int
    $idRemiseOperation: ID
  ) {
    addToPanier(
      idCanalArticle: $idCanalArticle
      idPharmacie: $idPharmacie
      codeCanal: $codeCanal
      optimiser: $optimiser
      quantite: $quantite
    ) {
      ...Panier
    }
  }
  ${PANIER_FRAGMENT}
`;

export const DO_DELETE_PANIER_LIGNE = gql`
  mutation DELETE_PANIER_LIGNE($id: ID!, $idPharmacie: ID, $skip: Int, $take: Int, $idRemiseOperation: ID) {
    deletePanierLigne(id: $id) {
      ...Panier
    }
  }
  ${PANIER_FRAGMENT}
`;

export const DO_UPDATE_QUANTITE = gql`
  mutation UPDATE_QUANTITE($id: ID!, $quantite: Int, $idPharmacie: ID, $skip: Int, $take: Int, $idRemiseOperation: ID) {
    updateQtePanierLigne(id: $id, quantite: $quantite) {
      ...Panier
    }
  }
  ${PANIER_FRAGMENT}
`;

export const DO_VALIDATE_PANIER = gql`
  mutation VALIDATE_PANIER(
    $idPanier: ID!
    $idPharmacie: ID!
    $commentaireInterne: String
    $commentaireExterne: String
    $codeCanalCommande: String!
  ) {
    validatePanierAndCreateCommande(
      idPanier: $idPanier
      idPharmacie: $idPharmacie
      commentaireInterne: $commentaireInterne
      commentaireExterne: $commentaireExterne
      codeCanalCommande: $codeCanalCommande
    ) {
      ...PanierCommande
    }
  }
  ${COMMANDE_FRAGMENT}
`;

export const DO_TAKE_PRODUCT_TO_MY_PANIER = gql`
  mutation takeProductToMyPanier(
    $id: ID!
    $idPharmacie: ID!
    $codeCanal: String!
    $skip: Int
    $take: Int
    $idRemiseOperation: ID
  ) {
    takeProductToMyPanier(id: $id, codeCanal: $codeCanal, idPharmacie: $idPharmacie) {
      ...Panier
    }
  }
  ${PANIER_FRAGMENT}
`;

export const DO_CLEAR_MY_PANIER = gql`
  mutation clearMyPanier($idPharmacie: ID!, $codeCanal: String!, $skip: Int, $take: Int, $idRemiseOperation: ID) {
    clearMyPanier(idPharmacie: $idPharmacie, codeCanal: $codeCanal) {
      ...Panier
    }
  }
  ${PANIER_FRAGMENT}
`;
