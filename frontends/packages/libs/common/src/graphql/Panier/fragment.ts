import gql from 'graphql-tag';
import { CANAL_SOUS_GAMME_PRODUIT_CANAL_FRAGMENT } from '../CanalSousGamme/fragment';
import { REMISES_PRODUIT_CANAL, PRODUIT_CANAL_FRAGMENT } from '../ProduitCanal/fragment';
import { COMMANDE_CANAL_INFO_FRAGMENT } from '../CommandeCanal/fragment';

export const PANIER_PHARMACIE_FRAGMENT = gql`
  fragment PanierPharmacie_Panier on Panier {
    pharmacie {
      type
      id
      nom
      cip
      adresse1
    }
  }
`;
export const OWNER_FRAGMENT = gql`
  fragment Owner_Panier on Panier {
    owner {
      id
      userName
    }
  }
`;

export const ARTICLE_SAME_PANACHEES_FRAGMENT = gql`
  fragment Article_Same_Panachees on ProduitCanal {
    articleSamePanachees(idPharmacie: $idPharmacie) {
      id
      produit {
        id
        libelle
        libelle2
        produitCode {
          id
          code
          typeCode
          referent
        }
        produitTechReg {
          id
          laboExploitant {
            id
            nomLabo
            sortie
          }
        }
      }
      prixPhv
      qteStock
      ...RemisesProduitCanal
    }
  }
  ${REMISES_PRODUIT_CANAL}
`;

export const PANIERLIGNE_ARTICLE_FRAGMENT = gql`
  fragment PanierLigne_Article on PanierLigne {
    produitCanal {
      ...MinimalProduitCanalInfo
      ...RemisesProduitCanal
      ...CanalSousGammeProduitCanal
      ...Article_Same_Panachees
      commandeCanal {
        ...CommandeCanalInfo
      }
    }
    validate
  }
  ${COMMANDE_CANAL_INFO_FRAGMENT}
  ${ARTICLE_SAME_PANACHEES_FRAGMENT}
  ${CANAL_SOUS_GAMME_PRODUIT_CANAL_FRAGMENT}
  ${REMISES_PRODUIT_CANAL}
  ${PRODUIT_CANAL_FRAGMENT}
`;

export const PANIERLIGNE_FRAGMENT = gql`
  fragment PanierLigne on PanierLigne {
    id
    optimiser
    quantite
    uniteGratuite
    prixBaseHT
    remiseLigne
    remiseGamme
    prixNetUnitaireHT
    prixTotalHT
    dateCreation
    dateModification
    ...PanierLigne_Article
  }
  ${PANIERLIGNE_ARTICLE_FRAGMENT}
`;

export const PANIER_PANIERLIGNES_FRAGMENT = gql`
  fragment Panier_PanierLignes on Panier {
    panierLignes {
      ...PanierLigne
      ...PanierLigne_Article
    }
  }
  ${PANIERLIGNE_FRAGMENT}
  ${PANIERLIGNE_ARTICLE_FRAGMENT}
`;

export const PANIER_FRAGMENT = gql`
  fragment Panier on Panier {
    id
    nbrRef
    quantite
    uniteGratuite
    prixBaseTotalHT
    valeurRemiseTotal
    prixNetTotalHT
    remiseGlobale
    fraisPort
    valeurFrancoPort
    dateCreation
    dateModification
    ...Panier_PanierLignes
    ...PanierPharmacie_Panier
    ...Owner_Panier
  }
  ${PANIER_PANIERLIGNES_FRAGMENT}
  ${PANIER_PHARMACIE_FRAGMENT}
  ${OWNER_FRAGMENT}
`;

export const COMMANDE_FRAGMENT = gql`
  fragment PanierCommande on Commande {
    id
  }
`;
