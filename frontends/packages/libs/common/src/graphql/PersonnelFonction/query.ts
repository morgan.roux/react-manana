import gql from 'graphql-tag';
import { PERSONNEL_FONCTION_INFO_FRAGMENT } from './fragment';

export const GET_PERSONNEL_FONTIONS = gql`
  query PERSONNEL_FONCTIONS($take: Int, $skip: Int) {
    personnelFonctions(take: $take, skip: $skip) {
      total
      data {
        ...PersonnelFonctionInfo
      }
    }
  }
  ${PERSONNEL_FONCTION_INFO_FRAGMENT}
`;
