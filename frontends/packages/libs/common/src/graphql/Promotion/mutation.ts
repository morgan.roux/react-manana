import gql from 'graphql-tag';
import { PROMOTION_INFO_FRAGMENT, PROMOTION_INFO_FRAGMENT_FOR_EDIT } from './fragment';

export const DO_CREATE_UPDATE_PROMOTION = gql`
  mutation CREATE_UPDATE_PROMOTION(
    $id: ID
    $nom: String!
    $dateDebut: String!
    $dateFin: String!
    $promotionType: PromotionType!
    $status: PromotioStatus!
    $codeCanal: String!
    $idsCanalArticle: [ID]
    $groupeClients: [GroupeClienRemisetInput!]!
  ) {
    createUpdatePromotion(
      id: $id
      nom: $nom
      dateDebut: $dateDebut
      dateFin: $dateFin
      promotionType: $promotionType
      status: $status
      codeCanal: $codeCanal
      idsCanalArticle: $idsCanalArticle
      groupeClients: $groupeClients
    ) {
      ...PromotionInfoForEdit
    }
  }
  ${PROMOTION_INFO_FRAGMENT_FOR_EDIT}
`;

export const DO_DELETE_PROMOTION = gql`
  mutation DELETE_PROMOTION($id: ID!) {
    softDeletePromotion(id: $id) {
      ...PromotionInfo
    }
  }
  ${PROMOTION_INFO_FRAGMENT}
`;
