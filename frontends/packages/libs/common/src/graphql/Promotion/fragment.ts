import gql from 'graphql-tag';

export const PROMOTION_INFO_FRAGMENT = gql`
  fragment PromotionInfo on Promotion {
    type
    id
    nom
    dateDebut
    dateFin
    promotionType
    promoStatus: status
    dateCreation
    dateModification
    commandeCanal {
      id
      code
      libelle
    }
    userCreated {
      id
      userName
    }
  }
`;

export const PROMOTION_INFO_FRAGMENT_FOR_EDIT = gql`
  fragment PromotionInfoForEdit on Promotion {
    type
    id
    nom
    dateDebut
    dateFin
    promotionType
    status
    dateCreation
    dateModification
    commandeCanal {
      id
      code
      libelle
    }
    userCreated {
      id
      userName
    }
    canalArticles {
      id
      produit {
        id
        libelle
        libelle2
        produitCode {
          id
          code
          typeCode
          referent
        }
      }
    }
    groupeClients {
      id
      groupeClient {
        id
        codeGroupe
      }
      remise {
        id
        remiseDetails {
          id
          quantiteMin
          quantiteMax
          pourcentageRemise
          remiseSupplementaire
        }
      }
    }
  }
`;
