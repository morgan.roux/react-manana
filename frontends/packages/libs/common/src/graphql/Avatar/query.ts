import gql from 'graphql-tag';
import { AVATAR_INFO_FRAGEMENT, AVATAR_MIN_INFO_FRAGEMENT } from './fragement';

export const GET_AVATAR = gql`
  query AVATAR($id: ID!) {
    avatar(id: $id) {
      ...AvatarInfo
    }
  }
  ${AVATAR_INFO_FRAGEMENT}
`;

export const DO_SEARCH_AVATARS = gql`
  query SEARCH_AVATARS($type: [String], $query: JSON, $skip: Int, $take: Int, $filterBy: JSON, $sortBy: JSON) {
    search(type: $type, query: $query, skip: $skip, take: $take, filterBy: $filterBy, sortBy: $sortBy) {
      total
      data {
        ... on Avatar {
          ...AvatarMinInfo
        }
      }
    }
  }
  ${AVATAR_MIN_INFO_FRAGEMENT}
`;
