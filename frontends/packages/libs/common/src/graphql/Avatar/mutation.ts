import gql from 'graphql-tag';
import { AVATAR_INFO_FRAGEMENT } from './fragement';

export const DO_CREATE_UPDATE_AVATAR = gql`
  mutation CREATE_UPDATE_AVATAR($id: ID, $description: String, $codeSexe: Sexe, $fichier: FichierInput) {
    createUpdateAvatar(id: $id, description: $description, codeSexe: $codeSexe, fichier: $fichier) {
      ...AvatarInfo
    }
  }
  ${AVATAR_INFO_FRAGEMENT}
`;

export const DO_DELETE_AVATARS = gql`
  mutation DELETE_AVATARS($ids: [ID!]!) {
    deleteAvatars(ids: $ids) {
      ...AvatarInfo
    }
  }
  ${AVATAR_INFO_FRAGEMENT}
`;
