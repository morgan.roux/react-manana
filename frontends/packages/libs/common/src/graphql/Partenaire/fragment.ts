import gql from 'graphql-tag';

export const SEARCH_PARTENAIRE_INFO = gql`
  fragment SearchPartenaireInfo on Partenaire {
    type
    id
    nom
    commentaire
    idGroupement
    idPharmacie
    prive
    _user: user {
      id
      email
      login
      status
      userName
    }
    role {
      id
      code
      nom
    }
  }
`;
