import gql from 'graphql-tag';

export const GET_PARTENAIRE = gql`
  query PARTENAIRE($id: ID!) {
    partenaire(id: $id) {
      id
      nom
      idPharmacie
      prive
      partenaireServiceSuite {
        id
        adresse1
        adresse2
      }
      user {
        id
        jourNaissance
        moisNaissance
        anneeNaissance
        email
        login
        status
        codeTraitements
        userPhoto {
          id
          fichier {
            id
            chemin
            nomOriginal
            publicUrl
          }
        }
      }
    }
  }
`;
