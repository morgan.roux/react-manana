import gql from 'graphql-tag';
import { URGENCE_FRAGMENT } from './fragment';

export const GET_URGENCES = gql`
  query URGENCES {
    urgences {
      ...UrgenceInfo
    }
  }
  ${URGENCE_FRAGMENT}
`;
