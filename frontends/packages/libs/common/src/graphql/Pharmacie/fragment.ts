import gql from 'graphql-tag';
import { GROSSISTE_INFO_FRAGMENT } from '../Grossiste/fragment';
import { GENERIQUEUR_INFO_FRAGMENT } from '../Generiqueur/fragment';
import { PHARMACIE_SATISFACTION_INFO_FRAGMENT } from '../PharmacieSatisfaction/fragment';
import { PHARMACIE_SEGMENTATION_INFO_FRAGMENT } from '../PharmacieSegmentation/fragment';
import { PHARMACIE_COMPTA_INFO_FRAGMENT } from '../PharmacieCompta/fragment';
import { PHARMACIE_INFORMATIQUE_INFO_FRAGMENT } from '../PharmacieInformatique/fragment';
import { PHARMACIE_ACHAT_INFO_FRAGMENT } from '../PharmacieAchat/fragment';
import { PHARMACIE_CAP_INFO_FRAGMENT } from '../PharmacieCap/fragment';
import { PHARMACIE_CONCEPT_INFO_FRAGMENT } from '../PharmacieConcept/fragment';
import { PHARMACIE_STAT_CA_INFO_FRAGMENT } from '../PharmacieStatCA/fragment';
import { PHARMACIE_DIGITALE_INFO_FRAGMENT } from '../PharmacieDigitale/fragment';
import { DEPARTEMENT_WITH_REGION_INFO_FRAGMENT } from '../Departement/fragment';
import { CONTACT_INFO_FRAGMENT } from '../Contact/fragment';

export const PHARMACIE_MINIM_INFO_FRAGMENT = gql`
  fragment PharmacieMinimInfo on Pharmacie {
    type
    id
    cip
    nom
    adresse1
    ville
    departement {
      id
      nom
      region {
        id
        nom
      }
    }
  }
`;

export const PHARMACIE_INFO_FRAGMENT = gql`
  fragment PharmacieInfo on Pharmacie {
    cip
    nom
    adresse1
    adresse2
    id
    type
    ville
    cp
    pays
    uga
    nbEmploye
    nbAssistant
    nbAutreTravailleur
    sortie
    numFiness
    nbJourOuvre
    latitude
    longitude
    commentaire
    departement {
      ...DepartementWithRegionInfo
    }
    titulaires {
      id
      civilite
      nom
      prenom
    }
    presidentRegion {
      id
      titulaire {
        id
        fullName
        nom
        prenom
      }
      departement {
        id
        nom
        region {
          id
          nom
        }
      }
    }
    contact {
      ...ContactInfo
    }
    entreeSortie {
      id
      dateEntree
      dateSortie
      commentaireEntree
      commentaireSortie
      dateSortieFuture
      commentaireSortieFuture
      pharmacieMotifEntree {
        id
        libelle
      }
      pharmacieMotifSortie {
        id
        libelle
      }
      pharmacieMotifSortieFuture {
        id
        libelle
      }
      contrat {
        id
        nom
        dateCreation
      }
      concurrent {
        id
        nom
      }
      concurrentAncien {
        id
        nom
      }
    }
    grossistes {
      ...GrossisteInfo
    }
    generiqueurs {
      ...GeneriqueurInfo
    }
    satisfaction {
      ...PharmacieSatisfactionInfo
    }
    segmentation {
      ...PharmacieSegmentationInfo
    }
    compta {
      ...PharmacieComptaInfo
    }
    informatique {
      ...PharmacieInformatiqueInfo
    }
    achat {
      ...PharmacieAchatInfo
    }
    cap {
      ...PharmacieCapInfo
    }
    concept {
      ...PharmacieConceptInfo
    }
    statCA {
      ...PharmacieStatCAInfo
    }
    digitales {
      ...PharmacieDigitaleInfo
    }
  }
  ${CONTACT_INFO_FRAGMENT}
  ${DEPARTEMENT_WITH_REGION_INFO_FRAGMENT}
  ${GROSSISTE_INFO_FRAGMENT}
  ${GENERIQUEUR_INFO_FRAGMENT}
  ${PHARMACIE_SATISFACTION_INFO_FRAGMENT}
  ${PHARMACIE_SEGMENTATION_INFO_FRAGMENT}
  ${PHARMACIE_COMPTA_INFO_FRAGMENT}
  ${PHARMACIE_INFORMATIQUE_INFO_FRAGMENT}
  ${PHARMACIE_ACHAT_INFO_FRAGMENT}
  ${PHARMACIE_CAP_INFO_FRAGMENT}
  ${PHARMACIE_CONCEPT_INFO_FRAGMENT}
  ${PHARMACIE_STAT_CA_INFO_FRAGMENT}
  ${PHARMACIE_DIGITALE_INFO_FRAGMENT}
`;

export const PHARMACIE_USER_FRAGMENT = gql`
  fragment Pharmacie_User on Pharmacie {
    ...PharmacieMinimInfo
  }
  ${PHARMACIE_MINIM_INFO_FRAGMENT}
`;

export const TITULAIRE_FRAGMENT = gql`
  fragment Titulaire on Titulaire {
    pharmacieUser {
      ...Pharmacie_User
    }
  }
  ${PHARMACIE_USER_FRAGMENT}
`;

export const USER_FRAGMENT = gql`
  fragment User on User {
    id
    pharmacie {
      id
      cip
      nom
      adresse1
      type
      ville
      departement {
        id
        nom
        region {
          id
          nom
        }
      }
    }
  }
`;

export const PHARMACIE_TABLE_INFO_FRAGMENT = gql`
  fragment PharmacieTableInfo on Pharmacie {
    id
    sortie
    cip
    numFiness
    nom
    departement {
      id
      nom
      region {
        id
        nom
      }
    }
    titulaires {
      id
      nom
      prenom
      fullName
      sortie
    }
    adresse1
    adresse2
    cp
    ville
    idGroupement
    actived
    users {
      id
      userName
      email
      login
      status
    }
    segmentation {
      id
      trancheCA {
        id
        libelle
      }
      contrat {
        id
        nom
      }
    }
    achat {
      id
      canal {
        id
        libelle
      }
    }
    nbReclamation
    nbAppel
  }
`;

export const SEARCH_PHARMACIE_INFO = gql`
  fragment SearchPharmacieInfo on Pharmacie {
    type
    id
    sortie
    cip
    numFiness
    nom
    departement {
      id
      nom
    }
    titulaires {
      id
      nom
      prenom
      fullName
    }
    adresse1
    adresse2
    cp
    ville
    idGroupement
    actived
    users {
      id
      userName
      email
      login
      status
    }
  }
`;
