import gql from 'graphql-tag';
import { GROUP_AMIS_FRAGMENT } from './fragment';

// export const GROUPE_AMISES = gql`
//   query GroupeAmises($isRemoved: Boolean) {
//     groupeAmises(isRemoved: $isRemoved) {
//       ...GroupAmisInfo
//     }
//   }
//   ${GROUP_AMIS_FRAGMENT}
// `;

export const GET_GROUPE_AMIS = gql`
  query GROUPE_AMIS($id: ID!) {
    groupeAmis(id: $id) {
      ...GroupAmisInfo
      pharmacieMembres {
        id
        nom
        users {
          id
          userName
        }
      }
    }
  }
  ${GROUP_AMIS_FRAGMENT}
`;

export const DO_SEARCH_GROUPES_AMIS = gql`
  query SEARCH_GROUPES_AMIS(
    $type: [String]
    $query: JSON
    $filterBy: JSON
    $sortBy: JSON
    $skip: Int
    $take: Int
  ) {
    search(
      type: $type
      query: $query
      filterBy: $filterBy
      sortBy: $sortBy
      skip: $skip
      take: $take
    ) {
      total
      data {
        ... on GroupeAmis {
          id
          nom
          description
          nbPharmacie
          nbMember
          nbPartage
          nbRecommandation
          dateCreation
        }
      }
    }
  }
`;

export const DO_SEARCH_GROUPES_AMIS_WITH_MINIM_INFO = gql`
  query SEARCH_GROUPES_AMIS_WITH_MINIM_INFO(
    $type: [String]
    $query: JSON
    $filterBy: JSON
    $sortBy: JSON
    $skip: Int
    $take: Int
  ) {
    search(
      type: $type
      query: $query
      filterBy: $filterBy
      sortBy: $sortBy
      skip: $skip
      take: $take
    ) {
      total
      data {
        ... on GroupeAmis {
          id
          nom
        }
      }
    }
  }
`;
