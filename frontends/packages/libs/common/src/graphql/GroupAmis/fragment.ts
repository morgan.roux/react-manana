import gql from 'graphql-tag';

export const GROUP_AMIS_FRAGMENT = gql`
  fragment GroupAmisInfo on GroupeAmis {
    id
    nom
    description
    typeGroupeAmis
    nbPharmacie
    groupStatus: status
    nbMember
    nbPartage
    nbRecommandation
    dateCreation
  }
`;
