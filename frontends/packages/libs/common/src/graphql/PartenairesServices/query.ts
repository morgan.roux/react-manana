import gql from 'graphql-tag';

export const GET_PARTENAIRE_SERVICE = gql`
  query PARTENAIRE_SERVICE($id: ID!) {
    partenaire(id: $id) {
      id
      type
      nom
      idSecteur
      cerclePartenariat
      idPharmacie
      prive
      partenaireServiceSuite {
        id
        adresse1
        telephone
        ville
        codePostal
      }
      services {
        id
        nom
        dateDemarrage
        nbCollaboQualifie
        commentaire
      }
      partenaireServicePartenaire {
        id
        dateDebutPartenaire
        dateFinPartenaire
        typePartenaire
        statutPartenaire
      }
    }
  }
`;
export const GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE = gql`
  query SEARCH_CUSTOM_CONTENT_PARTENAIRE(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take, filterBy: $filterBy, sortBy: $sortBy) {
      total
      data {
        ... on Partenaire {
          id
          nom
          idSecteur
          cerclePartenariat
          idPharmacie
          prive
          partenaireServiceSuite {
            id
            adresse1
            telephone
            codePostal
            ville
          }
          partenaireServicePartenaire {
            id
            dateDebutPartenaire
            dateFinPartenaire
            statutPartenaire
            typePartenaire
          }
          services {
            id
            nom
          }
          user {
            id
            userName
            jourNaissance
            moisNaissance
            anneeNaissance
            email
            login
            status
          }
        }
      }
    }
  }
`;
