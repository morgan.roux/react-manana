import gql from 'graphql-tag';
import { ACTION_INFO_FRAGEMENT } from '../Action/fragment';

const DELETE_PARTICIPANT_FROM_PROJECT = gql`
  mutation REMOVE_PARTICIPANT($input: RemoveParticipantInput!) {
    removeParticipantFromProject(input: $input) {
      id
      participants {
        id
        email
        login
        userName
        role {
          id
          nom
        }
        pharmacie {
          id
        }
      }
      userCreation {
        id
        userName
        email
        login
        role {
          id
          nom
        }
      }
    }
  }
`;

const ADD_PARTICIPANT_TO_PROJECT = gql`
  mutation ADD_PARTICIPANT($input: AddParticipantsInput!) {
    addParticipantsToProject(input: $input) {
      id
      participants {
        id
        email
        login
        userName
        role {
          id
          nom
        }
        pharmacie {
          id
        }
      }
      userCreation {
        id
        userName
        email
        login
        role {
          id
          nom
        }
      }
    }
  }
`;

const DO_ASSIGN_TASK_TO_USER = gql`
  mutation ASSIGN_TASK_TO_USER($input: AssignOrUnAssignActionInput!) {
    assignActionToUser(input: $input) {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;

const DO_UNASSIGN_TASK_TO_USER = gql`
  mutation UNASSIGN_TASK_TO_USER($input: AssignOrUnAssignActionInput!) {
    unAssignUserInAction(input: $input) {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;

const DO_ASSIGN_TASK_TO_USERS = gql`
  mutation ASSIGN_TASK_TO_USERS($idAction: ID!, $idUserDestinations: [ID!]!) {
    assignActionUsers(idAction: $idAction, idUserDestinations: $idUserDestinations) {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;

const DO_UNASSIGN_TASK_TO_USERS = gql`
  mutation UNASSIGN_TASK_TO_USERS($idAction: ID!, $idUserDestinations: [ID!]!) {
    unAssignActionUsers(idAction: $idAction, idUserDestinations: $idUserDestinations) {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;

export {
  DELETE_PARTICIPANT_FROM_PROJECT,
  ADD_PARTICIPANT_TO_PROJECT,
  DO_ASSIGN_TASK_TO_USER,
  DO_UNASSIGN_TASK_TO_USER,
  DO_ASSIGN_TASK_TO_USERS,
  DO_UNASSIGN_TASK_TO_USERS,
};
