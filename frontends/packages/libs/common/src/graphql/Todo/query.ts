import gql from 'graphql-tag';

const DO_SEARCH_PARTICIPANT = gql`
  query SEARCH_PARTCIPANT(
    $type: [String]
    $query: JSON
    $filterBy: JSON
    $sortBy: JSON
    $skip: Int
    $take: Int
  ) {
    search(
      type: $type
      query: $query
      filterBy: $filterBy
      sortBy: $sortBy
      skip: $skip
      take: $take
    ) {
      total
      data {
        ... on Project {
          id
          participants {
            id
            email
            login
            userName
            role {
              id
              nom
            }
            pharmacie {
              id
            }
          }
          userCreation {
            id
            userName
            email
            login
            role {
              id
              nom
            }
          }
        }
      }
    }
  }
`;
const DO_GET_PARTICIPANT_LIST = gql`
  query PARTICIPANT_LIST($id: ID!) {
    project(id: $id) {
      participants {
        id
        email
        login
        userName
        role {
          id
          nom
        }
      }
      userCreation {
        id
        userName
        email
        login
        role {
          id
          nom
        }
      }
    }
  }
`;
export { DO_SEARCH_PARTICIPANT, DO_GET_PARTICIPANT_LIST };
