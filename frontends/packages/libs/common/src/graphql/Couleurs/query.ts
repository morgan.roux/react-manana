import gql from 'graphql-tag';
import { COULEUR_INFO_FRAGMENT } from './fragment';

export const GET_COULEURS = gql`
  query couleurs {
    couleurs {
      ...CouleurInfo
    }
  }
  ${COULEUR_INFO_FRAGMENT}
`;
export const GET_COULEUR = gql`
  query couleur($id: ID!) {
    couleur(id: $id) {
      ...CouleurInfo
    }
  }
  ${COULEUR_INFO_FRAGMENT}
`;

export const DO_SEARCH_COULEURS = gql`
  query SEARCH_COULEURS($query: JSON, $skip: Int, $take: Int, $filterBy: JSON, $sortBy: JSON) {
    search(type: "couleur", query: $query, skip: $skip, take: $take, filterBy: $filterBy, sortBy: $sortBy) {
      total
      data {
        ... on Couleur {
          ...CouleurInfo
        }
      }
    }
  }
  ${COULEUR_INFO_FRAGMENT}
`;
