import gql from 'graphql-tag';

export const COULEUR_INFO_FRAGMENT = gql`
  fragment CouleurInfo on Couleur {
    id
    code
    libelle
  }
`;
