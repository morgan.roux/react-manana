import gql from 'graphql-tag';

export const SEARCH_PERSONNEL_PHARMACIE_FRAGMENT = gql`
  fragment SearchPersonnelPharmacieInfo on Ppersonnel {
    type
    id
    civilite
    nom
    prenom
    fullName
    estAmbassadrice
    commentaire
    sortie
    dateSortie
    idGroupement
    idPharmacie
    pharmacie {
      id
      cip
      nom
      titulaires {
        id
        nom
        prenom
        fullName
      }
    }
    _user: user {
      id
      email
      login
      status
      jourNaissance
      moisNaissance
      anneeNaissance
    }
    role {
      id
      code
      nom
    }
  }
`;
