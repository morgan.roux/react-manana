import gql from 'graphql-tag';
import { SEARCH_PERSONNEL_PHARMACIE_FRAGMENT } from './fragment';

export const DO_CREATE_UPDATE_PPERSONNEL = gql`
  mutation CREATE_UPDATE_PPERSONNEL($input: PpersonnelInput!) {
    createUpdatePpersonnel(input: $input) {
      type
      id
      civilite
      nom
      prenom
      estAmbassadrice
      commentaire
      sortie
      dateSortie
      idGroupement
      idPharmacie
      pharmacie {
        id
        cip
        nom
        titulaires {
          id
          nom
          prenom
        }
      }
      user {
        id
        email
        login
        status
      }
      role {
        id
        code
        nom
      }
    }
  }
`;

export const DO_DELETE_SOFT_PPERSONNELS = gql`
  mutation DELETE_SOFT_PPERSONNELS($ids: [ID!]!) {
    deleteSoftPpersonnels(ids: $ids) {
      ...SearchPersonnelPharmacieInfo
    }
  }
  ${SEARCH_PERSONNEL_PHARMACIE_FRAGMENT}
`;
