import gql from 'graphql-tag';
import { FICHIER_FRAGMENT } from '../Fichier/fragment';
export const ITEM_INFO_FRAGEMENT = gql`
  fragment ItemInfo on Item {
    type
    id
    code
    name
    codeItem
    fichier {
      ...FichierInfo
    }
    parent {
      id
      code
      name
      codeItem
      fichier {
        ...FichierInfo
      }
      dateCreation
      dateModification
    }
    dateCreation
    dateModification
  }
  ${FICHIER_FRAGMENT}
`;
