import gql from 'graphql-tag';
import { TODO_ETIQUETTE_INFO_FRAGMENT } from './fragment';

export const DO_CREATE_UPDATE_ETIQUETTE = gql`
  mutation CREATE_UPDATE_TODO_ETIQUETTE($input: TodoEtiquetteInput!, $actionStatus: String) {
    createUpdateTodoEtiquette(input: $input) {
      ...TodoEtiquetteInfo
    }
  }
  ${TODO_ETIQUETTE_INFO_FRAGMENT}
`;

export const DO_DELETE_ETIQUETTE = gql`
  mutation DELETE_ETIQUETTE($ids: [ID!]!, $actionStatus: String) {
    softDeleteTodoEtiquettes(ids: $ids) {
      ...TodoEtiquetteInfo
    }
  }
  ${TODO_ETIQUETTE_INFO_FRAGMENT}
`;

export const DO_ADD_PARTICIPANTS_TO_ETIQUETTE = gql`
  mutation ADD_PARTICIPANTS_TO_ETIQUETTE(
    $input: AddEtiquetteParticipantsInput!
    $actionStatus: String
  ) {
    addParticipantsToEtiquette(input: $input) {
      ...TodoEtiquetteInfo
    }
  }
  ${TODO_ETIQUETTE_INFO_FRAGMENT}
`;

export const DO_REMOVE_PARTICIPANT_FROM_ETIQUETTE = gql`
  mutation REMOVE_PARTICIPANT_FROM_ETIQUETTE(
    $input: RemoveEtiquetteParticipantInput!
    $actionStatus: String
  ) {
    removeParticipantFromEtiquette(input: $input) {
      ...TodoEtiquetteInfo
    }
  }
  ${TODO_ETIQUETTE_INFO_FRAGMENT}
`;
