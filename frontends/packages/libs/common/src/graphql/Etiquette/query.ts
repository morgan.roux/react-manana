import gql from 'graphql-tag';
import { TODO_ETIQUETTE_INFO_FRAGMENT } from './fragment';

export const GET_SEARCH_ETIQUETTE = gql`
  query SEARCH_ETIQUETTE($query: JSON, $type: [String], $sortBy: JSON, $actionStatus: String) {
    search(query: $query, type: $type, sortBy: $sortBy) {
      total
      data {
        ... on TodoEtiquette {
          ...TodoEtiquetteInfo
        }
      }
    }
  }
  ${TODO_ETIQUETTE_INFO_FRAGMENT}
`;

export const GET_ETIQUETTES = gql`
  query ETIQUETTES($isRemoved: Boolean) {
    todoEtiquettes(isRemoved: $isRemoved) {
      id
      nom
      nbAction
      activeActions
      isRemoved
      couleur {
        code
        id
        libelle
      }
    }
  }
`;

export const GET_ETIQUETTES_BY_USER_WITH_MIN_INFO = gql`
  query ETIQUETTES_BY_USER_WITH_MIN_INFO($idUser: ID!, $isRemoved: Boolean) {
    todoEtiquettesByUser(idUser: $idUser, isRemoved: $isRemoved) {
      id
      nom
      couleur {
        code
        id
      }
    }
  }
`;

export const GET_ETIQUETTE = gql`
  query ETIQUETTE($id: ID!) {
    todoEtiquette(id: $id) {
      id
      nom
      nbAction
      activeActions
      isRemoved
      couleur {
        code
        id
        libelle
      }
    }
  }
`;
