import gql from 'graphql-tag';
import { SERVICE_INFO_FRAGEMENT } from './fragment';

export const GET_SERVICES = gql`
  query QUERY_SERVICES($idGroupement: ID) {
    services {
      ...ServiceInfo
    }
  }
  ${SERVICE_INFO_FRAGEMENT}
`;

export const GET_MINIM_SERVICES = gql`
  query MINIM_SERVICES {
    services {
      id
      code
      nom
    }
  }
`;

export const GET_SEARCH_CUSTOM_CONTENT_SERVICE = gql`
  query SEARCH_CUSTOM_CONTENT_SERVICE(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $idGroupement: ID
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Service {
          ...ServiceInfo
        }
      }
    }
  }
  ${SERVICE_INFO_FRAGEMENT}
`;
