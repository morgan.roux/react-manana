import gql from 'graphql-tag';

export const LEAFLET_INFO_FRAGMENT = gql`
  fragment LeafletInfo on Leaflet {
    id
    modele
    codeMaj
    dateCreation
    dateModification
  }
`;
