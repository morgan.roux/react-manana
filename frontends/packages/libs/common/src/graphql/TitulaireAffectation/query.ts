import gql from 'graphql-tag';
import { TITULAIRE_AFFECTATION_INFO_FRAGMENT } from './fragment';

export const GET_SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION = gql`
  query SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on TitulaireAffectation {
          ...TitulaireAffectationInfo
        }
      }
    }
  }
  ${TITULAIRE_AFFECTATION_INFO_FRAGMENT}
`;

export const GET_SEARCH_AFFECTATION_TITULAIRE_LIST = gql`
  query SEARCH_AFFECTATION_TITULAIRE_LIST($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Titulaire {
          id
          fullName
        }
      }
    }
  }
`;
