import gql from 'graphql-tag';

export const SERVICE_PHARMACIE_INFO_FRAGMENT = gql`
  fragment ServicePharmacieInfo on ServicePharmacie {
    id
    nom
    typeService
  }
`;
