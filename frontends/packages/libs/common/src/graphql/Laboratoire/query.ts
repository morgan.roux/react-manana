import gql from 'graphql-tag';
import { LABORATOIRE_INFO_FRAGMENT, FICHE_LABORATOIRE_FRAGMENT } from './fragment';

export const GET_LABORATOIRE = gql`
  query LABORATOIRE($id: ID!) {
    laboratoire(id: $id) {
      ...LaboratoireInfo
      user {
        id
        email
        login
        status
        codeTraitements
        userPhoto {
          id
          fichier {
            id
            chemin
            nomOriginal
            publicUrl
          }
        }
      }
    }
  }
  ${LABORATOIRE_INFO_FRAGMENT}
`;

export const GET_LABORATOIRES = gql`
  query LABORATOIRES {
    laboratoires {
      ...LaboratoireInfo
      laboSuite {
        id
        adresse
        webSiteUrl
        telephone
        telecopie
        ville
        codePostal
      }
      user {
        id
        email
        login
        status
      }
    }
  }
  ${LABORATOIRE_INFO_FRAGMENT}
`;

export const GET_FICHE_LABORATOIRE = gql`
  query FICHE_LABORATOIRE($id: ID!) {
    laboratoire(id: $id) {
      ...FicheLaboratoire
    }
  }
  ${FICHE_LABORATOIRE_FRAGMENT}
`;

export const DO_SEARCH_LABORATOIRE = gql`
  query SEARCH_LABORATOIRE(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Laboratoire {
          type
          id
          nomLabo
          laboratoirePartenaire {
            id
            debutPartenaire
            finPartenaire
            typePartenaire
            statutPartenaire
          }
        }
      }
    }
  }
`;

export const GET_SEARCH_CUSTOM_CONTENT_LABORATOIRE = gql`
  query SEARCH_CUSTOM_CONTENT_LABORATOIRE($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Laboratoire {
          id
          nomLabo
          user {
            id
            userName
            email
            login
            status
            dateCreation
            dateModification
          }
          laboSuite {
            id
            adresse
            webSiteUrl
            telephone
            telecopie
            ville
            codePostal
          }
        }
      }
    }
  }
`;

export const GET_SEARCH_CUSTOM_CONTENT_LABORATOIRE_MINIM = gql`
  query SEARCH_CUSTOM_CONTENT_LABORATOIRE_MINIM(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Laboratoire {
          id
          nomLabo
          sortie
        }
      }
    }
  }
`;
export const GET_LIST_LABORATOIRE_PARTENAIRE = gql`
  query LIST_LABORATOIRE_PARTENAIRE($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Laboratoire {
          id
          nomLabo
          laboSuite {
            adresse
            codePostal
            ville
          }
          actualites {
            dateCreation
            dateFin
            partenairesCible {
              nom
            }
          }
          user {
            userPartenaire {
              partenaireServicePartenaire {
                dateDebutPartenaire
                dateFinPartenaire
                typePartenaire
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_SEARCH_LABORATOIRE_PARTENAIRE = gql`
  query SEARCH_LABORATOIRE_PARTENAIRE($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Laboratoire {
          __typename
          type
          id
          nomLabo
          sortie
          laboSuite {
            id
            adresse
            webSiteUrl
            telephone
            telecopie
            ville
            codePostal
          }
          actualites {
            id
            libelle
            dateCreation
            fichierPresentations {
              id
              publicUrl
            }
          }
          photo {
            id
            chemin
            nomOriginal
            publicUrl
          }
          laboratoirePartenaire {
            id
            debutPartenaire
            finPartenaire
            typePartenaire
            statutPartenaire
          }
        }
      }
    }
  }
`;
