import gql from 'graphql-tag';

export const DEPARTEMENT_INFO_FRAGMENT = gql`
  fragment DepartementInfo on Departement {
    id
    code
    nom
  }
`;

export const DEPARTEMENT_WITH_REGION_INFO_FRAGMENT = gql`
  fragment DepartementWithRegionInfo on Departement {
    id
    code
    nom
    region {
      id
      nom
    }
  }
`;
