import gql from 'graphql-tag';

export const GET_SEARCH_CUSTOM_PARTAGE = gql`
  query SEARCH_CUSTOM_PARTAGE($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Partage {
          id
          dateCreation
          typePartage
        }
      }
    }
  }
`;
