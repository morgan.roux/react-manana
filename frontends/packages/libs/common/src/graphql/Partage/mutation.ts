import gql from 'graphql-tag';

export const DO_CREATE_UPDATE_PARTAGE = gql`
  mutation CREATE_UPDATE_PARTAGE($input: PartageInput!) {
    createUpdatePartage(input: $input) {
      id
      typePartage
      item {
        code
      }
      userPartageant {
        userName
      }
    }
  }
`;
