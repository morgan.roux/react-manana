import gql from 'graphql-tag';
import { PARAMETER_INFO_FRAGEMENT } from './fragment';

export const GET_PARAMETERS = gql`
  query PARAMETERS {
    parameters {
      ...ParameterInfo
    }
  }
  ${PARAMETER_INFO_FRAGEMENT}
`;

export const GET_PARAMETER = gql`
  query PARAMETER($id: ID!) {
    parameter(id: $id) {
      ...ParameterInfo
    }
  }
  ${PARAMETER_INFO_FRAGEMENT}
`;

export const GET_PARAMETERS_BY_CATEGORIES = gql`
  query PARAMETERS_BY_CATEGORIES($categories: [ParamCategory!]!) {
    parametersByCategories(categories: $categories) {
      ...ParameterInfo
    }
  }
  ${PARAMETER_INFO_FRAGEMENT}
`;

export const GET_PARAMETER_BY_CODE = gql`
  query PARAMETER_BY_CODE($code: String!) {
    parameterByCode(code: $code) {
      ...ParameterInfo
    }
  }
  ${PARAMETER_INFO_FRAGEMENT}
`;

export const GET_PARAMETERS_GROUPES_CATEGORIES = gql`
  query PARAMETERS_GROUPES_CATEGORIES($categories: [ParamCategory!]!, $groupes: [String!]!) {
    parametersGroupesCategories(categories: $categories, groupes: $groupes) {
      ...ParameterInfo
    }
  }
  ${PARAMETER_INFO_FRAGEMENT}
`;
