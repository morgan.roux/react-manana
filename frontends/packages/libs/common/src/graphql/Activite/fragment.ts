import gql from 'graphql-tag';

export const ACTIVITY_INFO_FRAGEMENT = gql`
  fragment ActivityInfo on ActiviteUser {
    id
    type
    dateCreation
    idUser {
      id
      userName
      role {
        id
        code
        nom
      }
    }
    idActiviteType {
      id
      code
      nom
    }
    log
  }
`;
