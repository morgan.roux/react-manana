import gql from 'graphql-tag';
import { ACTIVITY_INFO_FRAGEMENT } from './fragment';

export const DO_GET_ACTIVITY = gql`
  query GET_ACTIVITY($idUser: ID!) {
    userActiviteUser(idUser: $idUser) {
      ...ActivityInfo
    }
  }
  ${ACTIVITY_INFO_FRAGEMENT}
`;
