import gql from 'graphql-tag';
import { INFORMATION_LIAISON_INFO_FRAGEMENT } from './fragment';

export const DO_CREATE_UPDATE_INFORMATION_LIAISON = gql`
  mutation CREATE_UPDATE_INFORMATION_LIAISON($input: InformationLiaisonInput!) {
    createUpdateInformationLiaison(input: $input) {
      ...InformationLiaisonInfo
    }
  }
  ${INFORMATION_LIAISON_INFO_FRAGEMENT}
`;

export const DO_TAKE_CHARGE_INFORMATION_LIAISON = gql`
  mutation TAKE_CHARGE_INFORMATION_LIAISON($input: TakeChargeInformationLiaisonInput!) {
    takeChargeInformationLiaison(input: $input) {
      ...InformationLiaisonInfo
    }
  }
  ${INFORMATION_LIAISON_INFO_FRAGEMENT}
`;

export const DO_DELETE_SOFT_INFORMATION_LIAISONS = gql`
  mutation DELETE_SOFT_INFORMATION_LIAISONS($ids: [ID!]!) {
    softDeleteInformationLiaisons(ids: $ids) {
      ...InformationLiaisonInfo
    }
  }
  ${INFORMATION_LIAISON_INFO_FRAGEMENT}
`;

export const DO_UPDATE_INFORMATION_LIAISON_STATUS = gql`
  mutation UPDATE_INFORMATION_LIAISON_STATUS($id: ID!, $statut: String!) {
    updateInformationLiaisonStatut(id: $id, statut: $statut) {
      ...InformationLiaisonInfo
    }
  }
  ${INFORMATION_LIAISON_INFO_FRAGEMENT}
`;

export const DO_UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED = gql`
  mutation UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED($id: ID!, $statut: String!) {
    updateInformationLiaisonStatutByUserConcerned(id: $id, statut: $statut) {
      id
      statut
      informationLiaison {
        ...InformationLiaisonInfo
      }
    }
  }
  ${INFORMATION_LIAISON_INFO_FRAGEMENT}
`;

export const DO_READ_INFORMATION_LIAISON_BY_USER_CONCERNED = gql`
  mutation READ_INFORMATION_LIAISON_BY_USER_CONCERNED($id: ID!, $lu: Boolean!) {
    readInformationLiaisonByUserConcerned(id: $id, lu: $lu) {
      id
      statut
      informationLiaison {
        ...InformationLiaisonInfo
      }
    }
  }
  ${INFORMATION_LIAISON_INFO_FRAGEMENT}
`;
