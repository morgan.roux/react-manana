import gql from 'graphql-tag';
import { ACTION_INFO_FRAGEMENT } from './fragment';

export const DO_GET_ACTION = gql`
  query GET_ACTION($id: ID!) {
    action(id: $id) {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;

export const DO_GET_ACTIONS = gql`
  query GET_ACTIONS {
    actions {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;

export const GET_SEARCH_ACTION = gql`
  query SEARCH_ACTION(
    $query: JSON
    $type: [String]
    $filterBy: JSON
    $sortBy: JSON
    $take: Int
    $skip: Int
  ) {
    search(
      query: $query
      type: $type
      filterBy: $filterBy
      sortBy: $sortBy
      take: $take
      skip: $skip
    ) {
      total
      data {
        ... on Action {
          ...ActionInfo
        }
      }
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;
