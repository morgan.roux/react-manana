import gql from 'graphql-tag';

export const SUB_ACTION_INFO_FRAGEMENT = gql`
  fragment SubActionInfo on Action {
    id
    ordre
    description
    dateDebut
    dateFin
    priority
    codeMaj
    status
    isInInbox
    isInInboxTeam
    isRemoved
    firstComment {
      id
      content
    }
    actionType {
      id
      code
      libelle
      isRemoved
    }
    dateCreation
    dateModification
    idItemAssocie
    nbComment
    origine {
      id
      code
      libelle
    }
    section {
      id
      ordre
      libelle
      isInInbox
      isInInboxTeam
    }
    actionParent {
      id
      ordre
      description
      dateDebut
      dateFin
      priority
      status
      nbComment
    }
    item {
      id
      code
      name
    }
    project {
      id
      name
      typeProject
      participants {
        id
      }
    }
    userCreation {
      id
      email
      login
      userName
      idGroupement
      role {
        id
        code
        nom
      }
    }
    assignedUsers {
      id
      email
      login
      userName
    }
    etiquettes {
      id
      ordre
      nom
      isRemoved
      couleur {
        id
        code
        libelle
        isRemoved
      }
    }
    importance{
        id
        ordre
        libelle
      }
  }
`;

export const ACTION_INFO_FRAGEMENT = gql`
  fragment ActionInfo on Action {
    id
    ordre
    description
    dateDebut
    dateFin
    priority
    codeMaj
    status
    isInInbox
    isInInboxTeam
    isRemoved
    firstComment {
      id
      content
    }
    actionType {
      id
      code
      libelle
      isRemoved
    }
    dateCreation
    dateModification
    idItemAssocie
    nbComment
    origine {
      id
      code
      libelle
    }
    section {
      id
      ordre
      libelle
      isInInbox
      isInInboxTeam
    }
    actionParent {
      id
      ordre
      description
      dateDebut
      dateFin
      priority
      status
      nbComment
    }
    idItemAssocie
    item {
      id
      code
      name
    }
    project {
      id
      name
      typeProject
      participants {
        id
      }
    }
    userCreation {
      id
      email
      login
      userName
      role {
        id
        code
        nom
      }
    }
    assignedUsers {
      id
      email
      login
      userName
    }
    etiquettes {
      id
      ordre
      nom
      isRemoved
      couleur {
        id
        code
        libelle
        isRemoved
      }
    }
    importance {
      id
      ordre
      libelle
    }
    subActions {
      id
      ordre
      description
      dateDebut
      dateFin
      priority
      codeMaj
      status
      isInInbox
      isInInboxTeam
      isRemoved
      firstComment {
        id
        content
      }
      actionType {
        id
        code
        libelle
        isRemoved
      }
      dateCreation
      dateModification
      idItemAssocie
      nbComment
      origine {
        id
        code
        libelle
      }
      section {
        id
        ordre
        libelle
        isInInbox
        isInInboxTeam
      }
      actionParent {
        id
        ordre
        description
        dateDebut
        dateFin
        priority
        status
        nbComment
      }
      item {
        id
        code
        name
      }
      project {
        id
        name
        typeProject
      }
      userCreation {
        id
        email
        login
        userName
        role {
          id
          code
          nom
        }
      }
      assignedUsers {
        id
        email
        login
        userName
      }
      etiquettes {
        id
        ordre
        nom
        isRemoved
        couleur {
          id
          code
          libelle
          isRemoved
        }
      }
      importance{
        id
        ordre
        libelle
      }
      subActions {
        id
        ordre
        description
        dateDebut
        dateFin
        priority
        codeMaj
        status
        isInInbox
        isInInboxTeam
        isRemoved
        firstComment {
          id
          content
        }
        actionType {
          id
          code
          libelle
          isRemoved
        }
        dateCreation
        dateModification
        idItemAssocie
        nbComment
        origine {
          id
          code
          libelle
        }
        section {
          id
          ordre
          libelle
          isInInbox
          isInInboxTeam
        }
        actionParent {
          id
          ordre
          description
          dateDebut
          dateFin
          priority
          status
          nbComment
        }
        item {
          id
          code
          name
        }
        project {
          id
          name
          typeProject
        }
        userCreation {
          id
          email
          login
          userName
          idGroupement
          role {
            id
            code
            nom
          }
        }
        assignedUsers {
          id
          email
          login
          userName
        }
        etiquettes {
          id
          ordre
          nom
          isRemoved
          couleur {
            id
            code
            libelle
            isRemoved
          }
        }
        importance{
        id
        ordre
        libelle
      }
        subActions {
          ...SubActionInfo
        }
      }
    }
  }
  ${SUB_ACTION_INFO_FRAGEMENT}
`;
