import gql from 'graphql-tag';
import { COMMANDE_FRAGMENT } from './fragment';

export interface ArticleCommande {
  id: string;
  quantite: number;
}
/*
export const DO_CREATE_OPERATION_COMMANDE = gql`
  mutation CREATE_OPERATION_COMMANDE(
    $idPharmacie: ID!
    $idOperation: ID!
    $idPublicite: String
    $articlesCommande: [ArticleCommande]
    $commentaireInterne: String
    $commentaireExterne: String
  ) {
    createOperationCommande(
      idPharmacie: $idPharmacie
      idOperation: $idOperation
      idPublicite: $idPublicite
      articlesCommande: $articlesCommande
      commentaireInterne: $commentaireInterne
      commentaireExterne: $commentaireExterne
    ) {
      ...Commande
    }
  }
  ${COMMANDE_FRAGMENT}
`;*/

export const DO_UPDATE_STATUT_COMMANDE = gql`
  mutation UPDATE_STATUT_COMMANDE($id: ID!, $statut: String) {
    updateStatutCommande(id: $id, statut: $statut) {
      type
      id
      commandeType {
        id
        code
        libelle
        dateCreation
        dateModification
      }
      commandeStatut {
        id
        code
        libelle
        dateCreation
        dateModification
      }
      dateCommande
      nbrRef
      quantite
      uniteGratuite
      prixBaseTotalHT
      valeurRemiseTotal
      prixNetTotalHT
      fraisPort
      valeurFrancoPort
      remiseGlobale
      commentaireInterne
      commentaireExterne
      pathFile
      dateCreation
      dateModification
    }
  }
`;
