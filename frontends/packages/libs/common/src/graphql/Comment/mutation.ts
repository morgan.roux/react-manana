import gql from 'graphql-tag';
import { COMMENT_INFO_FRAGEMENT } from './fragment';

export const DO_CREATE_COMMENT = gql`
  mutation CREATE_COMMENT($input: CommentInput!) {
    createComment(input: $input) {
      ...CommentInfo
    }
  }
  ${COMMENT_INFO_FRAGEMENT}
`;
