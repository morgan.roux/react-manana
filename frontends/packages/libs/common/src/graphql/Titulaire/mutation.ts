import gql from 'graphql-tag';

export const DO_CREATE_UPDATE_TITULAIRE = gql`
  mutation createUpdateTitulaire(
    $id: ID
    $civilite: String
    $nom: String!
    $prenom: String
    $contact: ContactInput
    $idPharmacies: [ID!]!
  ) {
    createUpdateTitulaire(
      id: $id
      civilite: $civilite
      nom: $nom
      prenom: $prenom
      contact: $contact
      idPharmacies: $idPharmacies
    ) {
      type
      id
      civilite
      nom
      prenom
      estPresidentRegion
      idGroupement
      users {
        id
        email
        login
        status
        jourNaissance
        moisNaissance
        anneeNaissance
        codeTraitements
        userPhoto {
          id
          fichier {
            id
            chemin
            nomOriginal
            publicUrl
          }
        }
      }
      pharmacieUser {
        id
        cip
      }
      pharmacies {
        id
        nom
      }
      contact {
        id
        cp
        ville
        pays
        adresse1
        adresse2
        faxProf
        faxPerso
        telProf
        telMobProf
        telPerso
        telMobPerso
        mailProf
        mailPerso
        siteProf
        sitePerso
      }
    }
  }
`;

export const DO_DELETE_TITULAIRES = gql`
  mutation deleteSoftTitulaires($ids: [ID!]!) {
    deleteSoftTitulaires(ids: $ids) {
      id
    }
  }
`;
