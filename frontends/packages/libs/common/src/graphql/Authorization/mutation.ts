import gql from 'graphql-tag';
import { GROUPEMENT_INFO } from '../Groupement/fragement';
import { USER_INFO_FRAGEMENT } from '../User/fragment';



export const DO_CREATE_AUTHORIZATION = gql`
  mutation CREATE_AUTHORIZATION($input: RgpdAutorisationInput!) {
    createUpdateRgpdAutorisation(input: $input) {
      id
      title
      description
      order
      dateCreation
      dateModification
      userCreation {
        ...UserInfo
      }
      groupement {
        ...GroupementInfo
      }
    }
    } 
    ${USER_INFO_FRAGEMENT}
    ${GROUPEMENT_INFO}
`;


export const DO_REMOVE_AUTHORIZATION = gql`
  mutation REMOVE_AUTHORIZATION($id: ID!) {
    deleteRgpdAutorisation(id: $id) {
      id
      title
      description
      order
      dateCreation
      dateModification
      userCreation {
        ...UserInfo
      }
      groupement {
        ...GroupementInfo
      }
    }
    } 
    ${USER_INFO_FRAGEMENT}
    ${GROUPEMENT_INFO}
`;

