import gql from 'graphql-tag';

export const PHARMACIE_COMPTA_INFO_FRAGMENT = gql`
  fragment PharmacieComptaInfo on PharmacieCompta {
    id
    siret
    codeERP
    ape
    tvaIntra
    rcs
    contactCompta
    chequeGrp
    valeurChequeGrp
    commentaireChequeGrp
    droitAccesGrp
    valeurChequeAccesGrp
    commentaireChequeAccesGrp
    structureJuridique
    denominationSociale
    telCompta
    modifStatut
    raisonModif
    dateModifStatut
    nomBanque
    banqueGuichet
    banqueCompte
    banqueCle
    dateMajRib
    iban
    swift
    dateMajIban
  }
`;
//TODO : banqueRibSearchCommandeCanalInfo
