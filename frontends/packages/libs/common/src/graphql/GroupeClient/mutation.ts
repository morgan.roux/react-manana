import gql from 'graphql-tag';
import { GROUPE_CLIENT_INFO_FRAGMENT_WITH_PHARMA } from './fragment';

export const DO_CREATE_UPDATE_GROUPE_CLIENT = gql`
  mutation CREATE_UPDATE_GROUPE_CLIENT(
    $id: ID
    $nom: String!
    $dateValiditeDebut: String!
    $dateValiditeFin: String!
    $idPharmacies: [ID!]!
  ) {
    createUpdateGroupeClient(
      id: $id
      nom: $nom
      dateValiditeDebut: $dateValiditeDebut
      dateValiditeFin: $dateValiditeFin
      idPharmacies: $idPharmacies
    ) {
      ...GroupeClientInfoWithPharma
    }
  }
  ${GROUPE_CLIENT_INFO_FRAGMENT_WITH_PHARMA}
`;

export const DO_DELETE_SOFT_GROUPE_CLIENT = gql`
  mutation DELETE_SOFT_GROUPE_CLIENT($id: ID!) {
    deleteSoftGroupeClient(id: $id) {
      ...GroupeClientInfoWithPharma
    }
  }
  ${GROUPE_CLIENT_INFO_FRAGMENT_WITH_PHARMA}
`;

export const DO_DELETE_SOFT_GROUPES_CLIENTS = gql`
  mutation DELETE_SOFT_GROUPES_CLIENTS($ids: [ID!]!) {
    deleteSoftGroupeClients(ids: $ids) {
      ...GroupeClientInfoWithPharma
    }
  }
  ${GROUPE_CLIENT_INFO_FRAGMENT_WITH_PHARMA}
`;
