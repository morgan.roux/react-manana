import gql from 'graphql-tag';
import { COMMENT_RESULT_INFO_FRAGEMENT } from '../Comment/fragment';
import { USER_SMYLEY_INFO_FRAGMENT } from '../Smyley/fragment';
import { COMMANDE_CANAL_INFO_FRAGMENT } from '../CommandeCanal/fragment';
import { COMMENT_INFO_FRAGEMENT } from '../Comment/fragment';
import { PRODUIT_PHOTO_INFO } from '../ProduitPhoto/fragment';

export const COMMENTS_PRODUIT_CANAL_FRAGMENT = gql`
  fragment CommentsProduitCanal on ProduitCanal {
    comments(take: $take, skip: $skip) {
      total
      data {
        ...CommentInfo
      }
    }
  }
  ${COMMENT_INFO_FRAGEMENT}
`;

export const REMISES_PRODUIT_CANAL = gql`
  fragment RemisesProduitCanal on ProduitCanal {
    remises(idPharmacie: $idPharmacie, idRemiseOperation: $idRemiseOperation) {
      id
      quantiteMin
      quantiteMax
      pourcentageRemise
      nombreUg
    }
  }
`;

export const PRODUIT_CANAL_INFO_FRAGMENT = gql`
  fragment FullProduitCanalInfo on ProduitCanal {
    type
    id
    qteStock
    qteMin
    stv
    unitePetitCond
    uniteSupCond
    prixFab
    prixPhv
    prixTtc
    dateCreation
    dateModification
    dateRetrait
    isActive
    ...RemisesProduitCanal
    qteTotalRemisePanachees(idPharmacie: $idPharmacie)
    inMyPanier(idPharmacie: $idPharmacie)
    inOtherPanier(idPharmacie: $idPharmacie) {
      id
    }
    articleSamePanachees {
      id
    }
    operations {
      id
    }
    userSmyleys {
      total
      data {
        ...UserSmyleyInfo
      }
    }
    comments(take: $take, skip: $skip) {
      ...CommentResultInfo
    }
    commandeCanal {
      ...CommandeCanalInfo
    }
    produit {
      id
      produitPhoto {
        ...ProduitPhotoInfo
      }
      id
      libelle
      libelle2
      supprimer
      surveillanceRenforcee
      dateCreation
      dateModification
      produitCode {
        id
        code
        typeCode
        referent
      }
      famille {
        id
        codeFamille
        libelleFamille
      }
      produitTechReg {
        id
        isTipsLpp
        isHomeo
        laboExploitant {
          id
          nomLabo
          sortie
        }
        acte {
          id
          codeActe
          libelleActe
        }
        liste {
          id
          codeListe
          libelle
        }
        libelleStockage {
          id
          codeInfo
          code
        }
        tauxss {
          id
          codeTaux
          tauxSS
        }
        tva {
          id
          codeTva
          tauxTva
        }
      }
    }
  }
  ${USER_SMYLEY_INFO_FRAGMENT}
  ${REMISES_PRODUIT_CANAL}
  ${COMMENT_RESULT_INFO_FRAGEMENT}
  ${COMMANDE_CANAL_INFO_FRAGMENT}
  ${PRODUIT_PHOTO_INFO}
`;

export const PRODUIT_CANAL_OPERATION_INFO_FRAGMENT = gql`
  fragment ProduitCanalOperationInfo on ProduitCanal {
    id
    qteStock
    stv
    prixPhv
    commandeCanal {
      id
      code
    }
    produit {
      id
      produitPhoto {
        id
        fichier {
          id
          publicUrl
          nomOriginal
        }
      }
    }
    articleSamePanachees(idPharmacie: $idPharmacie) {
      id
    }
    remises(idPharmacie: $idPharmacie, idRemiseOperation: $idRemiseOperation) {
      id
      quantiteMin
      quantiteMax
      pourcentageRemise
      nombreUg
    }
  }
`;

export const SEARCH_PRODUIT_CANAL_INFO = gql`
  fragment SearchProduitCanal on ProduitCanal {
    type
    id
    qteStock
    stv
    unitePetitCond
    prixPhv
    dateCreation
    isActive
    qteTotalRemisePanachees(idPharmacie: $idPharmacie)
    inMyPanier(idPharmacie: $idPharmacie)
    ...RemisesProduitCanal
    ...CommentsProduitCanal
    produit {
      id
      libelle
      famille {
        id
        codeFamille
        libelleFamille
      }
      produitCode {
        id
        code
        typeCode
        referent
      }
      produitTechReg {
        id
        laboExploitant {
          id
          nomLabo
          sortie
        }
      }
    }
    userSmyleys {
      total
      data {
        ...UserSmyleyInfo
      }
    }
    commandeCanal {
      id
      code
      libelle
    }
    inOtherPanier(idPharmacie: $idPharmacie) {
      id
    }
    articleSamePanachees(idPharmacie: $idPharmacie) {
      id
    }
    operations {
      id
    }
  }
  ${USER_SMYLEY_INFO_FRAGMENT}
  ${REMISES_PRODUIT_CANAL}
  ${COMMENTS_PRODUIT_CANAL_FRAGMENT}
`;

export const PRODUIT_CANAL_FRAGMENT = gql`
  fragment MinimalProduitCanalInfo on ProduitCanal {
    type
    id
    produit {
      id
      libelle
      libelle2
      produitPhoto {
        ...ProduitPhotoInfo
      }
      produitCode {
        id
        code
        typeCode
        referent
      }
      produitTechReg {
        id
        laboExploitant {
          id
          nomLabo
          sortie
        }
      }
    }
    qteStock
    qteMin
    stv
    unitePetitCond
    uniteSupCond
    prixFab
    prixPhv
    prixTtc
    dateCreation
    dateModification
    dateRetrait
    isActive
    qteTotalRemisePanachees(idPharmacie: $idPharmacie)
    inMyPanier(idPharmacie: $idPharmacie)
    inOtherPanier(idPharmacie: $idPharmacie) {
      id
    }
    ...RemisesProduitCanal
    ...CommentsProduitCanal
  }
  ${REMISES_PRODUIT_CANAL}
  ${PRODUIT_PHOTO_INFO}
  ${COMMENTS_PRODUIT_CANAL_FRAGMENT}
`;

export const PRODUIT_CANAL = gql`
  fragment ProduitCanal on ProduitCanal {
    type
    id
    qteStock
    stv
    prixPhv
    dateCreation
    isActive
    produit {
      id
      libelle
      libelle2
      produitCode {
        id
        code
        typeCode
        referent
      }
      produitTechReg {
        id
        laboExploitant {
          id
          nomLabo
          sortie
        }
      }
    }
    operations {
      id
    }
    qteTotalRemisePanachees(idPharmacie: $idPharmacie)
    inMyPanier(idPharmacie: $idPharmacie)
    inOtherPanier(idPharmacie: $idPharmacie) {
      id
    }
    articleSamePanachees(idPharmacie: $idPharmacie) {
      id
    }
    userSmyleys {
      total
      data {
        ...UserSmyleyInfo
      }
    }
    commandeCanal {
      id
      code
      libelle
    }
    ...RemisesProduitCanal
    ...CommentsProduitCanal
  }
  ${USER_SMYLEY_INFO_FRAGMENT}
  ${REMISES_PRODUIT_CANAL}
  ${COMMENTS_PRODUIT_CANAL_FRAGMENT}
`;

export const ARTICLE_SAME_PANACHEES_FRAGMENT = gql`
  fragment Canal_Article_Same_Panachees on ProduitCanal {
    articleSamePanachees(idPharmacie: $idPharmacie) {
      id
      prixPhv
      qteStock
      stv
      operations {
        id
      }
      ...RemisesProduitCanal
      produit {
        id
        libelle
        produitCode {
          id
          code
          typeCode
          referent
        }
        produitTechReg {
          id
          laboExploitant {
            id
            nomLabo
            sortie
          }
        }
      }
    }
  }
  ${REMISES_PRODUIT_CANAL}
`;
