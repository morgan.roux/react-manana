import gql from 'graphql-tag';
export const DELETE_CANAL_ARTICLE = gql`
  mutation softDeleteCanalArticle($id: ID!) {
    softDeleteCanalArticle(id: $id) {
      id
    }
  }
`;

export const DELETE_CANAL_ARTICLES = gql`
  mutation softDeleteCanalArticles($ids: [ID!]!) {
    softDeleteCanalArticles(ids: $ids) {
      id
    }
  }
`;

export const GET_GESTION_PRODUIT_CANAL = gql`
  query GESTION_PRODUIT_CANAL($id: ID!) {
    produitCanal(id: $id) {
      id
      stv
      prixPhv
      unitePetitCond
      produit {
        id
        libelle
        supprimer
        produitPhoto {
          id
          fichier {
            id
            publicUrl
            nomOriginal
          }
        }
        famille {
          id
          codeFamille
          libelleFamille
        }
        produitCode {
          id
          code
          typeCode
          referent
        }
        produitTechReg {
          id
          laboExploitant {
            id
            nomLabo
            sortie
          }
        }
      }
    }
  }
`;
