import gql from 'graphql-tag';
import { OPERATION_COMMERCIALE_INFO_FRAGEMENT } from './fragment';

export const CALCUL_PRIX_TOTAL_OPERATION = gql`
  mutation calculPrixTotalOperation(
    $articles: [ArticleCommande]
    $idPharmacie: ID!
    $idOperation: ID!
  ) {
    calculPrixTotalOperation(
      articles: $articles
      idPharmacie: $idPharmacie
      idOperation: $idOperation
    ) {
      qteTotal
      prixBaseTotal
      prixNetTotal
      uniteGratuite
    }
  }
`;

export const DO_CREATE_OPERATION = gql`
  mutation createUpdateOperation(
    $idOperation: ID
    $codeItem: String!
    $libelle: String!
    $description: String!
    $niveauPriorite: Int!
    $dateDebut: DateTime!
    $dateFin: DateTime!
    $idLaboratoire: ID
    $fichierPresentations: [FichierInput]!
    $globalite: Boolean
    $typeCommande: ID
    $idsPharmacie: [ID]
    $articles: [OperationArticleInput]
    $fichierPharmacie: FichierInput
    $fichierProduit: FichierInput
    $idCanalCommande: ID!
    $accordCommercial: Boolean!
    $idPharmacie: ID
    $userId: String
    $idPharmacieUser: ID
    $idProject: ID
    $actionPriorite: Int
    $actionDescription: String
    $actionDueDate: String
    $caMoyenPharmacie: Float
    $nbPharmacieCommande: Int
    $nbMoyenLigne: Float
    $idMarche: ID
    $idPromotion: ID
    $idRemiseOperation: ID
  ) {
    createUpdateOperation(
      idOperation: $idOperation
      codeItem: $codeItem
      libelle: $libelle
      description: $description
      niveauPriorite: $niveauPriorite
      dateDebut: $dateDebut
      dateFin: $dateFin
      idLaboratoire: $idLaboratoire
      fichierPresentations: $fichierPresentations
      globalite: $globalite
      typeCommande: $typeCommande
      idsPharmacie: $idsPharmacie
      articles: $articles
      fichierPharmacie: $fichierPharmacie
      fichierProduit: $fichierProduit
      idCanalCommande: $idCanalCommande
      accordCommercial: $accordCommercial
      idProject: $idProject
      actionPriorite: $actionPriorite
      actionDescription: $actionDescription
      actionDueDate: $actionDueDate
      caMoyenPharmacie: $caMoyenPharmacie
      nbPharmacieCommande: $nbPharmacieCommande
      nbMoyenLigne: $nbMoyenLigne
      idMarche: $idMarche
      idPromotion: $idPromotion
    ) {
      ...OperationCommercialeInfo
    }
  }
  ${OPERATION_COMMERCIALE_INFO_FRAGEMENT}
`;

export const DO_CREATE_OPERATION_VIEWER = gql`
  mutation createOperationViewer(
    $idPharmacie: ID!
    $idOperation: ID!
    $userId: String
    $idPharmacieUser: ID!
    $idRemiseOperation: ID
  ) {
    createOperationViewer(idOperation: $idOperation, idPharmacie: $idPharmacieUser) {
      ...OperationCommercialeInfo
    }
  }
  ${OPERATION_COMMERCIALE_INFO_FRAGEMENT}
`;

export const DO_DELETE_OPERATION_VIEWER = gql`
  mutation deleteOperationViewer(
    $idPharmacie: ID!
    $idOperation: ID!
    $userId: String
    $idPharmacieUser: ID!
    $idRemiseOperation: ID
  ) {
    deleteOperationViewer(idOperation: $idOperation, idPharmacie: $idPharmacieUser) {
      ...OperationCommercialeInfo
    }
  }
  ${OPERATION_COMMERCIALE_INFO_FRAGEMENT}
`;

export const DO_DELETE_OPERATION = gql`
  mutation deleteOperation($id: ID!) {
    deleteOperation(id: $id) {
      id
    }
  }
`;

export const DO_MARK_OPERATION_AS_SEEN = gql`
  mutation markAsSeenOperation($id: ID!, $userId: String, $idPharmacie: ID) {
    markAsSeenOperation(id: $id) {
      id
      seen(userId: $userId, idPharmacie: $idPharmacie)
    }
  }
`;

export const DO_MARK_OPERATION_AS_NOT_SEEN = gql`
  mutation markAsNotSeenOperation($id: ID!, $idPharmacieUser: ID, $userId: String) {
    markAsNotSeenOperation(id: $id, idPharmacie: $idPharmacieUser) {
      id
      seen(idPharmacie: $idPharmacieUser, userId: $userId)
      nombrePharmaciesConsultes
    }
  }
`;
