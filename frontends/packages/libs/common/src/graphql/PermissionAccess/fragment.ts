import gql from 'graphql-tag';

export const PERMISSION_ACCESS_INFO_FRAGMENT = gql`
  fragment PermissionAccessInfo on Traitement {
    id
    nom
    code
  }
`;
