import gql from 'graphql-tag';

export const TITULAIRE_FONCTION_INFO_FRAGMENT = gql`
  fragment TitulaireFonctionInfo on TitulaireFonction {
    id
    nom
    code
  }
`;
