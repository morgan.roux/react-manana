import gql from 'graphql-tag';

export const DO_CREATE_USER_COLLABORATEUR = gql`
  mutation CREATE_USER_COLLABORATEUR(
    $idGroupement: ID!
    $id: ID!
    $idPharmacie: ID!
    $email: String!
    $login: String!
    $role: String!
    $userId: ID
    $day: Int
    $month: Int
    $year: Int
    $userPhoto: FichierInput
    $codeTraitements: [String]
  ) {
    createUserCollaborateurOfficine(
      idGroupement: $idGroupement
      id: $id
      role: $role
      idPharmacie: $idPharmacie
      email: $email
      login: $login
      userId: $userId
      day: $day
      month: $month
      year: $year
      userPhoto: $userPhoto
      codeTraitements: $codeTraitements
    ) {
      type
      id
      civilite
      nom
      prenom
      estAmbassadrice
      commentaire
      sortie
      dateSortie
      idGroupement
      idPharmacie
      pharmacie {
        id
        cip
      }
      user {
        id
        email
        login
        status
      }
      role {
        code
        nom
      }
    }
  }
`;
