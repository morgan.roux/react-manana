import gql from 'graphql-tag';
import { USER_INFO_FRAGEMENT } from './fragment';

export const GET_USERS_GROUPEMENT = gql`
  query USERS_GROUPEMENT($idGroupement: ID!) {
    usersGroupement(idGroupement: $idGroupement) {
      ...UserInfo
    }
  }
  ${USER_INFO_FRAGEMENT}
`;

export const GET_USER = gql`
  query USER($id: ID!) {
    user(id: $id) {
      ...UserInfo
    }
  }
  ${USER_INFO_FRAGEMENT}
`;

export const DO_SEARCH_USERS = gql`
  query SEARCH_USERS($query: JSON, $skip: Int, $take: Int, $filterBy: JSON, $sortBy: JSON) {
    search(
      type: "user"
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on User {
          id
          userName
          email
          login
          status
          role {
            id
            nom
          }
          nbAppel
          nbReclamation
          pharmacie {
            id
          }
          userPhoto {
            id
            fichier {
              id
              urlPresigned
              publicUrl
            }
          }
          userPersonnel {
            id
            service {
              id
              nom
            }
            contact {
              id
              cp
            }
          }
          userPpersonnel {
            id
            contact {
              id
              cp
            }
          }
          userPartenaire {
            id
            contact {
              id
              cp
            }
          }
          userTitulaire {
            id
            titulaire {
              id
              contact {
                cp
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_USER_WITH_MIN_INFO = gql`
  query USER_WITH_MIN_INFO($id: ID!) {
    user(id: $id) {
      id
      userName
    }
  }
`;

export const DO_SEARCH_MIN_USERS = gql`
  query SEARCH_MIN_USERS($query: JSON, $skip: Int, $take: Int, $filterBy: JSON, $sortBy: JSON) {
    search(
      type: "user"
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on User {
          id
          userName
          email
          login
          status
          role {
            id
            nom
            typeRole
            code
          }
          phoneNumber
          contact {
            telProf
            telMobProf
            telPerso
            telMobPerso
          }
        }
      }
    }
  }
`;

export const DO_GET_SEARCH_USER_IDS = gql`
  query GET_SEARCH_USER_IDS($query: JSON, $skip: Int, $take: Int, $filterBy: JSON, $sortBy: JSON) {
    search(
      type: "user"
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on User {
          id
          userName
          login
          email
          role {
            id
            nom
          }
          status
          contact {
            telProf
            telMobProf
            telPerso
            telMobPerso
          }
        }
      }
    }
  }
`;

export const DO_GET_TOTAL_USER_IDS = gql`
  query GET_TOTAL_USER_IDS($query: JSON, $skip: Int, $take: Int, $filterBy: JSON, $sortBy: JSON) {
    search(
      type: "user"
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
    }
  }
`;
