import gql from 'graphql-tag';
import { USER_INFO_FRAGEMENT } from '../User/fragment';
export const ACTION_INFO_FRAGEMENT = gql`
  fragment ActionNewInfo on Action {
    id
    ordre
    description
    dateDebut
    dateFin
    _priority: priority
    status
    isInInbox
    isInInboxTeam
    isRemoved
    actionType {
      id
      code
      libelle
      isRemoved
    }
    dateCreation
    dateModification
    idItemAssocie
    nbComment
    origine {
      id
      code
      libelle
    }
    section {
      id
      ordre
      libelle
      isInInbox
      isInInboxTeam
    }
    actionParent {
      id
      ordre
      description
      dateDebut
      dateFin
      priority
      status
      nbComment
    }
    subActions {
      id
      ordre
      description
      dateDebut
      dateFin
      priority
      status
      isInInbox
      isInInboxTeam
      isRemoved
      actionType {
        id
        code
        libelle
        isRemoved
      }
      dateCreation
      dateModification
      nbComment
    }
    item {
      id
      code
      name
    }
    project {
      id
      name
      typeProject
    }
    userCreation {
      id
      email
      login
      userName
      role {
        id
        code
        nom
      }
    }
    assignedUsers {
      id
      email
      login
      userName
      userPhoto {
        id
        fichier {
          id
          publicUrl
        }
      }
    }
    etiquettes {
      id
      ordre
      nom
      isRemoved
      couleur {
        id
        code
        libelle
        isRemoved
      }
    }
  }
`;

export const PROJECT_INFO_FRAGMENT = gql`
  fragment ProjectNewInfo on Project {
    id
    type
    ordre
    name
    isArchived
    isRemoved
    isShared
    activeActions
    typeProject
    user {
      ...UserInfo
    }
    projetParent {
      id
      type
      name
      participants {
        ...UserInfo
      }
    }
    groupement {
      id
      nom
    }
    pharmacie {
      id
      nom
    }
    couleur {
      id
      code
    }
    codeMaj
    userCreation {
      ...UserInfo
    }
    userModification {
      ...UserInfo
    }
    dateCreation
    dateModification
    participants {
      ...UserInfo
    }
    isInFavoris
    subProjects {
      id
      type
      ordre
      name
      participants {
        ...UserInfo
      }
    }
  }
  ${USER_INFO_FRAGEMENT}
`;
