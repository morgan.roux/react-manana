import gql from 'graphql-tag';
import { USER_INFO_FRAGEMENT } from '../User/fragment';
import { ACTION_INFO_FRAGEMENT, PROJECT_INFO_FRAGMENT } from './fragment';

export const GET_TODO_ACTION_ATTRIBUTIONS = gql`
  query TODO_ACTION_ATTRIBUTIONS($isRemoved: Boolean) {
    todoActionAttributions(isRemoved: $isRemoved) {
      id
      action {
        ...ActionNewInfo
      }
      userSource {
        ...UserInfo
      }
      userDestination {
        ...UserInfo
      }
      isRemoved
      userCreation {
        ...UserInfo
      }
      userModification {
        ...UserInfo
      }
      dateCreation
      dateModification
    }
  }
  ${ACTION_INFO_FRAGEMENT}
  ${USER_INFO_FRAGEMENT}
`;

export const GET_SEARCH_CUSTOM_TODO_SECTION = gql`
  query SEARCH_CUSTOM_CONTENT_TODO_SECTION(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $actionStatus: String
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on TodoSection {
          id
          type
          ordre
          libelle
          isInInbox
          isInInboxTeam
          project {
            ...ProjectNewInfo
          }
          user {
            ...UserInfo
          }
          isRemoved
          isArchived
          codeMaj
          userCreation {
            ...UserInfo
          }
          userModification {
            ...UserInfo
          }
          dateCreation
          dateModification
        }
      }
    }
  }
  ${PROJECT_INFO_FRAGMENT}
  ${USER_INFO_FRAGEMENT}
`;
