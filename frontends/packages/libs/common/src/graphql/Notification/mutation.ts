import gql from 'graphql-tag';
import { NOTIFICATION_INFO_FRAGMENT } from './fragment';

export const DO_MARK_NOTIFICATION_AS_SEEN = gql`
  mutation MARK_NOTIFICATION_AS_SEEN($id: ID!, $seen: Boolean) {
    markNotificationAsSeen(id: $id, seen: $seen) {
      ...NotificationInfo
    }
  }
  ${NOTIFICATION_INFO_FRAGMENT}
`;
