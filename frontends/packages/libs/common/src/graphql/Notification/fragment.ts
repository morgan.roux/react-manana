import gql from 'graphql-tag';

export const NOTIFICATION_INFO_FRAGMENT = gql`
  fragment NotificationInfo on Notification {
    id
    type
    targetId
    targetName
    message
    seen
    typeActualite
    dateCreation
    dateModification
    from {
      id
      userName
      type
    }
    to {
      id
      userName
      type
    }
  }
`;
