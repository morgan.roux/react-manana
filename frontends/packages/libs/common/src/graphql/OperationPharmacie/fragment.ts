import gql from 'graphql-tag';
import { FICHIER_FRAGMENT } from '../Fichier/fragment';
export const OPERATION_PHARMACIE_DETAILS_INFO_FRAGEMENT = gql`
  fragment OperationPharmacieDetailsInfo on OperationPharmacieDetail {
    id
    pharmacie {
      id
      nom
    }
    dateCreation
    dateModification
  }
`;

export const OPERATION_PHARMACIE_INFO_FRAGEMENT = gql`
  fragment OperationPharmacieInfo on OperationPharmacie {
    id
    globalite
    dateCreation
    dateModification
    fichierCible {
      ...FichierInfo
    }
    pharmaciedetails {
      ...OperationPharmacieDetailsInfo
    }
  }
  ${FICHIER_FRAGMENT}
  ${OPERATION_PHARMACIE_DETAILS_INFO_FRAGEMENT}
`;
