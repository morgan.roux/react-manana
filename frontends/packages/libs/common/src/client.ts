import React from 'react';
import { useFetchJson } from '@lib/hooks';
import { ApolloClient, InMemoryCacheConfig } from '@apollo/client';
import { createFederationClient } from './federation-client';
import { createGraphqlClient } from './graphql-client';
import { ModuleDefinition } from './module';
import { ApplicationConfig } from './config';

let graphqlClient: ApolloClient<any>;
let federationClient: ApolloClient<any>;

export const getFederationClient = (): ApolloClient<any> => federationClient;

export const getGraphqlClient = (): ApolloClient<any> => graphqlClient;

export const computePublicBasePath = (data: any) =>
  data['STORAGE_PROVIDER'] === 'OVH_OBJECT_STORAGE'
    ? `https://storage.de.cloud.ovh.net/v1/AUTH_${data['OS_TENANT_ID']}/${data['OS_CONTAINER']}`
    : `https://${data['AWS_S3_BUCKET']}.s3.${data['AWS_S3_REGION']}.amazonaws.com`;

export const useInitClient = (
  modules?: ModuleDefinition[],
  commonCacheConfig: InMemoryCacheConfig[] = []
): {
  loading?: boolean;
  error?: Error;
  federation: ApolloClient<any>;
  graphql: ApolloClient<any>;
  config: ApplicationConfig;
} => {
  const { loading, data, error } = useFetchJson(
    //@ts-ignore
    `${__webpack_public_path__}env-config.json`
  );
  const [federation, setFederation] = React.useState<ApolloClient<any>>(federationClient);
  const [graphql, setGraphql] = React.useState<ApolloClient<any>>(graphqlClient);

  React.useEffect(() => {
    if (!loading && !error && data) {
      if (!federation) {
        federationClient = createFederationClient(data['GATEWAY_FEDERATION_URL'], undefined, data.ENVIRONMENT, [
          ...commonCacheConfig,
          ...((modules?.filter((m) => !!m.cacheConfig?.federation).map((m) => m.cacheConfig?.federation) as any) || []),
        ]);

        setFederation(federationClient);
      }

      if (!graphql) {
        graphqlClient = createGraphqlClient(data['API_URL'], undefined, data.ENVIRONMENT, [
          ...commonCacheConfig,
          ...((modules?.filter((m) => !!m.cacheConfig?.graphql).map((m) => m.cacheConfig?.graphql) as any) || []),
        ]);
        setGraphql(graphqlClient);
      }
    }
  }, [loading]);

  return {
    error,
    loading,
    federation,
    graphql,
    config: data
      ? ({
          ...data,
          STORAGE_PUBLIC_BASE_PATH: computePublicBasePath(data),
          APP_SSO_URL: data['REACT_APP_SAML_SSO_ENDPOINT'],
        } as any)
      : undefined,
  };
};
