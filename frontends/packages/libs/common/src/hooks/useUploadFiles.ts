import { useState } from 'react';
import { ComputeUploadedFilenameOptions, UploadedFile, useApplicationContext } from '../context';

interface UseUploadFilesResult {
  loading?: boolean;
  error?: Error;
  data?: UploadedFile[];
}

export const useUploadFiles = (): [
  (files: (File | UploadedFile)[], options?: Partial<ComputeUploadedFilenameOptions>) => Promise<UploadedFile[]>,
  UseUploadFilesResult
] => {
  const { uploadFiles } = useApplicationContext();
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<Error>();
  const [data, setData] = useState<UploadedFile[]>();

  const upload = async (
    files: (File | UploadedFile)[],
    options?: Partial<ComputeUploadedFilenameOptions>
  ): Promise<UploadedFile[]> => {
    setLoading(true);
    return uploadFiles(files, options)
      .then((response) => {
        setData(response);
        return response;
      })
      .catch((error) => {
        setError(error);
        return error;
      });
  };

  return [upload, { loading, error, data }];
};
