import { Namespace, DefaultNamespace, UseTranslationOptions, useTranslation, TFunction } from 'react-i18next';
import { useDomaineActivite } from '../context';

export const useTrans = <N extends Namespace = DefaultNamespace>(
  ns?: N | Readonly<N>,
  options?: UseTranslationOptions
) => {
  const { code } = useDomaineActivite();
  const { t } = useTranslation(ns, options);
  return (keys: any, defaultValue?: string, options?: any) => {
    return t(keys, defaultValue, options ? { ...options, context: code } : { context: code });
  };
};
