import { useLocation } from 'react-router-dom';

export const useURLSearchParams = (): URLSearchParams => {
  return new URLSearchParams(useLocation().search);
};
