import { getUser } from './auth-util';

export const permitRoles = (roles: string[]) => {
  const user = getUser();
  if (user?.role?.code) {
    if (roles.includes(user.role.code)) return true;
  }
  return false;
};
