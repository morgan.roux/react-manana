import { identity, pickBy } from 'lodash';
import { MeUserInfoFragment, DomaineActiviteInfoFragment } from '../federation';

const ACCESS_TOKEN_KEY = 'access_token';
const GROUPEMENT_KEY = 'groupement';
const PHARMACIE_KEY = 'pharmacie';
const USER = 'user';
const IP_KEY = 'ip';
const REQUEST_CHOOSE_GROUPEMENT = 'request_choose_groupement';
const REQUEST_UPDATE_PASSWORD = 'request_update_password';
const REQUEST_UPDATE_PASSWORD_REASON = 'request_update_password_reason';
const DOMAINE_ACTIVITE = 'domaine_activite';

export const setRequestChooseGroupement = (requested: boolean) =>
  localStorage.setItem(REQUEST_CHOOSE_GROUPEMENT, `${requested ? 1 : 0}`);
export const isChooseGroupementRequested = (): boolean => localStorage.getItem(REQUEST_CHOOSE_GROUPEMENT) === '1';

export const setRequestUpdatePassword = (requested: boolean) =>
  localStorage.setItem(REQUEST_UPDATE_PASSWORD, `${requested ? 1 : 0}`);
export const isUpdatePasswordRequested = (): boolean => localStorage.getItem(REQUEST_UPDATE_PASSWORD) === '1';

export const setRequestUpdatePasswordReason = (reason: 'RESETED' | 'EXPIRED') =>
  localStorage.setItem(REQUEST_UPDATE_PASSWORD_REASON, reason);

export const getRequestUpdatePasswordReason = (): 'RESETED' | 'EXPIRED' =>
  localStorage.getItem(REQUEST_UPDATE_PASSWORD_REASON) as any;

export const setAccessToken = (token: string) => localStorage.setItem(ACCESS_TOKEN_KEY, token);
export const getAccessToken = (): string | null => localStorage.getItem(ACCESS_TOKEN_KEY);
export const isAuthenticated = (): boolean => !!localStorage.getItem(ACCESS_TOKEN_KEY);
export const setGroupement = (groupement: any) => localStorage.setItem(GROUPEMENT_KEY, JSON.stringify(groupement));
export const getGroupement = (): any => {
  const item = localStorage.getItem(GROUPEMENT_KEY);
  try {
    return item !== null ? JSON.parse(item) : null;
  } catch (error) {
    return null;
  }
};

export const setDomaineActivite = (domaine: DomaineActiviteInfoFragment) =>
  localStorage.setItem(DOMAINE_ACTIVITE, JSON.stringify(domaine));
export const getDomaineActivite = (): DomaineActiviteInfoFragment | null => {
  const item = localStorage.getItem(DOMAINE_ACTIVITE);
  try {
    return item !== null ? JSON.parse(item) : null;
  } catch (error) {
    return null;
  }
};

export const setPharmacie = (pharmacie: any) => {
  localStorage.setItem(PHARMACIE_KEY, JSON.stringify(pharmacie));
};

export const getPharmacie = () => {
  const item = localStorage.getItem(PHARMACIE_KEY);
  try {
    return item !== null ? JSON.parse(item) : null;
  } catch (error) {
    return null;
  }
};

export const setUser = (user: MeUserInfoFragment) => {
  const token = localStorage.getItem(ACCESS_TOKEN_KEY);
  if (token) {
    try {
      localStorage.setItem(USER, JSON.stringify({ ...user, accessToken: token }));
    } catch (error) {
      // Ignore
    }
  }
};

export const getUser = (): MeUserInfoFragment | null => {
  const token = localStorage.getItem(ACCESS_TOKEN_KEY);
  if (token) {
    try {
      const item = localStorage.getItem(USER);
      if (item) {
        const user = JSON.parse(item);
        if (user.accessToken === token) return user;
      }
    } catch (error) {}
  }

  return null;
};

export const clearLocalStorage = () => localStorage.clear();

export const setIp = (ip: string) => {
  localStorage.setItem(IP_KEY, ip);
};

export const getIp = () => {
  return localStorage.getItem(IP_KEY) || '';
};

export const getAuthHeaders = (token?: string) => {
  const user = getUser();

  const groupement = getGroupement();
  const pharmacie = getPharmacie();
  const localAccessToken = getAccessToken();
  const ip = getIp();
  const headerToken = token || localAccessToken;
  const prestataire = user?.prestataireService?.id;
  const domaineActivite = getDomaineActivite();

  const headers = {
    authorization: headerToken ? `Bearer ${headerToken}` : undefined,
    idGroupement: groupement?.id,
    idPharmacie: pharmacie?.id,
    groupement: groupement?.id,
    pharmacie: pharmacie?.id,
    prestataire,
    ip,
    'domaine-activite': domaineActivite?.id,
    idDomaineActivite: domaineActivite?.id,
  };

  return pickBy(headers, identity);
};
