import { ApolloClient, createHttpLink, InMemoryCache, InMemoryCacheConfig } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { getAuthHeaders } from './auth/auth-util';
import { mergeCacheConfigs } from './merge-util';
import generatedIntrospection from './federation';

export const createFederationClient = (
  uri?: string,
  token?: string,
  environment?: string,
  cacheConfigs?: InMemoryCacheConfig[]
) => {
  const httpLink = createHttpLink({
    uri,
  });

  const authLink = setContext((_, { headers }) => {
    return {
      headers: {
        ...headers,
        ...getAuthHeaders(token),
      },
    };
  });

  const mergeCacheConfig: InMemoryCacheConfig = mergeCacheConfigs(cacheConfigs || [], {
    possibleTypes: generatedIntrospection.possibleTypes,
  });

  return new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(mergeCacheConfig),
    connectToDevTools: 'production' !== environment,
  });
};
