import { useEffect } from 'react';
import { useIdleTimer, IdleTimerAPI } from 'react-idle-timer';
import { ApolloClient, useApolloClient } from '@apollo/client';
import { useCreate_One_User_Tracking_EventMutation, useCreate_One_User_Tracking_TimerMutation } from '../federation';
import { getScreenSize } from '../device-detector';
import { getAccessToken, getIp, getUser } from '../auth/auth-util';
import { useHistory } from 'react-router-dom';

export interface TrackingEvent {
  module?: string;
  page?: string;
  action?: string;
  type?: 'INFO' | 'ERROR';
  log?: Record<string, any>;
}

const __doUseTrackingEvent = (client: ApolloClient<any>): ((event: TrackingEvent) => void) => {
  const [create] = useCreate_One_User_Tracking_EventMutation({ client });
  const path = window.location.hash.substr(1);

  const notify = (event: TrackingEvent) => {
    const { width, height } = getScreenSize();
    create({
      variables: {
        input: {
          userTrackingEvent: {
            ...event,
            path,
            type: event.type || 'INFO',
            token: getAccessToken(),
            idUser: getUser()?.id,
            log: event.log ? (typeof event.log === 'string' ? event.log : JSON.stringify(event.log)) : undefined,
            screenWidth: width,
            screenHeight: height,
            userAgent: navigator.userAgent,
            ip: getIp(),
          },
        },
      },
    });
  };

  return notify;
};

export const useTrackingEvent = (providedClient?: ApolloClient<any>): ((event: TrackingEvent) => void) => {
  const contextClient = useApolloClient();
  const client = providedClient ?? contextClient;
  return __doUseTrackingEvent(client);
};

export interface TrackingTimer {
  module?: string;
  page?: string;
  type: 'IDLE' | 'ACTIVE' | 'ACTION';
  remainingTime?: number;
  elaspedTime?: number;
  lastIdleTime?: number;
  totalIdleTime?: number;
  lastActiveTime?: number;
  totalActiveTime?: number;
}

const __doUseTrackingTimer = (client: ApolloClient<any>): ((event: TrackingTimer) => void) => {
  const [create] = useCreate_One_User_Tracking_TimerMutation({ client });
  const path = window.location.hash.substr(1);

  const notify = (event: TrackingTimer) => {
    const { width, height } = getScreenSize();
    create({
      variables: {
        input: {
          userTrackingTimer: {
            ...event,
            path,
            token: getAccessToken(),
            idUser: getUser()?.id,
            screenWidth: width,
            screenHeight: height,
            userAgent: navigator.userAgent,
            ip: getIp(),
          },
        },
      },
    });
  };

  return notify;
};

export const useTrackingTimer = (providedClient?: ApolloClient<any>): ((event: TrackingTimer) => void) => {
  const contextClient = useApolloClient();
  const client = providedClient ?? contextClient;
  return __doUseTrackingTimer(client);
};

export const registerUserTrackingEvent = (providedClient: ApolloClient<any>) => {
  const history = useHistory();
  const create = __doUseTrackingEvent(providedClient);

  useEffect(() => {
    if (history) {
      return history.listen(() => {
        create({
          // module?: string,
          // page?: string;
          action: 'CHANGE_ROUTE',
          type: 'INFO',
          // log?: Record<string, any>;
        });
      });
    }
  }, [history]);

  return null;
};

export const registerUserTrackingTimer = (providedClient: ApolloClient<any>): IdleTimerAPI => {
  const create = __doUseTrackingTimer(providedClient);

  const register = (
    {
      getElapsedTime,
      getLastActiveTime,
      getLastIdleTime,
      getRemainingTime,
      getTotalActiveTime,
      getTotalIdleTime,
    }: IdleTimerAPI,
    type: 'IDLE' | 'ACTIVE'
  ) => {
    create({
      type,
      remainingTime: getRemainingTime(),
      elaspedTime: getElapsedTime(),
      lastIdleTime: getLastIdleTime(),
      totalIdleTime: getTotalIdleTime(),
      lastActiveTime: getLastActiveTime(),
      totalActiveTime: getTotalActiveTime(),
    });
  };

  const result = useIdleTimer({
    startOnMount: true,
    timeout: 600000,
    onActive: () => register(result, 'ACTIVE'),
    onIdle: () => register(result, 'IDLE'),
  });

  return result;
};
