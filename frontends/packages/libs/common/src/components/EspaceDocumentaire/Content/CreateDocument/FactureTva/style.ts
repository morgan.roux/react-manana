import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
  multiple: {
    marginBottom: theme.spacing(2),
    padding: theme.spacing(1),
    border: `1px solid ${theme.palette.text.secondary}`,
    borderRadius: 5,
    '& input': {
      marginBottom: theme.spacing(2),
    },
  },
  text: {
    fontSize: 14,
    fontFamily: 'Roboto',
    fontWeight: 'normal',
  },
  textType: {
    fontSize: 14,
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    minWidth: 100,
  },
  fieldInput: {
    '& input': {
      fontWeight: 'normal !important',
      fontSize: 14,
    },
  },
}));
