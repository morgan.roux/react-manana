import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
  form: {
    padding: '16px 0',
    width: '100%',
    '& [class*=MuiFormControl-root]': {
      marginBottom: theme.spacing(2),
    },
    '& [class*=MuiChip-root]': {
      marginRight: theme.spacing(2),
      backgroundColor: '#F5F6FA',
      '& svg': {
        color: theme.palette.primary.main,
      },
    },
  },
  multiple: {
    marginBottom: theme.spacing(2),
    padding: theme.spacing(1),
    border: `1px solid ${theme.palette.text.secondary}`,
    borderRadius: 5,
    '& input': {
      marginBottom: theme.spacing(2),
    },
  },
  flex: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    '&>div': {
      flex: '1 1 calc(50% - 16px)',
      maxWidth: 'calc(50% - 8px)',
    },
    '&>div:last-of-type': {
      marginLeft: theme.spacing(2),
    },
    '& .main-MuiInputBase-root': {
      height: '54px',
    },
  },
  selectionCollaborateur: {
    [theme.breakpoints.down('sm')]: {
      minWidth: '148px',
      marginBottom: '16px',
    },
  },
  numeroCommande: {
    '& .main-MuiFormControl-root': {
      display: 'flex',
    },
    '& .main-MuiFormControlLabel-root': {
      display: 'flex',
    },
  },
  caption: {
    display: 'inline-block',
    marginBottom: theme.spacing(1.5),
  },
  actions: {
    padding: theme.spacing(2),
    '& button': {
      margin: 'auto',
      minWidth: '25%',
    },
    boxShadow: '0 -2px 6px rgba(0,0,0,.16)',
  },
  endValidationBox: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  field: {
    width: '33%',
  },

  switchControl: {
    display: 'flex',
    justifyContent: 'space-between',
    [theme.breakpoints.down('sm')]: {
      '& .main-MuiTypography-root': {
        fontSize: '12px',
        fontWeight: 'bold',
      },
    },
  },
  formSaisisser: {
    '& .main-MuiFormControlLabel-root': {
      marginLeft: 'auto',
    },
  },
  joindreButton: {
    paddingTop: 8,
    paddingBottom: 8,
    paddingLeft: 24,
    paddingRight: 24,
  },
  preavisBox: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  preavisField: {
    display: 'flex',
    alignItems: 'center',
  },
  preavisText: {
    fontSize: 16,
    fontFamily: 'Roboto',
    textAlign: 'left',
  },
  jourPreavis: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    '& .main-MuiInputBase-root': {
      maxWidth: '50px',
      height: '40px',
      marginRight: '8px',
    },
    '& .main-MuiTypography-root': {
      marginTop: '8px',
    },
  },
  jourPreavisItems: {
    display: 'flex',
    justifyContent: 'center',
    '& .main-MuiTypography-root': {
      marginTop: '8px',
    },
  },
  checkTacite: {
    display: 'flex',
    '& .main-MuiTypography-root': {
      fontSize: '12px',
    },
  },
  dropzoneMobile: {
    display: 'flex',
    alignItems: 'center',
    '& main-MuiTypography-root': {
      marginTop: '-10px',
    },
  },
  photoCamRoot: {
    '& .MuiSvgIcon-root': {
      color: theme.palette.primary.main,
      borderWidth: '1px',
      borderStyle: 'solid',
      borderColor: theme.palette.primary.main,
      width: '40px',
      height: '40px',
      padding: '6px',
    },
  },
  photoCam: {
    width: '100%',
  },
  selectReglement: {
    '& .main-MuiInputBase-root': {
      height: '54px',
    },
  },
}));
