import React, { useState } from 'react';
import { Box, Typography } from '@material-ui/core';
import { useStyles } from './styles';
import ListDocument from './ListDocument';
import { Dropzone } from '@lib/common';

const FormDocument = () => {

  const classes = useStyles();

  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);

  return (
    <Box className={classes.root}>
      <Box className={classes.information}>
        <Box display="flex" flexDirection="row" py={1}>
          <Typography className={classes.informationTitle}>Type de document: </Typography>
          <Box px={2}>
            <Typography className={classes.bold}>Facture</Typography>
          </Box>
        </Box>
        <Box display="flex" flexDirection="row" py={1}>
          <Typography className={classes.informationTitle}>Espace: </Typography>
          <Box px={2}>
            <Typography className={classes.bold}>Compta</Typography>
          </Box>
        </Box>
        <Box display="flex" flexDirection="row" py={1}>
          <Typography className={classes.informationTitle}>Nombre de fichier ajouté: </Typography>
          <Box px={2}>
            <Typography className={classes.bold}>{selectedFiles.length}</Typography>
          </Box>
        </Box>
      </Box>
      <Box className={classes.formZone}>
        <Box>
          <Dropzone selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles} multiple />
        </Box>
        {selectedFiles.length > 0 && (
          <Box className={classes.fieldBorder}>
            {selectedFiles.map((file) => {
              return <ListDocument file={file} />
            })}
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default FormDocument;
