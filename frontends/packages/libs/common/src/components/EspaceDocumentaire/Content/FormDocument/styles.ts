import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        root: {
            fontSize: 18,
            fontFamily: 'Roboto',
            width: '100%',
        },
        information: {
            padding: '16px 0',
            borderBottom: '1px solid rgba(0,0,0,0.3)',
        },
        informationTitle: {
            opacity: '0.5',
        },
        formZone: {
            marginTop: 16,
        },
        fieldBorder: {
            border: '1px solid rgba(0,0,0,0.5)',
            borderStyle: 'dotted',
        },
        listField: {
            padding: 16,
            borderRadius: 5,
            ' & .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch'
            }
        },
        textField: {
            width: '100%',
        },
        field: {
            width: '33%',
        },
        bold: {
            fontWeight: 'bold',
        }
    }),
);