import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
  success: {
    padding: 50,
    textAlign: 'center',
    '& svg': {
      fontSize: 250,
    },
    '& p': {
      maxWidth: '60%',
      margin: 'auto',
    },
  },
  btn: {
    marginTop: theme.spacing(2),
    backgroundColor: '#F5F6FA',
  },
}));
