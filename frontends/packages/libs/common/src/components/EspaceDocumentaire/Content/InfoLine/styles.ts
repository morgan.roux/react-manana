import { makeStyles, Theme, createStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    infoSupContainer: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'center',
      marginTop: 5,
      padding: 10,
      // height:51,
      fontFamily:'Roboto',
      fontSize:16,
      backgroundColor: '#f8f8f8',
    },
    infoValue: {
      width: 'auto',
      marginRight: 10,
      cursor: 'pointer',
    },
    labelContent: {
      color: '#27272F',
    },
    labelContentValue: {
      fontWeight: 'bold',
      color: '#212121',
      marginLeft: theme.spacing(1),
    },
  }),
);

export default useStyles;
