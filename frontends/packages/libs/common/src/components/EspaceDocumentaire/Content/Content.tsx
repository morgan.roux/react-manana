import { Box, Button, IconButton, Menu, Tooltip, Typography, Chip, InputAdornment } from '@material-ui/core';
import { Delete, Edit, GetApp, MoreHoriz, Restore, RestoreFromTrash, Share } from '@material-ui/icons';
import style from './style';
import React, { Dispatch, FC, SetStateAction, useEffect, useState } from 'react';
import { DocumentCardMenu } from '../Sidebar/DocumentCardMenu';
import { IDocumentSearch } from '../EspaceDocumentaire';
import { ErrorPage, Loader, CustomModal, CustomFormTextField, CustomEditorText, NoItemContentImage } from '@lib/ui-kit';
import Alert from '@material-ui/lab/Alert';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

import classNames from 'classnames';
import { MagnifierIcon } from '../Sidebar/DocumentCard/MagnifierIcon/MagnifierIcon';
import { InfoLine } from './InfoLine/InfoLine';
import CreateDocument from './CreateDocument';
import ReplyIcon from '@material-ui/icons/Reply';

import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

// import mimeTypes from 'mime-types';
import { Smyley, useSmyleysQuery, useUser_SmyleysLazyQuery } from '@lib/common/src/graphql';
import {
  FileViewer,
  parseParameterValueAsBoolean,
  parseParameterValueAsString,
  useApplicationContext,
  UserAction,
} from '@lib/common';
import {
  useOcr_DocumentsLazyQuery,
  useRestoreGedDocumentMutation,
  useReviserGedDocumentMutation,
  useSend_Ged_Document_By_EmailMutation,
  useUpdate_Document_StatusMutation,
  FullDocumentCategorieFragment,
} from '@lib/common/src/federation';
import moment from 'moment';
import { Comment, CommentList } from '@lib/common';
import { useHistory } from 'react-router-dom';

const STATUT_LABELS = {
  EN_ATTENTE_APPROBATION: 'En attente',
  REFUSE: 'Rejeté',
  APPROUVE: 'Validé',
  APPROUVE_PAYE: 'Payé',
  REFUSE_COMPTABLE: 'Validé',
  APPROUVE_COMPTABLE: 'Validé',
  APPROUVE_COMPTABLE_PAYE: 'Payé',
  PAYE: 'Payé',
};
const STATUT_LABELS_COMPTABLE = {
  APPROUVE_COMPTABLE: 'Comptabilisé',
  APPROUVE_COMPTABLE_PAYE: 'Comptabilisé',
  REFUSE_COMPTABLE: 'Rejeté',
};
export interface IContent {
  content: {
    loading: boolean;
    error: Error;
    data: FullDocumentCategorieFragment;
  };
  commentaires: {
    loading: boolean;
    error: Error;
    data: any;
  };
  documentIds?: string[];
  saving: boolean;
  saved: boolean;
  setSaved: (newValue: boolean) => void;
  activeSousCategorie: any | undefined;
  activeDocument: any | undefined;
  onRequestCreateDocument: (document: any) => void;
  onRequestAddToFavorite: (document: any) => void;
  onRequestDelete: (document: any) => void;
  onRequestRemoveFromFavorite: (document: any) => void;
  onRequestReplace: (document: any) => void;
  onRequestFetchMoreComments: (document: FullDocumentCategorieFragment) => void;
  onRequestLike: (document: FullDocumentCategorieFragment, smyley: Smyley | null) => void;
  onRequestDeplace: (document: FullDocumentCategorieFragment) => void;
  onCompleteDownload: (document: FullDocumentCategorieFragment) => void;
  handleFilterMenu(): void;
  handleSidebarMenu(): void;
  refetchComments: () => void;
  mobileView: boolean;
  tabletView: boolean;
  handleFilterMenu(): void;
  handleSidebarMenu(): void;
  refetchSmyleys: () => void;
  hidePartage?: boolean;
  hideRecommandation?: boolean;
  onRequestValider?: (id: string) => void;
  onRequestRefuser?: (id: string) => void;
  validation?: boolean;
  refetchDocument?: () => void;
  onRequestDocument: (id: string) => void;
  onRequestFetchAll: (withSearch: boolean) => void;
  searchVariables?: IDocumentSearch;
  setSearchVariables?: Dispatch<SetStateAction<IDocumentSearch>>;
  noValidation?: boolean;

  espaceType?: string;
  tvas: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  defaultCorrespondant?: any;
}

const Content: FC<IContent> = ({
  onRequestAddToFavorite,
  onRequestDelete,
  onRequestRemoveFromFavorite,
  onRequestReplace,
  onRequestFetchMoreComments,
  onRequestDeplace,
  refetchComments,
  refetchSmyleys,
  onCompleteDownload,
  saving,
  saved,
  setSaved,
  content,
  commentaires,
  validation,
  activeDocument,
  activeSousCategorie,
  onRequestValider,
  onRequestRefuser,
  documentIds,
  onRequestDocument,
  searchVariables,
  onRequestFetchAll,
  setSearchVariables,
  noValidation = false,

  espaceType,
  tvas,
  defaultCorrespondant,
}) => {
  const commentInputRef = React.useRef<HTMLInputElement>();
  const history = useHistory();

  const classes = style();
  const {
    isMobile,
    user,
    federation,
    graphql,
    currentGroupement,
    notify,
    auth,
    useParameterValueAsBoolean,
    useParameterValueAsString,
    config,
    parameters,
  } = useApplicationContext();
  const enableSendGedEmailGroupement = useParameterValueAsBoolean('0826');
  const enableSendGedEmailPharmacie = useParameterValueAsBoolean('0726');
  const enableValidationPrestataire = parseParameterValueAsBoolean(parameters, '0850');
  const idPrestataireValidateur = parseParameterValueAsString(parameters, '0851');
  const enableOCRisation = useParameterValueAsBoolean('0839');
  const enableWorkflowPaye = useParameterValueAsBoolean('0852');
  // const withPrestataireValidateur = enableValidationPrestataire && !!idPrestataireValidateur

  const [option, setOption] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [downloading, setDownloading] = useState<boolean>(false);
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [documentIdsCurrent, setDocumentIdsCurrent] = useState<string[] | undefined>();
  const [page, setPage] = useState<number>(0);
  const [openReplaceDocument, setOpenReplaceDocument] = useState<boolean>(false);

  const changementStatuts = activeDocument?.changementStatuts || [];
  const changementStatutValidationInterne = changementStatuts.find(
    ({ createdBy }: any) => !createdBy.prestataireService
  );
  const changementStatutValidationPrestataireValidateur = changementStatuts.find(
    ({ createdBy }: any) => createdBy.prestataireService
  );

  const dernierStatus = activeDocument?.dernierChangementStatut?.status;

  const changementStatut = (status: string) => {
    return changementStatuts.find((changement: any) => changement.status === status);
  };

  const isFactureValid = (document: any) => {
    return (
      document &&
      document.type === 'FACTURE' &&
      document.idOrigine &&
      document.idOrigineAssocie &&
      document.factureDate &&
      document.factureTotalHt
    );
  };

  useEffect(() => {
    // if the user have an acces to validate the doc
    if (validation) {
      setIsOpen(true);
    }
  }, []);

  useEffect(() => {
    setDocumentIdsCurrent(documentIds);
    if (documentIds && content.data) {
      setPage(documentIds?.indexOf(content.data.id || ''));
    }
  }, [documentIds, content.data]);

  useEffect(() => {
    if (saved) {
      setOpenReplaceDocument(false);
      // setSaved(false);
    }
  }, [saved]);

  const handlePrev = () => {
    if (documentIdsCurrent && page >= 1) {
      onRequestDocument(documentIdsCurrent[page - 1]);
      setPage(page - 1);
    }
  };
  const handleNext = () => {
    if (documentIdsCurrent && page < documentIdsCurrent.length - 1) {
      onRequestDocument(documentIdsCurrent[page + 1]);
      setPage(page + 1);
    }
  };

  const handleToggleOption = (event: React.MouseEvent) => {
    setOption((prev) => !prev);
    setAnchorEl(event.currentTarget as HTMLElement);
  };

  const changeStatutDocument = (
    statut:
      | 'REFUSE'
      | 'APPROUVE'
      | 'PAYE'
      | 'POUBELLE'
      | 'APPROUVE_COMPTABLE'
      | 'REFUSE_COMPTABLE'
      | 'APPROUVE_PAYE'
      | 'APPROUVE_COMPTABLE_PAYE'
  ): void => {
    if ((statut === 'REFUSE' || statut === 'REFUSE_COMPTABLE') && (commentaires?.data?.total || 0) > 0) {
      updateDocumentStatut({
        variables: {
          input: {
            idDocument: activeDocument.id as any,
            commentaire: '',
            status: statut,
          },
        },
      }).then(() => {
        setSearchVariables &&
          setSearchVariables((prev) => ({
            ...prev,
            type: 'FACTURE',
            accesRapide: ['EN_ATTENTE_APPROBATION'],
          }));
      });
    } else if ((statut === 'REFUSE' || statut === 'REFUSE_COMPTABLE') && commentaires?.data?.total === 0) {
      notify({
        message: 'Le commentaire est obligatoire en cas de refus de document.',
        type: 'error',
      });
      commentInputRef.current?.focus();
    } else if (statut === 'APPROUVE') {
      handleModifierDocument(statut);
    } else if (statut === 'PAYE' || statut === 'APPROUVE_PAYE' || statut === 'APPROUVE_COMPTABLE_PAYE') {
      if (activeDocument.type === 'FACTURE' && !isFactureValid(activeDocument)) {
        notify({
          message: `Veuillez remplir tous les champs obligatoires avant de payer la facture`,
          type: 'error',
        });
        handleModifierDocument(statut);
        return;
      }

      updateDocumentStatut({
        variables: {
          input: {
            idDocument: activeDocument.id as any,
            commentaire: '',
            status: statut,
          },
        },
      }).then(() => {
        setSearchVariables &&
          setSearchVariables((prev) => ({
            ...prev,
            type: 'FACTURE',
            accesRapide: ['EN_ATTENTE_APPROBATION'],
          }));
      });
    } else if (statut === 'POUBELLE' || statut === 'APPROUVE_COMPTABLE') {
      updateDocumentStatut({
        variables: {
          input: {
            idDocument: activeDocument.id as any,
            commentaire: '',
            status: statut,
          },
        },
      }).then(() => {
        setSearchVariables &&
          setSearchVariables((prev) => ({
            ...prev,
            type: 'FACTURE',
            accesRapide: ['EN_ATTENTE_APPROBATION'],
          }));
      });
    }
  };

  const handleClose = () => {
    setOption(false);
    setAnchorEl(null);
  };

  const downloadDocument = (documentToDownload: FullDocumentCategorieFragment): void => {
    const { publicUrl, nomOriginal } = documentToDownload.fichier || {};
    if (publicUrl && nomOriginal) {
      setDownloading(true);
      fetch(publicUrl)
        .then((response) => response.blob())
        .then((blob) => {
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(new Blob([blob]));
          downloadLink.download = nomOriginal;
          downloadLink.click();

          onCompleteDownload(documentToDownload);
          setDownloading(false);
        })
        .catch((error) => {
          notify({
            type: 'error',
            message: "Une erreur s'est produite lors de téléchargement du fichier",
          });
          setDownloading(false);
        });
    }
  };

  const [sendGedDocument, sendingGedDocument] = useSend_Ged_Document_By_EmailMutation({
    client: federation,
  });

  /**Change Status  */
  const [updateDocumentStatut, updatingDocumentStatut] = useUpdate_Document_StatusMutation({
    onCompleted: () => {
      notify({
        message: 'Le statut du document a été changé avec succès!',
        type: 'success',
      });

      onRequestFetchAll(true);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant le changement du statut du document!',
        type: 'error',
      });
    },
    client: federation,
  });

  /**get validation document */

  const getSmyleys = useSmyleysQuery({
    client: graphql,
    variables: {
      idGroupement: currentGroupement.id,
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });
  const [loadUserSmyleys, getUserSmyleys] = useUser_SmyleysLazyQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
  });

  const [restoreDocument] = useRestoreGedDocumentMutation({
    onCompleted: () => {
      notify({
        message: 'succès!',
        type: 'success',
      });

      onRequestFetchAll(true);
    },
    onError: (err) => {
      notify({
        message: `Des erreurs se sont survenues pendant la restauration de la facture: ${err.message}`,
        type: 'error',
      });
    },
  });
  const [reviserDocument] = useReviserGedDocumentMutation({
    onCompleted: () => {
      notify({
        message: 'succès!',
        type: 'success',
      });

      onRequestFetchAll(true);
    },
    onError: (err) => {
      notify({
        message: `Des erreurs se sont survenues pendant la révision de la facture: ${err.message}`,
        type: 'error',
      });
    },
  });
  const smyleys = getSmyleys?.data?.smyleys || [];

  /**
   * Handle open modal send email
   */
  const [openModal, setOpenModal] = useState<boolean>(false);
  const handleOpenModal = () => {
    setOpenModal(!openModal);
  };

  const handleModifierDocument = (status?: string) => {
    if ('espace-facture' === espaceType && activeDocument?.id)
      history.push(`/espace-facturation/form?id=${activeDocument.id}&status=${status}`);
    else {
      setOpenReplaceDocument(true);
      setSaved(false);
    }
  };

  const handleRestaurerFacture = () => {
    if (activeDocument.id) {
      restoreDocument({
        variables: {
          idDocument: activeDocument.id as any,
        },
      });
    }
  };
  const handleReviserFacture = () => {
    if (activeDocument.id) {
      reviserDocument({
        variables: {
          idDocument: activeDocument.id as any,
        },
      });
    }
  };

  /*
   * Send email ************************************************************************************
   */
  const [emailReceivers, setEmailReceivers] = React.useState<string[]>([]);
  const [emailHtml, setEmailHtml] = React.useState<any>('');

  const [messageSubmitted, setMessageSubmitted] = React.useState<{
    error: boolean;
    success: boolean;
    message: string | null | undefined;
  }>({
    error: false,
    success: false,
    message: '',
  });

  const [loadOcrDocument] = useOcr_DocumentsLazyQuery({
    onCompleted: () => {
      notify({
        message: "L'OCR de ce document a été effectué avec succès !",
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'OCR du document !",
        type: 'error',
      });
    },
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const handleChangeMotCle = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const { value } = event.target as HTMLInputElement;
    const temp: string[] = [];
    const input: string[] = value.split(',');
    input.forEach((motCle: string) => temp.push(motCle));

    setEmailReceivers(temp);
  };

  const handleDeleteMotsCle = (index: number) => (_: React.MouseEvent) => {
    if (typeof emailReceivers === 'object') {
      emailReceivers.splice(index, 1);
      setEmailReceivers([...emailReceivers]);
    } else setEmailReceivers(['']);
  };

  const handleSubmitSendEmail = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault();
    const emailRegex = /^([A-Za-z0-9_\-.+])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,})$/;
    let i = 0;
    emailReceivers.forEach((element: string) => {
      if (!emailRegex.test(element.trim())) {
        i++;
        setMessageSubmitted({ error: true, success: false, message: 'Email invalide' });
      }
    });
    if (i == 0) {
      const document = (documentToDownload: FullDocumentCategorieFragment): { url: string; name: string } => {
        const { publicUrl, nomOriginal } = documentToDownload.fichier || {};
        if (publicUrl && nomOriginal) {
          return { url: publicUrl, name: nomOriginal };
        }
        return { url: '', name: '' };
      };

      sendGedDocument({
        variables: {
          input: {
            to: emailReceivers,
            html: emailHtml,
            attachment: {
              content: document(content.data).url,
              filename: document(content.data).name,
              type: 'application/pdf',
            },
          },
        },
      })
        .then((response) => {
          let message = response.data?.sendGEDDocumentByEmail?.message;
          if (message == undefined || message == null) {
            message = 'Une erreur est survenue';
          }
          if (response.data?.sendGEDDocumentByEmail?.code == '200') {
            setMessageSubmitted({ error: false, success: true, message: message });
            setEmailReceivers([]);
          } else {
            setMessageSubmitted({ error: true, success: false, message: message });
          }
        })
        .catch(() => {
          setMessageSubmitted({ error: true, success: false, message: 'Une erreur est survenue' });
        });
    }
  };

  const handleOcR = () => {
    if (activeDocument?.id) {
      loadOcrDocument({
        variables: {
          idDocument: activeDocument?.id,
        },
      });
    }
  };

  /**Validation authorization */
  const isAuthorizedToValidateDocument = (document: any) => {
    if (document.type === 'FACTURE') {
      if (
        enableValidationPrestataire &&
        idPrestataireValidateur &&
        user?.prestataireService?.id &&
        idPrestataireValidateur === user?.prestataireService?.id
      ) {
        return true;
      }
      return auth.isAuthorizedToValidateGedDocument();
    }

    if (
      document.sousCategorie?.workflowValidation === 'INTERNE' ||
      document.categorie?.workflowValidation === 'INTERNE'
    ) {
      return auth.isAuthorizedToValidateGedDocument();
    }
    if (
      document.sousCategorie?.workflowValidation === 'UTILISATEUR' ||
      document.categorie?.workflowValidation === 'UTILISATEUR'
    ) {
      return true;
    }

    if (
      document.sousCategorie?.workflowValidation === 'PARTENAIRE_SERVICE' ||
      document.categorie?.workflowValidation === 'PARTENAIRE_SERVICE'
    ) {
      return (
        document.sousCategorie?.idPartenaireValidateur === user?.prestataireService?.id ||
        document.categorie?.idPartenaireValidateur === user?.prestataireService?.id
      );
    }
    return false;
  };

  /********************************************************************************************/

  React.useEffect(() => {
    if (activeDocument?.id) {
      loadUserSmyleys({
        variables: {
          codeItem: 'GED_DOCUMENT',
          idSource: activeDocument.id,
        },
      });
    }
  }, [activeDocument?.id]);

  const theme = useTheme();
  const otherDivice = useMediaQuery(theme.breakpoints.up('sm'));
  return (
    <>
      <Box
        display="flex"
        flexDirection="column"
        flex={1}
        // style={
        //   'CONTRAT' === activeDocument?.type
        //     ? undefined
        //     : {
        //         height: 'calc(100vh - 86px)',
        //       }
        // }
        className={classNames(classes.root, { [classes.hidescrollBarContent]: isMobile })}
      >
        {activeDocument ? (
          content.loading ? (
            <Loader />
          ) : !content.error && content.data ? (
            <Box
              className={classNames(classes.content, {
                [classes.hidescrollBarContent]: isMobile,
              })}
            >
              <Box className={classes.header}>
                <h2>
                  <Typography
                    // color="primary"
                    style={{ fontSize: '18px', fontFamily: 'Roboto', fontWeight: 600 }}
                    component="span"
                    dangerouslySetInnerHTML={{ __html: content.data.description || '' }}
                  />
                </h2>
                <Box className={classes.actionHeader}>
                  {/* <IconButton style={{ marginRight: 16 }} size="small" onClick={() => downloadDocument(content?.data)}>
                    <GetApp />
                  </IconButton> */}
                  <Box className={classes.pagination}>
                    <IconButton size="small" onClick={handlePrev}>
                      <ChevronLeftIcon />
                    </IconButton>
                    <IconButton size="small" onClick={handleNext}>
                      <ChevronRightIcon />
                    </IconButton>
                  </Box>
                  <IconButton size="small" onClick={handleToggleOption}>
                    <MoreHoriz />
                  </IconButton>
                  {/* {isMobile ? (
                    <IconButton
                      style={{ marginRight: 16 }}
                      size="small"
                      onClick={() => downloadDocument(content?.data)}
                    >
                      <GetApp />
                    </IconButton>
                  ) : (
                    <Box className={classes.pagination}>
                      <IconButton size="small" onClick={handlePrev}>
                        <ChevronLeftIcon />
                      </IconButton>
                      <IconButton size="small" onClick={handleNext}>
                        <ChevronRightIcon />
                      </IconButton>
                    </Box>
                  )}
                  <IconButton size="small" onClick={handleToggleOption}>
                    <MoreHoriz />
                  </IconButton> */}
                </Box>
              </Box>
              <Box marginBottom="25px">
                <InfoLine
                  items={[
                    {
                      label: 'Créé par',
                      value: (activeDocument?.createdBy?.fullName || '').split(' ')[0],
                    },
                    {
                      label: 'Origine',
                      value: activeDocument?.origineAssocie?.nom || activeDocument?.origineAssocie?.fullName,
                    },
                    {
                      label: 'Statut',
                      value:
                        'FACTURE' === activeDocument?.type || isAuthorizedToValidateDocument(activeDocument)
                          ? dernierStatus && dernierStatus !== 'EN_ATTENTE_APPROBATION'
                            ? `${(STATUT_LABELS as any)[dernierStatus]} par ${(changementStatutValidationInterne?.createdBy?.fullName || '').split(' ')[0]
                            }`
                            : STATUT_LABELS.EN_ATTENTE_APPROBATION
                          : undefined,
                    },
                    {
                      label: 'Validation comptable',
                      value:
                        ('FACTURE' === activeDocument?.type || isAuthorizedToValidateDocument(activeDocument)) &&
                          // enableValidationPrestataire &&
                          (dernierStatus === 'APPROUVE_COMPTABLE' ||
                            dernierStatus === 'APPROUVE_COMPTABLE_PAYE' ||
                            dernierStatus === 'PAYE' ||
                            dernierStatus === 'REFUSE_COMPTABLE')
                          ? `${(STATUT_LABELS_COMPTABLE as any)[dernierStatus]} par ${(
                            (dernierStatus === 'REFUSE_COMPTABLE'
                              ? changementStatut('REFUSE_COMPTABLE')
                              : changementStatut('APPROUVE_COMPTABLE') || changementStatut('APPROUVE_COMPTABLE_PAYE')
                            )?.createdBy?.fullName || ''
                          ).split(' ')[0]
                          }`
                          : undefined,
                    },
                  ]}
                />
                <InfoLine
                  items={[
                    {
                      label: 'Facture n°',
                      value: activeDocument.type === 'FACTURE' ? activeDocument?.numeroFacture : undefined,
                    },
                    {
                      label: 'Montant TTC',
                      value:
                        activeDocument.type === 'FACTURE' && activeDocument?.factureTotalTtc
                          ? `${(activeDocument?.factureTotalTtc).toFixed(2)} €`
                          : undefined,
                    },
                    {
                      label: 'Mode règlement',
                      value:
                        activeDocument.type === 'FACTURE' && activeDocument?.reglementMode?.libelle
                          ? activeDocument?.reglementMode?.libelle
                          : undefined,
                    },
                    {
                      label: 'Date signature',
                      value:
                        'CONTRAT' === activeDocument.type && activeDocument?.dateHeureParution
                          ? moment(activeDocument?.dateHeureParution).format('DD/MM/YYYY')
                          : undefined,
                    },
                    {
                      label: 'Date début',
                      value:
                        'CONTRAT' === activeDocument.type && activeDocument?.dateHeureDebutValidite
                          ? moment(activeDocument?.dateHeureDebutValidite).format('DD/MM/YYYY')
                          : undefined,
                    },
                    {
                      label: 'Date fin',
                      value:
                        'CONTRAT' === activeDocument.type && activeDocument?.dateHeureFinValidite
                          ? moment(activeDocument?.dateHeureFinValidite).format('DD/MM/YYYY')
                          : undefined,
                    },
                    {
                      label: 'Renouvellement par tacite',
                      value:
                        'CONTRAT' === activeDocument.type
                          ? activeDocument?.isRenouvellementTacite
                            ? 'Oui'
                            : 'Non'
                          : undefined,
                    },
                    {
                      label: 'Nombre de jour de préavis',
                      value:
                        'CONTRAT' === activeDocument.type && activeDocument?.nombreJoursPreavis
                          ? `${activeDocument?.nombreJoursPreavis} jour${activeDocument?.nombreJoursPreavis > 1 ? 's' : ''
                          }`
                          : undefined,
                    },
                  ]}
                />
              </Box>
              {!isMobile && otherDivice ? (
                <Box className={classes.actionBtnContainer}>
                  <Box className={classes.actionDocBtn}>
                    <Button
                      onClick={() => downloadDocument(content?.data)}
                      variant="contained"
                      color="secondary"
                      startIcon={<GetApp />}
                      style={{ cursor: 'pointer', padding: '11px 34px' }}
                      disabled={downloading}
                    >
                      Télécharger
                    </Button>
                    {enableSendGedEmailGroupement && enableSendGedEmailPharmacie && (
                      <Button
                        onClick={handleOpenModal}
                        variant="contained"
                        color="secondary"
                        startIcon={<Share />}
                        style={{ cursor: 'pointer', marginLeft: 5, padding: '11px 34px' }}
                      >
                        Envoyer par email
                      </Button>
                    )}

                    {enableOCRisation && !activeDocument?.isOcr && (
                      <Box pl={1}>
                        <Tooltip title="OCR-iser le document">
                          <IconButton onClick={handleOcR}>
                            <MagnifierIcon />
                          </IconButton>
                        </Tooltip>
                      </Box>
                    )}
                  </Box>
                  {((!user?.prestataireService?.id && !dernierStatus) ||
                    ((dernierStatus === 'APPROUVE' || dernierStatus === 'APPROUVE_PAYE') &&
                      (!enableValidationPrestataire || !!user?.prestataireService?.id))) &&
                    isAuthorizedToValidateDocument(activeDocument) &&
                    !noValidation && (
                      <Box className={classes.listActionValidation}>
                        <Box className={classes.btnValidation}>
                          {'espace-facture' === espaceType && !dernierStatus && (
                            <Tooltip title="Poubelle">
                              <Button
                                variant="text"
                                color="secondary"
                                disabled={updatingDocumentStatut.loading}
                                onClick={() => {
                                  changeStatutDocument('POUBELLE');
                                }}
                                startIcon={<Delete style={{ fontSize: 27 }} />}
                              />
                            </Tooltip>
                          )}
                          <Button
                            style={{ marginLeft: '5%', height: 37 }}
                            variant="outlined"
                            color="secondary"
                            disabled={updatingDocumentStatut.loading}
                            onClick={() => {
                              changeStatutDocument(dernierStatus ? 'REFUSE_COMPTABLE' : 'REFUSE');
                            }}
                          >
                            Rejeter
                          </Button>
                          <Button
                            style={{ marginLeft: '5%', height: 37 }}
                            variant="outlined"
                            color="secondary"
                            disabled={updatingDocumentStatut.loading}
                            onClick={() => {
                              changeStatutDocument(
                                dernierStatus
                                  ? dernierStatus === 'APPROUVE_PAYE'
                                    ? 'APPROUVE_COMPTABLE_PAYE'
                                    : 'APPROUVE_COMPTABLE'
                                  : 'APPROUVE'
                              );
                            }}
                          >
                            {dernierStatus ? 'Comptabiliser' : 'Valider'}
                          </Button>
                        </Box>
                      </Box>
                    )}
                  {!user?.prestataireService?.id &&
                    (dernierStatus === 'APPROUVE_COMPTABLE' || dernierStatus === 'APPROUVE') &&
                    isAuthorizedToValidateDocument(activeDocument) &&
                    !noValidation && (
                      <Box
                        className={
                          dernierStatus === 'APPROUVE' && !enableValidationPrestataire
                            ? undefined
                            : classes.listActionValidation
                        }
                      >
                        <Box className={classes.btnValidation}>
                          <Button
                            style={{ marginLeft: '5%', height: 37 }}
                            variant="outlined"
                            color="secondary"
                            disabled={updatingDocumentStatut.loading}
                            onClick={() => {
                              changeStatutDocument(
                                enableWorkflowPaye
                                  ? 'PAYE'
                                  : dernierStatus === 'APPROUVE_COMPTABLE'
                                    ? 'APPROUVE_COMPTABLE_PAYE'
                                    : 'APPROUVE_PAYE'
                              );
                            }}
                          >
                            Payer
                          </Button>
                        </Box>
                      </Box>
                    )}

                  {!user?.prestataireService?.id &&
                    content.data.type === 'FACTURE' &&
                    !content.data.factureLignes?.length &&
                    (!dernierStatus || dernierStatus === 'EN_ATTENTE_APPROBATION') &&
                    !noValidation ? (
                    <Box>
                      <Button
                        onClick={() => handleModifierDocument()}
                        style={{ marginLeft: 8 }}
                        variant="contained"
                        color="secondary"
                      >
                        <Edit />
                      </Button>
                    </Box>
                  ) : null
                  }
                  {!user?.prestataireService?.id &&
                    content.data.type === 'FACTURE' &&
                    dernierStatus &&
                    dernierStatus !== 'POUBELLE' &&
                    isAuthorizedToValidateDocument(activeDocument) &&
                    !noValidation && (
                      <Box
                        className={
                          !dernierStatus ||
                            dernierStatus === 'EN_ATTENTE_APPROBATION' ||
                            (dernierStatus === 'APPROUVE_PAYE' &&
                              (!enableValidationPrestataire || !!user?.prestataireService?.id)) ||
                            dernierStatus === 'APPROUVE' ||
                            dernierStatus === 'APPROUVE_COMPTABLE'
                            ? undefined
                            : classes.listActionValidation
                        }
                      >
                        <Tooltip title="Réviser">
                          <Button
                            onClick={handleReviserFacture}
                            style={{ marginLeft: 8 }}
                            variant="contained"
                            color="secondary"
                          >
                            <Restore />
                          </Button>
                        </Tooltip>
                      </Box>
                    )}
                  {!user?.prestataireService?.id &&
                    content.data.type === 'FACTURE' &&
                    dernierStatus === 'POUBELLE' &&
                    isAuthorizedToValidateDocument(activeDocument) &&
                    !noValidation && (
                      <Box className={classes.listActionValidation}>
                        <Button
                          onClick={handleRestaurerFacture}
                          style={{ marginLeft: 8 }}
                          variant="outlined"
                          color="secondary"
                          startIcon={<RestoreFromTrash />}
                        >
                          Restaurer
                        </Button>
                      </Box>
                    )}
                </Box>
              ) : (
                <Box className={classes.actionBtnContainerMobile}>
                  <Box className={classes.actionDocBtn}>
                    <Button
                      onClick={() => downloadDocument(content?.data)}
                      variant="contained"
                      color="secondary"
                      startIcon={<GetApp style={{ marginLeft: '8px' }} />}
                      style={{ cursor: 'pointer', padding: '11px 34px', width: '10%' }}
                      disabled={downloading}
                    />
                    <Button
                      onClick={handleOpenModal}
                      variant="contained"
                      color="secondary"
                      startIcon={<ReplyIcon style={{ transform: 'scaleX(-1)', marginLeft: '8px' }} />}
                      style={{ cursor: 'pointer', marginLeft: 5, padding: '11px 34px', width: '10%' }}
                    />
                  </Box>
                  <Button
                    onClick={() => handleModifierDocument()}
                    style={{ marginLeft: 8, width: '10%', height: '43px' }}
                    variant="contained"
                    color="secondary"
                  >
                    <Edit />
                  </Button>
                </Box>
              )}
              {content?.data?.fichier?.publicUrl && (
                <FileViewer config={config} style={{ maxHeight: '100%' }} fileUrl={content.data.fichier.publicUrl} />
              )}

              {activeDocument && (
                <UserAction
                  refetchSmyleys={refetchSmyleys}
                  codeItem="GED_DOCUMENT"
                  idSource={activeDocument.id}
                  nbComment={activeDocument.nombreCommentaires}
                  nbSmyley={activeDocument.nombreReactions}
                  userSmyleys={getUserSmyleys.data?.userSmyleys as any}
                  smyleys={smyleys}
                  hidePartage={true}
                  hideRecommandation={true}
                />
              )}
              {commentaires.error ? (
                <ErrorPage />
              ) : (
                <CommentList
                  key={`comment_list`}
                  total={commentaires.data?.total || 0}
                  comments={commentaires.data?.data || []}
                  loading={commentaires.loading}
                  fetchMoreComments={() => onRequestFetchMoreComments as any}
                />
              )}
              {!commentaires.loading && (
                <Comment
                  forwardedInputRef={commentInputRef as any}
                  refetch={refetchComments}
                  codeItem="GED_DOCUMENT"
                  idSource={content.data?.id as any}
                />
              )}
            </Box>
          ) : (
            <ErrorPage />
          )
        ) : (
          <NoItemContentImage
            title="Détails de document"
            subtitle="Veuilez cliquer sur un document de la partie gauche ou bien même en ajouter un pour voir les détails dans cette partie"
          />
        )}
      </Box>
      <Menu
        open={option}
        onClose={handleClose}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <DocumentCardMenu
          isOpenDetail={isOpen}
          document={content.data}
          setOpenOptions={setOption}
          saving={saving}
          saved={saved}
          setSaved={setSaved}
          onRequestAddToFavorite={onRequestAddToFavorite}
          onRequestDelete={onRequestDelete}
          onRequestRemoveFromFavorite={onRequestRemoveFromFavorite}
          onRequestReplaceDocument={onRequestReplace}
          onRequestDeplace={onRequestDeplace}
          onRequestRefuser={onRequestRefuser}
          onRequestValider={onRequestValider}
          searchVariables={searchVariables}
          activeSousCategorie={activeSousCategorie}
          espaceType={espaceType}
          tvas={tvas}
          defaultCorrespondant={defaultCorrespondant}
        />
      </Menu>
      <CustomModal
        open={openModal}
        setOpen={handleOpenModal}
        title="Envoyer par email"
        withBtnsActions={false}
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="lg"
      >
        <form onSubmit={handleSubmitSendEmail}>
          {/* <AlertDonneesMedicales style={{ marginBottom: 10 }} /> */}
          <Box my={3}>
            <CustomFormTextField
              required
              placeholder="Email"
              value={emailReceivers}
              onChange={handleChangeMotCle}
              variant="standard"
              InputProps={{
                startAdornment: <InputAdornment position="start">A:</InputAdornment>,
              }}
            />
            <Box>
              {typeof emailReceivers === 'object' && emailReceivers.length
                ? emailReceivers.map((element: string, index: number) => (
                  <Chip
                    size="small"
                    label={element}
                    onDelete={handleDeleteMotsCle(index)}
                    key={index}
                    style={{ marginLeft: 2, marginBottom: 2 }}
                  />
                ))
                : null}
            </Box>
            <Box>
              <CustomEditorText value={emailHtml} onChange={setEmailHtml} />
            </Box>
          </Box>
          <Box my={3}>
            {messageSubmitted.error && <Alert severity="error">{messageSubmitted.message}</Alert>}
            {messageSubmitted.success && <Alert severity="success">{messageSubmitted.message}</Alert>}
            <Button
              variant="contained"
              color="secondary"
              type="submit"
              style={{ marginLeft: 25 }}
              disabled={sendingGedDocument.loading}
            >
              Envoyer
            </Button>
          </Box>
        </form>
      </CustomModal>
      <CreateDocument
        open={openReplaceDocument}
        setOpen={setOpenReplaceDocument}
        title={'espace-facture' === espaceType ? 'Modification de la facture' : 'Modification de document'}
        saving={saving}
        activeSousCategorie={activeSousCategorie}
        document={content.data}
        mode="remplacement"
        onRequestCreateDocument={onRequestReplace as any}
        searchVariables={searchVariables}
        defaultType={'espace-facture' === espaceType ? 'FACTURE' : undefined}
        tvas={tvas}
      />
    </>
  );
};

export default Content;
