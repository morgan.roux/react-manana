import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
  appBar: {
    backgroundColor: theme.palette.common.white,
    borderBottom: '1px solid #E0E0E0',
  },
  tooltip: {
    padding: theme.spacing(1, 2),
  },
  contentPartageRecommandation: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end',
    '& .MuiTypography-root': {
      fontSize: '0.875rem',
    },
    marginBottom: 8,
  },
  iconButton: {
    marginLeft: theme.spacing(4),
    marginRight: theme.spacing(1),
  },
  pagination: {
    display: 'flex',
    justifyContent: 'space-between',
    width: 60,
    margin: '0 24px',
  },
  content: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    padding: 0,
    overflowY: 'auto',
    [theme.breakpoints.up('sm')]: {
      padding: theme.spacing(2),
    },
  },
  hidescrollBarContent: {
    scrollbarWidth: 'none',
    '&::-webkit-scrollbar': {
      height: 0,
      width: 0,
    },
  },
  view: {
    width: '100%',
    height: 600,
    border: 'none',
  },
  smiley: {
    padding: theme.spacing(2, 0),
    borderTop: `1px solid ${theme.palette.grey[300]}`,
  },
  userAction: {
    display: 'flex',
  },
  btnAction: {
    textTransform: 'initial',
    '& img': {
      width: 16,
      marginTop: -3,
    },
  },
  reaction: {
    '& img': {
      transition: theme.transitions.create(['transform'], {
        easing: theme.transitions.easing.easeInOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    '&:hover img': {
      transform: 'scale(1.3)',
    },
  },
  actionBtnContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    marginBottom: 24,
  },
  actionBtnContainerMobile: {
    width: 'auto',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  actionDocBtn: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  btnValidation: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  listActionValidation: {
    marginLeft: 'auto',
    maringRight: 0,
  },
  messageValidation: {
    marginRight: 'auto',
    marginLeft: 'auto',
  },
  header: {
    padding: '0 16px',
    display: 'flex',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 16,
    '& h2': {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 16,
      margin: 0,
      display: 'flex',
      justifyContent: 'flex-start',
    },
  },
  actionHeader: {
    display: 'flex',
    justifyContent: 'flex-end',
  },

  alert: {
    '& .MuiAlert-icon': {
      fontSize: '30px !important',
    },
    '& .MuiAlert-message': {
      fontSize: '16px !important',
    },
  },

  root: {
    // height: 'calc(100vh - 86px)',
    overflow: 'auto',
  },
}));
