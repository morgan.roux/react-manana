import React, { FC, ChangeEvent, useState, useEffect } from 'react';

import useStyles from './style';
import { ChangeDocumentProps, HandleChangeInterface, Tva, TvaLigne } from '../util/interface';
import { Box, InputAdornment, TableCell, TableRow, TextField, Typography } from '@material-ui/core';
import { Column, Table } from '@lib/ui-kit';

export interface FactureTvaProps {
  documentToSave: ChangeDocumentProps;
  setDocumentToSave: (value: React.SetStateAction<ChangeDocumentProps>) => void;
  tvas: { loading: boolean; error: Error; data: Tva[] };
  isGed?: boolean;
}

export const arrondir = (nombre: number, ordre: number = 2): number => {
  return parseFloat(nombre.toFixed(ordre));
};

const FactureTvaSelect: FC<FactureTvaProps> = ({ documentToSave, tvas, setDocumentToSave }) => {
  const classes = useStyles();

  const handleChangeHt = (row: TvaLigne, value: any) => {
    const tvaLigneChange = documentToSave.factureTvas?.filter(
      factureTva => factureTva.idTva === row.idTva,
    );
    const nouveauTauxTva = tvas.data.find(tva => tva.id === row.idTva)?.tauxTva;
    const nouveauMontantTva = arrondir((parseFloat(value || '0') * (nouveauTauxTva || 0)) / 100);
    setDocumentToSave(prev => ({
      ...prev,
      factureTvas: tvas.data.map(tva => {
        const tvaCorrespondant = prev.factureTvas?.filter(
          factureTva => factureTva.idTva === tva.id,
        );

        if (row.idTva === tva.id) {
          return {
            tva,
            idDocument: tvaCorrespondant?.length ? tvaCorrespondant[0].idDocument : undefined,
            idTva: tva.id,
            montantHt: value,
            montantTtc: arrondir(parseFloat(value || '0') + nouveauMontantTva),
            montantTva: nouveauMontantTva,
          };
        }
        return {
          tva,
          idDocument: tvaCorrespondant?.length ? tvaCorrespondant[0].idDocument : undefined,
          idTva: tva.id,
          montantHt: tvaCorrespondant?.length ? tvaCorrespondant[0].montantHt : undefined,
          montantTtc: tvaCorrespondant?.length ? tvaCorrespondant[0].montantTtc : 0,
          montantTva: tvaCorrespondant?.length ? tvaCorrespondant[0].montantTva : 0,
        };
      }),
      factureTotalHt:
        parseFloat(prev.factureTotalHt || '0') +
        parseFloat(value || '0') -
        (tvaLigneChange?.length ? tvaLigneChange[0].montantHt || 0 : 0),
      factureTva:
        parseFloat(prev.factureTva || 0) +
        nouveauMontantTva -
        (tvaLigneChange?.length ? tvaLigneChange[0].montantTva || 0 : 0),
      factureTotalTtc:
        parseFloat(prev.factureTotalTtc || 0) +
        parseFloat(value || 0) +
        nouveauMontantTva -
        (tvaLigneChange?.length ? tvaLigneChange[0].montantTtc || 0 : 0),
    }));
  };

  const columns: Column[] = [
    {
      name: 'type',
      label: <Typography>Type</Typography>,
      renderer: (row: TvaLigne) => {
        return <Typography className={classes.textType}>{row?.tva?.designation}</Typography>;
      },
    },
    {
      name: 'montant_ht',
      label: <Typography>Montant HT</Typography>,
      renderer: (row: TvaLigne) => {
        return (
          <TextField
            type="number"
            variant="standard"
            className={classes.fieldInput}
            value={
              documentToSave.factureTvas?.filter(factureTva => factureTva.idTva === row.idTva)[0]
                .montantHt
            }
            onChange={(event: ChangeEvent<HTMLInputElement>) => {
              handleChangeHt(row, event.target.value);
            }}
            InputLabelProps={{
              shrink: true,
            }}
            InputProps={{
              endAdornment: <InputAdornment position="start">€</InputAdornment>,
            }}
            inputProps={{
              step: 0.5,
            }}
          />
        );
      },
    },
    {
      name: 'montant_tva',
      label: <Typography>Montant TVA</Typography>,
      renderer: (row: TvaLigne) => {
        return (
          <Typography className={classes.text}>
            {(row?.montantTva || 0).toFixed(2)}&nbsp;€
          </Typography>
        );
      },
    },
    {
      name: 'montant_ttc',
      label: <Typography>Montant TTC</Typography>,
      renderer: (row: TvaLigne) => {
        return (
          <Typography className={classes.text}>
            {row?.montantTtc?.toFixed(2) || 0}&nbsp;€
          </Typography>
        );
      },
    },
  ];

  return (
    <Box width={1} pb={3}>
      <Table
        error={tvas.error}
        loading={tvas.loading}
        search={false}
        data={documentToSave.factureTvas || []}
        selectable={false}
        columns={columns}
        rowsTotal={tvas.data.length}
        dataIdName="idTva"
        notablePagination
        additionalRows={
          <TableRow>
            <TableCell style={{ borderBottomColor: '#fff' }}>
              <span
                style={{ display: 'flex', justifyContent: 'flex-end', borderBottomColor: '#fff' }}
              >
                Total
              </span>
            </TableCell>
            <TableCell style={{ borderBottomColor: '#fff' }}>
              <span>
                {parseFloat(
                  documentToSave.isMultipleTva ? documentToSave.factureTotalHt || 0 : 0,
                ).toFixed(2)}
                &nbsp;€
              </span>
            </TableCell>
            <TableCell style={{ borderBottomColor: '#fff' }}>
              <span>
                {parseFloat(
                  documentToSave.isMultipleTva ? documentToSave.factureTva || 0 : 0,
                ).toFixed(2)}
                &nbsp;€
              </span>
            </TableCell>
            <TableCell style={{ borderBottomColor: '#fff' }}>
              {parseFloat(
                documentToSave.isMultipleTva ? documentToSave.factureTotalTtc || 0 : 0,
              ).toFixed(2)}
              &nbsp;€
            </TableCell>
          </TableRow>
        }
      />
    </Box>
  );
};

export default FactureTvaSelect;
