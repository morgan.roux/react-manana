import {
  Box,
  Checkbox,
  Divider,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from '@material-ui/core';
import React, { FC, ChangeEvent, useState, Dispatch } from 'react';
import { useStyles } from './styles';
import {
  CustomDatePicker,
  CustomEditorText,
  CustomFormTextField,
  FormButtons,
  MontantInputField,
  NewCustomButton,
} from '@lib/ui-kit';
import CloseIcon from '@material-ui/icons/Close';
import { Dropzone, OrigineInput, formatMoney } from '@lib/common';
import {
  EfTvaInfoFragment,
  EfCompteComptableInfoFragment,
  FullDocumentCategorieFragment,
} from '@lib/common/src/federation';
import { useHistory } from 'react-router-dom';
import { ModeReglementInput } from './ModeReglementInput';
import { useEffect } from 'react';
import { useFactureLignes } from './useFactureLignes';
import { FactureViewer } from './FactureViewer';

interface NewGedFormProps {
  saving?: boolean;
  onSave: (value: FactureSave) => void;
  saved: boolean;
  setSaved: Dispatch<boolean>;
  documentToEdit?: FullDocumentCategorieFragment;
  mode?: string;
}

interface FactureLigne {
  compteComptable?: EfCompteComptableInfoFragment;
  montantHt?: number;
  montantTva?: number;
  libelle: string;
  tva?: EfTvaInfoFragment;
}
export interface FactureSave {
  factureDate?: Date;
  numeroFacture: string;
  origine?: any;
  correspondant?: any;
  idReglementMode: string;
  factureDateReglement: Date;
  factureLignes?: FactureLigne[];
  numeroCommande?: any;
  isGenererCommande: boolean;
  description: string;
  files?: File[];
  factureTotalHt: number;
  factureTva: number;
  factureTotalTtc: number;
}

const initialValue: FactureSave = {
  factureDate: new Date(),
  numeroFacture: '',
  idReglementMode: '',
  factureDateReglement: new Date(),
  factureLignes: [],
  numeroCommande: '',
  isGenererCommande: false,
  description: '',
  files: [],
  factureTotalHt: 0,
  factureTva: 0,
  factureTotalTtc: 0,
};

const initialLigne: FactureLigne = {
  tva: undefined,
  compteComptable: undefined,
  montantHt: 0,
  montantTva: 0,
  libelle: '',
};

export const NewGedForm: FC<NewGedFormProps> = ({ saving, onSave, saved, setSaved, documentToEdit, mode }) => {
  const history = useHistory();
  const [value, setValue] = useState<FactureSave>(initialValue);
  const { getFactureLignes } = useFactureLignes(setValue);
  const [previewFile, setPreviewFile] = useState<any>();

  useEffect(() => {
    if (saved) {
      setValue(initialValue);
      setSaved(false);
      handleClose();
    }
  }, [saved]);

  useEffect(() => {
    if (documentToEdit) {
      const fic: any = {
        name: documentToEdit?.fichier?.nomOriginal as any,
        text: documentToEdit?.fichier?.nomOriginal as any,
        publicUrl: documentToEdit?.fichier?.publicUrl,
      };
      setValue({
        factureDate: documentToEdit.factureDate,
        numeroFacture: documentToEdit.numeroFacture || '',
        idReglementMode: documentToEdit.idReglementMode || '',
        factureDateReglement: documentToEdit.factureDateReglement,
        factureLignes: documentToEdit.factureLignes?.length
          ? documentToEdit.factureLignes?.map(({ libelle, compteComptable, tva, montantHt, montantTva }) => ({
            montantHt,
            montantTva,
            libelle,
            tva: tva || undefined,
            compteComptable: compteComptable || undefined,
          }))
          : [],
        numeroCommande: documentToEdit.numeroCommande,
        isGenererCommande: documentToEdit.isGenererCommande || false,
        description: documentToEdit.description,
        files: [fic],
        factureTotalHt: 0,
        factureTva: 0,
        factureTotalTtc: 0,
        origine: documentToEdit.idOrigine,
        correspondant: documentToEdit.origineAssocie,
      });

      documentToEdit.fichier.publicUrl && setPreviewFile(documentToEdit.fichier.publicUrl)
    } else setValue(initialValue);
  }, [documentToEdit]);

  useEffect(() => {
    if (value.factureLignes?.length) {
      setValue((prev) => ({
        ...prev,
        factureTotalHt:
          prev.factureLignes?.reduce((accumulator: number, factureLigne) => {
            return accumulator + (factureLigne.montantHt || 0);
          }, 0) || 0,
        factureTva:
          prev.factureLignes?.reduce((accumulator: number, factureLigne) => {
            return accumulator + (factureLigne.montantTva || 0);
          }, 0) || 0,
      }));
    }
  }, [value.factureLignes]);

  useEffect(() => {
    setValue((prev) => ({ ...prev, factureTotalTtc: prev.factureTotalHt + prev.factureTva }));
  }, [value.factureTotalHt, value.factureTva]);

  const handleChange = (name: string, value?: any) => {
    setValue((prev) => ({
      ...prev,
      [name]: name === 'files' && value ? value.slice(value.length - 1, value.length) : value,
    }));
    if (name === 'files') {
      const file: any = value[value.length - 1];
      if (file) {
        const reader = new FileReader();
        reader.readAsArrayBuffer(file);

        reader.onload = (e) => {
          reader.result && setPreviewFile(new Uint8Array(reader.result as any));
        };
      } else {
        setPreviewFile(undefined);
      }
    }
  };
  const handleChangeEvent = (e: ChangeEvent<HTMLInputElement>) => {
    handleChange(e.target.name, e.target.value);
  };

  const handleClose = () => {
    history.goBack();
  };
  console.log('filels', value.files);
  const handleChangeLigne = (index: number, name: string, value?: any) => {
    setValue((prev) => ({
      ...prev,
      factureLignes: prev.factureLignes?.map((ligne, idx) => {
        if (index === idx) {
          // montantTva
          if (name === 'tva' || name === 'montantHt') {
            const montantHt = name === 'montantHt' ? value : ligne.montantHt;
            const taux = name === 'tva' ? value?.taux : ligne?.tva?.taux;

            return {
              ...ligne,
              [name]: value,
              montantTva: ((montantHt ?? 0) * (taux ?? 0)) / 100,
            };
          }

          return { ...ligne, [name]: value };
        }
        return ligne;
      }),
    }));
  };

  const handleAddRow = () => {
    setValue((prev) => ({ ...prev, factureLignes: [...(prev.factureLignes || []), initialLigne] }));
  };
  const handleRemoveRow = (index: any) => () => {
    setValue((prev) => ({ ...prev, factureLignes: prev.factureLignes?.filter((_, idx) => idx !== index) }));
  };

  const isValid = () => {
    return (
      (value.files || []).length > 0 &&
      // value.description &&
      value.origine &&
      value.correspondant &&
      value.factureDate &&
      value.idReglementMode &&
      value.factureTotalHt
      // (value.factureLignes?.length &&
      //   value.factureLignes.reduce((accumulator: boolean, ligne) => {
      //     return accumulator && !!ligne.compteComptable?.id;
      //   }, true))
    );
  };

  const classes = useStyles();
  return (
    <Box className={classes.root}>
      <Box className={classes.header}>
        <Typography style={{ margin: 'auto', fontSize: '24px', fontFamily: 'Roboto', fontWeight: 600 }}>
          {mode === 'APPROUVE'
            ? 'Validation de la Facture'
            : mode === 'PAYE' || mode === 'APPROUVE_PAYE' || mode === 'APPROUVE_COMPTABLE_PAYE'
              ? 'Paiement de la Facture'
              : mode === 'modification'
                ? 'Modification de la Facture'
                : 'Nouvelle Facture'}
        </Typography>
        <IconButton color="inherit" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </Box>
      <Box className={classes.body}>
        <Box className={classes.leftForm} p={2}>
          <Box width="100%" mb={2}>
            <Dropzone
              multiple={false}
              selectedFiles={value.files}
              setSelectedFiles={(files) => handleChange('files', files)}
              className={classes.dropZone}
            />
          </Box>
          {previewFile && <FactureViewer url={previewFile} />}
        </Box>
        <Box className={classes.rightForm} p={2}>
          <Box className={classes.contents}>
            <Box style={{ paddingBottom: '16px' }}>
              <Typography
                style={{
                  fontSize: '18px',
                  fontFamily: 'Roboto',
                  fontWeight: 700,
                  display: 'flex',
                  justifyContent: 'start',
                }}
              >
                Fournisseur
              </Typography>
            </Box>
            <Box className={classes.formulaire}>
              <Box className={classes.formulaire}>
                <Box display="flex" justifyContent="space-between">
                  <Box width="calc(50% - 10px)">
                    <CustomDatePicker
                      label="Date Facture"
                      onChange={(date) => handleChange('factureDate', date)}
                      value={value.factureDate}
                      className={classes.datePicker}
                    />
                  </Box>
                  <Box width="calc(50% - 10px)">
                    <CustomFormTextField
                      type="text"
                      label="N° de facture"
                      onChange={handleChangeEvent}
                      name="numeroFacture"
                      value={value.numeroFacture}
                      className={classes.textDialogue}
                    />
                  </Box>
                </Box>
                <Box className={classes.origineField}>
                  <OrigineInput
                    onChangeOrigine={(origine: any) => {
                      handleChange('origine', origine);
                    }}
                    origine={value.origine}
                    origineAssocie={value.correspondant}
                    onChangeOrigineAssocie={(correspondant) => {
                      handleChange('correspondant', correspondant);
                      getFactureLignes(correspondant);
                    }}
                    enabledCodes={['LABORATOIRE', 'PRESTATAIRE_SERVICE', 'CONFRERE']}
                    userInputClassName={classes.userInputClassName}
                    customSelectClassName={classes.customSelectClassName}
                  />
                </Box>
              </Box>
              <Box className={classes.formulaire}>
                <Box display="flex" justifyContent="space-between">
                  <Box width="calc(50% - 10px)">
                    <ModeReglementInput idReglementMode={value.idReglementMode} required={false} onChange={handleChangeEvent} />
                  </Box>
                  <Box width="calc(50% - 10px)">
                    <CustomDatePicker
                      label="Date règlement"
                      onChange={(date) => handleChange('factureDateReglement', date)}
                      value={value.factureDateReglement}
                      className={classes.datereglement}
                    />
                  </Box>
                </Box>
              </Box>
              <Box>
                <Typography style={{ fontSize: '16px', fontFamily: 'Roboto', fontWeight: 600 }}>
                  Détail facture
                </Typography>
              </Box>
              <Box style={{ marginTop: '16px' }}>
                <Table className="table table-bordered table-hover">
                  <TableHead>
                    <TableRow>
                      <TableCell className={classes.tableCellTitle}>Compte de charge</TableCell>
                      <TableCell className={classes.tableCellTitle} style={{ textAlign: 'center' }}>
                        Taux TVA
                      </TableCell>
                      <TableCell className={classes.tableCellTitle} style={{ textAlign: 'center' }}>
                        Montant HT
                      </TableCell>
                      <TableCell className={classes.tableCellTitle} style={{ textAlign: 'center' }}>
                        TVA
                      </TableCell>
                      <TableCell className={classes.tableCellTitle} />
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {value.factureLignes &&
                      value.factureLignes.map((factureLigne, idx) => (
                        <TableRow key={idx}>
                          <TableCell className={classes.tableCompte} align="right">
                            <Box width="100%">
                              <CustomFormTextField
                                type="text"
                                value={factureLigne.libelle}
                                fullWidth
                                onChange={(event) => handleChangeLigne(idx, 'libelle', event.target.value)}
                              />
                            </Box>
                          </TableCell>
                          <TableCell className={classes.tableTaux} align="center">
                            <Box width="100%">
                              <MontantInputField
                                value={factureLigne?.tva?.taux}
                                onChangeValue={() => { }}
                                suffix="%"
                                disabled
                              />
                            </Box>
                          </TableCell>

                          <TableCell className={classes.tableTaux} align="right">
                            <Box width="100%">
                              <MontantInputField
                                value={factureLigne.montantHt || 0}
                                onChangeValue={(montant) => handleChangeLigne(idx, 'montantHt', montant)}
                                suffix=" €"
                              />
                            </Box>
                          </TableCell>

                          <TableCell className={classes.tableTaux} align="right">
                            <Box width="100%">
                              <MontantInputField
                                value={factureLigne.montantTva || 0}
                                onChangeValue={(montant) => handleChangeLigne(idx, 'montantTva', montant)}
                                suffix=" €"
                              />
                            </Box>
                          </TableCell>

                          <TableCell className={classes.tableCell} align="right">
                            {(value.factureLignes?.length || 0) > 1 && (
                              <Box className={classes.actionDelete} onClick={handleRemoveRow(idx)}>
                                <Box>
                                  <CloseIcon />
                                </Box>
                                <Typography>Supprimer</Typography>
                              </Box>
                            )}
                          </TableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </Box>
              <Divider />
              <Box className={classes.tableCalcul}>
                <Table aria-label="customized table">
                  <TableBody>
                    <TableRow>
                      <TableCell>Total HT</TableCell>
                      <TableCell align="right">{`${formatMoney(value.factureTotalHt || 0)} €`}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>Total TVA</TableCell>
                      <TableCell align="right">{`${formatMoney(value.factureTva || 0)} €`}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>Total TTC</TableCell>
                      <TableCell align="right">{`${formatMoney(value.factureTotalTtc || 0)} €`}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </Box>
              <Box>
                <Typography style={{ fontSize: '16px', fontFamily: 'Roboto', fontWeight: 600 }}>
                  Associer une commande
                </Typography>
              </Box>
              <Box display="flex" marginTop="16px" mb={1}>
                <Box className={classes.textCommande}>
                  <TextField
                    type="number"
                    label="N° de commande"
                    variant="outlined"
                    value={value.numeroCommande}
                    onChange={(event: ChangeEvent<HTMLInputElement>): void => {
                      handleChange('numeroCommande', event.target.value);
                    }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    fullWidth
                    className={classes.inputCommande}
                  />
                </Box>

                <Box className={classes.checkbox}>
                  <Checkbox
                    checked={value.isGenererCommande}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => handleChange('isGenererCommande', e.target.checked)}
                    inputProps={{ 'aria-label': 'primary checkbox' }}
                  />
                  <Typography style={{ marginLeft: '6px' }}>Générer une commande</Typography>
                </Box>
              </Box>
              <Box>
                <CustomEditorText
                  value={value.description}
                  placeholder="Description"
                  onChange={(text) => handleChange('description', text)}
                  className={classes.editor}
                />
              </Box>
            </Box>
            <Box marginLeft="auto" marginTop="16px" marginBottom="16px">
              <FormButtons
                onClickCancel={handleClose}
                onClickConfirm={() => onSave(value)}
                disableConfirm={!isValid() || saving}
              />
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
