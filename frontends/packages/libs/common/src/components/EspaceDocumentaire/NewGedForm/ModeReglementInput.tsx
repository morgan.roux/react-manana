import React, { FC } from 'react';
import { CustomSelect } from '@lib/ui-kit';
import { useGet_Prt_Reglement_ModeQuery } from '@lib/common/src/federation';

export interface ModeReglementInputProps {
  label?: string;
  idReglementMode?: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  name?: string;
  required?: boolean;
}

export const ModeReglementInput: FC<ModeReglementInputProps> = ({ label, idReglementMode, onChange, name, required = true }) => {
  const loadModeReglements = useGet_Prt_Reglement_ModeQuery();

  return (
    <CustomSelect
      list={loadModeReglements.data?.pRTReglementModes?.nodes || []}
      loading={loadModeReglements.loading}
      listId="id"
      index="libelle"
      label={label || 'Mode règlement'}
      name={name || 'idReglementMode'}
      value={idReglementMode}
      onChange={onChange}
      required={required}
    />
  );
};
