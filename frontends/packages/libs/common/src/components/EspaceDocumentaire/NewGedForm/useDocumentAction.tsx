import { useState } from 'react';
import { useApplicationContext } from '@lib/common';
import {
  useCreate_Document_CategorieMutation,
  useDelete_Document_CategorieMutation,
  useUpdate_Document_CategorieMutation,
  useUpdate_Document_StatusMutation,
} from '@lib/common/src/federation';

export const useDocumentAction = (refetch?: () => void) => {
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [status, setStatus] = useState<string>();

  const { federation, notify,  uploadFiles } = useApplicationContext();


  const [addDocumentCategorie] = useCreate_Document_CategorieMutation({
    onCompleted: () => {
      setSaving(false);
      setSaved(true);

      notify({
        message: 'Le document a été ajouté avec succès !',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout du document!",
        type: 'error',
      });

      setSaving(false);
    },
    client: federation,
  });

  const [updateDocumentStatut] = useUpdate_Document_StatusMutation({
    onCompleted: () => {
      notify({
        message: 'Le statut du document a été changé avec succès!',
        type: 'success',
      });
      setStatus(undefined);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant le changement du statut du document!',
        type: 'error',
      });
      setStatus(undefined);
    },
    client: federation,
  });

  const [updateDocumentCategorie] = useUpdate_Document_CategorieMutation({
    onCompleted: (result) => {
      setSaving(false);
      setSaved(true);
      notify({
        message: 'Le document a été remplacé avec succès !',
        type: 'success',
      });
      if (status) {
        updateDocumentStatut({
          variables: {
            input: {
              idDocument: result.updateOneGedDocument.id,
              commentaire: '',
              status,
            },
          },
        });
      }
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant le remplacement du document!',
        type: 'error',
      });
      setSaving(false);
    },
    client: federation,
  });

  const [deleteDocumentCategorie] = useDelete_Document_CategorieMutation({
    onCompleted: () => {
      notify({
        message: 'Le document a été supprimé avec succès !',
        type: 'success',
      });

      refetch && refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression du document !',
        type: 'error',
      });
    },
    client: federation,
  });

  const upsertDocument = ({
    id,
    idSousCategorie,
    description,
    nomenclature,
    numeroVersion,
    idUserVerificateur,
    idUserRedacteur,
    dateHeureParution,
    dateHeureDebutValidite,
    dateHeureFinValidite,
    motCle1,
    motCle2,
    motCle3,
    fichier,
    idOrigine,
    idOrigineAssocie,
    dateFacture,
    hT,
    tva,
    ttc,
    type,

    numeroFacture,
    factureDateReglement,
    idReglementMode,
    numeroCommande,
    isGenererCommande,
    avoirType,
    avoirCorrespondants,
    nombreJoursPreavis,
    isRenouvellementTacite,
    isMultipleTva,
    factureTvas,
    factureLignes,
    status,
  }: any): void => {
    if (!id) {
      addDocumentCategorie({
        variables: {
          input: {
            idSousCategorie: idSousCategorie,
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: new Date(dateHeureParution),
            dateHeureDebutValidite: new Date(dateHeureDebutValidite),
            dateHeureFinValidite: new Date(dateHeureFinValidite),
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
            nombreJoursPreavis,
            isRenouvellementTacite,
            isMultipleTva,
            facturesTva: factureTvas,
            factureLignes,
            statut: status,
          },
        },
      });
    } else {
      updateDocumentCategorie({
        variables: {
          input: {
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: dateHeureParution,
            dateHeureDebutValidite: dateHeureDebutValidite,
            dateHeureFinValidite: dateHeureFinValidite,
            idSousCategorie: idSousCategorie,
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
            nombreJoursPreavis,
            isRenouvellementTacite,
            isMultipleTva,
            facturesTva: factureTvas,
            factureLignes,
            statut: status,
          },
          id: id,
        },
      });
    }
  };

  const showError = () => {
    notify({
      message: `Une erreur est survenue pendant le chargement de la facture`,
      type: 'error',
    });
  };

  const handleCreateDocument = (doc: any, mode?: string): void => {
    setSaving(true);
    setSaved(false);
    if (mode === 'APPROUVE' || mode === 'PAYE' || mode === 'APPROUVE_PAYE' || mode === 'APPROUVE_COMPTABLE_PAYE') {
      setStatus(mode);
    }
    if (doc.launchUpload) {

      uploadFiles([doc.selectedFile])
        .then((uploadedFiles) => {

          const { type, chemin, nomOriginal } = uploadedFiles[0]


          upsertDocument({
            id: doc.id,
            idSousCategorie: doc.idSousCategorie,
            description: doc.description,
            nomenclature: doc.nomenclature,
            numeroVersion: doc.numeroVersion,
            idUserVerificateur: doc.idUserVerificateur,
            idUserRedacteur: doc.idUserRedacteur,
            dateHeureParution: doc.dateHeureParution,
            dateHeureDebutValidite: doc.dateHeureDebutValidite,
            dateHeureFinValidite: doc.dateHeureFinValidite,
            motCle1: doc.motCle1,
            motCle2: doc.motCle2,
            motCle3: doc.motCle3,
            fichier: { type, chemin, nomOriginal },
            idOrigine: doc.origine?.id || doc.origine,
            idOrigineAssocie: doc.correspondant?.id || doc.origineAssocie?.id,
            dateFacture: doc.dateFacture,
            hT: doc.hT ? parseFloat(doc.hT) : undefined,
            tva: doc.tva ? parseFloat(doc.tva) : undefined,
            ttc: doc.ttc ? parseFloat(doc.ttc) : undefined,
            type: doc.type,

            numeroFacture: doc?.numeroFacture,
            factureDateReglement: doc?.factureDateReglement,
            idReglementMode: doc?.idReglementMode,
            numeroCommande: doc?.numeroCommande,
            isGenererCommande: doc?.isGenererCommande,
            avoirType: doc?.avoirType,
            avoirCorrespondants: doc?.avoirCorrespondants,
            nombreJoursPreavis: doc?.nombreJoursPreavis,
            isRenouvellementTacite: doc?.isRenouvellementTacite,
            isMultipleTva: doc?.isMultipleTva,
            factureTvas: doc?.factureTvas,
            factureLignes: doc?.factureLignes,
            status,
          });


        }).catch(error => {
          showError();
          setSaving(false);
        })

    } else {
      const { nomOriginal, chemin, type } = doc.previousFichier || {};

      upsertDocument({
        id: doc.id,
        idSousCategorie: doc.idSousCategorie,
        description: doc.description,
        nomenclature: doc.nomenclature,
        numeroVersion: doc.numeroVersion,
        idUserVerificateur: doc.idUserVerificateur,
        idUserRedacteur: doc.idUserRedacteur,
        dateHeureParution: doc.dateHeureParution,
        dateHeureDebutValidite: doc.dateHeureDebutValidite,
        dateHeureFinValidite: doc.dateHeureFinValidite,
        motCle1: doc.motCle1,
        motCle2: doc.motCle2,
        motCle3: doc.motCle3,
        fichier: doc.previousFichier ? { nomOriginal, chemin, type } : undefined,
        idOrigine: doc.origine?.id || doc.origine,
        idOrigineAssocie: doc.correspondant?.id,
        dateFacture: 'FACTURE' === doc.type ? doc.dateFacture : undefined,
        hT: 'FACTURE' === doc.type && doc.hT ? parseFloat(doc.hT) : undefined,
        tva: 'FACTURE' === doc.type && doc.tva ? parseFloat(doc.tva) : undefined,
        ttc: 'FACTURE' === doc.type && doc.ttc ? parseFloat(doc.ttc) : undefined,
        type: doc.type,

        numeroFacture: doc?.numeroFacture,
        factureDateReglement: doc?.factureDateReglement,
        idReglementMode: doc?.idReglementMode,
        numeroCommande: doc?.numeroCommande,
        isGenererCommande: doc?.isGenererCommande,
        avoirType: doc?.avoirType,
        avoirCorrespondants: doc?.avoirCorrespondants,
        nombreJoursPreavis: doc?.nombreJoursPreavis,
        isRenouvellementTacite: doc?.isRenouvellementTacite,
        isMultipleTva: doc?.isMultipleTva,
        factureTvas: doc?.factureTvas,
        factureLignes: doc?.factureLignes,
      });
    }
  };

  const handleDelete = (id?: string): void => {
    if (id) {
      deleteDocumentCategorie({
        variables: {
          input: {
            id,
          },
        },
      });
    }
  };

  return { handleCreateDocument, saving, saved, setSaved, handleDelete };
};
