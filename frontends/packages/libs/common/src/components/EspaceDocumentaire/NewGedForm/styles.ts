import { createStyles, lighten, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      '& img': {
        maxWidth: '100%',
        height: 'auto',
      },
    },
    header: {
      position: 'sticky',
      top: 0,
      zIndex: 999,
      width: '100%',
      height: 70,
      display: 'flex',
      padding: '0 24px',
      marginBottom: '16px',
      justifyContent: 'space-between',
      alignItems: 'center',
      color: theme.palette.common.white,
      background: lighten(theme.palette.primary.main, 0.1),
    },
    body: {
      width: '100%',
      display: 'flex',
      margin: 'auto',
    },
    contents: {
      // margin: 'auto',
      // display: 'flex',
      // flexDirection: 'column',
      // [theme.breakpoints.down('md')]: {
      //   width: '100%',
      //   display: 'flex',
      //   flexWrap: 'wrap',
      //   justifyContent: 'center',
      // },
    },
    leftForm: {
      width: '50%',
    },
    rightForm: {
      width: '50%',
    },
    formulaire: {
      display: 'flex',
      flexDirection: 'column',
    },
    leftFormulaire: {
      display: 'flex',
      // alignItems: 'center',
    },
    modeReglement: {
      paddingRight: 8,
    },
    datePicker: {
      marginBottom: 16,
    },
    datereglement: {
      // marginLeft: '16px',
    },
    input: {
      // height: '58px',
      marginLeft: '16px',

      marginTop: '-8px',
    },
    textDialogue: {
      // '& .main-MuiInputBase-root': {
      //     height: '50px',
      // },
    },
    textCommande: {
      '&.main-MuiFormControl-fullWidth': {
        width: 'auto',
      },
    },
    checkbox: {
      marginLeft: '6px',
      display: 'flex',
      alignItems: 'center',
      marginBottom: '16px',
    },
    tableCell: {
      padding: 0,
      color: '#fff',
      border: 'none',
      '&.main-MuiTableCell-alignRight': {
        textAlign: 'inherit',
      },
    },
    tableCellTitle: {
      color: '#000',
      fontSize: '16px',
      fontFamily: 'Roboto',
      fontWeight: 600,
      padding: 0,
      border: 'none',
      opacity: 0.8,
    },
    actionDelete: {
      display: 'flex',
      cursor: 'pointer',
      marginLeft: '8px',
      '& .main-MuiSvgIcon-root': {
        color: '#000',
      },
      '& .main-MuiTypography-root': {
        textTransform: 'uppercase',
        marginLeft: '8px',
        color: '#000',
      },
    },
    actionAdd: {
      display: 'flex',
      cursor: 'pointer',
      float: 'right',
      '& .main-MuiSvgIcon-root': {
        color: theme.palette.primary.main,
      },
      '& .main-MuiTypography-root': {
        textTransform: 'uppercase',
        marginLeft: '8px',
        color: theme.palette.primary.main,
      },
    },
    tableCalcul: {
      marginBottom: '16px',
      [theme.breakpoints.up('md')]: {
        width: '50%',
        marginLeft: 'auto',
      },
    },
    customFormTextTva: {
      marginLeft: '-8px',
    },
    customFormTextHT: {
      // marginLeft: '-8px',
    },
    editor: {
      marginBottom: '16px',
      '& .ql-container.ql-snow': {
        minHeight: '100px !important',
      },
    },
    dropZone: {
      // maxWidth: '610px !important',
    },
    origineField: {
      width: '100%',
    },
    tableTaux: {
      width: '20%',
      border: 'none',
      paddingBlock: 0,
    },
    tableCompte: {
      padding: 0,
      width: '38%',
      border: 'none',
    },
    inputCommande: {
      '& input': {
        padding: 14.5,
      },
    },
    userInputClassName: {
      width: '98%',
    },
    customSelectClassName: {
      marginRight: 12,
    },
  })
);
