import {
  useEfCompteComptablesLazyQuery,
  useEfModeleEcritureLignesLazyQuery,
  useEfModeleEcrituresLazyQuery,
} from '@lib/common/src/federation';
import React from 'react';
import { useApplicationContext } from '@lib/common';
import { FactureSave } from './NewGedForm';

export const useFactureLignes = (setValue: React.Dispatch<React.SetStateAction<FactureSave>>) => {
  const { notify, currentPharmacie } = useApplicationContext();
  /* const [loadModeleEcriture] = useEfModeleEcrituresLazyQuery({
    fetchPolicy: 'network-only',
    onCompleted: (result) => {
      if (result.eFModeleEcritures?.totalCount) {
        notify({type: 'info', message: 'modele'})

        const modeles = result.eFModeleEcritures.nodes || [];
        const modeleLignes = modeles.map((modele) => modele.lignes || []).flat();
        setValue((prev) => ({
          ...prev,
          factureLignes: modeleLignes
            .filter(({ compteComptable }) => compteComptable?.tva?.id)
            .map(({ compteComptable }) => ({
              compteComptable: compteComptable || undefined,
              montantHt: 0,
            })),
        }));
      }
    },
  }); */

  // const [loadModeleEcritureLigne] = useEfModeleEcritureLignesLazyQuery({
  //   fetchPolicy: 'network-only',
  //   onCompleted: (result) => {
  //     if (result.eFModeleEcritureLignes.nodes.length) {
  //       const idModeleEcritures = result.eFModeleEcritureLignes.nodes.map((ligne) => ligne.idModeleEcriture) || [];
  //       if (idModeleEcritures?.length) {
  //         loadModeleEcriture({ variables: { filter: { id: { in: idModeleEcritures } } } });
  //         console.log('modele', idModeleEcritures);
  //       }
  //     }
  //   },
  // });

  const [loadModeleEcriture] = useEfModeleEcrituresLazyQuery({
    fetchPolicy: 'network-only',
    onCompleted: (result) => {
      const modeleLignes = result.eFModeleEcritures.nodes?.length ? result.eFModeleEcritures.nodes[0].lignes || [] : []

      setValue((prev) => {
        const prevFacturesLignes = prev.factureLignes || [];

        return ({
          ...prev,
          idReglementMode: (result.eFModeleEcritures.nodes?.length && result.eFModeleEcritures.nodes[0].idModeReglement) || '',
          factureLignes: [
            ...prevFacturesLignes,
            ...modeleLignes
              .filter(({ tva }) => tva?.id && !prevFacturesLignes.some(pfl => pfl?.tva?.id === tva.id))
              .map(({ compteComptable, tva, libelle }) => ({
                libelle,
                compteComptable: compteComptable || undefined,
                tva: tva || undefined,
                montantHt: 0,
                montantTva: 0
              }))],
        })
      });
    }
  });

  const [loadCompteComptable] = useEfCompteComptablesLazyQuery({
    fetchPolicy: 'network-only',
    onCompleted: (result) => {
      if (result?.eFCompteComptables?.nodes?.length) {
        const compteComptable = result.eFCompteComptables.nodes[0];
        if (compteComptable?.id) {
          loadModeleEcriture({
            variables: {
              filter: {
                idCompteFournisseur: {
                  eq: compteComptable.id,
                },
                idPharmacie: {
                  eq: currentPharmacie.id,
                },
              },
              paging: {
                offset: 0,
                limit: 1,
              },
            },
          });
        }
      }
    },
  });

  const getFactureLignes = (correspondant: any) => {
    if (correspondant?.id) {
      setValue((prev) => ({
        ...prev,
        factureLignes: [],
      }));
      loadCompteComptable({
        variables: {
          filter: {
            idOrigineAssocie: {
              eq: correspondant.id,
            },
          },
        },
      });
    }
  };

  return { getFactureLignes };
};
