import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import { FileViewer } from '@lib/common';
import { ApplicationConfig } from '@lib/common';

interface FactureViewerProps {
  url: string;
  config?: ApplicationConfig;
}

const Viewer: FC<FactureViewerProps> = ({ url, config }) => {
  return (
    <Box>
      <FileViewer fileUrl={url} config={config} style={{ height: 700 }} />
    </Box>
  );
};

export const FactureViewer = React.memo(Viewer);
