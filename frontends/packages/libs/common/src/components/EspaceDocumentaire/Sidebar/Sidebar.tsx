import { ErrorPage, Loader, DebouncedSearchInput, NoItemContentImage, CustomModal, NewCustomButton } from '@lib/ui-kit';
import { Box, Button, Fab, IconButton, Menu, Tooltip, Typography } from '@material-ui/core';
import { Add, Sort, CloudDownload } from '@material-ui/icons';
import React, { FC, useEffect, useState, Dispatch, SetStateAction } from 'react';
import { IDocumentSearch } from '../EspaceDocumentaire';
import DocumentCard from './DocumentCard';
import SearchMenu from './SearchMenu/SearchMenu';
import style from './style';
import FilterAlt from '../../../assets/img/filter_alt.svg';
import CreateDocument from '../Content/CreateDocument';
import classNames from 'classnames';
import { FormDocument } from './../Content/FormDocument';
import { uniqBy } from 'lodash';
import { MagnifierIcon } from './DocumentCard/MagnifierIcon/MagnifierIcon';
import {
  Search_DocumentQuery,
  Search_DocumentQueryVariables,
  FullDocumentCategorieFragment,
} from '../../../federation';
import { useApplicationContext } from '@lib/common';
import { useOcr_DocumentsLazyQuery, useOcr_Pharmacie_Email_DataLazyQuery } from '@lib/common/src/federation';
import { useHistory } from 'react-router-dom';
import { LazyQueryResult } from '@apollo/client';
import { InfiniteItemList } from '../../../components';

export interface SideBarProps {
  documents: LazyQueryResult<Search_DocumentQuery, Search_DocumentQueryVariables>;
  saving: boolean;
  saved: boolean;
  setSaved: (newValue: boolean) => void;
  activeSousCategorie: any | undefined;
  searchVariables: any;
  setActiveDocument: (document: FullDocumentCategorieFragment | undefined) => void;
  onRequestAddToFavorite: (document: FullDocumentCategorieFragment) => void;
  onRequestRemoveFromFavorite: (document: FullDocumentCategorieFragment) => void;
  onRequestReplace: (document: FullDocumentCategorieFragment) => void;
  onRequestDelete: (document: FullDocumentCategorieFragment) => void;
  onRequestDetails: (document: FullDocumentCategorieFragment) => void;
  onRequestGetComments: (document: FullDocumentCategorieFragment) => void;
  onRequestDeplace: (document: FullDocumentCategorieFragment) => void;
  setSearchVariables: Dispatch<SetStateAction<IDocumentSearch>>;
  onRequestCreateDocument: (document: any) => void;
  document?: FullDocumentCategorieFragment;
  onRequestRefresh: () => void;

  espaceType?: string;
  tvas: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  defaultCorrespondant?: any;
  refetch?: () => void;
}

const Sidebar: FC<SideBarProps> = ({
  documents,
  activeSousCategorie,
  saving,
  saved,
  searchVariables,
  setSaved,
  setActiveDocument,
  onRequestCreateDocument,
  onRequestAddToFavorite,
  onRequestRemoveFromFavorite,
  onRequestReplace,
  onRequestDelete,
  onRequestGetComments,
  onRequestDeplace,
  setSearchVariables,
  onRequestRefresh,
  document,
  espaceType,
  tvas,
  defaultCorrespondant,
  refetch,
}) => {
  const classes = style();
  const { isMobile, notify, federation, auth, useParameterValueAsString, useParameterValueAsBoolean } =
    useApplicationContext();
  const history = useHistory();
  const [openFilter, setOpenFilter] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [textToSearch, setTextToSearch] = useState<string>('');
  const [isASC, setIsASC] = useState<boolean>(true);
  const [openModalCreateDoc, setOpenModalCreateDoc] = useState<boolean>(false);
  const [activeCard, setActiveCard] = useState<string | undefined>();
  const [openFormDocument, setOpenFormDocument] = useState<boolean>(false);
  const [savingFormDocument, setSavingFormDocument] = useState<boolean>(false);

  const smtpUser = useParameterValueAsString('0833');
  const smtpPassword = useParameterValueAsString('0834');
  const smtpHost = useParameterValueAsString('0835');
  const smtpPort = useParameterValueAsString('0836');

  const enableOCRisation = useParameterValueAsBoolean('0839');
  const enableSmtp = smtpUser && smtpPassword && smtpHost && smtpPort;

  useEffect(() => {
    if (saved) {
      setOpenModalCreateDoc(false);
      setSaved(false);
    }
  }, [saved]);

  useEffect(() => {
    if (document) {
      setActiveCard(document.id);
    }
  }, [document]);

  const [loadPharmacieEmail, loadingPharmacie] = useOcr_Pharmacie_Email_DataLazyQuery({
    onCompleted: () => {
      notify({
        message: 'Récupération effectuée avec succès !',
        type: 'success',
      });
      onRequestRefresh();
      setSearchVariables((prev) => ({
        ...prev,
        idSousCategorie: undefined,
        filterBy: 'TOUT',
        sortBy: undefined,
        type: 'FACTURE',
        validation: 'TOUT',
        idOrigine: undefined,
        idOrigineAssocie: undefined,
        dateDebut: undefined,
        dateFin: undefined,
        accesRapide: ['EN_ATTENTE_APPROBATION'],
      }));
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la récupération des factures dans le boîte email !',
        type: 'error',
      });
    },
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [loadOcrDocument, loadingOCRDocument] = useOcr_DocumentsLazyQuery({
    onCompleted: () => {
      notify({
        message: 'OCR des documents effectué avec succès !',
        type: 'success',
      });
      onRequestRefresh();
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'OCR des documents !",
        type: 'error',
      });
    },
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const handleCloseFilter = () => {
    setAnchorEl(null);
    setOpenFilter(false);
  };
  const handleClickFilter = (event: React.MouseEvent) => {
    setAnchorEl(event.currentTarget as HTMLElement);
    setOpenFilter((prev) => !prev);
  };

  const handleClick = (document: FullDocumentCategorieFragment): void => {
    setActiveDocument(document);
    // onRequestDetails(document);
    onRequestGetComments(document);
    // setActiveCard(currentActive);
  };

  const handleReplace = (document: FullDocumentCategorieFragment): void => {
    onRequestReplace(document);
  };

  const handleSort = (): void => {
    setIsASC(!isASC);
    setSearchVariables((prev) => ({
      ...prev,
      sortBy: 'description',
      sortDirection: isASC ? 'ASC' : 'DESC',
    }));
  };

  const handleAddClick = () => {
    if ('espace-facture' === espaceType) history.push('/espace-facturation/form');
    else setOpenModalCreateDoc(true);
  };

  const handleAttachFile = () => {
    setOpenFormDocument(true);
  };

  const handleScan = () => {
    loadPharmacieEmail();
  };

  const handleOcR = () => {
    loadOcrDocument();
  };

  return (
    <Box height={1}>
      {!isMobile && (
        <Box className={classes.header}>
          <NewCustomButton
            className={classes.newButton}
            disabled={!auth.isAuthorizedToAddGedDocument()}
            onClick={handleAddClick}
            startIcon={<Add />}
          >
            {'espace-facture' === espaceType ? 'NOUVELLE FACTURE' : 'NOUVEAU DOCUMENT'}
          </NewCustomButton>
          <Box className={classes.acitionContainer}>
            {/*
            <Box className={classes.attach}>
              <IconButton onClick={handleAttachFile}>
                <AttachFile />
              </IconButton>
            </Box>
            */}
            <Box>
              {enableSmtp && (
                <Tooltip title="Récupérer les factures dans le boîte email">
                  <IconButton onClick={handleScan} disabled={loadingPharmacie.loading}>
                    <CloudDownload />
                  </IconButton>
                </Tooltip>
              )}

              {enableOCRisation && (
                <Tooltip title="OCR-iser tous les documents">
                  <IconButton onClick={handleOcR} disabled={loadingOCRDocument.loading}>
                    <MagnifierIcon />
                  </IconButton>
                </Tooltip>
              )}
            </Box>
            {'espace-documentaire' === espaceType && (
              <>
                <IconButton onClick={handleSort}>
                  <Sort />
                </IconButton>
                <IconButton onClick={handleClickFilter}>
                  <img src={FilterAlt} alt="iconFilterAlt" />
                </IconButton>
              </>
            )}
          </Box>
        </Box>
      )}
      {isMobile && (
        <Box className={classes.searchTextContainer}>
          <DebouncedSearchInput wait={100} outlined onChange={setTextToSearch} value={textToSearch} />
        </Box>
      )}
      <Box className={classNames(classes.documentContainer, classes[isMobile ? 'heigthMobile' : 'heigthWeb'])}>
        <InfiniteItemList<FullDocumentCategorieFragment, Search_DocumentQuery, Search_DocumentQueryVariables>
          classes={{
            list: classes.listDocuments,
          }}
          queryResult={documents}
          extractItems={(previdedDocuments) => previdedDocuments.data?.searchGedDocuments.data || []}
          extractItemsTotalCount={(previdedDocuments) => previdedDocuments.data?.searchGedDocuments.total || 0}
          buildVariables={(queryResult, { startIndex, stopIndex }) => {
            return {
              ...(queryResult.variables || {}),
              input: {
                ...(queryResult.variables?.input || {}),
                offset: startIndex,
                limit: stopIndex - startIndex + 1,
              },
            } as any;
          }}
          updateQuery={(prev, { fetchMoreResult }) => {
            if (prev.searchGedDocuments.data && fetchMoreResult?.searchGedDocuments.data) {
              const { data: currentData } = prev.searchGedDocuments;
              return {
                ...prev,
                searchGedDocuments: {
                  ...prev.searchGedDocuments,
                  data: uniqBy([...currentData, ...fetchMoreResult.searchGedDocuments.data], 'id'),
                  total: fetchMoreResult.searchGedDocuments.total,
                },
              };
            }

            return prev;
          }}
          activeItem={{ id: activeCard } as any}
          onItemClick={handleClick}
          refetch={refetch}
          itemRenderer={({ onClick, active, data, style, loading }) => (
            <DocumentCard
              style={style}
              loading={loading}
              saving={saving}
              saved={saved}
              setSaved={setSaved}
              activeSousCategorie={activeSousCategorie}
              active={active}
              data={data as any}
              onRequestAddToFavorite={onRequestAddToFavorite}
              onRequestRemoveFromFavorite={onRequestRemoveFromFavorite}
              onRequestDelete={onRequestDelete}
              onRequestReplaceDocument={handleReplace}
              onRequestDeplace={onRequestDeplace}
              onClick={onClick}
              searchVariables={searchVariables}
              espaceType={espaceType}
              tvas={tvas}
              defaultCorrespondant={defaultCorrespondant}
            />
          )}
        />

        {/* <InfiniteScroll
          style={{ padding: '0 4px' }}
          dataLength={documents.total}
          next={onRequestScroll}
          hasMore={(documents?.data?.length || 0) < documents.total}
          loader={<></>}
          scrollableTarget="infinty-scroll"
        >
          {documents.loading ? (
            <Loader />
          ) : (
            <>
              {documents?.error || !documents?.data ? (
                <ErrorPage />
              ) : documents?.data?.length > 0 ? (
                documents?.data?.map((document: FullDocumentCategorieFragment, index: number) => (
                  <DocumentCard
                    saving={saving}
                    saved={saved}
                    setSaved={setSaved}
                    document={document}
                    activeSousCategorie={activeSousCategorie}
                    active={document.id === activeCard}
                    key={document?.id || index}
                    info={document}
                    onRequestAddToFavorite={onRequestAddToFavorite}
                    onRequestRemoveFromFavorite={onRequestRemoveFromFavorite}
                    onRequestDelete={onRequestDelete}
                    onRequestReplaceDocument={handleReplace}
                    onRequestDeplace={onRequestDeplace}
                    onClick={() => handleClick(document, document.id)}
                    searchVariables={searchVariables}
                    espaceType={espaceType}
                    tvas={tvas}
                    defaultCorrespondant={defaultCorrespondant}
                  />
                ))
              ) : (
                <NoItemContentImage
                  title="Affichage des documents"
                  subtitle="Aucun document à afficher. Créez-en un en cliquant sur le bouton adéquat."
                />
              )}
            </>
          )}
        </InfiniteScroll>*/}
      </Box>

      <Menu
        open={openFilter}
        anchorEl={anchorEl}
        onClose={handleCloseFilter}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <SearchMenu searchVariables={searchVariables} setSearchVariables={setSearchVariables} />
      </Menu>

      <CreateDocument
        open={openModalCreateDoc}
        setOpen={setOpenModalCreateDoc}
        title={'espace-facture' === espaceType ? 'Nouvelle facture' : "Ajout d'un document"}
        saving={saving}
        activeSousCategorie={activeSousCategorie}
        mode="creation"
        onRequestCreateDocument={onRequestCreateDocument}
        document={document}
        searchVariables={searchVariables}
        defaultType={'espace-facture' === espaceType ? 'FACTURE' : undefined}
        tvas={tvas}
        defaultCorrespondant={defaultCorrespondant}
      />
      <CustomModal
        open={openFormDocument}
        setOpen={setOpenFormDocument}
        title={'espace-facture' === espaceType ? 'Nouvelles factures' : 'Ajout des documents'}
        closeIcon
        withBtnsActions
        headerWithBgColor
        fullWidth
        maxWidth="sm"
        fullScreen={!!isMobile}
        disabledButton={savingFormDocument}
      >
        <FormDocument />
      </CustomModal>
      {isMobile && (
        <Fab className={classes.fab} color="primary" variant="round" onClick={handleAddClick}>
          <Add />
        </Fab>
      )}
    </Box>
  );
};

export default Sidebar;
