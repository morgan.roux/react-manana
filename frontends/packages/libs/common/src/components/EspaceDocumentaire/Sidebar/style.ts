import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
  sidebarContainer: {
    maxWidth: 400,
    height: '100%',
    flexShrink: 0,
    borderRight: `1px solid ${theme.palette.divider}`,
    overflowY: 'auto',
  },
  header: {
    padding: '0 16px',
    margin: '16px 0',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'space-between',
  },
  newButton: {
    padding: '6px 16px !important',
  },
  acitionContainer: {
    display: 'flex',
    // minWidth: '216px',
    justifyContent: 'space-between',
  },
  documentContainer: {
    marginTop: 16,
    overflowY: 'scroll',
    scrollbarWidth: 'none',
    '&::-webkit-scrollbar': {
      height: 0,
      width: 0,
    },
  },
  heigthMobile: {
    height: 'calc(100vh - 316px)',
  },
  heigthWeb: {
    height: '100%',
  },
  searchTextContainer: {
    width: '100%',
    margin: '16px 0 24px 0',
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
  fab: {
    position: 'absolute',
    right: 32,
    bottom: 75,
  },
  attach: {
    display: 'flex',
    marginRight: 3,
  },
  buttonFacture: {
    display: 'flex',
    margin: 'auto',
    '& .main-MuiSvgIcon-root': {
      color: theme.palette.primary.main,
      fontSize: '24px',
    },
    '& .main-MuiTypography-root': {
      marginLeft: '8px',
      color: theme.palette.primary.main,
      fontSize: '12px',
    },
  },

  listDocuments: {
    paddingBottom: '100px !important',
  },
}));
