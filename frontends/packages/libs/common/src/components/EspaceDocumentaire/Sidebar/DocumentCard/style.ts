import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
  root: {
    //  margin: '0 16px',
    /*'&:last-child': {
      marginBottom: -50,
    },*/
  },
  menuList: {
    color: theme.palette.grey[700],
    '& svg': {
      marginRight: theme.spacing(1),
      color: 'inherit',
    },
  },
  cardContent: {
    padding: 0,
  },
  separateur: {
    margin: '16px 0',
    // borderBottom: `2px solid ${theme.palette.grey[200]}`,
  },
  card: {
    padding: '16px 0 ',
    marginBottom: 16,
    width: '100%',
    position: 'relative',
    cursor: 'pointer',
    '&:hover': {
      background: '#F8F8F8',
    },
    [theme.breakpoints.down('md')]: {
      background: '#F8F8F8',
    },
  },
  active: {
    background: '#F8F8F8',
  },
  libelleContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  bold: {
    fontFamily: 'Roboto',
    fontWeight: 'bolder',
    fontSize: 16,
  },
  light: {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    fontSize: 14,
  },
  padding: {
    padding: '0 16px',
  },
  details: {
    display: 'flex',
    flexWrap: 'wrap',
    '&>div': {
      flex: '0 0 50%',
    },
    textAlign: 'left',
    [theme.breakpoints.down('sm')]: {
      flexWrap: 'nowrap',
    },
  },
  status: {
    display: 'flex',
    marginBottom: -8,
    justifyContent: 'space-between',
    '& [class*=MuiChip-root]': {
      justifyContent: 'flex-start',
      background: 'none',
      '& svg': {
        marginLeft: 0,
        flexWrap: 'wrap',
      },
    },
  },
  text: {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    fontSize: 14,
    textAlign: 'left',
  },
  affichageMobile: {
    [theme.breakpoints.down('md')]: {
      display: 'grid',
      maxWidth: '34%',
      '& .main-MuiTypography-root': {
        color: '#424242',
      },
    },
  },
  affichageDateMobile: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    '& .main-MuiSvgIcon-root': {
      display: 'flex',
      marginLeft: '14px',
      fontSize: '16px',
      color: '#9E9E9E',
    },
  },
}));
