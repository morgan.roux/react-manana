import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    menuList: {
      color: '#373740',
      '& svg': {
        marginRight: theme.spacing(1),
        color: '#373740',
      },
    },
    resilierBox: {
      display: 'flex',
      alignItems: 'center',
    },
    textResilier: {
      color: '#212121',
      fontSize: 14,
      fontWeight: 500 as any,
      paddingLeft: 24,
    },
  })
);

export default useStyles;
