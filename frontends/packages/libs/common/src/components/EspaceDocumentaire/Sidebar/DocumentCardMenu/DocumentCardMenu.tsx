import { useApplicationContext } from '@lib/common';
import { useUpdate_Document_StatusMutation } from '@lib/common/src/federation';
import { ConfirmDeleteDialog, CustomModal } from '@lib/ui-kit';
import { Box, MenuItem, MenuList, Typography } from '@material-ui/core';
import { Check, Close, DeleteForever, Favorite, FavoriteBorder, Flip, HighlightOff, Warning } from '@material-ui/icons';
import React, { FC, useEffect, useState } from 'react';
import CreateDocument from '../../Content/CreateDocument';
import InfoDocument from '../../Content/InfoDocument';
import ModalDocument from '../../Content/ModalDocument';
import { IDocumentSearch } from '../../EspaceDocumentaire';
import useStyles from './style';
import { FullDocumentCategorieFragment } from './../../../../federation';

interface DocumentCardMenuProps {
  saving: boolean;
  saved: boolean;
  setSaved: (newValue: boolean) => void;
  activeSousCategorie?: string;
  document?: FullDocumentCategorieFragment;
  isOpenDetail?: boolean;
  setOpenOptions: (openOptions: boolean) => void;
  onRequestReplaceDocument?: (document: FullDocumentCategorieFragment) => void;
  onRequestAddToFavorite: (document: FullDocumentCategorieFragment) => void;
  onRequestRemoveFromFavorite: (document: FullDocumentCategorieFragment) => void;
  onRequestDelete: (document: FullDocumentCategorieFragment) => void;
  onRequestDeplace: (document: FullDocumentCategorieFragment) => void;
  onRequestValider?: (id: string) => void;
  onRequestRefuser?: (id: string) => void;
  searchVariables?: IDocumentSearch;

  espaceType?: string;
  tvas: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  defaultCorrespondant?: any;
}

const DocumentCardMenu: FC<DocumentCardMenuProps> = ({
  activeSousCategorie,
  document,
  saving,
  saved,
  setSaved,
  setOpenOptions,
  onRequestReplaceDocument,
  onRequestAddToFavorite,
  onRequestDelete,
  onRequestRemoveFromFavorite,
  isOpenDetail,
  onRequestRefuser,
  onRequestValider,
  searchVariables,

  espaceType,
  tvas,
  defaultCorrespondant,
}) => {
  const classes = useStyles({});

  const { user, isMobile, notify, federation } = useApplicationContext();

  const [openReplaceDocument, setOpenReplaceDocument] = useState<boolean>(false);
  const [openDetails, setOpenDetails] = useState<boolean>(false);
  const [openDeleteDocumentConfirm, setOpenDeleteDocumentConfirm] = useState<boolean>(false);
  const [openResilierDialog, setOpenResilierDialog] = useState<boolean>(false);

  useEffect(() => {
    if (saved) {
      setOpenReplaceDocument(false);
      setSaved(false);
    }
  }, [saved]);

  const [updateDocumentStatut] = useUpdate_Document_StatusMutation({
    onCompleted: () => {
      notify({
        message: 'Le statut du document a été changé avec succès!',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant le changement du statut du document!',
        type: 'error',
      });
    },
    client: federation,
  });

  const handleReplace = (): void => {
    setOpenReplaceDocument(true);
  };

  const handleDelete = (): void => {
    onRequestDelete(document as any);
    setOpenDeleteDocumentConfirm(false);
  };

  const handleAddToFavorite = (): void => {
    onRequestAddToFavorite(document as any);
    closeActionsMenu();
  };

  const handleRemoveFromFavorite = (): void => {
    onRequestRemoveFromFavorite(document as any);
    closeActionsMenu();
  };

  const closeActionsMenu = (): void => setOpenOptions(false);

  const handleOnRequestValider = () => {
    if (onRequestValider && document && document.id) onRequestValider(document.id);
  };

  const handleOnRequestRefuser = () => {
    if (onRequestRefuser && document && document.id) onRequestRefuser(document.id);
  };

  const handleResilierContrat = (): void => {
    if (document?.id) {
      updateDocumentStatut({
        variables: {
          input: {
            idDocument: document.id,
            commentaire: '',
            status: 'RESILIE',
          },
        },
      });
    }
    setOpenResilierDialog(false);
  };

  return (
    <>
      <MenuList className={classes.menuList}>
        {'CONTRAT' === document?.type ? (
          <>
            <MenuItem
              button
              disabled={'RESILIE' === document?.dernierChangementStatut?.status}
              onClick={() => setOpenResilierDialog(true)}
            >
              <HighlightOff />
              <Typography variant="body2">Résilier le contrat</Typography>
            </MenuItem>
            <MenuItem button onClick={handleReplace}>
              <Flip />
              <Typography variant="body2">Modifier</Typography>
            </MenuItem>
            {user && user.id === document?.createdBy?.id && (
              <MenuItem
                button
                onClick={() => {
                  setOpenDeleteDocumentConfirm(true);
                }}
              >
                <DeleteForever />
                <Typography variant="body2">Supprimer</Typography>
              </MenuItem>
            )}
          </>
        ) : (
          <>
            {document?.type !== 'FACTURE' && (
              <>
                <MenuItem button disabled={document?.favoris} onClick={handleAddToFavorite}>
                  <Favorite />
                  <Typography variant="body2">Ajouter aux Favoris</Typography>
                </MenuItem>
                <MenuItem button disabled={!document?.favoris} onClick={handleRemoveFromFavorite}>
                  <FavoriteBorder />
                  <Typography variant="body2">Retirer des Favoris</Typography>
                </MenuItem>
                <MenuItem button onClick={handleReplace}>
                  <Flip />
                  <Typography variant="body2">Modifier</Typography>
                </MenuItem>
              </>
            )}
            {user && user.id === document?.createdBy?.id && (
              <MenuItem
                button
                onClick={() => {
                  setOpenDeleteDocumentConfirm(true);
                }}
              >
                <DeleteForever />
                <Typography variant="body2">Supprimer</Typography>
              </MenuItem>
            )}
          </>
        )}
        {isOpenDetail && isMobile && (
          <>
            <MenuItem button>
              <Check />
              <Typography variant="body2" onClick={handleOnRequestValider}>
                Valider
              </Typography>
            </MenuItem>
            <MenuItem button>
              <Close />
              <Typography variant="body2" onClick={handleOnRequestRefuser}>
                Refuser
              </Typography>
            </MenuItem>
          </>
        )}
      </MenuList>

      <CreateDocument
        open={openReplaceDocument}
        setOpen={setOpenReplaceDocument}
        title={'espace-facture' === espaceType ? 'Modification de la facture' : 'Modification de document'}
        saving={saving}
        activeSousCategorie={activeSousCategorie}
        document={document as any}
        mode="remplacement"
        onRequestCreateDocument={onRequestReplaceDocument as any}
        searchVariables={searchVariables}
        defaultType={'espace-facture' === espaceType ? 'FACTURE' : undefined}
        tvas={tvas}
        defaultCorrespondant={defaultCorrespondant}
      />

      <CustomModal
        open={openResilierDialog}
        setOpen={setOpenResilierDialog}
        disableBackdropClick
        title="Résilier contrat"
        closeIcon
        withBtnsActions
        headerWithBgColor={false}
        actionButtonTitle="Resilier"
        fullWidth
        onClickConfirm={handleResilierContrat}
        maxWidth="sm"
        fullScreen={!!isMobile}
      >
        <Box className={classes.resilierBox}>
          <Warning style={{ color: '#ffeb3b' }} />
          <Typography className={classes.textResilier}>Êtes-vous sûr de vouloir résilier le contrat ?</Typography>
        </Box>
      </CustomModal>

      <ModalDocument
        open={openDetails}
        onClose={() => setOpenDetails(false)}
        handleClose={() => setOpenDetails(false)}
        label="Détails"
        scroll="paper"
      >
        <InfoDocument
          document={{
            loading: false,
            error: false as any,
            data: document as any,
          }}
        />
      </ModalDocument>

      <ConfirmDeleteDialog
        open={openDeleteDocumentConfirm}
        setOpen={setOpenDeleteDocumentConfirm}
        title={`Suppression de ${'CONTRAT' === document?.type ? 'contrat' : 'document'}`}
        onClickConfirm={handleDelete}
      />
    </>
  );
};

export default DocumentCardMenu;
