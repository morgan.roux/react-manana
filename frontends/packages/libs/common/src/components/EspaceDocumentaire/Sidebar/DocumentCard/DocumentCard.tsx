import { useApplicationContext } from '@lib/common';
import {
  Box,
  Card,
  Chip,
  CircularProgress,
  IconButton,
  Menu,
  SvgIcon,
  SvgIconProps,
  Typography,
} from '@material-ui/core';
import { ChatBubble, GetApp, MoreHoriz, ThumbUp, Visibility } from '@material-ui/icons';
import classNames from 'classnames';
import moment from 'moment';
import React, { FC, CSSProperties } from 'react';
import { IDocumentSearch, User } from '../../EspaceDocumentaire';
import { DocumentCardMenu } from '../DocumentCardMenu';
import useStyles from './style';

import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import EventIcon from '@material-ui/icons/Event';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { FullDocumentCategorieFragment } from './../../../../federation';

const ContentCopy = (props: SvgIconProps) => (
  <SvgIcon {...props} viewBox="0 0 24 24">
    <path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z" />
  </SvgIcon>
);

interface DocumentCardProps {
  activeSousCategorie: string;
  data: FullDocumentCategorieFragment;
  loading: boolean;
  style?: CSSProperties;
  saving: boolean;
  saved: boolean;
  setSaved: (newValue: boolean) => void;
  onRequestAddToFavorite: (document: FullDocumentCategorieFragment) => void;
  onRequestRemoveFromFavorite: (document: FullDocumentCategorieFragment) => void;
  onRequestDelete: (document: FullDocumentCategorieFragment) => void;
  onRequestReplaceDocument: (document: FullDocumentCategorieFragment) => void;
  onRequestDeplace: (document: FullDocumentCategorieFragment) => void;
  onClick: (event: any) => void;
  active?: boolean;
  searchVariables?: IDocumentSearch;
  espaceType?: string;
  tvas: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  defaultCorrespondant?: any;
  timeLaps?: string;
}

export const DocumentCard = React.forwardRef<HTMLDivElement, DocumentCardProps>(
  (
    {
      activeSousCategorie,
      data,
      saving,
      saved,
      setSaved,
      onRequestDeplace,
      onRequestAddToFavorite,
      onRequestRemoveFromFavorite,
      onRequestReplaceDocument,
      onRequestDelete,
      onClick,
      active,
      searchVariables,
      espaceType,
      tvas,
      defaultCorrespondant,
      loading,
      style,
      timeLaps,
    },
    containerRef
  ) => {
    const classes = useStyles({});
    const [openOptions, setOpenOptions] = React.useState<boolean>(false);
    const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

    const handleCloseOptions = () => {
      setOpenOptions(false);
      setAnchorEl(null);
    };

    const handleClickOptions = (event: React.MouseEvent) => {
      event.preventDefault();
      event.stopPropagation();
      setAnchorEl(event.currentTarget as HTMLElement);
      setOpenOptions((prev) => !prev);
    };

    const { isMobile } = useApplicationContext();

    const open = Boolean(openOptions);

    // console.log('searchVariables', searchVariables);

    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.up('sm'));

    if (loading) {
      return (
        <div style={style} ref={containerRef}>
          Chargement ...
        </div>
      );
    }

    return (
      <>
        <div ref={containerRef} style={{ ...style, padding: '2px 4px' }}>
          <Card
            className={classNames(classes.root, { [classes.active]: active })}
            classes={{ root: classes.card }}
            elevation={active ? 0 : 1}
          >
            <Box onClick={onClick}>
              <Box className={classNames(classes.libelleContainer, classes.padding)}>
                <Typography color="secondary" variant="subtitle2" gutterBottom>
                  {'FACTURE' === data?.type
                    ? searchVariables?.libelleSousEspace
                      ? searchVariables?.libelleSousEspace
                      : 'Facture'
                    : 'CONTRAT' === data?.type
                    ? 'RESILIE' === data?.dernierChangementStatut?.status
                      ? 'Contrat résilié'
                      : 'Contrat'
                    : data?.sousCategorie?.libelle}
                </Typography>
                <Box style={{ display: 'flex' }}>
                  <Typography style={{ fontSize: 14, fontFamily: 'Roboto', color: '#9E9E9E' }}>{timeLaps}</Typography>
                  <span onClick={handleClickOptions}>
                    <MoreHoriz />
                  </span>
                </Box>
              </Box>
              {matches ? (
                <Box className={classNames(classes.padding)}>
                  <span className={classNames({ [classes.bold]: !isMobile }, { [classes.light]: isMobile })}>
                    <div dangerouslySetInnerHTML={{ __html: data.description || '' }} style={{ textAlign: 'left' }} />
                  </span>
                </Box>
              ) : (
                <Box>
                  <Box className={classNames(classes.padding)}>
                    <span className={classNames({ [classes.bold]: !isMobile }, { [classes.light]: isMobile })}>
                      <div
                        dangerouslySetInnerHTML={{ __html: data.description || '' }}
                        style={{ textAlign: 'left', fontSize: '14px', color: '#424242', fontFamily: 'Roboto' }}
                      />
                    </span>
                  </Box>
                  <Box marginBottom={2} className={classes.affichageDateMobile}>
                    <Box display="flex">
                      <EventIcon />
                      <Typography
                        component="span"
                        variant="subtitle2"
                        color="textSecondary"
                        style={{ marginLeft: '8px' }}
                      >
                        {data?.factureDate}
                      </Typography>
                    </Box>
                    <IconButton onClick={handleClickOptions}>
                      <ChevronRightIcon />
                    </IconButton>
                  </Box>
                </Box>
              )}
              {isMobile && <Box className={classes.separateur} />}
              {matches ? (
                'FACTURE' === data?.type ? (
                  <Box className={classNames(classes.details, classes.padding)} marginTop={isMobile ? 0 : 2}>
                    <Box marginBottom={2} className={classes.affichageMobile}>
                      <Typography component="span" variant="caption" color="textSecondary">
                        Facture n°&nbsp;:&nbsp;
                      </Typography>
                      <Typography component="span" variant="subtitle2" color="textSecondary">
                        {data?.numeroFacture}
                      </Typography>
                    </Box>
                    {/* <Box marginBottom={2}>
                  <Typography component="span" variant="caption" color="textSecondary">
                    Total TTC&nbsp;:&nbsp;
                  </Typography>
                  <Typography component="span" variant="subtitle2" color="textSecondary">
                    {(info?.factureTotalTtc || 0).toFixed(2)}&nbsp;€
                  </Typography>
                </Box> */}
                    <Box marginBottom={2} display="flex">
                      <Typography component="span" variant="caption" color="textSecondary">
                        Date :
                      </Typography>
                      <Typography component="span" variant="subtitle2" color="textSecondary" style={{ marginLeft: 16 }}>
                        {moment(data?.factureDate).format('DD/MM/YYYY')}
                      </Typography>
                    </Box>
                    {/* <Box>
                  <Typography component="span" variant="caption" color="textSecondary">
                    TVA&nbsp;:&nbsp;
                  </Typography>
                  <Typography component="span" variant="subtitle2" color="textSecondary">
                    {(info?.factureTva || 0).toFixed(2)}&nbsp;€
                  </Typography>
                </Box> */}
                    <Box className={classes.affichageMobile}>
                      <Typography component="span" variant="caption" color="textSecondary">
                        Origine :
                      </Typography>
                      <Typography component="span" variant="subtitle2" color="textSecondary">
                        {(data?.origineAssocie as any)?.nom || (data?.origineAssocie as any)?.fullName}
                      </Typography>
                    </Box>
                    {/* <Box>
                  <Typography component="span" variant="caption" color="textSecondary">
                    Total HT&nbsp;:&nbsp;
                  </Typography>
                  <Typography component="span" variant="subtitle2" color="textSecondary">
                    {(info?.factureTotalHt || 0).toFixed(2)}&nbsp;€
                  </Typography>
                </Box> */}
                    <Box className={classes.affichageMobile}>
                      <Typography component="span" variant="caption" color="textSecondary">
                        Total TTC&nbsp;:&nbsp;
                      </Typography>
                      <Typography component="span" variant="subtitle2" color="textSecondary">
                        {(data?.factureTotalTtc || 0).toFixed(2)}&nbsp;€
                      </Typography>
                    </Box>
                  </Box>
                ) : 'CONTRAT' === data?.type ? (
                  <Box className={classNames(classes.details, classes.padding)} marginTop={isMobile ? 0 : 2}>
                    <Box marginBottom={2} className={classes.affichageMobile}>
                      <Typography component="span" variant="caption" color="textSecondary">
                        Type&nbsp;:&nbsp;
                      </Typography>
                      <Typography component="span" variant="subtitle2" color="textSecondary">
                        Contrat
                      </Typography>
                    </Box>
                    <Box marginBottom={2} className={classes.affichageMobile}>
                      {moment(data?.dateHeureFinValidite).isValid() && (
                        <Typography component="span" variant="caption" color="textSecondary">
                          Date fin&nbsp;:&nbsp;
                        </Typography>
                      )}
                      {moment(data?.dateHeureFinValidite).isValid() && (
                        <Typography component="span" variant="subtitle2" color="textSecondary">
                          {moment(data?.dateHeureFinValidite).format('DD/MM/YYYY')}
                        </Typography>
                      )}
                    </Box>
                    <Box className={classes.affichageMobile}>
                      <Typography component="span" variant="caption" color="textSecondary">
                        Origine&nbsp;:&nbsp;
                      </Typography>
                      <Typography component="span" variant="subtitle2" color="textSecondary">
                        {(data?.origineAssocie as any)?.nom || (data?.origineAssocie as any)?.fullName}
                      </Typography>
                    </Box>
                    <Box className={classes.affichageMobile}>
                      <Typography component="span" variant="caption" color="textSecondary">
                        Date début&nbsp;:&nbsp;
                      </Typography>
                      <Typography component="span" variant="subtitle2" color="textSecondary">
                        {moment(data?.dateHeureDebutValidite).format('DD/MM/YYYY')}
                      </Typography>
                    </Box>
                  </Box>
                ) : (
                  <Box className={classNames(classes.details, classes.padding)} marginTop={isMobile ? 0 : 2}>
                    <Box marginBottom={2} className={classes.affichageMobile}>
                      <Typography component="span" variant="caption" color="textSecondary">
                        Rédacteur&nbsp;:&nbsp;
                      </Typography>
                      <Typography component="span" variant="subtitle2" color="textSecondary">
                        {data?.redacteur?.fullName}
                      </Typography>
                    </Box>
                    <Box marginBottom={2} className={classes.affichageMobile}>
                      <Typography component="span" variant="caption" color="textSecondary">
                        Nomenclature&nbsp;:&nbsp;
                      </Typography>
                      <Typography component="span" variant="subtitle2" color="textSecondary">
                        {data.nomenclature}
                      </Typography>
                    </Box>
                    <Box className={classes.affichageMobile}>
                      <Typography component="span" variant="caption" color="textSecondary">
                        Vérificateur&nbsp;:&nbsp;
                      </Typography>
                      <Typography component="span" variant="subtitle2" color="textSecondary">
                        {data?.verificateur?.fullName}
                      </Typography>
                    </Box>
                    <Box className={classes.affichageMobile}>
                      <Typography component="span" variant="caption" color="textSecondary">
                        N° version&nbsp;:&nbsp;
                      </Typography>
                      <Typography component="span" variant="subtitle2" color="textSecondary">
                        {data.numeroVersion}
                      </Typography>
                    </Box>
                  </Box>
                )
              ) : (
                <Box className={classNames(classes.details, classes.padding)} marginTop={isMobile ? 0 : 2}>
                  <Box marginBottom={2} className={classes.affichageMobile}>
                    <Typography component="span" variant="caption" color="textSecondary">
                      Facture n°&nbsp;:&nbsp;
                    </Typography>
                    <Typography component="span" variant="subtitle2" color="textSecondary">
                      {data?.numeroFacture}
                    </Typography>
                  </Box>
                  <Box className={classes.affichageMobile}>
                    <Typography component="span" variant="caption" color="textSecondary">
                      Origine&nbsp;:&nbsp;
                    </Typography>
                    <Typography component="span" variant="subtitle2" color="textSecondary">
                      {(data?.origineAssocie as any)?.nom || (data?.origineAssocie as any)?.fullName}
                    </Typography>
                  </Box>
                  <Box className={classes.affichageMobile}>
                    <Typography component="span" variant="caption" color="textSecondary">
                      Total TTC&nbsp;:&nbsp;
                    </Typography>
                    <Typography component="span" variant="subtitle2" color="textSecondary">
                      {(data?.factureTotalTtc || 0).toFixed(2)}&nbsp;€
                    </Typography>
                  </Box>
                </Box>
              )}
              {isMobile && <Box className={classes.separateur} />}
              <Box marginTop={isMobile ? -1 : 1} className={classNames(classes.status, classes.padding)}>
                <Chip
                  icon={<Visibility />}
                  label={
                    <Typography variant="subtitle2" color="textSecondary" component="span">
                      {data?.nombreConsultations}
                    </Typography>
                  }
                />
                <Chip
                  icon={<ThumbUp />}
                  label={
                    <Typography variant="subtitle2" color="textSecondary" component="span">
                      {data?.nombreReactions}
                    </Typography>
                  }
                />
                <Chip
                  icon={<ChatBubble />}
                  label={
                    <Typography variant="subtitle2" color="textSecondary" component="span">
                      {data?.nombreCommentaires}
                    </Typography>
                  }
                />
                <Chip
                  icon={<GetApp />}
                  label={
                    <Typography variant="subtitle2" color="textSecondary" component="span">
                      {data?.nombreTelechargements}
                    </Typography>
                  }
                />
              </Box>
            </Box>
          </Card>
        </div>
        <Menu
          open={open}
          anchorEl={anchorEl}
          onClose={handleCloseOptions}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
        >
          <DocumentCardMenu
            setOpenOptions={setOpenOptions}
            saving={saving}
            saved={saved}
            setSaved={setSaved}
            activeSousCategorie={activeSousCategorie}
            document={data as any}
            onRequestDelete={onRequestDelete}
            onRequestAddToFavorite={onRequestAddToFavorite}
            onRequestRemoveFromFavorite={onRequestRemoveFromFavorite}
            onRequestReplaceDocument={onRequestReplaceDocument}
            onRequestDeplace={onRequestDeplace}
            searchVariables={searchVariables}
            espaceType={espaceType}
            tvas={tvas}
            defaultCorrespondant={defaultCorrespondant}
          />
        </Menu>
      </>
    );
  }
);

export default DocumentCard;
