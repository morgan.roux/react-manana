import { useApplicationContext } from '@lib/common';
import { CustomModal } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC, useMemo } from 'react';
import { useHistory } from 'react-router';
import Content, { IContent } from '../Content/Content';
import Sidebar, { SideBarProps } from '../Sidebar/Sidebar';
import classnames from 'classnames';
import useStyles from './styles';
import { FullDocumentCategorieFragment } from '../../../federation';

export interface MainContainerProps {
  sideBarProps: SideBarProps;
  contentProps: IContent;
  mainContainerClassName?: string;
}

export const MainContainer: FC<MainContainerProps> = ({ sideBarProps, contentProps, mainContainerClassName }) => {
  const classes = useStyles();
  const history = useHistory();

  const setOpenModalContent = () => {
    history.push(sideBarProps.espaceType === 'espace-documentaire' ? '/espace-documentaire' : '/espace-facturation');
  };

  const { isMobile } = useApplicationContext();

  const content = useMemo(
    () => (
      <Content
        saving={contentProps.saving}
        saved={contentProps.saved}
        setSaved={contentProps.setSaved}
        activeSousCategorie={contentProps.activeSousCategorie}
        activeDocument={contentProps.activeDocument}
        mobileView={contentProps.mobileView}
        tabletView={contentProps.tabletView}
        validation
        content={contentProps.content}
        commentaires={contentProps.commentaires}
        refetchComments={contentProps.refetchComments}
        handleSidebarMenu={contentProps.handleFilterMenu}
        handleFilterMenu={contentProps.handleFilterMenu}
        onRequestCreateDocument={contentProps.onRequestCreateDocument}
        onRequestAddToFavorite={contentProps.onRequestAddToFavorite}
        onRequestDelete={contentProps.onRequestDelete}
        onRequestRemoveFromFavorite={contentProps.onRequestRemoveFromFavorite}
        onRequestReplace={contentProps.onRequestReplace}
        onRequestFetchMoreComments={contentProps.onRequestFetchMoreComments}
        onRequestLike={contentProps.onRequestLike}
        onRequestDeplace={contentProps.onRequestDeplace}
        onCompleteDownload={contentProps.onCompleteDownload}
        refetchSmyleys={contentProps.refetchSmyleys}
        hidePartage
        hideRecommandation
        refetchDocument={contentProps.refetchDocument}
        documentIds={
          (sideBarProps.documents.data?.searchGedDocuments.data || []).map(({ id }) => id).filter((e) => e) as any
        }
        onRequestDocument={contentProps.onRequestDocument}
        onRequestFetchAll={contentProps.onRequestFetchAll}
        setSearchVariables={contentProps.setSearchVariables}
        searchVariables={contentProps.searchVariables}
        espaceType={sideBarProps.espaceType}
        tvas={contentProps.tvas}
        defaultCorrespondant={contentProps.defaultCorrespondant}
      />
    ),
    [contentProps]
  );

  const handleShowDetails = (document: FullDocumentCategorieFragment) => {
    sideBarProps.onRequestDetails(document);
    /*if (isMobile) {
      setOpenModalContent(true);
    }*/
  };

  return (
    <Box className={mainContainerClassName ? classnames(classes.root, mainContainerClassName) : classes.root}>
      <Box className={classes.listContainer}>
        <Sidebar
          onRequestDeplace={sideBarProps.onRequestDeplace}
          saving={sideBarProps.saving}
          saved={sideBarProps.saved}
          setSaved={sideBarProps.setSaved}
          searchVariables={sideBarProps.searchVariables}
          activeSousCategorie={sideBarProps.activeSousCategorie}
          setActiveDocument={sideBarProps.setActiveDocument}
          documents={sideBarProps.documents}
          onRequestAddToFavorite={sideBarProps.onRequestAddToFavorite}
          onRequestDelete={sideBarProps.onRequestDelete}
          onRequestDetails={handleShowDetails}
          onRequestRemoveFromFavorite={sideBarProps.onRequestRemoveFromFavorite}
          onRequestReplace={sideBarProps.onRequestReplace}
          onRequestGetComments={sideBarProps.onRequestGetComments}
          setSearchVariables={sideBarProps.setSearchVariables}
          onRequestCreateDocument={sideBarProps.onRequestCreateDocument}
          onRequestRefresh={sideBarProps.onRequestRefresh}
          document={contentProps.content.data}
          espaceType={sideBarProps.espaceType}
          tvas={sideBarProps.tvas}
          defaultCorrespondant={sideBarProps.defaultCorrespondant}
        />
      </Box>
      <Box className={classes.contentContainer}>{content}</Box>
      {isMobile && (
        <CustomModal
          // padding="padding16"
          open={contentProps.activeDocument}
          setOpen={setOpenModalContent}
          title={'espace-facture' === sideBarProps.espaceType ? 'Espace facturation' : 'Espace Documentaire'}
          closeIcon
          withBtnsActions={false}
          headerWithBgColor
          fullWidth
          maxWidth="md"
          fullScreen={!!isMobile}
        >
          {content}
        </CustomModal>
      )}
    </Box>
  );
};
