import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      height: '100%',
      marginTop: 50,
      [theme.breakpoints.up('lg')]: {
        marginTop: 0,
      },
    },
    listContainer: {
      width: '100%',
      border: 'none',
      [theme.breakpoints.up('md')]: {
        height: 'calc(100vh - 174px)',
        // overflowY: 'auto',
        borderLeft: `1px solid ${theme.palette.grey[400]}`,
        borderRight: `1px solid ${theme.palette.grey[400]}`,
        width: '480px',
      },
    },
    contentContainer: {
      width: '100%',
      height: '100%',
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'block',
        height: 'calc(100vh - 174px)',
      },
      overflow: 'auto',
    },
  })
);

export default useStyles;
