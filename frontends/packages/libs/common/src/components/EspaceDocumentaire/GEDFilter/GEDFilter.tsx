import {
  Box,
  Divider,
  FormControl,
  FormControlLabel,
  IconButton,
  MenuItem,
  Radio,
  RadioGroup,
  Slider,
  Tooltip,
  Typography,
} from '@material-ui/core';
import React, { FC, useState, useEffect, Dispatch, SetStateAction, ChangeEvent } from 'react';
import style from './style';
import TreeStructure from './TreeStructure';
import { CustomDatePicker, CustomSelect, DebouncedSearchInput, ErrorPage, NewCustomButton } from '@lib/ui-kit';
import { IDocumentSearch } from '../EspaceDocumentaire';
import { TreeItem, TreeView } from '@material-ui/lab';
import ReplayIcon from '@material-ui/icons/Replay';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import classNames from 'classnames';
import moment from 'moment';
import { FactureStatusSelect, MobileTopBar, OrigineInput, PartenaireSelect, useApplicationContext } from '@lib/common';
import { useGet_OriginesQuery } from '@lib/common/src/federation';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import filter_alt from '../../../assets/img/science_black_24dp.svg';
import CustomSelectCheckBox from '../../../../../ui-kit/src/components/atoms/CustomSelect/CustomSelectCheckBox';
import { ModeReglementInput } from '../NewGedForm/ModeReglementInput';
import { Refresh } from '@material-ui/icons';

const filterStatutsDocument = [
  {
    id: '1',
    libelle: 'Tous les documents',
    code: 'TOUT',
  },
  {
    id: '2',
    libelle: 'Documents à traiter',
    code: 'EN_ATTENTE_APPROBATION',
  },

  {
    id: '3',
    libelle: 'Documents validés',
    code: 'APPROUVE',
  },

  {
    id: '4',
    libelle: 'Documents refusés',
    code: 'REFUSE',
  },
];

interface RechercheAvance {
  accesRapide?: any;
  idReglementMode?: string;
  origine?: any;
  origineAssocie?: any;
}

export interface FilterRadioProps {
  valueFilterSatut?: any;
  onChangeFilterStatut: (value: any) => void;
  espaceType?: string;
}

export const FilterRadio: FC<FilterRadioProps> = ({ valueFilterSatut = 'TOUT', onChangeFilterStatut, espaceType }) => {
  /* const handleChangeFilterType = (__event: React.ChangeEvent<HTMLInputElement>, value: string) => {
     onChangeFilterBy(JSON.parse(value));
   };
 
   const handleChangeFilterTime = (__event: React.ChangeEvent<HTMLInputElement>, value: string) => {
     onChangeFilterTime(JSON.parse(value));
   };
   */

  const { isMobile } = useApplicationContext();

  const handleChangeFilterStatut = (__event: React.ChangeEvent<HTMLInputElement>, value: string) => {
    onChangeFilterStatut(value);
  };
  return (
    <>
      <FormControl style={isMobile ? { marginTop: 32 } : undefined} component="fieldset">
        <RadioGroup name="doc" value={valueFilterSatut} onChange={handleChangeFilterStatut}>
          {('espace-facture' === espaceType ? [] : filterStatutsDocument).map((value) => (
            <FormControlLabel
              value={value.code}
              control={<Radio />}
              label={value.libelle}
              checked={value.code === valueFilterSatut}
            />
          ))}
        </RadioGroup>
      </FormControl>

      {/*<FormControl style={{ marginBottom: 24 }} component="fieldset">
        <RadioGroup name="doc" value={valueFilterBy} onChange={handleChangeFilterType}>
          {filterDocumentProprietaire.map(value => (
            <FormControlLabel
              value={JSON.stringify(value)}
              control={<Radio />}
              label={value.libelle}
            />
          ))}
        </RadioGroup>
      </FormControl>
      <hr />
      <FormControl style={{ margin: '24px 0' }} component="fieldset">
        <RadioGroup name="doc" value={valueFilterTime} onChange={handleChangeFilterTime}>
          {filterDocumentFilterBy.map(value => (
            <FormControlLabel
              value={JSON.stringify(value)}
              control={<Radio />}
              label={value.libelle}
            />
          ))}
        </RadioGroup>
      </FormControl>
      <hr />*/}
    </>
  );
};

interface GedFilterProps {
  categories: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  factureLaboratoires?: any[];
  facturePartenaires?: any[];
  facturesRejeteesInterneCategories?: { annee: number; mois: number }[];
  facturesRejeteesPrestataireServiceCategories?: { annee: number; mois: number }[];
  activeSousCategorie: any | undefined;
  setActiveSousCategorie: (sousCategorie: any | undefined) => void;
  setSearchVariables: Dispatch<SetStateAction<IDocumentSearch>>;
  searchVariables: IDocumentSearch;
  onRequestRefresh: () => void;
  onGoBack: () => void;
  contratFournisseurs?: any[]; // Laboratoire ou PrestataireService

  espaceType?: string;
  setOpenDrawer?: React.Dispatch<React.SetStateAction<boolean>>;
  maxTTC?: number;
}

const GEDFilter: FC<GedFilterProps> = ({
  setActiveSousCategorie,
  setSearchVariables,
  searchVariables,
  categories,
  factureLaboratoires,
  facturePartenaires,
  onRequestRefresh,
  onGoBack,
  espaceType,
  contratFournisseurs,
  setOpenDrawer,
  maxTTC,
}) => {
  const classes = style();
  const [expanded, setExpanded] = useState<any>({
    accesRapide: true,
    pharmacie: false,
    partage: false,
    facture: true,
  });

  const accesRapide = searchVariables.accesRapide?.length ? searchVariables.accesRapide[0] : undefined;
  const isPaye =
    (searchVariables.accesRapide || []).some((acces) => acces === 'APPROUVE_PAYE') &&
    (searchVariables.accesRapide || []).some((acces) => acces === 'APPROUVE_COMPTABLE_PAYE');
  const isApprouve = !isPaye && (searchVariables.accesRapide || []).some((acces) => acces === 'APPROUVE');
  const isApprouveComptable =
    !isPaye && (searchVariables.accesRapide || []).some((acces) => acces === 'APPROUVE_COMPTABLE');

  const { isMobile, user, federation, useParameterValueAsBoolean, useParameterValueAsString } = useApplicationContext();
  const isPrestataire = !!user?.prestataireService?.id;
  const initialRecherche = isPrestataire ? { accesRapide: { APPROUVE: true } } : undefined;

  const origines = useGet_OriginesQuery({
    client: federation,
  });
  const idOrigineLaboratoire = (origines.data?.origines.nodes || []).find(
    ({ code }: any) => code === 'LABORATOIRE'
  )?.id;
  const idOriginePrestataireService = (origines.data?.origines.nodes || []).find(
    ({ code }: any) => code === 'PRESTATAIRE_SERVICE'
  )?.id;

  const enableValidationPrestataire = useParameterValueAsBoolean('0850');
  const idPrestataireValidateur = useParameterValueAsString('0851');
  const enableWorkflowPaye = useParameterValueAsBoolean('0852');

  const [currentSousCategorie, setCurrentSousCategorie] = useState<any | undefined>();
  const [partagerSousCategorie, setPartagerSousCategorie] = useState<any[]>([]);
  const [pharmacieSousCategorie, setPharmacieSousCategorie] = useState<any[]>([]);
  // const [valueFilterTime, setValueFilterTime] = useState<string | undefined>();
  const [valueFilterStatut, setValueFilterStatut] = useState<string | undefined>();

  const [showRechercheAvance, setShowRechercheAvance] = useState<boolean>(true);
  const [recherches, setRecherches] = useState<RechercheAvance | undefined>(initialRecherche);

  useEffect(() => {
    if (
      isPrestataire &&
      (!recherches?.accesRapide || !Object.keys(recherches?.accesRapide).some((key) => recherches?.accesRapide[key]))
    ) {
      setRecherches((prev) => ({ ...prev, accesRapide: initialRecherche?.accesRapide }));
    }
  }, [recherches?.accesRapide]);

  // console.log('rechercheav', recherches);

  useEffect(() => {
    if (currentSousCategorie) {
      setActiveSousCategorie(currentSousCategorie);
    }
  }, [currentSousCategorie]);

  useEffect(() => {
    setValueFilterStatut((searchVariables.accesRapide?.length && searchVariables.accesRapide[0]) || 'TOUT');
  }, [searchVariables]);

  useEffect(() => {
    const _partageSousCategorie: any = [];
    const _pharmacieSousCategorie: any = [];
    (categories.data || []).map((categorie) => {
      const sousCategoriePartage: any = [];
      const sousCategoriePharmacie: any = [];
      for (let sousCategorie of categorie.mesSousCategories || []) {
        if (sousCategorie.idPartenaireValidateur) {
          sousCategoriePartage.push(sousCategorie as never);
        } else {
          sousCategoriePharmacie.push(sousCategorie as never);
        }
      }
      if (sousCategoriePartage.length > 0)
        _partageSousCategorie.push({
          ...categorie,
          mesSousCategories: sousCategoriePartage,
        } as never);

      if (sousCategoriePharmacie.length > 0)
        _pharmacieSousCategorie.push({
          ...categorie,
          mesSousCategories: sousCategoriePharmacie,
        } as never);
    });
    setPartagerSousCategorie(_partageSousCategorie);
    setPharmacieSousCategorie(_pharmacieSousCategorie);
  }, [categories.data]);

  const handleExpandAccordion = (id: string) => (_: React.ChangeEvent<{}>, expanded: boolean) => {
    setExpanded((prevState: any) => ({ ...prevState, [id]: expanded }));
  };

  const handleRechercheAvanceClick = () => {
    setShowRechercheAvance(!showRechercheAvance);
  };

  const handleSousCategorieClick = (sousCategorie: any): void => {
    setCurrentSousCategorie(sousCategorie);
    handleChangeSearchVariables({
      accesRapide: undefined,
      idSousCategorie: sousCategorie?.id as any,
      type: 'AUTRES',
    });
  };

  const quickAccess = (type: string): void => {
    handleChangeSearchVariables({
      accesRapide: type === 'TOUT' ? undefined : [type as any],
      type: 'espace-facture' === espaceType ? 'FACTURE' : 'AUTRES',
    });
  };

  // const handleTextToSearchChange = (value: string): void => {
  //   setTextToSearch(value);
  //   setSearchVariables((prev) => ({
  //     ...prev,
  //     idSousCategorie: undefined,
  //     accesRapide: undefined,
  //     libelleSousEspace: undefined,
  //     searchText: value,
  //   }));
  //   setOpenDrawer && setOpenDrawer(false);
  // };

  const handleChangeSearchVariables = (newValues: IDocumentSearch): void => {
    setSearchVariables((prev) => ({
      ...prev,
      idSousCategorie: undefined,
      filterBy: 'TOUT',
      sortBy: undefined,
      type: undefined,
      validation: 'TOUT',
      idOrigine: undefined,
      idOrigineAssocie: undefined,
      idReglementMode: undefined,
      dateDebut: undefined,
      dateFin: undefined,
      libelleSousEspace: undefined,
      accesRapide: undefined,
      montantMinimal: undefined,
      // montantMaximal: undefined,
      searchText: undefined,
      ...newValues,
    }));
    setOpenDrawer && setOpenDrawer(false);
  };

  const handleChangeFilterMontant = (_event: any, newValue: any) => {
    if (newValue?.length === 2) {
      setSearchVariables((prev) => ({
        ...prev,
        montantMinimal: newValue[0],
        montantMaximal: newValue[1],
      }));
    }
  };

  const handleChangeFilterDate = (name: string, value: any) => {
    setSearchVariables((prev) => ({
      ...prev,
      dateDebut: name === 'dateDebut' ? moment(value).toISOString() : searchVariables.dateDebut,
      dateFin: name === 'dateDebut' ? searchVariables.dateFin : moment(value).toISOString(),
    }));
  };

  const handleChangeRecherche = (name: string, value?: any) => {
    setRecherches((prev) => ({ ...prev, [name]: value }));
  };

  const handleChangeAcces = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRecherches((prev) => ({
      ...prev,
      accesRapide: { ...(prev?.accesRapide || {}), [event.target.name]: event.target.checked },
    }));
  };

  const handleSearch = (e: React.MouseEvent<HTMLElement>) => {
    e.stopPropagation();
    e.preventDefault();

    const accesRapide = recherches?.accesRapide
      ? Object.entries(recherches?.accesRapide)
          .filter((entry) => entry[1])
          .map((entry) => entry[0])
      : [];
    handleChangeSearchVariables({
      accesRapide: accesRapide as any,
      idReglementMode: recherches?.idReglementMode,
      idOrigine: recherches?.origine?.id,
      idOrigineAssocie: recherches?.origineAssocie?.id,
      type: 'FACTURE',
    });
  };
  const handleReinitialSearch = (e: React.MouseEvent<HTMLElement>) => {
    e.stopPropagation();
    e.preventDefault();

    setRecherches(initialRecherche);
    handleChangeSearchVariables({
      type: 'FACTURE',
    });
  };

  const withPrestataireServiceValidateur = enableValidationPrestataire && idPrestataireValidateur;

  return (
    <Box>
      {!categories.loading &&
        (!categories?.error && categories?.data ? (
          <>
            {!isMobile ? (
              <>
                <Box marginBottom={3} marginTop={3}>
                  {'espace-facture' === espaceType && (
                    <>
                      <Box className={classes.filterSubtitle}>Par période</Box>
                      <Box className={classes.flex}>
                        <CustomDatePicker
                          label="De"
                          value={searchVariables.dateDebut}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          onChange={(value) => {
                            handleChangeFilterDate('dateDebut', value);
                          }}
                          fullWidth
                          className={classes.dateField}
                        />
                        <CustomDatePicker
                          label="À"
                          value={searchVariables.dateFin}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          onChange={(value) => {
                            handleChangeFilterDate('dateFin', value);
                          }}
                          fullWidth
                          className={classes.dateField}
                        />
                      </Box>

                      <Typography className={classes.filterSubtitle} id="range-slider">
                        Par montant
                      </Typography>
                      <Box display="flex" justifyContent="space-between">
                        <Box>
                          min :{' '}
                          <span className={classes.filterSubtitle}>
                            {(searchVariables.montantMinimal || 0).toFixed(2)}&nbsp;€
                          </span>
                        </Box>
                        <Box>
                          max :{' '}
                          <span className={classes.filterSubtitle}>
                            {(searchVariables.montantMaximal || 0).toFixed(2)}&nbsp;€
                          </span>
                        </Box>
                      </Box>
                      <Slider
                        value={[searchVariables.montantMinimal || 0, searchVariables.montantMaximal || 0]}
                        onChange={handleChangeFilterMontant}
                        valueLabelDisplay="auto"
                        aria-labelledby="range-slider"
                        min={0}
                        max={maxTTC}
                      />
                      {/* <Box className={classes.rangeBox}>
                        <Box className={classes.textRange}>0.00 €</Box>
                        <Box className={classes.textRange}>{(maxTTC || 0).toFixed(2)} €</Box>
                      </Box> */}
                    </>
                  )}
                </Box>
              </>
            ) : (
              <>
                <MobileTopBar
                  title={espaceType === 'espace-facture' ? 'Espace facturation' : 'Espace documentaire'}
                  withBackBtn={true}
                  // handleDrawerToggle={handleDrawerToggle}
                  // optionBtn={optionBtn}
                  withTopMargin={true}
                  breakpoint="sm"
                  // backUrl={drawerBackUrl}
                  // closeUrl={drawerCloseUrl}
                  onClickBack={onGoBack}
                />
                <FilterRadio
                  //  onChangeFilterBy={handleChangeFilterType}
                  //  onChangeFilterTime={handleChangeFilterTime}
                  onChangeFilterStatut={quickAccess}
                  // valueFilterBy={valueFilterByDoc}
                  // valueFilterTime={valueFilterTime}
                  valueFilterSatut={valueFilterStatut}
                  espaceType={espaceType}
                />
              </>
            )}

            {espaceType === 'espace-facture' ? (
              <>
                <Box onClick={handleRechercheAvanceClick} className={classes.rechercheAvance}>
                  <Typography className={classes.rechercheTypography}>Recherche avancée</Typography>
                  <Box className={classes.rechercheIcons}>
                    {showRechercheAvance ? <AddCircleIcon /> : <CancelIcon />}
                  </Box>
                </Box>
                <Divider />
              </>
            ) : null}
            {!showRechercheAvance ? (
              <>
                <Box>
                  <FactureStatusSelect
                    accesRapide={recherches?.accesRapide}
                    onChange={handleChangeAcces}
                    isPrestataire={isPrestataire}
                  />
                </Box>
                {/* <Box>
                  <CustomSelect
                    label="Type Fournisseur"
                    listId="id"
                    index="label"
                    value={recherches?.origine || ''}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => handleChangeRecherche('origine', e.target.value)}
                    list={listFacture}
                  />
                </Box>
                <Box>
                  <PartenaireSelect
                    filterIcon={
                      <img src={filter_alt} alt="filter" style={{ filter: 'brightness(10) invert(1)', width: 20 }} />
                    }
                    value={recherches?.origineAssocie}
                    index="laboratoire"
                    onChange={(origineAssocie) => handleChangeRecherche('origineAssocie', origineAssocie)}
                    partenaireClassName={classes.partenaireClassName}
                  />
                </Box> */}
                <Box>
                  <OrigineInput
                    flexDirection="column"
                    onChangeOrigine={(origine: any) => {
                      handleChangeRecherche('origine', origine);
                    }}
                    origine={recherches?.origine}
                    origineAssocie={recherches?.origineAssocie}
                    onChangeOrigineAssocie={(origineAssocie) => {
                      handleChangeRecherche('origineAssocie', origineAssocie);
                    }}
                    enabledCodes={['CONFRERE', 'LABORATOIRE', 'PRESTATAIRE_SERVICE']}
                    withSelectAll
                  />
                </Box>
                <Box marginTop="24px">
                  <ModeReglementInput
                    label="Modalité de paiement"
                    idReglementMode={recherches?.idReglementMode}
                    onChange={(e: ChangeEvent<HTMLInputElement>) =>
                      handleChangeRecherche('idReglementMode', e.target.value)
                    }
                  />
                </Box>
                <Box style={{ display: 'flex', justifyContent: 'flex-end', marginBottom: '16px' }}>
                  {/* <IconButton className={classes.reinitialise} onClick={handleReinitialSearch}>
                    <Typography id="reinitial_id">Reinitialiser</Typography>
                    <RefreshIcon />
                  </IconButton> */}
                  <Tooltip title="Reinitialiser" className={classes.reinitialise} onClick={handleReinitialSearch}>
                    <Refresh />
                  </Tooltip>
                  <Box onClick={handleSearch}>
                    <NewCustomButton>Rechercher</NewCustomButton>
                  </Box>
                </Box>
              </>
            ) : (
              ''
            )}
            {'espace-facture' !== espaceType && (
              <TreeStructure
                summary="Accès rapide"
                expanded={expanded.accesRapide === true}
                onChange={handleExpandAccordion('accesRapide')}
                id="acces-rapide"
              >
                <MenuItem
                  style={{ marginBottom: 8 }}
                  className={classNames(
                    {
                      [classes.active]: searchVariables.accesRapide?.includes('FAVORIS'),
                    },
                    classes.fontMenu
                  )}
                  onClick={() => quickAccess('FAVORIS')}
                >
                  Document Favoris
                </MenuItem>
                <MenuItem
                  style={{ marginBottom: 8 }}
                  className={classNames(
                    {
                      [classes.active]: searchVariables.accesRapide?.includes('NOUVEAUX'),
                    },
                    classes.fontMenu
                  )}
                  onClick={() => quickAccess('NOUVEAUX')}
                >
                  Nouveaux document
                </MenuItem>
                <MenuItem
                  style={{ marginBottom: 8 }}
                  className={classNames(
                    {
                      [classes.active]: searchVariables.accesRapide?.includes('RECENTS'),
                    },
                    classes.fontMenu
                  )}
                  onClick={() => quickAccess('RECENTS')}
                >
                  Document récents
                </MenuItem>
                <MenuItem
                  style={{ marginBottom: 8 }}
                  className={classNames(
                    {
                      [classes.active]: searchVariables.accesRapide?.includes('PLUS_CONSULTES'),
                    },
                    classes.fontMenu
                  )}
                  onClick={() => quickAccess('PLUS_CONSULTES')}
                >
                  Document les plus consultés
                </MenuItem>
                <MenuItem
                  className={classNames(
                    {
                      [classes.active]: searchVariables.accesRapide?.includes('PLUS_TELECHARGES'),
                    },
                    classes.fontMenu
                  )}
                  onClick={() => quickAccess('PLUS_TELECHARGES')}
                >
                  Document les plus téléchargés
                </MenuItem>
              </TreeStructure>
            )}

            {'espace-facture' === espaceType && (
              <TreeStructure
                summary={isPrestataire ? 'Espace des factures partenaires' : 'Espace facturation'}
                expanded={expanded.facture === true}
                onChange={handleExpandAccordion('facture')}
                id="espace-facture"
              >
                <TreeView>
                  {!isPrestataire && (
                    <MenuItem
                      className={classNames(
                        {
                          [classes.active]: searchVariables.type === 'FACTURE' && !searchVariables.accesRapide,
                        },
                        classes.fontMenu
                      )}
                      onClick={() => quickAccess('TOUT')}
                    >
                      Toutes les factures
                    </MenuItem>
                  )}
                  {!isPrestataire && (
                    <MenuItem
                      className={classNames(
                        {
                          [classes.active]:
                            searchVariables.type === 'FACTURE' && accesRapide === 'EN_ATTENTE_APPROBATION',
                        },
                        classes.fontMenu
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          type: 'FACTURE',
                          accesRapide: ['EN_ATTENTE_APPROBATION'],
                          libelleSousEspace: 'Nouvelle Facture',
                        })
                      }
                    >
                      Nouvelles Factures
                    </MenuItem>
                  )}
                  <MenuItem
                    className={classNames(
                      {
                        [classes.active]: searchVariables.type === 'FACTURE' && isApprouve,
                        // &&
                        // searchVariables.validation === 'INTERNE',
                      },
                      classes.fontMenu
                    )}
                    onClick={() =>
                      handleChangeSearchVariables({
                        accesRapide: ['APPROUVE', 'APPROUVE_PAYE'],
                        type: 'FACTURE',
                        // validation: 'INTERNE',
                        libelleSousEspace: 'Facture validée',
                      })
                    }
                  >
                    <span>{isPrestataire ? 'Nouvelles Factures' : 'Factures validées'}</span>
                  </MenuItem>
                  {!isPrestataire && (
                    <MenuItem
                      className={classNames(
                        {
                          [classes.active]: searchVariables.type === 'FACTURE' && accesRapide === 'REFUSE',
                          // &&
                          // (!withPrestataireServiceValidateur || searchVariables.validation === 'INTERNE'),
                        },
                        classes.fontMenu
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          // validation: withPrestataireServiceValidateur ? 'INTERNE' : undefined,
                          type: 'FACTURE',
                          accesRapide: ['REFUSE'],
                          libelleSousEspace: withPrestataireServiceValidateur ? 'Rejetée pharmacie' : 'Facture rejetée',
                        })
                      }
                    >
                      <span>{withPrestataireServiceValidateur ? 'Rejets pharmacie' : 'Factures rejetées'}</span>
                    </MenuItem>
                  )}
                  {/* {(withPrestataireServiceValidateur) && ( */}
                  <MenuItem
                    className={classNames(
                      {
                        [classes.active]: searchVariables.type === 'FACTURE' && isApprouveComptable,
                        // &&
                        // searchVariables.validation !== 'INTERNE',
                      },
                      classes.fontMenu
                    )}
                    onClick={() =>
                      handleChangeSearchVariables({
                        type: 'FACTURE',
                        accesRapide: ['APPROUVE_COMPTABLE', 'APPROUVE_COMPTABLE_PAYE'],
                        libelleSousEspace: 'Facture comptabilisée',
                      })
                    }
                  >
                    Factures comptabilisées
                  </MenuItem>
                  {/* )} */}
                  {/* {withPrestataireServiceValidateur && ( */}
                  <MenuItem
                    className={classNames(
                      {
                        [classes.active]: searchVariables.type === 'FACTURE' && accesRapide === 'REFUSE_COMPTABLE',
                        // &&
                        // searchVariables.validation === 'PRESTATAIRE_SERVICE',
                      },
                      classes.fontMenu
                    )}
                    onClick={() =>
                      handleChangeSearchVariables({
                        // validation: 'PRESTATAIRE_SERVICE',
                        type: 'FACTURE',
                        accesRapide: ['REFUSE_COMPTABLE'],
                        libelleSousEspace: isPrestataire ? `Facture rejetée` : `Rejetée comptable`,
                      })
                    }
                  >
                    <span>Rejets comptable</span>
                  </MenuItem>
                  {/* )} */}
                  {/* {withPrestataireServiceValidateur && ( */}
                  <MenuItem
                    style={{ marginBottom: 8 }}
                    className={classNames(
                      {
                        [classes.active]: searchVariables.type === 'FACTURE' && (accesRapide === 'PAYE' || isPaye),
                      },
                      classes.fontMenu
                    )}
                    onClick={() =>
                      handleChangeSearchVariables({
                        type: 'FACTURE',
                        accesRapide: enableWorkflowPaye ? ['PAYE'] : ['APPROUVE_PAYE', 'APPROUVE_COMPTABLE_PAYE'],
                        libelleSousEspace: 'Facture payée',
                      })
                    }
                  >
                    Factures payées
                  </MenuItem>
                  {/* )} */}
                  {!isPrestataire && (
                    <MenuItem
                      style={{ marginBottom: 8 }}
                      className={classNames(
                        {
                          [classes.active]: searchVariables.type === 'FACTURE' && accesRapide === 'POUBELLE',
                        },
                        classes.fontMenu
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          type: 'FACTURE',
                          accesRapide: ['POUBELLE'],
                          libelleSousEspace: 'Facture au Poubelle',
                        })
                      }
                    >
                      Poubelle
                    </MenuItem>
                  )}
                </TreeView>
              </TreeStructure>
            )}

            {!isPrestataire && 'espace-facture' !== espaceType && (
              <TreeStructure
                summary="Espace Documentaire"
                expanded={expanded.pharmacie === true}
                onChange={handleExpandAccordion('pharmacie')}
                id="espace-pharmacie"
              >
                <TreeView>
                  {(pharmacieSousCategorie || []).map((categorie: any, index: number) => {
                    const reducer = (accumulator: any, currentValue: any) => accumulator + currentValue.nombreDocuments;
                    const nombreDocumentCategorie = (categorie?.mesSousCategories || []).reduce(reducer, 0);
                    return (
                      <TreeItem
                        className={classes.treeItem}
                        key={categorie?.id || index}
                        label={
                          <span className={classes.fontMenu} style={{ paddingTop: 8, paddingBottom: 8 }}>
                            <span>{categorie.libelle}</span>{' '}
                            <span style={{ fontWeight: 'bold' }}>{nombreDocumentCategorie}</span>
                          </span>
                        }
                        nodeId={categorie?.id || index}
                        id={categorie?.id}
                        classes={{ label: classes.treeItemSelected }}
                      >
                        {(categorie?.mesSousCategories || []).map((sousCategorie: any, indice: number) => (
                          <MenuItem
                            style={{ marginTop: 16 }}
                            className={classNames(
                              {
                                [classes.active]: currentSousCategorie?.id === sousCategorie?.id,
                              },
                              classes.fontMenu
                            )}
                            onClick={() => handleSousCategorieClick(sousCategorie)}
                            key={sousCategorie?.id || indice}
                          >
                            <span>{sousCategorie.libelle}</span>{' '}
                            <span style={{ fontWeight: 'bold' }}>{sousCategorie.nombreDocuments}</span>
                          </MenuItem>
                        ))}
                      </TreeItem>
                    );
                  })}
                  <TreeItem
                    className={classes.treeItem}
                    label={
                      <span className={classes.fontMenu}>
                        <span>Contrat</span>
                      </span>
                    }
                    nodeId="espace-contrat"
                    id="espace-contrat"
                    classes={{ label: classes.treeItemSelected }}
                  >
                    {(contratFournisseurs || []).map((partenaire: any, indice: number) => (
                      <MenuItem
                        style={{ marginTop: 16 }}
                        className={classNames(
                          {
                            [classes.active]:
                              searchVariables.type === 'CONTRAT' && searchVariables.idOrigineAssocie === partenaire.id,
                          },
                          classes.fontMenu
                        )}
                        onClick={() =>
                          handleChangeSearchVariables({
                            type: 'CONTRAT',
                            idOrigineAssocie: partenaire.id,
                            validation: 'TOUT',
                            idOrigine:
                              partenaire.dataType === 'Laboratoire'
                                ? idOrigineLaboratoire
                                : idOriginePrestataireService,
                            libelleSousEspace: 'Contrat',
                          })
                        }
                        key={partenaire?.id || indice}
                      >
                        <span>{partenaire.nom}</span>
                      </MenuItem>
                    ))}
                  </TreeItem>
                </TreeView>
              </TreeStructure>
            )}

            {'espace-facture' === espaceType && !isPrestataire && (
              <TreeStructure
                summary="Factures fournisseurs"
                expanded={expanded.facturation === true}
                onChange={handleExpandAccordion('facturation')}
                id="espace-facturation"
              >
                <TreeView>
                  <TreeItem
                    className={classes.treeItem}
                    label={
                      <span className={classes.fontMenu} style={{ paddingTop: 8, paddingBottom: 8 }}>
                        <span>Laboratoire</span>
                      </span>
                    }
                    nodeId="espace-laboratoire"
                    id="espace-laboratoire"
                    classes={{ label: classes.treeItemSelected }}
                  >
                    <MenuItem
                      style={{ marginTop: 16 }}
                      className={classNames(
                        {
                          [classes.active]:
                            searchVariables.type === 'FACTURE' &&
                            searchVariables.idOrigine === idOrigineLaboratoire &&
                            !searchVariables.idOrigineAssocie,
                        },
                        classes.fontMenu
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          type: 'FACTURE',
                          validation: 'TOUT',
                          idOrigine: idOrigineLaboratoire,
                          libelleSousEspace: 'Laboratoire',
                        })
                      }
                    >
                      <span>Tous les laboratoires</span>
                    </MenuItem>
                    {(factureLaboratoires || []).map((partenaire: any, indice: number) => (
                      <MenuItem
                        style={{ marginTop: 16 }}
                        className={classNames(
                          {
                            [classes.active]:
                              searchVariables.type === 'FACTURE' && searchVariables.idOrigineAssocie === partenaire.id,
                          },
                          classes.fontMenu
                        )}
                        onClick={() =>
                          handleChangeSearchVariables({
                            type: 'FACTURE',
                            idOrigineAssocie: partenaire.id,
                            validation: 'TOUT',
                            idOrigine: idOrigineLaboratoire,
                            libelleSousEspace: 'Laboratoire',
                          })
                        }
                        key={partenaire?.id || indice}
                      >
                        <span>{partenaire.nom}</span>
                      </MenuItem>
                    ))}
                  </TreeItem>
                  <TreeItem
                    className={classes.treeItem}
                    label={
                      <span className={classes.fontMenu} style={{ paddingTop: 8, paddingBottom: 8 }}>
                        <span>Partenaire de service</span>
                      </span>
                    }
                    nodeId="espace-partenaire"
                    id="espace-partenaire"
                    classes={{ label: classes.treeItemSelected }}
                  >
                    <MenuItem
                      style={{ marginTop: 16 }}
                      className={classNames(
                        {
                          [classes.active]:
                            searchVariables.type === 'FACTURE' &&
                            searchVariables.idOrigine === idOriginePrestataireService &&
                            !searchVariables.idOrigineAssocie,
                        },
                        classes.fontMenu
                      )}
                      onClick={() =>
                        handleChangeSearchVariables({
                          type: 'FACTURE',
                          validation: 'TOUT',
                          idOrigine: idOriginePrestataireService,
                          libelleSousEspace: 'Partenaire de service',
                        })
                      }
                    >
                      <span>Tous</span>
                    </MenuItem>
                    {(facturePartenaires || []).map((partenaire: any, indice: number) => (
                      <MenuItem
                        style={{ marginTop: 16 }}
                        className={classNames(
                          {
                            [classes.active]:
                              searchVariables.type === 'FACTURE' && searchVariables.idOrigineAssocie === partenaire.id,
                          },
                          classes.fontMenu
                        )}
                        onClick={() =>
                          handleChangeSearchVariables({
                            type: 'FACTURE',
                            idOrigineAssocie: partenaire.id,
                            validation: 'TOUT',
                            idOrigine: idOriginePrestataireService,
                            libelleSousEspace: 'Partenaire de service',
                          })
                        }
                        key={partenaire?.id || indice}
                      >
                        <span>{partenaire.nom}</span>
                      </MenuItem>
                    ))}
                  </TreeItem>
                  <MenuItem
                    style={{ marginLeft: 8 }}
                    className={classNames(
                      {
                        [classes.active]:
                          searchVariables.type === 'FACTURE' && searchVariables.libelleSousEspace === 'Autre Tier',
                      },
                      classes.fontMenu
                    )}
                    onClick={() =>
                      handleChangeSearchVariables({
                        type: 'FACTURE',
                        validation: 'TOUT',
                        idOrigine: 'TIERS',
                        libelleSousEspace: 'Autre Tier',
                      })
                    }
                  >
                    <span style={{ paddingTop: 8, paddingBottom: 8 }}>Autres tiers</span>
                  </MenuItem>
                </TreeView>
              </TreeStructure>
            )}

            {'espace-facture' !== espaceType && (partagerSousCategorie?.length || 0) > 0 && (
              <TreeStructure
                summary="Espace partagé"
                expanded={expanded.partage === true}
                onChange={handleExpandAccordion('partage')}
                id="espace-partage"
              >
                <TreeView>
                  {(partagerSousCategorie || []).map((categorie: any, index: number) => {
                    const reducer = (accumulator: any, currentValue: any) => accumulator + currentValue.nombreDocuments;
                    const nombreDocumentCategorie = (categorie?.mesSousCategories || []).reduce(reducer, 0);
                    return (
                      <TreeItem
                        className={classes.treeItem}
                        key={categorie?.id || index}
                        label={
                          <span className={classes.fontMenu}>
                            <span>{categorie.libelle}</span>{' '}
                            <span style={{ fontWeight: 'bold' }}>{nombreDocumentCategorie}</span>
                          </span>
                        }
                        nodeId={categorie?.id || ''}
                        id={categorie?.id}
                        classes={{
                          label: classes.treeItemSelected,
                          iconContainer: classes.treeItemIconContainer,
                        }}
                      >
                        {(categorie?.mesSousCategories || []).map((sousCategorie: any, indice: number) => (
                          <MenuItem
                            style={{ marginTop: 16 }}
                            className={classNames(
                              {
                                [classes.active]: currentSousCategorie?.id === sousCategorie?.id,
                              },
                              classes.fontMenu
                            )}
                            onClick={() => handleSousCategorieClick(sousCategorie)}
                            key={sousCategorie?.id || indice}
                          >
                            <span>{sousCategorie.libelle}</span>{' '}
                            <span style={{ fontWeight: 'bold' }}>{sousCategorie.nombreDocuments}</span>
                          </MenuItem>
                        ))}
                      </TreeItem>
                    );
                  })}
                </TreeView>
              </TreeStructure>
            )}
          </>
        ) : (
          <ErrorPage />
        ))}
    </Box>
  );
};

export default GEDFilter;
