import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
  menu: {
    minWidth: 300,
    padding: theme.spacing(2),
  },
  treeItemSelected: {
    background: '#FFFFFF !important',
    paddingLeft: 15,
  },

  treeItemIconContainer: {
    display: 'none',
  },

  heading: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(1.5),
    paddingBottom: theme.spacing(1),
    borderBottom: `1px solid ${theme.palette.divider}`,
    justifyContent: 'space-between',
  },
  active: {
    backgroundColor: theme.palette.grey[200],
    color: theme.palette.common.black,
    height: 48,
    fontWeight: 600,
  },
  fontMenu: {
    fontSize: 14,
    fontFamily: 'Roboto',
    display: 'flex',
    justifyContent: 'space-between',
  },
  treeItem: {
    marginBottom: 8,
    '& li': {
      margin: 0,
      marginLeft: 30,
      padding: 0,
    },
  },
  flex: {
    display: 'flex',
    alignItems: 'center',
    '&>div': {
      flex: '1 1 calc(50% - 16px)',
      maxWidth: 'calc(50% - 8px)',
    },
    '&>div:last-of-type': {
      marginLeft: theme.spacing(2),
    },
  },
  filterSubtitle: {
    fontSize: 14,
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    marginTop: 16,
    marginBottom: 16,
  },
  rangeBox: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: 8,
    paddingTop: 0,
    paddingBottom: 16,
    borderBottom: 'solid 1px #2121211A',
  },
  textRange: {
    fontSize: 14,
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    color: '#21212133',
  },
  dateField: {
    '& button': {
      padding: '0px !important',
    },
    '& input': {
      fontSize: '14px !important',
    },
    '& div': {
      paddingRight: '4px !important',
    },
  },
  rechercheAvance: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: '24px',
    cursor: 'pointer',
  },
  rechercheTypography: {
    color: theme.palette.primary.main,
    fontSize: '16px',
    fontFamily: 'Roboto',
    fontWeight: 500,
  },
  rechercheIcons: {
    '& .main-MuiSvgIcon-root': {
      color: theme.palette.primary.main,
    },
  },
  partenaireClassName: {
    '& .main-MuiButton-root': {
      minWidth: '270px',
    },
  },
  reinitialise: {
    marginRight: '8px',
    fontSize: '40px',
  },
  classNameSelectCheckBoxMenu: {
    display: 'flex',
    marginLeft: '8px',
  },
  partenaireFilter: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
}));
