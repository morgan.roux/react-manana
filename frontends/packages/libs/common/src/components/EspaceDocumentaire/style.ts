import { makeStyles } from '@material-ui/core';

export default makeStyles({
  GEDContainer: {
    display: 'flex',
    width: '100%',
    overflow: 'auto',
    overflowY: 'hidden',
    height: '100%',
  },
  container: {
    display: 'flex',
    flex: 1,
    height: '100%',
  },
  sidebar: {
    marginTop: '-40px',
    height: 'calc(100vh - 174px)',
    overflowY: 'auto',
  },
  mainContainer: {
    flexWrap: 'nowrap !important' as any,
    marginTop: 'auto !important' as any,
  },
});
