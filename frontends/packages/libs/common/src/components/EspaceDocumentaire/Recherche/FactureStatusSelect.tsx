import React, { FC } from 'react';
import CustomSelectCheckBox from '../../../../../ui-kit/src/components/atoms/CustomSelect/CustomSelectCheckBox';
import { useStyles } from './style';

export interface FactureStatusSelectProps {
  label?: string;
  accesRapide?: any;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  required?: boolean;
  isPrestataire?: boolean;
}

const listEtatFacture = [
  { id: 'EN_ATTENTE_APPROBATION', label: 'Nouvelle facture' },
  { id: 'APPROUVE', label: 'Facture validée' },
  { id: 'REFUSE', label: 'Rejets pharmacie' },
  { id: 'REFUSE_COMPTABLE', label: 'Rejet comptable' },
  { id: 'APPROUVE_COMPTABLE', label: 'Comptabilisée' },
  { id: 'PAYE', label: 'Facture payée' },
];

const listEtatFacturePrestataire = [
  { id: 'APPROUVE', label: 'Nouvelle facture' },
  { id: 'REFUSE_COMPTABLE', label: 'Rejet comptable' },
  { id: 'APPROUVE_COMPTABLE', label: 'Comptabilisée' },
  { id: 'PAYE', label: 'Facture payée' },
];

export const FactureStatusSelect: FC<FactureStatusSelectProps> = ({
  required = false,
  accesRapide,
  onChange,
  label,
  isPrestataire = false,
}) => {
  const classes = useStyles();
  return (
    <CustomSelectCheckBox
      label={label ?? 'Etat de facture'}
      listId="id"
      index="label"
      required={required}
      selected={accesRapide}
      onChange={onChange}
      list={isPrestataire ? listEtatFacturePrestataire : listEtatFacture}
      defaultValue={isPrestataire ? 'Nouvelle facture' : 'Toutes les Factures'}
      useCheckbox={true}
      classNameSelectCheckBoxMenu={classes.classNameSelectCheckBoxMenu}
    />
  );
};
