import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) => ({
  classNameSelectCheckBoxMenu: {
    display: 'flex',
    marginLeft: '8px',
  },
}));
