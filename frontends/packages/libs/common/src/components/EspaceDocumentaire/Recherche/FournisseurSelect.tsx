import React, { FC } from 'react';
import { CustomSelect } from '@lib/ui-kit';
import { useGet_OriginesQuery } from '@lib/common/src/federation';

export interface FournisseurSelectProps {
  label?: string;
  idOrigine?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  name?: string;
  required?: boolean;
}

export const FournisseurSelect: FC<FournisseurSelectProps> = ({
  label,
  idOrigine,
  onChange,
  name,
  required = false,
}) => {
  const origines = useGet_OriginesQuery();

  const idOrigineLaboratoire = (origines.data?.origines.nodes || []).find(
    ({ code }: any) => code === 'LABORATOIRE'
  )?.id;
  const idOriginePrestataireService = (origines.data?.origines.nodes || []).find(
    ({ code }: any) => code === 'PRESTATAIRE_SERVICE'
  )?.id;
  const idOrigineInterne = (origines.data?.origines.nodes || []).find(({ code }: any) => code === 'INTERNE')?.id;

  const fournisseurs = [
    { id: idOrigineLaboratoire, label: 'Laboratoire' },
    { id: idOriginePrestataireService, label: 'Partenaire de Service' },
    { id: idOrigineInterne, label: 'Confrères' },
  ];

  return (
    <CustomSelect
      label={label ?? 'Type Fournisseur'}
      listId="id"
      index="label"
      name={name || 'idOrigine'}
      value={idOrigine}
      onChange={onChange}
      list={fournisseurs}
      required={required}
    />
  );
};
