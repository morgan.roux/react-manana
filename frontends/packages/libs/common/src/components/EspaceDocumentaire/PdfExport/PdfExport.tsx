import { PDFExportProps, savePDF } from '@progress/kendo-react-pdf';
import React from 'react';
import '@progress/kendo-theme-default/dist/all.css';

const PageTemplate = (props: { pageNum: number; totalPages: number }) => (
  <span>
    Page {props.pageNum} sur {props.totalPages}
  </span>
);

export const makePdf = (html: any, fileName: string, option?: PDFExportProps) => {
  return savePDF(html, {
    paperSize: option?.paperSize || 'auto',
    fileName: `${fileName}.pdf`,
    margin: option?.margin || 3,
    pageTemplate: PageTemplate,
  });
};
