import { Box, IconButton } from '@material-ui/core';
import React, { Dispatch, FC, SetStateAction, useState } from 'react';
import GEDFilter from './GEDFilter';
import style from './style';
import { MainContainer } from './MainContainer/MainContainer';
import FilterAlt from '../../assets/img/filter_alt_white.svg';
import { Refresh } from '@material-ui/icons';
import { useApplicationContext } from '@lib/common';
import { Smyley } from '@lib/common/src/graphql';
import { Search_DocumentQuery, Search_DocumentQueryVariables, FullDocumentCategorieFragment } from '../../federation';
import { LeftSidebarAndMainPage } from '@lib/common';
import { LazyQueryResult } from '@apollo/client';

export interface User {
  id: string;
  fullName: string;
  prestataireService?: {
    id: string;
    nom: string;
  };
}

interface Fichier {
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string;
}

export interface Role {
  id: string;
  type: string;
  nom: string;
}

export interface IDocumentSearch {
  searchText?: string | undefined;
  accesRapide?:
    | (
        | 'FAVORIS'
        | 'NOUVEAUX'
        | 'RECENTS'
        | 'PLUS_CONSULTES'
        | 'PLUS_TELECHARGES'
        | 'EN_ATTENTE_APPROBATION'
        | 'REFUSE'
        | 'APPROUVE'
        | 'REFUSE_COMPTABLE'
        | 'APPROUVE_COMPTABLE'
        | 'APPROUVE_PAYE'
        | 'APPROUVE_COMPTABLE_PAYE'
        | 'PAYE'
        | 'POUBELLE'
      )[]
    | undefined;
  proprietaire?: 'MOI' | 'AUTRE' | 'TOUT';
  idSousCategorie?: string | undefined;
  filterBy?: 'DERNIERE_SEMAINE' | 'DERNIER_MOIS' | 'TOUT' | '12_DERNIERS_MOIS' | undefined;
  sortBy?: string;
  sortDirection?: 'ASC' | 'DESC' | undefined;
  libelleSousEspace?: string;

  type?: string;
  validation?: 'INTERNE' | 'PRESTATAIRE_SERVICE' | 'TOUT';
  idOrigine?: string;
  idOrigineAssocie?: string;
  dateDebut?: string;
  dateFin?: string;
  montantMinimal?: number;
  montantMaximal?: number;
  idReglementMode?: string;
}

export interface FactureTva {
  idDocument?: string;
  idTva: string;
  montantHt?: any;
  montantTva?: number;
  montantTtc?: number;
}
interface EspaceDocumentaireProps {
  categories: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  documents: LazyQueryResult<Search_DocumentQuery, Search_DocumentQueryVariables>;
  content: {
    loading: boolean;
    error: Error;
    data: FullDocumentCategorieFragment;
  };
  commentaires: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  mode: 'creation' | 'remplacement';
  saving: boolean;
  saved: boolean;
  setSaved: (newValue: boolean) => void;
  activeDocument?: FullDocumentCategorieFragment;
  onRequestCreateDocument: (document: any) => void;
  onRequestAddDocumentToFavorite: (document: any) => void;
  onRequestDeleteDocument: (document: any) => void;
  onRequestDetailsDocument: (document: any) => void;
  onRequestRemoveDocumentFromFavorite: (document: any) => void;
  onRequestReplaceDocument: (document: any) => void;
  onRequestFetchMoreComments: (document: FullDocumentCategorieFragment) => void;
  refetchComments: () => void;
  refetchSmyleys: () => void;
  onRequestGetComments: (document: FullDocumentCategorieFragment) => void;
  onRequestRefresh: () => void;
  onRequestDeplace: (document: FullDocumentCategorieFragment) => void;
  onRequestLike: (document: FullDocumentCategorieFragment, smyley: Smyley | null) => void;
  setSearchVariables: Dispatch<SetStateAction<IDocumentSearch>>;
  searchVariables: IDocumentSearch;
  facturesRejeteesInterneCategories?: { annee: number; mois: number }[];
  facturesRejeteesPrestataireServiceCategories?: { annee: number; mois: number }[];
  onCompleteDownload: (document: FullDocumentCategorieFragment) => void;
  onGoback: () => void;
  refetchDocument?: () => void;
  onRequestFetchAll: (withSearch: boolean) => void;
  onRequestDocument: (id: string) => void;
  contratFournisseurs?: any[];
  factureLaboratoires?: any[];
  facturePartenaires?: any[];

  espaceType?: string;
  tvas: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  maxTTC?: number;
  refetch?: () => void;
}

const EspaceDocumentaire: FC<EspaceDocumentaireProps> = ({
  categories,
  documents,
  commentaires,
  content,
  saving,
  facturesRejeteesInterneCategories,
  facturesRejeteesPrestataireServiceCategories,
  saved,
  setSaved,
  searchVariables,
  onRequestCreateDocument,
  onRequestAddDocumentToFavorite,
  onRequestDeplace,
  onRequestDeleteDocument,
  onRequestDetailsDocument,
  onRequestRemoveDocumentFromFavorite,
  onRequestReplaceDocument,
  onRequestFetchMoreComments,
  onRequestGetComments,
  onRequestRefresh,
  onRequestLike,
  setSearchVariables,
  refetchComments,
  refetchSmyleys,
  onCompleteDownload,
  onGoback,
  refetchDocument,
  activeDocument,
  onRequestDocument,
  onRequestFetchAll,

  espaceType = 'espace-documentaire',
  contratFournisseurs,
  tvas,
  factureLaboratoires,
  facturePartenaires,
  maxTTC,
  refetch,
}) => {
  const classes = style();

  const { isMobile } = useApplicationContext();

  // Handling drawer menu
  // filter
  const [openFilter, setOpenFilter] = useState<boolean>(false);
  const [filter, setFilter] = useState(typeof window !== 'undefined' && window.innerWidth < 1366);
  const handleClickFilterMenu = () => setOpenFilter((prev) => !prev);
  const [activeSousCategorie, setActiveSousCategorie] = useState<any | undefined>(undefined);
  // sidebar
  const [openSidebar, setOpenSidebar] = useState<boolean>(false);
  const [sidebar, setSidebar] = useState(typeof window !== 'undefined' && window.innerWidth < 1024);
  const handleClickSidebarMenu = () => setOpenSidebar((prev) => !prev);
  const [openDrawer, setOpenDrawer] = useState<boolean>(espaceType === 'espace-facture' && isMobile);

  const handleDrawerToggle = () => {
    setOpenDrawer((prev) => !prev);
  };

  React.useEffect(() => {
    window.addEventListener('resize', () => {
      if (window.innerWidth < 1366) {
        setFilter(true);
        if (window.innerWidth < 1024) setSidebar(true);
        else setSidebar(false);
      } else setFilter(false);
    });
  });

  const handleClickFilter = () => {
    setOpenDrawer(true);
  };

  const filterBtn = (
    <IconButton color="inherit" aria-label="settings" edge="start" onClick={handleClickFilter}>
      <img src={FilterAlt} />
    </IconButton>
  );

  const refreshBtn = (
    <IconButton color="inherit" aria-label="settings" edge="start" onClick={onRequestRefresh}>
      <Refresh />
    </IconButton>
  );

  return (
    <Box className={classes.GEDContainer}>
      <LeftSidebarAndMainPage
        drawerTitle={espaceType === 'espace-facture' ? 'Espace facturation' : 'Espace documentaire'}
        sidebarChildren={
          <GEDFilter
            factureLaboratoires={factureLaboratoires}
            facturesRejeteesInterneCategories={facturesRejeteesInterneCategories}
            facturesRejeteesPrestataireServiceCategories={facturesRejeteesPrestataireServiceCategories}
            searchVariables={searchVariables}
            setSearchVariables={setSearchVariables}
            activeSousCategorie={activeSousCategorie}
            setActiveSousCategorie={setActiveSousCategorie}
            categories={categories}
            onGoBack={onGoback}
            onRequestRefresh={onRequestRefresh}
            espaceType={espaceType}
            contratFournisseurs={contratFournisseurs}
            facturePartenaires={facturePartenaires}
            setOpenDrawer={setOpenDrawer}
            maxTTC={maxTTC}
          />
        }
        mainChildren={
          <MainContainer
            sideBarProps={{
              saving: saving,
              saved: saved,
              setSaved: setSaved,
              activeSousCategorie: activeSousCategorie,
              documents: documents,
              searchVariables: searchVariables,
              setActiveDocument: onRequestDetailsDocument,
              onRequestDeplace: onRequestDeplace,
              onRequestAddToFavorite: onRequestAddDocumentToFavorite,
              onRequestDelete: onRequestDeleteDocument,
              onRequestDetails: onRequestDetailsDocument,
              onRequestRemoveFromFavorite: onRequestRemoveDocumentFromFavorite,
              onRequestReplace: onRequestReplaceDocument,
              onRequestGetComments: onRequestGetComments,
              setSearchVariables: setSearchVariables,
              onRequestCreateDocument: onRequestCreateDocument,
              onRequestRefresh: onRequestRefresh,
              document: content.data,
              espaceType,
              tvas,
              refetch,
            }}
            contentProps={{
              saving: saving,
              saved: saved,
              setSaved: setSaved,
              activeSousCategorie: activeSousCategorie,
              activeDocument: activeDocument,
              mobileView: sidebar,
              tabletView: filter,
              content: content,
              commentaires: commentaires,
              onRequestDocument: onRequestDocument,
              refetchComments: refetchComments,
              handleSidebarMenu: handleClickSidebarMenu,
              handleFilterMenu: handleClickFilterMenu,
              onRequestCreateDocument: onRequestCreateDocument,
              onRequestAddToFavorite: onRequestAddDocumentToFavorite,
              onRequestDelete: onRequestDeleteDocument,
              onRequestRemoveFromFavorite: onRequestRemoveDocumentFromFavorite,
              onRequestReplace: onRequestReplaceDocument,
              onRequestFetchMoreComments: onRequestFetchMoreComments,
              onRequestLike: onRequestLike,
              onRequestDeplace: onRequestDeplace,
              onCompleteDownload: onCompleteDownload,
              refetchSmyleys: refetchSmyleys,
              refetchDocument: refetchDocument,
              onRequestFetchAll: onRequestFetchAll,
              setSearchVariables: setSearchVariables,
              searchVariables: searchVariables,
              tvas,
            }}
            mainContainerClassName={classes.mainContainer}
          />
        }
        optionBtn={[refreshBtn, filterBtn]}
        handleDrawerToggle={handleDrawerToggle}
        openDrawer={openDrawer}
        setOpenDrawer={setOpenDrawer}
        sidebarClassNameProps={classes.sidebar}
      />
    </Box>
  );
};

export default EspaceDocumentaire;
