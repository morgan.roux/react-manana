import React, { FC, useState } from 'react';
import ReactIframe from 'react-iframe';
import PdfIcon from '../../assets/img/pdfIcon';
import Image from '@material-ui/icons/Image';
import useStyles from './styles';
import { InsertDriveFile } from '@material-ui/icons';
import { NewCustomButton } from '@lib/ui-kit';
import { FileViewer } from './../FileViewer';
export interface AttachedFilesProps {
  files: any[];
}

const AttachedFiles: FC<AttachedFilesProps> = ({ files }) => {
  const classes = useStyles();
  const defaultFile: any = (files && files.length > 0 && files[0]) || null;

  const [clickedFile, setClickedFile] = useState<any>(defaultFile);

  const [showFile, setShowFile] = useState<boolean>(false);

  const toggleShowFile = (file: any) => {
    setClickedFile(file);
    setShowFile(!showFile);
  };

  React.useEffect(() => {
    if (clickedFile && !files.map((f) => f.id).includes(clickedFile.id)) {
      setShowFile(false);
    }
  }, [clickedFile, files]);

  const getIconFile = (fill: string, file: any) => {
    const ext: any = {
      png: <Image />,
      jpg: <Image />,
      jpeg: <Image />,
      pdf: <PdfIcon fill={fill} />,
    };
    const extension: string = file.nomOriginal.split(/[. ]+/).pop();

    return ext[extension.toLowerCase()] || <InsertDriveFile />;
  };

  return (
    <div className={classes.root}>
      <div className={classes.btnsContainerIfViewPdf}>
        {/* Buttons */}
        <div className={classes.btnsContainer}>
          {files &&
            files.length &&
            files.map(
              (file) =>
                file && (
                  <NewCustomButton
                    theme={clickedFile && clickedFile.id === file.id && showFile ? 'primary' : 'transparent'}
                    key={`btn_${file.id}`}
                    children={file.nomOriginal}
                    // tslint:disable-next-line: jsx-no-lambda
                    onClick={() => toggleShowFile(file)}
                    className={
                      clickedFile && clickedFile.id === file.id && showFile
                        ? classes.fileButtonActive
                        : classes.fileButton
                    }
                    startIcon={
                      clickedFile && clickedFile.id === file.id && showFile
                        ? getIconFile('#FFF', file)
                        : getIconFile('#424242', file)
                    }
                  />
                )
            )}
        </div>
        {/* Iframe */}
        {clickedFile && showFile && (
          <FileViewer key={clickedFile.id} fileUrl={clickedFile.publicUrl} />

          /*<ReactIframe
            id={clickedFile.id}
            key={clickedFile.id}
            className={classes.seeContentPdf}
            width="100%"
            url={`${clickedFile.publicUrl}`}
          />*/
        )}
      </div>
    </div>
  );
};

export default AttachedFiles;
