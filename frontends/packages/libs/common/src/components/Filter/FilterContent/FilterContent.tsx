import React, { FC, useState, useEffect, useRef } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box } from '@material-ui/core';
import Content from './Content';
import StaticContent from './StaticContent';
import { useApolloClient, useQuery } from '@apollo/client';
import { debounce } from 'lodash';
// import useStyles from './style';
import OtherContent from './OtherContent';
import CustomFrontFilter from './CustomFrontFilter';
import CanalChoice from '../CanalChoice';
import { useFilters } from '@lib/common';

interface ListInterface {
  id: any;
  nom: string;
  nbArticle: number;
  code: string;
  __typename: string;
  checked: boolean;
}

interface ParamInterface {
  title: string;
  type: string[];
  nom: string;
  skip: number;
  take: number;
  order: string;
  typename: string;
  sortBy: string;
  searchPlaceholder: string;
  listValues?: any[];
  countKey?: string;
}
interface FilterContentProps {
  param: any;
  sortCanal: boolean | undefined;
  history: {
    location: { state: any };
  };
}
const FilterContent: FC<FilterContentProps & RouteComponentProps> = ({
  param,
  sortCanal,
  history: {
    location: { state },
  },
}) => {
  const client = useApolloClient();

  const [idFamilles, setIdFamilles] = useState<string[]>([]);
  const [idLaboratoires, setIdLaboratoires] = useState<any[]>([]);
  const [idGammesCommercials, setIdGammesCommercials] = useState<number[]>([]);
  const [idCommandeCanal, setIdCommandeCanal] = useState<string | null>(null);
  const [idCommandeCanals, setIdCommandeCanals] = useState<any[]>([]);
  const [idCommandeTypes, setIdCommandeTypes] = useState<number[]>([]);
  const [idRemise, setIdRemise] = useState<number[]>([]);
  const [idOcCommande, setIdOcCommande] = useState<number[]>([]);
  const [sortie, setSortie] = useState([0, 1]);
  const [laboType, setLaboType] = useState([0, 1]);
  const [reaction, setReaction] = useState(state && state.comments ? [0] : [0, 1]);
  const [idActualiteOrigine, setIdActualiteOrigine] = useState<string[]>([]);
  const [idTodoType, setIdTodoType] = useState<number[]>([]);
  const [idSeen, setIdSeen] = useState<number[]>([]);
  const [idCheckeds, setIdCheckeds] = useState<string[]>([]);

  const { filters, setFilters } = useFilters();

  useEffect(() => {
    if (filters && filters.data && filters.data.filters) {
      const filter = filters.data.filters;

      if (filter.idFamilles.length === 0) {
        setIdFamilles([]);
      }
      if (filter.idLaboratoires.length === 0) {
        setIdLaboratoires([]);
      }
      if (filter.idGammesCommercials.length === 0) {
        setIdGammesCommercials([]);
      }
      if (filter.idCommandeCanal) {
        setIdCommandeCanal(filter.idCommandeCanal);
      }
      if (filter.idRemise.length === 0) {
        setIdRemise([]);
      }
      if (filter.idOcCommande.length === 0) {
        setIdOcCommande([]);
      }
      if (filter.idCommandeTypes.length === 0) {
        setIdCommandeTypes([]);
      }
      if (filter.idCommandeCanals.length === 0) {
        setIdCommandeCanals([]);
      }
      if (filter.idTodoType.length === 0) {
        setIdTodoType([]);
      }

      if (filter.sortie.length === 0) {
        setSortie([]);
      }

      if (filter.laboType.length === 0) {
        setLaboType([]);
      }

      if (filter.reaction.length === 0 && (!state || (state && !state.comments))) {
        setReaction([]);
      }

      setIdCheckeds([]);
    }
  }, [filters]);

  useEffect(() => {
    return () => {
      setIdLaboratoires([]);
      setReaction([]);
      debounceFilter.current(
        idCheckeds,
        [],
        idFamilles,
        idGammesCommercials,
        idCommandeTypes,
        idCommandeCanal,
        idCommandeCanals,
        idActualiteOrigine,
        idRemise,
        idOcCommande,
        idTodoType,
        idSeen,
        sortie,
        laboType,
        []
      );
    };
  }, []);

  const occommandeContent = {
    typename: 'occommande',
    title: 'Commande',
    list: [
      {
        id: 0,
        value: false,
        name: 'Non passée',
      },
      {
        id: 1,
        value: true,
        name: 'Passée',
      },
    ],
  };

  const seenContent = {
    typename: 'seen',
    title: 'Status',
    list: [
      {
        id: 0,
        value: 2,
        checked: true,
        name: 'Totalité',
      },
      {
        id: 1,
        value: 1,
        name: 'Lus',
      },
      {
        id: 2,
        value: 0,
        name: 'Non Lus',
      },
    ],
  };

  const debounceFilter = useRef(
    debounce(
      (
        idCheckeds: string[],
        idLaboratoires: any,
        idFamilles: string[],
        idGammesCommercials: number[],
        idCommandeTypes: number[],
        idCommandeCanal: string | null,
        idCommandeCanals: string[],
        idActualiteOrigine: string[],
        idRemise: number[],
        idOcCommande: number[],
        idTodoType: number[],
        idSeen: number[],
        sortie: any,
        laboType: number[],
        reaction: number[]
      ) => {
        setFilters({
          idCheckeds,
          idLaboratoires:
            !idLaboratoires.length && state && state.idLaboratoire ? [state.idLaboratoire] : idLaboratoires,
          idFamilles,
          idGammesCommercials,
          idCommandeTypes,
          idCommandeCanal,
          idCommandeCanals,
          idActualiteOrigine,
          idRemise,
          idOcCommande,
          idTodoType,
          idSeen,
          sortie,
          laboType,
          reaction,
        });
      },
      1500
    )
  );

  useEffect(() => {
    debounceFilter.current(
      idCheckeds,
      idLaboratoires,
      idFamilles,
      idGammesCommercials,
      idCommandeTypes,
      idCommandeCanal,
      idCommandeCanals,
      idActualiteOrigine,
      idRemise,
      idOcCommande,
      idTodoType,
      idSeen,
      sortie,
      laboType,
      reaction
    );
  }, [
    idCheckeds,
    idLaboratoires,
    idFamilles,
    idGammesCommercials,
    idCommandeTypes,
    idCommandeCanal,
    idCommandeCanals,
    idActualiteOrigine,
    idRemise,
    idOcCommande,
    idTodoType,
    idSeen,
    sortie,
    laboType,
    reaction,
  ]);

  const handleUpdateList = (list: ListInterface[], typename: string) => {
    switch (typename) {
      case 'laboratoire':
        checkLaboratoire(list);
        break;
      case 'famille':
        checkFamille(list);
        break;
      case 'gamme':
        checkGammesCommercials(list);
        break;
      case 'commandetype':
        checkCommandeTypes(list);
        break;
      case 'commandecanal':
        checkCommandeCanal(list);
        break;
      case 'commandecanals':
        checkCommandeCanals(list);
        break;
      case 'actualiteorigine':
        checkActualiteOrigine(list);
        break;
      case 'remise':
        checkRemise(list);
        break;
      case 'occommande':
        checkOcCommande(list);
        break;
      case 'todotype':
        checkTodoType(list);
        break;
      case 'seen':
        checkSeen(list);
        break;
      case 'sortie':
        checkSortie(list);
        break;
      case 'labotype':
        checkLaboType(list);
        break;
      case 'reaction':
        checkReaction(list);
        break;
      default:
        break;
    }
  };

  const checkReaction = (list: ListInterface[]) => {
    const newReactions: number[] = [];
    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'reaction') {
        newReactions.push(item.id);
      }
    });
    setReaction(newReactions);
    return newReactions;
  };

  const checkLaboType = (list: ListInterface[]) => {
    const newLaboType: number[] = [];
    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'labotype') {
        newLaboType.push(item.id);
      }
    });
    setLaboType(newLaboType);
    return newLaboType;
  };

  const checkSortie = (list: ListInterface[]) => {
    const newValuesSortie: number[] = [];
    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'sortie') {
        newValuesSortie.push(item.id);
      }
    });
    setSortie(newValuesSortie);
    return newValuesSortie;
  };

  const checkFamille = (list: ListInterface[]) => {
    const newIdFamille: string[] = [];
    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'famille') {
        newIdFamille.push(item.code);
      }
    });
    setIdFamilles(newIdFamille);
    return newIdFamille;
  };

  const checkLaboratoire = (list: ListInterface[]) => {
    const newIdLaboratoire: any = [];
    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'laboratoire') {
        newIdLaboratoire.push(item.id);
      }
    });
    setIdLaboratoires(newIdLaboratoire);

    return newIdLaboratoire;
  };

  const checkGammesCommercials = (list: ListInterface[]) => {
    const newIdGammesCommercials: number[] = [];

    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'gamme') {
        newIdGammesCommercials.push(item.id);
      }
    });
    setIdGammesCommercials(newIdGammesCommercials);
    return newIdGammesCommercials;
  };

  const checkCommandeCanal = (list: ListInterface[]) => {
    let newIdCommandeCanal: any = null;

    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'commandecanal') {
        newIdCommandeCanal = item.id;
      }
    });
    setIdCommandeCanal(newIdCommandeCanal);
    return newIdCommandeCanal;
  };

  const checkCommandeCanals = (list: ListInterface[]) => {
    const newIdCommandeCanals: number[] = [];

    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'commandecanals') {
        newIdCommandeCanals.push(item.id);
      }
    });
    setIdCommandeCanals(newIdCommandeCanals);
    return newIdCommandeCanals;
  };

  const checkCommandeTypes = (list: ListInterface[]) => {
    const newIdCommandeTypes: number[] = [];

    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'commandetype') {
        newIdCommandeTypes.push(item.id);
      }
    });
    setIdCommandeTypes(newIdCommandeTypes);
    return newIdCommandeTypes;
  };

  const checkActualiteOrigine = (list: ListInterface[]) => {
    const newIdActualiteOrigine: string[] = [];

    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'actualiteorigine') {
        newIdActualiteOrigine.push(item.id);
      }
    });
    setIdActualiteOrigine(newIdActualiteOrigine);
    return newIdActualiteOrigine;
  };

  const checkRemise = (list: ListInterface[]) => {
    const newIdRemise: number[] = [];

    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'remise') {
        newIdRemise.push(item.id);
      }
    });
    setIdRemise(newIdRemise);
    return newIdRemise;
  };

  const checkOcCommande = (list: ListInterface[]) => {
    const newIdOcCommande: number[] = [];

    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'occommande') {
        newIdOcCommande.push(item.id);
      }
    });
    setIdOcCommande(newIdOcCommande);
    return newIdOcCommande;
  };

  const checkTodoType = (list: ListInterface[]) => {
    const newIdTodoType: number[] = [];

    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'todotype') {
        newIdTodoType.push(item.id);
      }
    });
    setIdTodoType(newIdTodoType);
    return newIdTodoType;
  };

  const checkSeen = (list: ListInterface[]) => {
    const newIdSeen: number[] = [];

    list.map((item: ListInterface) => {
      if (item.checked && item.__typename === 'seen') {
        newIdSeen.push(item.id);
      }
    });
    setIdSeen(newIdSeen);
    return newIdSeen;
  };

  const isActualite: boolean = window.location.hash === '#/' || window.location.hash.startsWith('#/actualite');
  const isOperation: boolean =
    window.location.hash.startsWith('#/operations-commerciales') || window.location.hash.includes('#/oc');
  const dataType: string = isActualite ? 'actualite' : isOperation ? 'operation' : '';

  return (
    <Box>
      {sortCanal && <CanalChoice />}
      {(isActualite || isOperation) && <OtherContent dataType={dataType} />}
      {param &&
        param.map((item: ParamInterface, index: any) =>
          item.typename === 'occommande' ? (
            <CustomFrontFilter key={`custom-filter-oc-commande-${item.nom}`} data={occommandeContent} />
          ) : item.typename === 'seen' ? (
            <CustomFrontFilter key={`custom-filter-seen-${item.nom}`} data={seenContent} />
          ) : item.listValues ? (
            <StaticContent
              key={item.typename.concat(index)}
              title={item.title}
              updateList={handleUpdateList}
              list={item.listValues}
            />
          ) : (
            <Content
              title={item.title}
              type={item.type}
              nom={item.nom}
              skip={item.skip}
              take={item.take}
              order={item.order}
              typename={item.typename}
              sortBy={item.sortBy}
              updateList={handleUpdateList}
              key={item.typename.concat(index)}
              searchPlaceholder={item.searchPlaceholder && item.searchPlaceholder}
              countKey={item.countKey}
            />
          )
        )}
    </Box>
  );
};
export default withRouter(FilterContent);
