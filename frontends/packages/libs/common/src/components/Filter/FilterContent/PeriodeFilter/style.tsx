import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& .MuiFormControl-root': {
        width: '95%',
      },
      '& .DateInput_input': {
        fontSize: 15,
      },
      '& .DateRangePickerInput': {
        display: 'flex',
        alignItems: 'center',
      },
    },
  })
);

export default useStyles;
