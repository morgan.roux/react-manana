import React, { useState, ReactNode, FC, useEffect } from 'react';
import { Box, Typography, IconButton } from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import useStyles from './style';
import StatusFilter from '../StatusFilter';
import PeriodeFilter from '../PeriodeFilter/PeriodeFilter';
import { END_DAY_OF_CURRENT_MONTH, TITULAIRE_PHARMACIE, TODAY, useApplicationContext } from '@lib/common';
import { ApolloClient, gql, useApolloClient } from '@apollo/client';

export const ROLES_FILTER_DATES = [TITULAIRE_PHARMACIE];
interface Element {
  id: string;
  title: string;
  content: ReactNode;
}

export interface OtherContentProps {
  dataType: string;
  titleLegend?: string;
  startDatePlaceholderText?: string;
  endDatePlaceholderText?: string;
}

const GET_STATUS_FILTER = gql`
  {
    statusFilter @client
  }
`;

const GET_FILTER_DATE = gql`
  {
    dateFilter @client {
      startDate
      endDate
    }
  }
`;

export const initializeStatusAndPeriodeFilters = (client: ApolloClient<any>, role: string, dataType?: string) => {
  client.writeQuery({ query: GET_STATUS_FILTER, data: { statusFilter: 'all' } });
  // Reset dateFilter
  client.writeQuery({
    query: GET_FILTER_DATE,
    data: {
      dateFilter: {
        startDate: ROLES_FILTER_DATES.includes(role) ? TODAY : null,
        endDate: ROLES_FILTER_DATES.includes(role) ? END_DAY_OF_CURRENT_MONTH : null,
        __typename: 'DateFilter',
      },
    },
  });
};

const OtherContent: FC<OtherContentProps> = ({ dataType }) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [expanded, setExpanded] = useState<{ [key: string]: boolean }>({
    status: true,
    periode: true,
  });
  const { user: currentUser } = useApplicationContext();
  const role = currentUser && currentUser.role && currentUser.role.code;

  const toggleExpanded = (key: string) => {
    setExpanded((prevState) => ({
      ...prevState,
      [key]: !prevState[key],
    }));
  };

  const elements: Element[] = [
    {
      id: 'status',
      title: 'Status',
      content: <StatusFilter />,
    },
    {
      id: 'periode',
      title: 'Période de recherche',
      content: <PeriodeFilter dataType={dataType} />,
    },
  ];

  // Set statusFilter and dateFilter to default if dataType change
  useEffect(() => {
    initializeStatusAndPeriodeFilters(client, role || '', dataType);
  }, [dataType]);

  return (
    <Box marginLeft="8px" padding="0px">
      {elements.map((item, index) => {
        return (
          <div className={classes.itemContainer} key={`list_other_content_item_${index}`}>
            <div className={classes.itemTitleContainer} onClick={() => toggleExpanded(item.id)}>
              <Typography className={classes.title}> {item.title} </Typography>
              <IconButton size="small" className={classes.expandBtnContainer}>
                {expanded[item.id] ? <ExpandLess /> : <ExpandMore />}
              </IconButton>
            </div>
            {expanded[item.id] && <div className={classes.itemContentContainer}>{item.content}</div>}
          </div>
        );
      })}
    </Box>
  );
  return null;
};

export default OtherContent;
