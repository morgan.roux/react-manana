import React, { FC, useState } from 'react';
import useStyles from './styles';
import {
  Box,
  Typography,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Checkbox,
  IconButton,
} from '@material-ui/core';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import { useLocation } from 'react-router';
interface ContentProps {
  title: string;
  list: any[];
  updateList: Function;
}
const Content: FC<ContentProps> = ({ title, list, updateList }) => {
  const classes = useStyles({});
  const [expandedMore, setexpandedMore] = useState(true);
  const { state } = useLocation();

  React.useEffect(() => {
    let typename = '';
    const newList = list.map((item: any) => {
      typename = item.__typename;
      item.checked = false;
      if (state && (state as any).comments && typename === 'reaction' && item.id === 0) {
        item.checked = true;
        return item;
      }
      return item;
    });

    if (typename) updateList(newList, typename);
  }, [list, state]);

  const onGetValue = (id: number, typename: string) => {
    const newList = list.map((item: any) => {
      if (item.id === id) {
        item.checked = !item.checked;
        return item;
      }
      return item;
    });
    if (typename === 'reaction') console.log('newList', newList);
    updateList(newList, typename);
  };

  return (
    <Box marginLeft="8px" borderTop="1px solid #E3E3E3" padding="16px 0">
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        onClick={() => setexpandedMore(!expandedMore)}
      >
        <Typography className={classes.name}>{title}</Typography>
        <IconButton size="small">{expandedMore ? <ExpandLess /> : <ExpandMore />}</IconButton>
      </Box>
      <Box className={classes.noStyle}>
        {expandedMore &&
          list.map((item: any) => (
            <ListItem
              role={undefined}
              dense={true}
              button={true}
              onClick={() => onGetValue(item.id, item.__typename)}
              key={item.id}
            >
              <Box className={classes.checkBoxLeftName} display="flex" alignItems="center">
                <Checkbox tabIndex={-1} checked={item.checked} disableRipple={true} />
                <ListItemText className={classes.nom} primary={item.nom} />
              </Box>

              <ListItemSecondaryAction>
                <Typography className={classes.nbrProduits}>{item.nbArticle}</Typography>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
      </Box>
    </Box>
  );
};
export default Content;
