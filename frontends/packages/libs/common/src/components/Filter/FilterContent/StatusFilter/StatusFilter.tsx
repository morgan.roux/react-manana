import React, { FC, useEffect } from 'react';
import { FormControlLabel, Radio } from '@material-ui/core';
import { useApolloClient, useQuery } from '@apollo/client';
//import { GET_STATUS_FILTER } from '../../../../../graphql/Filter/local';
import { GET_LOCAL_FRONT_FILTERS, FrontFiltersInterface } from '../../../withSearch/withSearch';

export interface StatusFilterInterface {
  statusFilter: string;
}

const StatusFilter: FC = () => {
  /* const client = useApolloClient();

  const getStatusFilter = useQuery<StatusFilterInterface>(GET_STATUS_FILTER);
  const value = getStatusFilter.data && getStatusFilter.data.statusFilter;
  const frontFilters = useQuery<FrontFiltersInterface>(GET_LOCAL_FRONT_FILTERS);
  const occommande =
    (frontFilters &&
      frontFilters.data &&
      frontFilters.data.frontFilters &&
      frontFilters.data.frontFilters.occommande) ||
    [];
  const datefilter =
    (frontFilters &&
      frontFilters.data &&
      frontFilters.data.frontFilters &&
      frontFilters.data.frontFilters.occommande) ||
    null;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    client.writeData({ data: { statusFilter: (event.target as HTMLInputElement).value } });
    client.writeData({
      data: {
        frontFilters: {
          occommande,
          seen: (event.target as HTMLInputElement).value,
          datefilter,
          __typename: 'localFront',
        },
      },
    });
  };

  const status = [
    { id: 'all', title: 'Totalité' },
    { id: 'read', title: 'Lus' },
    { id: 'unread', title: 'Non Lus' },
  ];

  // Init statusFilter
  useEffect(() => {
    if (!value) {
      client.writeData({ data: { statusFilter: 'all' } });
    }
  }, []);

  return (
    <>
      {status.map((item, index) => {
        return (
          <FormControlLabel
            key={`status_filter_${index}`}
            control={<Radio onChange={handleChange} checked={value === item.id} value={item.id} />}
            label={item.title}
          />
        );
      })}
    </>
  );*/
  return null;
};

export default StatusFilter;
