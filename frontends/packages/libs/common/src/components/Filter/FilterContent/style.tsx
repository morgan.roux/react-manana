import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontFamily: 'Roboto',
      fontSize: '14px',
      color: '#1D1D1D',
      marginLeft: '16px',
    },
    expandBtnContainer: {
      marginRight: 10,
    },
  })
);

export default useStyles;
