import { Box, InputBase } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { debounce } from 'lodash';
import React, { FC, useEffect, useRef, useState } from 'react';
import { useFilters } from '../../../shared';
import { useStyles } from './style';

interface SearchContentProps {
  placeholder: string;
}
const SearchContent: FC<SearchContentProps> = ({ placeholder }) => {
  const classes = useStyles({});

  const [searchText, setSearchText] = useState('');

  const { searchText: localSearchText, setSearchText: setLocalSearchText } = useFilters();

  useEffect(() => {
    if (localSearchText.data?.searchText.text) {
      setSearchText(localSearchText.data.searchText.text);
    } else {
      setSearchText('');
    }
  }, [localSearchText]);

  const onChange = (value: string) => {
    setSearchText(value);
  };

  const debouncedSearchText = useRef(
    debounce((value: string) => {
      updateLocalState(value || '');
    }, 1500)
  );

  useEffect(() => {
    debouncedSearchText.current(searchText);
  }, [searchText]);

  const updateLocalState = (value: string) => {
    setLocalSearchText(value);
  };

  return (
    <Box className={classes.search}>
      <InputBase
        placeholder={placeholder}
        type="search"
        value={searchText}
        onChange={(evt) => onChange(evt.target.value)}
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ 'aria-label': 'search' }}
      />
      <Box className={classes.searchIcon}>
        <SearchIcon />
      </Box>
    </Box>
  );
};

export default SearchContent;
