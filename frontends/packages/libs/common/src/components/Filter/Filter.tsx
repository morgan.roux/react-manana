import React, { FC, useEffect } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import FilterContainer from './FilterContainer';
import { ShoppingBasket, StoreMallDirectory, Info } from '@material-ui/icons';
import { resetSearchFilters } from '../withSearch/withSearch';
import { useApolloClient } from '@apollo/client';
import { laboratoireFilterParam } from '../withSearch/FilterParams';

const Filter: FC<RouteComponentProps<any, any, any>> = ({ location }) => {
  const pathName = location.pathname;

  const client = useApolloClient();

  useEffect(() => {
    return () => {
      resetSearchFilters(client);
      client.writeData({
        data: {
          checkedsProduit: null,
        },
      });
    };
  }, []);
  const laboratoireSortParam = [
    {
      label: 'Nom',
      name: 'nomLabo',
      direction: 'desc',
      active: true,
      __typename: 'sortItem',
    },
  ];

  const ideeOuBonnePratiqueSortParam = [
    {
      label: 'Classification',
      name: 'classification',
      direction: 'desc',
      active: true,
      __typename: 'sortItem',
    },
  ];
  const afficheSortParam = [
    {
      label: 'Code',
      name: 'code',
      direction: 'asc',
      __typename: 'sortitem',
    },
    {
      label: 'Nom',
      name: 'nom',
      direction: 'asc',
      __typename: 'sortitem',
    },
  ];

  const operationSortParam = [
    {
      label: 'Libellé',
      name: 'libelle',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Date début',
      name: 'dateDebut',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Date fin',
      name: 'dateFin',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: "Date d'ajout",
      name: 'dateModification',
      active: true,
      direction: 'desc',
      __typename: 'sortItem',
    },
    {
      label: 'Nombre de pharmacies qui ont consultées',
      name: 'nombrePharmaciesConsultes',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Nombre de pharmacies qui ont commandées',
      name: 'nombrePharmaciesCommandees',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Nombre de ligne de produits commandés en total',
      name: 'nombreProduitsCommandes',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Nombre de ligne de produits commandé en moyenne par pharmacie',
      name: 'nombreProduitsMoyenParPharmacie',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'CA global de toutes les commandes passées',
      name: 'CATotal',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'CA moyen des commandes passées par pharmacie',
      name: 'CAMoyenParPharmacie',
      direction: 'asc',
      __typename: 'sortItem',
    },
  ];

  const todoSortParam = [
    {
      label: 'Type',
      name: 'typeProject',
      direction: 'desc',
      active: true,
      __typename: 'sortItem',
    },
    {
      label: 'Nom',
      name: 'name',
      direction: 'desc',
      __typename: 'sortItem',
    },
  ];

  const produitSortParam = [
    {
      label: 'Code Référence',
      name: 'produit.produiCode.code',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Libellé',
      name: 'produit.libelle',
      direction: 'desc',
      active: true,
      __typename: 'sortItem',
    },
    {
      label: 'Laboratoire',
      name: 'laboratoire.nomLabo',
      direction: 'desc',
      __typename: 'sortItem',
    },
    {
      label: 'Stv',
      name: 'stv',
      direction: 'desc',
      __typename: 'sortItem',
    },
    {
      label: 'Tarif HT',
      name: 'prixPhv',
      direction: 'desc',

      __typename: 'sortItem',
    },
    {
      label: 'Stock plateforme',
      name: 'qteStock',
      direction: 'desc',
      __typename: 'sortItem',
    },
  ];

  const pharmacieSortParam = [
    {
      label: 'Nom',
      name: 'nom',
      direction: 'asc',
      active: true,
      __typename: 'sortItem',
    },
  ];

  const ActualiteSortParam = [
    {
      label: 'Libellé',
      name: 'libelle',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Nom Laboratoire',
      name: 'laboratoire.nomLabo',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Date début',
      name: 'dateDebut',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Date fin',
      name: 'dateFin',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: "Date d'ajout",
      name: 'dateCreation',
      direction: 'desc',
      __typename: 'sortItem',
    },
    {
      label: 'Date de modification',
      name: 'dateModification',
      active: true,
      direction: 'desc',
      __typename: 'sortItem',
    },
    {
      label: 'Nombre de smyleys',
      name: 'userSmyleys.total',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Nombre de commentaires',
      name: 'comments.total',
      direction: 'asc',
      __typename: 'sortItem',
    },
    // {
    //   label: 'Nombre de pharmacies qui ont consultées',
    //   name: 'nbViewPharmacy',
    //   direction: 'asc',
    //   __typename: 'sortItem',
    // },
  ];

  const FriendSortParam = [
    {
      label: 'Date de création',
      name: 'dateCreation',
      direction: 'desc',
      __typename: 'sortItem',
    },
    {
      label: 'Date de modification',
      name: 'dateModification',
      active: true,
      direction: 'desc',
      __typename: 'sortItem',
    },
    {
      label: 'Date début',
      name: 'dateDebut',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Date fin',
      name: 'dateFin',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Libellé',
      name: 'libelle',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Nom du labo',
      name: 'laboratoire.nomLabo',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Nombre de réaction',
      name: 'userSmyleys.total',
      direction: 'asc',
      __typename: 'sortItem',
    },
    {
      label: 'Nombre de commentaires',
      name: 'comments.total',
      direction: 'asc',
      __typename: 'sortItem',
    },
    // {
    //   label: 'Nombre de pharmacies qui ont consultées',
    //   name: 'nbViewPharmacy',
    //   direction: 'asc',
    //   __typename: 'sortItem',
    // },
  ];

  const groupeClientSortParam = [
    {
      label: 'Code',
      name: 'codeGroupe',
      direction: 'desc',
      active: true,
      __typename: 'sortItem',
    },
    {
      label: 'Nom',
      name: 'nomGroupe',
      direction: 'desc',
      __typename: 'sortItem',
    },
    {
      label: 'Nom Commercial',
      name: 'nomCommercial',
      direction: 'desc',
      __typename: 'sortItem',
    },
    {
      label: 'Date de validité',
      name: 'dateValidite',
      direction: 'desc',
      __typename: 'sortItem',
    },
  ];

  const marcheSortParam = [
    {
      label: 'Nom',
      name: 'nom',
      direction: 'desc',
      active: true,
      __typename: 'sortItem',
    },
    {
      label: 'Nom créateur',
      name: 'userCreated.userName',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Canal',
      name: 'commandeCanal.libelle',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Type',
      name: 'marcheType',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Avec Palier',
      name: 'avecPalier',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Nbre. Adhésion',
      name: 'nbAdhesion',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Nbre. Paliers',
      name: 'nbPalier',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Statut',
      name: 'statuts',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Date de début',
      name: 'dateDebut',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Date de fin',
      name: 'dateFin',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Mise à jour',
      name: 'dateModification',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
  ];

  const promotionSortParam = [
    {
      label: 'Nom',
      name: 'nom',
      direction: 'desc',
      active: true,
      __typename: 'sortItem',
    },
    {
      label: 'Nom créateur',
      name: 'userCreated.userName',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Type',
      name: 'promotionType',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Canal',
      name: 'commandeCanal.libelle',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Statut',
      name: 'promoStatus',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Date de début',
      name: 'dateDebut',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Date de fin',
      name: 'dateFin',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
    {
      label: 'Mise à jour',
      name: 'dateModification',
      direction: 'desc',
      active: false,
      __typename: 'sortItem',
    },
  ];

  const produitFilterParam = [
    /*  {
      title: 'Canal Commande',
      type: ['commandecanal'],
      nom: 'libelle',
      skip: 0,
      take: 1000,
      order: 'desc',
      typename: 'commandecanal',
      sortBy: 'countCanalArticles',
    }, */
    {
      title: 'Remise',
      type: ['remise'],
      nom: 'libelle',
      skip: 0,
      take: 1000,
      order: 'desc',
      typename: 'remise',
      sortBy: 'remise',
    },
    {
      title: 'Famille',
      type: ['famille'],
      nom: 'libelleFamille',
      skip: 0,
      take: 2000,
      order: 'desc',
      typename: 'famille',
      sortBy: 'countCanalArticles',
      searchPlaceholder: 'Rechercher famille',
    },
    {
      title: 'Laboratoire',
      type: ['laboratoire'],
      nom: 'nomLabo',
      skip: 0,
      take: 1000,
      order: 'desc',
      typename: 'laboratoire',
      sortBy: 'countCanalArticles',
      searchPlaceholder: 'Rechercher laboratoire',
    },
    {
      title: 'Gammes commerciales',
      type: ['canalgamme'],
      nom: 'libelle',
      skip: 0,
      take: 1000,
      order: 'desc',
      typename: 'gamme',
      sortBy: 'countCanalArticles',
    },
  ];

  const ActualiteFilterParam = [
    {
      title: 'Origine',
      type: ['actualiteorigine'],
      nom: 'libelle',
      skip: 0,
      take: 1000,
      order: 'desc',
      typename: 'actualiteorigine',
      sortBy: '',
      countKey: 'countActualites',
    },
  ];

  const operationFilterParam = [
    {
      title: 'Commande',
      type: ['commande'],
      nom: 'libelle',
      skip: 0,
      take: 1000,
      order: 'desc',
      typename: 'occommande',
      sortBy: 'commandePasse',
    },
    {
      title: 'Type Commande',
      type: ['commandetype'],
      nom: 'libelle',
      skip: 0,
      take: 1000,
      order: 'desc',
      typename: 'commandetype',
      sortBy: 'countOperationsCommercials',
    } /*
    {
      title: 'Canal Commande',
      type: ['commandecanal'],
      nom: 'libelle',
      skip: 0,
      take: 1000,
      order: 'desc',
      typename: 'commandecanal',
      sortBy: 'countOperationsCommercials',
    }, */,
  ];

  const todoFilterParam = [
    {
      title: 'Type',
      type: ['todotype'],
      nom: 'libelle',
      skip: 0,
      take: 1000,
      order: 'desc',
      typename: 'todotype',
      sortBy: 'todotype',
    },
  ];

  if (pathName.startsWith('#/project')) {
    return (
      <FilterContainer
        key="todo"
        title={{ label: 'Todo' }}
        placeholder={'Recherche un projet'}
        button={{
          text: 'créer un projet',
          url: '/project/new',
          color: 'pink',
          icon: 'add',
        }}
        filterParam={todoFilterParam}
        sortParam={todoSortParam}
      />
    );
  }

  if (pathName === '/catalogue-produits/card' || pathName === '/catalogue-produits/list') {
    return (
      <FilterContainer
        key="produitcanal"
        title={{ icon: <StoreMallDirectory />, label: 'Catalogue des produits' }}
        sortParam={produitSortParam}
        filterParam={produitFilterParam}
        placeholder={'Rechercher un produit'}
        sortCanal={true}
      />
    );
  }

  if (pathName === '/gestion-produits/card' || pathName === '/gestion-produits/list') {
    return (
      <FilterContainer
        key="gestionProduit"
        sortParam={produitSortParam}
        filterParam={produitFilterParam}
        placeholder={'Rechercher un produit'}
        sortCanal={true}
      />
    );
  }

  if (pathName.startsWith('/operation-produits/card') || pathName.startsWith('/operation-produits/list')) {
    return (
      <FilterContainer
        key="produitcanal"
        sortParam={produitSortParam}
        filterParam={produitFilterParam}
        placeholder={'Rechercher un produit'}
        sortCanal={true}
      />
    );
  }

  if (pathName.startsWith('/operations-commerciales')) {
    return (
      <FilterContainer
        title={{ icon: <ShoppingBasket />, label: 'Opérations commerciales' }}
        placeholder={'Rechercher une opération commerciale'}
        sortParam={operationSortParam}
        filterParam={operationFilterParam}
      />
    );
  }

  if (pathName.startsWith('/demarche-qualite/outils/affiches')) {
    return (
      <FilterContainer
        title={{ icon: <ShoppingBasket />, label: 'Affiche' }}
        placeholder={'Rechercher une affiche'}
        sortParam={afficheSortParam}
      />
    );
  }

  if (pathName.startsWith('/demarche-qualite/outils/affiche')) {
    return (
      <FilterContainer
        title={{ icon: <ShoppingBasket />, label: 'Affiche' }}
        placeholder={'Rechercher une affiche'}
        sortParam={afficheSortParam}
      />
    );
  }

  if (pathName.startsWith('/demarche-qualite/outils/enregistrement')) {
    return (
      <FilterContainer
        title={{ icon: <ShoppingBasket />, label: 'Enregistrement' }}
        placeholder={'Rechercher un enregistrement'}
        sortParam={afficheSortParam}
      />
    );
  }

  if (pathName.startsWith('/demarche-qualite/outils/document')) {
    return (
      <FilterContainer
        title={{ icon: <ShoppingBasket />, label: 'Document' }}
        placeholder={'Rechercher un document'}
        sortParam={afficheSortParam}
      />
    );
  }

  if (pathName.startsWith('/demarche-qualite/outils/memo')) {
    return (
      <FilterContainer
        title={{ icon: <ShoppingBasket />, label: 'Memo' }}
        placeholder={'Rechercher un mémo'}
        sortParam={afficheSortParam}
      />
    );
  }

  if (pathName.startsWith('/demarche-qualite/outils/procedure')) {
    return (
      <FilterContainer
        title={{ icon: <ShoppingBasket />, label: 'Procedure' }}
        placeholder={'Rechercher une procédure'}
        sortParam={afficheSortParam}
      />
    );
  }

  /*****PILOTAGE BUSINESS ET FEEDBACK */
  if (pathName.startsWith('/pilotage') && pathName.includes('/operation')) {
    return (
      <FilterContainer
        title={{ icon: <ShoppingBasket />, label: 'Opérations commerciales' }}
        sortParam={operationSortParam}
        filterParam={operationFilterParam}
      />
    );
  }
  if (pathName.startsWith('/pilotage') && pathName.includes('/actualites')) {
    return (
      <FilterContainer
        title={{ icon: <Info />, label: 'Actualité' }}
        sortParam={ActualiteSortParam}
        filterParam={ActualiteFilterParam}
      />
    );
  }
  if (pathName.startsWith('/pilotage') && pathName.includes('/laboratoire')) {
    return (
      <FilterContainer
        title={{ label: 'Laboratoires' }}
        sortParam={laboratoireSortParam}
        filterParam={laboratoireFilterParam}
      />
    );
  }
  if (pathName.startsWith('/pilotage') && pathName.includes('/produit')) {
    return (
      <FilterContainer
        title={{ icon: <StoreMallDirectory />, label: 'Catalogue des produits' }}
        sortParam={produitSortParam}
        filterParam={produitFilterParam}
        sortCanal={true}
      />
    );
  }
  if (pathName.startsWith('/pilotage') && pathName.includes('/project')) {
    return <FilterContainer title={{ label: 'Todo' }} filterParam={todoFilterParam} sortParam={todoSortParam} />;
  }
  /*****PILOTAGE BUSINESS ET FEEDBACK */

  /**
   * GESTION MARCHE ET PROMOTION
   */
  if (pathName.includes('create/articles') || pathName.includes('edit/articles')) {
    return (
      <FilterContainer
        key="gestionMarchePromotionArticle"
        sortParam={produitSortParam}
        filterParam={produitFilterParam}
        placeholder="Rechercher un article"
      />
    );
  }

  if (pathName.includes('create/laboratoires') || pathName.includes('edit/laboratoires')) {
    return (
      <FilterContainer
        key="gestionMarchePromotionLabo"
        sortParam={laboratoireSortParam}
        placeholder="Rechercher un laboratoire"
      />
    );
  }

  if (pathName.includes('create/groups-client') || pathName.includes('edit/groups-client')) {
    return (
      <FilterContainer
        key="gestionMarchePromotionClient"
        sortParam={groupeClientSortParam}
        // filterParam={produitFilterParam}
        placeholder="Rechercher un groupe de client"
      />
    );
  }

  if (pathName === '/marches') {
    return (
      <FilterContainer
        key="gestionMarcheList"
        sortParam={marcheSortParam}
        // filterParam={}
        placeholder="Rechercher un marche"
      />
    );
  }

  if (pathName === '/promotions') {
    return (
      <FilterContainer
        key="gestionPromotionList"
        sortParam={promotionSortParam}
        // filterParam={}
        placeholder="Rechercher une promotion"
      />
    );
  }
  /**
   * FIN GESTION MARCHE ET PROMOTION
   */

  if (pathName === '/create/operations-commerciales') {
    return <FilterContainer key="createOC" title={{ label: "Création d'une opération commerciale" }} />;
  }

  if (pathName === '/edit/operations-commerciales') {
    return <FilterContainer key="editOC" title={{ label: "Modification d'une opération commerciale" }} />;
  }

  if (pathName.match('/laboratoires')) {
    return (
      <FilterContainer
        key="labo"
        title={{ label: 'Laboratoires' }}
        sortParam={laboratoireSortParam}
        placeholder={'Rechercher un laboratoire'}
        filterParam={laboratoireFilterParam}
      />
    );
  }

  if (pathName.startsWith('/edit/operations-commerciales') && pathName.includes('/produit')) {
    return (
      <FilterContainer
        key="editOcProduit"
        title={{ label: "Modification d'une opération commerciale" }}
        sortParam={produitSortParam}
        filterParam={produitFilterParam.filter((item) => item.typename !== 'commandecanal')}
      />
    );
  }

  if (pathName.startsWith('/create/operations-commerciales') && pathName.includes('/produit')) {
    return (
      <FilterContainer
        key="createOcProduit"
        title={{ label: "Création d'une opération commerciale" }}
        sortParam={produitSortParam}
        filterParam={produitFilterParam.filter((item) => item.typename !== 'commandecanal')}
      />
    );
  }

  if (pathName === '/create/actualite') {
    return (
      <FilterContainer key="createActualite" title={{ label: "Création d'une actualité" }} localisation="inActualite" />
    );
  }

  if (pathName.startsWith('/edit/actualite/')) {
    return (
      <FilterContainer
        key="editActualite"
        title={{ label: "Modification d'une actualité" }}
        localisation="inActualite"
      />
    );
  }

  if (pathName === '/' || pathName.startsWith('/actualite')) {
    return (
      <FilterContainer
        title={{ icon: <Info />, label: 'Actualité' }}
        sortParam={ActualiteSortParam}
        filterParam={ActualiteFilterParam}
        placeholder={'Rechercher une actualité'}
      />
    );
  }

  if (pathName.startsWith('/FriendsActualite')) {
    return (
      <FilterContainer
        title={{ icon: <Info />, label: 'Actualité' }}
        sortParam={FriendSortParam}
        placeholder={'Rechercher une actualité'}
      />
    );
  }

  return <></>;
};
export default withRouter(Filter);
