import { gql, useApolloClient } from '@apollo/client';
import { useFilters } from '@lib/common';
import { Box, IconButton, Typography } from '@material-ui/core';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import React, { FC, useEffect, useState } from 'react';
import { useApplicationContext } from '@lib/common';
import { useSearchQuery } from './../../../graphql';
import Content from './Content';
import useStyles from './styles';

interface CanalInterface {
  code: string;
  libelle: string;
  active: boolean;
}

const GET_CODE_CANA_ACTIVE = gql`
  {
    codeCanalActive @client
  }
`;

const SortContent: FC = () => {
  const classes = useStyles({});
  const client = useApolloClient();
  const { graphql } = useApplicationContext();
  const [expandedMore, setExpandedMore] = useState<boolean>(false);
  const [canals, setCanals] = useState<CanalInterface[]>([]);
  const [codeCanalActive, setCodeCanalActive] = useState<string>('PFL');

  const { filters, setFilters } = useFilters();

  const filterResult = filters && filters.data && filters.data.filters;

  const listResult = useSearchQuery({
    client: graphql,
    variables: {
      type: ['commandecanal'],
    },
  });

  useEffect(() => {
    if (
      listResult &&
      listResult.data &&
      listResult.data.search &&
      listResult.data.search.data &&
      listResult.data.search.data.length
    ) {
      setCanals(
        listResult.data.search.data.map((item: any) => {
          return {
            code: item.code,
            libelle: item.libelle,
            active: item.code === codeCanalActive,
          };
        })
      );
    }
  }, [listResult, codeCanalActive]);

  useEffect(() => {
    if (filterResult) {
      setFilters({ ...filterResult, idCommandeCanal: codeCanalActive });
    }
    client.writeQuery({
      query: GET_CODE_CANA_ACTIVE,
      data: {
        codeCanalActive,
      },
    });
  }, [codeCanalActive]);

  const handleSelectActive = (code: string) => {
    setCodeCanalActive(code);
  };

  return (
    <Box marginLeft="8px" borderTop="1px solid #E3E3E3" padding="16px 0">
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Typography className={classes.name} onClick={() => setExpandedMore(!expandedMore)}>
          Choisir canal
        </Typography>
        <Box display="flex" flexDirection="row" alignItems="center">
          {expandedMore ? (
            <IconButton size="small" onClick={() => setExpandedMore(!expandedMore)}>
              <ExpandLess />
            </IconButton>
          ) : (
            <IconButton size="small" onClick={() => setExpandedMore(!expandedMore)}>
              <ExpandMore />
            </IconButton>
          )}
        </Box>
      </Box>
      {expandedMore &&
        canals &&
        canals.length &&
        canals.map((item: CanalInterface) => (
          <Content
            key={item.code}
            code={item.code}
            libelle={item.libelle}
            active={item.active}
            selectActive={handleSelectActive}
          />
        ))}
    </Box>
  );
};
export default SortContent;
