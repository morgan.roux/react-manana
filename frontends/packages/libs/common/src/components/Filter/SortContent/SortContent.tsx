import { ADMINISTRATEUR_GROUPEMENT, SUPER_ADMINISTRATEUR, useApplicationContext } from '@lib/common';
import { Box, IconButton, Tooltip, Typography } from '@material-ui/core';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ReplayIcon from '@material-ui/icons/Replay';
import SortIcon from '@material-ui/icons/Sort';
import React, { FC, useEffect, useState } from 'react';
import { useFilters } from '../../../shared';
import Content from './Content';
import useStyles from './styles';
//import { resetSearchFilters } from '../../withSearch/withSearch';
interface ParamInterface {
  label: string;
  name: string;
  direction: string;
  active?: boolean;
  __typename: string;
}

interface SortContentProps {
  param: ParamInterface[];
}
const SortContent: FC<SortContentProps> = ({ param }) => {
  const classes = useStyles({});

  const { user: currentUser } = useApplicationContext();

  const [expandedMore, setExpandedMore] = useState<boolean>(false);

  const [order, setOrder] = useState<string>('asc');

  const [currentParam, setCurrentParam] = useState<ParamInterface[]>();

  const { sortItem: localSort, setSort } = useFilters();

  const isAdmin =
    currentUser &&
    currentUser.role &&
    (currentUser.role.code === SUPER_ADMINISTRATEUR || currentUser.role.code === ADMINISTRATEUR_GROUPEMENT);

  useEffect(() => {
    setSort(param.find((item: ParamInterface) => item.active === true));
  }, [param]);

  const isActualite: boolean = window.location.hash === '#/' || window.location.hash.startsWith('#/actualite');
  const isOperation: boolean = window.location.hash.startsWith('#/operations-commerciales');
  const dataType: string = isActualite ? 'actualite' : isOperation ? 'operation' : '';

  const handleResetFilter = () => {
    //resetSearchFilters(client, isAdmin, dataType);
  };
  const handleSelectActive = (name: string) => {
    const newList = param.map((item: ParamInterface) => {
      if (item.name !== name) {
        item.active = false;
      } else {
        item.active = true;
      }
      return item;
    });

    updateLocalState(newList);
    setCurrentParam(newList);
  };

  useEffect(() => {
    if (localSort && localSort.data && localSort.data.sort && localSort.data.sort.sortItem) {
      setCurrentParam(
        param.map((item) => {
          if (
            item.name ===
            (localSort && localSort.data && localSort.data.sort.sortItem && localSort.data.sort.sortItem.name)
          ) {
            item.active = true;
            return item;
          } else {
            item.active = false;
            return item;
          }
        })
      );
      setOrder(localSort.data.sort.sortItem.direction);
    }
  }, [localSort]);

  useEffect(() => {
    let order = 'asc';
    param.map((item: ParamInterface) => {
      if (item.active) order = item.direction;
    });
    setOrder(order);
    setCurrentParam(param);
  }, [param]);

  const handleChangeDirection = () => {
    const newList = param.map((item: ParamInterface) => {
      if (item.active) {
        item.direction = item.direction === 'asc' ? 'desc' : 'asc';
      }
      return item;
    });
    setOrder(order === 'asc' ? 'desc' : 'asc');
    updateLocalState(newList);
  };

  const updateLocalState = (list: ParamInterface[]) => {
    setSort(list.find((item: ParamInterface) => item.active === true));
  };

  return (
    <Box marginLeft="8px" borderTop="1px solid #E3E3E3" padding="16px 0" marginTop="24px">
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Typography className={classes.name} onClick={() => setExpandedMore(!expandedMore)}>
          Trier par
        </Typography>
        <Box display="flex" flexDirection="row" alignItems="center">
          <IconButton size="small" onClick={handleResetFilter}>
            <Tooltip title="Réinitialiser les filtres">
              <ReplayIcon />
            </Tooltip>
          </IconButton>
          <IconButton size="small" onClick={handleChangeDirection}>
            {order === 'asc' ? <SortIcon /> : <SortIcon className={classes.desc} />}
          </IconButton>
          {expandedMore ? (
            <IconButton size="small" onClick={() => setExpandedMore(!expandedMore)}>
              <ExpandLess />
            </IconButton>
          ) : (
            <IconButton size="small" onClick={() => setExpandedMore(!expandedMore)}>
              <ExpandMore />
            </IconButton>
          )}
        </Box>
      </Box>

      {expandedMore &&
        currentParam &&
        currentParam.map((item: ParamInterface) => (
          <Content
            key={item.name}
            label={item.label}
            name={item.name}
            direction={item.direction}
            active={item.active}
            selectActive={handleSelectActive}
          />
        ))}
    </Box>
  );
  return null;
};
export default SortContent;
