import { Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { useStyles } from './styles';
import classnames from 'classnames';

interface FichierJointsProps {
  fichiers: any;
}
const FichierJoints: FC<FichierJointsProps> = ({ fichiers }) => {
  const classes = useStyles();

  const openFile = (url: string) => () => {
    window.open(url, '_blank');
  };

  return (
    <Box className={classes.filesContainer} marginLeft="0px !important">
      {(fichiers ?? []).length === 0 && <Typography>Aucun fichier joint</Typography>}
      {(fichiers ?? []).map((file: any, index: number) => (
        <Box
          key={`${file.nomOriginal}_${index}`}
          className={classnames(classes.fileItem, classes.fileContainer)}
          onClick={openFile(`${file.publicUrl}`)}
        >
          <Box className={classes.filenameContainer} title={file.nomOriginal}>
            <Typography>{file.nomOriginal}</Typography>
          </Box>
        </Box>
      ))}
    </Box>
  );
};

export default FichierJoints;
