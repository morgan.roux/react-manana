import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    filesContainer: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      marginLeft: '10px',
      width: '100%',
      '@media (max-width: 1318px)': {
        margin: '5px 0px 0px',
      },
    },
    fileItem: {
      display: 'flex',
      alignItems: 'start',
      border: '1px solid #9E9E9E',
      padding: 4,
      marginLeft: 6,
      marginBottom: 6,
      '& .MuiTypography-root': {
        fontSize: '0.75rem',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        '-webkit-line-clamp': 1,
        '-webkit-box-orient': 'vertical',
        whiteSpace: 'nowrap',
        maxWidth: 110,
        width: '100%',
      },
    },
    filenameContainer: {
      display: 'flex',
      flexDirection: 'column',
    },
    fileContainer: {
      cursor: 'pointer',
      marginTop: 16,
    },
  })
);
