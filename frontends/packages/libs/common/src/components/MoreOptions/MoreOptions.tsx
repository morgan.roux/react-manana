import React, { MouseEvent, useState, FC } from 'react';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import { IconButton, Menu, MenuItem } from '@material-ui/core';
import { stopEvent } from '../../shared';
import { useStyles } from './styles';
export interface MoreOptionsItem {
  title: string;
  disabled?: boolean;
  onClick: (event: MouseEvent<any>) => void;
}

interface MoreOptionsProps {
  items: MoreOptionsItem[];
  active?: boolean;
}

const MoreOptions: FC<MoreOptionsProps> = ({ items, active }) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  // const ITEM_HEIGHT = 20;
  const classes = useStyles({});
  const handleClick = (event: MouseEvent<any>) => {
    stopEvent(event);
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (event: MouseEvent<any>) => {
    stopEvent(event);
    setAnchorEl(null);
  };

  const handleClickItem = (event: MouseEvent<any>, item: MoreOptionsItem) => {
    handleClose(event);
    event.stopPropagation();
    item.onClick(event);
  };

  return (
    <div>
      <IconButton onClick={handleClick} size="small">
        <MoreHorizIcon className={classes.black} />
      </IconButton>
      <Menu
        id="more-options-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        getContentAnchorEl={null}
        // PaperProps={{
        //   style: {
        //     maxHeight: ITEM_HEIGHT * 4.5,
        //   },
        // }}
      >
        {items.map((item) => (
          <MenuItem
            key={`more-options-item-key${item.title}`}
            onClick={(event) => handleClickItem(event, item)}
            disabled={item.disabled}
          >
            {item.title}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
};

export default MoreOptions;
