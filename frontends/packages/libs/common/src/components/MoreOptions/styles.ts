import { makeStyles, Theme, createStyles } from '@material-ui/core';
const drawerWidth = 328;
export const useStyles = makeStyles((theme) =>
  createStyles({
    white: {
      color: theme.palette.common.white,
    },
    black: {
      color: theme.palette.common.black,
    },
  })
);
