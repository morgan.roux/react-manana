import { useApplicationContext } from '@lib/common';
import { CustomSelect } from '@lib/ui-kit';
import React, { FC, useEffect } from 'react';
import { useUrgencesQuery } from '../../graphql';
interface UrgenceInputProps {
  name?: string;
  value: any;
  onChange: (importance: any) => void;
  useCode?: boolean; // Use code as value
  noMinHeight?: boolean;
  disabled?: boolean;
}
const UrgenceInput: FC<UrgenceInputProps> = ({ name, onChange, value, useCode = false, noMinHeight, disabled }) => {
  const { graphql } = useApplicationContext();
  const urgenceQuery = useUrgencesQuery({ client: graphql });
  const urgenceList = (urgenceQuery?.data?.urgences || []).map((urgence) => ({
    ...urgence,
    color: urgence?.couleur,
    libelle: `${urgence?.code} : ${urgence?.libelle}`,
  }));

  useEffect(() => {
    if (urgenceList.length > 0) {
      if (!value) {
        const urgenceSemaine = urgenceList.find((urgence: any) => urgence.code === 'B');
        urgenceSemaine && onChange(urgenceSemaine);
      }
    }
  }, [urgenceList]);

  const findUrgence = (code: string) =>
    urgenceList?.find((urgence: any) => (value && useCode ? urgence?.code === code : urgence?.id === code));

  const handleChange = (event: any) => {
    onChange(findUrgence(event.target.value));
  };

  const urgence: any = findUrgence(value);

  return (
    <CustomSelect
      noMinheight={noMinHeight}
      label="Urgence"
      list={urgenceList}
      name={name || 'idUrgence'}
      onChange={handleChange}
      listId={useCode ? 'code' : 'id'}
      index="libelle"
      value={value}
      required={true}
      color={urgence?.color}
      colorIndex="color"
      disabled={disabled}
    />
  );
};

export default UrgenceInput;
