import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) => ({
  leftcontainer: {
    borderRight: '1px solid #ccc',
    width: 400,
    // height: 'calc(100vh - 86px)',
    [theme.breakpoints.down('md')]: {
      height: 'calc(100vh - 150px)',
    },
  },
}));
