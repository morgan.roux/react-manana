import { NewCustomButton, NoItemContentImage } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { FC, PropsWithChildren, ReactNode } from 'react';
import { ErrorPage, Loader } from '@lib/ui-kit';
import { CardDocumentItem, MenuCardItem } from '../MenuCardItem';
import { useStyles } from './styles';
import { PageDetailDocument } from '../PageDetailDocument';
import { InfiniteItemList } from '../InfiniteItemList';
import {
  Identifiable,
  InfiniteItemListProps,
  OffsetPaginationBasedVariables,
} from '../InfiniteItemList/InfiniteItemList';
interface PageContainerWithDetailProps<
  ITem extends Identifiable,
  TData = any,
  TVariables = OffsetPaginationBasedVariables
> extends Omit<InfiniteItemListProps<ITem, TData, TVariables>, 'itemRenderer'> {
  addButtonLabel?: string;
  loading?: boolean;
  labelDeclarantInListMenu?: string;
  onClickAddButton?: () => void;
  error?: boolean;
  onClickItem?: (id: any) => void;
  documentSeleceted?: any;
  onClickCloturer?: (data?: any, id?: string) => void;
  onClickNext?: (currentID: string) => void;
  onClickPrev?: (currentID: string) => void;
  dateLabelDetail?: string;
  typeLabelDetail?: string;
  causeLabelDetail?: string;
  origineLabelDetail?: string;
  correspondantLabelDetail?: string;
  impactLabelDetail?: string;
  descriptionLabelDetail?: string;
  titleOrigineDetail?: string;
  refetchComments?: () => void;
  refetchSmyleys?: () => void;
  activeDocument?: any;
  tableSuiviReunion?: ReactNode;
  codeItemComment?: string;
  idSourceComment?: string;
  origine?: any;
  labelAddMenuAction?: string;
  handleVoirDetailMenuAction?: (data?: string) => void;
  handleEditMenuAction?: (data?: any, id?: string) => void;
  handleDeleteMenuAction?: (data?: any, id?: string) => void;
  handleApporterSolutionMenuAction?: (data?: any, id?: string) => void;
  page?: string;
  commentaires?: {
    total: number;
    data: any[];
    loading: boolean;
  };
  userSmyleys?: {
    data: any[];
    loading: boolean;
  };
}
export const PageContainerWithDetail = <
  ITem extends Identifiable,
  TData = any,
  TVariables = OffsetPaginationBasedVariables
>({
  page,
  addButtonLabel,
  labelDeclarantInListMenu,
  onClickAddButton,
  loading,
  error,
  onClickItem,
  documentSeleceted,
  onClickCloturer,
  onClickNext,
  onClickPrev,
  causeLabelDetail,
  correspondantLabelDetail,
  dateLabelDetail,
  descriptionLabelDetail,
  impactLabelDetail,
  origineLabelDetail,
  typeLabelDetail,
  titleOrigineDetail,
  refetchComments,
  refetchSmyleys,
  codeItemComment,
  idSourceComment,
  tableSuiviReunion,
  origine,
  handleVoirDetailMenuAction,
  labelAddMenuAction,
  handleEditMenuAction,
  handleDeleteMenuAction,
  handleApporterSolutionMenuAction,
  commentaires,
  userSmyleys,
  ...rest
}: PropsWithChildren<PageContainerWithDetailProps<ITem, TData, TVariables>>) => {
  const classes = useStyles();

  return (
    <Box display="flex" height={1}>
      <Box className={classes.leftcontainer}>
        <Box textAlign="center">
          <NewCustomButton startIcon={<Add />} onClick={onClickAddButton}>
            {addButtonLabel}
          </NewCustomButton>
        </Box>
        <InfiniteItemList<ITem, TData, TVariables>
          {...rest}
          itemRenderer={({ data, loading, refetch, registerChild, style }) => {
            return (
              <CardDocumentItem
                loading={loading}
                active={documentSeleceted?.id === data?.id}
                page={page}
                data={data}
                ref={registerChild}
                style={{ ...style, paddingInline: 10 }}
                labelDeclarant={labelDeclarantInListMenu}
                onClickItem={onClickItem}
                handleAddMenuAction={onClickAddButton}
                handleDeleteMenuAction={handleDeleteMenuAction}
                handleEditMenuAction={handleEditMenuAction}
                handleVoirDetailMenuAction={handleVoirDetailMenuAction}
                labelAddMenuAction={labelAddMenuAction}
                handleApporterSolutionMenuAction={handleApporterSolutionMenuAction}
                useDivider
              />
            );
          }}
        />
      </Box>
      <Box width="calc(100% - 400px)">
        {loading ? (
          <Loader />
        ) : (
          <>
            {documentSeleceted ? (
              <PageDetailDocument
                page={page}
                data={documentSeleceted}
                onClickCloturer={onClickCloturer}
                onClickNext={onClickNext}
                onClickPrev={onClickPrev}
                causeLabelDetail={causeLabelDetail}
                correspondantLabelDetail={correspondantLabelDetail}
                dateLabelDetail={dateLabelDetail}
                descriptionLabelDetail={descriptionLabelDetail}
                impactLabelDetail={impactLabelDetail}
                origineLabelDetail={origineLabelDetail}
                typeLabelDetail={typeLabelDetail}
                titleOrigineDetail={titleOrigineDetail}
                refetchComments={refetchComments}
                refetchSmyleys={refetchSmyleys}
                activeDocument={documentSeleceted}
                codeItemComment={codeItemComment}
                idSourceComment={idSourceComment}
                tableSuiviReunion={tableSuiviReunion}
                origine={origine}
                handleAddMenuAction={onClickAddButton}
                handleDeleteMenuAction={handleDeleteMenuAction}
                handleEditMenuAction={handleEditMenuAction}
                handleVoirDetailMenuAction={handleVoirDetailMenuAction}
                labelAddMenuAction={labelAddMenuAction}
                handleApporterSolutionMenuAction={handleApporterSolutionMenuAction}
                commentaires={commentaires}
                userSmyleys={userSmyleys}
              />
            ) : (
              <NoItemContentImage
                title="Détails de document"
                subtitle="Veuilez cliquer sur un document de la partie gauche ou bien même en ajouter un pour voir les détails dans cette partie"
              />
            )}
          </>
        )}
      </Box>
    </Box>
  );
};
