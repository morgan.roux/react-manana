import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    marginTop: 'unset',
    '& > div': {
      height: '48px !important',
      marginBottom: 0,
      '& .main-MuiInputBase-root, & .MuiInputBase-root': {
        height: '100%',
      },
    },
    // '& .main-MuiInputBase-root': {
    //   marginLeft: '7px',
    //   maxWidth: '267px',
    // },
    // [theme.breakpoints.down('md')]: {
    //   '& .main-MuiInputBase-root': {
    //     maxWidth: '99% !important',
    //     marginLeft: '7px !important',
    //   }
    // },
    // [theme.breakpoints.down('sm')]: {
    //   '& .main-MuiInputBase-root': {
    //     maxWidth: '98% !important',
    //     marginLeft: '7px !important',
    //   }
    // },[theme.breakpoints.down('xs')]: {
    //   '& .main-MuiInputBase-root': {
    //     maxWidth: '96% !important',
    //     marginLeft: '7px !important',
    //   }
    // }
  },
}));
