import React, { FC, useEffect, useRef, useState } from 'react';
import { debounce } from 'lodash';
import { useSearch_Parametre_LaboratoiresLazyQuery } from '../../federation';
import { useSearch_Custom_Content_PartenaireLazyQuery } from '../../graphql';

import useStyles from './style';
import classnames from 'classnames';
import { useApplicationContext } from '../../context';
import { CustomAutocomplete } from '@lib/ui-kit';

const isNumber = (value: string) => {
  try {
    return !isNaN(parseFloat(value));
  } catch (error) {
    return false;
  }
};

export interface PartenaireInputProps {
  className?: string;
  index: 'partenaire' | 'laboratoire';
  label?: string;
  value?: any;
  defaultValue?: any;
  onChange: (value: any) => void;
  disabled?: boolean;
  take?: number;
  includeRattaches?: boolean;
  includePrivate?: boolean;
}

const PartenaireInput: FC<PartenaireInputProps> = ({
  className,
  index,
  value,
  defaultValue,
  label,
  onChange,
  disabled,
  take = 20,
  includeRattaches = false,
  includePrivate = true,
}) => {
  const classes = useStyles();
  const { graphql, federation, notify, currentPharmacie } = useApplicationContext();

  const [searchText, setSearchText] = useState<string>('');
  const [initialized, setInitialized] = useState<boolean>(false);

  const getRattaches = () => {
    return index === 'laboratoire' && !includeRattaches
      ? { exists: { field: 'laboratoireRattachement.id' } }
      : undefined;
  };

  const [searchSelectedLaboratoire, searchSelectedLaboratoiresResult] = useSearch_Parametre_LaboratoiresLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: 'Une erreur est survenue lors du chargement des laboratoires',
        });
      });
    },
    onCompleted: (result) => {
      if (typeof value === 'string' && result.search?.data?.length === 1) {
        onChange(result.search?.data[0]);
      }
    },
  });

  // Get laboratoires list
  const [searchLaboratoires, searchLaboratoiresResult] = useSearch_Parametre_LaboratoiresLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: 'Une erreur est survenue lors du chargement des laboratoires',
        });
      });
    },
  });

  const [searchSelectedPartenaire, searchSelectedPartenaireResult] = useSearch_Custom_Content_PartenaireLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: graphql,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: 'Une erreur est survenue lors du chargement des prestataires de service',
        });
      });
    },
  });

  const [searchPartenaires, searchPartenairesResult] = useSearch_Custom_Content_PartenaireLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: graphql,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: 'Une erreur est survenue lors du chargement des prestataires de service',
        });
      });
    },

    onCompleted: (data) => {
      if (!initialized && defaultValue && data?.search?.data) {
        setInitialized(true);

        const defaultValuePartenaire: any = data.search.data.find(({ id }: any) => defaultValue == id);

        if (defaultValuePartenaire) {
          onChange(defaultValuePartenaire);
          // setSearchText(defaultValuePartenaire.nom)
        }
      }
    },
  });

  const debouncedSearch = useRef(
    debounce((searchText?: string) => {
      const must: any[] = [];
      const must_not = getRattaches();

      if (searchText) {
        let formattedSearchText = searchText.replace(/[&\/\\#,+()$~%.'":\-?<>{}]/g, ' ');
        if (isNumber(formattedSearchText)) {
          formattedSearchText = `"${formattedSearchText}"`;
        }
        must.push({
          query_string: {
            query:
              formattedSearchText.startsWith('*') || formattedSearchText.endsWith('*')
                ? formattedSearchText
                : `*${formattedSearchText}*`,
          },
        });
      }

     /* if (includePrivate && ['laboratoire', 'partenaire'].includes(index)) {
        const should = [
          {
            bool: {
              must_not: {
                exists: {
                  field: 'idPharmacie',
                },
              },
            },
          },

          {
            bool: {
              must: [
                {
                  exists: {
                    field: 'idPharmacie',
                  },
                },
                {
                  term: {
                    idPharmacie: currentPharmacie.id,
                  },
                },
              ],
            },
          },
        ];

        must.push({
          bool: {
            should,
          },
        });
      }*/

      const query =
        must.length === 0
          ? undefined
          : must.length === 1
          ? {
              query: {
                bool: {
                  must: must[0],
                  must_not,
                },
              },
            }
          : {
              query: {
                bool: {
                  should: must,
                  must_not,
                },
              },
            };

      switch (index) {
        case 'laboratoire':
          searchLaboratoires({
            variables: {
              index: ['laboratoires'],
              query,
              sortBy: [{ nom: { order: 'asc' } }],
              skip: 0,
              take,
              //  idPharmacie: currentPharmacie.id,
            },
          });
          break;
        case 'partenaire': {
          searchPartenaires({
            variables: {
              type: ['partenaire'],
              query,
              skip: 0,
              take,
            },
          });
        }
      }
    }, 1000)
  );

  useEffect(() => {
    debouncedSearch.current();
  }, []);

  useEffect(() => {
    if (value) {
      setSearchText('');
      const id = typeof value === 'string' ? value : value.id;
      if (!id) {
        return;
      }

      if (!id) {
        return;
      }

      /*if (typeof value === 'object' && value.nom !== searchText) {
        setSearchText(value.nom);
      }*/

      if (index === 'laboratoire') {
        searchSelectedLaboratoire({
          variables: {
            index: ['laboratoires'],
            //  idPharmacie: currentPharmacie.id,
            query: {
              query: {
                bool: {
                  must: [
                    {
                      term: {
                        _id: id,
                      },
                    },
                  ],
                },
              },
            },
          },
        });
      } else {
        searchSelectedPartenaire({
          variables: {
            type: ['partenaire'],
            query: {
              query: {
                bool: {
                  must: [
                    {
                      term: {
                        _id: id,
                      },
                    },
                  ],
                },
              },
            },
          },
        });
      }
    }
  }, [value]);

  // Debounced Search Laboratoire
  useEffect(() => {
    debouncedSearch.current(searchText);
  }, [searchText]);

  let options =
  ((index === 'laboratoire' ? searchLaboratoiresResult.data?.search?.data : searchPartenairesResult.data?.search?.data) || []).filter(({ prive, idPharmacie }: any) => (!prive || idPharmacie === currentPharmacie.id));

  const loading = index === 'laboratoire' ? searchLaboratoiresResult.loading : searchPartenairesResult.loading;
  //const error = index === 'laboratoire' ? searchLaboratoiresResult.error : searchPartenairesResult.error;

  const selectedValue: any =
    index === 'laboratoire' && value && searchSelectedLaboratoiresResult.data?.search?.data?.length
      ? searchSelectedLaboratoiresResult.data.search.data[0]
      : index === 'partenaire' && value && searchSelectedPartenaireResult.data?.search?.data?.length
      ? searchSelectedPartenaireResult.data.search.data[0]
      : undefined;

  if (selectedValue && options) {
    const selectedValueIndex = (options || []).findIndex((option: any) => option.id === selectedValue.id);
    if (selectedValueIndex >= 0) {
      options = [...options.slice(0, selectedValueIndex), selectedValue, ...options.slice(selectedValueIndex + 1)];
    } else {
      options = [...options, selectedValue];
    }
  }

  return (
    <CustomAutocomplete
      classes={
        {
          root: classnames(classes.root, className),
        } as any
      }
      loading={loading}
      options={options || []}
      optionLabelKey="nom"
      value={value}
      onAutocompleteChange={onChange}
      search={setSearchText}
      label={label}
      required={true}
      disabled={disabled}
      placeholder={index === 'laboratoire' ? 'Choisir le laboratoire' : 'Choisir le prestataire de service'}
    />
  );
};

export default PartenaireInput;
