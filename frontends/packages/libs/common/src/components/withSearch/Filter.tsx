import React, { FC } from 'react';
import { useStyles } from './styles';
import FilterContainer from '../Filter/FilterContainer';

interface FilterProps {
  filterProps: any;
}

const Filter: FC<FilterProps> = ({ filterProps }) => {
  const classes = useStyles({});
  return (
    <nav className={classes.drawer}>
      <FilterContainer
        key={filterProps.key}
        title={filterProps.title}
        sortParam={filterProps.sortParams}
        filterParam={filterProps.filterParams}
        placeholder={filterProps.placeholder}
        sortCanal={filterProps.sortCanal}
      />
    </nav>
  );
};
export default Filter;
