import { useQuery } from '@apollo/client';
import { Box } from '@material-ui/core';
import gql from 'graphql-tag';
import React, { FC, useContext, useEffect, useState } from 'react';

import { useApplicationContext, useSearchContext } from '../../context';
//import { ContentContext, ContentStateInterface } from '../../../AppContext';
//import { CODE_LIGNE_TABLEAU } from '../../../Constant/parameter';
//import { GET_PARAMETER_BY_CODE } from '../../../graphql/Parametre/query';
//import { PARAMETER_BY_CODE, PARAMETER_BY_CODEVariables } from '../../../graphql/Parametre/types/PARAMETER_BY_CODE';

import Filter from './Filter';
import { BuildQuery } from './queryBuilder';

export const GET_VIEW_MODE = gql`
  {
    viewMode @client
  }
`;

export const GET_SKIP_AND_TAKE = gql`
  {
    skipAndTake @client {
      skip
      take
    }
  }
`;

export const NULL_QUERY = gql`
  {
    nullQuery @client
  }
`;

interface WithSearchProps {
  // TODO : Solution provisoire
  useFederation?: boolean;

  WrappedComponent?: any;
  children?: any;
  columns?: any;
  type: string;
  take?: number;
  skip?: number;
  filterProps?: any;
  searchQuery?: any;
  customContentQuery?: any;
  isSelectable?: boolean;
  viewMode?: string;
  optionalMust?: any;
  optionalMustNot?: any[];
  optionalShould?: any[];
  sortByProps?: any[]; // type => [{ [sortName]: { order } }]
  defaultSort?: any[];
  minimumShouldMatch?: number;
  optionalVariables?: any;
  tableMode?: boolean;
  noFilter?: boolean;
  props?: any;
  fetchPolicy?: any;
  unableRefetch?: boolean;
  skipQueryValue?: boolean;
  skipSearch?: boolean;
  fields?: string[];
}

const useWithSearch = (params: WithSearchProps) => {
  const {
    type,
    take,
    skip,
    searchQuery,
    optionalMust,
    optionalMustNot,
    optionalShould,
    minimumShouldMatch,
    optionalVariables,
    tableMode,
    noFilter,
    skipQueryValue,
    fetchPolicy,
    unableRefetch,
    skipSearch,
    sortByProps,
    fields,
    defaultSort,
    useFederation = false,
  } = params;
  const [currentTake, setTake] = useState<number>(take || 12);
  const [currentSkip, setSkip] = useState<number>(skip || 0);
  const { currentPharmacie, graphql, federation, currentGroupement } = useApplicationContext();

  const searchContext = useSearchContext();

  const query = noFilter
    ? {}
    : BuildQuery({
        optionalMust,
        optionalMustNot,
        optionalShould,
        minimumShouldMatch,
        type,
        sortByProps,
        fields,
        defaultSort,
      });

  /*const myParameter = useQuery<PARAMETER_BY_CODE, PARAMETER_BY_CODEVariables>(GET_PARAMETER_BY_CODE, {
    variables: { code: CODE_LIGNE_TABLEAU },
  });*/

  useEffect(() => {
    if (searchContext?.skip && searchContext?.take) {
      setSkip(searchContext.skip);
      setTake(searchContext.take);
    }
  }, [searchContext]);

  const groupementId = currentGroupement.id;

  //const skipQuery = (!skipQueryValue && (!query || !myParameter.data)) || skipSearch ? true : false;

  const skipQuery = (!skipQueryValue && !query) || skipSearch ? true : false;

  const commonVariables = {
    query,
    skip: currentSkip,
    take: currentTake,
  };

  const searchResult = useQuery<any, any>(searchQuery, {
    variables: useFederation
      ? {
          index: [type],
          ...commonVariables,
        }
      : {
          type: [type],
          idPharmacie: currentPharmacie && currentPharmacie.id,
          ...commonVariables,
          ...optionalVariables,
          idGroupement: groupementId,
        },
    skip: skipQuery,
    fetchPolicy: fetchPolicy || 'cache-first',
    client: useFederation ? federation : graphql,
  });

  /*const {
    content: { page, rowsPerPage, searchPlaceholder, contextQuery },
    setContent,
  } = useContext<ContentStateInterface>(ContentContext);
*/
  useEffect(() => {
    if (searchResult && searchResult.data && searchResult.variables) {
      if (!unableRefetch) searchResult.refetch();
      /* setContent({
        page,
        type,
        searchPlaceholder,
        rowsPerPage,
        variables: searchResult.variables,
        operationName: searchQuery,
        refetch: searchResult.refetch,
        contextQuery,
      });*/
    }
  }, [
    /*contextQuery, page, rowsPerPage, searchPlaceholder,*/ searchQuery,
    searchResult,
    /*setContent,*/ type,
    unableRefetch,
  ]);
  return { searchResult };
};
export default useWithSearch;
