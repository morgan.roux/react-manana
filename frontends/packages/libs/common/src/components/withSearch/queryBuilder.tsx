import { useApplicationContext } from '@lib/common';
import { useFilters } from '@lib/common/src/shared';

export const BuildQuery = (props: any) => {
  const { optionalMust, optionalMustNot, optionalShould, minimumShouldMatch, type, sort, fields } = props;
  const { currentGroupement: groupement, currentPharmacie } = useApplicationContext();
  const { sortItem, idCheckedsQuery, myPharmacieFilter, filters, searchText, pagination, queryStrings } = useFilters();

  const skip = pagination.data?.pagination.skip || props.skip || 0;
  const take = pagination.data?.pagination.take || props.take || 12;

  const idCheckeds = (idCheckedsQuery && idCheckedsQuery.data && idCheckedsQuery.data.idCheckeds) || [];

  // set order from sortItem
  const order =
    sortItem && sortItem.data && sortItem.data.sort && sortItem.data.sort.sortItem
      ? sortItem.data.sort.sortItem.direction
      : 'asc';

  // set sortName from sortItem
  const sortName =
    (sortItem &&
      sortItem.data &&
      sortItem.data.sort &&
      sortItem.data.sort.sortItem &&
      sortItem.data.sort.sortItem.name) ||
    '';

  const sortQuery = sort ? sort : sortName ? [{ [sortName]: { order } }] : [];

  let must: any = [];
  let mustNot: any[] = [];

  if (queryStrings) {
    must = must.concat(queryStrings);
  }

  if (myPharmacieFilter?.data?.myPharmacieFilter) {
    must.push({ terms: { 'produit.pharmacieReferences': [currentPharmacie?.id] } });
  }

  if (optionalMust) {
    must = must.concat(optionalMust);
  }

  if (optionalMustNot) {
    mustNot = mustNot.concat(optionalMustNot);
  }

  if (
    searchText &&
    searchText.data &&
    searchText.data.searchText &&
    searchText.data.searchText.text &&
    searchText.data.searchText.text.length
  ) {
    const textSearch = searchText.data.searchText.text.replace(/\s+/g, '\\ ').replace('/', '\\/');
    must.push({
      query_string: {
        query: textSearch.startsWith('*') || textSearch.endsWith('*') ? textSearch : `*${textSearch}*`,
        fields: fields && fields.length ? fields : [],
        analyze_wildcard: true,
      },
    });
  }

  if (idCheckeds.length > 0) {
    must.push({ terms: { _id: idCheckeds } });
  } else if (filters && filters.data && filters.data.filters) {
    const filter = filters.data.filters;

    if (filter.idCommandeCanal && type && type === 'produitcanal') {
      must.push({
        term: {
          'commandeCanal.code': filter.idCommandeCanal,
        },
      });
    } else if (!filter.idCommandeCanal && type && type === 'produitcanal') {
      must.push({
        term: {
          'commandeCanal.code': 'PFL',
        },
      });
    }
    if (filter.idCommandeCanals && filter.idCommandeCanals.length && type && type === 'produit') {
      filter.idCommandeCanals.map((id) => {
        must.push({
          nested: {
            path: 'canauxArticle',
            query: {
              bool: {
                must: [
                  {
                    match: {
                      'canauxArticle.commandeCanal.id': id,
                    },
                  },
                  {
                    match: {
                      'canauxArticle.isRemoved': false,
                    },
                  },
                ],
              },
            },
          },
        });
      });
    }
    if (filter.idLaboratoires && filter.idLaboratoires.length) {
      must.push(
        type === 'produitcanal'
          ? { terms: { 'produit.produitTechReg.laboExploitant.id': filter.idLaboratoires } }
          : { terms: { 'produitTechReg.laboExploitant.id': filter.idLaboratoires } }
      );
    }
    if (filter.idGammesCommercials && filter.idGammesCommercials.length) {
      must.push({
        terms: { 'sousGammeCommercial.gammeCommercial.id': filter.idGammesCommercials },
      });
    }
    if (filter.idFamilles && filter.idFamilles.length) {
      const familles = filter.idFamilles.map((codeFamille) =>
        type === 'produitcanal'
          ? {
              prefix: {
                'produit.famille.codeFamille': codeFamille,
              },
            }
          : {
              prefix: {
                'famille.codeFamille': codeFamille,
              },
            }
      );
      must.push({ bool: { should: familles } });
    }
    if (filter.idRemise) {
      if (filter.idRemise.includes(0)) {
        must.push({
          terms: {
            'pharmacieRemisePaliers.id': [currentPharmacie && currentPharmacie.id],
          },
        });
      }
      if (filter.idRemise.includes(1)) {
        must.push({
          terms: {
            'pharmacieRemisePanachees.id': [currentPharmacie && currentPharmacie.id],
          },
        });
      }
    }
    if (filter.idCommandeTypes && filter.idCommandeTypes.length) {
      must.push({
        terms: {
          'commandeType.id': filter.idCommandeTypes,
        },
      });
    }
    if (filter.idOcCommande) {
      if (filter.idOcCommande.includes(0)) {
        must.push({
          term: {
            commandePassee: true,
          },
        });
      }
      if (filter.idOcCommande.includes(1)) {
        must.push({
          term: {
            commandePassee: false,
          },
        });
      }
    }
    if (filter.sortie) {
      if (filter.sortie.length === 1) {
        if (filter.sortie.includes(0)) {
          must.push({
            term: {
              sortie: 0,
            },
          });
        }
        if (filter.sortie.includes(1)) {
          must.push({
            term: {
              sortie: 1,
            },
          });
        }
      }
    }

    if (filter.laboType && type === 'laboratoire') {
      if (filter.laboType.length === 1) {
        if (filter.laboType.includes(0)) {
          must.push({
            exists: {
              field: 'laboratoirePartenaire',
            },
          });
        }
        if (filter.laboType.includes(1)) {
          mustNot.push({
            exists: {
              field: 'laboratoirePartenaire',
            },
          });
        }
      }
    }

    if (filter.reaction && type === 'produitcanal' && groupement) {
      if (filter.reaction.length === 1) {
        if (filter.reaction.includes(0)) {
          must.push({
            term: {
              'groupementComments.id': groupement && groupement.id,
            },
          });
        }
        if (filter.reaction.includes(1)) {
          must.push({
            term: {
              'groupementSmyleys.id': groupement && groupement.id,
            },
          });
        }
      } else {
        if (filter.reaction.includes(0) && filter.reaction.includes(1)) {
          must.push(
            {
              term: {
                'groupementComments.id': groupement && groupement.id,
              },
            },
            {
              term: {
                'groupementSmyleys.id': groupement && groupement.id,
              },
            }
          );
        }
      }
    }
  }

  const query = sortQuery
    ? {
        query: {
          bool: {
            must,
            must_not: mustNot,
            should: optionalShould,
            minimum_should_match: minimumShouldMatch,
          },
        },

        sort: sortQuery,
      }
    : {
        query: {
          bool: {
            must,
            must_not: mustNot,
            should: optionalShould,
            minimum_should_match: minimumShouldMatch,
          },
        },
      };

  return {
    query:
      filters && filters.data && sortItem && sortItem.data && currentPharmacie ? query : optionalMust ? query : null,
    skip,
    take,
  };
};
