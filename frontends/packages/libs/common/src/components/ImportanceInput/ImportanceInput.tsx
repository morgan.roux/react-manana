import React, { FC, useEffect, useState } from 'react';
import CustomSelectTask from './CustomSelectTask';
import { Box, FormControl, ListItem, ListItemText, Radio, Typography } from '@material-ui/core';
import { useImportancesQuery } from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';

interface ImportanceInputProps {
  name?: string;
  value: any;
  onChange: (importance: any) => void;
  NoMarginBottom?: boolean;
  disabled?: boolean;
  radioImportance?: boolean;
  useInSuivi?: boolean;
  label?: string;
}
const ImportanceInput: FC<ImportanceInputProps> = ({
  name,
  onChange,
  value,
  NoMarginBottom,
  disabled,
  radioImportance,
  useInSuivi,
  label,
}) => {
  const [checkedImportances, setCheckedImportances] = useState(value || []);

  const { federation } = useApplicationContext();

  const { data } = useImportancesQuery({
    client: federation,
  });

  const importances: any = [...(data?.importances.nodes || [])]
    .sort((a, b) => a.ordre - b.ordre)
    .map((et) => ({ ...et, nom: et.libelle, couleur: { code: et.couleur } }))
    .map((el) => {
      if (radioImportance) {
        return el.ordre === 2 ? { ...el, nom: 'Moyenne' } : { ...el, nom: 'Haute' };
      } else {
        return el;
      }
    });

  const handleChange = (event: any) => {
    onChange(importances.find(({ id }: any) => id === event.target.value));
  };

  useEffect(() => {
    if (data) {
      if (!value) {
        const moyenne = data?.importances.nodes?.find(({ ordre }: any) => ordre === 2);
        onChange(moyenne);
      } else {
        handleChange({ target: { value } });
      }
    }
  }, [data]);

  return !radioImportance ? (
    <CustomSelectTask
      NoMarginBottom={NoMarginBottom}
      multiple={false}
      label="Importance"
      list={importances}
      name={name || 'idImportance'}
      onChange={handleChange}
      listId="id"
      index="nom"
      checkeds={checkedImportances}
      setCheckeds={setCheckedImportances}
      value={[value]}
      required={true}
      disabled={disabled}
    />
  ) : (
    <FormControl>
      <Box display="flex">
        <Typography style={{ marginRight: 5 }}>{label ? label : 'Impact'}</Typography>{' '}
        <Typography color="secondary"> *</Typography>
      </Box>
      <Box display="flex">
        {importances.map((importance: any) => (
          <ListItem onClick={() => onChange(importance)}>
            <Radio checked={value === importance.id} name={name} tabIndex={-1} disableRipple={true} />
            <ListItemText primary={importance.ordre === 1 ? 'Fort' : 'Faible'} />
          </ListItem>
        ))}
      </Box>
    </FormControl>
  );
};

export default ImportanceInput;
