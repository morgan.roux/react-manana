import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      justifyContent: 'flex-start',
      padding: '10px 15px',
    },
  })
);
