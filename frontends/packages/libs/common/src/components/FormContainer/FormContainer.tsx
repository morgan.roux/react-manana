import * as React from 'react';
import { Typography } from '@material-ui/core';
import useStyles from './styles';
import classnames from 'classnames';

interface IInfoPersoFormProps {
  title?: string;
  className?: string;
}

const FormContainer: React.FC<IInfoPersoFormProps> = (props) => {
  const { title, children, className } = props;
  const classes = useStyles({});
  return (
    <div className={classes.infoPersoContainer}>
      <Typography color="primary" className={classes.inputTitle}>
        {title}
      </Typography>
      <div className={className ? classnames(classes.inputsContainer, className) : classes.inputsContainer}>
        {children}
      </div>
    </div>
  );
};

export default FormContainer;
