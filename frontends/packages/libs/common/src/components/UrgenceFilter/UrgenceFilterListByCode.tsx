import { Box, Checkbox, List, ListItem, ListItemText, Typography } from '@material-ui/core';
import { Error } from '@material-ui/icons';
import React, { FC } from 'react';
import { useApplicationContext } from '@lib/common';
import { useGet_UrgencesQuery } from '@lib/common/src/federation';
import { LoaderSmall } from '../../../../ui-kit/src';
import { useStyles } from './styles';

interface UrgenceFilterListCodeIdProps {
  codes?: string[];
  setUrgences: (urgence: any) => void;
}

const UrgenceFilterListByCode: FC<UrgenceFilterListCodeIdProps> = ({ codes, setUrgences }) => {
  const classes = useStyles({});
  const { federation } = useApplicationContext();
  const loadingUrgences = useGet_UrgencesQuery({ client: federation });

  const isChecked = (el: any) => {
    return (codes || []).some((code) => code === el.code);
  };

  const handleChange = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, el: any) => {
    setUrgences(
      (codes || []).includes(el.code) ? codes?.filter((code) => code !== el.code) : (codes || []).concat(el.code)
    );
  };

  if (loadingUrgences.loading) {
    return <LoaderSmall />;
  }

  return (
    <List>
      {loadingUrgences.data?.urgences.nodes?.map((urgence) => (
        <ListItem
          dense={true}
          button={true}
          key={`urgence-filter-${urgence.id}`}
          onClick={(event) => handleChange(event, urgence)}
        >
          <Box display="flex" alignItems="center">
            <Checkbox checked={isChecked(urgence)} tabIndex={-1} disableRipple={true} />
            <ListItemText
              primary={
                <Box display="flex" alignItems="center">
                  <Error style={{ color: urgence.couleur || 'black' }} />
                  <Typography className={classes.title} style={{ color: urgence.couleur || 'black' }}>
                    {`${urgence.code} : ${urgence.libelle}`}
                  </Typography>
                </Box>
              }
            />
          </Box>
          {/*
                      <Box>
                        <Typography className={classes.nbrProduits}>{item.count}</Typography>
                      </Box>
                    */}
        </ListItem>
      ))}
    </List>
  );
};

export default UrgenceFilterListByCode;
