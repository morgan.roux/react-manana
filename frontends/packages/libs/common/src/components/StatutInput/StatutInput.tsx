import React, { FC } from 'react';
import { useApplicationContext } from '@lib/common';
import { useStyles } from './styles';
import { useGet_StatutsQuery } from '@lib/common/src/federation';
import { CustomSelect } from '@lib/ui-kit';

interface StatutInputProps {
  name?: string;
  filterType: string;
  value: any;
  handleChange: any;
}
const StatusInput: FC<StatutInputProps> = ({ name, handleChange, value, filterType }) => {
  const {federation} = useApplicationContext();
  const classes = useStyles({});
  const statutsQuery = useGet_StatutsQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const statuts = statutsQuery.data?.dQStatuts?.nodes.filter(statut => statut.type === filterType);
  return (
    <CustomSelect
      key={value?.id}
      disabled={statutsQuery.loading || statutsQuery.error}
      list={statuts || []}
      listId="id"
      index="libelle"
      value={value?.id}
      variant="standard"
      //onChange={(e: any) => changeStatut(e, row)}
      onChange={handleChange}
      fullWidth={false}
    />
  );
};

export default StatusInput;
