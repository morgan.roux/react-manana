import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export default makeStyles(() => ({
  iframeView: {
    width: '100%',
    border: 'none',
  },

  pdfView: {
    width: '100%',
    border: '1px solid rgba(0, 0, 0, 0.3)',
    display: 'flex',
    flexDirection: 'column',
  },
}));
