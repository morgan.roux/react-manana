import React, { CSSProperties, FC } from 'react';
import { Viewer } from '@react-pdf-viewer/core';
import { pageNavigationPlugin, RenderCurrentPageLabelProps } from '@react-pdf-viewer/page-navigation';
import Iframe from 'react-iframe';
import useStyles from './style';
import classNames from 'classnames';
import { useApplicationContext } from '../../context';
import { ApplicationConfig } from '../../config';

interface FileViewerProps {
  fileUrl: string | Uint8Array;
  className?: string;
  style?: CSSProperties;
  config?: ApplicationConfig;
}

const IMAGES_EXTENSIONS = ['.jpg', '.png', '.jpeg', '.svg'];

export const FileViewer: FC<FileViewerProps> = ({ fileUrl, className, style, config: providedConfig }) => {
  const classes = useStyles();
  const pageNavigationPluginInstance = pageNavigationPlugin();
  const { CurrentPageLabel } = pageNavigationPluginInstance;

  const appContext = useApplicationContext();

  const { config: contextConfig } = appContext;

  const config = {
    ...(contextConfig || {}),
    ...(providedConfig || {}),
  };

  if (typeof fileUrl !== 'string') {
    return (
      <div style={style} className={classNames(classes.pdfView, className)}>
        <div
          style={{
            alignItems: 'center',
            backgroundColor: '#eeeeee',
            borderBottom: '1px solid rgba(0, 0, 0, 0.1)',
            display: 'flex',
            justifyContent: 'center',
            padding: '8px',
          }}
        >
          <CurrentPageLabel>
            {(props: RenderCurrentPageLabelProps) => <span>{`${props.currentPage + 1} / ${props.numberOfPages}`}</span>}
          </CurrentPageLabel>
        </div>
        <div
          style={{
            flex: 1,
            overflow: 'hidden',
          }}
        >
          <Viewer fileUrl={fileUrl} plugins={[pageNavigationPluginInstance]} />
        </div>
      </div>
    );
  }

  const fileExtension = fileUrl.toLowerCase().substr(fileUrl.lastIndexOf('.'));

  if (fileExtension === '.pdf') {
    if (!config?.PDF_FILE_VIEWER || config?.PDF_FILE_VIEWER === 'COMPONENT') {
      return (
        <div style={style} className={classNames(classes.pdfView, className)}>
          <div
            style={{
              alignItems: 'center',
              backgroundColor: '#eeeeee',
              borderBottom: '1px solid rgba(0, 0, 0, 0.1)',
              display: 'flex',
              justifyContent: 'center',
              padding: '8px',
            }}
          >
            <CurrentPageLabel>
              {(props: RenderCurrentPageLabelProps) => (
                <span>{`${props.currentPage + 1} / ${props.numberOfPages}`}</span>
              )}
            </CurrentPageLabel>
          </div>
          <div
            style={{
              flex: 1,
              overflow: 'hidden',
            }}
          >
            <Viewer fileUrl={fileUrl} plugins={[pageNavigationPluginInstance]} />
          </div>
        </div>
      );
    }

    if (config?.PDF_FILE_VIEWER === 'IFRAME') {
      return (
        <Iframe
          url={fileUrl}
          width="100%"
          height="100%"
          className={classNames(classes.iframeView, className)}
          display="block"
          position="relative"
        />
      );
    }
  }

  if (IMAGES_EXTENSIONS.indexOf(fileExtension) >= 0) {
    return (
      <div className={className}>
        <img src={fileUrl} />
      </div>
    );
  }

  return (
    <iframe
      key={Math.random()}
      className={classNames(classes.iframeView, className)}
      src={`https://docs.google.com/viewer?url=${fileUrl}?hl=fr&pid=explorer&efh=false&a=v&chrome=false&embedded=true`}
      frameBorder="0"
      width="100%"
      height="100%"
    />
  );
};
