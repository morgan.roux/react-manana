import { makeStyles, Theme, createStyles, darken, lighten } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customTableroot: {
      width: '100%',
      '& *': {
        fontWeight: 'bold',
      },
      '& .MuiTableHead-root': {
        '& *': {
          color: theme.palette.common.white,
          fontSize: 12,
        },
        '& th': {
          background: lighten(theme.palette.primary.main, 0.1),
          minWidth: 130,
        },
      },
    },
    customTableContainer: {
      maxHeight: 500,
    },
    toolbar: {
      padding: 0,
    },
    table: {
      minWidth: 750,
      '& svg': {
        width: 20,
        height: 20,
      },
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
    activeRow: {
      backgroundColor: theme.palette.secondary.main,
      '& *': {
        color: `${theme.palette.common.white} !important`,
      },
      '&:hover': {
        backgroundColor: `${darken(theme.palette.secondary.main, 0.1)} !important`,
      },
    },
    cursorPointer: {
      cursor: 'pointer',
    },
  })
);

export default useStyles;
