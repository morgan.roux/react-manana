import React, { MouseEvent, Dispatch, SetStateAction, ReactNode, FC } from 'react';
import { IconButton, TablePagination, LabelDisplayedRowsArgs } from '@material-ui/core';
import { makeStyles, useTheme, Theme, createStyles } from '@material-ui/core';
import { KeyboardArrowRight, KeyboardArrowLeft, LastPage, FirstPage } from '@material-ui/icons';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexShrink: 0,
      marginLeft: theme.spacing(2.5),
      '& .MuiIconButton-root': {
        color: theme.palette.common.black,
      },
      '& .MuiIconButton-root.Mui-disabled': {
        color: 'rgba(0, 0, 0, 0.54)',
      },
    },
  })
);

export interface SimpleCustomTablePaginationProps {
  total: number;
  page: number;
  setPage: Dispatch<SetStateAction<number>>;
  rowsPerPage: number;
  setRowsPerPage: Dispatch<SetStateAction<number>>;
}

const SimpleCustomTablePagination: FC<SimpleCustomTablePaginationProps> = ({
  total,
  page,
  setPage,
  rowsPerPage,
  setRowsPerPage,
}) => {
  const classes = useStyles({});
  const theme = useTheme();

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newRowsPerPage = parseInt(event.target.value, 10);
    if (rowsPerPage !== newRowsPerPage) {
      setRowsPerPage(newRowsPerPage);
      // setPage(0);
    }
  };

  const handleClickFirstPage = (event: MouseEvent<HTMLButtonElement>) => {
    handleChangePage(event, 0);
  };

  const handleClickPrev = (event: MouseEvent<HTMLButtonElement>) => {
    handleChangePage(event, page - 1);
  };

  const handleClickNext = (event: MouseEvent<HTMLButtonElement>) => {
    handleChangePage(event, page + 1);
  };

  const handleClickLastPage = (event: MouseEvent<HTMLButtonElement>) => {
    handleChangePage(event, Math.max(0, Math.ceil(total / rowsPerPage) - 1));
    const newSkip = total - rowsPerPage;
  };

  const labelDisplayedRows = (paginationInfo: LabelDisplayedRowsArgs): ReactNode => {
    const { from, to, count } = paginationInfo;
    return `${from}-${to === -1 ? count : to} sur ${count}`;
  };

  const disabledPrev: boolean = page === 0;
  const disabledNext: boolean = page >= Math.ceil(total / rowsPerPage) - 1;

  const SimpleCustomTablePaginationActions = () => {
    return (
      <div className={classes.root}>
        <IconButton onClick={handleClickFirstPage} disabled={disabledPrev} aria-label="first page">
          {theme.direction === 'rtl' ? <LastPage /> : <FirstPage />}
        </IconButton>
        <IconButton onClick={handleClickPrev} disabled={disabledPrev} aria-label="previous page">
          {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
        </IconButton>
        <IconButton onClick={handleClickNext} disabled={disabledNext} aria-label="next page">
          {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
        </IconButton>
        <IconButton onClick={handleClickLastPage} disabled={disabledNext} aria-label="last page">
          {theme.direction === 'rtl' ? <FirstPage /> : <LastPage />}
        </IconButton>
      </div>
    );
  };

  return (
    <div className={classes.root}>
      <TablePagination
        component="div"
        rowsPerPageOptions={[5, 10, 25, 50, 100]}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        count={total}
        onChangeRowsPerPage={handleChangeRowsPerPage}
        SelectProps={{
          inputProps: { 'aria-label': 'lignes par page' },
          native: true,
        }}
        labelRowsPerPage="Nombre de lignes par page :"
        labelDisplayedRows={labelDisplayedRows}
        ActionsComponent={SimpleCustomTablePaginationActions}
      />
    </div>
  );
};

export default SimpleCustomTablePagination;
