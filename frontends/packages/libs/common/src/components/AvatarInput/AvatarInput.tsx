import { Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import useStyles from './styles';
import classNames from 'classnames';
import { CustomAvatarGroup } from '@lib/ui-kit';

export interface AvatarInputProps {
  classes?: {
    root?: string;
    input?: string;
  };
  label?: string;
  list: any[];
  small?: boolean;
  icon?: any;
  standard?: boolean;
  isAllTeamSelected?: boolean;
  emailOnly?: boolean;
}

const AvatarInput: FC<AvatarInputProps> = ({
  label,
  list,
  isAllTeamSelected,
  standard,
  icon,
  classes: customClasses,
  emailOnly,
}) => {
  const classes = useStyles({});

  if (standard) {
    return (
      <Box className={classNames(classes.collaborateurBox, customClasses?.root)} id="collaborateur-input">
        <Typography component="span" style={{ display: 'flex', alignItems: 'center' }}>
          {label || ''}
        </Typography>
        <Box display="flex" flexDirection="row" alignItems="center" paddingLeft="8px">
          {emailOnly ? (
            (list.length > 0 ? list : []).map((item: any, index: number) => {
              console.log(item);
              return (
                <span key={index}>
                  {item.contact?.mailPerso && item.contact?.mailPerso}
                  {list.length > 1 && index + 1 < list.length ? <span>, &nbsp;</span> : ''}
                </span>
              );
            })
          ) : (
            <CustomAvatarGroup users={list} max={5} />
          )}
        </Box>
        {icon}
      </Box>
    );
  }
  return (
    <fieldset className={classNames(classes.collaborateurInput, customClasses?.root)}>
      {label && <legend>{label}</legend>}
      <Box
        className={classNames(classes.collaborateurBox, customClasses?.input)}
        id="collaborateur-input"
        padding={!label ? '8px' : '0px'}
      >
        <Box display="flex" flexDirection="row" alignItems="center" paddingLeft="8px">
          {!isAllTeamSelected ? (
            list.length === 0 ? (
              <Box className={classes.collaborateurPlaceholder}>Sélectionner le(s) collaborateur(s)</Box>
            ) : (
              <CustomAvatarGroup users={list} max={5} />
            )
          ) : (
            <Box>Toute l'équipe</Box>
          )}
        </Box>
        {icon}
      </Box>
    </fieldset>
  );
};

export default AvatarInput;
