import { createStyles, Theme, withStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    collaborateurInput: {
      marginBottom: 20,
      borderRadius: 5,
      height: 56,
      border: '1px solid #c4c4c4',
      '& legend': {
        marginLeft: 12,
        textAlign: 'left',
      },
      // [theme.breakpoints.down('sm')]: {
      //   marginBottom: 0,
      // }
    },
    collaborateurBox: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    collaborateurPlaceholder: {
      marginLeft: 0,
      color: '#aaa',
      fontSize: '12px !important',
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      },
    },
  })
);

export default useStyles;
