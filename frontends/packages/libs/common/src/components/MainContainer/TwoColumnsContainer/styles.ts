import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: 'inline',
    },
  })
);
