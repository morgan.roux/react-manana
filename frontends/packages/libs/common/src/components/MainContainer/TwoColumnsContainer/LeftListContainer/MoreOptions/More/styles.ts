import { makeStyles, Theme, createStyles, lighten } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 14,
      height: 14,
      borderRadius: '50%',
      background: theme.palette.secondary.main,
    },
  })
);
