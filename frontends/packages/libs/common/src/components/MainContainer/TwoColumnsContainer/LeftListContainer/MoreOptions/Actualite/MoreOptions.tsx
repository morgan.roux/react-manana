import React, { FC, useState, MouseEvent, Fragment } from 'react';
import { ConfirmDeleteDialog, Backdrop } from '@lib/ui-kit';
import { MoreOptionsItem } from '../../../../../MoreOptions/MoreOptions';
import {
  useMark_Actualite_Not_SeenMutation,
  useDelete_ActualiteMutation,
  SearchQuery,
  SearchQueryVariables,
  SearchDocument,
} from './../../../../../../graphql';
import { stopEvent } from '../../../../../../shared';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import MoreOptionsComponent from '../../../../../MoreOptions';
import { useApplicationContext } from 'src';

interface MoreOptionsProps {
  active?: boolean;
  url?: string;
  currentItem?: any;
  setCurrentItem?: (id: string | null) => void;
  listResult?: any;
}

const MoreOptions: FC<MoreOptionsProps & RouteComponentProps> = ({
  active,
  currentItem,
  url,
  setCurrentItem,
  listResult,
  history,
}) => {
  const { graphql, user: currentUser, currentPharmacie, notify } = useApplicationContext();
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const actualite = currentItem;
  const idPharmacieUser = currentPharmacie.id;

  const [doDeleteActualite, doDeleteActualiteResult] = useDelete_ActualiteMutation({
    client: graphql,
    variables: { id: actualite.id, idPharmacieUser, userId: currentUser && currentUser.id },
    update: (cache, { data }) => {
      if (data && data.deleteActualite) {
        const actu = data.deleteActualite;
        const req = cache.readQuery<SearchQuery, SearchQueryVariables>({
          query: SearchDocument,
          variables: listResult && listResult.variables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SearchDocument,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.filter((item: any) => {
                    if (item && item.id !== actu.id) {
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  const editActualite = (event: MouseEvent<any>) => {
    stopEvent(event);
    history.push(`/edit${url}/${currentItem.id}`);
  };

  const deleteActualite = (event: MouseEvent<any>) => {
    stopEvent(event);
    setOpenConfirmDialog(false);
    doDeleteActualite();
  };

  const markActualiteNotSeen = (event: MouseEvent<any>) => {
    stopEvent(event);
    event.preventDefault();
    event.stopPropagation();
    if (actualite.seen === true) {
      doMarkActualiteNotSeen({
        variables: { id: actualite.id, idPharmacieUser, userId: currentUser && currentUser.id },
      });
    }
  };

  const openDialog = (event: MouseEvent<any>) => {
    stopEvent(event);
    setOpenConfirmDialog(true);
  };

  let moreOptionsItems: MoreOptionsItem[] = [];
  /* userIsAuthorized(currentUser) && actualite && actualite.seen
      ? [
          {
            title: 'Marquer comme non lue',
            onClick: markActualiteNotSeen,
            disabled: !auth.isAuthorizedToMarkSeenOrNotSeenActu(),
          },
        ]
      : [];

  if (userIsAuthorized(currentUser) ) {
    if (actualite && actualite.operation) {
      moreOptionsItems = [
        { title: 'Modifier', onClick: editActualite, disabled: !auth.isAuthorizedToEditActu() },
        ...moreOptionsItems,
      ];
    } else {
      moreOptionsItems = [
        { title: 'Modifier', onClick: editActualite, disabled: !auth.isAuthorizedToEditActu() },
        { title: 'Supprimer', onClick: openDialog, disabled: !auth.isAuthorizedToDeleteActu() },
        ...moreOptionsItems,
      ];
    }
  }*/

  const [doMarkActualiteNotSeen, doMarkActualiteNotSeenResult] = useMark_Actualite_Not_SeenMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.markAsNotSeenActualite) {
        const req = cache.readQuery<SearchQuery, SearchQueryVariables>({
          query: SearchDocument,
          variables: listResult && listResult.variables,
        });
        if (req && req.search && req.search.data) {
          if (active && setCurrentItem) {
            window.history.pushState(null, '', `#${url}`);
            setCurrentItem(null);
          }
          cache.writeQuery({
            query: SearchDocument,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (item && data.markAsNotSeenActualite && item.id === data.markAsNotSeenActualite.id) {
                      item.seen = data.markAsNotSeenActualite.seen;
                      return item;
                    }
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  if (doMarkActualiteNotSeenResult.loading || doDeleteActualiteResult.loading) {
    return <Backdrop />;
  }

  return (
    <Fragment>
      {moreOptionsItems && moreOptionsItems.length > 0 && (
        <MoreOptionsComponent items={moreOptionsItems} active={active} />
      )}
      <ConfirmDeleteDialog
        title="Suppression d'actualité"
        content={`Etes-vous sûr de vouloir supprimer l'actualité « ${actualite.libelle} » ?`}
        open={openConfirmDialog}
        setOpen={setOpenConfirmDialog}
        onClickConfirm={deleteActualite}
      />
    </Fragment>
  );
};

export default withRouter(MoreOptions);
