import React, { FC } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box, Typography } from '@material-ui/core';
import { getObjectByPath, capitalizeFirstLetter } from '../../../../../shared';
import moment from 'moment';
import { Event } from '@material-ui/icons';
import classnames from 'classnames';
import SeenIcon from '../SeenIcon';
import { useApplicationContext } from '../../../../../context';

interface ListItemProps {
  moreOptionsItem?: any;
  listItemFields: any;
  item: any;
  setCurrentId: (id: any) => void;
  currentId: any;
  noContentValues: any;
  listResult: any;
}

const ListItem: FC<ListItemProps & RouteComponentProps> = ({
  listItemFields,
  moreOptionsItem,
  setCurrentId,
  currentId,
  item,
  history: { push },
  // noContentValues,
  // listResult,
}) => {
  const classes = useStyles({});
  const { user } = useApplicationContext();
  const type = listItemFields && listItemFields.type && getObjectByPath(item, listItemFields.type);
  const title = listItemFields && listItemFields.title && getObjectByPath(item, listItemFields.title);
  const lastChangeTime =
    item && item.dateModification !== item.dateCreation
      ? `Modifiée ${moment(item.dateModification).fromNow()}`
      : `Créée ${moment(item && item.dateModification).fromNow()}`;

  const isActualite: boolean = window.location.hash.includes('#actualite');
  const isOperatione: boolean = window.location.hash.includes('#operation');

  const handleClick = () => {
    // Check authorisation
    /*if ((isActualite && !auth.isAuthorizedToViewActu()) || (isOperatione && !auth.isAuthorizedToViewOC())) {
      push('/unauthorized');
      return;
    }*/

    if (item && item.id) {
      window.history.pushState(null, '', `#${listItemFields.url}/${item.id}`);
    } else {
      window.history.pushState(null, '', `#${listItemFields.url}`);
      setCurrentId(null);
    }
    //TODO : Migration
    /*client.writeData({
      data: {
        articleOcArray: [],
      },
    });*/
    setCurrentId(item);
  };

  const moreOptionClick = (e: any) => {
    e.stopPropagation();
  };

  return (
    <Box
      className={classnames(
        classes.root,
        currentId && item && currentId.id === item.id ? classes.active : classes.seen
      )}
      onClick={handleClick}
    >
      <Box className={classes.header}>
        <Typography>{capitalizeFirstLetter(type)}</Typography>
        <Box display="flex" alignItems="center">
          <div onClick={moreOptionClick}>{moreOptionsItem}</div>
          {item && !item.seen && <SeenIcon />}
        </Box>
      </Box>
      <Box className={classes.SubHeader}>
        <Typography className="title">{title}</Typography>

        <Typography>{lastChangeTime}</Typography>
      </Box>
      <Box className={classes.infoListContainer}>
        {listItemFields &&
          listItemFields.infoList &&
          listItemFields.infoList.map((itemField: any, index: any) => (
            <Box width="50%" key={`item-${index}`} display="flex">
              <Typography className="key">{itemField.key} :</Typography>
              <Typography>
                {itemField.dateFormat
                  ? moment(getObjectByPath(item, itemField.value)).format(itemField.dateFormat)
                  : `${capitalizeFirstLetter(getObjectByPath(item, itemField.value))} ${itemField.append || ''}`}
              </Typography>
            </Box>
          ))}
      </Box>
      {listItemFields && listItemFields.dateDebutFin && (
        <Box className={classes.dateTimeContainer}>
          <Event />
          <Typography className={classes.dateTimeDefaultFont}>
            du{' '}
            <span className={classes.dateTimeBoldFont}>
              {item && item.dateDebut && moment(item.dateDebut).format('L')}
            </span>{' '}
            au{' '}
            <span className={classes.dateTimeBoldFont}>{item && item.dateFin && moment(item.dateFin).format('L')}</span>
          </Typography>
        </Box>
      )}
      {listItemFields && listItemFields.iconList && (
        <Box display="flex" flexWrap="wrap" justifyContent="space-between" marginTop="8px" alignItems="center">
          {listItemFields.iconList.map((itemField: any, index: any) => (
            <Box
              key={`listItemField-${index}`}
              width="33%"
              display="flex"
              justifyContent={itemField && itemField.position ? itemField.position : 'center'}
            >
              <Box width={itemField && itemField.iconButton ? '72px' : '60px'} display="flex" alignItems="center">
                {itemField && itemField.icon}
                {/* <Typography className={classes.iconListLabel}>
                  {itemField && itemField.length
                    ? getObjectByPath(item, itemField.value).length
                    : itemField.firstValue && itemField.secondValue
                    ? `${getObjectByPath(item, itemField.firstValue)}/${getObjectByPath(
                        item,
                        itemField.secondValue,
                      )}`
                    : getObjectByPath(item, itemField.value)}
                </Typography> */}
              </Box>
            </Box>
          ))}
        </Box>
      )}
    </Box>
  );
};

export default withRouter(ListItem);
