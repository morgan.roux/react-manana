import React, { FC, Fragment, useState, ChangeEvent, useEffect } from 'react';
import MoreOptionsComponent from '../../../../../MoreOptions';
import { MoreOptionsItem } from '../../../../../MoreOptions/MoreOptions';
import { ConfirmDeleteDialog } from '@lib/ui-kit';
import { useApolloClient } from '@apollo/client';
import { withRouter, RouteComponentProps } from 'react-router-dom';

// import AddOutil from '../../../../../../Main/Content/DemarcheQualite/Outils/ThemePage/AddOutil/AddOutil';
// TODO: Migration
/*import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../../../graphql/S3';

import { formatFilename } from '../../../../../Dropzone/Dropzone';
*/
interface MoreOptionsProps {
  url?: string;
  typologie: string;
  active?: boolean;
  currentItem?: any;
  setCurrentItem?: (id: string | null) => void;
  listResult?: any;
}

const MoreOptions: FC<MoreOptionsProps & RouteComponentProps> = ({ active, currentItem, typologie }) => {
  const [open, setOpen] = useState<boolean>(false);
  const [openConfirm, setOpenConfirm] = useState<boolean>(false);
  const [mutationLoading, setMutationLoading] = useState<boolean>(false);
  const [selectedFile, setSelectedFile] = useState<File[]>([]);
  const [numeroOrdre, setNumeroOrdre] = useState<string>('');
  const [intitule, setIntitule] = useState<string>('');
  const [startFileUpload, setStartFileUpload] = useState<boolean>(false);
  const directory: string = ``;

  const handleSelectedFiles = (files: any[]) => {
    if (files) {
      setSelectedFile([files[files.length - 1]]);
    }
  };
  /*
  useEffect(() => {
    if (startFileUpload) {
      const filePathTab: string[] = [];
      selectedFile.map((file: File) => {
        filePathTab.push(formatFilename(file, `${directory}`));
      });

      doCreatePutPresignedUrl({
        variables: {
          filePaths: filePathTab,
        },
      });
    }
  }, [startFileUpload]);
*/
  const reInit = (): void => {
    setNumeroOrdre('');
    setIntitule('');
    setSelectedFile([]);
  };

  const handleNumeroOrdreChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNumeroOrdre((event.target as HTMLInputElement).value);
  };

  const handleIntituleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setIntitule((event.target as HTMLInputElement).value);
  };

  const client = useApolloClient();

  /*const [doCreatePutPresignedUrl] = useMutation<CREATE_PUT_PESIGNED_URL, CREATE_PUT_PESIGNED_URLVariables>(
    DO_CREATE_PUT_PESIGNED_URL,
    {
      onCompleted: (data) => {
        selectedFile.map((file) => {
          if (data && data.createPutPresignedUrls) {
            data.createPutPresignedUrls.map((item) => {
              if (item && item.filePath === formatFilename(file, `${directory}`)) {
                uploadToS3(file, item.presignedUrl)
                  .then((result) => {
                    if (result && result.status === 200 && result.statusText === 'OK' && setStartFileUpload) {
                      setStartFileUpload(false);
                      saveOutil({
                        chemin: item.filePath,
                        nomOriginal: file.name,
                        type: file.type,
                      });
                    }
                  })
                  .catch((error) => {
                    console.log(error);
                  });
              }
            });
          }
        });
      },
      client: FEDERATION_CLIENT,
    }
  );*/

  const saveOutil = (fichier?: any) => {
    /* updateOutil({
      variables: {
        id: currentItem.id,
        typologie: typologie,
        outilInput: {
          ordre: parseInt(`${numeroOrdre}`, 10),
          libelle: intitule,
          fichier,
        },
      },
    });*/
  };

  /*const [updateOutil] = useMutation<UPDATE_OUTIL_TYPE, UPDATE_OUTILVariables>(UPDATE_OUTIL, {
    onCompleted: () => {
      setMutationLoading(false);
      setOpen(false);
      reInit();
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: "L'outil a été modifié avec succès !",
        isOpen: true,
      });
    },
    onError: () => {
      setOpen(false);
      displaySnackBar(client, {
        type: 'ERROR',
        message: "Des erreurs se sont survenues pendant la modification de l'outil !",
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteOutil] = useMutation<DELETE_OUTIL_TYPE, DELETE_OUTILVariables>(DELETE_OUTIL, {
    onCompleted: () => {
      setOpenConfirm(false);
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: "L'outil a été supprimé avec succès !",
        isOpen: true,
      });
    },
    onError: () => {
      setOpenConfirm(false);
      displaySnackBar(client, {
        type: 'ERROR',
        message: "Des erreurs se sont survenues pendant la suppression de l'outil !",
        isOpen: true,
      });
    },
    update: (cache, deletionResult) => {
      if (deletionResult.data && deletionResult.data.deleteDQOutil && deletionResult.data.deleteDQOutil.id) {
        const previousList = cache.readQuery<GET_OUTILS_TYPE, GET_OUTILSVariables>({
          query: GET_OUTILS,
          variables: {
            typologie: typologie,
          },
        })?.dqOutils;

        const deletedId = deletionResult.data.deleteDQOutil.id;

        cache.writeQuery<GET_OUTILS_TYPE>({
          query: GET_OUTILS,
          variables: {
            typologie: typologie,
          },
          data: {
            dqOutils: (previousList || []).filter((item) => item?.id !== deletedId),
          },
        });
      }
    },
    client: FEDERATION_CLIENT,
  });*/

  const openUpdateModal = (): void => {
    setOpen(!open);
    setNumeroOrdre(currentItem.ordre);
    setIntitule(currentItem.libelle);
    setSelectedFile([{ ...currentItem.fichier, name: currentItem.fichier.nomOriginal }]);
  };

  const openDeleteConfirm = (): void => {
    setOpenConfirm(!openConfirm);
  };

  const doUpdate = (): void => {
    if (selectedFile.length > 0 && selectedFile[0].size) {
      // WHY: The uploaded file does not have size property
      setStartFileUpload(true);
    } else {
      setMutationLoading(true);

      saveOutil();
    }
  };

  const doDelete = (): void => {
    /* deleteOutil({
      variables: {
        id: currentItem && currentItem.id,
        typologie: typologie,
      },
    });*/
  };

  const moreOptionsItems: MoreOptionsItem[] = [
    { title: 'Modifier', onClick: openUpdateModal.bind(null) },
    { title: 'Supprimer', onClick: openDeleteConfirm.bind(null) },
  ];

  return (
    <Fragment>
      <MoreOptionsComponent items={moreOptionsItems} active={active} />
      <ConfirmDeleteDialog
        title={`Suppression ${typologie === 'affiche' ? "d'" : 'de'} ${typologie}`}
        content="Etes-vous sûr de vouloir supprimer ?"
        open={openConfirm}
        setOpen={setOpenConfirm}
        onClickConfirm={doDelete}
      />
      {/*<AddOutil
        title={`Modification ${typologie === 'affiche' ? "d'" : 'de '}${typologie}`}
        open={open}
        numeroOrdre={numeroOrdre}
        intitule={intitule}
        selectedFile={selectedFile}
        loading={startFileUpload || mutationLoading}
        setNumeroOrdre={handleNumeroOrdreChange}
        setIntitule={handleIntituleChange}
        setSelectedFile={handleSelectedFiles}
        setOpen={setOpen}
        onSubmit={doUpdate}
      />*/}
    </Fragment>
  );
};

export default withRouter(MoreOptions);
