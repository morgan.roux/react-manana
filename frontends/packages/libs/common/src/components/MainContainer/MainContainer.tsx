import React, { FC, useState, ReactElement, useEffect, Fragment } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box, Grid, Divider, AppBar, CssBaseline, IconButton, Toolbar, Typography } from '@material-ui/core';
import Filter from '../Filter';
import SideMenuButton from '../SideMenuButton';
import ListItem from './TwoColumnsContainer/LeftListContainer/ListItem';
import { Loader, NoItemContentImage, NoItemListImage } from '@lib/ui-kit';
import { SideMenuButtonInterface } from '../SideMenuButton/SideMenuButton';
import Drawer from '../Drawer';
import { Menu } from '@material-ui/icons';

export enum LayoutType {
  TwoColumns,
  WithFilter,
  Empty,
}

export interface MainContainerProps {
  centeredSearch?: boolean;
  handleScroll?: any;
  noContentValues?: any;
  listItemFields?: any;
  listButton?: SideMenuButtonInterface;
  listResult?: any;
  leftListItem?: any;
  mainItem?: any;
  moreOptionsItem?: any;
  layout: LayoutType;
  leftListChildren?: ReactElement;
  rightContentChildren?: ReactElement;
  match: {
    params: { id: string | undefined; view: string | undefined };
  };
  drawerTitle?: string;
  drawerBackUrl?: string;
  drawerCloseUrl?: string;
}

const MainContainer: FC<MainContainerProps & RouteComponentProps<any, any, any>> = ({
  centeredSearch,
  handleScroll,
  noContentValues,
  listItemFields,
  listButton,
  listResult,
  leftListChildren,
  rightContentChildren,
  moreOptionsItem,
  mainItem,
  layout,
  match,
  location: { pathname },
  drawerTitle,
  drawerBackUrl,
  drawerCloseUrl,
}) => {
  const classes = useStyles({});
  const [currentItem, setCurrentItem] = useState<any>();

  const isActive = (item: any) => {
    return currentItem && item && currentItem.id === item.id;
  };

  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const memorized = React.useMemo(() => {
    return (
      <Fragment>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              className={classes.menuButton}
            >
              <Menu />
            </IconButton>
            <Typography className={classes.toolbarTitle}>{drawerTitle}</Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          backUrl={drawerBackUrl}
          closeUrl={drawerCloseUrl}
          mobileOpen={mobileOpen}
          handleDrawerToggle={handleDrawerToggle}
        >
          <nav className={classes.drawer}>
            <Filter />
          </nav>
        </Drawer>
      </Fragment>
    );
  }, [mobileOpen]);

  useEffect(() => {
    if (listResult && listResult.data && listResult.data.search && listResult.data.search.data) {
      if (currentItem && currentItem.id) {
        const newValue = listResult.data.search.data.find((item: any) => item && item.id === currentItem.id);
        setCurrentItem(newValue);
      } else if (match.params.id && currentItem === undefined) {
        const newValue = listResult.data.search.data.find((item: any) => item && item.id === match.params.id);
        setCurrentItem(newValue);
      }
    }
  }, [listResult]);

  const data = (listResult && listResult.data && listResult.data.search && listResult.data.search.data) || [];

  // if (listResult && listResult.loading) {
  //   return <Loader />;
  // }

  if (layout === LayoutType.WithFilter) {
    return (
      <Box className={classes.root}>
        {pathname !== '/pilotage/business/pharmacie' &&
          pathname !== '/pilotage/feedback/pharmacie' &&
          pathname !== '/pilotage/feedback' &&
          memorized}
        {mainItem}
      </Box>
    );
  }

  return (
    <Box className={classes.root}>
      {layout !== LayoutType.Empty && memorized}
      <Box className={classes.leftListContainer}>
        {listButton && <SideMenuButton button={listButton} centeredSearch={centeredSearch} />}
        <Grid
          id="listLaboGrid"
          // tslint:disable-next-line: jsx-no-lambda
          onScroll={() => handleScroll({ id: 'listLaboGrid' })}
          item={true}
          className={classes.leftList}
        >
          {listResult && listResult.loading ? (
            <Loader />
          ) : listItemFields && data && data.length ? (
            data.map((item: any, index: any) => (
              <Fragment>
                <ListItem
                  key={`listItem-${item.id}_${index}`}
                  moreOptionsItem={
                    moreOptionsItem
                      ? React.cloneElement(
                          moreOptionsItem,
                          {
                            active: isActive(item),
                            currentItem: item,
                            setCurrentItem,
                            url: listItemFields && listItemFields.url,
                          },
                          null
                        )
                      : null
                  }
                  noContentValues={noContentValues}
                  listItemFields={listItemFields}
                  item={item}
                  setCurrentId={setCurrentItem}
                  currentId={currentItem}
                  listResult={listResult}
                />
                <Box paddingLeft="16px" paddingRight="16px">
                  <Divider />
                </Box>
              </Fragment>
            ))
          ) : (
            noContentValues && (
              <NoItemListImage
                title={(noContentValues && noContentValues.list && noContentValues.list.title) || ''}
                subtitle={(noContentValues && noContentValues.list && noContentValues.list.subtitle) || ''}
              />
            )
          )}
          {leftListChildren &&
            data &&
            data.map((item: any) => (
              <Fragment>
                {React.cloneElement(leftListChildren, {
                  item,
                  active: isActive(item),
                  setCurrentItem,
                })}
              </Fragment>
            ))}
        </Grid>
      </Box>
      <Box className={classes.rightContainer}>
        {rightContentChildren && currentItem ? (
          React.cloneElement(rightContentChildren, {
            currentItem,
            setCurrentItem,
            url: listItemFields && listItemFields.url,
          })
        ) : (
          <NoItemContentImage
            title={(noContentValues && noContentValues.content && noContentValues.content.title) || ''}
            subtitle={(noContentValues && noContentValues.content && noContentValues.content.subtitle) || ''}
          />
        )}
      </Box>
    </Box>
  );
};

export default withRouter(MainContainer);
