import { Theme, Tooltip } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import { px2rem } from './../../shared/helpers';

export const HtmlTooltip = withStyles((theme: Theme) => ({
  tooltip: {
    maxWidth: 250,
    fontSize: px2rem(12, theme),
  },
}))(Tooltip);
