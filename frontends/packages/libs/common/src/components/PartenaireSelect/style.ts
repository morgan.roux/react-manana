import { fade, Theme } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    button: {
      [theme.breakpoints.down('md')]: {
        padding: '5px 10px',
      },
      background: '#F2F2F2',
      // '& .main-MuiTypography-root': {
      //   textTransform: 'initial',
      //   fontSize: '14px',
      //   fontFamily: 'Roboto',
      //   color: theme.palette.common.black,
      // },
      padding: '11px 34px',
      textTransform: 'none',
    },
    formLabel: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
      margin: '0 !important',
    },
    tag: {
      marginTop: 3,
      height: 20,
      padding: '.15em 4px',
      fontWeight: 600,
      lineHeight: '15px',
      borderRadius: 2,
    },
    popper: {
      border: '1px solid rgba(27,31,35,.15)',
      boxShadow: '0 3px 12px rgba(27,31,35,.15)',
      borderRadius: 3,
      zIndex: 2,
      fontSize: 13,
      color: '#586069',
      padding: 20,
    },
    header: {
      borderBottom: '1px solid #e1e4e8',
      padding: '8px 10px',
      fontWeight: 600,
    },
    iconFilter: {
      background: theme.palette.primary.main,
      padding: 2,
      borderRadius: 30,
    },
    inputBase: {
      padding: 10,
      width: '100%',
      borderBottom: '1px solid #dfe2e5',
      '& input': {
        borderRadius: 4,
        backgroundColor: theme.palette.common.white,
        padding: 8,
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        border: '1px solid #ced4da',
        fontSize: 14,
        '&:focus': {
          boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
          borderColor: theme.palette.primary.main,
        },
      },
    },
    paper: {
      boxShadow: 'none',
      margin: 0,
      color: '#586069',
      fontSize: 13,
    },
    popoverPaper: {
      padding: 20,
      minWidth: 300,
    },
    option: {
      minHeight: 'auto',
      alignItems: 'center',
      padding: '5px 0',
      '&[aria-selected="true"]': {
        backgroundColor: 'transparent',
      },
      '&[data-focus="true"]': {
        backgroundColor: theme.palette.action.hover,
      },
    },
    avatarLogo: {
      width: 35,
      height: 35,
      marginRight: 10,
    },
    popperDisablePortal: {
      position: 'relative',
      width: '100% !important',
    },
    iconSelected: {
      width: 17,
      height: 17,
      marginRight: 5,
      marginLeft: -2,
    },
    color: {
      width: 14,
      height: 14,
      flexShrink: 0,
      borderRadius: 3,
      marginRight: 8,
      marginTop: 2,
    },
    text: {
      flexGrow: 1,
    },
    close: {
      opacity: 0.6,
      width: 18,
      height: 18,
    },
  })
);

export default useStyles;
