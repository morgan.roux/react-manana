import React, { FC, useEffect, useRef, useState } from 'react';
import { debounce } from 'lodash';
import { Laboratoire, PrestataireService, useSearch_Parametre_LaboratoiresLazyQuery } from '../../federation';
import { useSearch_Custom_Content_PartenaireLazyQuery } from '../../graphql';
import { Add } from '@material-ui/icons';
import useStyles from './style';
import { useApplicationContext } from '../../context';
import { CustomAutocomplete, CustomAvatar, CustomFormTextField } from '@lib/ui-kit';
import {
  Box,
  Button,
  CircularProgress,
  FormControlLabel,
  IconButton,
  Popover,
  Radio,
  Switch,
  Tooltip,
} from '@material-ui/core';
import { AutocompleteCloseReason } from '@material-ui/lab/Autocomplete';
import { PartenaireForm } from '../PartenaireForm';

import classnames from 'classnames';

const NOT_REMOVED_MUST = { term: { isRemoved: false } };

const isNumber = (value: string) => {
  try {
    return !isNaN(parseFloat(value));
  } catch (error) {
    return false;
  }
};

export interface PartenaireSelectProps {
  index: 'partenaire' | 'laboratoire';
  label?: string;
  value?: any;
  defaultValue?: any;
  onChange: (value: any) => void;
  filterIcon?: any;
  customFilterIcon?: any;
  noTitle?: boolean;
  iconDefaultStyle?: boolean;
  withSwitcher?: boolean;
  switcherLabel?: string;
  withSelectAll?: boolean;
  switchOn?: boolean;
  onSwitchChange?: (on: boolean) => void;
  onClickValidate?: () => void;
  onCreated?: (prestataire?: PrestataireService | Laboratoire) => void;
  mustFilter?: Record<string, any>[];
  includeRattaches?: boolean;
  required?: boolean;
  idSecteur?: string;
  cerclePartenariat?: 'LABORATOIRE' | 'PHARMACIE';
  partenaireClassName?: string;
  includePrivate?: boolean;
}

const PartenaireSelect: FC<PartenaireSelectProps> = ({
  index,
  value,
  defaultValue,
  label,
  onChange,
  filterIcon,
  customFilterIcon,
  noTitle,
  iconDefaultStyle,
  withSwitcher,
  switcherLabel,
  withSelectAll = true,
  onClickValidate,
  onSwitchChange,
  switchOn,
  mustFilter,
  onCreated,
  includeRattaches = false,
  includePrivate = true,
  required,
  idSecteur,
  cerclePartenariat,
  partenaireClassName,
}) => {
  const classes = useStyles();
  const { graphql, federation, notify, currentPharmacie } = useApplicationContext();

  const [openForm, setOpenForm] = useState<boolean>(false);
  const [searchText, setSearchText] = useState<string>('');
  const [initialized, setInitialized] = useState<boolean>(false);

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const getRattaches = () => {
    return index === 'laboratoire' && !includeRattaches
      ? { exists: { field: 'laboratoireRattachement.id' } }
      : undefined;
  };

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (event: React.ChangeEvent<{}>, reason: AutocompleteCloseReason) => {
    if (reason === 'toggleInput') {
      return;
    }
    if (anchorEl) {
      anchorEl.focus();
    }
    setAnchorEl(null);
  };

  const handleValidate = () => {
    if (anchorEl) {
      anchorEl.focus();
    }
    setAnchorEl(null);
    onClickValidate && onClickValidate();
  };

  const handleOpenPartenaireForm = () => {
    setOpenForm(true);
  };

  const [searchSelectedLaboratoire, searchSelectedLaboratoiresResult] = useSearch_Parametre_LaboratoiresLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: 'Une erreur est survenue lors du chargement des laboratoires',
        });
      });
    },
  });

  // Get laboratoires list
  const [searchLaboratoires, searchLaboratoiresResult] = useSearch_Parametre_LaboratoiresLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: 'Une erreur est survenue lors du chargement des laboratoires',
        });
      });
    },
  });

  const [searchSelectedPartenaire, searchSelectedPartenaireResult] = useSearch_Custom_Content_PartenaireLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: graphql,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: 'Une erreur est survenue lors du chargement des prestataires de service',
        });
      });
    },
  });

  const [searchPartenaires, searchPartenairesResult] = useSearch_Custom_Content_PartenaireLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: graphql,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: 'Une erreur est survenue lors du chargement des prestataires de service',
        });
      });
    },

    onCompleted: (data) => {
      if (!initialized && defaultValue && data?.search?.data) {
        setInitialized(true);

        const defaultValuePartenaire: any = data.search.data.find(({ id }: any) => defaultValue == id);

        if (defaultValuePartenaire) {
          onChange(defaultValuePartenaire);
        }
      }
    },
  });

  const debouncedSearch = useRef(
    debounce((searchText?: string) => {
      const must: any[] = mustFilter ? [...mustFilter] : [];
      const must_not = getRattaches();
      if (searchText) {
        let formattedSearchText = searchText.replace(/[&\/\\#,+()$~%.'":\-?<>{}]/g, ' ');
        if (isNumber(formattedSearchText)) {
          formattedSearchText = `"${formattedSearchText}"`;
        }
        must.push({
          query_string: {
            query:
              formattedSearchText.startsWith('*') || formattedSearchText.endsWith('*')
                ? formattedSearchText
                : `*${formattedSearchText}*`,
          },
        });
      }

      /*if (includePrivate && ['laboratoire', 'partenaire'].includes(index)) {
        const should = [
          {
            bool: {
              must_not: {
                exists: {
                  field: 'idPharmacie',
                },
              },
            },
          },

          {
            bool: {
              must: [
                {
                  exists: {
                    field: 'idPharmacie',
                  },
                },
                {
                  term: {
                    idPharmacie: currentPharmacie.id,
                  },
                },
              ],
            },
          },
        ];

        must.push({
          bool: {
            should,
          },
        });
      }*/

      const query = mustFilter
        ? {
          query: {
            bool: {
              must: index === 'partenaire' ? [...must, NOT_REMOVED_MUST] : must,
              must_not,
            },
          },
        }
        : must.length === 0
          ? undefined
          : must.length === 1
            ? {
              query: {
                bool: {
                  must: index === 'partenaire' ? [...must, NOT_REMOVED_MUST] : must[0],
                  must_not,
                },
              },
            }
            : {
              query: {
                bool: {
                  must: index === 'partenaire' ? NOT_REMOVED_MUST : undefined,
                  must_not,
                  should: must,
                },
              },
            };

      switch (index) {
        case 'laboratoire':
          searchLaboratoires({
            variables: {
              index: ['laboratoires'],
              query,
              sortBy: [{ "nom": { order: 'asc' } }],
              skip: 0,
              take: 20,
              //  idPharmacie: currentPharmacie.id,
            },
          });
          break;
        case 'partenaire': {
          searchPartenaires({
            variables: {
              type: ['partenaire'],
              query,
              skip: 0,
              take: 20,
            },
          });
        }
      }
    }, 1000)
  );

  useEffect(() => {
    debouncedSearch.current();
  }, []);

  // Debounced Search Laboratoire
  useEffect(() => {
    debouncedSearch.current(searchText);
  }, [searchText]);

  useEffect(() => {
    setSearchText('');
    // TODO : Search selected prestataire

    if (value) {
      const id = typeof value === 'string' ? value : value.id;

      if (index === 'laboratoire') {
        searchSelectedLaboratoire({
          variables: {
            index: ['laboratoires'],
            //   idPharmacie: currentPharmacie.id,
            query: {
              query: {
                bool: {
                  must: [
                    {
                      term: {
                        _id: id,
                      },
                    },
                  ],
                },
              },
            },
          },
        });
      } else {
        searchSelectedPartenaire({
          variables: {
            type: ['partenaire'],
            query: {
              query: {
                bool: {
                  must: [
                    {
                      term: {
                        _id: id,
                      },
                    },
                  ],
                },
              },
            },
          },
        });
      }
    }
  }, [value]);

  const options =
    ((index === 'laboratoire' ? searchLaboratoiresResult.data?.search?.data : searchPartenairesResult.data?.search?.data) || []).filter(({ prive, idPharmacie }: any) => (!prive || idPharmacie === currentPharmacie.id));

  const loading = index === 'laboratoire' ? searchLaboratoiresResult.loading : searchPartenairesResult.loading;
  // const error = index === 'laboratoire' ? searchLaboratoiresResult.error : searchPartenairesResult.error;

  const allOptions = withSelectAll
    ? [
      {
        id: 'TOUT',
        nom: index === 'laboratoire' ? `Tous les labos` : `Tous les prestataires`,
      },
      ...(options || []),
    ]
    : options || [];

  const selectedValue: any =
    index === 'laboratoire' && value && searchSelectedLaboratoiresResult.data?.search?.data?.length
      ? searchSelectedLaboratoiresResult.data.search.data[0]
      : index === 'partenaire' && value && searchSelectedPartenaireResult.data?.search?.data?.length
        ? searchSelectedPartenaireResult.data.search.data[0]
        : undefined;

  const title = selectedValue
    ? selectedValue.nom
    : index === 'laboratoire'
      ? `Tous les labos`
      : `Tous les prestataires`;

  let open = Boolean(anchorEl);
  const id = open ? 'tous-partenaire' : undefined;

  const handleClosePopover = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    setAnchorEl(null);
  };
  const handleShowPartenariat = (event: React.ChangeEvent<HTMLInputElement>) => {
    onSwitchChange && onSwitchChange(event.target.checked);
  };

  const logo = selectedValue?.photo?.publicUrl;

  const autoComplete = (
    <>
      <CustomAutocomplete
        open
        onClose={handleClose}
        classes={{
          paper: classes.paper,
          option: classes.option,
          popperDisablePortal: classes.popperDisablePortal,
        }}
        disableCloseOnSelect
        disablePortal
        renderTags={() => null}
        loading={loading}
        noOptionsText="Aucun résultat trouvé"
        options={allOptions}
        optionLabelKey="nom"
        value={{ id: '', nom: searchText }}
        onAutocompleteChange={(newValue) => {
          if (newValue?.id) {
            onChange(newValue.id === 'TOUT' ? undefined : newValue);
          } else {
            setSearchText(newValue?.nom);
          }
        }}
        search={setSearchText}
        label={label}
        disabled={false}
        required={required}
        renderOption={(option) => {
          const checked = !!(
            (selectedValue && selectedValue.id === option.id) ||
            (!selectedValue && option.id === 'TOUT')
          );
          return (
            <React.Fragment>
              <CustomAvatar name={option.nom} className={classes.avatarLogo} url={option.photo?.publicUrl} />
              <FormControlLabel
                value={checked}
                label={option.nom}
                control={<Radio checked={checked} style={{ padding: 0 }} />}
                labelPlacement="start"
                className={classes.formLabel}
              />
            </React.Fragment>
          );
        }}
        renderInput={
          onCreated
            ? (params) => (
              <Box style={{ display: 'flex' }}>
                <CustomFormTextField
                  {...params}
                  placeholder={index === 'laboratoire' ? 'Choisir le laboratoire' : 'Choisir le prestataire'}
                  label={label}
                  required={true}
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {loading ? <CircularProgress color="inherit" size={20} /> : null}
                        {params.InputProps.endAdornment}
                      </React.Fragment>
                    ),
                  }}
                />
                <Tooltip
                  title={
                    index === 'laboratoire'
                      ? 'Ajouter un nouveau laboratoire'
                      : 'Ajouter un nouveau prestataire de service'
                  }
                >
                  <IconButton
                    style={{ height: '100%' }}
                    onClick={(event) => {
                      event.stopPropagation();
                      event.preventDefault();
                      handleOpenPartenaireForm();
                    }}
                  >
                    <Add />
                  </IconButton>
                </Tooltip>
              </Box>
            )
            : undefined
        }
      />
      {onClickValidate && (
        <Box width="100%" textAlign="right" mt={2}>
          {allOptions.length > 0 && (
            <Button color="primary" variant="contained" onClick={handleValidate}>
              Valider
            </Button>
          )}
        </Box>
      )}
    </>
  );

  return (
    <React.Fragment>
      <div className={partenaireClassName ? classnames(classes.root, partenaireClassName) : classes.root}>
        <Button
          onClick={handleClick}
          className={logo ? '' : classes.button}
          startIcon={logo ? <CustomAvatar url={logo} name={selectedValue?.nom} /> : customFilterIcon || filterIcon}
          classes={{ startIcon: iconDefaultStyle || logo ? '' : classes.iconFilter }}
        >
          {noTitle ? null : title}
        </Button>
      </div>
      {/* <ClickAwayListener onClickAway={ClickAwayListenerPopper}> */}
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClosePopover}
        className={classes.popper}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        classes={{
          paper: classes.popoverPaper,
        }}
      >
        {withSwitcher ? (
          <>
            <FormControlLabel
              className={classes.formLabel}
              control={<Switch checked={switchOn} onChange={handleShowPartenariat} name="show" />}
              label={switcherLabel || '-'}
              labelPlacement="start"
            />
            {switchOn && autoComplete}
          </>
        ) : (
          autoComplete
        )}
      </Popover>
      {/* </ClickAwayListener> */}
      <PartenaireForm
        open={openForm}
        setOpen={setOpenForm}
        partenaireType={index === 'partenaire' ? 'PRESTATAIRE_SERVICE' : 'LABORATOIRE'}
        mode="creation"
        idSecteur={idSecteur}
        onCreated={onCreated}
        cerclePartenariat={cerclePartenariat}
      />
    </React.Fragment>
  );
};

export default PartenaireSelect;
