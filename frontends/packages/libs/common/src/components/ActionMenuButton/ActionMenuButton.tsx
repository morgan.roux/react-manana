import { CssBaseline, Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { MoreHoriz, Visibility, Edit, Delete, Add } from '@material-ui/icons';
import React, { FC, useState } from 'react';
import WbIncandescentIcon from '@material-ui/icons/WbIncandescent';
import { ConfirmDeleteDialog } from '@lib/ui-kit';
interface ActionMenuButtonProps {
  data?: any;
  labelAddMenuAction?: string;
  labelVoirDetailMenuAction?: string;
  handleAddMenuAction?: () => void;
  handleVoirDetailMenuAction?: (data?: string) => void;
  handleEditMenuAction?: (data?: any, id?: string) => void;
  handleDeleteMenuAction?: (data?: any, id?: string) => void;
  handleApporterSolutionMenuAction?: (data?: any, id?: string) => void;
  paddingBlock?: number;
  hideAjoutSolution?: boolean;
  page?: string;
  withVoirDetail?: boolean;
}

export const ActionMenuButton: FC<ActionMenuButtonProps> = ({
  page,
  data,
  handleVoirDetailMenuAction,
  handleAddMenuAction,
  labelAddMenuAction,
  handleEditMenuAction,
  handleDeleteMenuAction,
  handleApporterSolutionMenuAction,
  paddingBlock,
  withVoirDetail = true,
}) => {
  const [openDelete, setOpenDelete] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const open = Boolean(anchorEl);
  const handleShowMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (): void => {
    setAnchorEl(null);
  };
  const handleClickDelete = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    setOpenDelete(!openDelete);
  };
  const handleConfirmDelete = (event: React.MouseEvent<any, MouseEvent>) => {
    event.preventDefault();
    handleClose();
    setOpenDelete(!openDelete);
    handleDeleteMenuAction && handleDeleteMenuAction(data, data?.id);
  };
  if (data) {
    return (
      <>
        <CssBaseline />
        <IconButton
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleShowMenuClick}
          color="inherit"
          id={`fiche-menu-icon-${data?.id}`}
          style={{ paddingBlock: paddingBlock }}
        >
          <MoreHoriz />
        </IconButton>
        <Menu
          id={`fiche-incident-menu-${data?.id}`}
          anchorEl={anchorEl}
          keepMounted
          open={open && data?.id}
          onClose={handleClose}
          TransitionComponent={Fade}
        >
          {/*<MenuItem
            onClick={(event) => {
              event.stopPropagation();
              event.preventDefault();
              handleClose();
              handleAddMenuAction && handleAddMenuAction();
            }}
            disabled={false}
          >
            <ListItemIcon>
              <Add />
            </ListItemIcon>
            <Typography variant="inherit">{labelAddMenuAction || 'Ajouter'}</Typography>
          </MenuItem>*/}
          {withVoirDetail && (
            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                handleVoirDetailMenuAction && handleVoirDetailMenuAction(data);
              }}
              disabled={false}
            >
              <ListItemIcon>
                <Visibility />
              </ListItemIcon>
              <Typography variant="inherit">Voir détails</Typography>
            </MenuItem>
          )}
          {page !== 'action' && page !== 'reunion' && !data?.idSolution && !data?.idAction && (
            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                handleApporterSolutionMenuAction && handleApporterSolutionMenuAction(data, data?.id);
              }}
            >
              <ListItemIcon>
                <WbIncandescentIcon />
              </ListItemIcon>
              <Typography variant="inherit">
                {page === 'incident' ? 'Apporter une solution' : 'Apporter une action'}
              </Typography>
            </MenuItem>
          )}

          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              event.preventDefault();
              handleClose();
              handleEditMenuAction && handleEditMenuAction(data, data?.id);
            }}
          >
            <ListItemIcon>
              <Edit />
            </ListItemIcon>
            <Typography variant="inherit">Modifier</Typography>
          </MenuItem>

          <MenuItem onClick={handleClickDelete}>
            <ListItemIcon>
              <Delete />
            </ListItemIcon>
            <Typography variant="inherit">Supprimer</Typography>
          </MenuItem>
        </Menu>
        <ConfirmDeleteDialog open={openDelete} setOpen={setOpenDelete} onClickConfirm={handleConfirmDelete} />
      </>
    );
  } else return <></>;
};
