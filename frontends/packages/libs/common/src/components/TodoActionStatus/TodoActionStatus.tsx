import React, { FC } from 'react';
import { SearchTodoActionInfoFragment } from '../../federation';
import { OptionSelect } from '../../../../ui-kit/src';
import { ActionStatus, useUpdate_Action_StatusMutation } from '../../graphql';
import { useApplicationContext } from '@lib/common';
export interface TodoActionStatusProps {
  action?: SearchTodoActionInfoFragment;
  refetch?: () => void;
}

const statuses = [
  { id: 'ACTIVE', code: '1', libelle: 'A FAIRE', background: '#e1e1e2', color: '#000' },
  { id: 'DONE', code: '3', libelle: 'TERMINÉE', background: '#dbf3e5', color: '#3aba6d' },
];

export const TodoActionStatus: FC<TodoActionStatusProps> = ({ refetch, action }) => {
  const { graphql, notify } = useApplicationContext();

  const [updateActionStatus] = useUpdate_Action_StatusMutation({
    client: graphql,
    onCompleted: () => {
      refetch && refetch();
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  const handleChange = (status?: string) => {
    if (action?.id) {
      updateActionStatus({
        variables: {
          input: {
            idAction: action.id,
            status: status === 'DONE' ? ActionStatus.Done : ActionStatus.Active,
          },
        },
      });
    }
  };

  return (
    <OptionSelect onChange={handleChange} value={{ id: action?.status }} options={statuses} useDefaultStyle={false} />
  );
};
