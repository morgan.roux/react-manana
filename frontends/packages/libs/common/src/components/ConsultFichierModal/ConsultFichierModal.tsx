import { Fichier } from '@lib/types';
import { Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { CustomModal, NewCustomButton } from '@lib/ui-kit';
import { ConsultFichierModalProps } from './interface';
import { useStyles } from './styles';
import { FileViewer } from './../FileViewer/FileViewer';

const ConsultFichierModal: FC<ConsultFichierModalProps> = ({ open, setOpen, title, files }) => {
  const classes = useStyles();
  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={title}
      withBtnsActions={false}
      withCancelButton={false}
      closeIcon
      headerWithBgColor
      fullScreen={files && files.length > 0}
    >
      <Box display="flex" flexDirection="column" width="100%" className={classes.root}>
        {files && files.length === 0 && <Box>Il n'y a pas de fichier joint</Box>}
        {(files as any).map((file: Fichier) => (
          <>
            <Box>
              <Typography>{file.nomOriginal}</Typography>
            </Box>
            <FileViewer fileUrl={file.publicUrl as any} />
          </>
        ))}
        <Box py={2} textAlign="right">
          <NewCustomButton variant="contained" onClick={() => setOpen(false)}>
            Fermer
          </NewCustomButton>
        </Box>
      </Box>
    </CustomModal>
  );
};

export default ConsultFichierModal;
