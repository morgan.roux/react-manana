import { Fichier } from '@lib/types';

export interface ConsultFichierModalProps {
  open: boolean;
  setOpen: (open: boolean) => void;
  title: string;
  files: Fichier[] | [];
}
