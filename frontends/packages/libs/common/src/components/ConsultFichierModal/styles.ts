import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((_theme: Theme) =>
  createStyles({
    root: {
      ' & iframe': {
        height: '100vh',
      },
    },
  })
);
