import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '2.5%',
    },
    formControl: {
      width: 240,
      marginRight: theme.spacing(6),
      marginBottom: theme.spacing(3),
    },
    searchButton: {
      width: 240,
      height: 51,
    },
  })
);
