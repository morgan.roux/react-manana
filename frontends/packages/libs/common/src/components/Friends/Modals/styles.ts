import { createStyles, makeStyles, Theme, lighten, darken } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      padding: theme.spacing(3, 0),
      '& .MuiSvgIcon-root': {
        width: 20,
        height: 20,
      },
    },
    table: {
      minWidth: 750,
    },
    circularProgress: {
      width: '100%!important',
      display: 'flex',
      justifyContent: 'center',
      minHeight: '200px',
      alignItems: 'center',
    },
    tableWrapper: {
      overflow: 'auto',
      maxHeight: 650,
      '& .MuiTableCell-stickyHeader': {
        backgroundColor: lighten(theme.palette.primary.main, 0.1),
      },
      '& *': {
        fontWeight: 'bold',
      },
      '& thead *': {
        fontSize: 12,
      },
    },
    avatar: {
      maxWidth: 30,
      maxHeight: 30,
      borderRadius: '50%',
    },
    tableCellPadding: {
      '&.MuiTableCell-root': {
        padding: '6px 12px 6px 12px',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
      },
    },
    tablePagination: {
      display: 'flex',
      justifyContent: 'center',
    },
    activeRow: {
      backgroundColor: theme.palette.secondary.main,
      '& td ': {
        color: 'red',
      },
      '&:hover': {
        backgroundColor: `${darken(theme.palette.secondary.main, 0.1)} !important`,
        '& td ': {
          color: 'red',
        },
      },
    },
    alignCenter: {
      textAlign: 'center',
      margin: '40px 0px',
      fontSize: 20,
      fontWeight: 'bold',
    },
    largeurDroite: {
      width: 750,
    },
    espace: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
    },
    largeur: {
      width: 290,
      height: 'calc(100vh - 214px)',
      paddingTop: 10,
      borderRight: '1px solid #E0E0E0',
      overflowY: 'auto',
      overflowX: 'hidden',
      '& .MuiListItem-root': {
        paddingLeft: 0,
        paddingBottom: 2,
        paddingTop: 2,
        '& .MuiCheckbox-root': {
          padding: 0,
          marginRight: 10,
        },
      },
    },
    mainTableUser: {
      height: 'calc(100vh - 214px)',
      overflowY: 'auto',
      overflowX: 'hidden',
      padding: theme.spacing(2, 0, 10.75, 3),
    },
    actionButtonContent: {
      boxShadow: '0 -3px 6px rgb(0, 0, 0, 0.16)',
      position: 'absolute',
      bottom: 0,
      width: '100%',
      left: 0,
      background: theme.palette.common.white,
      padding: theme.spacing(3),
    },
  })
);
