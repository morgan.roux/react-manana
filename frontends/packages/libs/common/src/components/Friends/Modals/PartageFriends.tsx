import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { Backdrop, CustomModal } from '@lib/ui-kit';
import { Box, Button, Checkbox, FormControlLabel } from '@material-ui/core';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { PartageType, useCreate_Update_PartageMutation } from '../../../graphql';
import { useStyles } from './styles';
import TablePartageGauche from './ColumsFriends';
import TableAndColumns from './TableAndColumnsFriends';

interface propsFriend {
  openModal: boolean;
  persIds?: string[];
  setpersIds?: (value: any) => void;
  handleClose: (value: boolean) => void;
  idActualite: string;
  codeItem: string;
  actualite?: any;
  refetch?: any;
}
const Partage: FC<propsFriend> = ({
  codeItem,
  openModal,
  persIds,
  setpersIds,
  idActualite,
  handleClose,
  actualite,
  refetch,
}) => {
  const [value, setValue] = React.useState('GroupesAmis');
  const classes = useStyles({});
  const { graphql, user } = useApplicationContext();
  const displayNotification = useDisplayNotification();
  const [userIdsSelected, setUserIdsSelected] = useState<any[]>(persIds && persIds.length ? persIds : []);
  const [checkedrec, setCheckedrec] = React.useState<boolean>(false);

  const handleChangerec = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCheckedrec(!checkedrec);
  };
  const [clausemodal, setclausemodal] = useState<boolean>(false);

  useEffect(() => {
    setUserIdsSelected(['']);
    setUserIdsSelected(persIds || []);
  }, [persIds]);
  console.log('userIdsSelected', userIdsSelected, 'codeItem', codeItem);

  const [createAndUpdatePartage, { loading: loadingPartage }] = useCreate_Update_PartageMutation({
    client: graphql,
    onCompleted: (data) => {
      refetch();
      if (data && data.createUpdatePartage) {
        displayNotification({
          type: 'success',
          message: 'Partage réussi',
        });
      }
      setclausemodal(false);
      setCheckedrec(false);
      setUserIdsSelected(['']);
    },
    onError: (error) => {
      console.log('error :>> ', error);
      displayNotification({ type: 'error', message: error.message });
    },
  });

  const handleConfirm = () => {
    if (value === 'GroupesAmis') {
      createAndUpdatePartage({
        variables: {
          input: {
            codeItem: codeItem,
            idItemAssocie: idActualite,
            idUserPartageant: user.id,
            type: checkedrec ? PartageType.Recommandation : PartageType.Partage,
            dateHeurePartage: moment().toDate(),
            idsGroupesAmis: userIdsSelected,
          },
        },
      });
    } else {
      createAndUpdatePartage({
        variables: {
          input: {
            codeItem: codeItem,
            idItemAssocie: idActualite,
            idUserPartageant: user.id,
            type: checkedrec ? PartageType.Recommandation : PartageType.Partage,
            dateHeurePartage: moment().toDate(),
            idsPharmacies: userIdsSelected,
          },
        },
      });
    }
  };

  useEffect(() => {
    setclausemodal(openModal);
  }, [openModal]);

  useEffect(() => {
    setUserIdsSelected([]);
  }, [value]);

  return (
    <>
      {loadingPartage && <Backdrop />}

      <CustomModal
        open={clausemodal}
        setOpen={handleClose}
        title={'Partage'}
        withBtnsActions={false}
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="lg"
        fullWidth={false}
        centerBtns={true}
      >
        <Box display="flex" flexDirection="column">
          <Box className={classes.espace}>
            <Box className={classes.largeur}>
              <TablePartageGauche value={value} setValue={setValue} />
            </Box>
            <Box className={classes.mainTableUser}>
              <TableAndColumns
                selectionvalue={value}
                userIdsSelected={userIdsSelected}
                setUserIdsSelected={setUserIdsSelected}
              />
            </Box>
          </Box>
          <Box display="flex" justifyContent="center" className={classes.actionButtonContent}>
            <FormControlLabel
              control={
                <Checkbox checked={checkedrec} onChange={handleChangerec} name="recommandation" color="primary" />
              }
              label="Recommandation"
            />
            <Button
              variant="contained"
              color="secondary"
              onClick={handleConfirm}
              disabled={userIdsSelected.length === 0 ? true : false}
            >
              Partager
            </Button>
          </Box>
        </Box>
      </CustomModal>
    </>
  );
};

export default Partage;
