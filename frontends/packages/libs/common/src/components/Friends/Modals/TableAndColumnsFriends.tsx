import { SearchInput } from '@app/basis/src/components/Content/SearchInput';
import { useApplicationContext } from '@lib/common';
import { Box } from '@material-ui/core';
import { debounce } from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import { useSearch_All_PharmaciesLazyQuery, useSearch_Groupes_AmisLazyQuery } from '../../../graphql';
import { columns, getColumnsPharmacie } from './ColumsFriends';
import EnhancedTableFriends from './TableFriends';

interface FriendsProp {
  selectionvalue: any;
  query?: any;
  setUserIdsSelected?: (value: any) => void;
  userIdsSelected?: string[];
  setUsersSelected?: (value: any) => void;
  usersSelected?: any[];
}

export interface SkipAndTake {
  skip: number;
  take: number;
}

export interface SortBy {
  column: string;
  order: 'asc' | 'desc' | undefined;
}

const TableAndColumns: React.FC<FriendsProp> = ({
  query,
  setUserIdsSelected,
  userIdsSelected,
  setUsersSelected,
  usersSelected,
  selectionvalue,
}) => {
  const [debouncedText, setDebouncedText] = useState<string>('');
  const [textSearch, setTextSearch] = useState<string>('');
  const [skipAndTake, setSkipAndTake] = useState<SkipAndTake>({
    skip: 0,
    take: 10,
  });

  const { currentGroupement, graphql } = useApplicationContext();

  const [sortBy, setSortBy] = useState<SortBy>({
    column: 'nom',
    order: 'asc',
  });

  const onChangeSearchText = (value: string) => {
    setDebouncedText(value);
  };

  const handleClick = (rowId: string, row: any) => (event: React.MouseEvent<unknown>) => {
    event.preventDefault();
    event.stopPropagation();

    if (setUserIdsSelected) {
      setUserIdsSelected((prev: any) =>
        prev.includes(rowId) ? [...prev.filter((value: any) => value !== rowId)] : [...prev, rowId]
      );
    }

    if (setUsersSelected) {
      setUsersSelected((prev: any) =>
        prev.filter((user: any) => user && row && user.id === row.id).length > 0
          ? [...prev.filter((value: any) => value && row && value.id !== row.id)]
          : [...prev, row]
      );
    }
  };

  const mustdefault = [{ term: { idGroupement: currentGroupement.id } }, { term: { sortie: 0 } }];
  const mustdefaultFriends = [{ term: { isRemoved: 'false' } }];

  const debouncedSearchText = useRef(
    debounce((text: string) => {
      setTextSearch(text);
      searchPharmacie({
        variables: {
          ...query,
          query: {
            query: {
              bool: {
                must: [
                  ...mustdefault,
                  {
                    query_string: {
                      query: text.startsWith('*') || text.endsWith('*') ? text : `*${text}*`,
                      analyze_wildcard: true,
                    },
                  },
                ],
              },
            },
          },
          type: ['pharmacie'],
          skip: skipAndTake.skip,
          take: skipAndTake.take,
        },
      });
      searchGroupeAmis({
        variables: {
          query: {
            query: {
              bool: {
                must: [
                  ...mustdefaultFriends,
                  {
                    query_string: {
                      query: text.startsWith('*') || text.endsWith('*') ? text : `*${text}*`,
                      analyze_wildcard: true,
                    },
                  },
                ],
              },
            },
          },
          type: ['groupeamis'],
          skip: skipAndTake.skip,
          take: skipAndTake.take,
        },
      });
    }, 1500)
  );

  useEffect(() => {
    setDebouncedText(textSearch ? textSearch : '');
    if (textSearch === '') {
      searchPharmacie({
        variables: {
          query: {
            query: {
              bool: {
                must: mustdefault,
              },
            },
            sort: sortBy ? [{ [sortBy.column]: { order: sortBy.order } }] : [],
          },
          type: ['pharmacie'],
          skip: skipAndTake.skip,
          take: skipAndTake.take,
        },
      });
    }
  }, [textSearch]);

  useEffect(() => {
    debouncedSearchText.current(debouncedText);
  }, [debouncedText]);

  const updateSkipAndTake = (skip: number, take: number) => {
    setSkipAndTake({
      skip,
      take,
    });
  };

  const [searchGroupeAmis, results] = useSearch_Groupes_AmisLazyQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
  });

  const [searchPharmacie, resultPharmacie] = useSearch_All_PharmaciesLazyQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
    variables: {
      query: {
        query: {
          bool: {
            must: mustdefault,
          },
        },
        sort: sortBy ? [{ [sortBy.column]: { order: sortBy.order } }] : [],
      },
      type: ['pharmacie'],
      skip: skipAndTake.skip,
      take: skipAndTake.take,
    },
  });

  useEffect(() => {
    searchGroupeAmis({
      variables: {
        query: {
          query: { bool: { must: mustdefaultFriends } },
          sort: sortBy ? [{ [sortBy.column]: { order: sortBy.order } }] : [],
        },
        type: ['groupeamis'],
        skip: skipAndTake.skip,
        take: skipAndTake.take,
      },
    });

    searchPharmacie({
      variables: {
        query: {
          query: {
            bool: {
              must: mustdefault,
            },
          },
          sort: sortBy ? [{ [sortBy.column]: { order: sortBy.order } }] : [],
        },
        type: ['pharmacie'],
        skip: skipAndTake.skip,
        take: skipAndTake.take,
      },
    });
  }, [selectionvalue, query, skipAndTake, sortBy]);

  return (
    <>
      <Box>
        <SearchInput
          value={debouncedText}
          searchInFullWidth={'xl'}
          noPrefixIcon={true}
          onChange={onChangeSearchText}
          placeholder={'Entrer vos critères de recherche'}
          outlined={true}
        />
      </Box>
      <EnhancedTableFriends
        columns={selectionvalue === 'Pharmacie' ? getColumnsPharmacie : columns}
        handleClick={handleClick}
        updateSkipAndTake={updateSkipAndTake}
        data={
          selectionvalue === 'Pharmacie'
            ? resultPharmacie && resultPharmacie.data && resultPharmacie.data.search && resultPharmacie.data.search.data
              ? resultPharmacie.data.search.data
              : []
            : results && results.data && results.data.search && results.data.search.data
            ? results.data.search.data
            : []
        }
        total={
          selectionvalue === 'Pharmacie'
            ? resultPharmacie &&
              resultPharmacie.data &&
              resultPharmacie.data.search &&
              resultPharmacie.data.search.total
              ? resultPharmacie.data.search.total
              : 0
            : results && results.data && results.data.search && results.data.search.total
            ? results.data.search.total
            : 0
        }
        userIdsSelected={userIdsSelected}
        skipAndTake={skipAndTake}
        sortBy={sortBy}
        setSortBy={setSortBy}
      />
    </>
  );
};
export default TableAndColumns;
