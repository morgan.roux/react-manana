import { Theme, makeStyles, createStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatarListContainer: {
      width: '100%',
      minHeight: 330,
      border: '1px solid #DCDCDC',
      borderRadius: 12,
      padding: 8,
      marginBottom: 25,
    },
    avatarListFilterContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginBottom: 25,
      border: 'none !important',
      height: 'auto !important',
      '& > div': {
        margin: 0,
      },
      '& > div:nth-child(1)': {
        marginRight: 25,
      },
      '& > div:nth-child(2)': {
        width: 238,
      },
    },
    avatarListImgContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-start',
      marginBottom: 5,
      border: 'none !important',
      height: 'auto !important',
      position: 'relative',
    },
    avatarContainer: {
      width: '110px !important',
      height: 'auto !important',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      margin: '8px 4px 8px 0',
      '& img': {
        width: '60px !important',
        height: '60px !important',
        borderRadius: '50% !important',
        '&:hover': {
          border: `4px solid ${theme.palette.secondary.main}`,
          cursor: 'pointer',
        },
      },
      '& span': {
        textAlign: 'center',
        fontFamily: 'Montserrat',
        fontSize: 14,
        fontWeight: '600',
        marginTop: 5,
      },
    },
    textMsg: {
      fontWeight: 600,
    },
    selectedAvatar: {
      border: `4px solid ${theme.palette.secondary.main}`,
      cursor: 'pointer',
    },
  })
);

export default useStyles;
