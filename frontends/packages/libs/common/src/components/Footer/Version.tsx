import React, { FC, CSSProperties } from 'react';
import { useApplicationContext } from '@lib/common';

interface VersionProps {
  style?: CSSProperties;
}

const Version: FC<VersionProps> = ({ style }) => {
  const { config } = useApplicationContext();
  return <span style={style}>{config.CURRENT_VERSION}</span>;
};

export default Version;
