import React, { ChangeEvent, FC, useRef, useState } from 'react';
import { CustomFormTextField } from '@lib/ui-kit';
import { EfCompteComptableInfoFragment, useEfCompteComptablesLazyQuery } from '@lib/common/src/federation';
import { useEffect } from 'react';
import { CircularProgress, debounce } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import { useApplicationContext } from '../../context';

export interface CompteComptableInputProps {
  label?: string;
  value?: EfCompteComptableInfoFragment;
  onChange: (compte?: EfCompteComptableInfoFragment) => void;
  required?: boolean;
  optionKey?: 'numero' | 'libelle' | 'numero-libelle';
  type: 'CLASS_6' | 'TVA' | 'TIERS';
  idComptes?: string[];
  disabled?: boolean;
}

export const CompteComptableInput: FC<CompteComptableInputProps> = ({
  label,
  value,
  onChange,
  required = true,
  optionKey = 'numero-libelle',
  type,
  idComptes = [],
  disabled
}) => {
  const [inputValue, setInputValue] = useState<string>('');

  const [loadCompteComptable, loadingCompteComptable] = useEfCompteComptablesLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const { currentPharmacie } = useApplicationContext()

  console.log('inputValue', inputValue);

  useEffect(() => {
    handleSearch('');
  }, []);

  useEffect(() => {
    if (value) setInputValue(optionKey === 'numero-libelle' ? `${value.numero} - ${value.libelle}` : value[optionKey]);
    else setInputValue('');
  }, [value]);

  const debouncedSearch = useRef(
    debounce((searchText?: string) => {
      handleSearch(searchText || '');
    }, 1000)
  );

  useEffect(() => {
    debouncedSearch.current();
  }, []);

  /*
  useEffect(() => {
    debouncedSearch.current(inputValue);
  }, [inputValue]);*/

  const handleSearch = (searchText: string) => {
    const filterCondition = []
    if (type === 'CLASS_6') {
      filterCondition.push(
        {
          numero: {
            iLike: `6%`,
          }
        }
      )
    }
    else if (type === 'TVA') {
      filterCondition.push(
        {
          numero: {
            iLike: `445%`,
          }
        }
      )
    }
    else if (type === 'TIERS') {
      filterCondition.push(
        {
          idOrigineAssocie: { isNot: null },
        }
      )
    }


    if (idComptes.length) {
      filterCondition.push(
        {
          id: {
            notIn: idComptes,
          },
        }
      )
    }

    if(searchText){
      filterCondition.push(
        {
          numero: {
            iLike: `%${searchText}%`,
          },
        }
      )
    }


    loadCompteComptable({
      variables: {
        filter: {
          and: [
            {
              idPharmacie: {
                eq: currentPharmacie.id
              }
            },
            ...filterCondition,
            
          ],

        },
        paging: {
          offset: 0,
          limit: 1000,
        },
      },
    });
  };



  const handleChange = (e: ChangeEvent<any>, inputValue: any) => {
    if (e && e.type === 'click' && typeof inputValue !== 'string') {
      onChange(inputValue);
    } else if (e && e.type === 'change' && inputValue !== undefined && typeof inputValue === 'string') {
      handleSearch(inputValue);
    }
  };

  return (
    <Autocomplete
      autoSelect
      value={value}
      onChange={handleChange}
      inputValue={inputValue}
      onInputChange={(event, newInputValue) => {
        setInputValue(newInputValue);
      }}
      options={loadingCompteComptable.data?.eFCompteComptables?.nodes || []}
      style={{ width: '100%' }}
      disabled={disabled}
      getOptionLabel={(option: any) => {

        if (optionKey === 'numero-libelle') {
          return `${option.numero} - ${option.libelle}`
        }


        return option[optionKey]
      }}
      renderInput={(params) => (
        <CustomFormTextField
          {...params}
          placeholder=""
          label={label}
          required={required}
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <React.Fragment>
                {loadingCompteComptable.loading ? <CircularProgress color="inherit" size={20} /> : null}
                {params.InputProps.endAdornment}
              </React.Fragment>
            ),
          }}
        />
      )}
    />
  );
};
