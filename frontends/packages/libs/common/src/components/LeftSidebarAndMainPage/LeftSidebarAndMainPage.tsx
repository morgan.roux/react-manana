import { MobileTopBar } from '../MobileTopBar';
import { Box, CssBaseline } from '@material-ui/core';
import React, { ReactNode } from 'react';
import Drawer from '../Drawer';
import useStyles from './styles';
import classnames from 'classnames';

interface LeftSidebarAndMainPageProps {
  drawerTitle?: string;
  minHeight?: string;
  sidebarChildren?: ReactNode;
  mainChildren?: ReactNode;
  optionBtn?: ReactNode[];
  openDrawer?: boolean;
  drawerBackUrl?: string;
  drawerCloseUrl?: string;
  setOpenDrawer?: React.Dispatch<React.SetStateAction<boolean>>;
  handleDrawerToggle?: () => void;
  sidebarClassNameProps?: string;
}

const LeftSidebarAndMainPage: React.FC<LeftSidebarAndMainPageProps> = ({
  drawerTitle,
  minHeight = '100%',
  sidebarChildren,
  mainChildren,
  optionBtn,
  openDrawer,
  handleDrawerToggle,
  drawerBackUrl,
  drawerCloseUrl,
  sidebarClassNameProps,
}) => {
  const classes = useStyles({});

  return (
    <Box className={classes.root} minHeight={minHeight}>
      <CssBaseline />
      {/* <MobileTopBar
        title={drawerTitle}
        withBackBtn={true}
        handleDrawerToggle={handleDrawerToggle}
        optionBtn={optionBtn}
        withTopMargin={true}
        breakpoint="sm"
        backUrl={drawerBackUrl}
        closeUrl={drawerCloseUrl}
        onClickBack={handleDrawerToggle}
      /> */}

      <Drawer
        mobileOpen={openDrawer}
        handleDrawerToggle={handleDrawerToggle}
        mobileTobBarTitle="Filtre"
        backUrl={drawerBackUrl}
        closeUrl={drawerCloseUrl}
      >
        <Box borderRight="1px solid #E5E5E5">
          <Box className={sidebarClassNameProps ? classnames(classes.sidebar, sidebarClassNameProps) : classes.sidebar}>
            {sidebarChildren}
          </Box>
        </Box>
      </Drawer>

      <Box className={classes.main}>{mainChildren}</Box>
    </Box>
  );
};

export default LeftSidebarAndMainPage;
