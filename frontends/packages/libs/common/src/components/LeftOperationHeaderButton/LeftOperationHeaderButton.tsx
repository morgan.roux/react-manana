import React, { FC, ReactNode, MouseEvent } from 'react';
import { Box } from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CustomButton, NewCustomButton } from '@lib/ui-kit';

export interface LeftOperationHeaderButtonProps {
  button?: LeftOperationHeaderButtonInterface;
}
export interface LeftOperationHeaderButtonInterface {
  url?: string;
  text: string;
  color: string;
  onClick?: () => void;
  icon?: ReactNode;
  authorized?: string[];
  userIsAuthorized?: boolean;
}

const LeftOperationHeaderButton: FC<LeftOperationHeaderButtonProps & RouteComponentProps<any, any, any>> = ({
  button,
  history,
}) => {
  const goToUrl = () => {
    if (button && button.url) {
      history.push(button.url);
    }
  };

  const fonction = () => {
    if (button) {
      if (button.url) {
        goToUrl();
      } else {
        button.onClick && button.onClick();
      }
    }
  };

  return (
    <Box display="flex" justifyContent="center" marginTop="24px" paddingBottom="16px" borderBottom="1px solid #E5E5E5">
      {button && (
        <NewCustomButton onClick={fonction} startIcon={button.icon}>
          {button.text}
        </NewCustomButton>
      )}
    </Box>
  );
};

export default withRouter(LeftOperationHeaderButton);
