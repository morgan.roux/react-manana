import { Checkbox, TableCell, TableHead, TableRow, TableSortLabel } from '@material-ui/core';
import { differenceBy, intersectionBy, uniqBy } from 'lodash';
import React, { FC, useState } from 'react';
import { TableHeadProps } from '../interfaces';
import useStyles from './styles';
import { useFilters } from '../../../shared/operations/filter/filter';

const EnhancedTableHead: FC<TableHeadProps> = ({
  columns,
  isSelectable,
  checkedItems,
  checkedItemsQuery,
  listResult,
  selected,
  setSelected,
  allTotal,
}) => {
  const classes = useStyles();
  const [order, setOrder] = useState<'asc' | 'desc' | undefined>('asc');
  const [columnSorted, setColumnSorted] = useState('');

  const { setSort } = useFilters();

  const handleNewSorting = (newColumn: any) => {
    setColumnSorted(newColumn);
    setOrder(order === 'desc' ? 'asc' : 'desc');
    // TODO : Migration
    setSort({
      label: newColumn,
      name: newColumn,
      direction: order === 'desc' ? 'asc' : 'desc',
      active: true,
      __typename: 'sortItem',
    });
    /* client.writeData({
      data: {
        sort: {
          sortItem: {
            label: newColumn,
            name: newColumn,
            direction: order === 'desc' ? 'asc' : 'desc',
            active: true,
            __typename: 'sortItem',
          },
          __typename: 'sort',
        },
      },
    });*/
  };

  const data = (listResult && listResult.data && listResult.data.search && listResult.data.search.data) || [];

  const total = data.length;

  const intersection = intersectionBy(checkedItems, data, 'id');

  const numSelected = checkedItems ? (checkedItems.length > allTotal ? allTotal : checkedItems.length) : 0;
  const isIndeterminate: boolean = numSelected > 0 && numSelected < allTotal;

  const checkedAll = (): boolean => {
    return intersection && intersection.length && total === intersection.length ? true : false;
  };

  const handleChange = () => {
    if (setSelected && selected && !checkedAll()) {
      const checkeds = uniqBy([...checkedItems, ...data], 'id');
      setSelected(checkeds);
      return;
    } else if (setSelected && selected && checkedAll()) {
      const dif = differenceBy(checkedItems, data, 'id');
      setSelected(dif);
      return;
    }

    if (!checkedAll() && checkedItemsQuery && data) {
      const checkeds = uniqBy([...checkedItems, ...data], 'id');
      // TODO : Migration
      /*client.writeData({
        data: {
          [checkedItemsQuery.name]: checkeds.map(
            item => item && { id: item.id, __typename: item.__typename },
          ),
        },
      });*/
    } else if (checkedAll() && checkedItemsQuery) {
      const dif = differenceBy(checkedItems, data, 'id');
      //TODO : Migration
      /*client.writeData({
        data: {
          [checkedItemsQuery.name]: dif.map(
            (item: any) => item && item.id && { id: item.id, __typename: item.__typename },
          ),
        },
      });*/
    }
  };

  return (
    <TableHead className={classes.root}>
      <TableRow>
        {isSelectable ? (
          <TableCell style={{ padding: '0px 4px' }}>
            <Checkbox
              indeterminate={isIndeterminate}
              checked={checkedAll()}
              onChange={handleChange}
              inputProps={{ 'aria-label': 'Select all desserts' }}
              style={{ color: '#ffffff' }}
            />
          </TableCell>
        ) : null}

        {columns
          ? (columns || []).map((column, index) => (
              <TableCell
                key={`column-${index}`}
                align={column.centered ? 'center' : 'left'}
                sortDirection={columnSorted === column.name ? order : false}
                className={classes.label}
                style={column.style}
              >
                <TableSortLabel
                  active={columnSorted === column.name ? (column.name === '' ? false : true) : false}
                  hideSortIcon={column.name === '' ? true : false}
                  direction={columnSorted && columnSorted === column.name ? order : undefined}
                  onClick={() => (column.name ? handleNewSorting(column.name) : null)}
                >
                  {column.label}
                </TableSortLabel>
              </TableCell>
            ))
          : null}
      </TableRow>
    </TableHead>
  );
};

export default EnhancedTableHead;
