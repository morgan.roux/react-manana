import { useApolloClient, useQuery } from '@apollo/client';
import GroupementSelectionModal from '@app/basis/src/components/GroupementSelectionModal';
import HistoriquePersonnel from '@app/basis/src/components/HistoriquePersonnel';
import { ResetUserPasswordModal } from '@app/basis/src/components/ResetUserPasswordModal';
import UsersModal from '@app/basis/src/components/UsersModal';
import { useApplicationContext } from '@lib/common';
import { useSearch_TotalQuery } from '@lib/common/src/graphql';
import { useFilters } from '@lib/common/src/shared';
import { ConfirmDeleteDialog, CustomFormTextField, LoaderSmall, SmallLoading } from '@lib/ui-kit';
import { Checkbox, Table, TableBody, TableCell, TableContainer, TablePagination, TableRow } from '@material-ui/core';
import React, { FC, MouseEvent, useEffect, useState } from 'react';
import { NULL_QUERY } from '../withSearch/withSearch';
import { ItemSelected, TableProps } from './interfaces';
import { useStyles } from './styles';
import TableActionColumn from './TableActionColumn';
import TableHead from './TableHead';
import TablePaginationActions from './TablePaginationActions';
import TableToolbar from './TableToolbar';
import useTableActionColumn from './useTableActionColumn';
import classnames from 'classnames';

const EnhancedTable: FC<TableProps> = ({
  listResult,
  isSelectable,
  columns,
  hidePagination,
  checkedItemsQuery,
  paginationCentered,
  selected,
  showCheckedAll = true,
  showResetFilters = true,
  showRefreshTable = true,
  showCheckedElementOrResult = true,
  setSelected,
  onClickRow,
  activeRow,
  tableRowClassName,
  onRequestEdit,
  mutationLoading,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const { graphql } = useApplicationContext();
  const [dense, setDense] = useState(false);
  const isInOperationCommande = window.location.hash.includes('#/operation-produits');
  const [textFieldCell, setTextFieldCell] = useState<any>();
  const [columnToEdit, setColumnToEdit] = useState<any>();
  const type = listResult?.variables?.type;
  const query = listResult?.variables?.query;

  const take = (listResult && listResult.variables && listResult.variables.take) || 12;
  const skip = (listResult && listResult.variables && listResult.variables.skip) || 0;

  const { setPagination } = useFilters();

  const checkedItemsResult = useQuery((checkedItemsQuery && checkedItemsQuery.query) || NULL_QUERY, {
    skip: !checkedItemsQuery ? true : false,
  });

  const skipAllTotalQuery = (): boolean => {
    if (selected && setSelected) return false;
    if (!checkedItemsQuery || (listResult && listResult.variables && !listResult.variables.query)) {
      return true;
    }
    return false;
  };

  const allTotalResult = useSearch_TotalQuery({
    client: graphql,
    variables: { type, query },
    skip: skipAllTotalQuery(),
    fetchPolicy: 'cache-and-network',
  });

  const allTotal =
    (allTotalResult && allTotalResult.data && allTotalResult.data.search && allTotalResult.data.search.total) || 0;

  const checkedItems =
    selected ||
    (checkedItemsQuery &&
      checkedItemsResult.data &&
      checkedItemsQuery &&
      checkedItemsResult.data[checkedItemsQuery.name]) ||
    [];

  const [showCheckeds, setShowChecked] = useState<boolean>(false);

  const data = (listResult && listResult.data && listResult.data.search && listResult.data.search.data) || [];

  const searchIndex = (item: ItemSelected) => {
    if (checkedItems && checkedItems.length) {
      const ids = checkedItems && checkedItems.map((item: any) => item && item.id);
      const selectedIndex = ids && ids.indexOf(item.id);
      return selectedIndex;
    }
    return -1;
  };

  const selectedIds = (selected && selected.map((item) => item && item.id)) || [];

  const handleClick = (row: any, rowId: string, item: any) => (event: MouseEvent<unknown>) => {
    event.preventDefault();
    event.stopPropagation();

    // if (selected && setSelected) {
    //   const selectedIndex = selectedIds.indexOf(rowId);
    //   let newSelected: string[] = [];

    //   if (selectedIndex === -1) {
    //     newSelected = newSelected.concat(selected, row);
    //   } else if (selectedIndex === 0) {
    //     newSelected = newSelected.concat(selected.slice(1));
    //   } else if (selectedIndex === selected.length - 1) {
    //     newSelected = newSelected.concat(selected.slice(0, -1));
    //   } else if (selectedIndex > 0) {
    //     newSelected = newSelected.concat(
    //       selected.slice(0, selectedIndex),
    //       selected.slice(selectedIndex + 1),
    //     );
    //   }

    //   setSelected(newSelected);
    //   return;
    // }

    // if (selected && setSelected) {
    //   const checkeds = isSelected(item, rowId)
    //     ? selected &&
    //       selected.filter(itemChecked => itemChecked && item && itemChecked.id !== item.id)
    //     : selected
    //     ? [...selected, item]
    //     : [item];
    //   setSelected(checkeds);
    //   return;
    // }

    const checkeds = isSelected(item, rowId)
      ? checkedItems && checkedItems.filter((itemChecked: any) => itemChecked && item && itemChecked.id !== item.id)
      : checkedItems
      ? [...checkedItems, item]
      : [item];

    if (isSelectable && checkedItemsQuery && checkedItemsQuery.name) {
      client.writeQuery({
        query: checkedItemsQuery.query,
        data: {
          [checkedItemsQuery.name]: checkeds,
        },
      });
    }
    if (setSelected) {
      setSelected(checkeds);
    }
  };

  const {
    openHistory,
    setOpenHistory,
    openInitPwd,
    setOpenInitPwd,
    openDeleteDialog,
    setOpenDeleteDialog,
    activeRowActionColumnProps,
    setActiveRowActionColumnProps,
    openGroupementSelectionModal,
    setOpenGroupementSelectionModal,
    openUsersModal,
    setOpenUsersModal,
    handleSelectGroupement,
    changingPharmacieGroupement,
    handleClickDeleteRow,
    deletingRow,
    deleteValidationMessage,
  } = useTableActionColumn();

  const isSelected = (item: ItemSelected, rowId?: string) => {
    // if (selected && setSelected && rowId) {
    //   return selectedIds.indexOf(rowId) !== -1;
    // }

    return searchIndex(item) !== -1;
  };

  useEffect(() => {
    if (!mutationLoading && columnToEdit) {
      setColumnToEdit(undefined);
      setTextFieldCell(undefined);
    }
  }, [mutationLoading]);

  const showCellEditMode = (key: string, row: any) => {
    if (textFieldCell !== key) {
      setTextFieldCell(key);
      setColumnToEdit(row);
    }
  };

  const hideCellEditMode = (key: string) => {
    if (onRequestEdit) onRequestEdit(columnToEdit);
  };

  const onKeyPressEnter = (e: any) => {
    if (e.key === 'Enter' && onRequestEdit) {
      onRequestEdit(columnToEdit);
    }
  };

  const handleCellChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, row: any) => {
    const { value, name } = event.target;
    setColumnToEdit((prevState: any) => ({ ...prevState, [name]: value }));
  };

  const selectionLength: number = 0;
  // isSelectable && selected && handleSelection ? selected.length : -1;

  const handleClickRow = (row: any) => (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    if (onClickRow) {
      onClickRow(row);
    }
  };

  const clearSelection = () => {
    if (selected && setSelected) {
      setSelected([]);
    }

    if (isSelectable && checkedItemsQuery && checkedItemsQuery.name) {
      client.writeQuery({
        query: checkedItemsQuery.query,
        data: {
          [checkedItemsQuery.name]: null,
        },
      });
      setShowChecked(false);
    }
  };

  const updateSkipAndTake = (skip: number, take: number) => {
    const pagination = { take, skip };
    setPagination(pagination);
  };

  const ROWS_PER_PAGE_OPTIONS = [12, 25, 50, 100, 250, 500, 1000, 2000];

  return (
    <div className={classes.root}>
      {isSelectable && (
        <TableToolbar
          checkedItems={checkedItems}
          listResult={listResult}
          clearSelection={clearSelection}
          setShowCheckeds={setShowChecked}
          showCheckeds={showCheckeds}
          allTotal={allTotal}
          checkedItemsQuery={checkedItemsQuery}
          selected={selected}
          showCheckedAll={showCheckedAll}
          showResetFilters={showResetFilters}
          showRefreshTable={showRefreshTable}
          setSelected={setSelected}
          showCheckedElementOrResult={showCheckedElementOrResult}
        />
      )}
      <TableContainer className={classes.tableWrapper}>
        <Table
          className={classes.table}
          aria-labelledby="tableTitle"
          size={dense ? 'small' : 'medium'}
          stickyHeader={true}
        >
          <TableHead
            checkedItemsQuery={checkedItemsQuery}
            isSelectable={isSelectable}
            checkedItems={checkedItems}
            listResult={listResult}
            columns={columns}
            selected={selected}
            setSelected={setSelected}
            allTotal={allTotal}
          />
          {listResult && listResult.loading ? (
            <SmallLoading />
          ) : (
            <TableBody>
              {(showCheckeds ? selected : data).map((row: any, indexInArray: any) => {
                if (row) {
                  const index = row.id || indexInArray;
                  const itemDesc: ItemSelected = { ...row };
                  const isItemSelected = isSelected(itemDesc, row.id);
                  const labelId = `enhanced-table-checkbox-${index}`;
                  const isActive: boolean = activeRow?.id === row.id;

                  const selectSection = isSelectable ? (
                    <TableCell padding="checkbox" key={indexInArray}>
                      <Checkbox
                        onClick={handleClick(row, row.id, itemDesc)}
                        checked={isItemSelected}
                        inputProps={{ 'aria-labelledby': labelId }}
                      />
                    </TableCell>
                  ) : null;

                  return (
                    <TableRow
                      hover={true}
                      onClick={handleClickRow(row)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={`row-${index}`}
                      selected={isItemSelected}
                      style={{ cursor: onClickRow ? 'pointer' : 'default' }}
                      className={
                        isActive ? classnames(classes.activeRow, tableRowClassName || '') : tableRowClassName || ''
                      }
                    >
                      {selectSection}

                      {columns
                        ? columns.map((column, columnIndex) => {
                            const key = `row-${index}-${columnIndex}`;
                            return (
                              <TableCell
                                key={key}
                                className={classes.tableCellPadding}
                                align={column.centered ? 'center' : 'left'}
                                onClick={
                                  !mutationLoading && column.editable ? () => showCellEditMode(key, row) : undefined
                                }
                              >
                                {column.tableActionColumn ? (
                                  <TableActionColumn
                                    {...column.tableActionColumn}
                                    {...{
                                      row,
                                      setActiveRowProps: setActiveRowActionColumnProps,
                                      setOpenDeleteDialog,
                                      setOpenGroupementSelectionModal,
                                      setOpenHistory,
                                      setOpenUsersModal,
                                      setOpenInitPwd,
                                    }}
                                  />
                                ) : textFieldCell === key ? (
                                  mutationLoading ? (
                                    <LoaderSmall />
                                  ) : (
                                    <CustomFormTextField
                                      autoFocus={true}
                                      id={row}
                                      onChange={(event: any) => handleCellChange(event, row)}
                                      value={column.name ? columnToEdit[column.name] : ''}
                                      name={column.name}
                                      onBlur={() => hideCellEditMode(key)}
                                      onKeyDown={onKeyPressEnter}
                                    />
                                  )
                                ) : column.renderer ? (
                                  column.renderer(row)
                                ) : row[column.name] ? (
                                  row[column.name]
                                ) : (
                                  '-'
                                )}
                              </TableCell>
                            );
                          })
                        : null}
                    </TableRow>
                  );
                }
              })}
            </TableBody>
          )}
        </Table>
        <div>
          {data && data.length === 0 && !listResult.loading ? (
            <div className={classes.alignCenter}>Aucun résultat correspondant</div>
          ) : (
            ''
          )}
        </div>
      </TableContainer>

      {!hidePagination && data && data.length > 0 && (
        <TablePagination
          className={paginationCentered ? classes.tablePagination : ''}
          rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
          // tslint:disable-next-line: jsx-no-lambda
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to === -1 ? count : to} sur ${count !== -1 ? count : to}`
          }
          labelRowsPerPage="Nombre par page"
          component="div"
          count={listResult && listResult.data && listResult.data.search && listResult.data.search.total}
          rowsPerPage={take}
          page={skip / take}
          backIconButtonProps={{
            'aria-label': 'Page Précédente',
          }}
          nextIconButtonProps={{
            'aria-label': 'Page suivante',
          }}
          // tslint:disable-next-line: jsx-no-lambda
          onChangePage={(_, newPage) => updateSkipAndTake(newPage * take, take)}
          // tslint:disable-next-line: jsx-no-lambda
          onChangeRowsPerPage={(e) => updateSkipAndTake(0, parseInt(e.target.value, 10))}
          ActionsComponent={TablePaginationActions}
        />
      )}
      <HistoriquePersonnel open={openHistory} setOpen={setOpenHistory} userId={activeRowActionColumnProps?.row?.id} />
      <ResetUserPasswordModal
        open={openInitPwd}
        setOpen={setOpenInitPwd}
        userId={activeRowActionColumnProps?.row?.user?.id}
        email={activeRowActionColumnProps?.row?.user?.email}
        login={activeRowActionColumnProps?.row?.user?.login}
      />
      <UsersModal
        open={openUsersModal}
        setOpen={setOpenUsersModal}
        row={activeRowActionColumnProps?.row}
        baseUrl={activeRowActionColumnProps?.baseUrl}
      />
      <GroupementSelectionModal
        open={openGroupementSelectionModal}
        setOpen={setOpenGroupementSelectionModal}
        onSelect={handleSelectGroupement}
        saving={changingPharmacieGroupement.loading}
      />
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        title={deleteValidationMessage}
        onClickConfirm={handleClickDeleteRow}
        isLoading={deletingRow}
      />
    </div>
  );
};

export default EnhancedTable;
