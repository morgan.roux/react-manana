import { createStyles, makeStyles, Theme, lighten, darken } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      padding: theme.spacing(3),
      '& .MuiSvgIcon-root': {
        width: 20,
        height: 20,
      },
    },
    table: {
      minWidth: 750,
    },
    circularProgress: {
      width: '100%!important',
      display: 'flex',
      justifyContent: 'center',
      minHeight: '200px',
      alignItems: 'center',
    },
    tableWrapper: {
      '& .MuiTableCell-stickyHeader': {
        backgroundColor: lighten(theme.palette.primary.main, 0.1),
      },
      '& *': {
        fontWeight: 'bold',
      },
      '& thead *': {
        fontSize: 12,
      },
    },
    avatar: {
      maxWidth: 30,
      maxHeight: 30,
      borderRadius: '50%',
    },
    tableCellPadding: {
      '& .MuiTableCell-root': {
        padding: '6px 12px 6px 12px',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
      },
    },
    tablePagination: {
      display: 'flex',
      justifyContent: 'center',
    },
    activeRow: {
      backgroundColor: theme.palette.secondary.main,
      '& td ': {
        color: theme.palette.common.white,
      },
      '&:hover': {
        backgroundColor: `${darken(theme.palette.secondary.main, 0.1)} !important`,
        '& td ': {
          color: theme.palette.common.white,
        },
      },
    },
    alignCenter: {
      textAlign: 'center',
      margin: '40px 0px',
      fontSize: 20,
      fontWeight: 'bold',
    },
  })
);
