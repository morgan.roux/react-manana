import React, { MouseEvent, useState } from 'react';
import { GroupementInfoFragment } from 'src/graphql';
import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useChange_Pharmacie_GroupementMutation } from '../../federation';

const useTableActionColumn = () => {
  const [openHistory, setOpenHistory] = useState<boolean>(false);
  const [openInitPwd, setOpenInitPwd] = useState<boolean>(false);
  const [activeRowActionColumnProps, setActiveRowActionColumnProps] = useState<any>();
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [openGroupementSelectionModal, setOpenGroupementSelectionModal] = useState<boolean>(false);
  const [openUsersModal, setOpenUsersModal] = useState<boolean>(false);
  const [deletingRow, setDeletingRow] = useState<boolean>(false);

  const { isGroupePharmacie } = useApplicationContext();

  const { federation, currentPharmacie } = useApplicationContext();
  const displayNotification = useDisplayNotification();

  const [changePharmacieGroupement, changingPharmacieGroupement] = useChange_Pharmacie_GroupementMutation({
    client: federation,
    onCompleted: (data) => {
      displayNotification({
        type: 'success',
        message: `La pharmacie a été déplacée avec succès !`,
      });
      if (currentPharmacie?.id !== activeRowActionColumnProps?.row?.id && activeRowActionColumnProps?.refetch) {
        activeRowActionColumnProps.refetch();
      } else {
        localStorage.removeItem('pharmacie');
        if (setOpenGroupementSelectionModal) setOpenGroupementSelectionModal(false);
        window.location.reload();
      }
    },
  });

  const deleteValidationMessage = isGroupePharmacie
    ? `${activeRowActionColumnProps?.row?.civilite || ''} ${activeRowActionColumnProps?.row?.fullName || ''} ${
        activeRowActionColumnProps?.row?.prenom || ''
      } ${activeRowActionColumnProps?.row?.nom || ''}`
    : '';

  const deleteVariables = { ids: [activeRowActionColumnProps?.row?.id] };

  const handleSelectGroupement = (groupement: GroupementInfoFragment) => {
    changePharmacieGroupement({
      variables: { idGroupement: groupement.id, idPharmacie: activeRowActionColumnProps?.row?.id },
    });
  };

  const handleClickDeleteRow = async (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    if (activeRowActionColumnProps?.deleteMutation) {
      setDeletingRow(true);
      await activeRowActionColumnProps.deleteMutation({
        variables: deleteVariables,
      });
      setDeletingRow(false);
      setOpenDeleteDialog(false);
    }
  };

  return {
    openHistory,
    setOpenHistory,
    openInitPwd,
    setOpenInitPwd,
    openDeleteDialog,
    setOpenDeleteDialog,
    activeRowActionColumnProps,
    setActiveRowActionColumnProps,
    openGroupementSelectionModal,
    setOpenGroupementSelectionModal,
    openUsersModal,
    setOpenUsersModal,
    handleSelectGroupement,
    changingPharmacieGroupement,
    handleClickDeleteRow,
    deletingRow,
    deleteValidationMessage,
  };
};
export default useTableActionColumn;
