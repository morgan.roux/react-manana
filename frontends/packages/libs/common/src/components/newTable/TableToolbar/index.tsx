import { Typography } from '@material-ui/core';
import ReplayIcon from '@material-ui/icons/Replay';
import classnames from 'classnames';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useFilters } from '../../../shared';
import { useApplicationContext } from '../../../context';
import LaboratoireFilter from '../../newCustomContent/Filters/LaboratoireFilter';
import PersonnelAffectationFilter from '../../newCustomContent/Filters/PersonnelAffectationFilter';
import PersonnelGroupementFilter from '../../newCustomContent/Filters/PersonnelGroupementFilter';
// import { ContentContext, ContentStateInterface } from '../../../../AppContext';
import PharmacieFilter from '../../newCustomContent/Filters/PharmacieFilter';
import TitulaireFilter from '../../newCustomContent/Filters/TitulaireFilter';
import { TableToolbarProps } from '../interfaces';
import { useToolbarStyles } from './styles';
import { CustomCheckBox, CustomModal, NewCustomButton } from '@lib/ui-kit';
import { Delete, List, ListAlt, Refresh, Tune } from '@material-ui/icons';

const EnhancedTableToolbar: FC<TableToolbarProps & RouteComponentProps> = ({
  listResult,
  clearSelection,
  setShowCheckeds,
  allTotal,
  showCheckeds,
  selected,
  setSelected,
  showCheckedElementOrResult = true,
  showCheckedAll = true,
  showResetFilters = true,
  showRefreshTable = true,
  location: { pathname },
}) => {
  const classes = useToolbarStyles();
  const { isAdminGroupement, isSuperAdmin, graphql, notify } = useApplicationContext();
  const isAdmin = isAdminGroupement || isSuperAdmin;
  const { setQueryStrings, resetSearchFilters } = useFilters();

  const dashboardPharmacieUrl = '/db/pharmacies';
  const operationPharmacieUrl = '/pharmacie';
  const personnelGroupementUrl = '/db/personnel-groupement/list';
  const personnelAffectationUrl = '/db/personnel-groupement/affectation';
  const titulaireUrl = '/db/titulaires-pharmacies';
  const titulairePharmacieUrl = '/db/titulaires-pharmacies/create';
  const gestionClientUrl = '/db/groupes-clients';
  const laboratoireUrl = '/laboratoire';
  const onPharmacie =
    pathname.startsWith(dashboardPharmacieUrl) ||
    pathname.includes(operationPharmacieUrl) ||
    pathname.startsWith(`${gestionClientUrl}/create`) ||
    pathname.startsWith(`${gestionClientUrl}/edit`);

  const onPersonnelGroupement = pathname.startsWith(personnelGroupementUrl);
  const onPersonnelAffectation = pathname.startsWith(personnelAffectationUrl);
  const onTitulaire = pathname.startsWith(titulaireUrl) && !pathname.startsWith(titulairePharmacieUrl);
  const onPharmacieTitulaire = pathname.startsWith(titulairePharmacieUrl);
  const onLaboratoire = pathname.includes(laboratoireUrl);

  const [openFilter, setOpenFilter] = useState<boolean>(false);

  useEffect(() => {
    if (selected) {
      //TODO: Migration
      /* client.writeData({
        data: {
          idCheckeds: showCheckeds ? selected && selected.map((item) => item && item.id) : [],
        },
      });*/
      return;
    }

    //TODO: Migration
    /* client.writeData({
      data: {
        idCheckeds: showCheckeds ? checkedItems && checkedItems.map((item) => item && item.id) : [],
      },
    });*/
  }, [showCheckeds]);

  const total = (listResult && listResult.data && listResult.data.search && listResult.data.search.total) || 0;

  const isActualite: boolean = window.location.hash === '#/' || window.location.hash.startsWith('#/actualite');
  const isOperation: boolean = window.location.hash.startsWith('#/operations-commerciales');
  const dataType: string = isActualite ? 'actualite' : isOperation ? 'operation' : '';

  const handleResetFields = () => {
    setFieldsState({});
    setQueryStrings([]);
  };

  const handleResetFilter = () => {
    setFieldsState({});
    resetSearchFilters();
  };

  const handleCheckAll = () => {
    if (selected && setSelected) {
      if (selected.length === allTotal) {
        setSelected([]);
      } else {
        setSelected(listResult?.data?.search?.data || []);
      }
    }
  };

  const handleOpenFilter = () => {
    setOpenFilter(true);
  };

  const handleShowCheckeds = () => {
    setShowCheckeds(!showCheckeds);
  };

  const onClickRefresh = () => {
    if (listResult && listResult.refetch) {
      listResult.refetch();
    }
  };
  const [fieldsState, setFieldsState] = useState<any>({});

  const handleFieldChange = (event: any): void => {
    setFieldsState({
      ...fieldsState,
      [event.target.name]:
        event.target.name === 'actived' || event.target.name === 'pharmacies.actived'
          ? event.target.value !== -1
            ? event.target.value === 1
              ? true
              : false
            : undefined
          : event.target.value,
    });
  };

  const handleRunSearch = (query: any) => {
    setOpenFilter(false);
    setQueryStrings(query);
  };

  const filter =
    onPharmacie || onPharmacieTitulaire ? (
      <PharmacieFilter
        fieldsState={fieldsState}
        handleFieldChange={handleFieldChange}
        handleRunSearch={handleRunSearch}
        handleResetFields={handleResetFields}
      />
    ) : onPersonnelGroupement ? (
      <PersonnelGroupementFilter
        fieldsState={fieldsState}
        handleFieldChange={handleFieldChange}
        handleRunSearch={handleRunSearch}
        handleResetFields={handleResetFields}
      />
    ) : onTitulaire ? (
      <TitulaireFilter
        fieldsState={fieldsState}
        handleFieldChange={handleFieldChange}
        handleRunSearch={handleRunSearch}
        handleResetFields={handleResetFields}
      />
    ) : onPersonnelAffectation ? (
      <PersonnelAffectationFilter
        setFieldsState={setFieldsState}
        fieldsState={fieldsState}
        handleFieldChange={handleFieldChange}
        handleRunSearch={handleRunSearch}
        handleResetFields={handleResetFields}
      />
    ) : onLaboratoire ? (
      <LaboratoireFilter
        fieldsState={fieldsState}
        handleFieldChange={handleFieldChange}
        handleRunSearch={handleRunSearch}
        handleResetFields={handleResetFields}
      />
    ) : null;

  const numSelected = (): number => {
    if (selected && setSelected) {
      return selected.length > total ? total : selected.length;
    }

    return 0;
  };

  return (
    <div className={classnames(classes.root)}>
      <div className={classes.title}>
        <Typography color="inherit" variant="subtitle1">
          {`${numSelected()}/${allTotal}`} {numSelected() > 1 ? 'sélectionnés' : 'sélectionné'} {`(${total})`}
        </Typography>
      </div>
      <div>
        <div className={classes.buttons}>
          {showCheckedAll && (
            <CustomCheckBox
              variant="text"
              color="inherit"
              size="medium"
              name="checkedAll"
              label={`Tout sélectionner (${allTotal})`}
              value={selected && selected.length === allTotal ? true : false}
              checked={selected && selected.length === allTotal ? true : false}
              onChange={handleCheckAll}
            />
          )}
          {filter && (
            <NewCustomButton
              variant="text"
              theme="transparent"
              size="medium"
              startIcon={<Tune />}
              className={classes.btn}
              onClick={handleOpenFilter}
            >
              Filtres
            </NewCustomButton>
          )}
          {filter && (
            <NewCustomButton
              variant="text"
              theme="transparent"
              size="medium"
              startIcon={<ReplayIcon />}
              className={classes.btn}
              onClick={handleResetFilter}
            >
              Réinitialiser les filtres
            </NewCustomButton>
          )}
          {numSelected() > 0 && (
            <>
              {showCheckedElementOrResult && (
                <NewCustomButton
                  variant="text"
                  theme="transparent"
                  size="medium"
                  startIcon={showCheckeds ? <List /> : <ListAlt />}
                  className={classes.btn}
                  onClick={handleShowCheckeds}
                >
                  {showCheckeds ? 'Afficher les résultats' : 'Afficher les éléments selectionnés'}
                </NewCustomButton>
              )}

              <NewCustomButton
                variant="text"
                theme="transparent"
                size="medium"
                startIcon={<Delete />}
                className={classes.btn}
                onClick={clearSelection}
              >
                Vider la sélection
              </NewCustomButton>
            </>
          )}
          {showRefreshTable && (
            <NewCustomButton
              variant="text"
              theme="transparent"
              size="medium"
              startIcon={<Refresh />}
              className={classes.btn}
              onClick={onClickRefresh}
            >
              Réactualiser
            </NewCustomButton>
          )}
        </div>
      </div>
      <CustomModal
        title="Filtres de recherche"
        children={filter}
        open={openFilter}
        setOpen={setOpenFilter}
        withBtnsActions={false}
        headerWithBgColor={true}
        closeIcon={true}
      />
    </div>
  );
};

export default withRouter(EnhancedTableToolbar);
