import { CustomModal } from '@lib/ui-kit';
import React, { FC, useEffect, useState } from 'react';
import { Dropzone } from '../Dropzone';
import { useApplicationContext } from './../../context';
import { CustomFormFilesProps } from './interface';
export const CustomFormFiles: FC<CustomFormFilesProps> = ({
  open,
  setOpen,
  onRequestSave,
  saving,
  setSaving,
  saved,
}) => {
  const { isMobile } = useApplicationContext();

  const [fichiersJoints, setFichiersJoints] = useState<any[]>([]);

  useEffect(() => {
    if (!open) {
      setFichiersJoints([]);
    }
    if (saved) {
      setOpen(false);
      setSaving(false);
    }
  }, [open, saved]);

  const isValid = () => {
    return fichiersJoints.length > 0;
  };

  const handleSave = () => {
    setSaving(true);
    onRequestSave(fichiersJoints);
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Ajout des fichiers joints"
      withBtnsActions
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      disabledButton={!isValid() || saving}
      onClickConfirm={handleSave}
      fullScreen={!!isMobile}
    >
      <Dropzone
        multiple
        selectedFiles={fichiersJoints}
        setSelectedFiles={setFichiersJoints}
        contentText="Glissez et déposez vos documents"
      />
    </CustomModal>
  );
};
