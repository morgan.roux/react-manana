export interface CustomFormFilesProps {
  open: boolean;
  setOpen: (open: boolean) => void;
  onRequestSave: (files: any[]) => void;
  saving: boolean;
  setSaving: (saving: boolean) => void;
  saved: boolean;
}
