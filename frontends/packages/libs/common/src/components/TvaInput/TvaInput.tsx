import React, { FC } from 'react';
import { CustomSelect } from '@lib/ui-kit';
import { useEfTvaQuery } from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';
import moment from 'moment';

export interface TvaSelectInputProps {
  idTva?: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  label?: string;
  name?: string;
  disabled?: boolean;
  required?:boolean
}

const debutAnnee = moment().startOf('year').toDate();
const finAnnee = moment().endOf('year').toDate();

export const TvaInput: FC<TvaSelectInputProps> = ({ idTva, onChange, name, label, disabled,required }) => {
  const { currentPharmacie } = useApplicationContext();
  const loadTvas = useEfTvaQuery({
    variables: {
      filter: {
        idPharmacie: {
          eq: currentPharmacie.id,
        },
        dateDebutExercice: {
          between: {
            lower: debutAnnee,
            upper: finAnnee,
          },
        },
      },
    },
  });

  return (
    <CustomSelect
      list={loadTvas.data?.eFTvas?.nodes || []}
      loading={loadTvas.loading}
      error={loadTvas.error}
      index="libelle"
      listId="id"
      label={label || 'TVA'}
      name={name || 'idTva'}
      value={idTva}
      onChange={onChange}
      required={required}
      withPlaceholder
      placeholder="Choisir le TVA"
      disabled={disabled}
    />
  );
};
