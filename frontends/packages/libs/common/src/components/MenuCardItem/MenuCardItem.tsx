import { DocumentCategorie, IDocumentSearch, useApplicationContext } from '@lib/common';
import { Box, Card, Chip, Menu, SvgIcon, SvgIconProps, Typography } from '@material-ui/core';
import { ChatBubble, GetApp, MoreHoriz, ThumbUp, Visibility } from '@material-ui/icons';
import classNames from 'classnames';
import moment from 'moment';
import React, { FC, CSSProperties } from 'react';
import { DocumentCardMenu } from '../EspaceDocumentaire/Sidebar/DocumentCardMenu';
import { useStyles } from './style';

interface MenuCardItemProps {
  info: DocumentCategorie;
  activeSousCategorie: string;
  document: any;
  style?: CSSProperties;
  saving: boolean;
  saved: boolean;
  setSaved: (newValue: boolean) => void;
  onRequestAddToFavorite: (document: DocumentCategorie) => void;
  onRequestRemoveFromFavorite: (document: DocumentCategorie) => void;
  onRequestDelete: (document: DocumentCategorie) => void;
  onRequestReplaceDocument: (document: DocumentCategorie) => void;
  onRequestDeplace: (document: DocumentCategorie) => void;
  onClick: () => void;
  active?: boolean;
  searchVariables?: IDocumentSearch;

  espaceType?: string;
  tvas: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  defaultCorrespondant?: any;
}

const STATUT_LABELS = {
  EN_ATTENTE_APPROBATION: 'à valider',
  REFUSE: 'rejetée',
  APPROUVE: 'validée',
};

export const MenuCardItem: FC<MenuCardItemProps> = ({
  info,
  activeSousCategorie,
  document,
  saving,
  saved,
  setSaved,
  onRequestDeplace,
  onRequestAddToFavorite,
  onRequestRemoveFromFavorite,
  onRequestReplaceDocument,
  onRequestDelete,
  onClick,
  active,
  searchVariables,

  espaceType,
  tvas,
  defaultCorrespondant,
}) => {
  const classes = useStyles({});
  const [openOptions, setOpenOptions] = React.useState<boolean>(false);
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const handleCloseOptions = () => {
    setOpenOptions(false);
    setAnchorEl(null);
  };

  const handleClickOptions = (event: React.MouseEvent) => {
    event.preventDefault();
    event.stopPropagation();
    setAnchorEl(event.currentTarget as HTMLElement);
    setOpenOptions((prev) => !prev);
  };

  const { isMobile } = useApplicationContext();

  const open = Boolean(openOptions);

  // console.log('searchVariables', searchVariables);

  return (
    <>
      <Card
        className={classNames({ [classes.active]: active })}
        classes={{ root: classes.card }}
        elevation={active ? 0 : 1}
      >
        <Box onClick={onClick}>
          <Box className={classNames(classes.libelleContainer, classes.padding)}>
            <Typography color="secondary" variant="subtitle2" gutterBottom>
              {'FACTURE' === info?.type
                ? searchVariables?.libelleSousEspace
                  ? searchVariables?.libelleSousEspace
                  : 'Facture'
                : 'CONTRAT' === info?.type
                ? 'RESILIE' === info?.dernierChangementStatut?.status
                  ? 'Contrat résilié'
                  : 'Contrat'
                : info?.sousCategorie?.libelle}
            </Typography>
            <span onClick={handleClickOptions}>
              <MoreHoriz />
            </span>
          </Box>
          <Box className={classNames(classes.padding)}>
            <span className={classNames({ [classes.bold]: !isMobile }, { [classes.light]: isMobile })}>
              <div dangerouslySetInnerHTML={{ __html: info.description || '' }} style={{ textAlign: 'left' }} />
            </span>
          </Box>
          {isMobile && <Box className={classes.separateur} />}
          {'FACTURE' === info?.type ? (
            <Box className={classNames(classes.details, classes.padding)} marginTop={isMobile ? 0 : 2}>
              <Box marginBottom={2}>
                <Typography component="span" variant="caption" color="textSecondary">
                  Facture n°&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info?.numeroFacture}
                </Typography>
              </Box>
              <Box marginBottom={2}>
                <Typography component="span" variant="caption" color="textSecondary">
                  Total TTC&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {(info?.factureTotalTtc || 0).toFixed(2)}&nbsp;€
                </Typography>
              </Box>
              <Box>
                <Typography component="span" variant="caption" color="textSecondary">
                  TVA&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {(info?.factureTva || 0).toFixed(2)}&nbsp;€
                </Typography>
              </Box>
              <Box>
                <Typography component="span" variant="caption" color="textSecondary">
                  Total HT&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {(info?.factureTotalHt || 0).toFixed(2)}&nbsp;€
                </Typography>
              </Box>
            </Box>
          ) : 'CONTRAT' === info?.type ? (
            <Box className={classNames(classes.details, classes.padding)} marginTop={isMobile ? 0 : 2}>
              <Box marginBottom={2}>
                <Typography component="span" variant="caption" color="textSecondary">
                  Type&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  Contrat
                </Typography>
              </Box>
              <Box marginBottom={2}>
                {moment(info?.dateHeureFinValidite).isValid() && (
                  <Typography component="span" variant="caption" color="textSecondary">
                    Date fin&nbsp;:&nbsp;
                  </Typography>
                )}
                {moment(info?.dateHeureFinValidite).isValid() && (
                  <Typography component="span" variant="subtitle2" color="textSecondary">
                    {moment(info?.dateHeureFinValidite).format('DD/MM/YYYY')}
                  </Typography>
                )}
              </Box>
              <Box>
                <Typography component="span" variant="caption" color="textSecondary">
                  Origine&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info?.origineAssocie?.nom || info?.origineAssocie?.fullName}
                </Typography>
              </Box>
              <Box>
                <Typography component="span" variant="caption" color="textSecondary">
                  Date début&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {moment(info?.dateHeureDebutValidite).format('DD/MM/YYYY')}
                </Typography>
              </Box>
            </Box>
          ) : (
            <Box className={classNames(classes.details, classes.padding)} marginTop={isMobile ? 0 : 2}>
              <Box marginBottom={2}>
                <Typography component="span" variant="caption" color="textSecondary">
                  Rédacteur&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info?.redacteur?.fullName}
                </Typography>
              </Box>
              <Box marginBottom={2}>
                <Typography component="span" variant="caption" color="textSecondary">
                  Nomenclature&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info.nomenclature}
                </Typography>
              </Box>
              <Box>
                <Typography component="span" variant="caption" color="textSecondary">
                  Vérificateur&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info?.verificateur?.fullName}
                </Typography>
              </Box>
              <Box>
                <Typography component="span" variant="caption" color="textSecondary">
                  N° version&nbsp;:&nbsp;
                </Typography>
                <Typography component="span" variant="subtitle2" color="textSecondary">
                  {info.numeroVersion}
                </Typography>
              </Box>
            </Box>
          )}
          {isMobile && <Box className={classes.separateur} />}
          <Box marginTop={isMobile ? -1 : 1} className={classNames(classes.status, classes.padding)}>
            <Chip
              icon={<Visibility />}
              label={
                <Typography variant="subtitle2" color="textSecondary" component="span">
                  {info?.nombreConsultations}
                </Typography>
              }
            />
            <Chip
              icon={<ThumbUp />}
              label={
                <Typography variant="subtitle2" color="textSecondary" component="span">
                  {info?.nombreReactions}
                </Typography>
              }
            />
            <Chip
              icon={<ChatBubble />}
              label={
                <Typography variant="subtitle2" color="textSecondary" component="span">
                  {info?.nombreCommentaires}
                </Typography>
              }
            />
            <Chip
              icon={<GetApp />}
              label={
                <Typography variant="subtitle2" color="textSecondary" component="span">
                  {info?.nombreTelechargements}
                </Typography>
              }
            />
          </Box>
        </Box>
      </Card>

      <Menu
        open={open}
        anchorEl={anchorEl}
        onClose={handleCloseOptions}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <DocumentCardMenu
          setOpenOptions={setOpenOptions}
          saving={saving}
          saved={saved}
          setSaved={setSaved}
          activeSousCategorie={activeSousCategorie}
          document={document}
          onRequestDelete={onRequestDelete}
          onRequestAddToFavorite={onRequestAddToFavorite}
          onRequestRemoveFromFavorite={onRequestRemoveFromFavorite}
          onRequestReplaceDocument={onRequestReplaceDocument}
          onRequestDeplace={onRequestDeplace}
          searchVariables={searchVariables}
          espaceType={espaceType}
          tvas={tvas}
          defaultCorrespondant={defaultCorrespondant}
        />
      </Menu>
    </>
  );
};
