import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) => ({
  description: {
    fontWeight: 700,
  },
  card: {
    marginTop: 4,
    // marginBottom: 20,
    padding: 15,
    cursor: 'pointer',
  },
  flexcontainer: {
    '& *': {
      fontSize: 14,
    },
  },
  status: {
    display: 'flex',
    justifyContent: 'space-around',
    '& [class*=MuiChip-root]': {
      justifyContent: 'flex-start',
      background: 'none',
      '& svg': {
        marginLeft: 0,
        flexWrap: 'wrap',
      },
    },
  },
}));
