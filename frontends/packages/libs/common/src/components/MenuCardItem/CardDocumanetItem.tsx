import { Box, Card, Chip, Typography } from '@material-ui/core';
import { ChatBubble, Visibility } from '@material-ui/icons';
import moment from 'moment';
import React, { CSSProperties, FC, forwardRef } from 'react';
import { ActionMenuButton } from '../ActionMenuButton';
import { useStyles } from './styles';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import { Skeleton } from '@material-ui/lab';
interface CardDocumentItemProps {
  page?: string;
  data?: any;
  labelDeclarant?: string;
  onClickItem?: (id: any) => void;
  labelAddMenuAction?: string;
  handleAddMenuAction?: () => void;
  handleVoirDetailMenuAction?: (id?: string) => void;
  handleEditMenuAction?: (data?: any, id?: string) => void;
  handleDeleteMenuAction?: (data?: any, id?: string) => void;
  handleApporterSolutionMenuAction?: (data?: any, id?: string) => void;
  style?: CSSProperties;
  loading?: boolean;
  useDivider?: boolean;
  active?: boolean;
}
export const CardDocumentItem = forwardRef<HTMLDivElement, CardDocumentItemProps>(
  (
    {
      data,
      labelDeclarant,
      onClickItem,
      handleVoirDetailMenuAction,
      handleAddMenuAction,
      labelAddMenuAction,
      handleEditMenuAction,
      handleDeleteMenuAction,
      handleApporterSolutionMenuAction,
      page,
      style,
      loading,
      useDivider,
      active,
    },
    containerRef
  ) => {
    const classes = useStyles();

    const Importance = (importance?: any) => {
      return (
        <span
          style={{
            background: importance?.couleur,
            color: importance?.ordre === 1 ? '#fff' : '#000',
            padding: 5,
            borderRadius: 3,
          }}
        >
          {importance?.ordre === 1 ? 'Fort' : 'Faible'}
        </span>
      );
    };
    const handleClickItem = (event: React.MouseEvent<{}>) => {
      onClickItem && onClickItem(data.id);
    };
    if (loading) {
      return (
        <div ref={containerRef} className={classes.card}>
          <Skeleton variant="circle" width={35} height={35} animation="wave" />
        </div>
      );
    }
    return (
      <div ref={containerRef} style={style}>
        {useDivider && <Box height={5}></Box>}
        <Card
          className={classes.card}
          elevation={!active ? 2 : 0}
          onClick={handleClickItem}
          style={active ? { background: '#F8F8F8' } : {}}
        >
          <Box display="flex" justifyContent="space-between">
            <Box>
              <Typography color="primary">{data.type?.libelle}</Typography>
              <Typography dangerouslySetInnerHTML={{ __html: data.description }} className={classes.description} />
            </Box>
            <Box display="flex" alignItems="flex-start">
              <Typography variant="caption">{moment(data.createdAt).fromNow()}</Typography>

              <ActionMenuButton
                withVoirDetail={false}
                page={page}
                paddingBlock={0}
                data={data}
                handleAddMenuAction={handleAddMenuAction}
                handleDeleteMenuAction={handleDeleteMenuAction}
                handleEditMenuAction={handleEditMenuAction}
                handleVoirDetailMenuAction={handleVoirDetailMenuAction}
                labelAddMenuAction={labelAddMenuAction}
                handleApporterSolutionMenuAction={handleApporterSolutionMenuAction}
              />
            </Box>
          </Box>
          <div className={classes.flexcontainer}>
            <Box display="flex" justifyContent="space-between" mb={1}>
              <Typography>
                {labelDeclarant || 'Animateur'} :{' '}
                <span style={{ fontWeight: 500, marginLeft: 8 }}>
                  {data.declarant?.fullName || data.auteur?.fullName || data.animateur?.fullName}
                </span>
              </Typography>
              <Typography>
                Date :{' '}
                {data.dateIncident
                  ? moment(data.dateIncident).format('DD/MM/YYYY')
                  : data.dateAction
                  ? moment(data.dateAction).format('DD/MM/YYYY')
                  : ''}
              </Typography>
            </Box>
            {page !== 'reunion' && (
              <Box display="flex" justifyContent="space-between" mb={1}>
                <Typography>
                  Type : <span style={{ fontWeight: 500, marginLeft: 44 }}>{data.type?.libelle}</span>
                </Typography>
                <Typography>{Importance(data.importance)}</Typography>
              </Box>
            )}
          </div>
          <Box className={classes.status}>
            <Chip
              icon={<Visibility />}
              label={
                <Typography variant="subtitle2" color="textSecondary" component="span">
                  {data.nombreConsultations}
                </Typography>
              }
            />
            <Chip
              icon={<ChatBubble />}
              label={
                <Typography variant="subtitle2" color="textSecondary" component="span">
                  {data.nombreCommentaires}
                </Typography>
              }
            />
            {page === 'reunion' && (
              <Chip
                icon={<EqualizerIcon />}
                label={
                  <Typography variant="subtitle2" color="textSecondary" component="span">
                    {`${data?.nombreTotalClotureActions || 0}/${data?.nombreTotalActions || 0}`}
                  </Typography>
                }
              />
            )}
          </Box>
        </Card>
        {useDivider && <Box height={5}></Box>}
      </div>
    );
  }
);
