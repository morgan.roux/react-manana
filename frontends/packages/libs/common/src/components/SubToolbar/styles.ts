import { makeStyles, Theme, createStyles, lighten } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    searchBar: {
      top: 0,
      width: '100%',
      opacity: 1,
      zIndex: 10,
      position: 'sticky',
      display: 'flex',
      justifyContent: 'space-between',
      height: 70,
      alignItems: 'center',
      backgroundColor: theme.palette.common.white,
      '@media (max-width: 692px)': {
        flexWrap: 'wrap',
      },
      borderBottom: '1px solid #e0e0e0',
      padding: '0px 20px 0px 20px',
    },
    dark: {
      background: lighten(theme.palette.primary.main, 0.1),
      color: theme.palette.common.white,
    },
    search: {
      '@media (max-width: 580px)': {
        marginBottom: 15,
      },
    },
    title: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 20,
      width: '100%',
      textAlign: 'center',
    },
    childrenContainer: {
      display: 'flex',
      alignItems: 'center',
      position: 'absolute',
      right: 24,
      '& > button:not(:nth-child(1))': {
        marginLeft: 15,
      },
    },
  })
);

export default useStyles;
