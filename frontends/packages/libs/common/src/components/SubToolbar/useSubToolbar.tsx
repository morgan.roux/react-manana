import SubToolbar from './SubToolbar';
import { NewCustomButton } from '@lib/ui-kit';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React from 'react';
import { useHistory } from 'react-router';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    childrenRoot: {
      display: 'flex',
      '& > button:not(:nth-last-child(1))': {
        marginRight: 15,
      },
      '& .main-MuiButtonBase-root': {
        padding: '11px 34px !important',
      },
    },
    customButton: {
      padding: '11px 34px !important',
      color: `${theme.palette.common.white} !important`,
      backgroundColor: `${theme.palette.primary.main} !important`,
    },
  })
);

const useSubToolbar = (props: any) => {
  const { selected, url, title, addBtnLabel, withBackground, upsert, upsertBtnLabel, onClickBack, onDelete } = props;

  const { push } = useHistory();

  const classes = useStyles();

  const handleDelete = () => {
    onDelete && onDelete();
  };

  const CREATE_LINK = url ? `/db/${url}/create` : null;

  const gotToAdd = () => CREATE_LINK && push(CREATE_LINK);

  const children = (
    <div className={classes.childrenRoot}>
      {selected && selected.length > 0 && (
        <NewCustomButton color="primary" onClick={() => handleDelete()} className={classes.customButton}>
          Supprimer la sélection
        </NewCustomButton>
      )}
      {addBtnLabel && (
        <NewCustomButton color="secondary" startIcon={<Add />} onClick={gotToAdd} className={classes.customButton}>
          {addBtnLabel || 'Ajouter'}
        </NewCustomButton>
      )}
      {upsert && upsertBtnLabel && (
        <NewCustomButton color="secondary" onClick={upsert}>
          {upsertBtnLabel}
        </NewCustomButton>
      )}
    </div>
  );

  return (
    <SubToolbar
      {...{ children }}
      title={title}
      withBackBtn={onClickBack ? true : false}
      onClickBack={onClickBack}
      dark={withBackground}
    />
  );
};
export default useSubToolbar;
