import PharmacieSelectionTable from '@app/basis/src/components/PharmacieSelectionTable';
import { useApplicationContext } from '@lib/common';
import { ConfirmDialog, CustomFullScreenModal } from '@lib/ui-kit';
import { Typography } from '@material-ui/core';
import React, { Dispatch, FC, SetStateAction, useState } from 'react';
import { setPharmacie } from '../../auth/auth-util';
import useStyles from './styles';

interface ChangePharmacieModalProps {
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
}

const ChangePharmacieModal: FC<ChangePharmacieModalProps> = ({ open, setOpen }) => {
  const classes = useStyles({});
  const { currentGroupement: currentPharmacie, notify } = useApplicationContext();
  const [openConfirm, setOpenConfirm] = useState<boolean>(false);
  const [clickedPharmacie, setClickedPharmacie] = useState<any>(null);

  const handleSubmit = () => {
    const pharmacie = {
      cip: clickedPharmacie.cip,
      nom: clickedPharmacie.nom,
      id: clickedPharmacie.id,
      ville: clickedPharmacie.ville,
      departement: clickedPharmacie.departement,
      idGroupement: clickedPharmacie.idGroupement,
      __typename: 'Pharmacie',
    };
    setPharmacie(pharmacie);
    setOpenConfirm(false);
    setOpen(false);
    notify({
      type: 'success',
      message: 'Changement de pharmacie réussi',
    });
    window.location.reload();
  };

  const handleClickRow = (row: any) => {
    if (row) {
      setClickedPharmacie(row);
      setOpenConfirm(true);
    }
  };

  const closeConfirm = () => {
    setOpenConfirm(false);
    setClickedPharmacie(null);
  };

  const ConfirmDialogContent = () => {
    return <Typography style={{ marginBottom: 20 }}>Êtes-vous sur de vouloir changer de pharmacie ?</Typography>;
  };

  return (
    <CustomFullScreenModal open={open} setOpen={setOpen} title="Changer de pharmacie" withBtnsActions={false}>
      <div className={classes.container}>
        <PharmacieSelectionTable activeRow={currentPharmacie} onClickRow={handleClickRow} />
      </div>
      <ConfirmDialog
        open={openConfirm}
        message={<ConfirmDialogContent />}
        handleClose={closeConfirm}
        handleValidate={handleSubmit}
      />
    </CustomFullScreenModal>
  );
};

export default ChangePharmacieModal;
