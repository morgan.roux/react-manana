import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listRoot: {
      overflowY: 'auto',
      maxHeight: 'calc(100vh - 156px)',
      '& .MuiTableRow-head': {
        '& > th:nth-child(6), & > th:nth-child(7)': {
          minWidth: 116,
        },
        '& > th:nth-child(10), & > th:nth-child(11)': {
          minWidth: 128,
        },
      },
    },
    searchInputBox: {
      padding: '20px 0px 0px 20px',
      display: 'flex',
    },
    tableCell: {
      '& .main-MuiTableCell-root': {
        padding: '6px 12px 6px 12px !important',
      },
    },
    container: {
      width: '100%',
      //minHeight: 'calc(100vh - 64px)',
      display: 'flex',
      flexDirection: 'column',
      padding: '30px 25px',
      // '& > div:nth-child(1)': {
      //   maxWidth: 500,
      //   marginBottom: 25,
      //   '& > div': {
      //     height: 50,
      //   },
      // },
      // '& > div:nth-child(2)': {
      //   '& > div': {
      //     padding: 0,
      //     '& tbody > tr.MuiTableRow-root > td': {
      //       // height: 50,
      //       padding: '14px 12px',
      //     },
      //   },
      // },
    },
  })
);

export default useStyles;
