import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import ReactQuillLabel from '../ReactQuillLabel';
import { Editor, EditorTools } from '@progress/kendo-react-editor';
import ReactQuill from 'react-quill';

interface RichTextInputProps {
  label?: string;
  required?: boolean;
  className?: string;
  readOnly?: boolean;

  value: string;
  onChange: (value: string) => void;
}

export const RichTextInput: FC<RichTextInputProps> = ({ label, required, className, readOnly, value, onChange }) => {
  const editor = (
    <ReactQuill
      modules={{
        toolbar: [
          [{ header: [1, 2, false] }],
          ['bold', 'italic', 'underline', 'strike', 'blockquote'],
          [{ list: 'ordered' }, { list: 'bullet' }, { indent: '-1' }, { indent: '+1' }],
          ['link', 'image'],
          ['clean'],
        ],
      }}
      readOnly={readOnly}
      className="customized-title"
      theme="snow"
      value={value}
      onChange={onChange}
    />
  );
  return label ? (
    <Box className={className}>
      <ReactQuillLabel label={label} required={required} />
      {editor}
    </Box>
  ) : (
    editor
  );
};
