import React, { FC, useState } from 'react';
import { Box, IconButton, Tooltip } from '@material-ui/core';
import ReplayIcon from '@material-ui/icons/Replay';
import useStyles from './styles';
import SortIcon from '@material-ui/icons/Sort';
import { MoreVert } from '@material-ui/icons';

interface CenteredSortProps {}
const CenteredSort: FC<CenteredSortProps> = () => {
  const classes = useStyles({});

  const [order, setOrder] = useState<'asc' | 'desc'>('asc');

  const handleResetFilter = () => {};

  const handleChangeDirection = () => {
    setOrder((prevState) => (prevState === 'asc' ? 'desc' : 'asc'));
  };

  return (
    <Box display="flex" justifyContent="space-between" alignItems="center">
      <IconButton size="small" onClick={handleResetFilter}>
        <Tooltip title="Réinitialiser les filtres">
          <ReplayIcon />
        </Tooltip>
      </IconButton>
      <IconButton size="small" onClick={handleChangeDirection}>
        {order === 'asc' ? <SortIcon /> : <SortIcon className={classes.desc} />}
      </IconButton>
      <IconButton size="small">
        <MoreVert />
      </IconButton>
    </Box>
  );
};
export default CenteredSort;
