import React, { FC, ReactNode } from 'react';
import { Box } from '@material-ui/core';
import { RouteComponentProps, useHistory, withRouter } from 'react-router-dom';
// import useStyles from './styles';
import { CustomButton, NewCustomButton } from '@lib/ui-kit';
import SearchContent from '../Filter/SearchContent';
import CenteredSort from './CenteredSort';
import { useApplicationContext } from '../../context';
export interface SideMenuButtonProps {
  button?: SideMenuButtonInterface;
  centeredSearch?: boolean;
}
export interface SideMenuButtonInterface {
  url: string;
  text: string;
  color: string;
  icon?: ReactNode;
  authorized?: string[];
  userIsAuthorized?: boolean;
}

const SideMenuButton: FC<SideMenuButtonProps> = ({ button, centeredSearch }) => {
  const { user } = useApplicationContext();
  const { push } = useHistory();
  const userRole = user.role?.code;

  const goToUrl = () => {
    if (button && button.url) {
      push(button.url);
    }
  };

  const isAutorized = (): boolean => {
    if (button && button.authorized && userRole) {
      if (button.authorized.includes(userRole) || button.userIsAuthorized === true) {
        return true;
      }
    }
    return false;
  };

  if (!isAutorized()) {
    return null;
  }

  if (centeredSearch) {
    return (
      <Box display="flex">
        <SearchContent placeholder="Rechercher..." />
        <CenteredSort />
      </Box>
    );
  }

  return (
    <Box display="flex" justifyContent="center" marginTop="24px" paddingBottom="16px" borderBottom="1px solid #E5E5E5">
      {button && (
        <NewCustomButton onClick={goToUrl} startIcon={button.icon}>
          {button.text}
        </NewCustomButton>
      )}
    </Box>
  );
};

export default SideMenuButton;
