import { Box, Checkbox, List, ListItem, ListItemText, Typography } from '@material-ui/core';
import { Error } from '@material-ui/icons';
import React, { FC } from 'react';
import { useStyles } from './styles';
export interface ImportanceInterface {
  checked: boolean;
  __typename?: 'Importance';
  id?: string;
  ordre?: number;
  libelle?: string;
  couleur?: string;
}

interface ImportanceFilterListProps {
  importances: ImportanceInterface[] | null | undefined;
  setImportances: (importance: ImportanceInterface[] | null | undefined) => void;
}

const ImportanceFilterList: FC<ImportanceFilterListProps> = ({ importances, setImportances }) => {
  const classes = useStyles({});

  const handleChange = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, item: ImportanceInterface) => {
    setImportances(
      (importances || []).map((currentItem) => {
        if (currentItem.id === item.id) {
          return { ...currentItem, checked: !currentItem.checked };
        }
        return currentItem;
      })
    );
  };

  return (
    <List>
      {(importances || []).map(
        (item, index) =>
          item && (
            <ListItem
              dense={true}
              button={true}
              key={`${item.id}-${item.checked}`}
              onClick={(event) => handleChange(event, item)}
            >
              <Box display="flex" alignItems="center">
                <Checkbox checked={item.checked} tabIndex={-1} disableRipple={true} />
                <ListItemText
                  primary={
                    <Box display="flex" alignItems="center">
                      <Error style={{ color: item.couleur || 'black' }} />
                      <Typography className={classes.title} style={{ color: item.couleur || 'black' }}>
                        {item.libelle}
                      </Typography>
                    </Box>
                  }
                />
              </Box>
            </ListItem>
          )
      )}
    </List>
  );
};

export default ImportanceFilterList;
