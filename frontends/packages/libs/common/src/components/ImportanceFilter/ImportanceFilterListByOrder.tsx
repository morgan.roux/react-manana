import { Box, Checkbox, List, ListItem, ListItemText, Typography } from '@material-ui/core';
import { Error } from '@material-ui/icons';
import React, { FC } from 'react';
import { useApplicationContext } from '@lib/common';
import { useImportancesQuery } from '@lib/common/src/federation';
import { LoaderSmall } from '../../../../ui-kit/src';
import { useStyles } from './styles';

interface ImportanceFilterListByOrderProps {
  ordres?: number[];
  setImportances: (importances: any) => void;
}

const ImportanceFilterListByOrder: FC<ImportanceFilterListByOrderProps> = ({ ordres, setImportances }) => {
  const classes = useStyles({});
  const { federation } = useApplicationContext();
  const loadingImportances = useImportancesQuery({ client: federation });

  const isChecked = (el: any) => {
    return (ordres || []).some((ordre) => ordre === el.ordre);
  };

  const handleChange = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, el: any) => {
    setImportances(
      (ordres || []).includes(el.ordre)
        ? ordres?.filter((ordre) => ordre !== el.ordre)
        : (ordres || []).concat(el.ordre)
    );
  };

  if (loadingImportances.loading) {
    return <LoaderSmall />;
  }

  return (
    <List>
      {loadingImportances.data?.importances.nodes?.map((importance) => (
        <ListItem
          dense={true}
          button={true}
          key={`importance-filter-${importance.id}`}
          onClick={(event) => handleChange(event, importance)}
        >
          <Box display="flex" alignItems="center">
            <Checkbox checked={isChecked(importance)} tabIndex={-1} disableRipple={true} />
            <ListItemText
              primary={
                <Box display="flex" alignItems="center">
                  <Error style={{ color: importance.couleur || 'black' }} />
                  <Typography className={classes.title} style={{ color: importance.couleur || 'black' }}>
                    {importance.libelle}
                  </Typography>
                </Box>
              }
            />
          </Box>
          {/*
                      <Box>
                        <Typography className={classes.nbrProduits}>{item.count}</Typography>
                      </Box>
                    */}
        </ListItem>
      ))}
    </List>
  );
};

export default ImportanceFilterListByOrder;
