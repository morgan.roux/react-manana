import { CustomAvatar, CustomAvatarGroup, NewCustomButton } from '@lib/ui-kit';
import { Box, IconButton, List, ListItem, ListItemIcon, ListItemText, Menu, Typography } from '@material-ui/core';
import React, { FC, ReactNode } from 'react';
import CloseIcon from '@material-ui/icons/Close';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import { Event, MoreHoriz } from '@material-ui/icons';
import { useStyles } from './styles';
import moment from 'moment';
import { ActionMenuButton, AttachedFiles, Comment, CommentList, useApplicationContext, UserAction } from '@lib/common';
import {
  useCreate_Get_Presigned_UrlMutation,
  useSmyleysQuery,
  useUser_SmyleysLazyQuery,
  useUser_SmyleysQuery,
} from '@lib/common/src/graphql';

interface PageDetailDocumentProps {
  page?: string;
  data?: any;
  onClickCloturer?: (data?: any, id?: string) => void;
  onClickNext?: (currentID: string) => void;
  onClickPrev?: (currentID: string) => void;
  dateLabelDetail?: string;
  typeLabelDetail?: string;
  causeLabelDetail?: string;
  origineLabelDetail?: string;
  correspondantLabelDetail?: string;
  impactLabelDetail?: string;
  descriptionLabelDetail?: string;
  titleOrigineDetail?: string;
  refetchComments?: () => void;
  refetchSmyleys?: () => void;
  activeDocument?: any;
  codeItemComment?: string;
  idSourceComment?: string;
  tableSuiviReunion?: ReactNode;
  origine?: any;
  labelAddMenuAction?: string;
  handleAddMenuAction?: () => void;
  handleVoirDetailMenuAction?: (data?: string) => void;
  handleEditMenuAction?: (data?: any, id?: string) => void;
  handleDeleteMenuAction?: (data?: any, id?: string) => void;
  handleApporterSolutionMenuAction?: (data?: any, id?: string) => void;
  commentaires?: {
    total: number;
    data: any[];
    loading: boolean;
  };
  userSmyleys?: {
    data: any[];
    loading: boolean;
  };
}
export const PageDetailDocument: FC<PageDetailDocumentProps> = ({
  page,
  data,
  onClickCloturer,
  onClickNext,
  onClickPrev,
  causeLabelDetail,
  correspondantLabelDetail,
  dateLabelDetail,
  descriptionLabelDetail,
  impactLabelDetail,
  origineLabelDetail,
  typeLabelDetail,
  titleOrigineDetail,
  refetchComments,
  refetchSmyleys,
  activeDocument,
  codeItemComment,
  idSourceComment,
  tableSuiviReunion,
  origine,
  handleVoirDetailMenuAction,
  handleAddMenuAction,
  labelAddMenuAction,
  handleEditMenuAction,
  handleDeleteMenuAction,
  handleApporterSolutionMenuAction,
  commentaires,
  userSmyleys,
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClickMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const {
    isMobile,
    user,
    federation,
    graphql,
    currentGroupement,
    notify,
    auth,
    useParameterValueAsBoolean,
    useParameterValueAsString,
  } = useApplicationContext();

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };
  const handleClickCloturer = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    onClickCloturer && onClickCloturer(data, data.id);
  };
  const handleClickNext = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    onClickNext && onClickNext(data.id);
  };
  const handleClickPrev = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    onClickPrev && onClickPrev(data.id);
  };
  const Importance = (importance?: any) => {
    return (
      <>
        {importance ? (
          <span
            style={{
              background: importance.couleur,
              color: importance.ordre === 1 ? '#fff' : '#000',
              padding: 5,
              borderRadius: 3,
            }}
          >
            {importance.ordre === 1 ? 'Fort' : 'Faible'}
          </span>
        ) : null}
      </>
    );
  };
  const findOrigine = (id?: string): any | undefined => {
    if (!id) {
      return undefined;
    }

    return (origine?.data || []).find((o: any) => o.id === id);
  };
  const [doCreateGetPresignedUrls, doCreateGetPresignedUrlsResult] = useCreate_Get_Presigned_UrlMutation({
    client: graphql,
  });
  const getSmyleys = useSmyleysQuery({
    client: graphql,
    variables: {
      idGroupement: currentGroupement.id,
    },
    onCompleted: (smyleysResult) => {
      if (smyleysResult.smyleys) {
        const filePaths: string[] = smyleysResult.smyleys
          .filter((item) => !!item?.photo)
          .map((item: any) => item.photo);
        if (filePaths.length > 0) {
          doCreateGetPresignedUrls({ variables: { filePaths } });
        }
      }
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  const smyleys = getSmyleys?.data?.smyleys || [];
  const presignedUrls = doCreateGetPresignedUrlsResult?.data?.createGetPresignedUrls || [];

  return (
    <Box px={3}>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        {data.statut?.code !== 'CLOTURE' && page !== 'reunion' && (
          <Box>
            <NewCustomButton startIcon={<CloseIcon />} onClick={handleClickCloturer}>
              Clôturer
            </NewCustomButton>
          </Box>
        )}
        <Box marginLeft={data.statut?.code === 'CLOTURE' || page === 'reunion' ? 'auto' : ''}>
          <IconButton onClick={handleClickPrev}>
            <NavigateBeforeIcon />
          </IconButton>
          <IconButton>
            <NavigateNextIcon onClick={handleClickNext} />
          </IconButton>
          <ActionMenuButton
            withVoirDetail={false}
            page={page}
            data={data}
            handleAddMenuAction={handleAddMenuAction}
            handleDeleteMenuAction={handleDeleteMenuAction}
            handleEditMenuAction={handleEditMenuAction}
            handleVoirDetailMenuAction={handleVoirDetailMenuAction}
            labelAddMenuAction={labelAddMenuAction}
            handleApporterSolutionMenuAction={handleApporterSolutionMenuAction}
          />
        </Box>
      </Box>
      <Box>
        <Box>
          <Typography className={classes.subtitle}>{dateLabelDetail || 'Date incident'}</Typography>
          <Typography style={{ display: 'flex', alignItems: 'center' }}>
            <Event />{' '}
            <span style={{ marginLeft: 15 }}>
              {data?.dateIncident
                ? moment(data?.dateIncident).format('DD/MM/YYYY')
                : data?.dateAmelioration
                ? moment(data?.dateAmelioration).format('DD/MM/YYYY')
                : data?.dateAction
                ? moment(data?.dateAction).format('DD/MM/YYYY')
                : data?.dateReunion
                ? moment(data?.dateReunion).format('DD/MM/YYYY')
                : ''}
            </span>
          </Typography>
        </Box>
        {page !== 'reunion' && (
          <Box display="flex">
            <Box width="50%">
              <Typography className={classes.subtitle}>{typeLabelDetail || 'Type incident'}</Typography>
              <Typography>{data?.type?.libelle || '-'}</Typography>
            </Box>
            <Box width="50%">
              <Typography className={classes.subtitle}>{causeLabelDetail || 'Cause incident'}</Typography>
              <Typography>{data?.cause?.libelle}</Typography>
            </Box>
          </Box>
        )}
        <Typography className={classes.titleOrigine}>
          {titleOrigineDetail || "Qui est à l'origine de cet incident ?"}
        </Typography>
        <Box display="flex">
          <Box width="50%">
            <Typography className={classes.subtitle}>{origineLabelDetail || 'Origine'}</Typography>
            <Box display="flex" alignItems="center">
              {page === 'reunion' ? (
                <>
                  <CustomAvatar url={data?.animateur?.photo?.publicUrl} name={data?.animateur?.fullName} />
                  <Typography style={{ marginLeft: 15 }}>{data?.animateur?.fullName || '-'}</Typography>
                </>
              ) : (
                <Typography>{findOrigine(data?.origine?.id)?.libelle || '-'}</Typography>
              )}
            </Box>
          </Box>
          <Box width="50%">
            <Typography className={classes.subtitle}>{correspondantLabelDetail || 'Correspondant'}</Typography>
            <Box display="flex" alignItems="center">
              {data?.origineAssocie?.dataType === 'User' ? (
                <>
                  <CustomAvatar url={data?.origineAssocie?.photo?.publicUrl} name={data?.origineAssocie?.fullName} />
                  <Typography style={{ marginLeft: 15 }}>{data?.origineAssocie?.fullName || '-'}</Typography>
                </>
              ) : data?.origineAssocie?.dataType === 'Laboratoire' ? (
                <>
                  <CustomAvatar url={data?.origineAssocie?.photo?.publicUrl} name={data?.origineAssocie?.nom} />
                  <Typography style={{ marginLeft: 15 }}>{data?.origineAssocie?.nom || '-'}</Typography>
                </>
              ) : data?.responsable ? (
                <>
                  <CustomAvatar url={data?.responsable?.photo?.publicUrl} name={data?.responsable?.fullName} />
                  <Typography style={{ marginLeft: 15 }}>{data?.responsable?.fullName || '-'}</Typography>
                </>
              ) : (
                '-'
              )}
            </Box>
          </Box>
        </Box>
        {page !== 'reunion' && (
          <Box display="flex" alignItems="center">
            <Typography className={classes.subtitle} style={{ marginRight: 30 }}>
              {impactLabelDetail || 'Impact'}
            </Typography>
            <Typography style={{ marginTop: 25 }}>{Importance(data?.importance)}</Typography>
          </Box>
        )}
        {tableSuiviReunion && <Box my={2}>{tableSuiviReunion}</Box>}
        <Box>
          <Typography className={classes.subtitle}>{descriptionLabelDetail || 'Description'}</Typography>
          <Typography dangerouslySetInnerHTML={{ __html: data?.description }} />
        </Box>
        {(data?.fichiers?.length || 0) > 0 ? (
          <Box>
            <Typography className={classes.subtitle}>Fichier(s) joint(s)</Typography>
            <AttachedFiles files={data.fichiers} />
          </Box>
        ) : null}

        <Box>
          <UserAction
            refetchSmyleys={refetchSmyleys}
            codeItem={codeItemComment || 'FAKE'}
            idSource={activeDocument?.id}
            nbComment={activeDocument?.nombreCommentaires}
            nbSmyley={activeDocument?.nombreReactions}
            userSmyleys={userSmyleys?.data as any}
            presignedUrls={presignedUrls}
            smyleys={smyleys}
            hidePartage={true}
            hideRecommandation={true}
          />
          <CommentList
            key={`comment_list`}
            total={commentaires?.total || 0}
            comments={commentaires?.data || []}
            loading={commentaires?.loading}
            // fetchMoreComments={() => onRequestFetchMoreComments as any}
          />
          {!commentaires?.loading && idSourceComment && codeItemComment && (
            <Comment
              // forwardedInputRef={commentInputRef as any}
              refetch={refetchComments}
              codeItem={codeItemComment}
              idSource={idSourceComment}
            />
          )}
        </Box>
      </Box>
    </Box>
  );
};
