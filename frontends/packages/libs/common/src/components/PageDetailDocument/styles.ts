import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) => ({
  subtitle: {
    fontWeight: 500,
    marginTop: 25,
  },
  titleOrigine: {
    fontSize: 16,
    fontWeight: 700,
    marginTop: 25,
  },
}));
