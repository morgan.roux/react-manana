import React, { FC, useState } from 'react';
import useStyles from './style';
import { CustomAutocomplete, CustomAvatar } from '@lib/ui-kit';
import { Box, Button, FormControlLabel, Popover, Radio, Typography } from '@material-ui/core';
import GroupIcon from '@material-ui/icons/Group';
import { useParticipants } from '../CustomSelectUser';
import { AutocompleteCloseReason } from '@material-ui/lab';
import { useFilters } from '@lib/common/src/shared';

export interface UserSelectProps {
  withSelectAll?: boolean;
  label?: string;
  loading?: boolean;
  onChange?: (value: any) => void;
  // onSelectUser?: (user: any) => void;
}

const UserSelect: FC<UserSelectProps> = ({ loading, label, onChange, withSelectAll = true }) => {
  const classes = useStyles();

  const { searchText, setSearchText } = useFilters();

  const [selectedValue, setSelectedValue] = useState<any>();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (event: React.ChangeEvent<{}>, reason: AutocompleteCloseReason) => {
    if (reason === 'toggleInput') {
      return;
    }
    if (anchorEl) {
      anchorEl.focus();
    }
    setAnchorEl(null);
    setSearchText('');
  };

  let open = Boolean(anchorEl);
  const id = open ? 'tous-partenaire' : undefined;

  const handleClosePopover = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    setAnchorEl(null);
  };
  const handleChange = (user: any) => {
    setSelectedValue(user);
    setAnchorEl(null);
    onChange && onChange(user?.id === 'TOUT' ? undefined : user);
  };

  const { result } = useParticipants({ category: 'USER' });
  const userOptions = withSelectAll
    ? [
        {
          id: 'TOUT',
          userName: `Toute l'équipe`,
        },
        ...(result?.data?.search?.data || []),
      ]
    : result?.data?.search?.data || [];

  return (
    <React.Fragment>
      <div className={classes.root}>
        <Button
          onClick={handleClick}
          className={classes.button}
          startIcon={
            selectedValue && (selectedValue?.id as any) !== 'TOUT' ? (
              <CustomAvatar url={selectedValue.userPhoto?.fichier?.publicUrl} name={selectedValue?.userName} />
            ) : (
              <GroupIcon />
            )
          }
          style={selectedValue && (selectedValue?.id as any) !== 'TOUT' ? { padding: '0 10px' } : {}}
          classes={{ startIcon: classes.iconFilter }}
        >
          {selectedValue?.userName || "Toute l'équipe"}
        </Button>
      </div>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClosePopover}
        className={classes.popper}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        classes={{
          paper: classes.popoverPaper,
        }}
      >
        {userOptions && (
          <CustomAutocomplete
            open
            onClose={handleClose}
            classes={{
              paper: classes.paper,
              option: classes.option,
              popperDisablePortal: classes.popperDisablePortal,
            }}
            disablePortal
            onChange={handleChange}
            renderTags={() => null}
            noOptionsText="Aucun résultat trouvé"
            loading={loading}
            options={userOptions}
            optionLabelKey="userName"
            value={{ id: '', userName: searchText.data?.searchText.text }}
            onAutocompleteChange={(newValue) => {
              if (newValue?.id) {
                onChange && onChange(newValue.id === 'TOUT' ? undefined : newValue);
              } else {
                setSearchText(newValue?.userName);
              }
            }}
            search={setSearchText}
            label={label}
            required={true}
            disabled={false}
            placeholder="Choisir les participants"
            renderOption={(option) => {
              const checked = !!(
                (selectedValue && selectedValue.id === option.id) ||
                (!selectedValue && option.id === 'TOUT')
              );
              console.log('-------------------------- Option labo -------------------', option);
              return (
                <React.Fragment>
                  <Box onClick={() => handleChange(option)} display="flex" justifyContent="space-between" width="100%">
                    {option.id === 'TOUT' ? (
                      <GroupIcon className={classes.equipeIcon} />
                    ) : (
                      <CustomAvatar
                        name={option.userName || ''}
                        className={classes.avatarLogo}
                        url={option.userPhoto?.fichier?.publicUrl || ''}
                      />
                    )}
                    <FormControlLabel
                      value={checked}
                      label={
                        <Box>
                          <Typography>{option.userName || ''}</Typography>
                          <Typography variant="caption" className={classes.optionCaption}>
                            {option.role?.nom || ''}
                          </Typography>
                          <Typography variant="caption" className={classes.optionCaption}>
                            {option.email || ''}
                          </Typography>
                        </Box>
                      }
                      control={<Radio checked={checked} style={{ padding: 0 }} />}
                      labelPlacement="start"
                      className={classes.formLabel}
                    />
                  </Box>
                </React.Fragment>
              );
            }}
          />
        )}
      </Popover>
    </React.Fragment>
  );
};

export default UserSelect;
