import moment from 'moment';
import _ from 'lodash';

interface QuerySearch {
  selected: any[];
  defaultMustNot: any[];
  queryString: any[];
  setMustNot: (mustNot: any) => void;
  setMust: (must: any) => void;
  setFiltersSelected?: (value: any) => void;
  currentUser: any;
  myPharmacie: any;
}

export const updateQuerySearch = ({
  selected,
  setMustNot,
  setMust,
  defaultMustNot,
  queryString,
  currentUser,
  myPharmacie,
  setFiltersSelected,
}: QuerySearch) => {
  const now = moment().format('YYYY-MM-DD');

  if (selected.includes('PHARMACIE')) {
    if (
      _.isEqual(selected, ['MY_PHARMACIE', 'MY_REGION', 'OTHER_PHARMACIE', 'PHARMACIE']) &&
      currentUser &&
      ((currentUser.userTitulaire && currentUser.userTitulaire.id) ||
        (currentUser.userPpersonnel && currentUser.userPpersonnel.id))
    ) {
      setMustNot(defaultMustNot);
      setMust([
        ...queryString,
        {
          exists: {
            field: 'pharmacie.id',
          },
        },
      ]);
    } else if (
      _.isEqual(selected, ['PHARMACIE', 'MY_PHARMACIE']) &&
      currentUser &&
      ((currentUser.userTitulaire && currentUser.userTitulaire.id) ||
        (currentUser.userPpersonnel && currentUser.userPpersonnel.id)) &&
      myPharmacie &&
      myPharmacie.data &&
      myPharmacie.data.pharmacie &&
      myPharmacie.data.pharmacie.id
    ) {
      setMustNot(defaultMustNot);
      setMust([
        ...queryString,
        {
          term: {
            'pharmacie.id':
              myPharmacie &&
              myPharmacie.data &&
              myPharmacie.data.pharmacie &&
              myPharmacie.data.pharmacie.id,
          },
        },
      ]);
    } else if (
      _.isEqual(selected, ['PHARMACIE', 'MY_REGION']) &&
      currentUser &&
      ((currentUser.userTitulaire && currentUser.userTitulaire.id) ||
        (currentUser.userPpersonnel && currentUser.userPpersonnel.id)) &&
      myPharmacie &&
      myPharmacie.data &&
      myPharmacie.data.pharmacie &&
      myPharmacie.data.pharmacie.departement &&
      myPharmacie.data.pharmacie.departement.region &&
      myPharmacie.data.pharmacie.departement.region.id
    ) {
      setMustNot(defaultMustNot);
      setMust([
        ...queryString,
        {
          term: {
            'pharmacie.departement.region.id':
              myPharmacie &&
              myPharmacie.data &&
              myPharmacie.data.pharmacie &&
              myPharmacie.data.pharmacie.departement &&
              myPharmacie.data.pharmacie.departement.region &&
              myPharmacie.data.pharmacie.departement.region.id,
          },
        },
      ]);
    } else if (
      _.isEqual(selected, ['PHARMACIE', 'OTHER_PHARMACIE']) &&
      currentUser &&
      ((currentUser.userTitulaire && currentUser.userTitulaire.id) ||
        (currentUser.userPpersonnel && currentUser.userPpersonnel.id)) &&
      myPharmacie &&
      myPharmacie.data &&
      myPharmacie.data.pharmacie &&
      myPharmacie.data.pharmacie.id
    ) {
      setMustNot([
        ...defaultMustNot,
        ...[
          {
            term: {
              'pharmacie.id':
                myPharmacie &&
                myPharmacie.data &&
                myPharmacie.data.pharmacie &&
                myPharmacie.data.pharmacie.id,
            },
          },
        ],
      ]);
      setMust([
        ...queryString,
        {
          exists: {
            field: 'pharmacie.id',
          },
        },
      ]);
    } else {
      setMust([
        {
          term: {
            email: '',
          },
        },
      ]);
    }
  } else if (selected.includes('MY_GROUPEMENT')) {
    setMustNot(defaultMustNot);
    setMust([
      ...queryString,
      {
        exists: {
          field: 'userPersonnel.id',
        },
      },
    ]);
  } else if (selected.includes('PRESIDENT')) {
    setMustNot(defaultMustNot);
    setMust([
      ...queryString,
      {
        exists: {
          field: 'userTitulaire.id',
        },
      },
      {
        range: {
          'userTitulaire.titulaire.dateDebut': {
            lte: now,
          },
        },
      },
      {
        range: {
          'userTitulaire.titulaire.dateFin': {
            gte: now,
          },
        },
      },
    ]);
  } else if (selected.includes('LABORATOIRE')) {
    setMustNot(defaultMustNot);
    setMust([
      ...queryString,
      {
        exists: {
          field: 'userLaboratoire.id',
        },
      },
    ]);
  } else if (selected.includes('PARTENAIRE_SERVICE')) {
    setMustNot(defaultMustNot);
    setMust([
      ...queryString,
      {
        exists: {
          field: 'userPartenaire.id',
        },
      },
    ]);
  } else if (selected.includes('ALL')) {
    setMustNot(defaultMustNot);
    setMust([...queryString]);
  } else if (selected.includes('CLT')) {
    setMustNot(defaultMustNot);
    setMust([
      ...queryString,
      {
        term: {
          'role.code': 'CLT',
        },
      },
    ]);
  } else {
    setMust([
      {
        term: {
          email: '',
        },
      },
    ]);
  }
  if (setFiltersSelected) setFiltersSelected(selected && selected.length ? [...selected] : []);
};
