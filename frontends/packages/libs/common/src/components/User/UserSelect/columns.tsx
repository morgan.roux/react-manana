import React from 'react';
//import { groupementRoles, pharmacieAllRoles } from '../../../Constant/roles';
//import CustomAvatar from '../../Common/CustomAvatar';
import { Column } from '../../Table/interfaces';

export const getUserColumns = (): Column[] => {
  return [];
  /*return [
    {
      name: '',
      label: 'Profil',
      renderer: (value: any) => {
        const valueUser =
          value.users &&
          value.pharmacies &&
          value.pharmacies.length === 1 &&
          value.users.length === 1
            ? value.users[0]
            : null;
        return (
          <CustomAvatar
            name={value && value.fullName}
            url={
              valueUser &&
              valueUser.userPhoto &&
              valueUser.userPhoto.fichier &&
              valueUser.userPhoto.fichier.publicUrl
            }
          />
        );
      },
    },
    {
      name: 'userName',
      label: 'Nom et Prénom',
    },
    {
      name: 'role.code',
      label: 'Organisme',
      renderer: (value: any) => {
        return value && value.role && value.role.code ? getOrigin(value.role.code) : '-';
      },
    },
    {
      name: 'role.nom',
      label: 'Fonction',
      renderer: (value: any) => {
        return value && value.role && value.role.nom ? value.role.nom : '-';
      },
    },
    {
      name: 'contact',
      label: 'Téléphone',
      renderer: (value: any) => {
        return value && value.contact
          ? value.contact.telPerso
            ? value.contact.telPerso
            : value.phoneNumber
          : value.phoneNumber
          ? value.phoneNumber
          : '-';
      },
    },
    {
      name: 'email',
      label: 'email',
    },
  ];*/
};

export const getOrigin = (code: string) => {
  /* if (pharmacieAllRoles.includes(code)) {
    return 'PHARMACIE';
  } else if (groupementRoles.includes(code)) {
    return 'GROUPEMENT';
  } else if (code === 'PRTSERVICE') {
    return 'PRESTATAIRE DE SERVICE';
  } else if (code === 'PRTLAB') {
    return 'LABORATOIRE';
  } else if (code === 'PRTLAB' || code === 'PRDREGION') {
    return 'REGION';
  } else if (code === 'CLT') {
    return 'CLIENT';
  } else {
    return '';
  }*/
  return '';
};

export const columnsFilter = (
  isDisabled?: (code: string) => boolean,
  disableAllFilter?: boolean,
  activeAllContact?: boolean
) => [
  {
    id: 1,
    name: 'Tous les contacts',
    code: 'ALL',
    open: false,
    disable: activeAllContact ? false : disableAllFilter ? true : false,
  },
  {
    id: 2,
    name: 'Pharmacie',
    code: 'PHARMACIE',
    open: true,
    disable: disableAllFilter ? true : false,
    childList: [
      {
        id: 3,
        name: 'Ma pharmacie',
        code: 'MY_PHARMACIE',
        disable: disableAllFilter ? true : false,
      },
      {
        id: 4,
        name: 'Ma région',
        code: 'MY_REGION',
        disable: disableAllFilter ? true : isDisabled ? isDisabled('MY_REGION') : false,
      },
      {
        id: 5,
        name: 'Autres pharmacies',
        code: 'OTHER_PHARMACIE',
        disable: disableAllFilter ? true : isDisabled ? isDisabled('OTHER_PHARMACIE') : false,
      },
    ],
  },
  {
    id: 6,
    name: 'Groupement',
    code: 'MY_GROUPEMENT',
    open: false,
    disable: disableAllFilter ? true : isDisabled ? isDisabled('MY_GROUPEMENT') : false,
  },
  {
    id: 7,
    name: 'Présidents de régions',
    code: 'PRESIDENT',
    open: false,
    disable: disableAllFilter ? true : false,
  },
  {
    id: 8,
    name: 'Laboratoires partenaires',
    code: 'LABORATOIRE',
    open: false,
    disable: disableAllFilter ? true : isDisabled ? isDisabled('LABORATOIRE') : false,
  },
  {
    id: 9,
    name: 'Partenaires de services',
    code: 'PARTENAIRE_SERVICE',
    open: false,
    disable: disableAllFilter ? true : isDisabled ? isDisabled('PARTENAIRE_SERVICE') : false,
  },
];

export const columnsFilterWithContact = (
  isDisabled?: (code: string) => boolean,
  disableAllFilter?: boolean,
  activeAllContact?: boolean
) => [
  {
    id: 1,
    name: 'Tous les contacts',
    code: 'ALL',
    open: false,
    disable: activeAllContact ? false : disableAllFilter ? true : false,
  },
  {
    id: 2,
    name: 'Pharmacie',
    code: 'PHARMACIE',
    open: true,
    disable: disableAllFilter ? true : false,
    childList: [
      {
        id: 3,
        name: 'Ma pharmacie',
        code: 'MY_PHARMACIE',
        disable: disableAllFilter ? true : false,
      },
      {
        id: 4,
        name: 'Ma région',
        code: 'MY_REGION',
        disable: disableAllFilter ? true : isDisabled ? isDisabled('MY_REGION') : false,
      },
      {
        id: 5,
        name: 'Autres pharmacies',
        code: 'OTHER_PHARMACIE',
        disable: disableAllFilter ? true : isDisabled ? isDisabled('OTHER_PHARMACIE') : false,
      },
    ],
  },
  {
    id: 6,
    name: 'Groupement',
    code: 'MY_GROUPEMENT',
    open: false,
    disable: disableAllFilter ? true : isDisabled ? isDisabled('MY_GROUPEMENT') : false,
  },
  {
    id: 7,
    name: 'Présidents de régions',
    code: 'PRESIDENT',
    open: false,
    disable: disableAllFilter ? true : false,
  },
  {
    id: 8,
    name: 'Laboratoires partenaires',
    code: 'LABORATOIRE',
    open: false,
    disable: disableAllFilter ? true : isDisabled ? isDisabled('LABORATOIRE') : false,
  },
  {
    id: 9,
    name: 'Partenaires de services',
    code: 'PARTENAIRE_SERVICE',
    open: false,
    disable: disableAllFilter ? true : isDisabled ? isDisabled('PARTENAIRE_SERVICE') : false,
  },
  {
    id: 10,
    name: 'Clients',
    code: 'CLT',
    open: false,
  },
];
