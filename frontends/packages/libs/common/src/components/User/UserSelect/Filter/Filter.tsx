import React, { useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { useStyles } from '../styles';
import { Checkbox, Collapse, ListItem, ListItemText, List } from '@material-ui/core';
import { Fragment } from 'react';
import { ExpandLess, ExpandMore } from '@material-ui/icons';

interface filterColumns {
  name: string;
  code: string;
  open?: boolean;
  disable?: boolean;
  childList?: filterColumns[] | null;
}

interface FilterProps {
  filterColumns: filterColumns[];
  selected: any;
  handleChecked: (value: any) => any;
}

const Filter: React.FC<RouteComponentProps & FilterProps> = ({
  filterColumns,
  selected,
  handleChecked,
}) => {
  const classes = useStyles({});
  const [expands, setExpands] = useState<any[]>({
    ...((filterColumns && filterColumns.length && filterColumns.map((d, index) => d.open)) || []),
  });

  const handleExpand = (index: number) => () => {
    setExpands(prev => ({ ...prev, [index]: !expands[index] }));
  };

  return (
    <List component="nav" aria-labelledby="nested-list-subheader">
      {filterColumns &&
        filterColumns.length &&
        filterColumns.map((d, index) => {
          return (
            <Fragment key={index}>
              <ListItem button={true}>
                <Checkbox
                  color="primary"
                  checked={selected.includes(d.code)}
                  onClick={() => handleChecked(d.code)}
                />

                <ListItemText primary={d.name} />
                {d.childList && d.childList.length > 0 && (
                  <>
                    {expands[index] ? (
                      <ExpandMore onClick={handleExpand(index)} />
                    ) : (
                      <ExpandLess onClick={handleExpand(index)} />
                    )}
                  </>
                )}
              </ListItem>
              {d.childList && d.childList.length > 0 && (
                <Collapse in={expands[index]} timeout="auto" unmountOnExit={true}>
                  <List component="div">
                    {d.childList &&
                      d.childList.map((item, ind) => {
                        return (
                          <>
                            <ListItem button={true} style={{ marginLeft: `${index * 16}px` }}>
                              <Checkbox
                                color="primary"
                                checked={selected.includes(item.code)}
                                onClick={() => handleChecked(item.code)}
                              />
                              <ListItemText primary={item.name} />
                            </ListItem>
                          </>
                        );
                      })}
                  </List>
                </Collapse>
              )}
            </Fragment>
          );
        })}
    </List>
  );
};

export default withRouter(Filter);
