import { createStyles, Theme } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    searchInputBox: {
      width: '100%',
      marginBottom: 12,
    },
    container: {
      width: '100%',
      '& .MuiTableRow-head': {
        '& > th:nth-child(6), & > th:nth-child(7)': {
          minWidth: 116,
        },
        '& > th:nth-child(10), & > th:nth-child(11)': {
          minWidth: 128,
        },
      },
    },
  })
);
