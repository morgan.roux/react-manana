import { createStyles, Theme } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    largeurDroite: {
      width: 750,
    },
    espace: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
    },
    largeur: {
      width: 290,
      height: 'calc(100vh - 214px)',
      marginRight: 16,
      borderRight: '1px solid #E0E0E0',
      overflowY: 'auto',
      overflowX: 'hidden',
      '& .MuiListItem-root': {
        paddingLeft: 0,
        paddingBottom: 2,
        paddingTop: 2,
        '& .MuiCheckbox-root': {
          padding: 0,
          marginRight: 10,
        },
      },
    },
    mainTableUser: {
      width: '100%',
    },
    paddinleft: {
      paddingLeft: -50,
    },
    contactUserFilerContainer: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
  })
);
