import { CustomFormTextField, CustomButton, CustomModal, NewCustomButton } from '@lib/ui-kit';
import { Box, Grid, TextField } from '@material-ui/core';
import { Alert, Autocomplete } from '@material-ui/lab';
import React, { ChangeEvent, FC, useState } from 'react';
import { useApplicationContext } from '../../../context';
import { countries } from './listPays';

export interface OutputDataClient {
  nom: string;
  prenom: string;
  tel: string;
  email: string;
  adresse: string;
  ville: string;
  pays: string;
  cp: number;
}

export interface ModalFormAddClient {
  open: boolean;
  setOpen: () => void;
  onSave: (data: OutputDataClient) => void;
}

export const ModalFormAddClient: FC<ModalFormAddClient> = ({ open, setOpen, onSave }) => {
  const [values, setValues] = useState<any>();
  const [errorSubmit, setErrorSubmit] = useState<boolean>(false);
  const { isMobile } = useApplicationContext();

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [e.currentTarget.name]: e.currentTarget.value });
  };

  const handleSubmit = () => {
    if (
      values &&
      values.nom &&
      values.prenom &&
      values.tel &&
      values.email &&
      values.adresse &&
      values.ville &&
      values.pays &&
      values.cp
    ) {
      onSave(values as OutputDataClient);
    } else {
      setErrorSubmit(true);
    }
  };

  const countryToFlag = (isoCode: string) => {
    return typeof String.fromCodePoint !== 'undefined'
      ? isoCode.toUpperCase().replace(/./g, (char) => String.fromCodePoint(char.charCodeAt(0) + 127397))
      : isoCode;
  };

  const handleChangePays = (__e: any, value: any) => {
    setValues({ ...values, pays: value.label });
  };

  return (
    <CustomModal
      title="Ajout d'un client"
      open={open}
      setOpen={setOpen}
      withBtnsActions={false}
      closeIcon
      headerWithBgColor
      maxWidth="lg"
      fullScreen={isMobile ? true : false}
    >
      <Box maxWidth={600} paddingTop={3} paddingBottom={3}>
        {/*errorSubmit && (
          <Alert style={{ marginBottom: 16 }} severity="error">
            Veuillez bien remplir le formulaire!
          </Alert>
        )*/}
        <Grid container spacing={2}>
          <Grid item xs={12} md={6} lg={6}>
            <CustomFormTextField
              onChange={handleChange}
              name="nom"
              value={values?.nom}
              label="Nom"
              required
              type="text"
            />
          </Grid>
          <Grid item xs={12} md={6} lg={6}>
            <CustomFormTextField
              onChange={handleChange}
              name="prenom"
              value={values?.prenom}
              label="Prénom"
              required
              type="text"
            />
          </Grid>
          <Grid item xs={12} md={6} lg={6}>
            <CustomFormTextField
              onChange={handleChange}
              name="tel"
              value={values?.tel}
              label="Télephone"
              required
              type="text"
            />
          </Grid>
          <Grid item xs={12} md={6} lg={6}>
            <CustomFormTextField
              onChange={handleChange}
              name="email"
              value={values?.email}
              label="Email"
              required
              type="email"
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <CustomFormTextField
              onChange={handleChange}
              name="adresse"
              value={values?.adresse}
              label="Adresse"
              required
              type="text"
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <CustomFormTextField
              onChange={handleChange}
              name="ville"
              value={values?.ville}
              label="Ville"
              required
              type="text"
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <Autocomplete
              id="country-select-demo"
              // style={{ width: '100%' }}
              options={countries}
              autoHighlight
              onChange={handleChangePays}
              getOptionLabel={(option) => option.label}
              renderOption={(option) => (
                <React.Fragment>
                  <span>{countryToFlag(option.code)}</span>&nbsp;
                  {option.label}
                </React.Fragment>
              )}
              renderInput={(params) => (
                <CustomFormTextField
                  {...params}
                  label="Pays"
                  required
                  inputProps={{
                    ...params.inputProps,
                    autoComplete: 'new-password', // disable autocomplete and autofill
                  }}
                />
              )}
            />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <CustomFormTextField
              style={{ width: 100 }}
              onChange={handleChange}
              name="cp"
              value={values?.cp}
              label="Code Postal"
              required
              type="number"
            />
          </Grid>
        </Grid>
        <Box display="flex" justifyContent="flex-end">
          <NewCustomButton onClick={handleSubmit}>AJOUTER</NewCustomButton>
        </Box>
      </Box>
    </CustomModal>
  );
};
