import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    personnelGroupementFormRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      marginTop: 50,
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    inputTitle: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 15,
    },
    imgFormContainer: {
      width: 800,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      '& > div > div': {
        height: 300,
        border: '1px dashed #DCDCDC',
        alignItems: 'flex-start',
        '& img': {
          width: 225,
          height: 225,
          borderRadius: '50%',
        },
      },
    },
    infoPersoContainer: {
      width: 800,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
      marginBottom: 25,
    },
    inputsContainer: {
      border: '1px solid #DCDCDC',
      borderRadius: 12,
      width: '100%',
      '& > div:not(:nth-last-child(1))': {
        borderBottom: '1px solid #DCDCDC',
      },
    },
    formRow: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'row',
      padding: '15px 26px',
      // marginRight: '16px !important',
      '& > div': {
        maxWidth: 390,
        marginBottom: '0px !important',
        // marginRight: '16px',
        '& input, & .MuiSelect-root': {
          // fontWeight: '600',
          fontSize: 16,
          letterSpacing: 0.28,
        },
      },
      '& > div > div': {
        height: 55,
      },
    },
    divider: {
      lastChild: {
        content: '',
      },
    },
    formRowTitle: {
      fontSize: 14,
      fontWeight: 'normal',
    },
    radioGroup: {
      display: 'flex',
      alignItems: 'center',
      '& label, & p': {
        marginRight: 50,
      },
      marginBottom: 15,
    },
    avatarListContainer: {
      width: '100%',
      height: 330,
      border: '1px solid #DCDCDC',
      borderRadius: 12,
      padding: 25,
      marginBottom: 25,
    },
    avatarListFilterContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginBottom: 25,
      border: 'none !important',
      height: 'auto !important',
      '& > div': {
        margin: 0,
      },
      '& > div:nth-child(1)': {
        marginRight: 25,
      },
      '& > div:nth-child(2)': {
        width: 238,
      },
    },
    avatarListImgContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-start',
      marginBottom: 5,
      border: 'none !important',
      height: 'auto !important',
      position: 'relative',
    },
    avatarContainer: {
      width: '110px !important',
      height: 'auto !important',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      '& img': {
        width: '60px !important',
        height: '60px !important',
        borderRadius: '50% !important',
        '&:hover': {
          border: `4px solid ${theme.palette.secondary.main}`,
          cursor: 'pointer',
        },
      },
      '& span': {
        textAlign: 'center',
        fontFamily: 'Montserrat',
        fontSize: 14,
        fontWeight: '600',
        marginTop: 5,
      },
    },
    textMsg: {
      fontWeight: 600,
    },
    selectedAvatar: {
      border: `4px solid ${theme.palette.secondary.main}`,
      cursor: 'pointer',
    },
    permissionAccessSubTite: {
      fontSize: 16,
      fontWeight: 'normal',
      marginBottom: 15,
    },
    permissionAccessFormRow: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      padding: '0px',
      '& > label': {
        margin: '0px !important',
        padding: '10px 5px',
        '& .MuiFormControlLabel-label': {
          fontSize: 16,
          marginLeft: 25,
        },
        '& svg': {
          width: 24,
          height: 24,
        },
      },
      '& > label:not(:nth-last-child(1))': {
        borderBottom: '1px solid #DCDCDC',
      },
      '& *': {
        fontWeight: '600',
        fontFamily: 'Montserrat',
      },
    },
    inputLabel: {
      '& > span': {
        color: 'red',
      },
    },
  })
);

export default useStyles;
