import * as React from 'react';
import useStyles from './styles';
import { Typography } from '@material-ui/core';
import classNames from 'classnames';

interface IInputRowProps {
  title: string;
  required?: boolean;
  divider?: React.ReactNode;
  className?: string;
}

const InputRow: React.FC<IInputRowProps> = (props) => {
  const classes = useStyles({});
  const { title, children, required, divider, className } = props;
  return (
    <>
      <div className={className ? classNames(classes.formRow, className) : classes.formRow}>
        <Typography className={required ? classes.inputLabel : undefined}>
          {title}
          {required ? <span>*</span> : null}
        </Typography>
        {children}
      </div>
      {divider}
    </>
  );
};

export default InputRow;
