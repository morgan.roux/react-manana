import { Box, Button, Drawer, IconButton, Popover, Typography } from '@material-ui/core';
import React, { Dispatch, FC, useState, ReactNode, SetStateAction } from 'react';
import { useStyles } from './styles';
import { DebouncedSearchInput } from '@lib/ui-kit';
import { Search } from '@material-ui/icons';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import classnames from 'classnames';
import { LeftSidebarAndMainPage, PartenaireSelect } from '@lib/common';
import AddIcon from '@material-ui/icons/Add';
import GEDFilter from '../EspaceDocumentaire/GEDFilter';

export interface IDocumentSearch {
  searchText?: string | undefined;
  accesRapide?:
    | 'FAVORIS'
    | 'NOUVEAUX'
    | 'RECENTS'
    | 'PLUS_CONSULTES'
    | 'PLUS_TELECHARGES'
    | 'REFUSES'
    | 'APPROUVES'
    | 'EN_ATTENTE_APPROBATION'
    | 'PAYES'
    | undefined;
  proprietaire?: 'MOI' | 'AUTRE' | 'TOUT';
  idSousCategorie?: string | undefined;
  filterBy?: 'DERNIERE_SEMAINE' | 'DERNIER_MOIS' | 'TOUT' | '12_DERNIERS_MOIS' | undefined;
  sortBy?: string;
  sortDirection?: 'ASC' | 'DESC' | undefined;
  libelleSousEspace?: string;

  type?: string;
  validation?: 'INTERNE' | 'PRESTATAIRE_SERVICE' | 'TOUT';
  idOrigine?: string;
  idOrigineAssocie?: string;
  dateDebut?: string;
  dateFin?: string;
  montantMinimal?: number;
  montantMaximal?: number;
}

interface UseMobileTopSectionProps {
  topbarTitre?: string;
  searchValue?: any;
  filterIcon?: ReactNode;
  withFilter?: boolean;
  useMobileClassName?: boolean;
  laboFilterIcon?: any;
  partenaireSelectValue?: any;
  withButtonAjouter?: boolean;
  onClick?: () => void;
  factureLaboratoires?: any[];
  facturesRejeteesInterneCategories?: { annee: number; mois: number }[];
  facturesRejeteesPrestataireServiceCategories?: { annee: number; mois: number }[];
  searchVariables: IDocumentSearch;
  setSearchVariables: Dispatch<SetStateAction<IDocumentSearch>>;
  categories: {
    loading: boolean;
    error: Error;
    data: any[];
  };
  onGoback: () => void;
  onRequestRefresh: () => void;
  espaceType?: string;
  contratFournisseurs?: any[];
  facturePartenaires?: any[];
  maxTTC?: number;
}

export const UseMobileTopSection: FC<UseMobileTopSectionProps> = ({
  topbarTitre,
  searchValue,
  filterIcon,
  withFilter,
  useMobileClassName,
  laboFilterIcon,
  partenaireSelectValue,
  withButtonAjouter,
  onClick,
  factureLaboratoires,
  facturesRejeteesInterneCategories,
  facturesRejeteesPrestataireServiceCategories,
  searchVariables,
  setSearchVariables,
  categories,
  onGoback,
  onRequestRefresh,
  espaceType,
  contratFournisseurs,
  facturePartenaires,
  maxTTC,
}) => {
  const [openDrawer, setOpenDrawer] = useState(false);
  const [showMenu, setShoMenu] = useState<boolean>(true);
  const [activeSousCategorie, setActiveSousCategorie] = useState<any | undefined>(undefined);
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
  const handleMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const onChange = () => {};

  const handleDrawerOpen = () => {
    setOpenDrawer(true);
    // setShoMenu(!showMenu);
  };

  const handleDrawerClose = () => {
    setOpenDrawer(false);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const classes = useStyles();
  return (
    <>
      <Box className={useMobileClassName ? classnames(classes.topbarMobile, useMobileClassName) : classes.topbarMobile}>
        <Box display="flex">
          <Typography style={{ fontSize: '16px', fontFamily: 'Roboto', fontWeight: 500 }}>{topbarTitre}</Typography>
        </Box>
        <Box display="flex" alignItems="center">
          <Box>
            <IconButton onClick={handleMenuClick} style={{ padding: 5, marginRight: 10, color: '#000' }}>
              <Search />
            </IconButton>

            <Popover
              id={id}
              open={open}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: 'center',
                horizontal: 'center',
              }}
              transformOrigin={{
                vertical: 'center',
                horizontal: 'center',
              }}
            >
              <Box p={2}>
                <DebouncedSearchInput value={searchValue} outlined placeholder="Rechercher" onChange={() => {}} />
              </Box>
            </Popover>
          </Box>
          <IconButton onClick={handleDrawerOpen}>{showMenu ? <MenuIcon /> : <CloseIcon />}</IconButton>
          {withFilter && <IconButton>{filterIcon}</IconButton>}
        </Box>
      </Box>
      <Box className={classes.topbarMobile}>
        <Box>
          <PartenaireSelect
            // noTitle={isMobile}
            filterIcon={<img src={laboFilterIcon} alt="" style={{ filter: 'brightness(10) invert(1)', width: 20 }} />}
            value={partenaireSelectValue}
            index="laboratoire"
            onChange={() => {}}
          />
        </Box>
        <Box>
          <Typography style={{ fontSize: '14px', fontFamily: 'Roboto', color: '#424242' }}>
            Nouvelles factures
          </Typography>
        </Box>
      </Box>
      <Box className={classes.buttonAjouter}>
        {withButtonAjouter && (
          <Button className={classes.buttonAjouterClick} onClick={onClick}>
            <AddIcon />
            <Typography>Nouvelle facture</Typography>
          </Button>
        )}
      </Box>
      <Drawer
        className={classes.drawer}
        // variant="persistent"
        anchor="right"
        open={openDrawer}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Box style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', padding: '8px' }}>
          <Box display="flex">
            <Typography style={{ fontSize: '16px', fontFamily: 'Roboto', fontWeight: 500 }}>
              Espace facturation
            </Typography>
          </Box>
          <IconButton onClick={handleDrawerClose} style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <CloseIcon />
          </IconButton>
        </Box>
        <Box>
          <GEDFilter
            factureLaboratoires={factureLaboratoires}
            facturesRejeteesInterneCategories={facturesRejeteesInterneCategories}
            facturesRejeteesPrestataireServiceCategories={facturesRejeteesPrestataireServiceCategories}
            searchVariables={searchVariables}
            setSearchVariables={setSearchVariables}
            activeSousCategorie={activeSousCategorie}
            setActiveSousCategorie={setActiveSousCategorie}
            categories={categories}
            onGoBack={onGoback}
            onRequestRefresh={onRequestRefresh}
            espaceType={espaceType}
            contratFournisseurs={contratFournisseurs}
            facturePartenaires={facturePartenaires}
            setOpenDrawer={setOpenDrawer}
            maxTTC={maxTTC}
          />
        </Box>
      </Drawer>
    </>
  );
};
