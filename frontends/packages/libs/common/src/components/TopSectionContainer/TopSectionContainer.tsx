import { Banner, CustomModal, DebouncedSearchInput } from '@lib/ui-kit';
import { SearchInput, CustomSetting, CustomSelectMenu, CustomSelect } from '@lib/ui-kit';
import { Box, IconButton, Popover, Typography } from '@material-ui/core';
import React, { FC, ReactNode } from 'react';
import { useStyles } from './styles';
import { PartenaireSelect, PARTENAIRE_SERVICE, useApplicationContext } from '@lib/common';
// import { useLaboratoireParams } from '../../hooks/useLaboratoireParams';
import { useReactiveVar } from '@apollo/client';
// import { searchTextVar } from '../../reactive-vars';
import CloseIcon from '@material-ui/icons/Close';
import { CSSProperties } from '@material-ui/styles';
import { Search } from '@material-ui/icons';
import { UseMobileTopSection } from './UseMobileTopSection';
import { useGet_Mes_CategoriesQuery } from '@lib/common/src/federation';
// import { ReglementFilter } from '../ReglementFilter/ReglementFilter';

interface Parametre {
  libelle?: any;
  code?: any;
  active?: boolean;
}
interface TopSectionContainerProps {
  title?: string;
  titleStyle?: CSSProperties;
  operationFilter?: any[];
  categoryFilter?: any[];
  litigeFilter?: any[];
  withSearch?: boolean;
  onRequestGoBack?: () => void;
  selectProps?: any;
  onChangeParametre?: (id: any) => void;
  onChangePartenaireSelect: (value: any) => void;
  withPrestataireServices?: boolean;
  partenaireSelectValue?: any;
  searchValue?: any;
  onRunSearch?: (value: string) => void;
  parametres?: Parametre[];
  onSearchLaboChange?: any;
  listLabo?: any[];
  handleCategorieFilter?: (id?: any) => void;
  handleOperationFilter?: (id?: any) => void;
  handleLitigeFilter?: (id?: any) => void;
  withCloseBtn?: boolean;
  withStats?: boolean;
  handleCloseButton?: (event: React.MouseEvent<{}>) => void;
  operationFilterName?: string;
  reglementFilter?: {
    with: boolean;
    handleModeReglementFilter: (id?: string) => void;
    handleTypeDeclarationFilter: (id?: string) => void;
  };
  categorieFilterName?: string;
  additionnalFilter?: ReactNode;
  additionnalFilterPlacement?: 'nextPartenaire' | 'lastElement';
  filterIcon?: any;
  laboFilterIcon?: any;
  laboFilterPrestataireIcon?: string;
  prestataireSelectValue?: any;
  onChangePrestataireSelect?: (value: any) => void;
  defaultMobile?: boolean;
  showPartenaireSelectLabos?: boolean;
}
export const TopSectionContainer: FC<TopSectionContainerProps> = ({
  title,
  operationFilter,
  categoryFilter,
  litigeFilter,
  parametres,
  onChangeParametre,
  handleCategorieFilter,
  handleLitigeFilter,
  handleOperationFilter,
  onChangePartenaireSelect,
  additionnalFilter,
  additionnalFilterPlacement,
  onRunSearch,
  searchValue,
  selectProps,
  withSearch,
  titleStyle,
  handleCloseButton,
  withCloseBtn,
  operationFilterName,
  withStats = true,
  partenaireSelectValue,
  reglementFilter,
  categorieFilterName,
  filterIcon,
  laboFilterIcon,
  withPrestataireServices,
  laboFilterPrestataireIcon,
  prestataireSelectValue,
  onChangePrestataireSelect,
  defaultMobile,
  showPartenaireSelectLabos,
  onRequestGoBack,
}) => {
  const classes = useStyles();
  const { federation, graphql, computeUploadedFilename, notify, user } = useApplicationContext();
  const userPartenaireId = user?.role?.code === PARTENAIRE_SERVICE ? user.prestataireService?.idUser : undefined;
  // const { params, redirectTo } = useLaboratoireParams();
  const [getPath, setGetPath] = React.useState<boolean>();
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
  const [openFilterModal, setOpenFilterModal] = React.useState<boolean>(false);
  // const searchTextValue = useReactiveVar<string>(searchTextVar);
  const { isMobile } = useApplicationContext();
  const [searchParameters, setSearchParameters] = React.useState<any>({
    proprietaire: 'TOUT',
    accesRapide: userPartenaireId ? 'EN_ATTENTE_APPROBATION' : undefined,
    type: 'AUTRES',
  });

  React.useEffect(() => {
    if (window.location.href.includes('pilotages')) {
      setGetPath(true);
    }
  }, []);
  const iconFilter = <img src={filterIcon} alt="filter" />;
  const handleOpenMobileFilter = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    setOpenFilterModal(!openFilterModal);
  };
  const handleLitigeFiltere = (id?: any) => {
    setOpenFilterModal(!openFilterModal);
    handleLitigeFilter && handleLitigeFilter(id);
  };
  const handleCategorieFiltere = (id?: any) => {
    handleCategorieFilter && handleCategorieFilter(id);
    setOpenFilterModal(!openFilterModal);
  };
  const handleOperationFiltere = (id?: any) => {
    handleOperationFilter && handleOperationFilter(id);
    setOpenFilterModal(!openFilterModal);
  };
  const handleRequestModeDeclarationFilter = (id?: any) => {
    reglementFilter?.handleModeReglementFilter(id);
    setOpenFilterModal(!openFilterModal);
  };
  const handleRequestTypeDeclarationFilter = (id?: any) => {
    reglementFilter?.handleTypeDeclarationFilter(id);
    setOpenFilterModal(!openFilterModal);
  };
  const handleMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  const {
    loading: categoriesLoading,
    error: categoriesError,
    data: categoriesData,
    refetch: refetchCategories,
  } = useGet_Mes_CategoriesQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });
  return (
    <>
      {defaultMobile && isMobile ? (
        <UseMobileTopSection
          topbarTitre={title}
          withButtonAjouter
          onGoback={() => {}}
          categories={{
            loading: categoriesLoading,
            error: categoriesError as any,
            data: categoriesData?.gedMesCategories as any,
          }}
          onRequestRefresh={() => {}}
          setSearchVariables={() => {}}
          searchVariables={searchParameters}
        />
      ) : (
        <div className={classes.root}>
          <Box display="flex" style={titleStyle} alignItems="center" justifyContent="space-between">
            {onRequestGoBack ? (
              <Banner
                withDivider={false}
                title={title}
                titleClassName={classes.title}
                titleColor="inherit"
                titleStyle={{ color: '#373741', fontWeight: 500 }}
                onClick={() => onRequestGoBack()}
              />
            ) : (
              <Box>
                <Typography className={classes.title}>{title}</Typography>
              </Box>
            )}
            <Box display="flex" className={classes.containerMobileTop}>
              {withSearch && onRunSearch && (
                <Box mr={1}>
                  <DebouncedSearchInput value={searchValue} outlined placeholder="Rechercher" onChange={onRunSearch} />
                </Box>
              )}
              {showPartenaireSelectLabos ? (
                <Box mr={1} className={classes.laboFilterMobile}>
                  <PartenaireSelect
                    // noTitle={isMobile}
                    filterIcon={
                      <img
                        src={laboFilterIcon}
                        alt="filter"
                        style={{ filter: 'brightness(10) invert(1)', width: 20 }}
                      />
                    }
                    value={partenaireSelectValue}
                    index="laboratoire"
                    onChange={onChangePartenaireSelect}
                  />
                </Box>
              ) : null}
              {withPrestataireServices && onChangePrestataireSelect && (
                <Box mr={1} className={classes.laboFilterMobile}>
                  <PartenaireSelect
                    // noTitle={isMobile}
                    iconDefaultStyle
                    filterIcon={
                      <img
                        src={laboFilterPrestataireIcon}
                        alt="filter"
                        style={{ filter: 'brightness(10) invert(1)' }}
                      />
                    }
                    value={prestataireSelectValue}
                    index="partenaire"
                    onChange={onChangePrestataireSelect}
                  />
                </Box>
              )}
              <>
                {additionnalFilter && additionnalFilterPlacement === 'nextPartenaire' && { additionnalFilter }}
                {categoryFilter && categoryFilter?.length > 0 && (
                  <Box mr={1}>
                    <CustomSelectMenu
                      buttonLabel={categorieFilterName || 'Catégorie'}
                      listFilters={categoryFilter}
                      handleClickItem={handleCategorieFilter}
                      iconFilter={iconFilter}
                    />
                  </Box>
                )}
                {litigeFilter && litigeFilter.length && (
                  <Box mr={1}>
                    <CustomSelectMenu
                      buttonLabel="Litiges"
                      listFilters={litigeFilter}
                      handleClickItem={handleLitigeFilter}
                      iconFilter={iconFilter}
                    />
                  </Box>
                )}
                {operationFilter && operationFilter.length > 0 && (
                  <Box mr={1}>
                    <CustomSelectMenu
                      buttonLabel={operationFilterName || 'Opération'}
                      listFilters={operationFilter}
                      handleClickItem={handleOperationFilter}
                      iconFilter={iconFilter}
                    />
                  </Box>
                )}
                {/* {reglementFilter?.with && (
                  <ReglementFilter
                    onRequestModeDeclarationFilter={handleRequestModeDeclarationFilter}
                    onRequestTypeDeclarationFilter={handleRequestTypeDeclarationFilter}
                  />
                )} */}
                {additionnalFilter &&
                  (additionnalFilterPlacement === 'lastElement' || !additionnalFilterPlacement) &&
                  additionnalFilter}
              </>

              {parametres && (
                <Box mr={1} display="flex" alignItems="center">
                  <CustomSetting parametres={parametres} onChange={onChangeParametre} />
                </Box>
              )}
              {selectProps && (
                <CustomSelect {...selectProps} classes={{ formControl: classes.customSelect }} noMarginBottom />
              )}
              {withCloseBtn && (
                <Box mr={1} display="flex" alignItems="center">
                  <IconButton onClick={handleCloseButton} className={classes.closeBtn}>
                    <CloseIcon />
                  </IconButton>
                </Box>
              )}
            </Box>
          </Box>
        </div>
      )}
    </>
  );
};
