import { Theme, createStyles, makeStyles } from '@material-ui/core';
const drawerWidth = '100%';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: 20,
    },
    title: {
      [theme.breakpoints.down('md')]: {
        fontSize: 16,
      },
      fontSize: 20,
      fontWeight: 700,
      marginRight: 10,
    },
    customSelect: {
      width: 'auto !important',
      '&>div': {
        padding: '0 20px',
      },
      '& fieldset': {
        border: 'none !important',
      },
    },
    mobileAppBar: {
      position: 'relative',
      width: 'calc(100% + 48px)',
      background: theme.palette.primary.main,
      left: '-24px',
      top: '-24px',
      padding: 15,
      textAlign: 'center',
      color: '#fff',
      opacity: 0.8,
    },
    boxMobileCard: {
      display: 'inline-block',
      verticalAlign: 'top',
    },
    swiperSection: {
      display: 'content',
      [theme.breakpoints.down('sm')]: {
        overflow: 'auto',
        display: 'flex',
        alignItems: 'center',
        marginRight: '0 !important',
        '&::-webkit-scrollbar': {
          height: '0 !important',
          width: '0 !important',
        },
      },
    },
    laboFilterMobile: {
      [theme.breakpoints.down('md')]: {
        position: 'absolute',
        left: 0,
        top: 0,
        display: 'flex',
        whiteSpace: 'nowrap',
        maxWidth: '60%',
        overflow: 'hidden',
        borderRadius: 5,
      },
    },
    containerMobileTop: {
      [theme.breakpoints.down('md')]: {
        position: 'relative',
        justifyContent: 'flex-end',
        width: '100%',
        minHeight: 35,
      },
    },
    closeBtn: {
      padding: 5,
    },
    displayInline: {
      verticalAlign: 'top',
      display: 'inline-block',
      width: '50%',
    },
    topbarMobile: {
      width: 'auto',
      display: 'flex',
      justifyContent: 'space-between',
      padding: '16px',
      alignItems: 'center',
    },
    buttonAjouter: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    buttonAjouterClick: {
      display: 'flex',
      alignItems: 'center',
      '& .main-MuiSvgIcon-root': {
        fontSize: '24px',
        color: theme.palette.primary.main,
      },
      '& .main-MuiTypography-root': {
        fontSize: '12px',
        textTransform: 'uppercase',
        fontFamily: 'Roboto',
        marginLeft: '8px',
        color: theme.palette.primary.main,
      },
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
  })
);
