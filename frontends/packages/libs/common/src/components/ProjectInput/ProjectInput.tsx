import { capitalizeFirstLetter, useApplicationContext } from '@lib/common';
import { FiberManualRecord, SupervisedUserCircle } from '@material-ui/icons';
import React, { FC } from 'react';
import CustomSelectProject from './CustomSelectProject';
import { useStyles } from './styles';
import { useNiveauMatriceFonctions } from '@lib/hooks';
import {
  SearchTodoProjetInfoFragment,
  SearchTodoSousProjetInfoFragment,
  useSearch_Todo_ProjetQuery,
} from '@lib/common/src/federation';

export const filterSubProjects = (
  project: SearchTodoProjetInfoFragment,
  useMatriceResponsabilite: boolean,
  user: any,
  applyFilterParticipant: boolean
) => {
  return (project.sousProjets || []).filter(
    (subProject) =>
      subProject &&
      !subProject.archive &&
      (!applyFilterParticipant ||
        (subProject.type === 'PERSONNEL' && subProject.idUser === user.id) ||
        (subProject.type !== 'PERSONNEL' &&
          (applyFilterParticipant
            ? !useMatriceResponsabilite ||
              (useMatriceResponsabilite && subProject.participants.some((participant) => participant?.id === user.id))
            : true)))
  );
};

// projetId or fonctionId or tacheId
export const findProjetById = (projets: any, id: any) => {
  return id
    ? projets?.find((item: any) => item?.id === id || item.idTache === id || item.idFonction === id)
    : undefined;
};

interface ProjectInputProps {
  value?: any; // projetId or fonctionId or tacheId
  disabled?: boolean;
  onChange?: (projet: any) => void;
  termologie?: string;
  onLoaded?: (projets: any) => void;
  onlyMatriceFonctions: boolean;
}
const ProjectInput: FC<ProjectInputProps> = ({
  value,
  disabled,
  onChange,
  termologie,
  onLoaded,
  onlyMatriceFonctions,
}) => {
  const classes = useStyles({});
  const { user, federation, useParameterValueAsBoolean } = useApplicationContext();

  const niveauMatriceFonctions = useNiveauMatriceFonctions();
  const enableMatriceResponsabilite = useParameterValueAsBoolean('0501');

  /*const defaultTerms = [
    {
      term: { supprime: false },
    },
    {
      term: { archive: false },
    },
  ];

  const onlyMatriceFonctionsTerms = onlyMatriceFonctions
    ? [
        {
          exists: {
            field: 'idTache',
          },
        },
        {
          exists: {
            field: 'idFonction',
          },
        },
      ]
    : [];*/

  const { data, loading } = useSearch_Todo_ProjetQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
    variables: {
      index: ['todoprojet'],
      // take: 10000,
      // skip: 0,
      query: {
        query: {
          bool: {
            must: [
              {
                term: { supprime: false },
              },
              {
                term: { 'participants.id': user.id },
              },
            ],
          },
        },
      },
      sortBy: [{ ordre: { order: 'asc' } }],
      //actionStatus: 'ACTIVE',
    },

    onCompleted: (data) => {
      if (onLoaded) {
        onLoaded(data.search?.data);
      }
    },
  });

  const projetList = (data?.search?.data || []) as SearchTodoProjetInfoFragment[];
  const subProjects = projetList.map((projet) => projet.sousProjets).flat();

  const handleChange = (id: string) => {
    if (onChange) {
      const projet = id ? [...projetList, ...subProjects].find((item) => item.id === id) : undefined;

      onChange(projet);
    }
  };

  const project = findProjetById([...projetList, ...subProjects], value);

  const getProjetIcon = (projet: SearchTodoSousProjetInfoFragment) =>
    projet.type === 'PROFESSIONNEL' ? (
      <SupervisedUserCircle htmlColor={projet.couleur?.code || '#A3B1B0'} />
    ) : (
      <FiberManualRecord htmlColor={projet.couleur?.code || '#A3B1B0'} />
    );

  const options =
    niveauMatriceFonctions === 1
      ? [
          ...projetList.filter(({ type }) => type === 'PROFESSIONNEL'),
          ...projetList.filter(({ type }) => type === 'PERSONNEL'),
        ].map((projet) => ({
          id: projet.id,
          title: projet.nom,
          icon: getProjetIcon(projet),
        }))
      : projetList.reduce((listProject: any, projectParent: SearchTodoProjetInfoFragment) => {
          const optionGroup = {
            id: projectParent.id,
            title: projectParent.nom,
            icon: getProjetIcon(projectParent),
          };

          return [
            ...listProject,
            ...filterSubProjects(projectParent, enableMatriceResponsabilite, user, false).map((project) => ({
              optionGroup,
              id: project.id,
              title: project.nom,
              icon: getProjetIcon(project),
            })),
          ];
        }, []);

  return (
    <div className={classes.root}>
      <CustomSelectProject
        niveauMatriceFonctions={niveauMatriceFonctions}
        loading={loading}
        value={project?.id}
        disabled={disabled}
        onChange={handleChange}
        label={capitalizeFirstLetter(termologie || 'Fonction')}
        options={options}
      />
    </div>
  );
};

export default ProjectInput;
