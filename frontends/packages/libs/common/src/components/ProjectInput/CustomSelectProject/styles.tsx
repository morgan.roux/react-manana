import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& .MuiAutocomplete-inputRoot[class*="MuiOutlinedInput-root"]': {
        padding: 6,
      },
      '& .dialog-MuiInputBase-root,& .main-MuiInputBase-root,& .MuiInputBase-root': {
        height: '48px !important',
      },
    },
    icon: {},
    title: {
      color: '#212121',
      fontFamily: 'Roboto',
      marginLeft: 10,
      fontSize: '0.875rem',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      '-webkit-line-clamp': 1,
      '-webkit-box-orient': 'vertical',
      maxWidth: 280,
    },

    group: {
      paddingLeft: 18,
    },
    outerNoPadding: {
      '& .main-MuiInputBase-root': {
        padding: '0 !important',
      },
      '& input': {
        padding: '0 !important',
      },
    },
  })
);

export default useStyles;
