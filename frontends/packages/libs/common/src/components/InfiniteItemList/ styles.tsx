import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    list: {
      width: '100% !important',
      padding: '0px',
      // marginBottom: 16,
      '& > div': {
        // height: '100% !important',
        maxHeight: 'none !important',
      },
    },
    autoSizer: {
      width: '100% !important',
    },
  })
);

export default useStyles;
