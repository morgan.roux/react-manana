import { QueryResult } from '@apollo/client';
import classNames from 'classnames';
import { uniqBy } from 'lodash';
import React, { CSSProperties, PropsWithChildren, useEffect } from 'react';
import { AutoSizer, CellMeasurer, CellMeasurerCache, Index, IndexRange, InfiniteLoader, List } from 'react-virtualized';
import useStyles from './ styles';

const createCache = () => {
  return new CellMeasurerCache({
    defaultWidth: 500,
    minWidth: 75,
    fixedHeight: false,
  });
};

interface Identifiable {
  id: string;
}

interface ItemRenderingOptions<ITem extends Identifiable> {
  loading: boolean;
  data?: ITem;
  style: CSSProperties;
  registerChild: any;
  refetch: () => void;
  onClick: (event: any) => void;
  active: boolean;
  activeItem?: ITem;
}

interface OffsetPaginationBasedVariables {
  skip: number;
  take: number;
  [key: string]: any;
}

interface InfiniteItemListProps<ITem extends Identifiable, TData = any, TVariables = OffsetPaginationBasedVariables> {
  queryResult: QueryResult<TData, TVariables>;
  itemRenderer: (options: ItemRenderingOptions<ITem>) => React.ReactNode;
  buildVariables?: (queryResult: QueryResult<TData, TVariables>, range: IndexRange) => TVariables;
  updateQuery?: (
    previousQueryResult: TData,
    options: {
      fetchMoreResult?: TData;
      variables?: TVariables;
    }
  ) => TData;
  cache?: CellMeasurerCache;
  extractItems?: (queryResult: QueryResult<TData, TVariables>) => ITem[];
  extractItemsTotalCount?: (queryResult: QueryResult<TData, TVariables>) => number;
  onItemClick?: (item: ITem, event: any) => void;
  activeItem?: ITem;
  classes?: {
    autoSizer?: string;
    list?: string;
  };

  refetch?: () => void;
}

const InfiniteItemList = <ITem extends Identifiable, TData = any, TVariables = OffsetPaginationBasedVariables>({
  queryResult,
  itemRenderer,
  buildVariables,
  updateQuery: providedUpdateQuery,
  cache: providedCache,
  extractItems: providedExtractItems,
  extractItemsTotalCount: providedExtractItemsTotalCount,
  onItemClick,
  activeItem,
  classes: providedClasses,
  refetch,
}: PropsWithChildren<InfiniteItemListProps<ITem, TData, TVariables>>) => {
  const extractItems = (queryResult: any): ITem[] => {
    return queryResult.data?.search?.data || [];
  };

  const classes = useStyles();
  let listRef = React.useRef();
  const cache = providedCache ?? createCache();

  const data = providedExtractItems ? providedExtractItems(queryResult) : extractItems(queryResult);

  useEffect(() => {
    if (data) {
      resizeAll();
    }
  }, [data]);

  const resizeAll = () => {
    cache.clearAll();
    if (listRef.current) {
      (listRef.current as any).recomputeRowHeights();
    }
  };

  const updateQuery = (prev: any, { fetchMoreResult }: any) => {
    if (
      prev &&
      prev.search &&
      prev.search.data &&
      fetchMoreResult &&
      fetchMoreResult.search &&
      fetchMoreResult.search.data
    ) {
      const { data: currentData } = prev.search;
      return {
        ...prev,
        search: {
          ...prev.search,
          data: uniqBy([...currentData, ...fetchMoreResult.search.data], 'id'),
          total: fetchMoreResult.search.total,
        },
      };
    }
    return prev;
  };

  const loadMoreItems = ({ startIndex, stopIndex }: IndexRange) => {
    return queryResult.fetchMore({
      variables: buildVariables
        ? buildVariables(queryResult, { startIndex, stopIndex })
        : ({
            ...queryResult.variables,
            skip: startIndex,
            take: stopIndex - startIndex + 1,
          } as any),

      updateQuery: providedUpdateQuery ?? updateQuery,
    });
  };

  const extractItemsTotalCount = (queryResult: any): number => {
    return queryResult.data?.search?.total || 0;
  };

  const isItemLoaded = ({ index }: Index): boolean =>
    index < (providedExtractItems ? providedExtractItems(queryResult) : extractItems(queryResult)).length;
  const totalCount = providedExtractItemsTotalCount
    ? providedExtractItemsTotalCount(queryResult)
    : extractItemsTotalCount(queryResult);

  return (
    <InfiniteLoader isRowLoaded={isItemLoaded} rowCount={totalCount} loadMoreRows={loadMoreItems as any}>
      {({ onRowsRendered, registerChild }) => (
        <AutoSizer className={classNames(classes.autoSizer, providedClasses?.autoSizer)}>
          {({ width, height }) => {
            return (
              <List
                deferredMeasurementCache={cache}
                ref={(ref) => {
                  listRef = ref as any;
                  registerChild(ref);
                }}
                className={classNames(classes.list, providedClasses?.list)}
                onRowsRendered={onRowsRendered}
                width={width}
                height={height}
                rowHeight={cache.rowHeight}
                rowCount={totalCount}
                rowRenderer={({ index, key, parent, style }) => {
                  return (
                    <CellMeasurer cache={cache} columnIndex={0} key={key} rowIndex={index} parent={parent}>
                      {({ registerChild: registerChildCell }) => {
                        const data = providedExtractItems
                          ? providedExtractItems(queryResult)
                          : extractItems(queryResult);

                        const loaded = index < data.length;
                        return itemRenderer({
                          loading: !loaded,
                          data: loaded ? data[index] : undefined,
                          registerChild: registerChildCell,
                          refetch: refetch ?? queryResult.refetch,
                          style,
                          onClick: (event) => {
                            if (loaded && data[index] && onItemClick) {
                              onItemClick(data[index], event);
                            }
                          },
                          active: !!(activeItem?.id && loaded && data[index] && data[index].id === activeItem.id),
                          activeItem: activeItem,
                        });
                      }}
                    </CellMeasurer>
                  );
                }}
              />
            );
          }}
        </AutoSizer>
      )}
    </InfiniteLoader>
  );
};

export default InfiniteItemList;
