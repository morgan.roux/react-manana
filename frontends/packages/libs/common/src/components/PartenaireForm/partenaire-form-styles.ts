import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      margin: 'auto',
      width: '100%',
    },
    textField: {
      width: '100%',
      '& .main-MuiFormLabel-root': {
        color: '#999 !important'
      },
      '& .main-MuiInputBase-input': {
        color: '#000 !important'
      },
    },
    dropZone: {
      marginBottom: '16px',
      minWidth: '500px',
    },
    // customText: {
    //   '& .main-MuiFormLabel-root': {
    //     color: '#000 !important'
    //   }
    // },
  })
);
