import { Box, FormControl } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { CustomModal, CustomTextField } from '@lib/ui-kit';
import {
  Laboratoire,
  PrestataireService,
  PrestataireServiceSuite,
  LaboratoireSuite,
  useUpdate_One_LaboratoireMutation,
  useUpdate_One_Prestataire_ServiceMutation,
  useCreate_One_Prestataire_ServiceMutation,
  useCreate_One_LaboratoireMutation,
} from '@lib/common/src/federation';
import { useStyles } from './partenaire-form-styles';
import { Dropzone, isEmailValid, isUrlValid, UploadedFile, useApplicationContext } from '@lib/common';
import { useEffect } from 'react';
import { CustomCheckbox } from '../CustomCheckbox';

interface PartenaireFormProps {
  mode: 'creation' | 'modification';
  partenaireType: 'PRESTATAIRE_SERVICE' | 'LABORATOIRE';
  partenaireAssocieToEdit?: Laboratoire | PrestataireService;
  idSecteur?: string;
  cerclePartenariat?: 'LABORATOIRE' | 'PHARMACIE';
  open: boolean;
  defaultValues?: Partial<FormValues>
  setOpen: (open: boolean) => void;
  onCreated?: (prestataire?: PrestataireService | Laboratoire) => void;
}

interface FormValues {
  files: (File | UploadedFile)[]; // Logo

  nom: string;
  adresse: string;
  codePostal: string;
  ville: string;
  telephone: string;
  email: string;
  webSiteUrl: string;
  idSecteur?: string;
  cerclePartenariat?: 'LABORATOIRE' | 'PHARMACIE';
  prive: boolean;
}

const initialValues: FormValues = {
  files: [],
  nom: '',
  adresse: '',
  codePostal: '',
  ville: '',
  telephone: '',
  email: '',
  webSiteUrl: '',
  prive: true,
};

export const PartenaireForm: FC<PartenaireFormProps> = ({
  mode,
  partenaireType,
  partenaireAssocieToEdit,
  open,
  setOpen,
  idSecteur,
  cerclePartenariat,
  onCreated,
  defaultValues
}) => {
  const classes = useStyles();
  const { federation, uploadFiles, notify, currentPharmacie } = useApplicationContext();
  const [values, setValues] = useState<FormValues>(initialValues);
  const [saving, setSaving] = useState<boolean>(false);

  const [updateLaboratoire] = useUpdate_One_LaboratoireMutation({
    client: federation,
    onCompleted: () => {
      setSaving(false);
      setOpen(false);
    },
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de modification de laboratoire`,
      });
      setSaving(false);
    },
  });

  const [createOnePrestataireService, creatingOnePrestataireService] = useCreate_One_Prestataire_ServiceMutation({
    client: federation,
    onCompleted: (data) => {
      setSaving(false);
      setOpen(false);

      if (onCreated) {
        onCreated(data.createOnePrestataireService as any);
      }
    },
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de création de prestataire de service`,
      });
      setSaving(false);
    },
  });

  const [createOneLaboratoire, creatingOneLaboratoire] = useCreate_One_LaboratoireMutation({
    client: federation,
    onCompleted: (data) => {
      setSaving(false);
      setOpen(false);

      if (onCreated) {
        onCreated(data.createOneLaboratoire as any);
      }
    },
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de création de laboratoire`,
      });
      setSaving(false);
    },
  });

  const [updatePrestataireService] = useUpdate_One_Prestataire_ServiceMutation({
    client: federation,
    onCompleted: () => {
      setSaving(false);
      setOpen(false);
    },
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de modification de prestataire de service`,
      });
      setSaving(false);
    },
  });

  useEffect(() => {
    if (open) {
      if (mode === 'modification') {
        if (partenaireAssocieToEdit) {
          const partenaireSuite: PrestataireServiceSuite | LaboratoireSuite | undefined | null =
            partenaireType === 'LABORATOIRE'
              ? (partenaireAssocieToEdit as Laboratoire).laboratoireSuite
              : (partenaireAssocieToEdit as PrestataireService).prestataireServiceSuite;

          const adresse =
            partenaireType === 'LABORATOIRE'
              ? (partenaireSuite as LaboratoireSuite)?.adresse || ''
              : (partenaireSuite as PrestataireServiceSuite)?.adresse1 || '';

          setValues({
            files: partenaireAssocieToEdit.photo?.chemin ? ([partenaireAssocieToEdit.photo] as any) : [],
            nom: partenaireAssocieToEdit.nom,
            adresse,
            codePostal: partenaireSuite?.codePostal || '',
            ville: partenaireSuite?.ville || '',
            telephone: partenaireSuite?.telephone || '',
            email: partenaireSuite?.email || '',
            webSiteUrl: partenaireSuite?.webSiteUrl || '',
            prive: (partenaireAssocieToEdit as any).prive,
          });
        }
      } else {
        setValues(defaultValues ? {
          ...initialValues,
          ...defaultValues,
        } : initialValues);
      }
    }
  }, [partenaireAssocieToEdit, mode, open]);

  const handleChange = (name: keyof FormValues, value: any) => {
    setValues((prev) => ({ ...prev, [name]: value }));
  };

  const handleSave = () => {
    setSaving(true);
    uploadFiles(values.files, {
      directory: partenaireType === 'LABORATOIRE' ? `laboratoires` : `prestataires_service`,
    })
      .then((filesInput) => {
        const commonInput = {
          photo: filesInput.length > 0 ? filesInput[0] : undefined,
          nom: values.nom,
          codePostal: values.codePostal,
          ville: values.ville,
          telephone: values.telephone,
          email: values.email,
          webSiteUrl: values.webSiteUrl,
          idSecteur,
          cerclePartenariat,
          prive: values.prive,
          idPharmacie: values.prive ? currentPharmacie.id : undefined,
        };

        if (partenaireAssocieToEdit) {
          if (partenaireType === 'LABORATOIRE') {
            updateLaboratoire({
              variables: {
                id: partenaireAssocieToEdit.id,
                idPharmacie: currentPharmacie.id,
                input: {
                  ...commonInput,
                  adresse: values.adresse,
                },
              },
            });
          } else {
            updatePrestataireService({
              variables: {
                id: partenaireAssocieToEdit.id,
                input: {
                  ...commonInput,
                  adresse1: values.adresse,
                },
              },
            });
          }
        } else {
          if (partenaireType === 'LABORATOIRE') {
            //createLaboratoire
            createOneLaboratoire({
              variables: {
                idPharmacie: currentPharmacie.id,
                input: {
                  ...commonInput,
                  adresse: values.adresse,
                },
              },
            });
          } else {
            createOnePrestataireService({
              variables: {
                input: {
                  ...commonInput,
                  adresse1: values.adresse,
                },
              },
            });
          }
        }
      })
      .catch((error) => {
        notify({
          type: 'error',
          message: `Une erreur s'est produite lors de chargement du fichier`,
        });
        setSaving(false);
      });
  };

  const partenaireTitle = partenaireType === 'LABORATOIRE' ? 'Laboratoire' : 'Prestataire service';

  const isValid = () => {
    return (
      (values?.email ? isEmailValid(values?.email) : true) &&
      (values?.webSiteUrl ? isUrlValid(values?.webSiteUrl) : true)
    );
  };

  return (
    <CustomModal
      open={open}
      closeIcon
      headerWithBgColor
      maxWidth="xl"
      setOpen={setOpen}
      onClickConfirm={handleSave}
      disabledButton={saving || !values.nom || !isValid()}
      title={mode === 'modification' ? `Modification de ${partenaireTitle}` : `Création de ${partenaireTitle}`}
      withBtnsActions
    >
      <Box className={classes.root}>
        <Box className={classes.textField}>
          <Dropzone
            multiple={false}
            acceptFiles="image/*"
            selectedFiles={values.files}
            setSelectedFiles={(value) => handleChange('files', value)}
            className={classes.dropZone}
          />
        </Box>
        <CustomTextField
          className={classes.textField}
          placeholder="Nom"
          variant="outlined"
          required
          label="Nom"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.nom}
          onChange={(event) => handleChange('nom', event.target.value)}
        />
        <CustomTextField
          className={classes.textField}
          placeholder="Adresse"
          variant="outlined"
          label="Adresse"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.adresse}
          onChange={(event) => handleChange('adresse', event.target.value)}
        />
        <CustomTextField
          className={classes.textField}
          placeholder="Code postal"
          variant="outlined"
          label="CP"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.codePostal}
          onChange={(event) => handleChange('codePostal', event.target.value)}
        />
        <CustomTextField
          className={classes.textField}
          placeholder="Ville"
          variant="outlined"
          label="Ville"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.ville}
          onChange={(event) => handleChange('ville', event.target.value)}
        />
        <CustomTextField
          className={classes.textField}
          placeholder="Email"
          variant="outlined"
          label="Email"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.email}
          onChange={(event) => handleChange('email', event.target.value)}
        />
        <CustomTextField
          className={classes.textField}
          placeholder="Téléphone"
          variant="outlined"
          label="Téléphone"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.telephone}
          onChange={(event) => handleChange('telephone', event.target.value)}
        />

        <CustomTextField
          className={classes.textField}
          placeholder="http://exemple.com"
          variant="outlined"
          label="Site Web"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.webSiteUrl}
          onChange={(event) => handleChange('webSiteUrl', event.target.value)}
        />

        <FormControl className={classes.textField} style={{ marginBottom: '16px', marginTop: '10px', width: '100%' }}>
          <CustomCheckbox
            value={values.prive}
            name="prive"
            checked={values.prive}
            onClick={(e: any) => e.stopPropagation()}
            onChange={(event: any) => handleChange('prive', event.target.checked)}
            label="Propre à la pharmacie"
          />
        </FormControl>
      </Box>
    </CustomModal>
  );
};
