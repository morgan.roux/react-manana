import { Checkbox } from '@material-ui/core';
import React, { FC } from 'react';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import { useStyles } from './styles';
import { ActionStatus, useUpdate_Action_StatusMutation } from '../../graphql';
import { useApplicationContext } from '@lib/common';
import { TodoActionStatusProps } from '../TodoActionStatus';

export const ToggleTodoAction: FC<TodoActionStatusProps> = ({ refetch, action }) => {
  const classes = useStyles();
  const { graphql, notify } = useApplicationContext();

  const [updateActionStatus] = useUpdate_Action_StatusMutation({
    client: graphql,
    onCompleted: () => {
      refetch && refetch();
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  const toggleStatus = () => {
    if (action?.id) {
      updateActionStatus({
        variables: {
          input: {
            idAction: action?.id,
            status: action?.status === 'DONE' ? ActionStatus.Active : ActionStatus.Done,
          },
        },
      });
    }
  };
  return (
    <Checkbox
      icon={<CircleUnchecked fontSize="large" />}
      checkedIcon={<CircleChecked fontSize="large" />}
      onClick={(event) => {
        event.preventDefault();
        event.stopPropagation();
        toggleStatus();
      }}
      checked={action?.status === 'DONE'}
      color="primary"
      style={{ color: '#757575' }}
      classes={{ checked: classes.checkedBox }}
    />
  );
};
