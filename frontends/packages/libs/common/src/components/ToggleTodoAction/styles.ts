import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    checkedBox: {
      color: `${theme.palette.primary.main} !important`,
    },
  })
);
