import { createStyles, Theme, withStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      fontFamily: 'Roboto',
      color: 'rgba(0, 0, 0, 0.54)',
      fontSize: 12,
      width: 'fit-content',
      margin: '0px 0px 0px 16px',
      fontWeight: 400,
      lineHeight: 1,
      transform: 'translateY(7px)',
      background: 'white',
      padding: '0px 4px',
      '& span': {
        color: 'red',
        marginLeft: 4,
      },
    },
  })
);

export default useStyles;
