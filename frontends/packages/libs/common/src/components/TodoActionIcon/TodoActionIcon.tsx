import React, { FC } from 'react';
import { SearchTodoActionInfoFragment } from 'src/federation';
import TradeMarketingIcon from './market-icon.png';
import { isTraitementAutomatiqueAction, isReunionQualiteAction, isTradeMarketingAction } from '../../shared';
import { EventNote, Repeat } from '@material-ui/icons';

interface TodoActionIconProps {
  action: Partial<SearchTodoActionInfoFragment>;
  imgWidth?: number;
  className?: string;
}

export const TodoActionIcon: FC<TodoActionIconProps> = ({ action, imgWidth, className }) => {
  if (isTraitementAutomatiqueAction(action)) {
    return <Repeat className={className || ''} />;
  }

  if (isReunionQualiteAction(action)) {
    return <EventNote className={className || ''} />;
  }

  if (isTradeMarketingAction(action)) {
    return <img style={{ width: imgWidth || 30 }} src={TradeMarketingIcon} className={className || ''} />;
  }

  return null;
};
