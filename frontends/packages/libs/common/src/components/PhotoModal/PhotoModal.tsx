import React, { FC } from 'react';
import useStyles from './styles';
import { Dialog, AppBar, Toolbar, Tooltip, Fade, IconButton, Box } from '@material-ui/core';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import classnames from 'classnames';
import { Close } from '@material-ui/icons';

interface PhotoModalProps {
  open: boolean;
  setOpen: (value: boolean) => void;
  url: string | undefined;
  libelle?: string;
}

const PhotoModal: FC<PhotoModalProps & RouteComponentProps> = ({ open, setOpen, url, libelle }) => {
  const classes = useStyles({});
  return (
    <Dialog open={open} className={classes.root}>
      <Toolbar className={classes.flexToolbar}>
        {libelle}
        <Tooltip title="Fermer">
          <IconButton color="inherit" onClick={() => setOpen(false)}>
            <Close />
          </IconButton>
        </Tooltip>
      </Toolbar>
      <img src={url} />
    </Dialog>
  );
};

export default withRouter(PhotoModal);
