import { CssBaseline, Drawer, Hidden, useTheme } from '@material-ui/core';
import React, { FC, ReactNode } from 'react';
import { MobileTopBar } from '../MobileTopBar';
import useStyles from './styles';
interface DrawerProps {
  mobileOpen?: boolean;
  backUrl?: string;
  closeUrl?: string;
  children: ReactNode;
  handleDrawerToggle?: () => void;
  mobileTobBarTitle?: string;
}

const DrawerComponent: FC<DrawerProps> = ({ children, mobileOpen, handleDrawerToggle, mobileTobBarTitle, backUrl }) => {
  const classes = useStyles({});
  const container = window !== undefined ? () => window.document.body : undefined;
  const theme = useTheme();
  return (
    <>
      <Hidden lgUp={true} implementation="css">
        <Drawer
          container={container}
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <CssBaseline />
          <Hidden mdUp={true} implementation="css">
            <MobileTopBar
              title={mobileTobBarTitle}
              backUrl={backUrl}
              withBackBtn={false}
              withCloseBtn={true}
              handleDrawerToggle={handleDrawerToggle}
              fullScreen={true}
              onClose={handleDrawerToggle}
            />
          </Hidden>

          {children}
        </Drawer>
      </Hidden>
      <Hidden mdDown={true} implementation="css">
        {children}
      </Hidden>
    </>
  );
};

export default DrawerComponent;
