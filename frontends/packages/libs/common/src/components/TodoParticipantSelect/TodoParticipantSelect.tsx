import { IconButton } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { CustomAvatarGroup } from '@lib/ui-kit';
import { PersonAdd } from '@material-ui/icons';
import { CustomSelectUser, useApplicationContext } from '../../';
import { SearchTodoActionInfoFragment, useUpdate_Item_Scoring_For_UserLazyQuery } from '../../federation';
import { DO_UPDATE_TODO_ACTION_PARTICIPANTS } from '../../federation/basis/todo-action/mutation';
import { useMutation } from '@apollo/client';

export interface TodoParticipantSelectProps {
  todoAction?: SearchTodoActionInfoFragment;
  refetch?: () => void;
  maxAvatar?: number;
}

export const TodoParticipantSelect: FC<TodoParticipantSelectProps> = ({ todoAction, refetch, maxAvatar }) => {
  const participants = todoAction?.participants;
  const { notify, user } = useApplicationContext();

  const [openDialog, setOpenDialog] = useState<boolean>(false);

  const [updateItemScoringByUser] = useUpdate_Item_Scoring_For_UserLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [updateTodoActionParticipants] = useMutation(DO_UPDATE_TODO_ACTION_PARTICIPANTS, {
    onCompleted: () => {
      updateItemScoringByUser({
        variables: {
          idUser: user.id,
          codeItem: todoAction?.item?.code || 'L',
        },
      });

      refetch && refetch();
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  const handleParticipants = (newParticipants: any) => {
    const newAssignUsers = newParticipants.filter(
      ({ id }: any) => !(participants || []).some((assignment: any) => assignment.id === id)
    );
    if (todoAction) {
      updateTodoActionParticipants({
        variables: {
          input: {
            idAction: todoAction.id,
            idUserDestinations: newAssignUsers.map(({ id }: any) => id),
          },
        },
      });
    }
  };

  return (
    <>
      <IconButton onClick={() => setOpenDialog((prev) => !prev)} style={{ padding: 0 }}>
        {participants && participants.length > 0 ? (
          <CustomAvatarGroup users={(participants as any) || []} max={maxAvatar || 5} />
        ) : (
          <PersonAdd style={{ marginRight: 10 }} />
        )}
      </IconButton>
      <CustomSelectUser
        openModal={openDialog}
        setOpenModal={() => {
          setOpenDialog(false);
        }}
        idParticipants={(todoAction?.projet?.participants || []).map(({ id }: any) => id)}
        selected={todoAction?.participants || []}
        setSelected={(participants: any) => handleParticipants(participants)}
      />
    </>
  );
};
