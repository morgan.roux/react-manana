import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';

export default makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
  },
  field: {
    marginRight: '8px !important',
  },
  userInput: {
    width: '100%',
    marginTop: -8,
  },
  userInputRoot: {
    margin: 0,
  },

  userInputInput: {
    height: 40,
  },
  selectCorrespondant: {
    '& .main-MuiInputBase-root': {
      height: '48px',
    },
  },
}));
