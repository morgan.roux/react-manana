import { Box } from '@material-ui/core';
import React, { FC, useState, useEffect, useRef } from 'react';
import { CustomAutocomplete, CustomSelect, CustomTextField } from '@lib/ui-kit';
import UserInput from '../UserInput';
import useStyles from './style';
import { PartenaireInput, useApplicationContext } from '@lib/common';
import { useGet_OriginesQuery } from '@lib/common/src/federation';
import { useSearch_All_PharmaciesLazyQuery } from '@lib/common/src/graphql';
import { debounce } from 'lodash';
import classnames from 'classnames';

interface OrigineInputProps {
  origine?: any; // id or origine instance
  origineAssocie: any; // id or origineAssocie
  onChangeOrigine: (origine: any) => void;
  onChangeOrigineAssocie: (origineAssocie: any) => void;
  readonly?: boolean;
  enabledCodes?: string[];
  flexDirection?: string;
  withSelectAll?: boolean;
  userInputClassName?: string;
  customSelectClassName?: string;
}
const OrigineInput: FC<OrigineInputProps> = ({
  origineAssocie,
  origine,
  onChangeOrigine,
  onChangeOrigineAssocie,
  readonly,
  enabledCodes,
  flexDirection = 'row',
  withSelectAll = false,
  userInputClassName,
  customSelectClassName,
}) => {
  const classes = useStyles();

  const [openSelectUserModal, setOpenSelectUserModal] = useState<boolean>(false);
  const [currentOrigine, setCurrentOrigine] = useState<any>();
  const origines = useGet_OriginesQuery();
  const { graphql, currentGroupement } = useApplicationContext();
  const [pharmacieInputValue, setPharmacieInputValue] = useState<string>('');

  useEffect(() => {
    if (origine) {
      setCurrentOrigine(
        typeof origine === 'string' ? (origines.data?.origines.nodes || []).find((o) => o.id === origine) : origine
      );
    } else if (!withSelectAll) {
      const interneOrigine = (origines.data?.origines.nodes || []).find(({ code }: any) => code === 'INTERNE');
      onChangeOrigine(interneOrigine);
    }

    setCurrentOrigine(
      origine
        ? typeof origine === 'string'
          ? (origines.data?.origines.nodes || []).find((o) => o.id === origine)
          : origine
        : undefined
    );
  }, [origine, origines.loading]);

  const isInterne = currentOrigine && currentOrigine.code === 'INTERNE';
  const isLaboratoire = currentOrigine && currentOrigine.code === 'LABORATOIRE';
  const isPrestataire = currentOrigine && currentOrigine.code === 'PRESTATAIRE_SERVICE';
  const isConfrere = currentOrigine && currentOrigine.code === 'CONFRERE';

  const handleChangeOrigine = (event: any) => {
    const foundOrigine = (origines.data?.origines.nodes || []).find((o) => o.id === event.target.value);
    onChangeOrigineAssocie(isInterne ? [] : '');
    onChangeOrigine(foundOrigine);
  };

  const handleSearchPharmacies = (searchText: string) => {
    onChangeOrigineAssocie({ id: '', nom: searchText });
  };

  const handlePharmacieAutocompleChange = (pharmacie: any) => {
    onChangeOrigineAssocie(pharmacie);
  };

  const debouncedPharmacie = useRef(
    debounce((value: string) => {
      const formated = value.replace(/\s+/g, '\\ ').replace('/', '\\/');
      loadPharmacies({
        variables: {
          type: ['pharmacie'],
          query: {
            query: {
              bool: {
                must: [
                  { term: { idGroupement: currentGroupement.id } },
                  {
                    query_string: {
                      query: formated.startsWith('*') || formated.endsWith('*') ? formated : `*${formated}*`,
                      analyze_wildcard: true,
                    },
                  },
                ],
              },
            },
          },
        },
      });
    }, 1500)
  );

  useEffect(() => {
    origineAssocie?.nom && debouncedPharmacie.current(origineAssocie?.nom);
  }, [origineAssocie]);

  useEffect(() => {
    loadPharmacies({
      variables: {
        type: ['pharmacie'],
        take: 10,
        skip: 0,
        query: { query: { bool: { must: [{ term: { idGroupement: currentGroupement.id } }] } } },
      },
    });
  }, []);

  const [loadPharmacies, loadingPharmacies] = useSearch_All_PharmaciesLazyQuery({
    client: graphql,
  });

  const origineList = enabledCodes
    ? (origines.data?.origines.nodes || []).filter((origine) => enabledCodes.includes(origine.code))
    : origines.data?.origines.nodes || [];

  const allOptions = withSelectAll
    ? [
        {
          id: 'TOUT',
          libelle: `Tout`,
        },
        ...(origineList || []),
      ]
    : origineList || [];

  console.log();

  return (
    <Box display="flex" flexDirection={flexDirection} justifyContent="space-between" className={classes.root}>
      <CustomSelect
        loading={origines.loading}
        error={origines.error}
        className={classes.field}
        label="Origine"
        required={true}
        list={allOptions}
        name="origine"
        onChange={handleChangeOrigine}
        listId="id"
        index="libelle"
        value={!origine && withSelectAll ? 'TOUT' : currentOrigine?.id ? currentOrigine.id : ''}
        placeholder="Choisir l'origine"
        withPlaceholder={true}
        style={{ textAlign: 'left', marginRight: flexDirection === 'column' ? 0 : 8 }}
        inputProps={{
          readOnly: readonly,
        }}
        customSelectClassName={customSelectClassName}
      />
      {isInterne ? (
        <UserInput
          inputProps={{
            classes: {
              root: classes.userInputRoot,
              input: classes.userInputInput,
            },
          }}
          className={userInputClassName ? classnames(classes.userInput, userInputClassName) : classes.userInput}
          label="Correspondant"
          title="Choix de correspondant"
          singleSelect={true}
          withAssignTeam={false}
          selected={origineAssocie}
          setSelected={(users: any) => onChangeOrigineAssocie(users)}
          openModal={openSelectUserModal}
          setOpenModal={setOpenSelectUserModal}
        />
      ) : isLaboratoire ? (
        <PartenaireInput
          key="laboratoire"
          index="laboratoire"
          label="Correspondant"
          value={origineAssocie}
          onChange={(laboratoire) => onChangeOrigineAssocie(laboratoire)}
        />
      ) : isPrestataire ? (
        <PartenaireInput
          key="partenaire"
          index="partenaire"
          label="Correspondant"
          value={origineAssocie}
          onChange={(laboratoire) => onChangeOrigineAssocie(laboratoire)}
        />
      ) : isConfrere ? (
        <CustomAutocomplete
          value={origineAssocie}
          inputValue={origineAssocie?.nom || ''}
          options={loadingPharmacies.data?.search?.data || []}
          optionLabelKey="nom"
          search={handleSearchPharmacies}
          onAutocompleteChange={handlePharmacieAutocompleChange}
          placeholder="Choisissez une pharmacie"
          loading={loadingPharmacies.loading}
        />
      ) : (
        <CustomTextField
          label="Correspondant"
          variant="outlined"
          disabled={true}
          onChange={() => console.log('Do nothing')}
          value={''}
          fullWidth
          InputLabelProps={{
            shrink: true,
          }}
          className={classes.selectCorrespondant}
        />
      )}
    </Box>
  );
};

export default OrigineInput;
