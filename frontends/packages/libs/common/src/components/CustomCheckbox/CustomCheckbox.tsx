import React from 'react';
import { FormControlLabel, Checkbox, createStyles, Theme } from '@material-ui/core';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { makeStyles } from '@material-ui/core';
import { CheckboxProps } from '@material-ui/core/Checkbox';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'flex-left',
      '& .MuiFormControlLabel-label': {
        fontSize: 14,
        '@media (max-width: 992px)': {
          fontSize: 12,
        },
      },
    },
    icon: {
      // backgroundColor: 'transparent',
      // color: theme.palette.primary.main,
      // borderRadius: 1,
      '&:checked': {
        // color: theme.palette.common.white,
      },
    },
    iconOnLoginPage: {
      backgroundColor: 'transparent',
      color: theme.palette.common.white,
      borderRadius: 1,
      '&:checked': {
        color: theme.palette.secondary.main,
      },
    },
  })
);

const CustomCheckbox = (props: CheckboxProps & any) => {
  const classes = useStyles({});
  const { label, location, color, disabled } = props;

  return (
    <FormControlLabel
      className={classes.root}
      control={
        <Checkbox
          {...props}
          color={color}
          icon={
            <CheckBoxOutlineBlankIcon
              fontSize="small"
              className={location === 'inLogin' ? classes.iconOnLoginPage : classes.icon}
            />
          }
          checkedIcon={
            <CheckBoxIcon
              fontSize="small"
              className={location === 'inLogin' ? classes.iconOnLoginPage : classes.icon}
            />
          }
        />
      }
      label={label}
      disabled={disabled}
    />
  );
};

export default CustomCheckbox;

CustomCheckbox.defaultProps = {
  label: 'Rester connecté',
  location: '',
  color: 'primary',
};
