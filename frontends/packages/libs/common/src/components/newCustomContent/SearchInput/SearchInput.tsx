import { useFilters } from '@lib/common/src/shared/operations/filter';
import { SearchInput } from '@lib/ui-kit';
import { debounce } from 'lodash';
import React, { FC, useEffect, useRef, useState } from 'react';
import useStyles from './styles';

interface CustomContentSearchInputProps {
  fullWidth?: boolean;
  searchPlaceholder: string;
  disabled?: boolean;
}

const CustomContentSearchInput: FC<CustomContentSearchInputProps> = ({ fullWidth, searchPlaceholder, disabled }) => {
  const classes = useStyles({});
  const [debouncedText, setDebouncedText] = useState<string>('');
  const onChangeSearchText = (value: string) => {
    setDebouncedText(value);
  };
  const { searchText: searchTextQuery, setSearchText } = useFilters();
  const searchText =
    searchTextQuery && searchTextQuery.data && searchTextQuery.data.searchText && searchTextQuery.data.searchText.text;

  const debouncedSearchText = useRef(
    debounce((value: string) => {
      setSearchText(value);
    }, 1500)
  );

  useEffect(() => {
    setDebouncedText(searchText ? searchText : '');
  }, [searchText]);

  useEffect(() => {
    debouncedSearchText.current(debouncedText);
  }, [debouncedText]);

  useEffect(() => {
    return () => {
      setSearchText('');
    };
  }, []);

  return (
    <div className={fullWidth ? classes.fullWidth : classes.root}>
      <SearchInput
        value={debouncedText}
        searchInFullWidth={fullWidth}
        noPrefixIcon={true}
        onChange={onChangeSearchText}
        placeholder={searchPlaceholder}
        disabled={disabled}
        outlined={true}
      />
    </div>
  );
};

export default CustomContentSearchInput;
