import { makeStyles, Theme, createStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    fullWidth: {
      width: '50%',
    },
  })
);

export default useStyles;
