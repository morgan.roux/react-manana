import { FieldsOptions } from '../../interfaces';
/**
 * Search fields
 *
 */
export const SEARCH_FIELDS_OPTIONS: FieldsOptions[] = [
  {
    name: 'nomLabo',
    label: 'Nom Laboratoire',
    value: '',
    type: 'Search',
    placeholder: 'Contenant...',
    ordre: 1,
  },
  {
    name: 'laboSuite.adresse',
    label: 'Adresse',
    value: '',
    type: 'Search',
    placeholder: 'Commençant par...',
    filterType: 'StartsWith',
    ordre: 2,
  },
  {
    name: 'laboSuite.codePostal',
    label: 'Code Postal',
    value: '',
    type: 'Search',
    placeholder: 'Contenant...',
    ordre: 3,
  },
  {
    name: 'laboSuite.ville',
    label: 'Ville',
    value: '',
    type: 'Search',
    placeholder: 'Contenant...',
    ordre: 4,
  },
  {
    name: 'laboSuite.telephone',
    label: 'Telephone',
    value: '',
    type: 'Search',
    placeholder: 'Contenant...',
    extraNames: ['adresse2'],
    ordre: 5,
  },
];
