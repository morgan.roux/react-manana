import React, { FC } from 'react';
import useStyles from './styles';
/*import { useValueParameterAsBoolean } from '../../../../../utils/getValueParameter';
import {
  PHARMA_FILTER_PHARMACIE_NAME_CODE,
  PHARMA_FILTER_TITULAIRE_CODE,
  PHARMA_FILTER_SORTIE_CODE,
  PHARMA_FILTER_CODE_ENSEIGNE_CODE,
  PHARMA_FILTER_DEPARTEMENT_CODE,
  PHARMA_FILTER_STATUS_CODE,
  PHARMA_FILTER_CODE_IVRY_LAB_CODE,
  PHARMA_FILTER_TELEPHONE_CODE,
  PHARMA_FILTER_CIP_CODE,
  PHARMA_FILTER_VILLE_CODE,
  PHARMA_FILTER_CODE_POSTAL_CODE,
  PHARMA_FILTER_ADRESSE_CODE,
  PHARMA_FILTER_PRESIDENT_REGION_CODE,
  PHARMA_FILTER_CONTRAT_CODE,
  PHARMA_FILTER_SIRET_CODE,
  PHARMA_FILTER_REGION_CODE,
  PHARMA_FILTER_TRANCHE_CA_CODE,
  PHARMA_FILTER_COMMERCIALE_CODE,
  SEARCH_FIELDS_OPTIONS,
} from '../../../CustomPharmacie/constants';
import { FieldsOptions } from '../../../CustomContent/interfaces';
import SearchFilter from '../../../newCustomContent/SearchFilter';
*/
interface PharmacieFilterProps {
  fieldsState: any;
  handleFieldChange: (event: any) => void;
  handleRunSearch: (query: any) => void;
  handleResetFields: () => void;
}

const PharmacieFilter: FC<PharmacieFilterProps> = ({
  fieldsState,
  handleFieldChange,
  handleRunSearch,
  handleResetFields,
}) => {
  const classes = useStyles({});

  /* const objectState = {
    nomState: useValueParameterAsBoolean(PHARMA_FILTER_PHARMACIE_NAME_CODE),
    titulairesState: useValueParameterAsBoolean(PHARMA_FILTER_TITULAIRE_CODE),
    sortieState: useValueParameterAsBoolean(PHARMA_FILTER_SORTIE_CODE),
    codeEnseigneState: useValueParameterAsBoolean(PHARMA_FILTER_CODE_ENSEIGNE_CODE),
    departementState: useValueParameterAsBoolean(PHARMA_FILTER_DEPARTEMENT_CODE),
    activedState: useValueParameterAsBoolean(PHARMA_FILTER_STATUS_CODE),
    numIvrylabState: useValueParameterAsBoolean(PHARMA_FILTER_CODE_IVRY_LAB_CODE),
    tele1State: useValueParameterAsBoolean(PHARMA_FILTER_TELEPHONE_CODE),
    cipState: useValueParameterAsBoolean(PHARMA_FILTER_CIP_CODE),
    villeState: useValueParameterAsBoolean(PHARMA_FILTER_VILLE_CODE),
    cpState: useValueParameterAsBoolean(PHARMA_FILTER_CODE_POSTAL_CODE),
    adresse1State: useValueParameterAsBoolean(PHARMA_FILTER_ADRESSE_CODE),
    presidentRegionState: useValueParameterAsBoolean(PHARMA_FILTER_PRESIDENT_REGION_CODE),
    contratState: useValueParameterAsBoolean(PHARMA_FILTER_CONTRAT_CODE),
    siretState: useValueParameterAsBoolean(PHARMA_FILTER_SIRET_CODE),
    regionState: useValueParameterAsBoolean(PHARMA_FILTER_REGION_CODE),
    trancheCAState: useValueParameterAsBoolean(PHARMA_FILTER_TRANCHE_CA_CODE),
    commercialeState: useValueParameterAsBoolean(PHARMA_FILTER_COMMERCIALE_CODE),
  };

  const filterSearchFields = (objectState: any, searchFields: FieldsOptions[]): FieldsOptions[] => {
    const activeStateKeys = Object.keys(objectState).filter(key => objectState[key] === true);
    const activeSearchFields = searchFields.filter((field: FieldsOptions) =>
      activeStateKeys.includes(`${field.name}State`),
    );
    return activeSearchFields;
  };

  const activeSearcFields: FieldsOptions[] = filterSearchFields(objectState, SEARCH_FIELDS_OPTIONS);

  return (
    <SearchFilter
      fieldsState={fieldsState}
      handleFieldChange={handleFieldChange}
      searchInputs={activeSearcFields}
      labelWidth={55}
      handleRunSearch={handleRunSearch}
      handleResetFields={handleResetFields}
    />
  );*/

  return null;
};

export default PharmacieFilter;
