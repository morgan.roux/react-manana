import React, { FC, ReactNode } from 'react';
import useStyles from './styles';
import SearchInput from '../SearchInput';
import classnames from 'classnames';
import { Typography, IconButton, Box } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import { withRouter, RouteComponentProps } from 'react-router';
interface SubToolbarProps {
  children?: ReactNode;
  searchPlaceholder?: string;
  dark?: boolean;
  title?: string;
  titleBack?: string;
  withBackBtn?: boolean;
  backBtnIcon?: ReactNode;
  onClickBack?: () => void;
}

const SubToolbar: FC<SubToolbarProps & RouteComponentProps> = ({
  children,
  searchPlaceholder,
  dark,
  title,
  withBackBtn,
  onClickBack,
  titleBack,
  backBtnIcon,
  history: { push },
}) => {
  const classes = useStyles({});

  const goBack = () => {
    onClickBack ? onClickBack() : push('/');
  };

  return (
    <Box className={classnames(classes.searchBar, dark ? classes.dark : '')}>
      {withBackBtn && (
        <Box display="flex" alignItems="center">
          <IconButton
            color="inherit"
            onClick={goBack}
            style={{ position: 'absolute', left: 12, marginRight: title ? 15 : 'auto' }}
          >
            {backBtnIcon ? backBtnIcon : <ArrowBack />}
          </IconButton>
          <Typography>{titleBack}</Typography>
          {title && searchPlaceholder && <Typography className={classes.title}>{title}</Typography>}
        </Box>
      )}
      {searchPlaceholder && (
        <Box className={classes.search}>
          <SearchInput searchPlaceholder={searchPlaceholder || 'Rechercher ...'} />
        </Box>
      )}
      {title && !searchPlaceholder && <Typography className={classes.title}>{title}</Typography>}
      <Box className={classes.childrenContainer}>{children}</Box>
    </Box>
  );
};

export default withRouter(SubToolbar);
