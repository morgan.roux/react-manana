import { Loader } from '@lib/ui-kit';
import { Avatar, Box, Fab } from '@material-ui/core';
import React, { FC, ReactElement, ReactNode, useEffect, useState } from 'react';
import InfintyScroll from 'react-infinite-scroll-component';
import { useStyles } from './styles';
export interface ListPersonProps {
  listes: any;
  actionsRenderer: (user: any) => ReactNode;
  onNext: () => void;
  onClickItem?: (data: any) => void;
  loading: boolean;
  onscroll?: () => void;
  fab?: {
    variant: 'round' | 'extended';
    color?: 'inherit' | 'primary' | 'secondary' | 'default';
    icon: ReactElement;
    onClick: () => void;
  };
}
const ListPerson: FC<ListPersonProps> = ({ actionsRenderer, onClickItem, listes, onNext, loading, fab, onscroll }) => {
  const classes = useStyles();
  const [data, setData] = useState<any>([]);
  useEffect(() => {
    setData(listes);
  }, [listes]);

  return (
    <Box id="scroll" className={classes.root}>
      {fab && (
        <Fab onClick={fab.onClick} className={classes.fab} color={fab.color} variant={fab.variant}>
          {fab.icon}
        </Fab>
      )}
      {loading && <Loader />}
      <InfintyScroll
        onScroll={onscroll}
        scrollableTarget="scroll"
        dataLength={data.length}
        next={onNext}
        hasMore
        loader={''}
      >
        {data.map((item: any, i: number) => (
          <Box key={i} className={classes.rowContent} onClick={() => onClickItem && onClickItem(item)}>
            <Box display="flex" alignItems="center">
              <Box>
                <Avatar alt={item.nom} src={item && item.src} className={classes.avatar} />
              </Box>
              <Box className={classes.description}>
                <Box className={classes.title}>{item.nom}</Box>
                {item.autres &&
                  item.autres.map((info: any, key: number) => (
                    <Box key={key} className={classes.content}>
                      {info}
                    </Box>
                  ))}
              </Box>
            </Box>
            {actionsRenderer && <Box marginLeft="auto">{actionsRenderer(item)}</Box>}
          </Box>
        ))}
      </InfintyScroll>
    </Box>
  );
};
export default ListPerson;
