import React , { useState } from 'react';
import Dropzone from './Dropzone';

export default {
    title: 'Design system|Atoms/Dropzone',
    parameters: {
        info: { inline: true }
    }
};

export const DropzoneStory = () => {
    const [ selectedFile , setSelectedFiles ] = useState<File []>([]);

    return (
        <Dropzone
          contentText="Glissez et déposez votre document ici"
          selectedFiles={selectedFile}
          setSelectedFiles={setSelectedFiles}
          multiple={false}
          acceptFiles="application/pdf"
        />
    );
};
