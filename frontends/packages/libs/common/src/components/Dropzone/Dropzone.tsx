import React, { FC, useState, useEffect } from 'react';
import { useDropzone } from 'react-dropzone';
import useStyles from './style';
import FileIcon from '@material-ui/icons/InsertDriveFile';
import {
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Link,
  Typography,
  Box,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { AxiosResponse } from 'axios';
import classnames from 'classnames';
import { last } from 'lodash';
import { NewCustomButton } from '@lib/ui-kit/src/components/atoms/CustomButton';
import { v4 as uuidv4 } from 'uuid';
import { UploadedFile, useApplicationContext } from '../../context';

import { useTheme } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
// import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';

export interface DropzoneProps {
  idSource?: string;
  startUpload?: boolean;
  contentText?: string;
  acceptFiles?: string;
  multiple?: boolean;
  disabled?: boolean;
  setStartUpload?: (state: boolean) => void;
  setFileUploadResult?: (
    result: AxiosResponse<any | null>
  ) => void | React.Dispatch<React.SetStateAction<AxiosResponse<any> | null>>;
  setSelectedFiles?: (files: (File | UploadedFile)[]) => void;
  selectedFiles?: (File | UploadedFile)[];
  withImagePreview?: boolean;
  uploadDirectory?: string;
  identifierPrefix?: string;
  setFilesOut?: React.Dispatch<React.SetStateAction<(File | UploadedFile)[]>>;
  fileAlreadySetUrl?: string;
  setFileAlreadySet?: React.Dispatch<React.SetStateAction<string | undefined>>;
  customLabel?: string;
  where?: string;
  onClickDelete?: () => void;
  onClickResize?: () => void;
  previewUrl?: string;
  withFileIcon?: boolean;
  withImagePreviewCustomized?: boolean;
  capture?: any;
  className?: string;
}

const Dropzone: FC<DropzoneProps> = ({
  startUpload,
  // setStartUpload,
  // setFileUploadResult,
  contentText,
  acceptFiles,
  setSelectedFiles,
  selectedFiles,
  multiple,
  withImagePreview,
  uploadDirectory,
  identifierPrefix,
  setFilesOut,
  fileAlreadySetUrl,
  setFileAlreadySet,
  customLabel,
  disabled,
  where,
  onClickDelete,
  onClickResize,
  previewUrl,
  withFileIcon,
  withImagePreviewCustomized,
  capture,
  className,
}) => {
  const classes = useStyles();
  const [files, setFiles] = useState<(File | UploadedFile)[]>([]);
  const { computeUploadedFilename } = useApplicationContext();
  // const [oneFileError, setOneFileError] = useState<boolean>(false);

  const inUserSettings = where === 'inUserSettings';
  const isInPubForm = where === 'inPubForm' || where === 'inProduitForm';

  const onDrop = (acceptedFiles: (File | UploadedFile)[]) => {
    setFiles([...files, ...acceptedFiles]);
    if (setFilesOut) setFilesOut([...files, ...acceptedFiles]);
    if (setSelectedFiles && files) {
      setSelectedFiles([...files, ...acceptedFiles]);
    }
    if (setFileAlreadySet) setFileAlreadySet(undefined);
  };

  const { getRootProps, getInputProps, isDragActive, acceptedFiles, open, isDragAccept, isDragReject } = useDropzone({
    disabled,
    noClick: true,
    noKeyboard: true,
    multiple: multiple !== undefined ? multiple : false,
    onDrop,
    accept:
      acceptFiles ||
      'image/*, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf, application/vnd.ms-powerpoint, application/vnd.openxmlformats-officedocument.presentationml.presentation, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  });

  const removeFile = (file: File | UploadedFile) => () => {
    const newFiles = [...files];
    newFiles.splice(newFiles.indexOf(file), 1);
    acceptedFiles.splice(acceptedFiles.indexOf(file as any), 1);
    setFiles(newFiles);
    if (setFilesOut) setFilesOut(newFiles);
    if (setSelectedFiles) {
      setSelectedFiles(newFiles);
    }
  };

  const removeAllFile = () => {
    const newFiles: any[] = [];
    setFiles(newFiles);
    if (setFilesOut) setFilesOut(newFiles);
    if (setSelectedFiles) {
      setSelectedFiles(newFiles);
    }
    if (fileAlreadySetUrl && setFileAlreadySet) {
      setFileAlreadySet(undefined);
    }
  };

  useEffect(() => {
    if (selectedFiles) {
      setFiles(selectedFiles);
      if (setFilesOut) setFilesOut(selectedFiles);
    }
  }, [selectedFiles]);

  const FILE_EXIST = !!(fileAlreadySetUrl || (selectedFiles && selectedFiles.length > 0));
  const CUSTOMIZED_FILE_CONTENAIRE = !!((inUserSettings || withImagePreviewCustomized) && FILE_EXIST);

  const filesList = files.map((file: any) => {
    return (
      <ListItem
        key={file.name || file.nomOriginal}
        className={
          CUSTOMIZED_FILE_CONTENAIRE
            ? classnames(classes.fileContainer, classes.fileContainerCustomized)
            : classes.fileContainer
        }
      >
        {withImagePreview && (
          <img
            className={withImagePreviewCustomized ? classes.previewImageCustomized : classes.previewImage}
            // src={previewUrl ? previewUrl : URL.createObjectURL(file)}
            src={previewUrl && previewUrl}
            alt=""
          />
        )}

        {!withImagePreviewCustomized && (
          <>
            <ListItemText>{file.name || (file as any).nomOriginal}</ListItemText>
            <ListItemSecondaryAction className={classes.closeIcon}>
              <IconButton
                edge="end"
                aria-label="delete"
                size="small"
                className={classes.btnIcon}
                onClick={removeFile(file)}
              >
                <CloseIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </>
        )}
      </ListItem>
    );
  });

  const initFileAlreadySet = () => {
    if (setFileAlreadySet) setFileAlreadySet(undefined);
  };

  const ImagePreview = () => {
    return (
      <ListItem
        key={uuidv4()}
        className={
          CUSTOMIZED_FILE_CONTENAIRE
            ? classnames(classes.fileContainer, classes.fileContainerCustomized)
            : classes.fileContainer
        }
      >
        {withImagePreview && (
          <img
            className={withImagePreviewCustomized ? classes.previewImageCustomized : classes.previewImage}
            src={previewUrl || fileAlreadySetUrl || ''}
            alt=""
          />
        )}
        {!withImagePreviewCustomized && (
          <ListItemSecondaryAction className={classes.closeIcon}>
            <IconButton
              edge="end"
              aria-label="delete"
              size="small"
              className={classes.btnIcon}
              onClick={initFileAlreadySet}
            >
              <CloseIcon />
            </IconButton>
          </ListItemSecondaryAction>
        )}
      </ListItem>
    );
  };

  /* const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: data => {
      // On Complete presigned url, Upload to S3
      acceptedFiles.map(file => {
        if (data && data.createPutPresignedUrls) {
          data.createPutPresignedUrls.map((item : any) => {
            if (item && item.filePath === formatFilename(file, uploadDirectory, identifierPrefix)) {
              uploadToS3(file, item.presignedUrl)
                .then((result : any) => {
                  if (result && result.status === 200) {
                    if (setStartUpload && setFileUploadResult) {
                      setStartUpload(false);
                      setFileUploadResult(result);
                    }
                  }
                })
                .catch((error : any) => {
                  console.log(error);
                });
            }
          });
        }
      });
    },
  }); */

  // If startUpload true, start upload
  useEffect(() => {
    if (startUpload) {
      const filePathsTab: string[] = [];
      acceptedFiles.map((file: File) => {
        filePathsTab.push(computeUploadedFilename(file, { directory: uploadDirectory, prefix: identifierPrefix }));
      });
      // Create presigned url
      // Appel de la fonction en paramètre
      // doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
    }
  }, [startUpload]);

  const handleClickDelete = () => {
    removeAllFile();
    if (onClickDelete) {
      onClickDelete();
    }
  };

  const NoFileInDropzone = () => {
    const theme = useTheme();
    const match = useMediaQuery(theme.breakpoints.down('sm'));

    const label = contentText || customLabel || '';
    return (
      <>
        {withFileIcon && !selectedFiles?.length && <FileIcon />}
        {match ? (
          <Box>
            <span>{contentText || customLabel || ''}</span>
            {!selectedFiles?.length && (
              <Link component="button" variant="body2" onClick={open} className={classes.btnLink}>
                Importer vos documents
              </Link>
            )}
          </Box>
        ) : (
          <>
            <span>{contentText || customLabel || 'Glissez-déposer des fichiers ici'}</span>
            <span>ou</span>
            <Link component="button" variant="body2" onClick={open} className={classes.btnLink}>
              Parcourir
            </Link>
          </>
        )}
      </>
    );
  };

  const UserSettingsBtns = () => {
    return (
      <>
        <div className={classes.btnsActionsContainer}>
          {/* <CustomButton
            color="default"
            className={classnames(classes.btn, classes.btnDelete)}
            onClick={handleClickDelete}
          >
            Supprimer
          </CustomButton> */}
          <div className={classes.btnsEditContainer}>
            <NewCustomButton className={classes.btn} onClick={open}>
              Modifier
            </NewCustomButton>
            <NewCustomButton className={classes.btn} onClick={onClickResize}>
              Redimensionner
            </NewCustomButton>
          </div>
        </div>
      </>
    );
  };

  const PubFormBtns = () => {
    return (
      <>
        <div className={classes.btnsEditContainer}>
          <NewCustomButton
            theme="transparent"
            className={classnames(classes.btn, classes.btnDelete)}
            onClick={handleClickDelete}
          >
            Supprimer
          </NewCustomButton>
          <NewCustomButton className={classes.btn} onClick={open}>
            Modifier
          </NewCustomButton>
        </div>
      </>
    );
  };

  return (
    <div
      className={
        disabled
          ? classnames(classes.container, classes.containerDisabled, className)
          : classnames(classes.container, className)
      }
    >
      <div {...(getRootProps() as any)} className={classes.dropzone}>
        <input {...(getInputProps() as any)} />
        {isDragAccept && <p>Déposez les fichiers ici ...</p>}
        {isDragReject && <p>Certains fichiers seront rejetés</p>}
        {!isDragActive && (
          <>
            {inUserSettings && FILE_EXIST && (
              <Typography className={classes.previewText}>Aperçu de la photo redimensionnée</Typography>
            )}
            <div
              className={
                (inUserSettings || withImagePreviewCustomized) && FILE_EXIST
                  ? classnames(classes.layout, classes.layoutFlexRow)
                  : classes.layout
              }
            >
              {!multiple ? fileAlreadySetUrl ? <ImagePreview /> : last(filesList) : null}
              {(!FILE_EXIST || (!inUserSettings && !withImagePreviewCustomized)) && <NoFileInDropzone />}
              {inUserSettings && FILE_EXIST && <UserSettingsBtns />}
              {isInPubForm && FILE_EXIST && <PubFormBtns />}
            </div>
          </>
        )}
      </div>
      {/* oneFileError && (
        <Box textAlign="center">
          <Typography className={classes.btnLink}>
            <AlarmIcon /> Accepter un seul fichier
          </Typography>
        </Box>
      ) */}
      {multiple ? (
        <aside className={classes.filesList}>
          <List dense>{filesList}</List>
        </aside>
      ) : null}
    </div>
  );
};

Dropzone.defaultProps = {
  withFileIcon: true,
};

export default Dropzone;
