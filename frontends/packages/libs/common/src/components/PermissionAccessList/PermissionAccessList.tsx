import { useSearch_Permissions_AccessQuery } from '@lib/common/src/graphql';
import { NewCustomButton } from '@lib/ui-kit';
import CustomCheckbox from '@lib/ui-kit/src/components/atoms/CustomCheckBox/CustomCheckbox';
import { LinearProgress, Typography } from '@material-ui/core';
import React, { Dispatch, FC, MouseEvent, SetStateAction } from 'react';
import { useHistory } from 'react-router-dom';
import { useApplicationContext, useDisplayNotification } from '@lib/common';
import CustomContent, { SearchInput } from '../newCustomContent';
import { BuildQuery } from '../withSearch/queryBuilder';
import useStyles from './styles';

interface PermissionAccessList {
  selected: any[];
  setSelected: Dispatch<SetStateAction<any[]>>;
  withTitle?: boolean;
  withSubTitle?: boolean;
  withInputSearch?: boolean;
  rolesCodes?: string[];
  withFilterByGroupement?: boolean;
  withAddBtn?: boolean;
}

const PermissionAccessList: FC<PermissionAccessList> = ({
  selected,
  setSelected,
  withTitle,
  withSubTitle,
  withInputSearch,
  rolesCodes,
  withFilterByGroupement,
  withAddBtn,
}) => {
  const classes = useStyles({});
  const { currentGroupement, graphql } = useApplicationContext();
  const idGroupement = currentGroupement.id;
  const { push } = useHistory();
  const displayNotification = useDisplayNotification();

  const selectedCodes = (selected && selected.map((item: any) => item && item.code)) || [];
  const isSelected = (code: string | number) => selectedCodes.indexOf(code) !== -1;

  const type = 'traitement';

  const rolesMust = rolesCodes ? [{ terms: { 'roles.code': rolesCodes } }] : [];
  const groupementMust = withFilterByGroupement ? [{ terms: { idGroupements: [idGroupement] } }] : [];
  const must = [...rolesMust, ...groupementMust];

  const sort = [{ nom: { order: 'asc' } }];

  const { query, skip, take } = BuildQuery({ type, optionalMust: must, sort });

  const handleCheck = (row: any, rowCode: string) => (event: MouseEvent<unknown>) => {
    event.preventDefault();
    event.stopPropagation();

    const selectedIndex = selectedCodes.indexOf(rowCode);
    let newSelected: string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, row);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1));
    }

    setSelected(newSelected);
  };

  const goToRoles = () => push('/db/roles');

  const variables = { take, skip, type: [type], query };

  /**
   * Get permission access list
   */
  const searchingPermissionAccess = useSearch_Permissions_AccessQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
    variables,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  return (
    <div className={classes.infoPersoContainer}>
      {withTitle && <Typography className={classes.inputTitle}>Droits d'accès</Typography>}
      {withSubTitle && (
        <Typography className={classes.permissionAccessSubTite}>
          Choisissez les accès auxquels l'utilisateur aura droit
        </Typography>
      )}
      {withInputSearch && <SearchInput searchPlaceholder="Rechercher" />}
      <LinearProgress
        color="secondary"
        style={{
          width: '100%',
          height: 3,
          marginBottom: 20,
          visibility: searchingPermissionAccess.loading ? 'visible' : 'hidden',
        }}
      />
      {(searchingPermissionAccess.data?.search?.data || []).length === 0 && !searchingPermissionAccess.loading ? (
        <div className={classes.noDataContainer}>
          <span>Aucun droit d'accès trouvé</span>
          {withAddBtn && (
            <NewCustomButton variant="outlined" onClick={goToRoles}>
              Ajouter des droits
            </NewCustomButton>
          )}
        </div>
      ) : (
        <>
          <div className={classes.inputsContainer}>
            <div className={classes.permissionAccessFormRow}>
              {(searchingPermissionAccess.data?.search?.data || []).map((i: any, index) => {
                const isItemSelected = isSelected(i.code);
                const labelId = `permission-access-list-checkbox-${index}`;
                return (
                  <CustomCheckbox
                    key={`permission_access_${index}`}
                    label={i.nom}
                    checked={isItemSelected}
                    inputProps={{ 'aria-labelledby': labelId }}
                    onClick={handleCheck(i, i.code)}
                    color="secondary"
                  />
                );
              })}
            </div>
          </div>
          <div className={classes.paginationContainer}>
            <CustomContent
              selected={selected}
              setSelected={setSelected}
              listResult={searchingPermissionAccess.data?.search?.data || []}
            />
          </div>
        </>
      )}
    </div>
  );
};

PermissionAccessList.defaultProps = {
  withTitle: true,
  withSubTitle: true,
  withInputSearch: true,
  withFilterByGroupement: true,
  withAddBtn: true,
};

export default PermissionAccessList;
