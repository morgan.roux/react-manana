import {
  useSearch_Prt_ContactQuery,
  Search_Prt_ContactQuery,
  Search_Prt_ContactQueryVariables,
} from '../../federation';
import { useSearch_UsersQuery, Search_UsersQuery, Search_UsersQueryVariables } from '../../graphql';
import { BuildQuery } from '../withSearch/queryBuilder';
import { Category } from './ListUser';
import { useApplicationContext } from '@lib/common';
import { QueryResult } from '@apollo/client';

interface UseParticipantsOptions {
  category?: Category;
  partenaireType?: string;
  idPartenaireTypeAssocie?: string;
  idParticipants?: string[];
  skip?: number;
  take?: number;
}

interface UseParticipantsOptionsResult {
  take: number;
  skip: number;
  optionalMust?: any[];
  optionalShould?: any[];
  result:
    | QueryResult<Search_UsersQuery, Search_UsersQueryVariables>
    | QueryResult<Search_Prt_ContactQuery, Search_Prt_ContactQueryVariables>;
}

export const useParticipants = ({
  category,
  partenaireType,
  idPartenaireTypeAssocie,
  idParticipants,
  skip: skipProp,
  take: takeProp,
}: UseParticipantsOptions): UseParticipantsOptionsResult => {
  const { currentPharmacie, user, graphql, federation, isGroupePharmacie } = useApplicationContext();
  let pharmacie = '';
  let service = '';
  if (user?.role?.groupe === 'GROUPEMENT') {
  } else if (user?.role?.groupe === 'PHARMACIE') {
    pharmacie = currentPharmacie.id;
  }

  const optionalShould =
    category === 'CONTACT'
      ? currentPharmacie.departement?.id
        ? [
            {
              term: {
                idPharmacie: currentPharmacie.id,
              },
            },
            {
              term: {
                'departements.id': currentPharmacie.departement?.id,
              },
            },
          ]
        : [
            {
              term: {
                idPharmacie: currentPharmacie.id,
              },
            },
          ]
      : undefined;

  const optionalMust =
    category === 'CONTACT'
      ? [
          /*{
          term: {
            idPharmacie: currentPharmacie.id,
          },
        },*/
          {
            term: {
              idPartenaireTypeAssocie: idPartenaireTypeAssocie,
            },
          },
          {
            term: {
              partenaireType: partenaireType,
            },
          },
        ]
      : idParticipants && idParticipants.length
      ? [
          {
            terms: {
              _id: idParticipants,
            },
          },
        ]
      : !isGroupePharmacie
      ? [
          {
            term: {
              'userPersonnel.service.id': service,
            },
          },
          {
            term: {
              'userPersonnel.service.id': service,
            },
          },
        ]
      : isGroupePharmacie
      ? [
          {
            term: {
              'pharmacie.id': pharmacie,
            },
          },
          {
            term: {
              'pharmacie.id': pharmacie,
            },
          },
          {
            terms: {
              status: ['ACTIVATED', 'ACTIVATION_REQUIRED', 'RESETED'],
            },
          },
        ]
      : [];

  const type = category === 'USER' ? 'user' : 'prtcontact';

  const { query, take, skip } = BuildQuery({
    type,
    optionalMust,
    optionalShould,
    minimumShouldMatch: optionalShould ? 1 : undefined,
  });

  const variables = { take: takeProp ?? take, skip: skipProp ?? skip, query };

  const searchUsersResult = useSearch_UsersQuery({
    // fetchPolicy: 'cache-and-network',
    client: graphql,
    variables: {
      ...variables,
      type: ['user'],
    } as any,
    skip: category !== 'USER',
  });

  const searchContactsResult = useSearch_Prt_ContactQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
    variables: {
      ...variables,
      index: 'prtcontact',
    },
    skip: category !== 'CONTACT' || !idPartenaireTypeAssocie,
  });
  return {
    optionalMust,
    optionalShould,
    skip,
    take,
    result: category === 'USER' ? searchUsersResult : searchContactsResult,
  };
};
