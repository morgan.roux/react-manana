import { createStyles, Theme } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    usersModalRoot: {
      width: '100%',
      '& .MuiButton-label': {
        zIndex: 'auto',
      },
    },
    tableContainer: {
      width: '100%',
      marginTop: 20,
    },
    espaceHaut: {
      margin: '50px 0',
    },
    espacedroite: {
      marginRight: '12px',
    },
    section: {
      margin: 15,
    },
    inputText: {
      width: '100%',
      marginBottom: 12,
      '& .MuiOutlinedInput-root': {
        '&.Mui-focused': {
          '& .MuiOutlinedInput-notchedOutline': {
            borderColor: 'rgba(0, 0, 0, 0.23)!important',
          },
        },
      },
      '& .MuiSelect-selectMenu': {
        fontSize: '0.875rem',
      },
      '& .MuiOutlinedInput-input': { fontSize: '0.875rem' },
      '& .MuiFormLabel-root': {
        color: '#878787',
        fontSize: '0.875rem',
      },
    },

    contentDatePicker: {
      '& .MuiGrid-root': {
        position: 'relative',
      },
      '& .MuiInputLabel-formControl': {
        position: 'initial',
        transform: 'translate(0, 14px) scale(1)',
      },
      '& .MuiTextField-root': {
        width: '100%',
      },
      '& .MuiIconButton-root': {
        padding: 4,
        marginBottom: 10,
      },
      '& .MuiFormLabel-root': {
        color: '#B1B1B1',
      },

      '& .MuiFormControl-root': {
        padding: '7px 14px !important',
        borderStyle: 'solid',
        borderWidth: 1,
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.23)',
        marginTop: 0,
      },
      '& .MuiInput-underline:before': {
        display: 'none',
      },
      '& .MuiInputLabel-shrink': {
        transform: 'translate(0, -13.5px) scale(0.75)',
        background: '#ffffff',
        width: 152,
        paddingLeft: 6,
      },
      '& .MuiInputBase-input': {
        padding: '0 0 10px',
      },
    },
    dateSelectNone: {
      position: 'absolute',
      width: '95%',
      height: '100%',
      left: 0,
      zIndex: 2,
    },
    iconCustom: {
      backgroundColor: 'white',
      color: 'black',
      width: theme.spacing(7),
      height: theme.spacing(7),
    },
    label: {
      marginTop: 17,
      fontSize: '1.1rem',
    },
    description: {
      display: 'flex',
      flexDirection: 'column',
    },
    avatar: {
      width: theme.spacing(7),
      height: theme.spacing(7),
      marginRight: 20,
    },

    title: {
      fontWeight: 'bold',
      fontSize: '1.1rem',
    },
    content: {
      fontSize: '0.7rem',
    },
    icon: {
      marginTop: 15,
    },
    search: {
      borderColor: 'black',
    },
    rootListParticipant: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
    },
    contentRootListParticipant: {
      display: 'flex',
      alignItems: 'center',
      width: '100%',
      padding: '9px 0',
      justifyContent: 'space-between',
    },
  })
);
