import { useApplicationContext } from '@lib/common';
import { CustomModal } from '@lib/ui-kit';
import { Avatar, Box, Checkbox, Fab, Grid } from '@material-ui/core';
import AssignmentLateIcon from '@material-ui/icons/AssignmentLate';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import _ from 'lodash';
import React, { Dispatch, FC, Fragment, ReactNode, SetStateAction, useEffect, useState } from 'react';
import { useFilters } from '../../shared';
import { useGet_Search_User_IdsQuery } from '../../graphql';
import SearchInput from '../newCustomContent/SearchInput';
import { UserSelect } from '../User/UserSelect';
import ListUser, { Category } from './ListUser';
import { useStyles } from './style';
import CustomSelectTask from './Task/CustomSelectTask';
import { useParticipants } from './useParticipants';

const projectList = [
  { label: 'Tous les types', value: 'Tous' },
  { label: 'Pharmacie', value: 'pharmacie' },
  { label: 'Présidents de régions', value: 'userPartenaire' },
  { label: 'Partenaires de services', value: 'userLaboratoire' },
];
export interface UsersModalProps {
  useInCall?: boolean;
  category?: Category;
  // Pour les contacts
  partenaireType?: string;
  idPartenaireTypeAssocie?: string;
  onValidate?: (users: any[]) => void;
  page?: boolean;
  openModal: boolean;
  setOpenModal: Dispatch<SetStateAction<boolean>>;
  withNotAssigned?: boolean;
  withAssignTeam?: boolean;
  assignTeamText?: string;
  selected?: any[];
  idParticipants?: string[];
  setSelected?: Dispatch<SetStateAction<any[]>>;
  singleSelect?: boolean;
  searchPlaceholder?: string;
  title?: string;
  isCheckedTeam?: boolean;
  setIsCheckedTeam?: Dispatch<SetStateAction<boolean>>;
  moreContent?: ReactNode;
  optionPartage?: string;
  defaultValueMe?: boolean;
}

const CustomSelectUser: FC<UsersModalProps> = ({
  openModal,
  useInCall,
  page,
  category = 'USER',
  idPartenaireTypeAssocie,
  partenaireType,
  setOpenModal,
  withNotAssigned = true,
  withAssignTeam = true,
  idParticipants,
  selected,
  setSelected,
  singleSelect,
  title = 'Assignation de participants',
  searchPlaceholder = 'Rechercher un participant...',
  assignTeamText = 'Assignée à toute l’équipe',
  isCheckedTeam,
  setIsCheckedTeam,
  moreContent,
  optionPartage,
  onValidate,
  defaultValueMe,
}) => {
  const [currentSelected, setCurrentSelected] = React.useState<any[]>([]);
  const { user, graphql, notify, isMobile } = useApplicationContext();
  const { setPagination } = useFilters();
  //const [pagination, setPagination] = useState<{ skip: number; take: number }>({ skip: 0, take: 12 });
  const optionTout = 'TOUT_LE_MONDE';

  useEffect(() => {
    setCurrentSelected(selected || []);
  }, [selected]);

  const currentUserId = user.id;

  const [open, setOpen] = useState<boolean>(false);
  const [userIds, setUserIds] = useState<any[]>([]);
  const [filtersModal, setFiltersModal] = useState<string[]>(['ALL']);

  useEffect(() => {
    setOpen(openModal);
  }, [openModal]);

  useEffect(() => {
    if (defaultValueMe && category === 'USER' && !selected?.length) {
      setSelected && setSelected([user]);
    }
  }, []);

  const classes = useStyles({});

  const handleToogleCheckUser = (user: any, event: any) => {
    event.stopPropagation();
    setCurrentSelected((prev) => {
      if (prev.some(({ id }) => id === user.id)) {
        const next = prev.filter((currentUser) => currentUser.id !== user.id);
        return next;
      } else {
        const next = [...prev, user];
        return next;
      }
    });
  };

  const handleChange = (event: any) => {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
      // TODO : Make it operational

      //  const { name, value } = event.target;
      // setDataParticipantList({ ...dataParticipantList, [name]: value });
    }
  };

  const {
    result: listResult,
    skip,
    take,
    optionalMust,
  } = useParticipants({
    category,
    partenaireType,
    idPartenaireTypeAssocie,
    idParticipants,
  });

  const search = useGet_Search_User_IdsQuery({
    client: graphql,
    variables: {
      query: {
        query: {
          bool: {
            must: optionalMust,
          },
        },
      },
    },
    skip: category !== 'USER' || !optionalMust?.length,
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  const ids: any[] = search?.data?.search?.data ? search.data.search.data.map((user: any) => user && user.id) : [];

  const allUsers: any[] = search?.data?.search?.data || [];

  const handleSelectAll = (event: any) => {
    event.preventDefault();
    event.stopPropagation();
    if (currentSelected.length < allUsers.length - 1) {
      // !NOUVEAU : NOTION TOUTE L'EQUIPE
      setCurrentSelected(allUsers.filter((i) => i.id !== currentUserId));
      if (setIsCheckedTeam) setIsCheckedTeam(true);
    } else {
      setCurrentSelected([]);
      if (setIsCheckedTeam) setIsCheckedTeam(false);
    }
  };

  const handleSelectOne = (user: any, event: any) => {
    event.preventDefault();
    event.stopPropagation();
    if (singleSelect) {
      setCurrentSelected([user]);
    } else {
      const selectedIds = currentSelected.map((i) => i.id);
      if (selectedIds.includes(user.id)) {
        const newSelected = currentSelected.filter((u) => u.id !== user.id);
        setCurrentSelected(newSelected);
      } else {
        const newSelected = [...currentSelected, user];
        setCurrentSelected(newSelected);
      }
    }

    if (setIsCheckedTeam) setIsCheckedTeam(false);
  };

  const handleValidate = () => {
    if (setSelected) {
      setSelected(currentSelected);
      onValidate && onValidate(currentSelected);
    }
    setOpenModal(false);
  };

  const handleCustomClose = () => {
    setCurrentSelected(selected || []);
    setOpenModal(false);
  };

  const content = (
    <>
      <Box
        className={classes.tableContainer}
        // tslint:disable-next-line: jsx-no-lambda
        onClick={(event) => {
          event.preventDefault();
          event.stopPropagation();
        }}
        my={2}
      >
        <Box className={classes.section}>
          <Grid container={true} direction={isMobile ? 'column' : 'row'} spacing={3}>
            <Grid item={true} xs={12} lg={8}>
              <SearchInput searchPlaceholder={searchPlaceholder} />
            </Grid>
            {category === 'USER' && !useInCall && (
              <Grid item={true} xs={12} lg={4}>
                <CustomSelectTask
                  label="Type de contact"
                  list={projectList}
                  name="sortBy"
                  className={classes.espaceHaut}
                  onChange={handleChange}
                  disabled={true}
                  value={'pharmacie'}
                />
              </Grid>
            )}
          </Grid>
        </Box>
        {withNotAssigned && (
          <Box className={classes.section}>
            <Grid container={true} alignItems="center" justify="space-between">
              <Grid item={true}>
                <Fab style={{ boxShadow: 'none' }}>
                  <AssignmentLateIcon />
                </Fab>
                <strong className={classes.label}> Non-assignée </strong>
              </Grid>
              <Grid item={true} className={classes.label}>
                <Checkbox
                  icon={<CircleUnchecked fontSize="large" />}
                  checkedIcon={<CircleChecked fontSize="large" />}
                  // tslint:disable-next-line: jsx-no-lambda
                  onChange={() => {
                    setCurrentSelected([]);
                  }}
                  checked={currentSelected.length === 0}
                />
              </Grid>
            </Grid>
          </Box>
        )}
        {withAssignTeam && optionalMust?.length ? (
          <Fragment>
            {
              <Box className={classes.section}>
                <Grid container={true} alignItems="center">
                  <Grid item={true}>
                    <Checkbox
                      icon={<CircleUnchecked fontSize="large" />}
                      checkedIcon={<CircleChecked fontSize="large" />}
                      // tslint:disable-next-line: jsx-no-lambda
                      onClick={(event) => {
                        handleSelectAll(event);
                      }}
                      checked={
                        ids &&
                        ids.length &&
                        _.difference(
                          ids,
                          currentSelected.map(({ id }) => id)
                        ).length === 0
                          ? true
                          : isCheckedTeam
                          ? true
                          : currentSelected && currentSelected.length > 0 && allUsers.length === currentSelected.length
                          ? true
                          : false
                      }
                    />
                  </Grid>
                  <Grid item={true} className={classes.label}>
                    <strong className={classes.label}>{assignTeamText}</strong>
                  </Grid>
                </Grid>
              </Box>
            }
          </Fragment>
        ) : null}
        {moreContent && <Box className={classes.section}>{moreContent}</Box>}
        {user && (!idParticipants || idParticipants.includes(user.id)) && category === 'USER' && (
          <Box
            className={classes.section}
            style={{
              cursor: 'pointer',
              background: user?.id && currentSelected.some(({ id }) => id === user.id) ? '#F8F8F8' : undefined,
            }}
          >
            <Grid
              container={true}
              direction="row"
              alignItems="center"
              justify="space-between"
              wrap="nowrap"
              onClick={(event) => {
                if (singleSelect) {
                  handleSelectOne(user, event);
                } else {
                  handleToogleCheckUser(user, event);
                }
              }}
            >
              <Grid container={true} direction="row" wrap="nowrap">
                <Avatar alt="user" src={user.photo?.publicUrl || ''} className={classes.large} />
                <Grid item={true} className={classes.description}>
                  <Grid item={true} className={classes.title}>
                    Moi
                  </Grid>
                  <Grid item={true} className={classes.content}>
                    {user?.role?.nom}
                  </Grid>
                  <Grid item={true} className={classes.content}>
                    {user?.email}
                  </Grid>
                </Grid>
              </Grid>

              <Grid item={true} className={classes.description}>
                <Checkbox
                  icon={<CircleUnchecked fontSize="large" />}
                  checkedIcon={<CircleChecked fontSize="large" />}
                  // tslint:disable-next-line: jsx-no-lambda
                  onClick={(event) => {
                    if (singleSelect) {
                      handleSelectOne(user, event);
                    } else {
                      handleToogleCheckUser(user, event);
                    }
                  }}
                  checked={!!(user?.id && currentSelected.some(({ id }) => id === user.id))}
                />
              </Grid>
            </Grid>
          </Box>
        )}

        <Box className={classes.section}>
          <ListUser
            onChangePagination={(newPagination) => setPagination(newPagination)}
            listResult={{
              loading: listResult.loading,
              variables: {
                skip,
                take,
              },
              data: { search: { data: listResult.data?.search?.data as any, total: listResult.data?.search?.total } },
            }}
            {...(!useInCall
              ? {
                  category,
                  selected: currentSelected,
                  setSelected: setCurrentSelected,
                  handleToogleCheckUser,
                  handleSelectOne,
                  paginationCentered: true,
                }
              : { handleToogleCheckUser })}
          />
        </Box>
      </Box>
    </>
  );

  const contentToutLeMonde = (
    <Box display="flex" flexDirection="column" width="100%" style={{ padding: '16px 0' }}>
      <UserSelect
        userIdsSelected={userIds}
        setUserIdsSelected={setUserIds}
        usersSelected={currentSelected}
        setUsersSelected={setCurrentSelected}
        filtersSelected={filtersModal}
        setFiltersSelected={setFiltersModal}
        disableAllFilter={false}
        clientContact
      />
    </Box>
  );

  return (
    <>
      {page ? (
        content
      ) : (
        <CustomModal
          open={open}
          setOpen={setOpenModal}
          title={title}
          withBtnsActions={true}
          withCancelButton={true}
          actionButtonTitle="Valider"
          closeIcon={true}
          headerWithBgColor={true}
          maxWidth="md"
          fullWidth={true}
          className={classes.usersModalRoot}
          // tslint:disable-next-line: jsx-no-lambda
          // tslint:disable-next-line: jsx-no-lambda
          onClick={(event) => {
            event.preventDefault();
            event.stopPropagation();
          }}
          disableBackdropClick={true}
          onClickConfirm={handleValidate}
          customHandleClose={handleCustomClose}
          key={category}
        >
          {optionPartage === optionTout ? contentToutLeMonde : content}
        </CustomModal>
      )}
    </>
  );
};

export default CustomSelectUser;
