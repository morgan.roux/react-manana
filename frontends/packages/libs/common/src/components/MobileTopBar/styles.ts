import { makeStyles, Theme, createStyles, lighten } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      padding: '0px 16px',
      justifyContent: 'space-between',
      alignItems: 'center',
      background: lighten(theme.palette.secondary.main, 0.1),
      fontFamily: 'Roboto',
      fontSize: 18,
      [theme.breakpoints.up('lg')]: {
        display: 'none',
      },
    },
    menuItemRoot: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      color: '#27272FAB',
      fontFamily: 'Roboto',
      '& > .MuiTypography-root': {
        fontSize: '0.75em',
      },
    },
    active: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      color: theme.palette.secondary.main,
      fontFamily: 'Roboto',
      '& > .MuiTypography-root': {
        fontSize: '0.75em',
      },
    },
  })
);
