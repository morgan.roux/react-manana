import { Box, Button, IconButton, InputAdornment, InputBase, TextField, Typography } from '@material-ui/core';
import { AttachFile, Close, Send } from '@material-ui/icons';
import React, { FC } from 'react';
import { LoaderSmall } from '@lib/ui-kit';
import CommentFormInterface from '../Interface/CommentFormInterface';
import useStyles from './styles';
import useCommentForm from './useCommentForm';
import classnames from 'classnames';
import { useDropzone } from 'react-dropzone';
import useCommonStyles from './../styles';
import { formatBytes, useApplicationContext } from '@lib/common';

const CommentForm = React.forwardRef<HTMLInputElement, CommentFormInterface>(
  (
    {
      defaultState,
      submit,
      loading,
      withAttachement = false,
      rows = 4,
      selectedFiles = [],
      setSelectedFiles,
      mutationSucces = false,
      dispatchSubmitCb,
    },
    inputRef
  ) => {
    const classes = useStyles({});
    const commonClasses = useCommonStyles();
    const formRef = React.useRef<any>();
    const { isMobile } = useApplicationContext();

    const {
      state: { content },
      setState,
      handleChange,
      handleSubmit,
    } = useCommentForm(submit, defaultState);

    const onDrop = (acceptedFiles: File[]) => {
      if (setSelectedFiles) {
        setSelectedFiles([...selectedFiles, ...acceptedFiles]);
      }
    };

    const { getRootProps, getInputProps, open, acceptedFiles } = useDropzone({
      // Disable click and keydown behavior
      noClick: true,
      noKeyboard: true,
      multiple: true,
      onDrop,
    });

    const removeFile = (file: File) => () => {
      const newFiles = [...selectedFiles];
      newFiles.splice(newFiles.indexOf(file), 1);
      acceptedFiles.splice(acceptedFiles.indexOf(file), 1);
      if (setSelectedFiles) {
        setSelectedFiles(newFiles);
      }
    };

    const dispatchSubmit = () => {
      if (content) {
        handleSubmit(undefined);
      }
    };

    React.useEffect(() => {
      if (dispatchSubmitCb) {
        dispatchSubmitCb(dispatchSubmit);
      }
    }, [content]);

    // Init form
    React.useEffect(() => {
      if (mutationSucces) {
        setState((prevState) => ({ ...prevState, content: '', fichiers: [] }));
      }
    }, [mutationSucces]);

    return (
      <form onSubmit={handleSubmit} ref={formRef}>
        {withAttachement ? (
          <div className={classes.formContainer}>
            <InputBase
              id="comment-input"
              className={classnames(
                { [classes.textFieldWeb]: !isMobile },
                { [classes.textFiledMobile]: isMobile },
                classes.textFieldNoBorder
              )}
              multiline={true}
              fullWidth={true}
              rows={rows}
              placeholder="Votre commentaire..."
              value={content}
              onChange={handleChange}
              name="content"
            />
            <div {...getRootProps({ className: classes.inputFileContainer })}>
              <input {...getInputProps()} />
              <Button variant="text" startIcon={<AttachFile />} onClick={open}>
                Joindre un fichier
              </Button>
              <Box className={commonClasses.filesContainer}>
                {selectedFiles.map((file, index) => (
                  <Box
                    key={`${file.name}_${index}`}
                    className={classnames(commonClasses.fileItem, classes.file)}
                    title={file.name}
                  >
                    <Box className={commonClasses.filenameContainer}>
                      <Typography>{file.name}</Typography>
                      {file.size && <Typography>{formatBytes(file.size, 1)}</Typography>}
                    </Box>
                    <IconButton size="small" onClick={removeFile(file)}>
                      <Close />
                    </IconButton>
                  </Box>
                ))}
              </Box>
              <IconButton onClick={handleSubmit} disabled={!content}>
                {loading ? <LoaderSmall /> : <Send color={content ? 'secondary' : 'disabled'} />}
              </IconButton>
            </div>
          </div>
        ) : (
          <TextField
            inputRef={inputRef}
            id="comment-input"
            className={classnames({ [classes.textFieldWeb]: !isMobile }, { [classes.textFiledMobile]: isMobile })}
            multiline={true}
            fullWidth={true}
            rows={rows}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton onClick={handleSubmit} disabled={!content}>
                    {loading ? <LoaderSmall /> : <Send color={content ? 'secondary' : 'disabled'} />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
            placeholder="Votre commentaire..."
            value={content}
            onChange={handleChange}
            name="content"
            autoFocus={true}
          />
        )}
      </form>
    );
  }
);

export default CommentForm;
