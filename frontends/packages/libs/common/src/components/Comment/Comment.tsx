import { useApplicationContext, useUploadFiles } from '@lib/common';
import { AxiosResponse } from 'axios';
import React, { FC, useState } from 'react';
import {
  CommentsDocument,
  CommentsQuery,
  CommentsQueryVariables,
  FichierInput,
  useCreate_CommentMutation,
} from './../../graphql';
import CommentForm from './CommentForm';
import CommentInterface from './Interface/CommentInterface';

interface CommentProps {
  codeItem: string;
  idSource: string;
  withAttachement?: boolean;
  rows?: number;
  forwardedInputRef?: any;
  refetch?: () => void;
  dispatchSubmitCb?: (dispatcherSubmit: () => void) => void;
}

const Comment: FC<CommentProps> = ({
  codeItem,
  idSource,
  forwardedInputRef,
  refetch,
  withAttachement = false,
  rows = 4,
  dispatchSubmitCb,
}) => {
  const { notify, graphql } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();
  const [selectedFiles, setSelectedFiles] = React.useState<File[]>([]);

  const [mutationSucces, setMutationSucces] = useState<boolean>(false);

  let uploadResult: AxiosResponse<any> | null = null;
  let files: FichierInput[] = [];

  const [loading, setLoading] = useState(false);

  const [commentData, setCommentData] = useState<CommentInterface | null>(null);

  /*const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);
*/

  const [doCreateComment, { loading: cLoading }] = useCreate_CommentMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createComment) {
        setSelectedFiles([]);
        setLoading(false);
        setMutationSucces(true);
        if (refetch) refetch();
      }
    },
    update: (cache, { data, errors }) => {
      console.log('errors :>> ', errors);

      if (data && data.createComment) {
        /**
         * Update search query
         */
        /* if (variables && operationName) {
            const req: any = cache.readQuery({
              query: operationName,
              variables,
            });

            if (req && req.search && req.search.data) {
              cache.writeQuery({
                query: SEARCH_QUERY,
                data: {
                  search: {
                    ...req.search,
                    ...{
                      data: req.search.data.map((item: any) => {
                        if (item && data && data.createComment && item.id === idSource) {
                          item = {
                            ...item,
                            comments: {
                              total: item.comments.total + 1,
                              __typename: 'CommentResult',
                            },
                          };
                          return item;
                        } else {
                          return item;
                        }
                      }),
                    },
                  },
                },
                variables,
              });
            }
          }*/

        /**
         * Update get comment query
         */
        const query = cache.readQuery<CommentsQuery, CommentsQueryVariables>({
          query: CommentsDocument,
          variables: {
            codeItem,
            idItemAssocie: idSource,
            take: 10,
            skip: 0,
          },
        });
        if (query && query.comments) {
          cache.writeQuery({
            query: CommentsDocument,
            variables: {
              codeItem,
              idItemAssocie: idSource,
              take: 10,
              skip: 0,
            },
            data: {
              ...query,
              comments: {
                ...query.comments,
                total: query.comments.total + 1,
                data: [...(query.comments.data as any), data.createComment],
              },
            },
          });
        }
      }
    },
    onError: (error) => {
      setLoading(false);
      console.log('error :>> ', error);
      error.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  const submitCreate = (data: CommentInterface) => {
    setCommentData(data);
    if (selectedFiles.length > 0) {
      setLoading(true);
      uploadFiles(selectedFiles, { directory: 'comments' })
        .then((uploadedFiles) => {
          doCreateComment({
            variables: {
              input: {
                ...data,
                codeItem,
                idItemAssocie: idSource,
                fichiers: uploadedFiles,
              },
            },
          });
        })
        .catch((error) => {
          setLoading(false);
          notify({
            type: 'error',
            message: `Une erreur s'est produite lors de chargement des fichiers`,
          });
        });
    } else {
      doCreateComment({ variables: { input: { ...data, codeItem, idItemAssocie: idSource } } });
    }
  };

  // const isOnActu = pathname.includes('/actualite');
  // const isOnOC = pathname.includes('/operation');

  const showCommentForm = (): boolean => {
    // if (isOnActu) return auth.isAuthorizedToCommentActu();
    // if (isOnOC) return auth.isAuthorizedToCommentOC();
    return true;
  };

  return showCommentForm() ? (
    <CommentForm
      ref={forwardedInputRef}
      submit={submitCreate}
      loading={loading || cLoading || uploadingFiles.loading}
      withAttachement={withAttachement}
      rows={rows}
      selectedFiles={selectedFiles}
      setSelectedFiles={setSelectedFiles}
      mutationSucces={mutationSucces}
      dispatchSubmitCb={dispatchSubmitCb}
    />
  ) : null;
};

export default Comment;
