import Comment from './Comment';
import { CommentListNew, CommentList } from './CommentList';

export { Comment, CommentListNew, CommentList };
