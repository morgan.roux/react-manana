import { LoaderSmall, TaskActivitiesEmpty } from '@lib/ui-kit';
import { Link, List } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import React, { FC, Fragment } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { useApplicationContext, useDisplayNotification } from '@lib/common';
import CommentItem from '../CommentItem';
import { CommentInfoFragment, useCommentsQuery } from './../../../graphql';

interface CommentListNewProps {
  codeItem: string;
  idItemAssocie: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    linkAndLoadingContainer: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    link: {
      color: '#000',
    },
    loader: {
      textAlign: 'right',
    },
  })
);

const CommentListNew: FC<CommentListNewProps & RouteComponentProps> = ({
  codeItem,
  idItemAssocie,
  location: { pathname },
}) => {
  const classes = useStyles({});
  const { graphql } = useApplicationContext();
  const displayNotification = useDisplayNotification();

  const isOnTodo = pathname.includes('todo');

  const { data, loading, fetchMore } = useCommentsQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
    variables: {
      codeItem,
      idItemAssocie,
      take: 10,
      skip: 0,
    },
    onError: (error) => {
      displayNotification({
        type: 'error',
        message: error.message,
      });
    },
  });

  const comments: CommentInfoFragment[] = (data && data.comments && data.comments.data) || [];
  const total: number = (data && data.comments && data.comments.total) || 0;

  const fetchMoreComments = () => {
    fetchMore({
      variables: {
        codeItem,
        idItemAssocie,
        take: 10,
        skip: comments.length || 0,
      },
      updateQuery: (prev: any, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;

        return {
          ...prev,
          comments: {
            ...prev.comments,
            data: [
              ...((prev.comments && prev.comments.data) || []),
              ...((fetchMoreResult && fetchMoreResult.comments && fetchMoreResult.comments.data) || []),
            ],
          },
        } as any;
      },
    });
  };

  return total === 0 && isOnTodo ? (
    <TaskActivitiesEmpty />
  ) : (
    <Fragment>
      <div className={classes.linkAndLoadingContainer}>
        {comments.length < total && (
          <Link component="button" className={classes.link} onClick={fetchMoreComments}>
            Afficher plus de commentaires
          </Link>
        )}
        <div className={classes.loader}>{loading && <LoaderSmall />}</div>
      </div>
      <List>{comments.length > 0 && comments.map((c: any) => <CommentItem key={c.id} comment={c} />)}</List>
    </Fragment>
  );
};

export default withRouter(CommentListNew);
