import CommentList from './CommentList';
import CommentListNew from './CommentListNew';

export { CommentListNew, CommentList };

export default CommentList;
