import React, { FC } from 'react';
import { List, Link } from '@material-ui/core';
import CommentItem from '../CommentItem';
import { CommentInfoFragment } from '../../../graphql';
import { LoaderSmall } from '@lib/ui-kit';
import { makeStyles, Theme, createStyles } from '@material-ui/core';

interface CommentListProps {
  comments?: CommentInfoFragment[];
  total?: number;
  fetchMoreComments?: () => void;
  loading?: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    linkAndLoadingContainer: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    link: {
      color: '#000',
    },
    loader: {
      textAlign: 'right',
    },
  })
);

const CommentList: FC<CommentListProps> = ({ comments, fetchMoreComments, total, loading }) => {
  const classes = useStyles({});

  return (
    <>
      <div className={classes.linkAndLoadingContainer}>
        {fetchMoreComments && comments && total && comments.length < total ? (
          <Link component="button" className={classes.link} onClick={fetchMoreComments}>
            Afficher plus de commentaires
          </Link>
        ) : null}
        <div className={classes.loader}>{loading && <LoaderSmall />}</div>
      </div>
      <List>
        {(comments || []).map((comment: any) => (
          <CommentItem key={comment.id} comment={comment} />
        ))}
      </List>
    </>
  );
};

export default CommentList;
