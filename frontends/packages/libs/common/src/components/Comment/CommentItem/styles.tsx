import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    bigAvatar: {
      width: 49,
      height: 49,
      marginRight: 6,
      borderRadius: '50%',
      [theme.breakpoints.down('md')]: {
        width: 24,
        height: 24,
      },
    },

    listItemAvatar: {
      [theme.breakpoints.down('md')]: {
        minWidth: 24,
      },
    },
    userName: {
      fontSize: 16,
      fontWeight: 500,
      [theme.breakpoints.down('md')]: {
        fontSize: 12,
      },
    },
    commentDate: {
      textAlign: 'right',
      '& .MuiTypography-root': {
        color: '#9E9E9E',
        [theme.breakpoints.down('md')]: {
          fontSize: 9,
        },
      },
    },
    fileContainer: {
      cursor: 'pointer',
      marginTop: 16,
    },
    contentMessage: {
      backgroundColor: '#F5F6FA',
      padding: 15,
      borderRadius: 12,
      marginTop: '0 !important',
      marginBottom: '0!important',
      [theme.breakpoints.down('md')]: {
        padding: '0px 0px 16px 0px',
        background: theme.palette.common.white,
        borderBottom: '1px solid #E1E1E1',
        borderRadius: 0,
      },
    },
    contentListItem: {
      display: 'flex',
      alignItems: 'start',
    },
    fonctionName: {
      fontSize: '0.875rem',
      color: '#E34168',
      fontWeight: 'bold',
      lineHeight: 1,
      [theme.breakpoints.down('md')]: {
        fontSize: 9,
        color: '#9E9E9E',
      },
    },
    labelPharmacie: {
      fontSize: '1rem',
      color: '#616161',
      fontWeight: 'bold',
      marginRight: 8,
    },
    namePharmacie: {
      fontSize: '1rem',
      color: '#212121',
      fontWeight: 'bold',
    },
    commentText: {
      fontFamily: 'Roboto',
      fontSize: 14,
    },
    commentHeader: {
      marginBottom: 16,
      [theme.breakpoints.down('md')]: {
        marginBottom: 8,
      },
    },
  })
);
