import { CustomSelectUser, useApplicationContext, useParameter, UsersModalProps } from '@lib/common';
import { CustomAutocomplete, CustomAvatar, CustomFormTextField } from '@lib/ui-kit';
import { Box, IconButton, Typography } from '@material-ui/core';
import { Group, PersonAdd } from '@material-ui/icons';
import React, { FC, useEffect } from 'react';
import { useCollaborateursLazyQuery } from '../../federation';
import AvatarInput from '../AvatarInput';
import { useStyles } from './styles';
export interface UserAutocompleteProps {
  label?: string;
  required?: boolean;
  selected: any;
  onAutocompleteChange: (inputValue: any) => void;
}

interface AvatarInputProps {
  classes?: {
    root?: string;
    input?: string;
  };
  label?: string;
  list: any[];
  small?: boolean;
  icon?: any;
  standard?: boolean;
  isAllTeamSelected?: boolean;
}
interface UserInputProps extends UsersModalProps {
  className?: string;
  label?: string;
  disabled?: boolean;
  codeParameter?: string;
  inputProps?: Partial<AvatarInputProps>;
  mode?: 'modal' | 'autocomplete';
  autoCompleteProps?: UserAutocompleteProps;
  onValidate?: (users: any[]) => void;
  standard?: boolean;
  emailOnly?: boolean;
  userInputClassName?: string;
}
const UserInput: FC<UserInputProps> = ({
  mode,
  className,
  selected,
  openModal,
  disabled,
  setOpenModal,
  onValidate,
  label,
  isCheckedTeam,
  codeParameter,
  inputProps = {},
  autoCompleteProps,
  standard,
  emailOnly,
  userInputClassName,
  ...rest
}) => {
  const optionPartage = useParameter(codeParameter || '')?.value?.value;
  const allTeamOption = { id: 'all', fullName: "Toute l'équipe" };
  const classes = useStyles();

  const { currentPharmacie: pharmacie, federation } = useApplicationContext();

  const [getCollaborateurs, gettingCollaborateurs] = useCollaborateursLazyQuery({ client: federation });

  const collaborateurs = gettingCollaborateurs.data?.collaborateurs;

  useEffect(() => {
    if (mode === 'autocomplete' && !gettingCollaborateurs.called && pharmacie) {
      getCollaborateurs({ variables: { idPharmacie: pharmacie.id } });
    }
  }, [pharmacie]);

  return (
    <Box className={className ? className : userInputClassName}>
      {(!mode || mode === 'modal') && (
        <>
          <AvatarInput
            isAllTeamSelected={isCheckedTeam}
            label={label || 'Collaborateur'}
            list={selected || []}
            icon={
              disabled ? undefined : (
                <IconButton onClick={() => setOpenModal(true)} style={{ padding: 5 }}>
                  <PersonAdd />
                </IconButton>
              )
            }
            {...inputProps}
            standard={standard}
            emailOnly={emailOnly}
          />
          <CustomSelectUser
            {...rest}
            onValidate={onValidate}
            selected={selected}
            isCheckedTeam={isCheckedTeam}
            openModal={openModal}
            setOpenModal={setOpenModal}
            optionPartage={(codeParameter && optionPartage) || 'MON_EQUIPE'}
          />
        </>
      )}
      {autoCompleteProps && (
        <CustomAutocomplete
          id="idUserAutocomplete"
          loading={gettingCollaborateurs.loading}
          options={collaborateurs ? [allTeamOption, ...collaborateurs] : []}
          optionLabelKey="fullName"
          defaultValue={allTeamOption}
          value={autoCompleteProps.selected}
          onAutocompleteChange={autoCompleteProps.onAutocompleteChange}
          label={autoCompleteProps.label || ''}
          required={autoCompleteProps.required || false}
          renderInput={(params) => (
            <CustomFormTextField
              {...params}
              label={label}
              InputProps={{
                ...params.InputProps,
                endAdornment: <React.Fragment>{params.InputProps.endAdornment}</React.Fragment>,
              }}
            />
          )}
          renderOption={(option) =>
            option.id !== 'all' ? (
              <Box className={classes.autoCompleteInput}>
                <CustomAvatar
                  key={option.id}
                  url={option.photo?.publicUrl}
                  name={option.userName || option.fullName || ''}
                />
                <Typography>{option.userName || option.fullName || ''}</Typography>
              </Box>
            ) : (
              <Box className={classes.allTeamInput}>
                <Group />
                <Typography>{option.userName || option.fullName || ''}</Typography>
              </Box>
            )
          }
        />
      )}
    </Box>
  );
};

export default UserInput;
