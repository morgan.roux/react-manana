import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import { useApplicationContext, UserAction } from '@lib/common';
import useStyles from './styles';
import { useCommentsQuery, useSmyleysQuery } from '../../graphql';
import { Comment, CommentList } from '../Comment';

interface RightContentCommentProps {
  item: any;
  codeItem: string;
  listResult: any;
  refetch?: () => void;
}

const RightContentComment: FC<RightContentCommentProps> = ({ item, codeItem, refetch }) => {
  const classes = useStyles({});
  const loading = false;
  const { currentGroupement: groupement, graphql } = useApplicationContext();

  // Get smyleys by groupement
  const getSmyleys = useSmyleysQuery({
    client: graphql,
    variables: {
      idGroupement: groupement.id,
    },
  });

  const smyleys = getSmyleys && getSmyleys.data && getSmyleys.data.smyleys;

  const getComments = useCommentsQuery({
    client: graphql,
    variables: {
      codeItem,
      idItemAssocie: (item && item.id) || '',
      take: 10,
      skip: 0,
    },
    fetchPolicy: 'cache-and-network',
  });
  const nbComment =
    (getComments && getComments.data && getComments.data.comments && getComments.data.comments.total) || 0;

  const nbSmyley = (item && item.userSmyleys && item.userSmyleys.total) || 0;

  const combinedRefetch = () => {
    if (refetch) {
      refetch();
    }
    getComments.refetch();
  };

  console.log('$$$$ : ', item);

  const fetchMoreComments = () => {
    getComments.fetchMore({
      variables: {
        codeItem,
        idItemAssocie: (item && item.id) || '',
        take: 10,
        skip:
          (getComments &&
            getComments.data &&
            getComments.data.comments &&
            getComments.data.comments.data &&
            getComments.data.comments.data.length) ||
          0,
      },
      updateQuery: (prev: any, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;

        return {
          ...prev,
          comments: {
            ...prev.comments,
            data: [
              ...((prev.comments && prev.comments.data) || []),
              ...((fetchMoreResult && fetchMoreResult.comments && fetchMoreResult.comments.data) || []),
            ],
          },
        } as any;
      },
    });
  };

  return (
    <Box padding="15px 28px">
      {/* User Actions  */}

      <Box>
        <hr className={classes.hrHeight} />
        <Box marginTop="12px">
          {item && item.id && (
            <UserAction
              codeItem={codeItem}
              idSource={item && item.id}
              nbSmyley={nbSmyley}
              nbComment={nbComment || 0}
              nbShare={0}
              userSmyleys={(item && item.userSmyleys && item.userSmyleys) || undefined}
              smyleys={smyleys}
              refetchSmyleys={refetch}
            />
          )}
        </Box>
      </Box>
      {/* Comments */}
      <Box>
        {getComments && getComments.data && getComments.data.comments && (
          <CommentList
            comments={getComments.data.comments.data}
            fetchMoreComments={fetchMoreComments}
            loading={loading}
          />
        )}
        {item && item.id && <Comment refetch={combinedRefetch} codeItem={codeItem} idSource={item.id} />}
      </Box>
    </Box>
  );
};

export default RightContentComment;
