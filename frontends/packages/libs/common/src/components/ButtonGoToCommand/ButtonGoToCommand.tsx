import { NewCustomButton } from '@lib/ui-kit';
import { ArrowForward } from '@material-ui/icons';
import React, { FC, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useLocalArticleOCArray } from '@lib/common/src/shared/operations/operation-commerciale';

const ButtonGoToCommand: FC = ({}) => {
  // Get (selected) article in OC from apollo local state
  const [articleOCArray] = useLocalArticleOCArray();
  const [disableBtn, setDisableBtn] = useState<boolean>(true);
  const {
    push,
    location: { state },
  } = useHistory();

  const idPublicite = (state as any).idPublicite;

  // Passer à la commande
  const toOrder = () => {
    push('/recapitulatif/operation-commerciale', { idPublicite });
  };

  useEffect(() => {
    if (articleOCArray && articleOCArray.length > 0) {
      setDisableBtn(false);
    } else {
      setDisableBtn(true);
    }
  }, [articleOCArray]);

  return (
    <NewCustomButton
      style={{ textTransform: 'none', marginRight: 8 }}
      endIcon={<ArrowForward />}
      children="Passer commande"
      className={classes.btnPasserCommande}
      onClick={toOrder}
      disabled={disableBtn}
    />
  );
};

export default ButtonGoToCommand;
