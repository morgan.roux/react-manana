import { LoaderSmall } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import { useApplicationContext } from '@lib/common';
import { useGet_UrgencesQuery } from '@lib/common/src/federation';
import { useStyles } from './styles';

const COLOR_ORDRE_MAP: any = {
  '1A': { color: '#E34168', order: 1 },
  '1B': { color: '#F46036', order: 2 },
  '1C': { color: '#FBB104', order: 3 },
  '2A': { color: '#00A745', order: 4 },
  '2B': { color: '#CB48B7', order: 5 },
  '2C': { color: '#63B8DD', order: 6 },
};

interface UrgenceImportanceBadgeProps {
  importanceOrdre: number;
  urgenceCode: string;
}
const UrgenceImportanceBadge: FC<UrgenceImportanceBadgeProps> = ({ importanceOrdre, urgenceCode }) => {
  const classes = useStyles();
  const { federation } = useApplicationContext();
  const urgenceQuery = useGet_UrgencesQuery({ client: federation, fetchPolicy: 'cache-only' });
  const urgenceList = urgenceQuery?.data?.urgences?.nodes || [];
  const urgence: any = urgenceList?.find((urgence: any) => urgence?.code === urgenceCode);

  const colorOrdre = COLOR_ORDRE_MAP[`${importanceOrdre}${urgenceCode}`];
  return (
    <Box display="flex" flexDirection="column" className={classes.root} style={{ backgroundColor: colorOrdre.color }}>
      {urgenceQuery.loading ? (
        <LoaderSmall />
      ) : (
        <>
          <Box>{`Importance`}</Box>
          <Box>
            {`${importanceOrdre === 1 ? 'Haute' : 'Moyenne'} ${
              urgence?.code === 'C' ? 'du' : 'de la'
            } ${urgence?.libelle?.toLowerCase()}`}
          </Box>
        </>
      )}
    </Box>
  );
};

export default UrgenceImportanceBadge;
