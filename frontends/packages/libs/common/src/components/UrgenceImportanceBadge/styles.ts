import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 130,
      padding: 10,
      borderRadius: 5,
      color: '#FFF',
      textAlign: 'center',
    },
  })
);
