import { Box, Button, IconButton, InputAdornment, TextField, Typography } from '@material-ui/core';
import { AddCircle, Close, PictureAsPdf } from '@material-ui/icons';
import _ from 'lodash';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import ReactQuill from 'react-quill';
import { formatBytes } from '../../shared';
import { PartenaireInput } from '../PartenaireInput';
import UserInput from '../UserInput';
import { useStyles } from './style';

const initialValues = {
  recepteurs: [],
  objet: '',
  message: '',
  selectedFiles: [],
};

interface MessageValueProps {
  recepteurs: any[];
  objet: string;
  message: string;
  selectedFiles: any[];
}

interface CustomFormMessageProps {
  onOpenUserModal: () => void;
  recepteurs: {
    data: any[];
    loading: boolean;
    error: boolean;
    type: string;
    // id: string;
    category: string;
    setRecepteur: (data: any[]) => void;
  };
  onRequestSave: (values: MessageValueProps) => void;
  partenaireAssocie?: any;
  openForm?: boolean;
}

const CustomFormMessage: FC<CustomFormMessageProps> = ({
  onOpenUserModal,
  recepteurs,
  onRequestSave,
  partenaireAssocie,
  openForm,
}) => {
  const classes = useStyles();
  const [values, setValues] = useState<MessageValueProps>(initialValues);
  const [openUserModal, setOpenUserModal] = useState<boolean>(false);
  const [focused, setFocused] = React.useState<boolean>(false);
  const [partenaire, setPartenaire] = useState<any>();
  // const [userRecepteurs, setUserRecepteurs] = React.useState<any[]>([]);
  // const [filtersModal, setFiltersModal] = React.useState<string[]>(['ALL']);
  // const [userIds, setUserIds] = React.useState<string[]>([]);
  // const currentUser: ME_me = getUser();

  useEffect(() => {
    onRequestSave(values);
  }, [values]);

  useEffect(() => {
    setValues((prevState) => ({ ...prevState, recepteurs: recepteurs.data }));
  }, [recepteurs.data]);

  useEffect(() => {
    if (partenaireAssocie) {
      setPartenaire({
        id: partenaireAssocie?.id,
        nom: partenaireAssocie?.nom,
      });
    }
    if (!openForm) {
      setValues(initialValues);
      recepteurs.setRecepteur([]);
    }
  }, [partenaireAssocie, openForm]);

  const handleFocus = () => {
    setFocused(true);
  };

  // const onChangeUserTxt = (name: string) => (e: ChangeEvent<any>, value: string) => {};

  // const handleChangeAutocomplete = (name: string) => (e: React.ChangeEvent<any>, value: any) => {
  //   //('-+-++-++--++-+--++-+-', name, _.uniqBy(value, 'id'));
  //   setValues(prevState => ({ ...prevState, [name]: _.uniqBy(value, 'id') }));
  // };

  const onDrop = (acceptedFiles: File[]) => {
    //('acceptedFiles>>>>>>>>>>>', acceptedFiles);
    setValues((prevState) => ({
      ...prevState,
      selectedFiles: [...values.selectedFiles, ...acceptedFiles],
    }));
  };

  // const handleConfirm = () => {
  //   if (userIds) {
  //     setValues(prev => ({
  //       ...prev,
  //       recepteursIds: userIds && userIds.length ? userIds : [],
  //     }));
  //   }

  //   if (userRecepteurs) {
  //     setValues(prev => ({
  //       ...prev,
  //       recepteurs: userRecepteurs && userRecepteurs.length ? userRecepteurs : [],
  //     }));
  //   }
  //   if (setOpenUserModal) setOpenUserModal(false);
  // };

  // const handleClose = (open: boolean) => {
  //   // if (userIds) setUserIds(recepteursIds.length ? recepteursIds : []);
  //   if (userRecepteurs) setUserRecepteurs(recepteurs.data.length ? recepteurs.data : []);
  //   if (setOpenUserModal) setOpenUserModal(open);
  // };

  const handleChangeInput = (e: ChangeEvent<HTMLInputElement>) => {
    setValues((prevState) => ({ ...prevState, [e.target.name]: e.target.value }));
  };

  // const handleOpenUserModal = () => {
  //   onOpenUserModal();
  //   setOpenUserModal && setOpenUserModal(true);
  // };

  // const onOpenUserAutocomplete = () => {};

  const { getRootProps, getInputProps, open, acceptedFiles } = useDropzone({
    // Disable click and keydown behavior
    noClick: true,
    noKeyboard: true,
    multiple: true,
    onDrop,
  });

  const removeFile = (file: File) => () => {
    const newFiles = [...values.selectedFiles];
    newFiles.splice(newFiles.indexOf(file), 1);
    acceptedFiles.splice(acceptedFiles.indexOf(file), 1);
    setValues((prevState) => ({
      ...prevState,
      selectedFiles: newFiles,
    }));
  };

  const fileContainer = (
    <Box className={classes.filesContainer}>
      {values.selectedFiles.map((file: any, index) => (
        <Box key={`${file.name}_${index}`} className={classes.fileItem}>
          <PictureAsPdf />
          <Box className={classes.filenameContainer}>
            <Typography>{file.name || file.nomOriginal}</Typography>
            {file.size && <Typography>{formatBytes(file.size, 1)}</Typography>}
          </Box>
          <IconButton size="small" onClick={removeFile(file)}>
            <Close />
          </IconButton>
        </Box>
      ))}
    </Box>
  );

  return (
    <Box display="flex" flexDirection="column">
      <PartenaireInput
        // key="laboratoire"
        index="laboratoire"
        label="Laboratoire"
        value={{
          id: partenaire?.id,
          nom: partenaire?.nom,
        }}
        onChange={(laboratoire) => {
          setPartenaire(laboratoire);
        }}
        disabled={partenaireAssocie ? true : false}
      />
      <Box py={1}>
        <UserInput
          openModal={openUserModal}
          idPartenaireTypeAssocie={partenaire?.id}
          withAssignTeam={false}
          withNotAssigned={false}
          partenaireType={recepteurs.type}
          category={recepteurs.category as any}
          setOpenModal={setOpenUserModal}
          selected={recepteurs.data}
          setSelected={(value) => {
            recepteurs.setRecepteur(value as any);
          }}
          label="A"
          title="Récépteurs"
          standard
          emailOnly
          className={classes.rootUserInput}
        />
      </Box>
      <Box py={1}>
        <TextField
          fullWidth
          variant="standard"
          name="objet"
          value={values.objet}
          onChange={handleChangeInput}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <span>Objet : </span>
              </InputAdornment>
            ),
          }}
        />
      </Box>
      <Box py={1}>
        <div {...getRootProps({ className: classes.dropzone })}>
          <input {...getInputProps()} />
          <Box display="flex" width="100%">
            <Typography style={{ minWidth: 'fit-content' }}>Pièces Jointes :</Typography>
            {fileContainer}
          </Box>
          <Button
            variant="contained"
            endIcon={<AddCircle />}
            color="default"
            onClick={open}
            style={{ background: 'none' }}
          >
            Sélect. fichiers
          </Button>
        </div>
      </Box>
      <Box py={1}>
        <Box width="100%" onFocus={handleFocus} className={classes.mailContent}>
          <ReactQuill
            theme="snow"
            value={values.message}
            style={{
              height: '75%',
            }}
            className={focused ? 'div-focused' : 'blurred-editor'}
            onChange={(value) => handleChangeInput({ target: { name: 'message', value } } as any)}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default CustomFormMessage;
