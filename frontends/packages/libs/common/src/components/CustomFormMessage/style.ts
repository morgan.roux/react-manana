import { createStyles, makeStyles } from '@material-ui/core';
import { Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    dropzone: {
      display: 'flex',
      alignItems: 'start',
      marginTop: 5,
      borderBottom: '1px solid rgba(0, 0, 0, 0.42)',
      paddingBottom: 10,
      '& > button': {
        textTransform: 'none',
        boxShadow: 'none',
        margin: '0px auto',
      },
      '& > button:hover': {
        boxShadow: 'none',
      },
      '& > button, & > p': {
        minWidth: 'fit-content',
      },
      '@media (max-width: 1318px)': {
        flexWrap: 'wrap',
        '& > button': {
          margin: '5px 0px',
        },
      },
    },
    mailContent: {
      height: '75%',
      '& .div.blurred-editor div.ql-toolbar': {
        display: 'none',
      },
      '& .ql-container': {
        minHeight: 150,
      },
    },
    fileItem: {
      display: 'flex',
      alignItems: 'start',
      // border: '1px solid #9E9E9E',
      padding: 4,
      marginLeft: 6,
      marginBottom: 6,
      '& .MuiTypography-root': {
        fontSize: '0.75rem',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        '-webkit-line-clamp': 1,
        '-webkit-box-orient': 'vertical',
        whiteSpace: 'nowrap',
        maxWidth: 110,
        width: '100%',
      },
    },
    filenameContainer: {
      display: 'flex',
      flexDirection: 'column',
    },
    filesContainer: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      marginLeft: '10px',
      width: '100%',
      '@media (max-width: 1318px)': {
        margin: '5px 0px 0px',
      },
    },
    userSelectRoot: {
      '& .MuiDialogActions-root': {
        display: 'flex',
        [theme.breakpoints.down('md')]: {
          display: 'flex',
          width: '100vw',
          padding: 8,
          justifyContent: 'left',
        },
      },
      [theme.breakpoints.down('md')]: {
        '& .MuiDialog-scrollPaper': {
          justifyContent: 'left',
        },
        '& .MuiDialog-paperFullWidth': {
          width: '100%',
          maxWidth: '100%',
          minHeight: '100%',
        },
        '& .MuiDialog-paper': {
          margin: 0,
        },
        '& .MuiDialogContent-root': {
          padding: 0,
        },
        '& .MuiDialogTitle-root': {
          background: theme.palette.secondary.main,
        },
      },
    },
    rootUserInput: {
      borderBottom: '1px solid #949494 !important',
    },
  })
);

export { useStyles };
