import { makeStyles, Theme, createStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
      marginBottom: 20,
    },
    contentRS: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    contentPartageRecommandation: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-end',
      '& .MuiTypography-root': {
        fontSize: '0.875rem',
      },
      marginBottom: 8,
    },
    RobotoRegular: {
      fontFamily: 'Roboto',
      fontWeight: 'normal',
    },
    medium: {
      // fontSize: theme.typography.medium.fontSize,
    },
    panierListeSocialItemMargin: {
      marginLeft: '-6px',
    },

    panierListeSocialIcon: {
      height: '20px',
      width: '20px',
      color: '#B1B1B1',
    },

    panierListeSocialItem: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      cursor: 'pointer',
      position: 'relative',
      maxWidth: 120,
      border: '1px solid #e0e0e0',
      padding: 8,
      borderRadius: 4,
    },
    small: {
      // fontSize: theme.typography.small.fontSizefel
      display: 'flex',
      flexDirection: 'column',
    },
    emojiContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      position: 'absolute',
      top: -57,
      left: -5,
      padding: 5,
      borderRadius: 3,
      background: '#fff',
      boxShadow: '0px 3px 6px #00000021',
      '& > div': {
        marginRight: 15,
        padding: 9,
        display: 'flex',
        borderRadius: 20,
      },
      '& > div:nth-last-child(1)': {
        marginRight: 0,
      },
      '& > div:hover': {
        background: '#eeeeee',
      },
    },
    hr: {
      width: '100% !important',
      border: '0.5px solid #E5E5E5',
      marginBottom: 20,
      height: '0 !important',
    },
    smyleyContainer: {
      display: 'flex',
      alignItems: 'center',
      margin: '10px 0px',
      '& > img': {
        marginLeft: -5,
      },
      '& > img:nth-child(1)': {
        marginLeft: 0,
      },
    },
    nbSmyley: {
      marginLeft: 10,
    },
  })
);

export default useStyles;
