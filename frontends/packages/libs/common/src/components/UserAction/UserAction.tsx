import { useApolloClient } from '@apollo/client';
import { useApplicationContext, useDisplayNotification } from '@lib/common';
import {
  useCreate_User_SmyleyMutation,
  useSearch_Custom_PartageQuery,
  useUser_SmyleysLazyQuery,
} from '@lib/common/src/graphql';
import { CustomModal } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import LikeIcon from '@material-ui/icons/ThumbUp';
import classnames from 'classnames';
import React, { FC, MouseEvent, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useLocation } from 'react-router-dom';
import Partage from '../Friends/Modals/PartageFriends';
import { ListPerson } from '../ListPerson';
import useStyles from './styles';
import UserActionInfo from './UserActionInfo';

interface UserActionInfoProps {
  codeItem: string;
  idSource: string;
  nbSmyley?: number;
  nbComment?: number;
  nbShare?: number;
  idArticle?: string;
  userSmyleys?: any;
  presignedUrls?: any;
  smyleys?: any;
  actualite?: any;
  refetchSmyleys?: () => void;
  hidePartage?: boolean;
  hideRecommandation?: boolean;
}
const UserAction: FC<UserActionInfoProps> = ({
  codeItem,
  idSource,
  nbComment,
  nbShare,
  nbSmyley,
  idArticle,
  userSmyleys,
  smyleys,
  actualite,
  refetchSmyleys,
  hidePartage,
  hideRecommandation,
}) => {
  const classes = useStyles({});
  const [smylesPresigned, setSmylesPresigned] = useState<any[]>([]);
  const { push } = useHistory();
  const { pathname } = useLocation();
  const [showEmojisContainer, setShowEmojisContainer] = useState<boolean>(false);
  const [openReactionsModal, setOpenReactionsModal] = useState<boolean>(false);
  const { graphql, user: currentUser, auth, config } = useApplicationContext();
  const [currentUserSmyleys, setcurrentUserSmyleys] = useState<any | undefined>(userSmyleys);

  const displayNotification = useDisplayNotification();

  const [loadUserSmyleys, loadingUserSmyleys] = useUser_SmyleysLazyQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
    onCompleted: (data) => {
      if (data.userSmyleys?.data) {
        setcurrentUserSmyleys(data.userSmyleys);
      }
    },
  });

  // const {
  //   content: { variables, operationName },
  // } = useContext<ContentStateInterface>(ContentContext);

  const currentUserSmyley =
    currentUserSmyleys &&
    currentUserSmyleys.data &&
    currentUserSmyleys.data.find(
      (userSmyley: any) => userSmyley && userSmyley.user && userSmyley.user.id === currentUser.id
    );

  const {
    data,
    loading,
    refetch: refetchPartages,
  } = useSearch_Custom_PartageQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
    variables: {
      type: ['partage'],
      query: {
        query: {
          bool: {
            must: [{ term: { isRemoved: false } }, { term: { idItemAssocie: idSource } }],
          },
        },
      },
      take: 100,
      skip: 0,
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        displayNotification({
          type: 'error',
          message: err.message,
        });
      });
    },
    onCompleted: (data) => {
      console.log('data', data);
    },
  });

  const isOnActu = pathname.includes('/actualite');
  const isOnOC = pathname.includes('/operations');
  const isOnProduct = pathname.includes('/catalogue-produits');

  const focusToFormInput = () => {
    const input = document.getElementById('comment-input');
    if (input) input.focus();
  };

  const goToDetails = () => {
    if (idArticle && pathname !== `/catalogue-produits/card/${idArticle}`) {
      push(`/catalogue-produits/card/${idArticle}`);
    }
    focusToFormInput();
  };

  const handleOverLike = (event: React.MouseEvent<any>) => {
    setShowEmojisContainer(true);
  };

  const handleCloseSmyleysContainer = () => {
    setShowEmojisContainer(false);
  };

  // Set presigned smyleys
  useEffect(() => {
    const newSmyleys: any[] = [];
    if (smyleys) {
      smyleys.map((smyley: any) => {
        if (smyley) {
          const newFile: any = {
            ...smyley,
            photo: `${config.STORAGE_PUBLIC_BASE_PATH}/${smyley.photo}`,
          };
          newSmyleys.push(newFile);
        }
      });
    }
    setSmylesPresigned(newSmyleys);
  }, [smyleys]);

  useEffect(() => {
    setcurrentUserSmyleys(userSmyleys);
  }, [userSmyleys]);
  const handleOpenModalPartage = () => {
    setopenmodalpartage(!openmodalpartage);
  };
  console.log('partage', data?.search?.data);

  const numberRecommandation = () => {
    // const numbre;
    // data?.search?.data?.filter(item => item&&item.typePartage==="PArtage").length
  };

  const [doCreateUserSmyley] = useCreate_User_SmyleyMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createUserSmyley) {
        const isRemoved = data.createUserSmyley.isRemoved;
        const uniqueUser =
          currentUserSmyleys &&
          currentUserSmyleys.data &&
          currentUserSmyleys.data
            .map((smyley: any) => smyley && smyley.user && smyley.user.id)
            .includes(data.createUserSmyley && data.createUserSmyley.user && data.createUserSmyley.user.id);

        setcurrentUserSmyleys({
          total: isRemoved
            ? currentUserSmyleys && currentUserSmyleys.total - 1
            : uniqueUser
            ? currentUserSmyleys && currentUserSmyleys.total
            : currentUserSmyleys && currentUserSmyleys.total + 1,
          data: isRemoved
            ? currentUserSmyleys &&
              currentUserSmyleys.data &&
              currentUserSmyleys.data.filter(
                (item: any) =>
                  item &&
                  item.id &&
                  data.createUserSmyley &&
                  data.createUserSmyley.id &&
                  item.id !== data.createUserSmyley.id
              )
            : [
                ...((currentUserSmyleys &&
                  currentUserSmyleys.data &&
                  currentUserSmyleys.data.filter(
                    (item: any) =>
                      item &&
                      item.user &&
                      data.createUserSmyley &&
                      data.createUserSmyley.user &&
                      data.createUserSmyley.user.id &&
                      item.user.id !== data.createUserSmyley.user.id
                  )) ||
                  []),
                data.createUserSmyley,
              ],
        } as any);
        if (refetchSmyleys) refetchSmyleys();
      }
    },
  });

  const handleClickSmyley = (smyley: any) => (e: MouseEvent<any>) => {
    e.preventDefault();
    e.stopPropagation();
    handleCloseSmyleysContainer();
    if (smyley) {
      doCreateUserSmyley({
        variables: { codeItem, idSource, idSmyley: smyley.id },
      });
    }
  };

  const getSmyleyUrl = (id: string) => {
    const smyley = smylesPresigned.find((smyley) => smyley.id === id);

    return smyley && smyley.photo;
  };
  const [openmodalpartage, setopenmodalpartage] = useState<boolean>(false);

  const like: any | undefined = smylesPresigned && smylesPresigned.find((smyley) => smyley && smyley.nom === "J'aime");

  const showBtnLike = (): boolean => {
    if (isOnActu) return auth.isAuthorizedToLikeActu();
    if (isOnOC) return auth.isAuthorizedToLikeOC();
    if (isOnProduct) return auth.isAuthorizedToLikeProduct();
    return true;
  };

  const handleCountClick = () => {
    loadUserSmyleys({
      variables: {
        idSource,
        codeItem,
      },
    });

    setOpenReactionsModal(true);
  };

  const usersReactions = loadingUserSmyleys.data?.userSmyleys?.data || [];

  useEffect(() => {
    loadUserSmyleys({
      variables: {
        idSource,
        codeItem,
      },
    });
  }, [codeItem, idSource]);
  return (
    <Box onMouseLeave={handleCloseSmyleysContainer}>
      {/* <hr className={classes.hr} /> */}
      <Partage
        openModal={openmodalpartage}
        handleClose={handleOpenModalPartage}
        idActualite={idSource}
        codeItem={codeItem}
        actualite={actualite}
        refetch={refetchPartages}
      />
      <Box className={classnames(classes.contentRS, classes.RobotoRegular, classes.medium)}>
        {currentUserSmyleys && currentUserSmyleys.total > 0 ? (
          <UserActionInfo
            nbComment={nbComment}
            nbSmyley={currentUserSmyleys.total}
            userSmyleys={currentUserSmyleys}
            onCountClick={handleCountClick}
          />
        ) : (
          <Box className={classes.smyleyContainer}>
            {smylesPresigned &&
              smylesPresigned.map((smyley) => {
                return smyley && smyley.nom === "J'aime" && <img key={smyley.id} src={smyley.photo} alt={smyley.nom} />;
              })}
            <Box className={classes.nbSmyley}>{(currentUserSmyleys && currentUserSmyleys.total) || 0}</Box>
          </Box>
        )}
      </Box>
      {showBtnLike() && (
        <Box className={classes.contentRS}>
          <Box
            className={classes.panierListeSocialItem}
            onClick={handleClickSmyley(like as any)}
            onMouseOver={handleOverLike}
          >
            {currentUserSmyley ? (
              <Box display="flex">
                {currentUserSmyley && currentUserSmyley.smyley && currentUserSmyley.smyley.id && (
                  <img src={getSmyleyUrl(currentUserSmyley.smyley.id)} />
                )}
                <Box className={classes.small} marginLeft="8px">
                  {currentUserSmyley && currentUserSmyley.smyley && currentUserSmyley.smyley.nom}
                </Box>
              </Box>
            ) : (
              <Box display="flex">
                <LikeIcon className={classes.panierListeSocialIcon} />
                <Box className={classes.small} marginLeft="8px">
                  J'aime
                </Box>
              </Box>
            )}

            {showEmojisContainer && (
              <Box className={classes.emojiContainer}>
                {smylesPresigned &&
                  smylesPresigned.map((smyley) => {
                    return (
                      smyley && (
                        <Box key={smyley.id} onClick={handleClickSmyley(smyley)}>
                          <img src={smyley.photo} alt={smyley.nom} />
                        </Box>
                      )
                    );
                  })}
              </Box>
            )}
          </Box>
        </Box>
      )}
      <CustomModal
        open={openReactionsModal}
        setOpen={setOpenReactionsModal}
        title="Réactions"
        withBtnsActions={true}
        withCancelButton={false}
        actionButtonTitle="Fermer"
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="sm"
        fullWidth={true}
        // tslint:disable-next-line: jsx-no-lambda
        // tslint:disable-next-line: jsx-no-lambda
        onClick={(event: any) => {
          event.preventDefault();
          event.stopPropagation();
        }}
        onClickConfirm={() => setOpenReactionsModal(false)}
      >
        <Box width="100%">
          <ListPerson
            actionsRenderer={(user: any) => {
              const userSmyley = usersReactions.find((us) => us?.user?.id && us.user.id === user.id);
              return (
                <Box>
                  <img src={`${config.STORAGE_PUBLIC_BASE_PATH}/${userSmyley?.smyley?.photo}`} />
                </Box>
              );
            }}
            onNext={() => console.log('next')}
            listes={usersReactions.map((us) => ({
              ...(us?.user || {}),
              nom: us?.user?.userName,
              src: us?.user?.userPhoto?.fichier?.publicUrl,
            }))}
            loading={loadingUserSmyleys.loading}
          />
        </Box>
      </CustomModal>
    </Box>
  );
};

export default UserAction;
