import { makeStyles, Theme, createStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'center',
      margin: '10px 0px',
      '& > img': {
        marginLeft: -5,
      },
      '& > img:nth-child(1)': {
        marginLeft: 0,
      },
    },
    nbSmyley: {
      marginLeft: 10,
      cursor: 'pointer',
    },
  })
);

export default useStyles;
