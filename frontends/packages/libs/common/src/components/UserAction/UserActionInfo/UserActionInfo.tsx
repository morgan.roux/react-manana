import React, { FC, useState, useEffect } from 'react';
import useStyles from './styles';
import { Box } from '@material-ui/core';
import { useApplicationContext } from '@lib/common';
interface UserActionInfoProps {
  nbSmyley?: number;
  nbComment?: number;
  nbShare?: number;
  userSmyleys?: any;
  onCountClick?: () => void;
}

const UserActionInfo: FC<UserActionInfoProps> = ({ nbSmyley, nbComment, nbShare, onCountClick, userSmyleys }) => {
  const classes = useStyles({});
  const [userSmylesPresigned, setUserSmylesPresigned] = useState<any[]>([]);

  const { config } = useApplicationContext();

  // Set presigned smyleys
  useEffect(() => {
    const newUserSmyleys: any[] = [];
    if (userSmyleys && userSmyleys.data) {
      userSmyleys.data.map((userSmyley: any) => {
        if (userSmyley && userSmyley.smyley) {
          const newFile: any = {
            ...userSmyley.smyley,
            photo: `${config.STORAGE_PUBLIC_BASE_PATH}/${userSmyley.smyley.photo}`,
          };
          newUserSmyleys.push(newFile);
        }
      });
    }

    const filteredArr = newUserSmyleys.reduce((prev: any, current: any) => {
      const x = prev.find((item: any) => item.id === current.id);
      if (!x) {
        return prev.concat([current]);
      } else {
        return prev;
      }
    }, []);

    setUserSmylesPresigned(filteredArr);
  }, [userSmyleys?.data]);

  return (
    <div className={classes.root}>
      {userSmylesPresigned &&
        userSmylesPresigned.map((smyley) => {
          return <img key={smyley.id} src={smyley.photo} alt={smyley.nom} />;
        })}
      <Box onClick={() => onCountClick && onCountClick()} className={classes.nbSmyley}>
        {nbSmyley}
      </Box>
    </div>
  );
};

export default UserActionInfo;
