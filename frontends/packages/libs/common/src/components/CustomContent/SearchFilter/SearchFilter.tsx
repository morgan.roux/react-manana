import React, { FC } from 'react';
import useStyles from './styles';
import { FieldsOptions, Option } from '../interfaces';
import {
  FormControl,
  TextField,
  InputLabel,
  Select,
  OutlinedInput,
  MenuItem,
  Button,
  Typography,
  Box,
  Divider,
} from '@material-ui/core';
import { orderBy } from 'lodash';

interface SearchFilterProps {
  searchInputs: FieldsOptions[] | undefined;
  handleRunSearch: (event: any) => void;
  handleResetFields: () => void;
  handleFieldChange: (event: any) => void;
  disableResetButton?: boolean;
  labelWidth: number;
  fieldsState: any;
  searchInputsLabel?: string;
}

const SearchFilter: FC<SearchFilterProps> = ({
  searchInputs,
  handleRunSearch,
  handleResetFields,
  handleFieldChange,
  disableResetButton,
  labelWidth,
  fieldsState,
  searchInputsLabel,
}) => {
  const classes = useStyles({});
  const inputLabel = React.useRef<HTMLLabelElement>(null);
  const removeAllOptionInSelect = (fieldsState: any, searchInputs: FieldsOptions[]): any => {
    const newStateKeys = Object.keys(fieldsState).filter(name => {
      const selectElement = searchInputs.find(
        (input: FieldsOptions) => input.name === name && input.type === 'Select',
      );

      if (!selectElement) return true;

      const fieldValue = fieldsState[name];
      return !(selectElement.options || []).some(
        (option: Option) => option.value === fieldValue && option.all,
      );
    });

    const newState: any = {};
    newStateKeys.forEach(key => {
      newState[key] = fieldsState[key];
    });
    return newState;
  };

  if (!searchInputs || searchInputs.length === 0) {
    return null;
  }

  return (
    <div className={classes.root}>
      <Typography className={classes.label}>
        {searchInputsLabel || 'Filtre de recherche'}
      </Typography>
      {searchInputs && searchInputs.length > 0 && (
        <Box display="flex">
          <div className={classes.form} onSubmit={handleRunSearch}>
            {orderBy(searchInputs, ['code'], ['asc']).map(
              (inputItem: FieldsOptions, index: number) => {
                if (inputItem.type === 'Search') {
                  return (
                    <FormControl variant="outlined" key={index} className={classes.formControl}>
                      <TextField
                        name={inputItem.name}
                        label={inputItem.label}
                        value={fieldsState[inputItem.name]}
                        placeholder={inputItem.placeholder || 'Commençant par..'}
                        onChange={handleFieldChange}
                        InputProps={{
                          classes: {
                            root: classes.cssOutlinedInput,
                            notchedOutline: classes.notchedOutline,
                          },
                        }}
                        InputLabelProps={inputItem.inputLabelProps || { shrink: true }}
                        variant={'outlined'}
                      />
                    </FormControl>
                  );
                } else if (inputItem.type === 'Select') {
                  return (
                    <FormControl variant="outlined" key={index} className={classes.formControl}>
                      <InputLabel
                        ref={inputLabel}
                        htmlFor={inputItem.id ? `${inputItem.id}` : `input-select-${index}`}
                        className={classes.selectLabel}
                      >
                        {inputItem.label}
                      </InputLabel>
                      <Select
                        name={inputItem.name}
                        value={
                          (inputItem.dataType && inputItem.dataType === 'boolean'
                            ? fieldsState[inputItem.name] !== undefined
                              ? fieldsState[inputItem.name] === true
                                ? 1
                                : 0
                              : -1
                            : fieldsState[inputItem.name]) || 0
                        }
                        onChange={handleFieldChange}
                        input={
                          <OutlinedInput
                            type="number"
                            classes={{
                              notchedOutline: classes.notchedOutline,
                              root: classes.cssOutlinedInput,
                              input: classes.cssOutlinedSelect,
                            }}
                            labelWidth={labelWidth}
                            id={inputItem.id ? `${inputItem.id}` : `input-select-${index}`}
                          />
                        }
                      >
                        {inputItem.options &&
                          (inputItem.options || []).map((menuItem: Option, index) => (
                            <MenuItem value={menuItem.value} key={index}>
                              {menuItem.label}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  );
                }
              },
            )}
          </div>
          <div className={classes.verticalDivider}>
            <Divider orientation="vertical" />
          </div>
          <Box
            display="flex"
            flexDirection="column"
            padding="0px 112px 0px 112px"
            alignSelf="center"
          >
            <Button
              type="submit"
              variant="contained"
              color="secondary"
              className={classes.searchButton}
              onClick={handleRunSearch}
            >
              Rechercher
            </Button>
            {!disableResetButton && (
              <Button
                onClick={handleResetFields}
                type="button"
                variant="contained"
                color="default"
                className={classes.resetButton}
              >
                Réinitialiser
              </Button>
            )}
          </Box>
        </Box>
      )}
    </div>
  );
};

export default SearchFilter;
