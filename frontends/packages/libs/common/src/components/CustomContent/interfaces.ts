export interface Option {
  label: string;
  value: any;
  all?: boolean;
}

export interface Column {
  name: string;
  label: string;
  sortable?: boolean;
  renderer?: (row: any) => any;
}

export interface FieldsOptions {
  name: string;
  extraNames?: string[];
  label: string;
  value: any;
  ordre: number;
  placeholder?: string;
  inputLabelProps?: object;
  variant?: string;
  options?: Option[];
  id?: string;
  dataType?: string;
  type: InputType; // 'Select' | 'Search';
  filterType?: FilterType;
}

type InputType = 'Search' | 'Select';

type FilterType = 'Contains' | 'StartsWith' | 'EndsWith' | 'Match';
