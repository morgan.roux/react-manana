import { useApolloClient } from '@apollo/client';
import { NewCustomButton } from '@lib/ui-kit';
import { Box, IconButton, Step as MUIStep, StepLabel, Stepper as MUIStepper, Typography } from '@material-ui/core';
//import { GET_SHOW_SUB_SIDEBAR } from '../../../graphql/Actualite/local';
import { ArrowBack } from '@material-ui/icons';
import classnames from 'classnames';
import React, { Dispatch, FC, Fragment, ReactNode, SetStateAction, useEffect, useState } from 'react';
//import { resetSearchFilters } from '../withSearch/withSearch';
import useStyles from './styles';

export interface Step {
  title: string;
  content: JSX.Element;
  pageTitle?: string;
}

export interface StepperProps {
  title: string;
  steps: Step[];
  disableNextBtn: boolean;
  disablePrevBtn?: boolean;
  fullWidth?: boolean;
  finished?: boolean;
  noFinalStepNext?: boolean;
  resume?: JSX.Element;
  loading?: boolean;
  where?: string;
  finalStep?: any;
  history?: any;
  backBtnText?: string;
  activeStepFromProps?: number;
  setActiveStepFromProps?: Dispatch<SetStateAction<number | undefined>>;
  children?: ReactNode;
  onConfirm?: () => void;
  backToHome?: () => void;
  onClickNext?: () => void;
  onClickPrev?: () => void;
  unResetFilter?: boolean;
}

const Stepper: FC<StepperProps> = ({
  steps,
  backToHome,
  disableNextBtn,
  disablePrevBtn,
  fullWidth,
  finished,
  onConfirm,
  noFinalStepNext,
  resume,
  loading,
  where,
  title,
  finalStep,
  history,
  backBtnText,
  onClickNext,
  onClickPrev,
  // activeStepFromProps,
  setActiveStepFromProps,
  children,
  unResetFilter,
}) => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const client = useApolloClient();

  // const getShowSubSidebar = useQuery<{ showSubSidebar: boolean }>(GET_SHOW_SUB_SIDEBAR);
  // const showSubSidebar =
  //   getShowSubSidebar && getShowSubSidebar.data && getShowSubSidebar.data.showSubSidebar;

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    if (setActiveStepFromProps) setActiveStepFromProps(activeStep + 1);
    if (onClickNext) onClickNext();
    //if (!unResetFilter) resetSearchFilters(client);
  };

  const handleBack = () => {
    if (activeStep > 0) {
      setActiveStep((prevActiveStep) => prevActiveStep - 1);
      if (setActiveStepFromProps) setActiveStepFromProps(activeStep - 1);
      if (onClickPrev) onClickPrev();
      //if (!unResetFilter) resetSearchFilters(client);
    }
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  // const hideSubSidebar = () => {
  //   client.writeData({ data: { showSubSidebar: false } });
  //   client.writeData({ data: { clickedCible: undefined } });
  // };

  const fromPath = history && history.location && history.location.state && history.location.state.from;

  useEffect(() => {
    if (fromPath) {
      handleReset();
    }
  }, [fromPath]);

  return (
    <div className={fullWidth ? classnames(classes.root, classes.fullWidth) : classes.root}>
      <Box className={classes.stepperHeader}>
        <Box display="flex" alignItems="center">
          {backBtnText ? (
            <NewCustomButton
              variant="text"
              startIcon={<ArrowBack />}
              //onClick={backToHome ? backToHome : showSubSidebar ? hideSubSidebar : handleBack}
              onClick={backToHome ? backToHome : handleBack}
              style={{ textTransform: 'none' }}
            >
              {backBtnText}
            </NewCustomButton>
          ) : (
            <IconButton
              color="inherit"
              //onClick={backToHome ? backToHome : showSubSidebar ? hideSubSidebar : handleBack}
              onClick={backToHome ? backToHome : handleBack}
            >
              <ArrowBack />
            </IconButton>
          )}
        </Box>
        <Typography className={classes.title}>{title}</Typography>
        {noFinalStepNext && steps.length - 1 === activeStep ? null : finalStep && activeStep === steps.length - 1 ? (
          <Box className={classes.btnActionsContainer}>
            <NewCustomButton
              theme="transparent"
              disabled={(activeStep === 0 && !backToHome) || disablePrevBtn}
              onClick={
                activeStep === 0 && backToHome
                  ? backToHome
                  : // : showSubSidebar
                    // ? hideSubSidebar
                    handleBack
              }
            >
              Retour
            </NewCustomButton>
            <NewCustomButton
              onClick={finalStep && finalStep.action}
              disabled={(finalStep && finalStep.loading) || disableNextBtn}
            >
              {finalStep && finalStep.buttonLabel}
            </NewCustomButton>
          </Box>
        ) : activeStep === 0 ? (
          <NewCustomButton onClick={handleNext} disabled={disableNextBtn}>
            {finished ? 'Confirmer mes sélections' : 'Suivant'}
          </NewCustomButton>
        ) : activeStep === steps.length - 1 ? (
          <NewCustomButton
            theme="transparent"
            disabled={(activeStep === 0 && !backToHome) || disablePrevBtn}
            onClick={
              activeStep === 0 && backToHome
                ? backToHome
                : // : showSubSidebar
                  // ? hideSubSidebar
                  handleBack
            }
          >
            Retour
          </NewCustomButton>
        ) : (
          <Box className={classes.btnActionsContainer}>
            <NewCustomButton
              theme="transparent"
              disabled={(activeStep === 0 && !backToHome) || disablePrevBtn}
              onClick={
                activeStep === 0 && backToHome
                  ? backToHome
                  : // : showSubSidebar
                    // ? hideSubSidebar
                    handleBack
              }
            >
              Retour
            </NewCustomButton>
            <NewCustomButton onClick={handleNext} disabled={disableNextBtn}>
              {finished ? 'Confirmer mes sélections' : 'Suivant'}
            </NewCustomButton>
          </Box>
        )}
      </Box>
      <MUIStepper activeStep={activeStep} alternativeLabel={true} className={classes.stepperRoot}>
        {steps.map((step) => (
          <MUIStep key={step.title}>
            <StepLabel>{step.title}</StepLabel>
          </MUIStep>
        ))}
      </MUIStepper>
      {children}
      <Box width="100%" className={classes.stepperContainer}>
        <div className={classes.content}>
          {activeStep === steps.length ? (
            <Fragment>
              {resume && resume}
              <div className={classes.btnActionsContainer}>
                <NewCustomButton theme="transparent" fullWidth={true} onClick={where === 'inCreateActualite' ? handleBack : handleReset}>
                  Retour
                </NewCustomButton>
                <NewCustomButton
                  fullWidth={true}
                  onClick={onConfirm ? onConfirm : () => {}}
                  disabled={loading ? true : false}
                >
                  {loading ? 'Chargement ...' : 'Confirmer'}
                </NewCustomButton>
              </div>
            </Fragment>
          ) : (
            <div className={classes.instructions}>{steps && steps.length > 0 && steps[activeStep].content}</div>
          )}
        </div>
      </Box>
    </div>
  );
};

// activeStep === steps.length - 1

export default Stepper;
