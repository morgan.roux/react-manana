import gql from 'graphql-tag';
import { FULL_TRAITEMENT_AUTO_INFO } from './../auto/traitement-automatique/fragment';
import { FULL_USER_INFO } from './../iam/user/fragment';
import { FULL_SUIVI_APPEL } from '../basis/suiviAppel/fragment';
import { PARAMETRE_LABORATOIRE_INFO } from '../partenaire-service/laboratoire/fragment';
import { PRT_CONTACT_SEARCH_INFO } from '../partenaire-service/contact/fragment';
import { SEARCH_TODO_PROJET_INFO } from '../basis/todo-projet/fragment';
import { SEARCH_TODO_ACTION_INFO } from '../basis/todo-action/fragment';
import { SEARCH_INFORMATION_LIAISON_INFO } from '../basis/information-liaison/fragment';

export const SEARCH = gql`
  query SEARCH(
    $index: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $include: [String]
    $exclude: [String]
  ) {
    search(
      index: $index
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
      include: $include
      exclude: $exclude
    ) {
      total
      data {
        ... on TATraitement {
          ...FullTraitementInfo
        }
        ... on User {
          ...FullUserInfo
        }
        ... on SuiviAppelType {
          ...suiviAppelInfo
        }
      }
    }
  }
  ${FULL_TRAITEMENT_AUTO_INFO}
  ${FULL_USER_INFO}
  ${FULL_SUIVI_APPEL}
`;

export const SEARCH_PARAMETRE_LABORATOIRES = gql`
  query SEARCH_PARAMETRE_LABORATOIRES(
    $index: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $include: [String]
    $exclude: [String]
  ) {
    search(
      index: $index
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
      include: $include
      exclude: $exclude
    ) {
      total
      data {
        ... on Laboratoire {
          ...LaboratoireInfo
        }
      }
    }
  }
  ${PARAMETRE_LABORATOIRE_INFO}
`;

export const SEARCH_PRT_CONTACT = gql`
  query SEARCH_PRT_CONTACT(
    $index: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $include: [String]
    $exclude: [String]
  ) {
    search(
      index: $index
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
      include: $include
      exclude: $exclude
    ) {
      total
      data {
        ... on PRTContact {
          ...PRTContactInfo
        }
      }
    }
  }
  ${PRT_CONTACT_SEARCH_INFO}
`;

export const SEARCH_TODO_PROJET = gql`
  query SEARCH_TODO_PROJET(
    $index: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $include: [String]
    $exclude: [String]
  ) {
    search(
      index: $index
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
      include: $include
      exclude: $exclude
    ) {
      total
      data {
        ... on TodoProjet {
          ...SearchTodoProjetInfo
        }
      }
    }
  }
  ${SEARCH_TODO_PROJET_INFO}
`;

export const SEARCH_TODO_ACTION = gql`
  query SEARCH_TODO_ACTION(
    $index: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $include: [String]
    $exclude: [String]
  ) {
    search(
      index: $index
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
      include: $include
      exclude: $exclude
    ) {
      total
      data {
        ... on TodoAction {
          ...SearchTodoActionInfo
          sousActions {
            ...SearchTodoActionInfo
          }
        }
      }
    }
  }
  ${SEARCH_TODO_ACTION_INFO}
`;

export const SEARCH_INFORMATION_LIAISON = gql`
  query SEARCH_INFORMATION_LIAISON(
    $index: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $include: [String]
    $exclude: [String]
  ) {
    search(
      index: $index
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
      include: $include
      exclude: $exclude
    ) {
      total
      data {
        ... on InformationLiaison {
          ...SearchInformationLiaisonInfo
        }
      }
    }
  }
  ${SEARCH_INFORMATION_LIAISON_INFO}
`;

export const COUNT = gql`
  query COUNT($index: [String], $query: JSON) {
    count(index: $index, query: $query)
  }
`;

export const BULK_COUNT = gql`
  query BULK_COUNT($queries: JSON) {
    bulkCount(queries: $queries)
  }
`;

export const AGGREGATE_DATE_HISTOGRAM_BY_DAY = gql`
  query AGGREGATE_DATE_HISTOGRAM_BY_DAY(
    $index: [String]
    $query: JSON
    $startFieldName: String
    $endFieldName: String
    $startDate: Date
    $endDate: Date
  ) {
    aggregateDateHistogramByDay(
      index: $index
      query: $query
      startFieldName: $startFieldName
      endFieldName: $endFieldName
      startDate: $startDate
      endDate: $endDate
    ) {
      min
      max
      data {
        date
        total
      }
    }
  }
`;
