import gql from 'graphql-tag';

export const SEARCH_TODO_SOUS_PROJET_INFO = gql`
  fragment SearchTodoSousProjetInfo on TodoProjet {
    id
    idFonction
    idTache
    idUser
    dataType
    ordre
    nom
    archive
    type

    participants {
      id
    }
    couleur {
      id
      code
      libelle
    }
  }
`;

export const SEARCH_TODO_PROJET_INFO = gql`
  fragment SearchTodoProjetInfo on TodoProjet {
    id
    idFonction
    idTache
    idUser
    dataType
    ordre
    nom
    archive
    supprime
    partage
    type
    createdAt
    updatedAt
    stats {
      user {
        id
      }
      countsByStatus {
        status
        todoCount
        teamCount
      }
    }
    sousProjets {
      ...SearchTodoSousProjetInfo
    }
    parent {
      id
    }
    createdBy {
      id
      fullName
      photo {
        id
        publicUrl
      }
    }
    updatedBy {
      id
      fullName
      photo {
        id
        publicUrl
      }
    }
    participants {
      id
      fullName
      photo {
        id
        publicUrl
      }
    }
    inFavorisIdUsers
    couleur {
      id
      code
      libelle
    }
  }
  ${SEARCH_TODO_SOUS_PROJET_INFO}
`;
