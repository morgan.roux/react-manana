import gql from 'graphql-tag';

export const DO_CHANGE_PHARMACIE_GROUPEMENT = gql`
  mutation CHANGE_PHARMACIE_GROUPEMENT($idPharmacie: String!, $idGroupement: String!) {
    changePharmacieGroupement(idPharmacie: $idPharmacie, idGroupement: $idGroupement)
  }
`;
