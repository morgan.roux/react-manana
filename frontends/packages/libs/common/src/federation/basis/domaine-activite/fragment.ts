import gql from 'graphql-tag';
import { FICHIER_INFO } from '../fragment';

export const DOMAINE_ACTIVITE_INFO = gql`
  fragment DomaineActiviteInfo on DomaineActivite {
    id
    code
    libelle
    sousDomaine
    parDefaut
    connexionLandingPage {
      ...FichierInfo
    }
  }
  ${FICHIER_INFO}
`;
