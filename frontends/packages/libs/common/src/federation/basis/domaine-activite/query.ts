import gql from 'graphql-tag';
import { DOMAINE_ACTIVITE_INFO } from './fragment';

export const GET_DOMAINE_ACTIVITE = gql`
  query GET_DOMAINE_ACTIVITE($id: ID!) {
    domaineActivite(id: $id) {
      ...DomaineActiviteInfo
    }
  }
  ${DOMAINE_ACTIVITE_INFO}
`;

export const GET_DOMAINE_ACTIVITES = gql`
  query GET_DOMAINE_ACTIVITES($paging: OffsetPaging, $filter: DomaineActiviteFilter, $sorting: [DomaineActiviteSort!]) {
    domaineActivites(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...DomaineActiviteInfo
      }
    }
  }
  ${DOMAINE_ACTIVITE_INFO}
`;
