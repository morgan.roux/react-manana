import gql from 'graphql-tag';
import { SEARCH_TODO_ACTION_INFO } from './fragment';

export const DO_CREATE_ONE_TODO_ACTION = gql`
  mutation CREATE_ONE_TODO_ACTION($input: TodoActionInput!) {
    createOneTodoAction(input: $input) {
      ...SearchTodoActionInfo
    }
  }
  ${SEARCH_TODO_ACTION_INFO}
`;

export const DO_UPDATE_ONE_TODO_ACTION = gql`
  mutation UPDATE_ONE_TODO_ACTION($id: String!, $update: TodoActionInput!) {
    updateOneTodoAction(id: $id, update: $update) {
      ...SearchTodoActionInfo
    }
  }
  ${SEARCH_TODO_ACTION_INFO}
`;

export const DO_UPDATE_ONE_TODO_ACTION_STATUT = gql`
  mutation UPDATE_ONE_TODO_ACTION_STATUT($id: String!, $code: String!) {
    updateTodoActionStatut(id: $id, code: $code) {
      ...SearchTodoActionInfo
    }
  }
  ${SEARCH_TODO_ACTION_INFO}
`;

export const DO_DELETE_ONE_TODO_ACTION = gql`
  mutation DELETE_ONE_TODO_ACTION($input: DeleteOneTodoActionInputType!) {
    deleteOneTodoAction(input: $input) {
      ...SearchTodoActionInfo
    }
  }
  ${SEARCH_TODO_ACTION_INFO}
`;

export const DO_UPDATE_TODO_ACTION_PARTICIPANTS = gql`
  mutation UPDATE_TODO_ACTION_PARTICIPANTS($input: TodoActionParticipantInput!) {
    updateTodoActionParticipants(input: $input) {
      ...SearchTodoActionInfo
    }
  }
  ${SEARCH_TODO_ACTION_INFO}
`;
