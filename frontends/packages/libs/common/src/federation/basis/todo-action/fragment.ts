import gql from 'graphql-tag';

export const SEARCH_TODO_ACTION_INFO = gql`
  fragment SearchTodoActionInfo on TodoAction {
    id
    ordre
    dataType
    description
    dateDebut
    dateFin
    idProjet
    idParent
    isPrivate
    idItemAssocie
    status
    supprime
    createdAt
    updatedAt
    fichierJoints {
      id
      nomOriginal
      type
      publicUrl
    }
    urgence {
      id
      code
      couleur
      libelle
    }
    nombreCommentaires
    createdBy {
      id
      fullName
    }
    updatedBy {
      id
      fullName
    }
    participants {
      id
      fullName
      photo {
        id
        publicUrl
      }
    }
    priorite
    todoIdUsers
    teamIdUsers
    item {
      id
      code
      name
    }
    projet {
      id
      nom
      participants {
        id
      }
    }
    importance {
      id
      couleur
      ordre
      libelle
    }
  }
`;
