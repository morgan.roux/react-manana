import gql from 'graphql-tag';

export const DEPARTEMENT_INFO = gql`
  fragment DepartementInfo on Departement {
    id
    code
    nom
  }
`;
