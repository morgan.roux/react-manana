import gql from 'graphql-tag';
import { DEPARTEMENT_INFO } from './fragment';

export const DEPARTEMENTS = gql`
  query DEPARTEMENTS(
    $paging: OffsetPaging
    $filter: DepartementFilter
    $sorting: [DepartementSort!]
  ) {
    departements(paging: $paging, filter: $filter, sorting: $sorting) {
      totalCount
      nodes {
        ...DepartementInfo
      }
    }
  }
  ${DEPARTEMENT_INFO}
`;
