import gql from 'graphql-tag';
import { INFORMATION_LIAISON_TYPE_INFO, SEARCH_INFORMATION_LIAISON_INFO } from './fragment';

export const GET_INFORMATION_LIAISON_TYPES = gql`
  query INFORMATION_LIAISON_TYPES(
    $paging: OffsetPaging
    $filter: InformationLiaisonTypeFilter
    $sorting: [InformationLiaisonTypeSort!]
  ) {
    informationLiaisonTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      totalCount
      nodes {
        ...InformationLiaisonTypeInfo
      }
    }
  }
  ${INFORMATION_LIAISON_TYPE_INFO}
`;

export const GET_INFORMATION_LIAISON_TYPE = gql`
  query INFORMATION_LIAISON_TYPE($id: ID!) {
    informationLiaisonType(id: $id) {
      ...InformationLiaisonTypeInfo
    }
  }
  ${INFORMATION_LIAISON_TYPE_INFO}
`;

export const GET_INFORMATION_LIAISON = gql`
  query INFORMATION_LIAISON($id: ID!) {
    informationLiaison(id: $id) {
      ...SearchInformationLiaisonInfo
    }
  }
  ${SEARCH_INFORMATION_LIAISON_INFO}
`;
