import gql from 'graphql-tag';
import { FULL_USER_INFO } from '../../iam/user/fragment';
import { FICHIER_INFO, IMPORTANCE_INFO, URGENCE_INFO } from '../fragment';

export const SEARCH_INFORMATION_LIAISON_INFO = gql`
  fragment SearchInformationLiaisonInfo on InformationLiaison {
    id
    type {
      id
    }
    idItem
    item {
      id
      name
      code
      codeItem
    }
    idItemAssocie
    idType
    idOrigine
    idOrigineAssocie
    origineAssocie {
      ... on Laboratoire {
        id
        nom
      }
      ... on PrestataireService {
        id
        nom
      }
      ... on User {
        ...FullUserInfo
      }
    }
    statut
    priority
    bloquant
    titre
    description
    createdAt
    updatedAt
    nbCollegue
    nbComment
    nbLue
    declarant {
      ...FullUserInfo
    }
    idFonction
    idTache
    importance {
      ...ImportanceInfo
    }
    urgence {
      ...UrgenceInfo
    }
    colleguesConcernees {
      id
      statut
      createdAt
      dateStatutModification
      updatedAt
      informationLiaison {
        id
      }
      userConcernee {
        ...FullUserInfo
      }
    }
    idFicheIncident
    idFicheAmelioration
    # ficheReclamation {
    #   id
    # }
    todoAction {
      id
      description
    }
    groupement {
      id
    }
    codeMaj
    supprime
    createdBy {
      id
    }
    updatedBy {
      id
    }
    createdAt
    updatedAt
    fichiers {
      ...FichierInfo
    }
    nbUserSmyleys
    nbPartage
    nbRecommandation
    isShared
  }
  ${IMPORTANCE_INFO}
  ${URGENCE_INFO}
  ${FULL_USER_INFO}
  ${FICHIER_INFO}
`;

export const INFORMATION_LIAISON_TYPE_INFO = gql`
  fragment InformationLiaisonTypeInfo on InformationLiaisonType {
    id
    code
    libelle
  }
  ${IMPORTANCE_INFO}
  ${URGENCE_INFO}
  ${FULL_USER_INFO}
  ${FICHIER_INFO}
`;
