import gql from 'graphql-tag';
import { SEARCH_INFORMATION_LIAISON_INFO } from './fragment';

export const UPDATE_INFORMATION_LIAISON_STATUT = gql`
  mutation UPDATE_INFORMATION_LIAISON_STATUT($code: String!, $id: String!, $information: String) {
    updateInformationLiaisonStatut(code: $code, id: $id, information: $information) {
      ...SearchInformationLiaisonInfo
    }
  }
  ${SEARCH_INFORMATION_LIAISON_INFO}
`;

export const CREATE_ONE_INFORMATION_LIAISON = gql`
  mutation CREATE_ONE_INFORMATION_LIAISON($input: InformationLiaisonInput!) {
    createOneInformationLiaison(input: $input) {
      ...SearchInformationLiaisonInfo
    }
  }
  ${SEARCH_INFORMATION_LIAISON_INFO}
`;

export const UPDATE_ONE_INFORMATION_LIAISON = gql`
  mutation UPDATE_ONE_INFORMATION_LIAISON($id: String!, $input: InformationLiaisonInput!) {
    updateOneInformationLiaison(id: $id, input: $input) {
      ...SearchInformationLiaisonInfo
    }
  }
  ${SEARCH_INFORMATION_LIAISON_INFO}
`;

export const DELETE_ONE_INFORMATION_LIAISON = gql`
  mutation DELETE_ONE_INFORMATION_LIAISON($id: String!) {
    deleteOneInformationLiaison(id: $id)
  }
`;
