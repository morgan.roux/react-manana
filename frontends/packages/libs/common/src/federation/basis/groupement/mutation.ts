export const DEPLOY_GROUPEMENT = gql`
  mutation DEPLOY_GROUPEMENT($idGroupement: String!) {
    deployGroupement(idGroupement: $idGroupement)
  }
`;
