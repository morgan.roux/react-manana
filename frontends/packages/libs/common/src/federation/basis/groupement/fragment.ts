import gql from 'graphql-tag';

export const GROUPEMENT_INFO = gql`
  fragment GroupementInfo on Groupement {
    id
    nom
    adresse1
    adresse2
    cp
    ville
    pays
    telBureau
    telMobile
    email
    site
    commentaire
    nomResponsable
    prenomResponsable
    sortie
    dateSortie
    createdAt
    updatedAt
    logo {
      id
      chemin
      nomOriginal
      type
      publicUrl
    }
  }
`;
