import gql from 'graphql-tag';
import { GROUPEMENT_INFO } from './fragment';

export const GET_GROUPEMENT = gql`
  query GROUPEMENT($id: ID!) {
    groupement(id: $id) {
      id
      ...GroupementInfo
    }
  }
  ${GROUPEMENT_INFO}
`;

export const GET_GROUPEMENTS = gql`
  query GROUPEMENTS($filter: GroupementFilter, $sorting: [GroupementSort!]) {
    groupements(filter: $filter, sorting: $sorting) {
      ...GroupementInfo
    }
  }
  ${GROUPEMENT_INFO}
`;
