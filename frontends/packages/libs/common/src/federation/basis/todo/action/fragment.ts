import gql from 'graphql-tag';
import { IMPORTANCE_INFO, URGENCE_INFO } from '../../fragment';

export const FULL_ACTION = gql`
  fragment ActionInfo on TodoAction {
    id
    ordre
    description
    dateDebut
    dateFin
    idParent
    idItemAssocie
    idImportance
    status
    importance {
      ...ImportanceInfo
    }
    urgence {
      ...UrgenceInfo
    }
  }
  ${IMPORTANCE_INFO}
  ${URGENCE_INFO}
`;
