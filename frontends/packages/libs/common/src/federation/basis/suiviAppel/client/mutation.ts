import gql from 'graphql-tag';
import { CLIENT_INFO } from './fragment';

export const CREATE_CLIENT = gql`
  mutation CREATE_CLIENT($input: ClientInput!) {
    createOneClient(input: $input) {
      ...clientInfo
    }
  }
  ${CLIENT_INFO}
`;
