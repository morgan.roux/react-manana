import gql from 'graphql-tag';

export const MINIMAL_USER_INFO = gql`
  fragment MinimalUserInfo on User {
    id
    dataType
    fullName
    photo {
      id
      chemin
      type
      nomOriginal
      publicUrl
    }
    phoneNumber
  }
`;

export const FULL_USER_INFO = gql`
  fragment FullUserInfo on User {
    id
    dataType
    fullName
    status
    lastPasswordChangedDate
    role {
      id
      code
      nom
    }
    photo {
      id
      chemin
      type
      nomOriginal
      publicUrl
    }
    phoneNumber
  }
`;

export const ME_USER_INFO = gql`
  fragment MeUserInfo on User {
    id
    dataType
    email
    login
    fullName
    theme
    jourNaissance
    moisNaissance
    anneeNaissance
    status
    emailConfirmed
    role {
      id
      code
      nom
      groupe
      type
    }
    photo {
      id
      chemin
      type
      nomOriginal
      publicUrl
    }
    phoneNumber
    phoneNumberConfirmed
    twoFactorEnabled
    lockoutEndDateUtc
    lockoutEnabled
    accessFailedCount
    userName
    lastLoginDate
    lastPasswordChangedDate
    isLockedOut
    isLockedOutPermanent
    isObligationChangePassword
    accessFailedCountBeforeLockoutPermanent
    dateCreation
    dateModification
    idGroupement
    codeTraitements
    contact {
      id
    }
    groupement {
      id
      nom
      adresse1
      adresse2
      cp
      ville
      pays
      telBureau
      telMobile
      email
      site
      commentaire
      nomResponsable
      prenomResponsable
      sortie
      dateSortie
      createdAt
      updatedAt
      logo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }
    pharmacies {
      id
      cip
      nom
      ville
      departement {
        id
        nom
        region {
          id
          nom
        }
      }
      idGroupement
    }
    laboratoire {
      id
      nom
    }
    prestataireService {
      id
      nom
    }
  }
`;
