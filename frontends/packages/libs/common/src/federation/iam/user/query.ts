import gql from 'graphql-tag';
import { MINIMAL_USER_INFO, FULL_USER_INFO, ME_USER_INFO } from './fragment';

export const COLLABORATEURS = gql`
  query COLLABORATEURS($idPharmacie: ID) {
    collaborateurs(idPharmacie: $idPharmacie) {
      ...MinimalUserInfo
    }
  }

  ${MINIMAL_USER_INFO}
`;

export const FULL_COLLABORATEURS = gql`
  query FULL_COLLABORATEURS($idPharmacie: ID) {
    collaborateurs(idPharmacie: $idPharmacie) {
      ...FullUserInfo
    }
  }

  ${FULL_USER_INFO}
`;

export const COLLABORATEUR = gql`
  query COLLABORATEUR($id: ID!) {
    user(id: $id) {
      ...MinimalUserInfo
    }
  }

  ${MINIMAL_USER_INFO}
`;

export const ME = gql`
  query ME {
    me {
      ...MeUserInfo
    }
  }

  ${ME_USER_INFO}
`;
