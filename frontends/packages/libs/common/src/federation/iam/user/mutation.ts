import gql from 'graphql-tag';
import { FULL_USER_INFO } from './fragment';

export const LOGIN = gql`
  mutation LOGIN($input: LoginInput!) {
    login(loginInput: $input) {
      user {
        ...FullUserInfo
      }
      accessToken
    }
  }

  ${FULL_USER_INFO}
`;

export const LOGOUT = gql`
  mutation LOGOUT($input: LogoutInput!) {
    logout(logoutInput: $input)
  }
`;
