import gql from 'graphql-tag';
import { ROLE_INFO } from './fragment';

export const ROLES = gql`
  query ROLES {
    roles {
      ...RoleInfo
    }
  }

  ${ROLE_INFO}
`;
