import gql from 'graphql-tag';

export const MINIMAL_USER_TRACKING_TIMER_INFO = gql`
  fragment MinimalUserTrackingTimerInfo on UserTrackingTimer {
    id
    token
    idUser
    type
    page
    path
    screenWidth
    screenHeight
    remainingTime
    elaspedTime
    lastIdleTime
    totalIdleTime
    lastActiveTime
    totalActiveTime
    ip
    userAgent
    idGroupement
    idPharmacie
    createdAt
    updatedAt
  }
`;
