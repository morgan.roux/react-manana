import gql from 'graphql-tag';
import { MINIMAL_USER_TRACKING_SESSION_INFO } from './fragment';

export const CREATE_ONE_USER_TRACKING_SESSION = gql`
  mutation CREATE_ONE_USER_TRACKING_SESSION($input: CreateOneUserTrackingSessionInput!) {
    createOneUserTrackingSession(input: $input) {
      ...MinimalUserTrackingSessionInfo
    }
  }

  ${MINIMAL_USER_TRACKING_SESSION_INFO}
`;
