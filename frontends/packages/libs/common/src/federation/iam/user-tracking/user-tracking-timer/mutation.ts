import gql from 'graphql-tag';
import { MINIMAL_USER_TRACKING_TIMER_INFO } from './fragment';

export const CREATE_ONE_USER_TRACKING_TIMER = gql`
  mutation CREATE_ONE_USER_TRACKING_TIMER($input: CreateOneUserTrackingTimerInput!) {
    createOneUserTrackingTimer(input: $input) {
      ...MinimalUserTrackingTimerInfo
    }
  }

  ${MINIMAL_USER_TRACKING_TIMER_INFO}
`;
