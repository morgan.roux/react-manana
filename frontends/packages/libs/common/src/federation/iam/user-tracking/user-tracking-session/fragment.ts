import gql from 'graphql-tag';

export const MINIMAL_USER_TRACKING_SESSION_INFO = gql`
  fragment MinimalUserTrackingSessionInfo on UserTrackingSession {
    id
    token
    idUser
    startDate
    endDate
    autoClosed
    clientType
    clientName
    clientVersion
    osName
    osVersion
    osPlatform
    deviceType
    deviceBrand
    deviceModel
    botName
    botCategory
    botUrl
    botProducerName
    botProducerUrl
    screenWidth
    screenHeight
    ip
    userAgent
    idGroupement
    idPharmacie
    createdAt
    updatedAt
  }
`;
