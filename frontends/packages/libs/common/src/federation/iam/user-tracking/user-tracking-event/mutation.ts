import gql from 'graphql-tag';
import { MINIMAL_USER_TRACKING_EVENT_INFO } from './fragment';

export const CREATE_ONE_USER_TRACKING_EVENT = gql`
  mutation CREATE_ONE_USER_TRACKING_EVENT($input: CreateOneUserTrackingEventInput!) {
    createOneUserTrackingEvent(input: $input) {
      ...MinimalUserTrackingEventInfo
    }
  }

  ${MINIMAL_USER_TRACKING_EVENT_INFO}
`;
