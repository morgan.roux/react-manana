import gql from 'graphql-tag';

export const MINIMAL_USER_TRACKING_EVENT_INFO = gql`
  fragment MinimalUserTrackingEventInfo on UserTrackingEvent {
    id
    token
    idUser
    type
    page
    path
    action
    log
    screenWidth
    screenHeight
    ip
    userAgent
    idGroupement
    idPharmacie
  }
`;
