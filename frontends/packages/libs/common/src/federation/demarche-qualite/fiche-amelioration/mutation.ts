import gql from 'graphql-tag';

import { FICHE_AMELIORATION_ACTION_INFO, FICHE_AMELIORATION_INFO, DELETE_FICHE_AMELIORATION_INFO } from './fragment';

export const CREATE_FICHE_AMELIORATION_ACTION = gql`
  mutation CREATE_FICHE_AMELIORATION_ACTION($idFicheAmelioration: String!, $action: DQFicheAmeliorationActionInput!) {
    createOneDQFicheAmeliorationAction(idFicheAmelioration: $idFicheAmelioration, action: $action) {
      ...FicheAmeliorationActionInfo
    }
  }
  ${FICHE_AMELIORATION_ACTION_INFO}
`;

export const CREATE_FICHE_AMELIORATION = gql`
  mutation CREATE_FICHE_AMELIORATION($idFicheIncident: String, $input: DQFicheAmeliorationInput!) {
    createOneDQFicheAmelioration(idFicheIncident: $idFicheIncident, input: $input) {
      ...FicheAmeliorationInfo
    }
  }
  ${FICHE_AMELIORATION_INFO}
`;

export const UPDATE_FICHE_AMELIORATION = gql`
  mutation UPDATE_FICHE_AMELIORATION($id: String!, $update: DQFicheAmeliorationInput!) {
    updateOneDQFicheAmelioration(id: $id, update: $update) {
      ...FicheAmeliorationInfo
    }
  }
  ${FICHE_AMELIORATION_INFO}
`;

export const DELETE_FICHE_AMELIORATION = gql`
  mutation DELETE_FICHE_AMELIORATION($input: DeleteOneDQFicheAmeliorationInput!) {
    deleteOneDQFicheAmelioration(input: $input) {
      ...DeleteFicheAmeliorationInfo
    }
  }
  ${DELETE_FICHE_AMELIORATION_INFO}
`;

export const DELETE_MANY_FICHE_AMELIORATIONS = gql`
  mutation DELETE_MANY_FICHE_AMELIORATIONS($input: DeleteManyDQFicheAmeliorationsInput!) {
    deleteManyDQFicheAmeliorations(input: $input) {
      deletedCount
    }
  }
`;

export const UPDATE_FICHE_AMELIORATION_STATUT = gql`
  mutation UPDATE_FICHE_AMELIORATION_STATUT($id: String!, $idItem: String!) {
    updateFicheAmeliorationStatut(id: $id, idItem: $idItem) {
      ...FicheAmeliorationInfo
    }
  }
  ${FICHE_AMELIORATION_INFO}
`;

export const UPDATE_FICHE_AMELIORATION_IMPORTANCE = gql`
  mutation UPDATE_FICHE_AMELIORATION_IMPORTANCE($id: String!, $idItem: String!) {
    updateFicheAmeliorationImportance(id: $id, idItem: $idItem) {
      ...FicheAmeliorationInfo
    }
  }
  ${FICHE_AMELIORATION_INFO}
`;

export const CREATE_ONE_FICHE_AMELIORATION_SOLUTION = gql`
  mutation CREATE_ONE_FICHE_AMELIORATION_SOLUTION($input: DQFicheAmeliorationSolutionInput!) {
    createOneFicheAmeliorationSolution(input: $input) {
      ...FicheAmeliorationInfo
    }
  }
  ${FICHE_AMELIORATION_INFO}
`;
