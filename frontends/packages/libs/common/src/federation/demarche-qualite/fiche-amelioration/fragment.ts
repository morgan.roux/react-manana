import gql from 'graphql-tag';

export const FICHE_AMELIORATION_INFO = gql`
  fragment FicheAmeliorationInfo on DQFicheAmelioration {
    id
    description
    idOrigine
    idOrigineAssocie
    dateAmelioration
    dateEcheance
    idType
    idCause
    idAction
    createdAt
    updatedAt
    origineAssocie {
      ... on User {
        id
        dataType
        fullName
        photo {
          id
          publicUrl
          chemin
          nomOriginal
          type
        }
      }
      ... on Laboratoire {
        id
        dataType
        nom
      }
      ... on PrestataireService {
        id
        dataType
        nom
      }
    }
    statut {
      id
      libelle
      code
    }
    cause {
      id
      libelle
    }
    type {
      id
      libelle
    }
    fichiers {
      id
      chemin
      nomOriginal
      type
      publicUrl
    }
    auteur {
      id
      fullName
    }
    origine {
      id
    }
    urgence {
      id
      code
      libelle
      couleur
    }
    importance {
      id
      ordre
      libelle
      couleur
    }
    fonction {
      id
      libelle
    }
    tache {
      id
      libelle
    }
    fonctionAInformer {
      id
      libelle
    }
    tacheAInformer {
      id
      libelle
    }
    participants {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }
    participantsAInformer {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }
    reunion {
      id
    }
  }
`;

export const DELETE_FICHE_AMELIORATION_INFO = gql`
  fragment DeleteFicheAmeliorationInfo on DQFicheAmeliorationDeleteResponse {
    id
    description
  }
`;

export const FICHE_AMELIORATION_ACTION_INFO = gql`
  fragment FicheAmeliorationActionInfo on DQFicheAmeliorationAction {
    id
    description
    dateHeureMiseEnplace
    createdAt
    updatedAt
    fichiers {
      id
      chemin
      type
      nomOriginal
      publicUrl
    }
  }
`;
