import gql from 'graphql-tag';

import {
  MINIMAL_DOCUMENT_INFO,
  MINIMAL_ENREGISTREMENT_INFO,
  MINIMAL_MEMO_INFO,
  MINIMAL_PROCEDURE_INFO,
  MINIMAL_AFFICHE_INFO,
  FULL_DOCUMENT_INFO,
  FULL_AFFICHE_INFO,
  FULL_ENREGISTREMENT_INFO,
  FULL_MEMO_INFO,
  FULL_PROCEDURE_INFO,
} from './fragment';

export const CREATE_OUTIL = gql`
  mutation CREATE_OUTIL($typologie: String!, $outilInput: DQOutilInput!) {
    createDQOutil(typologie: $typologie, outilInput: $outilInput) {
      ... on DQAffiche {
        ...FullAfficheInfo
      }
      ... on DQDocument {
        ...FullDocumentInfo
      }
      ... on DQEnregistrement {
        ...FullEnregistrementInfo
      }
      ... on DQMemo {
        ...FullMemoInfo
      }
      ... on DQProcedure {
        ...FullProcedureInfo
      }
    }
  }
  ${FULL_DOCUMENT_INFO}
  ${FULL_AFFICHE_INFO}
  ${FULL_ENREGISTREMENT_INFO}
  ${FULL_MEMO_INFO}
  ${FULL_PROCEDURE_INFO}
`;

export const UPDATE_OUTIL = gql`
  mutation UPDATE_OUTIL($id: ID!, $typologie: String!, $outilInput: DQOutilInput!) {
    updateDQOutil(id: $id, typologie: $typologie, outilInput: $outilInput) {
      ... on DQAffiche {
        ...FullAfficheInfo
      }
      ... on DQDocument {
        ...FullDocumentInfo
      }
      ... on DQEnregistrement {
        ...FullEnregistrementInfo
      }
      ... on DQMemo {
        ...FullMemoInfo
      }
      ... on DQProcedure {
        ...FullProcedureInfo
      }
    }
  }
  ${FULL_AFFICHE_INFO}
  ${FULL_DOCUMENT_INFO}
  ${FULL_ENREGISTREMENT_INFO}
  ${FULL_MEMO_INFO}
  ${FULL_PROCEDURE_INFO}
`;

export const DELETE_OUTIL = gql`
  mutation DELETE_OUTIL($id: ID!, $typologie: String!) {
    deleteDQOutil(id: $id, typologie: $typologie) {
      ... on DQAffiche {
        ...AfficheInfo
      }
      ... on DQDocument {
        ...DocumentInfo
      }
      ... on DQEnregistrement {
        ...EnregistrementInfo
      }
      ... on DQMemo {
        ...MemoInfo
      }
      ... on DQProcedure {
        ...ProcedureInfo
      }
    }
  }
  ${MINIMAL_DOCUMENT_INFO}
  ${MINIMAL_AFFICHE_INFO}
  ${MINIMAL_ENREGISTREMENT_INFO}
  ${MINIMAL_MEMO_INFO}
  ${MINIMAL_PROCEDURE_INFO}
`;
