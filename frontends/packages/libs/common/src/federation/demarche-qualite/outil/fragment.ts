import gql from 'graphql-tag';

/*

NOT WORKING : Find another solution

const createFullOutilFragment = (type: string) => {
  const interpolated = `
  fragment ${type}Info on DQ${type} {
    id
    typologie
    ordre
    libelle
    createdAt
    updatedAt
    fichier {
      id
      chemin
      type
      nomOriginal
    }
  } 
`;
  return gql(interpolated);
};



const createFullMinimalFragment = (type: string) => {
  const interpolated = `
  fragment ${type}Info on DQ${type} {
    id
    typologie
    ordre
    libelle
    createdAt
    updatedAt
  } 
`;
  return gql(interpolated);
};
*/

export const FULL_ENREGISTREMENT_INFO = gql`
  fragment FullEnregistrementInfo on DQEnregistrement {
    id
    typologie
    ordre
    code
    libelle
    createdAt
    updatedAt
    fichier {
      id
      chemin
      type
      nomOriginal
      publicUrl
    }
  }
`;
export const FULL_AFFICHE_INFO = gql`
  fragment FullAfficheInfo on DQAffiche {
    id
    typologie
    ordre
    code
    libelle
    createdAt
    updatedAt
    fichier {
      id
      chemin
      type
      nomOriginal
      publicUrl
    }
  }
`;
export const FULL_DOCUMENT_INFO = gql`
  fragment FullDocumentInfo on DQDocument {
    id
    typologie
    ordre
    code
    libelle
    createdAt
    updatedAt
    fichier {
      id
      chemin
      type
      nomOriginal
      publicUrl
    }
  }
`;
export const FULL_PROCEDURE_INFO = gql`
  fragment FullProcedureInfo on DQProcedure {
    id
    typologie
    ordre
    code
    libelle
    createdAt
    updatedAt
    fichier {
      id
      chemin
      type
      nomOriginal
      publicUrl
    }
  }
`;
export const FULL_MEMO_INFO = gql`
  fragment FullMemoInfo on DQMemo {
    id
    typologie
    ordre
    code
    libelle
    createdAt
    updatedAt
    fichier {
      id
      chemin
      type
      nomOriginal
      publicUrl
    }
  }
`;

export const MINIMAL_ENREGISTREMENT_INFO = gql`
  fragment EnregistrementInfo on DQEnregistrement {
    id
    typologie
    code
    ordre
    libelle
    createdAt
    updatedAt
  }
`;

export const MINIMAL_AFFICHE_INFO = gql`
  fragment AfficheInfo on DQAffiche {
    id
    typologie
    code
    ordre
    libelle
    createdAt
    updatedAt
  }
`;
export const MINIMAL_DOCUMENT_INFO = gql`
  fragment DocumentInfo on DQDocument {
    id
    typologie
    code
    ordre
    libelle
    createdAt
    updatedAt
  }
`;
export const MINIMAL_PROCEDURE_INFO = gql`
  fragment ProcedureInfo on DQProcedure {
    id
    typologie
    code
    ordre
    libelle
    createdAt
    updatedAt
  }
`;
export const MINIMAL_MEMO_INFO = gql`
  fragment MemoInfo on DQMemo {
    id
    typologie
    code
    ordre
    libelle
    createdAt
    updatedAt
  }
`;
