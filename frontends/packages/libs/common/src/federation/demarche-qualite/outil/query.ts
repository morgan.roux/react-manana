import gql from 'graphql-tag';
import {
  FULL_AFFICHE_INFO,
  FULL_DOCUMENT_INFO,
  FULL_ENREGISTREMENT_INFO,
  FULL_MEMO_INFO,
  FULL_PROCEDURE_INFO,
  MINIMAL_PROCEDURE_INFO,
  MINIMAL_MEMO_INFO,
  MINIMAL_ENREGISTREMENT_INFO,
  MINIMAL_DOCUMENT_INFO,
  MINIMAL_AFFICHE_INFO,
} from './fragment';

import { MINIMAL_CHECKLIST_INFO } from './../checklist/fragment';

export const GET_OUTILS = gql`
  query GET_OUTILS($typologie: String!) {
    dqOutils(typologie: $typologie) {
      ... on DQAffiche {
        ...FullAfficheInfo
      }
      ... on DQDocument {
        ...FullDocumentInfo
      }
      ... on DQEnregistrement {
        ...FullEnregistrementInfo
      }
      ... on DQMemo {
        ...FullMemoInfo
      }
      ... on DQProcedure {
        ...FullProcedureInfo
      }
    }
  }
  ${FULL_DOCUMENT_INFO}
  ${FULL_AFFICHE_INFO}
  ${FULL_ENREGISTREMENT_INFO}
  ${FULL_MEMO_INFO}
  ${FULL_PROCEDURE_INFO}
`;

export const GET_OUTIL = gql`
  query GET_OUTIL($id: ID!, $typologie: String!) {
    dqOutil(id: $id, typologie: $typologie) {
      ... on DQAffiche {
        ...FullAfficheInfo
      }
      ... on DQDocument {
        ...FullDocumentInfo
      }
      ... on DQEnregistrement {
        ...FullEnregistrementInfo
      }
      ... on DQMemo {
        ...FullMemoInfo
      }
      ... on DQProcedure {
        ...FullProcedureInfo
      }
    }
  }
  ${FULL_DOCUMENT_INFO}
  ${FULL_AFFICHE_INFO}
  ${FULL_ENREGISTREMENT_INFO}
  ${FULL_MEMO_INFO}
  ${FULL_PROCEDURE_INFO}
`;

export const GET_OUTILS_GROUPED_BY_TYPOLOGIE = gql`
  query GET_OUTILS_GROUPED_BY_TYPOLOGIE {
    dqOutilsGroupedByTypologie {
      affiches {
        ...AfficheInfo
      }
      enregistrements {
        ...EnregistrementInfo
      }
      memos {
        ...MemoInfo
      }
      procedures {
        ...ProcedureInfo
      }
      documents {
        ...DocumentInfo
      }
      checklists {
        ...ChecklistInfo
      }
    }
  }
  ${MINIMAL_DOCUMENT_INFO}
  ${MINIMAL_AFFICHE_INFO}
  ${MINIMAL_ENREGISTREMENT_INFO}
  ${MINIMAL_MEMO_INFO}
  ${MINIMAL_PROCEDURE_INFO}
  ${MINIMAL_CHECKLIST_INFO}
`;
