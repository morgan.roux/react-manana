import gql from 'graphql-tag';
import { MINIMAL_TACHE_INFO, RESPONSABLE_TYPE_INFO, MINIMAL_TACHE_RESPONSABLE_INFO } from './fragment';

import { MINIMAL_USER_INFO } from '../../iam/user/fragment';

export const GET_FONCTIONS = gql`
  query GET_FONCTIONS(
    $paging: OffsetPaging
    $filter: DQMTFonctionFilter
    $sorting: [DQMTFonctionSort!]
    $idPharmacie: String!
  ) {
    dQMTFonctions(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        id
        ordre(idPharmacie: $idPharmacie)
        libelle(idPharmacie: $idPharmacie)
        nombreTaches
        active(idPharmacie: $idPharmacie)
      }
    }
  }
`;

export const GET_FONCTIONS_FULL_INFOS = gql`
  query GET_FONCTIONS_FULL_INFOS(
    $paging: OffsetPaging
    $filter: DQMTFonctionFilter
    $sorting: [DQMTFonctionSort!]
    $idPharmacie: String!
  ) {
    dQMTFonctions(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        id
        ordre(idPharmacie: $idPharmacie)
        libelle(idPharmacie: $idPharmacie)
        nombreTaches
        idProjet(idPharmacie: $idPharmacie)
        taches {
          id
          ordre
          libelle
          idProjet(idPharmacie: $idPharmacie)
        }
      }
    }
  }
`;

export const GET_FONCTIONS_WITH_RESPONSABLE_INFOS = gql`
  query GET_FONCTIONS_WITH_RESPONSABLE_INFOS(
    $paging: OffsetPaging
    $filter: DQMTFonctionFilter
    $sorting: [DQMTFonctionSort!]
    $idPharmacie: String!
  ) {
    dQMTFonctions(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        id
        ordre(idPharmacie: $idPharmacie)
        libelle(idPharmacie: $idPharmacie)
        nombreTaches
        idProjet(idPharmacie: $idPharmacie)
        active(idPharmacie: $idPharmacie)
        collaborateurResponsables(idPharmacie: $idPharmacie) {
          idFonction
          idTache
          type {
            id
            code
            ordre
            couleur
            libelle
            max
          }
          responsables {
            id
            fullName
          }
        }
        taches {
          id
          ordre
          libelle
          idProjet(idPharmacie: $idPharmacie)
          active(idPharmacie: $idPharmacie)
          collaborateurResponsables(idPharmacie: $idPharmacie) {
            idFonction
            idTache
            type {
              id
              code
              ordre
              couleur
              libelle
              max
            }
            responsables {
              id
              fullName
            }
          }
        }
      }
    }
  }
`;

export const GET_FONCTION = gql`
  query GET_FONCTION($id: ID!, $idPharmacie: String!) {
    dQMTFonction(id: $id) {
      id
      ordre(idPharmacie: $idPharmacie)
      libelle(idPharmacie: $idPharmacie)
      nombreTaches
      active(idPharmacie: $idPharmacie)
      taches {
        id
        ordre(idPharmacie: $idPharmacie)
        libelle(idPharmacie: $idPharmacie)
        active(idPharmacie: $idPharmacie)
      }
    }
  }
`;

export const GET_TACHE = gql`
  query GET_TACHE($id: ID!) {
    dQMTTache(id: $id) {
      ...TacheInfo
    }
  }

  ${MINIMAL_TACHE_INFO}
`;

export const GET_RESPONSABLE_TYPES = gql`
  query GET_RESPONSABLE_TYPES(
    $paging: OffsetPaging
    $filter: DQMTResponsableTypeFilter
    $sorting: [DQMTResponsableTypeSort!]
  ) {
    dQMTResponsableTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...ResponsableTypeInfo
      }
    }
  }

  ${RESPONSABLE_TYPE_INFO}
`;

export const GET_TACHE_RESPONSABLES = gql`
  query GET_TACHE_RESPONSABLES(
    $paging: OffsetPaging
    $filter: DQMTTacheResponsableFilter
    $sorting: [DQMTTacheResponsableSort!]
  ) {
    dQMTTacheResponsables(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...TacheResponsableInfo
      }
    }
  }

  ${MINIMAL_TACHE_RESPONSABLE_INFO}
`;

export const GET_TACHE_USER_RESPONSABLES = gql`
  query GET_TACHE_USER_RESPONSABLES($idTache: String!, $idPharmacie: String!) {
    dQMTTacheUserResponsables(idTache: $idTache, idPharmacie: $idPharmacie) {
      ...MinimalUserInfo
    }
  }

  ${MINIMAL_USER_INFO}
`;

export const GET_FONCTION_USER_RESPONSABLES = gql`
  query GET_FONCTION_USER_RESPONSABLES($idFonction: String!, $idPharmacie: String!) {
    dQMTFonctionUserResponsables(idFonction: $idFonction, idPharmacie: $idPharmacie) {
      ...MinimalUserInfo
    }
  }

  ${MINIMAL_USER_INFO}
`;
