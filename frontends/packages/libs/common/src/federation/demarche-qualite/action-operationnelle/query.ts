import gql from 'graphql-tag';
import { ACTION_OPERATIONNELLE } from './fragment';

export const GET_ACTIONS_OPERATIONNELLES = gql`
  query GET_ACTIONS_OPERATIONNELLES(
    $paging: OffsetPaging
    $filter: DQActionOperationnelleFilter
    $sorting: [DQActionOperationnelleSort!]
  ) {
    dQActionOperationnelles(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...ActionOperationnelle
      }
      totalCount
    }
  }

  ${ACTION_OPERATIONNELLE}
`;

export const GET_ACTION_OPERATIONNELLE = gql`
  query GET_ACTION_OPERATIONNELLE($id: ID!) {
    dQActionOperationnelle(id: $id) {
      ...ActionOperationnelle
    }
  }
  ${ACTION_OPERATIONNELLE}
`;

export const GET_ENCOURS_ACTION_OPERATIONNELLES = gql`
  query GET_ENCOURS_ACTION_OPERATIONNELLES {
    dQEncoursActionOperationnelles {
      ...ActionOperationnelle
    }
  }

  ${ACTION_OPERATIONNELLE}
`;
