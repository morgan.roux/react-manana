import gql from 'graphql-tag';
import {
  MINIMAL_PROCEDURE_INFO,
  MINIMAL_MEMO_INFO,
  MINIMAL_ENREGISTREMENT_INFO,
  MINIMAL_DOCUMENT_INFO,
  MINIMAL_AFFICHE_INFO,
} from './../outil/fragment';
import { MINIMAL_CHECKLIST_INFO } from './../checklist/fragment';

export const FULL_EXIGENCE_INFO = gql`
  fragment FullExigenceInfo on DQExigence {
    id
    ordre
    finalite
    titre
    exemples {
      id
      ordre
      exemple
    }
    questions {
      id
      ordre
      question
    }
    outils {
      affiches {
        ...AfficheInfo
      }
      enregistrements {
        ...EnregistrementInfo
      }
      memos {
        ...MemoInfo
      }
      procedures {
        ...ProcedureInfo
      }
      documents {
        ...DocumentInfo
      }
      checklists {
        ...ChecklistInfo
      }
    }
  }

  ${MINIMAL_DOCUMENT_INFO}
  ${MINIMAL_AFFICHE_INFO}
  ${MINIMAL_ENREGISTREMENT_INFO}
  ${MINIMAL_MEMO_INFO}
  ${MINIMAL_PROCEDURE_INFO}
  ${MINIMAL_CHECKLIST_INFO}
`;

export const FULL_SOUS_THEME_INFO = gql`
  fragment FullSousThemeInfo on DQSousTheme {
    id
    ordre
    nom
    createdAt
    updatedAt
    nombreExigences
    exigences {
      ...FullExigenceInfo
    }
  }

  ${FULL_EXIGENCE_INFO}
`;

export const FULL_THEME_INFO = gql`
  fragment FullThemeInfo on DQTheme {
    id
    ordre
    nom
    createdAt
    updatedAt
    nombreSousThemes
    sousThemes {
      ...SousThemeInfo
    }
  }
  ${FULL_SOUS_THEME_INFO}
`;

export const THEME_INFO = gql`
  fragment ThemeInfo on DQTheme {
    id
    ordre
    nom
    createdAt
    updatedAt
    nombreSousThemes
  }
`;

export const SOUS_THEME_INFO = gql`
  fragment SousThemeInfo on DQSousTheme {
    id
    ordre
    nom
    createdAt
    updatedAt
    nombreExigences
  }
`;

export const MINIMAL_THEME_INFO = gql`
  fragment MinimalThemeInfo on DQTheme {
    id
    ordre
    nom
    createdAt
    updatedAt
    nombreSousThemes
  }
`;

export const MINIMAL_SOUS_THEME_INFO = gql`
  fragment MinimalSousThemeInfo on DQSousTheme {
    id
    ordre
    nom
    createdAt
    updatedAt
    nombreExigences
    exigences {
      id
      ordre
      finalite
      titre
      nombreOutils
      nombreOutils
    }
  }
`;

export const MINIMAL_EXIGENCE_INFO = gql`
  fragment MinimalExigenceInfo on DQExigence {
    id
    ordre
    finalite
    titre
  }
`;
