import gql from 'graphql-tag';
import {
  FULL_THEME_INFO,
  FULL_SOUS_THEME_INFO,
  MINIMAL_SOUS_THEME_INFO,
  FULL_EXIGENCE_INFO,
  THEME_INFO,
  SOUS_THEME_INFO,
} from './fragment';

export const GET_THEMES = gql`
  query GET_THEMES {
    dqThemes {
      ...ThemeInfo
      sousThemes {
        ...SousThemeInfo
      }
    }
  }
  ${THEME_INFO}
  ${SOUS_THEME_INFO}
`;

export const GET_FULL_THEMES = gql`
  query GET_FULL_THEMES {
    dqThemes {
      ...FullThemeInfo
    }
  }
  ${FULL_THEME_INFO}
`;

export const GET_THEME = gql`
  query GET_THEME($id: ID!) {
    dqTheme(id: $id) {
      ...FullThemeInfo
    }
  }
  ${FULL_THEME_INFO}
`;

export const GET_MINIMAL_SOUS_THEME = gql`
  query GET_MINIMAL_SOUS_THEME($id: ID!) {
    dqSousTheme(id: $id) {
      ...MinimalSousThemeInfo
    }
  }
  ${MINIMAL_SOUS_THEME_INFO}
`;

export const GET_FULL_SOUS_THEME = gql`
  query GET_FULL_SOUS_THEME($id: ID!) {
    dqSousTheme(id: $id) {
      ...FullSousThemeInfo
    }
  }
  ${FULL_SOUS_THEME_INFO}
`;

export const GET_EXIGENCE = gql`
  query GET_EXIGENCE($id: ID!) {
    dqExigence(id: $id) {
      ...FullExigenceInfo
    }
  }
  ${FULL_EXIGENCE_INFO}
`;
