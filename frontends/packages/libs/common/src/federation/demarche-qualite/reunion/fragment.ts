import gql from 'graphql-tag';
import { ACTION_OPERATIONNELLE } from '../action-operationnelle/fragment';
import { FICHE_AMELIORATION_INFO } from '../fiche-amelioration/fragment';
import { FICHE_INCIDENT_INFO } from '../fiche-incident/fragment';

export const FULL_REUNION_ACTION_INFO = gql`
  fragment ReunionActionInfo on DQReunionAction {
    id
    type
    idTodoAction
    createdAt
    updatedAt
    idTypeAssocie
    typeAssocie {
      ... on DQFicheAmelioration {
        ...FicheAmeliorationInfo
      }
      ... on DQFicheIncident {
        ...FicheIncidentInfo
      }
      ... on DQActionOperationnelle {
        ...ActionOperationnelle
      }
    }
  }
  ${FICHE_AMELIORATION_INFO}
  ${ACTION_OPERATIONNELLE}
  ${FICHE_INCIDENT_INFO}
`;

export const FULL_REUNION_INFO = gql`
  fragment FullReunionInfo on DQReunion {
    id
    description
    idUserAnimateur
    idResponsable
    createdAt
    updatedAt
    nombreTotalActions
    nombreTotalClotureActions
    dateReunion
    animateur {
      id
      fullName
      email
      role {
        id
        nom
      }
    }
    responsable {
      id
      fullName
      email
      role {
        id
        nom
      }
    }
    participants {
      id
      fullName
      email
      role {
        id
        nom
      }
    }
    actions {
      ...ReunionActionInfo
    }
  }

  ${FULL_REUNION_ACTION_INFO}
`;

export const MINIMAL_REUNION_INFO = gql`
  fragment ReunionInfo on DQReunion {
    id
    description
    idUserAnimateur
    idResponsable
    createdAt
    updatedAt
    nombreTotalActions
    nombreTotalClotureActions
    animateur {
      id
      fullName
      email
      role {
        id
        nom
      }
    }
    responsable {
      id
      fullName
      email
      role {
        id
        nom
      }
    }
    participants {
      id
      fullName
      email
      role {
        id
        nom
      }
    }
  }
`;
