import gql from 'graphql-tag';

import { FULL_EVALUATION_INFO, MINIMAL_EVALUATION_INFO } from './fragment';

export const CREATE_CHECKLIST_EVALUATION = gql`
  mutation CREATE_CHECKLIST_EVALUATION($idChecklist: ID!, $checklistEvaluationInput: DQChecklistEvaluationInput!) {
    createDQChecklistEvaluation(idChecklist: $idChecklist, checklistEvaluationInput: $checklistEvaluationInput) {
      ...FullChecklistEvaluationInfo
    }
  }
  ${FULL_EVALUATION_INFO}
`;

export const UPDATE_CHECKLIST_EVALUATION = gql`
  mutation UPDATE_CHECKLIST_EVALUATION($id: ID!, $checklistEvaluationInput: DQChecklistEvaluationInput!) {
    updateDQChecklistEvaluation(id: $id, checklistEvaluationInput: $checklistEvaluationInput) {
      ...FullChecklistEvaluationInfo
    }
  }
  ${FULL_EVALUATION_INFO}
`;

export const CLOTURE_CHECKLIST_EVALUATION = gql`
  mutation CLOTURE_CHECKLIST_EVALUATION($id: ID!, $checklistEvaluationInput: DQChecklistEvaluationClotureInput!) {
    clotureDQChecklistEvaluation(id: $id, checklistEvaluationInput: $checklistEvaluationInput) {
      ...FullChecklistEvaluationInfo
    }
  }
  ${FULL_EVALUATION_INFO}
`;

export const DELETE_CHECKLIST_EVALUATION = gql`
  mutation DELETE_CHECKLIST_EVALUATION($id: ID!) {
    deleteDQChecklistEvaluation(id: $id) {
      ...ChecklistEvaluationInfo
    }
  }
  ${MINIMAL_EVALUATION_INFO}
`;

// toggleDQChecklistSectionItemEvaluation
export const TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION = gql`
  mutation TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION($idChecklistEvaluation: ID!, $idChecklistSectionItem: ID!) {
    toggleDQChecklistSectionItemEvaluation(
      idChecklistEvaluation: $idChecklistEvaluation
      idChecklistSectionItem: $idChecklistSectionItem
    ) {
      ...FullChecklistEvaluationInfo
    }
  }
  ${FULL_EVALUATION_INFO}
`;
