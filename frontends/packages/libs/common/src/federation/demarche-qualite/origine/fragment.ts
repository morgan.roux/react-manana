import gql from 'graphql-tag';

export const ORIGINE_INFO = gql`
  fragment OrigineInfo on Origine {
    id
    libelle
    code
  }
`;

export const ORIGINE_WITH_NOMBRE_FICHE_AMELIORATIONS_INFO = gql`
  fragment OrigineInfoWithNombreFicheAmeliorations on Origine {
    id
    libelle
    code
    nombreFicheAmeliorations
  }
`;

export const ORIGINE_WITH_NOMBRE_FICHE_INCIDENTS_INFO = gql`
  fragment OrigineInfoWithNombreFicheIncidents on Origine {
    id
    libelle
    code
    nombreFicheIncidents
  }
`;
