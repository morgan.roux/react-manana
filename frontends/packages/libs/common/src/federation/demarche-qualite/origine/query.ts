import gql from 'graphql-tag';
import {
  ORIGINE_WITH_NOMBRE_FICHE_AMELIORATIONS_INFO,
  ORIGINE_WITH_NOMBRE_FICHE_INCIDENTS_INFO,
  ORIGINE_INFO,
} from './fragment';

export const GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS = gql`
  query GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS {
    origines {
      nodes {
        ...OrigineInfoWithNombreFicheAmeliorations
      }
    }
  }
  ${ORIGINE_WITH_NOMBRE_FICHE_AMELIORATIONS_INFO}
`;

export const GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS = gql`
  query GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS {
    origines {
      nodes {
        ...OrigineInfoWithNombreFicheIncidents
      }
    }
  }
  ${ORIGINE_WITH_NOMBRE_FICHE_INCIDENTS_INFO}
`;

export const GET_ORIGINES = gql`
  query GET_ORIGINES($paging: OffsetPaging, $filter: OrigineFilter, $sorting: [OrigineSort!]) {
    origines(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...OrigineInfo
      }
    }
  }

  ${ORIGINE_INFO}
`;
