import gql from 'graphql-tag';

import { FICHE_INCIDENT_INFO, SOLUTION_INFO, DELETE_FICHE_INCIDENT_INFO } from './fragment';

export const CREATE_FICHE_INCIDENT = gql`
  mutation CREATE_FICHE_INCIDENT($input: DQFicheIncidentInput!) {
    createOneDQFicheIncident(input: $input) {
      ...FicheIncidentInfo
    }
  }
  ${FICHE_INCIDENT_INFO}
`;

export const CREATE_SOLUTION = gql`
  mutation CREATE_SOLUTION($idFicheIncident: String!, $solution: DQSolutionInput!) {
    createOneDQSolution(idFicheIncident: $idFicheIncident, solution: $solution) {
      ...SolutionInfo
    }
  }
  ${SOLUTION_INFO}
`;

export const UPDATE_FICHE_INCIDENT = gql`
  mutation UPDATE_FICHE_INCIDENT($id: String!, $update: DQFicheIncidentInput!) {
    updateOneDQFicheIncident(id: $id, update: $update) {
      ...FicheIncidentInfo
    }
  }
  ${FICHE_INCIDENT_INFO}
`;

export const DELETE_FICHE_INCIDENT = gql`
  mutation DELETE_FICHE_INCIDENT($input: DeleteOneDQFicheIncidentInput!) {
    deleteOneDQFicheIncident(input: $input) {
      ...DeleteFicheIncidentInfo
    }
  }
  ${DELETE_FICHE_INCIDENT_INFO}
`;

export const DELETE_MANY_FICHE_INCIDENTS = gql`
  mutation DELETE_MANY_FICHE_INCIDENTS($input: DeleteManyDQFicheIncidentsInput!) {
    deleteManyDQFicheIncidents(input: $input) {
      deletedCount
    }
  }
`;

export const UPDATE_FICHE_INCIDENT_STATUT = gql`
  mutation UPDATE_FICHE_INCIDENT_STATUT($id: String!, $code: String!) {
    updateFicheIncidentStatut(id: $id, code: $code) {
      ...FicheIncidentInfo
    }
  }
  ${FICHE_INCIDENT_INFO}
`;
