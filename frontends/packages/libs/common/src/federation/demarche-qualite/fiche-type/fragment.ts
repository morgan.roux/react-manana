import gql from 'graphql-tag';

export const FICHE_TYPE_INFO = gql`
  fragment FicheTypeInfo on DQFicheType {
    id
    libelle
  }
`;
