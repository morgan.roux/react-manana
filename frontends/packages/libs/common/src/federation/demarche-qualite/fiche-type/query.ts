import gql from 'graphql-tag';
import { FICHE_TYPE_INFO } from './fragment';

export const GET_FICHE_TYPES = gql`
  query FICHE_TYPES($paging: OffsetPaging, $filter: DQFicheTypeFilter, $sorting: [DQFicheTypeSort!]) {
    dQFicheTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...FicheTypeInfo
      }
    }
  }

  ${FICHE_TYPE_INFO}
`;



export const GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE = gql`
  query GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE($paging: OffsetPaging, $filter: DQFicheTypeFilter, $sorting: [DQFicheTypeSort!],$idPharmacie:String) {
    dQFicheTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      id
      code
      libelle
      itemScoringOrdre(idPharmacie:$idPharmacie)
      itemScoringIdPersonnalisation(idPharmacie:$idPharmacie)
      }
    }
  }

`;
