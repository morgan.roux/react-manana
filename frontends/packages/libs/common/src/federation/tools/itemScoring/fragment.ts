import gql from 'graphql-tag';
/*
const ORIGINE = gql`
  fragment ItemScoringOrigine on Origine {
    id
    libelle
  }
`;

const STATUT = gql`
  fragment ItemScoringStatut on DQStatut {
    id
    libelle
    type
  }
`;
*/
const PARTICIPANT = gql`
  fragment ItemScoringParticipant on User {
    id
    fullName
    photo {
      id
      chemin
      nomOriginal
      publicUrl
    }
  }
`;

const URGENCE = gql`
  fragment ItemScoringUrgence on Urgence {
    id
    code
    libelle
    couleur
  }
`;

export const IMPORTANCE = gql`
  fragment ItemScoringImportance on Importance {
    id
    ordre
    libelle
    couleur
  }
`;
/*
const DQ_ACTION_OPERATIONELLE = gql`
  fragment ItemScoringDQActionOperationnelle on DQActionOperationnelle {
    id
    dataType
    description
    origine {
      ...ItemScoringOrigine
    }
    statut {
      ...ItemScoringStatut
    }
    participants {
      ...ItemScoringParticipant
    }
    urgence {
      ...ItemScoringUrgence
    }
    importance {
      ...ItemScoringImportance
    }
  }
  ${ORIGINE}
  ${STATUT}
  ${PARTICIPANT}
  ${URGENCE}
  ${IMPORTANCE}
`;

const DQ_FICHE_AMELIORATION = gql`
  fragment ItemScoringDQFicheAmelioration on DQFicheAmelioration {
    id
    dataType
    description
    origine {
      ...ItemScoringOrigine
    }
    statut {
      ...ItemScoringStatut
    }
    participants {
      ...ItemScoringParticipant
    }
    urgence {
      ...ItemScoringUrgence
    }
    importance {
      ...ItemScoringImportance
    }
  }
  ${ORIGINE}
  ${STATUT}
  ${PARTICIPANT}
  ${URGENCE}
  ${IMPORTANCE}
`;

const DQ_FICHE_INCIDENT = gql`
  fragment ItemScoringDQFicheIncident on DQFicheIncident {
    id
    dataType
    description
    origine {
      ...ItemScoringOrigine
    }
    statut {
      ...ItemScoringStatut
    }
    participants {
      ...ItemScoringParticipant
    }
    urgence {
      ...ItemScoringUrgence
    }
    importance {
      ...ItemScoringImportance
    }
  }
  ${ORIGINE}
  ${STATUT}
  ${PARTICIPANT}
  ${URGENCE}
  ${IMPORTANCE}
`;*/

export const FULL_ITEM_SCORING = gql`
  fragment ItemScoring on ItemScoring {
    id
    idItem
    idItemAssocie
    idUser
    description
    dateEcheance
    idUrgence
    statut
    nomItem
    ordreStatut
    urgence {
      ...ItemScoringUrgence
    }
    importance {
      ...ItemScoringImportance
    }
    participants {
      ...ItemScoringParticipant
    }
    idImportance
    nombreJoursRetard
    score
    item {
      id
      code
      codeItem
      name
    }
  }
  ${URGENCE}
  ${IMPORTANCE}
  ${PARTICIPANT}
`;

export const ITEM_SCORING_COUNT = gql`
  fragment ItemScoringCount on ItemScoringCount {
    urgence {
      ...ItemScoringUrgence
    }
    importance {
      ...ItemScoringImportance
    }
    occurence
  }
  ${URGENCE}
  ${IMPORTANCE}
`;

export const ITEM_SCORING_PERSONNALISATION = gql`
  fragment ItemScoringPersonnalisationInfo on ItemScoringPersonnalisation {
    id
    type
    idTypeAssocie
    ordre
    idPharmacie
  }
`;
