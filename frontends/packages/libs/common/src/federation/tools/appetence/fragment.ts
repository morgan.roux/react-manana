import gql from 'graphql-tag';
import { FULL_ITEM_INFO } from '../../basis/item/fragment';

export const FULL_APPETENCE_INFO = gql`
  fragment AppetenceInfo on Appetence {
    id
    idItem
    ordre
    createdAt
    updatedAt
    item {
      ...FullItemInfo
    }
  }
  ${FULL_ITEM_INFO}
`;
