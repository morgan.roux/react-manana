import gql from 'graphql-tag';
import { FULL_POINT_ACCES_INFO } from './fragment';

export const GET_POINT_ACCES = gql`
  query GET_POINT_ACCES(
    $paging: OffsetPaging
    $filter: RTPointAccesFilter
    $sorting: [RTPointAccesSort!]
  ) {
    rTPointAcces(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...RTPointAccesInfo
      }
    }
  }

  ${FULL_POINT_ACCES_INFO}
`;

