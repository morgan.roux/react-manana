import gql from 'graphql-tag';
import { FULL_CAPTEUR_INFO } from './fragment';

export const GET_CAPTEURS = gql`
  query GET_CAPTEURS(
    $paging: OffsetPaging
    $filter: RTCapteurFilter
    $sorting: [RTCapteurSort!]
  ) {
    rTCapteurs(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...RTCapteurInfo
      }
    }
  }

  ${FULL_CAPTEUR_INFO}
`;

