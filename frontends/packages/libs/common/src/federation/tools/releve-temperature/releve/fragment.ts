import gql from 'graphql-tag';

export const FULL_RELEVE_INFO = gql`
  fragment RTReleveInfo on RTReleve {
    id
    temperature
    type
    createdAt
    releveur {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }
    status {
      id
      code
      couleur
      libelle
    }
    frigo {
      id
      nom
      temperatureBasse
      temperatureHaute
      temporisation
    }
  }
`;

export const FULL_STATUS = gql`
  fragment RTStatus on RTStatus {
    id
    code
    couleur
    libelle
    createdAt
    updatedAt
  }
`;
