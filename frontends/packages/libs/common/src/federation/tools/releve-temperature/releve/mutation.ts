import gql from 'graphql-tag';
import { FULL_RELEVE_INFO } from './fragment';

export const CREATE_RELEVE = gql`
  mutation CREATE_RELEVE($input: RTReleveInput!) {
    createOneRTReleve(input: $input) {
      ...RTReleveInfo
    }
  }
  ${FULL_RELEVE_INFO}
`;

export const DELETE_ONE_RELEVE = gql`
  mutation DELETE_ONE_RELEVE($input: DeleteOneRTReleveInput!) {
    deleteOneRTReleve(input: $input) {
      id
    }
  }
`;
