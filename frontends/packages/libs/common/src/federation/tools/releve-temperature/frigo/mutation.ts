import gql from 'graphql-tag';
import { MINIMAL_FRIGO_INFO } from './fragment';

export const CREATE_FRIGO = gql`
  mutation CREATE_FRIGO($input: RTFrigoInput!) {
    createOneRTFrigo(input: $input) {
      ...RTFrigoInfo
    }
  }
  ${MINIMAL_FRIGO_INFO}
`;

export const UPDATE_FRIGO = gql`
  mutation UPDATE_FRIGO($id: String!, $input: RTFrigoInput!) {
    updateOneRTFrigo(id: $id, input: $input) {
      ...RTFrigoInfo
    }
  }
  ${MINIMAL_FRIGO_INFO}
`;

export const DELETE_ONE_FRIGO = gql`
  mutation DELETE_ONE_FRIGO($input: DeleteOneRTFrigoInput!) {
    deleteOneRTFrigo(input: $input) {
      id
    }
  }
`;
