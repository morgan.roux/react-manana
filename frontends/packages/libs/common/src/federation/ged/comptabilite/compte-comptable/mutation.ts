import gql from 'graphql-tag';
import { EF_COMPTE_COMPTABLE_INFO } from './fragment';

export const createEFCompteComptable = gql`
  mutation createEFCompteComptable($input: CreateOneEFCompteComptableInput!) {
    createOneEFCompteComptable(input: $input) {
      ...EFCompteComptableInfo
    }
  }

  ${EF_COMPTE_COMPTABLE_INFO}
`;

export const updateEFCompteComptable = gql`
  mutation updateEFCompteComptable($input: UpdateOneEFCompteComptableInput!) {
    updateOneEFCompteComptable(input: $input) {
      ...EFCompteComptableInfo
    }
  }

  ${EF_COMPTE_COMPTABLE_INFO}
`;

export const deleteEFCompteComptable = gql`
  mutation deleteEFCompteComptable($input: DeleteOneEFCompteComptableInput!) {
    deleteOneEFCompteComptable(input: $input) {
      id
    }
  }
`;
