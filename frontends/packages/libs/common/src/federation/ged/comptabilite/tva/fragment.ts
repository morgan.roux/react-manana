import gql from 'graphql-tag';

export const EF_TVA = gql`
  fragment EFTvaInfo on EFTva {
    id
    taux
    libelle
    annee
    dateDebutExercice
    dateFinExercice
    numeroCompte
  }
`;
