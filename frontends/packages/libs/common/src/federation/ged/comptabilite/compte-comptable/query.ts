import gql from 'graphql-tag';
import { EF_COMPTE_COMPTABLE_INFO } from './fragment';

export const EFCompteComptables = gql`
  query EFCompteComptables(
    $paging: OffsetPaging
    $filter: EFCompteComptableFilter
    $sorting: [EFCompteComptableSort!]
  ) {
    eFCompteComptables(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...EFCompteComptableInfo
      }
      totalCount
    }
  }

  ${EF_COMPTE_COMPTABLE_INFO}
`;
