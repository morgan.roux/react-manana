import gql from 'graphql-tag';
import { EF_MODELE_ECRITURE } from './fragment';

export const createEFModeleEcriture = gql`
  mutation createEFModeleEcriture($input: EFModeleEcritureInput!) {
    createOneEFModeleEcriture(input: $input) {
      ...EFModeleEcritureInfo
    }
  }

  ${EF_MODELE_ECRITURE}
`;

export const updateEFModeleEcriture = gql`
  mutation updateEFModeleEcriture($input: EFModeleEcritureInput!, $id: String!) {
    updateOneEFModeleEcriture(input: $input, id: $id) {
      ...EFModeleEcritureInfo
    }
  }

  ${EF_MODELE_ECRITURE}
`;

export const deleteEFModeleEcriture = gql`
  mutation deleteEFModeleEcriture($input: DeleteOneEFModeleEcritureInput!) {
    deleteOneEFModeleEcriture(input: $input) {
      id
    }
  }
`;

export const createEFModeleCompteComptable = gql`
  mutation createEFModeleCompteComptable($input: EFModeleCompteComptableInput!) {
    createOneEFModeleCompteComptable(input: $input) {
      ...EFModeleEcritureInfo
    }
  }

  ${EF_MODELE_ECRITURE}
`;

export const updateEFModeleCompteComptable = gql`
  mutation updateEFModeleCompteComptable($input: EFModeleCompteComptableInput!, , $id: String!) {
    updateOneEFModeleCompteComptable(input: $input, id: $id) {
      ...EFModeleEcritureInfo
    }
  }

  ${EF_MODELE_ECRITURE}
`;
