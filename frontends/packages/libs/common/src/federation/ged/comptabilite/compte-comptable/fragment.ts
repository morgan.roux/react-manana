import gql from 'graphql-tag';

export const EF_COMPTE_COMPTABLE_INFO = gql`
  fragment EFCompteComptableInfo on EFCompteComptable {
    id
    numero
    libelle
    sens
    suiviCompte
    isLettrage
    isExclureLettrage
    type
    idOrigine
    idOrigineAssocie
    idPharmacie
    idGroupement
    isActif
    tva {
      id
      taux
      libelle
      annee
      dateDebutExercice
      dateFinExercice
    }
  }
`;
