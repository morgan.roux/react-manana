import gql from 'graphql-tag';
import { EF_MODELE_ECRITURE, EF_MODELE_ECRITURE_LIGNE } from './fragment';

export const EFModeleEcritures = gql`
  query EFModeleEcritures($paging: OffsetPaging, $filter: EFModeleEcritureFilter, $sorting: [EFModeleEcritureSort!]) {
    eFModeleEcritures(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...EFModeleEcritureInfo
      }
      totalCount
    }
  }

  ${EF_MODELE_ECRITURE}
`;

export const EFModeleEcriture = gql`
  query EFModeleEcriture($id: ID!) {
    eFModeleEcriture(id: $id) {
      ...EFModeleEcritureInfo
    }
  }

  ${EF_MODELE_ECRITURE}
`;

export const EFModeleEcritureLignes = gql`
  query EFModeleEcritureLignes($paging: OffsetPaging, $filter: EFModeleEcritureLigneFilter, $sorting: [EFModeleEcritureLigneSort!]) {
    eFModeleEcritureLignes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...EFModeleEcritureLigneInfo
      }
      totalCount
    }
  }

  ${EF_MODELE_ECRITURE_LIGNE}
`;
