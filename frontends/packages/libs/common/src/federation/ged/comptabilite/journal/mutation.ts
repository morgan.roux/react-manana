import gql from 'graphql-tag';
import { EF_JOURNAL_INFO } from './fragment';

export const createEFJournal = gql`
  mutation createEFJournal($input: CreateOneEFJournalInput!) {
    createOneEFJournal(input: $input) {
      ...EFJournalInfo
    }
  }

  ${EF_JOURNAL_INFO}
`;

export const updateEFJournal = gql`
  mutation updateEFJournal($input: UpdateOneEFJournalInput!) {
    updateOneEFJournal(input: $input) {
      ...EFJournalInfo
    }
  }

  ${EF_JOURNAL_INFO}
`;

export const deleteEFJournal = gql`
  mutation deleteEFJournal($input: DeleteOneEFJournalInput!) {
    deleteOneEFJournal(input: $input) {
      id
    }
  }
`;

export const generateEFJournals = gql`
  mutation generateEFJournals {
    generateEFJournals
  }
`;
