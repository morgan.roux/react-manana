import gql from 'graphql-tag';
import { EF_TVA } from './fragment';

export const EFTva = gql`
  query EFTva($paging: OffsetPaging, $filter: EFTvaFilter, $sorting: [EFTvaSort!]) {
    eFTvas(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...EFTvaInfo
      }
      totalCount
    }
  }

  ${EF_TVA}
`;
