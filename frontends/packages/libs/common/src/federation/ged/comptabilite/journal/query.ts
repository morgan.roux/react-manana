import gql from 'graphql-tag';
import { EF_JOURNAL_INFO, EF_JOURNAL_TYPE_INFO } from './fragment';

export const EFJournal = gql`
  query EFJournal ($paging: OffsetPaging, $filter: EFJournalFilter, $sorting: [EFJournalSort!]) {
    eFJournals(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...EFJournalInfo
      }
      totalCount
    }
  }

  ${EF_JOURNAL_INFO}
`;

export const EFJournalType = gql`
  query EFJournalType ($paging: OffsetPaging, $filter: EFJournalTypeFilter, $sorting: [EFJournalTypeSort!]) {
    eFJournalTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...EFJournalTypeInfo
      }
      totalCount
    }
  }

  ${EF_JOURNAL_TYPE_INFO}
`;
