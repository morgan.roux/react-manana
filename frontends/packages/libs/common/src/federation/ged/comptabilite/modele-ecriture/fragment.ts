import gql from 'graphql-tag';
import { EF_COMPTE_COMPTABLE_INFO } from '../compte-comptable/fragment';
import {EF_TVA} from '../tva/fragment'

export const EF_MODELE_ECRITURE_LIGNE = gql`
  fragment EFModeleEcritureLigneInfo on EFModeleEcritureLigne {
    id
    idModeleEcriture
    isContrepartie
    libelle
    tva{
      ...EFTvaInfo
    }
    sens
    categorie
    compteComptableTva{
      ...EFCompteComptableInfo
    }
    compteComptable {
      ...EFCompteComptableInfo
    }
  }

  ${EF_COMPTE_COMPTABLE_INFO}
  ${EF_TVA}
`;

export const EF_MODELE_ECRITURE = gql`
  fragment EFModeleEcritureInfo on EFModeleEcriture {
    id
    titre
    description
    idJournalType
    isActif
    isControleTva
    idModeReglement
    idCompteFournisseur
    journalType {
      id
      code
      libelle
    }
    lignes {
      ...EFModeleEcritureLigneInfo
    }
  }

  ${EF_MODELE_ECRITURE_LIGNE}
`;
