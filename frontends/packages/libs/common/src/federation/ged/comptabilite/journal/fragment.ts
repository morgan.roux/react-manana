import gql from 'graphql-tag';

export const EF_JOURNAL_TYPE_INFO = gql`
  fragment EFJournalTypeInfo on EFJournalType {
    id
    code
    libelle
  }
`;

export const EF_JOURNAL_INFO = gql`
  fragment EFJournalInfo on EFJournal {
    id
    code
    type {
      id
      code
      libelle
    }
  }
`;
