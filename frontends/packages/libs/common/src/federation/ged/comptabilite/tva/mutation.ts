import gql from 'graphql-tag';
import { EF_TVA } from './fragment';

export const createEFTva = gql`
  mutation createEFTva($input: EFTvaInput!) {
    createOneEFTva(input: $input) {
      ...EFTvaInfo
    }
  }

  ${EF_TVA}
`;

export const updateEFTva = gql`
  mutation updateEFTva($input: UpdateOneEFTvaInput!) {
    updateOneEFTva(input: $input) {
      ...EFTvaInfo
    }
  }

  ${EF_TVA}
`;

export const deleteEFTva = gql`
  mutation deleteEFTva($input: DeleteOneEFTvaInput!) {
    deleteOneEFTva(input: $input) {
      id
    }
  }
`;

export const generateEFTvas = gql`
  mutation generateEFTvas {
    generateEFTvas
  }
`;
