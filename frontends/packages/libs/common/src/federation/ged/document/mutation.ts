import gql from 'graphql-tag';
import { FULL_DOCUMENT_CATEGORIE_INFO } from './fragment';

export const CREATE_DOCUMENT_CATEGORIE = gql`
  mutation CREATE_DOCUMENT_CATEGORIE($input: GedDocumentInput!) {
    createOneGedDocument(input: $input) {
      ...FullDocumentCategorie
    }
  }

  ${FULL_DOCUMENT_CATEGORIE_INFO}
`;

export const UPDATE_DOCUMENT_CATEGORIE = gql`
  mutation UPDATE_DOCUMENT_CATEGORIE($input: GedDocumentInput!, $id: String!) {
    updateOneGedDocument(input: $input, id: $id) {
      ...FullDocumentCategorie
    }
  }

  ${FULL_DOCUMENT_CATEGORIE_INFO}
`;

export const DELETE_DOCUMENT_CATEGORIE = gql`
  mutation DELETE_DOCUMENT_CATEGORIE($input: DeleteOneGedDocumentInput!) {
    deleteOneGedDocument(input: $input) {
      id
    }
  }
`;

export const UPDATE_DOCUMENT_STATUS = gql`
  mutation UPDATE_DOCUMENT_STATUS($input: GedDocumentChangementStatutInput!) {
    updateStatutGedDocument(input: $input) {
      ...FullDocumentCategorie
    }
  }

  ${FULL_DOCUMENT_CATEGORIE_INFO}
`;

export const TOGGLE_DOCUMENT_TO_FAVORITE = gql`
  mutation TOGGLE_DOCUMENT_TO_FAVORITE($idDocument: String!) {
    toggleToGedDocumentFavoris(idDocument: $idDocument) {
      ...FullDocumentCategorie
    }
  }

  ${FULL_DOCUMENT_CATEGORIE_INFO}
`;

export const restoreGEDDocument = gql`
  mutation restoreGEDDocument($idDocument: String!) {
    restoreGEDDocument(idDocument: $idDocument)
  }
`;

export const reviserGEDDocument = gql`
  mutation reviserGEDDocument($idDocument: String!) {
    reviserGEDDocument(idDocument: $idDocument)
  }
`;


export const exportGEDDocument = gql`
  mutation exportGEDDocument($idDocument: String!) {
    exportGEDDocument(idDocument: $idDocument) {
      ...FullDocumentCategorie
    }
  }

  ${FULL_DOCUMENT_CATEGORIE_INFO}
`;