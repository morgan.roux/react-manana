import gql from 'graphql-tag';
import { MINIMAL_USER_INFO } from '../../iam/user/fragment'

export const MINIMAL_CATEGORIE_INFO = gql`
  fragment MinimalCategorieInfo on GedCategorie {
    id
    libelle
    base
    idGroupement
    idPharmacie
    createdAt
    updatedAt
    nombreSousCategories
    type
  }
`;

export const FULL_CATEGORIE_INFO = gql`
  fragment FullCategorieInfo on GedCategorie {
    id
    libelle
    base
    idGroupement
    idPharmacie
    createdAt
    updatedAt
    nombreSousCategories
    type
    validation
    workflowValidation
    participants{
      ...MinimalUserInfo
    }
    partenaireValidateur {
        id
        nom
      }
    sousCategories {
      id
      libelle
      validation
      workflowValidation
      idPartenaireValidateur
      nombreDocuments
      partenaireValidateur {
        id
        nom
      }
      participants{
        ...MinimalUserInfo
      }
    }
  }
  ${MINIMAL_USER_INFO}
`;

export const SOUS_CATEGORIE_INFO = gql`
  fragment SousCategorieInfo on GedSousCategorie {
    id
  }
`;

export const FULL_MY_CATEGORIE_INFO = gql`
  fragment FullMyCategorieInfo on GedCategorie {
    id
    libelle
    base
    idGroupement
    idPharmacie
    createdAt
    updatedAt
    type
    mesSousCategories {
      id
      libelle
      workflowValidation
      idPartenaireValidateur
      nombreDocuments
      partenaireValidateur {
        id
        nom
      }
      participants {
        id
        fullName
      }
    }
  }
`;
