import gql from 'graphql-tag';
import { SEND_EMAIL_RESULT_INFO } from './fragment';

export const SEND_EMAIL = gql`
  mutation SEND_EMAIL($input:  EmailInput) {
    sendEmail(input: $input) {
      ...SendEmailResultInfo
    }
  }

  ${SEND_EMAIL_RESULT_INFO}
`;

export const SEND_GED_DOCUMENT_BY_EMAIL = gql`
  mutation SEND_GED_DOCUMENT_BY_EMAIL($input: GedDocumentEmailInput) {
    sendGEDDocumentByEmail(input: $input) {
      ...SendEmailResultInfo
    }
  }
  ${SEND_EMAIL_RESULT_INFO}
`;
