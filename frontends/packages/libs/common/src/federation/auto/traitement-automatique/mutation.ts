import gql from 'graphql-tag';

import { FULL_TRAITEMENT_AUTO_INFO } from './fragment';

export const CREATE_TRAITEMENT_AUTOMATIQUE = gql`
  mutation CREATE_TRAITEMENT_AUTOMATIQUE($input: TATraitementInput!) {
    createOneTATraitement(input: $input) {
      ...FullTraitementInfo
    }
  }
  ${FULL_TRAITEMENT_AUTO_INFO}
`;

export const CREATE_MANY_TRAITEMENTS_AUTOMATIQUES = gql`
  mutation CREATE_MANY_TRAITEMENTS_AUTOMATIQUES($input: [TATraitementInput!]!) {
    createManyTATraitements(input: $input) {
      ...FullTraitementInfo
    }
  }
  ${FULL_TRAITEMENT_AUTO_INFO}
`;

export const UPDATE_TRAITEMENT_AUTOMATIQUE = gql`
  mutation UPDATE_TRAITEMENT_AUTOMATIQUE($input: TATraitementInput!, $id: String!) {
    updateOneTATraitement(input: $input, id: $id) {
      ...FullTraitementInfo
    }
  }
  ${FULL_TRAITEMENT_AUTO_INFO}
`;

export const DELETE_TRAITEMENT_AUTOMATIQUE = gql`
  mutation DELETE_TRAITEMENT_AUTOMATIQUE($id: String!) {
    deleteOneTATraitement(id: $id) {
      id
    }
  }
`;

export const DELETE_MANY_TRAITEMENTS_AUTOMATIQUES = gql`
  mutation DELETE_MANY_TRAITEMENTS_AUTOMATIQUES($ids: [String!]!) {
    deleteManyTATraitements(ids: $ids)
  }
`;

export const CHANGER_STATUT_TRAITEMENT_EXECUTION = gql`
  mutation CHANGER_STATUT_TRAITEMENT_EXECUTION($input: TATraitementExecutionChangementStatutInput!) {
    changerStatutTATraitementExecution(input: $input) {
      ...FullTraitementInfo
    }
  }
  ${FULL_TRAITEMENT_AUTO_INFO}
`;
