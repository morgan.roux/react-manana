import gql from 'graphql-tag';
import { TRAITEMENT_EXECUTION_AUTO_INFO } from './fragment';

export const GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS = gql`
  query GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS(
    $paging: OffsetPaging
    $filter: TATraitementExecutionFilter
    $sorting: [TATraitementExecutionSort!]
  ) {
    tATraitementExecutions(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...TraitementExecutionInfo
      }
    }
  }

  ${TRAITEMENT_EXECUTION_AUTO_INFO}
`;



export const GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE = gql`
  query GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE(
    $filter: TATraitementExecutionAggregateFilter
  ) {
    tATraitementExecutionAggregate(filter: $filter) {
      count{
        id
      }
    }
  }

`;

