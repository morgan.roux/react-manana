import gql from 'graphql-tag';

export const CONDITION_INFO = gql`
  fragment PRTConditionInfo on PRTConditionCommerciale {
    id
    idPartenaireTypeAssocie
    partenaireType
    idCanal
    idType
    idStatut
    titre
    dateDebut
    dateFin
    idGroupement
    idPharmacie
    description
    typeAssocie {
      ... on Laboratoire {
        id
        nom
        photo {
          id
          chemin
          nomOriginal
          type
          publicUrl
        }
        idLaboSuite
      }
      ... on PrestataireService {
        id
        nom
        photo {
          id
          chemin
          nomOriginal
          type
          publicUrl
        }
        idPrestataireServiceSuite
      }
    }
    type {
      id
      libelle
      code
    }
    statut {
      id
      libelle
      code
    }
    canal {
      id
      libelle
      code
    }
    fichiers {
      id
      chemin
      nomOriginal
      type
      publicUrl
    }
  }
`;
