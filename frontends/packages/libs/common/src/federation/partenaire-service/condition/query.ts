import gql from 'graphql-tag';
import { CONDITION_INFO } from './fragment';

export const GET_CONDITIONS = gql`
  query GET_CONDITIONS(
    $paging: OffsetPaging
    $filter: PRTConditionCommercialeFilter
    $sorting: [PRTConditionCommercialeSort!]
  ) {
    pRTConditionCommerciales(paging: $paging, filter: $filter, sorting: $sorting) {
      totalCount
      nodes {
        ...PRTConditionInfo
      }
    }
  }

  ${CONDITION_INFO}
`;

export const GET_CONDITION_COMMERCIALE = gql`
  query GET_CONDITION_COMMERCIALE($id: ID!) {
    pRTConditionCommerciale(id: $id) {
      ...PRTConditionInfo
    }
  }

  ${CONDITION_INFO}
`;
