import gql from 'graphql-tag';
import { TYPE_CONDITION_INFO } from './fragment';

export const GET_TYPES_CONDITION = gql`
  query GET_TYPES_CONDITION(
    $paging: OffsetPaging
    $filter: PRTConditionCommercialeTypeFilter
    $sorting: [PRTConditionCommercialeTypeSort!]
  ) {
    pRTConditionCommercialeTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...ConditionTypeInfo
      }
    }
  }

  ${TYPE_CONDITION_INFO}
`;
