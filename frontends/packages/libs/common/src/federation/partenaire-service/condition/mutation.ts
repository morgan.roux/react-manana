import gql from 'graphql-tag';
import { CONDITION_INFO } from './fragment';

export const CREATE_CONDITION = gql`
  mutation CREATE_CONDITION($input: PRTConditionCommercialeInput!) {
    createOnePRTConditionCommerciale(input: $input) {
      ...PRTConditionInfo
    }
  }
  ${CONDITION_INFO}
`;

export const UPDATE_CONDITION = gql`
  mutation UPDATE_CONDITION($input: PRTConditionCommercialeInput!, $id: String!) {
    updateOnePRTConditionCommerciale(input: $input, id: $id) {
      ...PRTConditionInfo
    }
  }
  ${CONDITION_INFO}
`;

export const DELETE_CONDITION = gql`
  mutation DELETE_CONDITION($id: String!) {
    deleteOnePRTConditionCommerciale(id: $id)
  }
`;

export const CHANGE_STATUS_CONDITION = gql`
  mutation CHANGE_STATUS_CONDITION($idStatut: String!, $id: String!) {
    changeStatutPRTConditionCommerciale(idStatut: $idStatut, id: $id) {
      ...PRTConditionInfo
    }
  }
  ${CONDITION_INFO}
`;
