import gql from 'graphql-tag';

export const STATUS_CONDITION_INFO = gql`
  fragment StatusConditionInfo on PRTConditionCommercialeStatut {
    id
    libelle
    code
  }
`;
