import gql from 'graphql-tag';
import { FULL_SUIVI_OPERATIONNEL_INFO, SUIVI_OPERATIONNEL_TYPE_INFO, SUIVI_OPERATIONNEL_STATUT_INFO } from './fragment';

export const GET_SUIVI_OPERATIONNELS = gql`
  query GET_SUIVI_OPERATIONNELS(
    $paging: OffsetPaging
    $filter: PRTSuiviOperationnelFilter
    $sorting: [PRTSuiviOperationnelSort!]
  ) {
    pRTSuiviOperationnels(paging: $paging, filter: $filter, sorting: $sorting) {
      totalCount
      nodes {
        ...PRTSuiviOperationnelInfo
      }
    }
  }

  ${FULL_SUIVI_OPERATIONNEL_INFO}
`;

export const GET_SUIVI_OPERATIONNEL = gql`
  query GET_SUIVI_OPERATIONNEL($id: ID!) {
    pRTSuiviOperationnel(id: $id) {
      ...PRTSuiviOperationnelInfo
    }
  }

  ${FULL_SUIVI_OPERATIONNEL_INFO}
`;

export const GET_LIST_SUIVI_OPERATIONNEL_TYPES = gql`
  query GET_LIST_SUIVI_OPERATIONNEL_TYPES(
    $paging: OffsetPaging
    $filter: PRTSuiviOperationnelTypeFilter
    $sorting: [PRTSuiviOperationnelTypeSort!]
  ) {
    pRTSuiviOperationnelTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTSuiviOperationnelTypeInfo
      }
    }
  }
  ${SUIVI_OPERATIONNEL_TYPE_INFO}
`;

export const GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU = gql`
  query GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU(
    $idCompteRendu: String
    $searchText: String
    $take: Int
    $skip: Int
    $idTypeAssocie: String!
    $categorie: String
  ) {
    getSuiviOperationnelForCompteRendu(
      idCompteRendu: $idCompteRendu
      searchText: $searchText
      take: $take
      skip: $skip
      categorie: $categorie
      idTypeAssocie: $idTypeAssocie
    ) {
      ...PRTSuiviOperationnelInfo
    }
  }
  ${FULL_SUIVI_OPERATIONNEL_INFO}
`;

export const GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWS = gql`
  query GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_TOTAL_ROWS(
    $idCompteRendu: String
    $searchText: String
    $idTypeAssocie: String!
  ) {
    getSuiviOperationnelForCompteRenduRowsTotal(
      idCompteRendu: $idCompteRendu
      searchText: $searchText
      idTypeAssocie: $idTypeAssocie
    )
  }
`;

export const GET_LIST_SUIVI_OPERATIONNEL_STATUT = gql`
  query GET_LIST_SUIVI_OPERATIONNEL_STATUT(
    $paging: OffsetPaging
    $filter: PRTSuiviOperationnelStatutFilter
    $sorting: [PRTSuiviOperationnelStatutSort!]
  ) {
    pRTSuiviOperationnelStatuts(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTSuiviOperationnelStatutInfo
      }
    }
  }
  ${SUIVI_OPERATIONNEL_STATUT_INFO}
`;

export const GET_COMPUTE_PRT_ACTION_OPERATIONNELS = gql`
  query GET_COMPUTE_PRT_ACTION_OPERATIONNELS($input: ComputeActionOperationnelsInput!) {
    computeActionOperationnels(input: $input) {
      total
      currentYear {
        enCours
        cloture
        annee
      }
      lastYear {
        enCours
        cloture
        annee
      }
    }
  }
`;

export const GET_COMPUTE_PRT_ACTION_OPERATIONNElS_BY_TYPE = gql`
  query GET_COMPUTE_PRT_ACTION_OPERATIONNElS_BY_TYPE($input: ComputeActionOperationnelsInput!) {
    computeActionOperationnelsByType(input: $input) {
      type {
        id
        code
        libelle
        couleur
      }
      count
      total
    }
  }
`;

export const SEND_EMAIL_ACTION_LITIGE = gql`
  query SEND_EMAIL_ACTION_LITIGE($input: PRTSendEmailLitigeInput!) {
    sendEmailActionLitige(input: $input) {
      statut
      message
    }
  }
`;
