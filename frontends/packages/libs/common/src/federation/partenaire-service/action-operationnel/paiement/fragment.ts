import { gql } from '@apollo/client';

export const FULL_SUIVI_OPERATIONNEL_PAIEMENT_INFO = gql`
  fragment PRTSuiviOperationnelPaiementInfo on PRTSuiviOperationnelPaiement {
    id
    date
    montant
    idSuiviOperationnel
    idModeReglement
    modePaiement {
      id
      code
      libelle
    }
  }
`;
