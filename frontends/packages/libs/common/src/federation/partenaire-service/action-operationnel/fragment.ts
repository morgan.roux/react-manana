import gql from 'graphql-tag';
import { FULL_USER_INFO } from 'src/federation/iam/user/fragment';
import { PRT_CONTACT_FULL_INFO } from '../contact/fragment';
import { IMPORTANCE } from './../../tools/itemScoring/fragment';
import { FULL_SUIVI_OPERATIONNEL_PAIEMENT_INFO } from './paiement/fragment';

export const FULL_SUIVI_OPERATIONNEL_INFO = gql`
  fragment PRTSuiviOperationnelInfo on PRTSuiviOperationnel {
    id
    description
    dateHeure
    createdBy
    partenaireType
    idTypeAssocie
    idGroupement
    montant
    idImportance
    idTache
    idFonction
    restePaiement
    paiement {
      id
      montant
      modePaiement {
        id
        code
        libelle
      }
      date
    }
    typeAssocie {
      ... on Laboratoire {
        id
        nom
        photo {
          id
          chemin
          nomOriginal
          type
          publicUrl
        }
        idLaboSuite
      }
      ... on PrestataireService {
        id
        nom
        photo {
          id
          chemin
          nomOriginal
          type
          publicUrl
        }
        idPrestataireServiceSuite
      }
    }
    contacts {
      ...PRTContactFullInfo
    }
    importance {
      ...ItemScoringImportance
    }
    idPharmacie
    participants {
      ...FullUserInfo
    }
    type {
      id
      libelle
      code
      categorie
    }
    statut {
      id
      libelle
      code
    }
    fichiers {
      id
      chemin
      nomOriginal
      type
      publicUrl
    }
    nombreCommentaires
    nombreReaction
    updatedAt
    createdAt
  }
  ${FULL_USER_INFO}
  ${IMPORTANCE}
  ${PRT_CONTACT_FULL_INFO}
  ${FULL_SUIVI_OPERATIONNEL_PAIEMENT_INFO}
`;

export const SUIVI_OPERATIONNEL_TYPE_INFO = gql`
  fragment PRTSuiviOperationnelTypeInfo on PRTSuiviOperationnelType {
    id
    code
    libelle
    categorie
  }
`;

export const SUIVI_OPERATIONNEL_STATUT_INFO = gql`
  fragment PRTSuiviOperationnelStatutInfo on PRTSuiviOperationnelStatut {
    id
    code
    libelle
  }
`;
