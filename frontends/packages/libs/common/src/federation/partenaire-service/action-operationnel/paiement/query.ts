import { gql } from '@apollo/client';
import { FULL_SUIVI_OPERATIONNEL_PAIEMENT_INFO } from './fragment';

export const GET_PRT_SUIVI_OPERATIONNEL_PAIEMENTS = gql`
  query GET_PRT_SUIVI_OPERATIONNEL_PAIEMENTS(
    $paging: OffsetPaging
    $sorting: [PRTSuiviOperationnelPaiementSort!]
    $filter: PRTSuiviOperationnelPaiementFilter
  ) {
    pRTSuiviOperationnelPaiements(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTSuiviOperationnelPaiementInfo
      }
      totalCount
    }
  }
  ${FULL_SUIVI_OPERATIONNEL_PAIEMENT_INFO}
`;

export const GET_PRT_SUIVI_OPERATIONNEL_PAIEMENT = gql`
  query GET_PRT_SUIVI_OPERATIONNEL_PAIEMENT($id: ID!) {
    pRTSuiviOperationnelPaiement(id: $id) {
      ...PRTSuiviOperationnelPaiementInfo
    }
  }
  ${FULL_SUIVI_OPERATIONNEL_PAIEMENT_INFO}
`;
