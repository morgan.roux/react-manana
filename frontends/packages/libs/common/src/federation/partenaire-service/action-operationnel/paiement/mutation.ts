import { gql } from '@apollo/client';
import { FULL_SUIVI_OPERATIONNEL_PAIEMENT_INFO } from './fragment';

export const CREATE_PRT_SUIVI_OPERATIONNEL_PAIEMENT = gql`
  mutation CREATE_PRT_SUIVI_OPERATIONNEL_PAIEMENT($input: PRTSuiviOperationnelPaiementInput!) {
    createOnePrtSuiviOperationnelPaiement(input: $input) {
      ...PRTSuiviOperationnelPaiementInfo
    }
  }
  ${FULL_SUIVI_OPERATIONNEL_PAIEMENT_INFO}
`;

export const UPDATE_PRT_SUIVI_OPERATIONNEL_PAIEMENT = gql`
  mutation UPDATE_PRT_SUIVI_OPERATIONNEL_PAIEMENT($id: String!, $input: PRTSuiviOperationnelPaiementInput!) {
    updateOnePrtSuiviOperationnelPaiement(id: $id, input: $input) {
      ...PRTSuiviOperationnelPaiementInfo
    }
  }
  ${FULL_SUIVI_OPERATIONNEL_PAIEMENT_INFO}
`;

export const DELETE_PRT_SUIVI_OPERATIONNEL_PAIEMENT = gql`
  mutation DELETE_PRT_SUIVI_OPERATIONNEL_PAIEMENT($id: String!) {
    deleteOnePrtSuiviOperationnelPaiement(id: $id) {
      id
    }
  }
`;
