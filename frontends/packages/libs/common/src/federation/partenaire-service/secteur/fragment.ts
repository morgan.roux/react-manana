import gql from 'graphql-tag';
export const SECTEUR_INFO = gql`
  fragment SecteurInfo on Secteur {
    id
    code
    libelle
  }
`;
