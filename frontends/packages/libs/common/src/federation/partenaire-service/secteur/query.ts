import gql from 'graphql-tag';
import { SECTEUR_INFO } from './fragment';

export const GET_SECTEURS = gql`
  query GET_SECTEURS($paging: OffsetPaging, $filter: SecteurFilter, $sorting: [SecteurSort!]) {
    secteurs(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...SecteurInfo
      }
    }
    ${SECTEUR_INFO}
  }
`;
