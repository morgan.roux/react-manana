import gql from 'graphql-tag';
import { FULL_COMPTE_RENDU_INFO } from './fragment';

export const CREATE_PRT_COMPTE_RENDU = gql`
  mutation CREATE_PRT_COMPTE_RENDU($idPharmacie: String, $input: PRTCompteRenduInput!) {
    createOnePRTCompteRendu(input: $input) {
      ...PRTCompteRenduInfo
    }
  }
  ${FULL_COMPTE_RENDU_INFO}
`;

export const UPDATE_PRT_COMPTE_RENDU = gql`
  mutation UPDATE_PRT_COMPTE_RENDU(
    $idPharmacie: String
    $id: String!
    $input: PRTCompteRenduInput!
  ) {
    updateOnePRTCompteRendu(id: $id, input: $input) {
      ...PRTCompteRenduInfo
    }
  }
  ${FULL_COMPTE_RENDU_INFO}
`;

export const DELETE_ONE_PRT_COMPTE_RENDU = gql`
  mutation DELETE_ONE_PRT_COMPTE_RENDU($id: String!) {
    deleteOnePRTCompteRendu(id: $id) {
      id
    }
  }
`;

export const UPDATE_ONE_PRT_COMPTE_RENDU_NOTIFICATION_lOGS = gql`
  mutation UPDATE_PRT_COMPTE_RENDU_NOTIFICATION_LOGS(
    $idPharmacie: String
    $input: PRTCompteRenduNotificationInput!
  ) {
    updateOnePRTCompteRenduNotificationLogs(input: $input) {
      ...PRTCompteRenduInfo
    }
  }
  ${FULL_COMPTE_RENDU_INFO}
`;
