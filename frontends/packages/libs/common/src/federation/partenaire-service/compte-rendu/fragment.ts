import gql from 'graphql-tag';
import { PRT_CONTACT_FULL_INFO } from '../contact/fragment';
import { FULL_USER_INFO } from './../../iam/user/fragment';
import { FULL_SUIVI_OPERATIONNEL_INFO } from '../action-operationnel/fragment';
import { FULL_PLAN_MARKETING_INFO } from '../planMarketing/fragment';
import { NOTIFICATION_LOGS_INFO } from './../../notification/notification-logs/fragment';
import { CONDITION_INFO } from '../condition/fragment';

export const FULL_COMPTE_RENDU_INFO = gql`
  fragment PRTCompteRenduInfo on PRTCompteRendu {
    id
    titre
    remiseEchantillon
    gestionPerime
    rapportVisite
    conclusion
    idPartenaireTypeAssocie
    partenaireType
    heureDebut
    heureFin
    dateRendezVous
    typeReunion
    partenaireTypeAssocie {
      ... on Laboratoire {
        id
        nom
        photo {
          id
          chemin
          nomOriginal
          type
          publicUrl
        }
        idLaboSuite
      }
      ... on PrestataireService {
        id
        nom
        photo {
          id
          chemin
          nomOriginal
          type
          publicUrl
        }
        idPrestataireServiceSuite
      }
    }
    createdAt
    idNotificationLogs
    idPharmacie
    collaborateurs {
      ...FullUserInfo
    }
    responsables {
      ...PRTContactFullInfo
    }
    suiviOperationnels {
      ...PRTSuiviOperationnelInfo
    }
    planMarketings {
      ...PRTPlanMarketingInfo
    }
    conditionsCommerciales {
      ...PRTConditionInfo
    }
    notificationLogs {
      ...NotificationLogsInfo
    }
  }

  ${PRT_CONTACT_FULL_INFO}
  ${FULL_USER_INFO}
  ${FULL_SUIVI_OPERATIONNEL_INFO}
  ${FULL_PLAN_MARKETING_INFO}
  ${NOTIFICATION_LOGS_INFO}
  ${CONDITION_INFO}
`;
