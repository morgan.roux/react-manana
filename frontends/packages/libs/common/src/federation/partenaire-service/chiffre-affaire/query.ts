import { gql } from '@apollo/client';
import { PRT_CHIFFRE_AFFAIRE_INFO } from './fragment';

export const COMPUTE_PRT_CHIFFRE_AFFAIRE = gql`
  query COMPUTE_PRT_CHIFFRE_AFFAIRE($input: PRTComputeChiffreAffaireInput!) {
    computeChiffreAffaire(input: $input) {
      currentYear {
        montant
        annee
      }
      previousYear {
        montant
        annee
      }
      pourcentage
    }
  }
`;

export const COMPUTE_PRT_CHIFFRE_AFFAIRES = gql`
  query COMPUTE_PRT_CHIFFRE_AFFAIRES(
    $paging: OffsetPaging
    $filter: PRTChiffreAffaireFilter
    $sorting: [PRTChiffreAffaireSort!]
  ) {
    pRTChiffreAffaires(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTChiffreAffaireInfo
      }
      totalCount
    }
  }
  ${PRT_CHIFFRE_AFFAIRE_INFO}
`;
