import { gql } from '@apollo/client';

export const SET_PRT_CHIFFRE_AFFAIRE = gql`
  mutation SET_PRT_CHIFFRE_AFFAIRE($input: PRTChiffreAffaireInput!) {
    setPRTChiffreAffaire(input: $input) {
      id
      montant
      annee
      idPharmacie
    }
  }
`;
