import { gql } from '@apollo/client';

export const PRT_CHIFFRE_AFFAIRE_INFO = gql`
  fragment PRTChiffreAffaireInfo on PRTChiffreAffaire {
    id
    idPharmacie
    idPartenaireTypeAssocie
    partenaireType
    montant
    annee
  }
`;
