import gql from 'graphql-tag';
export const CONTACT_CREDENTIAL_INFO = gql`
  fragment ContactCredentialInfo on PRTContactCredential {
    id
    idPartenaireTypeAssocie
    partenaireType
    nomService
    dataType
    url
    idSecteur
    commentaire
    idPharmacie
    idGroupement
    createdAt
    updatedAt
  }
`;
