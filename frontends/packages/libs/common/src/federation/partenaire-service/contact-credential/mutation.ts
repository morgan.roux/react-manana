import gql from 'graphql-tag';
import { CONTACT_CREDENTIAL_INFO } from './fragment';

export const CREATE_ONE_CONTACT_CREDENTIAL = gql`
  mutation CREATE_ONE_CONTACT_CREDENTIAL($input: CreateOnePRTContactCredentialInput!) {
    createOnePRTContactCredential(input: $input) {
      ...ContactCredentialInfo
    }
  }
  ${CONTACT_CREDENTIAL_INFO}
`;

export const UPDATE_ONE_CONTACT_CREDENTIAL = gql`
  mutation UPDATE_ONE_CONTACT_CREDENTIAL($input: UpdateOnePRTContactCredentialInput!) {
    updateOnePRTContactCredential(input: $input) {
      ...ContactCredentialInfo
    }
  }
  ${CONTACT_CREDENTIAL_INFO}
`;

export const DELETE_ONE_CONTACT_CREDENTIAL = gql`
  mutation DELETE_ONE_CONTACT_CREDENTIAL($input: DeleteOnePRTContactCredentialInput!) {
    deleteOnePRTContactCredential(input: $input) {
      id
    }
  }
`;
