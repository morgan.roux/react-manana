import gql from 'graphql-tag';
import { REMUNERATION_INFO, REMUNERATION_REGLEMENT_INFO } from './fragment';

export const CREATE_PRT_REMUNERATION = gql`
    mutation CREATE_PRT_REMUNERATION($input: PRTRemunerationInput!) {
        createOnePRTRemuneration(input: $input) {
            ...PRTRemunerationInfo
        }
    }
  ${REMUNERATION_INFO}
`;

export const UPDATE_PRT_REMUNERATION = gql`
    mutation UPDATE_PRT_REMUNERATION($id: String!, $input: PRTRemunerationInput!) {
        updateOnePRTRemuneration(id: $id, input: $input) {
            ...PRTRemunerationInfo
        }
    }
  ${REMUNERATION_INFO}
`;

export const DELETE_PRT_REMUNERATION = gql`
  mutation DELETE_PRT_REMUNERATION($id: String!) {
    deleteOnePRTRemuneration(id: $id) {
      id
    }
  }
`;

export const CREATE_PRT_REMUNERATION_REGLEMENT = gql`
  mutation CREATE_PRT_REMUNERATION_REGLEMENT($input: PRTRemunerationReglementInput!) {
    createOnePRTRemunerationReglement(input: $input) {
      ...PRTRemunerationReglementInfo
    }
  }
  ${REMUNERATION_REGLEMENT_INFO}
`;

export const UPDATE_PRT_REMUNERATION_REGLEMENT = gql`
  mutation UPDATE_PRT_REMUNERATION_REGLEMENT($id: String!, $input: PRTRemunerationReglementInput!) {
    updateOnePRTRemunerationReglement(id: $id, input: $input) {
        ...PRTRemunerationReglementInfo
    }
  }
  ${REMUNERATION_REGLEMENT_INFO}
`;

export const DELETE_PRT_REMUNERATION_REGLEMENT = gql`
  mutation DELETE_PRT_REMUNERATION_REGLEMENT($id: String!) {
    deleteOnePRTRemunerationReglement(id: $id) {
      id
    }
  }
`;