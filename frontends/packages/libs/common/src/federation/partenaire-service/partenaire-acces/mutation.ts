import gql from 'graphql-tag';
import { PARTENAIRE_ACCES_INFO } from './fragment';

export const CREATE_ONE_PARTENAIRE_ACCES = gql`
  mutation CREATE_ONE_PARTENAIRE_ACCES($input: CreateOnePartenaireAccesInput!) {
    createOnePartenaireAcces(input: $input) {
      ...PartenaireAccesInfo
    }
  }
  ${PARTENAIRE_ACCES_INFO}
`;

export const UPDATE_ONE_PARTENAIRE_ACCES = gql`
  mutation UPDATE_ONE_PARTENAIRE_ACCES($input: UpdateOnePartenaireAccesInput!) {
    updateOnePartenaireAcces(input: $input) {
      ...PartenaireAccesInfo
    }
  }
  ${PARTENAIRE_ACCES_INFO}
`;

export const DELETE_ONE_PARTENAIRE_ACCES = gql`
  mutation DELETE_ONE_PARTENAIRE_ACCES($input: DeleteOnePartenaireAccesInput!) {
    deleteOnePartenaireAcces(input: $input) {
      id
    }
  }
`;
