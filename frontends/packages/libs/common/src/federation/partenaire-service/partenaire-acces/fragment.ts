import gql from 'graphql-tag';
export const PARTENAIRE_ACCES_INFO = gql`
  fragment PartenaireAccesInfo on PartenaireAcces {
    id
    idPartenaireTypeAssocie
    partenaireType
    url
    commentaire
    idPharmacie
    idGroupement
    createdAt
    updatedAt
  }
`;
