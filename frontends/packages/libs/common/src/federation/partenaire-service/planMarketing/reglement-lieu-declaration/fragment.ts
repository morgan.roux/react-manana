import { gql } from '@apollo/client';

export const PRT_REGLEMENT_LIEU_DECLARATION_INFO = gql`
  fragment PrtReglementLieuDeclarationInfo on PRTReglementLieuDeclaration {
    id
    code
    libelle
  }
`;
