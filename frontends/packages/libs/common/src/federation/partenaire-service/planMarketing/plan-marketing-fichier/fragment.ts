import { gql } from '@apollo/client';

export const PRT_PLAN_MARKETING_FICHIER_FULL_INFO = gql`
  fragment PrtPlanMarketingFichierInfo on PRTPlanMarketingFichier {
    id
    idFichier
    fichierType
    idPlanMarketing
    createdAt
    updatedAt
  }
`;
