import { gql } from '@apollo/client';
import { PRT_REGLEMENT_MODE_DECLARATION_INFO } from './fragment';

export const GET_PRT_REGLEMENT_MODE_DECLARATIONS = gql`
  query GET_PRT_REGLEMENT_MODE_DECLARATIONS(
    $paging: OffsetPaging
    $filter: PRTReglementModeDeclarationFilter
    $sorting: [PRTReglementModeDeclarationSort!]
  ) {
    pRTReglementModeDeclarations(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTReglementModeDeclarationInfo
      }
    }
  }
  ${PRT_REGLEMENT_MODE_DECLARATION_INFO}
`;
