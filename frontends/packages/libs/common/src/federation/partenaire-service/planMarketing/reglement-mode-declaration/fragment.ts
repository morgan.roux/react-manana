import { gql } from '@apollo/client';

export const PRT_REGLEMENT_MODE_DECLARATION_INFO = gql`
  fragment PRTReglementModeDeclarationInfo on PRTReglementModeDeclaration {
    id
    code
    libelle
  }
`;
