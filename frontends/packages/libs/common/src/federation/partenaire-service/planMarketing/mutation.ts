import gql from 'graphql-tag';
import { FULL_PLAN_MARKETING_INFO, PLAN_MARKETING_TYPE_INFO } from './fragment';

export const CREATE_PLAN_MARKETING = gql`
  mutation CREATE_PLAN_MARKETING($idPharmacie: String, $input: PRTPlanMarketingInput!) {
    createOnePRTPlanMarketing(input: $input) {
      ...PRTPlanMarketingInfo
    }
  }
  ${FULL_PLAN_MARKETING_INFO}
`;

export const UPDATE_PLAN_MARKETING = gql`
  mutation UPDATE_PLAN_MARKETING(
    $idPharmacie: String
    $id: String!
    $input: PRTPlanMarketingInput!
  ) {
    updateOnePRTPlanMarketing(id: $id, input: $input) {
      ...PRTPlanMarketingInfo
    }
  }
  ${FULL_PLAN_MARKETING_INFO}
`;

export const DELETE_ONE_PLAN_MARKETING = gql`
  mutation DELETE_ONE_PLAN_MARKETING($input: PRTPlanMarketingDeleteOneInput!) {
    deleteOnePRTPlanMarketing(input: $input) {
      id
    }
  }
`;

export const CHANGE_PLAN_MARKETING_STATUS = gql`
  mutation CHANGE_PLAN_MARKETING_STATUS(
    $idPharmacie: String
    $id: String!
    $input: PRTPlanMarketingChangeStatutInput!
  ) {
    changePRTPlanMarketingStatut(id: $id, input: $input) {
      ...PRTPlanMarketingInfo
    }
  }
  ${FULL_PLAN_MARKETING_INFO}
`;

export const CREATE_PLAN_MARKETING_TYPE = gql`
  mutation CREATE_PLAN_MARKETING_TYPE(
    $idPharmacie: String
    $input: CreateOnePRTPlanMarketingTypeInput!
  ) {
    createOnePRTPlanMarketingType(input: $input) {
      ...PRTPlanMarketingTypeInfo
    }
  }
  ${PLAN_MARKETING_TYPE_INFO}
`;

export const DELETE_ONE_PLAN_MARKETING_TYPE = gql`
  mutation DELETE_ONE_PLAN_MARKETING_TYPE($id: String!) {
    deleteOnePRTPlanMarketingType(id: $id)
  }
`;

export const UPDATE_ONE_PLAN_MARKETIN_TYPE = gql`
  mutation UPDATE_ONE_PLAN_MARKETIN_TYPE(
    $idPharmacie: String
    $input: UpdateOnePRTPlanMarketingTypeInput!
  ) {
    updateOnePRTPlanMarketingType(input: $input) {
      ...PRTPlanMarketingTypeInfo
    }
  }
  ${PLAN_MARKETING_TYPE_INFO}
`;

export const DO_PERSONNALISE_ONE_PLAN_MARKETIN_TYPE = gql`
  mutation PERSONNALISE_ONE_PLAN_MARKETIN_TYPE(
    $idPharmacie: String!
    $input: PRTPlanMarketingTypeInput!
    $idPlanMarketingType: String!
  ) {
    personnaliserPRTPlanMarketingType(
      input: $input
      idPlanMarketingType: $idPlanMarketingType
      idPharmacie: $idPharmacie
    ) {
      ...PRTPlanMarketingTypeInfo
    }
  }
  ${PLAN_MARKETING_TYPE_INFO}
`;

export const DO_CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS = gql`
  mutation CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS(
    $idPharmacie: String!
    $idPlanMarketingType: String!
    $active: Boolean!
  ) {
    changePlanMarketingTypeActivationStatus(
      idPlanMarketingType: $idPlanMarketingType
      active: $active
      idPharmacie: $idPharmacie
    ) {
      ...PRTPlanMarketingTypeInfo
    }
  }
  ${PLAN_MARKETING_TYPE_INFO}
`;
