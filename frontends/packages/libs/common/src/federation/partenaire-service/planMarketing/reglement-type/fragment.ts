import { gql } from '@apollo/client';

export const PRT_REGLEMENT_TYPE_INFO = gql`
  fragment PRTReglementTypeInfo on PRTReglementType {
    id
    code
    libelle
  }
`;
