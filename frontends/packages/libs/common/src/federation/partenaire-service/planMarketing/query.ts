import gql from 'graphql-tag';
import {
  FULL_PLAN_MARKETING_INFO,
  PLAN_MARKETING_TYPE_INFO,
  PLAN_MARKETING_STATUT_INFO,
  MISE_AVANT_INFO,
  PLAN_MARKETING_CATEGORIE_INFO,
  REMISE_ARRIERE_TYPE_INFO,
} from './fragment';

export const GET_PLAN_MARKETINGS = gql`
  query GET_PLAN_MARKETINGS(
    $idPharmacie: String
    $paging: OffsetPaging
    $filter: PRTPlanMarketingFilter
    $sorting: [PRTPlanMarketingSort!]
  ) {
    pRTPlanMarketings(paging: $paging, filter: $filter, sorting: $sorting) {
      totalCount
      nodes {
        ...PRTPlanMarketingInfo
      }
    }
  }

  ${FULL_PLAN_MARKETING_INFO}
`;

export const GET_PLAN_MARKETING = gql`
  query GET_PLAN_MARKETING($id: ID!, $idPharmacie: String) {
    pRTPlanMarketing(id: $id) {
      ...PRTPlanMarketingInfo
    }
  }

  ${FULL_PLAN_MARKETING_INFO}
`;

export const GET_PLAN_MARKETING_AGGREGATES = gql`
  query GET_PLAN_MARKETING_AGGREGATES($filter: PRTPlanMarketingAggregateFilter) {
    pRTPlanMarketingAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_LIST_PLAN_MARKETING_TYPES = gql`
  query GET_LIST_PLAN_MARKETING_TYPES(
    $idPharmacie: String
    $paging: OffsetPaging
    $filter: PRTPlanMarketingTypeFilter
    $sorting: [PRTPlanMarketingTypeSort!]
  ) {
    pRTPlanMarketingTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      totalCount
      nodes {
        ...PRTPlanMarketingTypeInfo
      }
    }
  }
  ${PLAN_MARKETING_TYPE_INFO}
`;
export const GET_LIST_PLAN_MARKETING_STATUT = gql`
  query GET_LIST_PLAN_MARKETING_STATUT(
    $paging: OffsetPaging
    $filter: PRTPlanMarketingStatutFilter
    $sorting: [PRTPlanMarketingStatutSort!]
  ) {
    pRTPlanMarketingStatuts(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTPlanMarketingStatutInfo
      }
    }
  }
  ${PLAN_MARKETING_STATUT_INFO}
`;

export const GET_LIST_MISE_AVANT = gql`
  query LIST_MISE_AVANT($paging: OffsetPaging, $filter: PRTMiseAvantFilter, $sorting: [PRTMiseAvantSort!]) {
    pRTMiseAvants(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTMiseAvantInfo
      }
    }
  }
  ${MISE_AVANT_INFO}
`;

export const PLAN_MARKETING_CATEGORIES = gql`
  query PLAN_MARKETING_CATEGORIES(
    $paging: OffsetPaging
    $filter: PRTPlanMarketingCategorieFilter
    $sorting: [PRTPlanMarketingCategorieSort!]
  ) {
    pRTPlanMarketingCategories(paging: $paging, filter: $filter, sorting: $sorting) {
      totalCount
      nodes {
        ...PRTPlanMarketingCategorieInfo
      }
    }
  }
  ${PLAN_MARKETING_CATEGORIE_INFO}
`;

export const REMISE_ARRIERE_TYPES = gql`
  query REMISE_ARRIERE_TYPES(
    $paging: OffsetPaging
    $filter: PRTRemiseArriereTypeFilter
    $sorting: [PRTRemiseArriereTypeSort!]
  ) {
    pRTRemiseArriereTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      totalCount
      nodes {
        ...PRTRemiseArriereTypeInfo
      }
    }
  }
  ${REMISE_ARRIERE_TYPE_INFO}
`;

export const COMPUTE_PLAN_BY_CATEGORIE = gql`
  query COMPUTE_PLAN_BY_CATEGORIE($filter: PRTPlanMarketingAggregateFilter) {
    pRTPlanMarketingAggregate(filter: $filter) {
      groupBy {
        idCategorie
      }
      sum {
        montant
        montantStatutCloture
      }
    }
  }
`;

export const SEND_EMAIL_TRADE = gql`
  query SEND_EMAIL_TRADE($input: PRTSendEmailTradeInput!) {
    sendEmailTradeMarketing(input: $input)
  }
`;

export const SEND_EMAIL_TRADE_MANUEL = gql`
  query SEND_EMAIL_TRADE_MANUEL($input: PRTSendEmailTradeInput!) {
    sendEmailTradeManuel(input: $input) {
      statut
      message
    }
  }
`;
