import { gql } from '@apollo/client';
import { PRT_PLAN_MARKETING_FICHIER_FULL_INFO } from './fragment';

export const CREATE_ONE_PRT_PLAN_MARKETING_FICHIER = gql`
  mutation CREATE_ONE_PRT_PLAN_MARKETING_FICHIER($input: PRTPlanMarketingFichierInput!) {
    createOnePrtPlanMarketingFichier(input: $input) {
      ...PrtPlanMarketingFichierInfo
    }
  }
  ${PRT_PLAN_MARKETING_FICHIER_FULL_INFO}
`;
