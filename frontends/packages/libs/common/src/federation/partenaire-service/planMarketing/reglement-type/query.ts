import { gql } from '@apollo/client';
import { PRT_REGLEMENT_TYPE_INFO } from './fragment';

export const GET_PRT_REGLEMENT_TYPES = gql`
  query GET_PRT_REGLEMENT_TYPES(
    $paging: OffsetPaging
    $filter: PRTReglementTypeFilter
    $sorting: [PRTReglementTypeSort!]
  ) {
    pRTReglementTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTReglementTypeInfo
      }
    }
  }
  ${PRT_REGLEMENT_TYPE_INFO}
`;
