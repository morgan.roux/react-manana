import { gql } from '@apollo/client';
import { PRT_REGLEMENT_LIEU_DECLARATION_INFO } from './fragment';

export const GET_PRT_REGLEMENT_LIEU_DECLARATIONS = gql`
  query GET_PRT_REGLEMENT_LIEU_DECLARATIONS(
    $paging: OffsetPaging
    $filter: PRTReglementLieuDeclarationFilter
    $sorting: [PRTReglementLieuDeclarationSort!]
  ) {
    pRTReglementLieuDeclarations(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PrtReglementLieuDeclarationInfo
      }
    }
  }
  ${PRT_REGLEMENT_LIEU_DECLARATION_INFO}
`;
