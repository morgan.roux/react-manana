import gql from 'graphql-tag';
import { PRODUIT_STOCK_INFO } from './fragment';

export const PRODUIT_STOCKS = gql`
  query PRODUIT_STOCKS($paging: OffsetPaging, $filter: ProduitStockFilter, $sorting: [ProduitStockSort!]) {
    produitStocks(filter: $filter, paging: $paging, sorting: $sorting) {
      nodes {
        ...ProduitStockInfo
      }
      totalCount
    }
  }
  ${PRODUIT_STOCK_INFO}
`;

export const PRODUIT_STOCK_AGGREGATE = gql`
  query PRODUIT_STOCK_AGGREGATE($filter: ProduitStockAggregateFilter) {
    produitStockAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;
