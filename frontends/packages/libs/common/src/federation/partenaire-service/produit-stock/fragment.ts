import gql from 'graphql-tag';

export const PRODUIT_STOCK_INFO = gql`
  fragment ProduitStockInfo on ProduitStock {
    id
    idProduit
    idPharmacie
    idLaboratoire
    dateChangementStock
    qteStock
  }
`;
