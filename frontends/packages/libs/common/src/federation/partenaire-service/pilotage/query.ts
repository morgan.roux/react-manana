import gql from 'graphql-tag';
import { PRESTATION_TYPE_INFO, REMUNERATION_MENSUELLE_INFO, REMUNERATION_SUIVI_OPERATIONNEL_INFO } from './fragment';

export const GET_REMUNERATION_MENSUELLE = gql`
  query GET_REMUNERATION_MENSUELLE($input: PRTRemunerationMensuelleInput!) {
    remunerationMensuelle(input: $input) {
      ...RemunerationMensuelleInfo
    }
  }
  ${REMUNERATION_MENSUELLE_INFO}
`;

export const GET_REMUNERATION_SUIVI_OPERATIONNEL = gql`
  query GET_REMUNERATION_SUIVI_OPERATIONNEL($input: PRTRemunerationEvolutionInput!) {
    remunerationSuiviOperationnel(input: $input) {
      ...RemunerationSuiviOperationnelInfo
    }
  }
  ${REMUNERATION_SUIVI_OPERATIONNEL_INFO}
`;

export const GET_REMUNERATION_SUIVI_OPERATIONNELLES = gql`
  query GET_REMUNERATION_SUIVI_OPERATIONNELLES($input: PRTRemunerationsEvolutionsInput!) {
    remunerationsSuiviOperationnelles(input: $input) {
      ...RemunerationSuiviOperationnelInfo
    }
  }
  ${REMUNERATION_SUIVI_OPERATIONNEL_INFO}
`;

export const GET_PRESTATION_TYPE = gql`
  query GET_PRESTATION_TYPE($paging: OffsetPaging, $filter: PrestationTypeFilter, $sorting: [PrestationTypeSort!]) {
    prestationTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PrestationTypeInfo
      }
    }
  }
  ${PRESTATION_TYPE_INFO}
`;
