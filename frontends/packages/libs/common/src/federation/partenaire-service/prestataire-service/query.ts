import { gql } from '@apollo/client';
import { PRESTATAIRE_SERVICE_INFO } from './fragment';

export const GET_PRESTATAIRE_SERVICE = gql`
  query GET_PRESTATAIRE_SERVICE($id: ID!) {
    prestataireService(id: $id) {
      ...PrestataireServiceInfo
    }
  }
  ${PRESTATAIRE_SERVICE_INFO}
`;

export const GET_PRESTATAIRE_SERVICES = gql`
  query GET_PRESTATAIRE_SERVICES(
    $paging: OffsetPaging
    $filter: PrestataireServiceFilter
    $sorting: [PrestataireServiceSort!]
  ) {
    prestataireServices(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PrestataireServiceInfo
      }
      totalCount
    }
  }
  ${PRESTATAIRE_SERVICE_INFO}
`;
