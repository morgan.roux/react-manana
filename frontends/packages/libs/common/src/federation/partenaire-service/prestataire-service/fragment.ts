import gql from 'graphql-tag';
export const PRESTATAIRE_SERVICE_INFO = gql`
  fragment PrestataireServiceInfo on PrestataireService {
    id
    nom
    dataType
    idPrestataireServiceSuite
    sortie
    idPharmacie
    prive
    prestataireServiceSuite {
      id
      adresse1
      adresse2
      codePostal
      telephone
      ville
      email
      webSiteUrl
    }
    createdAt
    updatedAt
    photo {
      id
      chemin
      nomOriginal
      type
      publicUrl
    }
  }
`;
