import gql from 'graphql-tag';
import { PRESTATAIRE_SERVICE_INFO } from './fragment';

export const UPDATE_ONE_PRESTATAIRE_SERVICE = gql`
  mutation UPDATE_ONE_PRESTATAIRE_SERVICE($input: PrestataireServiceInput!, $id: String!) {
    updateOnePrestataireService(input: $input, id: $id) {
      ...PrestataireServiceInfo
    }
  }
  ${PRESTATAIRE_SERVICE_INFO}
`;

export const CREATE_ONE_PRESTATAIRE_SERVICE = gql`
  mutation CREATE_ONE_PRESTATAIRE_SERVICE($input: PrestataireServiceInput!) {
    createOnePrestataireService(input: $input) {
      ...PrestataireServiceInfo
    }
  }
  ${PRESTATAIRE_SERVICE_INFO}
`;
