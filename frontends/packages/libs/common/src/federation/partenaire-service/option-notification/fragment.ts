import { gql } from '@apollo/client';

export const FULL_PRT_OPTION_NOTIFICATION_INFO = gql`
  fragment PRTOptionNotification on PRTOptionNotification {
    id
    idPharmacie
    idGroupement
    idPartenaireTypeAssocie
    partenaireType
    code
    valeur
  }
`;
