import { gql } from '@apollo/client';
import { FULL_PRT_OPTION_NOTIFICATION_INFO } from './fragment';

export const GET_PRT_OPTION_NOTIFICATION = gql`
  query GET_PRT_OPTION_NOTIFICATION(
    $paging: OffsetPaging
    $filter: PRTOptionNotificationFilter
    $sorting: [PRTOptionNotificationSort!]
  ) {
    pRTOptionNotifications(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTOptionNotification
      }
    }
  }
  ${FULL_PRT_OPTION_NOTIFICATION_INFO}
`;
