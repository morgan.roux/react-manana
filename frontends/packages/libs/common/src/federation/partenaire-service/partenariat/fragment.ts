import gql from 'graphql-tag';
import { MINIMAL_USER_INFO } from '../../iam/user/fragment';
import { FULL_PRT_OPTION_NOTIFICATION_INFO } from '../option-notification/fragment';
import { FULL_PRT_PARTENARIAT_CORRESPONDANT } from '../partenariat-correspondant/fragment';

export const FULL_PARTENARIAT_STATUT_INFO = gql`
  fragment PartenariatStatutInfo on PRTPartenariatStatut {
    id
    code
    libelle
  }
`;

export const FULL_PARTENARIAT_TYPE_INFO = gql`
  fragment PartenariatTypeInfo on PRTPartenariatType {
    id
    code
    libelle
  }
`;

export const FULL_PARTENARIAT_INFO = gql`
  fragment PartenariatInfo on PRTPartenariat {
    id
    dateDebut
    dateFin
    optionNotifications {
      ...PRTOptionNotification
    }
    partenariatCorrespondants {
      ...PRTPartenariatCorrespondant
    }
    typeReunion
    type {
      ...PartenariatTypeInfo
    }
    statut {
      ...PartenariatStatutInfo
    }
    responsables {
      ...MinimalUserInfo
    }
  }
  ${FULL_PARTENARIAT_STATUT_INFO}
  ${FULL_PARTENARIAT_TYPE_INFO}
  ${MINIMAL_USER_INFO}
  ${FULL_PRT_OPTION_NOTIFICATION_INFO}
  ${FULL_PRT_PARTENARIAT_CORRESPONDANT}
`;
