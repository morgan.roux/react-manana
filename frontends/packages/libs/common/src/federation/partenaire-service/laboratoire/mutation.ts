import gql from 'graphql-tag';
import { FICHE_LABORATOIRE_INFO, PARAMETRE_LABORATOIRE_INFO } from './fragment';

export const LABORATOIRE_SET_PRESTATAIRE_SERVICE_AS_SECTEUR_CONTACT = gql`
  mutation LABORATOIRE_SET_PRESTATAIRE_SERVICE_AS_SECTEUR_CONTACT(
    $idSecteur: String!
    $idPrestataireService: String!
    $idLaboratoire: String!
    $idPharmacie: String!
  ) {
    laboratoireSetPrestataireServiceAsSecteurContact(
      idSecteur: $idSecteur
      idPrestataireService: $idPrestataireService
      idLaboratoire: $idLaboratoire
    ) {
      ...FicheLaboratoireInfo
    }
  }
  ${FICHE_LABORATOIRE_INFO}
`;

export const LABORATOIRE_DELETE_PRESTATAIRE_SERVICE_AS_SECTEUR_CONTACT = gql`
  mutation LABORATOIRE_DELETE_PRESTATAIRE_SERVICE_AS_SECTEUR_CONTACT(
    $idSecteur: String!
    $idLaboratoire: String!
    $idPharmacie: String!
  ) {
    laboratoireDeletePrestataireServiceAsSecteurContact(idSecteur: $idSecteur, idLaboratoire: $idLaboratoire) {
      ...FicheLaboratoireInfo
    }
  }
  ${FICHE_LABORATOIRE_INFO}
`;

export const UPDATE_ONE_LABORATOIRE = gql`
  mutation UPDATE_ONE_LABORATOIRE($input: LaboratoireInput!, $id: String!, $idPharmacie: String!) {
    updateOneLaboratoire(input: $input, id: $id) {
      ...FicheLaboratoireInfo
    }
  }
  ${FICHE_LABORATOIRE_INFO}
`;

export const CREATE_ONE_LABORATOIRE = gql`
  mutation CREATE_ONE_LABORATOIRE($input: LaboratoireInput!, $idPharmacie: String!) {
    createOneLaboratoire(input: $input) {
      ...FicheLaboratoireInfo
    }
  }
  ${FICHE_LABORATOIRE_INFO}
`;

export const DO_PUBLISH_PRODUIT_REFERENCES = gql`
  query PUBLISH_PRODUIT_REFERENCES($idPharmacie: String) {
    publishProduitReferences(idPharmacie: $idPharmacie)
  }
`;

export const RATTACHER_LABORATOIRE = gql`
  mutation RATTACHER_LABORATOIRE($idLaboratoireRattachement: String!, $idLaboratoire: String!) {
    rattacherLaboratoire(idLaboratoireRattachement: $idLaboratoireRattachement, idLaboratoire: $idLaboratoire) {
      ...LaboratoireInfo
    }
  }
  ${PARAMETRE_LABORATOIRE_INFO}
`;

export const DELETE_LABORATOIRE_RATTACHEMENT = gql`
  mutation DELETE_LABORATOIRE_RATTACHEMENT($id: String!) {
    deleteLaboratoireRattachement(id: $id) {
      ...LaboratoireInfo
    }
  }
  ${PARAMETRE_LABORATOIRE_INFO}
`;
