import gql from 'graphql-tag';
import { MINIMAL_USER_INFO } from 'src/federation/iam/user/fragment';
import { PARAMETRE_LABORATOIRE_INFO } from './fragment';
import { SEARCH_TODO_ACTION_INFO } from '../../basis/todo-action/fragment';
import { FICHE_LABORATOIRE_INFO } from './fragment';
import { FULL_PRT_OPTION_NOTIFICATION_INFO } from '../option-notification/fragment';
import { FULL_PRT_PARTENARIAT_CORRESPONDANT } from '../partenariat-correspondant/fragment';

export const GET_LABORATOIRES = gql`
  query GET_LABORATOIRES(
    $paging: OffsetPaging
    $filter: LaboratoireFilter
    $sorting: [LaboratoireSort!]
    $idPharmacie: String!
  ) {
    laboratoires(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        id
        nom
        idPharmacie
        prive
        partenariat(idPharmacie: $idPharmacie) {
          id
          dateDebut
          dateFin
          idPharmacie
          typeReunion
          optionNotifications {
            ...PRTOptionNotification
          }
          partenariatCorrespondants {
            ...PRTPartenariatCorrespondant
          }
          type {
            id
            libelle
            code
          }
          statut {
            id
            libelle
            code
          }
        }
      }
    }
  }
  ${FULL_PRT_OPTION_NOTIFICATION_INFO}
  ${FULL_PRT_PARTENARIAT_CORRESPONDANT}
`;

export const GET_LABORATOIRE = gql`
  query GET_LABORATOIRE($id: ID!, $idPharmacie: String!) {
    laboratoire(id: $id) {
      id
      nom
      idPharmacie
    prive  
      partenariat(idPharmacie: $idPharmacie) {
        id
        dateDebut
        dateFin
        idPharmacie
        optionNotifications {
            ...PRTOptionNotification
          }
          partenariatCorrespondants{
            ...PRTPartenariatCorrespondant
          },
        typeReunion
        responsables(idPharmacie: $idPharmacie) {
          ...MinimalUserInfo
        }
        type {
          id
          libelle
          code
        }
        statut {
          id
          libelle
          code
        }
      }
      laboratoireSuite {
        id
        adresse
        codePostal
        telephone
        ville
        email
        webSiteUrl
      }
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }
    ${MINIMAL_USER_INFO}
    ${FULL_PRT_OPTION_NOTIFICATION_INFO}
  ${FULL_PRT_PARTENARIAT_CORRESPONDANT}
  }
`;

export const SEARCH_PARAMETRE_LABORATOIRES = gql`
  query SEARCH_PARAMETRE_LABORATOIRES(
    $index: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $include: [String]
    $exclude: [String]
  ) {
    search(
      index: $index
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
      include: $include
      exclude: $exclude
    ) {
      total
      data {
        ... on Laboratoire {
          ...LaboratoireInfo
        }
      }
    }
  }
  ${PARAMETRE_LABORATOIRE_INFO}
`;

export const SEARCH_PARTENAIRE_TODO_ACTION = gql`
  query SEARCH_PARTENAIRE_TODO_ACTION(
    $index: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $include: [String]
    $exclude: [String]
  ) {
    search(
      index: $index
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
      include: $include
      exclude: $exclude
    ) {
      total
      data {
        ... on TodoAction {
          ...SearchTodoActionInfo
          partenaireType
          idPartenaireTypeAssocie
          partenaireTypeAssocie {
            ... on Laboratoire {
              id
              nom
              photo {
                id
                chemin
                nomOriginal
                type
                publicUrl
              }
              idLaboSuite
            }
            ... on PrestataireService {
              id
              nom
              photo {
                id
                chemin
                nomOriginal
                type
                publicUrl
              }
              idPrestataireServiceSuite
            }
          }
        }
      }
    }
  }
  ${SEARCH_TODO_ACTION_INFO}
`;

export const GET_FICHE_LABORATOIRE = gql`
  query GET_FICHE_LABORATOIRE($id: ID!,$idPharmacie: String!) {
   laboratoire(id: $id) {
     ...FicheLaboratoireInfo
  }
  ${FICHE_LABORATOIRE_INFO}
  }
`;
