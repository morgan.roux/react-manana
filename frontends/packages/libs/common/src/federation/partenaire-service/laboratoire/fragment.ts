import gql from 'graphql-tag';
import { PRT_CONTACT_SEARCH_INFO } from './../contact/fragment';
import { PRESTATAIRE_SERVICE_INFO } from './../prestataire-service/fragment';
import { PARTENAIRE_ACCES_INFO } from './../partenaire-acces/fragment';
import { CONTACT_CREDENTIAL_INFO } from './../contact-credential/fragment';
export const PARAMETRE_LABORATOIRE_INFO = gql`
  fragment LaboratoireInfo on Laboratoire {
    id
    nom
    idLaboSuite
    sortie
    idPharmacie
    prive
    laboratoireRattachement {
      id
      nom
    }
    laboratoireRattaches {
      id
      nom
    }
    laboratoireSuite {
      id
      adresse
      codePostal
      telephone
      ville
      email
      webSiteUrl
    }
    pharmacieInfos
    createdAt
    updatedAt
    photo {
      id
      chemin
      nomOriginal
      type
      publicUrl
    }
  }
`;

export const FICHE_LABORATOIRE_INFO = gql`
  fragment FicheLaboratoireInfo on Laboratoire {
    id
    nom
    idLaboSuite
    sortie
    idPharmacie
    prive
    laboratoireRattachement {
      id
      nom
    }
    laboratoireRattaches {
      id
      nom
    }
    laboratoireSuite {
      id
      adresse
      codePostal
      telephone
      ville
      email
      webSiteUrl
    }
    createdAt
    updatedAt
    photo {
      id
        chemin
        nomOriginal
        type
      publicUrl
    }

    secteurContacts(idPharmacie:$idPharmacie) {
      secteur {
        id
        code
        libelle
      }
      contacts {
        ... on PRTContact {
          ...PRTContactInfo
        }
        ... on PRTContactCredential {
          ...ContactCredentialInfo
        }
        ... on PrestataireService {
          id
    nom
    dataType
    idPrestataireServiceSuite
    sortie
    prestataireServiceSuite {
      id
      adresse1
      adresse2
      codePostal
      telephone
      ville
      email
      webSiteUrl
    }
    createdAt
    updatedAt
    photo {
      id
      chemin
      nomOriginal
      type
      publicUrl
    }
          acces(idPharmacie:$idPharmacie) {
            ...PartenaireAccesInfo
          }
        }
      }
    }
   

  ${PRT_CONTACT_SEARCH_INFO}
  ${PARTENAIRE_ACCES_INFO}
  ${CONTACT_CREDENTIAL_INFO}
  }
`;
