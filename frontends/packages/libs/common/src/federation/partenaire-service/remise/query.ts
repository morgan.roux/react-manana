import gql from 'graphql-tag';
import { FULL_REMISE_INFO } from './fragment';

export const GET_REMISES = gql`
  query REMISES($paging: OffsetPaging, $filter: RemiseFilter, $sorting: [RemiseSort!]) {
    remises(filter: $filter, paging: $paging, sorting: $sorting) {
      nodes {
        ...FullRemiseInfo
      }
      totalCount
    }
  }
  ${FULL_REMISE_INFO}
`;
