import gql from 'graphql-tag';
import { MINIMAL_REMISE_INFO } from './fragment';

export const CREATE_ONE_REMISE = gql`
  mutation CREATE_ONE_REMISE($input: RemiseInput!) {
    createOneRemise(input: $input) {
      ...RemiseInfo
    }
  }
  ${MINIMAL_REMISE_INFO}
`;

export const DELETE_ONE_REMISE = gql`
  mutation DELETE_ONE_REMISE($input: DeleteOneRemiseInput!) {
    deleteOneRemise(input: $input) {
      id
    }
  }
`;

export const DELETE_MANY_REMISE = gql`
  mutation DELETE_MANY_REMISE($input: DeleteManyRemisesInput!) {
    deleteManyRemises(input: $input) {
      deletedCount
    }
  }
`;
