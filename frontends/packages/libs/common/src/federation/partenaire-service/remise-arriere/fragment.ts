import { gql } from '@apollo/client';

export const PRT_REMISE_ARRIERE_TYPE_INFO = gql`
  fragment PRTRemiseArriereTypeInfo on PRTRemiseArriereType {
    id
    code
    libelle
  }
`;
