import { gql } from '@apollo/client';
import { PRT_REMISE_ARRIERE_TYPE_INFO } from './fragment';

export const GET_PRT_REMISE_ARRIERE_TYPES = gql`
  query GET_PRT_REMISE_ARRIERE_TYPES(
    $paging: OffsetPaging
    $filter: PRTRemiseArriereTypeFilter
    $sorting: [PRTRemiseArriereTypeSort!]
  ) {
    pRTRemiseArriereTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTRemiseArriereTypeInfo
      }
    }
  }
  ${PRT_REMISE_ARRIERE_TYPE_INFO}
`;

export const COMPUTE_REMISE_ARRIERE = gql`
  query COMPUTE_REMISE_ARRIERE($input: ComputeRemiseArriereInput!) {
    computeRemiseArriere(input: $input) {
      prevue {
        count
        type
      }
      paye {
        count
        type
      }
    }
  }
`;
