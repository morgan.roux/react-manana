import gql from 'graphql-tag';
import { DEPARTEMENT_INFO } from '../../basis/departement/fragment';
import { FONCTION_INFO } from './../../basis/fonction/fragment';
export const CONTACT_INFO = gql`
  fragment ContactInfo on ContactType {
    id
    cp
    ville
    pays
    adresse1
    adresse2
    faxProf
    faxPerso
    telProf
    telMobProf
    telPerso
    telMobPerso
    mailProf
    mailPerso
    siteProf
    sitePerso
    whatsappMobProf
    whatsappMobPerso
    compteSkypeProf
    compteSkypePerso
    urlLinkedInProf
    urlLinkedInPerso
    urlTwitterProf
    urlTwitterPerso
    urlFacebookProf
    urlFacebookPerso
    urlMessenger
    urlYoutube
    codeMaj
    idUserCreation
    idUserModification
  }
`;

export const PRT_CONTACT_FULL_INFO = gql`
  fragment PRTContactFullInfo on PRTContact {
    id
    prive
    civilite
    nom
    prenom
    idFonction
    fonction {
      ...BasisFonctionInfo
    }
    idPartenaireTypeAssocie
    partenaireType
    partenaireTypeAssocie {
      ... on Laboratoire {
        id
        nom
        idLaboSuite
      }
      ... on PrestataireService {
        id
        nom
        idPrestataireServiceSuite
      }
    }
    photo {
      id
      publicUrl
      chemin
      nomOriginal
      type
    }
    contact {
      ...ContactInfo
    }
    departements {
      ...DepartementInfo
    }
  }
  ${CONTACT_INFO}
  ${FONCTION_INFO}
  ${DEPARTEMENT_INFO}
`;

export const PRT_CONTACT_SEARCH_INFO = gql`
  fragment PRTContactInfo on PRTContact {
    id
    dataType
    prive
    civilite
    nom
    prenom
    idFonction
    idPartenaireTypeAssocie
    partenaireType
    partenaireTypeAssocie {
      ... on Laboratoire {
        id
        nom
        idLaboSuite
      }
      ... on PrestataireService {
        id
        nom
        idPrestataireServiceSuite
      }
    }
    fonction {
      ...BasisFonctionInfo
    }
    departements {
      ...DepartementInfo
    }
    photo {
      id
      publicUrl
      chemin
      nomOriginal
      type
    }
    contact {
      id
      mailProf
      mailPerso
      adresse1
      sitePerso
      telMobPerso
      telPerso
      telProf
      urlFacebookPerso
      urlLinkedInPerso
      urlMessenger
      urlYoutube
      ville
      whatsappMobProf
      cp
      ville
      whatsappMobPerso
    }
  }
  ${DEPARTEMENT_INFO}
  ${FONCTION_INFO}
`;
