import gql from 'graphql-tag';
import { PRT_CONTACT_FULL_INFO } from './fragment';

export const GET_PRT_CONTACT = gql`
  query GET_PRT_CONTACT($paging: OffsetPaging, $filter: PRTContactFilter, $sorting: [PRTContactSort!]) {
    pRTContacts(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTContactFullInfo
      }
    }
  }
  ${PRT_CONTACT_FULL_INFO}
`;

export const SEND_EMAIL_CONTACT = gql`
  query SEND_EMAIL_CONTACT($input: PRTSendEmailContactInput!) {
    sendEmailContact(input: $input) {
      statut
      message
    }
  }
`;

export const SEND_EMAIL_AUTO_CONTACT = gql`
  query SEND_EMAIL_AUTO_CONTACT {
    sendEmailAutoContact
  }
`;
