import { gql } from '@apollo/client';
import { PRT_CONTACT_FULL_INFO } from '../contact/fragment';

export const FULL_PRT_PARTENARIAT_CORRESPONDANT = gql`
  fragment PRTPartenariatCorrespondant on PRTPartenariatCorrespondant {
    id
    idPharmacie
    idGroupement
    idPartenaireTypeAssocie
    partenaireType
    code
    idContact
    contact {
      ...PRTContactFullInfo
    }
  }
  ${PRT_CONTACT_FULL_INFO}
`;
