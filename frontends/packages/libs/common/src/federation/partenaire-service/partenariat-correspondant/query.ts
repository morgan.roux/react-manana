import { gql } from '@apollo/client';
import { FULL_PRT_PARTENARIAT_CORRESPONDANT } from './fragment';

export const GET_CORRESPONDANTS_PRT_PARTENARIAT = gql`
  query GET_CORRESPONDANTS_PRT_PARTENARIAT(
    $paging: OffsetPaging
    $filter: PRTPartenariatCorrespondantFilter
    $sorting: [PRTPartenariatCorrespondantSort!]
  ) {
    pRTPartenariatCorrespondants(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTPartenariatCorrespondant
      }
    }
  }
  ${FULL_PRT_PARTENARIAT_CORRESPONDANT}
`;
