/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_MODELE_OPERATION_MARKETING
// ====================================================

export interface DELETE_MODELE_OPERATION_MARKETING_deleteOnePRTPlanMarketingTypeAction {
  __typename: "PlanMarketingTypeAction";
  id: string;
}

export interface DELETE_MODELE_OPERATION_MARKETING {
  deleteOnePRTPlanMarketingTypeAction: DELETE_MODELE_OPERATION_MARKETING_deleteOnePRTPlanMarketingTypeAction;
}

export interface DELETE_MODELE_OPERATION_MARKETINGVariables {
  id: string;
}
