import gql from 'graphql-tag';
import { FULL_MODELE_OPERATION_MARKETING_INFO } from './fragment';

export const GET_MODELE_OPERATION_MARKETINGS = gql`
  query GET_MODELE_OPERATION_MARKETINGS(
    $idPharmacie: String
    $filter: PlanMarketingTypeActionFilter
    $paging: OffsetPaging
    $sorting: [PlanMarketingTypeActionSort!]
  ) {
    planMarketingTypeActions(filter: $filter, paging: $paging, sorting: $sorting) {
      nodes {
        ...ModeleOperationMarketingInfo
      }
    }
  }
  ${FULL_MODELE_OPERATION_MARKETING_INFO}
`;
