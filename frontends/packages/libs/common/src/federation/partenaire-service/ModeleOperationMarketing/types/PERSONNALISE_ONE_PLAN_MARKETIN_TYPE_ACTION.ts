/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanMarketingTypeActionInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION
// ====================================================

export interface PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION_personnaliserPRTPlanMarketingTypeAction_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION_personnaliserPRTPlanMarketingTypeAction_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION_personnaliserPRTPlanMarketingTypeAction_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION_personnaliserPRTPlanMarketingTypeAction_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION_personnaliserPRTPlanMarketingTypeAction_participants_role | null;
  photo: PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION_personnaliserPRTPlanMarketingTypeAction_participants_photo | null;
  phoneNumber: string | null;
}

export interface PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION_personnaliserPRTPlanMarketingTypeAction {
  __typename: "PlanMarketingTypeAction";
  id: string;
  pendant: number | null;
  active: boolean | null;
  jourLancement: number | null;
  description: string;
  idPlanMarketingType: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idGroupement: string;
  idPharmacie: string | null;
  importance: PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION_personnaliserPRTPlanMarketingTypeAction_importance;
  participants: PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION_personnaliserPRTPlanMarketingTypeAction_participants[];
}

export interface PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION {
  personnaliserPRTPlanMarketingTypeAction: PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION_personnaliserPRTPlanMarketingTypeAction;
}

export interface PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTIONVariables {
  idPharmacie: string;
  input: PRTPlanMarketingTypeActionInput;
  idPlanMarketingTypeAction: string;
}
