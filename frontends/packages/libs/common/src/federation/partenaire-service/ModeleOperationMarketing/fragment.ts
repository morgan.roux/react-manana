import gql from 'graphql-tag';
import { FULL_USER_INFO } from '../../iam/user/fragment';

export const FULL_MODELE_OPERATION_MARKETING_INFO = gql`
  fragment ModeleOperationMarketingInfo on PlanMarketingTypeAction {
    id
    pendant(idPharmacie: $idPharmacie)
    active(idPharmacie: $idPharmacie)
    jourLancement(idPharmacie: $idPharmacie)
    description(idPharmacie: $idPharmacie)
    idPlanMarketingType
    idFonction(idPharmacie: $idPharmacie)
    idTache(idPharmacie: $idPharmacie)
    idImportance(idPharmacie: $idPharmacie)
    idGroupement
    idPharmacie(idPharmacie: $idPharmacie)
    importance(idPharmacie: $idPharmacie) {
      id
      ordre
      libelle
      couleur
    }
    participants(idPharmacie: $idPharmacie) {
      ...FullUserInfo
    }
  }
  ${FULL_USER_INFO}
`;
