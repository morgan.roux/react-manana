/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS
// ====================================================

export interface CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS_changePRTPlanMarketingTypeActionActivationStatus_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS_changePRTPlanMarketingTypeActionActivationStatus_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS_changePRTPlanMarketingTypeActionActivationStatus_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS_changePRTPlanMarketingTypeActionActivationStatus_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS_changePRTPlanMarketingTypeActionActivationStatus_participants_role | null;
  photo: CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS_changePRTPlanMarketingTypeActionActivationStatus_participants_photo | null;
  phoneNumber: string | null;
}

export interface CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS_changePRTPlanMarketingTypeActionActivationStatus {
  __typename: "PlanMarketingTypeAction";
  id: string;
  pendant: number | null;
  active: boolean | null;
  jourLancement: number | null;
  description: string;
  idPlanMarketingType: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idGroupement: string;
  idPharmacie: string | null;
  importance: CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS_changePRTPlanMarketingTypeActionActivationStatus_importance;
  participants: CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS_changePRTPlanMarketingTypeActionActivationStatus_participants[];
}

export interface CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS {
  changePRTPlanMarketingTypeActionActivationStatus: CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS_changePRTPlanMarketingTypeActionActivationStatus;
}

export interface CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUSVariables {
  idPharmacie: string;
  idPlanMarketingTypeAction: string;
  active: boolean;
}
