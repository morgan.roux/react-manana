/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanMarketingTypeActionInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_MODELE_OPERATION_MARKETING
// ====================================================

export interface CREATE_MODELE_OPERATION_MARKETING_createOnePRTPlanMarketingTypeAction_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_MODELE_OPERATION_MARKETING_createOnePRTPlanMarketingTypeAction_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_MODELE_OPERATION_MARKETING_createOnePRTPlanMarketingTypeAction_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface CREATE_MODELE_OPERATION_MARKETING_createOnePRTPlanMarketingTypeAction_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: CREATE_MODELE_OPERATION_MARKETING_createOnePRTPlanMarketingTypeAction_participants_role | null;
  photo: CREATE_MODELE_OPERATION_MARKETING_createOnePRTPlanMarketingTypeAction_participants_photo | null;
  phoneNumber: string | null;
}

export interface CREATE_MODELE_OPERATION_MARKETING_createOnePRTPlanMarketingTypeAction {
  __typename: "PlanMarketingTypeAction";
  id: string;
  pendant: number | null;
  active: boolean | null;
  jourLancement: number | null;
  description: string;
  idPlanMarketingType: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idGroupement: string;
  idPharmacie: string | null;
  importance: CREATE_MODELE_OPERATION_MARKETING_createOnePRTPlanMarketingTypeAction_importance;
  participants: CREATE_MODELE_OPERATION_MARKETING_createOnePRTPlanMarketingTypeAction_participants[];
}

export interface CREATE_MODELE_OPERATION_MARKETING {
  createOnePRTPlanMarketingTypeAction: CREATE_MODELE_OPERATION_MARKETING_createOnePRTPlanMarketingTypeAction;
}

export interface CREATE_MODELE_OPERATION_MARKETINGVariables {
  input: PRTPlanMarketingTypeActionInput;
  idPharmacie?: string | null;
}
