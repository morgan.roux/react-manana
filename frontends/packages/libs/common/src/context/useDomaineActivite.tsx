import React from 'react';
import { DomaineActiviteInfoFragment } from '../federation';
import DispatchContext from './DomaineActiviteContext';

export const useDomaineActivite = (): DomaineActiviteInfoFragment => {
  return React.useContext(DispatchContext);
};
