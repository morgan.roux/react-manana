import React from 'react';
import DispatchContext from './ModulesContext';

const useModules = () => {
  return React.useContext(DispatchContext);
};

export default useModules;
