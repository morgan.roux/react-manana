import React, { useState } from 'react';
import { ApplicationSearchContext } from './ApplicationContext';
import DispatchContext from './ApplicationContext';
import useApplicationContext from './useApplicationContext';

interface PagingInterface {
  skip: number;
  take: number;
}
const useSearch = () => {
  const [paging, setPaging] = useState<PagingInterface>();
  const onUpdatePaging = (skip: number, take: number) => {
    setPaging({ skip, take });
  };
  return { take: paging?.take, skip: paging?.skip, setPaging: onUpdatePaging } as ApplicationSearchContext;
};

const useSearchContext = () => {
  const { search } = useApplicationContext();
  return search;
};

export default useSearch;
export { useSearchContext };
