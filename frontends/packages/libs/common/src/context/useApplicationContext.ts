import React from 'react';
import DispatchContext from './ApplicationContext';

const useApplicationContext = () => {
  return React.useContext(DispatchContext);
};

export default useApplicationContext;
