import { createContext } from 'react';
import { DomaineActiviteInfoFragment } from '../federation';

const DomaineActiviteContext = createContext<DomaineActiviteInfoFragment>({} as any);

export default DomaineActiviteContext;
