import { createContext } from 'react';
import { ModuleDefinition } from './../module';

type Context = [
  ModuleDefinition[],
  (newModules: ModuleDefinition[] | ((previousModules: ModuleDefinition[]) => ModuleDefinition[])) => void
];

const ModulesContext = createContext<Context>([[], () => {}]);

export default ModulesContext;
