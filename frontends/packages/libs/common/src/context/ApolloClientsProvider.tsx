import { ApolloClient } from '@apollo/client';
import React, { FC, useState } from 'react';
import AppolloClientsContext, { ApolloClients } from './AppolloClientsContext';

interface ApolloClientsProviderProps {
  federation: ApolloClient<any>;
  graphql: ApolloClient<any>;
}

const ApolloClientsProvider: FC<ApolloClientsProviderProps> = (props) => {
  const { children, federation, graphql } = props;
  const [clients, setClients] = useState<ApolloClients>({ federation, graphql });

  return <AppolloClientsContext.Provider value={[clients, setClients]}> {children} </AppolloClientsContext.Provider>;
};

export default ApolloClientsProvider;
