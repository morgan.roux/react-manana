import { useEffect, useState } from 'react';

const getWindowDimension = (): 'isLg' | 'isMd' | 'isSm' | 'unknown' => {
  const { innerWidth: width } = window;
  if (width >= 1280) {
    return 'isLg';
  }
  if (width >= 960 && width < 1280) {
    return 'isMd';
  } else if (width < 960) {
    return 'isSm';
  }
  return 'unknown';
};

export interface Device {
  isMobile: boolean;
  isLg: boolean;
  isMd: boolean;
  isSm: boolean;
  width: number;
}

export const useDevice = (): Device => {
  const [windowDimension, setWindowDimension] = useState<'isLg' | 'isMd' | 'isSm' | 'unknown'>(getWindowDimension());

  useEffect(() => {
    const handleResize = () => {
      setWindowDimension(getWindowDimension());
    };
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return {
    isMobile: windowDimension === 'isSm' || windowDimension === 'isMd',
    isLg: windowDimension === 'isLg',
    isMd: windowDimension === 'isMd',
    isSm: windowDimension === 'isSm',
    width: window.innerWidth,
  };
};
