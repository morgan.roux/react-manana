import React, { FC } from 'react';
import DomaineActiviteContext from './DomaineActiviteContext';
import { DomaineActiviteInfoFragment } from '../federation';

interface DomaineActiviteProviderProps {
  value: DomaineActiviteInfoFragment;
}

const DomaineActiviteProvider: FC<DomaineActiviteProviderProps> = (props) => {
  const { children, value } = props;

  return <DomaineActiviteContext.Provider value={value}> {children} </DomaineActiviteContext.Provider>;
};

export default DomaineActiviteProvider;
