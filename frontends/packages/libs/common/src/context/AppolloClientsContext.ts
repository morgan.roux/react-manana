import { ApolloClient } from '@apollo/client';
import { createContext } from 'react';

export interface ApolloClients {
  federation: ApolloClient<any>;
  graphql: ApolloClient<any>;
}

type Context = [
  ApolloClients,
  (newClients: ApolloClients | ((previousClients: ApolloClients) => ApolloClients)) => void
];

const ApolloClientsContext = createContext<Context>([{} as any, () => {}]);

export default ApolloClientsContext;
