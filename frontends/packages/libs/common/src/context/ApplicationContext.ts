import { ApolloClient } from '@apollo/client';
import { Feature, ModuleDefinition, ProtectedRouteProps } from '../module';
import { DomaineActiviteInfoFragment, Groupement, Pharmacie, User } from '../federation';
import { ParameterInfoFragment, TypeFichier } from '../graphql';
import { Notification } from '../shared';
import { createContext } from 'react';
import moment from 'moment';
import { ApplicationConfig } from '../config';
import { AppAuthorization } from '../authorization';

export interface ComputeUploadedFilenameOptions {
  directory?: string;
  prefix?: string;
  groupement: Groupement;
  pharmacie: Pharmacie;
}

export const uploadFilename = (file: File, options: ComputeUploadedFilenameOptions): string => {
  const directory = options?.directory || 'files';
  const identifierPrefix = options?.prefix || moment().format('YYYYMMDDhmmss');

  const cleanFilename = file.name.toLowerCase().replace(/[^a-z0-9]/g, '');
  const extension = file.name.split('.').pop();
  const newFilename = `groupements/${options.groupement.id}/pharmacies/${options.pharmacie.id}/${
    directory ? `${directory}/` : ``
  }${identifierPrefix}${cleanFilename}.${extension}`;
  return newFilename;
};

export const uploadFilenameWithoutDate = (file: File, options: ComputeUploadedFilenameOptions): string => {
  const directory = options?.directory || 'files';

  const cleanFilename = file.name.toLowerCase().replace(/[^a-z0-9]/g, '-');
  const extension = file.name.split('.').pop();
  const newFilename = `groupements/${options.groupement.id}/pharmacies/${options.pharmacie.id}/${
    directory ? `${directory}/` : ``
  }${cleanFilename}.${extension}`;

  return newFilename.replace(/ |-|\)|\(/g, '');
};

interface ApplicationParametersContext {
  parameters: ParameterInfoFragment[];
  useParameter: (code: string) => ParameterInfoFragment | undefined;
  useParameterValue: (code: string) => string | null | undefined;
  useParameterValueAsBoolean: (code: string) => boolean;
  useParameterValueEnabled: (codeGroupement: string, codePharmacie: string) => boolean;
  useParameterValueAsMoney: (code: string) => string | number;
  useParameterValueAsString: (code: string) => string;
  useParameterValueAsNumber: (code: string) => number;
  useParameterValueAsDate: (code: string) => string | null;
}

interface ApplicationModulesContext {
  modules: ModuleDefinition[];
  features: Feature[];
  routes: ProtectedRouteProps[];
  // useFeatures: (filter: Omit<FeatureFilter, 'device'>) => Feature[];
}

interface ApplicationUserContext {
  user: User;
  ip?: string;
  accessToken?: string;
  isSuperAdmin: boolean;
  isAdminGroupement: boolean;
  isTitulaire: boolean;
  isGroupePharmacie: boolean;
}

interface ApplicationDeviceContext {
  isMobile: boolean;
  isLg: boolean;
  isMd: boolean;
  isSm: boolean;
  width: number;
}

export interface ApplicationSearchContext {
  take?: number;
  skip?: number;
  setPaging: (skip: number, take: number) => void;
}

export interface UploadedFile {
  chemin: string;
  nomOriginal: string;
  type: TypeFichier;
}

export type ApplicationContextOptions = {
  pharmacies: Pharmacie[];
  currentPharmacie: Pharmacie;
  currentGroupement: Groupement;
  federation: ApolloClient<any>;
  graphql: ApolloClient<any>;
  config: ApplicationConfig;
  showAppBar: boolean;
  setShowAppBar: (newValue: boolean) => void;
  // notification
  notify: (notification: Omit<Notification, 'open'>) => void;
  // file name
  computeUploadedFilename: (file: File, options?: Partial<ComputeUploadedFilenameOptions>) => string;
  computeUploadedFilenameWithoutDate: (file: File, options?: Partial<ComputeUploadedFilenameOptions>) => string;

  uploadFiles: (
    files: (File | UploadedFile)[],
    options?: Partial<ComputeUploadedFilenameOptions>
  ) => Promise<UploadedFile[]>;

  domaineActivite: DomaineActiviteInfoFragment;

  //FUTURE USE search?: ApplicationSearchContext;
  auth: AppAuthorization;
} & ApplicationDeviceContext &
  ApplicationUserContext &
  ApplicationParametersContext &
  ApplicationModulesContext;

const ApplicationContext = createContext<ApplicationContextOptions>({} as any);

export default ApplicationContext;
