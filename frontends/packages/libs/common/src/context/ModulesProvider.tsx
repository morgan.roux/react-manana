import React, { FC, useState } from 'react';
import { ModuleDefinition } from 'src/module';
import ModulesContext from './ModulesContext';

interface ModulesProviderProps {
  modules: ModuleDefinition[];
}

const ModulesProvider: FC<ModulesProviderProps> = (props) => {
  const { children, modules: initialModules } = props;
  const [modules, setModules] = useState<ModuleDefinition[]>(initialModules);

  return <ModulesContext.Provider value={[modules, setModules]}> {children} </ModulesContext.Provider>;
};

export default ModulesProvider;
