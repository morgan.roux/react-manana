import React from 'react';
import DispatchContext from './AppolloClientsContext';

const useApolloClients = () => {
  return React.useContext(DispatchContext);
};

export default useApolloClients;
