import DeviceDetector from 'device-detector-js';

const detector = new DeviceDetector();

export interface DeviceInfo {
  clientType: string;
  clientName: string;
  clientVersion: string;
  osName: string;
  osVersion: string;
  osPlatform: string;
  deviceType: string;
  deviceBrand: string;
  deviceModel: string;
  botName: string;
  botCategory: string;
  botUrl: string;
  botProducerName: string;
  botProducerUrl: string;
  screenWidth: number;
  screenHeight: number;
  userAgent: string;
}

export interface ScreenSize {
  width: number;
  height: number;
}

let _cachedDevice: DeviceInfo | undefined = undefined;
export const getDevice = (): DeviceInfo => {
  if (_cachedDevice) {
    return _cachedDevice;
  }

  const userAgent = navigator.userAgent;
  const device = detector.parse(userAgent);
  const size = getScreenSize();

  _cachedDevice = {
    clientType: device.client?.type,
    clientName: device.client?.name,
    clientVersion: device.client?.version,
    osName: device.os?.name,
    osVersion: device.os?.version,
    osPlatform: device.os?.platform,
    deviceType: device.device?.type,
    deviceBrand: device.device?.brand,
    deviceModel: device.device?.model,
    botName: device.bot?.name,
    botCategory: device.bot?.category,
    botUrl: device.bot?.url,
    botProducerName: device.bot?.producer.name,
    botProducerUrl: device.bot?.producer.url,
    screenWidth: size.width,
    screenHeight: size.height,
    userAgent,
  } as any;

  return _cachedDevice as any;
};

export const getScreenSize = (): ScreenSize => {
  return {
    width: window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
    height: window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
  };
};
