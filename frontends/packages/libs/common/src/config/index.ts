export interface ApplicationConfig {
  STORAGE_PROVIDER: 'AWS_S3' | 'OVH_OBJECT_STORAGE';
  STORAGE_PUBLIC_BASE_PATH: string;

  UPLOAD_MODE: 'CLIENT_SIDE' | 'SERVER_SIDE';

  AWS_S3_BUCKET: string;
  AWS_S3_REGION: string;
  OS_TENANT_ID: string;
  OS_CONTAINER: string;

  APP_SSO_URL: string;
  CURRENT_VERSION: string;
  API_URL: string;
  ENVIRONMENT: string;
  WEBHOOK_URL: string;

  PDF_FILE_VIEWER: 'IFRAME' | 'COMPONENT' | 'GOOGLE_DOCS_VIEWER';
}
