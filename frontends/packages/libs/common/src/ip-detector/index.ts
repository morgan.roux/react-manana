let _cachedIp: string | undefined = undefined;

export const getIp = async (): Promise<string | undefined> => {
  if (_cachedIp) {
    return _cachedIp;
  }
  return fetch('https://api.ipify.org/?format=json')
    .then((results) => results.json())
    .then((data) => {
      _cachedIp = data.ip;
      return _cachedIp;
    })
    .catch(() => undefined);
};
