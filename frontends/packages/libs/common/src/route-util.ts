import { matchPath, RouteProps } from 'react-router';
import { ApplicationContextOptions } from './context/ApplicationContext';
import { ProtectedRouteProps } from './module';
export const isRoutePath = (routePath: string | string[] | RouteProps, currentPath: string): boolean =>
  matchPath(currentPath, routePath) ? true : false;

export const findRouteByPath = (
  routes: ProtectedRouteProps[],
  currentPath: string
): ProtectedRouteProps | undefined => {
  return routes.find((route) => isRoutePath(route, currentPath));
};

export const getOptionValue = <T>(
  value: T | ((context: ApplicationContextOptions) => T),
  context: ApplicationContextOptions
): T => {
  //@ts-ignore
  return typeof value === 'function' ? value(context) : value;
};
