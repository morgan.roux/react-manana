import * as React from 'react';
import { RouteProps } from 'react-router-dom';
import { InMemoryCacheConfig } from '@apollo/client';
import {
  FeatureAccueilOptions,
  FeatureFavoriteOptions,
  FeatureMobileQuickAccessOptions,
  FeatureQuickAccessOptions,
  FeatureAlertOptions,
} from './types';
import { ApplicationContextOptions } from './context/ApplicationContext';
import { MobileTopBarProps } from './components';

export type FeatureLocation =
  | 'BANNIERE'
  | 'MENU_PRINCIPAL'
  | 'ACCUEIL'
  | 'PARAMETRE'
  | 'ALERT'
  | 'QUICK_ACCESS'
  | 'MOBILE_QUICK_ACCESS'
  | 'FAVORITE';
export type FeatureDevice = 'WEB' | 'MOBILE' | 'ALL';

export interface Authorization {
  permissions?: string | string[]; // Permission based authorization (User.traitementCodes)
  roles?: string | string[]; // (rolesAllowed) roles based authorization (User.role.code)
  rolesDenied?: string | string[];
}

export type ActivationParameter =
  | string
  | {
      groupement?: string;
      pharmacie?: string;
    };
export interface FilterableByParameter {
  activationParameters?: ActivationParameter | ActivationParameter[];
}

export interface Feature extends FilterableByParameter {
  id: string;
  location: FeatureLocation;
  preferredOrder?: number;
  component?: React.ReactNode;
  icon?: React.ReactNode | string;
  name?: string | ((context: ApplicationContextOptions) => string);
  to?: string | ((context: ApplicationContextOptions) => string);
  activeWhen?: string | string[] | ((context: ApplicationContextOptions) => string | string[]); // Active feature when path
  device?: FeatureDevice | FeatureDevice[];
  authorize?: Authorization;
  options?: FeatureAccueilOptions &
    FeatureMobileQuickAccessOptions &
    FeatureQuickAccessOptions &
    FeatureFavoriteOptions &
    FeatureAlertOptions &
    Record<string, any>;
}

export interface ProtectedRouteProps extends RouteProps, FilterableByParameter {
  device?: FeatureDevice | FeatureDevice[];
  authorize?: Authorization; // if boolean, user must connected or traitementCodes
  options?: Record<string, any>;
  mobileOptions?: {
    topBar?: MobileTopBarProps | ((context: ApplicationContextOptions) => MobileTopBarProps);
  };
  attachTo?: 'PARAMETRE' | 'MAIN'; // used to know if this route must be to add at MainRouter or ParametreRouter
}
export interface ModuleDefinition {
  routes?: ProtectedRouteProps[];
  features?: Feature[];
  cacheConfig?: {
    federation?: InMemoryCacheConfig;
    graphql?: InMemoryCacheConfig;
  };
}

export const extractEnabledFeatures = (modules: ModuleDefinition[]): Feature[] => {
  return modules
    .reduce((features: Feature[], module: ModuleDefinition) => {
      return [...features, ...(module.features || [])];
    }, [])
    .sort((feature1, feature2) => (feature1.preferredOrder || 0) - (feature2.preferredOrder || 0));
};

export const extractEnabledRoutes = (modules: ModuleDefinition[]): ProtectedRouteProps[] => {
  return modules.reduce((routes: ProtectedRouteProps[], module: ModuleDefinition) => {
    return [...routes, ...(module.routes || [])];
  }, []);
};

export const extractFeaturesByLocation = (modules: ModuleDefinition[], location: FeatureLocation): Feature[] => {
  return modules
    .reduce((features: Feature[], module: ModuleDefinition) => {
      return [...features, ...(module.features || []).filter((feature) => feature.location === location)];
    }, [])
    .sort((feature1, feature2) => (feature1.preferredOrder || 0) - (feature2.preferredOrder || 0));
};

export interface RouteFilter {
  attachTo?: 'PARAMETRE' | 'MAIN';
}

export const filterRoutes = (routes: ProtectedRouteProps[], options: RouteFilter) => {
  return routes.filter((route) => {
    if (options.attachTo && route.attachTo && route.attachTo !== options.attachTo) {
      return false;
    }
    return true;
  });
};

export interface FeatureFilter {
  location?: FeatureLocation;
  device?: FeatureDevice;
  skipUndefined?: boolean;
}

export const filterFeatures = (features: Feature[], options: FeatureFilter) => {
  return sortFeaturesByPreferredOrder(
    features.filter((feature) => {
      // Check location
      if (options.location && feature.location !== options.location) {
        return false;
      }
      // check device
      if (
        options.device &&
        !(feature.device
          ? Array.isArray(feature.device)
            ? feature.device.includes(options.device)
            : feature.device === options.device
          : options.skipUndefined || false)
      ) {
        return false;
      }

      // section
      return true;
    })
  );
};

export const filterFeaturesByDevice = (
  features: Feature[],
  device: FeatureDevice,
  skipUndefined: boolean = false
): Feature[] => {
  return features.filter((feature) =>
    feature.device
      ? Array.isArray(feature.device)
        ? feature.device.includes(device)
        : feature.device === device
      : !skipUndefined
  );
};

const sortFeaturesByPreferredOrder = (features: Feature[]): Feature[] => {
  return features.sort((a, b) => (a.preferredOrder || 0) - (b.preferredOrder || 0));
};
