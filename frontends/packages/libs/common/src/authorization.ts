import {
  SUPER_ADMINISTRATEUR,
  ADMINISTRATEUR_GROUPEMENT,
  TITULAIRE_PHARMACIE,
  PARTENAIRE_SERVICE,
} from './shared/roles';
import {
  ACTU_CREATE_CODE,
  ACTU_EDIT_CODE,
  OC_CREATE_CODE,
  OC_EDIT_CODE,
  ACTU_VIEW_CODE,
  OC_VIEW_CODE,
  ACTU_DELETE_CODE,
  ACTU_MARK_SEEN_AND_NOT_SEEN_CODE,
  ACTU_LIKE_CODE,
  ACTU_COMMENT_CODE,
  OC_DELETE_CODE,
  OC_MARK_SEEN_AND_NOT_SEEN_CODE,
  OC_LIKE_CODE,
  OC_COMMENT_CODE,
  OC_VIEW_BUSINESS_CODE,
  OC_VIEW_FEEDBACK_CODE,
  INDEXING_CODE,
  THEME_CHANGE_CODE,
  PILOTAGE_BUSINESS_CODE,
  PILOTAGE_FEEDBACK_CODE,
  PILOTAGE_CHANGE_BY_OP_CODE,
  PILOTAGE_CHANGE_BY_PHARMA_CODE,
  PROMO_VIEW_LIST_CODE,
  PROMO_ADD_CODE,
  PROMO_EDIT_CODE,
  PROMO_DELETE_CODE,
  MARCHE_VIEW_LIST_CODE,
  MARCHE_ADD_CODE,
  MARCHE_EDIT_CODE,
  MARCHE_DELETE_CODE,
  PRODUCT_ADD_CODE,
  PRODUCT_EDIT_CODE,
  PRODUCT_DELETE_CODE,
  PRODUCT_VIEW_SALE_HISTORY_CODE,
  PUB_VIEW_LIST_CODE,
  PUB_ADD_CODE,
  PUB_EDIT_CODE,
  PUB_DELETE_CODE,
  GPR_VIEW_OPT_CODE,
  GPR_EDIT_OPT_CODE,
  SERV_VIEW_LIST_CODE,
  SERV_INIT_PWD_CODE,
  SERV_ACTIVATE_DEACTIVATE_USER_CODE,
  SERV_ADD_USER_CODE,
  SERV_EDIT_USER_CODE,
  SERV_VIEW_DETAILS_CODE,
  SERV_VIEW_HISTORY_CODE,
  PHARMA_VIEW_LIST_CODE,
  PHARMA_EDIT_CODE,
  PPERSO_VIEW_LIST_CODE,
  PPERSO_INIT_PWD_CODE,
  PPERSO_ACTIVATE_DEACTIVATE_USER_CODE,
  PPERSO_ADD_USER_CODE,
  PPERSO_EDIT_USER_CODE,
  PPERSO_VIEW_DETAILS_CODE,
  PPERSO_VIEW_HISTORY_CODE,
  LABO_VIEW_LIST_CODE,
  LABO_INIT_PWD_CODE,
  LABO_ACTIVATE_DEACTIVATE_USER_CODE,
  LABO_ADD_USER_CODE,
  LABO_EDIT_USER_CODE,
  LABO_VIEW_DETAILS_CODE,
  LABO_VIEW_HISTORY_CODE,
  PDR_VIEW_LIST_CODE,
  PDR_INIT_PWD_CODE,
  PDR_ACTIVATE_DEACTIVATE_USER_CODE,
  PDR_ADD_USER_CODE,
  PDR_EDIT_USER_CODE,
  PDR_VIEW_DETAILS_CODE,
  PDR_VIEW_HISTORY_CODE,
  TIT_VIEW_LIST_CODE,
  TIT_INIT_PWD_CODE,
  TIT_ACTIVATE_DEACTIVATE_USER_CODE,
  TIT_ADD_USER_CODE,
  TIT_EDIT_USER_CODE,
  TIT_VIEW_DETAILS_CODE,
  TIT_VIEW_HISTORY_CODE,
  CART_REMOVE_PRODUCT_CODE,
  CART_EMPTY_CODE,
  CMD_VIEW_LIST_CODE,
  CMD_VIEW_DETAILS_CODE,
  CMD_VALIDATE_CODE,
  CAT_VIEW_LIST_CODE,
  CAT_VIEW_DETAILS_CODE,
  CAT_ADD_TO_CART_CODE,
  CAT_LIKE_ART_CODE,
  RELEVE_TEMPERATURE_VIEW_CODE,
  GED_ADD_DOCUMENT,
  GED_VALIDATE_DOCUMENT,
} from './shared/authorization';
import { permitRoles } from './shared/user';
import { User } from './federation';

export class AppAuthorization {
  user: User;
  isSupAdmin: boolean;
  isGpmAdmin: boolean;
  isSupAdminOrIsGpmAdmin: boolean;
  codesTraitements: string[];
  isTitulaire: boolean;


  constructor(user: User) {
    this.user = user;
    this.isSupAdmin = user?.role?.code === SUPER_ADMINISTRATEUR ? true : false;
    this.isGpmAdmin = user?.role?.code === ADMINISTRATEUR_GROUPEMENT ? true : false;
    this.isSupAdminOrIsGpmAdmin = this.isSupAdmin || this.isGpmAdmin;
    this.isTitulaire = user?.role?.code === TITULAIRE_PHARMACIE ? true : false;
    this.codesTraitements = user?.codeTraitements ? (user.codeTraitements as any) : [];
  }

  /**
   * Actualite
   */
  isAuthorizedToViewActu(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(ACTU_VIEW_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToCreateActu(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(ACTU_CREATE_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditActu(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(ACTU_EDIT_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToDeleteActu(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(ACTU_DELETE_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToMarkSeenOrNotSeenActu(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(ACTU_MARK_SEEN_AND_NOT_SEEN_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToLikeActu(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(ACTU_LIKE_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToCommentActu(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(ACTU_COMMENT_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Operation commercial
   */
  isAuthorizedToViewOC(): boolean {
    /*  if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(OC_VIEW_CODE)) {
      return true;
    } */
    return true;
  }
  isAuthorizedToCreateOC(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(OC_CREATE_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditOC(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(OC_EDIT_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToDeleteOC(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(OC_DELETE_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToMarkSeenOrNotSeenOC(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(OC_MARK_SEEN_AND_NOT_SEEN_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToLikeOC(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(OC_LIKE_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToCommentOC(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(OC_COMMENT_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewPilotageBusinessOC(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(OC_VIEW_BUSINESS_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewPilotageFeedbackOC(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(OC_VIEW_FEEDBACK_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Indexing
   */
  isAuthorizedToReIndex(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(INDEXING_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Theme
   */
  isAuthorizedToChangeTheme(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(THEME_CHANGE_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Pilotage
   */
  isAuthorizedToViewPilotageBusiness(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PILOTAGE_BUSINESS_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewPilotageFeedback(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PILOTAGE_FEEDBACK_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewPilotageByOperation(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PILOTAGE_CHANGE_BY_OP_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewPilotageByPharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PILOTAGE_CHANGE_BY_PHARMA_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Promotion
   */
  isAuthorizedToViewPromoList(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PROMO_VIEW_LIST_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToAddPromo(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PROMO_ADD_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditPromo(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PROMO_EDIT_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToDeletePromo(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PROMO_DELETE_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewReleveTemperature(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(RELEVE_TEMPERATURE_VIEW_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Marche
   */
  isAuthorizedToViewMarcheList(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(MARCHE_VIEW_LIST_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToAddMarche(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(MARCHE_ADD_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditMarche(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(MARCHE_EDIT_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToDeleteMarche(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(MARCHE_DELETE_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Article (Produits)
   */
  isAuthorizedToAddProduct(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PRODUCT_ADD_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditProduct(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PRODUCT_EDIT_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToDeleteProduct(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PRODUCT_DELETE_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewProductSaleHistory(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PRODUCT_VIEW_SALE_HISTORY_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Publicite
   */
  isAuthorizedToViewPubList(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PUB_VIEW_LIST_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToAddPub(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PUB_ADD_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditPub(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PUB_EDIT_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToDeletePub(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PUB_DELETE_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Groupement
   */
  isAuthorizedToViewGroupementOptions(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(GPR_VIEW_OPT_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditGroupementOptions(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(GPR_EDIT_OPT_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Partenaire service
   */
  isAuthorizedToViewPartenaireServiceList(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.isTitulaire || this.codesTraitements.includes(SERV_VIEW_LIST_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToInitPasswordPartenaireService(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(SERV_INIT_PWD_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToActivateOrDeactivatePartenaireService(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(SERV_ACTIVATE_DEACTIVATE_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToAddUserPartenaireService(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.isTitulaire || this.codesTraitements.includes(SERV_ADD_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditUserPartenaireService(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.isTitulaire || this.codesTraitements.includes(SERV_EDIT_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewDetailsPartenaireService(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(SERV_VIEW_DETAILS_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewHistoryPartenaireService(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(SERV_VIEW_HISTORY_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Pharmacie
   */
  isAuthorizedToViewPharmacieList(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PHARMA_VIEW_LIST_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditPharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PHARMA_EDIT_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Personnel Pharmacie
   */
  isAuthorizedToViewPersonnelPharmacieList(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PPERSO_VIEW_LIST_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToInitPasswordPersonnelPharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PPERSO_INIT_PWD_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToActivateOrDeactivatePersonnelPharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PPERSO_ACTIVATE_DEACTIVATE_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToAddUserPersonnelPharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PPERSO_ADD_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditUserPersonnelPharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PPERSO_EDIT_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewDetailsPersonnelPharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PPERSO_VIEW_DETAILS_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewHistoryPersonnelPharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PPERSO_VIEW_HISTORY_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Laboratoire partenaire
   */
  isAuthorizedToViewLaboratoirePartenaireList(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(LABO_VIEW_LIST_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToInitPasswordLaboratoirePartenaire(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(LABO_INIT_PWD_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToActivateOrDeactivateLaboratoirePartenaire(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(LABO_ACTIVATE_DEACTIVATE_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToAddUserLaboratoirePartenaire(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(LABO_ADD_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditUserLaboratoirePartenaire(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(LABO_EDIT_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewDetailsLaboratoirePartenaire(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(LABO_VIEW_DETAILS_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewHistoryLaboratoirePartenaire(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(LABO_VIEW_HISTORY_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * President des regions
   */
  isAuthorizedToViewPresidentRegionList(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PDR_VIEW_LIST_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToInitPasswordPresidentRegion(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PDR_INIT_PWD_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToActivateOrDeactivatePresidentRegion(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PDR_ACTIVATE_DEACTIVATE_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToAddUserPresidentRegion(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PDR_ADD_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditUserPresidentRegion(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PDR_EDIT_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewDetailsPresidentRegion(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PDR_VIEW_DETAILS_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewHistoryPresidentRegion(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(PDR_VIEW_HISTORY_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Titulaire pharmacie
   */
  isAuthorizedToViewTitulairePharmacieList(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.isTitulaire || this.codesTraitements.includes(TIT_VIEW_LIST_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToInitPasswordTitulairePharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(TIT_INIT_PWD_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToActivateOrDeactivateTitulairePharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(TIT_ACTIVATE_DEACTIVATE_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToAddUserTitulairePharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(TIT_ADD_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEditUserTitulairePharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(TIT_EDIT_USER_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewDetailsTitulairePharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(TIT_VIEW_DETAILS_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewHistoryTitulairePharmacie(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(TIT_VIEW_HISTORY_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Panier
   */
  isAuthorizedToRemoveProductInCart(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(CART_REMOVE_PRODUCT_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToEmptyCart(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(CART_EMPTY_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Commande
   */
  isAuthorizedToViewListCommande(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(CMD_VIEW_LIST_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewDetailsCommande(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(CMD_VIEW_DETAILS_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToValidateCommande(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(CMD_VALIDATE_CODE)) {
      return true;
    }
    return false;
  }

  /**
   * Catalogue produits
   */
  isAuthorizedToViewListCatalogProduct(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(CAT_VIEW_LIST_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToViewDetailsProductFromCatalogProduct(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(CAT_VIEW_DETAILS_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToAddProductToCart(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(CAT_ADD_TO_CART_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToLikeProduct(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(CAT_LIKE_ART_CODE)) {
      return true;
    }
    return false;
  }

  isAuthorizedToManageChecklist(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.isTitulaire) {
      return true;
    }
    return false;
  }

  onlyGedIsAuthorized(): boolean {
    return permitRoles([PARTENAIRE_SERVICE]);
  }
  /**Ged */
  isAuthorizedToAddGedDocument(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(GED_ADD_DOCUMENT)) {
      return true;
    }
    return false;
  }

  isAuthorizedToValidateGedDocument(): boolean {
    if (this.isSupAdminOrIsGpmAdmin || this.codesTraitements.includes(GED_VALIDATE_DOCUMENT)) {
      return true;
    }
    return false;
  }
}
