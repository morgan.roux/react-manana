import { makeVar } from '@apollo/client';
export const queryStringsVar = makeVar([]);
export const todoBulkCountVar = makeVar<any>({});

// Liste de state open pour les alerts à l'écran de démarrage
// key : nom du state
export const openAlerts = makeVar<Record<string, boolean>>({});
