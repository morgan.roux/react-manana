import { ApolloClient } from '@apollo/client';
import { CSSProperties, ReactNode } from 'react';
import { ApplicationContextOptions } from './context';
import { MeUserInfoFragment, GroupementInfoFragment, Pharmacie } from './federation';

export interface ComputeTotalCountOptions {
  user: MeUserInfoFragment;
  graphql: ApolloClient<any>;
  federation: ApolloClient<any>;
  currentGroupement: GroupementInfoFragment;
  currentPharmacie: Pharmacie;
}

export interface FeatureAccueilOptions {
  totalCount?: (options: ComputeTotalCountOptions) => Promise<number>;
}

export interface FeatureQuickAccessOptions {
  style?: CSSProperties;
}
export interface FeatureFavoriteOptions {
  group?: string;
}
export interface FeatureMobileQuickAccessOptions {
  onClick?: () => void;
  active?: (path: string, context: ApplicationContextOptions) => boolean;
}

export interface OpenAlertOptions {
  user: MeUserInfoFragment;
  graphql: ApolloClient<any>;
  federation: ApolloClient<any>;
  currentGroupement: GroupementInfoFragment;
}

export interface FeatureAlertOptions {
  openAlert?: (options: OpenAlertOptions, onClose: () => void) => ReactNode;
}
