
export const round2Decimals = (num?: number) => {
  return num ? Math.round((num + Number.EPSILON) * 100) / 100 : 0;
};

export const formatMoney = (num?: number | null, defaultReturn: string = '0,00'): string => {
  return num ? round2Decimals(num).toFixed(2).replace('.', ',') : defaultReturn;
};