import SnackVariableInterface from '../Interface/SnackVariableInterface';

export const displaySnackBar = (client: any, data: SnackVariableInterface) => {
  client.writeData({
    data: {
      snackBar: { ...data, __typename: 'SnackBar' },
    },
  });
};
