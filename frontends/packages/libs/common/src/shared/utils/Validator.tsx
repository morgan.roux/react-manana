import { MINYEAR } from '../date';
import moment from 'moment';

const EMAIL_REGEX =
  /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;

export const isEmailValid = (email: string): boolean => {
  if (email) {
    return email !== undefined && EMAIL_REGEX.test(email);
  }
  return true;
};

export const isValidPassword = (password: string): boolean => {
  return /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(password) ? true : false;
};

const testNumber = /^[0-9]+$/;

export const isValidYears = (year: number | string): boolean => {
  const y = parseInt(year.toString(), 10);
  if (y === 0) return false;
  if (y) {
    if (!testNumber.test(y.toString())) {
      return false;
    }

    if (y.toString().length !== 4) {
      return false;
    }

    const currentYear = new Date().getFullYear();
    if (y < MINYEAR || y > currentYear) {
      return false;
    }

    return true;
  }

  return true;
};

const daysInMonth = (month, year) => {
  return moment(`${year}-${month}`, 'YYYY-MM').daysInMonth();
};

export const isValidDays = (days: number | string, month: number, year?: string) => {
  const d = parseInt(days.toString(), 10);
  if (d === 0) return false;
  if (d && month) {
    return month >= 1 && month <= 12 && d > 0 && d <= daysInMonth(month, year);
  }
  return true;
};

export const isUrlValid = (url: string): boolean => {
  const res = url.match(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm);
  return res === null ? false : true;
};
