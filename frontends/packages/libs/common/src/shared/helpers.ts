import axios from 'axios';
import { CSSProperties, MouseEvent } from 'react';
import { isInteger } from 'lodash';
import { Theme } from '@material-ui/core';
import { TypeFichier } from '../graphql';
import moment from 'moment';
import { ComputeUploadedFilenameOptions, UploadedFile, uploadFilename } from '../context/ApplicationContext';
import { ApplicationConfig } from '../config';
import { MeUserInfoFragment } from 'src/federation';

interface ObjectStorageConnectionDetails {
  token: string;
  endpoint: {
    region_id: string;
    url: string;
    region: string;
    interface: string;
    id: string;
  };
  connected_at: string;
}

const getObjectStorageConnectionDetails = async (hostUrl: string): Promise<ObjectStorageConnectionDetails> => {
  return axios
    .get(`${hostUrl}/connection-details`, {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
    .then((response) => {
      return response.data;
    });
};

export const uploadToS3 = (file: File, presignedUrl: string) => {
  // console.log('file : ', file);
  const options = {
    headers: {
      'Content-Type': file.type,
    },
  };

  return axios.put(presignedUrl, file, options);
};

export const getTypeFichier = (file: File): TypeFichier => {
  return file.type.includes('pdf')
    ? TypeFichier.Pdf
    : file.type.includes('xlsx')
    ? TypeFichier.Excel
    : TypeFichier.Photo;
};

export const uploadToStorage = async (
  files: File | File[] = [],
  options: ComputeUploadedFilenameOptions,
  config: ApplicationConfig,
  user: MeUserInfoFragment
): Promise<UploadedFile[]> => {
  const toUploadFiles = Array.isArray(files) ? files : [files];

  if (toUploadFiles.length > 0) {
    if (config.UPLOAD_MODE === 'CLIENT_SIDE') {
      const connectionDetails = await getObjectStorageConnectionDetails(config.WEBHOOK_URL);

      return Promise.all(
        toUploadFiles.map(async (file) => {
          const filePath = uploadFilename(file, options);

          const buffer = await file.arrayBuffer();
          return axios
            .put(
              encodeURI(`${connectionDetails.endpoint.url}/${config.OS_CONTAINER}/${filePath}`),
              new Uint8Array(buffer),
              {
                headers: {
                  'X-Auth-Token': connectionDetails.token,
                  Accept: 'application/json',
                },
              }
            )
            .then(() => ({
              chemin: filePath,
              nomOriginal: file.name,
              //  publicUrl: `${config.STORAGE_PUBLIC_BASE_PATH}/${filePath}`,
              type: getTypeFichier(file),
            }));
        })
      );
    }

    let formData = new FormData();
    formData.append('idUser', user.id);
    formData.append('idGroupement', options.groupement.id);
    formData.append('idPharmacie', options.pharmacie.id);
    if (options.directory) {
      formData.append('directory', options.directory);
    }
    if (options.prefix) {
      formData.append('prefix', options.prefix);
    }

    toUploadFiles.forEach((file) => formData.append('files', file));
    return axios
      .post(`${config.WEBHOOK_URL}/upload`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      })
      .then((response) => {
        return response.data.map(({ chemin, nomOriginal, type }: any) => ({
          chemin,
          nomOriginal,
          type,
        }));
      });
  }

  return [];
};

export const stringToAvatar = (str?: string | null): string => {
  return str
    ? str
        .split(' ')
        .map((item: any) => item.slice(0, 1))
        .join('')
        .toUpperCase()
        .slice(0, 2)
    : '';
};

export const capitalize = (text: string): string => {
  const texLower = text.toLowerCase();
  return texLower.charAt(0).toUpperCase() + texLower.slice(1);
};

export const nl2br = (str: string | null): string | null => {
  return str ? str.replace(new RegExp('\n', 'g'), '<br/>') : str;
};

export const capitalizeFirstLetter = (text: any) => {
  return typeof text === 'number'
    ? text
    : text && text.length > 0
    ? text[0].toUpperCase() + text.slice(1).toLowerCase()
    : '';
};

export const stopEvent = (event: MouseEvent<any>) => {
  event.preventDefault();
  event.stopPropagation();
};

export const formatBytes = (bytes: number, decimals?: number) => {
  if (bytes === 0) return '0 Bytes';
  const k = 1024;
  const dm = decimals || 2;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  const i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
};

export const startTime = (date: any): string | null => {
  if (!date) {
    return null;
  }

  const m = moment(date).utcOffset(0);
  m.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
  return m.toISOString();
};

export const endTime = (date: any): string | null => {
  if (!date) {
    return null;
  }

  const m = moment(date).utcOffset(0);
  m.set({ hour: 23, minute: 59, second: 59, millisecond: 59 });
  return m.toISOString();
};

const STATUTS_CODE_ORDER_MAP: any = {
  NOUVEAU: 1,
  EN_COURS: 2,
  CLOTURE: 3,
};

export const sortStatuts = (statuts: any[]): any[] => {
  if (!statuts) {
    return statuts;
  }

  return statuts.sort((a, b) => STATUTS_CODE_ORDER_MAP[a.code] - STATUTS_CODE_ORDER_MAP[b.code]);
};

export const makeElementsUnique = (elements: any, uniqueKey: any): any => {
  if (!elements || !uniqueKey) {
    return elements;
  }

  return [...(new Set(elements.map((elt: any) => elt[uniqueKey])) as any)].map((elementKey) =>
    elements.find((elt: any) => elt[uniqueKey] === elementKey)
  );
};

export const strippedString = (text: string) => {
  const tmp = document.createElement('DIV');
  tmp.innerHTML = text;
  return tmp.textContent || tmp.innerText || '';

  // return text.replace(/(<([^>]+)>)/gi, '');
};

/**
 *
 * @param str {String}
 */

/**
 *
 * @param num {number}
 */
export const roundNumber = (num: number): number => {
  return isInteger(num) ? num : parseFloat(parseFloat(num.toString()).toFixed(2));
};

/**
 *
 * @param str {string}
 * @param length {number}
 */
export const trimmedString = (str: string, length: number): string => {
  return str.length > length ? `${str.substring(0, length)}...` : str;
};

export const getMonthNumber = (month?: string): number => {
  switch (month) {
    case 'Janvier':
      return 1;
    case 'Février':
      return 2;
    case 'Mars':
      return 3;
    case 'Avril':
      return 4;
    case 'Mai':
      return 5;
    case 'Juin':
      return 6;
    case 'Juillet':
      return 7;
    case 'Août':
      return 8;
    case 'Septembre':
      return 9;
    case 'Octobre':
      return 10;
    case 'Novembre':
      return 11;
    default:
      return 12;
  }
};

export const getMonthName = (month?: number): string => {
  switch (month) {
    case 1:
      return 'Janvier';
    case 2:
      return 'Février';
    case 3:
      return 'Mars';
    case 4:
      return 'Avril';
    case 5:
      return 'Mai';
    case 6:
      return 'Juin';
    case 7:
      return 'Juillet';
    case 8:
      return 'Août';
    case 9:
      return 'Septembre';
    case 10:
      return 'Octobre';
    case 11:
      return 'Novembre';
    default:
      return 'Décembre';
  }
};

export const getMonths = () => [
  'Janvier',
  'Février',
  'Mars',
  'Avril',
  'Mai',
  'Juin',
  'Juillet',
  'Août',
  'Septembre',
  'Octobre',
  'Novembre',
  'Décembre',
];

export const isInvalidArray = (array: any[]): boolean => {
  return array.some((el: any) => {
    return el === null || el === undefined;
  });
};

export const formatIBAN = (iban: string): string => {
  const parts: string[] = iban.split('.');
  const firstPart: string = parts[0]
    .toString()
    .replace(/[^\dA-Z]/g, '')
    .replace(/(.{4})/g, '$1 ')
    .trim();
  return firstPart + (parts.length > 1 ? '.' + parts[1] : '');
};

export const years = (startYear: number): number[] => {
  const currentYear = new Date().getFullYear();
  const years: number[] = [];
  while (startYear <= currentYear) {
    years.push(startYear++);
  }
  return years;
};

export const normalizeString = (str: string) => {
  return str
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/[^a-zA-Z]/g, '');
};

export const isMobile = () => {
  return window.innerWidth < 960;
};

export const isMd = () => {
  return window.innerWidth >= 960 && window.innerWidth < 1280;
};

export const isLg = () => {
  return window.innerWidth >= 1280 && window.innerWidth < 1920;
};

export const isSm = () => {
  return window.innerWidth >= 600 && window.innerWidth < 960;
};

export const strippedColumnStyle: CSSProperties = {
  whiteSpace: 'nowrap',
  maxWidth: 200,
  overflow: 'hidden',
  textOverflow: 'ellipsis',
};

export const px2rem = (px: number, theme?: Theme) => {
  return theme?.typography?.pxToRem ? theme.typography.pxToRem(px) : `${px / 16}rem`;
};
