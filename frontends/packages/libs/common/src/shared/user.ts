import { Sexe } from '../graphql';
import { useApplicationContext } from '../context';

export const permitRoles = (roles: string[]) => {
  const { user } = useApplicationContext();
  if (user.role?.code) {
    if (roles.includes(user.role.code)) return true;
  }
  return false;
};

export const CIVILITE_LIST = [
  { id: 'M.', code: 'MONSIEUR', value: 'Monsieur', libelle: 'Monsieur' },
  { id: 'Mme', code: 'Madame', value: 'Madame', libelle: 'Madame' },
  { id: 'Mlle', code: 'MADEMOISELLE', value: 'Mademoiselle', libelle: 'Mademoiselle' },
];

export const SEXE_LIST = [
  { id: Sexe.M, value: 'Homme' },
  { id: Sexe.F, value: 'Femme' },
];
