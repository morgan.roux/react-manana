import XLSX from 'xlsx';
interface IExportExcel {
  data: any[];
  title: string;
  filename: string;
}
export const exportExcel = ({ data, title, filename }: IExportExcel) => {
  const ws = XLSX.utils.aoa_to_sheet(data);
  const wb = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(wb, ws, title);
  XLSX.writeFile(wb, `${filename}.xlsx`);
};
