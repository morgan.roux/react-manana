// const removeNull = (array: any[]) => {
//   return array.filter(item => item !== null);
// };

import {
  ADMINISTRATEUR_GROUPEMENT,
  AMBASSADEUR,
  SERVICE_COMMUNICATION,
  SERVICE_MARKETING,
  SERVICE_SALES,
  SUPER_ADMINISTRATEUR,
} from '@lib/common';
import { TypePresidentCible } from '../graphql';

export const adminFilter = (idGroupement: string) => {
  const must: any[] = [{ term: { isRemoved: false } }, { term: { idGroupement } }];
  return {
    must,
  };
};

export const userPersonnelFilter = (currentUser: any) => {
  return [
    {
      terms: {
        'servicesCible.id': [
          currentUser &&
            currentUser.userPersonnel &&
            currentUser.userPersonnel.service &&
            currentUser.userPersonnel.service.id,
        ],
      },
    },
  ];
};

export const userPpersonnelFilter = (currentUser: any) => {
  let ppersonnelTerms = {
    terms: {
      'pharmaciesRolesCible.code': [
        currentUser &&
          currentUser.userPpersonnel &&
          currentUser.userPpersonnel.role &&
          currentUser.userPpersonnel.role.code,
      ],
    },
  };

  if (currentUser && currentUser.userPpersonnel && currentUser.userPpersonnel.estAmbassadrice) {
    ppersonnelTerms = {
      terms: {
        'pharmaciesRolesCible.code': [AMBASSADEUR],
      },
    };
  }

  return [
    {
      bool: {
        must: [
          ppersonnelTerms,
          {
            terms: {
              'pharmaciesCible.id': [
                currentUser &&
                  currentUser.userPpersonnel &&
                  currentUser.userPpersonnel.pharmacie &&
                  currentUser.userPpersonnel.pharmacie.id,
              ],
            },
          },
        ],
      },
    },
  ];
};

export const userLaboratoireFilter = (currentUser: any) => {
  return [
    {
      terms: {
        'laboratoiresCible.id': [currentUser && currentUser.userLaboratoire && currentUser.userLaboratoire.id],
      },
    },
  ];
};

export const userPartenaireFilter = (currentUser: any) => {
  return [
    {
      terms: {
        'partenairesCible.id': [currentUser && currentUser.userPartenaire && currentUser.userPartenaire.id],
      },
    },
  ];
};

export const userTitulaireFilter = (currentUser: any): any[] => {
  let filter: any[] = [
    {
      bool: {
        must: [
          {
            terms: {
              'pharmaciesRolesCible.code': [currentUser && currentUser.role && currentUser.role.code],
            },
          },
          {
            terms: {
              'pharmaciesCible.id': [
                currentUser &&
                  currentUser.userTitulaire &&
                  currentUser.userTitulaire.titulaire &&
                  currentUser.userTitulaire.titulaire.pharmacies &&
                  currentUser.userTitulaire.titulaire.pharmacies.map((pharma: any) => pharma && pharma.id),
              ].flat(),
            },
          },
        ],
      },
    },
  ];

  if (currentUser && currentUser.userTitulaire && currentUser.userTitulaire.isPresident) {
    const titulairePresidentFilter: any[] = [
      {
        terms: {
          'presidentsCibles.id': [
            currentUser &&
              currentUser.userTitulaire &&
              currentUser.userTitulaire.titulaire &&
              currentUser.userTitulaire.titulaire.id,
          ],
        },
      },
      { term: { typePresidentCible: TypePresidentCible.Pharmacie } },
      { term: { typePresidentCible: TypePresidentCible.President } },
    ];
    filter = [...filter, ...titulairePresidentFilter];
  } else {
    const titulaireFilter: any[] = [
      {
        terms: {
          'presidentsCibles.pharmacies.id': [
            currentUser &&
              currentUser.userTitulaire &&
              currentUser.userTitulaire.titulaire &&
              currentUser.userTitulaire.titulaire.pharmacieUser &&
              currentUser.userTitulaire.titulaire.pharmacieUser.id,
          ],
        },
      },
      { term: { typePresidentCible: TypePresidentCible.Pharmacie } },
    ];
    filter = [...filter, ...titulaireFilter];
  }

  return filter;
};

export const noAdminFilter = (currentUser: any, idGroupement: string) => {
  let moreShould: any[] = [];
  const moreMust: any[] = [];

  if (currentUser) {
    if (currentUser.userPersonnel) {
      moreShould = userPersonnelFilter(currentUser);
    }

    if (currentUser.userPpersonnel) {
      moreShould = userPpersonnelFilter(currentUser);
    }

    if (currentUser.userLaboratoire) {
      moreShould = userLaboratoireFilter(currentUser);
    }

    if (currentUser.userPartenaire) {
      moreShould = userPartenaireFilter(currentUser);
    }

    if (currentUser.userTitulaire) {
      moreShould = userTitulaireFilter(currentUser);
    }
  }

  const must = [
    { term: { isRemoved: false } },
    { term: { idGroupement } },
    // { range: { dateDebut: { lte: 'now/d' } } },
    // { range: { dateFin: { gte: 'now/d' } } },
    ...moreMust,
  ];

  return {
    must,
    should: [{ term: { 'actualiteCible.globalite': true } }, ...moreShould],
    minimum_should_match: 1,
  };
};

export const actualiteFilter = (currentUser: any, idGroupement: string) => {
  return currentUser &&
    currentUser.role &&
    (currentUser.role.code === SUPER_ADMINISTRATEUR || currentUser.role.code === ADMINISTRATEUR_GROUPEMENT)
    ? adminFilter(idGroupement)
    : noAdminFilter(currentUser, idGroupement);
};

export const userIsAuthorized = (currentUser: any): boolean => {
  if (
    currentUser &&
    currentUser.role &&
    (currentUser.role.code === SUPER_ADMINISTRATEUR || currentUser.role.code === ADMINISTRATEUR_GROUPEMENT)
  ) {
    return true;
  }

  if (currentUser && currentUser.role && currentUser.userPersonnel && currentUser.userPersonnel.service) {
    const serviceCode = currentUser.userPersonnel.service.code;
    if (
      serviceCode &&
      (serviceCode === SERVICE_COMMUNICATION || serviceCode === SERVICE_MARKETING || serviceCode === SERVICE_SALES)
    ) {
      return true;
    }
  }

  return false;
};
