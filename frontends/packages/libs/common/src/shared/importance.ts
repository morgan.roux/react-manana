import { CSSProperties } from 'react';
import { ImportanceInfoFragment, StatutInfoFragment } from '../federation';

export const importanceToImpact = (importance?: ImportanceInfoFragment): ImportanceInfoFragment | undefined => {
  return importance
    ? ({
        ...importance,
        libelle: `${importance.ordre}` === `1` ? 'Fort' : 'Faible',
        background: importance?.couleur,
        color: importance?.ordre === 1 ? '#fff' : '#000',
      } as any)
    : undefined;
};

export const importancesToImpacts = (importances?: ImportanceInfoFragment[]): ImportanceInfoFragment[] => {
  return importances ? importances.map((importance) => importanceToImpact(importance) as any) : [];
};

export const DEFAULT_STATUS_STYLE: Record<'NOUVEAU' | 'EN_COURS' | 'CLOTURE', CSSProperties> = {
  NOUVEAU: {
    color: '#000',
    background: '#e1e1e2',
  },
  EN_COURS: {
    background: '#d8eef7',
    color: '#63b8dd',
  },
  CLOTURE: {
    background: '#dbf3e5',
    color: '#3aba6d',
  },
};

export const statutWithStyle = (statut?: StatutInfoFragment): StatutInfoFragment | undefined => {
  return statut
    ? ({
        ...statut,
        ...((DEFAULT_STATUS_STYLE as any)[statut.code as any] || {}),
      } as any)
    : undefined;
};

export const statutsWithStyles = (statuts?: StatutInfoFragment[]): StatutInfoFragment[] => {
  return statuts ? statuts.map((statut) => statutWithStyle(statut) as any) : [];
};
