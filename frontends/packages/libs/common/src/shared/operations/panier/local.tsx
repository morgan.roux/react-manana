import gql from 'graphql-tag';
export const GET_CURRENT_PANIER = gql`
  {
    panier @client {
      id
      pharmacie {
        cip
        nom
        adresse1
        id
        type
      }
      owner {
        id
        userName
      }
      nbrRef
      quantite
      uniteGratuite
      prixBaseTotalHT
      valeurRemiseTotal
      prixNetTotalHT
      remiseGlobale
      fraisPort
      valeurFrancoPort
      dateCreation
      dateModification
      panierLignes {
        id
        article {
          type
          id
          nomCanalArticle
          codeReference
          qteStock
          qteMin
          stv
          unitePetitCond
          uniteSupCond
          prixFab
          prixPhv
          prixTtc
          dateCreation
          dateModification
          dateRetrait
          isActive
          laboratoire {
            id
            nomLabo
          }
          inMyPanier
          inOtherPanier
          articlePhoto {
            id
            fichier {
              id
              chemin
              nomOriginal
              type
              urlPresigned
            }
          }
          remises {
            id
            quantiteMin
            quantiteMax
            pourcentageRemise
            nombreUg
          }
          comments {
            total
            data {
              id
              item {
                id
                code
                name
                dateCreation
                dateModification
              }
              idSource
              user {
                userName
              }
              content
              isRemoved
              dateCreation
              dateModification
            }
          }
          famille {
            type
            id
            codeFamille
            nomFamille
            majFamille
            codeMajFamille
            countCanalArticles
          }
        }
        optimiser
        quantite
        uniteGratuite
        prixBaseHT
        remiseLigne
        remiseGamme
        prixNetUnitaireHT
        prixTotalHT
        dateCreation
        dateModification
      }
    }
  }
`;

export const GET_LOCAL_PANIER = gql`
  {
    localPanier @client {
      panierLignes {
        produitCanal {
          id
          qteStock
          stv
          prixPhv
          produit {
            id
            libelle
            produitPhoto {
              id
              fichier {
                id
                publicUrl
                nomOriginal
              }
            }
            produitCode {
              id
              code
              typeCode
              referent
            }
            produitTechReg {
              id
              laboExploitant {
                id
                nomLabo
                sortie
              }
            }
          }
          articleSamePanachees {
            id
          }
          commandeCanal {
            id
            code
          }
          remises {
            id
            quantiteMin
            quantiteMax
            pourcentageRemise
            nombreUg
          }
        }
        quantite
      }
    }
  }
`;
