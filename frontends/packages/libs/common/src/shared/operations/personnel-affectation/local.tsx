import gql from 'graphql-tag';

export const GET_CHECKEDS_PERSONNEL_AFFECTATION = gql`
  {
    checkedsPersonnelAffecation @client {
      id
    }
  }
`;
