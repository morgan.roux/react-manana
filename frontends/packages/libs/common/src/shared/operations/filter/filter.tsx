import { QueryResult, useApolloClient, useQuery, useReactiveVar } from '@apollo/client';
import {
  GET_ID_CHECKEDS,
  GET_LOCAL_FILTERS,
  GET_LOCAL_SEARCH,
  GET_LOCAL_SORT,
  GET_MY_PHARMACIE_FILTER,
  GET_PAGINATION,
} from './local';
import { queryStringsVar } from '../../../reactiveVars';

export interface FiltersInterface {
  filters: {
    idCheckeds: string[];
    idLaboratoires: number[];
    idFamilles: number[];
    idGammesCommercials: number[];
    idCommandeCanal: string;
    idCommandeCanals: string[];
    idCommandeTypes: number[];
    idActualiteOrigine: string[];
    idRemise: number[];
    idOcCommande: number[];
    idSeen: number[];
    idTodoType: number[];
    sortie: number[];
    laboType: number[];
    reaction: number[];
  };
}

interface MyPharmacieFilterInterface {
  myPharmacieFilter: boolean;
}

export interface IdCheckedsInterface {
  idCheckeds: string[];
}
export interface SortInterface {
  sort: {
    sortItem: {
      label: string;
      name: string;
      direction: string;
      active?: boolean;
      __typename: string;
    };
  };
}

export interface FrontFiltersInterface {
  frontFilters: {
    occommande: number[];
    seen: string;
    datefilter: { dateDebut: any; dateFin: any };
  };
}

export interface SearchInterface {
  searchText: {
    text: string;
  };
}

export interface PaginationInterface {
  pagination: {
    skip: number;
    take: number;
  };
}

export interface CodeCanalActiveInterface {
  codeCanalActive: string;
}

interface useFiltersResult {
  sortItem: QueryResult<SortInterface>;
  idCheckedsQuery: QueryResult<IdCheckedsInterface>;
  myPharmacieFilter: QueryResult<MyPharmacieFilterInterface>;
  filters: QueryResult<FiltersInterface>;
  searchText: QueryResult<SearchInterface>;
  pagination: QueryResult<PaginationInterface>;
  queryStrings: any;
  setPagination: (pagination: any) => void;
  setSort: (sortItem: any) => void;
  setFilters: (filters: any) => void;
  setSearchText: (searchText: any) => void;
  setMyPharmacieFilter: (myPharmacieFilter: boolean) => void;
  setQueryStrings: (value: any) => void;
  resetSearchFilters: () => void;
}

export const useFilters = (): useFiltersResult => {
  const client = useApolloClient();

  const queryStrings = useReactiveVar(queryStringsVar);

  const sortItem = useQuery<SortInterface>(GET_LOCAL_SORT, { client });

  const idCheckedsQuery = useQuery<IdCheckedsInterface>(GET_ID_CHECKEDS, { client });

  const myPharmacieFilter = useQuery<MyPharmacieFilterInterface>(GET_MY_PHARMACIE_FILTER, { client });

  const filters = useQuery<FiltersInterface>(GET_LOCAL_FILTERS, { client });

  const searchText = useQuery<SearchInterface>(GET_LOCAL_SEARCH, { client });

  const pagination = useQuery<PaginationInterface>(GET_PAGINATION, { client });

  const setQueryStrings = (value: any) => {
    queryStringsVar(value);
  };

  const setPagination = (pagination: any) => {
    client.writeQuery({
      query: GET_PAGINATION,
      data: {
        pagination,
      },
    });
  };

  const setSort = (sortItem: any) => {
    client.writeQuery({
      query: GET_LOCAL_SORT,
      data: {
        sort: { sortItem },
      },
    });
  };

  const setFilters = (filters: any) => {
    client.writeQuery({
      query: GET_LOCAL_FILTERS,
      data: {
        filters,
        __typename: 'local',
      },
    });
  };

  const setSearchText = (searchText: any) => {
    client.writeQuery({
      query: GET_LOCAL_SEARCH,
      data: {
        searchText: {
          text: searchText,
        },
      },
    });
  };

  const setMyPharmacieFilter = (myPharmacieFilter: boolean) => {
    client.writeQuery({
      query: GET_MY_PHARMACIE_FILTER,
      data: {
        myPharmacieFilter,
      },
    });
  };

  const resetSearchFilters = () => {
    setQueryStrings([]);
    setSearchText('');
    setPagination({ take: 12, skip: 0 });
  };

  return {
    sortItem,
    idCheckedsQuery,
    myPharmacieFilter,
    filters,
    searchText,
    pagination,
    queryStrings,
    setMyPharmacieFilter,
    setPagination,
    setSort,
    setFilters,
    setSearchText,
    setQueryStrings,
    resetSearchFilters,
  };
};
