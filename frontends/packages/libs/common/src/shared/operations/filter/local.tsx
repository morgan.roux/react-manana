import gql from 'graphql-tag';

export const GET_STATUS_FILTER = gql`
  {
    statusFilter @client
  }
`;

export const GET_FILTER_DATE = gql`
  {
    dateFilter @client {
      startDate
      endDate
    }
  }
`;

export const GET_ID_CHECKEDS = gql`
  {
    idCheckeds @client
  }
`;

export const GET_MY_PHARMACIE_FILTER = gql`
  {
    myPharmacieFilter @client
  }
`;

export const GET_LOCAL_FILTERS = gql`
  {
    filters @client {
      idCheckeds
      idLaboratoires
      idFamilles
      idGammesCommercials
      idCommandeCanal
      idCommandeCanals
      idCommandeTypes
      idActualiteOrigine
      idRemise
      idOcCommande
      idSeen
      idTodoType
      sortie
      laboType
      reaction
    }
  }
`;

export const GET_VIEW_MODE = gql`
  {
    viewMode @client
  }
`;

export const GET_PAGINATION = gql`
  {
    pagination @client {
      skip
      take
    }
  }
`;

export const NULL_QUERY = gql`
  {
    nullQuery @client
  }
`;

export const GET_LOCAL_SEARCH = gql`
  {
    searchText @client {
      text
    }
  }
`;

export const GET_LOCAL_SORT = gql`
  {
    sort @client {
      sortItem {
        label
        name
        direction
        active
      }
    }
  }
`;

export const GET_CODE_CANA_ACTIVE = gql`
  {
    codeCanalActive @client
  }
`;
