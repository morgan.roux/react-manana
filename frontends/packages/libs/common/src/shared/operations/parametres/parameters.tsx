import { DO_GET_PARAMETER_LOCAL } from './_parametre';
import { ApolloClient, useApolloClient, useQuery } from '@apollo/client';
import { ParameterInfoFragment } from '../../../graphql';
import {
  findParameter,
  findParameterValue,
  isParameterValueEnabled,
  parseParameterValueAsBoolean,
  parseParameterValueAsDate,
  parseParameterValueAsMoney,
  parseParameterValueAsNumber,
  parseParameterValueAsString,
} from './util';
/*
interface Parameter {
  id: string;
  code: string;
  name: string;
  defaultValue: string;
  dateCreation: Date;
  dateModification: Date;
  category: string;
  options: {
    label: string;
    value: string;
  };
  parameterGroupe: {
    id: string;
    code: string;
    nom: string;
  };
  parameterType: {
    id: string;
    code: string;
    nom: string;
  };
  value: {
    id: string;
    value: string;
  };
}*/

export const useParametersWithProvidedClient = (
  client: ApolloClient<any>
): [ParameterInfoFragment[], (params: ParameterInfoFragment[]) => void] => {
  const parameters = useQuery(DO_GET_PARAMETER_LOCAL, { client });

  const setParameters = (newParameters: ParameterInfoFragment[]) => {
    client.writeQuery({
      query: DO_GET_PARAMETER_LOCAL,
      data: {
        parameters: newParameters,
      },
    });
  };

  return [parameters?.data?.parameters || [], setParameters];
};

export const useParameters = (
  providedClient?: ApolloClient<any>
): [ParameterInfoFragment[], (params: ParameterInfoFragment[]) => void] => {
  const contextClient = useApolloClient();
  const client = providedClient ?? contextClient;

  return useParametersWithProvidedClient(client);
};

export interface ParameterValue {
  id: string;
  value: string;
}

export const useParameter = (code: string) => {
  const [getparameter] = useParameters();
  if (getparameter) {
    const result = findParameter(getparameter, code);
    if (result) return result;
  }
  return null;
};

export const useValueParameter = (code: string) => {
  const [getparameter] = useParameters();
  if (getparameter) {
    return findParameterValue(getparameter, code);
  }

  return null;
};

export const useValueParameterAsBoolean = (code: string) => {
  const [parameters] = useParameters();
  return parseParameterValueAsBoolean(parameters, code);
};

export const useValueParameterByView = (codeGroupement: string, codePharmacie: string, isMobile: boolean) => {
  const [parameters] = useParameters();
  return isParameterValueEnabled(parameters, codeGroupement, codePharmacie, isMobile);
};

export const useValueParameterAsMoney = (code: string) => {
  const [parameters] = useParameters();
  return parseParameterValueAsMoney(parameters, code);
};

export const useValueParameterAsString = (code: string) => {
  const [parameters] = useParameters();
  return parseParameterValueAsString(parameters, code);
};

export const useValueParameterAsNumber = (code: string) => {
  const [parameters] = useParameters();
  return parseParameterValueAsNumber(parameters, code);
};

export const useValueParameterAsDate = (code: string) => {
  const [parameters] = useParameters();
  return parseParameterValueAsDate(parameters, code);
};
