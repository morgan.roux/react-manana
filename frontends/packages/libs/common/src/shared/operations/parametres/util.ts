import { ParameterInfoFragment } from '../../../graphql';

export const findParameter = (parameters: ParameterInfoFragment[], code: string): ParameterInfoFragment | undefined => {
  return parameters.find((parameter: ParameterInfoFragment) => parameter.code === code);
};

export const findParameterValue = (parameters: ParameterInfoFragment[], code: string): string | null | undefined => {
  const result = findParameter(parameters, code);
  if (result) {
    return result?.value?.value ? result.value.value : result.defaultValue;
  }

  return null;
};

export const parseParameterValueAsBoolean = (parameters: ParameterInfoFragment[], code: string): boolean => {
  try {
    return 'true' === findParameterValue(parameters, code);
  } catch (e) {
    return false;
  }
};

export const isParameterValueEnabled = (
  parameters: ParameterInfoFragment[],
  codeGroupement: string,
  codePharmacie: string,
  isMobile: boolean
): boolean => {
  const valueGroupement = findParameterValue(parameters, codeGroupement);
  let valuePharmacie = findParameterValue(parameters, codePharmacie);

  try {
    if (valuePharmacie === 'disabled' || valueGroupement === 'disabled') {
      return false;
    } else if (valueGroupement === 'all') {
      return !((valuePharmacie === 'mobile' && !isMobile) || (valuePharmacie === 'web' && isMobile));
    }

    return (valueGroupement === 'mobile' && isMobile) || (valueGroupement === 'web' && !isMobile);
  } catch (e) {}
  return false;
};

export const parseParameterValueAsMoney = (parameters: ParameterInfoFragment[], code: string) => {
  try {
    const value = findParameterValue(parameters, code);
    return value ? parseFloat(value).toFixed(2) : 0;
  } catch (e) {
    return 0;
  }
};

export const parseParameterValueAsString = (parameters: ParameterInfoFragment[], code: string): string => {
  try {
    return findParameterValue(parameters, code) as string;
  } catch (e) {
    return '';
  }
};

export const parseParameterValueAsNumber = (parameters: ParameterInfoFragment[], code: string): number => {
  try {
    const value = findParameterValue(parameters, code);
    return value ? Number(value) * 1 : 0;
  } catch (e) {
    return 0;
  }
};

export const parseParameterValueAsDate = (parameters: ParameterInfoFragment[], code: string): string | null => {
  try {
    const value = findParameterValue(parameters, code);
    return value ? new Date(value).toDateString() : null;
  } catch (e) {
    return null;
  }
};
