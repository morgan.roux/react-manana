import { useApolloClient, useQuery } from '@apollo/client';
import { ProduitCanal } from 'src/graphql';
import { GET_ARTICLE_OC_ARRAY, GET_OPERATION_PANIER, GET_SELECTED_OC } from './local';

export interface OCInterface {
  selectedOC: any;
}

export interface ArticleOCInterface extends ProduitCanal {
  quantite: number;
}

export interface ArticleOCArray {
  articleOcArray: ArticleOCInterface[];
}

export const useLocalSelectedOC = (): [any, (selectedOC: any) => void] => {
  const client = useApolloClient();

  const selected = useQuery<OCInterface>(GET_SELECTED_OC, { client });

  const set = (selectedOC: any) => {
    client.writeQuery({
      query: GET_SELECTED_OC,
      data: { selectedOC },
    });
  };

  return [selected.data?.selectedOC || [], set];
};

export const useLocalArticleOCArray = (): [any, (articleOCArray: any) => void] => {
  const client = useApolloClient();

  const articles = useQuery<ArticleOCArray>(GET_ARTICLE_OC_ARRAY, { client });

  const set = (articleOCArray: any) => {
    client.writeQuery({
      query: GET_ARTICLE_OC_ARRAY,
      data: { articleOCArray },
    });
  };

  return [articles.data?.articleOcArray || [], set];
};

export const useLocalOperationPanier = (): [any, (operationPanier: any) => void] => {
  const client = useApolloClient();

  const panier = useQuery(GET_OPERATION_PANIER, { client });

  const set = (operationPanier: any) => {
    client.writeQuery({
      query: GET_OPERATION_PANIER,
      data: { operationPanier },
    });
  };

  return [panier.data?.operationPanier || [], set];
};
