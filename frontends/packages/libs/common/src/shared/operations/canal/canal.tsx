import { GET_CODE_CANAL } from './_canal';
import { ApolloClient, useApolloClient, useQuery } from '@apollo/client';

interface CanalCode {
  id: string;
  code: string;
}
export const useCanalWithProvidedClient = (client: ApolloClient<any>): [CanalCode, (canalCode: CanalCode) => void] => {
  const canal = useQuery(GET_CODE_CANAL, { client });

  const set = (CurrentCanalCode: CanalCode) => {
    client.writeQuery({
      query: GET_CODE_CANAL,
      data: { CurrentCanalCode },
    });
  };

  return [canal.data?.CurrentCanalCode, set];
};

export const useCanal = (providedClient?: ApolloClient<any>): [CanalCode, (canalCode: CanalCode) => void] => {
  const contextClient = useApolloClient();
  const client = providedClient ?? contextClient;

  return useCanalWithProvidedClient(client);
};
