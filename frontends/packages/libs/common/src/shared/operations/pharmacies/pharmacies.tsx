import { GET_CURRENT_PHARMACIE, GET_PHARMACIES } from './_pharmacies';
import { ApolloClient, useApolloClient, useQuery } from '@apollo/client';
import { Pharmacie } from '../../../federation';
import { GET_MY_PHARMACIE_FILTER } from './local';
export interface MyPharmacieFilterInterface {
  myPharmacieFilter: boolean;
}

export const usePharmaciesWithProvidedClient = (
  client: ApolloClient<any>
): [Pharmacie[], (pharmacies: Pharmacie[]) => void] => {
  const canal = useQuery(GET_PHARMACIES, { client });

  const set = (pharmacies: Pharmacie[]) => {
    client.writeQuery({
      query: GET_PHARMACIES,
      data: { pharmacies },
    });
  };

  return [canal.data?.pharmacies, set];
};

export const usePharmacies = (providedClient?: ApolloClient<any>): [Pharmacie[], (pharmacies: Pharmacie[]) => void] => {
  const contextClient = useApolloClient();
  const client = providedClient ?? contextClient;

  return usePharmaciesWithProvidedClient(client);
};

export const useCurrentPharmacieWithProvidedClient = (
  client: ApolloClient<any>
): [Pharmacie, (pharmacie: Pharmacie) => void] => {
  const canal = useQuery(GET_CURRENT_PHARMACIE, { client });

  const set = (pharmacie: Pharmacie) => {
    client.writeQuery({
      query: GET_CURRENT_PHARMACIE,
      data: { pharmacie },
    });
  };

  return [canal.data?.pharmacie, set];
};

export const useCurrentPharmacie = (
  providedClient?: ApolloClient<any>
): [Pharmacie, (pharmacie: Pharmacie) => void] => {
  const contextClient = useApolloClient();
  const client = providedClient ?? contextClient;

  return useCurrentPharmacieWithProvidedClient(client);
};

export const useLocalMyPharmacieFilter = (): [any, (myPharmacieFilter: any) => void] => {
  const client = useApolloClient();
  const filter = useQuery<MyPharmacieFilterInterface>(GET_MY_PHARMACIE_FILTER, { client });

  const set = (myPharmacieFilter: any) => {
    client.writeQuery({
      query: GET_MY_PHARMACIE_FILTER,
      data: { myPharmacieFilter },
    });
  };

  return [filter.data?.myPharmacieFilter, set];
};
