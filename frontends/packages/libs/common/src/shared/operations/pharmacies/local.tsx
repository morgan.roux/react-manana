import gql from 'graphql-tag';

export const GET_CHECKEDS_PHARMACIE = gql`
  {
    checkedsPharmacie @client {
      id
    }
  }
`;

export const GET_MY_PHARMACIE_FILTER = gql`
  {
    myPharmacieFilter @client
  }
`;
