import { useApolloClient, useQuery } from '@apollo/client';
import { GET_CHECKEDS_PRODUIT } from './local';

export const useLocalCheckedsProduitCanal = (): [any, (checkedsProduit: any) => void] => {
  const client = useApolloClient();

  const checkeds = useQuery(GET_CHECKEDS_PRODUIT, { client });

  const set = (checkedsProduit: any) => {
    client.writeQuery({
      query: GET_CHECKEDS_PRODUIT,
      data: { checkedsProduit },
    });
  };

  return [checkeds.data?.checkedsProduit || [], set];
};
