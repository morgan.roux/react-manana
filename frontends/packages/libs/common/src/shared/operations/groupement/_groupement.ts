import gql from 'graphql-tag';

export const GET_CURRENT_GROUPEMENT = gql`
  {
    groupement @client {
      id
      nom
      adresse1
      adresse2
      cp
      ville
      pays
      telBureau
      telMobile
      email
      site
      commentaire
      nomResponsable
      prenomResponsable
      sortie
      dateSortie
      createdAt
      updatedAt
      logo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }
  }
`;
