import { GET_CURRENT_GROUPEMENT } from './_groupement';
import { ApolloClient, useApolloClient, useQuery } from '@apollo/client';
import { Groupement } from '../../../federation';

export const useCurrentGroupementWithProvidedClient = (
  client: ApolloClient<any>
): [Groupement, (groupement: Groupement) => void] => {
  const currentGroupement = useQuery(GET_CURRENT_GROUPEMENT, { client });

  const set = (groupement: Groupement) => {
    client.writeQuery({
      query: GET_CURRENT_GROUPEMENT,
      data: { groupement },
    });
  };

  return [currentGroupement.data?.groupement, set];
};

export const useCurrentGroupement = (
  providedClient?: ApolloClient<any>
): [Groupement, (groupement: Groupement) => void] => {
  const contextClient = useApolloClient();
  const client = providedClient ?? contextClient;

  return useCurrentGroupementWithProvidedClient(client);
};
