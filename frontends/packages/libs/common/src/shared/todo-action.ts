export const isTraitementAutomatiqueAction = (action: any): boolean => {
  return action?.item?.code === 'TA_EXECUTION';
};

const REUNION_QUALITE_ACTION_CODES = ['DQ_AMELIORATION', 'DQ_INCIDENT', 'DQ_ACTION_OPERATIONNELLE'];
export const isReunionQualiteAction = (action: any): boolean => {
  return action?.item?.code && REUNION_QUALITE_ACTION_CODES.includes(action.item.code);
};

export const isTradeMarketingAction = (action: any) => {
  return action?.item?.code === 'OPERATION_MARKETING';
};

export const isManuelAction = (action: any) => {
  return !isTraitementAutomatiqueAction(action) && !isReunionQualiteAction(action) && !isTradeMarketingAction(action);
};
