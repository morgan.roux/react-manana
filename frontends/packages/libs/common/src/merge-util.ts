import { InMemoryCacheConfig } from '@apollo/client';
import { merge } from 'lodash';
import { ModuleDefinition } from './module';

export const mergeCacheConfigs = (
  cacheConfigs: (InMemoryCacheConfig | undefined)[],
  initialValue = {}
): InMemoryCacheConfig => {
  return (cacheConfigs || []).reduce((config, currentConfig) => {
    return currentConfig ? merge(config, currentConfig) : config;
  }, initialValue);
};

export const mergeModules = (...modules: ModuleDefinition[]): ModuleDefinition => {
  return modules.reduce((merged, currentModule) => {
    return {
      features: [...(merged.features || []), ...(currentModule.features || [])],
      routes: [...(merged.routes || []), ...(currentModule.routes || [])],
      cacheConfig: {
        federation: mergeCacheConfigs([merged.cacheConfig?.federation, currentModule.cacheConfig?.federation]),
        graphql: mergeCacheConfigs([merged.cacheConfig?.graphql, currentModule.cacheConfig?.graphql]),
      },
    };
  }, {});
};
