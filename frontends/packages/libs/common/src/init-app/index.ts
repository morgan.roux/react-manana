import { useEffect, useState } from 'react';
import { ApolloClient } from '@apollo/client';
import {
  setIp,
  setGroupement,
  setPharmacie,
  setUser,
  getAccessToken,
  isChooseGroupementRequested,
  getGroupement,
  getPharmacie,
} from '../auth/auth-util';
import { getIp as detectIp } from '../ip-detector';
import { ParamCategory, ParameterInfoFragment, useParameters_By_CategoriesLazyQuery } from './../graphql';
import { SUPER_ADMINISTRATEUR } from './../auth/roles';
import { useParametersWithProvidedClient } from '../shared/operations/parametres/parameters';
import { Groupement, Pharmacie, useMeLazyQuery, User } from '../federation';
import {
  useCurrentGroupementWithProvidedClient,
  useCurrentPharmacieWithProvidedClient,
  usePharmaciesWithProvidedClient,
} from '../shared';
export interface InitAppResult {
  parameters: ParameterInfoFragment[];
  pharmacies: Pharmacie[];
  currentPharmacie: Pharmacie;
  currentGroupement: Groupement;
  user?: User;
  ip?: string;
  accessToken?: string;
  loading: boolean;
  error?: Error;
}

export const initApp = (federationClient: ApolloClient<any>, graphqlClient: ApolloClient<any>): InitAppResult => {
  const [pharmacies, setPharmacies] = usePharmaciesWithProvidedClient(graphqlClient);
  const [currentPharmacie, setCurrentPharmacie] = useCurrentPharmacieWithProvidedClient(graphqlClient);
  const [currentGroupement, setCurrentGroupement] = useCurrentGroupementWithProvidedClient(graphqlClient);
  const [loadingIp, setLoadingIp] = useState<boolean>(true);
  const [currentIp, setCurrentIp] = useState<string>();

  const goToSignin = () => {
    localStorage.clear();
    window.history.pushState(null, '', '/#/signin');
    window.location.reload();
  };

  const [loadMe, loadingMe] = useMeLazyQuery({
    client: federationClient,
    onCompleted: (data) => {
      const pharmacies = data.me?.pharmacies || [];

      const localStoragePharmacie = getPharmacie();
      const defaultPharmacie = pharmacies[0] as any;

      setPharmacies(pharmacies as any);
      setCurrentPharmacie(localStoragePharmacie || defaultPharmacie);
      setPharmacie(localStoragePharmacie || defaultPharmacie);

      if (isChooseGroupementRequested()) {
        const localGroupement = getGroupement();
        if (!localGroupement) {
          goToSignin();
          return;
        }
        setCurrentGroupement(localGroupement);
      } else {
        setGroupement(data.me?.groupement);
        setCurrentGroupement(data.me?.groupement as any);
      }

      setUser({ ...data.me, pharmacies: undefined, groupement: undefined } as any);
    },
    onError: (error) => {
      console.log(error.message);
      goToSignin();
    },
  });

  const [parameters, setParameters] = useParametersWithProvidedClient(graphqlClient);
  const [loadParameters, loadingParameters] = useParameters_By_CategoriesLazyQuery({
    client: graphqlClient,
    onCompleted: (data) => {
      if (data.parametersByCategories) {
        const values = data.parametersByCategories
          .map((parameter: any) => {
            let newParameter = parameter;

            if (newParameter && newParameter.value === null) {
              newParameter = {
                ...newParameter,
                value: { id: null, value: null, __typename: 'ParameterValue' },
              };
            }

            if (parameter && parameter.parameterType === null) {
              newParameter = {
                ...newParameter,
                parameterType: { id: null, code: '', nom: '', __typename: 'parameterType' },
              };
            }

            if (parameter && parameter.parameterGroupe === null) {
              newParameter = {
                ...newParameter,
                parameterGroupe: { id: null, code: '', nom: '', __typename: 'ParameterGroupe' },
              };
            }

            return newParameter;
          })
          .map((value) => {
            return {
              ...value,
              options: value.options || [
                { label: 'Désactivé', value: 'disabled', __typename: 'ParameterOption' }, //Why : option par défaut sinon error dans le store
              ],
            };
          });
        setParameters(values);
      }
    },
    onError: () => {
      localStorage.clear();
      window.history.pushState(null, '', '/#/signin');
      window.location.reload();
    },
  });

  useEffect(() => {
    loadMe();
    detectIp()
      .then((ip) => {
        setLoadingIp(false);
        ip && setIp(ip);
        setCurrentIp(ip);
      })
      .catch(() => setLoadingIp(false));
  }, []);

  useEffect(() => {
    if (loadingMe.data?.me?.role?.code) {
      const user = loadingMe.data.me;

      let categories = [ParamCategory.User, ParamCategory.Plateforme, ParamCategory.Groupement];

      //@ts-ignore
      if (user.role.code === SUPER_ADMINISTRATEUR) {
        categories = [...categories, ParamCategory.GroupementSuperadmin, ParamCategory.Pharmacie];
      }
      //@ts-ignore
      if (user.role.groupe === 'PHARMACIE' || user.role.groupe === 'SERVICE') {
        categories = [...categories, ParamCategory.Pharmacie];
      }

      loadParameters({
        variables: {
          categories,
        },
      });
    }
  }, [loadingMe.data?.me]);

  const loading =
    loadingParameters.loading ||
    loadingMe.loading ||
    !currentPharmacie ||
    !currentGroupement ||
    !parameters ||
    parameters.length === 0 ||
    !pharmacies ||
    loadingIp;
  const error = loadingParameters.error || loadingMe.error;
  return {
    loading,
    parameters,
    error,
    pharmacies,
    currentPharmacie,
    currentGroupement,
    user: loadingMe.data?.me as any,
    ip: currentIp,
    accessToken: getAccessToken() as any,
  };
};
