const getCookie = (name: string): string => {
  const regex = new RegExp(`(?:(?:^|.*;*)${name}*=*([^;]*).*$)|^.*$`);
  return document.cookie.replace(regex, '$1');
};

const setCookie = (name: string, value: string, maxAge = 31536000): void => {
  document.cookie = `${name}=${value};path=/;max-age=${maxAge}`;
};

const getCookies = (): string[] => {
  return (document.cookie && document.cookie.split(';')) || [];
};

const clearCookies = (): void => {
  const cookies = getCookies();

  for (const cookie of cookies) {
    const eqPos = cookie.indexOf('=');
    const name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
  }
};

export { getCookie, getCookies, clearCookies, setCookie };
