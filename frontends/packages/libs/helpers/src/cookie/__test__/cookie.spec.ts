import { setCookie, getCookie, clearCookies, getCookies } from './../index';
import faker from 'faker';

describe('Cookie helper', () => {
  beforeAll(() => clearCookies());

  it('Set/Get/clear cookie value', () => {
    const cookieName = faker.random.word();
    const cookieValue = faker.random.word();

    expect(getCookies()).toHaveLength(0);

    setCookie(cookieName, cookieValue);
    expect(getCookie(cookieName)).toEqual(cookieValue);
    expect(getCookies()).toHaveLength(1);

    clearCookies();
    expect(getCookies()).toHaveLength(0);

    setCookie(cookieName, cookieValue, 0);
    expect(getCookies()).toHaveLength(0);
  });
});
