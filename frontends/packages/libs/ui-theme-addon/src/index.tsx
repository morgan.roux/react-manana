import React from 'react';
import addons, { DecoratorFunction } from '@storybook/addons';
import { THEME_CHANGED_EVENT } from './config';
import { useTheme, ThemeProvider } from '@lib/ui-theme';

export const withUiKitThemeAddon: DecoratorFunction = (storyFn, context) => {
  const channel = addons.getChannel();
  const [, changeTheme] = useTheme();
  channel.on(THEME_CHANGED_EVENT, changeTheme);
  return (
    <ThemeProvider>
      <>{storyFn(context)}</>
    </ThemeProvider>
  );
};
