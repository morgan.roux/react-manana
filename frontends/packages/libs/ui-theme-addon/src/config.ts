export const ADDON_ID = 'uiKitTheme';
export const PARAM_KEY = 'uiKitTheme';
export const PANEL_ID = `${ADDON_ID}/panel`;
export const THEME_CHANGED_EVENT = `${ADDON_ID}/themeChanged`;
