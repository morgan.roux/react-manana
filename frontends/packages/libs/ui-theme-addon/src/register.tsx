import React from 'react';
import { addons, types, RenderOptions } from '@storybook/addons';
import { ADDON_ID, PANEL_ID, PARAM_KEY } from './config';
import { AddonPanel } from '@storybook/components';

import ThemePanel from './ThemePanel';
addons.register(ADDON_ID, () => {
  const render = ({ key, active }: RenderOptions) => (
    <AddonPanel active={active ?? true} key={key}>
      <ThemePanel active={active} key={key} />
    </AddonPanel>
  );
  const title = 'Theme';
  addons.add(PANEL_ID, {
    type: types.PANEL,
    title,
    render,
    paramKey: PARAM_KEY,
  });
});
