import React, { FC } from 'react';
import { RenderOptions } from '@storybook/addons';
import { useChannel } from '@storybook/api';
import { AddonPanel } from '@storybook/components';
import { THEME_CHANGED_EVENT } from './config';
import { availableThemes } from '@lib/ui-theme';
import { useCookie } from '@lib/hooks';

const ThemePanel: FC<RenderOptions> = ({ active, key }: RenderOptions) => {
  const emit = useChannel({});
  const themeNames = Object.keys(availableThemes);
  const [fieldValue, setFieldValue] = useCookie<string>('story-theme', themeNames[0]);

  const handleThemeChanged = (newTheme: string) => {
    setFieldValue(newTheme);
    emit(THEME_CHANGED_EVENT, newTheme);
  };

  return (
    <AddonPanel active={active ?? true} key={key}>
      <div>
        <select value={fieldValue} onChange={(event) => handleThemeChanged(event.target.value)}>
          {themeNames.map((name) => (
            <option key={name}>{name}</option>
          ))}
        </select>
      </div>
    </AddonPanel>
  );
};

export default ThemePanel;
