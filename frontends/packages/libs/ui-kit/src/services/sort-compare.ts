export function getCollatorComparator() {
  if (Intl) {
    const collator = new Intl.Collator(undefined, { numeric: true, sensitivity: 'base' });
    return collator.compare;
  }

  const fallbackComparator = (a: any, b: any) => a.localeCompare(b);
  return fallbackComparator;
}

export function sortCompare(order: 'asc' | 'desc') {
  return (a: any, b: any) => {
    const aData = a.data === null || typeof a.data === 'undefined' ? '' : a.data;
    const bData = b.data === null || typeof b.data === 'undefined' ? '' : b.data;
    return (
      (typeof aData.localeCompare === 'function' ? aData.localeCompare(bData) : aData - bData) *
      (order === 'asc' ? 1 : -1)
    );
  };
}
