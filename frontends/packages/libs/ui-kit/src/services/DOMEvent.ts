import { MouseEvent } from 'react';

export const stopEvent = (event: MouseEvent<any>) => {
  event.preventDefault();
  event.stopPropagation();
};
