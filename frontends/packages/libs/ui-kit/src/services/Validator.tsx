export const isEmailValid = (email: string): boolean => {
  if (email.length > 0) {
    const regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
    return email !== undefined && regexEmail.test(email.trim());
  }
  return true;
};

export const isValidPassword = (password: string): boolean => {
  return !!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(password);
};

export const isUrlValid = (url: string): boolean => {
  const res = url.match(/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/gm);
  return res !== null;
};
