import React, { FC } from 'react';
import { Box } from '@material-ui/core';
import { useStyles } from './styles';

// TODO : Add style and icon
// TODO Panel and not page
const ChecklistPage: FC<{}> = ({}) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      Une erreur est survenue, merci de recharger la page puis de contacter la hotline si cela ne fonctionne pas mieux
    </Box>
  );
};

export default ChecklistPage;
