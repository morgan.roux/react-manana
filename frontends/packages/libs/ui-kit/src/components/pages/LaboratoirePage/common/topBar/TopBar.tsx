import React, { FC, ReactNode } from 'react';
import { Box } from '@material-ui/core';
import { NewCustomButton } from './../../../../atoms';
import { Add } from '@material-ui/icons';
import { styles } from './style';
import { useTheme } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import SortIcon from '@material-ui/icons/Sort';
import classnames from 'classnames';
import { SearchInputBase } from '../../../../atoms/SearchInputBase';

interface TopBarProps {
  searchComponent?: ReactNode;
  handleOpenAdd?: () => void;
  titleTopBar?: string;
  titleButton?: string;
  withFilter: boolean;
  filterComponent?: ReactNode;
  noAddBtn?: boolean;
  fullwidth?: boolean;
  variantButton?: 'contained' | 'outlined' | 'text';
  loading?: boolean;
  withMoreButton?: boolean;
  buttons?: buttonProps[];
  buttonClassName?: string;
  topBarClassName?: string;
  filterClassName?: string;
  searchContainerClassName?: string;
  className?: string;
  style?: string | undefined;
  titleTopBarClassName?: string;
}

interface buttonProps {
  component: ReactNode;
}

const TopBar: FC<TopBarProps> = ({
  searchComponent,
  handleOpenAdd,
  titleTopBar,
  titleButton,
  withFilter,
  filterComponent,
  noAddBtn,
  fullwidth,
  variantButton,
  loading,
  withMoreButton = false,
  buttons,
  topBarClassName,
  buttonClassName,
  filterClassName,
  searchContainerClassName,
  className,
  style,
  titleTopBarClassName,
}) => {
  const classes = styles();
  const theme = useTheme();
  const matchSM = useMediaQuery(theme.breakpoints.down('sm'));
  return (
    <>
      {matchSM ? (
        <Box py={2} width={fullwidth ? '100%' : undefined} className={`${classes.root} ${topBarClassName || ''}`}>
          <div className={classes.topbar}>
            {titleTopBar && (
              <Box className={classnames(classes.titleTopBar, titleTopBarClassName || '')}>{titleTopBar}</Box>
            )}
            <div>
              <SortIcon />
            </div>
          </div>
          <div className={classes.search}>
            <SearchInputBase dark onChange={function noRefCheck() {}} value="" placeholder="Recherche" />
          </div>
        </Box>
      ) : (
        <Box py={2} width={fullwidth ? '100%' : undefined} className={classes.root}>
          {titleTopBar && (
            <Box className={classnames(classes.titleTopBar, titleTopBarClassName || '')}>{titleTopBar}</Box>
          )}
          <Box
            display="flex"
            className={
              style
                ? className
                  ? classnames(`${classes.filterContainer} ${filterClassName || ''}`, className)
                  : classnames(`${classes.filterContainer} ${filterClassName || ''}`, style)
                : `${classes.filterContainer} ${filterClassName || ''}`
            }
            justifyContent="flex-end"
          >
            {withFilter && filterComponent}
            <Box ml={2} className={searchContainerClassName || ''}>
              {searchComponent}
            </Box>
            {!withMoreButton && !noAddBtn && titleButton && (
              <NewCustomButton
                key={`action-button-${0}`}
                onClick={handleOpenAdd}
                startIcon={<Add />}
                className={`${classes.btnAction} ${buttonClassName || ''}`}
                variant={variantButton}
                disabled={loading}
              >
                {titleButton}
              </NewCustomButton>
            )}
            {withMoreButton &&
              (buttons || []).map((button) => {
                return button.component;
              })}
          </Box>
        </Box>
      )}
    </>
  );
};

export default TopBar;
