import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const styles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      flexWrap: 'wrap',
    },
    titleTopBar: {
      fontWeight: 'bold',
      fontSize: 18,
      fontFamily: 'Roboto',
      marginBottom: 16,
      [theme.breakpoints.up('sm')]: {
        marginBottom: 0,
        fontSize: '16px',
        fontWeight: '400',
      },
      [theme.breakpoints.down('sm')]: {
        fontSize: '16px',
        fontWeight: '400',
      },
    },
    btnAction: {
      marginLeft: 16,
      marginTop: 16,
      '&.MuiButton-containedSecondary': {
        background: theme.palette.primary.main,
      },
      [theme.breakpoints.up('sm')]: {
        marginTop: 0,
      },
    },
    filterContainer: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-between',
      width: '100%',
      '& > div > div > div': {
        height: '48px !important',
      },
      // '& > div > div > div > div': {
      //   height: '48px !important',
      // },
    },
    topbar: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      marginBottom: '8px',
      [theme.breakpoints.down('sm')]: {
        display: 'flex',
        justifyContent: 'space-between',
      },
    },
    search: {
      width: '100%',
    },
  })
);
