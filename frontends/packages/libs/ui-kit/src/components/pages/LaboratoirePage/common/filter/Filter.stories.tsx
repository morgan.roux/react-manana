import { FilterList } from '@material-ui/icons';
import React from 'react';
import { Filter } from './Filter';

export default {
  title: 'Design system|Pages/LaboratoirePage/common/Filter',
  parameters: {
    info: { inline: true },
  },
};

export const FilterComponent = () => {
  const handleChange = (data: any) => {
    console.log(data);
  };

  return (
    <Filter
      filterIcon={<FilterList />}
      onChange={handleChange}
      filters={[
        {
          titre: 'test1',
          itemFilters: [
            {
              id: 'id1',
              libelle: 'lib1',
              code: 'fsdfsdfs',
              keyword: 'type1',
            },
            {
              id: 'id2',
              libelle: 'lib2',
              code: 'fsdfsdfs',
              keyword: 'type1',
            },
            {
              id: 'id3',
              libelle: 'lib3',
              code: 'fsdfsdfs',
              keyword: 'type1',
            },
          ],
        },
        {
          titre: 'test2',
          itemFilters: [
            {
              id: 'id4',
              libelle: 'lib4',
              code: 'fsdfsdfs',
              keyword: 'type2',
            },
            {
              id: 'id5',
              libelle: 'lib5',
              code: 'fsdfsdfs',
              keyword: 'type2',
            },
            {
              id: 'id6',
              libelle: 'lib6',
              code: 'fsdfsdfs',
              keyword: 'type2',
            },
          ],
        },
      ]}
    />
  );
};
