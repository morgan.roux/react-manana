export * from './ErrorPage';
export * from './SinglePage';
export * from './CategoriePage';
export * from './DashboardPage';
export * from './SelectBoxPage';
export * from './LaboratoirePage';
