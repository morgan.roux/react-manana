import React from 'react';
import SelectBoxItem, { SelectBoxItemProps } from '../../atoms/SelectBoxItem/SelectBoxItem';
import SelectBoxPage from './SelectBoxPage';
import { Banner } from '../../atoms/Banner';

const title: string = 'Ceci est un titre';

const subTitle: string = 'Ceci est un sous--titre';
const noTitle: string = '';

const noSubTitle: string = '';

const handleClick = (name: string) => {
  alert(name);
};

const data: SelectBoxItemProps[] = [
  {
    text: 'Matin',
    onClick: handleClick.bind(null, 'matin'),
    image: {
      src: 'https://groupe-metropole.mg/wp-content/uploads/2018/04/kanacia-592x400.jpg',
      alt: 'GGGG',
    },
  },
  {
    text: 'Après-Midi',
    onClick: handleClick.bind(null, 'Après-midi'),
    image: {
      src: 'https://groupe-metropole.mg/wp-content/uploads/2018/04/kanacia-592x400.jpg',
      alt: 'GGGG',
    },
  },
  {
    text: 'Soir',
    onClick: handleClick.bind(null, 'Soir'),
    image: {
      src: 'https://groupe-metropole.mg/wp-content/uploads/2018/04/kanacia-592x400.jpg',
      alt: 'GGGG',
    },
  },
  {
    text: 'Nuit',
    onClick: handleClick.bind(null, 'Nuit'),
    image: {
      src: 'https://groupe-metropole.mg/wp-content/uploads/2018/04/kanacia-592x400.jpg',
      alt: 'GGGG',
    },
  },
  {
    text: 'Nuit',
    onClick: handleClick.bind(null, 'Nuit'),
    image: {
      src: 'https://groupe-metropole.mg/wp-content/uploads/2018/04/kanacia-592x400.jpg',
      alt: 'GGGG',
    },
  },
  {
    text: 'Nuit',
    onClick: handleClick.bind(null, 'Nuit'),
    image: {
      src: 'https://groupe-metropole.mg/wp-content/uploads/2018/04/kanacia-592x400.jpg',
      alt: 'GGGG',
    },
  },
  {
    text: 'Nuit',
    onClick: handleClick.bind(null, 'Nuit'),
    image: {
      src: 'https://groupe-metropole.mg/wp-content/uploads/2018/04/kanacia-592x400.jpg',
      alt: 'GGGG',
    },
  },
  {
    text: 'Nuit',
    onClick: handleClick.bind(null, 'Nuit'),
    image: {
      src: 'https://groupe-metropole.mg/wp-content/uploads/2018/04/kanacia-592x400.jpg',
      alt: 'GGGG',
    },
  },
  {
    text: 'Nuit',
    onClick: handleClick.bind(null, 'Nuit'),
    image: {
      src: 'https://groupe-metropole.mg/wp-content/uploads/2018/04/kanacia-592x400.jpg',
      alt: 'GGGG',
    },
  },
];

export default {
  title: 'Design system|Pages/SelectBoxPage',
  parameters: {
    info: { inline: true },
  },
};

export const SelectBoxPageStorie = () => (
  <SelectBoxPage
    title={title}
    subTitle={subTitle}
    banner={<Banner title="Outils" onClick={handleClick.bind(null, 'Outils')} />}
  >
    {data.map((currentData: SelectBoxItemProps, index: number) => (
      <SelectBoxItem
        key={index}
        image={currentData.image}
        text={currentData.text}
        onClick={currentData.onClick}
      />
    ))}
  </SelectBoxPage>
);

export const SelectBoxPageStorieNotitle = () => (
  <SelectBoxPage
    title={noTitle}
    subTitle={noSubTitle}
    banner={<Banner title="Outils" onClick={handleClick.bind(null, 'Outils')} />}
  >
    {data.map((currentData: SelectBoxItemProps, index: number) => (
      <SelectBoxItem
        key={index}
        image={currentData.image}
        text={currentData.text}
        onClick={currentData.onClick}
      />
    ))}
  </SelectBoxPage>
);
