import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      overflowY: 'auto',
      height: 'calc(100vh - 86px)',
    },
    banner: {
      zIndex: 1,
      position: 'fixed',
      top: '86px',
      left: '0',
      width: '100%',
      background: '#fff',
      height: '70px',
      paddingLeft: 10,
      paddingTop: 10,
    },
    content: {
      width: '100%',
      maxWidth: '1100px',
      margin: '0 auto',
      alignContent: 'center',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      paddingTop: '130px',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '@media (max-width: 768px)': {
        overflowY: 'auto',
        paddingTop: 86,
        margin: 'auto',
        alignContent: 'center',
      },
    },
    contentNoTitle: {
      width: '100%',
      maxWidth: '980px',
      margin: '0 auto',
      alignContent: 'center',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      paddingTop: 60,
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '@media (max-width: 768px)': {
        overflowY: 'auto',
        paddingTop: 86,
        margin: 'auto',
        alignContent: 'center',
      },
    },

    title: {
      color: '#616161',
      fontWeight: 'bold',
      fontSize: 25,
    },
    noText: {
      paddingTop: '70px',
    },

    subTitle: {
      marginTop: 15,
      color: '#9E9E9E',
      fontWeight: 'normal',
      fontSize: 14,
      marginBottom: 50,
      [theme.breakpoints.down('sm')]: {
        padding: '0 25px',
        textAlign: 'center',
      },
    },
  })
);

export default useStyles;
