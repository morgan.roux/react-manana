import { Box, Typography } from '@material-ui/core';
import React, { FC, ReactNode, CSSProperties } from 'react';
import SelectBoxList from '../../molecules/SelectBoxList/SelectBoxList';
import useStyles from './style';

interface SelectBoxPageProps {
  title: string;
  subTitle: string;
  banner?: ReactNode;
  innerStyle: CSSProperties;
}

const SelectBoxPage: FC<SelectBoxPageProps> = ({ title, subTitle, banner, children, innerStyle }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.banner}>{banner}</Box>
      <Box className={!title ? classes.contentNoTitle : classes.content}>
        {title && <Typography className={classes.title}>{title}</Typography>}
        {subTitle && <Typography className={classes.subTitle}>{subTitle}</Typography>}

        <SelectBoxList style={innerStyle}>{children}</SelectBoxList>
      </Box>
    </Box>
  );
};

export default SelectBoxPage;
