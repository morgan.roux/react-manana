import CategoriePage, { ICategorie, ISousCategorie } from './CategoriePage';

export { CategoriePage, ICategorie, ISousCategorie };
