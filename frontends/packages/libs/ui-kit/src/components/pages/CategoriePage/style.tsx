import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {},
    modalContent: {
      width: '100%',
      marginTop: 20,
    },
    detailsTitle: {
      fontWeight: 'bold',
    },
    detailsHeader: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      padding: 15,
      marginBottom: 10,
      alignItems: 'center',
    },
    validationBox: {
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'center',
    },
  })
);

export default useStyles;
