import {
  Box,
  Divider,
  IconButton,
  Switch,
  Menu,
  Fade,
  MenuItem,
  ListItemIcon,
  Typography,
  FormControlLabel,
  Checkbox,
} from '@material-ui/core';
import { Add, Delete, Edit, MoreHoriz } from '@material-ui/icons';
import React, { FC, ReactNode, useState, ChangeEvent, useEffect } from 'react';
import {
  CardTheme,
  Column,
  ConfirmDeleteDialog,
  CustomFormTextField,
  CustomModal,
  CustomSelect,
  Loader,
  NewCustomButton,
  Table,
} from '../../atoms';
import { ItemContainer } from '../../templates';
import useStyles from './style';

interface Fichier {
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string;
}

export interface Role {
  id: string;
  nom: string;
  code: string;
}

interface User {
  id: string;
  fullName: string;
  email?: string;
  role?: Role;
  photo?: Fichier;
}

export interface ICategorie {
  id?: string;
  libelle?: string;
  type?: string;
  base?: boolean;
  participants?: User[];
  validation?: boolean;
  workflowValidation?: string;
  idPartenaireValidateur?: string;
  sousCategories?: ICategorie[];
  partenaireValidateur?: any;
  nombreSousCategories?: number;
  idPharmacie?: string;
}

export interface ISousCategorie {
  id?: string;
  libelle?: string;
  base?: boolean;
  parent?: ICategorie;
  validation?: boolean;
  participants?: User[];
  workflowValidation?: string;
  partenaireValidateur?: any;
}

export interface Workflow {
  code: string;
  libelle: string;
}

export interface CategoriePageProps {
  currentIdPharmacie?: string;
  loading: boolean;
  error: Error;
  categories: ICategorie[];
  emptyListComponent: ReactNode;
  noCategorieSelected: ReactNode;
  categorie: {
    loading?: boolean;
    error?: Error;
    data?: ICategorie;
  };
  onRequestSave: (categorie: ICategorie) => void;
  onRequestSaveSousCategorie: (sousCategorie: ISousCategorie) => void;
  onRequestDelete: (categorie: ICategorie) => void;
  onRequestShowDetail: (categorie: ICategorie) => void;
  onRequestDeleteSousCategorie: (sousCategorie: any) => void;
  PartageComponent?: ReactNode;
  workflows?: Workflow[];
  participants?: User[];
  SelectPartenaireComponent?: ReactNode;
  partenaireValidateur?: any;

  // Use to reset fields
  setParticipants: (newParticipants: User[]) => void;
  setPartenaireValidateur: (newValidateur: any) => void;
}

const CategoriePage: FC<CategoriePageProps> = ({
  onRequestSave,
  onRequestDelete,
  onRequestSaveSousCategorie,
  onRequestShowDetail,
  onRequestDeleteSousCategorie,
  currentIdPharmacie,
  emptyListComponent,
  categorie,
  noCategorieSelected,
  loading,
  error,
  categories,
  PartageComponent,
  workflows,
  participants,
  SelectPartenaireComponent,
  partenaireValidateur,
  setParticipants,
  setPartenaireValidateur,
}) => {
  const classes = useStyles();

  const [openSaveModal, setOpenSaveModal] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [activeCategorie, setActiveCategorie] = useState<ICategorie | undefined>();
  const [categorieToEdit, setCategorieToEdit] = useState<ICategorie | undefined>();
  const [sousCategorieToEdit, setSousCategorieToEdit] = useState<any | undefined>();
  const [itemToDelete, setItemToDelete] = useState<'categorie' | 'sous-categorie'>('categorie');
  const [workflowValidation, setWorkflowValidation] = useState<string>(workflows ? workflows[0].code : '');
  const [type, setType] = useState<string>('');

  // idPharmacie
  const [libelle, setLibelle] = useState<string>('');
  const [mode, setMode] = useState<'creation' | 'modification'>('creation');
  const [isSavingSousCategorie, setIsSavingSousCategorie] = useState<boolean>(false);
  // const [openPartageModal, setOpenPartageModal] = useState<boolean>(false);
  // const [rowsSelected, setRowsSelected] = useState<User[]>([]);
  const [validation, setValidation] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const open = Boolean(anchorEl);

  useEffect(() => {
    if (!openSaveModal) {
      setLibelle('');
      setType('');
      setWorkflowValidation('');
      setPartenaireValidateur('');
      setParticipants([]);
    }
  }, [openSaveModal]);

  const handleLibelleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setLibelle((event.target as HTMLInputElement).value);
  };

  const reInit = (): void => {
    setLibelle('');
    // setRowsSelected([]);
    setValidation(true); // setRowsSelected([]);
  };

  const saveCategorie = (): void => {
    if (libelle || type) {
      if (isSavingSousCategorie) {
        onRequestSaveSousCategorie({
          parent: {
            id: activeCategorie?.id,
          },
          id: sousCategorieToEdit?.id,
          libelle: libelle,
          participants: participants,
          validation: validation,
          workflowValidation: validation ? workflowValidation : '',
          partenaireValidateur:
            validation && workflowValidation === 'PARTENAIRE_SERVICE' ? partenaireValidateur : undefined,
        });
      } else {
        const categorie: ICategorie = {
          libelle: libelle,
          type: type ?? undefined,
          participants: type === 'FACTURE' ? participants : undefined,
          validation: type === 'FACTURE' ? validation : undefined,
          workflowValidation: type === 'FACTURE' && validation ? workflowValidation : '',
          partenaireValidateur:
            type === 'FACTURE' && validation && workflowValidation === 'PARTENAIRE_SERVICE'
              ? partenaireValidateur
              : undefined,
        };
        if (mode === 'creation') {
          onRequestSave(categorie);
        } else {
          onRequestSave({
            id: categorieToEdit?.id,
            libelle: libelle,
            type: type ?? undefined,
            participants: type === 'FACTURE' ? participants : undefined,
            validation: type === 'FACTURE' ? validation : undefined,
            workflowValidation: type === 'FACTURE' && validation ? workflowValidation : '',
            partenaireValidateur:
              type === 'FACTURE' && validation && workflowValidation === 'PARTENAIRE_SERVICE'
                ? partenaireValidateur
                : undefined,
          });
        }
      }
      reInit();
      setOpenSaveModal(false);
    }
  };

  const editCategorie = (categorie: ICategorie): void => {
    setMode('modification');
    setIsSavingSousCategorie(false);
    setOpenSaveModal(true);
    setCategorieToEdit(categorie);
    setLibelle(categorie.libelle as any);
    setWorkflowValidation(categorie.workflowValidation || '');
    setType(categorie.type || '');
    setValidation(categorie.validation || false);
    setParticipants(categorie.participants || []);
    setPartenaireValidateur(categorie.partenaireValidateur);
  };

  const deleteCategorie = (): void => {
    if (categorieToEdit) {
      setActiveCategorie(undefined);
      onRequestDelete(categorieToEdit);
      setOpenDeleteDialog(false);
    }
  };

  const deleteSousCategorie = (): void => {
    if (sousCategorieToEdit) {
      onRequestDeleteSousCategorie(sousCategorieToEdit);
      setOpenDeleteDialog(false);
    }
  };

  const handleCategorieClick = (categorie: ICategorie): void => {
    setActiveCategorie(categorie);
    onRequestShowDetail(categorie);
  };

  // const handleOpenPartageModal = (): void => {
  //   setOpenPartageModal(!openPartageModal);
  // };

  const isActivedAddButton = (): boolean => {
    if (isSavingSousCategorie) {
      return (
        libelle === '' ||
        (participants || []).length === 0 ||
        (validation && !workflowValidation) ||
        (validation && workflowValidation === 'PARTENAIRE_SERVICE' && !partenaireValidateur?.id)
      );
    }
    return libelle === '';
  };

  // const getTypeCategorie = categories.map((categorie) => { return categorie.type==='FACTURE'})

  // const handlePartageClick = (): void => {
  //   /* let value = '';
  //   rowsSelected.map((data: User, index: number) => {
  //     value += `${data?.fullName}${index === rowsSelected.length - 1 ? '' : ' , '}`;
  //   });
  //   */

  //   // setShareWith(value);
  //   setOpenPartageModal(false);
  // };

  const handleShowMenuClick = (sousCategorie: any, event: React.MouseEvent) => {
    event.preventDefault();
    event.stopPropagation();
    setSousCategorieToEdit(sousCategorie);
    setAnchorEl(event.currentTarget as any);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const handleDeleteSousCategorie = (sousCategorie: any): void => {
    setSousCategorieToEdit(sousCategorie);
  };

  const handleEditSousCategorie = (row: ISousCategorie): void => {
    setMode('modification');
    setIsSavingSousCategorie(true);
    setOpenSaveModal(true);

    setSousCategorieToEdit(row);
    setLibelle(row.libelle || '');
    setValidation(row.validation || false);
    setWorkflowValidation(row.workflowValidation || '');
    setParticipants(row.participants || []);
    setPartenaireValidateur(row.partenaireValidateur);
    // setRowsSelected(row.participants);
  };

  const handleAddSousCategorie = (): void => {
    setMode('creation');
    setOpenSaveModal(true);
    setIsSavingSousCategorie(true);

    setSousCategorieToEdit(null);
    setLibelle('');
    setValidation(false);
    setWorkflowValidation(workflows ? workflows[0].code : '');
    setParticipants([]);
    setPartenaireValidateur('');
  };

  const handleChangeType = (): void => {
    setType((prev) => (prev === 'FACTURE' ? '' : 'FACTURE'));
  };

  const columns: Column[] = [
    {
      name: 'libelle',
      label: 'Titre',
    },

    {
      name: 'validation',
      label: 'Nécessitant une validation',
      renderer: (row: any) => {
        return row?.validation ? 'Oui' : 'Non';
      },
    },

    {
      name: 'workflow',
      label: 'Workflow de validation',
      renderer: (row: any) => {
        return row?.workflowValidation ? row.workflowValidation : '-';
      },
    },

    {
      name: 'partenaire',
      label: 'Prestataire',
      renderer: (row: any) => {
        return row?.partenaireValidateur?.nom ? row.partenaireValidateur.nom : '-';
      },
    },

    {
      name: '',
      label: 'Partagé avec',
      renderer: (row: any) => {
        return row?.participants?.length > 0 ? row.participants.map(({ fullName }: any) => fullName).join(', ') : '-';
      },
    },
  ];

  if (!categorie.data?.type) {
    columns.push({
      name: '',
      label: '',
      renderer: (row: any) => {
        return (
          <div key={`row-${row.id}`}>
            {!categorie.data?.type && (
              <>
                <IconButton
                  aria-controls="simple-menu"
                  aria-haspopup="true"
                  onClick={handleShowMenuClick.bind(null, row)}
                >
                  <MoreHoriz />
                </IconButton>

                <>
                  <Menu
                    id={`fiche-amelioration-menu-${row.id}`}
                    anchorEl={anchorEl}
                    keepMounted
                    open={open && row.id === sousCategorieToEdit?.id}
                    onClose={handleClose}
                    TransitionComponent={Fade}
                  >
                    <MenuItem
                      onClick={(event) => {
                        event.stopPropagation();
                        event.preventDefault();
                        handleClose();
                        handleEditSousCategorie(row);
                      }}
                      disabled={row?.cloture}
                    >
                      <ListItemIcon>
                        <Edit />
                      </ListItemIcon>
                      <Typography variant="inherit">Modifier</Typography>
                    </MenuItem>

                    <MenuItem
                      onClick={(event) => {
                        event.stopPropagation();
                        event.preventDefault();
                        handleClose();
                        setItemToDelete('sous-categorie');
                        handleDeleteSousCategorie(row);
                        setOpenDeleteDialog(true);
                      }}
                      disabled={row?.cloture}
                    >
                      <ListItemIcon>
                        <Delete />
                      </ListItemIcon>
                      <Typography variant="inherit">Supprimer</Typography>
                    </MenuItem>
                  </Menu>
                </>
              </>
            )}
          </div>
        );
      },
    });
  }

  const currentTypeCategorie = (categories || []).find((categorie) => 'FACTURE' === categorie.type);
  const showTypeOption =
    !isSavingSousCategorie &&
    (!currentTypeCategorie || (mode === 'modification' && currentTypeCategorie.id === categorieToEdit?.id));

  return (
    <>
      <ItemContainer
        loading={loading}
        error={error}
        emptyListComponent={emptyListComponent}
        items={categories as any}
        listBanner={
          <Box style={{ textAlign: 'center' }}>
            <NewCustomButton
              onClick={() => {
                setIsSavingSousCategorie(false);
                setMode('creation');
                setOpenSaveModal(true);
              }}
              startIcon={<Add />}
              style={{ fontSize: 12 }}
            >
              Ajouter un espace documentaire
            </NewCustomButton>
            <Divider style={{ marginTop: 10 }} />
          </Box>
        }
        listItemRenderer={(categorie: any, index: number) => {
          return (
            <CardTheme
              key={categorie.id || index}
              title={categorie.type ? `${categorie.libelle} (${categorie.type})` : categorie.libelle}
              onClick={() => handleCategorieClick(categorie)}
              active={activeCategorie?.id === categorie.id}
              littleComment={`Nbre de sous-espace doc : ${categorie.nombreSousCategories}`}
              moreOptions={[
                {
                  menuItemLabel: {
                    title: 'Modifier',
                    icon: <Edit />,
                  },
                  disabled: !(currentIdPharmacie === categorie?.idPharmacie),
                  onClick: () => {
                    editCategorie(categorie);
                  },
                },
                {
                  menuItemLabel: {
                    title: 'Supprimer',
                    icon: <Delete />,
                  },
                  disabled: !(currentIdPharmacie === categorie?.idPharmacie),
                  onClick: () => {
                    setOpenDeleteDialog(true);
                    setCategorieToEdit(categorie);
                  },
                },
              ]}
            />
          );
        }}
        detailRenderer={() =>
          activeCategorie && categorie ? (
            categorie.loading ? (
              <Loader />
            ) : (
              <Box>
                <Box className={classes.detailsHeader}>
                  <Box className={classes.detailsTitle}>
                    <Typography color="primary" style={{ fontWeight: 'bold' }}>
                      Détail d'espace documentaire
                    </Typography>
                  </Box>
                  <Box>
                    {!categorie.data?.type && (
                      <NewCustomButton onClick={handleAddSousCategorie} startIcon={<Add />}>
                        Ajouter un sous-espace documentaire
                      </NewCustomButton>
                    )}
                  </Box>
                </Box>
                <Divider />
                <Table
                  error={categorie?.error}
                  loading={categorie?.loading}
                  columns={columns}
                  data={(categorie?.data?.sousCategories || []) as any}
                  // rowsSelected={tableRowsSelected}
                  selectable={false}
                  search={false}
                />
              </Box>
            )
          ) : (
            noCategorieSelected
          )
        }
      />
      <CustomModal
        title={
          isSavingSousCategorie
            ? mode === 'creation'
              ? 'Ajout de sous-espace documentaire'
              : 'Modification de sous-espace documentaire'
            : mode === 'creation'
            ? "Ajout d'espace documentaire"
            : "Modification d'espace documentaire"
        }
        open={openSaveModal}
        setOpen={setOpenSaveModal}
        withBtnsActions
        centerBtns
        onClickConfirm={saveCategorie}
        headerWithBgColor
        closeIcon
        maxWidth="sm"
        disabledButton={isActivedAddButton()}
        fullWidth
      >
        <Box className={classes.modalContent}>
          <Box>
            <CustomFormTextField value={libelle} label="Titres" onChange={handleLibelleChange} />
          </Box>
          {showTypeOption && (
            <Box>
              <FormControlLabel
                control={
                  <Checkbox checked={type === 'FACTURE'} onChange={handleChangeType} name="type" color="primary" />
                }
                label="Facture"
              />
            </Box>
          )}

          {(isSavingSousCategorie || type === 'FACTURE') && (
            <>
              <Box py={1}>{PartageComponent}</Box>
              <Box className={classes.validationBox}>
                <Switch checked={validation} onClick={() => setValidation((prev) => !prev)} />
                Nécessitant une validation
              </Box>
              {validation && (
                <Box pt={2}>
                  <CustomSelect
                    listId="code"
                    index="libelle"
                    label="Workflow de Validation"
                    value={workflowValidation}
                    onChange={(event: any) => setWorkflowValidation(event.target.value)}
                    list={workflows}
                    style={{ height: '56px', textAlign: 'left' }}
                    required
                  />
                </Box>
              )}

              {validation && workflowValidation === 'PARTENAIRE_SERVICE' && SelectPartenaireComponent}
            </>
          )}
        </Box>
      </CustomModal>
      <ConfirmDeleteDialog
        title={`Suppression ${itemToDelete === 'categorie' ? "d'espace documentaire" : 'de sous-espace documentaire'}`}
        content={`Etes-vous sûr de vouloir supprimer ${
          itemToDelete === 'categorie' ? 'cet espace documentaire' : 'ce sous-espace documentaire'
        } ? Cette action est irréversible.`}
        onClickConfirm={itemToDelete === 'categorie' ? () => deleteCategorie() : () => deleteSousCategorie()}
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
      />
    </>
  );
};

export default CategoriePage;
