import React, { useState, useEffect } from 'react';
import CategoriePage, { ICategorie, ISousCategorie } from './CategoriePage';

export default {
  title: 'Design system|Pages/CategoriePage',
  parameters: {
    info: { inline: true },
  },
};

export const CategoriePageStory = () => {
  const [loading, setLoading] = useState<boolean>(true);
  const [categorieLoading, setCategorieLoading] = useState<boolean>(false);
  const [currentCategorie, setCurrentCategorie] = useState<ICategorie | undefined>();

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 100);
  }, []);

  const categoriesData: any[] = [
    {
      id: '1',
      libelle: 'Categorie 1',
      nombreSousCategories: 9,
      idPharmacie: '1',
      type: 'FACTURE',
      sousCategories: [
        {
          libelle: 'XXX',
          validation: true,
          participants: [
            {
              id: '11',
              fullName: 'Dalilah HJ',
            },
            {
              id: '22',
              fullName: 'Kori CARTER',
            },
            {
              id: '33',
              fullName: 'Carmelita JETER',
            },
            {
              id: '44',
              fullName: 'Bianca KNIGHT',
            },
          ],
        },
      ],
    },
    {
      id: '2',
      libelle: 'Categorie 2',
      nombreSousCategories: 9,
      idPharmacie: '1',
      type: '',
      sousCategories: [
        {
          libelle: 'XXX',
          validation: true,
          participants: [
            {
              id: '11',
              fullName: 'Ling XIAYPU',
            },
          ],
        },
        {
          libelle: 'YYY',
          validation: true,
          participants: [
            {
              id: '11',
              fullName: 'm h j',
            },
          ],
        },
      ],
    },
    {
      id: '3',
      libelle: 'Categorie 3',
      nombreSousCategories: 9,
      idPharmacie: '1',
      sousCategories: [
        {
          libelle: 'XXX',
        },
        {
          libelle: 'YYY',
        },
        {
          libelle: 'ZZZ',
        },
      ],
    },
  ];

  const handleDelete = (categorie: ICategorie): void => {
    console.log('**Category to delete', categorie);
  };

  const handleSaveCategorie = (categorie: ICategorie): void => {
    console.log('**Category to save', categorie);
  };

  const handleSaveSousCategorie = (sousCategorie: ISousCategorie): void => {
    console.log('**Sous catégorie to save', sousCategorie);
  };

  const handleConsult = (categorie: ICategorie): void => {
    setCategorieLoading(true);
    setTimeout(() => {
      setCategorieLoading(false);
      setCurrentCategorie(categorie);
    }, 100);
  };


  const handleDeleteSousCategorie = (sousCategorie: any): void => {
    console.log('***sousCategorie to delete****', sousCategorie);
  };

  return (
    <CategoriePage
      loading={loading}
      error={false as any}
      emptyListComponent={<div>No Category found.</div>}
      currentIdPharmacie="1"
      noCategorieSelected={<div>Aucune catégorie séléctionnée</div>}
      categories={categoriesData}
      onRequestDelete={handleDelete}
      onRequestSave={handleSaveCategorie}
      onRequestSaveSousCategorie={handleSaveSousCategorie}
      onRequestShowDetail={handleConsult}
      onRequestDeleteSousCategorie={handleDeleteSousCategorie}
      setParticipants={console.log}
      setPartenaireValidateur={console.log}
      categorie={{
        loading: categorieLoading,
        error: false as any,
        data: currentCategorie,
      }}
      PartageComponent={<div>Partage Component</div>}
      workflows={[
        {
          code: 'INTERNE',
          libelle: 'Interne',
        },
        {
          code: 'UTILISATEUR',
          libelle: 'Utilisateur',
        },
      ]}
    />
  );
};
