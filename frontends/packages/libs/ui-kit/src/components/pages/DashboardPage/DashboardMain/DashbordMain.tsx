import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import { DateFilterDashboard } from './RightContent/CalendarFilter/CalendarFilter';
import { CenterContent, CenterContentProps } from './CenterContent/CenterContent';
import { UrgenceImportance } from './RightContent/ImportanceFilter/ImportanceFilter';
import { RightContent, RightContentProps } from './RightContent/RightContent';
import { useStyles } from './styles';

export interface DashboardMainProps {
  centerContentProps?: CenterContentProps;
  rightContentProps?: RightContentProps;
  urgenceImportanceFilter?: UrgenceImportance;
  onRequestFilterImportanceUrgence?: (filter: UrgenceImportance | undefined) => void;
  dateFilter?: DateFilterDashboard;
  onRequestFilterDate?: (dateFilter: DateFilterDashboard) => void;
  onRequestSearch?: (skip: number, take: number, searchText: string) => void;
  onRequestSort?: (column: string, direction: 'DESC' | 'ASC') => void;
  onGoBack?: () => void;
}

export const DashboardMain: FC<DashboardMainProps> = ({
  centerContentProps,
  rightContentProps,
  urgenceImportanceFilter,
  dateFilter,
  onRequestFilterImportanceUrgence,
  onRequestFilterDate,
  onRequestSearch,
  onRequestSort,
  onGoBack,
}) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.centerContent}>
        <CenterContent
          page={centerContentProps?.page}
          series={centerContentProps?.series}
          graphType={centerContentProps?.graphType}
          statuts={centerContentProps?.statuts}
          dashboards={centerContentProps?.dashboards}
          rowsTotal={centerContentProps?.rowsTotal || 12}
          onRequestChangeStatut={centerContentProps?.onRequestChangeStatut}
          onRequestCloture={centerContentProps?.onRequestCloture}
          onRequestLike={centerContentProps?.onRequestLike}
          onRequestShare={centerContentProps?.onRequestShare}
          clotureSaved={centerContentProps?.clotureSaved}
          clotureSaving={centerContentProps?.clotureSaving}
          importanceFilterProps={rightContentProps?.importanceFilterProps}
          onRequestSearch={onRequestSearch}
          onGoBack={onGoBack}
          urgenceImportanceFilter={urgenceImportanceFilter}
          onRequestFilterImportanceUrgence={onRequestFilterImportanceUrgence}
          onRequestSort={onRequestSort}
          onRequestGetItemAssocie={centerContentProps?.onRequestGetItemAssocie}
          commentListComponent={centerContentProps?.commentListComponent}
          commentComponent={centerContentProps?.commentComponent}
        />
      </Box>
      <Box className={classes.rigthContent}>
        <RightContent
          urgenceImportanceFilter={urgenceImportanceFilter}
          onRequestFilterImportanceUrgence={onRequestFilterImportanceUrgence}
          importanceFilterProps={rightContentProps?.importanceFilterProps}
          dateFilter={dateFilter}
          onRequestFilterDate={onRequestFilterDate}
          rendezVousProps={{
            data: rightContentProps?.rendezVousProps?.data || [],
            error: rightContentProps?.rendezVousProps?.error,
            loading: rightContentProps?.rendezVousProps?.loading,
            onRequestSearchRendezVous:
              rightContentProps?.rendezVousProps?.onRequestSearchRendezVous,
            onRequestSave: rightContentProps?.rendezVousProps?.onRequestSave,
            nombreRdv: rightContentProps?.rendezVousProps?.nombreRdv,
          }}
        />
      </Box>
    </Box>
  );
};
