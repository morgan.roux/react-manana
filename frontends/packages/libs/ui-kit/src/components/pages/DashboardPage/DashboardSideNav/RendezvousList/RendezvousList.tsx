import { Box, Typography } from '@material-ui/core';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
// import { RendezVousRequestSavingProps } from '../../../LaboratoirePage/SideNavList/About/RendezVousForm/interface';
// import { RendezVousForm } from '../../../LaboratoirePage/SideNavList/About/RendezVousForm';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import useStyles from './style';
import { PRTRendezVous } from '@lib/types';
// import MoreHoriz from '@material-ui/icons/MoreHoriz';
// import { MenuRdv } from './MenuRdv';
// import { ConfirmDeleteDialog } from '../../../../molecules';
import InfiniteScroll from 'react-infinite-scroll-component';
import { ErrorPage } from '../../../ErrorPage';
import { RendezFormReporter } from './RendezFormReporter';

export interface OnRequestNextRdvProps {
  take: number;
  skip: number;
}

export interface RendezVousListProps {
  rendezVous?: PRTRendezVous[];
  nombreRdv?: number;
  error?: Error;
  loading?: boolean;
  // collaborateurComponent?: ReactNode;
  // inviteComponent?: ReactNode;
  // selectedParticipants?: any[];
  // selectedInvites?: any[];
  // onRequestParticipantInviteEdit?: (participants: any, invites: any) => void;
  // onRequestDelete?: (data: PRTRendezVous) => void;
  // onRequestCancel?: (data: PRTRendezVous) => void;
  onRequestSearch?: (data: OnRequestNextRdvProps) => void;
  onRequestSave?: (data: PRTRendezVous) => void;
}

const RendezVousList: FC<RendezVousListProps> = ({
  rendezVous,
  nombreRdv,
  onRequestSearch,
  loading,
  error,
  onRequestSave,
  // onRequestSaveRendezVous,
  // collaborateurComponent,
  // inviteComponent,
  // selectedParticipants,
  // selectedInvites,
  // onRequestParticipantInviteEdit,
  // onRequestCancel,
  // onRequestDelete,
  // onRequestNextRdv,
}) => {
  const classes = useStyles();
  const [paging, setPaging] = useState<OnRequestNextRdvProps>({
    skip: 0,
    take: 12,
  });
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [valueToEdit, setValueToEdit] = useState<PRTRendezVous | undefined>();

  const handleOpenModal = () => {
    setOpenModal(prev => !prev);
  };

  useEffect(() => {
    if (onRequestSearch) onRequestSearch(paging);
  }, [paging]);

  const handleSave = (data: PRTRendezVous) => {
    if (onRequestSave) onRequestSave(data);
    handleOpenModal();
  };

  // const [openFormRdv, setOpenFormRdv] = useState<boolean>(false);
  // const [rdvEdit, setRdvEdit] = useState<PRTRendezVous | undefined>();
  // const [typeConfirmDialog, setTypeConfirmDialog] = useState<string>('');
  // const [openConfirmDialog, setOpenConfirmDialog] = useState<boolean>(false);
  // const [modeRdv, setModeRdv] = useState<'creation' | 'modification' | 'report'>('creation');
  // const [skip, setSkip] = useState<number>(0);
  // const [take, setTake] = useState<number>(12);

  // useEffect(() => {
  //   if (onRequestNextRdv) onRequestNextRdv({ skip, take });
  // }, [skip, take]);

  // const handleClickOpenAction = (
  //   event: React.MouseEvent<HTMLElement, MouseEvent>,
  //   itemChoosen: PRTRendezVous,
  // ) => {
  //   event.stopPropagation();
  //   setAnchorEl(event.currentTarget); // open Menu
  //   setRdvEdit(itemChoosen);
  // };

  // const onCloseMenu = () => {
  //   setAnchorEl(null);
  // };

  // const handleDeleteClick = () => {
  //   setTypeConfirmDialog('delete');
  //   setOpenConfirmDialog(true);
  //   setAnchorEl(null); // close Menu
  // };

  // const handleEditClick = () => {
  //   setAnchorEl(null); // close Menu
  //   rdvEdit && setRdvEdit(rdvEdit);
  //   setModeRdv('modification');
  //   setOpenFormRdv(true);
  //   onRequestParticipantInviteEdit &&
  //     onRequestParticipantInviteEdit(rdvEdit?.participants, rdvEdit?.invites);
  // };

  // const handleReportClick = () => {
  //   setAnchorEl(null); // close Menu
  //   rdvEdit && setRdvEdit(rdvEdit);
  //   setModeRdv('report');
  //   setOpenFormRdv(true);
  //   onRequestParticipantInviteEdit &&
  //     onRequestParticipantInviteEdit(rdvEdit?.participants, rdvEdit?.invites);
  // };

  // const handleCancelClick = () => {
  //   setTypeConfirmDialog('cancel');
  //   setOpenConfirmDialog(true);
  //   setAnchorEl(null); // close Menu
  // };

  // const handleConfirmDeleteRdv = () => {
  //   rdvEdit && onRequestDelete && onRequestDelete(rdvEdit);
  //   setOpenConfirmDialog(false);
  // };

  // const handleConfirmCancelRdv = () => {
  //   setOpenConfirmDialog(false);
  //   rdvEdit && onRequestCancel && onRequestCancel(rdvEdit);
  // };
  const handleClickRendezVous = (data: PRTRendezVous) => {
    setValueToEdit(data);
    setOpenModal(true);
  };

  const handleNextInfiniteScroll = () => {
    setPaging(prev => ({ ...prev, skip: prev.skip + 12 }));
  };

  return (
    <>
      <Box display="flex" alignItems="center">
        <Box>
          <Typography className={classes.textProchain}>Mes prochains RDV: {}</Typography>
        </Box>
        <Box>
          <Typography className={classes.nbr}>
            {(nombreRdv || 0) < 10 ? `0` : ''}
            {nombreRdv}
          </Typography>
        </Box>
      </Box>
      <Box pt={1} id="infinity-rdv" className={classes.infiniteScrollContainer}>
        <InfiniteScroll
          dataLength={(rendezVous || []).length}
          next={handleNextInfiniteScroll}
          hasMore
          loader={<></>}
          scrollableTarget="infinity-rdv"
        >
          {error ? (
            <ErrorPage />
          ) : (
            <>
              {loading ? (
                <h4 style={{ fontFamily: 'Roboto' }}>Chargement en cours...</h4>
              ) : (
                <>
                  {(rendezVous || []).map((rdv, index) => (
                    <Box
                      key={`RendezVous${rdv.partenaireType}${index}`}
                      display="flex"
                      flexDirection="row"
                      alignItems="center"
                      className={classes.List}
                      onClick={event => {
                        event.preventDefault();
                        event.stopPropagation();
                        handleClickRendezVous(rdv);
                      }}
                    >
                      <Box marginLeft={2} marginRight={1}>
                        <RadioButtonUncheckedIcon />
                      </Box>
                      <Box display="flex" flexDirection="column" padding={1} mr={5}>
                        <Box display="flex" flexDirection="row">
                          {(rdv?.invites?.length || 0) > 0 && (
                            <Typography className={classes.subtitle}>
                              {rdv.partenaireType}&nbsp;{rdv.invites[0].civilite}&nbsp;
                              {rdv.invites[0].prenom}&nbsp;{rdv.invites[0].nom}
                            </Typography>
                          )}
                          {/* <Box
                            onClick={(event) => {
                              event.preventDefault();
                              event.stopPropagation();
                              handleClickOpenAction(event, rdv);
                            }}
                          >
                            <MoreHoriz />
                          </Box> */}
                        </Box>
                        <Box>
                          <Typography color="textSecondary" className={classes.text}>
                            {moment(rdv.dateRendezVous).format('DD/MM/YY')}, {rdv.heureDebut} -{' '}
                            {rdv.heureFin}
                          </Typography>
                        </Box>
                      </Box>
                    </Box>
                  ))}
                </>
              )}
            </>
          )}
        </InfiniteScroll>
      </Box>
      {/* <MenuRdv
          anchorEl={anchorEL}
          onClose={onCloseMenu}
          onDeleteClick={handleDeleteClick}
          onEditClick={handleEditClick}
          onCancelClick={handleCancelClick}
          onReportClick={handleReportClick}
          statutRdv={rdvEdit?.statut}
        /> */}
      {/* <ConfirmDeleteDialog
          title={
            typeConfirmDialog === 'delete'
              ? 'Suppression du rendez-vous'
              : 'Annulation du rendez-vous'
          }
          content={
            typeConfirmDialog === 'delete'
              ? 'Voulez-vous supprimez ce rendez-vous'
              : 'Voulez-vous annuler ce rendez-vous'
          }
          open={openConfirmDialog}
          setOpen={setOpenConfirmDialog}
          titleBtnAction={typeConfirmDialog === 'delete' ? 'Supprimer' : 'Confirmer'}
          onClickConfirm={
            typeConfirmDialog === 'delete' ? handleConfirmDeleteRdv : handleConfirmCancelRdv
          }
        /> */}
      <RendezFormReporter
        value={valueToEdit}
        open={openModal}
        setOpen={handleOpenModal}
        onSave={handleSave}
      />
    </>
  );
};

export default RendezVousList;
