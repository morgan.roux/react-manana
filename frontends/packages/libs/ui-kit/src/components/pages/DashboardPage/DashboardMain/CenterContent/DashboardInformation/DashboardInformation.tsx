import { Popper, Grow, ClickAwayListener } from '@material-ui/core';
import React, { FC, useEffect, MouseEvent, ReactNode } from 'react';
import { Dashboard } from '../../../types';
import Information from './Information';
import useStyles from './styles';

export interface DashboardInformationProps {
  anchorEl: any;
  dashboard: Dashboard;
  onRequestLike?: (dashboard: Dashboard) => void;
  onRequestShare?: (dashboard: Dashboard) => void;
  onRequestCloture?: (dashboard: Dashboard, commentaire: string) => void;
  clotureSaving?: boolean;
  clotureSaved?: boolean;
  onPrevStep: (e: MouseEvent<HTMLElement>) => void;
  onNextStep: (e: MouseEvent<HTMLElement>) => void;
  onFullScreen: (fullscreen: boolean) => void;
  step: number;
  stepLength: number;
  fullscreen: boolean;
  onCommentaire: (commentaire: string) => void;
  commentaire: string;
  onOpen: (close: boolean) => void;
  commentComponent?: ReactNode;
  commentListComponent?: ReactNode;
}

const DashboardInformation: FC<DashboardInformationProps> = ({
  dashboard,
  onRequestLike,
  onRequestShare,
  onRequestCloture,
  clotureSaved,
  clotureSaving,
  onNextStep,
  onPrevStep,
  step,
  stepLength,
  fullscreen,
  onFullScreen,
  commentaire,
  onOpen,
  commentComponent,
  commentListComponent,
  anchorEl,
}) => {
  const classes = useStyles();

  useEffect(() => {
    if (clotureSaved) {
      onOpen(false);
    }
  }, [clotureSaved]);

  const handleClose = () => {
    onFullScreen(false);
    onOpen(false);
  };

  return (
    <Popper
      open={Boolean(anchorEl)}
      anchorEl={anchorEl}
      role={undefined}
      transition
      disablePortal
      className={classes.popper}
    >
      {({ TransitionProps, placement }) => (
        <Grow
          {...TransitionProps}
          style={{
            transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
          }}
        >
          <ClickAwayListener onClickAway={handleClose}>
            <Information
              dashboard={dashboard}
              fullscreen={fullscreen}
              step={step}
              stepLength={stepLength}
              onRequestLike={onRequestLike}
              onRequestShare={onRequestShare}
              onPrevStep={onPrevStep}
              onNextStep={onNextStep}
              onOpen={onOpen}
              onFullScreen={onFullScreen}
              commentaire={commentaire}
              onRequestCloture={onRequestCloture}
              clotureSaving={clotureSaving}
              commentComponent={commentComponent}
              commentListComponent={commentListComponent}
            />
          </ClickAwayListener>
        </Grow>
      )}
    </Popper>
  );
};

export default DashboardInformation;
