import React from 'react';
import CalendarFilter from './CalendarFilter';

export default {
  title: 'Design system|Pages/DashboardPage/RightSidebar/CalendarFilter',
  parameters: {
    info: { inline: true },
  },
};

export const FilterCard = () => {
  return <CalendarFilter />;
};
