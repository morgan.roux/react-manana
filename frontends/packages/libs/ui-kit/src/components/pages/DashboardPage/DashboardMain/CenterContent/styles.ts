import { makeStyles, Theme, createStyles } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      width: '100%',
      scrollbarColor: theme.palette.secondary.main,
      height: 'calc(100vh - 86px)',
    },
    title: {
      fontSize: 16,
      fontWeight: 'normal',
      fontFamily: 'Roboto',
      marginTop: 16,
    },
    cursor: {
      cursor: 'pointer',
    },
    inputSelect: {
      padding: '0 25px',
    },
    tableBox: {
      paddingTop: 16,
    },
    repartitionBox: {
      width: '100%',
      minWidth: 408,
    },
    proportionBox: {
      minWidth: 128,
    },
    goBack: {
      padding: 0,
    },
    dashboard: {
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 20,
      color: '#424242',
      paddingLeft: 16,
    },
    typeGraphe: {
      paddingRight: 16,
    },
  })
);
