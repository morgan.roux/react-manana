import { IconButton } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { ParameterContainer as Container } from '../../templates/ParameterContainer';
import { CenterContentProps } from './DashboardMain/CenterContent/CenterContent';
import { DashboardMain } from './DashboardMain/DashbordMain';
import { SideNavFilter, DashboardSideNav } from './DashboardSideNav';
import { isMobile } from '../../../services/Helpers';
import { RightContentProps } from './DashboardMain/RightContent/RightContent';
import { DateFilterDashboard } from './DashboardMain/RightContent/CalendarFilter/CalendarFilter';
import {
  ImportanceDashboard,
  UrgenceDashboard,
  UrgenceImportance,
} from './DashboardMain/RightContent/ImportanceFilter/ImportanceFilter';
import { Dashboard } from './types';

export interface Fichier {
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string;
}

export interface User {
  id: string;
  fullName: string;
  email?: string;
  role?: Role;
  photo?: Fichier;
}

export interface Role {
  id: string;
  nom: string;
  code: string;
}

export interface DashboardPageProps {
  sideNavProps?: {
    filters: SideNavFilter[];
    loading?: boolean;
    error?: any;
    user: User;
    defaultFilter?: SideNavFilter;
  };
  iconFilter?: any;
  onGoBack?: () => void;
  onRequestSearch: (filter: DashboardFilter) => void;
  centerContentProps?: CenterContentProps;
  rightContentProps?: RightContentProps;
  onRequestGetItemAssocie?: (dashboard: Dashboard) => void;
}

interface Paging {
  offset: number;
  limit: number;
}

export interface DashboardFilter {
  items?: SideNavFilter[];
  importance?: ImportanceDashboard;
  searchText?: string;
  urgence?: UrgenceDashboard;
  date?: Date;
  dateDebut?: Date;
  dateFin?: Date;
  paging: Paging;
  order?: {
    field: string;
    direction: 'DESC' | 'ASC';
  };
}

const DEFAULT_PAGING = {
  offset: 0,
  limit: 12,
};

export const DashboardPage: FC<DashboardPageProps> = ({
  sideNavProps,
  onGoBack,
  onRequestSearch,
  centerContentProps = {},
  rightContentProps,
  iconFilter,
}) => {
  const [openSideNav, setOpenSideNav] = useState<boolean>(false);
  const [itemsChecked, setItemsChecked] = useState<string[]>([]);
  const [filter, setFilter] = useState<DashboardFilter>({
    paging: DEFAULT_PAGING,
  });

  const handleDrawerToggle = () => {
    setOpenSideNav((prev) => !prev);
  };

  useEffect(() => {
    if (isMobile()) handleDrawerToggle();
  }, []);

  useEffect(() => {
    onRequestSearch(filter);
  }, [filter]);

  const optionBtn = (
    <IconButton onClick={handleDrawerToggle}>
      <img src={iconFilter} alt="filterIcon" />
    </IconButton>
  );

  const handleChangeFilterSideNav = (sideNavFilters: SideNavFilter[]) => {
    setFilter((prev) => ({ ...prev, items: sideNavFilters, paging: DEFAULT_PAGING }));
  };

  const handleFilterDate = ({ date, dateFin, dateDebut }: DateFilterDashboard) => {
    setFilter((prev) => ({ ...prev, date, dateDebut, dateFin, paging: DEFAULT_PAGING }));
  };

  const handleRequestFilterImportanceUrgence = (data: UrgenceImportance | undefined) => {
    if (data) {
      setFilter((prev) => ({
        ...prev,
        urgence: data.urgence,
        importance: data.importance,
        paging: DEFAULT_PAGING,
      }));
    } else {
      setFilter((prev) => ({
        ...prev,
        urgence: undefined,
        importance: undefined,
        paging: DEFAULT_PAGING,
      }));
    }
  };

  const handleRequestSearch = (skip: number, take: number, searchText: string) => {
    setFilter((prev) => ({ ...prev, searchText, paging: { offset: skip, limit: take } }));
  };

  const handleRequestSort = (field: string, direction: 'ASC' | 'DESC') => {
    setFilter((prev) => ({ ...prev, order: { direction, field } }));
  };

  const handleResetFilters = () => {
    setFilter((prev) => ({
      ...prev,
      urgence: undefined,
      importance: undefined,
      items: [],
      date: undefined,
      dateDebut: undefined,
      dateFin: undefined,
      searchText: '',
      paging: { offset: 0, limit: 12 },
    }));
    setItemsChecked([]);
  };

  return (
    <Container
      handleDrawerToggle={handleDrawerToggle}
      openDrawer={openSideNav}
      optionBtn={[optionBtn]}
      banner={false}
      tab={false}
      onGoBack={onGoBack}
      drawerTitle="Mon Dashboard d’Activité"
      sideNavCustomContent={
        <DashboardSideNav
          itemsChecked={itemsChecked}
          onChangeItemsChecked={setItemsChecked}
          onResetFilters={handleResetFilters}
          onGoBack={onGoBack}
          filters={sideNavProps?.filters || []}
          onChange={handleChangeFilterSideNav}
          username={sideNavProps?.user.fullName || ''}
          error={sideNavProps?.error}
          loading={sideNavProps?.loading}
          onClose={handleDrawerToggle}
          defaultFilter={sideNavProps?.defaultFilter}
        />
      }
      mainContent={
        <DashboardMain
          centerContentProps={
            {
              ...centerContentProps,
              page: filter.paging.offset / filter.paging.limit,
            } as any
          }
          rightContentProps={rightContentProps}
          dateFilter={filter as any}
          onRequestFilterDate={handleFilterDate}
          urgenceImportanceFilter={filter as any}
          onRequestFilterImportanceUrgence={handleRequestFilterImportanceUrgence}
          onRequestSearch={handleRequestSearch}
          onRequestSort={handleRequestSort}
          onGoBack={onGoBack}
        />
      }
    />
  );
};
