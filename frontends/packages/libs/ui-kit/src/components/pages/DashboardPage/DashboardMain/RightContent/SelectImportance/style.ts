import { createStyles, makeStyles } from '@material-ui/core';
// import GREY from '@material-ui/core/colors/grey';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      borderRadius: 4,
      width: '100%',
      cursor: 'pointer',
    },
    subtitle: {
      color: '#FFFFFF',
      textAlign: 'center',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 12,
      padding: 8,
    },
    text: {
      textAlign: 'center',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 12,
      padding: 8,
    },
    importanceBox: {
      height: 72,
    },
  })
);

export default useStyles;
