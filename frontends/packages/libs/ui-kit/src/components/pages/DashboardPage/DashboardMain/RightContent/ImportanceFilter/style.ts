import { createStyles, makeStyles } from '@material-ui/core';
// import GREY from '@material-ui/core/colors/grey';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      borderRadius: 4,
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    subtitle: {
      color: '#FFFFFF',
      textAlign: 'center',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 12,
      padding: 8,
    },
    text: {
      textAlign: 'center',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 12,
      padding: 8,
    },
  })
);

export default useStyles;
