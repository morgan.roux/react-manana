import { makeStyles, createStyles, Theme } from '@material-ui/core';

const style = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: 24,
      background: 'white',
    },
    popper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      width: 750,
      overflow: 'auto',
      marginTop: 90,
      marginRight: 142,
      marginLeft: 193,
      marginBottom: 80,
      boxShadow: '0px 8px 16px #4040401A',
    },
    gridCursor: {
      position: 'absolute',
      top: 22,
      right: 75,
    },
    cursorLink: {
      padding: '0 10px',
      cursor: 'pointer',
      ' &:hover': {
        textDecoration: 'none',
      },
    },
    listItem: {
      padding: 0,
    },
    gridItem: {
      padding: '10px 0',
      display: 'block',
    },
    subtitle1: {
      color: '#9E9E9E',
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 12,
      [theme.breakpoints.between('xs', 'sm')]: {
        marginTop: 5,
      },
    },
    text1: {
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 12,
      verticalAlign: 'middle',
    },
    subtitle2: {
      color: '#424242',
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 20,
    },
    span: {
      color: 'black',
    },
    btnDashed: {
      border: '1px dashed #1D1D1D',
      paddingRight: 100,
      [theme.breakpoints.between('xs', 'sm')]: {
        paddingRight: 10,
        margin: '10px 5px 10px 0px',
      },
    },
    editor: {
      [theme.breakpoints.between('xs', 'sm')]: {
        width: '100%',
      },
    },
    avatar: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
    footer: {
      padding: '0',
    },
    titleTab: {
      fontSize: 16,
      textTransform: 'none',
      [theme.breakpoints.down('sm')]: {
        fontSize: 14,
      },
    },
    origineDetail: {
      background: '#F8F8F8',
    },
    detail: {
      background: '#F8F8F8',
      paddingTop: 8,
      paddingBottom: 8,
    },
    text2: {
      color: '#424242',
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 16,
    },
    textDate: {
      color: '#212121',
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 16,
      paddingTop: 8,
      paddingBottom: 8,
    },
    text3: {
      color: '#9E9E9E',
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 12,
      paddingRight: 8,
      paddingTop: 16,
      paddingBottom: 16,
    },
    textLike: {
      color: '#424242',
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 14,
      paddingLeft: 16,
    },
    likeBox: {
      borderTop: 'solid 1px #e0e0e0',
      borderBottom: 'solid 1px #e0e0e0',
      marginBottom: 16,
    },
  })
);

export default style;
