import React from 'react';
import ImportanceFilter from './ImportanceFilter';

export default {
  title: 'Design system|Pages/DashboardPage/ImportanceFilter',
  parameters: {
    info: { inline: true },
  },
};

export const Filter = () => {
  return <ImportanceFilter />;
};
