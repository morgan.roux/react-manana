import { Grid, IconButton, MenuItem, Select, Typography, Box } from '@material-ui/core';
import React, { FC, useEffect, useState, MouseEvent, useRef, ReactNode, CSSProperties } from 'react';
import { Column, CustomAvatarGroup, ImportanceLabel, Table, TableChange, UrgenceLabel } from '../../../../atoms';
import { isMobile, strippedColumnStyle, strippedString } from '../../../../../services/Helpers';

import { SeriesItem, DashboardRepartition } from '../../DashboardRepartition';
import { Dashboard, DashboardStatut, Item } from '../../types';
import { useStyles } from './styles';
import DashboardInformation from './DashboardInformation/DashboardInformation';
import NotificationImportantIcon from '@material-ui/icons/NotificationImportant';
import ImportanceFilter, {
  combinaisonColor,
  ImportanceFilterProps,
  UrgenceImportance,
} from '../RightContent/ImportanceFilter/ImportanceFilter';
import ArrowBack from '@material-ui/icons/ArrowBack';
import { DashboardProportion } from '../../DashboardProportion';
import Information from './DashboardInformation/Information';
// import { DashboardProportion } from '../../DashboardProportion';

export interface Statuts {
  error?: boolean | undefined;
  loading?: boolean;
  data: DashboardStatut[];
}

const COLORS = ['#673ab7', '#009688', '#ff5722'];

function getWindowDimensions(): 'isLg' | 'isMd' | 'isSm' | 'unknown' {
  const { innerWidth: width } = window;
  if (width >= 1280) {
    return 'isLg';
  }
  if (width >= 960 && width < 1280) {
    return 'isMd';
  } else if (width < 960) {
    return 'isSm';
  }
  return 'unknown';
}

const useWindowDimensions = () => {
  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return windowDimensions;
};

export interface CenterContentProps {
  page?: number;
  series?: SeriesItem[];
  graphType?: 'pie' | 'donut';
  statuts?: Statuts;
  dashboards?: {
    loading?: boolean;
    error?: boolean | undefined;
    data?: Dashboard[];
  };
  rowsTotal: number;
  clotureSaving?: boolean;
  clotureSaved?: boolean;
  urgenceImportanceFilter?: UrgenceImportance;
  onRequestFilterImportanceUrgence?: (filter: UrgenceImportance | undefined) => void;
  importanceFilterProps?: ImportanceFilterProps;
  onGoBack?: () => void;
  onRequestLike?: (dashboard: Dashboard) => void;
  onRequestShare?: (dashboard: Dashboard) => void;
  onRequestSort?: (column: string, direction: 'DESC' | 'ASC') => void;
  onRequestChangeStatut?: (id: string, value: string, item: Item) => void;
  onRequestCloture?: (dashboard: Dashboard, commentaire: string) => void;
  onRequestSearch?: (skip: number, take: number, searchText: string) => void;
  onRequestGetItemAssocie?: (dashboard: Dashboard) => void;
  commentListComponent?: ReactNode;
  commentComponent?: ReactNode;
}

export const CenterContent: FC<CenterContentProps> = ({
  page,
  series,
  statuts,
  dashboards,
  rowsTotal,
  clotureSaved,
  clotureSaving,
  onGoBack,
  onRequestLike,
  onRequestShare,
  onRequestChangeStatut,
  onRequestCloture,
  onRequestSearch,
  importanceFilterProps,
  urgenceImportanceFilter,
  onRequestFilterImportanceUrgence,
  onRequestSort,
  onRequestGetItemAssocie,
  commentComponent,
  commentListComponent,
}) => {
  const classes = useStyles();

  const [openInfo, setOpenInfo] = useState<boolean>(false);
  const [dashboard, setDashboard] = useState<Dashboard>();
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(10);
  const [searchText, setSearchText] = useState<string>('');
  const grapheType = 'donut';
  const showProportion = true;
  const [stepInformation, setStepInformation] = useState<number>(1);
  const [fullScreen, setFullScreen] = useState<boolean>(false);
  const [commentaire, setCommentaire] = useState<string>('');
  const prevOpenInfo = useRef(openInfo);
  const anchorRef = useRef<any>(null);
  const [anchorEl, setAnchotEl] = useState<any>(null);
  const dimensions = useWindowDimensions();

  useEffect(() => {
    onRequestSearch && onRequestSearch(skip, take, searchText);
  }, [skip, take, searchText]);

  useEffect(() => {
    setDashboard(dashboards?.data ? dashboards?.data[stepInformation - 1] : undefined);
  }, [stepInformation]);

  useEffect(() => {
    if (!openInfo) {
      setCommentaire('');
    }

    if (openInfo) {
      onRequestGetItemAssocie && dashboard && onRequestGetItemAssocie(dashboard);
    }

    if (prevOpenInfo.current === true && openInfo === false) {
      // eslint-disable-next-line no-unused-expressions
      anchorRef.current?.focus();
    }
    prevOpenInfo.current = openInfo;
  }, [openInfo]);

  const handleNextStepInformation = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault();
    if (stepInformation >= rowsTotal) {
      // dashboards?.data && setDashboard(dashboards?.data[stepInformation - 1]);
      setStepInformation(1);
    } else {
      // dashboards?.data && setDashboard(dashboards?.data[stepInformation + 1]);
      setStepInformation((prev) => prev + 1);
    }
  };

  const handlePrevStepInformation = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault();
    if (stepInformation === 1) {
      // dashboards?.data && setDashboard(dashboards?.data[stepInformation + 1]);
      setStepInformation(rowsTotal);
    } else {
      // dashboards?.data && setDashboard(dashboards?.data[stepInformation - 1]);
      setStepInformation((prev) => prev - 1);
    }
  };

  const handleFullScreen = (fullscreen: boolean) => {
    setFullScreen(fullscreen);
  };

  const handleCommentaire = (commentaire: string) => {
    setCommentaire(commentaire);
  };

  const handleOpenInformation = (__open: boolean) => {
    setAnchotEl(null);
  };

  const openInformation = (e: MouseEvent<HTMLElement>, newDashboard: Dashboard, index: number | undefined) => {
    if (!isMobile()) {
      setAnchotEl(e.currentTarget);
      setDashboard(newDashboard);
      setOpenInfo((prev) => !prev);
      setStepInformation(index ? index + 1 : 1);
    }
  };

  const handleSearch = ({ skip, take, searchText }: TableChange) => {
    setSkip(skip);
    setTake(take);
    setSearchText(searchText);
  };

  interface SelectCompProps {
    id: string;
    value: string;
    statutList: DashboardStatut[];
    item: Item;
    onChange: (id: string, value: string, item: Item) => void;
  }

  const SelectComp: FC<SelectCompProps> = ({ value, id, onChange, statutList, item }) => {
    const [idCurrent, setIdCurent] = useState<string | undefined>();

    useEffect(() => {
      setIdCurent(id);
    }, [id]);

    const handleChange = (
      event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
      }>,
      __child: React.ReactNode
    ) => {
      event.stopPropagation();
      event.preventDefault();
      if (idCurrent) onChange(idCurrent, event.target.value as string, item);
    };

    return (
      <>
        <Select style={{ minWidth: 120 }} variant="standard" value={value} onChange={handleChange} displayEmpty>
          {statutList.map(({ code, libelle }) => (
            <MenuItem key={code} value={code}>
              {libelle}
            </MenuItem>
          ))}
        </Select>
      </>
    );
  };

  const handleChangeStatut = (id: string, value: string, item: Item) => {
    if (onRequestChangeStatut) onRequestChangeStatut(id, value, item);
  };

  const headerColumnStyle: CSSProperties = {
    textAlign: 'center',
  };

  const columns: Column[] = [
    {
      name: 'source',
      label: 'Source',
      sortable: true,
      renderer: (row: Dashboard, index?: number) => {
        return (
          <Grid
            ref={anchorRef}
            onClick={(event) => openInformation(event, row, index)}
            aria-haspopup="true"
            aria-controls="dashboard-menu"
          >
            <Box className={classes.cursor}>{row.item.name}</Box>
          </Grid>
        );
      },
      headerStyle: headerColumnStyle,
    },
    {
      name: 'urgence',
      label: 'Urgence',
      sortable: true,
      renderer: (row: Dashboard, index?: number) => {
        return (
          <Grid
            ref={anchorRef}
            onClick={(event) => openInformation(event, row, index)}
            aria-haspopup="true"
            aria-controls="dashboard-menu"
          >
            <Box className={classes.cursor}>
              <UrgenceLabel urgence={row?.urgence} />
            </Box>
          </Grid>
        );
      },
      headerStyle: headerColumnStyle,
    },
    {
      name: 'importance',
      label: 'Importance',
      sortable: true,
      renderer: (row: Dashboard, index?: number) => {
        return (
          <Grid
            ref={anchorRef}
            onClick={(event) => openInformation(event, row, index)}
            aria-haspopup="true"
            aria-controls="dashboard-menu"
          >
            <Box className={classes.cursor}>
              <ImportanceLabel importance={row?.importance as any} />
            </Box>
          </Grid>
        );
      },
      headerStyle: headerColumnStyle,
    },
    {
      name: 'detail',
      label: 'Détail',
      renderer: (row: Dashboard, index?: number) => {
        return (
          <Grid
            ref={anchorRef}
            onClick={(event) => openInformation(event, row, index)}
            aria-haspopup="true"
            aria-controls="dashboard-menu"
          >
            <Box className={classes.cursor}>{row.description ? strippedString(row.description) : '-'}</Box>
          </Grid>
        );
      },
      style: strippedColumnStyle,
      headerStyle: headerColumnStyle,
    },

    {
      name: 'equipe',
      label: 'Equipe',
      renderer: (row: Dashboard, index?: number) => {
        return (
          <Grid
            ref={anchorRef}
            onClick={(event) => openInformation(event, row, index)}
            aria-haspopup="true"
            aria-controls="dashboard-menu"
          >
            <Box display="flex" flexDirection="row" justifyContent="flex-start" className={classes.cursor}>
              <CustomAvatarGroup max={4} sizes="30px" users={(row.participants || []) as any} />
            </Box>
          </Grid>
        );
      },
      headerStyle: headerColumnStyle,
    },
    {
      name: 'statut',
      label: 'Statut',
      sortable: true,
      renderer: (row: Dashboard) => {
        return (
          <SelectComp
            id={row.idItemAssocie || ''}
            value={row.statut}
            statutList={statuts?.data || []}
            onChange={handleChangeStatut}
            item={row.item}
          />
        );
      },
      headerStyle: headerColumnStyle,
    },
    {
      name: 'retard',
      sortable: true,
      label: 'En retard',
      renderer: (row: Dashboard, index?: number) => {
        return (
          <Grid
            ref={anchorRef}
            onClick={(event) => openInformation(event, row, index)}
            aria-haspopup="true"
            aria-controls="dashboard-menu"
          >
            <Box className={classes.cursor} style={{ textAlign: 'center' }}>
              {row?.nombreJoursRetard > 0 ? (
                <>
                  <NotificationImportantIcon color="secondary" />({row.nombreJoursRetard})
                </>
              ) : (
                ''
              )}
            </Box>
          </Grid>
        );
      },
      headerStyle: headerColumnStyle,
    },

    {
      name: 'score',
      sortable: true,
      label: 'Score',
      headerStyle: headerColumnStyle,
      style: { textAlign: 'center' },
    },
  ];

  return (
    <Box>
      {dimensions === 'isLg' && (
        <Box display="flex" alignItems="center">
          <IconButton classes={{ root: classes.goBack }} onClick={onGoBack}>
            <ArrowBack />
          </IconButton>
          <Typography className={classes.dashboard}>Mon Dashboard d’Activité</Typography>
        </Box>
      )}
      {series && series.length > 0 && series[0].valeur !== 0 && !fullScreen ? (
        <Box>
          <Grid container spacing={2} alignItems="flex-end">
            <Grid xs={12} md item className={isMobile() ? undefined : classes.repartitionBox}>
              <DashboardRepartition
                series={(series || []).map((serie, index) => ({
                  ...serie,
                  couleur: COLORS[index % COLORS.length],
                }))}
                graphType={grapheType || 'pie'}
                showProportion={showProportion}
              />
            </Grid>

            {showProportion && (
              <Grid item xs={12} md style={{ minWidth: 128 * series.length }}>
                <Grid container spacing={2}>
                  {series.map((serie, index) => (
                    <Grid item xs={12} md className={classes.proportionBox} key={`proportion${serie.categorie}`}>
                      <DashboardProportion
                        categorie={serie.categorie}
                        valeur={serie.valeur}
                        color={COLORS[index % COLORS.length]}
                      />
                    </Grid>
                  ))}
                </Grid>
              </Grid>
            )}
          </Grid>
          {/* <Box pl={3} display="flex" alignItems="center">
            <Typography className={classes.typeGraphe}>Type de graphe: </Typography>
            <Select
              labelId="select-graph-type"
              id="select-graph"
              value={grapheType}
              onChange={(event) => {
                setGrapheType(event.target.value as any);
              }}
            >
              <MenuItem value="pie">Disque</MenuItem>
              <MenuItem value="donut">Anneau</MenuItem>
            </Select>
          </Box> */}
          {/* <Box pl={3}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={showProportion}
                  color="secondary"
                  onChange={(event) => {
                    setShowProportion(event.target.checked);
                  }}
                />
              }
              label="Afficher les proportions"
            />
          </Box> */}
        </Box>
      ) : (
        ''
      )}
      {dimensions === 'isSm' && (
        <Box py={2}>
          <ImportanceFilter {...importanceFilterProps} onRequestFilter={onRequestFilterImportanceUrgence} />
        </Box>
      )}

      {urgenceImportanceFilter?.importance && urgenceImportanceFilter.urgence && (
        <Typography
          style={{
            textAlign: 'center',
            marginTop: 6,
            fontWeight: 500,
          }}
        >
          Importance {urgenceImportanceFilter.importance.libelle}{' '}
          {urgenceImportanceFilter.urgence.code === 'C' ? 'du' : 'de la'} {urgenceImportanceFilter.urgence.libelle}
        </Typography>
      )}

      <>
        <Box style={{ display: fullScreen ? 'none' : 'block' }} className={classes.tableBox}>
          <Table
            page={page}
            error={dashboards?.error as Error | undefined}
            loading={dashboards?.loading}
            search={false}
            data={(dashboards?.data || []).map((item) => {
              const __combinaisonColor = combinaisonColor as any;
              const couleur = __combinaisonColor[`${item.importance.ordre}${item.urgence.code}`]
                ? __combinaisonColor[`${item.importance.ordre}${item.urgence.code}`].color
                : '#000000';
              return {
                ...item,
                urgence: { ...item.urgence, couleur },
                importance: { ...item.importance, couleur },
              };
            })}
            selectable={false}
            columns={columns}
            rowsTotal={rowsTotal}
            onRunSearch={handleSearch}
            onSortColumn={onRequestSort}
          />
        </Box>
        {dashboard && (
          <DashboardInformation
            anchorEl={anchorEl}
            onOpen={handleOpenInformation}
            dashboard={dashboard}
            onRequestLike={onRequestLike}
            onRequestShare={onRequestShare}
            onRequestCloture={onRequestCloture}
            clotureSaved={clotureSaved}
            clotureSaving={clotureSaving}
            onPrevStep={handlePrevStepInformation}
            onNextStep={handleNextStepInformation}
            step={stepInformation}
            stepLength={rowsTotal}
            onFullScreen={handleFullScreen}
            fullscreen={fullScreen}
            onCommentaire={handleCommentaire}
            commentaire={commentaire}
            commentComponent={commentComponent}
            commentListComponent={commentListComponent}
          />
        )}
      </>
      {fullScreen && dashboard && (
        <Information
          fullscreen={fullScreen}
          step={stepInformation}
          stepLength={rowsTotal}
          onRequestLike={onRequestLike}
          onRequestShare={onRequestShare}
          onPrevStep={handlePrevStepInformation}
          onNextStep={handleNextStepInformation}
          onOpen={handleOpenInformation}
          onRequestCloture={onRequestCloture}
          onFullScreen={handleFullScreen}
          dashboard={dashboard}
          commentaire={commentaire}
          clotureSaving={clotureSaving}
          commentComponent={commentComponent}
          commentListComponent={commentListComponent}
        />
      )}
    </Box>
  );
};
