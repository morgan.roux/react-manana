import React, { FC } from 'react';
import { isMobile } from '../../../../services/Helpers';

import { Chart, ChartLegend, ChartSeries, ChartSeriesItem } from '@progress/kendo-react-charts';

const labelContent = (props: any) => {
  const formatedNumber = Number(props.dataItem.valeur).toLocaleString(undefined, {
    style: 'percent',
    minimumFractionDigits: 2,
  });
  return `${isMobile() ? '' : `${props.dataItem.categorie}:`} ${formatedNumber}`;
};
export interface SeriesItem {
  categorie: string;
  valeur: number;
  couleur?: string
}
export interface DashboardRepartitionProps {
  series: SeriesItem[];
  graphType: 'pie' | 'donut';
  showProportion: boolean;
}

const DashboardRepartition: FC<DashboardRepartitionProps> = ({
  series,
  graphType,
  showProportion,
}) => {
  return (
    <Chart style={showProportion && !isMobile() ? { height: 140, minWidth: 400 } : undefined}>
      <ChartLegend position={isMobile() ? 'bottom' : 'right'} orientation="vertical" />
      <ChartSeries>
        <ChartSeriesItem
          type={graphType}
          data={series}
          field="valeur"
          colorField="couleur"
          categoryField="categorie"
          labels={{ visible: !showProportion, content: labelContent, position: 'above' }}
        />
      </ChartSeries>
    </Chart>
  );
};

export default DashboardRepartition;
