import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    date: {
      marginTop: 24,
      boxShadow: '2px 2px 5px 0px #565656',
      borderRadius: 4,
      overflow: 'hidden',
      '& .MuiPickersStaticWrapper-staticWrapperRoot': {
        width: '100% !important',
      },
      '& .MuiPickersBasePicker-pickerView': {
        maxWidth: '100%',
        minWidth: '100%',
        padding: 8,
        boxShadow: '2px 2px 5px black',
      },
      '& .MuiPickersDatePickerRoot-toolbar': {
        display: 'none',
      },
      '& .MuiPickersCalendarHeader-daysHeader': {
        justifyContent: 'space-between !important',
      },
      '& .MuiPickersCalendar-week': {
        justifyContent: 'space-between !important',
      },
    },
    datePicker: {
      '& .main-MuiOutlinedInput-adornedEnd': {
        padding: 0,
      },
      '& input': {
        padding: '14.5px 12px 14.5px 12px',
      },
    },
    text: {
      textAlign: 'center',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 12,
      padding: 8,
    },
  })
);
