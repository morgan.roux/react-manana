import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      marginTop: 0,
      [theme.breakpoints.up('md')]: {
        marginTop: 50,
      },
      [theme.breakpoints.up('lg')]: {
        marginTop: 0,
      },
    },
    centerContent: {
      height: 'calc(100vh - 86px)',
      width: '100%',
      padding: '16px 16px 32px 16px',
      [theme.breakpoints.up('md')]: {
        width: 'calc(100% - 375px)',
        padding: '24px 24px 0 24px',
      },
      overflowY: 'scroll',
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
    rigthContent: {
      right: 16,
      width: '100%',
      padding: 16,
      height: 'calc(100vh - 86px)',
      [theme.breakpoints.up('md')]: {
        width: 375,
      },
      overflowY: 'scroll',
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
    calendar: {
      marginTop: 24,
    },
    rendezVous: {
      marginTop: 40,
    },
  })
);
