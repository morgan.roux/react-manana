import { Importance, Urgence, User } from '@lib/types';

export interface DashboardStatut {
  id: string;
  code: string;
  libelle: string;
}

export interface Fournisseur {
  id?: string;
  nom: string;
}

export interface Prestataire {
  id?: string;
  nom: string;
}

export interface Groupe {
  id?: string;
  nom: string;
}

export interface Item {
  id: string;
  code: string;
  codeItem: string;
  name: string;
}

export interface Dashboard {
  id?: string;
  item: Item;
  idItemAssocie: string;
  dateEcheance: Date;
  urgence: Urgence;
  importance: Importance;
  participants: User[];
  nombreJoursRetard: number;
  statut: string;
  createdAt?: Date;
  description?: string;
}
