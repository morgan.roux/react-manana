import { Grid } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import SelectImportance from '../SelectImportance/SelectImportance';
import useStyles from './style';
import { ErrorPage } from '../../../../../pages';
import { Loader } from '../../../../../atoms';

export interface ImportanceDashboard {
  id: string;
  order: number;
  libelle: string;
}

export interface UrgenceDashboard {
  id: string;
  code: string;
  libelle: string;
}

export interface UrgenceImportance {
  urgence: UrgenceDashboard;
  importance: ImportanceDashboard;
  nombreTache: number;
  couleur: string;
}

export interface NbrTachesUrgenceImportance {
  hauteJournee: number;
  hauteMois: number;
  hauteSemaine: number;
  moyenneJournee: number;
  moyenneMois: number;
  moyenneSemaine: number;
}

export const combinaisonColor = {
  '1A': { color: '#f61902', key: 'hauteJournee' },
  '1B': { color: '#3133cb', key: 'hauteMois' },
  '1C': { color: '#43ce36', key: 'hauteSemaine' },
  '2A': { color: '#faa015', key: 'moyenneJournee' },
  '2B': { color: '#9f15f5', key: 'moyenneMois' },
  '2C': { color: '#3acff7', key: 'moyenneSemaine' },
};

export interface ImportanceFilterProps {
  urgencesImportances?: {
    urgences: UrgenceDashboard[];
    importances: ImportanceDashboard[];
  };
  nombreTaches?: NbrTachesUrgenceImportance;
  loading?: boolean;
  error?: Error;
  urgenceImportanceFilter?: UrgenceImportance;
  onRequestFilter?: (urgenceImportance: UrgenceImportance | undefined) => void;
}

const ImportanceFilter: FC<ImportanceFilterProps> = ({
  onRequestFilter,
  urgenceImportanceFilter,
  urgencesImportances,
  nombreTaches,
  loading,
  error,
}) => {
  const classes = useStyles();
  const [data, setData] = useState<UrgenceImportance[]>([]);

  useEffect(() => {
    const _data: UrgenceImportance[] = [];
    const _combinaison = combinaisonColor as any;
    const _nombreTaches = nombreTaches as any;
    (urgencesImportances?.importances || []).map((importance) => {
      (urgencesImportances?.urgences || []).map((urgence) => {
        _data.push({
          urgence,
          importance,
          nombreTache: _nombreTaches
            ? _combinaison[`${importance.order}${urgence.code}`].key in _nombreTaches
              ? _nombreTaches[_combinaison[`${importance.order}${urgence.code}`].key] || 0
              : 0
            : 0,
          couleur: _combinaison[`${importance.order}${urgence.code}`].color,
        });
      });
    });
    setData(_data);
  }, [urgencesImportances, nombreTaches]);

  const handleClickUrgenceImportance = (urgenceImportance: UrgenceImportance) => {
    if (!urgenceImportance) {
      onRequestFilter && onRequestFilter(undefined);
      return
    }


    if (
      urgenceImportanceFilter?.importance?.id === urgenceImportance.importance.id &&
      urgenceImportanceFilter?.urgence?.id === urgenceImportance.urgence.id
    ) {
      onRequestFilter && onRequestFilter(undefined);
    } else {
      onRequestFilter && onRequestFilter(urgenceImportance);
    }
  };

  return (
    <Grid container spacing={1} className={classes.root}>
      {error ? (
        <ErrorPage />
      ) : (
        <>
          {loading ? (
            <Loader />
          ) : (
            <>
              {data && data.length > 0
                ? data.map((urgenceImportance) => {
                  return (
                    <Grid
                      item
                      xs={4}
                      key={`SelectImportance${urgenceImportance.urgence.id}${urgenceImportance.importance.id}`}
                      onClick={(event) => {
                        event.preventDefault();
                        event.stopPropagation();
                        handleClickUrgenceImportance(urgenceImportance);
                      }}
                    >
                      <SelectImportance
                        urgence={urgenceImportance.urgence}
                        importance={urgenceImportance.importance}
                        nombreTache={urgenceImportance.nombreTache}
                        couleur={urgenceImportance.couleur}
                        active={
                          urgenceImportanceFilter?.importance?.id === urgenceImportance.importance.id &&
                          urgenceImportanceFilter?.urgence?.id === urgenceImportance.urgence.id
                        }
                      />
                    </Grid>
                  );
                })
                : ''}
            </>
          )}
        </>
      )}
    </Grid>
  );
};

export default ImportanceFilter;
