import React from 'react';

export const ArrowDrop = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
      <path fill="#FFF" d="M7,10l5,5,5-5Z" transform="translate(0 3)" />
      <path fill="none" d="M0,0H24V24H0Z" />
      <path fill="#FFF" d="M7,14l5-5,5,5Z" transform="translate(0 -4)" />
    </svg>
  );
};
