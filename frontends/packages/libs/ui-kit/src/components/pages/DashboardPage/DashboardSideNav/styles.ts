import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      position: 'relative',
      width: '100%',
      height: '100%',
      padding: '24px 0 0 0',
      '& hr': {
        margin: '16px 0 32px 0',
        background: theme.palette.grey[400],
        height: 1,
        border: 0,
        [theme.breakpoints.up('sm')]: {
          margin: '32px 0',
        },
      },
      [theme.breakpoints.up('sm')]: {
        padding: '24px 24px 0 24px',
      },
    },
    checkboxContainer: {
      display: 'flex',
      width: '100%',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    userNameContainer: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    username: {
      fontSize: 20,
      fontFamily: 'Roboto',
    },
    goBack: {
      padding: 0,
    },
    dateContainer: {
      marginTop: 0,
      fontSize: 16,
      fontFamily: 'Roboto',
      textAlign: 'center',
      [theme.breakpoints.up('sm')]: {
        textAlign: 'left',
      },
    },
    crack: {
      width: 24,
    },
    filterContainer: {
      height: 'calc(100vh - 316px)',
      width: '100%',
      paddingLeft: 16,
      overflowY: 'scroll',
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
    btnClose: {
      position: 'absolute',
      bottom: 40,
      left: 0,
      display: 'flex',
      width: '100%',
      justifyContent: 'center',
      marginTop: 8,
    },
    formBox: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      width: '100%',
    },
    count: {
      marginLeft: 10,
      textAlign: 'center',
      background: '#E0E0E0',
      height: 20,
      minWidth: 20,
      borderRadius: 2,
      fontSize: 10,
      color: '#212121',
      lineHeight: '20px',
    },
  })
);
