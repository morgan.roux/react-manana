import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    List: {
      background: theme.palette.grey[200],
      width: '100%',
      height: '63px',
      marginBottom: 3,
      cursor: 'pointer',
      '&:hover': {
        background: theme.palette.grey[300],
      },
    },
    nbr: {
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 12,
      color: '#FFFFFF',
      background: '#FBB104',
      paddingRight: 8,
      paddingLeft: 8,
      marginLeft: 8,
      borderRadius: 3,
      textAlign: 'left',
    },
    subtitle: {
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 14,
      color: '#212121',
    },
    text: {
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 12,
      color: '#42424261',
    },
    textProchain: {
      textAlign: 'left',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 16,
      color: '#424242',
    },
    infiniteScrollContainer: {
      width: '100%',
      overflowY: 'scroll',
      height: 'calc(100vh - 700px)',
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
  }),
);

export default useStyles;
