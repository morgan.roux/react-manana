import React from 'react';
import DashboardProportion from './DashboardProportion';

export default {
  title: 'Design system|Pages/DashboardPage/DashboardProportion',
  parameters: {
    info: { inline: true },
  },
};

export const Proportion = () => {
  return <DashboardProportion categorie="Relation Laboratoire" valeur={40} color="#63B8DD" />;
};
