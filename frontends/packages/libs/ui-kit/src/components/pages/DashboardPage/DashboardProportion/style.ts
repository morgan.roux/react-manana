import { createStyles, Theme, makeStyles } from '@material-ui/core';
// import GREY from '@material-ui/core/colors/grey';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    rooty: {
      [theme.breakpoints.down('sm')]: {
        height: 'calc(100vh - 155px)',
        width: '100%',
        padding: 0,
        overflowY: 'auto',
        overflowX: 'auto',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 50,
      },
    },
    root: {
      minHeight: 124,
      minWidth: 100,
    },
    card: {
      minHeight: 124,
      minWidth: 100,
    },
    mobile: {},
    textValeur: {
      color: '#FFFFFF',
      textAlign: 'center',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 24,
    },
    textCategorie: {
      color: '#FFFFFF',
      textAlign: 'center',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 16,
    },
  })
);

export default useStyles;
