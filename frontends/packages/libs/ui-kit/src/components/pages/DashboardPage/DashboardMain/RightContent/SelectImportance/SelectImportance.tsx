import { Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { ImportanceDashboard, UrgenceDashboard } from '../ImportanceFilter/ImportanceFilter';
import useStyles from './style';

export interface SelectImportanceProps {
  urgence: UrgenceDashboard;
  importance: ImportanceDashboard;
  nombreTache: number;
  couleur: string;
  active?: boolean;
}

const SelectImportance: FC<SelectImportanceProps> = ({
  urgence,
  importance,
  nombreTache,
  couleur,
  active,
}) => {
  const classes = useStyles();

  return (
    <Box className={classes.root} style={{ border: `solid 1px ${couleur}` }}>
      <Box
        style={{ background: active ? `${couleur}BF` : couleur }}
        className={classes.importanceBox}
      >
        <Typography className={classes.subtitle}>
          {`Importance ${importance.libelle} ${urgence.code === 'C' ? 'du' : 'de la'} ${
            urgence.libelle
          }`}
          &nbsp;({nombreTache})
        </Typography>
      </Box>
      {/* <Box
        style={{ background: active ? `${couleur}1F` : 'inherit', color: couleur }}
        className={classes.text}
      >
        Taches:
      </Box> */}
    </Box>
  );
};

export default SelectImportance;
