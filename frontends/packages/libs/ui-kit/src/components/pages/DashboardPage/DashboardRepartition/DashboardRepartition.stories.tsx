import React from 'react';
import DashboardRepartition from './DashboardRepartition';

export default {
  title: 'Design system|Pages/DashboardPage/DashboardRepartition',
  parameters: {
    info: { inline: true },
  },
};

const series = [
  { categorie: 'Relation Laboratoire', valeur: 0.25, couleur: 'red' },
  { categorie: 'Information de Liaison', valeur: 0.15, couleur: 'green' },
  { categorie: 'Ma To-do', valeur: 0.4, couleur: 'blue' },
  { categorie: 'Autre', valeur: 0.2, couleur: '#888' },
];

export const Repartition = () => {
  return <DashboardRepartition series={series} graphType="pie" showProportion={false} />;
};
