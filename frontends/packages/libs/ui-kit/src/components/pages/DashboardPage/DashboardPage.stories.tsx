import { PRTRendezVous, User } from '@lib/types';
import React from 'react';
import { DashboardFilter, DashboardPage } from '.';
import { CenterContentProps } from './DashboardMain/CenterContent/CenterContent';
// import { RightContentProps } from './DashboardMain/RightContent/RightContent';

export default {
  title: 'Design system|pages/DashboardPage',
  parameters: {
    info: { inline: true },
  },
};

const filters = [
  {
    code: 'INFORMATION_LIAISON',
    codeItem: 'Q',
    id: 'ckgqhc2w53qie0785wuedjqxx',
    name: 'Information de liaison',
    occurence: 19,
    __typename: 'Item',
  },
  {
    code: 'TODO',
    codeItem: 'L',
    id: 'ckb0z98q41g3o0855iady6lge',
    name: 'TODO',
    occurence: 4,
    __typename: 'Item',
  },
  {
    code: 'DQ_ACTION_OPERATIONNELLE',
    codeItem: 'SC',
    id: 'ckgqhc2w53qie0785wudzjyxx',
    name: 'Action operationnelle',
    occurence: 0,
    __typename: 'Item',
  },
  {
    code: 'DQ_INCIDENT',
    codeItem: 'SB',
    id: 'ckgqhc2w53qie0785wueyjyxx',
    name: "Fiche d'incident",
    occurence: 0,
    __typename: 'Item',
  },
  {
    code: 'DQ_AMELIORATION',
    codeItem: 'SA',
    id: 'ckgqhc2w53qie0785wuexjyxx',
    name: "Fiche d'amélioration",
    occurence: 0,
    __typename: 'Item',
  },
];

// Center
export const Dashboard = () => {
  const series = [
    { categorie: 'Relation Laboratoire', valeur: 25, couleur: 'red' },
    { categorie: 'Information de Liaison', valeur: 15, couleur: 'blue' },
    { categorie: 'Autre', valeur: 60, couleur: '#00FF00' },
  ];
  const statuts = {
    error: false,
    loading: false,
    data: [
      {
        id: 'statut1',
        code: 'EN_COURS',
        libelle: 'En cours',
      },
      {
        id: 'statut2',
        code: 'EN_RETARD',
        libelle: 'En retard',
      },
      {
        id: 'statut3',
        code: 'PLANIFIE',
        libelle: 'Planifiée',
      },
      {
        id: 'statut4',
        code: 'CLOTURE',
        libelle: 'Clôturée',
      },
    ],
  };

  const users: User[] = [
    {
      id: 'id1',
      email: '',
      emailConfirmed: true,
      passwordHash: '',
      passwordSecretAnswer: '',
      phoneNumber: '',
      phoneNumberConfirmed: true,
      twoFactorEnabled: true,
      lockoutEndDateUtc: new Date(),
      lockoutEnabled: true,
      accessFailedCount: 0,
      userName: 'Cécile ZUSSY',
      lastLoginDate: new Date(),
      lastPasswordChangedDate: new Date(),
      isLockedOut: false,
      isLockedOutPermanent: false,
      isObligationChangePassword: false,
      accessFailedCountBeforeLockoutPermanent: 0,
      idGroupement: 'id1',
      status: '',
      theme: '',
      jourNaissance: 2,
      moisNaissance: 7,
      anneeNaissance: 2000,
      createdAt: new Date(),
      updatedAt: new Date(),
      photo: {
        id: '',
        publicUrl:
          'https://avatars0.githubusercontent.com/u/47183345?s=460&u=d325a99dd569d0759a59c5d6d80feea36451c707&v=4',
      },
    },
    {
      id: 'id2',
      email: '',
      emailConfirmed: true,
      passwordHash: '',
      passwordSecretAnswer: '',
      phoneNumber: '',
      phoneNumberConfirmed: true,
      twoFactorEnabled: true,
      lockoutEndDateUtc: new Date(),
      lockoutEnabled: true,
      accessFailedCount: 0,
      userName: 'Jennifer DIDIER',
      lastLoginDate: new Date(),
      lastPasswordChangedDate: new Date(),
      isLockedOut: false,
      isLockedOutPermanent: false,
      isObligationChangePassword: false,
      accessFailedCountBeforeLockoutPermanent: 0,
      idGroupement: 'id2',
      status: '',
      theme: '',
      jourNaissance: 2,
      moisNaissance: 7,
      anneeNaissance: 2000,
      createdAt: new Date(),
      updatedAt: new Date(),
      photo: {
        id: '',
        publicUrl:
          'https://avatars0.githubusercontent.com/u/47183345?s=460&u=d325a99dd569d0759a59c5d6d80feea36451c707&v=4',
      },
    },
  ];

  const dashboards = {
    error: false,
    loading: false,
    data: [
      {
        id: 'dashboard1',
        item: {
          id: '1',
          code: 'L',
          codeItem: 'LA',
          name: 'Information de liason',
        },
        idItemAssocie: '10',
        statut: 'NOUVEAU',
        dateEcheance: new Date(),
        nombreJoursRetard: 1,
        urgence: {
          id: '',
          code: 'A',
          libelle: 'Journé',
          couleur: '#FF3A20',
        },
        importance: {
          id: '',
          ordre: '1',
          libelle: 'Important',
          couleur: '#FFCA3A',
        },
        description: '<p>Mise en place des nouveaux produits</p>',
        participants: users,
        type: 'Amélioration',
      },
      {
        id: 'dashboard1',
        item: {
          id: '1',
          code: 'L',
          codeItem: 'LA',
          name: 'Actualité',
        },
        idItemAssocie: '10',
        statut: 'NOUVEAU',
        dateEcheance: new Date(),
        nombreJoursRetard: 10,
        urgence: {
          id: '',
          code: 'A',
          libelle: 'Journé',
          couleur: '#FF3A20',
        },
        importance: {
          id: '',
          ordre: '',
          libelle: 'Important',
          couleur: '#FFCA3A',
        },
        description: '<p>Mise en place des nouveaux produits</p>',
        participants: users,
      },
      {
        id: 'dashboard1',
        item: {
          id: '1',
          code: 'L',
          codeItem: 'LA',
          name: 'Actualité',
        },
        idItemAssocie: '11',
        statut: 'CLOTURE',
        dateEcheance: new Date(),
        nombreJoursRetard: 0,

        urgence: {
          id: '',
          code: 'A',
          libelle: 'Journé',
          couleur: '#FF3A20',
        },
        importance: {
          id: '',
          ordre: '',
          libelle: 'Important',
          couleur: '#FFCA3A',
        },
        description: '<p>Mise en place des nouveaux produits</p>',
        participants: users,
        declarant: users[0],
      },
      {
        id: 'dashboard1',
        item: {
          id: '1',
          code: 'L',
          codeItem: 'LA',
          name: 'Actualité',
        },
        idItemAssocie: '12',
        statut: 'NOUVEAU',
        dateEcheance: new Date(),
        nombreJoursRetard: 1,

        urgence: {
          id: '',
          code: 'A',
          libelle: 'Journé',
          couleur: '#FF3A20',
        },
        importance: {
          id: '',
          ordre: '',
          libelle: 'Important',
          couleur: '#FFCA3A',
        },
        description: '<p>Mise en place des nouveaux produits</p>',
        participants: users,
      },
      {
        id: 'dashboard1',
        item: {
          id: '1',
          code: 'L',
          codeItem: 'LA',
          name: 'Actualité',
        },
        idItemAssocie: '10',
        statut: 'NOUVEAU',
        dateEcheance: new Date(),
        nombreJoursRetard: 1,

        urgence: {
          id: '',
          code: 'A',
          libelle: 'Journé',
          couleur: '#FF3A20',
        },
        importance: {
          id: '',
          ordre: '',
          libelle: 'Important',
          couleur: '#FFCA3A',
        },
        description: '<p>Mise en place des nouveaux produits</p>',
        participants: users,
      },
      {
        id: 'dashboard1',
        item: {
          id: '1',
          code: 'L',
          codeItem: 'LA',
          name: 'Actualité',
        },
        idItemAssocie: '10',
        statut: 'NOUVEAU',
        dateEcheance: new Date(),
        nombreJoursRetard: 11,

        urgence: {
          id: '',
          code: 'A',
          libelle: 'Journé',
          couleur: '#FF3A20',
        },
        importance: {
          id: '',
          ordre: '',
          libelle: 'Important',
          couleur: '#FFCA3A',
        },
        description: '<p>Mise en place des nouveaux produits</p>',
        participants: users,
      },
    ],
  };

  const handleRequestLike = (dashboard: any) => {
    console.log('like', dashboard.id);
  };
  const handleRequestShare = (dashboard: any) => {
    console.log('share', dashboard.id);
  };
  const handleRequestCloture = (dashboard: any, commentaire: string) => {
    console.log('cloture', dashboard, commentaire);
  };
  const handleRequestChangeStatut = (id: string, value: string) => {
    console.log('changement de statut', id, value);
  };

  const handleOnRequestSearch = (filter: DashboardFilter) => {
    console.log('filter', filter);
  };

  const handleGetItemAssocie = (dashboard: any) => {
    console.log('getItemAssocie: ', dashboard);
  };

  const handleGoBack = () => {
    alert('goBack');
  };

  const centerContentProps: CenterContentProps = {
    rowsTotal: dashboards.data.length,
    series,
    graphType: 'donut',
    statuts,
    dashboards,
    clotureSaving: false,
    onRequestLike: handleRequestLike,
    onRequestChangeStatut: handleRequestChangeStatut,
    onRequestCloture: handleRequestCloture,
    onRequestShare: handleRequestShare,
    onRequestGetItemAssocie: handleGetItemAssocie,
    commentListComponent: <>ComponentListComponent</>,
    commentComponent: <>CommentComponent</>,
  };

  // Right
  const invites = [
    {
      id: 'id1',
      civilite: 'CIV2',
      nom: 'Nom',
      prenom: 'Prenom',
      fonction: 'fonction',
      photo: {
        id: '',
        publicUrl:
          'https://secure.gravatar.com/avatar/65ac898a11bad870bf704061f1b0e105?s=80&d=identicon',
      },
      contact: {
        cp: 'cp',
        ville: 'ville',
        pays: 'pays',
        adresse1: 'adresse 1',
        adresse2: 'adresse 2',
        faxProf: 'faxProf',
        faxPerso: 'faxPerso',
        telProf: 'telProf',
        telMobProf: 'telMobProf',
        telPerso: 'telPerso',
        telMobPerso: 't',
        mailProf: 'mfdgd',
        mailPerso: 'fdsf',
        siteProf: 'fgdsf',
        sitePerso: 'fdf',
        whatsappMobProf: 'dfsd',
        whatsappMobPerso: 'fdf',
        compteSkypeProf: 'fqdqsd',
        compteSkypePerso: 'fgfsd',
        urlLinkedInProf: 'fdf',
        urlLinkedInPerso: 'fdfd',
        urlTwitterProf: 'fdfs',
        urlTwitterPerso: 'fdfs',
        urlFacebookProf: 'sqd',
        urlFacebookPerso: 'fdf',
        codeMaj: 'fdfds',
        idUserCreation: 'fdfd',
        idUserModification: 'dqsd',
      },
    },
    {
      id: 'id2',
      civilite: 'CIV1',
      nom: 'Nom',
      prenom: 'Prenom',
      fonction: 'fonction',
      photo: {
        id: '',
        publicUrl:
          'https://secure.gravatar.com/avatar/65ac898a11bad870bf704061f1b0e105?s=80&d=identicon',
      },
      contact: {
        cp: 'cp',
        ville: 'ville',
        pays: 'pays',
        adresse1: 'adresse 1',
        adresse2: 'adresse 2',
        faxProf: 'faxProf',
        faxPerso: 'faxPerso',
        telProf: 'telProf',
        telMobProf: 'telMobProf',
        telPerso: 'telPerso',
        telMobPerso: 't',
        mailProf: 'mfdgd',
        mailPerso: 'fdsf',
        siteProf: 'fgdsf',
        sitePerso: 'fdf',
        whatsappMobProf: 'dfsd',
        whatsappMobPerso: 'fdf',
        compteSkypeProf: 'fqdqsd',
        compteSkypePerso: 'fgfsd',
        urlLinkedInProf: 'fdf',
        urlLinkedInPerso: 'fdfd',
        urlTwitterProf: 'fdfs',
        urlTwitterPerso: 'fdfs',
        urlFacebookProf: 'sqd',
        urlFacebookPerso: 'fdf',
        codeMaj: 'fdfds',
        idUserCreation: 'fdfd',
        idUserModification: 'dqsd',
      },
    },
  ];

  const rendezVous: PRTRendezVous[] = [
    {
      id: 'rdv1',
      dateRendezVous: new Date(),
      heureDebut: '07:30',
      heureFin: '08:00',
      ordreJour: '',
      statut: 'PLANIFIE',
      idPartenaireTypeAssocie: '',
      partenaireType: 'LABORATOIRE',
      invites,
      participants: users,
    },
    {
      id: 'rdv1',
      dateRendezVous: new Date(),
      heureDebut: '07:30',
      heureFin: '08:00',
      ordreJour: '',
      statut: 'PLANIFIE',
      idPartenaireTypeAssocie: '',
      partenaireType: 'LABORATOIRE',
      invites,
      participants: users,
    },
  ];

  // const rightContentProps: RightContentProps = {
  //   rendezVous,
  //   nombreRdv: 2,
  // };

  return (
    <DashboardPage
      rightContentProps={{
        rendezVousProps: {
          data: rendezVous as any,
          nombreRdv: rendezVous.length,
        },
        importanceFilterProps: {
          urgencesImportances: {
            urgences: [
              {
                id: 'dasd',
                code: 'A',
                libelle: 'CodeA',
              },
              {
                id: 'dsds',
                code: 'B',
                libelle: 'CodeB',
              },
              {
                id: 'test',
                code: 'C',
                libelle: 'CodeC',
              },
            ],
            importances: [
              {
                id: 'fsdf',
                order: 1,
                libelle: 'Importance1',
              },
              {
                id: 'fdfsdk',
                order: 2,
                libelle: 'Importance2',
              },
            ],
          },
          nombreTaches: {
            hauteJournee: 1,
            hauteMois: 2,
            hauteSemaine: 3,
            moyenneJournee: 2,
            moyenneMois: 2,
            moyenneSemaine: 6,
          },
        },
      }}
      centerContentProps={centerContentProps}
      sideNavProps={{
        defaultFilter: {
          code: 'DQ_ACTION_OPERATIONNELLE',
          codeItem: 'SC',
          id: 'ckgqhc2w53qie0785wudzjyxx',
          name: 'Action operationnelle',
          occurence: 0,
          __typename: 'Item',
        } as any,
        filters,
        user: { fullName: 'Mickael' } as any,
      }}
      onRequestSearch={handleOnRequestSearch}
      onGoBack={handleGoBack}
    />
  );
};
