import { Box, Checkbox, FormControl, FormControlLabel, FormGroup, IconButton, Typography } from '@material-ui/core';
import ArrowBack from '@material-ui/icons/ArrowBack';
import React, { FC, useEffect, useState } from 'react';
import { useStyles } from './styles';
import { isMobile } from '../../../../services/Helpers';
import { NewCustomButton } from '../../../atoms';
import { Dashboard, Refresh } from '@material-ui/icons';
import moment from 'moment';
import ErrorPage from '../../ErrorPage/ErrorPage';

export interface SideNavFilter {
  id: string;
  code: string;
  name: string;
  occurence?: number;
  __typename: string;
}

export interface DashboardSideNavProps {
  onGoBack?: () => void;
  defaultFilter?: SideNavFilter;
  filters: SideNavFilter[];
  itemsChecked: string[];
  onChangeItemsChecked: (itemsChecked: string[]) => void;
  onChange: (filter: SideNavFilter[]) => void;
  onResetFilters: () => void;
  username: string;
  onClose?: () => void;
  error?: boolean;
  loading?: boolean;
}

export const DashboardSideNav: FC<DashboardSideNavProps> = ({
  onGoBack,
  filters,
  onChange,
  itemsChecked,
  onChangeItemsChecked,
  defaultFilter,
  username,
  onClose,
  error,
  loading,
  onResetFilters,
}) => {
  const classes = useStyles();
  const [filterResults, setFilterResults] = useState<SideNavFilter[]>([]);
  const [allFilters, setAllFilters] = useState<SideNavFilter[]>([]);
  const [currentDate, setCurrentDate] = useState<string>('');

  useEffect(() => {
    setAllFilters(filters);
    if (defaultFilter) {
      onChangeItemsChecked([...itemsChecked, defaultFilter.id]);
      const _filtersResult = filterResults;
      const filter = filters.find((filter) => filter.id === defaultFilter.id);
      if (filter) {
        _filtersResult.push(filter);
        onChange(_filtersResult);
        setFilterResults(_filtersResult);
      }
    }
    // date
    moment.locale('fr');
    setCurrentDate(moment().format('DD MMMM YYYY'));
  }, [filters, defaultFilter]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newItemsChecked = !itemsChecked.includes(event.target.name)
      ? allFilters.find((filter) => filter.id === event.target.name)
      : null;
    if (itemsChecked.includes(event.target.name)) {
      onChangeItemsChecked(itemsChecked.filter((item) => item !== event.target.name));
    } else {
      onChangeItemsChecked([...itemsChecked, event.target.name]);
    }
    const results = filterResults;
    if (newItemsChecked) {
      results.push(newItemsChecked);
      setFilterResults(results);
      onChange(results);
    } else {
      const temp = results.filter((f) => f.id !== event.target.name);
      onChange(temp);
      setFilterResults(temp);
    }
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.userNameContainer}>
        {isMobile() && (
          <Box>
            <IconButton classes={{ root: classes.goBack }} onClick={onGoBack}>
              <ArrowBack />
            </IconButton>
          </Box>
        )}
        <Box width={1} display="flex" alignItems="center" justifyContent="space-between">
          <Box className={classes.username}>Bonjour {username.trim().split(' ')[0]}</Box>
          <IconButton onClick={onResetFilters}>
            <Refresh />
          </IconButton>
        </Box>
        <Box className={classes.crack} />
      </Box>
      <Box marginTop={1} className={classes.dateContainer}>
        On est le {currentDate}
      </Box>
      <hr style={{ margin: '16px 0 16px 0' }} />
      <Box className={classes.filterContainer}>
        <FormControl style={{ width: '100%' }} component="fieldset">
          <FormGroup>
            {error ? (
              <ErrorPage />
            ) : loading ? (
              <>Chargement ...</>
            ) : (
              (filters || []).map(({ id, name, occurence }) => (
                <Box key={id} className={classes.checkboxContainer}>
                  <FormControlLabel
                    control={<Checkbox checked={itemsChecked.includes(id)} onChange={handleChange} name={id} />}
                    label={name}
                  />
                  <Typography className={classes.count}>{occurence ?? '0'}</Typography>
                </Box>
              ))
            )}
          </FormGroup>
        </FormControl>
      </Box>
      {isMobile() && (
        <Box className={classes.btnClose}>
          <NewCustomButton onClick={onClose} startIcon={<Dashboard />}>
            Mon Dashboard d’Activité
          </NewCustomButton>
        </Box>
      )}
    </Box>
  );
};
