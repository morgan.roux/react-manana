import { PRTRendezVous } from '@lib/types';
import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import {
  CustomDatePicker,
  CustomFormTextField,
  CustomModal,
  CustomTimePicker,
  NewCustomButton,
} from '../../../../../atoms';
import { useStyles } from './styles';
import { isMobile } from '../../../../../../services/Helpers';
import moment from 'moment';

export interface RendezFormReporterProps {
  open: boolean;
  setOpen: () => void;
  value?: PRTRendezVous;
  onSave: (data: PRTRendezVous) => void;
}

export const RendezFormReporter: FC<RendezFormReporterProps> = ({ open, setOpen, value, onSave }) => {
  const classes = useStyles();
  const [valueRdv, setValueRdv] = React.useState<PRTRendezVous | undefined>();
  const [heureFinValue, setHeureFinValue] = React.useState<Date | null>(null);
  const [heureDebutValue, setHeureDebutValue] = React.useState<Date | null>(null);
  const [errorHeureDebut, setErrorHeureDebut] = React.useState<boolean>(false);
  const [errorHeureFin, setErrorHeureFin] = React.useState<boolean>(false);
  const [errorDate, setErrorDate] = React.useState<boolean>(false);

  React.useEffect(() => {
    setValueRdv(value);
    if (value) {
      setHeureFinValue(moment(value.heureFin, 'HH:mm').toDate());
      setHeureDebutValue(moment(value.heureDebut, 'HH:mm').toDate());
      console.log(moment(value.heureDebut, 'HH:mm').toDate());
    }
  }, [value]);

  const handleDateChange = (date: any, __value?: string | null | undefined) => {
    if (date >= (valueRdv?.dateRendezVous as any)) {
      setErrorDate(false);
    } else {
      setErrorDate(true);
    }
    setValueRdv({ ...valueRdv, dateRendezVous: date } as any);
  };

  const handleHeureFinChange = (date: any, __value?: string | null | undefined) => {
    console.log(date);
    if ((heureDebutValue as any) < date) {
      setErrorHeureFin(false);
      setValueRdv({ ...valueRdv, heureFin: date } as any);
    } else {
      setErrorHeureFin(true);
    }
    setHeureFinValue(date);
  };

  const handleChangeDebut = (date: any, __value?: string | null | undefined) => {
    if ((heureFinValue as any) > date) {
      setErrorHeureDebut(false);
      setValueRdv({ ...valueRdv, heureDebut: date } as any);
    } else {
      setErrorHeureDebut(true);
    }
    setHeureDebutValue(date);
  };

  const convertToStringHeure = (date: Date): string => {
    return moment(date).format('HH:mm');
  };

  const handleSave = () => {
    if (
      valueRdv &&
      valueRdv.dateRendezVous &&
      valueRdv.heureDebut &&
      valueRdv.heureFin &&
      !errorDate &&
      !errorHeureDebut &&
      !errorHeureFin
    ) {
      onSave({
        ...valueRdv,
        heureDebut:
          (valueRdv.heureDebut as any) instanceof Date
            ? convertToStringHeure(valueRdv.heureDebut as any)
            : valueRdv.heureDebut,
        heureFin:
          (valueRdv.heureFin as any) instanceof Date
            ? convertToStringHeure(valueRdv.heureFin as any)
            : valueRdv.heureFin,
      });
    }
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Détails RDV"
      withBtnsActions={false}
      headerWithBgColor
      closeIcon
      maxWidth="sm"
    >
      <Box className={classes.root}>
        <CustomFormTextField
          value={`${valueRdv?.invites[0].nom} ${valueRdv?.invites[0].prenom}`}
          label="Nom et prénom"
        />
        <CustomFormTextField value={`${valueRdv?.invites[0].contact?.telMobProf}`} label="Téléphone" />
        <CustomFormTextField value={`${valueRdv?.partenaireType}`} label="Origine" />
        <Box display="flex" flexDirection={isMobile() ? 'column' : 'row'} justifyContent="space-between">
          <Box pb={2} pr={isMobile() ? 0 : 2}>
            <CustomDatePicker
              style={{ width: '100%' }}
              name="dateRendezVous"
              value={valueRdv?.dateRendezVous}
              onChange={handleDateChange}
              required
              label="Date"
            />
          </Box>
          <Box pb={2} pr={isMobile() ? 0 : 2}>
            <CustomTimePicker
              style={{ width: '100%' }}
              name="heureDebut"
              value={heureDebutValue}
              onChange={handleChangeDebut}
              required
              label="A"
            />
          </Box>
          <Box pb={2}>
            <CustomTimePicker
              style={{ width: '100%' }}
              name="heureFin"
              value={heureFinValue}
              onChange={handleHeureFinChange}
              label="Jusqu'à"
            />
          </Box>
        </Box>
        <Box className={classes.btnAcionContainer}>
          <NewCustomButton disabled={errorHeureDebut || errorHeureFin || errorDate} onClick={handleSave}>
            REPORTER
          </NewCustomButton>
        </Box>
      </Box>
    </CustomModal>
  );
};
