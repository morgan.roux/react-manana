import { Box, CardContent, Typography, Card } from '@material-ui/core';
import React, { FC } from 'react';
// import { isMobile } from '../../../../services/Helpers';
import useStyles from './style';

export interface DashboardProportionProps {
  categorie?: string;
  valeur?: number;
  color: string;
}

const DashboardProportion: FC<DashboardProportionProps> = ({ categorie, valeur, color }) => {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      <Card className={classes.card} style={{ background: color }}>
        <CardContent>
          <Box>
            <Typography className={classes.textValeur}>{`${valeur?.toFixed(0)}%`}</Typography>
            <Typography className={classes.textCategorie}>
              {(categorie?.length || 0) > 22 ? `${categorie?.substr(0, 22)}...` : categorie}
            </Typography>
          </Box>
        </CardContent>
      </Card>
    </Box>
  );
};

export default DashboardProportion;
