// import React, { Fragment, useState } from 'react';
// import Button from '@material-ui/core/Button';
// import HistoriqueExecution from './HistoriqueExecution';
// import { TATraitement } from '@lib/types';
// import { users } from '../../TraitementAutomatiqueForm/TraitementAutomatiqueForm.stories';

// export default {
//   title: 'Design system|pages/TraitementAutomatiquePage/HistoriqueExecution',
//   parameters: {
//     info: { inline: true },
//   },
// };

// const date: Date = new Date('2012/12/12');

// const data: TATraitement[] = [
//   {
//     id: '',
//     idType: '',
//     idFonction: '',
//     idTache: '',
//     idImportance: '',
//     idUrgence: '',
//     idPharmacie: '',
//     createdAt: date,
//     updatedAt: date,
//     repetition: 2,
//     repetitionUnite: 'JOUR',
//     heure: '8:00',
//     tache: {
//       id: '',
//       ordre: 2,
//       libelle: '',
//       idFonction: '',
//       idPharmacie: '',
//       idGroupement: '',
//       createdAt: date,
//       updatedAt: date,
//       idProjet: '',
//       active: false,
//       collaborateurResponsables: [],
//     },
//     importance: {
//       id: '',
//       ordre: '',
//       libelle: 'etiquette',
//       couleur: '#FFCA3A',
//     },
//     type: {
//       id: '',
//       code: 'code',
//       libelle: 'libelle',
//       createdAt: date,
//       updatedAt: date,
//     },
//     urgence: {
//       id: '',
//       code: 'A',
//       libelle: 'Journé',
//       couleur: '#FF3A20',
//     },
//     description: 'RelanceMrDupont',
//     fonction: {
//       id: '',
//       ordre: 2,
//       libelle: 'libelle',
//       idPharmacie: '',
//       idGroupement: '',
//       createdAt: date,
//       updatedAt: date,
//       nombreTaches: 4,
//       active: true,
//       taches: [
//         {
//           id: '',
//           ordre: 2,
//           libelle: 'libelle',
//           idFonction: '',
//           idPharmacie: '',
//           idGroupement: '',
//           createdAt: date,
//           updatedAt: date,
//           idProjet: '',
//           active: false,
//           collaborateurResponsables: [
//             {
//               idTache: '',
//               type: {
//                 id: '',
//                 ordre: 3,
//                 code: 'code',
//                 couleur: 'red',
//                 libelle: 'libelle',
//                 idPharmacie: '',
//                 idGroupement: '',
//                 createdAt: date,
//                 updatedAt: date,
//               },
//               responsable: users[0],
//             },
//           ],
//         },
//       ],
//     },
//     participants: users,
//     recurrence: 'Tous les jours',
//     execution: {
//       id: '',
//       idTraitement: '',
//       idPharmacie: '',
//       dateEcheance: date,
//       createdAt: date,
//       updatedAt: date,
//       prochainement: date,
//       changementStatus: [
//         {
//           id: '',
//           idTraitementExecution: '',
//           idPharmacie: '',
//           status: 'en cours',
//           createdAt: date,
//           updatedAt: date,
//         },
//       ],
//       dernierChangementStatut: {
//         id: '',
//         idTraitementExecution: '',
//         idPharmacie: '',
//         status: 'Clôturer',
//         createdAt: date,
//         updatedAt: date,
//       },
//       urgence: {
//         id: '',
//         code: 'A',
//         libelle: 'Journé',
//         couleur: '#F374AE',
//       },
//       importance: {
//         id: '',
//         ordre: '',
//         libelle: 'etiquette',
//         couleur: '#FFCA3A',
//       },
//     },

//     nombreTraitementsCloture: 0,
//     nombreTraitementsEnRetard: 1
//   },
// ];

// export const historiqueStory = () => {
//   const [open, setOpen] = useState<boolean>(false);

//   const change = () => {
//     setOpen(!open);
//   };

//   return (
//     <Fragment>
//       <Button onClick={change}>Open</Button>
//       <HistoriqueExecution
//         traitement={data[0]}
//         open={open}
//         setOpen={change}
//         previousExecutions={{
//           data: data.map((item) => item.execution)
//         }}
//         nextExecutions={{
//           data: []
//         }}
//         onChangeStatut={() => console.log('changed')}
//       />
//     </Fragment>
//   );
// };
