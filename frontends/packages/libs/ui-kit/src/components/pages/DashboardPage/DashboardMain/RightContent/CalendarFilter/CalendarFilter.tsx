import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { CustomDatePicker } from '../../../../../atoms';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import frLocale from 'date-fns/locale/fr';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { useStyles } from './styles';
import moment from 'moment';

export interface DateFilterDashboard {
  date?: Date;
  dateDebut?: Date;
  dateFin?: Date;
}

export interface CalendarFilterProps {
  dateFilter?: DateFilterDashboard;
  onRequestFilterDate?: (dateFilter: DateFilterDashboard) => void;
}

const CalendarFilter: FC<CalendarFilterProps> = ({ dateFilter, onRequestFilterDate }) => {
  const [date, changeDate] = useState<Date | undefined>();
  const [dateDebut, setDateDebut] = useState<Date | undefined>();
  const [dateFin, setDateFin] = useState<Date | undefined>();
  const classes = useStyles();
  const [errorDateDebut, setErrorDateDebut] = useState<boolean>(false);
  const [errorDateFin, setErrorDateFin] = useState<boolean>(false);

  useEffect(() => {
    changeDate(dateFilter?.date);
    setDateDebut(dateFilter?.dateDebut);
    setDateFin(dateFilter?.dateFin);
  }, [dateFilter]);

  useEffect(() => {
    if (onRequestFilterDate) {
      if (!errorDateDebut && !errorDateFin) {
        onRequestFilterDate({
          date,
          dateDebut,
          dateFin,
        });
      }
    }
  }, [date, dateDebut, dateFin]);

  const handleChangeDate = (_date: MaterialUiPickersDate) => {
    if (_date) {
      changeDate(_date);
      setDateDebut(undefined);
      setDateFin(undefined);
    }
  };

  const handleChangeDateDebut = (_date: MaterialUiPickersDate) => {
    if (_date) {
      if (dateFin && dateFin > _date) {
        setErrorDateDebut(false);
        setErrorDateFin(false);
      } else if (!dateFin) {
        setErrorDateDebut(false);
        setErrorDateFin(false);
      } else {
        setErrorDateDebut(true);
      }
      setDateDebut(_date);
      changeDate(undefined);
    }
  };

  const handleChangeDateFin = (_date: MaterialUiPickersDate) => {
    if (_date) {
      if (dateDebut && dateDebut < _date) {
        setErrorDateFin(false);
        setErrorDateDebut(false);
      } else if (!dateDebut) {
        setErrorDateFin(false);
        setErrorDateDebut(false);
      } else {
        setErrorDateFin(true);
      }
      setDateFin(_date);
      changeDate(undefined);
    }
  };

  return (
    <>
      <Box display="flex">
        <Box display="flex">
          <CustomDatePicker
            onChange={handleChangeDateDebut}
            className={classes.datePicker}
            value={dateDebut}
            label="Du"
            error={errorDateDebut}
            inputValue={dateDebut ? moment(dateDebut).format('DD/MM/YY') : '00/00/00'}
          />
        </Box>
        <Box ml={1} display="flex" displayPrint="column">
          <CustomDatePicker
            onChange={handleChangeDateFin}
            className={classes.datePicker}
            value={dateFin}
            label="Au"
            inputValue={dateFin ? moment(dateFin).format('DD/MM/YY') : '00/00/00'}
            error={errorDateFin}
          />
        </Box>
      </Box>
      <Box className={classes.date}>
        <MuiPickersUtilsProvider locale={frLocale} utils={DateFnsUtils}>
          <DatePicker
            autoOk
            orientation="portrait"
            variant="static"
            openTo="date"
            value={date || new Date()}
            onChange={handleChangeDate}
          />
        </MuiPickersUtilsProvider>
      </Box>
    </>
  );
};

export default CalendarFilter;
