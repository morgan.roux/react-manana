import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import CalendarFilter, { DateFilterDashboard } from './CalendarFilter/CalendarFilter';
import ImportanceFilter, { ImportanceFilterProps, UrgenceImportance } from './ImportanceFilter/ImportanceFilter';
import { useStyles } from '../styles';
import RendezVousList from '../../DashboardSideNav/RendezvousList/RendezvousList';
import { PRTRendezVous } from '@lib/types';
import { useWindowDimensions } from '../../../../atoms/MobileTopBar/MobileTopBar';
// import { RendezVousRequestSavingProps } from '../../../LaboratoirePage/SideNavList/About/RendezVousForm/interface';

export interface RightContentProps {
  dateFilter?: DateFilterDashboard;
  onRequestFilterDate?: (dateFilter: DateFilterDashboard) => void;
  rendezVousProps?: {
    data: PRTRendezVous[];
    loading?: boolean;
    error?: Error;
    nombreRdv?: number;
    onRequestSearchRendezVous?: (paging: any) => void;
    onRequestSave?: (data: PRTRendezVous) => void;
    // onRequestSaveRendezVous?: {
    //   save?: (data: RendezVousRequestSavingProps) => void;
    //   saving?: boolean;
    //   saved?: boolean;
    //   setSaved?: (newSaved: boolean) => void;
    // };
    // collaborateurComponent?: ReactNode;
    // inviteComponent?: ReactNode;
    // selectedParticipants?: any[];
    // selectedInvites?: any[];
    // onRequestParticipantInviteEdit?: (participants: any, invites: any) => void;
    // onRequestDelete?: (data: PRTRendezVous) => void;
    // onRequestCancel?: (data: PRTRendezVous) => void;
  };
  urgenceImportanceFilter?: UrgenceImportance;
  onRequestFilterImportanceUrgence?: (filter: UrgenceImportance | undefined) => void;
  importanceFilterProps?: ImportanceFilterProps;
}

export const RightContent: FC<RightContentProps> = ({
  importanceFilterProps,
  dateFilter,
  onRequestFilterDate,
  rendezVousProps,
  onRequestFilterImportanceUrgence,
  urgenceImportanceFilter,
  // onRequestSaveRendezVous,
  // collaborateurComponent,
  // inviteComponent,
  // selectedParticipants,
  // selectedInvites,
  // onRequestParticipantInviteEdit,
  // onRequestCancel,
  // onRequestDelete,
}) => {
  const classes = useStyles();
  const dimensions = useWindowDimensions();
  return (
    <Box>
      {dimensions !== 'isSm' && (
        <Box>
          <ImportanceFilter
            {...importanceFilterProps}
            urgenceImportanceFilter={urgenceImportanceFilter}
            onRequestFilter={onRequestFilterImportanceUrgence}
          />
        </Box>
      )}
      <Box className={classes.calendar}>
        <CalendarFilter dateFilter={dateFilter} onRequestFilterDate={onRequestFilterDate} />
      </Box>
      <Box className={classes.rendezVous}>
        <RendezVousList
          rendezVous={rendezVousProps?.data}
          nombreRdv={rendezVousProps?.nombreRdv}
          onRequestSave={rendezVousProps?.onRequestSave}
          error={rendezVousProps?.error}
          loading={rendezVousProps?.loading}
          onRequestSearch={rendezVousProps?.onRequestSearchRendezVous}
        />
      </Box>
    </Box>
  );
};

export default RightContent;
