import { Menu, MenuItem, Typography } from '@material-ui/core';
import { Cancel, Delete, Edit, Schedule } from '@material-ui/icons';
import React, { FC, Fragment } from 'react';

export interface MenuRdvProps {
  onEditClick: () => void;
  onDeleteClick: () => void;
  onReportClick: () => void;
  onCancelClick: () => void;
  anchorEl: HTMLElement | null;
  onClose: () => void;
  statutRdv?: string;
}

export const MenuRdv: FC<MenuRdvProps> = ({
  anchorEl,
  onClose,
  onDeleteClick,
  onEditClick,
  onReportClick,
  onCancelClick,
  statutRdv,
}) => {
  return (
    <Menu id="simple-menu-rdv" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={onClose}>
      {statutRdv === 'PLANIFIE' && (
        <Fragment>
          <MenuItem
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              onReportClick();
            }}
          >
            <Schedule />
            &nbsp;
            <Typography variant="inherit">Reporter</Typography>
          </MenuItem>

          <MenuItem
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              onCancelClick();
            }}
          >
            <Cancel />
            &nbsp;
            <Typography variant="inherit">Annuler</Typography>
          </MenuItem>

          <MenuItem
            onClick={(e) => {
              e.stopPropagation();
              e.preventDefault();
              onEditClick();
            }}
          >
            <Edit />
            &nbsp;
            <Typography variant="inherit">Modifier</Typography>
          </MenuItem>
        </Fragment>
      )}
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          e.preventDefault();
          onDeleteClick();
        }}
      >
        <Delete />
        &nbsp;
        <Typography variant="inherit">Supprimer</Typography>
      </MenuItem>
    </Menu>
  );
};
