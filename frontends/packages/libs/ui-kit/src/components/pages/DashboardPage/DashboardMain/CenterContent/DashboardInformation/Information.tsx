import React, { FC, MouseEvent, ReactNode } from 'react';
import { Grid, Box, Typography, Link, IconButton } from '@material-ui/core';
import { Fullscreen, FullscreenExit, ThumbUp, Share, Close } from '@material-ui/icons';
import moment from 'moment';
import { CustomAvatarGroup, NewCustomButton } from '../../../../../atoms';
import useStyles from './styles';
import classNames from 'classnames';
import { Dashboard } from './../../../types';

interface InformationProps {
  fullscreen: boolean;
  onRequestLike?: (dashboard: Dashboard) => void;
  onRequestShare?: (dashboard: Dashboard) => void;
  onRequestCloture?: (dashboard: Dashboard, commentaire: string) => void;
  onPrevStep: (e: MouseEvent<HTMLElement>) => void;
  onNextStep: (e: MouseEvent<HTMLElement>) => void;
  onOpen: (open: boolean) => void;
  onFullScreen: (fullscreen: boolean) => void;
  dashboard?: Dashboard;
  step: number;
  stepLength: number;
  commentaire: string;
  clotureSaving?: boolean;
  commentComponent?: ReactNode;
  commentListComponent?: ReactNode;
}

const Information: FC<InformationProps> = ({
  dashboard,
  fullscreen,
  step,
  stepLength,
  onRequestLike,
  onRequestShare,
  onPrevStep,
  onNextStep,
  onOpen,
  onFullScreen,
  commentaire,
  onRequestCloture,
  clotureSaving,
  commentListComponent,
  commentComponent,
}) => {
  const classes = useStyles();

  console.log(onRequestLike, onRequestShare, IconButton, ThumbUp, Share);

  const handleClose = () => {
    onOpen(false);
    onFullScreen(false);
  };

  const handleFullScreen = () => {
    onFullScreen(!fullscreen);
  };

  return (
    <Grid container className={classes.root}>
      <Grid container item xs={12} className={classes.gridItem}>
        <Grid item xs={12}>
          <Box display="flex" flexDirection="row" justifyContent="space-between">
            <Box>
              <Typography className={classes.subtitle2}>Information</Typography>
            </Box>
            <Box display="flex" flexDirection="row" justifyContent="space-between">
              <Box>
                <Typography>
                  <Link href="#" onClick={onPrevStep} className={classes.cursorLink}>
                    {'<'}
                  </Link>
                  {step}/{stepLength}
                  <Link href="#" onClick={onNextStep} className={classes.cursorLink}>
                    &gt;
                  </Link>
                </Typography>
              </Box>
              <Box className={classes.cursorLink} onClick={handleFullScreen}>
                {fullscreen ? <FullscreenExit /> : <Fullscreen />}
              </Box>
              <Box onClick={handleClose} className={classes.cursorLink}>
                <Close />
              </Box>
            </Box>
          </Box>
        </Grid>
        <Grid container item xs={12} lg={12} sm={12} md={12} className={classNames(classes.gridItem)}>
          <Box
            display="flex"
            flexDirection="row"
            justifyContent="flex-start"
            flexWrap="wrap"
            px={2}
            py={2}
            className={classes.origineDetail}
          >
            {dashboard?.participants && (
              <Grid item xs={12} sm={6} lg={2} md={2}>
                <Typography variant="subtitle2" className={classes.subtitle1}>
                  Equipe:
                </Typography>
                <Box display="flex" flexDirection="row" justifyContent="flex-start">
                  <CustomAvatarGroup max={3} sizes="24px" users={(dashboard.participants || []) as any} />
                </Box>
              </Grid>
            )}
            {dashboard?.item && (
              <Grid item xs={12} sm={6} lg={2} md={2}>
                <Typography variant="subtitle2" className={classes.subtitle1}>
                  Type:
                </Typography>
                <Typography variant="body2" className={classes.text1}>
                  {dashboard.item.name || '-'}
                </Typography>
              </Grid>
            )}
            {dashboard?.createdAt && (
              <Grid item xs={12} sm={6} lg={2} md={2}>
                <Typography variant="subtitle2" className={classes.subtitle1}>
                  Date de création:
                </Typography>
                <Typography variant="body2" className={classes.text1}>
                  {dashboard.createdAt ? moment(dashboard.createdAt).format('DD/MM/YY') : '-'}
                </Typography>
              </Grid>
            )}
            {dashboard?.statut && (
              <Grid item xs={12} sm={6} lg={2} md={2}>
                <Typography variant="subtitle2" className={classes.subtitle1}>
                  Statut:
                </Typography>
                <Typography variant="body2" className={classes.text1}>
                  {dashboard.statut || '-'}
                </Typography>
              </Grid>
            )}
            {/*
        <Grid item xs={12} sm={6} lg={2} md={2}>
          <Typography variant="subtitle2" className={classes.subtitle1}>
            Declarant:
          </Typography>
          <Box>
            <CustomAvatarGroup
              sizes="24px"
              users={(dashboard.declarant ? [dashboard.declarant] : []) as any}
            />
          </Box>
        </Grid>
        <Grid item xs={12} sm={6} lg={2} md={2}>
          <Typography variant="subtitle2" className={classes.subtitle1}>
            Origine:
          </Typography>
          <Typography variant="body2" className={classes.text1}>
            {dashboard.origine || '-'}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} lg={2} md={2}>
          <Typography variant="subtitle2" className={classes.subtitle1}>
            Priorité:
          </Typography>
          <Typography variant="body2" className={classes.text1}>
            {dashboard.priorite || '-'}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} lg={2} md={2}>
          <Typography variant="subtitle2" className={classes.subtitle1}>
            Fournisseur:
          </Typography>
          <Typography variant="body2" className={classes.text1}>
            {dashboard.fournisseur?.nom || '-'}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} lg={2} md={2}>
          <Typography variant="subtitle2" className={classes.subtitle1}>
            Prestataire:
          </Typography>
          <Typography variant="body2" className={classes.text1}>
            {dashboard.prestataire?.nom || '-'}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} lg={2} md={2}>
          <Typography variant="subtitle2" className={classes.subtitle1}>
            Service concerné:
          </Typography>
          <Typography variant="body2" className={classes.text1}>
            {dashboard.service || '-'}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} lg={2} md={2}>
          <Typography variant="subtitle2" className={classes.subtitle1}>
            Groupe d' Amis:
          </Typography>
          <Typography variant="body2" className={classes.text1}>
            {dashboard.groupe?.nom || '-'}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6} lg={2} md={2}>
          <Typography variant="subtitle2" className={classes.subtitle1}>
            Action-To-Do:
          </Typography>
          <Typography variant="body2" className={classes.text1}>
            {dashboard.actionTodo || 'Non'}
          </Typography>
        </Grid>
        */}
          </Box>
        </Grid>
        {dashboard?.description ? (
          <Box pt={2}>
            <Typography className={classes.subtitle2}>Description:</Typography>
            <Typography variant="body2" className={classes.text1}>
              <div dangerouslySetInnerHTML={{ __html: dashboard.description || '' }} />
            </Typography>
          </Box>
        ) : (
          ''
        )}

        {/* <Box pt={2}>
      <Typography className={classes.subtitle2}>Prise en charge</Typography>
      {dashboard.datePriseEnCharge ? (
        <Typography variant="body2" className={classes.textDate}>
          Date de prise en charge:{' '}
          {moment(dashboard.datePriseEnCharge).format('DD/MM/YY')}
        </Typography>
      ) : (
          ''
        )}
      </Box> */}
        {fullscreen ? (
          <>
            {/*
            {dashboard?.description ? (
              <Box className={classes.detail} p={2}>
                <Typography variant="body2" className={classes.text2}>
                  Détail
                </Typography>
                <Typography variant="body2" className={classes.text2}>
                  <div dangerouslySetInnerHTML={{ __html: dashboard.description || '' }} />
                </Typography>
              </Box>
            ) : (
              ''
            )}
            <Box display="flex" flexDirection="row" className={classes.likeBox}>
              <IconButton
                onClick={event => {
                  event.preventDefault();
                  event.stopPropagation();
                  onRequestLike && dashboard && onRequestLike(dashboard);
                }}
              >
                <Box display="flex" alignItems="center">
                  <ThumbUp />
                  <Typography variant="body2" className={classes.textLike}>
                    J'aime
                  </Typography>
                </Box>
              </IconButton>
              <IconButton
                onClick={event => {
                  event.preventDefault();
                  event.stopPropagation();
                  onRequestShare && dashboard && onRequestShare(dashboard);
                }}
              >
                <Box display="flex" alignItems="center">
                  <Share />
                  <Typography variant="body2" className={classes.textLike}>
                    Partager
                  </Typography>
                </Box>
              </IconButton>
            </Box>
            */}
            {dashboard?.id ? (
              <>
                <Box py={1}>{commentListComponent}</Box>
                <Box py={1}>{commentComponent}</Box>
                <Box pt={4} display="flex" justifyContent="flex-end">
                  <NewCustomButton
                    disabled={clotureSaving}
                    onClick={(event) => {
                      event.preventDefault();
                      event.stopPropagation();
                      onRequestCloture && onRequestCloture(dashboard || '', commentaire);
                      onOpen(false);
                      handleClose();
                    }}
                  >
                    Clôturer
                  </NewCustomButton>
                </Box>
              </>
            ) : (
              ''
            )}
          </>
        ) : (
          <>
            {dashboard?.id && dashboard?.statut !== 'CLOTURE' ? (
              <>
                <Box pt={4} display="flex" justifyContent="flex-end">
                  <NewCustomButton
                    disabled={clotureSaving}
                    onClick={(event) => {
                      event.preventDefault();
                      event.stopPropagation();
                      onRequestCloture && onRequestCloture(dashboard || '', commentaire);
                      handleClose();
                    }}
                  >
                    <Close /> Clôturer
                  </NewCustomButton>
                </Box>
              </>
            ) : (
              ''
            )}
          </>
        )}
      </Grid>
    </Grid>
  );
};

export default Information;
