import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      width: '100%',
      minWidth: 300,
    },
    btnAcionContainer: {
      width: '100%',
      display: 'flex',
      justifyContent: 'flex-end',
    },
  }),
);
