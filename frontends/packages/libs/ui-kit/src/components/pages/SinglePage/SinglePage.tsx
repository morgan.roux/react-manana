import React, { ReactNode, FC } from 'react';
import { Typography } from '@material-ui/core/';
import { withStyles } from '@material-ui/core';
import logo from './images/digital4win.svg';
import styles from './styles';

interface SinglePageProps {
  children?: ReactNode;
  title?: string;
  maxWidth?: string;
  classes: {
    container?: string;
    content?: string;
    top?: string;
    topTitle?: string;
    title?: string;
    img?: string;
  };
}

const SinglePage: FC<SinglePageProps> = (props) => {
  const { classes, children, title, maxWidth = '400px' } = props;
  return (
    <div className={classes.container}>
      <div className={classes.content} style={{ width: '100%', maxWidth }}>
        <div className={classes.top}>
          <img src={logo} className={classes.img} alt="Logo digital4win" />
          <Typography variant="h6" gutterBottom={true} className={classes.title}>
            {title}
          </Typography>
        </div>
        {children}
      </div>
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(SinglePage);
