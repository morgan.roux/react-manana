import { createStyles } from '@material-ui/core';

export default () =>
  createStyles({
    container: {
      display: 'flex',
      justifyContent: 'center',
      minHeight: '100vh',
      padding: '0 24px',
      '@media (max-height: 440px)': {
        height: '100%',
      },
    },
    content: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      '@media (max-width: 442px)': {
        width: 300,
      },
      '@media (max-width: 356px)': {
        width: 270,
      },
    },
    top: {
      display: 'flex',
      flexDirection: 'column',
      marginBottom: 10,
    },
    title: {
      textAlign: 'center',
      letterSpacing: 0,
      color: '#1D1D1D',
      marginTop: 24,
      fontSize: '1.5rem',
      marginBottom: 24,
      '@media (max-width: 1024px)': {
        fontSize: '1.25rem',
      },
    },
    topTitle: {
      margin: 'auto',
      letterSpacing: 0,
      color: '#5D5D5D',
      opacity: 1,
    },
    mb25: {
      marginBottom: 25,
    },
  });
