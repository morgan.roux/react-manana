import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 15,
      width: '100%',
    },
    content: {
      marginBottom: 25,
      width: '100%',
      '& > div:not(:nth-last-child(1))': {
        borderBottom: '1px solid #DCDCDC',
      },
    },

    contentBorder: {
      border: '1px solid #DCDCDC',
      borderRadius: 12,
    },
  })
);

export default useStyles;
