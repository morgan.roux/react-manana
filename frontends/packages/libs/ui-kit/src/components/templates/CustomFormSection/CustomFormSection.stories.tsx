import React from 'react';
import CustomFormSection from './CustomFormSection';


export default {
  title: 'Design system|Molecules/CustomFormSection',
  parameters: {
    info: { inline: true },
  },
};

export const customFormSectionStory = () => {
  return <CustomFormSection title="Informations de base" > Form </CustomFormSection>;
};
