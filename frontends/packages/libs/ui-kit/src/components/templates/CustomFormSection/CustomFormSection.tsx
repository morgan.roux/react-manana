import React, { FC, Fragment } from 'react';
import useStyles from './styles';
import { Typography } from '@material-ui/core';
import classNames from 'classnames';

export interface CustomFormSectionProps {
  title: string;
  bordered?: boolean;
}

const CustomFormSection: FC<CustomFormSectionProps> = ({ title, bordered = true, children }) => {
  const classes = useStyles();

  return (
    <Fragment>
      <Typography className={classes.title}>{title}</Typography>
      <div
        className={bordered ? classNames(classes.content, classes.contentBorder) : classes.content}
      >
        {children}
      </div>
    </Fragment>
  );
};

export default CustomFormSection;
