import { makeStyles, Theme, createStyles } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      width: '100%',
      overflow: 'hidden',
      scrollbarColor: theme.palette.secondary.main,
      height: 'calc(100vh - 86px)',
      '& .ql-editor': {
        minHeight: 100,
      },
    },
    mainContainer: {
      width: '100%',
      overflow: 'auto',
      [theme.breakpoints.down('sm')]: {
        paddingTop: 50,
        height: 'calc(100vh - 155px)',
        overflow: 'auto',
      },
    },
  })
);
