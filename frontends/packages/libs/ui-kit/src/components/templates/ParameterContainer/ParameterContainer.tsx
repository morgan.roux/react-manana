import { AppBar, Box, CssBaseline, Hidden, Tab, Tabs } from '@material-ui/core';
import React, { FC, ReactNode, useMemo } from 'react';
import SideNav from './SideNav';
import { useStyles } from './styles';
import { SideNavProps } from './SideNav/SideNav';
import TopBar, { TopBarProps } from './TopBar/TopBar';
import MobileTopBar from '../../atoms/MobileTopBar';
import Drawer from '../../atoms/Drawer';
import SideNavList, { SideNavListProps } from './SideNavList/SideNavList';
import { Banner } from '../../atoms';

export interface ParameterContainerProps {
  sideNavListProps?: SideNavListProps;
  sideNavProps?: SideNavProps;
  topBarProps?: TopBarProps;
  mainContent?: any;
  optionBtn?: ReactNode[];
  openDrawer?: boolean;
  handleDrawerToggle?: () => void;
  drawerTitle?: string;
  handleSideNavListClick?: (item: string) => void;
  onGoBack?: () => void;
  banner?: boolean;
  sideNavCustomContent?: ReactNode;
  tab?: boolean;
  drawerLgUpClassName?: string;
  drawerMdDownClassName?: string;
  tabScrollableClassNameMobile?: string;
  additionnalSubtitle?: ReactNode;
  isMobile?: boolean;
}

const ParameterContainer: FC<ParameterContainerProps> = ({
  sideNavListProps,
  sideNavProps,
  topBarProps,
  mainContent,
  optionBtn,
  handleDrawerToggle,
  drawerTitle,
  openDrawer,
  onGoBack,
  handleSideNavListClick,
  banner,
  sideNavCustomContent,
  tab,
  drawerLgUpClassName,
  drawerMdDownClassName,
  tabScrollableClassNameMobile,
  additionnalSubtitle,
  isMobile = false,
}) => {
  const classes = useStyles();

  const sideNav = useMemo(
    () => (
      <SideNav
        customContent={sideNavCustomContent || undefined}
        title={sideNavProps && sideNavProps.title}
        searchPlaceholder={sideNavProps && sideNavProps.searchPlaceholder}
        dropdownFilters={sideNavProps && sideNavProps.dropdownFilters}
        onRequestSearchText={sideNavProps && sideNavProps.onRequestSearchText}
        onRequestGoBack={sideNavProps && sideNavProps.onRequestGoBack}
      />
    ),
    [sideNavProps, sideNavCustomContent]
  );

  const activeSideNavItem = sideNavListProps?.sideNavList?.find((item) => item.active);
  const tabsList = sideNavListProps?.sideNavList?.filter((el) => el.mobile);
  const activeTab = tabsList?.find((el) => el.active);
  const activeTabIndex = tabsList?.findIndex((el) => el.active);

  // eslint-disable-next-line @typescript-eslint/ban-types
  const handleTabChange = (_event: React.ChangeEvent<{}>, newValue: number) => {
    if (handleSideNavListClick && tabsList) {
      const tab = tabsList[newValue];
      handleSideNavListClick(tab.item);
    }
  };

  const tabs = (
    <AppBar position="static">
      <Tabs
        variant="scrollable"
        scrollButtons="off"
        value={activeTabIndex}
        onChange={handleTabChange}
        className={tabScrollableClassNameMobile || ''}
      >
        {tabsList?.map((el, index) => (
          <Tab key={`mobile-tab-${index}`} label={el.label} />
        ))}
      </Tabs>
    </AppBar>
  );
  return (
    <Box className={classes.root}>
      <CssBaseline />
      <MobileTopBar
        title={drawerTitle}
        withBackBtn
        handleDrawerToggle={handleDrawerToggle}
        optionBtn={optionBtn}
        withTopMargin
        onGoBack={onGoBack}
      />
      <Drawer
        mobileOpen={openDrawer}
        handleDrawerToggle={handleDrawerToggle}
        mobileTobBarTitle="Filtre"
        width={sideNavListProps?.width}
        lgUpClassName={drawerLgUpClassName}
        mdDownClassName={drawerMdDownClassName}
      >
        {sideNavListProps ? isMobile ? sideNav : <SideNavList {...sideNavListProps} /> : sideNav}
      </Drawer>
      <Box className={classes.mainContainer}>
        <CssBaseline />
        <Hidden smDown implementation="css">
          {banner && (
            <Banner
              titleStyle={{
                flexGrow: 1,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
              style={{ marginBottom: 'unset', height: 60 }}
              filled
              title={activeSideNavItem?.label || topBarProps?.title || ''}
              onClick={() => onGoBack && onGoBack()}
            />
          )}
          {topBarProps && (
            <TopBar
              title={activeSideNavItem?.label || topBarProps?.title}
              actionButtons={topBarProps && topBarProps.actionButtons}
            />
          )}
        </Hidden>
        {tab && (
          <Hidden mdUp implementation="css">
            {activeTab === activeSideNavItem && tabs}
          </Hidden>
        )}
        {additionnalSubtitle || ''}
        {activeSideNavItem?.children || mainContent}
      </Box>
    </Box>
  );
};

export default ParameterContainer;
