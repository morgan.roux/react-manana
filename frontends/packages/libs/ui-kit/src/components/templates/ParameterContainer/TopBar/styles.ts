import { makeStyles, createStyles } from '@material-ui/core';
export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      height: 73,
      borderBottom: '1px solid #E1E1E1',
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: '0 28px',
    },
    title: {
      fontSize: 20,
      fontFamily: 'Roboto',
      fontWeight: 'bold',
    },
  })
);
