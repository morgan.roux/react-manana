import { Theme, makeStyles, createStyles } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 326,
      borderRight: '1px solid #E1E1E1',
      '& > hr': {
        marginTop: 24,
      },
      [theme.breakpoints.down('sm')]: {
        width: '100%',
        paddingLeft: 16,
        paddingRight: 16,
        height: 'calc(100vh - 86px)',
        overflow: 'auto',
      },
      [theme.breakpoints.down('md')]: {
        borderRight: 0,
      },
    },
    scrollFilter: {
      padding: '0 24px 100px',
      [theme.breakpoints.up('md')]: {
        overflow: 'auto',
        height: 'calc(100vh - 140px)',
      },
    },
    customContent: {
      height: 'calc(100vh - 86px)',
      minWidth: 331,
    },
  })
);
