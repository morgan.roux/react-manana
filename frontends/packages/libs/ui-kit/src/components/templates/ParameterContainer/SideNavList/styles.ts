import { Theme, makeStyles, createStyles } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      borderRight: '1px solid rgba(0,0,0,0.1)',
      height: '100%',
    },
    inactive: {
      background: '#FFFFFF',
    },
    active: {
      background: theme.palette.primary.main,
      color: '#FFFFFF',
    },
    withBorderTop: {
      borderTop: '#FFF solid 2px',
    },
    tab: {
      fontFamily: 'Roboto',
      fontSize: 14,
      fontWeight: 'bold',
      cursor: 'pointer',
    },
    scrollable: {
      height: 'calc(100vh - 120px)',
      width: '100%',
      overflowY: 'scroll',
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
  })
);
