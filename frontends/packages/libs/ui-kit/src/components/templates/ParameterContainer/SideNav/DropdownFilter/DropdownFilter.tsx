import {
  Box,
  Checkbox,
  Divider,
  IconButton,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Typography,
  ListItemIcon,
  FormControlLabel,
  Radio,
} from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import React, { FC, ReactNode, useState, useEffect } from 'react';
import classNames from 'classnames';
import { useStyles } from './styles';
import { LoaderSmall } from './../../../../atoms/Loader';
import { ErrorPage } from '../../../../pages/ErrorPage';

export interface FilterData {
  [key: string]: any;
}

export interface DropdownFilterProps {
  title: string;
  dataIdName?: string;
  dataLabelName?: string;
  dataCountName?: string;
  dataIcon?: ReactNode;
  dataNumberName?: string;
  prefixNumber?: string;
  filterList?: {
    loading?: boolean;
    error?: Error;
    data?: FilterData[];
  };
  filtersSelected?: FilterData[];
  children?: ReactNode;
  radio?: boolean;
  selectable?: boolean;
  countable?: boolean;
  numbered?: boolean;
  withExpandMore?: boolean;
  onClickItem?: () => void;

  onItemClick?: (item: FilterData) => void;
  onFiltersSelectionChange?: (filtersSelected: FilterData[]) => void;
}

const DropdownFilter: FC<DropdownFilterProps> = ({
  title,
  filterList,
  dataCountName = 'total',
  children,
  dataLabelName = 'libelle',
  dataIdName = 'id',
  dataNumberName = 'ordre',
  prefixNumber = '',
  filtersSelected = [],
  selectable = true,
  countable = true,
  numbered = false,
  dataIcon,
  radio,
  withExpandMore,
  onFiltersSelectionChange,
  onItemClick,
  onClickItem,
}) => {
  const classes = useStyles();

  const [expandedMore, setExpandedMore] = useState<boolean>(true);

  const [list, setList] = useState<FilterData[]>([]);

  useEffect(() => {
    if (filterList?.data) {
      setList(filterList.data);
    }
  }, [filterList]);

  const isEqualFilter = (filter1: FilterData, filter2: FilterData): boolean => {
    return filter1[dataIdName] === filter2[dataIdName];
  };

  const handleToggleSelectFilter = (filter: FilterData) => {
    onClickItem && onClickItem();
    if (radio && onFiltersSelectionChange) {
      onFiltersSelectionChange([filter]);
      return;
    }

    if (selectable && onFiltersSelectionChange) {
      const foundInFiltersSelected = filtersSelected.some((itemFilter) => isEqualFilter(itemFilter, filter));

      if (foundInFiltersSelected) {
        onFiltersSelectionChange(filtersSelected.filter((itemFilter) => !isEqualFilter(itemFilter, filter)));
      } else {
        onFiltersSelectionChange([...filtersSelected, filter]);
      }
    } else if (onItemClick) {
      onItemClick(filter);
    }
  };

  return (
    <Box className={selectable ? classes.root : classNames(classes.root, classes.unselectableRoot)}>
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        marginBottom="8px"
        onClick={() => setExpandedMore(!expandedMore)}
      >
        <Typography className={!selectable ? classNames(classes.name, classes.unselectableTitle) : classes.name}>
          {title}
        </Typography>
        {withExpandMore && (
          <IconButton className={!selectable ? classes.unselectableTitle : undefined} size="small">
            {expandedMore ? <ExpandLess /> : <ExpandMore />}
          </IconButton>
        )}
      </Box>
      {expandedMore &&
        (filterList?.loading ? (
          <Box style={{ width: '100%', textAlign: 'center' }}>
            <LoaderSmall />
          </Box>
        ) : filterList?.error ? (
          <ErrorPage />
        ) : (
          list.map((filter: FilterData) => (
            <ListItem
              role={undefined}
              dense={true}
              button={true}
              onClick={() => handleToggleSelectFilter(filter)}
              className={
                !selectable && (filtersSelected || []).some((selected) => selected[dataIdName] === filter[dataIdName])
                  ? classNames(classes.unselectableListItemActive, classes.unselectableListItem)
                  : classes.unselectableListItem
              }
              key={filter[dataIdName]}
            >
              <Box className={classes.checkBoxLeftName} display="flex" alignItems="center">
                {selectable &&
                  (radio ? (
                    <FormControlLabel
                      key={`status_filter`}
                      control={
                        <Radio
                          onChange={() => handleToggleSelectFilter(filter)}
                          checked={(filtersSelected || []).some(
                            (selected) => selected[dataIdName] === filter[dataIdName]
                          )}
                          value={filter[dataIdName]}
                        />
                      }
                      label={filter[dataLabelName]}
                      style={{ marginLeft: 0 }}
                    />
                  ) : (
                    <Checkbox
                      tabIndex={-1}
                      checked={filtersSelected.some((itemFilter) => isEqualFilter(itemFilter, filter))}
                      disableRipple={true}
                    />
                  ))}

                {dataIcon && (
                  <ListItemIcon
                    className={
                      !selectable &&
                      (filtersSelected || []).some((selected) => selected[dataIdName] === filter[dataIdName])
                        ? classNames(classes.unselectableListIcon, classes.unselectableListIconActive)
                        : classes.unselectableListIcon
                    }
                  >
                    {dataIcon}
                  </ListItemIcon>
                )}

                {!radio && (
                  <ListItemText
                    className={classes.nom}
                    primary={`${numbered ? `${prefixNumber}.${filter[dataNumberName]}. ` : ``}${filter[dataLabelName]}`}
                  />
                )}
              </Box>
              {countable && (
                <ListItemSecondaryAction>
                  <Typography className={classes.nbrItem}>{filter[dataCountName]}</Typography>
                </ListItemSecondaryAction>
              )}
            </ListItem>
          ))
        ))}
      {expandedMore && children && <Box>{children}</Box>}
      <Divider />
    </Box>
  );
};

export default DropdownFilter;
