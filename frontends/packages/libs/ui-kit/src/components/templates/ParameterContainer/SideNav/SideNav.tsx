import { Box, CssBaseline, Divider, Hidden } from '@material-ui/core';
import React, { FC, ReactNode, useState } from 'react';
import { Banner, DebouncedSearchInput } from '../../../atoms';
import DropdownFilter from './DropdownFilter';
import { DropdownFilterProps } from './DropdownFilter/DropdownFilter';
import { useStyles } from './styles';

export interface SideNavProps {
  onRequestGoBack?: () => void;
  onRequestSearchText?: (value: string) => void;
  searchPlaceholder?: string;
  title?: string;
  dropdownFilters?: DropdownFilterProps[];
  customContent?: ReactNode;
}

const SideNav: FC<SideNavProps> = ({
  onRequestSearchText,
  onRequestGoBack,
  searchPlaceholder,
  title,
  dropdownFilters,
  customContent,
}) => {
  const [searchValue, setSearchValue] = useState<string>('');

  const classes = useStyles();

  const handleGoBackClick = () => {
    if (onRequestGoBack) {
      onRequestGoBack();
    }
  };

  const handleSearchValueChange = (value: string) => {
    if (onRequestSearchText) {
      onRequestSearchText(value);
    }
    setSearchValue(value);
  };

  return (
    <Box className={classes.root}>
      {customContent ? (
        <Box className={classes.customContent}>{customContent}</Box>
      ) : (
        <>
          <CssBaseline />
          <Hidden smDown>
            <Banner
              style={{ padding: '16px 24px 0px' }}
              withDivider={false}
              title={title || 'Check-list'}
              onClick={handleGoBackClick}
            />
            <Box style={{ padding: '16px 24px 0' }}>
              <DebouncedSearchInput
                wait={100}
                fullWidth={false}
                dark
                onChange={handleSearchValueChange}
                value={searchValue}
                placeholder={searchPlaceholder}
              />
            </Box>
            <Divider />
          </Hidden>
          <Box className={classes.scrollFilter}>
            {dropdownFilters &&
              dropdownFilters.map((dropdownFilter, index) => (
                <DropdownFilter key={`dropdown-filter-${index}`} {...dropdownFilter}>
                  {dropdownFilter.children}
                </DropdownFilter>
              ))}
          </Box>
        </>
      )}
    </Box>
  );
};

export default SideNav;
