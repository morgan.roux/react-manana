import React from 'react';
import ParameterContainer from './ParameterContainer';

export default {
  title: 'Design system|templates/ParameterContainer',
  parameters: {
    info: { inline: true },
  },
};

export const itemContainer = () => {
  return (
    <ParameterContainer
      sideNavCustomContent={<h2>TEST SIDENAV</h2>}
      mainContent={<h2>MAIN CONTENT</h2>}
    />
  );
};
