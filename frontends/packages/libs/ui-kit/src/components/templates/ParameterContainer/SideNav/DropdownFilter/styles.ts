import { makeStyles, Theme, createStyles } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginTop: 25,
      listStyleType: 'none',
      '& .main-MuiListItem-gutters': {
        paddingLeft: 0,
      },
      '& .main-MuiDivider-root': {
        marginTop: 20,
      },
    },
    unselectableRoot: {
      width: '100%',
      marginTop: 'unset',
    },
    unselectableListItem: {
      // padding: 0,
      padding: 8,
      // paddingLeft: '8px !important',
    },
    unselectableListItemActive: {
      backgroundColor: theme.palette.secondary.main,
      borderRadius: 5,
      '& > *': {
        color: '#FFFFFF !important',
      },
    },

    unselectableListIcon: {
      width: 30,
      minWidth: 'unset',
    },
    unselectableListIconActive: {
      color: theme.palette.common.white,
    },

    unselectableTitle: {
      color: '#9E9E9E',
      cursor: 'pointer',
      padding: '10px',
      // background: theme.palette.primary.main,
      // color: '#fff',
      width: '100%',
    },

    name: {
      fontFamily: 'Roboto',
      fontSize: '14px',
    },

    nom: {
      fontFamily: 'Roboto',
      fontSize: '14px',
    },
    nbrItem: {
      background: '#F3F3F3',
      fontSize: '0.75rem',
      padding: '2px 8px',
    },
    checkBoxLeftName: {
      padding: 6,
      color: '#000',
      '& .MuiCheckbox-root': {
        padding: 0,
        color: '#000000',
        marginRight: 8,
      },
      '&$checked': {
        color: '#000000',
      },
    },
  })
);
