import { Box, List, ListItem, ListItemText } from '@material-ui/core';
import React, { FC, ReactNode } from 'react';
import { useStyles } from './styles';
import classnames from 'classnames';

export interface SideNavListItem {
  label: string | ReactNode;
  item: string;
  active: boolean;
  children?: ReactNode;
  mobile?: boolean;
}
export interface SideNavListProps {
  sideNavList: SideNavListItem[];
  onSideNavListClick: (item: string) => void;
  component?: ReactNode;
  width?: number;
}

const SideNavList: FC<SideNavListProps> = ({ sideNavList, onSideNavListClick, component }) => {
  const classes = useStyles();

  const active = sideNavList?.find((el) => el.active);

  const handleListItemClick = (item: string) => {
    onSideNavListClick(item);
  };

  if (component) {
    return <>{component}</>;
  }

  return (
    <Box className={classes.scrollable}>
      <List className={classes.root} disablePadding>
        {(active?.item === 'list' ? [active] : sideNavList).map((el, index) => (
          <ListItem
            key={index}
            className={classnames(
              el.active || el.item === 'list' ? classes.active : classes.inactive,
              classes.tab,
              el.item !== 'list' ? classes.withBorderTop : undefined
            )}
          >
            <ListItemText
              key={`sidenavlist-item-${index}`}
              onClick={() => handleListItemClick(el.item)}
              primary={el.label}
            />
          </ListItem>
        ))}
      </List>
    </Box>
  );
};

export default SideNavList;
