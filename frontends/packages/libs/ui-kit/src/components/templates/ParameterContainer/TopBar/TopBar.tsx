import { Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import clsx from 'clsx';
import { useStyles } from './styles';

export interface TopBarProps {
  title?: string | React.ReactNode;
  actionButtons?: any[];
  className?: string;
  onRequestGoBack?: () => void;
}

const TopBar: FC<TopBarProps> = ({ title, actionButtons, className, onRequestGoBack }) => {
  const classes = useStyles();
  console.log('===', onRequestGoBack);

  return (
    <Box className={clsx(classes.root, className)}>
      <Typography className={classes.title}>{title}</Typography>
      <Box>{actionButtons && actionButtons.map((actionButton) => actionButton)}</Box>
    </Box>
  );
};

export default TopBar;
