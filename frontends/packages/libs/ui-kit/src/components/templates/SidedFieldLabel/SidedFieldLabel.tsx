import React, { FC } from 'react';
import useStyles from './styles';
import { Typography } from '@material-ui/core';

export interface SidedFieldLabelProps {
  label: string;
  required?: boolean;
}

const SidedFieldLabel: FC<SidedFieldLabelProps> = ({ label, required, children }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography className={classes.label}>
        {label} {required && <span>*</span>}
      </Typography>
      {children}
    </div>
  );
};

export default SidedFieldLabel;
