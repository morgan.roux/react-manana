import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      inlineSize: '-webkit-fill-available',
      width: '100%',
      display: 'flex',
      // alignItems: 'center',
      // justifyContent: 'space-between',
      flexDirection: 'column',
      padding: '15px 25px',
      '& > div': {
        // maxWidth: 300,
        marginBottom: 0,
        '& input, & .MuiSelect-root': {
          fontWeight: '600',
          fontSize: 16,
          letterSpacing: 0.28,
        },
      },
    },
    label: {
      color: theme.palette.secondary.main,
      '& > span': {
        color: 'red',
      },
    },
  })
);

export default useStyles;
