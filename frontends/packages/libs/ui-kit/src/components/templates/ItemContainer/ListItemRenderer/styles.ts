import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      padding: theme.spacing(3, 2),
      borderRadius: 6,
    },

    noPaddingRoot: {
      display: 'flex',
      flexDirection: 'column',
      borderRadius: 6,
    },
  })
);
