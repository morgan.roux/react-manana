import React, { FC, ReactNode } from 'react';
import { Box, List, ListItem } from '@material-ui/core';
import { Loader } from './../../../atoms/Loader';
import { ErrorPage } from './../../../pages/ErrorPage';
import { useStyles } from './styles';

export interface Identifiable {
  id: string;
}

export interface ListItemRendererProps {
  items?: Identifiable[];

  loading?: boolean;

  error?: Error;

  isMenu?: boolean;

  emptyListComponent: ReactNode;

  listBanner?: ReactNode;
  listItemRenderer<T extends Identifiable>(item: T, index: number): ReactNode;
}

const ListItemRenderer: FC<ListItemRendererProps> = ({
  error,
  loading,
  items,
  emptyListComponent,
  listBanner,
  isMenu,
  listItemRenderer,
}) => {
  const classes = useStyles();
  const displayItems = items || [];

  return (
    <Box className={isMenu ? classes.noPaddingRoot : classes.root}>
      {listBanner && listBanner}

      {error ? (
        <ErrorPage />
      ) : loading ? (
        <Loader />
      ) : displayItems.length === 0 ? (
        emptyListComponent
      ) : (
        <List>
          {displayItems.map((item, index) => (
            <ListItem style={{ padding: 0 }} key={item.id || index}>
              {listItemRenderer(item, index)}
            </ListItem>
          ))}
        </List>
      )}
    </Box>
  );
};

export default ListItemRenderer;
