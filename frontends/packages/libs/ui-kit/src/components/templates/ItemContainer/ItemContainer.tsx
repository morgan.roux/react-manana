import React, { FC, ReactNode, CSSProperties, MouseEvent, useState } from 'react';
import { Box, Divider, Grid, IconButton } from '@material-ui/core';
import { ListItemRendererProps, ListItemRenderer } from './ListItemRenderer';
import { useStyles } from './styles';
import { Banner, DebouncedSearchInput } from '../../atoms';
import classnames from 'classnames';
import { useApplicationContext } from '@lib/common';
import { Dispatch } from 'react';

export interface IntemContainerProps extends ListItemRendererProps {
  style?: CSSProperties;
  menuStyle?: CSSProperties;
  rightStyle?: CSSProperties;
  banner?: {
    title: string;
    style?: CSSProperties;
    withDivider?: boolean;
    onClick: (event: MouseEvent) => void;
  };
  onListScroll?: (event: React.UIEvent<HTMLDivElement, UIEvent>) => void;
  detailRenderer(): ReactNode;
  onSearchChange?: (input: string) => void;
  minWidth?: boolean;
  mobileFilter?: boolean;
  filterIcon?: any;
  filterState?: boolean;
  setFilterState?: Dispatch<boolean>;
}

const ItemContainer: FC<IntemContainerProps> = ({
  onListScroll,
  detailRenderer,
  rightStyle,
  style,
  menuStyle,
  banner,
  onSearchChange,
  mobileFilter,
  filterIcon,
  filterState,
  setFilterState,
  ...restProps
}) => {
  const classes = useStyles();
  const [textToSearch, setTextToSearch] = useState<string>('');
  const handleSearchChange = (value: string) => {
    setTextToSearch(value);
    if (onSearchChange) {
      onSearchChange(value);
    }
  };
  const handleFilterChange = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    setFilterState && setFilterState(!filterState);
  };
  const { isMobile } = useApplicationContext();
  // useEffect(() => {
  //   setShowFilter(filterState || false);
  // }, [filterState]);
  return (
    <Box className={classes.root} style={style}>
      <div
        className={mobileFilter && isMobile ? classes.leftListContainerMobile : classes.leftListContainer}
        style={menuStyle}
      >
        {banner && (
          <Banner title={banner.title} withDivider={banner.withDivider} style={banner.style} onClick={banner.onClick} />
        )}
        {onSearchChange && (
          <Box
            className={
              mobileFilter && isMobile ? classnames(classes.themeSearch, classes.sidedByIcon) : classes.themeSearch
            }
          >
            <DebouncedSearchInput
              fullWidth={false}
              value={textToSearch}
              wait={100}
              onChange={handleSearchChange}
              minWidth={250}
            />
          </Box>
        )}
        {mobileFilter && (
          <Box className={classes.boxFilter}>
            <IconButton onClick={handleFilterChange}>{filterIcon}</IconButton>
          </Box>
        )}

        {(banner || onSearchChange) && <Divider style={{ marginTop: 23 }} />}
        <Grid
          onScroll={onListScroll}
          item
          className={classes.leftList}
          style={isMobile ? (filterState ? { background: '#fff' } : { display: 'none' }) : {}}
        >
          <ListItemRenderer {...restProps} />
        </Grid>
      </div>
      <div
        className={
          mobileFilter && isMobile
            ? classnames(classes.rightContainer, classes.withFilterMobile)
            : classes.rightContainer
        }
        style={rightStyle}
      >
        {detailRenderer()}
      </div>
    </Box>
  );
};

export default ItemContainer;
