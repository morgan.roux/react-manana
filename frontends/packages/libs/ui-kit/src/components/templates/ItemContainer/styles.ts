import { makeStyles, Theme, createStyles } from '@material-ui/core';
const drawerWidth = 275;
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      scrollbarColor: theme.palette.secondary.main,
      height: 'calc(100vh - 86px)',
      width: '100%',
    },
    drawer: {
      position: 'sticky',
      top: 0,
      width: drawerWidth,
      flexShrink: 0,
      height: 'calc(100vh - 86px)',
      borderRight: '1px solid #E5E5E5',
    },
    leftListContainer: {
      maxWidth: 360,
      borderRight: '1px solid #E5E5E5',
      //  overflowY: 'auto',
      //  overflowX: 'hidden',
    },
    leftListContainerMobile: {
      maxWidth: '100%',
      width: '100% !important',
      position: 'absolute',
      borderRight: '1px solid #E5E5E5',
      overflowY: 'scroll',
      height: '100%',
      //  overflowX: 'hidden',
    },
    rightContainer: {
      flex: 1,
      overflowY: 'auto',
      width: '100%',
    },
    withFilterMobile: {
      paddingTop: 135,
    },
    leftList: {
      // borderRight: '2px solid #E5E5E5',

      height: 'calc(100vh - 140px)',
      overflowY: 'auto',
    },
    makeStylesWhiteBackground: {
      minWidth: '100%',
    },
    themeSearch: {
      marginLeft: 40,
      maxWidth: 100,
    },
    boxFilter: {
      width: 60,
      textAlign: 'right',
      display: 'inline-block',
      '& img': {
        filter: 'brightness(0)',
      },
    },
    sidedByIcon: {
      marginLeft: 8,
      width: 'calc(100% - 80px)',
      display: 'inline-block',
      maxWidth: '100%',
    },
  })
);
