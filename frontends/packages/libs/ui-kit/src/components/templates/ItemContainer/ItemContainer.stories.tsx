import React from 'react';
import ItemContainer from './ItemContainer';
import { ListItem, Box } from '@material-ui/core';

export default {
  title: 'Design system|templates/ItemContainer',
  parameters: {
    info: { inline: true },
  },
};

export const itemContainer = () => {
  return (
    <ItemContainer
      emptyListComponent={<div>Empty</div>}
      listItemRenderer={(item) => <ListItem>{item.id}</ListItem>}
      detailRenderer={() => <Box>Detail of *</Box>}
    />
  );
};
