import React, { FC, ReactNode, MouseEvent, CSSProperties } from 'react';
import { Box, CssBaseline, Divider, Hidden, IconButton } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import classNames from 'classnames';
import useStyles from './style';

export interface CustomContainerProps {
  bannerBack?: boolean;
  filled?: boolean;
  onBackClick?: (event: MouseEvent) => void;
  bannerLeft?: ReactNode;
  bannerTitle?: string;
  bannerRight?: ReactNode | string;
  contentStyle?: CSSProperties;
  bannerContentStyle?: CSSProperties;
  bannerTitleStyle?: CSSProperties;
  onlyBackAndTitle?: boolean;
  fullWidth?: boolean;
  breakpoint?: 'md' | 'sm';
  showBanner?: boolean;
  divider?: boolean;
}

const CustomContainer: FC<CustomContainerProps> = ({
  bannerBack,
  onBackClick,
  bannerTitle,
  bannerRight,
  bannerLeft,
  filled,
  children,
  contentStyle,
  bannerContentStyle,
  bannerTitleStyle,
  onlyBackAndTitle = false,
  breakpoint = 'sm',
  showBanner = true,
  divider = true,
}) => {
  const classes = useStyles();

  const handleBackClick = (event: MouseEvent): void => {
    if (onBackClick) {
      onBackClick(event);
    }
  };

  const hiddenOptionProps = breakpoint === 'sm' ? { smDown: true } : { mdDown: true };
  return (
    <Box className={classes.root}>
      <CssBaseline />
      {showBanner && (
        <Hidden {...hiddenOptionProps} implementation="css">
          <Box className={classes.bannerRoot}>
            <Box
              className={
                filled ? classNames(classes.bannerContent, classes.filledBannerContent) : classes.bannerContent
              }
              style={onlyBackAndTitle ? bannerContentStyle : { padding: '0px 30px', ...(bannerContentStyle || {}) }}
            >
              {bannerBack && (
                <Box>
                  <IconButton>
                    <ArrowBackIcon
                      className={filled ? classes.filledBannerContent : undefined}
                      onClick={handleBackClick}
                    />
                  </IconButton>
                </Box>
              )}
              {bannerLeft && <Box>{bannerLeft}</Box>}
              {bannerTitle && (
                <Box
                  className={classes.bannerTitle}
                  style={
                    onlyBackAndTitle
                      ? { flexGrow: 1, textAlign: 'center', ...(bannerTitleStyle || {}) }
                      : bannerTitleStyle
                  }
                >
                  {bannerTitle}
                </Box>
              )}
              {bannerRight && <Box>{bannerRight}</Box>}
            </Box>
            <Divider />
          </Box>
        </Hidden>
      )}
      {divider ? <Divider /> : ''}

      <Box style={contentStyle} className={classes.content}>
        {children}
      </Box>
    </Box>
  );
};

export default CustomContainer;
