import { createStyles, Theme, makeStyles, lighten } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      maxWidth: '100%',
      minHeight: '100%',
    },

    bannerRoot: {},
    bannerContent: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: 70,
      opacity: 1,
      //    padding: '0px 30px',
      position: 'sticky',
      top: 0,
      zIndex: 1,
    },

    filledBannerContent: {
      background: lighten(theme.palette.primary.main, 0.1),
      color: '#FFFFFF',
    },

    bannerTitle: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 20,
    },

    content: {
      width: '100%',
      margin: '0 auto',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      // [theme.breakpoints.down('sm')]: {
      //   padding: 16,
      // },
    },
  })
);

export default useStyles;
