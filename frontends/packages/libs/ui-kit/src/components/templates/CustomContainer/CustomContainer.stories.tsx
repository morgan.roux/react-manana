import { Box, Button } from '@material-ui/core';
import React from 'react';
import CustomContainer from './CustomContainer';
import { CustomButton } from '../../atoms/CustomButton'

export default {
  title: 'Design system|Molecules/CustomContainer',
  parameters: {
    info: { inline: true },
  },
};

export const withTitle = () => {
  return <CustomContainer bannerTitle="Test title" />;
};

export const withBack = () => {
  return <CustomContainer bannerBack={true} bannerTitle="Test title" />;
};

export const fullProps = () => {
  return (
    <CustomContainer bannerBack={true} bannerTitle="Test title" bannerRight={<Button>Ajouter</Button>}>
      <Box>Content</Box>
    </CustomContainer>
  );
};


export const checklist = () => {
  return (
    <CustomContainer bannerTitle="Liste des check-list" bannerRight={<CustomButton color="primary">Ajouter un check-list</CustomButton>}>
      <Box>Content</Box>
    </CustomContainer>
  );
};


export const newChecklist = () => {
  return (
    <CustomContainer filled={true} bannerBack={true} bannerTitle="Ajout de check-list" bannerRight={<CustomButton color="primary">Créer</CustomButton>}>
      <Box>Form</Box>
    </CustomContainer>
  );
};
