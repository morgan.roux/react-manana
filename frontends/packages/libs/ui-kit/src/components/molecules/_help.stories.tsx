import React from 'react';
export default {
  title: 'Design system|Molecules',
};

export const Help = () => (
  <p>
    They are the composition of one or more components of atoms. Here we begin to compose complex components and reuse
    some of those components. Molecules can have their own properties and create functionalities by using atoms, which
    alone have no function or action.
  </p>
);
