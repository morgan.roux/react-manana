import React from 'react';
import { TodoTask as Task } from './TodoTask';

export default {
  title: 'Design system|Molecules/Todotask',
  parameters: {
    info: { inline: true },
  },
};

const Tasks = {
  id: '1',
  description: 'description',
  pharmacie: {
    id: '12',
  },
  planMarketingType: {
    id: '0',
    code: '',
    libelle: '',
  },
  fonction: { id: '', libelle: '' },
  importance: {
    id: '1',
    ordre: '1',
    libelle: 'libelle',
    couleur: '#209c13',
  },
  urgence: {
    id: '11',
    code: 'A',
    libelle: 'libelle',
  },
  jourlancement: 2,
};

export const taskStory = () => {
  return <Task task={Tasks} />;
};
