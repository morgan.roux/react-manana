import { Box, Checkbox } from '@material-ui/core';
import React, { FC, ReactNode, useCallback, useEffect, useState } from 'react';
import { useStyles } from './styles';
import { Today, ChatBubble, RadioButtonChecked, RadioButtonUnchecked } from '@material-ui/icons';
import 'moment/locale/fr';
import moment from 'moment';

export interface ActionValue {
  id?: string;
  description?: string;
  dateDebut?: any;
  status?: string;
  importance?: {
    id: string;
    ordre: string;
    libelle: string;
    couleur: string;
  };
  urgence?: {
    id: string;
    code: string;
    libelle: string;
  };
  nbComment?: number;
}

export interface TodoTaskProps {
  task?: ActionValue;
  iconIndication?: ReactNode;
  onChangeStatutClick?: (id: string, statut: 'DONE' | 'ACTIVE') => void;
}

export const TodoTask: FC<TodoTaskProps> = ({ task, onChangeStatutClick, iconIndication }) => {
  const classes = useStyles();
  const [taskStatus, setTaskStatus] = useState<'DONE' | 'ACTIVE'>();

  useEffect(() => {
    if (task?.status) {
      setTaskStatus(task.status as any);
    }
  }, [task?.status]);

  const handleChangeCheckbox = useCallback(
    (__event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
      if (task && task.id) {
        if (checked) {
          onChangeStatutClick && onChangeStatutClick(task.id, 'DONE');
          setTaskStatus('DONE');
        } else {
          onChangeStatutClick && onChangeStatutClick(task.id, 'ACTIVE');
          setTaskStatus('ACTIVE');
        }
      }
    },
    [task]
  );

  return (
    <Box className={classes.root}>
      <Box className={classes.labelContainer}>
        <Box className={classes.radioContainer}>
          <Checkbox
            onChange={handleChangeCheckbox}
            icon={<RadioButtonUnchecked />}
            checkedIcon={<RadioButtonChecked />}
            checked={taskStatus === 'DONE'}
          />
        </Box>
        <Box className={classes.description} dangerouslySetInnerHTML={{ __html: task?.description || '' }} />
      </Box>
      <Box className={classes.othersInformationContainer}>
        <Box className={classes.containnerOtherInformation}>
          <span>
            <Today />
          </span>
          &nbsp;&nbsp;&nbsp;
          <span>{moment(task?.dateDebut).format('LL')}</span>
        </Box>
        <Box className={classes.containnerOtherInformation}>
          <span>
            <ChatBubble />
          </span>
          &nbsp;&nbsp;&nbsp;
          <span>{task?.nbComment || 0}</span>
        </Box>
        <Box className={classes.containnerOtherInformation}>
          <Box
            style={{ background: task?.importance?.couleur || 'black' }}
            className={classes.InportanceUrgenceContainer}
          >
            <span>
              {task?.importance?.ordre}.&nbsp;{task?.importance?.libelle}
            </span>
            <span>
              {task?.urgence?.code}:&nbsp;{task?.urgence?.libelle}
            </span>
          </Box>
        </Box>
        <Box className={classes.containnerOtherInformation}>{iconIndication}</Box>
      </Box>
    </Box>
  );
};
