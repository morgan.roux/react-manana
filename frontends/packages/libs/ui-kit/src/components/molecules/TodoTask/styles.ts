import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      margin: '8px 0',
      border: `1px solid ${theme.palette.grey[300]}`,
      padding: 16,
      borderRadius: 10,
    },
    labelContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'baseline',
      marginBottom: 32,
    },
    description: {
      fontFamily: 'Roboto',
      fontSize: 16,
      marginLeft: 16,
    },
    radioContainer: {
      margin: -8,
    },
    othersInformationContainer: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
    },
    containnerOtherInformation: {
      display: 'flex',
      flexDirection: 'row',
      marginRight: 96,
      alignItems: 'center',
      flexWrap: 'wrap',
      fontSize: 16,
      fontFamily: 'Roboto',
    },
    InportanceUrgenceContainer: {
      padding: 8,
      display: 'flex',
      flexDirection: 'column',
      alignItem: 'center',
      color: '#fff',
      fontSize: 13,
      borderRadius: 4,
      '& span:last-child': {
        marginTop: 16,
      },
    },
  }),
);
