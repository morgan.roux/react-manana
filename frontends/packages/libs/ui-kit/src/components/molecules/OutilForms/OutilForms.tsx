import { Box } from '@material-ui/core';
import { CustomFormTextField } from './../../atoms/CustomTextField';
import React, { FC, ChangeEvent, CSSProperties } from 'react';
import useStyles from './style';
import { Dropzone } from '@lib/common';

export interface OutilFormsProps {
  style?: CSSProperties;
  selectedFile: File[];
  numeroOrdre: string;
  intitule: string;
  setSelectedFile: (file: File[]) => void;
  setNumeroOrdre: (event: ChangeEvent<HTMLInputElement>) => void;
  setIntitule: (event: ChangeEvent<HTMLInputElement>) => void;
}

const OutilForms: FC<OutilFormsProps> = ({
  style,
  selectedFile,
  numeroOrdre,
  intitule,
  setSelectedFile,
  setIntitule,
  setNumeroOrdre,
}) => {
  const classes = useStyles();

  return (
    <Box style={style}>
      <Dropzone
        contentText="Glissez et déposez votre document ici (Obligatoire)"
        selectedFiles={selectedFile}
        setSelectedFiles={setSelectedFile}
        multiple={false}
        acceptFiles="application/pdf"
      />
      <CustomFormTextField
        variant="outlined"
        label="Numéro d'ordre"
        type="number"
        value={numeroOrdre}
        onChange={setNumeroOrdre}
        className={classes.form}
        required
        style={{ marginTop: 15 }}
      />
      <CustomFormTextField variant="outlined" label="Intitulé" value={intitule} onChange={setIntitule} required />
    </Box>
  );
};

export default OutilForms;
