import React , { ChangeEvent, useState } from 'react'
import OutilForms from './OutilForms';
export default {
    title: 'Design system|Molecules/OutilForms',
    parameters: {
        info: { inline: true }
    } 
};

export const outilFormsStory = () => {
    const [ selectedFile , setSelectedFIle ] = useState<File []>([]);
    const [ numeroOrdre , setNumeroOrdre ] = useState<string>('');
    const [ intitule , setIntitule ] = useState<string>('');

    const handleNumeroOrdreChange = (event : ChangeEvent<HTMLInputElement>) : void => {
        setNumeroOrdre((event.target as HTMLInputElement).value);
    };

    const handleIntituleChange = (event : ChangeEvent<HTMLInputElement>) : void => {
        setIntitule((event.target as HTMLInputElement).value);
    };

    return (
        <OutilForms 
            numeroOrdre={numeroOrdre}
            intitule={intitule}
            setSelectedFile={setSelectedFIle}
            selectedFile={selectedFile}
            setNumeroOrdre={handleNumeroOrdreChange}
            setIntitule={handleIntituleChange}
        />
    );
};