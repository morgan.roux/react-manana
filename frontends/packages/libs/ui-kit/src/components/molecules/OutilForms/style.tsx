import { makeStyles, createStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      width: '100%',
    },

    form: {
      width: '100%',
      marginBottom: '15px',
    },
  })
);

export default useStyles;
