export interface Task {
  id: string;
  description?: string;
  jourLancement: number;
  importance?: ImportanceProps;
  idPharmacie?: string;
  idUrgence?: string;
  idPlanMarketingType: string;
  fonction?: FonctionProps;
  avantApres?: string;
  participants?: [];
  pendant: boolean;
  active?: boolean;
}
export interface ImportanceProps {
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}
export interface FonctionProps {
  id?: string;
  libelle: string;
}
// export interface PlanMarketingTypeProps {
//   id?: string;
//   code: string;
//   libelle: string;
// }
export interface PharmacieProps {
  id?: string;
}
