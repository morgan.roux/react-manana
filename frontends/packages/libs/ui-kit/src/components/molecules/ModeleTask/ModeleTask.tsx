import { Box, Icon, Typography } from '@material-ui/core';
import { Cancel, CheckCircle, Delete, Edit, Event, EventNote } from '@material-ui/icons/';
import React, { FC } from 'react';
//import AvatarInput from '../../atoms/AvatarInput';
import { MoreOptions } from '../../atoms/MoreOptionsComponent';
import { ModeleOperationMarketingInfoFragment } from '@lib/common/src/federation';
import useStyles from './styles';

export interface ModeleTaskProps {
  isAdmin: boolean;
  task: ModeleOperationMarketingInfoFragment;
  handlePersonnaliseTask: (task: ModeleOperationMarketingInfoFragment) => void;
  handleChangeTaskStatus: (task: ModeleOperationMarketingInfoFragment) => void;
  handleEditTask: (task: ModeleOperationMarketingInfoFragment) => void;
  handleDeletetask: (task: ModeleOperationMarketingInfoFragment) => void;
}

export const ModeleTask: FC<ModeleTaskProps> = ({
  isAdmin,
  task,
  handlePersonnaliseTask,
  handleChangeTaskStatus,
  handleEditTask,
  handleDeletetask,
}) => {
  const classes = useStyles();

  const userPharmacieButtons = [
    {
      menuItemLabel: {
        title: 'Personnaliser',
        icon: <Edit />,
      },
      onClick: () => handlePersonnaliseTask(task),
    },
    {
      menuItemLabel: {
        title: task.active ? 'Desactiver' : 'Activer',
        icon: task.active ? <Cancel /> : <CheckCircle />,
      },
      onClick: () => handleChangeTaskStatus(task),
    },
  ];

  const userAdminButtons = [
    {
      menuItemLabel: {
        title: 'Modifier',
        icon: <Edit />,
      },
      onClick: () => handleEditTask(task),
    },
    {
      menuItemLabel: {
        title: 'Supprimer',
        icon: <Delete />,
      },
      onClick: () => handleDeletetask(task),
    },
  ];

  return (
    <>
      <Box border={1} borderColor="#dbd7d7" borderRadius={8} mt={3}>
        <Box ml={3}>
          <Box display="flex" justifyContent="space-between" alignItems="center">
            <Box display="flex" alignItems="center">
              {task.active && <CheckCircle style={{ marginRight: 8 }} color="secondary" />}
              <div dangerouslySetInnerHTML={{ __html: task.description || '' }} />
            </Box>
            <Box display="flex">
              <span style={{ marginTop: 15 }}>
                {task.pendant ? '(Pendant)' : task?.jourLancement && task.jourLancement < 0 ? '(Avant)' : '(Après)'}
              </span>
              <MoreOptions
                items={isAdmin ? [...userPharmacieButtons, ...userAdminButtons] : [...userPharmacieButtons]}
              />
            </Box>
          </Box>
          <Box mt={2}>
            <Box className={classes.contentLabels}>
              <Box className={classes.labels}>
                <Icon>
                  <Event />
                </Icon>
                {task?.jourLancement && task.jourLancement >= 0 ? (
                  <Typography className={classes.labelText} style={{ fontSize: '16px' }}>
                    J {` +${task?.jourLancement}`}
                  </Typography>
                ) : (
                  <Typography className={classes.labelText} style={{ fontSize: '16px' }}>
                    J {` ${task?.jourLancement}`}
                  </Typography>
                )}
              </Box>
              <Box
                className={classes.labels}
                style={{
                  borderRadius: '5px',
                  backgroundColor: task.importance?.couleur,
                  height: '50px',
                  textAlign: 'center',
                  fontSize: '12px',
                }}
                ml={4}
                display="flex"
                alignItems="center"
                justifyContent="center"
                padding="8px"
              >
                <Typography style={{ color: 'white', fontSize: '12px' }}>
                  {`${task.importance?.ordre} : ${task.importance?.libelle}`}
                </Typography>
              </Box>
              <Box className={classes.labels} ml={4}>
                <EventNote />
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};
