import { Box } from '@material-ui/core';
import React , { FC } from 'react';
import { TwoColumns } from '../../atoms/TwoColumns';

export interface TwoColumnsParametersProps {
    rightContent : any;
    leftHeaderContent : any;
}

const TwoColumnsParameters : FC<TwoColumnsParametersProps> = ({ leftHeaderContent , children , rightContent}) => {

    return (
        <Box>
            <TwoColumns
                rightContent={rightContent}
                leftOperationHeader={
                    <>
                        { leftHeaderContent }
                    </>
                }
            >
                {
                    children
                }
            </TwoColumns>
        </Box>
    );
};

export default TwoColumnsParameters;