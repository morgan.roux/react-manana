import { Box } from '@material-ui/core';
import React , { FC } from 'react';

const CardThemeList : FC = ({children}) => {
    return (
        <Box>
            { children }
        </Box>
    );
};

export default CardThemeList;