import { Box } from '@material-ui/core';
import React , { FC } from 'react';
import useStyles from './style';

const GroupText : FC = ({children}) => {
    const classes = useStyles();

    return (
        <Box className={classes.root} >
            {
                children
            }
        </Box>
    );
};

export default GroupText;