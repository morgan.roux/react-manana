import { createStyles, makeStyles } from "@material-ui/core";

const useStyles = makeStyles( () => 
    createStyles({
        root : {
            padding : 15
        },
    })
);

export default useStyles;