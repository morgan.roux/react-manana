import { Box } from '@material-ui/core';
import React, { FC, ReactNode } from 'react';
import useStyles from './style';
import { DefinitionBase } from './DefinitionBase';
import CustomTabs, { TabInterface } from '../../atoms/CustomTabs/CustomTabs';
import OutilsAssocies from './OutilsAssocies/OutilsAssocies';
import DocumentsReference from './DocumentsReference/DocumentsReference';
import { IExigence } from '../../atoms/Exigence/Exigence';

export interface ExigenceTabProps {
  exigence: IExigence;
  commentComponentRenderer: (exigence: IExigence) => ReactNode;
  onOutilClick: (typologie: string, id: string) => void;
}

const ExigenceTab: FC<ExigenceTabProps> = ({ exigence, commentComponentRenderer, onOutilClick }) => {
  const classes = useStyles();

  const tabs: TabInterface[] = [
    {
      id: 0,
      label: 'Définition de base',
      content: <DefinitionBase exigence={exigence} />,
    },
    {
      id: 1,
      label: 'Outils associés',
      content: <OutilsAssocies exigence={exigence} onOutilClick={onOutilClick} />,
    },
    {
      id: 2,
      label: 'Documents de référence',
      content: <DocumentsReference exigence={exigence} onOutilClick={onOutilClick} />,
    },
    {
      id: 3,
      label: 'Commentaires',
      content: commentComponentRenderer(exigence),
    },
  ];

  return (
    <Box className={classes.root}>
      <CustomTabs hideArrow={true} tabs={tabs} />
    </Box>
  );
};

export default ExigenceTab;
