import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import useStyles from './style';
import { Text } from '../../../atoms/Text';
import { SpanBold } from '../../../atoms/SpanBold';
import { IExigence, IDocument } from '../../../atoms/Exigence/Exigence';

export interface DocumentsReferenceProps {
  exigence: IExigence;
  onOutilClick: (typologie: string, id: string) => void;
}

const DocumentsReference: FC<DocumentsReferenceProps> = ({ exigence, onOutilClick }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <SpanBold label="Liste de documents associés à l'exigence" />
      {exigence.outils &&
        exigence.outils.documents &&
        exigence.outils.documents.map((currentData: IDocument) => (
          <Text
            key={currentData.id}
            label={currentData.libelle}
            onClick={() => onOutilClick('document', currentData.id)}
          />
        ))}
    </Box>
  );
};

export default DocumentsReference;
