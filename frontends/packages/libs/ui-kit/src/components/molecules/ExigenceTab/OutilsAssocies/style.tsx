import { createStyles, makeStyles } from "@material-ui/core";

const useStyles = makeStyles( () => 
    createStyles({
        root : {
            padding : 15,
            font : 'normal normal bold 14px/17px Roboto',
            letterSpacing : 0,
            color : '#212121',
            opacity : 1,
            display : 'block'
        }
    })
);

export default useStyles;