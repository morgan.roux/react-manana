import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import { GroupText } from '../../GroupText';
import { Text } from '../../../atoms/Text';
import useStyles from './style';
import { SpanBold } from '../../../atoms/SpanBold';
import {
  IExigence,
  IAffiche,
  IEnregistrement,
  ICheckList,
  IProcedure,
  IMemo,
} from '../../../atoms/Exigence/Exigence';

export interface OutilsAssociesProps {
  exigence: IExigence;
  onOutilClick: (typologie: string, id: string) => void;
}

const OutilsAssocies: FC<OutilsAssociesProps> = ({ exigence, onOutilClick }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <SpanBold label="Liste des outils associés à l'exigence" />
      <GroupText>
        {exigence.outils &&
          exigence.outils.affiches.map((currentData: IAffiche) => (
            <Text
              key={currentData.id}
              label={`${currentData.code}- ${currentData.libelle}`}
              onClick={() => onOutilClick('affiche', currentData.id)}
            />
          ))}
        {exigence.outils &&
          exigence.outils.enregistrements.map((currentData: IEnregistrement) => (
            <Text
              key={currentData.id}
              label={`${currentData.code}- ${currentData.libelle}`}
              onClick={() => onOutilClick('enregistrement', currentData.id)}
            />
          ))}
        {exigence.outils &&
          exigence.outils.memos.map((currentData: IMemo) => (
            <Text
              key={currentData.id}
              label={`${currentData.code}- ${currentData.libelle}`}
              onClick={() => onOutilClick('memo', currentData.id)}
            />
          ))}
        {exigence.outils &&
          exigence.outils.procedures.map((currentData: IProcedure) => (
            <Text
              key={currentData.id}
              label={`${currentData.code}- ${currentData.libelle}`}
              onClick={() => onOutilClick('procedure', currentData.id)}
            />
          ))}
        {exigence.outils &&
          exigence.outils.checklists.map((currentData: ICheckList) => (
            <Text
              key={currentData.id}
              label={`${currentData.code}- ${currentData.libelle}`}
              onClick={() => onOutilClick('checklist', currentData.id)}
            />
          ))}
      </GroupText>
    </Box>
  );
};

export default OutilsAssocies;
