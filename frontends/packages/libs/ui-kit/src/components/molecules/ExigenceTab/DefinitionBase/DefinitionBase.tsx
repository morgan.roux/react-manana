import { Box, ListItem, ListItemText } from '@material-ui/core';
import React , { FC } from 'react';
import { SpanBold } from '../../../atoms/SpanBold';
import SousTheme  from '../../../atoms/SousTheme/SousTheme';
import useStyles from './style';
import { IExigence } from '../../../atoms/Exigence/Exigence';

export interface DefinitionBaseProps {
    exigence : IExigence
}

const DefinitionBase : FC<DefinitionBaseProps> = ({exigence}) => {
    const classes = useStyles();

    return (
        <Box className={classes.root} >
            <SpanBold label='Finalité' />
                <ListItem button={true}>
                    <ListItemText primary={`${exigence.finalite}`} />
                </ListItem>
            <SpanBold label='Questions à se poser' />
            {
                exigence.questions && exigence.questions.map( ( currentData : any , index : number ) => (
                    <SousTheme key={index} nom={currentData.question} />
                ) )
            }
            <SpanBold label='Exemples de pratiques et de preuves' />
            {
                exigence.exemples && exigence.exemples.map( ( currentData : any , index : number ) => (
                    <SousTheme key={index} nom={currentData.exemple} />
                ) )
            }
        </Box>
    );
};

export default DefinitionBase;