import React from 'react';
import SelectBoxList from './SelectBoxList';
import SelectBoxItem, { SelectBoxItemProps } from '../../atoms/SelectBoxItem/SelectBoxItem';

const handleClick = ( name : string ) => {
    alert(name);
};

const data: SelectBoxItemProps [] = [
    {
        text: 'Matin',
        onClick: handleClick.bind(null,'Matin'),
        image: {
            src: 'images/molecules.png',
            alt: 'GGGG'
        }
    },
    {
        text: 'Après-Midi',
        onClick: handleClick.bind(null,'APrès M'),
        image: {
            src: 'images/molecules.png',
            alt: 'GGGG'
        }
    },
    {
        text: 'Soir',
        onClick: handleClick.bind(null,'Soir'),
        image: {
            src: 'images/molecules.png',
            alt: 'GGGG'
        }
    },
    {
        text: 'Nuit',
        onClick: handleClick.bind(null,'Nuit'),
        image: {
            src: 'images/molecules.png',
            alt: 'GGGG'
        }
    },
];

export default {
    title: 'Design system|Molecules/SelectBoxList',
    parameters: {
        info: { inline: true }
    }
}

export const ListBox = () => (
    <SelectBoxList>
        {
            data.map( ( currentData : SelectBoxItemProps , index : number ) => (
                    <SelectBoxItem  key={index} image={currentData.image} text={currentData.text} onClick={currentData.onClick} />
            ) )
        }
    </SelectBoxList>
);