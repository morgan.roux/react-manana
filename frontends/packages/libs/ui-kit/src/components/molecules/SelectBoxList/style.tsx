import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    choiceContainer: {
      [theme.breakpoints.down('xs')]: {
        paddingBottom: 95,
        display: 'block',
      },
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'normal',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      width: '100%',
      padding: '0 24px',
      alignContent: 'center',
      '@media (max-width: 768px)': {
        justifyContent: 'center',
        alignItems: 'center',
      },
      [theme.breakpoints.down('sm')]: {
        flexDirection: 'column',
        alignContent: 'center',
        alignItems: 'center',
      },
    },
  })
);

export default useStyles;
