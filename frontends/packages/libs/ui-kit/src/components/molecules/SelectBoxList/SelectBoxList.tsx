import { Box } from '@material-ui/core';
import React, { FC, CSSProperties } from 'react';
import useStyles from './style';

interface SelectBoxListProps {
  style?: CSSProperties;
}

const SelectBoxList: FC<SelectBoxListProps> = ({ children, style }) => {
  const classes = useStyles();

  return (
    <Box className={classes.choiceContainer} style={style}>
      {children}
    </Box>
  );
};

export default SelectBoxList;