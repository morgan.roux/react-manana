import { makeStyles } from '@material-ui/styles';
import { createStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      position: 'relative',
      '& .apexcharts-legend-marker': {
        borderRadius: '0px !important',
      },

      '& .apexcharts-legend-series': {
        marginRight: '80px !important',
      },
      '& .apexcharts-legend-text': {
        fontWeight: 'bold !important',
        fontSize: '18px',
        font: 'normal normal medium 18px/22px Roboto',
      },
      '& .apexcharts-title-text': {
        display: 'none',
        color: '#27272F !important',
        fontSize: '20px !important',
        fontWeight: 'bold !important',
      },
      '& .apexcharts-zoomin-icon': {
        display: 'none !important',
      },
      '& .apexcharts-zoomout-icon': {
        display: 'none !important',
      },
      '& .apexcharts-pan-icon': {
        display: 'none !important',
      },
      '& .apexcharts-reset-icon': {
        display: 'none !important',
      },
      '& .apexcharts-zoom-icon.apexcharts-selected': {
        display: 'none !important',
      },
      '& .apexcharts-text.apexcharts-yaxis-label': {
        fontSize: 16,
      },
      ' & .apexcharts-text.apexcharts-xaxis-label ': {
        fontSize: '16px !important',
        color: '#424242 !important',
      },
      ' & .apexcharts-toolbar': {
        display: 'none',
      },
      ' & .apexcharts-legend': {
        paddingTop: '50px !important',
      },
    },
    otherContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'first baseline',
      flexWrap: 'wrap',
      flexDirection: 'row',
      '& h2': {
        margin: 0,
        color: '#27272F !important',
        fontSize: '20px !important',
        fontWeight: 'bold !important',
      },
    },
    subtitle: {
      position: 'absolute',
      bottom: 50,
      display: 'flex',
      width: '100%',
      right: 18,
      '&>div': {
        width: '33.33%',
        textAlign: 'center',
        fontWeight: 'bold',
      },
    },
  })
);

export default useStyles;
