import CustomPieChart from './CustomPieChart';
import { CustomPieChartProps } from './interfaces';

export { CustomPieChart, CustomPieChartProps };
