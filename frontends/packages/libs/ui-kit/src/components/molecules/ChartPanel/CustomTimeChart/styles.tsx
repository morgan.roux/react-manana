import { makeStyles } from '@material-ui/core';
import { createStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      '& .k-widget.k-toolbar.k-scheduler-toolbar': {
        // display: 'none',
      },
      '& .k-widget.k-toolbar.k-scheduler-footer': {
        display: 'none',
      },
      '& .MuiBox-root.MuiBox-root-8.header.row.flex-middle': {
        fontWeight: 'bold',
      },
      '& .MuiBox-root.MuiBox-root-15.header.row.flex-middle': {
        fontWeight: 'bold',
      },
      '& .MuiBox-root.MuiBox-root-22.days.row': {
        fontWeight: 'bold',
      },
      ' & .MuiTableCell-root.MuiTableCell-body': {
        borderBottom: 'none',
      },
    },

    icon: {
      fontFamily: 'Material Icons',
      fontStyle: 'normal',
      display: 'inline-block',
    },
    jours: {
      color: '#777',
      wordSpacing: '3px',
    },
    titre: {
      color: '#27272F !important',
      fontSize: '20px !important',
      fontWeight: 'bold',
    },
    barChart: {
      height: 26,
      borderRadius: 2,
      padding: 8,
      backgroundColor: 'red',
      margin: '2px 0 2px 0',
    },
    barCharText: {
      color: 'white',
    },
    day: {},
    month: {},
    week: {},
  })
);
export default useStyles;
