import React, { FC } from 'react';
import Chart from 'react-apexcharts';
import { Box } from '@material-ui/core';
// import { Fullscreen, FullscreenExit } from '@material-ui/icons';
import { useStyles } from './style';
import { CustomLineChartProps } from './interfaces';

const CustomLineChart: FC<CustomLineChartProps> = ({ series, optionsProps, otherComponent }) => {
  const classes = useStyles();

  // const { fullScreen, onRequestFullScreen } = optionsProps;

  const options = {
    chart: {
      height: 350,
      type: 'line',
      zoom: {
        enabled: false,
      },
    },
    dataLabels: {
      enabled: true,
      style: {
        fontSize: 18,
        color: '#000',
      },
      background: {
        enabled: false,
        foreColor: '#000',
      },
      formatter: (value: any) => {
        return `${value} ${optionsProps.formatter}`;
        // return `${opts.w.config.series[opts.seriesIndex].data} ${optionsProps.formatter || ''}`;
      },
    },
    stroke: {
      curve: 'straight',
    },
    title: {
      text: optionsProps.title || '',
      align: 'left',
    },
    grid: {
      row: {
        colors: optionsProps.gridRowColor || ['transparent', 'transparent'], // takes an array which will be repeated on columns
        opacity: 0.5,
      },
      padding: {
        left: 56,
        right: 86,
      },
    },
    xaxis: {
      categories: optionsProps.categories,
    },
    yaxis: {
      labels: {
        formatter: (value: any) => {
          return `${value}  ${optionsProps.formatter}`;
        },
      },
    },
  };

  // const handleFullScreen = () => {
  //   onRequestFullScreen && onRequestFullScreen(!fullScreen);
  // };

  return (
    <>
      <Box className={classes.root}>
        {/* <Box className={classes.fullScreen} onClick={handleFullScreen}>
          {/* <IconButton>{fullScreen ? <FullscreenExit /> : <Fullscreen />}</IconButton> 
        </Box> */}
        {otherComponent}
        <Chart series={series} options={options as any} type="line" height={350} />
      </Box>
    </>
  );
};

export default CustomLineChart;
