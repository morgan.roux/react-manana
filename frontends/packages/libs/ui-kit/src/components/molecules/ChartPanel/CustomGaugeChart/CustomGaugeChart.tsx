import React, { FC } from 'react';
import GaugeChart from 'react-gauge-chart';
import { useStyles } from './styles';
import { CustomGaugeChartProps } from './interface';
import { Box, Typography } from '@material-ui/core';

const CustomGaugeChart: FC<CustomGaugeChartProps> = ({ options, descriptionComponent, title, style }) => {
  const classes = useStyles();
  const number = options.percent * 100;
  return (
    <Box className={classes.root}>
      <Box>
        <Typography className={classes.title}>{title}</Typography>
      </Box>
      <GaugeChart
        id="gauge-chart4"
        nrOfLevels={options.level}
        arcPadding={0}
        arcWidth={0.3}
        cornerRadius={0}
        percent={options.percent > 1 ? 1 : options.percent}
        textColor="#000"
        className={classes.root}
        fontSize="1.5em"
        colors={options.colors}
        formatTextValue={(value: string) => {
          console.log(value);
          return `${Math.round(number)} %`;
        }}
        style={style}
      />
      {descriptionComponent && <Box>{descriptionComponent}</Box>}
    </Box>
  );
};

export default CustomGaugeChart;
