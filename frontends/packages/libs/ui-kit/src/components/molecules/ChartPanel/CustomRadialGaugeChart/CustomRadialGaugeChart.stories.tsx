import React from 'react';
import { Box, Typography } from '@material-ui/core';
import CustomRadialGaugeChart from './CustomRadialGaugeChart';
import { useStyles } from './styles';

export default {
  title: 'Design system|Molecules/ChartPanel/CustomRadialGaugeChart',
  parameters: {
    info: { inline: true },
  },
};

export const CustomRadialGaugeChartStory = () => {
  const classes = useStyles();
  const radialOptions = {
    shape: 'arrow',
    scale: {
      minorUnit: 5,
      majorUnit: 20,
      max: 180,
      ranges: [
        { from: 0, to: 20, color: '#ffc700' },
        { from: 20, to: 120, color: '#ffc200' },
        { from: 120, to: 180, color: '#ffc500' },
      ],
      labels: {
        visible: false,
        position: 'none',
      },
      radialLabels: {
        visible: false,
        position: 'none',
      },
    },
    pointer: [
      {
        value: 10,
        color: '#ffd246',
      },
    ],
  };

  const descriptionComponent = (
    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
      <Box>
        <Typography className={classes.radialTitle}>13000 $</Typography>
      </Box>
      <Box display="flex" flexDirection="column" py={2}>
        <Box display="flex" flexDirection="row">
          <Typography className={classes.infoTitre}>Titre 1: &nbsp;</Typography>
          <Typography>2000 $</Typography>
        </Box>
        <Box display="flex" flexDirection="row">
          <Typography className={classes.infoTitre}>Titre 2: &nbsp;</Typography>
          <Typography>11000 $</Typography>
        </Box>
      </Box>
    </Box>
  );

  return (
    <CustomRadialGaugeChart
      radialOptions={radialOptions}
      radialTitle="Titre du gauge"
      descriptionComponent={descriptionComponent}
    />
  );
};
