import CustomGaugeChart from './CustomGaugeChart';
import { CustomGaugeChartProps } from './interface';

export { CustomGaugeChart, CustomGaugeChartProps };
