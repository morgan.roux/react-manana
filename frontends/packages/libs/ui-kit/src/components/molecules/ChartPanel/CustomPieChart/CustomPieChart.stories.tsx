import React from 'react';
import CustomPieChart from './CustomPieChart';

export default {
  title: 'Design system|Molecules/ChartPanel/CustomPieChart',
  parameters: {
    info: { inline: true },
  },
};

export const CustomPieChartStory = () => {
  const type = 'donut';
  const options = {
    value: [
      {
        color: '#2F9520',
        legend: 'Serie 1',
        serie: 13000,
      },
      {
        color: '#D53555',
        legend: 'Serie 2',
        serie: 15000,
      },
      {
        color: '#35D590',
        legend: 'Serie 3',
        serie: 35000,
      },
    ],
    formatter: '£',
    title: 'Titre du custom pie chart',
    info: [
      {
        titre: 'Titre 1',
        value: 'value 1',
      },
      {
        titre: 'Titre 2',
        value: 'value 2',
      },
    ],
  };

  return <CustomPieChart type={type} optionsProps={options} otherComponent={<>2021</>} />;
};
