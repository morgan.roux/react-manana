import moment from 'moment';
import React from 'react';
import { CustomTimeChart } from './CustomTimeChart';

export default {
  title: 'Design system|Molecules/ChartPanel/CustomTimeChart',
  parameters: {
    info: { inline: true },
  },
};

const titreChart = 'Suivi opérationnel';

const data = [
  {
    option: {
      title: 'titre1',
    },
    bar: {
      libelle: 'blue',
      intervalle: [
        {
          startDate: moment('2021-03-01').toDate(),
          endDate: moment('2021-03-02').toDate(),
        },
        {
          startDate: moment('2021-03-04').toDate(),
          endDate: moment('2021-03-07').toDate(),
        },
      ],
      color: '#63B8DD',
    },
  },
  {
    option: {
      title: 'titre2',
    },
    bar: {
      libelle: 'red',
      intervalle: [
        {
          startDate: moment('2021-03-02').toDate(),
          endDate: moment('2021-03-03').toDate(),
        },
        {
          startDate: moment('2021-03-05').toDate(),
          endDate: moment('2021-03-06').toDate(),
        },
      ],
      color: '#F460A5',
    },
  },
];

export const customTimeChartStory = () => {
  return <CustomTimeChart currentDate={new Date()} barLine={data} titreChart={titreChart} />;
};

// export const CustomLineChartStory = () => {
//   const dataTask = [
//     {
//       taskId: 5,
//       createurId: 2,
//       titre: 'CCCCC',
//       decription: '',
//       debutTask: new Date('2021-06-26T00:00:00.000Z'),
//       finTask: new Date('2021-06-26'),
//       StartTimezone: null,
//       EndTimezone: null,
//       RecurrenceRule: null,
//       RecurrenceID: null,
//       RecurrenceException: null,
//       isAllDay: false,
//       roomId: 1,
//     },
//   ];
//   return <CustomTimeLineChart dataTask={dataTask} resources={[]} />;
// };
