import React, { FC } from 'react';
import ReactApexChart from 'react-apexcharts';
import { Grid, Box, Typography, IconButton } from '@material-ui/core';
import { Fullscreen, FullscreenExit } from '@material-ui/icons';
import { useStyles } from './style';
import { CustomPieChartProps } from './interfaces';

const CustomPieChart: FC<CustomPieChartProps> = ({ type, optionsProps, otherComponent }) => {
  const classes = useStyles();
  const { fullScreen, onRequestFullScreen } = optionsProps;

  const series = optionsProps.value.map((option) => {
    return option.serie;
  });

  const options = {
    title: {
      text: optionsProps.title || '',
    },
    colors: optionsProps.value.map((option) => {
      return option.color;
    }),
    chart: {
      type: type,
      height: '100%',
    },
    dataLabels: {
      enabled: true,
      style: {
        fontSize: 18,
        background: '#FFF',
      },
      background: {
        enabled: true,
        borderRadius: 5,
        foreColor: '#000',
      },
      dropShadow: {
        enabled: false,
      },
      formatter: (__val: any, opts: any) => {
        return `${opts.w.config.series[opts.seriesIndex]} ${optionsProps.formatter || ''}`;
      },
    },
    plotOptions: {
      pie: {
        startAngle: -90,
        endAngle: 90,
        offsetY: 10,
        donut: {
          labels: {
            show: true,
            name: {
              show: true,
            },
            value: {
              show: true,
            },
            total: {
              show: false,
              showAlways: false,
              label: '',
              // formatter: (_w: any) => {
              //   return (
              //     // w.globals.seriesTotals
              //     //   .reduce((a: number, b: number) => {
              //     //     return a + b;
              //     //   }, 0)
              //     //   .toLocaleString() + optionsProps.formatter || ''
              //   );
              // },
            },
          },
        },
      },
    },
    legend: {
      show: false,
    },
    grid: {
      padding: {
        bottom: -80,
      },
    },
    responsive: [
      {
        breakpoint: 600,
        options: {
          chart: {
            width: 400,
          },
          dataLabels: {
            style: {
              fontSize: 12,
            },
          },
        },
      },
    ],
  };

  const handleFullScreen = () => {
    onRequestFullScreen && onRequestFullScreen(!fullScreen);
  };

  return (
    <Grid className={classes.root}>
      <Box className={classes.fullScreen} onClick={handleFullScreen}>
        {otherComponent}
        <IconButton>{fullScreen ? <FullscreenExit /> : <Fullscreen />}</IconButton>
      </Box>
      <ReactApexChart options={options as any} series={series} type={type} height={350} />
      <Box display="flex" flexDirection="row" justifyContent="center" className={classes.label}>
        {(optionsProps.value || []).map((option, index) => {
          return (
            <Box px={2} key={index} display="flex" flexDirection="row" alignItems="center">
              <Box
                style={{ backgroundColor: option.color }}
                className={classes.boxLegendColor}
                mr={1}
              />
              <Typography variant="caption" className={classes.legend}>
                {option.legend}
              </Typography>
            </Box>
          );
        })}
      </Box>
      <Box display="flex" flexDirection="column" justifyContent="center" alignItems="center" py={2}>
        {(optionsProps.info || []).map((option, index) => {
          return (
            <Box display="flex" key={index} flexDirection="row" alignItems="center">
              <Typography className={classes.infoTitre}>{option.titre}: &nbsp;</Typography>
              <Typography> {option.value}</Typography>
            </Box>
          );
        })}
      </Box>
    </Grid>
  );
};

export default CustomPieChart;

// "apexcharts-series apexcharts-pie-series"
