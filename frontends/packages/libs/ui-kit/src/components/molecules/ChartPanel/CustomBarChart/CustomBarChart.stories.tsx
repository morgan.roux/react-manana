import React from 'react';
import { CustomBarChart } from './CustomBarChart';
// import { CustomTimeChart } from '../CustomTimeChart';

export default {
  title: 'Design system|Molecules/ChartPanel/CustomBarChart',
  parameters: {
    info: { inline: true },
  },
};

export const customBarChartStory = () => {
  const series = [
    {
      name: 'Prévue',
      type: 'column',
      data: [10, 30, 40, 20],
    },
    {
      name: 'Réalisée',
      type: 'column',
      data: [0, 25, 45, 50],
    },
  ];

  const MoisAnnee = ['Janvier', 'Février', 'Mars', 'Avril'];

  const options = {
    title: 'Rémunération mensuelle',
    xaxis: MoisAnnee,
    formatter: '€',
    withRefPrecedent: true,
  };

  return <CustomBarChart series={series} options={options} />;
};

export const customBarChartStory2 = () => {
  const series = [
    {
      name: 'Réaliser',
      type: 'column',
      data: [10000, 200000, 20000.5, 100000.5],
    },
    {
      name: 'Prévue',
      type: 'column',
      data: [10000.1, 30000, 30000.1, 40000],
    },
  ];

  const MoisAnnee = ['Janvier', 'Février', 'Mars', 'Avril'];

  const options = {
    title: 'Rémunération mensuelle',
    xaxis: MoisAnnee,
    formatter: 'Ar',
  };

  return <CustomBarChart series={series} options={options} />;
};
