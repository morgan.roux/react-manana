import React, { FC } from 'react';
import { RadialGauge } from '@progress/kendo-react-gauges';
import { Box, Typography } from '@material-ui/core';
import { CustomRadialGaugeChartProps } from './interface';
import { useStyles } from './styles';

const CustomRadialGaugeChart: FC<CustomRadialGaugeChartProps> = ({
  radialOptions,
  radialTitle,
  descriptionComponent,
}) => {
  const classes = useStyles();

  return (
    <Box display="flex" flexDirection="column" alignItems="center" className={classes.root}>
      <Box>
        <Typography className={classes.radialTitle}>{radialTitle}</Typography>
      </Box>
      <div className={classes.gauges}>
        <RadialGauge {...radialOptions} />
      </div>
      <Box>{descriptionComponent}</Box>
    </Box>
  );
};

export default CustomRadialGaugeChart;
