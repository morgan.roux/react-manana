import { ReactNode } from 'react';

export interface Label {
  color: string;
  legend: string;
  serie: number;
}

export interface Option {
  value: Label[];
  formatter?: string;
  title?: string;
  fullScreen?: boolean;
  onRequestFullScreen?: (fullScreen: boolean) => void;
  info?: Info[];
}

export interface Info {
  titre: string;
  value: string;
}

export interface CustomPieChartProps {
  type: 'donut' | 'pie';
  optionsProps: Option;
  otherComponent?: ReactNode;
}
