// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable @typescript-eslint/class-name-casing */
import {
  Box,
  Divider,
  IconButton,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from '@material-ui/core';
import { ChevronLeft, ChevronRight, Fullscreen, FullscreenExit } from '@material-ui/icons';
import { addDays, addMonths, addWeeks, startOfWeek, subMonths, subWeeks } from 'date-fns';
import moment from 'moment';
import React, { FC, useState } from 'react';
import useStyles from './styles';

export interface CustomTimeChartProps {
  currentDate: Date;
  barLine: BarChartProps[];
  titreChart: string;
  fullScreen?: boolean;
  onRequestFullScreen?: (fullScreen: boolean) => void;
}

export interface MonthProps {
  date: Date;
  onRequestMonth: (date: Date) => void;
}

export interface WeekProps {
  date: Date;
  onRequestWeek: (date: Date) => void;
}

export interface DayProps {
  date: Date;
}

export interface IntervalProps {
  startDate: Date;
  endDate: Date;
}

export interface BarProps {
  libelle: string;
  intervalle: IntervalProps[];
  color: string;
}

export interface OptionProps {
  title: string;
}

export interface BarLineProps {
  barLine: BarChartProps[];
  date: Date;
}

export interface BarChartProps {
  option: OptionProps;
  bar: BarProps;
}

export interface CustomTimeChartProps {
  currentDate: Date;
  barLine: BarChartProps[];
  titreChart: string;
}

export interface MonthProps {
  date: Date;
  onRequestMonth: (date: Date) => void;
}

export interface WeekProps {
  date: Date;
  onRequestWeek: (date: Date) => void;
}

export interface DayProps {
  date: Date;
}

export interface intervalProps {
  startDate: Date;
  endDate: Date;
}

export interface BarProps {
  libelle: string;
  intervalle: intervalProps[];
  color: string;
}

export interface optionProps {
  title: string;
}

export interface BarLineProps {
  barLine: BarChartProps[];
  date: Date;
}

export interface BarChartProps {
  option: optionProps;
  bar: BarProps;
}

const Month: FC<MonthProps> = ({ date, onRequestMonth }) => {
  const classes = useStyles();
  const handleBefore = () => {
    onRequestMonth(subMonths(date, 1));
  };
  const handleNext = () => {
    onRequestMonth(addMonths(date, 1));
  };

  return (
    <Box className={classes.month}>
      <Box className="header row flex-middle" display="flex">
        <Box className="col col-start" flexDirection="row">
          <Box mr={5} className={classes.icon}>
            <IconButton onClick={handleBefore}>
              <ChevronLeft />
            </IconButton>
          </Box>
        </Box>
        <Box className="col col-center" display="flex" mt={2}>
          <span>{moment(date).locale('fr').format('MMMM YYYY')}</span>
        </Box>
        <Box className="col col-end">
          <Box ml={5} className="icon" display="flex">
            <IconButton onClick={handleNext}>
              <ChevronRight />
            </IconButton>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

const Week: FC<WeekProps> = ({ date, onRequestWeek }) => {
  const classes = useStyles();
  const handleBefore = () => {
    onRequestWeek(subWeeks(date, 1));
  };
  const handleNext = () => {
    onRequestWeek(addWeeks(date, 1));
  };

  return (
    <Box className={classes.week}>
      <Box className="header row flex-middle" display="flex">
        <Box className="col col-start" flexDirection="row">
          <Box mr={5} className={classes.icon}>
            <IconButton onClick={handleBefore}>
              <ChevronLeft />
            </IconButton>
          </Box>
        </Box>
        <Box className="col col-center" display="flex" mt={2}>
          <span>{`Semaine ${moment(date, 'MMDDYYYY').isoWeek()}`}</span>
        </Box>
        <Box className="col col-end">
          <Box ml={5} className="icon" display="flex">
            <IconButton onClick={handleNext}>
              <ChevronRight />
            </IconButton>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

const Day = (date: Date) => {
  const days = [];
  const startDate = startOfWeek(date);
  for (let i = 1; i < 8; i++) {
    const fullDay = moment(addDays(startDate, i));
    const day = parseInt(fullDay.startOf('day').locale('fr').format('D'));
    const date = fullDay.startOf('day').locale('fr').format('ddd');
    days.push({
      day: day < 10 ? `0${day}` : `${day} `,
      jour: date,
      fullDay,
    });
  }
  return days;
};

export const BarChart: FC<BarLineProps> = ({ barLine }) => {
  const classes = useStyles();
  return (
    <>
      {(barLine || []).map((element) => {
        return (
          // eslint-disable-next-line react/jsx-key
          <Box marginBottom={2} display="flex" flexDirection="row">
            <Box marginRight={3}>
              <span>{element.option.title}</span>
            </Box>
            <Box
              className={classes.barChart}
              style={{
                width: '',
                backgroundColor: element.bar.color,
              }}
            >
              <span style={{ color: 'white', fontWeight: 'bold', marginRight: 4 }}>
                {element.bar.libelle}
              </span>
            </Box>
          </Box>
        );
      })}
    </>
  );
};

export const CustomTimeChart: FC<CustomTimeChartProps> = ({
  fullScreen,
  currentDate,
  barLine,
  titreChart,
  onRequestFullScreen,
}) => {
  const classes = useStyles();
  const [date, setDate] = useState<Date>(currentDate || new Date());

  const handleChangeDate = (data: Date) => {
    setDate(data);
  };

  const handleFullScreen = () => {
    onRequestFullScreen && onRequestFullScreen(!fullScreen);
  };

  return (
    <>
      <Box display="flex" flexDirection="row" justifyContent="space-between" alignItems="center">
        <Box>
          <Typography className={classes.titre}>{titreChart}</Typography>
        </Box>
        <Box>
          <IconButton onClick={handleFullScreen}>
            {fullScreen ? <FullscreenExit /> : <Fullscreen />}
          </IconButton>
        </Box>
      </Box>
      <Box className={classes.root} display="flex" flexDirection="column" mt={2}>
        <Box>
          <Box
            className={classes.root}
            display="flex"
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
          >
            <Month date={date} onRequestMonth={handleChangeDate} />
            <Week date={date} onRequestWeek={handleChangeDate} />
          </Box>
        </Box>

        <Table style={{ width: 200 }}>
          <TableHead>
            <TableRow>
              <TableCell />
              {Day(date).map((element) => {
                return (
                  <TableCell className={classes.jours} key={element.jour}>
                    <Box>{element.jour}</Box>
                    <Typography>{element.day}</Typography>
                  </TableCell>
                );
              })}
            </TableRow>
          </TableHead>
          <Divider variant="middle" />
          <TableBody>
            {(barLine || []).map((element) => {
              const intervalles = element.bar.intervalle;
              return (
                // eslint-disable-next-line react/jsx-key
                <TableRow>
                  <TableCell>{element.option.title}</TableCell>
                  {Day(date).map((dateElement) => {
                    // const debutWeek = addDays(startOfWeek(date), 1);
                    const dateSuivi = intervalles.filter((intervalle) => {
                      return moment(dateElement.fullDay).isBetween(
                        moment(intervalle.startDate),
                        moment(intervalle.endDate, undefined, '[]'),
                      );
                    });
                    console.log(
                      '----------------------dataSuivi---------------------------: ',
                      dateSuivi,
                    );
                    return (
                      <>
                        <TableCell key={dateElement.day}>
                          {intervalles.length > 0 && (
                            <Box
                              className={classes.barChart}
                              style={{ backgroundColor: element.bar.color }}
                            >
                              <Typography variant="caption" className={classes.barCharText}>
                                {element.bar.libelle}
                              </Typography>
                            </Box>
                          )}
                        </TableCell>
                      </>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
        {/* <Box py={1} alignItems="start">
          <BarChart barLine={barLine} date={date} />
        </Box> */}
      </Box>
    </>
  );
};

// width: 'nombre intervalle date * width date 1'
