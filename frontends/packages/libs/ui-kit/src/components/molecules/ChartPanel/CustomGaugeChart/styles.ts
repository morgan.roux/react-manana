import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((__theme: Theme) =>
  createStyles({
    root: {
      fontFamily: 'Roboto',
      ' & text': {
        color: 'red',
      },
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    title: {
      fontWeight: 'bold',
      fontSize: 16,
    },
  }),
);
