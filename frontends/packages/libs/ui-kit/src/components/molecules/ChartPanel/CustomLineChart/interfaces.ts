import { ReactNode } from 'react';

export interface Serie {
  name: string;
  data: number[];
}

export interface Options {
  title?: string;
  gridRowColor?: string[];
  formatter?: string;
  categories: string[];
  fullScreen?: boolean;
  onRequestFullScreen?: (fullScreen: boolean) => void;
}

export interface CustomLineChartProps {
  series: Serie[];
  optionsProps: Options;
  otherComponent?: ReactNode;
}
