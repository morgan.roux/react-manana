import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      [theme.breakpoints.down('sm')]: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      },
      // ' & .k-gauge text': {
      //   display: 'none',
      // },
    },
    boxLegendColor: {
      height: 16,
      width: 16,
    },
    legend: {
      fontSize: 16,
      [theme.breakpoints.down('sm')]: {
        fontSize: 12,
      },
    },
    fullScreen: {
      position: 'absolute',
      top: -15,
      right: '3px',
      zIndex: 99,
    },
    label: {
      marginTop: '-50px',
    },
    infoTitre: {
      color: 'rgba(0,0,0,0.7)',
    },
    radialTitle: {
      fontWeight: 'bold',
      fontSize: 16,
    },
    gauges: {
      ' & .k-gauge div svg g g g path': {
        display: 'none',
      },
    },
  }),
);
