import { ReactNode } from 'react';

export interface CustomRadialGaugeChartProps {
  radialOptions: RadialOptions;
  radialTitle?: string;
  descriptionComponent?: ReactNode;
}

export interface RadialOptions {
  shape: string;
  scale: RadialScale;
  pointer: RadialPointer[];
}

export interface RadialScale {
  minorUnit: number;
  majorUnit: number;
  max: number;
  ranges: RadialRange[];
}

export interface RadialRange {
  from: number;
  to: number;
  color: string;
}

export interface RadialPointer {
  value: number;
  color: string;
}
