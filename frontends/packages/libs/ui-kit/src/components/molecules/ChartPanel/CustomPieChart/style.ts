import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      [theme.breakpoints.down('sm')]: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      },
      ' & .apexcharts-legend-marker': {
        borderRadius: '0px !important',
      },
      ' & .apexcharts-text.apexcharts-datalabel-value': {
        fontSize: '1.8em',
        marginTop: 16,
      },
      ' & .apexcharts-datalabels-group': {
        transform: 'translate(0,-50px) scale(1) !important',
      },
      ' & .apexcharts-text.apexcharts-datalabel-label': {
        fontSize: '1.5em',
        color: '#ADB5B1',
      },
      ' & .apexcharts-title-text': {
        fontSize: '20px !important',
        fontWeight: 'bold',
        [theme.breakpoints.down('sm')]: {
          fontSize: '16px',
        },
      },
    },
    boxLegendColor: {
      height: 16,
      width: 16,
    },
    legend: {
      fontSize: 16,
      [theme.breakpoints.down('sm')]: {
        fontSize: 12,
      },
    },
    fullScreen: {
      position: 'absolute',
      top: -15,
      right: '3px',
      zIndex: 99,
    },
    label: {
      marginTop: '-50px',
    },
    infoTitre: {
      color: 'rgba(0,0,0,0.7)',
    },
  }),
);
