import React from 'react';
import CustomLineChart from './CustomLineChart';

export default {
  title: 'Design system|Molecules/ChartPanel/CustomLineChart',
  parameters: {
    info: { inline: true },
  },
};

export const LineChartStory = () => {
  const series = [
    {
      name: 'Serie 1',
      data: [4500, 5200, 3800, 2400, 3300, 2600, 2100, 2000, 600],
      color: '#D12363',
    },
    {
      name: 'Serie 2',
      data: [3500, 4100, 6200, 4200, 1300, 2600, 2900, 3700, 3600],
      color: '#23D1C0',
    },
  ];
  const options = {
    title: 'Titre du customLineChart',
    gridRowColor: ['transparent', 'transparent'],
    formatter: '$',
    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
  };

  return <CustomLineChart series={series} optionsProps={options} />;
};
