import { ReactNode, CSSProperties } from 'react';

export interface CustomGaugeChartProps {
  options: CustomGaugeChartOptionsProps;
  descriptionComponent?: ReactNode;
  title?: string;
  style?: CSSProperties;
}

export interface CustomGaugeChartOptionsProps {
  percent: number;
  level: number;
  colors?: string[];
}
