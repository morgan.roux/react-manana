import React from 'react';
import CustomGaugeChart from './CustomGaugeChart';

export default {
  title: 'Design system|Molecules/ChartPanel/CustomGaugeChart',
  parameters: {
    info: { inline: true },
  },
};

export const CustomGaugeChartStory = () => {
  const options = {
    percent: 1.103333,
    level: 3,
    colors: ['#fb2900', '#fbb102', '#1da745'],
  };
  return (
    <CustomGaugeChart
      options={options}
      descriptionComponent={<>Description component</>}
      title="Titre du chart"
    />
  );
};
