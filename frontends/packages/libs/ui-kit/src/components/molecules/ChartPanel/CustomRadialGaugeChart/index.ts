import CustomRadialGaugeChart from './CustomRadialGaugeChart';
import { CustomRadialGaugeChartProps } from './interface';

export { CustomRadialGaugeChart, CustomRadialGaugeChartProps };
