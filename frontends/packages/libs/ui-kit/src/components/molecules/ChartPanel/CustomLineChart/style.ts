import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      position: 'relative',
      ' & .apexcharts-legend-marker': {
        borderRadius: '0px !important',
        height: '6px !important',
      },
      ' & .apexcharts-datalabel': {
        fill: '#424242',
        fontSize: 16,
        transform: 'translate(0px, -15px)',
      },
      ' & .apexcharts-text.apexcharts-yaxis-label ': {
        fontSize: 16,
      },
      ' & .apexcharts-text.apexcharts-xaxis-label ': {
        fontSize: '16px !important',
        color: '#424242 !important',
      },
      ' & .apexcharts-legend-text': {
        fontSize: '18px !important',
      },
      ' & .apexcharts-title-text': {
        fontSize: '20px !important',
        fontWeight: 'bold',
        [theme.breakpoints.down('sm')]: {
          fontSize: '16px',
        },
      },
      ' & .apexcharts-toolbar': {
        display: 'none',
      },
    },
    fullScreen: {
      position: 'absolute',
      top: -15,
      right: '3px',
      zIndex: 99,
    },
  }),
);
