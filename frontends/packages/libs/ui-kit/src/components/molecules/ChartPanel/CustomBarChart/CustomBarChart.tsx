import { Box /* IconButton */ } from '@material-ui/core';
// import { Fullscreen, FullscreenExit } from '@material-ui/icons';
import React, { useState, useEffect, FC, ReactNode } from 'react';
import ReactApexChart from 'react-apexcharts';
import useStyles from './styles';
interface Serie {
  name: string;
  type: string;
  data: number[] | string[];
}

export interface Options {
  title: string;
  xaxis: string[];
  colorBar?: string[];
  formatter?: string;
  fullScreen?: boolean;
  onRequestFullScreen?: (fullScreen: boolean) => void;
  withRefPrecedent?: boolean;
}

export interface CustomBarChartProps {
  options: Options;
  series: Serie[] | undefined;
  otherComponent?: ReactNode;
  withSubtitle?: boolean;
}

export const CustomBarChart: FC<CustomBarChartProps> = ({ options, series, otherComponent, withSubtitle }) => {
  const [witdh, setWitdh] = useState<any>('100%');
  const classes = useStyles();
  const optionProps = {
    dataLabels: {
      enabled: true,
      formatter: (_val: any, opts: any) => {
        console.log('-------------------opts--------------------------: ', options);
        if (opts.dataPointIndex % 2 === 1 && series && options.withRefPrecedent) {
          const currentValue = series[opts.seriesIndex].data[opts.dataPointIndex];
          const refValue = series[opts.seriesIndex].data[opts.dataPointIndex - 1] || 0;
          if (typeof refValue === 'number' && typeof currentValue === 'number') {
            const diffValue = currentValue - refValue;
            if (refValue) {
              const diffValuePercent = (diffValue * 100) / refValue;
              const sign = diffValuePercent >= 0 ? '+' : '';
              if (diffValuePercent) {
                return `${sign}${Math.round(diffValuePercent)}%`;
              } else {
                return '';
              }
            } else {
              return '';
            }
          }
          return '';
        }

        if (opts.seriesIndex % 2 === 1 && series && !options.withRefPrecedent) {
          const refValue = series[0].data[opts.dataPointIndex];
          const currentValue = series[1].data[opts.dataPointIndex];
          if (typeof refValue === 'number' && typeof currentValue === 'number') {
            const diffValue = currentValue - refValue;
            const diffValuePercent = (diffValue * 100) / refValue;
            const sign = diffValuePercent >= 0 ? '+' : '';
            if (diffValuePercent) {
              return `${sign}${Math.round(diffValuePercent)}%`;
            } else {
              return '';
            }
          }
          return '';
        }
        // if (typeof val === 'number' && series && series.length >= 2) {
        //   const seriesIndexMaxValue: number = series.reduce(
        //     (currentMaxIndex, currentSerie, currentIndex) => {
        //       if (currentMaxIndex === -1) {
        //         return currentIndex;
        //       }
        //       if (currentSerie.data && series[currentIndex].data) {
        //         if (
        //           currentSerie.data[opts.dataPointIndex] >
        //           series[currentMaxIndex].data[opts.dataPointIndex]
        //         ) {
        //           return currentIndex;
        //         }
        //       }

        //       return 0;
        //     },
        //     -1,
        //   );

        //   const isTheMaxValue = seriesIndexMaxValue === opts.seriesIndex;
        //   if (isTheMaxValue) {
        //     const refValue = series[0].data[opts.dataPointIndex];
        //     const currentValue = series[1].data[opts.dataPointIndex];
        //     if (
        //       refValue &&
        //       currentValue &&
        //       refValue !== 0 &&
        //       typeof refValue === 'number' &&
        //       typeof currentValue === 'number'
        //     ) {
        //       const diffValue = currentValue - refValue;

        //       const diffValuePercent = (diffValue * 100) / refValue;
        //       const sign = diffValuePercent >= 0 ? '+' : '';
        //       return `${sign}${diffValuePercent.toFixed(2)}%`;
        //     }
        //   }
        // }
        return '';
      },
      offsetY: -10,
      style: {
        fontSize: '12px',
        colors: ['#424242'],
      },
      background: {
        enabled: false,
      },
    },
    stroke: {
      width: [1, 1, 4],
    },
    title: {
      text: options?.title,
    },
    xaxis: {
      categories: options?.xaxis,
    },
    yaxis: {
      labels: {
        formatter: function (value: any) {
          return `${value}  ${options?.formatter || '€'}`;
        },
      },
    },
    colors: options.colorBar,
  };
  useEffect(() => {
    const innerApex: SVGSVGElement = document.querySelector('#apexSubtitled .apexcharts-inner') as any;
    setWitdh(innerApex?.getBBox().width);
  }, []);
  const subtitle = (
    <div className={classes.subtitle} style={{ width: witdh }}>
      <div>Autres Rém.</div>
      <div>BRI</div>
      <div>Total</div>
    </div>
  );
  return (
    <Box className={classes.root} id={withSubtitle ? 'apexSubtitled' : ''}>
      <Box className={classes.otherContainer}>
        <h2>{options.title}</h2>
        {otherComponent && <Box>{otherComponent}</Box>}
      </Box>
      <ReactApexChart options={optionProps as any} series={series} type="line" height={350} />
      {withSubtitle ? subtitle : ''}
    </Box>
  );
};
