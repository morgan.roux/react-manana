export interface MonthProps {
  date: Date;
  onRequestMonth: (date: Date) => void;
}

export interface WeekProps {
  date: Date;
  onRequestWeek: (date: Date) => void;
}

export interface DayProps {
  date: Date;
}

export interface intervalProps {
  startDate: Date;
  endDate: Date;
}

export interface BarProps {
  title: string;
  intervalle: intervalProps[];
  color: string;
}

export interface optionProps {
  title: string;
}

export interface BarLineProps {
  barLine: BarChartProps[];
  date: Date;
}

export interface BarChartProps {
  option: optionProps;
  bar: BarProps;
}
