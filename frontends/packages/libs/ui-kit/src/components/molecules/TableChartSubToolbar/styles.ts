import { createStyles, makeStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    subToolbarContainer: {
      display: 'flex',
      /* '& > *:nth-child(2)': {
        marginLeft: 51,
      },*/
    },
    calendarContainer: {
      display: 'flex',
      marginLeft: 5,
      alignItems: 'center',
      '& .MuiTypography-root': {
        fontFamily: 'Roboto',
        fontSize: 14,
      },
    },
    calendar: {
      '& .MuiOutlinedInput-notchedOutline': {
        border: 'none',
      },
      '& .MuiInputBase-input.MuiOutlinedInput-input.MuiInputBase-inputAdornedEnd.MuiOutlinedInput-inputAdornedEnd': {
        display: 'none',
      },
      [theme.breakpoints.down('sm')]: {
        marginBottom: '8px',
        '& .main-MuiInputBase-root': {
          height: '48px !important',
        },
      },
    },
    parameterCalandar: {
      display: 'flex',
      alignContent: 'center',
      marginLeft: '16px !important',
      [theme.breakpoints.down('xs')]: {
        marginLeft: '0px !important',
      },
    },
    parameterCalandarContainer: {
      display: 'flex',
      alignItems: 'center',
      [theme.breakpoints.down('sm')]: {
        display: 'flex',
        alignContent: 'center',
        marginTop: '10px',
      },
    },
    datePicker: {
      maxWidth: 190
    }
  })
);
