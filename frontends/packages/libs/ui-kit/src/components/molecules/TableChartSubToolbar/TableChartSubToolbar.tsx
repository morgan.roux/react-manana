import { Box, createGenerateClassName, Typography, StylesProvider, IconButton } from '@material-ui/core';
import moment from 'moment';
import React, { FC, useState } from 'react';
import NavigationButtons from '../../atoms/NavigationButtons';
import { useStyles } from './styles';
import { SearchInputBase } from '../../atoms/SearchInputBase';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import EventIcon from '@material-ui/icons/Event';
import { CustomDatePicker } from '../../atoms/CustomDateTimePicker/CustomDatePicker';
import { Cancel } from '@material-ui/icons';
const generateClassName = createGenerateClassName({
  //  disableGlobal: true,
  productionPrefix: 'datepicker',
  seed: 'datepicker',
});

interface TableChartSubToolbarProps {
  onRequestChange?: (searchText: string, date: any) => void;
  showMonth?: boolean;
  showDay?: boolean;
  setDisplayDate?: React.Dispatch<React.SetStateAction<Date>>;
  displayDate?: Date;
  displayDay?: Date;
  ToolBarBoxClassName?: string;
  withSearch?: boolean;
  searchClassName?: string;
  intervalDate?: boolean;
  handleDateDebutChange?: (date: any, value?: string | null | undefined) => void;
  handleDateFinChange?: (date: any, value?: string | null | undefined) => void;
  selectedDateDebut?: Date;
  selectedDateFin?: Date;
  handleTradeChange?: () => void;
  dateTrade?: boolean;
}

const TableChartSubToolbar: FC<TableChartSubToolbarProps> = ({
  onRequestChange,
  showMonth,
  showDay,
  setDisplayDate,
  displayDate,
  displayDay,
  ToolBarBoxClassName,
  withSearch = true,
  intervalDate,
  searchClassName,
  handleDateDebutChange,
  handleDateFinChange,
  selectedDateDebut,
  selectedDateFin,
  handleTradeChange,
  dateTrade,
}) => {
  const classes = useStyles();
  const [searchText, setSearchText] = useState<string>('');
  const [date, setDate] = useState<any>(moment.utc().format());
  const year = moment.utc(date).year();

  const [expandedMorePeriodeFilter, setExpandedMorePeriodeFilter] = useState<boolean>(true);

  const handleDateChange = (date: any) => {
    setDate(date);
    setDisplayDate && setDisplayDate(date);
    if (onRequestChange) {
      onRequestChange(searchText, date);
    }
  };

  const handleSearchTextChange = (value: string) => {
    setSearchText(value);
    if (onRequestChange) {
      onRequestChange(searchText, date);
    }
  };

  const handleLeftBtnClick = () => {
    const newDate = showDay
      ? moment.utc(date).subtract(1, 'd')
      : showMonth
      ? moment.utc(date).subtract(1, 'M')
      : moment.utc(date).subtract(1, 'y');
    setDate(newDate.toDate());
    setDisplayDate && setDisplayDate(moment.utc(displayDate).subtract(1, 'M').toDate());
    if (onRequestChange) {
      onRequestChange(searchText, newDate);
    }
  };

  const handleRightBtnClick = () => {
    const newDate = showDay
      ? moment.utc(date).add(1, 'd')
      : showMonth
      ? moment.utc(date).add(1, 'M')
      : moment.utc(date).add(1, 'y');
    setDate(newDate.toDate());
    setDisplayDate && setDisplayDate(moment.utc(displayDate).add(1, 'M').toDate());
    if (onRequestChange) {
      onRequestChange(searchText, newDate);
    }
  };

  const theme = useTheme();
  const match = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <>
      {match ? (
        withSearch ? (
          <Box className={`${classes.subToolbarContainer} ${ToolBarBoxClassName || ''}`}>
            <SearchInputBase
              value={searchText}
              outlined
              placeholder="Rechercher"
              onChange={handleSearchTextChange}
              fullWidth={false}
              className={searchClassName}
            />
          </Box>
        ) : null
      ) : (
        <Box className={`${classes.subToolbarContainer} ${ToolBarBoxClassName || ''}`}>
          {withSearch && (
            <SearchInputBase
              value={searchText}
              outlined
              placeholder="Rechercher"
              onChange={handleSearchTextChange}
              fullWidth={false}
              className={searchClassName}
            />
          )}
          <div className={classes.parameterCalandar}>
            <NavigationButtons
              onLeftBtnClick={handleLeftBtnClick}
              onRightBtnClick={handleRightBtnClick}
              //   className={classes.btnChevron}
            />
            <Box className={classes.calendarContainer}>
              <div style={{ marginLeft: 10 }} className={classes.parameterCalandarContainer}>
                {intervalDate ? (
                  ''
                ) : (
                  <Box>
                    <EventIcon />
                  </Box>
                )}
                {/* <Box>{expandedMorePeriodeFilter && <CustomDatePicker />}</Box> */}
                <Typography style={{ marginLeft: 10 }}>
                  {intervalDate ? (
                    <Box>
                      <Box onClick={handleTradeChange}>
                        {!dateTrade && (
                          <Box style={{ display: 'flex' }}>
                            <EventIcon style={{ cursor: 'pointer' }} />
                            <Typography
                              style={{ marginLeft: 8 }}
                              onClick={() => setExpandedMorePeriodeFilter(!expandedMorePeriodeFilter)}
                            >
                              {`${moment.utc(selectedDateDebut).format('DD/MM/YYYY')}`} -{' '}
                              {`${moment.utc(selectedDateFin).format('DD/MM/YYYY')}`}
                            </Typography>
                          </Box>
                        )}
                      </Box>
                      {dateTrade && (
                        <StylesProvider generateClassName={generateClassName}>
                          <Box style={{ display: 'flex' }}>
                            <Box>
                              <CustomDatePicker
                                label={`Du`}
                                value={selectedDateDebut}
                                InputLabelProps={{
                                  shrink: true,
                                }}
                                onChange={(value) => {
                                  handleDateDebutChange && handleDateDebutChange(value);
                                }}
                                fullWidth
                                required
                                className={classes.datePicker}
                              />
                            </Box>
                            <Box style={{ marginLeft: 8 }}>
                              <CustomDatePicker
                                label={`Au`}
                                value={selectedDateFin}
                                InputLabelProps={{
                                  shrink: true,
                                }}
                                onChange={handleDateFinChange || (() => {})}
                                fullWidth
                                required
                                className={classes.datePicker}
                              />
                            </Box>
                            <IconButton onClick={handleTradeChange}>
                              <Cancel />
                            </IconButton>
                          </Box>
                        </StylesProvider>
                      )}
                    </Box>
                  ) : showDay ? (
                    `${moment.utc(displayDay).format('dddd')}`
                  ) : showMonth ? (
                    `${moment.utc(displayDate).format('MMMM')} ${moment.utc(displayDate).format('yyyy')}`
                  ) : (
                    year
                  )}
                </Typography>
              </div>
            </Box>
          </div>
        </Box>
      )}
    </>
  );
};

export default TableChartSubToolbar;
