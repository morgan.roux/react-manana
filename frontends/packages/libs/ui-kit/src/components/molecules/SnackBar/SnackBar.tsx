import React, { FC } from 'react';
import classnames from 'classnames';
import CheckCircleIcon from '@material-ui/icons/CheckCircleOutlineRounded';
import ErrorIcon from '@material-ui/icons/ErrorOutlineRounded';
import InfoIcon from '@material-ui/icons/InfoRounded';
import CloseIcon from '@material-ui/icons/CloseRounded';
import { amber, green, red, blue } from '@material-ui/core/colors';
import WarningIcon from '@material-ui/icons/Warning';
import {
  makeStyles,
  Theme,
  IconButton,
  Snackbar as MuiSnackbar,
  SnackbarContent as MuiSnackbarContent,
} from '@material-ui/core';

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

const useStyles = makeStyles((theme: Theme) => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: red[600],
  },
  info: {
    backgroundColor: blue[600],
  },
  warning: {
    backgroundColor: amber[700],
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1),
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
}));

export interface SnackbarContentWrapperProps {
  className?: string;
  message?: string;
  onClose?: () => void;
  variant: keyof typeof variantIcon;
}

const SnackbarContentWrapper: FC<SnackbarContentWrapperProps> = (props) => {
  const classes = useStyles({});
  const { className, message, onClose, variant, ...other } = props;
  const Icon = variantIcon[variant];

  return (
    <MuiSnackbarContent
      className={classnames(classes[variant], className)}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          <Icon className={classnames(classes.icon, classes.iconVariant)} />
          {message}
        </span>
      }
      action={[
        <IconButton key="close" aria-label="Close" color="inherit" onClick={onClose} size="small">
          <CloseIcon className={classes.icon} />
        </IconButton>,
      ]}
      {...other}
    />
  );
};

interface SnackBarProps {
  open?: boolean;
  type?: 'success' | 'warning' | 'error' | 'info';
  message: string;
  onClose: () => void;
}

const SnackBar: FC<SnackBarProps> = ({ open, message, onClose, type = 'info' }) => {
  return (
    <MuiSnackbar
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={open}
      autoHideDuration={5000}
      onClose={onClose}
    >
      <SnackbarContentWrapper onClose={onClose} variant={type} message={message} />
    </MuiSnackbar>
  );
};

export default SnackBar;
