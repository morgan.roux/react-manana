import { Box } from '@material-ui/core';
import React, { FC, useState } from 'react';
interface SwipeableContainerProps {
  onNext: () => void;
  onPrevious: () => void;
  children?: any;
}

const SwipeableContainer: FC<SwipeableContainerProps> = ({ onNext, onPrevious, children }) => {
  const [touchStart, setTouchStart] = useState(0);
  const [touchEnd, setTouchEnd] = useState(0);

  const handleTouchStart = (e: any) => {
    setTouchStart(e.targetTouches[0].clientX);
  };

  const handleTouchMove = (e: any) => {
    setTouchEnd(e.targetTouches[0].clientX);
  };

  const handleTouchEnd = () => {
    if (touchStart - touchEnd > 150) {
      onNext();
    }

    if (touchStart - touchEnd < -150) {
      onPrevious();
    }
  };

  return (
    <Box onTouchStart={handleTouchStart} onTouchMove={handleTouchMove} onTouchEnd={handleTouchEnd}>
      {children}
    </Box>
  );
};

export default SwipeableContainer;
