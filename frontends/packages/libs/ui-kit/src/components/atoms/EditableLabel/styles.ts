import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },

  textfieldRoot: {
    '& .MuiInputBase-input': {
      height: 0,
    },
  },
}));

export default useStyles;
