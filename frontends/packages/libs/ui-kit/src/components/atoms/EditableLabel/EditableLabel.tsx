import React, { FC, useEffect, useState } from 'react';
import { CustomFormTextField } from '../CustomTextField';
import useStyles from './styles';

interface EditableLabelProps {
  onRequestSaveInput?: (value: string) => void;
  value?: string;
  labelElement?: any;
  loading?: boolean;
  editionDisabled?: boolean;
  labelOnly?: boolean;
  data?: any;
  onClick?: () => void;
}

const EditableLabel: FC<EditableLabelProps> = ({
  onRequestSaveInput,
  value,
  labelElement,
  editionDisabled,
  loading,
  labelOnly,
  onClick,
}) => {
  const classes = useStyles();

  const [mode, setMode] = useState<'label' | 'input'>('label');
  const [label, setLabel] = useState<string>(value || '');

  const handleRequestSave = () => {
    if (onRequestSaveInput) {
      onRequestSaveInput(label);
    }
  };

  const handleEnterKeyPress = (event: any) => {
    if (event.key === 'Enter' && onRequestSaveInput) {
      onRequestSaveInput(label);
    }
  };

  const handleChange = (e: any) => {
    const { value } = e.target;
    setLabel(value);
  };

  useEffect(() => {
    if (value) {
      setLabel(value);
      setMode('label');
    }
  }, [value]);

  const handleShowModal = () => {
    onClick && onClick();
  };

  return (
    <div className={classes.root}>
      {loading && <>Loading</>}
      {!loading &&
        (mode === 'label' || editionDisabled ? (
          <div onClick={handleShowModal}>{labelElement || label}</div>
        ) : (
          <>
            {labelOnly && mode === 'input' && (
              <CustomFormTextField
                className={classes.textfieldRoot}
                value={label}
                onKeyPress={handleEnterKeyPress}
                onBlur={handleRequestSave}
                onChange={handleChange}
              />
            )}
          </>
        ))}
      {/* // <CustomModal
      //   open={open}
      //   setOpen={setOpen || ((_open: boolean) => {})}
      //   closeIcon
      //   headerWithBgColor
      //   withBtnsActions={false}
      //   title="Details rémuneration"
      // >
      //   <Typography>{data}</Typography>
      // </CustomModal> */}
    </div>
  );
};

export default EditableLabel;
