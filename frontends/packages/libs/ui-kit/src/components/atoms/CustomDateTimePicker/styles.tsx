import { makeStyles, Theme, createStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customDatePickerRoot: {
      '& .MuiIconButton-root': {
        color: theme.palette.common.black,
      },
      '&:hover .MuiOutlinedInput-notchedOutline': {
        borderColor: theme.palette.primary.main,
      },
      '&:hover .MuiFormLabel-root': {
        color: theme.palette.primary.main,
      },
      '& .MuiFormLabel-asterisk': {
        color: 'red',
      },
      '& input': {
        padding: 14.5,
      },
    },
  })
);

export default useStyles;
