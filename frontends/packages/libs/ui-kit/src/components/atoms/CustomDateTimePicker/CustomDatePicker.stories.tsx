import React from 'react';
import { CustomDatePicker } from './CustomDatePicker';

export default {
  title: 'Design system|Atoms/CustomDatePicker',
  parameters: {
    info: { inline: true },
  },
};

export const CustomDatePickerStory = () => <CustomDatePicker value="" onChange={(e) => e} />;
