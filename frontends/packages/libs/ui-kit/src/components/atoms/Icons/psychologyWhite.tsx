import React from 'react';
import { SvgIcon } from '@material-ui/core';
import { SvgIconProps } from '@material-ui/core/SvgIcon';
interface CustomProps {
  color?: string;
}
const PsychologyWhite: React.FC<SvgIconProps & CustomProps> = props => {
  const { color } = props;
  return (
    <SvgIcon {...props} width="49" height="49" viewBox="0 0 49 49">
      <g id="Groupe_14306" data-name="Groupe 14306">
        <rect id="Rectangle_19551" data-name="Rectangle 19551" width="49" height="49" fill="none" />
      </g>
      <g id="Groupe_14308" data-name="Groupe 14308" transform="translate(8.163 6.125)">
        <g id="Groupe_14307" data-name="Groupe 14307">
          <path
            id="Tracé_5535"
            data-name="Tracé 5535"
            d="M14.49,8.57a2.92,2.92,0,1,0,2.92,2.92A2.919,2.919,0,0,0,14.49,8.57Z"
            transform="translate(3.889 2.802)"
            fill={color ? color : '#fff'}
          />
          <path
            id="Tracé_5536"
            data-name="Tracé 5536"
            d="M22.377,3A14.253,14.253,0,0,0,8.126,16.557l-3.92,5.227a1.02,1.02,0,0,0,.817,1.633H8.085v6.125a4.1,4.1,0,0,0,4.083,4.083H14.21V39.75H28.5V30.195A14.286,14.286,0,0,0,22.377,3ZM28.5,17.292c0,.265-.02.531-.041.8l1.695,1.347a.383.383,0,0,1,.1.51l-1.633,2.838a.41.41,0,0,1-.49.184l-2.021-.817a6.135,6.135,0,0,1-1.368.8l-.327,2.164a.4.4,0,0,1-.408.347H20.744a.415.415,0,0,1-.408-.347l-.306-2.164a5.84,5.84,0,0,1-1.388-.8l-2.021.817a.428.428,0,0,1-.51-.184l-1.633-2.838a.387.387,0,0,1,.1-.51l1.715-1.347c-.02-.265-.041-.531-.041-.8a5.008,5.008,0,0,1,.082-.8L14.6,15.148a.4.4,0,0,1-.1-.531L16.129,11.8a.4.4,0,0,1,.49-.184l2.042.817a6.482,6.482,0,0,1,1.368-.8l.306-2.164a.415.415,0,0,1,.408-.347H24.01a.415.415,0,0,1,.408.347l.306,2.164a6.135,6.135,0,0,1,1.368.8l2.042-.817a.394.394,0,0,1,.49.184l1.633,2.817a.409.409,0,0,1-.1.531L28.42,16.5A3.289,3.289,0,0,1,28.5,17.292Z"
            transform="translate(-3.998 -3)"
            fill={color ? color : '#fff'}
          />
        </g>
      </g>
    </SvgIcon>
  );
};

export default PsychologyWhite;
