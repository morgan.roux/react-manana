import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    inputPassword: {
      '& input': {
        // color: theme.palette.common.white,
        padding: 14.5,
      },
      // '& label': {
      //   color: theme.palette.common.white,
      // },
      '& label.Mui-focused': {
        color: theme.palette.primary.main,
      },
      '& input:invalid + fieldset': {},
      '& .Mui-focused .MuiIconButton-label svg': {
        color: `${theme.palette.primary.main} !important`,
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: theme.palette.common.white,
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          padding: 14.5,
          borderColor: theme.palette.common.white,
        },
        '&:hover fieldset': {
          borderColor: theme.palette.primary.main,
        },
        '&.Mui-focused fieldset': {
          borderColor: theme.palette.primary.main,
        },
        '&:hover .MuiOutlinedInput-notchedOutline': {
          borderColor: theme.palette.primary.main,
        },
      },
    },
  })
);

export default useStyles;
