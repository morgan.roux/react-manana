import React from 'react';

import CustomFormTextField from './CustomFormTexField';

export default {
  title: 'Design system|Atoms/CustomFormTextField',
  parameters: {
    info: { inline: true },
  },
};

export const CustomFormFieldTextStory = () => {
  return <CustomFormTextField type="text" label="Text" placeholder="Placeholder"></CustomFormTextField>;
};

export const CustomFormFieldDateStory = () => {
  return (
    <CustomFormTextField
      type="datetime-local"
      label="Date et Heure"
      placeholder="00/00/00, OO:OO AM"
    ></CustomFormTextField>
  );
};

export const CustomFormFieldNumberStory = () => {
  return <CustomFormTextField type="number" placeholder="00" label="Numero"></CustomFormTextField>;
};

export const CustomFormFieldTextareaStory = () => {
  return (
    <CustomFormTextField
      label="Content"
      multiline
      rows={2}
      rowsMax={4}
      placeholder="Textarea field"
    ></CustomFormTextField>
  );
};
