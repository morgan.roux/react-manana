import React, { FC } from 'react';
import { TextField } from '@material-ui/core';
import { OutlinedTextFieldProps } from '@material-ui/core/TextField';
import classnames from 'classnames';
import useStyles from './styles';

const CustomTextField: FC<OutlinedTextFieldProps> = (props) => {
  const { name, label, variant, fullWidth, className, ...inputProps } = props;
  const styles = useStyles();
  return (
    <TextField
      {...props}
      color="secondary"
      InputLabelProps={{
        shrink: true,
      }}
      {...inputProps}
      className={
        className
          ? classnames(styles.inputPassword, props.className, className)
          : classnames(styles.inputPassword, className)
      }
    />
  );
};

export default CustomTextField;
