import React, { FC } from 'react';
import { makeStyles, Theme, createStyles, TextFieldProps, TextField } from '@material-ui/core';
import classnames from 'classnames';

interface CustomFormTextFieldProps {
  shrink?: boolean;
  className?: string;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    inputText: {
      '& .MuiInputBase-root': {
        height: '48px !important',
      },
      '& input': {
        color: theme.palette.common.black,
        padding: '14.5px !important',
      },
      '& label': {
        '& .MuiFormLabel-asterisk': {
          color: 'red',
        },
      },
      '& label.Mui-focused, &:hover label': {
        color: theme.palette.primary.main,
      },
      '& input:invalid + fieldset': {},
      '& .Mui-focused .MuiIconButton-label svg': {
        color: `${theme.palette.primary.main} !important`,
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: theme.palette.common.white,
      },
      '& .MuiOutlinedInput-root': {
        '&:hover fieldset': {},
        '&.Mui-focused fieldset': {
          borderColor: theme.palette.primary.main,
        },
      },
      '& .main-MuiInputBase-root, & .dialog-MuiInputBase-root, & .MuiInputBase-root': {
        padding: '0 !important',
      },
      // '& .main-MuiOutlinedInput-root , & .MuiOutlinedInput-root': {
      //   padding: '0 !important',
      // },
      marginBottom: 12,
    },
  })
);

const CustomFormTextField: FC<TextFieldProps & CustomFormTextFieldProps> = ({
  shrink,
  inputRef,
  className,
  InputProps,
  inputProps,
  type,
  ...props
}) => {
  const classes = useStyles();

  return (
    <TextField
      {...props}
      InputLabelProps={{
        shrink,
      }}
      inputProps={inputProps}
      className={className ? classnames(classes.inputText, className) : classes.inputText}
      InputProps={InputProps}
      type={type}
      inputRef={inputRef}
    />
  );
};

CustomFormTextField.defaultProps = {
  label: '',
  name: undefined,
  color: 'primary',
  variant: 'outlined',
  fullWidth: true,
  inputProps: {},
  shrink: true,
};

export default CustomFormTextField;
