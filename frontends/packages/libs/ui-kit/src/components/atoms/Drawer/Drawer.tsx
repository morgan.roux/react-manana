import { CssBaseline, Drawer, Hidden, useTheme } from '@material-ui/core';
import React, { FC, ReactNode } from 'react';
import MobileTopBar from '../MobileTopBar';

import useStyles from './styles';
interface DrawerProps {
  mobileOpen?: boolean;
  children: ReactNode;
  handleDrawerToggle?: () => void;
  mobileTobBarTitle?: string;
  noTopBar?: boolean;
  width?: number;
  lgUpClassName?: string;
  mdDownClassName?: string;
}

const DrawerComponent: FC<DrawerProps> = ({
  children,
  mobileOpen,
  handleDrawerToggle,
  mobileTobBarTitle,
  noTopBar,
  width,
  lgUpClassName,
  mdDownClassName,
}) => {
  const classes = useStyles(width)();
  const container = window !== undefined ? () => window.document.body : undefined;
  const theme = useTheme();
  return (
    <>
      <Hidden lgUp implementation="css">
        <Drawer
          className={lgUpClassName || ''}
          container={container}
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <CssBaseline />
          {!noTopBar && (
            <Hidden mdUp implementation="css">
              <MobileTopBar
                title={mobileTobBarTitle}
                withBackBtn={false}
                withCloseBtn
                handleDrawerToggle={handleDrawerToggle}
                fullScreen
                onClose={handleDrawerToggle}
              />
            </Hidden>
          )}
          {children}
        </Drawer>
      </Hidden>
      <Hidden mdDown implementation="css">
        <div className={mdDownClassName || ''}>{children}</div>
      </Hidden>
    </>
  );
};

export default DrawerComponent;
