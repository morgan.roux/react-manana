import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';
const useStyles = (width?: number) =>
  makeStyles((theme: Theme) =>
    createStyles({
      drawerPaper: {
        width: width || 336,
        top: 86,
        boxShadow: 'none!important',
        [theme.breakpoints.down('sm')]: {
          width: '100%',
          top: 0,
          height: '100%',
        },
      },
    })
  );
export default useStyles;
