import React, { FC } from 'react';
import useStyles from './styles';
import { Avatar, AvatarProps } from '@material-ui/core';
import { stringToAvatar } from '@lib/common';
interface CustomAvatarProps {
  name?: string | null;
  url?: string | null;
  width?: string | undefined;
  className?: string;
}

const CustomAvatar: FC<CustomAvatarProps & Partial<AvatarProps>> = ({ name, url, width, className, ...rest }) => {
  const classes = useStyles({});
  return url ? (
    <Avatar {...rest} src={url} className={`${classes.avatar} ${className || ''}`} />
  ) : (
    <Avatar className={`${classes.avatar} ${className || ''}`} sizes={width} {...rest}>
      {name && stringToAvatar(name)}
    </Avatar>
  );
};

export default CustomAvatar;
