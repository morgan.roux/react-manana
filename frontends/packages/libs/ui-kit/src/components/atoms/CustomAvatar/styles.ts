import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatar: {
      maxWidth: 51,
      maxHeight: 51,
      borderRadius: '50%',
      border: '1px solid #e0e0e0',
      fontSize: '12px !important',
    },
  })
);
export default useStyles;
