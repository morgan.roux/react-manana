import React, { FC, CSSProperties } from 'react';
import { Alert } from '@material-ui/lab';
import { createStyles, makeStyles } from '@material-ui/styles';
interface CustomAlertDonneesMedicalesProps {
  message?: string;
  style?: CSSProperties;
}
const useStyles = makeStyles(() =>
  createStyles({
    root: { backgroundColor: 'rgb(255, 244, 229) !important' },
  })
);
const CustomAlertDonneesMedicales: FC<CustomAlertDonneesMedicalesProps> = ({
  style,
  message = 'Veuilliez à ne pas échanger des données médicales des patients via ce canal de communication!',
}) => {
  const classes = useStyles();
  return (
    <Alert style={style} className={classes.root} severity="warning">
      {message}
    </Alert>
  );
};
// à ne pas échanger des données médicales des patients
export default CustomAlertDonneesMedicales;
