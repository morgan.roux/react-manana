import React, { FC } from 'react';
import { useStyles } from './style';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';
import FieldsGroup, { FieldGroupValue } from './../FieldsGroup/FieldsGroup';
import { List, ListItem, Button, IconButton, ListItemSecondaryAction, TextField, Box } from '@material-ui/core';

export interface SectionFieldsGroupValue {
  name: string;
  ordre: number;
  value: string; // section field value
  fieldValues: FieldGroupValue[];
}

export interface MultiSectionsFieldsGroupProps {
  addFieldLabel: string;
  fieldPlaceholder: string;
  addSectionLabel: string;
  sectionPlaceholder: string;
  values: SectionFieldsGroupValue[];
  onChange: (newValues: SectionFieldsGroupValue[]) => void;
  inputClassName?: string;
}

const MultiSectionsFieldsGroup: FC<MultiSectionsFieldsGroupProps> = ({
  onChange,
  addFieldLabel,
  addSectionLabel,
  values,
  fieldPlaceholder,
  sectionPlaceholder,
  inputClassName,
}) => {
  const classes = useStyles();

  const reorder = (valuesToReorder: SectionFieldsGroupValue[]) => {
    return valuesToReorder.map((element, index) => ({ ...element, order: index + 1 }));
  };

  const handleChangeFieldValuesAt = (index: number, fieldValues: FieldGroupValue[]) => {
    values[index].fieldValues = fieldValues;
    onChange(reorder(values));
  };

  const handleChangeValueAt = (index: number, value: string) => {
    values[index].value = value;
    onChange(reorder(values));
  };

  const handleRemoveAt = (index: number) => {
    onChange(reorder(values.filter((_value, currentIndex) => currentIndex === index)));
  };

  const handleAdd = () => {
    values.push({
      name: '',
      ordre: values.length,
      value: '',
      fieldValues: [],
    });

    onChange(reorder(values));
  };

  return (
    <Box>
      <List disablePadding>
        {values.map(({ name, fieldValues, value }, index) => {
          return (
            <ListItem key={name || index} disableGutters dense>
              <Box className={classes.section}>
                <TextField
                  variant="outlined"
                  value={value}
                  placeholder={sectionPlaceholder}
                  name={name || `field-${index}`}
                  onChange={(event) => handleChangeValueAt(index, event.target.value)}
                  fullWidth
                />

                <FieldsGroup
                  placeholder={fieldPlaceholder}
                  onChange={handleChangeFieldValuesAt.bind(null, index)}
                  addFieldLabel={addFieldLabel}
                  values={fieldValues}
                  style={{ paddingLeft: 20 }}
                  className={inputClassName}
                />
              </Box>
              <ListItemSecondaryAction style={{ top: 30 }}>
                <IconButton size="small" aria-label="delete" onClick={() => handleRemoveAt(index)}>
                  <CloseIcon />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          );
        })}

        <Button
          onClick={() => handleAdd()}
          startIcon={<AddIcon />}
          color="secondary"
          style={{ textTransform: 'initial' }}
        >
          {addSectionLabel}
        </Button>
      </List>
    </Box>
  );
};

export default MultiSectionsFieldsGroup;
