import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    root: {},
    section: {
      width: '100%',
    },
    addButton: {
      textAlign: 'left',
      borderRadius: 5,
    },
  })
);
