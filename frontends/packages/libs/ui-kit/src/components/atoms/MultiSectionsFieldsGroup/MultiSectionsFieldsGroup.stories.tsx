import React, { useState } from 'react';
import MultiSectionsFieldsGroup, { SectionFieldsGroupValue } from './MultiSectionsFieldsGroup';

export default {
  title: 'Design system|Atoms/MultiSectionsFieldsGroup',
  parameters: {
    info: { inline: true },
  },
};

export const fieldGroup = () => {
  const [values, setValues] = useState<SectionFieldsGroupValue[]>([]);

  return <MultiSectionsFieldsGroup fieldPlaceholder="Votre text" sectionPlaceholder="Titre"  addSectionLabel="Add section" addFieldLabel="Add checklist" values={values} onChange={setValues} />;
};
