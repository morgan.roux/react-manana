import React, { FC } from 'react';
import { Card, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@material-ui/core';
import useStyles from './styles';
import classnames from 'classnames';
import { CSSProperties } from 'react';

interface DataProps {
  lastYear?: any;
  currentYear?: any;
  currentYearMontantPrevu?: any;
  lastYearNombres1?: any;
  currentYearNombres1?: any;
  lastYearNombres2?: any;
  currentYearNombres2?: any;
}
interface CustomCardProps {
  data: DataProps;
  label1?: string;
  label2?: string;
  title?: string;
  bgColor?: string;
  bgImages?: any;
  backgroundFilter?: string;
  backgroundPosition?: string;
  style?: CSSProperties;
}

export const CustomCard: FC<CustomCardProps> = ({ data, label1, label2, bgColor, title, style, backgroundFilter }) => {
  const classes = useStyles();
  const { lastYear, currentYear, lastYearNombres1, currentYearNombres1, lastYearNombres2, currentYearNombres2 } = data;
  return (
    <div className={classes.divRoot}>
      <Card
        className={classnames(classes.cardRoot)}
        style={{
          background: bgColor,
        }}
      >
        <div style={style} className={classes.makeHeight}>
          <div
            style={{
              padding: 10,
              background: `${backgroundFilter}`,
            }}
            className={classes.makeHeight}
          >
            <div className={classes.title}>
              {/* <Typography variant="h4" component="div" className={classes.titleText}>
                {currentYearMontantPrevu}
          </Typography>*/}
              <div className={classes.duree}>
                <Typography variant="h6" component="div" className={classes.textDuree}>
                  {title}
                </Typography>
              </div>
            </div>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell className={classes.tableCell} />
                  <TableCell className={classes.tableCellTitle} align="right">
                    {lastYear}
                  </TableCell>
                  <TableCell className={classes.tableCellTitle} align="right">
                    {currentYear}
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell component="th" scope="row" className={classes.tableCellTitle}>
                    {label1}
                  </TableCell>
                  <TableCell className={classes.tableCell} align="right">
                    {lastYearNombres1}
                  </TableCell>
                  <TableCell className={classes.tableCell} align="right">
                    {currentYearNombres1}
                  </TableCell>
                </TableRow>

                <TableRow>
                  <TableCell className={classes.tableCellTitle} component="th" scope="row">
                    {label2}
                  </TableCell>
                  <TableCell className={classes.tableCell} align="right">
                    {lastYearNombres2}
                  </TableCell>
                  <TableCell className={classes.tableCell} align="right">
                    {currentYearNombres2}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </div>
        </div>
      </Card>
    </div>
  );
};
