import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    divRoot: {
      display: 'flex',
      justifyContent: 'space-between',
      minWidth: '24%',
      height: '100%',
      borderRadius: theme.spacing(0.5),
      [theme.breakpoints.down('md')]: {
        display: 'flex',
        justifuContent: 'center',
        width: theme.spacing(40),
      },
    },
    cardRoot: {
      background: 'none',
      minWidth: 320,
      '& .main-MuiCard-root': {
        maxWidth: '250px',
        padding: '10px',
      },
      '& .main.MuiPaper-root': {
        padding: '10px',
        width: '100%',
      },
      [theme.breakpoints.down('md')]: {
        width: '100%',
        borderRadius: 25,
      },
    },
    title: {
      display: 'flex',
      width: '100%',
    },
    makeHeight: {
      height: '100%',
    },
    titleText: {
      fontSize: 30,
      marginTop: theme.spacing(0.5),
      fontWeight: '600 !important' as any,
      fontFamily: 'Roboto',
      color: '#fff',
    },
    duree: {
      display: 'flex',
      alignItems: 'center',
    },
    textDuree: {
      fontSize: '16px',
      fontWeight: 400,
      fontFamily: 'Roboto',
      marginLeft: '8px',
      marginTop: '4px',
      color: '#fff',
    },
    tableCell: {
      padding: 0,
      color: '#fff',
      border: 'none',
      fontSize: 16,
      fontWeight: 600,
    },
    tableCellTitle: {
      padding: '0 10px',
      color: '#fff',
      border: 'none',
      opacity: 0.8,
      fontSize: 16,
      fontFamily: 'Roboto',
      fontWeight: 400,
      marginLeft: 8,
    },
    partenaireItems: {
      display: 'flex',
      alignItems: 'center',
    },
    titlePartenaire: {
      fontSize: '20px',
      marginTop: theme.spacing(0.5),
      fontWeight: '600 !important' as any,
      fontFamily: 'Roboto',
      color: '#fff',
    },
    currentYear: {},
  })
);

export default useStyles;
