import { Box, Card, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import useStyle from './styles';
import classnames from 'classnames';

interface DataProps {
  paye: string;
  prevue: string;
  realise: string;
  titre: string;
}

interface CardPartenaireProps {
  data: DataProps[];
  currentYear?: any;
  titre?: string;
  bgColor?: string;
  bgImages?: any;
  backgroundFilter?: string;
  backgroundPosition?: string;
  backgroundPositionY?: string;
}

export const CardPartenaire: FC<CardPartenaireProps> = ({
  data,
  bgColor,
  bgImages,
  backgroundPosition,
  backgroundPositionY,
  currentYear,
  titre,
}) => {
  // const { differanceOffre, differaneAvant, differanceChalleng, currentYear } = data;
  const classes = useStyle();
  return (
    <div className={classes.divRoot}>
      <Card
        className={classnames(classes.cardRoot)}
        style={{
          background: bgColor,
        }}
      >
        <div
          style={{
            backgroundImage: `url(${bgImages})`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: '84% auto',
            backgroundPosition: `${backgroundPosition}`,
            backgroundPositionY: `${backgroundPositionY}`,
            padding: 10,
          }}
          className={classes.makeHeight}
        >
          <div className={classes.title}>
            <div className={classes.duree}>
              <Typography variant="h6" component="div" className={classes.textDuree}>
                {titre} <span className={classes.currentYear}>{currentYear}</span>
              </Typography>
            </div>
          </div>
          <Table aria-label="siple-table">
            <TableHead>
              <TableRow>
                <TableCell className={classes.tableCell} />
                <TableCell className={classes.tableCell} />
                <TableCell align="right" className={classes.tableCellTitle}>
                  Réalisée
                </TableCell>
                <TableCell align="right" className={classes.tableCellTitle}>
                  Payée
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {(data || []).map((element: DataProps) => {
                return (
                  <TableRow>
                    <TableCell component="th" scope="row" className={classes.tableCellTitle}>
                      <Box className={classes.partenaireItems}>
                        <Box className={classes.textDuree}>{element.titre}</Box>
                      </Box>
                    </TableCell>
                    <TableCell className={classes.tableCell} />
                    <TableCell align="right" component="th" scope="row" className={classes.tableCellTitle}>
                      <Typography style={{ fontSize: '16px', fontWeight: 600, color: '#FFFFFF' }}>
                        {element.realise}
                      </Typography>
                    </TableCell>
                    <TableCell align="right" component="th" scope="row" className={classes.tableCellTitle}>
                      <Typography style={{ fontSize: '16px', fontWeight: 600, color: '#FFFFFF' }}>
                        {element.paye}
                      </Typography>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </div>
      </Card>
    </div>
  );
};
