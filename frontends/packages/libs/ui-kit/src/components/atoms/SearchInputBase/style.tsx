import { createStyles, Theme, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    whiteBackground: {
      height: 48,
      display: 'flex',
      alignItems: 'center',
      borderRadius: 4,
      borderColor: theme.palette.common.white,
      borderWidth: 1,
      borderStyle: 'solid',
      background: theme.palette.common.white,
      '&.main-makeStyles-outlined': {
        height: '41px !important',
      },
    },
    darkBackground: {
      height: 40,
      display: 'flex',
      alignItems: 'center',
      borderRadius: 4,
      borderColor: theme.palette.common.black,
      borderWidth: 1,
      borderStyle: 'solid',
      background: theme.palette.common.white,
    },
    fullWidth: {
      // width: '40vw',
    },
    outlined: {
      height: 48,
      display: 'flex',
      alignItems: 'center',
      borderRadius: 4,
      borderColor: theme.palette.common.black,
      borderWidth: 1,
      borderStyle: 'solid',
      background: theme.palette.common.white,
    },
    input: {
      marginLeft: 8,
      flex: 1,
      width: '100%',
    },
    iconButton: {
      padding: 10,
      color: theme.palette.text.primary,
    },
    divider: {
      width: 1,
      height: 28,
      margin: 4,
    },
    peopleIcon: {
      margin: '0 12px',
    },
  })
);
