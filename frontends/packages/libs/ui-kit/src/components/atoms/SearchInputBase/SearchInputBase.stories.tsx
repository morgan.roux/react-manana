import React, { useState } from 'react';
import SearchInputBase from './SearchInputBase';

export default {
  title: 'Design system|Atoms/SearchInputBase',
  parameters: {
    info: { inline: true },
  },
};

export const fullCustomPlaceholder = () => {
  const [value, setValue] = useState<string>('');

  return (
    <SearchInputBase
      value={value}
      placeholder="Custom placeholder"
      fullWidth={true}
      onChange={setValue}
    />
  );
};

export const fullWith = () => {
  const [value, setValue] = useState<string>('');
  return <SearchInputBase value={value} fullWidth={true} onChange={setValue} />;
};

export const dark = () => {
  const [value, setValue] = useState<string>('');

  return <SearchInputBase value={value} dark={true} onChange={setValue} />;
};

export const white = () => {
  const [value, setValue] = useState<string>('');

  return <SearchInputBase value={value} dark={false} onChange={setValue} />;
};
