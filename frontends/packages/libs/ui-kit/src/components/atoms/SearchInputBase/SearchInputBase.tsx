import React, { FC } from 'react';
import { useStyles } from './style';
import { InputBase, IconButton, Tooltip, Fade, Box } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import classnames from 'classnames';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
export interface SeachInputBaseProps {
  makeStylesWhiteBackground?: boolean;
  value: string;
  placeholder?: string;
  disabled?: boolean;
  fullWidth?: boolean;
  minWidth?: number;
  outlined?: boolean;
  dark?: boolean; // WHY ?:
  onChange: (value: string) => void;
  className?: string;
  searchIconFirst?: boolean;
}

const SearchInputBase: FC<SeachInputBaseProps> = ({
  onChange,
  value,
  placeholder,
  disabled,
  fullWidth,
  outlined,
  minWidth,
  dark,
  className,
  searchIconFirst,
}) => {
  const classes = useStyles();
  console.log('---------------searchIconFirst---------', searchIconFirst);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <Tooltip
      TransitionComponent={Fade}
      TransitionProps={{ timeout: 600 }}
      title="Veuillez saisir votre critère de recherche"
    >
      {searchIconFirst ? (
        <>
          <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
            <SearchIcon />
          </IconButton>
          <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
            <MenuItem>
              <Box
                style={{ minWidth }}
                className={classnames(
                  outlined ? classes.outlined : '',
                  fullWidth ? classes.fullWidth : '',
                  dark ? classes.darkBackground : classes.whiteBackground,
                  className
                )}
              >
                <InputBase
                  placeholder={placeholder || 'Que recherchez-vous?'}
                  value={value}
                  autoComplete="off"
                  inputProps={{
                    'aria-label': 'search',
                  }}
                  onChange={(e) => onChange(e.target.value)}
                  className={classes.input}
                  type="search"
                  disabled={disabled}
                />
                <IconButton className={classes.iconButton} aria-label="search">
                  <SearchIcon />
                </IconButton>
              </Box>
            </MenuItem>
          </Menu>
        </>
      ) : (
        <div
          style={{ minWidth }}
          className={classnames(
            outlined ? classes.outlined : '',
            fullWidth ? classes.fullWidth : '',
            dark ? classes.darkBackground : classes.whiteBackground,
            className
          )}
        >
          <InputBase
            placeholder={placeholder || 'Que recherchez-vous?'}
            value={value}
            autoComplete="off"
            inputProps={{
              'aria-label': 'search',
            }}
            onChange={(e) => onChange(e.target.value)}
            className={classes.input}
            type="search"
            disabled={disabled}
          />
          <IconButton className={classes.iconButton} aria-label="search">
            <SearchIcon />
          </IconButton>
        </div>
      )}
    </Tooltip>
  );
};

export default SearchInputBase;
