import React , { FC , CSSProperties } from 'react';
import useStyles from './style';

export interface SpanBoldProps {
    label : string;
    style? : CSSProperties
}

const SpanBold : FC<SpanBoldProps> = ({label,style}) => {
    const classes = useStyles();

    return (
        <span className={classes.root} style={style} >{label}</span>
    );
};

export default SpanBold;