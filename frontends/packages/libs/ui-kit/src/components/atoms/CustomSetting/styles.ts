import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((_theme: Theme) =>
  createStyles({
    buttonSetting: {
      display: 'flex',
      cursor: 'pointer',
      '& svg': {
        fontSize: '26px',
      },
    },
    formeControl: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between !important',
      flexDirection: 'row-reverse !important' as any,
      marginLeft: '0 !important',
      marginRight: '0 !important',
      padding: '8px',
    },
  })
);

export default useStyles;
