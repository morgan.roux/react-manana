import { Box, FormControlLabel, FormGroup, Menu, Switch } from '@material-ui/core';
import React, { ChangeEvent, FC, useState, MouseEvent, useEffect } from 'react';
import SettingsIcon from '@material-ui/icons/Settings';
import useStyles from './styles';

interface Parametre {
  libelle?: any;
  code?: any;
  active?: boolean;
}

interface CustomSettingProps {
  parametres?: Parametre[];
  onChange?: (value: any) => void;
}

export const CustomSetting: FC<CustomSettingProps> = ({ parametres, onChange }) => {
  const initialState = (parametres || []).reduce((accumulator, parametre) => {
    return { ...accumulator, [parametre.code]: parametre.active };
  }, {});

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const [state, setState] = useState({});

  useEffect(() => {
    setState(initialState);
  }, [JSON.stringify(initialState)]);

  const handleSwitchChange = (event: ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [event.target.name]: event.target.checked });
    onChange && onChange({ code: event.target.name, value: event.target.checked });
  };

  const handleClickMenu = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  const classes = useStyles();

  return (
    <Box>
      <Box onClick={handleClickMenu} className={classes.buttonSetting}>
        <SettingsIcon />
      </Box>
      <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleCloseMenu}>
        <FormGroup>
          {(parametres || []).map((item) => (
            <FormControlLabel
              className={classes.formeControl}
              label={item.libelle}
              value={item.code}
              control={<Switch name={item.code} onChange={handleSwitchChange} checked={state[item.code]} />}
            />
          ))}
        </FormGroup>
      </Menu>
    </Box>
  );
};
