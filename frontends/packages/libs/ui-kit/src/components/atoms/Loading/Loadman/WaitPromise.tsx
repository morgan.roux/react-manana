import delay from 'delay';

export default (promise: Promise<any>, ms: number) => {
  let promiseErr: any;

  promise = promise.catch((err) => (promiseErr = err));

  return Promise.all([promise, delay(ms)]).then((val) => (promiseErr ? Promise.reject(promiseErr) : val[0]));
};
