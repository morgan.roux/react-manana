import Loadman from './Loadman';
import MainLoading from './MainLoading';
import SmallLoading from './SmallLoading';

export { Loadman, MainLoading, SmallLoading };
