import { LinearProgress, Modal, withStyles } from '@material-ui/core';
import React, { Component } from 'react';
import loadingPhoto from './../../../../assets/images/digital4win.svg';

const styles = () => ({
  col: {
    display: 'flex' as 'flex',
    flex: '0 1 auto',
    flexDirection: 'column' as 'column',
    left: 0,
    msFlex: '0 1 auto',
    position: 'absolute' as 'absolute',
    right: 0,
    top: '35%',
    webkitFlex: '0 1 auto',
  },
  loadingPwg: {
    margin: '0 auto 16px',
    width: '11rem',
    zIndex: 999,
  },
  loadingProgress: {
    margin: '8px auto',
    width: '15rem',
    zIndex: 999,
  },

  loadingContent: {
    height: '100vh',
    background: '#FFF',
  },
});

interface LoadingProps {
  classes: any;
}

class Loading extends Component<LoadingProps, {}> {
  render() {
    const { classes } = this.props;
    return (
      <Modal open={true}>
        <div className={classes.loadingContent}>
          <div className={classes.col}>
            <img className={classes.loadingPwg} src={loadingPhoto} alt="loading" />
            <LinearProgress className={classes.loadingProgress} color="secondary" />
          </div>
        </div>
      </Modal>
    );
  }
}

export default withStyles(styles)(Loading);
