import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {
    // '@global': {
    //   '*::-webkit-scrollbar': {
    //     height: '0px !important',
    //   },
    // },
    overflowY: 'hidden',
    ' & .k-grid-content.k-virtual-content': {
      overflowY: 'auto',
    },
  },
  grid: {
    border: 0,
    borderBottom: '1px solid rgba(0, 0, 0, 0.08) !important',
    borderRight: '1px solid rgba(0, 0, 0, 0.08) !important',
    '& .k-grid-header-wrap': {
      background: '#FFFFFF',
    },
    // '& .k-grid-header': {
    //   paddingRight: '0 !important',
    // },
  },
  columnCell: {
    padding: '0 8px !important',
    height: 40,
    '& div': {
      height: 26,
      borderRadius: 4,
      display: 'flex',
      alignItems: 'center',
      fontSize: 12,
      fontFamily: 'Roboto',
      paddingLeft: 8,
      color: '#FFFFFF',
    },
  },
  firstColumnCell: {
    fontFamily: 'Roboto',
    textAlign: 'left',
    textTransform: 'uppercase',
    padding: '8px 12px',
    fontSize: 14,
    border: '0 !important',
    borderLeft: '1px solid rgba(0, 0, 0, 0.08) !important',
    position: 'sticky',
  },
  columnHeaderCell: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    fontFamily: 'Roboto',
  },
  subColumnHeaderContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    fontFamily: 'Roboto',
    '& .MuiTypography-root:nth-child(1)': {
      fontWeight: 'bold',
      fontSize: 14,
    },
    '& .MuiTypography-root:nth-child(2)': {
      fontSize: 14,
      color: '#42424261',
    },
  },
  headerColumn: {
    borderTop: '1px solid rgba(0, 0, 0, 0.08) !important',
  },
  firstColumn: {
    display: 'none',
    background: 'white',
    color: 'white',
  },
}));

export default useStyles;
