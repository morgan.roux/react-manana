import {
  Grid,
  GridGroupableSettings,
  GridPagerSettings,
  GridRowProps,
  GridSortSettings,
  GridToolbar,
} from '@progress/kendo-react-grid';
import { IntlProvider, load, loadMessages, LocalizationProvider } from '@progress/kendo-react-intl';
import currencyData from 'cldr-core/supplemental/currencyData.json';
import likelySubtags from 'cldr-core/supplemental/likelySubtags.json';
import weekData from 'cldr-core/supplemental/weekData.json';
/* ES */
import caGregorian_ES from 'cldr-dates-full/main/es/ca-gregorian.json';
import dateFields_ES from 'cldr-dates-full/main/es/dateFields.json';
import timeZoneNames_ES from 'cldr-dates-full/main/es/timeZoneNames.json';
/* FR */
import caGregorian_FR from 'cldr-dates-full/main/fr/ca-gregorian.json';
import dateFields_FR from 'cldr-dates-full/main/fr/dateFields.json';
import timeZoneNames_FR from 'cldr-dates-full/main/fr/timeZoneNames.json';
import currencies_ES from 'cldr-numbers-full/main/es/currencies.json';
import numbers_ES from 'cldr-numbers-full/main/es/numbers.json';
import currencies_FR from 'cldr-numbers-full/main/fr/currencies.json';
import numbers_FR from 'cldr-numbers-full/main/fr/numbers.json';
import React, { FC } from 'react';
import { esMessages } from './locales/es';
import { frMessages } from './locales/fr';
import useStyles from './styles';

load(
  likelySubtags,
  currencyData,
  weekData,
  numbers_FR,
  currencies_FR,
  caGregorian_FR,
  dateFields_FR,
  timeZoneNames_FR,
  numbers_ES,
  currencies_ES,
  caGregorian_ES,
  dateFields_ES,
  timeZoneNames_ES
);

loadMessages(frMessages, 'fr-FR');
loadMessages(esMessages, 'es-ES');

interface CustomGridProps {
  className?: string;
  filterable?: boolean;
  columns?: any;
  data?: any;
  detailComponent?: any;
  toolbar?: any;
  locale?: 'fr' | 'es' | 'en';
  pageable?: boolean | GridPagerSettings;
  groupable?: boolean | GridGroupableSettings;
  sortable?: GridSortSettings;
  reorderable?: boolean;
  resizable?: boolean;
  rowRender?: (row: React.ReactElement<HTMLTableRowElement>, props: GridRowProps) => React.ReactNode;
}

const CustomGrid: FC<CustomGridProps> = ({
  className,
  filterable,
  columns,
  data,
  detailComponent,
  toolbar,
  locale,
  pageable,
  groupable,
  sortable,
  reorderable,
  resizable,
  rowRender,
}) => {
  const classes = useStyles();

  const locales = {
    en: { language: 'en-US', locale: 'en' },
    es: { language: 'es-ES', locale: 'es' },
    fr: { language: 'fr-FR', locale: 'fr' },
  };

  const activeLocale = locale ? locales[locale] : locales['fr'];

  return (
    <div className={classes.root}>
      <LocalizationProvider language={activeLocale.language}>
        <IntlProvider locale={activeLocale.locale}>
          <div>
            <Grid
              className={className}
              sortable={sortable}
              filterable={filterable}
              groupable={groupable}
              reorderable={reorderable}
              pageable={pageable}
              data={data}
              onDataStateChange={undefined}
              detail={detailComponent}
              expandField="expanded"
              onExpandChange={undefined}
              resizable={resizable}
              rowRender={rowRender}
            >
              {toolbar && <GridToolbar>{toolbar}</GridToolbar>}
              {columns}
            </Grid>
          </div>
        </IntlProvider>
      </LocalizationProvider>
    </div>
  );
};

export default CustomGrid;
