import { GridColumn } from '@progress/kendo-react-grid';
import React from 'react';
import { getDaysInAMonth, getMonthNumber, getMonths } from '../../../services/Helpers';
import CustomGrid from './CustomGrid';
import useStyles from './styles';
import { data } from './data';
import { Box, Typography } from '@material-ui/core';

export default {
  title: 'Design system|Atoms/CustomGrid',
  parameters: {
    info: { inline: true },
  },
};

export const customGridStory = () => {
  const classes = useStyles();

  const headersData = getMonths().map(month => ({
    month,
    weeks: getDaysInAMonth(2021, getMonthNumber(month) - 1),
  }));

  const columnWithChildren = (title: string, subTitles: { range: string; week: number }[]) => {
    return (
      <GridColumn
        headerCell={props => <ColumnHeaderCell {...props} {...{ title }} />}
        children={subTitles.map(subTitle => (
          <GridColumn
            width="70px"
            headerCell={props => <SubColumnHeaderCell {...props} {...subTitle} />}
            cell={props => (
              <ColumnCell
                {...props}
                {...{ week: subTitle.week }}
                {...{
                  value: '876',
                }}
              />
            )}
          />
        ))}
        headerClassName={classes.headerColumn}
      />
    );
  };

  const ColumnCell = (props: any) => {
    const { dataItem, week } = props;
    const weeks = dataItem?.weeks || [];
    const element = weeks.find((el: any) => el.week.includes(week));
    const label = element?.week.includes(week) ? element?.value : undefined;
    return element?.week[0] === week ? (
      <td className={classes.columnCell} colSpan={label ? element?.week.length : undefined}>
        <div style={{ background: element.color || 'white' }}>{label}</div>
      </td>
    ) : (
      <td></td>
    );
  };

  const ColumnHeaderCell = (props: any) => {
    const { title } = props;
    return <Typography className={classes.columnHeaderCell}>{title}</Typography>;
  };

  const SubColumnHeaderCell = (props: any) => {
    const { week, range } = props;
    return (
      <Box className={classes.subColumnHeaderContainer}>
        <Typography>{`S${week}`}</Typography>
        <Typography>{range}</Typography>
      </Box>
    );
  };

  const firstColumn = (
    <GridColumn className={classes.firstColumnCell} field="label" width="180px" locked={true} />
  );

  const columns = headersData.map(el =>
    columnWithChildren(
      el.month,
      el.weeks.map(week => ({ range: `${week.start}-${week.end}`, week: week.week })),
    ),
  );

  return (
    <CustomGrid
      pageable={false}
      groupable={false}
      columns={[firstColumn, columns]}
      filterable={false}
      data={data}
      className={classes.grid}
    />
  );
};
