export const data = [
  {
    label: 'AVENE 1',
    weeks: [
      { value: 'STREPSILS', week: [1, 2, 3], color: '#00A745' },
      {
        value: 'NUROFEN',
        week: [4, 5, 6],
        color: '#63B8DD',
      },
      { value: 'STREPSILS', week: [16, 17, 18, 19], color: '#00A745' },
    ],
  },
  {
    label: 'AVENE 2',
    weeks: [{ value: 'STREPSILS', week: [7, 8, 9], color: '#00A745' }],
  },
  {
    label: 'AVENE 3',
    weeks: [
      { value: 'STREPSILS', week: [3, 4, 5], color: '#00A745' },
      {
        value: 'NUROFEN',
        week: [13, 14, 15],
        color: '#63B8DD',
      },
      { value: 'STREPSILS', week: [30, 31, 32, 33], color: '#00A745' },
    ],
  },
  {
    label: 'AVENE 4',
    weeks: [
      { value: 'STREPSILS', week: [1, 2, 3], color: '#00A745' },
      {
        value: 'NUROFEN',
        week: [4, 5, 6],
        color: '#63B8DD',
      },
      { value: 'STREPSILS', week: [16, 17, 18, 19], color: '#00A745' },
    ],
  },
];
