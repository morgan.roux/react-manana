import { createStyles, Theme, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    collaborateurInput: {
      minHeight: 48,
      marginBottom: 16,
      borderRadius: 5,
      border: '1px solid #c4c4c4',
      [theme.breakpoints.up('sm')]: {
        marginTop: 16,
      },
      '& legend': {
        marginLeft: 12,
      },
    },
    collaborateurBox: {
      display: 'flex',
      justifyContent: 'space-between',
    },
  })
);

export default useStyles;
