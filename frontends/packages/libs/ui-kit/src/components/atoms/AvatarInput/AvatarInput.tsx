import { Avatar, Box } from '@material-ui/core';
import { uniqBy } from 'lodash';
import React, { FC } from 'react';
import { stringToAvatar } from '../../../services/Helpers';
import useStyles from './styles';

interface AvatarInputProps {
  label?: string;
  list: any[];
  small?: boolean;
  icon?: any;
  standard?: boolean;
  className?: string;
}

const AvatarInput: FC<AvatarInputProps> = ({ label, list, small, standard, icon, className }) => {
  const classes = useStyles({});

  if (standard) {
    return (
      <Box className={`${classes.collaborateurBox} ${className || ''}`} id="collaborateur-input">
        <Box display="flex" flexDirection="row" alignItems="center" paddingLeft="8px">
          {uniqBy(list, 'id').map((u: any) => {
            const photo = u?.userPhoto?.fichier?.publicUrl || u?.photo?.publicUrl;
            return photo ? (
              <img src={photo} style={{ width: small ? 24 : 30, height: small ? 24 : 30, borderRadius: '50%' }} />
            ) : (
              <Avatar style={{ width: small ? 24 : 30, height: small ? 24 : 30, fontSize: 12 }} key={u?.id}>
                {stringToAvatar(u?.userName || u?.fullName || '')}
              </Avatar>
            );
          })}
        </Box>
        {icon}
      </Box>
    );
  }
  return (
    <fieldset className={classes.collaborateurInput}>
      {label && <legend>{label}</legend>}
      <Box className={classes.collaborateurBox} id="collaborateur-input" padding={!label ? '8px' : '0px'}>
        <Box display="flex" flexDirection="row" alignItems="center" paddingLeft="8px">
          {uniqBy(list, 'id').map((u: any) => {
            const photo = u?.userPhoto?.fichier?.publicUrl || u?.photo?.publicUrl;
            return photo ? (
              <img src={photo} style={{ width: small ? 24 : 30, height: small ? 24 : 30, borderRadius: '50%' }} />
            ) : (
              <Avatar style={{ width: small ? 24 : 30, height: small ? 24 : 30, fontSize: 12 }} key={u?.id}>
                {stringToAvatar(u?.userName || u?.fullName || '')}
              </Avatar>
            );
          })}
        </Box>
        {icon}
      </Box>
    </fieldset>
  );
};

export default AvatarInput;
