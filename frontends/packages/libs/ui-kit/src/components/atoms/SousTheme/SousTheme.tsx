import React, { FC, MouseEvent } from 'react';
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { FiberManualRecord } from '@material-ui/icons';

export interface SousThemeProps {
  ordreParent?: number | string;
  onClick?: (event: MouseEvent) => void;
}

const SousTheme: FC<{ ordre?: string; nom: string } & SousThemeProps> = ({ ordreParent, ordre, nom, onClick }) => {
  let index: string = '';
  ordreParent && (index += `${ordreParent}.`);
  ordre && (index += `${ordre}-`);

  return (
    <ListItem button={true} onClick={onClick}>
      <ListItemIcon>
        <FiberManualRecord />
      </ListItemIcon>
      <ListItemText primary={`${index}${nom}`} />
    </ListItem>
  );
};

export default SousTheme;
