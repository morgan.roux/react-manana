import React, { FC } from 'react';
import {
  makeStyles,
  createStyles,
  Theme,
  InputLabel,
  MenuItem,
  FormControl,
  Select,
  FormGroup,
  FormControlLabel,
  Checkbox,
  Typography,
} from '@material-ui/core';
import classnames from 'classnames';

const styles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      width: '100%',
      position: 'relative',
      marginBottom: 16,
      '& .MuiSelect-selectMenu': {
        // minHeight: '30px !important',
        display: 'flex',
        alignItems: 'center',
      },
      '& label': {
        '& .MuiFormLabel-asterisk': {
          color: 'red',
        },
      },
      '&:hover label, & label.Mui-focused': {
        color: theme.palette.primary.main,
      },
      '&.Mui-focused fieldset': {
        border: `2px solid ${theme.palette.primary.main} !important`,
      },
      '& input:invalid + fieldset': {
        borderColor: 'red',
      },
      '& .Mui-focused .MuiIconButton-label svg': {
        color: `${theme.palette.primary.main} !important`,
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: theme.palette.common.white,
      },
      '& .MuiOutlinedInput-root': {
        minHeight: 48,
        '& fieldset': {
          // borderColor: theme.palette.common.black,
        },
        '&:hover fieldset': {
          borderColor: theme.palette.primary.main,
        },
        '&.Mui-focused fieldset': {
          borderColor: theme.palette.primary.main,
        },
      },
      transition:
        'padding-left 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,border-color 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms,border-width 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms',
    },
    label: {
      background: theme.palette.common.white,
      // color: theme.palette.common.black,
      padding: '0 4px',
    },
    noLabel: {
      '& fieldset > legend > span': {
        display: 'none',
      },
    },
    textLeft: {
      '& .main-MuiSelect-outlined': {
        textAlign: 'left',
        padding: '14.5px !important',
      },
    },
    valueAsLabel: {
      position: 'absolute',
      top: '50%',
      transform: 'translateY(-50%)',
      paddingInline: 17,
      whiteSpace: 'nowrap',
      width: '85%',
      overflow: 'hidden',
    },
  })
);

interface CustomSelectCheckBoxProps {
  index?: any;
  listId?: any;
  list?: any[] | undefined;
  error?: boolean;
  variant?: any;
  shrink?: boolean;
  required?: boolean;
  withNoneValue?: boolean;
  placeholder?: string;
  withPlaceholder?: boolean;
  disabled?: boolean;
  defaultValue?: any;
  color?: string;
  colorIndex?: any;
  noMarginBottom?: boolean;
  useCheckbox?: boolean;
  label?: string;
  classNameSelectCheckBoxMenu?: string;
  selected?: any;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const CustomSelectCheckBox: FC<CustomSelectCheckBoxProps> = ({
  index,
  listId,
  placeholder,
  disabled,
  defaultValue,
  color,
  colorIndex,
  noMarginBottom,
  useCheckbox,
  list = [],
  label = '',
  variant = 'outlined',
  error = false,
  shrink = true,
  required = false,
  withNoneValue = false,
  withPlaceholder = false,
  classNameSelectCheckBoxMenu,
  selected,
  onChange,
}) => {
  const classes = styles();
  const selectRootClasses = makeStyles({ select: { color: color || '#000', padding: 14.5 } })();

  const valueAsLabel = selected
    ? list
        .filter((li) => selected[li.id])
        .map((li) => li.label)
        .join(', ')
    : '';
  return (
    <FormControl
      required={required}
      className={!label ? classnames(classes.formControl, classes.noLabel) : classes.formControl}
      variant={variant}
      error={error}
      disabled={disabled}
      style={{ marginBottom: variant === 'standard' || noMarginBottom ? '0px' : '16px' }}
    >
      <InputLabel shrink={shrink} htmlFor="name" className={classes.label}>
        {label}
      </InputLabel>
      <Typography className={classes.valueAsLabel}>{!!valueAsLabel ? valueAsLabel : defaultValue}</Typography>
      <Select
        className={`${classes.textLeft}`}
        MenuProps={{
          getContentAnchorEl: null,
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'left',
          },
        }}
        classes={{ select: selectRootClasses.select }}
        placeholder={defaultValue}
      >
        {withNoneValue && (
          <MenuItem value="">
            <em>Aucun</em>
          </MenuItem>
        )}
        {withPlaceholder && (
          <MenuItem value="" disabled={true} selected={true}>
            {placeholder}
          </MenuItem>
        )}
        {!useCheckbox
          ? list &&
            list.map((item: any, key: number) => {
              const val = typeof item === 'object' ? item[listId] : item;
              const show = typeof item === 'object' ? item[index] : item;
              if (val && show) {
                return (
                  <MenuItem
                    key={`custom_select_menu_item_${key}`}
                    value={val}
                    // className={classes.menuItem}
                    selected={val === defaultValue}
                    style={{ color: item[colorIndex] || '#000', display: 'block', padding: 10 }}
                  >
                    {show}
                  </MenuItem>
                );
              }
              return null;
            })
          : list &&
            list.map((item: any, key: number) => {
              const val = typeof item === 'object' ? item[listId] : item;
              const show = typeof item === 'object' ? item[index] : item;
              if (val && show) {
                return (
                  <FormControl className={classNameSelectCheckBoxMenu} key={`list-${key}`}>
                    <FormGroup>
                      <FormControlLabel
                        control={<Checkbox checked={selected ? selected[val] : false} onChange={onChange} />}
                        label={show}
                        name={val}
                      />
                    </FormGroup>
                  </FormControl>
                );
              }
              return null;
            })}
      </Select>
    </FormControl>
  );
};

export default CustomSelectCheckBox;

CustomSelectCheckBox.defaultProps = {
  list: [],
  label: '',
  variant: 'outlined',
  error: false,
  shrink: true,
  required: false,
  withNoneValue: false,
  withPlaceholder: false,
};
