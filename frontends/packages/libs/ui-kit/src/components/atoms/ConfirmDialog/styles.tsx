import { createStyles, Theme, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((_theme: Theme) =>
  createStyles({
    root: {
      '& *': {
        letterSpacing: 0,
        fontFamily: 'Roboto',
      },
      '& .MuiDialogActions-root': {
        marginBottom: '25px !important',
        justifyContent: 'center',
        '& button:nth-child(1)': {
          marginRight: 25,
        },
      },
    },
    content: {
      '& *': {
        color: '#616161 !important',
        fontSize: '20px !important',
        fontWeight: 500,
        maxWidth: 600,
        textAlign: 'center',
        // padding: '16px 44px 24px',
        padding: '16px 39px 24px',
      },
    },
    btn: {
      textTransform: 'uppercase',
      padding: '10px 35px !important',
    },
    cancelBtn: {
      background: '#e6e6e6',
    },
    title: {
      textAlign: 'center',
      padding: '30px 0 0',
      // backgroundColor: theme.palette.primary.main,
      '& .MuiTypography-root': {
        fontWeight: 'bold',
        fontSize: 30,
        color: '#424242',
      },
    },
    boxShadow: {
      boxShadow: '0 0 18px 0 #ccc',
    },
  })
);

export default useStyles;
