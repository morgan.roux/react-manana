import React, { Dispatch, SetStateAction, FC, MouseEvent } from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Theme } from '@material-ui/core';

import { createStyles, makeStyles } from '@material-ui/core';
import { NewCustomButton } from '../CustomButton';

const useStyles = makeStyles((_theme: Theme) =>
  createStyles({
    root: {
      '& *': {
        letterSpacing: 0,
        fontFamily: 'Roboto',
      },
      '& .MuiDialogActions-root': {
        marginBottom: '25px !important',
        justifyContent: 'center',
        '& button:nth-child(1)': {
          marginRight: 25,
        },
      },
    },
    content: {
      '& *': {
        color: '#616161 !important',
        fontSize: '20px !important',
        fontWeight: 500,
        maxWidth: 600,
        textAlign: 'center',
        // padding: '16px 44px 24px',
        padding: '16px 39px 24px',
      },
    },
    btn: {
      textTransform: 'uppercase',
      padding: '10px 35px !important',
    },
    cancelBtn: {
      background: '#e6e6e6',
    },
    title: {
      textAlign: 'center',
      padding: '30px 0 0',
      // backgroundColor: theme.palette.primary.main,
      '& .MuiTypography-root': {
        fontWeight: 'bold',
        fontSize: 30,

        color: '#424242',
      },
    },
    centerButton: {
      justifyContent: 'center',
    },
  })
);

interface ConfirmDeleteDialogProps {
  title?: string;
  content?: string | JSX.Element;
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  isLoading?: boolean;
  confirmBtnLabel?: string;
  onClickConfirm: (event: MouseEvent<any>) => void;
  centeredBtns?: boolean;
}

const ConfirmDeleteDialog: FC<ConfirmDeleteDialogProps> = ({
  title,
  content,
  open,
  setOpen,
  onClickConfirm,
  isLoading,
  centeredBtns,
  confirmBtnLabel = 'Supprimer',
}) => {
  const classes = useStyles({});
  const handleCloseConfirmDialog = (e: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
    e.stopPropagation();
    setOpen(false);
  };

  return (
    <Dialog
      // tslint:disable-next-line: jsx-no-lambda
      onClick={(event) => {
        event.preventDefault();
        event.stopPropagation();
      }}
      className={classes.root}
      open={open}
      onClose={handleCloseConfirmDialog}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle className={classes.title} id="alert-dialog-title">
        {title || 'Suppression'}
      </DialogTitle>
      <DialogContent className={classes.content}>
        <DialogContentText id="alert-dialog-description">
          {content || 'Êtes-vous sur de vouloir supprimer ?'}
        </DialogContentText>
      </DialogContent>
      <DialogActions
        // tslint:disable-next-line: jsx-no-lambda
        onClick={(event) => {
          event.preventDefault();
          event.stopPropagation();
        }}
        className={centeredBtns ? classes.centerButton : ''}
        style={{ padding: 20 }}
      >
        <NewCustomButton
          className={`${classes.btn} ${classes.cancelBtn}`}
          onClick={handleCloseConfirmDialog}
          disabled={isLoading ? true : false}
          theme="flatTransparent"
        >
          Annuler
        </NewCustomButton>
        <NewCustomButton
          className={classes.btn}
          onClick={onClickConfirm}
          autoFocus={true}
          disabled={isLoading ? true : false}
        >
          {confirmBtnLabel}
        </NewCustomButton>
      </DialogActions>
    </Dialog>
  );
};

export default ConfirmDeleteDialog;
