import React, { FC, ReactNode } from 'react';
import { Box, Typography } from '@material-ui/core';
import useStyles from './styles';
import Image from './image_default22.png';
interface NoItemContentImageProps {
  title: string;
  subtitle: string;
  src?: string;
  children?: ReactNode;
}
const NoItemContentImage: FC<NoItemContentImageProps> = ({ title, subtitle, src, children }) => {
  const classes = useStyles({});
  return (
    <Box
      width="100%"
      height="100%"
      display="flex"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      padding="0px 20px"
    >
      <img src={src || Image} className={classes.img} />
      <Typography className={classes.title}>{title}</Typography>
      <Typography className={classes.subtitle}>{subtitle}</Typography>
      {children}
    </Box>
  );
};

export default NoItemContentImage;
