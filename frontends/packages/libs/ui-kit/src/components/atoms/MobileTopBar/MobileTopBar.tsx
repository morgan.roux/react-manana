import { AppBar, Box, CssBaseline, IconButton, Typography } from '@material-ui/core';
import { ArrowBack, Close, Menu } from '@material-ui/icons';
import React, { FC, ReactNode, useEffect, useState } from 'react';
import { useStyles } from './styles';

interface MobileTopBarProps {
  title?: string;
  withBackBtn?: boolean;
  withCloseBtn?: boolean;
  optionBtn?: ReactNode[];
  closeUrl?: string | null;
  onClickBack?: () => void;
  handleDrawerToggle?: () => void;
  withTopMargin?: boolean;
  fullScreen?: boolean;
  onClose?: (closeUrl?: string) => void;
  push?: (url: string) => void;
  onGoBack?: () => void;
}

function getWindowDimensions(): 'isLg' | 'isMd' | 'isSm' | 'unknown' {
  const { innerWidth: width } = window;
  if (width >= 1280) {
    return 'isLg';
  }
  if (width >= 960 && width < 1280) {
    return 'isMd';
  } else if (width < 960) {
    return 'isSm';
  }
  return 'unknown';
}

export const useWindowDimensions = () => {
  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return windowDimensions;
};

const MobileTopBar: FC<MobileTopBarProps> = ({
  title,
  withBackBtn,
  withCloseBtn,
  optionBtn,
  closeUrl,
  onClickBack,
  handleDrawerToggle,
  withTopMargin,
  fullScreen,
  onClose,
  push,
  onGoBack,
}) => {
  const classes = useStyles({});
  const dimensions = useWindowDimensions();

  const handleGoBack = () => {
    if (onClickBack) {
      onClickBack();
    } else if (onGoBack) {
      onGoBack();
    }
  };

  const handleClose = () => {
    if (onClose) {
      onClose();
    } else if (push) {
      push(closeUrl || '/');
    }
  };

  return (
    <AppBar
      position={fullScreen ? 'relative' : 'fixed'}
      className={classes.root}
      style={{ marginTop: withTopMargin ? '86px' : '0px', height: fullScreen ? '86px' : '50px' }}
    >
      <CssBaseline />
      <Box display="flex" alignItems="center">
        {dimensions === 'isMd' ? (
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
          >
            <Menu />
          </IconButton>
        ) : (
          withBackBtn && (
            <IconButton color="inherit" onClick={handleGoBack}>
              <ArrowBack />
            </IconButton>
          )
        )}
        <Typography>{title}</Typography>
      </Box>
      {withCloseBtn && (
        <IconButton color="inherit" onClick={handleClose}>
          <Close />
        </IconButton>
      )}
      {dimensions === 'isSm' && <>{optionBtn && <Box>{optionBtn.map((btn) => btn)}</Box>}</>}
    </AppBar>
  );
};

export default MobileTopBar;
