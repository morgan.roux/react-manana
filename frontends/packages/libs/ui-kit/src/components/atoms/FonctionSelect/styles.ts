import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      background: '#F2F2F2 !important',
      marginRight: '8px',
      '& .main-MuiTypography-root': {
        textTransform: 'initial',
        fontSize: '14px',
        fontFamily: 'Roboto',
        color: theme.palette.common.black,
      },
    },
    list: {
      '&  .main-MuiList-root': {
        paddingTop: '0 !important',
        paddingBottom: '0 !important',
      },
      '& .main-MuiListItem-root': {
        paddingTop: '0 !important',
        paddingBottom: '0 !important',
      },
    },
  })
)

export default useStyles;