import { ListItemText } from '@material-ui/core';
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  List,
  ListItem,
  Menu,
  Typography,
} from '@material-ui/core';
import React, { useState, ChangeEvent, MouseEvent } from 'react';
import useStyles from './styles';

export const FonctionSelect = () => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const handleMenuClick = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  const [state, setState] = useState({
    todo: false,
    liaison: false,
    actualite: false,
    messageries: false,
    qualite: false,
    document: false,
    relation: false,
    appel: false,
    produit: false,
  });

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  // const { todo, liaison, actualite, messageries, qualite, document, relation, appel, produit } = state;

  const listMenus = [
    { id: 1, name: 'todo', label: 'Ma To-Do' },
    { id: 2, name: 'liaison', label: 'Information de liaison' },
    { id: 3, name: 'actualite', label: 'Actualité' },
    { id: 4, name: 'messageries', label: 'Messageries' },
    { id: 5, name: 'qualite', label: 'Démarche qualité' },
    { id: 6, name: 'document', label: 'Documents' },
    { id: 7, name: 'relation', label: 'Relation labo' },
    { id: 8, name: 'appel', label: 'Appel reçus' },
    { id: 9, name: 'produit', label: 'Produit' },
  ];

  const classes = useStyles();

  return (
    <Box>
      <Button onClick={handleMenuClick} className={classes.button}>
        <Typography>Tous les fonctions</Typography>
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleCloseMenu}
      >
        <FormControl className={classes.list} component="fieldset">
          <List>
            <ListItem button onClick={handleCloseMenu}>
              <ListItemText>
                <FormGroup>
                  {listMenus.map((index) => (
                    <FormControlLabel
                      control={<Checkbox onChange={handleChange} name={index.name} />}
                      label={index.label}
                    />
                  ))}
                </FormGroup>
              </ListItemText>
            </ListItem>
          </List>
        </FormControl>
      </Menu>
    </Box>
  );
};
