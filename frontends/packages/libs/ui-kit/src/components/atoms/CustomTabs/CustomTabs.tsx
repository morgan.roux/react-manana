import React, { ReactNode, FC, useState, ChangeEvent, useEffect } from 'react';
import useStyles from './styles';
import { Box, Tabs, Tab, AppBar, IconButton } from '@material-ui/core';
import { ArrowBackIos, ArrowForwardIos } from '@material-ui/icons';

export interface TabInterface {
  id: number;
  label: string;
  content: ReactNode;
  clickHandlerParams?: string;
}

export interface CustomTabsProps {
  tabs: TabInterface[];
  clickHandler?: (params: string) => void | undefined;
  activeStep?: number;
  hideArrow?: boolean;
}

const a11yProps = (index: any) => {
  return {
    id: `custom-tab-${index}`,
    'aria-controls': `custom-tabpanel-${index}`,
  };
};

const CustomTabs: FC<CustomTabsProps> = ({ tabs, clickHandler, activeStep, hideArrow }) => {
  const classes = useStyles();
  const [value, setValue] = useState(activeStep || 0);

  const handleChange = (_event: ChangeEvent<{}>, newValue: number) => {
    setValue(newValue);
  };

  const muiTabs = () => {
    const muiTabsScroller = document.querySelector('.MuiTabs-scroller');
    return {
      element: muiTabsScroller,
      scrollLeft: muiTabsScroller ? muiTabsScroller.scrollLeft : 0,
    };
  };

  const onClickLeftArrow = () => {
    const { element, scrollLeft } = muiTabs();
    if (element && scrollLeft > 0) {
      element.scrollLeft -= 194 * 2;
    }
  };

  const onClickRightArrow = () => {
    const { element } = muiTabs();
    if (element) {
      element.scrollLeft += 194 * 2;
    }
  };

  const handleClickEvent = () => {
    const activeTab = tabs.find((tab) => tab.id === value);
    if (activeTab && activeTab.clickHandlerParams && clickHandler) {
      clickHandler(activeTab.clickHandlerParams);
    }
  };

  useEffect(() => {
    handleClickEvent();
  }, [value]);

  return (
    <div className={classes.customTabsRoot}>
      <AppBar className={classes.customTabsAppBar} color="transparent" position="static" id="customTabsAppBarId">
        {!hideArrow && (
          <IconButton onClick={onClickLeftArrow}>
            <ArrowBackIos />
          </IconButton>
        )}
        <Tabs style={{ width: '100%' }} value={value} onChange={handleChange} aria-label="customtabs tabs">
          {tabs.map((tab: TabInterface, index: number) => {
            return <Tab key={`tabs_tab_${index}`} label={tab.label} {...a11yProps(tab.id)} />;
          })}
        </Tabs>
        {!hideArrow && (
          <IconButton onClick={onClickRightArrow}>
            <ArrowForwardIos />
          </IconButton>
        )}
      </AppBar>
      {tabs.map((tab: TabInterface, index: number) => {
        return (
          <div
            key={`tabs_tabpanel_${index}`}
            role="tabpanel"
            hidden={value !== index}
            id={`custom-tabpanel-${index}`}
            aria-labelledby={`custom-tab-${index}`}
          >
            {value === index && <Box>{tab.content}</Box>}
          </div>
        );
      })}
    </div>
  );
};

export default CustomTabs;
