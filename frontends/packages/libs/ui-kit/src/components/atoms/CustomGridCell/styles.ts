import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: { textAlign: 'right', ' & .k-grid-content': { overflowY: 'auto' } },
  header: {},
  body: {},
  footer: {},
}));

export default useStyles;
