import React, { FC } from 'react';
import EditableLabel from '../EditableLabel';
import useStyles from './styles';

interface CustomGridCellProps {
  backgroundColor?: string;
  optionBtn?: any;
  tooltip?: string;
  onRequestSaveInput?: (value: string) => void;
  bodyValue?: string;
  footerValue?: string;
  bodyElement?: any;
  footerElement?: any;
  editionDisabled?: boolean;
  loading?: boolean;
  labelOnly?: boolean;
  onClick?: () => void;
}

const CustomGridCell: FC<CustomGridCellProps> = ({
  backgroundColor,
  optionBtn,
  tooltip,
  onRequestSaveInput,
  bodyValue,
  footerValue,
  bodyElement,
  footerElement,
  editionDisabled,
  loading,
  labelOnly,
  onClick,
}) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.header}>{optionBtn}</div>
      <div className={classes.body}>
        <EditableLabel
          value={bodyValue}
          labelElement={bodyElement}
          onRequestSaveInput={onRequestSaveInput}
          editionDisabled={editionDisabled}
          loading={loading}
          labelOnly={labelOnly}
          onClick={onClick}
        />
      </div>
      <div className={classes.footer}>{footerElement || footerValue}</div>
    </div>
  );
};

export default CustomGridCell;
