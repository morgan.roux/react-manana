import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => 
  createStyles({
    root: {
      '& .main-MuiTypography-root': {
        fontSize: '16px',
        fontWeight: '500',
        fontFamily: 'Roboto',
      },
    }
  })
)

export default useStyles;