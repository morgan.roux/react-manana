import { Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import useStyles from './styles';

interface CustomeTitleProps {
  title?: string;
  className?: string 
}

export const CustomTitle: FC<CustomeTitleProps> = ({ title }) => {
  
  const classes = useStyles();

  return(
    <Box className={classes.root}>
      <Typography>{title}</Typography>
    </Box>
  )
}