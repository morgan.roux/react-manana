import React, { FC } from 'react';
import { ListItem, ListItemAvatar, Avatar, ListItemText, Typography } from '@material-ui/core';
import { useStyles } from './styles';

export interface PresentationItemProps {
  presentation: any | null;
}

const PresentationItem: FC<PresentationItemProps> = ({ presentation }) => {
  const classes = useStyles();

  return (
    <ListItem>
      {presentation.photo && (
        <ListItemAvatar>
          <Avatar
            alt="Remy Sharp"
            src={presentation.photo.publicUrl}
            className={classes.bigAvatar}
          />
        </ListItemAvatar>
      )}
      <ListItemText>
        <Typography className={classes.userName}>
          {presentation && presentation.fullName ? presentation.fullName : ''}
        </Typography>
      </ListItemText>
    </ListItem>
  );
};

export default PresentationItem;
