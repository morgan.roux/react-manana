import { createStyles, makeStyles } from "@material-ui/core";

const useStyles = makeStyles( () => 
    createStyles({
        root : {
            display : 'flex',
            flexDirection : 'row'
        }
    })
);

export default useStyles;