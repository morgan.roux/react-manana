import { makeStyles, createStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    bigAvatar: {
      width: 60,
      height: 60,
      marginRight: 24,
    },
    userName: {
      fontSize: 16,
      fontWeight: 500,
    },
    commentDate: {
      textAlign: 'right',
    },
  })
);
