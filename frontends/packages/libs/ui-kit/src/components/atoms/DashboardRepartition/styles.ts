import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 8,
  },
  textCategorie: {
    fontFamily: 'Roboto',
    fontSize: 12,
    paddingRight: 16,
    paddingLeft: 16,
    fontWeight: 500,
  },
  textValeur: {
    fontFamily: 'Roboto',
    fontSize: 10,
    height: 16,
    backgroundColor: '#bdbdbd',
    minWidth: 16,
    textAlign: 'center',
    borderRadius: 2,
    paddingTop: 2,
  },
}));

export default useStyles;
