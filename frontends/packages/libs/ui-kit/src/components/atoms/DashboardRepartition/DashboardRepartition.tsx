import React, { FC } from 'react';

import { Chart, ChartLegend, ChartSeries, ChartSeriesItem, ChartSeriesLabels } from '@progress/kendo-react-charts';
import { useApplicationContext } from '@lib/common';
import { Box } from '@material-ui/core';
import useStyles from './styles';

export interface SeriesItem {
  categorie: string;
  valeur: number;
  couleur?: string;
}
export interface DashboardRepartitionProps {
  series: SeriesItem[];
  graphType: 'pie' | 'donut';
  showProportion: boolean;
  withLabel?: boolean;
}

const DashboardRepartition: FC<DashboardRepartitionProps> = ({
  series,
  graphType,
  showProportion,
  withLabel = false,
}) => {
  const { isMobile } = useApplicationContext();
  const classes = useStyles();
  const labelContent = (props: any) => {
    const total = series.reduce((accumulator, serie) => accumulator + serie.valeur, 0);
    const formatedNumber = total
      ? Number(props.dataItem.valeur / total).toLocaleString(undefined, {
          style: 'percent',
          minimumFractionDigits: 0,
        })
      : '';
    return formatedNumber;
  };
  return (
    <Box className={classes.root}>
      <Chart
        style={
          showProportion
            ? withLabel
              ? { height: 140, maxWidth: 160, minWidth: 160 }
              : { height: 140, minWidth: isMobile ? 320 : 350 }
            : undefined
        }
      >
        <ChartLegend position="right" orientation="vertical" visible={!withLabel} />
        <ChartSeries>
          <ChartSeriesItem
            type={graphType}
            data={series}
            field="valeur"
            colorField="couleur"
            categoryField="categorie"
            labels={{ visible: !showProportion, content: labelContent, position: 'outsideEnd' }}
          >
            {withLabel && (
              <ChartSeriesLabels font="bold 12px Roboto" color="#fff" background="none" content={labelContent} />
            )}
          </ChartSeriesItem>
        </ChartSeries>
      </Chart>
      {withLabel && (
        <Box>
          {series
            .filter((serie) => serie.valeur)
            .map((serie) => (
              <Box key={`serie${serie.categorie}`} display="flex" width="100%" paddingBottom={1}>
                <Box style={{ height: 16, width: 16, backgroundColor: serie.couleur }} />
                <Box display="flex" justifyContent="space-between" width="100%">
                  <span className={classes.textCategorie}>{serie.categorie}</span>
                  <span className={classes.textValeur}>{serie.valeur}</span>
                </Box>
              </Box>
            ))}
        </Box>
      )}
    </Box>
  );
};

export default DashboardRepartition;
