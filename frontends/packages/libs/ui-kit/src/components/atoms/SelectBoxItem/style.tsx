import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    photo: {
      maxHeight: 160,
      width: '100%',
    },
    choice: {
      flex: '1 1 300px',
      maxHeight: 230,
      margin: theme.spacing(0, 2, 4),
      borderRadius: 12,
      [theme.breakpoints.up('xs')]: {
        maxWidth: 255,
      },
      [theme.breakpoints.down('xs')]: {
        display: 'inline-block',
        width: 'calc(50% - 16px)',
        verticalAlign: 'top',
        height: 230,
      },
      '&>button': {
        width: '100%',
        height: '100%',
        padding: theme.spacing(2),
        textAlign: 'center',
      },
      '& img': {
        maxWidth: '100%',
        '@media(max-width:1200px)': {
          maxWidth: 250,
        },
        '@media(max-width:991px)': {
          maxWidth: 150,
        },
      },
      '& p': {
        marginTop: theme.spacing(2),
      },
      '@media(max-width:1200px)': {
        margin: theme.spacing(0, 1, 2),
      },
    },
  })
);

export default useStyles;
