import { Card, CardActionArea, Typography } from '@material-ui/core';
import React, { FC, MouseEvent, CSSProperties } from 'react';
import classnames from 'classnames';

import useStyles from './style';

export interface SelectBoxItemImageProps {
  src: string;
  alt: string;
}

export interface SelectBoxItemProps {
  image: SelectBoxItemImageProps;
  text: string | FC;
  style?: CSSProperties;
  className?: string;
  onClick: (event: MouseEvent) => void;
}

const SelectBoxItem: FC<SelectBoxItemProps> = ({ image, text, style, className, onClick }) => {
  const classes = useStyles();

  return (
    <Card className={classnames(classes.choice, className)} style={style} variant="outlined">
      <CardActionArea onClick={onClick}>
        <img className={classes.photo} src={image.src} alt={image.alt} />
        <Typography align="center">{text}</Typography>
      </CardActionArea>
    </Card>
  );
};

export default SelectBoxItem;
