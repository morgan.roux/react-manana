import React , { FC } from 'react';
import SelectBoxItem, { SelectBoxItemImageProps } from './SelectBoxItem';

export default {
    title: 'Design system|Atoms/SelectBoxItem',
    parameters: {
        info: { inline: true }
    }
};

const image : SelectBoxItemImageProps = {
    src : 'images/atoms.png',
    alt : 'Photo du jour'
};

const handleClick = ( name : string ) : void => {
    alert(name);
};

const component : FC = () => {
    return (
        <div>
            Bonjour
        </div>
    );
};

export const withText = () => (
    <SelectBoxItem text='withText' onClick={handleClick.bind(null,'withText')} image={image} />
);

export const withComponent = () => (
    <SelectBoxItem text={component} onClick={handleClick.bind(null,"withComponent")} image={image} />
); 
