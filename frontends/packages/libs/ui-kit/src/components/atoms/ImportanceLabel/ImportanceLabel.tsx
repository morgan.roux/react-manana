import { Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { useStyles } from './styles';
export interface ImportanceInterface {
  id?: string;
  ordre?: number;
  libelle?: string;
  couleur?: string;
}

interface ImportanceLabelProps {
  importance: ImportanceInterface
}

const ImportanceLabel: FC<ImportanceLabelProps> = ({
  importance,
}) => {
  const classes = useStyles({});

  return importance ? (<Typography
    className={classes.title}
    style={{ color: importance.couleur || 'black' }}
  >
    {importance.libelle}
  </Typography>
  ) : null;
};

export default ImportanceLabel;
