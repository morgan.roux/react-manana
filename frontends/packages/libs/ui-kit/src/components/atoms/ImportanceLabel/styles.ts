import { createStyles } from '@material-ui/core';
import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles(() =>
  createStyles({
    title: {
      fontFamily: 'Roboto',
      marginLeft: 10,
      fontSize: '0.875rem',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      '-webkit-line-clamp': 1,
      '-webkit-box-orient': 'vertical',
      maxWidth: 280,
    },
  })
);
