import { createStyles, makeStyles } from "@material-ui/core";

const useStyles = makeStyles( () => 
    createStyles({
        root : {

        },

        item : {
            textDecoration : 'underline',
            color : 'blue',
            cursor : 'pointer'
        }
    })
);

export default useStyles;