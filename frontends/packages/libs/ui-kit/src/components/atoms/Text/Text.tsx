import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import React , { FC , MouseEvent , CSSProperties } from 'react';
import { FiberManualRecord } from '@material-ui/icons';
import useStyles from './style';

export interface TextProps {
    label : string,
    style? : CSSProperties,
    onClick? : ( event : MouseEvent ) => void;
}

const Text : FC<TextProps> = ({label,style,onClick}) => {
    const classes = useStyles();

    return (
        <ListItem button={true} onClick={onClick} className={classes.root} >
            <ListItemIcon>
                <FiberManualRecord />
            </ListItemIcon>
            <ListItemText primary={`${label}`} className={classes.item} style={style} />
        </ListItem>
    );
};

export default Text;