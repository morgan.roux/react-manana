import React, { ChangeEvent, FC, Fragment, ReactNode } from 'react';
import { Autocomplete as MUIAutocomplete } from '@material-ui/lab';
import { TextField, CircularProgress, Typography } from '@material-ui/core';
import { ArrowDropDown } from '@material-ui/icons';
import { nl2br } from '@lib/common';

export interface AutocompleteProps {
  name: string;
  options: any[];
  optionLabel: string;
  multiple?: boolean;
  startAdornment?: ReactNode;
  endAdornment?: ReactNode;
  loading?: boolean;
  popupIcon?: ReactNode;
  variant?: 'filled' | 'outlined' | 'standard';
  label?: string;
  disabled?: boolean;
  style?: any;
  placeholder?: string;
  noOptionsText?: string;
  required?: boolean;
  className?: string;
  handleChange: (name: string) => (event: ChangeEvent<any>, value: any) => void;
  handleInputChange?: (name: string) => (event: ChangeEvent<any>, value: string) => void;
  handleOpenAutocomplete?: () => void;
  handleEnterKeyPressed?: () => void;
  popupIndicatorOpen?: any;
  renderOption?: any;
  inputValue?: any;
  clearOnBlur?: boolean;
  clearOnEscape?: boolean;
  renderGroup?: any;
  groupBy?: any;
  PaperComponent?: any;
  PopperComponent?: any;
}

const Autocomplete: FC<AutocompleteProps> = ({
  multiple = false,
  name,
  handleChange,
  handleInputChange,
  options,
  optionLabel,
  startAdornment,
  endAdornment,
  loading = false,
  handleOpenAutocomplete,
  popupIcon = <ArrowDropDown />,
  variant = 'standard',
  label = '',
  disabled = false,
  style = null,
  placeholder,
  noOptionsText,
  required,
  className,
  popupIndicatorOpen,
  renderOption,
  inputValue,
  clearOnBlur,
  clearOnEscape,
  renderGroup,
  groupBy,
  PaperComponent,
  PopperComponent,
  handleEnterKeyPressed,
}) => {
  const render = (option: any) => {
    return (
      <React.Fragment>
        <Typography dangerouslySetInnerHTML={{ __html: nl2br((option && option[optionLabel]) || '') } as any} />
      </React.Fragment>
    );
  };
  return (
    <MUIAutocomplete
      className={className}
      inputValue={inputValue}
      style={style ? style : {}}
      disabled={disabled}
      multiple={multiple}
      onChange={handleChange(name)}
      PaperComponent={PaperComponent}
      PopperComponent={PopperComponent}
      onInputChange={handleInputChange ? handleInputChange(name) : undefined}
      options={options || []}
      // tslint:disable-next-line: jsx-no-lambda
      getOptionLabel={(opt: any) => {
        return (opt && opt[optionLabel]) || '';
      }}
      renderGroup={renderGroup}
      // tslint:disable-next-line: jsx-no-lambda
      renderOption={renderOption || render}
      // tslint:disable-next-line: jsx-no-lambda
      renderInput={(params: any) => (
        <TextField
          {...params}
          variant={variant}
          label={label}
          required={required}
          onKeyPress={(e: any) => {
            if (e.key === 'Enter') {
              handleEnterKeyPressed && handleEnterKeyPressed();
            }
          }}
          InputProps={{
            ...params.InputProps,
            startAdornment: (
              <Fragment>
                {startAdornment}
                {params.InputProps.startAdornment}
              </Fragment>
            ),
            endAdornment: (
              <Fragment>
                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                {endAdornment}
                {params.InputProps.endAdornment}
              </Fragment>
            ),
          }}
        />
      )}
      popupIcon={popupIcon}
      onOpen={handleOpenAutocomplete}
      loading={loading}
      loadingText="Chargement..."
      placeholder={placeholder ? placeholder : ''}
      noOptionsText={noOptionsText ? noOptionsText : 'Aucune option'}
      classes={{ popupIndicatorOpen }}
      clearOnBlur={clearOnBlur}
      clearOnEscape={clearOnEscape}
      groupBy={groupBy}
    />
  );
};

export default Autocomplete;
