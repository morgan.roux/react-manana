import { Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import useStyles from './style';

interface InfoListItemContent {
  label?: string;
  children?: any;
  value?: string;
  color?: string;
  icon?: any;
}

export interface InfoListItem {
  title?: string;
  actionBtns?: any[];
  content?: InfoListItemContent[];
  children?: any;
}

interface InfoListProps {
  info: InfoListItem;
}

const InfoList: FC<InfoListProps> = ({ info }) => {
  const classes = useStyles();

  return (
    <Box>
      {info.title && (
        <Box className={classes.titleContainer}>
          <Typography>{info.title}</Typography>
          {info.actionBtns?.map((btn) => btn)}
        </Box>
      )}
      <Box padding="12px 16px" bgcolor="#F8F8F8">
        {info.content?.map((item, index) => (
          <Box key={`infolist-${index}`} className={classes.itemContainer}>
            <Typography className={classes.itemLabel}>{item.label}</Typography>
            {item.value && (
              <Typography
                style={{ color: item.color ? item.color : '#424242' }}
                className={classes.itemValue}
              >
                {item.icon}
                {item.value}
              </Typography>
            )}
            {item.children}
          </Box>
        ))}
        {info.children}
      </Box>
    </Box>
  );
};

export default InfoList;
