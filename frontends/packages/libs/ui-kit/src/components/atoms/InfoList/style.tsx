import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    titleContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      '& .MuiTypography-root': {
        fontFamily: 'Roboto',
        fontSize: 16,
        fontWeight: 'bold',
      },
    },
    itemContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      marginBottom: 16,
    },
    itemLabel: {
      fontFamily: 'Roboto',
      fontSize: 12,
      color: '#9E9E9E',
    },
    itemValue: {
      fontFamily: 'Roboto',
      fontSize: 12,
      display: 'flex',
      alignItems: 'center',
    },
  })
);

export default useStyles;
