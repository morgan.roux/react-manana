import { Box } from '@material-ui/core';
import React, { FC, MouseEvent } from 'react';
import { NewCustomButton } from '../CustomButton';
import useStyles from './styles';

export interface FormButtonsProps {
  disableConfirm?: boolean;
  disableCancel?: boolean;
  onClickConfirm?: (event: MouseEvent<any>) => void;
  onClickCancel?: (event: MouseEvent<any>) => void;
}

const FormButtons: FC<FormButtonsProps> = ({ onClickCancel, onClickConfirm, disableConfirm, disableCancel }) => {
  const classes = useStyles({});

  return (
    <Box className={classes.button}>
      <NewCustomButton
        theme="transparent"
        className={`${classes.btn} ${classes.cancelBtn}`}
        onClick={(event) => {
          event.preventDefault();
          event.stopPropagation();

          if (onClickCancel) {
            onClickCancel(event);
          }
        }}
        disabled={disableCancel}
      >
        Annuler
      </NewCustomButton>
      <NewCustomButton
        className={classes.btn}
        // tslint:disable-next-line: jsx-no-lambda
        onClick={(event) => {
          event.preventDefault();
          event.stopPropagation();
          if (onClickConfirm) onClickConfirm(event);
        }}
        disabled={disableConfirm}
      >
        Valider
      </NewCustomButton>
    </Box>
  );
};

export default FormButtons;
