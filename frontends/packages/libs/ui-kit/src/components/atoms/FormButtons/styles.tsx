import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    btn: {
      fontWeight: 'bold',
      margin: 5,
      height: 50,
    },
    cancelBtn: {
      background: '#e6e6e6',
    },
    closeButton: {
      color: 'inherit',
    },
    button: {
      width: '100%',
      display: 'flex',
      justifyContent: 'flex-end',
    },
  })
);

export default useStyles;
