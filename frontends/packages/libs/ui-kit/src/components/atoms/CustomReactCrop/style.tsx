import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    reactCrop: {
      // width: '495px !important',
      // height: '400px !important ',
      // minWidth: '350px !important',
      // minHeight: '250px !important',
      '& .ReactCrop__crop-selection': {
        border: '0px !important',
      },
    },
  })
);

export default useStyles;
