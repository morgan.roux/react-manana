import React, { FC } from 'react';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { CustomAvatar } from '../CustomAvatar';
import { uniqBy } from 'lodash';

interface Fichier {
  publicUrl: string;
}

interface User {
  id: string;
  userName?: string; // Prisma
  fullName?: string; // Gateway

  userPhoto?: {
    fichier: Fichier;
  };

  photo?: Fichier;
}

interface CustomAvatarGroupProps {
  users: User[];
  max?: number;
  sizes?: string | undefined;
  onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void
}

const CustomAvatarGroup: FC<CustomAvatarGroupProps & RouteComponentProps> = ({ users = [], sizes, max = 5, onClick }) => {
  if (users.length === 0) {
    return null;
  }
  return (
    <AvatarGroup max={max} onClick={onClick}>
      {uniqBy(users as any, 'id').map((u: any) => {
        const url = (u.userPhoto?.fichier || u.photo)?.publicUrl;
        return <CustomAvatar key={u.id} width={sizes} url={url} name={u.userName || u.fullName || ''} />;
      })}
    </AvatarGroup>
  );
};

export default withRouter(CustomAvatarGroup);
