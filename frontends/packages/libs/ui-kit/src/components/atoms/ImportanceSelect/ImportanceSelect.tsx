import React, { FC, useEffect } from 'react';
import { createStyles, makeStyles } from '@material-ui/core';
import { MenuItem, FormControl, Select } from '@material-ui/core';
interface Importance {
  id?: string;
  code?: string;
  libelle?: string;
}
interface ImportanceSelectProps {
  importance?: Importance[];
  onChangeImportance?: (importance?: string) => void;
  currentValue?: Importance;
  displaySelectIndicator?: boolean;
  styles?: React.CSSProperties;
  useDefaultStyle?: boolean;
}

const useStyles = makeStyles(() =>
  createStyles({
    rootSelect: {
      padding: '5px !important',
    },
    noIcon: {
      display: 'none',
    },
    formControl: {
      '&>div': {
        borderRadius: 3,
      },
      '&>div:before': {
        display: 'none',
      },
      '& *:after': {
        display: 'none',
      },
    },
  })
);
export const ImportanceSelect: FC<ImportanceSelectProps> = ({
  importance,
  onChangeImportance,
  currentValue,
  displaySelectIndicator = false,
  styles,
  useDefaultStyle = true,
}) => {
  const classes = useStyles();
  const [style, setStyle] = React.useState<React.CSSProperties>({});
  const [importanceState, setImportanceState] = React.useState<string>('Pas important');

  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setImportanceState(event.target.value as string);
    onChangeImportance && onChangeImportance(event.target.value as string);
    console.log(importanceState);
  };
  useEffect(() => {
    let cleanup = false;
    setImportanceState(currentValue?.id || '1');
    return () => {
      cleanup = true;
    };
  }, []);

  const importanceDefault = [
    { id: '1', code: '1', libelle: 'Pas important', background: '#fbb105', color: '#fff' },
    { id: '2', code: '2', libelle: 'Important', background: '#f05365', color: '#fff' },
  ];
  useEffect(() => {
    let cleanup = false;
    if (useDefaultStyle) {
      importanceState === '1'
        ? setStyle({
            background: importanceDefault[0].background,
            color: importanceDefault[0].color,
          })
        : importanceState === '2'
        ? setStyle({
            background: importanceDefault[1].background,
            color: importanceDefault[1].color,
          })
        : undefined;
    }
    return () => {
      cleanup = true;
    };
  }, [importanceState]);

  return (
    <div>
      <FormControl className={classes.formControl}>
        <Select
          labelId="importance-labelid"
          id="importance-select"
          value={importanceState}
          onChange={handleChange}
          classes={{ root: classes.rootSelect, icon: displaySelectIndicator ? '' : classes.noIcon }}
          inputProps={{}}
          variant="filled"
          style={styles || style}
          MenuProps={{
            MenuListProps: {
              style: {
                padding: 0,
              },
            },
          }}
        >
          {useDefaultStyle
            ? importanceDefault?.map((item, _index) => {
                return (
                  <MenuItem
                    key={`select-${item.id}`}
                    value={item.id}
                    style={{ background: item.background, color: item.color }}
                  >
                    {item.libelle}
                  </MenuItem>
                );
              })
            : (importance?.length || 0) > 0 &&
              importance?.map((item, _index) => {
                return (
                  <MenuItem key={`select-${item.id}`} value={item.id}>
                    {item.libelle}
                  </MenuItem>
                );
              })}
        </Select>
      </FormControl>
    </div>
  );
};
