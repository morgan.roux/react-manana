import React, { ChangeEvent, FC } from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

export interface CustomEditorTextProps {
  value: string;
  onChange?: (content: string) => void;
  onChangeWithName?: (event: ChangeEvent<HTMLInputElement>) => void;
  theme?: string;
  modules?: object;
  formats?: string[];
  placeholder?: string;
  style?: object;
  className?: string;
  name?: string;
  disabled?: boolean;
}

const CustomEditor: FC<CustomEditorTextProps> = ({
  value,
  onChange,
  theme,
  modules,
  formats,
  placeholder,
  style,
  className,
  name,
  onChangeWithName,
  disabled,
}: CustomEditorTextProps) => {
  const handleChange = (content: string) => {
    if (onChangeWithName && name) {
      onChangeWithName({ target: { name, value: content } } as ChangeEvent<HTMLInputElement>);
    }
    if (onChange) {
      onChange(content);
    }
  };

  return (
    <ReactQuill
      readOnly={disabled}
      value={value}
      onChange={handleChange}
      theme={theme}
      modules={modules}
      formats={formats}
      placeholder={placeholder}
      style={style}
      className={className}
    />
  );
};

const EDITOR_MODULES = {
  toolbar: [
    [{ size: [] }],
    ['bold', 'italic', 'underline'],
    [{ list: 'ordered' }, { list: 'bullet' }],
    ['link'],
    ['clean'],
  ],
};

const EDITOR_FORMATS = [
  'header',
  'font',
  'size',
  'bold',
  'italic',
  'underline',
  'strike',
  'blockquote',
  'list',
  'bullet',
  'indent',
  'link',
  'image',
  'video',
];

CustomEditor.defaultProps = {
  value: '',
  theme: 'snow',
  formats: EDITOR_FORMATS,
  modules: EDITOR_MODULES,
  placeholder: '',
};

export default CustomEditor;
