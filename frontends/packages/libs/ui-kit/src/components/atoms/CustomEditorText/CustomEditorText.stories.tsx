import React, { useState } from 'react';

import { CustomEditorText } from './index';

export default {
  title: 'Design system|Atoms/CustomEditorText',
  parameters: {
    info: { inline: true },
  },
};

export const CustomEditorTextStory = () => {
  const [value, setValue] = useState<string>('');

  const handleChange = (contents: string) => {
    setValue(contents);
  };

  return <CustomEditorText value={value} onChange={handleChange} theme="snow" placeholder="Ajouter du commentaire" />;
};
