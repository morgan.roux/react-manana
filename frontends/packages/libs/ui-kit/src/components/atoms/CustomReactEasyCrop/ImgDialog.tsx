import React, { useState, FC } from 'react';
import {
  Slide,
  makeStyles,
  Theme,
  createStyles,
  Dialog,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

interface ImgDialogProps {
  img: any;
  onClose: () => void;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      position: 'relative',
    },
    flex: {
      flex: 1,
    },
    imgContainer: {
      position: 'relative',
      flex: 1,
      padding: 16,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    img: {
      maxWidth: '100%',
      maxHeight: '100%',
    },
  })
);

const Transition = (props) => {
  return <Slide direction="up" {...props} />;
};

const ImgDialog: FC<ImgDialogProps> = ({ img, onClose }) => {
  const [open, setOpen] = useState<boolean>(false);
  const classes = useStyles({});

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Dialog fullScreen={true} open={!!img} onClose={onClose} TransitionComponent={Transition}>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <IconButton color="inherit" onClick={onClose} aria-label="Close">
            <CloseIcon />
          </IconButton>
          <Typography variant="h6" color="inherit" className={classes.flex}>
            Cropped image
          </Typography>
        </Toolbar>
      </AppBar>
      <div className={classes.imgContainer}>
        <img src={img} alt="Cropped" className={classes.img} />
      </div>
    </Dialog>
  );
};

export default ImgDialog;
