import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    cropContainer: {
      position: 'relative',
      width: '100%',
      minWidth: 400,
      minHeight: '350px !important',
      // background: '#333',
      [theme.breakpoints.up('sm')]: {
        height: 400,
      },
    },
    cropButton: {
      flexShrink: 0,
      marginLeft: 16,
    },
    controls: {
      padding: 16,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'stretch',
      [theme.breakpoints.up('sm')]: {
        flexDirection: 'column',
        alignItems: 'center',
      },
    },
    sliderContainer: {
      display: 'flex',
      flex: '1',
      alignItems: 'center',
      width: '100%',
      '& .MuiSlider-root': {
        color: theme.palette.secondary.main,
      },
      '& .MuiSvgIcon-root': {
        color: theme.palette.common.black,
      },
      '& .MuiSlider-rail, & .MuiSlider-track': {
        height: 6,
        opacity: 1,
        borderRadius: 3,
      },
      '& .MuiSlider-thumb': {
        width: 20,
        height: 20,
        top: 20,
      },
    },
    sliderLabel: {
      [theme.breakpoints.down('xs')]: {
        minWidth: 65,
      },
    },
    slider: {
      padding: '22px 0px',
      marginLeft: 16,
      [theme.breakpoints.up('sm')]: {
        flexDirection: 'row',
        alignItems: 'center',
        margin: '0 16px',
      },
    },

    cropArea: {
      height: '225px !important',
      width: '225px !important',
    },
  })
);
