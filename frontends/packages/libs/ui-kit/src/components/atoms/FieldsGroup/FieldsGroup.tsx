import React, { FC, CSSProperties } from 'react';
import { useStyles } from './style';
import { List, ListItem, ListItemSecondaryAction, IconButton, Button, TextField } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
export interface FieldGroupValue {
  name: string;
  ordre: number;
  value: string;
  deletable: boolean;
}

export interface FieldsGroupProps {
  style?: CSSProperties;
  multiline?: boolean;
  addFieldLabel: string;
  placeholder: string;
  className?: string;
  values: FieldGroupValue[];
  onChange: (newValues: FieldGroupValue[]) => void;
}

const FieldsGroup: FC<FieldsGroupProps> = ({
  onChange,
  className,
  values,
  addFieldLabel,
  placeholder,
  multiline = true,
  style,
}) => {
  const classes = useStyles();

  const reorder = (valuesToReorder: FieldGroupValue[]) => {
    return valuesToReorder.map((element, index) => ({ ...element, ordre: index + 1 }));
  };

  const handleChangeValueAt = (index: number, value: string) => {
    values[index].value = value;
    onChange(reorder(values));
  };

  const handleRemoveAt = (index: number) => {
    delete values[index];
    onChange(reorder(values));
  };

  const handleAdd = () => {
    values.push({
      name: '',
      ordre: values.length,
      value: '',
      deletable: true,
    });

    onChange(reorder(values));
  };

  return (
    <List className={classes.root} style={style} disablePadding>
      {values.map(({ name, value }, index) => {
        return (
          <ListItem key={name || index} disableGutters dense>
            <TextField
              variant="outlined"
              value={value}
              placeholder={placeholder}
              name={name || `field-${index}`}
              onChange={(event) => handleChangeValueAt(index, event.target.value)}
              multiline={multiline}
              rows={2}
              style={{ marginBottom: 0, resize: 'both' }}
              fullWidth
              className={className || ''}
            />
            <ListItemSecondaryAction>
              <IconButton size="small" aria-label="delete" onClick={() => handleRemoveAt(index)}>
                <CloseIcon />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        );
      })}

      <Button
        variant="outlined"
        className={classes.addButton}
        onClick={() => handleAdd()}
        style={{ textTransform: 'initial', marginTop: 5 }}
        startIcon={<AddIcon />}
        fullWidth
      >
        {addFieldLabel}
      </Button>
    </List>
  );
};

export default FieldsGroup;
