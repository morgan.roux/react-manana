import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      inlineSize: '-webkit-fill-available',
      padding: 15,
    },
    addButton: {
      padding: theme.spacing(1.5),
      border: '1px dashed #1D1D1D',
      borderRadius: 6,
    },
  })
);
