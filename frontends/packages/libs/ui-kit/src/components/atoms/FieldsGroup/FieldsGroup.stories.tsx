import React, { useState } from 'react';
import FieldsGroup, { FieldGroupValue } from './FieldsGroup';

export default {
  title: 'Design system|Atoms/FieldsGroup',
  parameters: {
    info: { inline: true },
  },
};

export const fieldGroup = () => {
  const [values, setValues] = useState<FieldGroupValue[]>([]);

  return <FieldsGroup placeholder="votre text" addFieldLabel="Add checklist" values={values} onChange={setValues} />;
};
