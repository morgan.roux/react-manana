import React from 'react';

import Stepper from './Stepper';

export default {
  title: 'Design system|Atoms/Stepper',
  parameters: {
    info: { inline: true },
  },
};

export const StepperStory = () => (
  <Stepper
    disableNextBtn={false}
    steps={[
      {
        title: 'Step 1',
        content: <div>Step one</div>,
      },
      {
        title: 'Step 2',
        content: <div>Step 2</div>,
      },
    ]}
  />
);
