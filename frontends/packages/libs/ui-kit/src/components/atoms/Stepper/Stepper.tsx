import React, { FC, useEffect, useState, ReactNode, CSSProperties } from 'react';
import useStyles from './styles';
import { Stepper as MUIStepper, StepLabel, Step as MUIStep, Box } from '@material-ui/core';
import classnames from 'classnames';
import { NewCustomButton } from '../CustomButton';

export interface Step {
  title: string;
  content: JSX.Element;
  pageTitle?: string;
}

export interface StepperProps {
  contentStyle?: CSSProperties;
  steps: Step[];
  disableNextBtn: boolean;
  disablePrevBtn?: boolean;
  disableValidateBtn?: boolean;
  fullWidth?: boolean;
  loading?: boolean;
  history?: any;
  onStepChange?: (step: number) => void;
  children?: ReactNode;
  onValidate?: () => void;
  onCancel?: () => void;
  onClickNext?: () => void;
  onClickPrev?: () => void;
}

const Stepper: FC<StepperProps> = ({
  contentStyle,
  steps,
  loading,
  onCancel,
  disableNextBtn,
  disablePrevBtn,
  fullWidth,
  history,
  onClickNext,
  onClickPrev,
  onStepChange,
  children,
  disableValidateBtn,
  onValidate: onConfirm,
}) => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);

  const finished = activeStep === steps.length - 1;

  const handleNext = () => {
    if (finished) {
      if (onConfirm) {
        onConfirm();
      }
    } else {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
      if (onStepChange) onStepChange(activeStep + 1);
      if (onClickNext) onClickNext();
    }
  };

  const handleBack = () => {
    if (activeStep > 0) {
      setActiveStep((prevActiveStep) => prevActiveStep - 1);
      if (onStepChange) onStepChange(activeStep - 1);
      if (onClickPrev) onClickPrev();
    }
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const fromPath = history?.location?.state?.from;
  useEffect(() => {
    if (fromPath) {
      handleReset();
    }
  }, [fromPath]);

  return (
    <div className={fullWidth ? classnames(classes.root, classes.fullWidth) : classes.root}>
      <div className={classes.content} style={contentStyle}>
        <MUIStepper activeStep={activeStep} className={classes.stepperRoot} alternativeLabel>
          {steps.map((step) => (
            <MUIStep key={step.title}>
              <StepLabel>{step.title}</StepLabel>
            </MUIStep>
          ))}
        </MUIStepper>
        {steps && steps.length > 0 && steps[activeStep].content}
        {children}
        <Box className={classes.btnActionsContainer}>
          <NewCustomButton
            className={classnames(classes.btnAction, classes.cancelAndReturn)}
            disabled={disablePrevBtn || loading}
            theme="transparent"
            onClick={activeStep === 0 && onCancel ? onCancel : handleBack}
          >
            {activeStep === 0 ? 'Annuler' : 'Précédent'}
          </NewCustomButton>
          {finished ? (
            <NewCustomButton
              className={classes.btnAction}
              onClick={handleNext}
              disabled={disableValidateBtn || loading}
            >
              Valider
            </NewCustomButton>
          ) : (
            <NewCustomButton className={classes.btnAction} onClick={handleNext} disabled={disableNextBtn || loading}>
              Suivant
            </NewCustomButton>
          )}
        </Box>
      </div>
    </div>
  );
};

// activeStep === steps.length - 1

export default Stepper;
