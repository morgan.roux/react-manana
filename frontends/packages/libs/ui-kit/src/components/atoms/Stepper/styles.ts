import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      height: '100%',
      minHeight: 'calc(100vh - 86px)',
      width: '100%',
      '& .MuiPaper-root': {
        // boxShadow: 'none !important',
      },
      '& .MuiStepIcon-root': {
        color: '#EFEFEF',
        '& .MuiStepIcon-text': {
          fill: '#878787',
        },
      },
      '& .MuiStepIcon-root.MuiStepIcon-active, & .MuiStepIcon-root.MuiStepIcon-completed': {
        color: theme.palette.secondary.main,
        '& .MuiStepIcon-text': {
          fill: '#ffffff',
        },
      },
      '& .MuiStepLabel-label, & .MuiStepLabel-label.MuiStepLabel-active': {
        color: '#878787',
        fontSize: 12,
      },
      '& button': {
        fontWeight: 'bold',
      },
    },
    fullWidth: {
      '& .MuiStepper-root': {
        width: '100%',
        maxWidth: '100%',
      },
    },
    title: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 20,
    },
    content: {
      margin: '10px 0px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    btnActionsContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end',
      width: '100%',
      '& button:nth-child(1)': {
        marginRight: theme.spacing(3),
      },
    },
    cancelAndReturn: {
      background: '#e0e0e0',
    },
    btnAction: {
      height: 50,
      minWidth: 100,
    },
    stepperRoot: {
      // marginTop: 75,
      boxShadow: 'unset !important',
      width: '100%',
      '@media (max-width: 825px)': {
        minWidth: '80%',
      },
      '@media (max-width: 450px)': {
        minWidth: '100%',
      },
    },
  })
);

export default useStyles;
