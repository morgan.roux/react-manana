import React, { FC, MouseEvent, ReactNode } from 'react';
import useStyles from './style';
import classnames from 'classnames';
import { Box, Card, CardActionArea, Typography } from '@material-ui/core';
import { MoreOptions, MoreOptionsItem } from '../MoreOptionsComponent';

export const getObjectByPath = (obj: any, path: any) => {
  const splitedPath = path.split('.');
  try {
    for (let i = 0, path = splitedPath, len = path.length; i < len; i++) {
      obj = obj[path[i]];
    }
    return obj;
  } catch (e) {
    return null;
  }
};

export interface CardThemeProps {
  icon?: ReactNode;
  title?: string | ReactNode;
  subTitle?: string | ReactNode;
  createDate?: string;
  updateDate?: string;
  currentId?: any;
  listItemFields?: any;
  item?: any;
  setCurrentId?: (value: any) => void;
  moreOptionsItem?: any;
  moreOptions?: MoreOptionsItem[];
  littleComment?: string | ReactNode;
  active?: boolean;
  onClick?: (event: MouseEvent) => void;
  applyMinHeight?: boolean;
}

const CardTheme: FC<CardThemeProps> = ({
  item,
  active,
  currentId,
  setCurrentId,
  listItemFields,
  title,
  subTitle,
  createDate,
  updateDate,
  moreOptionsItem,
  moreOptions,
  littleComment,
  applyMinHeight = true,
  onClick,
}) => {
  const classes = useStyles();

  const moreOptionClick = (e: any) => {
    e.stopPropagation();
  };

  // TODO: Add handler in props
  const handleClick = (event: MouseEvent) => {
    if (setCurrentId && listItemFields.url) {
      if (item && item.id) {
        window.history.pushState(null, '', `#${listItemFields.url}/${item.id}`);
      } else {
        window.history.pushState(null, '', `#${listItemFields.url}`);
        setCurrentId(null);
      }

      setCurrentId(item);
    }

    if (onClick) {
      onClick(event);
    }
  };

  return (
    <Card
      elevation={0}
      className={classnames(
        classes.root,
        (currentId && item && currentId.id === item.id) || active ? classes.active : classes.seen
      )}
    >
      <CardActionArea
        onClick={handleClick}
        className={applyMinHeight ? classnames(classes.button, classes.buttonMinHeigt) : classes.button}
      >
        <Box width="100%" display="flex" alignItems="center" justifyContent="space-between">
          <Typography style={{ fontWeight: 'bold' }}>{title}</Typography>
          {subTitle && <Typography variant="subtitle2">{subTitle}</Typography>}
          {moreOptions && <MoreOptions items={moreOptions} />}
          {moreOptionsItem && <Typography onClick={moreOptionClick}>{moreOptionsItem}</Typography>}
        </Box>

        {littleComment ? (
          typeof littleComment === 'string' ? (
            <span style={{ marginTop: 'auto', color: '#9E9E9E' }}>{littleComment}</span>
          ) : (
            { littleComment }
          )
        ) : (
          <Box className={classes.details}>
            <Typography variant="caption" color="textSecondary">
              Créée le&nbsp;:&nbsp;
              <span style={{ fontWeight: 'bold', marginRight: 10 }}>{createDate}</span>
            </Typography>
            <Typography variant="caption" color="textSecondary">
              Modifiée le&nbsp;:&nbsp;<span style={{ fontWeight: 'bold' }}>{updateDate}</span>
            </Typography>
          </Box>
        )}
      </CardActionArea>
    </Card>
  );
};

export default CardTheme;
