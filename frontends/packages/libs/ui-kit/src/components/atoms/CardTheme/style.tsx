import { makeStyles, Theme, createStyles, lighten } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      position: 'relative',
      width: 'calc(100% - 4px)',
      margin: 'auto',
      borderBottom: '1px solid #E5E5E5',
      borderRadius: 6,
    },

    buttonMinHeigt: {
      minHeight: 115,
    },
    button: {
      padding: theme.spacing(2),
      flexDirection: 'column',
      alignItems: 'flex-start',
    },
    active: {
      borderWidth: 2,
      borderRadius: 6,
      borderStyle: 'solid',
      borderColor: theme.palette.primary.main,
      color: theme.palette.common.black,
    },
    seen: {
      background: theme.palette.common.white,
      color: theme.palette.common.black,
    },
    notSeen: {
      background: lighten(theme.palette.secondary.main, 0.3),
      color: theme.palette.common.black,
    },
    details: {
      display: 'flex',
      alignItems: 'center',
      marginTop: 'auto',
    },
  })
);

export default useStyles;
