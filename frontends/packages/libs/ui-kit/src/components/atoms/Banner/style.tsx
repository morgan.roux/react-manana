import { createStyles, lighten, makeStyles } from '@material-ui/core';
import { Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { marginBottom: 12 },
    filledBanner: {
      background: lighten(theme.palette.primary.main, 0.1),
      color: '#FFFFFF',
    },
    content: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
      height: '100%',
      alignItems: 'center',
      cursor: 'pointer',
    },
    divider: {
      marginBottom: 50,
    },
    title: {
      fontFamily: 'Montserrat',
      fontSize: 20,
      fontWeight: 'bold',
      color: theme.palette.primary.main,
      marginLeft: 15,
    },
    filledTitle: {
      color: '#FFFFFF',
      '& .MuiBox-root': {
        marginBottom: 0,
      },
    },
  })
);

export default useStyles;
