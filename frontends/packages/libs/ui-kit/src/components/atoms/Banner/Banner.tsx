import React, { FC, MouseEvent, CSSProperties } from 'react';
import useStyles from './style';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Box, Typography, IconButton, Divider } from '@material-ui/core';
import classNames from 'classnames';

export interface BannerProps {
  title: string | React.ReactNode;
  titleStyle?: CSSProperties;
  style?: CSSProperties;
  titleClassName?: string;
  withDivider?: boolean;
  filled?: boolean;
  titleColor?: 'inherit' | 'initial' | 'primary' | 'secondary' | 'textPrimary' | 'textSecondary' | 'error';
  onClick: (event: MouseEvent) => void;
}

const Banner: FC<BannerProps> = ({
  title,
  style,
  titleColor = 'primary',
  withDivider = true,
  filled = false,
  titleStyle,
  onClick,
  titleClassName,
}) => {
  const classes = useStyles();

  return (
    <Box className={classNames(classes.root, filled ? classes.filledBanner : undefined)} style={style}>
      <Box className={classes.content} onClick={onClick}>
        <IconButton className={filled ? classes.filledTitle : undefined}>
          <ArrowBackIcon />
        </IconButton>
        <Typography
          className={classNames(classes.title, filled ? classes.filledTitle : undefined, titleClassName)}
          style={titleStyle}
          color={titleColor}
        >
          {title}
        </Typography>
      </Box>
      {withDivider && <Divider />}
    </Box>
  );
};

export default Banner;
