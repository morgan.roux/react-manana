import { useApplicationContext } from '@lib/common';
import { Popover, Typography } from '@material-ui/core';
import { Box, Button, Checkbox, FormControlLabel, List, ListItem, ListItemText } from '@material-ui/core';
import { KeyboardDatePickerProps } from '@material-ui/pickers';
import { ParsableDate } from '@material-ui/pickers/constants/prop-types';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import React, { FC, ReactNode, ReactElement, MouseEvent, useState, ChangeEvent, useEffect } from 'react';
import { CustomDatePicker } from '../CustomDateTimePicker';
import useStyles from './styles';

interface CustomSelectMenuProps {
  buttonLabel?: string;
  iconFilter?: ReactNode;
  radio?: ReactElement;
  titreMenu?: ReactElement;
  iconMenu?: ReactNode;
  className?: string;
  withCheckbox?: boolean;
  checkboxPlacement?: 'start' | 'end';
  listFilters?: any[];
  handleClickItem?: (id?: any) => void;
  whiteBtn?: boolean;
  type?: 'date';
  onChangeDatePickers?: (date?: any, value?: string | null) => void;
  valueDatePickers?: ParsableDate;
  propsDatePickers?: KeyboardDatePickerProps;
  currentSelectedData?: any;
}

export const CustomSelectMenu: FC<CustomSelectMenuProps> = ({
  buttonLabel,
  iconFilter,
  className,
  withCheckbox,
  checkboxPlacement,
  listFilters,
  handleClickItem,
  whiteBtn,
  type,
  onChangeDatePickers,
  valueDatePickers,
  currentSelectedData,
  ...propsDatePickers
}) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
  const [state, setState] = useState<any>({});
  const [selectedValue, setSelectedValue] = useState<any | undefined>();
  const handleMenuClick = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleSwitchChange = (event: ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };
  const onClickItem = (_event, id?: string, _name?: string, item?: any) => {
    handleClickItem && handleClickItem(id);
    setSelectedValue(item);
    setAnchorEl(null);
  };
  const { isMobile } = useApplicationContext();
  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  const handleChangeDatePichers = (date: MaterialUiPickersDate, value?: string | null | undefined) => {
    onChangeDatePickers && onChangeDatePickers(date, value);
  };

  useEffect(() => {
    if (currentSelectedData) {
      setSelectedValue(currentSelectedData || currentSelectedData);
    }
  }, []);

  return (
    <>
      {isMobile ? (
        <Box>
          <Typography style={{ fontWeight: 700 }}>{buttonLabel}</Typography>
          {type && type === 'date' ? (
            <>
              <Box p={2} maxWidth="200px">
                <CustomDatePicker
                  {...propsDatePickers}
                  label="Date"
                  onChange={handleChangeDatePichers}
                  value={valueDatePickers}
                />
              </Box>
            </>
          ) : (
            <List style={{ minWidth: 200 }}>
              {listFilters &&
                listFilters.length > 0 &&
                listFilters.map((item, _index) => (
                  <ListItem button className={classes.listMobile}>
                    <ListItemText>
                      {/* {withCheckbox ? ( */}
                      <FormControlLabel
                        label={item.name || item.libelle}
                        control={
                          <Checkbox
                            name={item.id}
                            onChange={(event) => onClickItem(event, item.id, item.name || item.libelle, item)}
                          />
                        }
                        labelPlacement={checkboxPlacement}
                      />
                      {/* ) : (
                      <Typography>{item.name}</Typography>
                    )} */}
                    </ListItemText>
                  </ListItem>
                ))}
            </List>
          )}
        </Box>
      ) : (
        <Box className={className}>
          <Button
            onClick={handleMenuClick}
            startIcon={iconFilter}
            className={classes.button}
            style={
              whiteBtn
                ? { background: '#ffffff', color: selectedValue?.couleur || selectedValue?.color }
                : { color: selectedValue?.couleur || selectedValue?.color }
            }
          >
            {selectedValue && selectedValue !== 'Tous' ? selectedValue.libelle || selectedValue.name : buttonLabel}
          </Button>
          <Popover
            id={id}
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          >
            {type && type === 'date' ? (
              <Box p={2} maxWidth="200px">
                <CustomDatePicker
                  {...propsDatePickers}
                  label="Date"
                  onChange={handleChangeDatePichers}
                  value={valueDatePickers}
                />
              </Box>
            ) : (
              <List style={{ minWidth: 200 }}>
                {listFilters &&
                  listFilters.length > 0 &&
                  listFilters.map((item, _index) => (
                    <ListItem
                      button
                      onClick={(event) => onClickItem(event, item.id, item.name || item.libelle, item)}
                      classes={{ root: classes.listTypography }}
                    >
                      <ListItemText>
                        {withCheckbox ? (
                          <FormControlLabel
                            label={item.name || item.libelle}
                            control={<Checkbox name={item.id} checked={item.id} onChange={handleSwitchChange} />}
                            labelPlacement={checkboxPlacement}
                          />
                        ) : (
                          <Typography
                            // classes={{ root: classes.listTypography }}
                            style={{
                              borderRadius: 5,
                              background: item.background,
                              color: item.color || item.couleur,
                              padding: '0 10px',
                            }}
                          >
                            {item.name || item.libelle}
                          </Typography>
                        )}
                      </ListItemText>
                    </ListItem>
                  ))}
              </List>
            )}
          </Popover>
        </Box>
      )}
    </>
  );
};
