import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    button: {
      background: '#F2F2F2',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      borderRadius: '4px !important',
      padding: '11px 34px',
      textTransform: 'none',
    },
    listMobile: {
      paddingTop: 0,
      paddingBottom: 0,
      '& div': {
        paddingTop: 0,
        paddingBottom: 0,
        margin: 0,
      },
    },
    listTypography: {
        height: 30
    },
  })
);

export default useStyles;
