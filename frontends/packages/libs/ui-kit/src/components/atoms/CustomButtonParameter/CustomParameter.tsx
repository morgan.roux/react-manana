import { Box, Button, List, ListItem, ListItemIcon, ListItemText, Menu, Typography } from '@material-ui/core';
import React, { FC, ReactNode, ReactElement, MouseEvent, useState } from 'react';
import useStyles from './styles';

interface CustomParameterProps {
  titreButton?: string | undefined;
  iconFilter?: ReactNode;
  radio?: ReactElement;
  titreMenu?: ReactElement;
  iconMenu?: ReactNode;
  className?: string;
}

export const CustomParameter: FC<CustomParameterProps> = ({
  titreButton,
  iconFilter,
  iconMenu,
  radio,
  titreMenu,
  className,
}) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const handleMenuClick = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setAnchorEl(null);
  };

  return (
    <Box className={className}>
      <Box className={classes.button}>
        <Box>{iconFilter}</Box>
        <Button onClick={handleMenuClick}>
          <Typography>{titreButton}</Typography>
        </Button>
        <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleCloseMenu}>
          <List>
            <ListItem button onClick={handleCloseMenu}>
              <ListItemIcon>{iconMenu}</ListItemIcon>
              <ListItemText>
                {radio}
                {titreMenu}
              </ListItemText>
            </ListItem>
          </List>
        </Menu>
      </Box>
    </Box>
  );
};
