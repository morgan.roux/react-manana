import React from 'react';
import Theme from './Theme';
import { ITheme } from '../Exigence';
import { Delete, Edit, Add } from '@material-ui/icons';
import { MoreOptionsItem } from '../MoreOptionsComponent';
import { Box } from '@material-ui/core';

export default {
  title: 'Design system|Atoms/Theme',
  parameters: {
    info: { inline: true },
  },
};

const theme: ITheme = {
  id: '1',
  ordre: 1,
  nom: 'Ceci est un thème',
  nombreSousThemes: 3,
  sousThemes: [],
  createdAt: new Date('2020-8-9'),
  updatedAt: new Date('2020-7-8'),
};

const handleClick = (name: string): void => {
  alert(name);
};

const moreOptions: MoreOptionsItem[] = [
  {
    menuItemLabel: {
      title: 'Ajouter une exigence',
      icon: <Add />,
    },
    onClick: handleClick.bind(null, 'Ajouter une exigence'),
  },
  {
    menuItemLabel: {
      title: 'Ajouter sous-thème',
      icon: <Edit />,
    },
    onClick: handleClick.bind(null, 'Ajouter sous-thème'),
  },
  {
    menuItemLabel: {
      title: 'Supprimer sous-thème',
      icon: <Delete />,
    },
    onClick: handleClick.bind(null, 'Supprimer sous-thème'),
  },
];

export const WithBorderThemeStory = () => (
  <Theme theme={theme} textStyle={{ color: '#000', fontWeight: 'bold' }} moreOptions={moreOptions}>
    <div>With border</div>
  </Theme>
);

export const MenuThemeStory = () => (
  <Theme
    menu={true}
    theme={theme}
    checkList={true}
    textStyle={{ color: '#000', fontWeight: 'bold' }}
    moreOptions={moreOptions}
  >
    <div>No Border</div>
  </Theme>
);

export const WithSummarySubContentThemeStory = () => (
  <Theme
    menu={true}
    theme={theme}
    checkList={true}
    summarySubContent={<Box style={{ marginLeft: 20, marginBottom: 5 }}>Sub content</Box>}
    textStyle={{ color: '#000', fontWeight: 'bold' }}
    moreOptions={moreOptions}
  >
    <div>No Border</div>
  </Theme>
);
