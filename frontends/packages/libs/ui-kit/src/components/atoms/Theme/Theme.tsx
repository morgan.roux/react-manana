import React, { FC, CSSProperties, MouseEvent, useState, ReactNode } from 'react';
import { Box, List, IconButton, Accordion, AccordionSummary, AccordionDetails } from '@material-ui/core';
import useStyles from './style';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import classNames from 'classnames';

import { ITheme } from '../Exigence';
import { MoreOptions, MoreOptionsItem } from '../MoreOptionsComponent';

export interface ThemeProps {
  menu?: boolean;
  theme: Partial<Pick<ITheme, 'ordre' | 'nom'>>;
  summarySubContent?: ReactNode;
  style?: CSSProperties;
  textStyle?: CSSProperties;
  moreOptions?: MoreOptionsItem[];
  expended?: boolean;
  checkList?: boolean; // TODO : Find another name
  onClick?: (event: MouseEvent, expended: boolean) => void;
}

const Theme: FC<ThemeProps> = ({
  theme,
  style,
  expended,
  textStyle,
  moreOptions,
  onClick,
  children,
  // menu,
  checkList,
  summarySubContent = null,
}) => {
  const classes = useStyles();
  const [expandedState, setExpandedState] = useState<boolean>(expended || false);

  const handleClick = (event: MouseEvent) => {
    if (onClick) {
      onClick(event, !expandedState);
    }
    setExpandedState(!expandedState);
  };
  return (
    <Accordion
      expanded={expandedState}
      className={checkList ? classNames(classes.root, classes.rootChekclist) : classes.root}
      style={style}
    >
      <AccordionSummary
        className={checkList ? classNames(classes.summary, classes.summaryCheckList) : classes.summary}
        aria-controls="theme-content"
        onClick={handleClick}
      >
        <Box className={classes.icon}>
          <IconButton>
            <ExpandMoreIcon />
          </IconButton>
          <span style={textStyle}>{`${theme.ordre ? `${theme.ordre}. ` : ''}${theme.nom}`}</span>
          {summarySubContent}
        </Box>
        <Box>{moreOptions && <MoreOptions items={moreOptions} />}</Box>
      </AccordionSummary>
      <AccordionDetails className={classes.detail}>
        <List className={classes.list} component="nav" aria-label="main">
          {children}
        </List>
      </AccordionDetails>
    </Accordion>
  );
};

export default Theme;
