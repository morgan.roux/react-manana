import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',

      '&  .main-MuiAccordionSummary-content, &  .MuiAccordionSummary-content': {
        minHeight: 40,
        margin: 0,
        justifyContent: 'space-between',
        width: 'fit-content',
      },
    },
    menuRoot: {
      boxShadow: 'none !important',

      '&  .main-MuiAccordionSummary-content, &  .MuiAccordionSummary-content': {
        minHeight: 40,
        margin: 0,
        justifyContent: 'space-between',
        width: 'fit-content',
      },
    },
    rootChekclist: {
      boxShadow: 'none !important',
      width: '99%',
    },
    heading: {
      fontSize: theme?.typography?.pxToRem ? theme.typography.pxToRem(15) : `${15 / 16}rem`,
      fontWeight: 'normal',
    },
    summary: {},
    summaryCheckList: {
      borderBottom: '1px solid #E3E3E3',
    },

    detail: {
      width: '100%',
    },

    icon: {
      marginRight: 5,
      display: 'inline',
    },

    flexGrow: {
      flexGrow: 1,
    },
    list: {
      width: '100%',
      paddingTop: 0,
    },
  })
);

export default useStyles;
