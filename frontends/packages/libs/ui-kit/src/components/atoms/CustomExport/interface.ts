export interface CustomExportProps {
  onRequestDownloadPdf?: () => void;
  onRequestDownloadExcel?: () => void;
}
