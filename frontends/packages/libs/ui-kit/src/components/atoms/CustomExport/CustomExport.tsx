import { Box, Fade, IconButton, Menu, MenuItem, Typography } from '@material-ui/core';
import { SaveAlt } from '@material-ui/icons';
import React, { useState, MouseEvent, FC } from 'react';
import { CustomExportProps } from './interface';

const CustomExport: FC<CustomExportProps> = ({ onRequestDownloadPdf, onRequestDownloadExcel }) => {
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const open = Boolean(anchorEl);
  const handleClose = (): void => {
    setAnchorEl(null);
  };
  const handleOpen = (event: MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };
  return (
    <Box>
      <IconButton aria-controls="simple-menu-export" aria-haspopup="true" onClick={handleOpen}>
        <SaveAlt />
        <Typography>Exporter</Typography>
      </IconButton>
      <Menu
        id="simple-menu-export"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={(event: any) => {
          event.stopPropagation();
          handleClose();
        }}
        TransitionComponent={Fade}
      >
        <MenuItem
          onClick={(event) => {
            event.stopPropagation();
            onRequestDownloadPdf && onRequestDownloadPdf();
            handleClose();
          }}
        >
          <Typography variant="inherit">Export en pdf</Typography>
        </MenuItem>
        <MenuItem
          onClick={(event) => {
            event.stopPropagation();
            onRequestDownloadExcel && onRequestDownloadExcel();
            handleClose();
          }}
        >
          <Typography variant="inherit">Export en excel</Typography>
        </MenuItem>
      </Menu>
    </Box>
  );
};

export default CustomExport;
