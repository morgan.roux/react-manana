import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'center',
      '& .MuiButton-root': {
        width: 38,
        height: 38,
        minWidth: 'auto',
      },
    },
  })
);

export default useStyles;
