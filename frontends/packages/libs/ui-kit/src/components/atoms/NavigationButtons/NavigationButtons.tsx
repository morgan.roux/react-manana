import { Box, Button, ButtonGroup } from '@material-ui/core';
import { ChevronLeft, ChevronRight } from '@material-ui/icons';
import React, { FC } from 'react';
import useStyles from './style';

export interface NavigationButtonsProps {
  onLeftBtnClick?: () => void;
  onRightBtnClick?: () => void;
}

const NavigationButtons: FC<NavigationButtonsProps> = ({ onLeftBtnClick, onRightBtnClick, children }) => {
  const classes = useStyles();

  const buttonStyle = children ? { border: 0 } : undefined;

  return (
    <Box className={classes.root}>
      <ButtonGroup aria-label="outlined button group">
        <Button style={buttonStyle} onClick={onLeftBtnClick}>
          <ChevronLeft />
        </Button>
        {children}
        <Button style={buttonStyle} onClick={onRightBtnClick}>
          <ChevronRight />
        </Button>
      </ButtonGroup>
    </Box>
  );
};

export default NavigationButtons;
