import React, { FC } from 'react';
import { ListItem, ListItemAvatar, Avatar, ListItemText, Typography } from '@material-ui/core';
import ImgAvatarExemple from '../../../assets/images/atoms.png';
import { useStyles } from './styles';
import moment from 'moment';

export interface CommentItemProps {
  comment: any | null;
}

const CommentItem: FC<CommentItemProps> = ({ comment }) => {
  const classes = useStyles();

  const dateCreation =
    comment &&
    moment(comment.dateCreation)
      .subtract(30, 'seconds')
      .fromNow();

  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar alt="Remy Sharp" src={ comment.image || ImgAvatarExemple } className={classes.bigAvatar} />
      </ListItemAvatar>
      <ListItemText>
        <Typography className={classes.userName}>
          {comment && comment.user ? comment.user.userName : ''}
        </Typography>
        <Typography variant="body2">{comment ? comment.content : ''}</Typography>
        <div className={classes.commentDate}>
          <Typography variant="caption">{dateCreation ? dateCreation : ''}</Typography>
        </div>
      </ListItemText>
    </ListItem>
  );
};

export default CommentItem;
