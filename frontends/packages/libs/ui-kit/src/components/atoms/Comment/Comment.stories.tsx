import React from 'react';
import Comment from './Comment';

export default {
  title: 'Design system|Atoms/Comment',
  parameters: {
    info: { inline: true },
  },
};

export const commentStory = () => {
    return (
        <Comment 
            comment={{
                user : {
                    userName : 'Dalilah MUHAMMAD'
                },
                content : 'Responsable administratif et financier'
            }}
        />
    );
};
