import React, { FC } from 'react';
import { CustomSelect } from '../CustomSelect';


interface Importance {
  id: string
  ordre: number
  libelle: string
  couleur: string
}

interface ImportanceInputProps {
  name?: string;
  value: any;
  onChange: (importance: any) => void;
  options: Importance[]
  noMinHeight?: boolean;
  noLabel?: boolean
  noBorder?:boolean
}
const ImportanceInput: FC<ImportanceInputProps> = ({
  name,
  onChange,
  value,
  noMinHeight,
  options,
  noLabel,
  noBorder=false
}) => {

  const importances = ([...(options || [])]).sort((a, b) => a.ordre - b.ordre).map(et => ({
    ...et, nom: et.libelle, color: et?.couleur,
  }))


  const findImportance = (id: string) => id ? importances.find((importance: any) =>
    id === importance.id
  ) : undefined;

  const handleChange = (event: any) => {
    onChange(findImportance(event.target.value))
  }

  const selectedImportance = findImportance(value)

  return (
    <CustomSelect
      noMinheight={noMinHeight}
      label={noLabel ? undefined : "Importance"}
      noLabel={noLabel}
      list={importances}
      name={name || 'idUrgence'}
      onChange={handleChange}
      listId="id"
      index="nom"
      value={value}
      required={!noLabel}
      color={selectedImportance?.color}
      colorIndex="color"
      selectStyle={{ fontWeight: 'normal' }}
      noFieldset={noBorder}
    />
  );
};

export default ImportanceInput;
