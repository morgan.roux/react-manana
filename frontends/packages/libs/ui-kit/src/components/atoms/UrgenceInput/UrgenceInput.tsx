import React, { FC } from 'react';
import { CustomSelect } from '../CustomSelect';

interface Urgence {
  id: string
  code: string
  couleur: string
  libelle: string
}

interface UrgenceInputProps {
  name?: string;
  value: any;
  onChange: (importance: any) => void;
  useCode?: boolean; // Use code as value
  noMinHeight?: boolean;
  noLabel?: boolean
  noBorder?:boolean
  options: Urgence[]
}
const UrgenceInput: FC<UrgenceInputProps> = ({
  name,
  onChange,
  value,
  useCode = false,
  noLabel = false,
  noMinHeight,
  noBorder=false,
  options
}) => {
  const urgenceList = (options || []).map(urgence => ({
    ...urgence,
    color: urgence?.couleur,
    libelle: `${urgence?.code} : ${urgence?.libelle}`,
  }));

  const findUrgence = (code: string) => urgenceList?.find((urgence: any) =>
    value && useCode ? urgence?.code === code : urgence?.id === code,
  );

  const handleChange = (event: any) => {
    onChange(findUrgence(event.target.value))
  }

  const urgence: any = findUrgence(value)

  return (
    <CustomSelect
      noMinheight={noMinHeight}
      label={noLabel ? undefined : "Urgence"}
      noLabel={noLabel}
      list={urgenceList}
      name={name || 'idUrgence'}
      onChange={handleChange}
      listId={useCode ? 'code' : 'id'}
      index="libelle"
      value={value}
      required={!noLabel}
      color={urgence?.color}
      colorIndex="color"
      selectStyle={{ fontWeight: 'normal' }}
      noFieldset={noBorder}
    />
  );
};

export default UrgenceInput;
