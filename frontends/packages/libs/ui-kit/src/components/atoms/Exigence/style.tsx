import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      padding: 10,
      marginBottom: 10,
      marginRight: 10
    },
    tete: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      cursor:'pointer'
    },
    title: {
      display: 'block',
      fontWeight: 'bold',
      marginBottom: '3px',
      cursor:'pointer'
    },
    label: {
      display: 'block',
      color: 'inherit',
      cursor:'pointer'

    },

    span: {
      marginRight: 15,
      color: '#9E9E9E',
    },
  }),
);

export default useStyles;
