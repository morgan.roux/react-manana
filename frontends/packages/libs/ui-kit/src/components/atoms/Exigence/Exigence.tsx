import { Paper, Typography } from '@material-ui/core';
import React, { FC, CSSProperties, MouseEvent } from 'react';
import { MoreOptions } from '../MoreOptionsComponent';
import { MoreOptionsItem } from '../MoreOptionsComponent/MoreOptions';
import useStyles from './style';

export interface IFile {
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string;
}

export interface ITheme {
  id: string;
  ordre: number;
  nom: string;
  createdAt: Date;
  updatedAt: Date;
  nombreSousThemes: number;
  sousThemes: ISousTheme[];
}

export interface IExemple {
  id: string;
  ordre: number;
  exemple: string;
}

export interface IQuestion {
  id: string;
  ordre: number;
  question: string;
}

export interface ISousTheme {
  id: string;
  ordre: number;
  nom: string;
  createdAt: Date;
  updatedAt: Date;
  nombreExigences: number;
  exigences: IExigence[];
}

export interface IAffiche {
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  fichier: IFile;
  createdAt: Date;
  updatedAt: Date;
}

export interface IDocument {
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  fichier: IFile;
  createdAt: Date;
  updatedAt: Date;
}

export interface IEnregistrement {
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  fichier: IFile;
  createdAt: Date;
  updatedAt: Date;
}

export interface IMemo {
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  fichier: IFile;
  createdAt: Date;
  updatedAt: Date;
}

export interface IProcedure {
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  fichier: IFile;
  createdAt: Date;
  updatedAt: Date;
}

export interface IExigenceOutils {
  affiches: IAffiche[];
  documents: IDocument[];
  enregistrements: IEnregistrement[];
  memos: IMemo[];
  procedures: IProcedure[];
  checklists: ICheckList[];
}

export interface ICheckListSection {
  id: string;
  ordre: number;
  libelle: string;
  checklist: ICheckList;
  items: ICheckListSectionItem[];
}

export interface ICheckListSectionItem {
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
}

export interface ICheckList {
  id: string;
  code: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
  createdAt: Date;
  updatedAt: Date;
  sections: ICheckListSection[];
}

export interface IExigence {
  id: string;
  ordre: number;
  titre: string;
  finalite: string;
  createdAt: Date;
  updatedAt: Date;
  exemples: IExemple[];
  questions: IQuestion[];
  sousTheme: ISousTheme[];
  nombreOutils?: number;
  nombreDocuments?: number;
  nombreCommentaires?: number;
  outils?: IExigenceOutils;
}

export interface ExigenceProps {
  exigence: IExigence;
  moreOptions?: MoreOptionsItem[];
  style?: CSSProperties;
  titleStyle?: CSSProperties;
  onClick?: (event: MouseEvent) => void;
}

const Exigence: FC<ExigenceProps> = ({ exigence, style, titleStyle, moreOptions, onClick }) => {
  const classes = useStyles();

  return (
    <Paper elevation={2} className={classes.root} style={style} onClick={onClick}>
      <div className={classes.tete}>
        <Typography color="secondary" className={classes.title} style={titleStyle}>
          {`Exigence ${exigence.ordre}`}
        </Typography>
        {moreOptions && <MoreOptions items={moreOptions} />}
      </div>
      <span className={classes.label}>{exigence.titre}</span>
      <div style={{ marginTop: 20 }}>
        {typeof exigence.nombreOutils !== 'undefined' ? (
          <span className={classes.span}>{`Outils associés : ${exigence.nombreOutils}`}</span>
        ) : null}
        {typeof exigence.nombreDocuments !== 'undefined' ? (
          <span className={classes.span}>{`Documents associés : ${exigence.nombreDocuments}`}</span>
        ) : null}
        {typeof exigence.nombreCommentaires !== 'undefined' && (
          <span
            className={classes.span}
          >{`Nbre commentaire : ${exigence.nombreCommentaires}`}</span>
        )}
      </div>
    </Paper>
  );
};

export default Exigence;
