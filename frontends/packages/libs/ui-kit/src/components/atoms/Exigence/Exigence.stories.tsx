import { Delete, Edit, LocalSee } from '@material-ui/icons';
import React from 'react';
import Exigence, { IExigence } from './Exigence';

export default {
    title: 'Design system|Atoms/Exigence',
    parameters: {
        info: { inline: true }
    }
};

const exigence : IExigence = {
    id: '1',
    ordre : 2,
    titre : 'Exigence 1',
    finalite : 'XXXXXXXXXXXXXXXXX',
    createdAt : new Date('2020-5-8'),
    updatedAt : new Date('2020-9-8'),
    exemples :[],
    questions :[],
    sousTheme : [],
    nombreOutils : 2,
    nombreDocuments : 3,
    nombreCommentaires : 5
};

const handleClick = ( name : string ) : void => {
    alert(`Bonjour ${name}`)
};

const moreOptions = [
    {
        menuItemLabel : {
            title : "Voir historique",
            icon : <LocalSee />
        },
        onClick : handleClick.bind(null,'Voir historique')
    },
    {
        menuItemLabel : {
            title : "Modifier",
            icon : <Edit />
        },
        onClick : handleClick.bind(null,'Modifier')
    },
    {
        menuItemLabel : {
            title : 'Supprimer',
            icon : <Delete />
        },
        onClick : handleClick.bind(null,'Supprimer')
    },
];

export const ThemeStory = () => (
    <Exigence moreOptions={moreOptions} exigence={exigence} onClick={handleClick.bind(null,'XXX')} />
);
