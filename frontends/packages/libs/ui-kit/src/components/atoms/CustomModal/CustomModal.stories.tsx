import React, { useState } from 'react';

import CustomModal from './CustomModal';
export default {
  title: 'Design system|Atoms/CustomModal',
  parameters: {
    info: { inline: true },
  },
};

export const customModalStory = () => {
  const [open, setOpen] = useState<boolean>(false);

  const change = () => {
    setOpen(!open);
  };

  return (
    <div>
      <button onClick={change}>Modal</button>
      <CustomModal
        open={open}
        setOpen={change}
        title={'Ajouter une affiche'}
        withBtnsActions={true}
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="xl"
      >
        Bonjour les gens
      </CustomModal>
    </div>
  );
};
