import React, { FC } from 'react';
import { Dialog, IconButton, DialogProps, Slide, AppBar, Toolbar, Typography } from '@material-ui/core';

import classnames from 'classnames';
import { NewCustomButton } from '../CustomButton';
import CloseIcon from '@material-ui/icons/Close';
import useStyles from './styles';
import { CustomModalInterface } from './CustomModal';
import { TransitionProps } from '@material-ui/core/transitions';

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const CustomFullScreenModal: FC<CustomModalInterface & DialogProps> = ({
  title,
  actionButton,
  open,
  setOpen,
  children,
  onClickConfirm,
  centerBtns,
  withBtnsActions,
  className,
  withHeader,
}) => {
  const classes = useStyles();

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      className={
        centerBtns
          ? classnames(classes.customModalRoot, classes.centerBtnsDialogActions, className)
          : classnames(classes.customModalRoot, className)
      }
      TransitionComponent={Transition}
      fullScreen={true}
    >
      {withHeader !== false && (
        <AppBar className={classes.appBar}>
          <Toolbar className={classes.toolbar}>
            <Typography className={classnames(classes.customModalTitle, classes.customFullScreenModalTitle)}>
              {title}
            </Typography>
            <div>
              {withBtnsActions && (
                <NewCustomButton onClick={onClickConfirm}>{actionButton || 'SAUVEGARDER'}</NewCustomButton>
              )}
              <IconButton color="inherit" onClick={handleClose} aria-label="Close">
                <CloseIcon />
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
      )}
      {children}
    </Dialog>
  );
};

CustomFullScreenModal.defaultProps = {
  withBtnsActions: true,
  headerWithBgColor: false,
};

export default CustomFullScreenModal;
