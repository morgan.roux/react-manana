import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customModalRoot: {
      color: theme.palette.common.black,
      '& * ': {
        letterSpacing: 0,
      },
      [theme.breakpoints.up('md')]: {
        minWidth: 800,
      },
    },
    centerBtnsDialogActions: {
      '& .MuiDialogActions-root': {
        justifyContent: 'center',
      },
    },
    customModalTitle: {
      fontSize: 18,
      fontWeight: 'bold',
    },
    customFullScreenModalTitle: {
      fontSize: 25,
    },
    customModalTitleWithBgColor: {
      background: theme.palette.primary.main,
      '& * ': {
        color: theme.palette.common.white,
      },
      '& > h2': {
        display: 'flex',
        justifyContent: 'space-between',
      },
    },
    btn: {
      fontWeight: 'bold',
      height: '50px',
      borderRadius: '4px !important',
    },
    cancelBtn: {
      background: '#e6e6e6',
    },
    closeButton: {
      color: 'inherit',
      float: 'right',
    },
    appBar: {
      position: 'sticky',
      top: 0,
    },
    toolbar: {
      justifyContent: 'space-between',
    },
    button: {
      padding: 20,
      boxShadow: '0 0 18px 0 #ccc',
    },
    centerButton: {
      justifyContent: 'center',
    },
    fullscreen: {
      '& .main-MuiDialog-paper': {
        margin: 0,
        maxHeight: '100%',
        width: '100%',
      },
      '& .MuiDialog-paper': {
        margin: 0,
        maxHeight: '100%',
        width: '100%',
      },
    },
  })
);

export default useStyles;
