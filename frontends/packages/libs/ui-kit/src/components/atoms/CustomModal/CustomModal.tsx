import { useApplicationContext } from '@lib/common';
import {
  Box,
  createGenerateClassName,
  Dialog,
  DialogActions,
  DialogContent,
  DialogProps,
  DialogTitle,
  IconButton,
  StylesProvider,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import classnames from 'classnames';
import React, { FC, MouseEvent, ReactNode } from 'react';
import { NewCustomButton } from '../CustomButton';
import useStyles from './styles';

const generateClassName = createGenerateClassName({
  //  disableGlobal: true,
  productionPrefix: 'dialog',
  seed: 'dialog',
});
export interface CustomModalInterface {
  title?: string;
  open: boolean;
  children: ReactNode;
  actionButton?: string;
  disabledButton?: boolean;
  closeIcon?: boolean;
  centerBtns?: boolean;
  withBtnsActions?: boolean;
  headerWithBgColor?: boolean;
  withHeader?: boolean;
  setOpen: (open: boolean) => void;
  onClickConfirm?: (event: MouseEvent<any>) => void;
  withCancelButton?: boolean;
  withConfirmButton?: boolean;
  actionButtonTitle?: string;
  cancelButtonTitle?: string;
  customHandleClose?: () => void;
  fullScreen?: boolean;
  noDialogContent?: boolean;
}

const CustomModal: FC<CustomModalInterface & DialogProps> = ({
  title,
  actionButton,
  open,
  setOpen,
  children,
  onClickConfirm,
  disabledButton,
  closeIcon,
  withBtnsActions = true,
  headerWithBgColor = false,
  className,
  maxWidth = false,
  fullWidth,
  withCancelButton,
  withConfirmButton = true,
  actionButtonTitle = 'VALIDER',
  cancelButtonTitle = 'Annuler',
  customHandleClose,
  fullScreen,
  noDialogContent,
}) => {
  const classes = useStyles({});
  const { isMobile, isLg } = useApplicationContext();
  const handleClose = () => {
    if (customHandleClose) {
      customHandleClose();
    } else {
      setOpen(false);
    }
  };

  return (
    <StylesProvider generateClassName={generateClassName}>
      <Dialog
        onClick={(event) => event.stopPropagation()}
        open={open}
        onClose={handleClose}
        disableBackdropClick={true}
        disableEnforceFocus
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        className={
          fullScreen
            ? classnames(classes.customModalRoot, classes.fullscreen, className)
            : classnames(classes.customModalRoot, className)
        }
        maxWidth={maxWidth as any}
        fullWidth={fullWidth !== undefined ? fullWidth : false}
        fullScreen={isMobile}
        PaperProps={{
          style: {
            maxWidth: maxWidth ? undefined : '100%',
          },
        }}
      >
        {title && (
          <DialogTitle
            id="alert-dialog-title"
            className={
              headerWithBgColor
                ? classnames(classes.customModalTitle, classes.customModalTitleWithBgColor)
                : classes.customModalTitle
            }
          >
            {title}
            {closeIcon && (
              <IconButton
                size="small"
                aria-label="close"
                className={classes.closeButton}
                // tslint:disable-next-line: jsx-no-lambda
                onClick={(event) => {
                  event.preventDefault();
                  event.stopPropagation();
                  handleClose();
                }}
              >
                <CloseIcon />
              </IconButton>
            )}
          </DialogTitle>
        )}

        {noDialogContent ? (
          <Box display="flex">{children}</Box>
        ) : (
          <DialogContent>
            <Box display="flex" py={3}>
              {children}
            </Box>
          </DialogContent>
        )}

        {withBtnsActions && (
          <DialogActions className={classes.button}>
            {(withCancelButton || withCancelButton === undefined) && (
              <NewCustomButton
                className={`${classes.btn} ${classes.cancelBtn}`}
                // tslint:disable-next-line: jsx-no-lambda
                onClick={(event: any) => {
                  event.preventDefault();
                  event.stopPropagation();
                  handleClose();
                }}
                theme="flatTransparent"
              >
                {cancelButtonTitle}
              </NewCustomButton>
            )}

            {withConfirmButton && (
              <NewCustomButton
                className={classes.btn}
                // tslint:disable-next-line: jsx-no-lambda
                onClick={(event: any) => {
                  event.preventDefault();
                  event.stopPropagation();
                  if (onClickConfirm) onClickConfirm(event);
                }}
                disabled={disabledButton && disabledButton === true ? disabledButton : false}
              >
                {actionButton || actionButtonTitle}
              </NewCustomButton>
            )}
          </DialogActions>
        )}
      </Dialog>
    </StylesProvider>
  );
};

export default CustomModal;
