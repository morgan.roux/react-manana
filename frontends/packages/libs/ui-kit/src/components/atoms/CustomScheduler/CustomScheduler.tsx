import { IntlProvider, load, loadMessages, LocalizationProvider } from '@progress/kendo-react-intl';
import {
  AgendaView,
  DayView,
  MonthView,
  Scheduler,
  SchedulerHeaderProps,
  SchedulerViewChangeEvent,
  TimelineView,
  WeekView,
} from '@progress/kendo-react-scheduler';
// import { guid } from '@progress/kendo-react-common';
import { SchedulerGroup } from '@progress/kendo-react-scheduler/dist/npm/models';
import currencyData from 'cldr-core/supplemental/currencyData.json';
import likelySubtags from 'cldr-core/supplemental/likelySubtags.json';
import weekData from 'cldr-core/supplemental/weekData.json';
/* ES */
import caGregorian_ES from 'cldr-dates-full/main/es/ca-gregorian.json';
import dateFields_ES from 'cldr-dates-full/main/es/dateFields.json';
import timeZoneNames_ES from 'cldr-dates-full/main/es/timeZoneNames.json';
/* FR */
import caGregorian_FR from 'cldr-dates-full/main/fr/ca-gregorian.json';
import dateFields_FR from 'cldr-dates-full/main/fr/dateFields.json';
import timeZoneNames_FR from 'cldr-dates-full/main/fr/timeZoneNames.json';
import currencies_ES from 'cldr-numbers-full/main/es/currencies.json';
import numbers_ES from 'cldr-numbers-full/main/es/numbers.json';
import currencies_FR from 'cldr-numbers-full/main/fr/currencies.json';
import numbers_FR from 'cldr-numbers-full/main/fr/numbers.json';
import React, { FC, useEffect, useState } from 'react';
import { customModelFields } from './data';
import { esMessages } from './locales/es';
import { frMessages } from './locales/fr';
// import { FormWithCustomDialog } from './CustomForm';

const locales = {
  en: { language: 'en-US', locale: 'en' },
  es: { language: 'es-ES', locale: 'es' },
  fr: { language: 'fr-FR', locale: 'fr' },
};

load(
  likelySubtags,
  currencyData,
  weekData,
  numbers_FR,
  currencies_FR,
  caGregorian_FR,
  dateFields_FR,
  timeZoneNames_FR,
  numbers_ES,
  currencies_ES,
  caGregorian_ES,
  dateFields_ES,
  timeZoneNames_ES
);

loadMessages(frMessages, 'fr-FR');
loadMessages(esMessages, 'es-ES');

// interface ViewProps {
//   label: string;
//   slot?: ReactNode;
//   clientAvance?: ReactNode;
// }
interface CustomSchedulerProps {
  header?: React.ComponentType<SchedulerHeaderProps>;
  footer?: any;
  // views?: ViewProps[] | undefined;
  views?: any;
  locale?: 'fr' | 'es' | 'en';
  group?: SchedulerGroup;
  ressources?: any;
  timezone?: string;
  activeView?: 'timeline' | 'day' | 'week' | 'month' | 'agenda';
  className?: string;
  editable?: boolean | undefined;
  slot?: any;
  selectClient?: any;
  displayDate?: Date;
  item?: any;
  CustomForm?: any;
  CustomData?: any;
  customDataChange?: any;
  customDateChange?: any;
}

const CustomScheduler: FC<CustomSchedulerProps> = ({
  views,
  locale,
  timezone,
  header,
  footer,
  group,
  ressources,
  activeView,
  className,
  editable,
  displayDate,
  item,
  CustomForm,
  CustomData,
  customDataChange,
  customDateChange,
}) => {
  const activeLocale = locale ? locales['fr'] : locales['en'];

  const viewList: any = {
    // timeline: <TimelineView />,
    // day: <DayView />,
    // week: <WeekView showWorkHours={false} />,
    month: <MonthView />,
    // agenda: <AgendaView />,
  };

  const [view, setView] = useState<'timeline' | 'day' | 'week' | 'month' | 'agenda'>(activeView || 'week');

  const activeViews = (views || [{ label: 'week' }]).map((viewItem: any) => viewList[viewItem]);

  const handleViewChange = (args: SchedulerViewChangeEvent) => {
    setView(args.value as 'timeline' | 'day' | 'week' | 'month' | 'agenda');
  };

  useEffect(() => {
    if (activeView) setView(activeView);
  }, [activeView]);

  // const [data, setData] = useState(sampleData);
  // console.log(data);

  // const handleDataChange = ({ created, updated, deleted }: any) => {
  //   console.log('****************************************gfdgfdgfd ', created, updated, deleted);
  //   setData((old) =>
  //     old
  //       .filter((item) => deleted.find((current: any) => current.id === item.id) === undefined)
  //       .map((item) => updated.find((current: any) => current.id === item.id) || item)
  //       .concat(created.map((item: any) => Object.assign({}, item, { id: guid() })))
  //   );
  // };

  return (
    <LocalizationProvider language={activeLocale.language}>
      <IntlProvider locale={activeLocale.locale}>
        <Scheduler
          // data={sampleDataWithCustomSchema}
          data={CustomData}
          onDataChange={customDataChange}
          // form={FormWithCustomDialog}
          form={CustomForm}
          view={view}
          onViewChange={handleViewChange}
          date={displayDate}
          onDateChange={customDateChange}
          editable={editable}
          timezone={timezone || 'Etc/UTC'}
          modelFields={customModelFields}
          group={group}
          header={header ? header : undefined}
          footer={!views ? () => <></> : footer ? footer : undefined}
          resources={ressources || []}
          className={className}
          item={item}
        >
          {activeViews}
        </Scheduler>
      </IntlProvider>
    </LocalizationProvider>
  );
};

export default CustomScheduler;
