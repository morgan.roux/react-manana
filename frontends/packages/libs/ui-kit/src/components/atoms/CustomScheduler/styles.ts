// import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
// const drawerWidth = 336;
const useStyles = makeStyles(() =>
  createStyles({
    modalRoot: {
      '& .MuiPaper-root': {
        width: '690px',
      },
      '& .MuiBackdrop-root': {
        backgroung: 'transparent',
      },
      '& .MuiDialogTitle-root': {
        background: '#F34F9C',
      },
      '& .MuiTypography-root': {
        fontSize: '20px',
        fontFamily: 'Roboto,Bold',
      },
    },
    selectRoot: {
      '& .MuiSelect-root': {
        width: '610px',
        height: '20px',
      },
    },
    client: {
      display: 'flex',
      justifyContent: 'flex-start',
      fontSize: '18px',
      fontFamily: 'Roboto,Medium',
    },
    recherche: {
      display: 'flex',
      justifyContent: 'flex-end',
      color: '#F34F9C',
      lineHeight: '1px',
    },
    rechecheCollaborateur: {
      display: 'flex',
      justifyContent: 'flex-end',
      fontSize: '14px',
      fontFamily: 'Roboto,Regular',
      color: '#f34f9c',
    },
    paragraphe: {
      display: 'flex',
      justifyContent: 'flex-start',
      fontSize: '14px',
      color: '#42424261',
      fontFamily: 'Roboto,Regular',
    },
  }),
);
export default useStyles;
