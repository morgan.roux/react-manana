import { SchedulerHeader, SchedulerHeaderProps } from '@progress/kendo-react-scheduler';
import React, { useState } from 'react';
import TableChartSubToolbar from '../../molecules/TableChartSubToolbar';
import { CustomButton } from '../CustomButton';
import CustomScheduler from './CustomScheduler';

export default {
  title: 'Design system|Atoms/CustomScheduler',
  parameters: {
    info: { inline: true },
  },
};

export const customSchedulerStory = () => {
  const [activeView, setActiveView] = useState<'timeline' | 'day' | 'week' | 'month' | 'agenda'>(
    'month',
  );

  const handleRequestChange = (searchText: string, date: any) => {
    console.log(searchText, date);
  };

  const Header = (props: SchedulerHeaderProps) => {
    console.log(props);
    return (
      <SchedulerHeader>
        <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
          <TableChartSubToolbar onRequestChange={handleRequestChange} />
          <div style={{ display: 'flex' }}>
            <CustomButton onClick={() => setActiveView('day')}>Jour</CustomButton>
            <CustomButton onClick={() => setActiveView('week')}>Semaine</CustomButton>
            <CustomButton onClick={() => setActiveView('month')}>Mois</CustomButton>
            <CustomButton onClick={() => setActiveView('timeline')}>Planning</CustomButton>
          </div>
        </div>
      </SchedulerHeader>
    );
  };
  return (
    <CustomScheduler
      locale="fr"
      header={(props) => <Header {...props} />}
      activeView={activeView}
      views={[{ label: 'day' }, { label: 'week' }, { label: 'month' }, { label: 'timeline' }]}
    />
  );
};
