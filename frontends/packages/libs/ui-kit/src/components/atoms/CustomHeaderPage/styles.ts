import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => 
  createStyles({
    root: {
      width: '100%',
      padding: '8px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    filtre: {
      display: 'flex !important',
      alignItems: 'center !important',
      justifyContent: 'space-between !important',
    },
  })
)

export default useStyles;