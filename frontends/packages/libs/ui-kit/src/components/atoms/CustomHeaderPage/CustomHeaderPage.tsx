import { Box } from '@material-ui/core';
import React, { FC , ReactElement} from 'react';
import useStyles from './styles';

interface CustomHeaderPageProps {
  onSearch?: any;
  parametre?: ReactElement;
  titre?: ReactElement;
}

export const CustomHeaderPage: FC<CustomHeaderPageProps> = ({ onSearch, parametre, titre }) => {

  const classes = useStyles();

  return(
    <Box className={classes.root}>
      {titre}
      <Box className={classes.filtre}>
        {onSearch}
        {parametre}
      </Box>
    </Box>
  )
}