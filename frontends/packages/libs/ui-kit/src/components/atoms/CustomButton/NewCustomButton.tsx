import React, { FC } from 'react';
import { Button, ButtonProps } from '@material-ui/core';
import { useStyles } from './newStyles';
import classnames from 'classnames';

interface CustomButtonProps {
  theme?: 'primary' | 'secondary' | 'transparent' | 'flatTransparent' | 'flatPrimary';
  shadow?: boolean;
}
const NewCustomButton: FC<ButtonProps & CustomButtonProps> = (props: ButtonProps & CustomButtonProps) => {
  const { className, children, theme, shadow = true } = props;
  const classes = useStyles();

  const getAppropriateClass = (theme?: 'primary' | 'secondary' | 'transparent' | 'flatTransparent' | 'flatPrimary') => {
    switch (theme) {
      case 'secondary':
        return classes.secondary;

      case 'transparent':
        return classes.transparent;

      case 'flatTransparent':
        return classes.flat;
      case 'flatPrimary':
        return classes.flatPrimary;

      default:
        return classes.primary;
    }
  };
  const hasShadow = (bool: boolean) => (!bool ? classes.noShadow : '');

  return (
    <Button
      {...props}
      className={
        className
          ? classnames(classes.newCustomButton, getAppropriateClass(theme), hasShadow(shadow), className)
          : classnames(classes.newCustomButton, getAppropriateClass(theme), hasShadow(shadow))
      }
      classes={{ disabled: classes.disabled }}
    >
      {children}
    </Button>
  );
};

NewCustomButton.defaultProps = {
  color: 'default',
  size: 'medium',
  disabled: false,
};

export default NewCustomButton;
