import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    newCustomButton: {
      fontSize: 14,
      letterSpacing: 0,
      borderRadius: '4px !important',
      padding: '11px 34px !important',
      transition: '.5s',
      boxShadow: '0px 3px 1px -2px rgba(0,0,0,0.2), 0px 2px 2px 0px rgba(0,0,0,0.14), 0px 1px 5px 0px rgba(0,0,0,0.12)',
    },
    noShadow: {
      boxShadow: 'none !important',
    },
    primary: {
      background: `${theme.palette.primary.main} !important`,
      color: `${theme.palette.common.white}  !important`,
      '&:hover': {
        background: `${theme.palette.primary.main} !important`,
        color: `${theme.palette.common.white}  !important`,
        transition: '.5s',
      },
    },
    secondary: {
      background: `${theme.palette.secondary.main}  !important`,
      color: `${theme.palette.common.white} !important`,
      '&:hover': {
        background: `${theme.palette.secondary.main} !important`,
        color: `${theme.palette.common.white} !important`,
        transition: '.5s',
      },
    },
    transparent: {
      background: theme.palette.common.white,
      color: '#000',
    },
    flat: {
      background: '#f5f5f5 !important',
      color: '#000',
      '&:hover': {
        background: 'rgb(237 237 237) !important',
      },
    },
    flatPrimary: {
      background: theme.palette.common.white,
      color: theme.palette.primary.main,
      boxShadow: 'none',
      borderColor: theme.palette.primary.main,
      '&:hover': {
        background: 'rgb(237 237 237) !important',
      },
    },
    disabled: {
      opacity: 0.5,
    },
  })
);
