import React from 'react';
import CustomButton from './CustomButton';

export default {
  title: 'Design system|Atoms/CustomButton',
  parameters: {
    info: { inline: true },
  },
};

export const CustomButtonStory = () => <CustomButton>Ajouter un sous thème</CustomButton>;
