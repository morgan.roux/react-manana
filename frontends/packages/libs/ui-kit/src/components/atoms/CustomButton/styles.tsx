import { makeStyles, createStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customButtonRoot: {
      fontSize: 14,
      letterSpacing: 0,
      padding: '11px 34px',
      background: theme.palette.primary.main,
    },
    // '&.MuiButton-containedSecondary': {
    //   backgroundColor: theme.palette.primary.main,
    // },
  })
);

export default useStyles;
