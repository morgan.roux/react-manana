import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    pic: {
      marginTop: '10px',
      height: '200px',
      width: '200px',
      border: '1px solid #000',
    },
    videoFeed: {},
    button: {
      '&.MuiButton-contained': {
        backgroundColor: theme.palette.primary.main,
        color: '#FFFFFF',
        padding: 'auto',
        marginLeft: '25px',
        display: 'flex',
      },
      '& .MuiButton-label': {
        textTransform: 'initial',
      },
    },
  })
);

export default useStyles;
