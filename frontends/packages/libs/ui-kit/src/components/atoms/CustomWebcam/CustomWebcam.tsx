import React, { FC, useState, useRef } from 'react';
import Webcam from 'react-webcam';
import { NewCustomButton } from '../CustomButton';

import useStyles from './styles';

interface CustomWebcamProps {
  WIDTH: number;
  HEIGHT: number;
}

export const CustomWebcam: FC<CustomWebcamProps> = ({ WIDTH, HEIGHT }) => {
  const classes = useStyles();

  const webcamRef = useRef(null);
  const [imgSrc, setImgSrc] = useState(null);

  const capture = React.useCallback(() => {
    const imageSrc = (webcamRef.current as any).getScreenshot();
    setImgSrc(imageSrc);
  }, [webcamRef, setImgSrc]);

  // const HEIGHT = 200;
  // const WIDTH = 200;

  return (
    <>
      <Webcam audio={false} ref={webcamRef} screenshotFormat="image/jpeg" width={WIDTH} height={HEIGHT} />
      <NewCustomButton onClick={capture} className={classes.button}>
        Capture photo
      </NewCustomButton>
      {imgSrc && <img src={imgSrc || ''} />}
    </>
  );
};
