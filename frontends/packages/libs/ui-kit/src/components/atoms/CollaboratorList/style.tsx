import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      width: '100%',
    },
    formContainer: {
      width: '100%',
      marginTop: 30,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      '& > div:nth-child(1)': {
        width: 494,
      },
      '& > div:nth-child(2)': {
        marginLeft: 20,
        width: 275,
      },
      '& > div': {
        marginBottom: '0px !important',
      },
    },
    collaboListContainer: {
      width: '100%',
      height: 'calc(100vh - 480px)',
      overflow: 'auto',
      marginTop: 30,
      '& .MuiAvatar-root, & img': {
        width: 50,
        height: 50,
        marginRight: 15,
      },
      '& img': {
        borderRadius: '50%',
      },
    },
  })
);

export default useStyles;
