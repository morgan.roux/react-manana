import {
  Avatar,
  Box,
  InputAdornment,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  Radio,
  TextField,
  Typography,
} from '@material-ui/core';
import { CheckCircle, Person, Search } from '@material-ui/icons';
import { isEqual } from 'lodash';
import React, { ChangeEvent, FC, useState } from 'react';
import { User } from '../../pages/DashboardPage';
import { ErrorPage } from '../../../components/pages/ErrorPage';
import { Backdrop } from './../../atoms/Backdrop';
import useStyles from './style';

export interface FilterCollaboratorValue {
  text: string;
  type: any;
}

export interface CollaboratorListProps {
  data: User[];
  selectable?: boolean;
  filterable?: boolean;
  loading?: boolean;
  multipleSelection?: boolean;
  error?: Error;
  rowsSelected?: User[];
  onSelectionChange?: (rowsSelected: User[]) => void;
}

const CollaboratorList: FC<CollaboratorListProps> = ({
  data,
  selectable,
  filterable,
  loading,
  error,
  rowsSelected,
  multipleSelection = true,
  onSelectionChange,
}) => {
  const classes = useStyles();

  const checkedsIds = (rowsSelected && rowsSelected.map((i) => i.id)) || [];

  const [filter, setFilter] = useState<FilterCollaboratorValue>({ text: '', type: null });

  const [filteredData, setFilteredData] = useState<User[]>(data);

  const [isCheckedAll, setIsCheckedAll] = useState<boolean>(false);

  const { text, type } = filter;

  console.log('type :>> ', type);

  const handleChange = (event: ChangeEvent<any>) => {
    const { name, value } = event.target;
    setFilter((prev) => ({ ...prev, [name]: value }));
  };

  const handleCheck = (item: any) => {
    if (rowsSelected && onSelectionChange) {
      if (multipleSelection) {
        if (checkedsIds.includes(item.id)) {
          const newCheckeds = rowsSelected.filter((i) => i.id !== item.id);
          onSelectionChange(newCheckeds);
        } else {
          onSelectionChange([...rowsSelected, item]);
        }
      } else {
        onSelectionChange([item]);
      }
    }
  };

  const handleCheckAll = (): void => {
    setIsCheckedAll(!isCheckedAll);
    if (onSelectionChange) {
      if (!isCheckedAll) {
        onSelectionChange(data);
      } else {
        onSelectionChange([]);
      }
    }
  };

  // Filter data
  React.useMemo(() => {
    // Filter by search (fullName, email, role)
    if (filter.text) {
      const newData = data.filter(
        (d) =>
          d.fullName?.toLowerCase().includes(filter.text.toLowerCase()) ||
          d.email?.toLowerCase().includes(filter.text.toLowerCase()) ||
          (d.role && d.role.nom.toLowerCase().includes(filter.text.toLowerCase()))
      );
      setFilteredData(newData);
    } else if (!isEqual(data, filteredData)) {
      setFilteredData(data);
    }
  }, [filter]);

  if (error) {
    return <ErrorPage />;
  }

  if (loading) {
    return <Backdrop />;
  }

  return (
    <Box className={classes.root}>
      {filterable && (
        <div className={classes.formContainer}>
          <TextField
            variant="outlined"
            placeholder="Recherche..."
            value={text}
            name="text"
            onChange={handleChange}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <Search />
                </InputAdornment>
              ),
            }}
          />
          {/* <CustomSelect
            label="Type"
            list={[]}
            listId="id"
            index="value"
            name="type"
            value={type}
            onChange={handleChange}
          /> */}
        </div>
      )}
      <div className={classes.collaboListContainer}>
        <List dense>
          {selectable && (
            <ListItem>
              <ListItem>
                <ListItemText primary="Séléctionner tout" />
              </ListItem>
              <ListItemSecondaryAction>
                <Radio checked={isCheckedAll} onClick={handleCheckAll} checkedIcon={<CheckCircle />} />
              </ListItemSecondaryAction>
            </ListItem>
          )}
          {filteredData.map((item, index) => {
            return (
              <ListItem key={index}>
                {item.photo?.publicUrl ? (
                  <img src={item.photo.publicUrl} />
                ) : (
                  <ListItemAvatar>
                    <Avatar>
                      <Person />
                    </Avatar>
                  </ListItemAvatar>
                )}
                <ListItemText
                  primary={item.fullName}
                  secondary={
                    <React.Fragment>
                      <Typography component="span" variant="body2" color="textPrimary">
                        {item?.role?.nom}
                      </Typography>
                      <br />
                      {item?.email}
                    </React.Fragment>
                  }
                />
                {selectable && (
                  <ListItemSecondaryAction>
                    <Radio
                      checked={isCheckedAll || checkedsIds.includes(item.id)}
                      onClick={() => handleCheck(item)}
                      checkedIcon={<CheckCircle />}
                    />
                  </ListItemSecondaryAction>
                )}
              </ListItem>
            );
          })}
        </List>
      </div>
    </Box>
  );
};

export default CollaboratorList;
