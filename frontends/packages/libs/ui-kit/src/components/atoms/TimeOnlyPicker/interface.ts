import React from 'react';

export interface TimePickersProps {
  labelName?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  defaultValue?: string;
  fullWidth?: boolean;
  id?: string;
}
