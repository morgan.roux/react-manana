import React, { FC, useState } from 'react';
import ptBR from 'dayjs/locale/fr';
import DayJsUtils from '@date-io/dayjs';
import { MuiPickersUtilsProvider, KeyboardTimePicker } from '@material-ui/pickers';
import AccessTimeIcon from '@material-ui/icons/AccessTime';

interface DateTimeInputProps {
  label?: string;
  itemRef?: string;
  onChange?: (
    date: Date | null | string,
    values?: string | null,
    name?: string,
    itemRef?: string,
  ) => void;
  id?: string;
  defaultHours?: Date | null | string;
  name?: string;
  className?: string | undefined;
}
/**
 * Extension of Material-ui 'KeyboardTimePicker'
 * TimeOnlyPicker :
 * @type {DateTimeInputProps}
 * @returns Custom Time only picker
 */
export const TimeOnlyPicker: FC<DateTimeInputProps> = ({
  label,
  onChange,
  id,
  defaultHours,
  name,
  className,
  itemRef,
}) => {
  const [selectedDate, setSelectedDate] = useState<Date | null | string>(
    defaultHours || new Date(),
  );
  const handleChange = (date: Date | null | string, values?: string | null) => {
    setSelectedDate(date);
    if (onChange) {
      onChange(date, values, name, itemRef);
    }
  };

  return (
    <MuiPickersUtilsProvider locale={ptBR} utils={DayJsUtils}>
      <KeyboardTimePicker
        margin="normal"
        id={id}
        label={label}
        ampm={false}
        name={name}
        itemRef={itemRef}
        inputVariant="outlined"
        className={className}
        value={selectedDate}
        onChange={handleChange}
        keyboardIcon={<AccessTimeIcon />}
        KeyboardButtonProps={{
          'aria-label': 'time-only-picker',
        }}
        cancelLabel="Annuler"
      />
    </MuiPickersUtilsProvider>
  );
};
