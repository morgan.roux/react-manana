import React from 'react';
import { TimeOnlyPicker } from './TimeOnlyPicker';

export default {
  title: 'Design system|Atoms/TimeOnlyPicker',
  parameters: {
    info: { inline: true },
  },
};

export const TimeOnlyPickerStory = () => {
  return <TimeOnlyPicker label="Heure de début" />;
};
