import React, { FC } from 'react';
import { Backdrop as MUIBackdrop, CircularProgress } from '@material-ui/core';
import useStyles from './styles';

export interface BackdropProps {
  open?: boolean;
  value?: string | undefined;
}

const Backdrop: FC<BackdropProps> = ({ open, value }) => {
  const classes = useStyles();
  return (
    <MUIBackdrop className={classes.backdrop} open={open !== undefined ? open : true}>
      <div className={classes.container}>
        <div className={classes.element}>
          <CircularProgress color="inherit" />
        </div>
        {value && (
          <div className={classes.element}>
            <div>{value}</div>
          </div>
        )}
      </div>
    </MUIBackdrop>
  );
};

Backdrop.defaultProps = {
  value: 'Chargement en cours...',
};

export default Backdrop;
