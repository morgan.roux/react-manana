import React from 'react';
import Backdrop from './Backdrop';

export default {
  title: 'Design system|Atoms/Backdrop',
  parameters: {
    info: { inline: true },
  },
};

export const openBackdrop = () => {

  return <Backdrop open={true} />;
};
