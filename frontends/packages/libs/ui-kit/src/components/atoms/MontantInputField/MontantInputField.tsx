import React from 'react';
import NumberFormat from 'react-number-format';
import { FC } from 'react';
import { CustomFormTextField } from '../CustomTextField';

interface MontantInputFieldProps {
  label?: string;
  value?: number;
  onChangeValue: (values?: number) => void;
  variant?: 'filled' | 'standard' | 'outlined';
  disabled?: boolean;
  suffix?: string;
}
export const MontantInputField: FC<MontantInputFieldProps> = ({
  label,
  variant,
  value,
  onChangeValue,
  disabled,
  suffix,
}) => {
  return (
    <NumberFormat
      customInput={CustomFormTextField}
      label={label}
      variant={variant ?? 'outlined'}
      value={value}
      thousandSeparator=" "
      decimalSeparator=","
      decimalScale={2}
      //fixedDecimalScale={true}
      suffix={suffix || '€'}
      disabled={disabled}
      onValueChange={(values) => {
        onChangeValue(values.floatValue);
      }}
    />
  );
};
