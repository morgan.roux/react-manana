import React, { FC } from 'react';
import useStyles from './style';
import { Box, Divider, Grid } from '@material-ui/core';

interface ITwoColumns {
  rightContent: any;
  leftOperationHeader: any;
}

const TwoColumns: FC<ITwoColumns> = ({ children, leftOperationHeader, rightContent }) => {
  const classes = useStyles();

  return (
    <Grid container={true} className={classes.root}>
      <Grid item={true} xs={3} className={classes.leftOperation}>
        <Box>{leftOperationHeader}</Box>
        <Divider />
        <Box className={classes.listItemContent}>{children}</Box>
      </Grid>
      <Grid item={true} xs={9}>
        {rightContent}
      </Grid>
    </Grid>
  );
};

export default TwoColumns;
