import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { flexWrap: 'nowrap' },
    leftOperation: {
      minWidth: 280,
      borderRight: '2px solid #E5E5E5',
      height: 'calc(100vh - 86px)',
      overflowY: 'auto',
    },
    nested: {
      paddingLeft: theme.spacing(4),
    },
    icon: {
      fontSize: 20,
    },

    leftOperationHeader: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },

    leftOperationHeaderTitle: {
      display: 'block',
    },

    addIconContent: {
      display: 'block',
    },

    listItemContent: {
      marginTop: 8,
    },
  })
);

export default useStyles;
