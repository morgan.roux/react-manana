import React, { FC, useEffect } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { MenuItem, FormControl, Select } from '@material-ui/core';
interface Option {
  id?: string;
  code?: string;
  libelle?: string;
  background?: string;
  color?: string;
}

const statusDefault = [
  { id: '1', code: '1', libelle: 'A FAIRE', background: '#e1e1e2', color: '#000' },
  { id: '2', code: '2', libelle: 'EN COURS', background: '#d8eef7', color: '#63b8dd' },
  { id: '3', code: '3', libelle: 'TERMINÉE', background: '#dbf3e5', color: '#3aba6d' },
];

interface OptionSelectProps {
  options?: Option[];
  onChange?: (status?: string) => void;
  value?: Option;
  displaySelectIndicator?: boolean;
  styles?: React.CSSProperties;
  useDefaultStyle?: boolean;
  disabled?: boolean;
}

const useStyles = makeStyles(() =>
  createStyles({
    rootSelect: {
      padding: '5px !important',
      // textTransform: 'uppercase',
    },
    noIcon: {
      display: 'none !important',
    },
    formControl: {
      '&>div': {
        borderRadius: 3,
      },
      '&>div:before': {
        display: 'none',
      },
      '& *:after': {
        display: 'none',
      },
    },
  })
);
export const OptionSelect: FC<OptionSelectProps> = ({
  options,
  onChange,
  value,
  // displaySelectIndicator = false,
  styles,
  useDefaultStyle = true,
  disabled = false,
}) => {
  const classes = useStyles();
  const [optionState, setOptionState] = React.useState<string>('1');
  const [style, setStyle] = React.useState<React.CSSProperties>({});
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setOptionState(event.target.value as string);
    onChange && onChange(event.target.value as string);
  };
  useEffect(() => {
    let cleanup = false;
    setOptionState(value?.id || '1');
    return () => {
      cleanup = true;
    };
  }, []);

  const liste = options?.length ? options : statusDefault;
  useEffect(() => {
    let cleanup = false;

    const item = liste?.find((option) => option.id === optionState);
    setStyle({ background: item?.background, color: item?.color });

    return () => {
      cleanup = true;
    };
  }, [optionState]);
  return (
    <div>
      <FormControl className={classes.formControl}>
        <Select
          labelId="status-labelid"
          id="status-select"
          value={optionState}
          onChange={handleChange}
          classes={{ root: classes.rootSelect, icon: classes.noIcon }}
          inputProps={{}}
          variant="filled"
          style={styles || style}
          MenuProps={{
            MenuListProps: {
              style: {
                padding: 0,
              },
            },
          }}
          disabled={disabled}
        >
          {useDefaultStyle
            ? statusDefault?.map((item, _index) => {
                return (
                  <MenuItem
                    key={`select-${item.id}`}
                    value={item.id}
                    style={{ background: item.background, color: item.color }}
                  >
                    {item.libelle}
                  </MenuItem>
                );
              })
            : (options?.length || 0) > 0 &&
              options?.map((item, _index) => {
                return (
                  <MenuItem
                    key={`select-${item.id}`}
                    value={item.id}
                    style={{ background: item.background, color: item.color }}
                  >
                    {item.libelle}
                  </MenuItem>
                );
              })}
        </Select>
      </FormControl>
    </div>
  );
};
