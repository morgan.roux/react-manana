import React from 'react';
import { StatusSelect } from './StatusSelect';
export default {
  title: 'Design system|Atoms/StatusSelect',
  parameters: {
    info: { inline: true },
  },
};

export const StatusSelectStory = () => {
  return (
    <StatusSelect
      status={[
        { id: '1', code: '1', libelle: 'A FAIRE' },
        { id: '2', code: '2', libelle: 'EN COURS' },
        { id: '3', code: '3', libelle: 'TERMINEE' },
      ]}
      currentValue={{ id: '2', code: '2', libelle: 'Important' }}
    />
  );
};
