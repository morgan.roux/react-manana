import { Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { useStyles } from './styles';

export interface UrgenceInterface {
  id?: string;
  code?: string | null;
  couleur?: string | null;
  libelle?: string | null;
}

interface UrgenceLabelProps {
  urgence: UrgenceInterface;
}

const UrgenceLabel: FC<UrgenceLabelProps> = ({
  urgence,
}) => {
  const classes = useStyles({});

  return urgence ? (
    <Typography
      className={classes.title}
      style={{ color: urgence.couleur || 'black' }}
    >
      {urgence.libelle}
    </Typography>

  ) : null;
};

export default UrgenceLabel;
