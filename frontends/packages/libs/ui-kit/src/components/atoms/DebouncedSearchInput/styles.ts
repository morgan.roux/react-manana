import { makeStyles, createStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {},
    fullWidth: {
      width: '100%',
    },
  })
);

export default useStyles;
