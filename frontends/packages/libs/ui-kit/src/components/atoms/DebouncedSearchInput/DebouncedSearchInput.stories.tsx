import React, { useState } from 'react';
import DebouncedSearchInput from './DebouncedSearchInput';

export default {
  title: 'Design system|Atoms/DebouncedSearchInput',
  parameters: {
    info: { inline: true },
  },
};

export const fullCustomPlaceholder = () => {
  const [value, setValue] = useState<string>('');

  return (
    <DebouncedSearchInput
      value={value}
      placeholder="Custom placeholder"
      fullWidth={true}
      onChange={setValue}
    />
  );
};

export const debounced1000Ms = () => {
  const [value, setValue] = useState<string>('');

  return <DebouncedSearchInput value={value} wait={1000} dark={false} onChange={setValue} />;
};


export const fullWith = () => {
  const [value, setValue] = useState<string>('');
  return <DebouncedSearchInput value={value} fullWidth={true} onChange={setValue} />;
};

export const dark = () => {
  const [value, setValue] = useState<string>('');

  return <DebouncedSearchInput value={value} dark={true} onChange={setValue} />;
};

export const white = () => {
  const [value, setValue] = useState<string>('');

  return <DebouncedSearchInput value={value} dark={false} onChange={setValue} />;
};
