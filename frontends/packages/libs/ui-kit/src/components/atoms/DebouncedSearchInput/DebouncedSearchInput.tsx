import React, { FC, useState, useRef } from 'react';
import useStyles from './styles';
import { SeachInputBaseProps, SearchInputBase } from '../SearchInputBase';
import { debounce } from 'lodash';

const DebouncedSearchInput: FC<SeachInputBaseProps & { wait?: number }> = ({
  fullWidth,
  placeholder,
  disabled,
  onChange,
  value,
  dark,
  minWidth,
  wait,
  className,
  searchIconFirst,
}) => {
  const classes = useStyles();
  const [internalValue, setInternalValue] = useState<string>(value);

  const debouncedOnChange = useRef(
    debounce((value: string) => {
      onChange(value);
    }, wait || 1500)
  );

  const handleChange = (newValue: string) => {
    setInternalValue(newValue);
    debouncedOnChange.current(newValue);
  };

  return (
    <div className={fullWidth ? classes.fullWidth : classes.root}>
      <SearchInputBase
        searchIconFirst={searchIconFirst}
        value={internalValue}
        fullWidth={fullWidth}
        dark={dark}
        onChange={handleChange}
        placeholder={placeholder}
        disabled={disabled}
        minWidth={minWidth}
        outlined={true}
        className={className}
      />
    </div>
  );
};

export default DebouncedSearchInput;
