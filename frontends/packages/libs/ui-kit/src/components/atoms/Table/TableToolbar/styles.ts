import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useToolbarStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginBottom: 10,
      width: '100%',
      // paddingLeft: theme.spacing(2),
      // paddingRight: theme.spacing(1),
      display: 'flex',
      justifyContent: 'space-between',
      flexWrap: 'wrap',
      alignItems: 'center',
      '& *': {
        fontWeight: 'bold',
      },
    },
    highlight:
      theme.palette.type === 'light'
        ? {
            display: 'flex',
            color: theme.palette.secondary.main,
            // backgroundColor: lighten(theme.palette.secondary.light, 0.85),
          }
        : {
            display: 'flex',
            color: theme.palette.text.primary,
            // backgroundColor: theme.palette.secondary.dark,
          },

    title: {
      flex: '0 0 auto',
    },
    buttons: {
      display: 'flex',
      justifyContent: 'flex-end',
    },
    btn: {
      textTransform: 'none',
    },
  })
);
