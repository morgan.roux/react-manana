import { SortDirection } from '@lib/common/src/federation';
import { TableCellProps } from '@material-ui/core';
import { ReactNode, CSSProperties } from 'react';
export interface Column {
  name: string;
  label: ReactNode | string;
  sortable?: boolean;
  renderer?: (row: any, index?: number) => any;
  centered?: boolean;
  align?: 'center' | 'left' | 'right';
  colSpan?: number;
  headerColSpan?: number;
  className?: string;
  style?: CSSProperties;
  headerStyle?: CSSProperties;
  component?: TableCellProps['component'];
  scope?: string;
  headerClassName?: string;
}

export interface TableRowData {
  [name: string]: any;
}

export interface TableProps {
  dataIdName?: string; // Key of data id
  columns: Column[];
  headerColumns?: Column[];
  data: TableRowData[];
  rowsPerPageOptions?: number[];
  search?: boolean; // Show/hide search icon from toolbar.
  selectable?: boolean;
  onRowClick?: (row: any, e?: React.MouseEvent<HTMLTableRowElement, MouseEvent>) => void;
  notablePagination?: boolean;
  className?: string;
  // Selection
  rowsSelected?: TableRowData[]; // Compare by dataIdName
  onRowsSelectionChange?: (currentRowsSelected: TableRowData[]) => void;
  showSelected?: boolean;
  showSelectedRows?: boolean;
  onShowSelected?: (show: boolean) => void;
  onClearSelection?: () => void;
  onSelectAll?: () => void; // Select all rows.
  showSelectionCheckBox?: boolean;

  // Filters
  onResetFilter?: () => void;
  onResetFields?: () => void;
  filterComponent?: ReactNode;

  // Server side search
  rowsTotal?: number;
  loading?: boolean;
  error?: Error;
  onRunSearch?: (change: TableChange) => void;
  onSortColumn?: (column: any, direction: SortDirection) => void;

  topBarComponent?: (searchComponent: ReactNode) => ReactNode;
  tableCellStyle?: CSSProperties;
  style?: CSSProperties;

  additionalRows?: ReactNode;
  noTableToolbarSearch?: boolean;
  page?: number;
  tableBodyClassName?: string;
  emptyLabel?: string;
  searchIconFirst?: boolean;
  componentTable?: any;
  showSelectedNotice?: boolean;
  paginationPropsClassName?: string;
  tableContainerProps?: string;
}

export interface TableToolbarProps {
  selectable?: boolean;
  data: TableRowData[];
  rowsSelected?: TableRowData[]; // Compare by dataIdName
  onRowsSelectionChange?: (currentRowsSelected: TableRowData[]) => void;
  showSelectedRows?: boolean;
  rowsTotal?: number;
  //  onRunSearch?: () => void;
  onResetFilter?: () => void;
  onResetFields?: () => void;
  onShowSelected?: (show: boolean) => void;
  onSelectAll?: () => void;
  onClearSelection?: () => void;
  filterComponent?: ReactNode;
  showSelectedNotice?: boolean;
}

export interface TableChange {
  searchText: string;
  take: number;
  skip: number;
  sortBy?: any;
  sortDirection?: SortDirection;
  sortTable?: {
    column: string;
    direction: 'ASC' | 'DESC';
  };
  // TODO : Add order
}
