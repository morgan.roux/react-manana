/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable @typescript-eslint/no-use-before-define */
import { SortDirection } from '@lib/common/src/federation';
import {
  Box,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableRow,
} from '@material-ui/core';
import classenames from 'classnames';
import React, { FC, useEffect, useState } from 'react';
import { ErrorPage } from './../../pages/ErrorPage';
import { DebouncedSearchInput } from './../DebouncedSearchInput';
import { Loader } from './../Loader';
import { TableProps, TableRowData } from './interfaces';
import { useStyles } from './styles';
import TableHead from './TableHead';
import TablePaginationActions from './TablePaginationActions';
import TableToolbar from './TableToolbar';
import { useTableParams } from './useTableParams';
import { filtered } from './util';
import classnames from 'classnames';

const ROWS_PER_PAGE_OPTIONS = [12, 25, 50, 100, 250, 500, 1000, 2000];

const EnhancedTable: FC<TableProps> = ({
  columns,
  headerColumns,
  data,
  rowsPerPageOptions,
  dataIdName,
  search,
  selectable,
  rowsSelected,
  showSelectedRows,
  onRowsSelectionChange,
  onSelectAll,
  onResetFields,
  onResetFilter,
  onShowSelected,
  onClearSelection,
  onRowClick,
  rowsTotal,
  onRunSearch,
  loading,
  error,
  topBarComponent,
  style,
  tableCellStyle,
  onSortColumn,
  notablePagination,
  additionalRows,
  noTableToolbarSearch = false,
  page: pageProps,
  tableBodyClassName,
  showSelectionCheckBox = true,
  emptyLabel,
  searchIconFirst,
  componentTable,
  showSelectedNotice,
  paginationPropsClassName,
  tableContainerProps,
}) => {
  const classes = useStyles();
  const { params } = useTableParams();
  const { sorting } = params;
  const [serverSideFirstPageRequested, setServerSideFirstPageRequested] = useState<boolean>(false);
  const [page, setPage] = useState<number>(
    params.skip && params.take ? parseInt(params.skip) / parseInt(params.take) : 0
  );
  const [rowsPerPage, setRowsPerPage] = useState<number>(
    params.take ? parseInt(params.take) : (rowsPerPageOptions || ROWS_PER_PAGE_OPTIONS)[0]
  );
  const [searchText, setSearchText] = useState<string>(params.searchText || '');
  const [displayData, setDisplayData] = useState<TableRowData[]>([]);
  const [filteredRowsTotal, setFilteredRowsTotal] = useState<number>(0);

  const runClientSideSearch = () => {
    const startIndex = page * rowsPerPage;
    const endIndex = startIndex + rowsPerPage;

    const filteredData = data.filter((row) => filtered(row, searchText));

    setFilteredRowsTotal(filteredData.length);
    setDisplayData(filteredData.slice(startIndex, endIndex));
  };

  const dispatchSearch = () => {
    if (onRunSearch) {
      onRunSearch({
        skip: page * rowsPerPage,
        take: rowsPerPage,
        searchText: searchText,
        sortBy: sorting && sorting[0] && sorting[0].field,
        sortDirection: sorting && sorting[0] && sorting[0].direction,
      });
    } else {
      runClientSideSearch();
    }
  };

  useEffect(() => {
    if (onRunSearch && !serverSideFirstPageRequested) {
      dispatchSearch();
      setServerSideFirstPageRequested(true);
    }
  }, [serverSideFirstPageRequested]);

  useEffect(() => {
    if (showSelectedRows) {
      const filteredData = data.filter((row) =>
        (rowsSelected || []).some((selectedRow) => isRowEquals(row, selectedRow))
      );

      setDisplayData(filteredData);
    } else {
      runClientSideSearch();
    }
  }, [showSelectedRows]);

  useEffect(() => {
    if (isServerSideSearch()) {
      setDisplayData(data);
    } else {
      runClientSideSearch();
    }
  }, [data]);

  useEffect(() => {
    dispatchSearch();
  }, [
    page,
    rowsPerPage,
    searchText,
    sorting && sorting[0] && sorting[0].field,
    sorting && sorting[0] && sorting[0].direction,
  ]);

  const handleSearchTextChange = (text: string) => {
    setPage(0);
    setSearchText(text);
  };

  useEffect(() => {
    if (typeof pageProps !== 'undefined') {
      setPage(pageProps);
    }
  }, [pageProps]);

  const handlePageChange = (newPage: number) => {
    setPage(newPage);
  };

  const handleRowsPerPageChange = (newRowsPerPage: number) => {
    setRowsPerPage(newRowsPerPage);
    setPage(0);
  };

  const isServerSideSearch = () => !!onRunSearch;
  const isRowEquals = (row1: TableRowData, row2: TableRowData): boolean => {
    return dataIdName ? row1[dataIdName] === row2[dataIdName] : row1 === row2;
  };

  const handleSelection = (item: TableRowData, selection: boolean) => {
    if (onRowsSelectionChange) {
      if (selection) {
        onRowsSelectionChange([...(rowsSelected || []), item]);
      } else {
        onRowsSelectionChange((rowsSelected || []).filter((selectedRow) => !isRowEquals(selectedRow, item)));
      }
    }
  };

  const handleSelectionAll = () => {
    if (onRowsSelectionChange) {
      onRowsSelectionChange(displayData);
    }
  };

  const handleUnSelectionAll = () => {
    if (onRowsSelectionChange) {
      onRowsSelectionChange([]);
    }
  };

  const handleToolbarSelectAll = () => {
    if (onSelectAll) {
      setPage(0);
      setSearchText('');
      onSelectAll();
    }
  };

  const handleToolbarClearSelection = () => {
    if (onClearSelection) {
      setPage(0);
      setSearchText('');
      onClearSelection();
    }
  };

  const handleToolBarShowSelected = (show: boolean) => {
    if (onShowSelected) {
      setPage(0);
      setSearchText('');
      onShowSelected(show);
    }
  };

  const handleRowClick = (row: any, e: React.MouseEvent<HTMLTableRowElement, MouseEvent>) => {
    if (onRowClick) {
      onRowClick(row, e);
    }
  };

  const handleSortColumn = (column: any, direction: SortDirection) => {
    if (onSortColumn) {
      onSortColumn(column, direction);
    }
  };

  const searchComponent = (
    <DebouncedSearchInput
      onChange={handleSearchTextChange}
      wait={onRunSearch ? 1000 : 100}
      value={searchText}
      searchIconFirst={searchIconFirst}
    />
  );
  return (
    <div className={classes.root}>
      {(search || search === undefined) && !selectable ? (
        <Box>{searchComponent}</Box>
      ) : topBarComponent ? (
        topBarComponent(searchComponent)
      ) : null}

      {selectable && displayData.length > 0 && (
        <TableToolbar
          showSelectedNotice={showSelectedNotice}
          data={data}
          searchComponent={selectable && !noTableToolbarSearch ? searchComponent : null}
          showSelectedRows={showSelectedRows}
          rowsTotal={onRunSearch ? rowsTotal : data.length}
          rowsSelected={rowsSelected}
          onResetFilter={onResetFilter}
          onResetFields={onResetFields}
          onShowSelected={handleToolBarShowSelected}
          onClearSelection={handleToolbarClearSelection}
          onSelectAll={onSelectAll ? handleToolbarSelectAll : undefined}
          showSelectionCheckBox={showSelectionCheckBox}
        />
      )}
      <TableContainer
        className={tableContainerProps ? classnames(classes.tableWrapper, tableContainerProps) : classes.tableWrapper}
      >
        {loading ? (
          <Loader />
        ) : error ? (
          <ErrorPage />
        ) : displayData.length === 0 ? (
          <Box display="flex" justifyContent="center">
            {emptyLabel || 'Aucun résultat correspondant'}
          </Box>
        ) : (
          <div ref={componentTable}>
            <Table className={classes.table} aria-labelledby="tableTitle" stickyHeader size="small" style={style}>
              <TableHead
                headerColumns={headerColumns}
                columns={columns}
                selectable={selectable}
                selectedAll={rowsSelected && rowsSelected.length === data.length}
                onSelectAll={handleSelectionAll}
                onUnselectAll={handleUnSelectionAll}
                onSortColumn={handleSortColumn}
                order={sorting && sorting[0] && sorting[0].direction}
                orderBy={sorting && sorting[0] && sorting[0].field}
                showSelectionCheckBox={showSelectionCheckBox}
              />
              <TableBody className={tableBodyClassName}>
                {displayData.map((row: TableRowData, index: number) => {
                  const rowKey = dataIdName ? row[dataIdName] : index;
                  const selected = !!rowsSelected?.some((selectedRow) => isRowEquals(selectedRow, row));
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={rowKey}
                      onClick={(e) => {
                        handleRowClick(row, e);
                      }}
                    >
                      {selectable && showSelectionCheckBox ? (
                        <TableCell padding="checkbox" key={rowKey}>
                          <Checkbox
                            onClick={() => handleSelection(row, !selected)}
                            checked={selected}
                            inputProps={{ 'aria-labelledby': rowKey }}
                          />
                        </TableCell>
                      ) : null}

                      {columns
                        ? columns.map((column, columnIndex) => {
                            return (
                              <TableCell
                                variant="body"
                                component={column.component}
                                scope={column.scope}
                                key={`${rowKey}-${columnIndex}`}
                                className={classenames(classes.tableCellPadding, column.className)}
                                align={column.centered ? 'center' : 'left'}
                                style={column.style ? { ...tableCellStyle, ...column.style } : tableCellStyle}
                              >
                                {column.renderer
                                  ? column.renderer(row, index)
                                  : row[column.name]
                                  ? row[column.name]
                                  : '-'}
                              </TableCell>
                            );
                          })
                        : null}
                    </TableRow>
                  );
                })}
                {additionalRows}
              </TableBody>
            </Table>
          </div>
        )}
      </TableContainer>

      {displayData.length > 0 ? (
        <TablePagination
          style={{ display: notablePagination ? 'none' : 'block' }}
          rowsPerPageOptions={rowsPerPageOptions || ROWS_PER_PAGE_OPTIONS}
          labelDisplayedRows={({ from, to, count }) =>
            `${from}-${to === -1 ? count : to} sur ${count !== -1 ? count : to}`
          }
          labelRowsPerPage="Nombre par page"
          component="div"
          count={isServerSideSearch() ? rowsTotal || 0 : filteredRowsTotal}
          rowsPerPage={rowsPerPage}
          page={page}
          backIconButtonProps={{
            'aria-label': 'Page Précédente',
          }}
          nextIconButtonProps={{
            'aria-label': 'Page suivante',
          }}
          onChangePage={(_, newPage) => handlePageChange(newPage)}
          onChangeRowsPerPage={(e) => handleRowsPerPageChange(parseInt(e.target.value, 10))}
          ActionsComponent={TablePaginationActions}
          className={paginationPropsClassName || ''}
        />
      ) : (
        ''
      )}
    </div>
  );
};

export default EnhancedTable;
