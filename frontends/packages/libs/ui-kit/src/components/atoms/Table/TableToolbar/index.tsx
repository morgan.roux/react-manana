import React, { FC, useState, useEffect, ReactNode } from 'react';
import ListIcon from '@material-ui/icons/List';
import ListAltIcon from '@material-ui/icons/ListAlt';
import classnames from 'classnames';
import { useToolbarStyles } from './styles';
import { Hidden, Typography } from '@material-ui/core';
import ReplayIcon from '@material-ui/icons/Replay';
import { TableToolbarProps } from '../interfaces';
import { Delete, Tune } from '@material-ui/icons';
import { NewCustomButton } from '../../CustomButton';
import { CustomCheckBox } from '../../CustomCheckBox';
import { CustomModal } from '../../CustomModal';

const EnhancedTableToolbar: FC<TableToolbarProps & { searchComponent?: ReactNode; showSelectionCheckBox?: boolean }> =
  ({
    data,
    showSelectedRows,
    rowsTotal,
    rowsSelected,
    //  onRunSearch,
    onResetFilter,
    //  onResetFields,
    onShowSelected,
    onSelectAll,
    onClearSelection,
    searchComponent = null,
    showSelectionCheckBox,
    showSelectedNotice = true,
  }) => {
    const classes = useToolbarStyles();

    const [checkedAll, setCheckedAll] = useState<boolean>(
      (rowsSelected && (rowsTotal || 0) > 0 && rowsSelected.length === (rowsTotal || 0)) || false
    );

    const [openFilter, setOpenFilter] = useState<boolean>(false);

    useEffect(() => {
      if (rowsSelected && rowsSelected.length > 0 && rowsSelected.length === rowsTotal) {
        setCheckedAll(true);
      } else {
        setCheckedAll(false);
      }
    }, [rowsSelected]);

    const total = data.length;

    /*
  const handleResetFields = () => {
    if (onResetFields) {
      onResetFields();
      return;
    }
    //setFieldsState({});
  };
  */

    const handleResetFilter = () => {
      if (onResetFilter) {
        onResetFilter();
        return;
      }
      //setFieldsState({});
    };

    const handleCheckAll = () => {
      if (onSelectAll) {
        onSelectAll();
        return;
      } else {
        setCheckedAll((prev) => !prev);
      }
    };

    const handleOpenFilter = () => {
      setOpenFilter(true);
    };

    const handleShowCheckeds = () => {
      if (onShowSelected) {
        onShowSelected(!showSelectedRows);
      }
    };

    // const onClickRefresh = () => {
    //   if (listResult && listResult.refetch) {
    //     listResult.refetch();
    //   }
    // };

    // const [fieldsState, setFieldsState] = useState<any>({});

    // const handleFieldChange = (event: any): void => {
    //   setFieldsState({
    //     ...fieldsState,
    //     [event.target.name]:
    //       event.target.name === 'actived' || event.target.name === 'pharmacies.actived'
    //         ? event.target.value !== -1
    //           ? event.target.value === 1
    //             ? true
    //             : false
    //           : undefined
    //         : event.target.value,
    //   });
    // };

    // const handleRunSearch = (query: any) => {
    //   if (onRunSearch) {
    //     onRunSearch();
    //   } else {
    //   }
    // };

    const handleClearSelection = () => {
      if (onClearSelection) {
        onClearSelection();
      }
    };

    const filter = '';

    const numSelected = (): number => {
      // if (selected && setSelected) {
      //   return selected.length > total ? total : selected.length;
      // }
      return (rowsSelected && rowsSelected.length) || 0;
    };

    return (
      <div className={classnames(classes.root)}>
        {/* {filter} */}
        {searchComponent && showSelectedNotice && (
          <div className={classes.title}>
            {searchComponent ? (
              searchComponent
            ) : (
              <Typography color="inherit" variant="subtitle1">
                {`${numSelected()}/${rowsTotal}`} {numSelected() > 1 ? 'sélectionnés' : 'sélectionné'} {`(${total})`}
              </Typography>
            )}
          </div>
        )}
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          <div className={classes.buttons}>
            {showSelectionCheckBox && onSelectAll && (
              <CustomCheckBox
                variant="text"
                color="inherit"
                size="medium"
                name="checkedAll"
                label={`Tout sélectionner (${rowsTotal})`}
                value={checkedAll}
                checked={checkedAll}
                onChange={handleCheckAll}
              />
            )}
            {filter && (
              <NewCustomButton
                variant="text"
                theme="transparent"
                shadow={false}
                size="medium"
                startIcon={<Tune />}
                className={classes.btn}
                onClick={handleOpenFilter}
              >
                Filtres
              </NewCustomButton>
            )}

            {onResetFilter && (
              <NewCustomButton
                variant="text"
                theme="transparent"
                shadow={false}
                size="medium"
                startIcon={<ReplayIcon />}
                className={classes.btn}
                onClick={handleResetFilter}
              >
                Réinitialiser les filtres
              </NewCustomButton>
            )}
            {rowsSelected && numSelected() > 0 && (
              <>
                <NewCustomButton
                  variant="text"
                  theme="transparent"
                  shadow={false}
                  size="medium"
                  startIcon={showSelectedRows ? <ListIcon /> : <ListAltIcon />}
                  className={classes.btn}
                  onClick={handleShowCheckeds}
                >
                  <Hidden mdDown>
                    {showSelectedRows ? 'Afficher les résultats' : 'Afficher les éléments selectionnés'}
                  </Hidden>
                </NewCustomButton>

                <NewCustomButton
                  variant="text"
                  theme="transparent"
                  shadow={false}
                  size="medium"
                  startIcon={<Delete />}
                  className={classes.btn}
                  onClick={handleClearSelection}
                >
                  <Hidden mdDown>Vider la sélection</Hidden>
                </NewCustomButton>
              </>
            )}
            {/*<CustomButton
            variant="text"
            color="inherit"
            size="medium"
            startIcon={<Refresh />}
            className={classes.btn}
            //onClick={onClickRefresh}
          >
            Réactualiser
          </CustomButton>*/}
          </div>
        </div>
        <CustomModal
          title="Filtres de recherche"
          children={filter}
          open={openFilter}
          setOpen={setOpenFilter}
          withBtnsActions={false}
          headerWithBgColor={true}
          closeIcon={true}
        />
      </div>
    );
  };

export default EnhancedTableToolbar;
