import { createStyles, Theme, makeStyles, lighten } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      background: lighten(theme.palette.primary.main, 0.1),
      '& .MuiTableSortLabel-root.MuiTableSortLabel-active, & .MuiTableSortLabel-icon': {
        color: `${theme.palette.common.white} !important`,
      },
      '& .MuiTableCell-root': {
        padding: '0px 16px',
      },
      '& .MuiTableCell-head': {
        lineHeight: '1rem',
        height: 40,
      },
    },
    loader: {
      color: theme.palette.common.white,
      marginLeft: 12,
    },
    label: {
      flexDirection: 'unset',
      color: theme.palette.common.white,
      background: lighten(theme.palette.primary.main, 0.1),
      fontSize: 12,
      '&:hover': {
        color: theme.palette.common.white,
        opacity: '0.6',
      },
    },
  })
);

export default useStyles;
