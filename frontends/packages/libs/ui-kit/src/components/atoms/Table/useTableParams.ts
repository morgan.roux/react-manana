import { useURLSearchParams } from '@lib/common';
import { SortNulls, SortDirection } from '@lib/common/src/federation';
import { useMemo } from 'react';

export interface TableURLParams {
  take?: string;
  skip?: string;
  searchText?: string;
  sorting?: Array<{ field: any; direction: SortDirection; nulls?: SortNulls }>;
}

export interface UseTableURLParamsResult {
  params: TableURLParams;
}

const decodeParam = (
  params: URLSearchParams,
  name: keyof TableURLParams,
  isArray: boolean,
  isObjectArray?: boolean
): any | undefined => {
  const value = params.get(name);
  if (value) {
    const decodedValue = decodeURIComponent(value);
    if (isObjectArray) {
      return JSON.parse(`[${decodedValue}]`);
    }
    if (isArray) {
      return decodedValue.split(',');
    }
    return decodedValue;
  }

  return undefined;
};

export const useTableParams = (): UseTableURLParamsResult => {
  const query = useURLSearchParams();

  const params: TableURLParams = useMemo(
    () => ({
      take: decodeParam(query, 'take', false),
      skip: decodeParam(query, 'skip', false),
      searchText: decodeParam(query, 'searchText', false),
      sorting: decodeParam(query, 'sorting', true, true),
    }),
    [query]
  );

  return {
    params,
  };
};
