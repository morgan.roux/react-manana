import React, { FC } from 'react';
import { TableHead, TableRow, TableCell, Checkbox, TableSortLabel } from '@material-ui/core';
import { Column } from '../interfaces';
import useStyles from './styles';
import { ArrowDropDown, ArrowDropUp } from '@material-ui/icons';
import { SortDirection } from '@lib/common/src/federation';
import classNames from 'classnames';

const IconSortable: FC = () => {
  return (
    <span
      style={{ marginTop: 5 }}
      dangerouslySetInnerHTML={{
        __html:
          '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><defs><style>.a{fill:#fff;}.b{fill:none;}</style></defs><path class="a" d="M7,10l5,5,5-5Z" transform="translate(0 3)"/><path class="b" d="M0,0H24V24H0Z"/><path class="a" d="M7,14l5-5,5,5Z" transform="translate(0 -4)"/></svg>',
      }}
    />
  );
};

const IconDesc: FC = () => {
  return <ArrowDropDown style={{ width: 24, height: 24 }} />;
};

const IconAsc: FC = () => {
  return <ArrowDropUp style={{ width: 24, height: 24 }} />;
};
export interface TableHeadProps {
  columns: Column[];
  headerColumns?: Column[];
  selectable?: boolean;
  selectedAll?: boolean;
  orderBy?: string;
  order?: SortDirection;
  onSelectAll?: () => void;
  onUnselectAll?: () => void;
  onSortColumn?: (column: any, order: SortDirection) => void;
  showSelectionCheckBox?: boolean;
}

const EnhancedTableHead: FC<TableHeadProps> = ({
  columns,
  headerColumns,
  selectable,
  selectedAll,
  onSelectAll,
  onUnselectAll,
  orderBy,
  order,
  onSortColumn,
  showSelectionCheckBox,
}) => {
  const classes = useStyles();

  const handleChange = () => {
    if (selectedAll && onUnselectAll) onUnselectAll();
    else if (!selectedAll && onSelectAll) onSelectAll();
  };

  const handleChangeSort = (nameColumn: any) => {
    if (onSortColumn)
      onSortColumn(
        nameColumn,
        order ? (order === SortDirection.Asc ? SortDirection.Desc : SortDirection.Asc) : SortDirection.Desc
      );
  };

  return (
    <TableHead className={classes.root}>
      <TableRow>
        {/* selectable ? (
          <TableCell style={{ padding: '0px 4px' }}>
            <Checkbox
              checked={selectedAll}
              onChange={handleChange}
              inputProps={{ 'aria-label': 'Select all desserts' }}
              style={{ color: '#ffffff' }}
            />
          </TableCell>
        ) : null */}
        {headerColumns
          ? (headerColumns || []).map((column, index) => (
              <TableCell
                className={classes.label}
                key={`column-${index}`}
                // align={column.centered ? 'center' : 'left'}
                align={column.align}
                colSpan={column.colSpan}
              >
                {column.label}
              </TableCell>
            ))
          : null}
      </TableRow>
      <TableRow>
        {selectable && showSelectionCheckBox ? (
          <TableCell style={{ padding: '0px 4px', background: 'none' }}>
            <Checkbox
              checked={selectedAll}
              onChange={handleChange}
              inputProps={{ 'aria-label': 'Select all desserts' }}
              style={{ color: '#ffffff' }}
            />
          </TableCell>
        ) : null}

        {columns
          ? (columns || []).map((column, index) => (
              <TableCell
                className={classNames(classes.label, column.headerClassName)}
                key={`column-${index}`}
                // align={column.centered ? 'center' : 'left'}
                align={column.align}
                style={column.headerStyle}
                colSpan={column.headerColSpan}
              >
                {column.sortable ? (
                  <TableSortLabel
                    active={orderBy === column.name}
                    onClick={() => handleChangeSort(column.name)}
                    IconComponent={
                      orderBy && orderBy === column.name ? (order === 'ASC' ? IconDesc : IconAsc) : IconSortable
                    }
                  >
                    {column.label}
                  </TableSortLabel>
                ) : (
                  column.label
                )}
              </TableCell>
            ))
          : null}
      </TableRow>
    </TableHead>
  );
};

export default EnhancedTableHead;
