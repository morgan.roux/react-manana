import React, { useState } from 'react';
import Table from './Table';
import { Column, TableRowData, TableChange } from './interfaces';

export default {
  title: 'Design system|Atoms/Table',
  parameters: {
    info: { inline: true },
  },
};

export const defaultTable = () => {
  const columns: Column[] = [
    { name: 'column1', label: 'Column1', sortable: true },
    { name: 'column2', label: 'Column2' },
  ];

  const data: TableRowData[] = [];
  for (let index = 0; index < 100; index++) {
    data.push({ column1: `row${index}col1`, column2: `row${index}col2` });
  }

  return <Table columns={columns} data={data} />;
};

const columns: Column[] = [
  { name: 'column1', label: 'Column1', sortable: true },
  { name: 'column2', label: 'Column2' },
];

const data: TableRowData[] = [];
for (let index = 0; index < 20; index++) {
  data.push({ column1: `row${index}col1`, column2: `row${index}col2` });
}

export const selectableTable = () => {
  const [rowsSelected, setRowsSelected] = useState<TableRowData[]>([]);

  const [showSelected, setShowSelected] = useState<boolean>(false);

  const onClearSelection = () => {
    setRowsSelected([]);
  };

  const onShowSelected = () => {
    setShowSelected(!showSelected);
  };

  const onSelectAll = () => {
    if (rowsSelected.length === data.length) {
      setRowsSelected([]);
    } else {
      setRowsSelected(data);
    }
  };

  const handleSortColumn = (columns: string, order: any) => {
    console.log(columns, order);
  };

  return (
    <Table
      selectable
      rowsSelected={rowsSelected}
      onRowsSelectionChange={setRowsSelected}
      columns={columns}
      data={showSelected ? rowsSelected : data}
      onClearSelection={onClearSelection}
      onShowSelected={onShowSelected}
      showSelected={showSelected}
      onSelectAll={onSelectAll}
      rowsTotal={data.length}
      onSortColumn={handleSortColumn}
    />
  );
};

export const serverSideTable = () => {
  const [rowsSelected, setRowsSelected] = useState<TableRowData[]>([]);
  const [pageData, setPageData] = useState<TableRowData[]>([]);
  const [loadingPageData, setLoadingPageData] = useState<boolean>(false);

  const [showSelected, setShowSelected] = useState<boolean>(false);

  const onClearSelection = () => {
    setRowsSelected([]);
  };

  const onShowSelected = () => {
    setShowSelected(!showSelected);
  };

  const onSelectAll = () => {
    if (rowsSelected.length === data.length) {
      setRowsSelected([]);
    } else {
      setRowsSelected(data);
    }
  };

  const handleSearch = (change: TableChange) => {
    setLoadingPageData(true);

    setTimeout(() => {
      const newData = [];
      for (let index = change.skip; index < change.skip + change.take; index++) {
        newData.push({ column1: `row${index}col1`, column2: `row${index}col2` });
      }

      setPageData(newData);
      setLoadingPageData(false);
    }, 300);
  };

  return (
    <Table
      loading={loadingPageData}
      data={pageData}
      selectable
      rowsSelected={rowsSelected}
      onRowsSelectionChange={setRowsSelected}
      columns={columns}
      onClearSelection={onClearSelection}
      onShowSelected={onShowSelected}
      showSelected={showSelected}
      onSelectAll={onSelectAll}
      onRunSearch={handleSearch}
      rowsTotal={200}
    />
  );
};
