import { TableRowData } from './interfaces';

function isPrimitive(test: any): boolean {
  return test !== Object(test);
}

export const filtered = (rowData: TableRowData, searchText: string): boolean => {
  const normalizedSearchText = searchText.trim().toLowerCase();
  if (!normalizedSearchText) {
    return true;
  }
  return Object.values(rowData).some(
    (columnValue) =>
      isPrimitive(columnValue) && `${columnValue}`.toLowerCase().indexOf(normalizedSearchText) >= 0,
  );
};
