import React, { FC } from 'react';
import { createStyles, makeStyles, withStyles } from '@material-ui/core/styles';
import Menu, { MenuProps } from '@material-ui/core/Menu';
import { MenuItem, ListItemIcon, ListItemText, IconButton } from '@material-ui/core';
import RepeatIcon from '@material-ui/icons/Repeat';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import SemaineIcon from '@material-ui/icons/Event';
import DateRangeIcon from '@material-ui/icons/DateRange';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
const StyledMenu = withStyles({
  paper: {},
})((props: MenuProps) => (
  <Menu
    elevation={1}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    MenuListProps={{
      style: { padding: 0 },
    }}
    {...props}
  />
));
const useStyles = makeStyles(() =>
  createStyles({
    rootIcon: {
      minWidth: 35,
      color: '#000',
    },
  })
);
interface RepeatTimeProps {
  iconColor?: string;
  repeatIcon?: any;
}
export const RepeatTime: FC<RepeatTimeProps> = ({ iconColor, repeatIcon }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <IconButton aria-controls="customized-menu" aria-haspopup="true" color="primary" onClick={handleClick}>
        {repeatIcon || <RepeatIcon style={iconColor ? { color: iconColor } : {}} />}
      </IconButton>
      <StyledMenu id="customized-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
        <MenuItem>
          <ListItemIcon classes={{ root: classes.rootIcon }}>
            <CalendarTodayIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Tous les jours" />
        </MenuItem>
        <MenuItem>
          <ListItemIcon classes={{ root: classes.rootIcon }}>
            <CalendarTodayIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Tous les jours" />
        </MenuItem>
        <MenuItem>
          <ListItemIcon classes={{ root: classes.rootIcon }}>
            <SemaineIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Jours de la semaine" />
        </MenuItem>
        <MenuItem>
          <ListItemIcon classes={{ root: classes.rootIcon }}>
            <DateRangeIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Toutes les semaines" />
        </MenuItem>
        <MenuItem>
          <ListItemIcon classes={{ root: classes.rootIcon }}>
            <EventAvailableIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Tous les mois" />
        </MenuItem>
        <MenuItem>
          <ListItemIcon classes={{ root: classes.rootIcon }}>
            <EventAvailableIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Tous les ans" />
        </MenuItem>
        <MenuItem>
          <ListItemIcon classes={{ root: classes.rootIcon }}>
            <EventAvailableIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText primary="Personnalise" />
        </MenuItem>
      </StyledMenu>
    </div>
  );
};
