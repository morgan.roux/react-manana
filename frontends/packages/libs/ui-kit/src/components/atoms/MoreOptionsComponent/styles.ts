import { makeStyles, Theme, createStyles } from '@material-ui/core';
//const drawerWidth = 275;
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    more: {
      position: 'relative',
      zIndex: 1,
    },
    menu: {
      marginBottom: theme.spacing(1),
      '& svg': {
        marginRight: theme.spacing(1),
        fontSize: '1.25rem',
      },
    },
  })
);
