import React, { MouseEvent, useState, FC } from 'react';
import { IconButton, Menu, MenuItem, Typography } from '@material-ui/core';
import { stopEvent } from '../../../services/DOMEvent';
import { useStyles } from './styles';
import { MoreHoriz } from '@material-ui/icons';

export interface IMenuItem {
  title: string;
  icon: any;
}

export interface MoreOptionsItem {
  menuItemLabel: IMenuItem;
  disabled?: boolean;
  onClick: (event: MouseEvent<any>) => void;
}

export interface MoreOptionsProps {
  items: MoreOptionsItem[];
  //active?: boolean;
}

const MoreOptions: FC<MoreOptionsProps> = ({ items /*, active*/ }) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  // const ITEM_HEIGHT = 20;
  const classes = useStyles();
  const handleClick = (event: MouseEvent<any>) => {
    stopEvent(event);
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (event: MouseEvent<any>) => {
    stopEvent(event);
    setAnchorEl(null);
  };

  const handleClickItem = (event: MouseEvent<any>, item: MoreOptionsItem) => {
    handleClose(event);
    event.stopPropagation();
    item.onClick(event);
  };

  return (
    <>
      <IconButton onClick={handleClick} className={classes.more}>
        <MoreHoriz htmlColor="#000" />
      </IconButton>
      <Menu
        id="more-options-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        getContentAnchorEl={null}
        // PaperProps={{
        //   style: {
        //     maxHeight: ITEM_HEIGHT * 4.5,
        //   },
        // }}
      >
        {items.map((item: MoreOptionsItem) => (
          <MenuItem
            className={classes.menu}
            key={`more-options-item-key${item.menuItemLabel.title}`}
            onClick={(event) => handleClickItem(event, item)}
            disabled={item.disabled}
          >
            {item.menuItemLabel.icon}
            <Typography variant="subtitle2">{item.menuItemLabel.title}</Typography>
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

export default MoreOptions;
