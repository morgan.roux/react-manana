import * as React from 'react';
import { SchedulerForm } from '@progress/kendo-react-scheduler';
import { CustomFormDialog } from './CustomFormDialog';

export const FormWithCustomDialog = (props: any) => {
  return <SchedulerForm {...props} dialog={CustomFormDialog} />;
};
