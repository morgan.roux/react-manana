import React, { useState } from 'react';
import { FormGroup, FormControlLabel } from '@material-ui/core';

import {
  // useSchedulerEditItemPropsContext,
  useSchedulerEditItemRemoveItemContext,
  useSchedulerEditItemFormItemContext,
  useSchedulerEditItemShowRemoveDialogContext,
} from '@progress/kendo-react-scheduler';
import useStyles from './styles';

export const CustomFormDialog = () => {
  // const editItemProps = useSchedulerEditItemPropsContext();
  const [, setRemoveItem] = useSchedulerEditItemRemoveItemContext();
  const [, setFormItem] = useSchedulerEditItemFormItemContext();
  const [, setShowRemoveItemDialog] = useSchedulerEditItemShowRemoveDialogContext();

  const handleRemoveClick = (event: any) => {
    // Sets the item to be removed
    setRemoveItem(event);
    // Closes the form
    setFormItem(event);
    // Shows the remove confirmation dialog
    setShowRemoveItemDialog(true);
  };

  return null;
};
