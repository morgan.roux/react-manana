import React, { FC, useState } from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Card,
  Grid,
  Paper,
  ButtonGroup,
  Checkbox,
  FormControlLabel,
  CardContent,
  Typography,
  Avatar,
} from '@material-ui/core';
import { TimeOnlyPicker } from '../../../TimeOnlyPicker/TimeOnlyPicker';
import { ButtonWeekDay } from './ButtonWeekDay';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      minWidth: 275,
    },
    flexCentered: {
      display: 'flex',
      alignItems: 'center',
    },
    paper: {
      padding: theme.spacing(0),
      marginTop: theme.spacing(2),
      textAlign: 'left',
      color: theme.palette.text.secondary,
    },
    putButtonMargin: {
      '& button': {
        marginRight: 10,
      },
    },
    firstCildMR10: {
      '& .MuiFormControl-root:first-child': {
        marginRight: 10,
      },
    },
  })
);
interface CalendarVaccinationListProps {
  nom?: string;
  prenom?: string;
  avatarUrl?: string;
  workday?: any;
  onChange?: (date: string | Date | null, value?: string | null | undefined, name?: string) => void;
  defaultDebut?: Date | null;
  defaultFin?: Date | null;
}
export const CalendarVaccinationList: FC<CalendarVaccinationListProps> = (props: CalendarVaccinationListProps) => {
  const classes = useStyles();
  const [state, setState] = useState({
    checkbox: false,
  });
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState({ ...state, checkbox: event.target.checked });
  };
  const { nom, prenom, avatarUrl, workday, onChange, defaultDebut, defaultFin } = props;
  const MapWeekDay = workday || [];
  const handleChangeTime = (date: string | Date | null, value?: string | null | undefined, name?: string) => {
    if (onChange) {
      onChange(date, value, name);
    }
  };
  return (
    <Card className={classes.root} variant="outlined">
      <CardContent>
        <Typography gutterBottom component="div">
          <div className={classes.flexCentered}>
            <Avatar alt={nom || ''} src={avatarUrl} style={{ marginRight: 20 }} />
            <span>{`${prenom || ''} ${nom || ''}`}</span>
          </div>
        </Typography>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <Paper className={classes.paper} elevation={0} style={{ borderRight: '2px solid #ccc', borderRadius: '0' }}>
              <FormControlLabel
                control={<Checkbox checked={state.checkbox} onChange={handleChange} name="touslesjours" />}
                label="Tous les jours de travail"
              />
            </Paper>
          </Grid>
          <Grid item xs={8}>
            <Paper className={classes.paper} elevation={0}>
              <Grid>
                <ButtonGroup className={classes.putButtonMargin}>
                  {MapWeekDay.map((item: any, index: number) => {
                    return <ButtonWeekDay day={item} key={index} />;
                  })}
                </ButtonGroup>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
        <Grid container spacing={3}>
          <Grid item xs={8}>
            <Paper className={classes.paper} elevation={0}>
              <div style={{ display: 'flex' }} className={classes.firstCildMR10}>
                <TimeOnlyPicker
                  label="Heure de début"
                  defaultHours={defaultDebut}
                  name="debut"
                  onChange={handleChangeTime}
                />
                <TimeOnlyPicker label="Heure de fin" defaultHours={defaultFin} name="fin" onChange={handleChangeTime} />
              </div>
            </Paper>
          </Grid>
          <Grid item xs={4} />
        </Grid>
      </CardContent>
    </Card>
  );
};
