export const WeekDay = [
  { day: 'Lun.', index: 1, workday: true },
  { day: 'Mar.', index: 2, workday: true },
  { day: 'Mer.', index: 3, workday: true },
  { day: 'Jeu.', index: 4, workday: true },
  { day: 'Ven.', index: 5, workday: true },
  { day: 'Sam.', index: 6, workday: false },
  { day: 'Dim.', index: 7, workday: false },
];
