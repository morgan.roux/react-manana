/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable prettier/prettier */
import React, { Dispatch, useState } from 'react';
import { Typography, Container, Grid, Paper, ButtonGroup } from '@material-ui/core';
import { useStyles } from './style';
import CustomContainer from '../../../templates/CustomContainer/CustomContainer';
import { TimeOnlyPicker } from '../../TimeOnlyPicker/TimeOnlyPicker';
import { WeekDay } from './utils/weekDay';
import { TreeViewCheckbox } from './utils/TreeViewCheckbox';
import { ButtonWeekDay } from './utils/ButtonWeekDay';
import { SelectFirstDay } from './utils/SelectFirstDay';
import PeopleIcon from '@material-ui/icons/People';
import { InputReunionDuree } from './utils/InputReunionDuree';
import { CalendarVaccinationList } from './utils/CalendarVaccinationList';
import { SchedulerParameter } from '../Scheduler';

interface CalendarPlanningSettingProps {
  heureTravailDebutOnChange?: (date: Date | null | string, value?: string | null, name?: string) => void;
  heureTravailFinOnChange?: (date: Date | null | string, value?: string | null, name?: string) => void;
  heureTravailDefaultDebut?: Date | null;
  heureTravailDefaultFin?: Date | null;
  vaccinationList?: Array<any>;
  dureeReunionDefault?: number;
  dureeReunionOnChange?: (value: number) => void;
  semainDeTravail?: Array<any>;
  setOpenDrawer: Dispatch<boolean>;
}
export const CalendarPlanningSetting = (props: CalendarPlanningSettingProps) => {
  const classes = useStyles();

  const {
    vaccinationList,
    heureTravailDebutOnChange,
    heureTravailFinOnChange,
    heureTravailDefaultDebut,
    heureTravailDefaultFin,
    dureeReunionDefault,
    dureeReunionOnChange,
  } = props;
  const handleGoBack = () => {
    setGoBackToPlanning(!goBackToPlanning);
  };
  const renderListVaccination = () => {
    if (vaccinationList) {
      return vaccinationList.map((person, index) => {
        return (
          <CalendarVaccinationList
            key={index}
            workday={person.workday}
            nom={person.nom}
            prenom={person.prenom}
            defaultDebut={person.defaultDebut}
            defaultFin={person.defaultFin}
            onChange={person.onChange}
          />
        );
      });
      // eslint-disable-next-line no-useless-return
    } else return;
  };

  const [goBackToPlanning, setGoBackToPlanning] = useState(false);
  return (
    <>
      {goBackToPlanning ? (
        <SchedulerParameter setOpenDrawer={props.setOpenDrawer} />
      ) : (
        <div className={classes.root}>
          <CustomContainer
            filled={false}
            bannerContentStyle={{
              justifyContent: 'center',
            }}
            bannerBack
            onBackClick={handleGoBack}
            bannerTitle="Paramètres calendrier"
            contentStyle={{
              width: '100%',
            }}
            onlyBackAndTitle
          >
            <Container maxWidth="sm">
              <div style={{ marginBottom: '35px' }}>
                <Typography component="div" className={classes.titleSize20}>
                  Horaire de travail
                </Typography>
                <Typography className={classes.innerTextTitle} component="div">
                  Heures de travail
                </Typography>
                <Grid container spacing={3}>
                  <Grid item xs={6}>
                    <Paper className={classes.paper} elevation={0}>
                      <TimeOnlyPicker
                        label="Heure de début"
                        name="heure_de_debut"
                        defaultHours={heureTravailDefaultDebut}
                        onChange={heureTravailDebutOnChange}
                      />
                    </Paper>
                  </Grid>
                  <Grid item xs={6}>
                    <Paper className={classes.paper} elevation={0}>
                      <TimeOnlyPicker
                        label="Heure de fin"
                        name="heure_de_fin"
                        defaultHours={heureTravailDefaultFin}
                        onChange={heureTravailFinOnChange}
                      />
                    </Paper>
                  </Grid>
                </Grid>
                <Typography className={classes.innerTextTitle} component="div">
                  Semaine de travail
                </Typography>
                <Grid>
                  <ButtonGroup className={classes.buttonGroup}>
                    {WeekDay.map((item, index) => {
                      return <ButtonWeekDay day={item} key={index} />;
                    })}
                  </ButtonGroup>
                </Grid>
                <Typography className={classes.innerTextTitle} component="div">
                  Premier jour de la semaine
                </Typography>
                <div>
                  <SelectFirstDay />
                </div>
              </div>
              <div style={{ marginBottom: '35px' }}>
                <Typography component="div" className={classes.titleSize20}>
                  Paramètre par defaut du calendrier
                </Typography>
                <Typography className={classes.innerTextTitle} component="div">
                  Choisir les calendriers
                </Typography>
                <div>
                  <TreeViewCheckbox />
                </div>
                <div>
                  <Typography className={classes.innerTextTitle} component="div">
                    Choisir les personnes
                  </Typography>
                  <Typography component="div" className={classes.choisirPersonne}>
                    <PeopleIcon className={classes.svgPeople} /> Tout le monde
                  </Typography>
                </div>
              </div>
              <div style={{ marginBottom: '35px' }}>
                <Typography component="div" className={classes.titleSize20}>
                  Options du calendrier
                </Typography>
                <Typography className={classes.innerTextTitle} component="div">
                  Durée par défaut des nouveaux rendez-vous
                </Typography>
                <div className={classes.flexCenter}>
                  Réunion
                  <InputReunionDuree value={dureeReunionDefault} onChange={dureeReunionOnChange} />
                  <span style={{ fontWeight: 'bold' }}> mn</span>
                </div>
                <Typography className={classes.innerTextTitle} component="div">
                  Vaccination
                </Typography>
                <div style={{ marginTop: '20px', marginBottom: '20px' }}>{renderListVaccination()}</div>
              </div>
            </Container>
          </CustomContainer>
        </div>
      )}
    </>
  );
};
