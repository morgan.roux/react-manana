import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import { TreeItemCheckbox } from './TreeItemCheckbox';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
    '@media (min-width: 767px)': {
      '& .MuiCollapse-container li.MuiTreeItem-root': {
        display: 'inline-block',
        width: '50%',
      },
    },
  },
});

export const TreeViewCheckbox = () => {
  const classes = useStyles();
  const [isCheck, setIsCheck] = useState({
    agendaEquipe: false,
    gardes: true,
    vacances: true,
    horaireOuv: true,
    formations: false,
    deplacement: false,
    partnerExt: false,
    labosPartner: true,
    partnerServ: true,
    servicesMed: false,
    gestionVacc: true,
    agendaPrive: false,
  });
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
    const name = event.target.name;
    setIsCheck((prev) => {
      if (name === 'agendaEquipe' && prev.agendaEquipe) {
        return {
          ...prev,
          agendaEquipe: false,
          gardes: false,
          horaireOuv: false,
          vacances: false,
          deplacement: false,
          formations: false,
        };
      } else if (name === 'agendaEquipe' && !prev.agendaEquipe) {
        return {
          ...prev,
          agendaEquipe: true,
          gardes: true,
          horaireOuv: true,
          vacances: true,
          deplacement: true,
          formations: true,
        };
      } else if (name === 'partnerExt' && !prev.partnerExt) {
        return {
          ...prev,
          partnerExt: true,
          labosPartner: true,
          partnerServ: true,
        };
      } else if (name === 'partnerExt' && prev.partnerExt) {
        return {
          ...prev,
          partnerExt: false,
          labosPartner: false,
          partnerServ: false,
        };
      } else if (name === 'servicesMed' && prev.servicesMed) {
        return {
          ...prev,
          gestionVacc: false,
          servicesMed: false,
        };
      } else if (name === 'servicesMed' && !prev.servicesMed) {
        return {
          ...prev,
          gestionVacc: true,
          servicesMed: true,
        };
      } else {
        return { ...prev, [name]: checked };
      }
    });
  };

  return (
    <TreeView className={classes.root} defaultExpanded={['1', '7', '10']}>
      <TreeItemCheckbox
        onChangeCheckbox={handleChange}
        checked={isCheck.agendaEquipe}
        nodeId="1"
        labelName="Agenda Equipe"
        checkboxName="agendaEquipe"
        indeterminate={
          (isCheck.gardes ||
            isCheck.deplacement ||
            isCheck.horaireOuv ||
            isCheck.vacances ||
            isCheck.formations) &&
          !isCheck.agendaEquipe
            ? true
            : false
        }
      >
        <TreeItemCheckbox
          onChangeCheckbox={handleChange}
          checked={isCheck.gardes}
          nodeId="2"
          labelName="Gardes"
          boxColor="#F46036"
          checkboxName="gardes"
        />
        <TreeItemCheckbox
          onChangeCheckbox={handleChange}
          checked={isCheck.horaireOuv}
          nodeId="3"
          labelName="Horaires d'ouvertures"
          boxColor="#63B8DD"
          checkboxName="horaireOuv"
        />
        <TreeItemCheckbox
          onChangeCheckbox={handleChange}
          checked={isCheck.vacances}
          nodeId="4"
          labelName="Vacances/Absances"
          boxColor="#27272F"
          checkboxName="vacances"
        />
        <TreeItemCheckbox
          onChangeCheckbox={handleChange}
          checked={isCheck.formations}
          nodeId="5"
          labelName="Formations"
          checkboxName="formations"
        />
        <TreeItemCheckbox
          onChangeCheckbox={handleChange}
          checked={isCheck.deplacement}
          nodeId="6"
          labelName="Déplacement"
          checkboxName="deplacement"
        />
      </TreeItemCheckbox>
      <TreeItemCheckbox
        onChangeCheckbox={handleChange}
        checked={isCheck.partnerExt}
        nodeId="7"
        checkboxName="partnerExt"
        labelName="Partenaires externes"
        indeterminate={
          (isCheck.labosPartner || isCheck.partnerServ) && !isCheck.partnerExt ? true : false
        }
      >
        <TreeItemCheckbox
          onChangeCheckbox={handleChange}
          checked={isCheck.labosPartner}
          nodeId="8"
          checkboxName="labosPartner"
          labelName="Labos partenaires"
          boxColor="#00A745"
        />
        <TreeItemCheckbox
          onChangeCheckbox={handleChange}
          checked={isCheck.partnerServ}
          nodeId="9"
          checkboxName="partnerServ"
          labelName="Partenaires de services"
          boxColor="#F46036"
        />
      </TreeItemCheckbox>
      <TreeItemCheckbox
        onChangeCheckbox={handleChange}
        checked={isCheck.servicesMed}
        nodeId="10"
        checkboxName="servicesMed"
        labelName="Services médicaux"
        indeterminate={!isCheck.servicesMed && isCheck.gestionVacc ? true : false}
      >
        <TreeItemCheckbox
          onChangeCheckbox={handleChange}
          checked={isCheck.gestionVacc}
          nodeId="11"
          checkboxName="gestionVacc"
          labelName="Gestion de Vaccination"
          boxColor="#FFE43A"
        />
      </TreeItemCheckbox>
      <TreeItemCheckbox
        onChangeCheckbox={handleChange}
        checked={isCheck.agendaPrive}
        nodeId="12"
        checkboxName="agendaPrive"
        labelName="Agenda privé"
        boxColor="#00A745"
      />
    </TreeView>
  );
};
