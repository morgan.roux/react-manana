import { Button, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import UpdateIcon from '@material-ui/icons/Update';
import useStyles from './styles';

interface ButtonReunionProps {
  onRequestReunion: () => void;
}

export const ButtonReunion: FC<ButtonReunionProps> = ({ onRequestReunion }) => {
  const classes = useStyles();

  return (
    <Button onClick={onRequestReunion} className={classes.buttonReunion} style={{ marginLeft: 10 }}>
      <UpdateIcon style={{ marginRight: '8px', transform: 'rotatey(160deg)' }} />
      <Typography>Nouveau Rendez-vous</Typography>
    </Button>
  );
};
