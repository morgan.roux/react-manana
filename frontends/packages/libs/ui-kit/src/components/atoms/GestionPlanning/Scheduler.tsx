import React, { FC, useState, ReactNode, MouseEvent, Dispatch, useEffect } from 'react';
import CustomScheduler from '../CustomScheduler/CustomScheduler';
import { SchedulerHeader, SchedulerHeaderProps, SchedulerItem } from '@progress/kendo-react-scheduler';
import { guid } from '@progress/kendo-react-common';
import SettingsIcon from '@material-ui/icons/Settings';
import PrintIcon from '@material-ui/icons/Print';
// import PeopleIcon from '@material-ui/icons/People';
import useStyles from './styles';
// import { HeaderPage } from '../components/HeaderPage';
import TableChartSubToolbar from '../../molecules/TableChartSubToolbar/TableChartSubToolbar';
import classnames from 'classnames';
import {
  Box,
  Button,
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Typography,
} from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { FormWithCustomDialog } from './CustomForm';
// import { sampleDataWithCustomSchema } from './data';
import MenuIcon from '@material-ui/icons/Menu';

import CalendarViewDayIcon from '@material-ui/icons/CalendarViewDay';
import ViewColumnIcon from '@material-ui/icons/ViewColumn';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ViewStreamIcon from '@material-ui/icons/ViewStream';
import { ButtonReunion } from './ButtonReunion';
import { sampleData } from './data';
import { DebouncedSearchInput } from '../DebouncedSearchInput';
export const listButtonScheduler = [
  // { id: 1, view: 'day', title: 'Jour', name: 'day', code: 'DAY' },
  // { id: 2, view: 'week', title: 'Semaine', name: 'week', code: 'WEEK' },
  { id: 3, view: 'month', title: 'Mois', name: 'month', code: 'MONTH' },
  // { id: 4, view: 'timeline', title: 'Planning', name: 'timeline', code: 'TIMELINE' },
];

interface SchedulerProps {
  data: any;
  collaborateurComponent?: ReactNode;
  setOpenDrawer: Dispatch<boolean>;
  onRequestReunion: () => void;
  onRequestChangeFilter?: (searchText: string, date: any) => void;
  onRequestUpdate?: (id: string) => void;
  onRequestDelete?: (id: string) => void;
  onSearch?: (searchText: string) => void;
  withSearch?: boolean;
}
export const SchedulerParameter: FC<SchedulerProps> = ({
  data,
  onRequestReunion,
  onRequestChangeFilter,
  onRequestUpdate,
  onRequestDelete,
  onSearch,
  withSearch = false,
}) => {
  const classes = useStyles();

  const [displayDate, setDisplayDate] = useState<Date>(new Date());
  const [value, setValue] = useState<string>('');

  useEffect(() => {
    onSearch && onSearch(value);
  }, [value]);

  const [activeView, setActiveView] = useState<'timeline' | 'day' | 'week' | 'month' | 'agenda'>('agenda');
  // const handleRequestChange = (searchText: string, date: any) => {
  //   console.log('Affichage', searchText, moment(date).toDate());
  // };

  const Header = (props: SchedulerHeaderProps) => {
    console.log(props);
    const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

    const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
      setAnchorEl(event.currentTarget);
    };

    const handleShowClick = (_event: React.MouseEvent<HTMLLIElement, globalThis.MouseEvent>) => {
      setShowParameter(!showParameter);
    };

    const handleClose = () => {
      setAnchorEl(null);
    };

    const theme = useTheme();
    const match = useMediaQuery(theme.breakpoints.down('sm'));
    return (
      <SchedulerHeader>
        <div className={classes.schedulerHeader}>
          <TableChartSubToolbar
            showMonth={true}
            displayDate={displayDate}
            setDisplayDate={setDisplayDate}
            withSearch={false}
            onRequestChange={onRequestChangeFilter}
          />
          <div className={classes.schedulerButtonHeader} style={{ display: 'flex', alignItems: 'center' }}>
            <div className={classes.parametreButtonScheduler}>
              {match ? (
                <div>
                  <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                    <MenuIcon />
                    <div className={classes.menuActive}>
                      {listButtonScheduler.find((el) => el.name === activeView)?.title}
                    </div>
                  </Button>
                </div>
              ) : (
                <></>
                // listButtonScheduler.map((item: any) => (
                //   <CustomButton
                //     style={{ marginRight: '5px' }}
                //     key={item.id}
                //     className={classnames(classes.buttonScheduler, activeView === item.view ? classes.active : '')}
                //     onClick={() => {
                //       setActiveView(item.view as 'timeline' | 'day' | 'week' | 'month' | 'agenda');
                //     }}
                //   >
                //     {item.title}
                //   </CustomButton>
                // ))
              )}
            </div>

            <div className={classes.parametreScheduler}>
              <div>
                {/* <CustomButton className={classes.buttonSetting} onClick={handleClick}>
                  <SettingsIcon />
                </CustomButton> */}
                <Box display="flex">
                  {withSearch && (
                    <Box>
                      <DebouncedSearchInput
                        value={value}
                        placeholder="Rechercher"
                        fullWidth={false}
                        onChange={setValue}
                      />
                    </Box>
                  )}
                  <ButtonReunion onRequestReunion={onRequestReunion} />
                </Box>
                {match ? (
                  <div>
                    <Menu
                      id="simple-menu"
                      anchorEl={anchorEl}
                      keepMounted
                      open={Boolean(anchorEl)}
                      onClose={handleClose}
                      className={classes.menuSchedulerMobile}
                    >
                      <Box className={classes.menuTitleMobile}>
                        <Typography>Gestion de planning</Typography>
                      </Box>
                      <List>
                        <ListItem
                          button
                          className={classnames(classes.buttonScheduler, activeView === 'day' ? classes.active : '')}
                          onClick={() => {
                            setActiveView('day');
                          }}
                        >
                          <ListItemIcon>
                            <CalendarViewDayIcon />
                          </ListItemIcon>
                          <ListItemText>Jour</ListItemText>
                        </ListItem>

                        <ListItem
                          button
                          className={classnames(classes.buttonScheduler, activeView === 'week' ? classes.active : '')}
                          onClick={() => {
                            setActiveView('week');
                          }}
                        >
                          <ListItemIcon>
                            <ViewColumnIcon />
                          </ListItemIcon>
                          <ListItemText>Semaine</ListItemText>
                        </ListItem>

                        <ListItem
                          button
                          className={classnames(classes.buttonScheduler, activeView === 'month' ? classes.active : '')}
                          onClick={() => {
                            setActiveView('month');
                          }}
                        >
                          <ListItemIcon>
                            <ViewModuleIcon />
                          </ListItemIcon>
                          <ListItemText>Mois</ListItemText>
                        </ListItem>

                        <ListItem
                          button
                          className={classnames(
                            classes.buttonScheduler,
                            activeView === 'timeline' ? classes.active : ''
                          )}
                          onClick={() => {
                            setActiveView('timeline');
                          }}
                        >
                          <ListItemIcon>
                            <ViewStreamIcon />
                          </ListItemIcon>
                          <ListItemText>Planning</ListItemText>
                        </ListItem>
                      </List>
                      <Divider />
                      <Divider />
                      <MenuItem onClick={handleShowClick} className={classes.iconMenuMobile}>
                        <SettingsIcon /> Paramètres
                      </MenuItem>
                      <MenuItem onClick={handleClose} className={classes.iconMenuMobile}>
                        <PrintIcon /> Imprimer
                      </MenuItem>
                    </Menu>
                  </div>
                ) : (
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                    className={classes.dropdownMenu}
                  >
                    <MenuItem onClick={handleShowClick} className={classes.menuSetting}>
                      <SettingsIcon /> Paramètres
                    </MenuItem>
                    <MenuItem onClick={handleClose} className={classes.menuSetting}>
                      <PrintIcon /> Imprimer
                    </MenuItem>
                  </Menu>
                )}
              </div>
            </div>
          </div>
        </div>
      </SchedulerHeader>
    );
  };

  const [showParameter, setShowParameter] = useState(false);

  const CustomItem = (props: any) => {
    const { image, imageFermeture, Title, TitleFermeture } = props.dataItem;

    console.log('image', props);
    return (
      <SchedulerItem
        {...props}
        style={{
          ...props.style,
          backgroundColor: props.dataItem.Couleur ? props.dataItem.Couleur : props.isAllDay ? '#00A745' : '#9E9E9E',
          image: `url(${props.dataItem.image})`,
          imageFermeture: `url(${props.dataItem.imageFermeture})`,
        }}
        onClick={(event: any) => {
          event.syntheticEvent.preventDefault();
          event.syntheticEvent.stopPropagation();
          onRequestUpdate && onRequestUpdate(props.dataItem.TaskID);
        }}
        onRemoveClick={(event: any) => {
          event.syntheticEvent.preventDefault();
          event.syntheticEvent.stopPropagation();
          onRequestDelete && onRequestDelete(props.dataItem.TaskID);
        }}
      >
        <div>
          {image && <img style={{ float: 'left', width: '20px', padding: '3px' }} src={image} />}
          {imageFermeture && (
            <img
              style={{
                float: 'left',
                width: '20px',
                padding: '3px',
                height: '20px',
                background: '#ffffff',
              }}
              src={imageFermeture}
            />
          )}
          {TitleFermeture ? (
            <Typography
              style={{
                fontSize: '12px',
                padding: '4px',
                fontFamily: 'Roboto, Medium',
                marginLeft: '30px',
              }}
            >
              {TitleFermeture}
            </Typography>
          ) : (
            <Typography style={{ fontSize: '12px', padding: '4px', fontFamily: 'Roboto, Medium' }}>{Title}</Typography>
          )}
        </div>
      </SchedulerItem>
    );
  };

  // const theme = useTheme();
  // const isMobile = useMediaQuery(theme.breakpoints.down('md'));
  const [dataChange, setDataChange] = useState(sampleData);
  console.log(dataChange);

  const handleDataChange = ({ created, updated, deleted }: any) => {
    console.log('****************************************gfdgfdgfd ', created, updated, deleted);
    setDataChange((old) =>
      old
        .filter((item) => deleted.find((current: any) => current.id === item.id) === undefined)
        .map((item) => updated.find((current: any) => current.id === item.id) || item)
        .concat(created.map((item: any) => Object.assign({}, item, { id: guid() })))
    );
  };

  const handleDateChange = () => {};

  return (
    <>
      <CustomScheduler
        customDateChange={handleDateChange}
        customDataChange={handleDataChange}
        className={classes.schedulerRoot}
        displayDate={displayDate}
        item={CustomItem}
        // footer={!views ? () => <></> : footer ? footer : undefined}
        locale="fr"
        header={(props) => <Header {...props} />}
        // footer={(props) => <Footer {...props} />}
        activeView={activeView}
        // views={[{ label: 'day' }, { label: 'week' }, { label: 'month' }, { label: 'timeline' }]}
        views={['month']}
        editable
        CustomForm={FormWithCustomDialog}
        CustomData={data}
      />
    </>
  );
};
