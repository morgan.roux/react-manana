import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginLeft: theme.spacing(30),
      background: '#ffffff',
      [theme.breakpoints.down('md')]: {
        marginLeft: '0',
        overFlow: '0',
      },
    },
    titleCenter: {
      textAlign: 'center',
      fontWeight: 'bold',
    },
    title: {
      fontWeight: 'bold',
    },
    paper: {
      padding: theme.spacing(0),
      marginTop: theme.spacing(2),
      textAlign: 'left',
      color: theme.palette.text.secondary,
    },
    buttonGroup: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'start',
      '& > *': {
        margin: theme.spacing(1),
      },
    },
    noCheckedDay: {
      border: 'none',
      padding: '7px 0px !important',
      fontSize: '12px',
      background: '#fff',
      minWidth: 35,
      borderRadius: '50% !important',
      textTransform: 'none',
      '& .MuiButton-label': {
        width: 'auto',
      },
    },
    checkedDay: {
      border: 'none',
      padding: '7px 0px !important',
      fontSize: '12px',
      background: '#F34F9C',
      minWidth: 35,
      borderRadius: '50% !important',
      textTransform: 'none',
      color: '#fff',
      '& .MuiButton-label': {
        width: 'auto',
      },
    },
    innerTextTitle: {
      margin: '15px 0',
    },
    titleSize20: {
      fontSize: '20px',
      fontWeight: 'bold',
    },
    svgPeople: {
      background: '#ed5784',
      borderRadius: '51px',
      padding: '9px',
      width: '40px',
      height: '40px',
      fill: '#fff',
      marginRight: '20px',
    },
    choisirPersonne: {
      display: 'flex',
      alignItems: 'center',
      cursor: 'pointer',
    },
    flexCenter: {
      display: 'flex',
      alignItems: 'center',
    },
  }),
);
