import React, { FC, useState } from 'react';
import { Button } from '@material-ui/core';
import { useStyles } from '../style';

interface ButtonWeekDayProps {
  day: any;
  onChange?: (day: string) => void;
}

export const ButtonWeekDay: FC<ButtonWeekDayProps> = ({ day, onChange }) => {
  const classes = useStyles();
  const [state, setState] = useState(day.workday);
  const selectDay = (event: React.MouseEvent<HTMLElement>) => {
    event.preventDefault();
    setState(!state);
    if (onChange) {
      onChange(day.day);
    }
  };
  return (
    <Button onClick={selectDay} className={state ? classes.checkedDay : classes.noCheckedDay}>
      {day.day}
    </Button>
  );
};
