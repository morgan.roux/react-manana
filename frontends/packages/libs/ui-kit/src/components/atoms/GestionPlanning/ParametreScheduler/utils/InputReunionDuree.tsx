import { FormControl, TextField } from '@material-ui/core';
import React, { FC, useState } from 'react';
interface InputPropsR {
  onChange?: (value: number) => void;
  value?: number;
}
export const InputReunionDuree: FC<InputPropsR> = (props: InputPropsR) => {
  const { onChange, value } = props;
  const [state, setState] = useState(value || 30);
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    setState(Number(event.target.value));
    if (onChange) {
      onChange(state);
    }
  };
  return (
    <>
      <FormControl style={{ maxWidth: '80px', margin: '0 15px 0 15px' }}>
        <TextField type="number" value={state} onChange={handleChange} variant="outlined" />
      </FormControl>
    </>
  );
};
