import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) =>
  createStyles({
    // Gestion Planning
    body: {
      marginLeft: theme.spacing(30),
      zIndex: 10,
      [theme.breakpoints.down('md')]: {
        marginLeft: '0',
        overFlow: '0',
      },
    },
    calendrierRoot: {
      display: 'flex',
      justifyContent: 'flex-start',
      borderBottom: '1px solid #42424261',
      marginBottom: '8px',
    },
    tabRoot: {
      '& .PrivateTabIndicator-colorSecondary-82': {
        backgroundColor: theme.palette.primary.main,
      },
      '& .Mui-selected': {
        color: theme.palette.primary.main,
      },
      '& .MuiTab-wrapper': {
        textTransform: 'initial',
        fontSize: '16px',
        fontFamily: 'family-roboto, Medium',
        font: 'menu',
        padding: '6px',
      },
    },
    buttonRoot: {
      '& .MuiButton-label': {
        textTransform: 'initial',
        color: '#42424261',
        fontSize: '16px',
        fontFamily: 'family-roboto, Medium',
        font: 'menu',
        padding: '6px',
      },
      '& .Mui-focusVisible': {
        color: '#cb48b7',
      },
      '&:hover': {
        backgroundColor: 'none',
      },
    },
    checkbox: {
      display: 'flex',
      alingItems: 'center',
      justifyContent: 'space-between',
      '& .MuiBox-root': {
        display: 'flex',
        alingItems: 'center',
        // justifyContent: 'flex-start',
      },
    },
    checkboxItems: {
      display: 'flex',
      alingItems: 'center',
    },
    todo: {
      '&.Mui-checked': {
        color: '#CB48B7',
      },
    },
    conges: {
      '&.Mui-checked': {
        color: '#FBB104',
      },
    },
    marketing: {
      '&.Mui-checked': {
        color: '#FF0000',
      },
    },
    rendezvous: {
      '&.Mui-checked': {
        color: '#00A745',
      },
    },
    reunions: {
      '&.Mui-checked': {
        color: '#63B8DD',
      },
    },
    vaccination: {
      '&.Mui-checked': {
        color: '#6B9080',
      },
      '&:hover': {
        backgroung: 'none',
      },
    },
    boxRoot: {
      display: 'flex',
      alignItems: 'center !important',
      '& .MuiBox-root': {
        display: 'flex',
        alingItems: 'center',
        // justifyContent: 'flex-start',
      },
    },
    label: {
      display: 'flex',
      alingItems: 'center',
      fontSize: '12px',
    },
    buttonUser: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      '& .MuiButton-root': {
        display: 'content',
        marginBottom: '16px',
        background: 'transparent',
        border: 'none',
      },
      '& .MuiButton-root:hover': {
        background: 'none',
      },
      '&.MuiButton-contained': {
        boxShadow: 'none',
        backgroundColor: 'none',
        marginBottom: '16px',
      },
    },
    buttonAllUser: {
      display: 'flex',
      alignItems: 'center',
      '& .MuiSvgIcon-root': {
        margin: '0',
        marginBottom: '8px',
      },
      '& .MuiButton-root': {
        marginBottom: '16px',
        background: 'transparent',
        border: 'none',
      },
    },
    buttonAllUserMobile: {
      display: 'flex',
      alignItems: 'center',
      textTransform: 'initial',
      '& .MuiSvgIcon-root': {
        margin: '0',
        marginBottom: '8px',
        marginRight: '8px',
      },
      '& .MuiButton-root': {
        marginBottom: '16px',
        background: 'transparent',
        border: 'none',
      },
      [theme.breakpoints.down('sm')]: {
        '& .MuiButton-label': {
          textTransform: 'initial',
          display: 'flex',
          fontSize: '14px',
          fontFamily: 'Roboto, Regular',
        },
        '& .MuiSvgIcon-root': {
          background: theme.palette.primary.main,
          color: '#fff',
          borderRadius: '50%',
          padding: '5px',
          marginTop: '8px',
          marginLeft: '8px',
          width: '19px',
          height: '18px',
        },
      },
    },
    bottonUser: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginBottom: '16px',
      '& .MuiButton-contained': {
        backgroundColor: 'transparent',
        boxShadow: 'none',
      },
      '&:hover': {
        backgroundColor: 'none',
      },
      '& .MuiButton-label': {
        textTransform: 'initial',
        display: 'flex',
        flexDirection: 'row-reverse',
      },
      '& .MuiCard-root': {
        marginRight: '8px',
      },
      '& .MuiSvgIcon-root': {
        backgroundColor: theme.palette.primary.main,
        padding: '6px',
        color: '#fff',
        borderRadius: '50%',
        fontSize: '16px',
        marginTop: '8px',
        marginLeft: '8px',
        '&:hover': {
          backgroundColor: 'none',
        },
      },
      [theme.breakpoints.down('sm')]: {
        display: 'flex',
        justifyContent: 'flex-start',
      },
    },
    schedulerRoot: {
      '& .k-button-group .k-button': {
        border: 'none',
        background: '#fff',
      },
      '& .k-button': {
        color: '#212121',
        borderColor: 'none',
        backgroundColor: '#fff',
      },
      '& .k-button:hover': {
        backgroundColor: '#fff',
      },
      '& .k-scheduler-toolbar': {
        backgroundColor: '#fff',
      },
      '& .k-button-group .k-button:active': {
        borderColor: theme.palette.primary.main,
        backgroundColor: theme.palette.primary.main,
      },
      '& .k-button-group .k-button.k-state-active': {
        backgroundColor: theme.palette.primary.main,
      },
      '& .k-event-template': {
        fontSize: '12px',
      },
    },
    schedulerHeader: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
      [theme.breakpoints.down('sm')]: {
        display: 'flex',
        flexDirection: 'row-reverse',
        '& .MuiBox-root': {
          width: '85px',
          flexWrap: 'wrap',
        },
      },
      [theme.breakpoints.down('xs')]: {
        '& .MuiBox-root': {
          width: 'auto',
          flexWrap: 'nowrap',
          marginRight: '-30px',
        },
      },
    },
    schedulerButtonHeader: {
      display: 'flex',
      alignItems: 'center',
      [theme.breakpoints.down('sm')]: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        marginTop: '8px',
        width: '100%',
        '& .MuiButton-root': {
          minWidth: 'auto',
        },
        '& .MuiBox-root': {
          marginTop: '8px',
        },
      },
    },
    parametreButtonScheduler: {
      [theme.breakpoints.down('sm')]: {
        display: 'flex',
        alignItems: 'center',
        marginBottom: '8px',
        '& .MuiSelect-root': {
          width: '114px',
        },
      },
      [theme.breakpoints.down('xs')]: {
        '& .MuiInput-root': {
          width: '114px',
          height: '40px',
        },
      },
    },
    menuTitleMobile: {
      padding: '10px',
      '& .MuiTypography-root': {
        fontSize: '16px',
        fontFamily: 'Roboto,Regular',
        fontWeight: 600,
      },
    },
    menuActive: {
      marginLeft: '8px',
      textTransform: 'capitalize',
    },
    menuSchedulerMobile: {
      [theme.breakpoints.down('sm')]: {
        '& .MuiPaper-root': {
          width: '292px !important',
          height: '758px !important',
        },
      },
    },
    parametreScheduler: {
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      },
    },
    buttonScheduler: {
      background: 'none',
      boxShadow: 'none',
      padding: '6px 16px',
      textTransform: 'initial',
      '&:hover': {
        background: 'none',
        boxShadow: 'none',
      },
      '&.MuiButton-contained': {
        background: '#fff',
        marginLeft: '8px',
        height: '40px',
        boxShadow: 'none',
      },
      '& .MuiButton-root:hover': {
        background: 'none',
      },
      '&.MuiButton-contained:hover': {
        boxShadow: 'none',
      },
    },
    dropdownMenu: {
      '& .MuiPaper-root': {
        marginLeft: '-70px',
      },
    },
    buttonSetting: {
      padding: '6px 16px',
      '&:hover': {
        boxShadow: 'none',
        background: theme.palette.primary.main,
      },
      '&.MuiButton-contained': {
        background: '#fff',
        marginLeft: '8px',
        height: '40px',
        boxShadow: 'none',
      },
      '& .MuiButton-root:hover': {
        background: 'none',
      },
      '&.MuiButton-contained:hover': {
        boxShadow: 'none',
      },
      '&:active': {
        backgroundColor: theme.palette.primary.main,
      },
    },
    menuSetting: {
      '& .MuiSvgIcon-root': {
        marginRight: '8px',
        width: '16px',
      },
      '& .MuiMenuItem-root': {
        fontFamily: 'Roboto, Regular',
        fontSize: '14px',
      },
    },
    iconMenuMobile: {
      '& .MuiSvgIcon-root': {
        top: '79px',
        marginRight: '8px',
        width: '19px',
        opacity: '1',
      },
      '& .MuiMenuItem-root': {
        fontFamily: 'Roboto, Regular',
        fontSize: '14px',
        width: '28px',
        height: '17px',
      },
    },
    active: {
      color: '#fff',
      background: theme.palette.primary.main,
      '& .MuiSvgIcon-root': {
        color: '#FFFFFF',
      },
      '&:hover': {
        boxShadow: 'none',
        color: '#fff',
        background: theme.palette.primary.main,
        '& .MuiSvgIcon-root': {
          color: '#FFFFFF',
        },
      },
      // '& .MuiButton-root':{
      //     background: 'red'
      // }
    },
    modal: {
      '& .MuiDialogActions-root': {
        display: 'none',
      },
      '& .MuiBackdrop-root': {
        backgroundColor: 'transparent',
      },
      '& .MuiPaper-root': {
        marginLeft: 'auto',
      },
    },
    modalAction: {
      display: 'flex',
      flexDirection: 'column',
      '& .MuiSvgIcon-root': {
        display: 'flex',
        alignItems: 'center',
        marginRight: '8px',
      },
    },
    modalUser: {
      '& .MuiPaper-root': {
        width: '690px',
        display: 'flex',
        // height: '70px',
      },
      [theme.breakpoints.down('md')]: {
        '& .MuiPaper-root': {
          widows: '100%',
        },
      },
      '& .MuiTypography-root': {
        fontSize: '20px',
        fontFamily: 'Roboto,Bold',
      },
      '& .MuiDialogTitle-root': {
        background: theme.palette.primary.main,
      },
    },
    colaborateur: {
      width: '570px',
    },
    search: {
      '& .MuiInputBase-root': {
        display: 'flex',
        justifyContent: 'center',
        width: '515px',
      },
    },
    selectRoot: {
      marginTop: '16px',
      width: '100%',
    },
    select: {
      '& .MuiSelect-root': {
        width: '420px',
        height: '10px',
      },
    },
    dropdown: {
      position: 'absolute',
      top: 28,
      right: 0,
      left: 0,
      zIndex: 1,
      border: '1px solid',
      padding: theme.spacing(1),
      backgroundColor: theme.palette.background.paper,
    },
    modalRoot: {
      '& .MuiPaper-root': {
        width: '690px',
      },
      '& .MuiBackdrop-root': {
        backgroung: 'transparent',
      },
      '& .MuiDialogTitle-root': {
        background: theme.palette.primary.main,
      },
      '& .MuiTypography-root': {
        fontSize: '20px',
        fontFamily: 'Roboto,Bold',
      },
    },
    modalBody: {
      width: '100%',
      '& .MuiFormControl-root': {
        width: '100%',
      },
    },
    selectScheduler: {
      '& .MuiSelect-root': {
        width: '610px',
        height: '20px',
      },
    },
    client: {
      display: 'flex',
      justifyContent: 'flex-start',
      fontSize: '18px',
      fontFamily: 'Roboto,Medium',
    },
    rechecheCollaborateur: {
      display: 'flex',
      justifyContent: 'flex-end',
      fontSize: '14px',
      fontFamily: 'Roboto,Regular',
      color: theme.palette.primary.main,
    },
    paragraphe: {
      display: 'flex',
      justifyContent: 'flex-start',
      fontSize: '14px',
      color: '#42424261',
      fontFamily: 'Roboto,Regular',
    },
    buttonReunion: {
      display: 'flex',
      padding: '10px',
      color: '#FFFFFF',
      background: theme.palette.primary.main,
      '&:hover': {
        color: '#FFFFFF',
        background: theme.palette.primary.main,
      },
    },
  })
);

export default useStyles;
