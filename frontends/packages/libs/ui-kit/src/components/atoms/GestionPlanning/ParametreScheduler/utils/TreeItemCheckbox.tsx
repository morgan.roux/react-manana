import React from 'react';
import TreeItem, { TreeItemProps } from '@material-ui/lab/TreeItem';
import { FormControlLabel, Checkbox } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() =>
  createStyles({
    noToogle: {
      '&.MuiTreeItem-root .MuiButtonBase-root input': {
        pointerEvents: 'none',
      },
    },
  })
);

interface TreeItemCheckboxProps {
  nodeId: string;
  labelName?: string;
  checkboxName?: string;
  boxColor?: string;
  indeterminate?: boolean;
  checked?: boolean;
  children?: React.ReactNode;
  onChangeCheckbox?: (event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => void;
}
/**
 * Custom TreeItem with Checkbox - Its is inherited by native TreeItem of Material-ui
 * @param props props accepted by treeItem
 * @type { TreeItemCheckboxProps & TreeItemProps } Both custom and native Props of TreeItem
 * @returns new Material-ui TreeItem combined with Checkbox
 */
export const TreeItemCheckbox = (props: TreeItemCheckboxProps & TreeItemProps) => {
  const { nodeId, labelName, checkboxName, boxColor, children, checked, onChangeCheckbox, indeterminate } = props;

  const classes = useStyles();
  return (
    <TreeItem
      className={classes.noToogle}
      nodeId={nodeId}
      label={
        <FormControlLabel
          control={
            <Checkbox
              checked={checked}
              onChange={onChangeCheckbox}
              indeterminate={indeterminate}
              name={checkboxName}
              value={labelName}
              style={{ color: boxColor || '#9e9e9e' }}
            />
          }
          label={labelName}
        />
      }
    >
      {children}
    </TreeItem>
  );
};
