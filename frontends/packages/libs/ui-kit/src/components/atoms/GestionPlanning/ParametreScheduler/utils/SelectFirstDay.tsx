import React, { useState } from 'react';
import { FormControl, Select } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(4),
      marginRight: theme.spacing(0),
      marginLeft: theme.spacing(0),
      width: '100%',
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    selectCustom: {
      borderRadius: '5px',
      background: 'none',
      '&:before': {
        display: 'none',
      },
      '& select.MuiSelect-root': {
        padding: '10px',
      },
    },
  })
);
export const SelectFirstDay = () => {
  const classes = useStyles();
  const [state, setState] = useState<{ day: string; name: string }>({
    day: '',
    name: 'first-day',
  });

  const handleChange = (event: React.ChangeEvent<{ name?: string; value: unknown }>) => {
    const name = event.target.name as keyof typeof state;
    setState({
      ...state,
      [name]: event.target.value,
    });
  };
  return (
    <>
      <FormControl variant="outlined" className={classes.formControl}>
        <Select
          native
          value={state.day}
          inputProps={{
            name: 'day',
          }}
          onChange={handleChange}
          className={classes.selectCustom}
        >
          <option value="lundi">Lundi</option>
          <option value="mardi">Mardi</option>
          <option value="mercredi">Mercredi</option>
          <option value="jeudi">Jeudi</option>
          <option value="vendredi">Vendredi</option>
          <option value="samedi">Samedi</option>
          <option value="dimanche">Dimanche</option>
        </Select>
      </FormControl>
    </>
  );
};
