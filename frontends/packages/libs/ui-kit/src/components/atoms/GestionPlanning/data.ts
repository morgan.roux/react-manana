const baseData = [
  {
    TaskID: 1,
    Title: "Ouvert de 07:00 à 17:00",
    Description: "",
    Start: "2021-03-01T07:00:00.000Z",
    End: "2021-03-01T17:00:00.000Z",
    isAllDay: false,
    Couleur: '#63B8DD'
  },
  {
    TaskID: 2,
    Title: "Congés vacance Mr Dupond",
    Description: "Vaccination Mr Olivier Dupon, Tel: +33 1 25 42 83 90, Nom du vaccin: Vaccin covid-19",
    Start: "2021-03-01T08:00:00.000Z",
    End: "2021-03-03T17:00:00.000Z",
    isAllDay: false,
    Couleur: '#9E9E9E',
    image: 'https://avatars.dicebear.com/v2/male/8c7d2f3db5c8b5748ea290ecb39a540e.svg'
  },
  {
    TaskID: 3,
    Title: "Congés vacance Mme Claudia",
    Description: "",
    Start: "2021-03-16T07:00:00.000Z",
    End: "2021-03-17T17:00:00.000Z",
    isAllDay: false,
    Couleur: '#9E9E9E',
    image: 'https://avatars.dicebear.com/v2/female/8c7d2f3db5c8b5748ea290ecb39a540e.svg'
  },
  {
    TaskID: 4,
    Title: "RDV Mr Olivier",
    Description: "",
    Start: "2021-03-16T08:30:00.000Z",
    End: "2021-03-16T09:00:00.000Z",
    isAllDay: false,
    Couleur: '#00A745'
  },
  {
    TaskID: 5,
    OwnerID: 4,
    Title: "RDV vaccin Mme Marie",
    Description: "",
    Start: "2021-03-19T09:30:00.000Z",
    End: "2021-03-19T10:00:00.000Z",
    isAllDay: false,
    Couleur: '#FFE43A'
  },
  {
    TaskID: 6,
    Title: "RDV Mme Sandrine",
    Description: "",
    Start: "2021-03-19T10:00:00.000Z",
    End: "2021-03-19T10:20:00.000Z",
    isAllDay: false,
    Couleur: '#00A745',
  },
  {
    TaskID: 7,
    Title: "RDV vaccin Mme Marie",
    Description: "",
    Start: "2021-03-20T08:30:00.000Z",
    End: "2021-03-20T09:00:00.000Z",
    isAllDay: false,
    Couleur: '#FFE43A',
    Background: 'blue',
  },
  {
    TaskID: 8,
    Title: "RDV Mr George",
    Description: "",
    Start: "2021-03-30T09:45:00.000Z",
    End: "2021-03-30T10:00:00.000Z",
    isAllDay: false,
    Couleur: '#00A745'
  },
  {
    TaskID: 9,
    Title: "RDV Mr Olivier",
    Description: "",
    Start: "2021-03-30T10:00:00.000Z",
    End: "2021-03-30T10:45:00.000Z",
    isAllDay: false,
    Couleur: '#00A745'
  },
  {
    TaskID: 10,
    TitleFermeture: "Fermeture officine",
    Description: "",
    Start: "2021-04-02T10:00:00.000Z",
    End: "2021-04-04T18:00:00.000Z",
    isAllDay: false,
    Couleur: '#63B8DD',
    imageFermeture: 'https://api.adorable.io/avatars/200/37a697e5ad220391542925fae3c305c0.png'
  },
  {
    TaskID: 11,
    Title: "Congés vacance Mlle Françia",
    Description: "",
    Start: "2021-04-08T07:00:00.000Z",
    End: "2021-04-09T18:00:00.000Z",
    isAllDay: false,
    Couleur: '#9E9E9E',
    image: 'https://avatars.dicebear.com/v2/female/8c7d2f3db5c8b5748ea290ecb39a540e.svg'
  },
  {
    TaskID: 12,
    Title: "Congés vancance Mr Romi",
    Description: "",
    Start: "2021-04-19T07:00:00.000Z",
    End: "2021-04-20T18:00:00.000Z",
    isAllDay: false,
    Couleur: '#9E9E9E',
    image: 'https://avatars.dicebear.com/v2/male/8c7d2f3db5c8b5748ea290ecb39a540e.svg'
  },
  {
    TaskID: 13,
    Title: "RDV vaccin Mme Marie",
    Description: "",
    Start: "2021-04-23T08:30:00.000Z",
    End: "2021-04-23T09:00:00.000Z",
    isAllDay: false,
    Couleur: '#FFE43A'
  },
  {
    TaskID: 14,
    Title: "RDV Mme Sandrine",
    Description: "",
    Start: "2021-04-23T09:00:00.000Z",
    End: "2021-04-23T10:30:00.000Z",
    isAllDay: false,
    Couleur: '#00A745'
  },
  {
    TaskID: 15,
    Title: "RDV vaccin Mme Marie",
    Description: "",
    Start: "2021-04-24T08:30:00.000Z",
    End: "2021-04-24T09:00:00.000Z",
    isAllDay: false,
    Couleur: '#FFE43A'
  },
];

export const customModelFields = {
  id: 'TaskID',
  title: 'Title',
  TitleFermeture: 'TitleFermeture',
  description: 'Description',
  start: 'Start',
  end: 'End',
  couleur: 'Couleur',
  image: 'image',
  imageFermeture: 'imageFermeture',
  background: 'Background',
};

const currentYear = new Date().getFullYear();
const parseAdjust = (eventDate: any) => {
  const date = new Date(eventDate);
  date.setFullYear(currentYear);
  return date;
};


export const displayDate = new Date(Date.UTC(currentYear, 5, 24));

export const sampleData = baseData.map((dataItem) => ({
  id: dataItem.TaskID,
  start: parseAdjust(dataItem.Start),
  end: parseAdjust(dataItem.End),
  isAllDay: dataItem.isAllDay,
  title: dataItem.Title,
  description: dataItem.Description,
  couleur: dataItem.Couleur,
  image: dataItem.image,
  TitleFermeture: dataItem.TitleFermeture,
  imageFermeture: dataItem.imageFermeture,
  background: dataItem.Background,
}));

export const sampleDataWithResources = baseData.map((dataItem) => ({
  id: dataItem.TaskID,
  start: parseAdjust(dataItem.Start),
  end: parseAdjust(dataItem.End),
  isAllDay: dataItem.isAllDay,
  title: dataItem.Title,
  description: dataItem.Description,
  couleur: dataItem.Couleur,
  image: dataItem.image,
  TitleFermeture: dataItem.TitleFermeture,
  imageFermeture: dataItem.imageFermeture,
  background: dataItem.Background,
}));

export const sampleDataWithCustomSchema = baseData.map((dataItem) => ({
  ...dataItem,
  Start: parseAdjust(dataItem.Start),
  End: parseAdjust(dataItem.End),
}));
