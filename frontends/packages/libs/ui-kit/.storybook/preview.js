import { addDecorator } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import { withUiKitThemeAddon } from './../../ui-theme-addon/lib/cjs';

addDecorator(withInfo);
addDecorator(withUiKitThemeAddon);
