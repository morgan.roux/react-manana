const path = require('path');

module.exports = {
  stories: ['../src/**/*.stories.[tj]sx'],
  addons: [
    '@storybook/addon-actions/register',
    path.resolve(__dirname, '../../ui-theme-addon/lib/cjs/register.js'),
  ],
};
