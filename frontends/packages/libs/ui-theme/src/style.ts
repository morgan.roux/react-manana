import { createMuiTheme, ThemeOptions } from '@material-ui/core';
export default function createMyTheme(options: ThemeOptions) {
  return createMuiTheme({
    ...options,
  });
}
