import React, { FC } from 'react';
import { ThemeProvider as MuiThemeProvider, Theme, createMuiTheme } from '@material-ui/core';
import ThemeContext from './ThemeContext';
import { availableThemes, ThemeName } from './config';
import { useState } from 'react';

const ThemeProvider: FC<{}> = ({ children }) => {
  const [themeName, changeTheme] = useState<ThemeName>('angel');

  const theme: Theme = React.useMemo(() => {
    const nextTheme = createMuiTheme(availableThemes[themeName]);
    return nextTheme;
  }, [themeName]);

  React.useLayoutEffect(() => {
    // Expose the theme as a global variable so people can play with it.
    if (window) {
      (window as any).theme = theme;
    }
  }, [theme]);

  console.log('************************************Theme', themeName, theme);
  return (
    <MuiThemeProvider theme={theme}>
      <ThemeContext.Provider value={[themeName, changeTheme]}> {children} </ThemeContext.Provider>
    </MuiThemeProvider>
  );
};

export default ThemeProvider;
