import { createContext } from 'react';
import { ThemeName } from './config';

type Context = [ThemeName, (newTheme: ThemeName | ((previousTheme: ThemeName) => ThemeName)) => void];

const ThemeContext = createContext<Context>(['onyx', () => {}]);

if (process.env.NODE_ENV !== 'production') {
  ThemeContext.displayName = 'ThemeContext';
}

export default ThemeContext;
