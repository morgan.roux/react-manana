export interface Fonction {
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}
