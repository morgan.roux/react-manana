import { User } from './User';
import { DQMTFonction, DQMTTache } from './DQMT';
import { Urgence } from './Urgence';
import { Importance } from './Importance'

export interface TATraitement {
  id: string;
  description: string;
  idType: string;
  idFonction: string;
  idTache: string;
  idImportance: string;
  idUrgence: string;
  repetition: number;
  repetitionUnite: string;
  heure: string;
  jourMois?: number;
  moisAnnee?: number;
  dateHeureFin?: Date;
  nombreOccurencesFin?: number;
  idPharmacie: string;
  createdAt: Date;
  updatedAt: Date;
  importance: Importance;
  urgence: Urgence;
  type: TATraitementType;
  fonction: DQMTFonction;
  tache: DQMTTache;
  participants: User[];
  recurrence?: string;
  execution: TATraitementExecution;
  derniereExecutionCloturee?: TATraitementExecution;
  termine?: boolean
  joursSemaine?: number[]
  nombreTraitementsEnRetard: number
  nombreTraitementsCloture: number;
  dateDebut?: Date;
}

export interface TATraitementType {
  id: string;
  code: string;
  libelle: string;
  createdAt: Date;
  updatedAt: Date;
}

export interface TATraitementExecution {
  id: string;
  idTraitement: string;
  idPharmacie: string;
  dateEcheance: Date;
  createdAt: Date;
  updatedAt: Date;
  importance: Importance;
  urgence: Urgence;
  traitement?: TATraitement;
  changementStatus: TATraitementExecutionChangementStatus[];
  dernierChangementStatut: TATraitementExecutionChangementStatus;
  prochainement?: Date;
}

export interface TATraitementExecutionChangementStatus {
  id: string;
  idTraitementExecution: string;
  idPharmacie: string;
  status: string;
  createdAt: Date;
  updatedAt: Date;
}
