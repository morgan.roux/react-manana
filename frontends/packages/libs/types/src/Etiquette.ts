import { Couleur } from './Couleur';

export interface Etiquette {
    id: string;
    ordre: string;
    nom: string;
    idCouleur: string;
    couleur: Couleur;
    idPharmacie: string;
}