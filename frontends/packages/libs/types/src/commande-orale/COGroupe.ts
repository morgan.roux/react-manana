export interface COGroupe {
  id: string;
  ordre: number;
  nom: string;
  nombreSourceAppros: number;
}
