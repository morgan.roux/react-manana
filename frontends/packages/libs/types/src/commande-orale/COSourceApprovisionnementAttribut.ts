export interface COSourceApprovisionnementAttribut {
  id?: string;
  identifiant: string;
  valeur: string;
}
