import { User } from '../User';
import { COSourceApprovisionnement } from './COSourceApprovisionnement';

export interface COCommandeOrale {
  commande: CommandeOraleCommande;
  passation?: CommandeOralePassation;
  reception?: CommandeOraleReception;
}

export interface CommandeOraleCommande {
  id?: string;
  collaborateur: User;
  dateHeure: Date | null;
  quantite: number;
  forme: string;
  designation: string;
  commentaire?: string;
  cloturee?: boolean;
}

export interface CommandeOralePassation {
  id?: string;
  collaborateur: User;
  dateHeure: Date | null;
  commentaire: string;
  sourceAppro: COSourceApprovisionnement;
  idCommandeOrale?: string;
}

export interface CommandeOraleReception {
  id?: string;
  collaborateur: User;
  dateHeure: Date | null;
  commentaire: string;
  idCommandeOrale?: string;
}
