import { COSourceApprovisionnementAttribut } from './COSourceApprovisionnementAttribut';

export interface COSourceApprovisionnement {
  id?: string;
  nom?: string;
  ordre?: number;
  tel?: string;
  attributs?: COSourceApprovisionnementAttribut[];
  commentaire?: string;
  idGroupeSourceAppro?: string;
}
