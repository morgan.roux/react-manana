export interface Couleur {
    id: string;
    code: string;
    libelle: string;
}