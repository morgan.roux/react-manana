import { PRTAnalyse, PRTAnalyseType } from './Analyse';
export interface PRTLaboratoire {
  id?: string;
  nomLabo: string;
  analyse?: PRTAnalyse[];
}
