import { User } from '../User';
import { PRTContact } from './PRTContact';

interface Subject {
  id: string;
  code: string;
  libelle: string;
}

export interface PRTRendezVous {
  id: string;
  ordreJour: string;
  subject?: Subject;
  dateRendezVous: Date;
  heureDebut: string;
  heureFin?: string;
  statut: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  invites: PRTContact[];
  participants: User[];
  note?: string;
  typeReunion: string;
}
