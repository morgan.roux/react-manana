import { Fonction } from '../Fonction';
import { Fichier } from '../User';
import { Contact } from './../Contact';

export interface PRTContact {
  id?: string;
  civilite: string;
  nom: string;
  prenom?: string;
  idFonction: string;
  fonction?: Fonction;
  photo?: Fichier;
  contact?: Contact;
  idDepartements?: string[];
  idPartenaireTypeAssocie: string;
  prive?: boolean;
}
