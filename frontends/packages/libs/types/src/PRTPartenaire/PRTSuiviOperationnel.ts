import { Fichier, User } from '../User';

export interface PRTSuiviOperationnel {
  id?: string;
  titre: string;
  description: string;
  dateHeure: Date;
  createdBy?: string;
  idGroupement?: string;
  idPharmacie?: string;
  partenaireType: string;
  idTypeAssocie?: string;
  type: PRTSuiviOperationnelType;
  statut: PRTSuiviOperationnelStatut;
  fichiers?: Fichier[];
  participants?: User[];
  idImportance?: string;
  montant: number;
  idTache?: string;
  idFonction?: string;
  contacts?: any[];
  importance?: {
    id: string;
    ordre: number;
    libelle: string;
    couleur: string;
    supprime: boolean;
    createdAt: Date;
    updatedAt: Date;
  };
  actionMode?: string;
}

export interface PRTSuiviOperationnelType {
  id: string;
  libelle: string;
  code: string;
}

export interface PRTSuiviOperationnelStatut {
  id: string;
  libelle: string;
  code: string;
}
