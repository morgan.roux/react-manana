import { Fichier } from '../User';

export interface PRTPlanMarketing {
  id?: string;
  titre: string;
  description: string;
  dateDebut: Date;
  dateFin: Date;
  idUser?: string;
  idGroupement?: string;
  idPharmacie?: string;
  partenaireType: string;
  idPartenaireTypeAssocie?: string;
  type: PRTPlanMarketingType;
  statut: PRTPlanMarketingStatut;
  fichiers?: Fichier[];
  miseAvants?: PRTMiseAvant[];
  produits?: any[];
  montant: number;
  remise: number | null;
  montantStatutRealise?: number;
  typeRemuneration?: string;
  remuneration?: string;
}

export interface PRTMiseAvant {
  id: string;
  libelle: string;
  code: string;
  couleur?: string;
}

export interface PRTPlanMarketingType {
  id: string;
  libelle: string;
  code: string;
  couleur?: string;
  idPharmacie?: string;
}

export interface PRTPlanMarketingStatut {
  id: string;
  libelle: string;
  code: string;
}

export interface PRTPlanMarketingProduit {
  id: string;
  idProduit: string;
  idPlanMarketing: string;
}
