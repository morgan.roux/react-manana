import { Fichier } from '../User';
import { PRTLaboratoire } from './Laboratoire';

export interface PRTAnalyse {
  id?: string;
  titre: string;
  dateDebut: Date;
  dateFin: Date;
  dateChargement: Date;
  createdAt?: Date;
  updatedAt?: Date;
  fichiers?: Fichier[];
  partenaireType: string;
  partenaireTypeAssocie?: PRTLaboratoire;
  type: PRTAnalyseType;
}

export interface PRTAnalyseType {
  id?: string;
  libelle: string;
  code: string;
}
