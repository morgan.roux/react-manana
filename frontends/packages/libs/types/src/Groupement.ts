export interface Groupement {
  id: string;
  nom: string;
  adresse1?: string;
  adresse2?: string;
  cp?: string;
  ville?: string;
  pays?: string;
  telBureau?: string;
  telMobile?: string;
  mail?: string;
  site?: string;
  commentaire?: string;
  nomResponsable?: string;
  prenomResponsable?: string;
  dateSortie?: Date;
  sortie?: number;
  createdAt?: Date;
  updatedAt?: Date;
}
