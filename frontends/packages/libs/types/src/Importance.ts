import { Couleur } from './Couleur';

export interface Importance {
    id: string;
    ordre: string;
    libelle: string;
    couleur: string;
}