import { Groupement } from './Groupement';

export interface User {
  id: string;
  email: string;
  emailConfirmed: boolean;
  passwordHash: string;
  passwordSecretAnswer: string;
  phoneNumber: string;
  phoneNumberConfirmed: boolean;
  twoFactorEnabled: boolean;
  lockoutEndDateUtc: Date;
  lockoutEnabled: boolean;
  accessFailedCount: number;
  userName: string;
  fullName: string;
  lastLoginDate: Date;
  lastPasswordChangedDate: Date;
  isLockedOut: boolean;
  isLockedOutPermanent: boolean;
  isObligationChangePassword: boolean;
  accessFailedCountBeforeLockoutPermanent: number;
  idGroupement: string;
  status: string;
  theme: string;
  jourNaissance: number;
  moisNaissance: number;
  anneeNaissance: number;
  createdAt: Date;
  updatedAt: Date;
  groupement?: Groupement;
  photo: Fichier;
}

export interface Fichier {
  id: string;
  chemin?: string;
  nomOriginal?: string;
  type?: string;
  avatar?: Avatar;
  urlPresigned?: string;
  publicUrl?: string;
  codeMaj?: string;
  userCreation?: User;
  userModification?: User;
  dateCreation?: Date;
  dateModification?: Date;
}

export interface Avatar {
  type?: string;
  id: string;
  description?: string;
  codeSexe?: string;
  fichier?: Fichier;
  userCreation?: User;
  dateCreation?: Date;
  userModification?: User;
  dateModification?: Date;
  idGroupement?: string;
}
