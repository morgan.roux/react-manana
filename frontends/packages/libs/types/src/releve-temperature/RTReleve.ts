import { User } from '../User';
import { RTFrigo } from './RTFrigo';
import { RTStatus } from './RTStatus';
import { RTAlerteAnomalie } from './RTAlerteAnomalie';

export interface RTReleve {
  id?: string;
  temperature?: number;
  type: string;
  idUser?: string;
  releveur?: User;
  idStatus?: string;
  status: RTStatus;
  idFrigo?: string;
  frigo?: RTFrigo;
  alerteAnomalies?: RTAlerteAnomalie[];
  createdAt: Date;
  updatedAt?: Date;
}
