import { Pharmacie } from '../Pharmacie';
import { TATraitement } from './../TATraitement';
import { RTReleve } from './RTReleve';
import { RTCapteur } from './RTCapteur'

export interface RTFrigo {
  id?: string;
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  temporisation: number;
  typeReleve: string;
  idPharmacie?: string;
  pharmacie?: Pharmacie;
  createdAt?: Date;
  updatedAt?: Date;
  traitements?: TATraitement[];
  releves?: RTReleve[];
  dernierReleve?: RTReleve;
  capteurs?: RTCapteur[]

}
