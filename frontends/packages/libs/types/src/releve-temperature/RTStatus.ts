export interface RTStatus {
  id?: string;
  code: string;
  couleur: string;
  libelle: string;
  createdAt: Date;
  updatedAt: Date;
}
