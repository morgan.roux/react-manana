import { Model } from 'sequelize-typescript';
import { RTPointAcces } from './RTPointAcces';
export declare class RTCapteur extends Model<RTCapteur> {
    id: string;
    nom: string;
    numeroSerie: string;
    pointAcces: RTPointAcces;
}
