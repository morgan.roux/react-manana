import { Model } from 'sequelize-typescript';
export declare class RTPointAcces extends Model<RTPointAcces> {
    id: string;
    nom: string;
    numeroSerie: string;
}
