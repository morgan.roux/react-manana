import { User } from '../User';
import { RTReleve } from './RTReleve';

export interface RTAlerteAnomalie {
  id?: string;
  type: string;
  idReleve: string;
  releve: RTReleve;
  idUser: string;
  user: User;
  message: string;
  status: string;
  createdAt: Date;
  updatedAt: Date;
}
