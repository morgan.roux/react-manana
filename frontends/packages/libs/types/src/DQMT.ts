import { User } from "./User";

export interface DQMTFonction {
    id: string;
    ordre: number;
    libelle: string;
    idPharmacie: string;
    idGroupement: string;
    createdAt: Date;
    updatedAt: Date;
    nombreTaches: number;
    idProjet?: string;
    active: boolean;
    taches: DQMTTache[]
}

export interface DQMTTache {
    id: string;
    ordre: number;
    libelle: string;
    idFonction: string;
    idPharmacie: string;
    idGroupement: string;
    createdAt: Date;
    updatedAt: Date;
    idProjet: string;
    active: boolean;
    collaborateurResponsables: DQMTTacheResponsableCollaborateur[]
}

export interface DQMTTacheResponsableCollaborateur {
    idTache: string;
    type: DQMTResponsableType;
    responsable: User;
}

export interface DQMTResponsableType {
    id: string;
    ordre: number;
    max?: number;
    code: string;
    couleur: string;
    libelle: string;
    idPharmacie: string;
    idGroupement: string;
    createdAt: Date;
    updatedAt: Date;
}