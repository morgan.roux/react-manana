module.exports = {
  globals: {
    'ts-jest': {
      isolatedModules: true,
    },
  },
  transform: {
    '\\.(ts)$': 'ts-jest',
  },
  collectCoverage: false,
  testPathIgnorePatterns: ['<rootDir>/node_modules'],
};
