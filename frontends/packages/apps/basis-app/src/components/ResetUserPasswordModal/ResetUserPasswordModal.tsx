import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useReset_User_PasswordMutation } from '@lib/common/src/graphql';
import { CustomFormTextField, CustomModal } from '@lib/ui-kit';
import React, { ChangeEvent, FC, useCallback, useState } from 'react';
import { useStyles } from './styles';

interface ResetUserPasswordInterface {
  userId: string;
  email: string;
  login: string;
  open: boolean;
  setOpen: (open: boolean) => void;
}

const ResetUserPasswordModal: FC<ResetUserPasswordInterface> = ({ userId, open, setOpen, email, login }) => {
  const [password, setPassword] = useState<string>('');
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const displayNotification = useDisplayNotification();
  const classes = useStyles({});
  const { graphql } = useApplicationContext();

  const [resetUserPassword, resettingUserPassword] = useReset_User_PasswordMutation({
    client: graphql,
    onError: (errors) => {
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map((error) => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'ERROR_RESER_PASSWORD_USER':
              errorMessage = 'Une erreur est survenue lors de la Réinitialisation du mot de passe';
              break;
          }
        }
      });

      displayNotification({
        type: 'error',
        message: errorMessage,
      });
    },
    onCompleted: (data) => {
      if (data.resetUserPassword) {
        setOpen(false);
        setShowPassword(false);
        setPassword('');
        displayNotification({
          type: 'success',
          message: 'Réinitialisation du mot de passe se termine avec succès',
        });
      }
    },
  });

  const toggleShowPassword = useCallback(() => {
    setShowPassword((prev) => !prev);
  }, []);

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    event.stopPropagation();
  };

  const handleValueChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setPassword(value);
  };

  const handleSubmit = () => {
    if (userId) {
      resetUserPassword({ variables: { id: userId, password } });
    }
  };

  return (
    <CustomModal
      open={open}
      actionButton="Réinitialiser"
      title="Réinitialisation du mot de passe"
      setOpen={setOpen}
      onClickConfirm={handleSubmit}
      disabledButton={(password && userId ? false : true) || resettingUserPassword.loading}
    >
      <div>
        {login && <CustomFormTextField type="text" label="Login" value={login} disabled={true} />}
        <CustomFormTextField type="text" label="Email de lien" value={email} disabled={true} />
        <CustomFormTextField
          label="Mot de passe"
          // type={showPassword ? 'text' : 'password'}
          name="password"
          value={password}
          onChange={handleValueChange}
          margin="dense"
          autoComplete="off"
          // InputProps={{
          //   endAdornment: (
          //     <InputAdornment position="end">
          //       <IconButton
          //         size="small"
          //         aria-label="toggle password visibility"
          //         onClick={toggleShowPassword}
          //         onMouseDown={handleMouseDownPassword}
          //       >
          //         {showPassword ? <Visibility /> : <VisibilityOff />}
          //       </IconButton>
          //     </InputAdornment>
          //   ),
          // }}
        />
      </div>
    </CustomModal>
  );
};

export default ResetUserPasswordModal;
