import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useItemsLazyQuery } from '@lib/common/src/federation';
import { CustomButton, CustomFormTextField, NewCustomButton } from '@lib/ui-kit';
import { Box, LinearProgress, Typography, Divider } from '@material-ui/core';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import useStyles from './styles';

interface AppetenceListProps {
  itemList: any[];
  setItemList: (itemList: any[]) => void;
  loading: boolean;
  sorting?: boolean;
  appetences: any[];
  withTitle?: boolean;
  withSubTitle?: boolean;
  withInputSearch?: boolean;
  withAddBtn?: boolean;
  handleOrderChange: (id: string, event: any) => void;
}

const AppetenceList: FC<AppetenceListProps> = ({
  itemList,
  setItemList,
  loading,
  appetences,
  sorting = false,
  withTitle,
  withSubTitle,
  withInputSearch,
  withAddBtn,
  handleOrderChange,
}) => {
  const classes = useStyles({});
  const [searchText, setSearchText] = useState<string>('');
  const { push } = useHistory();
  const { federation, currentPharmacie } = useApplicationContext();
  const displayNotification = useDisplayNotification();

  const handleChangeSearchText = (e: ChangeEvent<HTMLInputElement>) => {
    if (e && e.target) {
      setSearchText(e.target.value);
    }
  };

  const goToRoles = () => push('/db/roles');

  const [searchAllItems, { data: allData, loading: allLoading }] = useItemsLazyQuery({
    client: federation,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  useEffect(() => {
    searchAllItems({
      variables: {
        filter: searchText?.length > 0 ? { name: { iLike: `%${searchText}%` } } : undefined,
        idPharmacie: currentPharmacie.id,
      },
    });
  }, [searchText]);

  useEffect(() => {
    if (allData?.items && appetences) {
      let newItemList = allData.items.nodes
        .filter((e) => e.active) // if item actived
        .map((item) => {
          const appetence = appetences.find((appetence) => appetence.idItem === item.id);
          return appetence ? { ...item, ordre: appetence.ordre } : item;
        });

      if (sorting) {
        newItemList = [...newItemList].sort((a: any, b: any) => (a.ordre || 1) - (b.ordre || 1)).reverse();
      }

      setItemList(newItemList);
    }
  }, [allData, appetences]);

  return (
    <div className={classes.infoPersoContainer}>
      {withTitle && <Typography className={classes.inputTitle}>Scores Pour Dashboard d’activité</Typography>}
      {withSubTitle && (
        <Typography className={classes.permissionAccessSubTite}>
          Définir un score selon l’importance de chacun de ces Items (De 1 à 10)
        </Typography>
      )}
      {withInputSearch && (
        <CustomFormTextField placeholder="Rechercher" onChange={handleChangeSearchText} value={searchText} />
      )}
      <LinearProgress
        color="secondary"
        style={{
          width: '100%',
          height: 3,
          marginBottom: 20,
          visibility: allLoading || loading ? 'visible' : 'hidden',
        }}
      />
      {itemList.length === 0 && !loading ? (
        <div className={classes.noDataContainer}>
          <span>Aucun score trouvé</span>
          {withAddBtn && (
            <NewCustomButton variant="outlined" onClick={goToRoles}>
              Définir scores
            </NewCustomButton>
          )}
        </div>
      ) : (
        <>
          <div className={classes.inputsContainer}>
            <div className={classes.appetenceFormRow}>
              {itemList.map((i: any, index) => {
                const lengthItem = itemList.length;
                return (
                  <>
                    <Box
                      display="flex"
                      width="100%"
                      justifyContent="space-between"
                      alignItems="center"
                      key={`appetence-${i.id}`}
                      style={{ padding: '0 15px' }}
                    >
                      <Typography style={{ marginRight: 15 }}>{i.name}</Typography>
                      <CustomFormTextField
                        type="number"
                        value={i.ordre}
                        onChange={(event) => handleOrderChange(i.id, event)}
                        className={classes.inputForm}
                      />
                    </Box>
                    {index + 1 < lengthItem ? <Divider className={classes.divider} /> : ''}
                  </>
                );
              })}
            </div>
          </div>
        </>
      )}
    </div>
  );
};

AppetenceList.defaultProps = {
  withTitle: true,
  withSubTitle: true,
  withInputSearch: true,
  withAddBtn: true,
};

export default AppetenceList;
