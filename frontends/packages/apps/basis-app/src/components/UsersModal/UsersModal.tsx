import React, { FC, Dispatch, SetStateAction, useState, useCallback } from 'react';

import { RouteComponentProps, withRouter } from 'react-router-dom';
import useStyles from './styles';
import { IconButton, Tooltip, Fade } from '@material-ui/core';
import { Edit, VpnKey, History } from '@material-ui/icons';
import { ResetUserPasswordModal } from '../ResetUserPasswordModal';
import HistoriquePersonnel from '../HistoriquePersonnel';
import ButtonResendEmail from '../Content/ButtonResendEmail';
import { CustomAvatar, CustomModal } from '@lib/ui-kit';
import UserStatus from '../Content/UserStatus';
import CustomContent from '@lib/common/src/components/newCustomContent';
import { Column } from '@lib/common/src/components/newCustomContent/interfaces';

interface UsersModalProps {
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  row: any;
  baseUrl: string;
}

const UsersModal: FC<UsersModalProps & RouteComponentProps> = ({
  open,
  setOpen,
  row,
  baseUrl,
  location: { pathname },
  history: { push },
}) => {
  const classes = useStyles({});
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);
  const [take, setTake] = useState<number>(5);
  const [skip, setSkip] = useState<number>(0);

  const [openHistory, setOpenHistory] = useState<boolean>(false);
  const [openInitPassword, setOpenInitPassword] = useState<boolean>(false);

  const [clickedUser, setClickedUser] = useState<any>(null);

  const title = `Liste des comptes`;
  const users: any[] = (row && row.users) || [];

  const handleClose = useCallback(() => {
    setOpen(false);
  }, []);

  const handleClickEdit = (userId: string) => () => {
    handleClose();
    push(`/db/${baseUrl}/user/edit/${row.id}/${userId}`);
  };

  const handleClickInitPassword = (user: any) => {
    if (user) {
      // handleClose();
      setClickedUser(user);
      setOpenInitPassword(true);
    }
  };

  const handleClickHistorique = useCallback(
    (user: any) => () => {
      // handleClose();
      setClickedUser(user);
      setOpenHistory(true);
    },
    []
  );

  const columns: Column[] = [
    {
      name: '',
      label: 'Photo',
      sortable: false,
      renderer: (value: any) => {
        return (
          <CustomAvatar
            name={value.userName}
            url={value.userPhoto && value.userPhoto.fichier && value.userPhoto.fichier.publicUrl}
          />
        );
      },
    },
    {
      name: 'userName',
      label: 'Nom',
    },
    {
      name: 'email',
      label: 'Email de connexion',
    },
    {
      name: 'anneeNaissance',
      label: 'Année de naissance',
    },
    {
      name: 'pharmacie.nom',
      label: 'Pharmacie',
      renderer: (value: any) => {
        return (value && value.pharmacie && value.pharmacie.nom) || '-';
      },
    },
    {
      name: 'status',
      label: 'Statut',
      renderer: (value: any) => {
        return <UserStatus user={value} pathname={pathname} />;
      },
    },
    {
      name: '',
      label: '',
      sortable: false,
      renderer: (value: any) => {
        const actionsBtns = [
          { title: 'Historique', onClick: () => handleClickHistorique(value), icon: <History /> },
          { title: 'Modifier', onClick: () => handleClickEdit(value.id), icon: <Edit /> },
          {
            title: 'Réinitialiser mot de passe',
            onClick: () => handleClickInitPassword(value),
            icon: <VpnKey />,
          },
        ];

        return (
          <>
            {actionsBtns.map((btn, index) => (
              <Tooltip key={index} TransitionComponent={Fade} TransitionProps={{ timeout: 500 }} title={btn.title}>
                <IconButton onClick={btn.onClick}>{btn.icon}</IconButton>
              </Tooltip>
            ))}
            <ButtonResendEmail email={value.email} login={value.login} />
          </>
        );
      },
    },
  ];

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={title}
      withBtnsActions={false}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="xl"
      fullWidth={true}
      className={classes.usersModalRoot}
    >
      <div className={classes.tableContainer}>
        <CustomContent
          columns={columns}
          //showToolbar={false}
          //selectable={false}
          listResult={users}

          //rowCount={users.length}
          //total={users.length}
          //page={page}
          //rowsPerPage={rowsPerPage}
          // take={take}
          // skip={skip}
          // setTake={setTake}
          // setSkip={setSkip}
          // setPage={setPage}
          // setRowsPerPage={setRowsPerPage}
        />
      </div>
      <HistoriquePersonnel open={openHistory} setOpen={setOpenHistory} userId={clickedUser && clickedUser.id} />
      <ResetUserPasswordModal
        open={openInitPassword}
        setOpen={setOpenInitPassword}
        userId={clickedUser && clickedUser.id}
        email={clickedUser && clickedUser.email}
        login={clickedUser && clickedUser.login}
      />
    </CustomModal>
  );
};

export default withRouter(UsersModal);
