import React, { FC } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme, CircularProgress } from '@material-ui/core';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  root: {
    display: 'flex',
    width: '100%',
    height: 'calc(100vh - 220px)',
  },

  progress: {
    // color: theme.palette.text.secondary,
    margin: 'auto',
  },
});

const Loader: FC<WithStyles> = ({ classes }: any) => {
  return (
    <div className={classes.root}>
      <CircularProgress size={50} disableShrink={true} className={classes.progress} />
    </div>
  );
};

const SmallLoader: FC<WithStyles> = ({ classes }: any) => {
  return <CircularProgress size={20} disableShrink={true} className={classes.progress} />;
};

const LoaderSmall = withStyles(styles)(SmallLoader);
export { LoaderSmall };

export default withStyles(styles)(Loader);
