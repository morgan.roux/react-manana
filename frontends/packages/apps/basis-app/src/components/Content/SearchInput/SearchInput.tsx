import React from 'react';
import { useStyles } from './style';
import { InputBase, IconButton, Tooltip, Fade } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import classnames from 'classnames';

function SearchInput({ onChange, value, placeholder, disabled, fullWidth, outlined, noPrefixIcon, dark }: any) {
  const classes = useStyles({});

  return (
    <Tooltip
      TransitionComponent={Fade}
      TransitionProps={{ timeout: 600 }}
      title="Veuillez saisir votre critère de recherche"
    >
      <div
        className={classnames(
          outlined ? classes.outlined : '',
          fullWidth ? classes.fullWidth : '',
          dark ? classes.darkBackground : classes.whiteBackground
        )}
      >
        {/*!noPrefixIcon && <PeopleIcon className={classes.peopleIcon} />*/}
        <InputBase
          placeholder={placeholder || 'Que recherchez-vous?'}
          value={value}
          autoComplete="off"
          inputProps={{
            'aria-label': 'search',
          }}
          onChange={(e) => onChange(e.target.value)}
          className={classes.input}
          type="search"
          disabled={disabled}
        />
        <IconButton className={classes.iconButton} aria-label="search">
          <SearchIcon />
        </IconButton>
      </div>
    </Tooltip>
  );
}

export default SearchInput;
