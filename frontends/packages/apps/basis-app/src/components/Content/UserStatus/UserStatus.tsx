import { useApolloClient } from '@apollo/client';
import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useUpdate_User_StatusMutation } from '@lib/common/src/graphql';
import { Button, Menu, MenuItem, Fade, Tooltip } from '@material-ui/core';
import moment from 'moment';
import React, { FC } from 'react';
import {
  PARTENAIRE_LABORATOIRE_URL,
  PARTENAIRE_SERVICE_URL,
  // PERSONNEL_GROUPEMENT_URL,
  PERSONNEL_PHARMACIE_URL,
  PRESIDENT_REGION_URL,
  TITULAIRE_PHARMACIE_URL,
} from '@lib/common/src/shared/url';
import { LoaderSmall } from '../../Content/Loader';
import useStyles from './styles';
interface UserStatusProps {
  user: any;
  pathname: string;
}

const UserStatus: FC<UserStatusProps> = ({ user, pathname }) => {
  const classes = useStyles({});
  const { auth, useParameterValueAsNumber, graphql } = useApplicationContext();
  const displayNotification = useDisplayNotification();

  const isTitulaire: boolean = pathname.includes(TITULAIRE_PHARMACIE_URL);
  const isPresident: boolean = pathname.includes(PRESIDENT_REGION_URL);
  const isPartenaireLaboratoire: boolean = pathname.includes(PARTENAIRE_LABORATOIRE_URL);
  // const isPersonnelGroupement: boolean = pathname.includes( PERSONNEL_GROUPEMENT_URL)
  const isPersonnelPharamcie: boolean = pathname.includes(PERSONNEL_PHARMACIE_URL);
  const isPartenaireService: boolean = pathname.includes(PARTENAIRE_SERVICE_URL);

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const client = useApolloClient();
  const inactivationAccountParameter = useParameterValueAsNumber('0321') || 60;
  let bntText = '-';
  if (user && user.status) {
    switch (user.status) {
      case 'ACTIVATED':
        bntText = 'Activé';
        if (user.lastLoginDate) {
          const now = moment();
          const lastLoginDate = moment(user.lastLoginDate);
          if (now.diff(lastLoginDate, 'days') > inactivationAccountParameter) {
            bntText = 'Bloqué';
          }
        }

        break;
      case 'ACTIVATION_REQUIRED':
        bntText = "Demande d'activation";
        break;
      case 'BLOCKED':
        bntText = 'Bloqué';
        break;
      case 'RESETED':
        bntText = `Mot de passe réinitialisé`;
        break;
      default:
        break;
    }
  }

  const [doUpdateUserStatus, { loading }] = useUpdate_User_StatusMutation({
    client: graphql,
    onError: (error) => {
      displayNotification({ type: 'error', message: error.message });
    },
  });

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const updateStatus = (id: string, status: string) => {
    handleClose();
    doUpdateUserStatus({
      variables: { id, status },
    });
  };

  const disabledBtn = (): boolean => {
    if (isPartenaireService && !auth.isAuthorizedToViewDetailsPartenaireService()) {
      return true;
    }

    if (isTitulaire && !auth.isAuthorizedToViewDetailsTitulairePharmacie()) {
      return true;
    }

    if (isPresident && !auth.isAuthorizedToViewDetailsPresidentRegion()) {
      return true;
    }

    if (isPartenaireLaboratoire && !auth.isAuthorizedToViewDetailsLaboratoirePartenaire()) {
      return true;
    }

    if (isPersonnelPharamcie && !auth.isAuthorizedToViewDetailsPersonnelPharmacie()) {
      return true;
    }

    /* if (user.status === 'ACTIVATION_REQUIRED' || user.status === 'RESETED') {
      return true;
    }*/
    return false;
  };

  if (loading) {
    return (
      <div className={classes.loaderContainer}>
        <LoaderSmall />
      </div>
    );
  }

  return user && user.status ? (
    <>
      <Tooltip
        TransitionComponent={Fade}
        TransitionProps={{ timeout: 600 }}
        title="Activé ou desactivé le compte utilisateur"
      >
        <Button
          className={classes.btn}
          variant="text"
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={handleClick}
          disabled={disabledBtn()}
        >
          {bntText}
        </Button>
      </Tooltip>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        getContentAnchorEl={null}
      >
        <MenuItem onClick={() => updateStatus(user.id, 'ACTIVATED')}>Activer</MenuItem>
        <MenuItem onClick={() => updateStatus(user.id, 'BLOCKED')}>Bloquer</MenuItem>
      </Menu>
    </>
  ) : (
    <>-</>
  );
};

export default UserStatus;
