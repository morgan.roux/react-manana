import React, { FC } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core';
import { TableBody as MaterialTableBody } from '@material-ui/core';
import { Column } from './../../Interface';
import { TableRow } from './TableRow';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  body: {},
});
interface TableBodyProps {
  columns?: Column[];
  data: any;
  className?: string;
}

const TableBody: FC<TableBodyProps & WithStyles> = ({ classes, columns, data, className }) => {
  return (
    <MaterialTableBody>
      {data.map((value: any) => (
        <TableRow key={value.id} value={value} columns={columns} classes={{ root: className || '' }} />
      ))}
    </MaterialTableBody>
  );
};

export default withStyles(styles)(TableBody);
