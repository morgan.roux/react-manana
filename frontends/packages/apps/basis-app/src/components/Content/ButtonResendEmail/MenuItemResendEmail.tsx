import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useResend_User_EmailMutation } from '@lib/common/src/graphql';
import { CircularProgress, ListItemIcon, MenuItem, Typography } from '@material-ui/core';
import { Send } from '@material-ui/icons';
import React, { FC } from 'react';

interface ResendEmailProps {
  email: string;
  login: string;
  disabled: boolean;
}

const MenuItemResendEmail: FC<ResendEmailProps> = ({ email, login, disabled }) => {
  const displayNotification = useDisplayNotification();
  const { graphql } = useApplicationContext();

  const [doResendMail, { loading }] = useResend_User_EmailMutation({
    client: graphql,
    onError: (error) => {
      displayNotification({
        type: 'error',
        message: error.message,
      });
    },
    onCompleted: (_) => {
      displayNotification({
        type: 'success',
        message: 'Mail de définition de mot de passe re-envoyé avec succès',
      });
    },
  });

  const resendMail = () => {
    if (email && login) {
      doResendMail({ variables: { email, login } });
    }
  };

  return (
    <MenuItem onClick={resendMail} disabled={disabled}>
      <ListItemIcon>{loading ? <CircularProgress size="20px" /> : <Send />}</ListItemIcon>
      <Typography variant="inherit">Ré-envoyer l'email d'activation</Typography>
    </MenuItem>
  );
};

export default MenuItemResendEmail;
