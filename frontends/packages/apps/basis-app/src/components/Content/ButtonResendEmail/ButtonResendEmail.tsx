import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useResend_User_EmailMutation } from '@lib/common/src/graphql';
import { IconButton, Fade, Tooltip } from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';
import React, { FC } from 'react';
import { LoaderSmall } from '../Loader';

interface ResendProps {
  email: string;
  login: string;
}

const ButtonResendEmail: FC<ResendProps> = ({ email, login }) => {
  const { graphql } = useApplicationContext();
  const displayNotification = useDisplayNotification();
  const [doResendMail, { loading }] = useResend_User_EmailMutation({
    client: graphql,
    onError: (error) => {
      displayNotification({
        type: 'error',
        message: error.message,
      });
    },
    onCompleted: (_) => {
      displayNotification({
        type: 'success',
        message: `Mail de définition de mot de passe re-envoyé avec succès`,
      });
    },
  });

  const resendMail = () => {
    doResendMail({
      variables: { email, login },
    });
  };

  if (loading) return <LoaderSmall />;

  return (
    <>
      <Tooltip
        TransitionComponent={Fade}
        TransitionProps={{ timeout: 600 }}
        title="Ré-envoyer l'email de demande d'activation"
      >
        <IconButton
          style={{ textTransform: 'none' }}
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={resendMail}
          disabled={email && login ? false : true}
        >
          <SendIcon />
        </IconButton>
      </Tooltip>
    </>
  );
};

export default ButtonResendEmail;
