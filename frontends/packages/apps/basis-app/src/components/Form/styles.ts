import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '& .main-MuiStepper-root': {
        maxWidth: '40%',
        width: '40%',
      },
    },
    formRow: {
      width: '100%',
      // marginLeft: '70px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      flexDirection: 'row',
      padding: '20px 26px',
      '& > div': {
        maxWidth: 390,
        marginBottom: '0px !important',
        '& input, & .MuiSelect-root': {
          // fontWeight: '600',
          fontSize: 16,
          letterSpacing: 0.28,
        },
      },
      '& > div > div': {
        height: 55,
      },
    },
  })
);

export default useStyles;
