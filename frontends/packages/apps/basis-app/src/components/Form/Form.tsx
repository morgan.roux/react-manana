import { useFilters, useSubToolbar } from '@lib/common';
import FormContainer from '@lib/common/src/components/FormContainer';
import InputRow from '@lib/common/src/components/InputRow';
import { Stepper } from '@lib/common/src/components/Stepper';
import {
  Backdrop,
  CustomAutocomplete,
  CustomDatePicker,
  CustomFormTextField,
  CustomSelect,
  SmallLoading,
} from '@lib/ui-kit';
import { Box, Divider, Switch } from '@material-ui/core';
import React, { FC, ReactNode, useEffect } from 'react';
import { useParams } from 'react-router';
import useStyles from './styles';

interface FormControlInterface {}

interface FormStepContentInterface {
  title?: string;
  formControls?: FormControlInterface[];
  children?: ReactNode;
}

interface FormStepInterface {
  title: string;
  content?: FormStepContentInterface[];
  children?: ReactNode;
}

export interface FormStepperInterface {
  steps: FormStepInterface[];
  goToHome?: any;
  disableNextButton?: boolean;
  finalStep?: any;
  activeStep: number;
  setActiveStep: any;
}
interface FormProps {
  createTitle: string;
  editTitle: string;
  stepper?: FormStepperInterface;
  form?: FormStepContentInterface[];
  upsert?: any;
  upsertBtnLabel?: string;
  loading?: boolean;
  mutationLoading?: boolean;
  onClickBack?: () => void;
  className?: string;
}

const Form: FC<FormProps> = ({
  createTitle,
  editTitle,
  stepper,
  loading,
  mutationLoading,
  form,
  upsert,
  upsertBtnLabel,
  onClickBack,
  className,
}) => {
  const classes = useStyles({});

  const { id }: any = useParams();

  const { resetSearchFilters } = useFilters();

  const subToolbar = useSubToolbar({
    title: id ? editTitle : createTitle,
    upsert,
    upsertBtnLabel,
    withBackground: true,
    onClickBack,
  });

  useEffect(() => {
    return () => {
      resetSearchFilters();
    };
  }, []);

  if (loading) {
    return <SmallLoading />;
  }

  if (stepper) {
    return (
      <div className={classes.root}>
        {mutationLoading && <Backdrop value={`${id ? 'Modification' : 'Ajout'} en cours ...`} />}
        <Stepper
          title={id ? editTitle : createTitle}
          steps={stepper.steps.map((step) => ({
            title: step.title,
            content: (
              <Box width="100%" justifyContent="center" alignItems="center" display="flex" flexDirection="column">
                {step.content &&
                  step.content.map((content) => (
                    <FormContainer className={className} title={content.title}>
                      {content.formControls &&
                        content.formControls.map((formControl: any) => {
                          if (formControl.hidden) {
                            return null;
                          }

                          if (formControl.select) {
                            return (
                              <InputRow
                                divider={<Divider />}
                                className={classes.formRow}
                                title={formControl.label}
                                required={formControl.required}
                              >
                                <CustomSelect {...formControl.select} />
                              </InputRow>
                            );
                          }
                          if (formControl.textfield) {
                            return (
                              <InputRow
                                divider={<Divider />}
                                className={classes.formRow}
                                title={formControl.label}
                                required={formControl.required}
                              >
                                <CustomFormTextField {...formControl.textfield} />
                              </InputRow>
                            );
                          }
                          if (formControl.autocomplete) {
                            return (
                              <InputRow title={formControl.label} required={formControl.required}>
                                <CustomAutocomplete {...formControl.autocomplete} />
                              </InputRow>
                            );
                          }
                          if (formControl.datepicker) {
                            return (
                              <InputRow title={formControl.label} required={formControl.required}>
                                <CustomDatePicker {...formControl.datepicker} />
                              </InputRow>
                            );
                          }

                          if (formControl.switch) {
                            return (
                              <InputRow title={formControl.label} required={formControl.required}>
                                <Switch {...formControl.switch} />
                              </InputRow>
                            );
                          }
                        })}
                      {content.children}
                    </FormContainer>
                  ))}
                {step.children}
              </Box>
            ),
          }))}
          backToHome={stepper.goToHome}
          disableNextBtn={stepper.disableNextButton || false}
          fullWidth={false}
          finalStep={stepper.finalStep}
          activeStepFromProps={stepper.activeStep}
          setActiveStepFromProps={stepper.setActiveStep}
        />
      </div>
    );
  }

  return (
    <Box width="100%" justifyContent="center" alignItems="center" display="flex" flexDirection="column">
      {subToolbar}
      {form &&
        form.map((container) => (
          <FormContainer className={className} title={container.title}>
            {container.formControls &&
              container.formControls.map((formControl: any) => {
                if (formControl.select) {
                  return (
                    <InputRow title={formControl.label} required={formControl.required}>
                      <CustomSelect {...formControl.select} />
                    </InputRow>
                  );
                }
                if (formControl.textfield) {
                  return (
                    <InputRow title={formControl.label} required={formControl.required}>
                      <CustomFormTextField {...formControl.textfield} />
                    </InputRow>
                  );
                }
                if (formControl.autocomplete) {
                  return (
                    <InputRow title={formControl.label} required={formControl.required}>
                      <CustomAutocomplete {...formControl.autocomplete} />
                    </InputRow>
                  );
                }
                if (formControl.datepicker) {
                  return (
                    <InputRow title={formControl.label} required={formControl.required}>
                      <CustomDatePicker {...formControl.datepicker} />
                    </InputRow>
                  );
                }
              })}
            {container.children}
          </FormContainer>
        ))}
    </Box>
  );
};

export default Form;
