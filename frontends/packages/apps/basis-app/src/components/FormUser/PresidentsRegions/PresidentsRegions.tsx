import { getMonthName, getMonthNumber, isInvalidArray, useApplicationContext, useUploadFiles } from '@lib/common';
import {
  FichierInput,
  TypeFichier,
  useCreate_User_PrsidentMutation,
  useTitulaireQuery,
  useTitulaire_PharmaciesQuery,
} from '@lib/common/src/graphql';
import { Backdrop } from '@lib/ui-kit';
import React, { FC, useEffect, useState } from 'react';
import { Formulaire } from '../Formulaire';
import { IRoles } from '../FormUser';
import InfoFormUser from '../InfoFormUser';
import { IFragementPharmacie } from '../TitulairePharmacies/TitulairePharmacies';

interface PresidentsRegionsProps {
  id: string;
  userId: string;
  goTo: () => any;
}

const initUser = {
  id: '',
  userName: '',
  email: '',
  status: '',
};

const initPharmacie = {
  id: '',
  nom: '',
  mail: '',
  adresse: '',
  tel: '',
  users: [initUser],
};

const PresidentsRegions: FC<PresidentsRegionsProps> = ({ id, userId, goTo }) => {
  const [nom, setNom] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [code, setCode] = useState<string>('');
  const [roles, setRoles] = useState<IRoles[]>([{ code: '', nom: '' }]);
  const [pharmacies, setPharmacies] = useState<IFragementPharmacie[]>([initPharmacie]);
  const [pharmacie, setPharmacie] = useState<IFragementPharmacie>(initPharmacie);
  const [login, setLogin] = useState<string>('');

  /** birth date */
  const [days, setDays] = useState<string>('');
  const [month, setMonth] = useState<string>('');
  const [year, setYear] = useState<string>('');

  /** Photo */
  const [photo, setPhoto] = useState<File | any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const [permissionsAccess, setPermissionsAccess] = useState<any[]>([]);
  const permissionsAccessCodes = permissionsAccess.map((i: any) => i && i.code);

  const { currentGroupement, graphql, notify } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();
  const { loading: getLoading } = useTitulaireQuery({
    client: graphql,
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: (data) => {
      if (data && data.titulaire) {
        const nom = data.titulaire.nom ? data.titulaire.nom.trim() : '';
        const prenom = data.titulaire.prenom ? data.titulaire.prenom.trim() : '';
        setEmail('');
        /* if (data.titulaire.mail) setEmail(data.titulaire.mail.trim()); */
        setNom(`${nom} ${prenom}`);

        if (userId) {
          const titUser = data.titulaire.users && data.titulaire.users.find((u) => u && u.id === userId);
          if (titUser && titUser.email) {
            setEmail(titUser.email);
          }

          const defaultLogin = titUser ? titUser.login || titUser.email : '';
          setLogin(defaultLogin || '');

          if (data.titulaire.pharmacieUser && data.titulaire.pharmacieUser.id) {
            setCode(data.titulaire.pharmacieUser.id);
          }

          if (titUser && titUser.anneeNaissance) {
            setYear(titUser.anneeNaissance.toString());
          }

          if (titUser && titUser.moisNaissance) {
            setMonth(getMonthName(titUser.moisNaissance));
          }

          if (titUser && titUser.jourNaissance) {
            setDays(titUser.jourNaissance.toString());
          }

          /**
           * Set photo on edit
           */
          const newPhoto = data.titulaire && titUser && titUser.userPhoto && titUser.userPhoto.fichier;
          if (newPhoto) {
            setPhoto(newPhoto);
          }

          /**
           * Set selected permissionsAccess
           */
          const codeTraitements = data.titulaire && titUser && titUser.codeTraitements;
          if (codeTraitements && codeTraitements.length > 0) {
            const newUserTraitements: any[] = codeTraitements.map((i) => {
              if (i) return { code: i };
            });
            setPermissionsAccess(newUserTraitements);
          }
        }
      }
      if (
        data &&
        data.titulaire &&
        data.titulaire.users &&
        data.titulaire.users.length > 0 &&
        data.titulaire.users[0] &&
        data.titulaire.users[0].login
      ) {
        setLogin(data.titulaire.users[0].login);
      }
    },
  });

  const [createUserPresidentRegion, { loading: createUserLoading }] = useCreate_User_PrsidentMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createUserPresidentRegion) {
        setLoading(false);
        notify({
          type: 'success',
          message: "Mail de définition de mot de passe envoyé à l'utilisateur",
        });
        goTo();
      }
    },
    onError: (errors) => {
      setLoading(false);
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map((error) => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'MAIL_NOT_SENT':
              errorMessage = "L'email n'est pas envoyé à l'utilisatuer";
              break;
            case 'LOGIN_ALREADY_EXIST':
              errorMessage = 'Cet email est déjà utilisé par un autre utilisateur';
              break;
            case 'ACCOUNT_ALREADY_CREATED':
              errorMessage = 'Le président de région a déjà un compte créer sur cette pharmacie';
              break;
          }
        }
      });
      notify({
        type: 'error',
        message: errorMessage,
      });
    },
  });

  const { loading: getPharmacieLoading } = useTitulaire_PharmaciesQuery({
    client: graphql,
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: (data) => {
      if (data && data.titulairePharmacies && data.titulairePharmacies) {
        if (data.titulairePharmacies[0] !== null) {
          const pharmacies = [initPharmacie];
          setRoles(
            data.titulairePharmacies.map((pharmacie: any) => {
              const id = pharmacie.id ? pharmacie.id : '';
              const cip = pharmacie.cip ? pharmacie.cip : '';
              const nom = pharmacie.nom ? pharmacie.nom : '';
              const mail = pharmacie.mail ? pharmacie.mail : '';
              const adresse = pharmacie.adresse1 ? pharmacie.adresse1 : pharmacie.adresse2 ? pharmacie.adresse2 : '';
              const users = pharmacie.users ? pharmacie.users : [];
              const tel = pharmacie.tele1 ? pharmacie.tele1 : pharmacie.tele2 ? pharmacie.tele2 : '';
              pharmacies.push({ id, nom, mail, adresse, tel, users });
              return { code: id, nom: `${cip} - ${nom}` };
            })
          );
          setPharmacies(pharmacies);
        }
      }
    },
  });

  useEffect(() => {
    pharmacies.map((pharmacie) => {
      if (pharmacie.id === code) setPharmacie(pharmacie);
    });
  }, [pharmacies]);

  useEffect(() => {
    if (roles && roles.length === 1) {
      setCode(roles[0].code);
    }
  }, [roles]);

  const handleChange = (name: string, value: string) => {
    console.log('name value', name, value);

    switch (name) {
      case 'code':
        setCode(value);
        break;
      case 'email':
        setEmail(value);
        break;
      case 'days':
        setDays(value);
        break;
      case 'month':
        setMonth(value);
        break;
      case 'year':
        setYear(value);
        break;
      case 'login':
        setLogin(value);
        break;
    }
  };

  const submit = () => {
    setLoading(true);
    if (photo && photo.size) {
      uploadFiles([photo], {
        directory: 'users/photos',
      })
        .then((uploadedFiles) => {
          if (uploadedFiles.length > 0) {
            createUserPresidentRegion({
              variables: {
                idGroupement: currentGroupement.id,
                id,
                email,
                login,
                userId,
                idPharmacie: code,
                day: Number(days),
                month: getMonthNumber(month),
                year: Number(year),
                userPhoto: {
                  chemin: uploadedFiles[0].chemin,
                  nomOriginal: uploadedFiles[0].nomOriginal,
                  type: TypeFichier.Photo,
                },
                codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
              },
            });
          }
        })
        .catch((error: any) => {
          console.error(error);
          setLoading(false);
          notify({
            type: 'error',
            message:
              "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
          });
        });
      return;
    }

    let userPhoto: FichierInput | null = null;
    if (photo && photo.id) {
      userPhoto = {
        chemin: photo.chemin,
        nomOriginal: photo.nomOriginal,
        type: TypeFichier.Avatar,
        idAvatar: photo && photo.idAvatar,
      };
    }
    createUserPresidentRegion({
      variables: {
        idGroupement: currentGroupement.id,
        id,
        email,
        login,
        userId,
        idPharmacie: code,
        day: Number(days),
        month: getMonthNumber(month),
        year: Number(year),
        userPhoto,
        codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
      },
    });
  };

  return (
    <>
      {(createUserLoading || uploadingFiles.loading || loading) && <Backdrop />}
      <Formulaire
        user={{ nom, email, login }}
        userId={userId}
        values={{ code, roles, days, month, year }}
        label="Pharmacie"
        doCreateOrUpdate={submit}
        handleChange={handleChange}
        goTo={goTo}
        photo={photo}
        setPhoto={setPhoto}
        permissionsAccess={permissionsAccess}
        setPermissionsAccess={setPermissionsAccess}
        loading={getLoading || getPharmacieLoading}
      >
        {pharmacie && pharmacie.id && (
          <div>
            <InfoFormUser pharmacie={pharmacie} />
          </div>
        )}
      </Formulaire>
    </>
  );
};

export default PresidentsRegions;
