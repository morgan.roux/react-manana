import {
  formatFilenameWithoutDate,
  getMonthNumber,
  isInvalidArray,
  PARTENAIRE_SERVICE,
  useApplicationContext,
  useUploadFiles,
} from '@lib/common';
import {
  FichierInput,
  TypeFichier,
  useCreate_User_Partenaire_ServiceMutation,
  usePartenaireQuery,
} from '@lib/common/src/graphql';
import { Backdrop } from '@lib/ui-kit';
import { Theme } from '@material-ui/core/styles';
import withStyles, { CSSProperties } from '@material-ui/core/styles/withStyles';
import React, { FC, useState } from 'react';
import { Formulaire } from '../Formulaire';
import { IRoles } from '../FormUser';

const styles = (theme: Theme): Record<string, CSSProperties> => ({});

interface PROPS {
  id: string;
  userId: string;
  goTo: () => any;
}

const PartenairesServices: FC<PROPS> = ({ id, userId, goTo }) => {
  const [nom, setNom] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [login, setLogin] = useState<string>('');
  const [days, setDays] = useState<string>('');
  const [month, setMonth] = useState<string>('');
  const [year, setYear] = useState<string>('');
  const [code, setCode] = useState<string>('');
  const [roles, setRoles] = useState<IRoles[]>([{ code: '', nom: '' }]);

  /** Photo */
  const [photo, setPhoto] = useState<File | any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const [permissionsAccess, setPermissionsAccess] = useState<any[]>([]);
  const permissionsAccessCodes = permissionsAccess.map((i: any) => i && i.code);
  const { currentGroupement: groupement, currentPharmacie: pharmacie, graphql, notify } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const { loading: getLoading } = usePartenaireQuery({
    client: graphql,
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: (data) => {
      if (data && data.partenaire) {
        const nom = data.partenaire.nom ? data.partenaire.nom : '';
        setNom(nom);

        const defaultEmail =
          userId && data.partenaire.user && data.partenaire.user.email ? data.partenaire.user.email : '';
        setEmail(defaultEmail);

        const defaultLogin =
          userId && data.partenaire.user && data.partenaire.user.login ? data.partenaire.user.login : defaultEmail;

        setLogin(defaultLogin);

        /**
         * Set photo on edit
         */
        const newPhoto =
          data.partenaire &&
          data.partenaire.user &&
          data.partenaire.user.userPhoto &&
          data.partenaire.user.userPhoto.fichier;
        if (newPhoto) {
          setPhoto(newPhoto);
        }

        /**
         * Set selected permissionsAccess
         */
        const codeTraitements = data.partenaire && data.partenaire.user && data.partenaire.user.codeTraitements;
        if (codeTraitements && codeTraitements.length > 0) {
          const newUserTraitements: any[] = codeTraitements.map((i) => {
            if (i) return { code: i };
          });
          setPermissionsAccess(newUserTraitements);
        }
      }
      if (data && data.partenaire && data.partenaire.user && data.partenaire.user.login) {
        setLogin(data.partenaire.user.login);
      }
    },
  });

  const [createUserPartenaireService, { loading: createUserLoading }] = useCreate_User_Partenaire_ServiceMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createUserPartenaireService) {
        setLoading(false);
        notify({ type: 'success', message: `Mail de définition de mot de passe envoyé à l'utilisateur` });
        goTo();
      }
    },
    onError: (errors) => {
      setLoading(false);
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map((error) => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'MAIL_NOT_SENT':
              errorMessage = "L'email n'est pas envoyé à l'utilisatuer";
              break;
            case 'LOGIN_ALREADY_EXIST':
              errorMessage = 'Ce login est déjà utilisé par un autre utilisateur';
              break;
          }
        }
      });
      notify({ type: 'error', message: errorMessage });
    },
  });

  const handleChange = (name: string, value: string) => {
    if (name === 'email') setEmail(value);
    else if (name === 'login') setLogin(value);
    else if (name === 'days') setDays(value);
    else if (name === 'month') setMonth(value);
    else if (name === 'year') setYear(value);
    else setCode(value);
  };

  const submit = () => {
    setLoading(true);
    if (photo && photo.size) {
      uploadFiles([photo], {
        directory: 'users/photos',
      })
        .then((uploadedFiles) => {
          if (uploadedFiles.length > 0) {
            createUserPartenaireService({
              variables: {
                idGroupement: groupement.id,
                id,
                email,
                login,
                userPhoto: {
                  chemin: uploadedFiles[0].chemin,
                  nomOriginal: uploadedFiles[0].nomOriginal,
                  type: TypeFichier.Photo,
                },
                codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
                idPharmacie: pharmacie?.id,
              },
            });
          }
        })
        .catch((error) => {
          console.error(error);
          setLoading(false);
          notify({
            type: 'error',
            message:
              "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
          });
        });
      return;
    }

    let userPhoto: FichierInput | null = null;
    if (photo && photo.id) {
      userPhoto = {
        chemin: photo.chemin,
        nomOriginal: photo.nomOriginal,
        type: TypeFichier.Avatar,
        idAvatar: photo && photo.idAvatar,
      };
    }
    createUserPartenaireService({
      variables: {
        idGroupement: groupement.id,
        id,
        email,
        login,
        userPhoto,
        day: Number(days),
        month: getMonthNumber(month),
        year: Number(year),
        codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
        idPharmacie: pharmacie?.id,
      },
    });
  };

  return (
    <>
      {(createUserLoading || uploadingFiles.loading || loading) && <Backdrop />}
      <Formulaire
        user={{ nom, email, login }}
        userId={userId}
        //values={{ code, roles, year, month, days }}
        userRole={PARTENAIRE_SERVICE}
        doCreateOrUpdate={submit}
        handleChange={handleChange}
        goTo={goTo}
        photo={photo}
        setPhoto={setPhoto}
        permissionsAccess={permissionsAccess}
        setPermissionsAccess={setPermissionsAccess}
        loading={getLoading}
      />
    </>
  );
};

export default withStyles(styles)(PartenairesServices);
