import { getMonthName, getMonthNumber, isInvalidArray, useApplicationContext, useUploadFiles } from '@lib/common';
import {
  TacheResponsableInfoFragment,
  useGenerate_One_Collaborateur_FonctionMutation,
  useUpdate_Many_Tache_ResponsablesMutation,
} from '@lib/common/src/federation';
import {
  FichierInput,
  TypeFichier,
  useCreate_User_CollaborateurMutation,
  usePersonnel_PharmacyQuery,
  useSearch_RolesQuery,
} from '@lib/common/src/graphql';
import { Backdrop } from '@lib/ui-kit';
import { Theme } from '@material-ui/core';
import withStyles, { CSSProperties } from '@material-ui/core/styles/withStyles';
import React, { FC, useEffect, useState } from 'react';
import { Formulaire } from '../Formulaire';

const styles = (theme: Theme): Record<string, CSSProperties> => ({});

interface PROPS {
  id: string;
  userId: string;
  goTo: () => any;
}

const PersonnelPharmacies: FC<PROPS> = ({ id, userId, goTo }) => {
  const [nom, setNom] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [emailLink, setEmailLink] = useState<string>('');
  const [idPharmacie, setIdPharmacie] = useState<string>('');
  const [code, setCode] = useState<string>('');
  const [roles, setRoles] = useState<any[]>([]);
  const [login, setLogin] = useState<string>('');

  /** birth date */
  const [days, setDays] = useState<string>('');
  const [month, setMonth] = useState<string>('');
  const [year, setYear] = useState<string>('');

  /** Photo */
  const [photo, setPhoto] = useState<File | any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const [permissionsAccess, setPermissionsAccess] = useState<any[]>([]);
  const permissionsAccessCodes = permissionsAccess.map((i: any) => i && i.code);

  const [matriceTacheResponsables, setMatriceTacheResponsables] = useState<TacheResponsableInfoFragment[]>([]);

  const { currentGroupement: groupement, graphql, federation, notify } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  /**
   * Search roles
   */
  const { data: dataRoles, loading: loadingRoles } = useSearch_RolesQuery({
    client: graphql,
    variables: {
      type: ['role'],
      filterBy: [{ term: { typeRole: 'PHARMACIE' } }],
      sortBy: [{ nom: { order: 'asc' } }],
    },
  });

  const [updateManyTacheResponsables, modificationManyTacheResponsables] = useUpdate_Many_Tache_ResponsablesMutation({
    client: federation,
    onCompleted: () => {
      displaySucessMessage();
    },
    onError: () => {
      setLoading(false);
      notify({
        type: 'error',
        message: 'Erreur lors de la modification de la matrice des fonctions de base',
      });
    },
  });

  const [generateCollaborateurProjetPerso] = useGenerate_One_Collaborateur_FonctionMutation({
    client: federation,
    onError: () => {
      setLoading(false);
      notify({
        type: 'error',
        message: 'Erreur lors de la modification de la création du projet perso du collaborateur',
      });
    },
  });

  const upsertMatriceTaches = (idUser: string, idPharmacie: string) => {
    updateManyTacheResponsables({
      variables: {
        idUser,
        idPharmacie,
        taches: matriceTacheResponsables.map(({ idFonction, idTache, idType }) => ({ idFonction, idTache, idType })),
      },
    });

    generateCollaborateurProjetPerso({
      variables: {
        idPharmacie,
        idUser,
      },
    });
  };

  /**
   * Set init roles
   */
  useEffect(() => {
    if (dataRoles && dataRoles.search && dataRoles.search.data && dataRoles.search.data.length > 0) {
      setRoles(dataRoles.search.data);
    }
  }, [dataRoles]);

  const { loading: getLoading, data: personnelData } = usePersonnel_PharmacyQuery({
    client: graphql,
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: (data) => {
      if (data && data.ppersonnel) {
        const nom = data.ppersonnel.nom ? data.ppersonnel.nom : '';
        const prenom = data.ppersonnel.prenom ? data.ppersonnel.prenom : '';
        setNom(`${prenom} ${nom}`);
        if (data.ppersonnel.pharmacie && data.ppersonnel.pharmacie.id) {
          setIdPharmacie(data.ppersonnel.pharmacie.id);
        }
        const defaultEmail =
          userId && data.ppersonnel.user && data.ppersonnel.user.email ? data.ppersonnel.user.email : '';
        setEmail(defaultEmail);

        const defaultLogin =
          userId && data.ppersonnel.user && data.ppersonnel.user.login ? data.ppersonnel.user.login : defaultEmail;

        setLogin(defaultLogin);

        if (userId && data.ppersonnel.role && data.ppersonnel.role.code) {
          setCode(data.ppersonnel.role.code);
        }
        // setRoles(initiRoles);
        if (data.ppersonnel.user && data.ppersonnel.user.anneeNaissance) {
          setYear(data.ppersonnel.user.anneeNaissance.toString());
        }
        if (data.ppersonnel.user && data.ppersonnel.user.moisNaissance) {
          setMonth(getMonthName(data.ppersonnel.user.moisNaissance));
        }
        if (data.ppersonnel.user && data.ppersonnel.user.jourNaissance) {
          setDays(data.ppersonnel.user.jourNaissance.toString());
        }
        if (data.ppersonnel.user && data.ppersonnel.user.login) {
          setLogin(data.ppersonnel.user.login);
        }

        if (
          data.ppersonnel.pharmacie &&
          data.ppersonnel.pharmacie.titulaires &&
          data.ppersonnel.pharmacie.titulaires &&
          data.ppersonnel.pharmacie.titulaires.length > 0
        ) {
          const titulaire = data.ppersonnel.pharmacie.titulaires[0];
          const userTitulaire = titulaire && titulaire.users && titulaire.users.length > 0 && titulaire.users[0];
          if (userTitulaire && userTitulaire.email) {
            // setEmailLink(userTitulaire.email);
          }
        }

        /**
         * Set photo on edit
         */
        const newPhoto =
          data.ppersonnel &&
          data.ppersonnel.user &&
          data.ppersonnel.user.userPhoto &&
          data.ppersonnel.user.userPhoto.fichier;
        if (newPhoto) {
          setPhoto(newPhoto);
        }

        /**
         * Set selected permissionsAccess
         */
        const codeTraitements = data.ppersonnel && data.ppersonnel.user && data.ppersonnel.user.codeTraitements;
        if (codeTraitements && codeTraitements.length > 0) {
          const newUserTraitements: any[] = codeTraitements.map((i) => {
            if (i) return { code: i };
          });
          setPermissionsAccess(newUserTraitements);
        }
      }
    },
  });

  const displaySucessMessage = () => {
    setLoading(false);
    notify({
      type: 'success',
      message: `Mail de définition de mot de passe envoyé à l'utilisateur`,
    });
    goTo();
  };

  const [createUserCollaborateurOfficine, { loading: createUserLoading }] = useCreate_User_CollaborateurMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createUserCollaborateurOfficine) {
        if (data.createUserCollaborateurOfficine.user?.id && data.createUserCollaborateurOfficine.idPharmacie) {
          upsertMatriceTaches(
            data.createUserCollaborateurOfficine.user.id,
            data.createUserCollaborateurOfficine.idPharmacie
          );
        } else {
          displaySucessMessage();
        }
      }
    },
    onError: (errors) => {
      setLoading(false);
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map((error) => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'MAIL_NOT_SENT':
              errorMessage = "L'email n'est pas envoyé à l'utilisatuer";
              break;
            case 'LOGIN_ALREADY_EXIST':
              errorMessage = 'Ce login est déjà utilisé par un autre utilisateur';
              break;
            case 'ACCOUNT_ALREADY_CREATED':
              errorMessage = 'Compte est déjà créé pour ce personnel';
              break;
          }
        }
      });

      notify({
        type: 'error',
        message: errorMessage,
      });
    },
  });

  const handleChange = (name: string, value: string) => {
    switch (name) {
      case 'email':
        setEmail(value);
        break;
      case 'login':
        setLogin(value);
        break;
      case 'days':
        setDays(value);
        break;
      case 'month':
        setMonth(value);
        break;
      case 'year':
        setYear(value);
        break;
      default:
        setCode(value);
    }
  };

  const submit = () => {
    setLoading(true);
    if (photo && photo.size) {
      uploadFiles([photo], {
        directory: 'users/photos',
      })
        .then((uploadedFiles) => {
          if (uploadedFiles.length > 0) {
            createUserCollaborateurOfficine({
              variables: {
                idGroupement: groupement.id,
                id,
                idPharmacie,
                email,
                login,
                role: code,
                userId,
                day: Number(days),
                month: getMonthNumber(month),
                year: Number(year),
                userPhoto: {
                  chemin: uploadedFiles[0].chemin,
                  nomOriginal: uploadedFiles[0].nomOriginal,
                  type: TypeFichier.Photo,
                },
                codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
              },
            });
          }
        })
        .catch((error) => {
          console.error(error);
          setLoading(false);
          notify({
            type: 'error',
            message:
              "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
          });
        });
      return;
    }

    let userPhoto: FichierInput | null = null;
    if (photo && photo.id) {
      userPhoto = {
        chemin: photo.chemin,
        nomOriginal: photo.nomOriginal,
        type: TypeFichier.Avatar,
        idAvatar: photo && photo.idAvatar,
      };
    }
    createUserCollaborateurOfficine({
      variables: {
        idGroupement: groupement.id,
        id,
        idPharmacie,
        email,
        login,
        role: code,
        userId,
        day: Number(days),
        month: getMonthNumber(month),
        year: Number(year),
        userPhoto,
        codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
      },
    });
  };

  return (
    <>
      {(createUserLoading ||
        uploadingFiles.loading ||
        loading ||
        loadingRoles ||
        modificationManyTacheResponsables.loading) && <Backdrop />}
      <Formulaire
        idPharmacie={idPharmacie}
        matriceTacheResponsables={matriceTacheResponsables}
        onChangeMatriceTacheResponsables={setMatriceTacheResponsables}
        user={{
          nom,
          email,
          login,
          emailLink,
          userLinks: (personnelData?.ppersonnel?.pharmacie?.titulaires || [])
            .filter((titulaire) => titulaire?.users)
            .reduce((links, titulaire) => {
              return [...(titulaire?.users || []), ...links] as any;
            }, []),
        }}
        userId={userId}
        values={{ code, roles, days, month, year }}
        label="Rôle"
        doCreateOrUpdate={submit}
        handleChange={handleChange}
        goTo={goTo}
        photo={photo}
        setPhoto={setPhoto}
        permissionsAccess={permissionsAccess}
        setPermissionsAccess={setPermissionsAccess}
        loading={getLoading}
      />
    </>
  );
};

export default withStyles(styles)(PersonnelPharmacies);
