import {
  PARTENAIRE_LABORATOIRE_URL,
  PARTENAIRE_SERVICE_URL,
  PERSONNEL_GROUPEMENT_URL,
  PERSONNEL_PHARMACIE_URL,
  PRESIDENT_REGION_URL,
  TITULAIRE_PHARMACIE_URL,
} from '@lib/common';
import React, { FC } from 'react';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import { PartenairesServices } from './PartenairesServices';
// import { PartenairesLaboratoires } from './PartenairesLaboratoires';
// import { PartenairesServices } from './PartenairesServices';
// import { PersonnelGroupement } from './PersonnelGroupement';
import { PersonnelPharmacies } from './PersonnelPharmacies';
import { PresidentsRegions } from './PresidentsRegions';
import { TitulairePharmacies } from './TitulairePharmacies';

export interface IRoles {
  code: string;
  nom: string;
}

const FormUser: FC = () => {
  const { push } = useHistory();
  const { pathname } = useLocation();
  const { id, userId }: any = useParams();

  const isOnPresidentRegion = pathname.includes(PRESIDENT_REGION_URL);
  const isOnPersonnelGroupement = pathname.includes(PERSONNEL_GROUPEMENT_URL);
  const isOnPartenaireLaboratoire = pathname.includes(PARTENAIRE_LABORATOIRE_URL);
  const isOnPersonnelPharmacie = pathname.includes(PERSONNEL_PHARMACIE_URL);
  const isOnPartenaireService = pathname.includes(PARTENAIRE_SERVICE_URL);
  const isOnTitulairePharmacie = pathname.includes(TITULAIRE_PHARMACIE_URL);

  const goTo = () => {
    let url = '';
    if (isOnPresidentRegion) url = PRESIDENT_REGION_URL;
    if (isOnPersonnelGroupement) url = PERSONNEL_GROUPEMENT_URL;
    if (isOnPartenaireLaboratoire) url = PARTENAIRE_LABORATOIRE_URL;
    if (isOnPersonnelPharmacie) url = PERSONNEL_PHARMACIE_URL;
    if (isOnPartenaireService) url = PARTENAIRE_SERVICE_URL;
    if (isOnTitulairePharmacie) url = TITULAIRE_PHARMACIE_URL;

    if (url) push(`/db/${url}`);
  };

  if (isOnPresidentRegion) {
    return <PresidentsRegions id={id} userId={userId} goTo={goTo} />;
  }

  // if (isOnPersonnelGroupement) {
  //   return <PersonnelGroupement id={id} userId={userId} goTo={goTo} />;
  // }

  // if (isOnPartenaireLaboratoire) {
  //   return <PartenairesLaboratoires id={id} userId={userId} goTo={goTo} />;
  // }

  if (isOnPersonnelPharmacie) {
    return <PersonnelPharmacies id={id} userId={userId} goTo={goTo} />;
  }

  if (isOnPartenaireService) {
    return <PartenairesServices id={id} userId={userId} goTo={goTo} />;
  }

  if (isOnTitulairePharmacie) {
    return <TitulairePharmacies id={id} userId={userId} goTo={goTo} />;
  }

  return null;
};

export default FormUser;
