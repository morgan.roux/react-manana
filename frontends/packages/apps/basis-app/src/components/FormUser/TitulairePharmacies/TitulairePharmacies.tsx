import { getMonthName, getMonthNumber, isInvalidArray, useApplicationContext, useUploadFiles } from '@lib/common';
import {
  TacheResponsableInfoFragment,
  useGenerate_One_Collaborateur_FonctionMutation,
  useUpdate_Many_Tache_ResponsablesMutation,
} from '@lib/common/src/federation';
import {
  FichierInput,
  TypeFichier,
  useCreate_User_TitulaireMutation,
  useTitulaireQuery,
  useTitulaire_PharmaciesQuery,
} from '@lib/common/src/graphql';
import { Backdrop } from '@lib/ui-kit';
import React, { FC, useEffect, useState } from 'react';
import { Formulaire } from '../Formulaire';
import { IRoles } from '../FormUser';
import InfoFormUser from '../InfoFormUser';

interface TitulairePharmaciesProps {
  id: string;
  userId: string;
  goTo: () => any;
}

export interface IFragementPharmacie {
  id: string;
  nom: string;
  mail: string;
  adresse: string;
  tel: string;
  users: IUsers[];
}

export interface IUsers {
  id: string;
  userName: string;
  email: string;
  status: string;
}

const initUser = {
  id: '',
  userName: '',
  email: '',
  status: '',
};

const initPharmacie = {
  id: '',
  nom: '',
  mail: '',
  adresse: '',
  tel: '',
  users: [initUser],
};

const TitulairePharmacies: FC<TitulairePharmaciesProps> = ({ id, userId, goTo }) => {
  const [nom, setNom] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [code, setCode] = useState<string>('');
  const [roles, setRoles] = useState<IRoles[]>([{ code: '', nom: '' }]);
  const [pharmacies, setPharmacies] = useState<IFragementPharmacie[]>([initPharmacie]);
  const [pharmacie, setPharmacie] = useState<IFragementPharmacie>(initPharmacie);
  const [login, setLogin] = useState<string>('');

  /** birth date */
  const [days, setDays] = useState<string>('');
  const [month, setMonth] = useState<string>('');
  const [year, setYear] = useState<string>('');

  /** Photo */
  const [photo, setPhoto] = useState<File | any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const [permissionsAccess, setPermissionsAccess] = useState<any[]>([]);
  const permissionsAccessCodes = permissionsAccess.map((i: any) => i && i.code);

  const [matriceTacheResponsables, setMatriceTacheResponsables] = useState<TacheResponsableInfoFragment[]>([]);

  const { federation, currentGroupement: groupement, graphql, notify } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const [updateManyTacheResponsables] = useUpdate_Many_Tache_ResponsablesMutation({
    client: federation,
    onCompleted: () => {
      displaySucessMessage('Modification de la matrice des fonctions de base réussie');
    },
    onError: () => {
      setLoading(false);
      notify({
        type: 'error',
        message: 'Erreur lors de la modification de la matrice des fonctions de base',
      });
    },
  });

  const [generateCollaborateurProjetPerso] = useGenerate_One_Collaborateur_FonctionMutation({
    client: federation,
    onError: () => {
      setLoading(false);
      notify({
        type: 'error',
        message: 'Erreur lors de la modification de la création du projet perso du collaborateur',
      });
    },
  });

  const displaySucessMessage = (message: string) => {
    setLoading(false);
    notify({
      type: 'success',
      message: `Mail de définition de mot de passe envoyé à l'utilisateur`,
    });
    goTo();
  };

  const upsertMatriceTaches = (idUser: string, idPharmacie: string) => {
    updateManyTacheResponsables({
      variables: {
        idUser,
        idPharmacie,
        taches: matriceTacheResponsables.map(({ idFonction, idTache, idType }) => ({ idFonction, idTache, idType })),
      },
    });

    generateCollaborateurProjetPerso({
      variables: {
        idPharmacie,
        idUser,
      },
    });
  };

  const { loading: getLoading } = useTitulaireQuery({
    client: graphql,
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: (data) => {
      if (data && data.titulaire) {
        const nom = data.titulaire.nom ? data.titulaire.nom.trim() : '';
        const prenom = data.titulaire.prenom ? data.titulaire.prenom.trim() : '';
        setEmail('');
        /* if (data.titulaire.mail) setEmail(data.titulaire.mail.trim()); */
        setNom(`${prenom} ${nom}`);

        if (userId) {
          const titUser = data.titulaire.users && data.titulaire.users.find((u) => u && u.id === userId);
          if (titUser && titUser.email) {
            setEmail(titUser.email);
          }

          const defaultLogin = titUser ? titUser.login || titUser.email : '';
          setLogin(defaultLogin || '');

          if (data.titulaire.pharmacieUser && data.titulaire.pharmacieUser.id) {
            setCode(data.titulaire.pharmacieUser.id);
          }

          if (titUser && titUser.anneeNaissance) {
            setYear(titUser.anneeNaissance.toString());
          }

          if (titUser && titUser.moisNaissance) {
            setMonth(getMonthName(titUser.moisNaissance));
          }

          if (titUser && titUser.jourNaissance) {
            setDays(titUser.jourNaissance.toString());
          }

          /**
           * Set photo on edit
           */
          const newPhoto = data.titulaire && titUser && titUser.userPhoto && titUser.userPhoto.fichier;
          if (newPhoto) {
            setPhoto(newPhoto);
          }

          /**
           * Set selected permissionsAccess
           */
          const codeTraitements = data.titulaire && titUser && titUser.codeTraitements;
          if (codeTraitements && codeTraitements.length > 0) {
            const newUserTraitements: any[] = codeTraitements.map((i) => {
              if (i) return { code: i };
            });
            setPermissionsAccess(newUserTraitements);
          }
        }
      }
    },
  });

  const { loading: getPharmacieLoading } = useTitulaire_PharmaciesQuery({
    client: graphql,
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: (data) => {
      if (data && data.titulairePharmacies && data.titulairePharmacies) {
        if (data.titulairePharmacies[0] !== null) {
          const pharmacies = [initPharmacie];
          setRoles(
            data.titulairePharmacies.map((pharmacie: any) => {
              const id = pharmacie.id ? pharmacie.id : '';
              const cip = pharmacie.cip ? pharmacie.cip : '';
              const nom = pharmacie.nom ? pharmacie.nom : '';
              const mail = pharmacie.mail ? pharmacie.mail : '';
              const adresse = pharmacie.adresse1 ? pharmacie.adresse1 : pharmacie.adresse2 ? pharmacie.adresse2 : '';
              const users = pharmacie.users ? pharmacie.users : [];
              const tel = pharmacie.tele1 ? pharmacie.tele1 : pharmacie.tele2 ? pharmacie.tele2 : '';
              pharmacies.push({ id, nom, mail, adresse, tel, users });
              return { code: id, nom: `${cip} - ${nom}` };
            })
          );
          setPharmacies(pharmacies);
        }
      }
    },
  });

  const [createUserTitulaire, { loading: createUserLoading }] = useCreate_User_TitulaireMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createUserTitulaire && data.createUserTitulaire.pharmacieUser?.id) {
        const user = (data.createUserTitulaire?.users || []).find((user) => user?.login === login);
        if (user) {
          upsertMatriceTaches(user.id, data.createUserTitulaire.pharmacieUser.id);
        } else {
          displaySucessMessage(`Titulaire ${userId ? 'modifié' : 'ajouté'} avec succès`);
        }
      }
    },
    onError: (errors) => {
      setLoading(false);
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map((error) => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'MAIL_NOT_SENT':
              errorMessage = "L'email n'est pas envoyé à l'utilisatuer";
              break;
            case 'LOGIN_ALREADY_EXIST':
              errorMessage = 'Ce login est déjà utilisé par un autre utilisateur';
              break;
            case 'ACCOUNT_ALREADY_CREATED':
              errorMessage = 'Le titulaire a déjà un compte créer sur cette pharmacie';
              break;
          }
        }
      });

      notify({
        type: 'error',
        message: errorMessage,
      });
    },
  });

  useEffect(() => {
    pharmacies.map((pharmacie) => {
      if (pharmacie.id === code) setPharmacie(pharmacie);
    });
  }, [pharmacies]);

  useEffect(() => {
    if (roles && roles.length === 1) {
      setCode(roles[0].code);
    }
  }, [roles]);

  const handleChange = (name: string, value: string) => {
    switch (name) {
      case 'email':
        setEmail(value);
        break;
      case 'login':
        setLogin(value);
        break;
      case 'days':
        setDays(value);
        break;
      case 'month':
        setMonth(value);
        break;
      case 'year':
        setYear(value);
        break;
      default:
        setCode(value);
    }
  };

  const submit = () => {
    setLoading(true);
    if (photo && photo.size) {
      uploadFiles([photo], {
        directory: 'users/photos',
      })
        .then((uploadedFiles) => {
          if (uploadedFiles.length > 0) {
            createUserTitulaire({
              variables: {
                idGroupement: groupement.id,
                id,
                email,
                login,
                idPharmacie: code,
                userId,
                day: Number(days),
                month: getMonthNumber(month),
                year: Number(year),
                userPhoto: {
                  chemin: uploadedFiles[0].chemin,
                  nomOriginal: uploadedFiles[0].nomOriginal,
                  type: TypeFichier.Photo,
                },
                codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
              },
            });
          }
        })
        .catch((error) => {
          console.error(error);
          setLoading(false);
          notify({
            type: 'error',
            message:
              "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
          });
        });
      return;
    }

    let userPhoto: FichierInput | null = null;
    if (photo && photo.id) {
      userPhoto = {
        chemin: photo.chemin,
        nomOriginal: photo.nomOriginal,
        type: TypeFichier.Avatar,
        idAvatar: photo && photo.idAvatar,
      };
    }
    createUserTitulaire({
      variables: {
        idGroupement: groupement.id,
        id,
        email,
        login,
        idPharmacie: code,
        userId,
        day: Number(days),
        month: getMonthNumber(month),
        year: Number(year),
        userPhoto,
        codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
      },
    });
  };

  return (
    <>
      {(createUserLoading || uploadingFiles.loading || loading) && <Backdrop />}
      <Formulaire
        matriceTacheResponsables={matriceTacheResponsables}
        onChangeMatriceTacheResponsables={setMatriceTacheResponsables}
        idPharmacie={code}
        user={{ nom, email, login }}
        values={{ code, roles, year, month, days }}
        label="CIP"
        userId={userId}
        doCreateOrUpdate={submit}
        handleChange={handleChange}
        goTo={goTo}
        photo={photo}
        setPhoto={setPhoto}
        permissionsAccess={permissionsAccess}
        setPermissionsAccess={setPermissionsAccess}
        loading={getLoading || getPharmacieLoading}
      >
        {pharmacie && pharmacie.id && (
          <div>
            <InfoFormUser pharmacie={pharmacie} />
          </div>
        )}
      </Formulaire>
    </>
  );
};

export default TitulairePharmacies;
