import { px2rem } from '@lib/common';
import { withStyles, Theme, Tooltip } from '@material-ui/core';

const HtmlTooltip = withStyles((theme: Theme) => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    color: 'rgba(0, 0, 0, 0.87)',
    fontSize: px2rem(12, theme),
    border: '1px solid #dadde9',
  },
}))(Tooltip);

export default HtmlTooltip;
