import { isValidDays, isValidYears, useApplicationContext, useDisplayNotification } from '@lib/common';
import Stepper, { Step } from '@lib/common/src/components/Stepper/Stepper';
import { TacheResponsableInfoFragment, useSave_User_AppetencesMutation } from '@lib/common/src/federation';
import { Backdrop } from '@lib/ui-kit';
import React, { Dispatch, FC, SetStateAction, useState } from 'react';
import { useLocation } from 'react-router';
import { FormulaireStepOne, FormulaireStepTwo } from './FormulaireSteps';
import FormulaireAppetence from './FormulaireSteps/FormulaireAppetence';
import FormulaireMatriceTache from './FormulaireSteps/FormulaireMatriceTache';

export interface FormulaireProps {
  user: {
    nom: string;
    email: string;
    login?: string;
    emailLink?: string;
    userLinks?: {
      id: string;
      email: string;
    }[];
  };
  values?: {
    code: string;
    roles: any[];
    year: string;
    month: string;
    days: string;
  };
  userRole?: string;
  label?: string;
  userId: string;
  idPharmacie?: string;
  goTo: () => any;
  doCreateOrUpdate: () => any;
  handleChange: (name: string, value: string) => any;
  photo: File | any;
  setPhoto: Dispatch<SetStateAction<File | any>>;
  permissionsAccess: any[];
  setPermissionsAccess: Dispatch<SetStateAction<any[]>>;
  loading?: boolean;

  matriceTacheResponsables?: TacheResponsableInfoFragment[];
  onChangeMatriceTacheResponsables?: (selected: TacheResponsableInfoFragment[]) => void;
}

const Formulaire: FC<FormulaireProps> = ({
  label,
  user,
  values,
  userRole,
  idPharmacie,
  goTo,
  userId,
  doCreateOrUpdate,
  handleChange,
  photo,
  setPhoto,
  permissionsAccess,
  setPermissionsAccess,
  loading,
  matriceTacheResponsables = [],
  onChangeMatriceTacheResponsables,
}) => {
  const { pathname } = useLocation();
  const isOnCreate: boolean = pathname.includes('/create/');

  const displayNotification = useDisplayNotification();

  const { federation } = useApplicationContext();

  const title = `${isOnCreate ? 'Création' : 'Modification'} d'un compte utilisateur`;

  const [appetences, setAppetences] = useState<any[]>([]);

  const [doSaveUserAppetences, { loading: mutationLoading }] = useSave_User_AppetencesMutation({
    client: federation,
    variables: {
      idUser: userId,
      appetences: appetences.filter((item) => item.ordre).map((item) => ({ idItem: item.id, ordre: item.ordre })),
    },
    onError: (error) => {
      displayNotification({
        type: 'error',
        message: error.message,
      });
    },
  });

  const doSubmit = () => {
    doCreateOrUpdate();
    if (appetences.length > 0) {
      doSaveUserAppetences();
    }
  };

  const isDisabled = () => {
    if ((values && !values.year) || !user.login) return true; //!isEmailValid(user.email)) return true;
    if (label && values && !values.code) return true;
    if (values && !values.year) return true;
    if (!isValidYears(values?.year || '')) return true;
    if (values && values.days && values.month) {
      if (!isValidDays(values.days, Number(values.month))) {
        return true;
      }
    }
    return false;
  };

  const finalStepBtn = {
    buttonLabel: isOnCreate ? 'Créer' : 'Modifier',
    action: doSubmit,
  };
  const steps: Step[] = [
    {
      title: 'Informations personnelles',
      content: (
        <FormulaireStepOne
          user={user}
          values={values}
          label={label}
          userId={userId}
          doCreateOrUpdate={doCreateOrUpdate}
          handleChange={handleChange}
          goTo={goTo}
          photo={photo}
          setPhoto={setPhoto}
          permissionsAccess={permissionsAccess}
          setPermissionsAccess={setPermissionsAccess}
        />
      ),
    },
    ...(idPharmacie
      ? [
          {
            title: 'Matrice des fonctions de base',
            content: (
              <FormulaireMatriceTache
                fullNameUser={user.nom}
                idUser={userId}
                selected={matriceTacheResponsables || []}
                onSelectionChange={(selected) =>
                  onChangeMatriceTacheResponsables && onChangeMatriceTacheResponsables(selected)
                }
                idPharmacie={idPharmacie}
              />
            ),
          },
        ]
      : []),

    {
      title: "Droits d'accès",

      content: (
        <FormulaireStepTwo
          user={user}
          values={values}
          userRole={userRole}
          label={label}
          userId={userId}
          doCreateOrUpdate={doCreateOrUpdate}
          handleChange={handleChange}
          goTo={goTo}
          photo={photo}
          setPhoto={setPhoto}
          permissionsAccess={permissionsAccess}
          setPermissionsAccess={setPermissionsAccess}
        />
      ),
    },
    {
      title: 'Scores',
      content: <FormulaireAppetence fullNameUser={user.nom} setAppetences={setAppetences} />,
    },
  ];

  return (
    <>
      {loading && <Backdrop />}
      <Stepper
        title={title}
        steps={steps}
        disableNextBtn={isDisabled()}
        // disableNextBtn={false}
        backToHome={goTo}
        finalStep={finalStepBtn}
        finished={false}
      />
    </>
  );
};

Formulaire.defaultProps = {
  label: 'Rôle',
};

export default Formulaire;
