import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useUser_AppetencesQuery } from '@lib/common/src/federation';
import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import AppetenceList from '../../../AppetenceList';
import useStyles from '../styles';

interface FormulaireAppetenceProps {
  match: {
    params: {
      userId: string | undefined;
    };
  };
  fullNameUser: string;
  setAppetences?: (appetences: any[]) => void;
}

const FormulaireAppetence: FC<FormulaireAppetenceProps> = ({ fullNameUser, setAppetences }) => {
  const classes = useStyles({});
  const { userId }: any = useParams();
  const [itemList, setItemList] = useState<any[]>([]);
  const { federation } = useApplicationContext();
  const displayNotification = useDisplayNotification();
  const { data, loading } = useUser_AppetencesQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
    variables: {
      idUser: userId,
    },
    skip: !userId,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const handleOrderChange = (id: string, event: any) => {
    const { value } = event.target;

    setItemList((prevState) =>
      prevState.map((item: any, index) => (item.id === id ? { ...item, ordre: parseInt(value) } : item))
    );
  };

  useEffect(() => {
    const appetences = itemList.filter((item) => item.ordre);
    if (appetences.length > 0 && setAppetences) {
      setAppetences(appetences);
    }
  }, [itemList]);

  return (
    <div className={classes.content}>
      <Box className={classes.title}>{fullNameUser}</Box>
      <AppetenceList
        handleOrderChange={handleOrderChange}
        itemList={itemList}
        setItemList={setItemList}
        loading={loading}
        appetences={data?.userAppetences || []}
        withAddBtn={false}
        sorting={true}
      />
    </div>
  );
};

export default FormulaireAppetence;
