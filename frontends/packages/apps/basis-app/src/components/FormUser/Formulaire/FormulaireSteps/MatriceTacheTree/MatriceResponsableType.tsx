import React, { FC } from 'react';
import { Button } from '@material-ui/core';
import { ArrowDropDown } from '@material-ui/icons';

interface MatriceResponsableTypeProps {
  collaborateurResponsables: any[];
  value?: string;
  onClick?: (event: React.MouseEvent<HTMLElement>) => void;
}

const MatriceResponsableType: FC<MatriceResponsableTypeProps> = ({ collaborateurResponsables, value, onClick }) => {
  const type = value ? collaborateurResponsables.find(({ type }) => type.id === value)?.type : undefined;
  const code = type?.code;
  const couleur = type?.couleur || '#E0E0E0';

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    if (onClick) {
      onClick(event);
    }
  };

  return (
    <Button
      style={{
        backgroundColor: couleur,
        minWidth: 'unset',
        width: 30,
        height: 30,
        color: code ? 'white' : undefined,
      }}
      variant="outlined"
      onClick={handleClick}
    >
      {code ? code : <ArrowDropDown />}
    </Button>
  );
};

export default MatriceResponsableType;
