import { useApplicationContext } from '@lib/common';
import {
  DqmtTache,
  FonctionInfoFragment,
  ResponsableTypeInfoFragment,
  TacheResponsableInfoFragment,
  useGet_Fonctions_With_Responsable_InfosQuery,
} from '@lib/common/src/federation';
import { ErrorPage } from '@lib/ui-kit';
import { Box, CircularProgress, Typography, Collapse } from '@material-ui/core';
import { createStyles, fade, Theme, withStyles } from '@material-ui/core';
import { TransitionProps } from '@material-ui/core/transitions';
import { KeyboardArrowDown, KeyboardArrowRight } from '@material-ui/icons';
import TreeItem, { TreeItemProps } from '@material-ui/lab/TreeItem';
import TreeView from '@material-ui/lab/TreeView';
import React, { FC, useEffect, useState } from 'react';
import { animated, useSpring } from 'react-spring'; // web.cjs is required for IE 11 support
import MatriceResponsableType from './MatriceResponsableType';
import MatriceResponsableTypeMenu from './MatriceResponsableTypeMenu';
import useStyles from './styles';

export interface MatriceTacheTreeProps {
  niveauMatriceFonctions?: number;
  idPharmacie: string;
  idUser?: string;
  fullNameUser: string;
  selected: TacheResponsableInfoFragment[];
  onSelectionChange: (selected: TacheResponsableInfoFragment[]) => void;
}

const MatriceTacheTree: FC<MatriceTacheTreeProps> = ({
  niveauMatriceFonctions = 2,
  selected,
  idPharmacie,
  idUser,
  onSelectionChange,
}) => {
  const classes = useStyles({});
  const { federation } = useApplicationContext();
  const { loading, data, error } = useGet_Fonctions_With_Responsable_InfosQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
    variables: {
      idPharmacie,
      paging: {
        offset: 0,
        limit: 50,
      },
    },
  });

  function TransitionComponent(props: TransitionProps) {
    const style = useSpring({
      from: { opacity: 0, transform: 'translate3d(20px,0,0)' },
      to: { opacity: props.in ? 1 : 0, transform: `translate3d(${props.in ? 0 : 20}px,0,0)` },
    });

    return (
      <animated.div style={style}>
        <Collapse {...props} />
      </animated.div>
    );
  }

  const StyledTreeItem = withStyles((theme: Theme) =>
    createStyles({
      iconContainer: {
        '& .close': {
          opacity: 0.3,
        },
      },
      group: {
        marginLeft: 7,
        paddingLeft: 18,
        borderLeft: `1px dashed ${fade(theme.palette.text.primary, 0.4)}`,
      },
    })
  )((props: TreeItemProps) => <TreeItem {...props} TransitionComponent={TransitionComponent} />);

  const [expanded, setExpanded] = useState<string[]>([]);
  const [menuAnchor, setMenuAnchor] = React.useState<null | HTMLElement>(null);
  const [selectedFonctionOrTache, setSelectedFonctionOrTache] = useState<null | any>(null);

  useEffect(() => {
    if (!loading && data?.dQMTFonctions) {
      setExpanded((data.dQMTFonctions.nodes || []).map(({ id }) => id));
    }
  }, [loading]);

  const handleExpand = (item: FonctionInfoFragment) => {
    setExpanded((prevState) => [...prevState, item.id]);
  };

  const handleCloseExpand = (item: FonctionInfoFragment) => {
    setExpanded((prevState) => prevState.filter((old) => old !== item.id));
  };

  const handleOpenMenu = (tache: DqmtTache, event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setSelectedFonctionOrTache(tache);
    setMenuAnchor(event.currentTarget);
  };

  const handleCloseMenu = () => {
    setMenuAnchor(null);
  };

  const handleClickItemMenu = (type: ResponsableTypeInfoFragment | undefined, event: React.MouseEvent<HTMLElement>) => {
    if (selectedFonctionOrTache) {
      setMenuAnchor(null);

      if (!type) {
        onSelectionChange(
          selected.filter((item) =>
            niveauMatriceFonctions !== 1
              ? item.idTache !== selectedFonctionOrTache.id
              : item.idFonction !== selectedFonctionOrTache.id
          )
        );
      } else {
        onSelectionChange([
          ...selected.filter((item) =>
            niveauMatriceFonctions !== 1
              ? item.idTache !== selectedFonctionOrTache.id
              : item.idFonction !== selectedFonctionOrTache.id
          ),
          {
            idTache: niveauMatriceFonctions === 1 ? undefined : selectedFonctionOrTache.id,
            idFonction: niveauMatriceFonctions === 1 ? selectedFonctionOrTache.id : selectedFonctionOrTache.idFonction,
            idUser: idUser || '',
            idPharmacie,
            idType: type.id,
            id: '',
            idGroupement: '',
            __typename: 'DQMTTacheResponsable',
          },
        ]);
      }
    }
  };

  console.log('************************SELECTED****************', selected);

  const tachesComponent = (taches: any[], type: 'fonction' | 'tache') => {
    return (taches || [])
      .filter(({ idProjet, active }: any) => !!idProjet && active)
      .sort((node1: any, node2: any) => node1.ordre - node2.ordre)
      .map((item: any) => (
        <div className={classes.nodeItem} key={item.id}>
          <MatriceResponsableType
            value={
              selected.find((responsable) =>
                type === 'tache' ? responsable.idTache === item.id : responsable.idFonction === item.id
              )?.idType
            }
            collaborateurResponsables={(item as any).collaborateurResponsables}
            onClick={(event) => handleOpenMenu(item, event)}
          />
          <StyledTreeItem nodeId={`${item.id}`} label={item.libelle} />
        </div>
      ));
  };

  const TreeViewMemo = React.useMemo(
    () =>
      niveauMatriceFonctions === 1 ? (
        tachesComponent(data?.dQMTFonctions.nodes || [], 'fonction')
      ) : (
        <TreeView className={classes.treeView} expanded={expanded}>
          {(data?.dQMTFonctions.nodes || [])
            .filter(({ idProjet, active }) => !!idProjet && active)
            .sort((node1, node2) => node1.ordre - node2.ordre)
            .map((item) => (
              <div className={classes.nodeItem} key={item.id}>
                {expanded.includes(item.id) ? (
                  <KeyboardArrowDown onClick={() => handleCloseExpand(item)} />
                ) : (
                  item.ordre > 0 && <KeyboardArrowRight onClick={() => handleExpand(item)} />
                )}
                {item.ordre > 0 && <Typography className={classes.total}>{item.ordre}</Typography>}

                <StyledTreeItem nodeId={`${item.id}`} label={`${item.libelle} (${item.nombreTaches})`}>
                  {tachesComponent(item.taches, 'tache')}
                </StyledTreeItem>
              </div>
            ))}
        </TreeView>
      ),
    [loading, expanded, selected]
  );

  if (loading) {
    return (
      <Box display="flex" justifyContent="center">
        <CircularProgress />
      </Box>
    );
  }

  if (error) {
    return <ErrorPage />;
  }

  return (
    <>
      <MatriceResponsableTypeMenu
        idUser={idUser}
        anchorEl={menuAnchor || undefined}
        collaborateurResponsables={(selectedFonctionOrTache as any)?.collaborateurResponsables || []}
        onClickItem={handleClickItemMenu}
        onClose={handleCloseMenu}
      />
      {TreeViewMemo}
    </>
  );
};

export default MatriceTacheTree;
