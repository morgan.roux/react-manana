import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    fieldset: {
      maxWidth: 500,
      width: '100%',
      '& > fieldset': {
        border: '1px solid rgba(0, 0, 0, 0.23)',
        borderRadius: 5,
        padding: '11px 26px 20px 18px',
      },
      '& .MuiCheckbox-root, & .MuiCheckbox-colorSecondary.Mui-checked': {
        color: theme.palette.primary.main,
        padding: '0px 9px',
      },
      '& .MuiInputBase-root.Mui-disabled': {
        color: '#ccc',
        '&:hover fieldset': {
          border: '1px solid rgba(0, 0, 0, 0.38)',
          // borderColor: 'rgba(0, 0, 0, 0.38)',
        },
        '& .Mui-focused fieldset': {
          border: '1px solid rgba(0, 0, 0, 0.38)',
          // borderColor: 'rgba(0, 0, 0, 0.38)',
        },
      },
    },
    fieldsetLegend: {
      marginLeft: 5,
    },
    fieldsetLabel: {
      color: theme.palette.common.black,
    },
    treeView: {
      flexGrow: 1,
      // maxWidth: 400,
      '& .MuiTreeItem-label': {
        fontSize: '0.875rem',
      },
    },
    nodeItem: {
      display: 'flex',
      margin: '8px 0',
    },
    checkbox: {
      maxHeight: 24,
    },
    total: {
      maxHeight: 24,
      width: 'auto',
      minWidth: 24,
      padding: 4,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 14,
      background: '#E0E0E0',
    },
  })
);
export default useStyles;
