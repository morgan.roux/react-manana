import { ErrorPage, LoaderSmall } from '@lib/ui-kit';
import { useApplicationContext } from '@lib/common';
import { useGet_Tache_ResponsablesLazyQuery } from '@lib/common/src/federation';
import { Box } from '@material-ui/core';
import React, { FC, useEffect } from 'react';
import useStyles from '../styles';
import MatriceTacheTree, { MatriceTacheTreeProps } from './MatriceTacheTree/MatriceTacheTree';
import { useNiveauMatriceFonctions } from '@lib/hooks';

const FormulaireMatriceTache: FC<MatriceTacheTreeProps> = ({
  idUser,
  fullNameUser,
  idPharmacie,
  selected,
  onSelectionChange,
}) => {
  const classes = useStyles({});
  const niveauMatriceFonctions = useNiveauMatriceFonctions();
  const { federation } = useApplicationContext();

  const [loadTacheResponsables, loadingResponsables] = useGet_Tache_ResponsablesLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
    onCompleted: (result) => {
      onSelectionChange(result.dQMTTacheResponsables.nodes);
    },
  });

  useEffect(() => {
    if (idUser) {
      loadTacheResponsables({
        variables: {
          filter: {
            idUser: {
              eq: idUser,
            },
          },
          paging: {
            offset: 0,
            limit: 200,
          },
        },
      });
    }
  }, [idUser]);

  const loading = loadingResponsables.called && loadingResponsables.loading;
  const error = loadingResponsables.called ? loadingResponsables.error : undefined;

  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Box className={classes.title}>{fullNameUser}</Box>
        {loading ? (
          <LoaderSmall />
        ) : error ? (
          <ErrorPage />
        ) : (
          <MatriceTacheTree
            niveauMatriceFonctions={niveauMatriceFonctions}
            idUser={idUser}
            selected={selected}
            onSelectionChange={onSelectionChange}
            idPharmacie={idPharmacie}
            fullNameUser={fullNameUser}
          />
        )}
      </Box>
    </Box>
  );
};

export default FormulaireMatriceTache;
