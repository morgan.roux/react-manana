import React, { FC } from 'react';
import useStyles from '../styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { FormulaireProps } from '../Formulaire';
import { Box } from '@material-ui/core';
import { TITULAIRE_PHARMACIE_URL, PRESIDENT_REGION_URL } from '@lib/common';
import { TITULAIRE_PHARMACIE, PRESIDENT_REGION } from '@lib/common/src/auth/roles';
import PermissionAccessList from '@lib/common/src/components/PermissionAccessList';

const FormulaireStepTwo: FC<FormulaireProps & RouteComponentProps> = ({
  children,
  user,
  permissionsAccess,
  setPermissionsAccess,
  values,
  location: { pathname },
  userRole,
}) => {
  const classes = useStyles({});

  const isTitulaire = pathname.includes(TITULAIRE_PHARMACIE_URL);
  const isPresident = pathname.includes(PRESIDENT_REGION_URL);

  const rolesCodes: string[] = isTitulaire
    ? [TITULAIRE_PHARMACIE]
    : isPresident
    ? [PRESIDENT_REGION]
    : values && values.code
    ? [values.code]
    : userRole
    ? [userRole]
    : [];

  return (
    <div className={classes.content}>
      <Box className={classes.title}>{user.nom}</Box>

      {children}
      <PermissionAccessList selected={permissionsAccess} setSelected={setPermissionsAccess} rolesCodes={rolesCodes} />
    </div>
  );
};

export default withRouter(FormulaireStepTwo);
