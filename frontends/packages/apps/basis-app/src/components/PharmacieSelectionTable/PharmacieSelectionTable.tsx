import { useApplicationContext } from '@lib/common';
import CustomContent, { SearchInput } from '@lib/common/src/components/newCustomContent';
import { BuildQuery } from '@lib/common/src/components/withSearch/queryBuilder';
import { useSearch_Custom_Content_PharmacieQuery } from '@lib/common/src/graphql';
import { CustomFullScreenModal, SmallLoading } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { useLocation } from 'react-router';
import DetailsTitulaire from '../../pharmacie/pages/TitulairesPharmacies/FicheTitulaire';
import useStyles from './styles';
import useColumns from './useColumns';

interface PharmacieSelectionTableProps {
  selected?: any;
  setSelected?: (selected: any) => void;
  onClickRow?: (row: any) => void;
  isSelectable?: boolean;
  activeRow?: any;
}

const PharmacieSelectionTable: FC<PharmacieSelectionTableProps> = ({
  selected,
  setSelected,
  onClickRow,
  isSelectable,
  activeRow,
}) => {
  const classes = useStyles({});

  const { pathname } = useLocation();

  const { currentGroupement, graphql, isSuperAdmin, pharmacies } = useApplicationContext();

  const [openModalTitulaire, setOpenModalTitulaire] = useState<boolean>(false);
  const [idTitulaire, setIdTitulaire] = useState<string>('');

  const [openModalPharmacie, setOpenModalPharmacie] = useState<boolean>(false);
  //const [idPharmacie, setIdPharmacie] = useState<string>('');

  const handleOpenTitulaireDetails = (id: string) => {
    setIdTitulaire(id);
    setOpenModalTitulaire(true);
  };

  const handleOpenPharmacieDetails = (id: string) => {
    // setIdPharmacie(id);
    setOpenModalPharmacie(true);
  };

  const isOnPilotage: boolean = pathname.includes('pilotage');

  const viewInModal: boolean = isOnPilotage ? true : false;

  const must = [{ term: { idGroupement: currentGroupement.id } }];

  const { query, skip, take } = BuildQuery({ type: 'pharmacie', optionalMust: must });

  const variables = { take, skip, type: ['pharmacie'], query };

  let listResult = useSearch_Custom_Content_PharmacieQuery({ client: graphql, variables, skip: !isSuperAdmin });
  if (!isSuperAdmin) {
    listResult = {
      loading: false,
      data: {
        search: {
          total: pharmacies.length,
          data: pharmacies,
        },
      },
    } as any;
  }

  const columns = useColumns({ viewInModal, handleOpenTitulaireDetails, handleOpenPharmacieDetails });

  const listPharmacie = (
    <Box width="100%">
      {isSuperAdmin && (
        <Box className={classes.searchInputBox}>
          <SearchInput searchPlaceholder="Rechercher une pharmacie" />
        </Box>
      )}
      <Box className={classes.listRoot}>
        <CustomContent {...{ listResult, columns, activeRow, isSelectable, onClickRow, selected, setSelected }} />
      </Box>
    </Box>
  );

  if (listResult.loading) {
    return <SmallLoading />;
  }

  return (
    <Box width="100%" display="flex" justifyContent="center">
      {listPharmacie}
      <CustomFullScreenModal
        title="Fiche Titulaire"
        open={openModalTitulaire}
        setOpen={setOpenModalTitulaire}
        closeIcon={true}
        withBtnsActions={false}
        headerWithBgColor={true}
        fullScreen={true}
      >
        <DetailsTitulaire id={idTitulaire} hideFooterActions={true} />
      </CustomFullScreenModal>
      <CustomFullScreenModal
        title="Détails de pharmacie"
        open={openModalPharmacie}
        setOpen={setOpenModalPharmacie}
        withBtnsActions={false}
        fullScreen={true}
        withHeader={false}
      >
        {/*<FichePharmacie id={pharmacieId} inModalView={true} handleClickBack={closeModalPharma} />*/}
      </CustomFullScreenModal>
    </Box>
  );
};

export default PharmacieSelectionTable;
