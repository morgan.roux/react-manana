import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listRoot: {
      overflow: 'auto',
      maxHeight: 400,
      '& .MuiTableRow-head': {
        '& > th:nth-child(6), & > th:nth-child(7)': {
          minWidth: 116,
        },
        '& > th:nth-child(10), & > th:nth-child(11)': {
          minWidth: 128,
        },
      },
    },
    searchInputBox: {
      padding: '20px 20px',
      display: 'flex',
    },
    root: {
      width: '100%',
      padding: 24,
    },
  })
);

export default useStyles;
