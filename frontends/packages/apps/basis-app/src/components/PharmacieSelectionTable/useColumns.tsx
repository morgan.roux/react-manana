import { PHARMACIE_URL, TITULAIRE_PHARMACIE_URL } from '@lib/common';
import { Fade, IconButton, Link, Tooltip } from '@material-ui/core';
import { Visibility } from '@material-ui/icons';
import React, { Fragment } from 'react';
import { useHistory, useLocation } from 'react-router';

interface UseColumnsParams {
  viewInModal: boolean;
  handleOpenTitulaireDetails?: (id: string) => void;
  handleOpenPharmacieDetails?: (id: string) => void;
}

export const useColumns = (params: UseColumnsParams) => {
  const { viewInModal, handleOpenPharmacieDetails, handleOpenTitulaireDetails } = params;
  const { push } = useHistory();
  const { pathname } = useLocation();
  const isOnOC = pathname.includes('operations-commerciales');
  const isOnPilotage = pathname.includes('pilotage');
  const isOnCreate: boolean = pathname.includes('create');
  const isOnEdit: boolean = pathname.includes('edit');

  return [
    {
      name: 'cip',
      label: 'CIP',
      renderer: (value: any) => {
        return value.cip ? value.cip : '-';
      },
    },
    {
      name: 'achat.canal.libelle',
      label: 'Plateforme',
      renderer: (value: any) => {
        return (value.achat && value.achat.canal && value.achat.canal.libelle) || '-';
      },
    },
    {
      name: 'segmentation.contrat.nom',
      label: 'Contrat',
      renderer: (value: any) => {
        return (value && value.segmentation && value.segmentation.contrat && value.segmentation.contrat.nom) || '-';
      },
    },
    {
      name: 'nom',
      label: 'Nom',
      renderer: (value: any) => {
        return value.nom ? value.nom : '-';
      },
    },
    {
      name: 'titulaire.fullName',
      label: 'Titulaires',
      renderer: (value: any) => {
        const handleClick = (titulaireId: string) => (event: React.MouseEvent<any>) => {
          event.preventDefault();
          event.stopPropagation();
          if (push && !viewInModal) {
            push(`/db/${TITULAIRE_PHARMACIE_URL}/fiche/${titulaireId}`);
          }
          if (viewInModal || isOnOC || isOnPilotage || isOnCreate || isOnEdit) {
            if (handleOpenTitulaireDetails) {
              handleOpenTitulaireDetails(titulaireId);
            }
          }
        };
        const titulaires: any[] = value.titulaires || [];
        return titulaires.map((t: any, index: number) => {
          const isMultiple: boolean = titulaires.length > 1 && index < titulaires.length - 1;
          if (t.sortie == 0) {
            return (
              <Fragment key={`titulaire_link_${index}`}>
                <Link
                  color="secondary"
                  component="button"
                  variant="body2"
                  onClick={handleClick(t.id)}
                  style={{ textAlign: 'left' }}
                >
                  {`${t.fullName}`}
                  {isMultiple && (
                    <>
                      ,<span style={{ visibility: 'hidden' }}>&nbsp;</span>
                    </>
                  )}
                </Link>
              </Fragment>
            );
          }
          // else {
          //   return '-';
          // }
        });
      },
    },
    {
      name: 'adresse1',
      label: 'Adresse',
      renderer: (value: any) => {
        return value.adresse1 ? value.adresse1 : '-';
      },
    },
    {
      name: 'cp',
      label: 'Code postal',
      renderer: (value: any) => {
        return value.cp ? value.cp : '-';
      },
    },
    {
      name: 'departement.nom',
      label: 'Département',
      renderer: (value: any) => {
        return value.departement && value.departement.nom ? value.departement.nom : '-';
      },
    },
    {
      name: 'ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value.ville ? value.ville : '-';
      },
    },
    {
      name: 'tele1',
      label: 'Téléphone',
      renderer: (value: any) => {
        return value.tele1 ? value.tele1 : '-';
      },
    },
    {
      name: 'numIvrylab',
      label: 'Code plateforme',
      renderer: (value: any) => {
        return value.numIvrylab ? value.numIvrylab : '-';
      },
    },
    {
      name: 'sortie',
      label: 'Sortie',
      renderer: (value: any) => {
        return value.sortie ? (value.sortie === 1 ? 'Oui' : 'Non') : '-';
      },
    },
    {
      name: 'segmentation.trancheCA.libelle',
      label: 'Tranche CA',
      renderer: (value: any) => {
        return (
          (value && value.segmentation && value.segmentation.trancheCA && value.segmentation.trancheCA.libelle) || '-'
        );
      },
    },
    {
      name: 'actived',
      label: 'Statut',
      renderer: (value: any) => {
        return value && value.actived !== undefined && value.actived !== null
          ? value.actived
            ? 'Activée'
            : 'Inactive'
          : '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        const handleClick = (event: React.MouseEvent<any>) => {
          event.preventDefault();
          event.stopPropagation();
          if (value.id) {
            if (push && !viewInModal) {
              push(`/db/${PHARMACIE_URL}/fiche/${value.id}`);
            }
            if (viewInModal || isOnOC || isOnPilotage || isOnCreate || isOnEdit) {
              if (handleOpenPharmacieDetails) {
                handleOpenPharmacieDetails(value.id);
              }
            }
          }
        };

        return (
          <>
            <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Voir détails">
              <IconButton onClick={handleClick}>
                <Visibility />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];
};

export default useColumns;
