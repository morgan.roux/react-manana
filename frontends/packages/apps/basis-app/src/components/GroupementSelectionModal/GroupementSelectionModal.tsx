import { useApplicationContext } from '@lib/common';
import { useGroupementsQuery, GroupementInfoFragment } from '@lib/common/src/graphql';
import { Backdrop, CustomModal } from '@lib/ui-kit';
import { Box, ListItemAvatar, ListItemText, MenuItem, MenuList } from '@material-ui/core';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import no_image from '../../assets/img/pas_image.png';
import useStyles from './styles';

interface GroupementSelectionModalProps {
  open: boolean;
  setOpen: (open: boolean) => void;
  onSelect: (groupement: GroupementInfoFragment) => void;
  saving?: boolean;
}

const GroupementSelectionModal: FC<GroupementSelectionModalProps & RouteComponentProps> = ({
  onSelect,
  open,
  setOpen,
  saving,
}) => {
  const classes = useStyles({});
  const { currentGroupement, graphql } = useApplicationContext();
  const groupementsQuery = useGroupementsQuery({ client: graphql });
  const groupements = groupementsQuery.data?.groupements;

  return (
    <CustomModal open={open} setOpen={setOpen} title="Changement de groupement" withBtnsActions={false}>
      <Box maxHeight="200px">
        <Backdrop open={saving} value="Changement de groupement en cours ..." />
        <MenuList>
          {(groupements || [])
            .filter((el) => el?.id !== currentGroupement?.id)
            .map((groupement) => (
              <MenuItem key={`groupement-${groupement?.id}`} onClick={() => onSelect(groupement)}>
                <ListItemAvatar
                  children={<img width="64px" src={groupement?.groupementLogo?.fichier?.publicUrl || no_image} />}
                />
                <ListItemText className={classes.groupement}>{groupement?.nom}</ListItemText>
              </MenuItem>
            ))}
        </MenuList>
      </Box>
    </CustomModal>
  );
};

export default withRouter(GroupementSelectionModal);
