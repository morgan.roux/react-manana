import React from 'react';
import loadable from '@loadable/component';
import GroupIcon from '@material-ui/icons/Group';
import PharmacieIcon from '@material-ui/icons/LocalHospital';
import TitulaireIcon from '@material-ui/icons/AccountBox';

import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';

const TitulairesPharmaciesPage = loadable(() => import('./pages/TitulairesPharmacies/TitulairesPharmacies'), {
  fallback: <SmallLoading />,
});

const PersonnelsPharmaciesPage = loadable(() => import('./pages/PersonnelsPharmacies/PersonnelsPharmacies'), {
  fallback: <SmallLoading />,
});

const PharmaciesPage = loadable(() => import('./pages/Pharmacies/Pharmacies'), {
  fallback: <SmallLoading />,
});

const pharmacieModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_PHARMACIES',
      location: 'PARAMETRE',
      name: 'Pharmacies',
      to: '/pharmacies',
      icon: <PharmacieIcon />,
      preferredOrder: 210,
      authorize: {
        roles: ['SUPADM', 'GRPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_PARAMETRE_TITULAIRES',
      location: 'PARAMETRE',
      name: (context) => (context.isTitulaire ? 'Titulaires de la pharmacie' : 'Titulaires des pharmacies'),
      to: '/titulaires-pharmacies',
      icon: <TitulaireIcon />,
      preferredOrder: 220,
      authorize: {
        roles: ['SUPADM', 'GRPADM', 'PRMTIT'],
        rolesDenied: ['PRTSERVICE']
      },
    },
    {
      id: 'FEATURE_PARAMETRE_PERSONNEL_PHARMACIES',
      location: 'PARAMETRE',
      name: (context) =>
        context.isTitulaire ? 'Liste du Personnel de la Pharmacie' : 'Liste des Personnel des pharmacies',
      to: '/personnel-pharmacies',
      icon: <GroupIcon />,
      preferredOrder: 230,
      authorize: {
        roles: ['SUPADM', 'GRPADM', 'PRMTIT'],
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
  routes: [
    {
      path: [
        '/titulaires-pharmacies',
        '/titulaires-pharmacies/fiche/:id',
        '/titulaires-pharmacies/edit/:id',
        '/titulaires-pharmacies/create',
        '/titulaires-pharmacies/user/create/:id',
        '/titulaires-pharmacies/user/edit/:id/:userId',
      ],
      component: TitulairesPharmaciesPage,
      exact: false,
      attachTo: 'PARAMETRE',
    },
    {
      path: ['/pharmacies', '/pharmacies/create', '/pharmacies/edit/:id'],
      component: PharmaciesPage,
      exact: false,
      attachTo: 'PARAMETRE',
    },
    {
      path: [
        '/personnel-pharmacies',
        '/personnel-pharmacies/fiche/:id',
        '/personnel-pharmacies/edit/:id',
        '/personnel-pharmacies/create',
        '/personnel-pharmacies/user/create/:id',
        '/personnel-pharmacies/user/edit/:id/:userId',
      ],
      component: PersonnelsPharmaciesPage,
      exact: false,
      attachTo: 'PARAMETRE',
    },
  ],
};

export default pharmacieModule;
