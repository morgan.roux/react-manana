import Form from '@app/basis/src/components/Form';
import {
  TITULAIRE_PHARMACIE_URL,
  useApplicationContext,
  useDisplayNotification,
  useFilters,
  useSubToolbar,
} from '@lib/common';
import CustomContent, { SearchInput } from '@lib/common/src/components/newCustomContent';
import { BuildQuery } from '@lib/common/src/components/withSearch/queryBuilder';

import {
  useDeleteSoftTitulairesMutation,
  useSearch_Custom_Content_Titulaire_PharmacieQuery,
} from '@lib/common/src/graphql';
import { ConfirmDeleteDialog } from '@lib/ui-kit';
import { Divider } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useLocation, useParams } from 'react-router';
import FormUser from '../../../components/FormUser';
import DetailsTitulaire from './FicheTitulaire';
import useStyles from './styles';
import useColumns from './useColumns';
import useForm from './useForm';

const TitulairePharmacies: FC = () => {
  const type = 'titulaire';

  const { pathname } = useLocation();

  const { id }: any = useParams();

  const displayNotification = useDisplayNotification();

  const { resetSearchFilters } = useFilters();

  const onForm: boolean = pathname.includes('/create') || pathname.includes('/edit');

  const onUserForm: boolean = pathname.includes('/user');

  const classes = useStyles({});

  const [selected, setSelected] = useState<any>([]);

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const { graphql, isSuperAdmin, isAdminGroupement, currentGroupement, currentPharmacie } = useApplicationContext();

  const isAdmin = isAdminGroupement || isSuperAdmin;

  const must = isAdmin
    ? [{ term: { idGroupement: currentGroupement.id } }, { term: { sortie: 0 } }]
    : [
        { term: { idGroupement: currentGroupement.id } },
        { term: { sortie: 0 } },
        { term: { 'pharmacies.id': currentPharmacie.id } },
      ];

  const { query, skip, take } = BuildQuery({ type, optionalMust: must });

  const variables = { take, skip, type: [type], query };

  const listResult = useSearch_Custom_Content_Titulaire_PharmacieQuery({ client: graphql, variables });

  const [deleteTitulaires, deletingTitulaires] = useDeleteSoftTitulairesMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data.deleteSoftTitulaires) {
        displayNotification({ type: 'success', message: 'Titulaire supprimé avec succès' });
        setSelected([]);
        setOpenDeleteDialog(false);
        listResult.refetch();
      }
    },
    onError: () => {
      displayNotification({ type: 'error', message: 'Erreur lors de la suppression du titulaire' });
    },
  });

  const handleDeleteTitulaires = () => {
    if (selected && selected.length > 0) {
      deleteTitulaires({ variables: { ids: selected.map((titulaire: any) => titulaire.id) } });
    }
  };

  const handleOpenDeleteDialog = () => {
    setOpenDeleteDialog(true);
  };

  const columns = useColumns({ deleteMutation: deleteTitulaires, refetch: listResult.refetch });

  const listTitulairePharmacie = (
    <div className={classes.listTitulaireRoot}>
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher un titulaire" />
      </div>
      <CustomContent
        tableRowClassName={classes.tableCell}
        selected={selected}
        setSelected={setSelected}
        {...{ listResult, columns }}
        isSelectable={true}
      />
    </div>
  );

  const { stepper, loadingForm, mutationLoading } = useForm({ refetch: listResult.refetch });

  const subToolbar = useSubToolbar({
    url: TITULAIRE_PHARMACIE_URL,
    title: 'Liste des titulaires de pharmacie',
    selected,
    addBtnLabel: 'Ajouter un titulaire',
    onDelete: handleOpenDeleteDialog,
  });

  const confirmDeleteDialog = (
    <ConfirmDeleteDialog
      open={openDeleteDialog}
      setOpen={setOpenDeleteDialog}
      title="Supprimer la sélection"
      onClickConfirm={handleDeleteTitulaires}
      isLoading={deletingTitulaires.loading}
    />
  );

  useEffect(() => {
    return () => {
      resetSearchFilters();
    };
  }, []);

  if (onUserForm) {
    return <FormUser />;
  }

  if (onForm) {
    return (
      <>
        <Form
          className={classes.form}
          createTitle="Ajout d'un nouveau titulaire"
          editTitle="Modification d'un titulaire"
          stepper={stepper}
          loading={loadingForm}
          mutationLoading={mutationLoading}
        />
        <Divider />
      </>
    );
  }

  if (id) {
    return <DetailsTitulaire />;
  }
  return (
    <div>
      <Helmet>
        <title>Titulaires des pharmacies</title>
      </Helmet>
      {subToolbar}
      {listTitulairePharmacie}
      {confirmDeleteDialog}
    </div>
  );
};

export default TitulairePharmacies;
