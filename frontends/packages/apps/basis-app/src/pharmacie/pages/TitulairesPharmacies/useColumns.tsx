import { TITULAIRE_PHARMACIE_URL } from '@lib/common';
import { Column } from '@lib/common/src/components/newCustomContent/interfaces';
import { CustomAvatar } from '@lib/ui-kit';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import moment from 'moment';
import React from 'react';

interface UseColumnsInterface {
  deleteMutation?: any;
  refetch?: any;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    linkContainer: {
      '& > span:nth-last-child(1)': {
        display: 'none',
      },
    },
  })
);

const useColumns = (params: UseColumnsInterface): Column[] => {
  const { deleteMutation, refetch } = params;

  const classes = useStyles();

  return [
    {
      name: '',
      label: 'Photo',
      renderer: (value: any) => {
        const valueUser =
          value.users && value.pharmacies && value.pharmacies.length === 1 && value.users.length === 1
            ? value.users[0]
            : null;
        return (
          <CustomAvatar
            name={value && value.fullName}
            url={
              valueUser && valueUser.userPhoto && valueUser.userPhoto.fichier && valueUser.userPhoto.fichier.publicUrl
            }
          />
        );
      },
    },
    {
      name: 'civilite',
      label: 'Civilité',
    },
    {
      name: 'prenom',
      label: 'Prénom',
    },
    {
      name: 'nom',
      label: 'Nom',
    },
    {
      name: 'contact.adresse1',
      label: 'Adresse 1',
      renderer: (value: any) => {
        return value && value.contact && value.contact.adresse1 ? value.contact.adresse1 : '-';
      },
    },
    {
      name: 'contact.ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value && value.contact && value.contact.ville ? value.contact.ville : '-';
      },
    },
    {
      name: 'contact.cp',
      label: 'CP',
      renderer: (value: any) => {
        return value && value.contact && value.contact.cp ? value.contact.cp : '-';
      },
    },
    {
      name: 'contact.telProf',
      label: 'Téléphone 1',
      renderer: (value: any) => {
        return value && value.contact && value.contact.telProf ? value.contact.telProf : '-';
      },
    },
    {
      name: 'contact.telMobProf',
      label: 'Téléphone 2',
      renderer: (value: any) => {
        return value && value.contact && value.contact.telMobProf ? value.contact.telMobProf : '-';
      },
    },
    {
      name: 'contact.mailProf',
      label: 'Email',
      renderer: (value: any) => {
        return (value && value.contact && value.contact.mailProf) || '-';
      },
    },
    {
      name: 'president.region',
      label: 'Président de region',
      renderer: (value: any) => {
        return (value && value.estPresidentRegion) || '-';
      },
    },
    {
      name: 'contact.dateCreation',
      label: 'Date de création',
      renderer: (value: any) => {
        if (value && value.dateCreation) return moment(value.dateCreation).format('L');
        return '-';
      },
    },
    {
      name: 'contact.dateModification',
      label: 'Date de modification',
      renderer: (value: any) => {
        if (value && value.dateModification) return moment(value.dateModification).format('L');
        return '-';
      },
    },
    {
      name: '',
      label: '',
      tableActionColumn: {
        baseUrl: TITULAIRE_PHARMACIE_URL,
        deleteMutation,
        refetch,
        withViewDetails: true,
      },
    },
  ];
};

export default useColumns;
