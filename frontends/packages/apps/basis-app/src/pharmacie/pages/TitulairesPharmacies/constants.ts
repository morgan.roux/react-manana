import { FieldsOptions } from '@lib/common/src/components/newCustomContent/interfaces';

/**
 * Search fields
 *
 */
export const SEARCH_FIELDS_OPTIONS: FieldsOptions[] = [
  {
    name: 'pharmacies.nom',
    label: 'Nom Pharmacie',
    value: '',
    type: 'Search',
    placeholder: 'Contenant...',
    ordre: 1,
  },
  {
    name: 'pharmacies.cip',
    label: 'CIP',
    value: '',
    type: 'Search',
    placeholder: 'Commençant par...',
    filterType: 'StartsWith',
    ordre: 2,
  },

  {
    name: 'prenom',
    label: 'Prénom',
    value: '',
    type: 'Search',
    placeholder: 'Contenant...',
    ordre: 4,
  },
  {
    name: 'nom',
    label: 'Nom',
    value: '',
    type: 'Search',
    placeholder: 'Contenant...',
    ordre: 3,
  },
  {
    name: 'adresse1',
    label: 'Adresse',
    value: '',
    type: 'Search',
    placeholder: 'Contenant...',
    extraNames: ['adresse2'],
    ordre: 5,
  },
  {
    name: 'ville',
    label: 'Ville',
    value: '',
    type: 'Search',
    placeholder: 'Contenant...',
    ordre: 6,
  },
  {
    name: 'cp',
    label: 'Code Postal',
    value: '',
    type: 'Search',
    filterType: 'StartsWith',
    placeholder: 'Commençant par...',
    ordre: 7,
  },
  {
    name: 'tele1',
    label: 'Téléphone',
    value: '',
    type: 'Search',
    placeholder: 'Commençant par...',
    filterType: 'StartsWith',
    extraNames: ['tele2'],
    ordre: 8,
  },

  {
    name: 'fax',
    label: 'Fax',
    value: '',
    type: 'Search',
    placeholder: 'Commençant par...',
    filterType: 'StartsWith',
    ordre: 9,
  },
  {
    name: 'email',
    label: 'Email',
    value: '',
    type: 'Search',
    placeholder: 'Commençant par...',
    filterType: 'StartsWith',
    ordre: 10,
  },
];
