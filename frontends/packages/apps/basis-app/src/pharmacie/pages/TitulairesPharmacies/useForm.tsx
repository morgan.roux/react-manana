import { TITULAIRE_PHARMACIE_URL, useApplicationContext, useDisplayNotification } from '@lib/common';
import { useCreateUpdateTitulaireMutation, useTitulaireQuery } from '@lib/common/src/graphql';
import { CIVILITE_LIST } from '@lib/common/src/shared/user';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import { FormStepperInterface } from '../../../components/Form/Form';
import PharmacieSelectionTable from '../../../components/PharmacieSelectionTable';
const useForm = (props: any) => {
  const [values, setValues] = useState<any>({});

  const { refetch } = props;

  const { id }: any = useParams();

  const { push } = useHistory();

  const { graphql, isGroupePharmacie, currentPharmacie } = useApplicationContext();

  const [activeStep, setActiveStep] = useState<number>(0);

  const [selectedPharmacie, setSelectedPharmacie] = useState<any>(isGroupePharmacie ? [currentPharmacie] : []);

  const displayNotification = useDisplayNotification();

  const { civilite, prenom, nom, contact } = values;

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;

      setValues((prevState: any) => ({ ...prevState, [name]: value }));
      if (name != 'nom' && name != 'prenom' && name != 'civilite') {
        setValues((prevState: any) => ({
          ...prevState,
          contact: { ...prevState.contact, [name]: value },
        }));
      }
    }
  };

  const loadingTitulaire = useTitulaireQuery({
    client: graphql,
    variables: { id },
    skip: !id,
    onCompleted: (data) => {
      if (data) {
        setValues(data.titulaire);
        setSelectedPharmacie(data.titulaire?.pharmacies || []);
      }
    },
  });

  const stepOne = [
    {
      title: 'Informations personnelles',
      formControls: [
        {
          label: 'Civilité',
          select: {
            label: '',
            list: CIVILITE_LIST,
            listId: 'id',
            index: 'value',
            name: 'civilite',
            value: civilite,
            onChange: handleChange,
            shrink: false,
            placeholder: 'Choisissez une civilité',
            withPlaceholder: true,
          },
        },
        {
          label: 'Prénom',
          required: true,
          textfield: {
            name: 'prenom',
            value: prenom,
            onChange: handleChange,
            placeholder: 'Prénom de la personne',
          },
        },
        {
          label: 'Nom',
          required: true,
          textfield: {
            name: 'nom',
            value: nom,
            onChange: handleChange,
            placeholder: 'Nom de la personne',
          },
        },
      ],
    },
  ];

  const stepTwo = [
    {
      title: 'Internet',
      formControls: [
        {
          label: 'Mail',
          textfield: {
            name: 'mailProf',
            value: contact?.mailProf,
            onChange: handleChange,
            placeholder: 'Son mail',
          },
        },
        {
          label: 'Page web',
          textfield: {
            name: 'siteProf',
            value: contact?.siteProf,
            onChange: handleChange,
            placeholder: 'Son page web',
          },
        },
      ],
    },
    {
      title: 'Numéros de téléphone',
      formControls: [
        {
          label: 'Bureau',
          textfield: {
            name: 'telProf',
            value: contact?.telProf,
            onChange: handleChange,
            placeholder: 'Son num bureau',
          },
        },
        {
          label: 'Domicile',
          textfield: {
            name: 'telPerso',
            value: contact?.telPerso,
            onChange: handleChange,
            placeholder: 'Son num domicile',
          },
        },
        {
          label: 'Fax',
          textfield: {
            name: 'faxProf',
            value: contact?.faxProf,
            onChange: handleChange,
            placeholder: 'Son num fax',
          },
        },
        {
          label: 'Mobile',
          textfield: {
            name: 'telMobProf',
            value: contact?.telMobProf,
            onChange: handleChange,
            placeholder: 'Son num mobile',
          },
        },
      ],
    },
    {
      title: 'Coordonnées',
      formControls: [
        {
          label: 'Adresse 1',
          textfield: {
            name: 'telProf',
            value: contact?.adresse1,
            onChange: handleChange,
            placeholder: 'Son adresse 1',
          },
        },
        {
          label: 'Adresse 2',
          textfield: {
            name: 'adresse2',
            value: contact?.adresse2,
            onChange: handleChange,
            placeholder: 'Son adresse 2',
          },
        },
        {
          label: 'Ville',
          textfield: {
            name: 'ville',
            value: contact?.ville,
            onChange: handleChange,
            placeholder: 'Sa ville',
          },
        },
        {
          label: 'Code postal',
          textfield: {
            name: 'cp',
            value: contact?.cp,
            onChange: handleChange,
            placeholder: 'Son Code postal',
          },
        },
      ],
    },
  ];

  const disableNextButton =
    activeStep === 0 ? !civilite || !prenom || !nom : activeStep === 2 ? selectedPharmacie.length === 0 : false;

  const [upsertTitulaire, upsertingTitulaire] = useCreateUpdateTitulaireMutation({
    client: graphql,
    variables: { ...values, idPharmacies: selectedPharmacie.map((pharmacie: any) => pharmacie.id) },
    onCompleted: (data) => {
      if (data.createUpdateTitulaire) {
        displayNotification({ type: 'success', message: `${id ? 'Modification' : 'Ajout'} d'un titulaire réussi` });
        refetch && refetch();
        setValues({});
        setSelectedPharmacie([]);
        goToHome();
      }
    },
  });

  const finalStep = {
    buttonLabel: id ? 'Modifier' : 'Ajouter',
    loading: upsertingTitulaire.loading,
    action: () => {
      upsertTitulaire();
    },
  };

  const goToHome = () => {
    setValues({});
    setSelectedPharmacie([]);
    push(`/db/${TITULAIRE_PHARMACIE_URL}`);
  };

  const stepper: FormStepperInterface = {
    steps: isGroupePharmacie
      ? [
          {
            title: 'Informations personnelles',
            content: stepOne,
          },
          {
            title: 'Contact',
            content: stepTwo,
          },
        ]
      : [
          {
            title: 'Informations personnelles',
            content: stepOne,
          },
          {
            title: 'Contact',
            content: stepTwo,
          },
          {
            title: 'Choix des pharmacies',
            children: (
              <PharmacieSelectionTable
                isSelectable={true}
                selected={selectedPharmacie}
                setSelected={setSelectedPharmacie}
              />
            ),
          },
        ],
    goToHome,
    activeStep,
    setActiveStep,
    disableNextButton,
    finalStep,
  };
  return { stepper, loadingForm: loadingTitulaire.loading, mutationLoading: upsertingTitulaire.loading };
};

export default useForm;
