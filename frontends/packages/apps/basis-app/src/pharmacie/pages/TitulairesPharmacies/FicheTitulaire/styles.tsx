import { Theme, Badge } from '@material-ui/core';
import { createStyles, makeStyles, withStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      justifyContent: 'center',
      marginTop: '15px',
    },
    large: {
      width: theme.spacing(14),
      height: theme.spacing(14),
    },
    tableau: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
    },
    h5: {
      textAlign: 'right',
      font: 'Medium 16px/33px Montserrat',
      letterSpacing: 0,
      opacity: 1,
      color: '#1D1D1D',
    },
    element: {
      cursor: 'pointer',
      '&:hover': {
        color: '#F11957',
      },
    },
  })
);

export const StyledBadge = withStyles((theme) => ({
  badge: {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: '$ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    },
  },
}))(Badge);

// export default useStyles;
