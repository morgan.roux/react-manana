import { PHARMACIE_URL, useApplicationContext, useSubToolbar } from '@lib/common';
import { useDelete_Soft_PharmaciesMutation } from '@lib/common/src/graphql';
import CustomContent, { SearchInput } from '@lib/common/src/components/newCustomContent';
import { BuildQuery } from '@lib/common/src/components/withSearch/queryBuilder';
import { useSearch_Custom_Content_PharmacieQuery } from '@lib/common/src/graphql';
import { ConfirmDeleteDialog } from '@lib/ui-kit';
import React, { FC, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory, useLocation, useParams } from 'react-router';
import useColumns from './useColumns';
import useStyles from './styles';
import Form from '../../../components/Form';
import useForm, { initialState } from './useForm';

const Pharmacies: FC = () => {
  const type = 'pharmacie';
  const { pathname } = useLocation();
  const { push } = useHistory();
  const { id }: any = useParams();

  const classes = useStyles({});

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const onForm: boolean = pathname.includes('/create') || pathname.includes('/edit');

  const [selected, setSelected] = useState<any>([]);

  const { graphql, currentGroupement, notify } = useApplicationContext();

  const [deletePharmacies, deletingPharmacies] = useDelete_Soft_PharmaciesMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data.deleteSoftPharmacies) {
        notify({ type: 'success', message: 'Pharmacie supprimée avec succès' });
        setSelected([]);
        setOpenDeleteDialog(false);
        listResult.refetch();
      }
    },
    onError: () => {
      notify({ type: 'error', message: 'Erreur lors de la suppression de la pharmacie' });
    },
  });

  const must = [
    { term: { idGroupement: currentGroupement.id } },
    { term: { sortie: 0 } },
    /* { term: { 'titulaires.sortie': 0 } }, */
  ];

  const { query, skip, take } = BuildQuery({ type, optionalMust: must });

  const variables = { take, skip, type: [type], query };

  const listResult = useSearch_Custom_Content_PharmacieQuery({ client: graphql, variables });

  const columns = useColumns({ deleteMutation: deletePharmacies, refetch: listResult.refetch });

  const listPharmacie = (
    <div className={classes.listTitulaireRoot}>
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher une pharmacie" />
      </div>
      <CustomContent selected={selected} setSelected={setSelected} {...{ listResult, columns }} isSelectable={true} />
    </div>
  );

  const { upsertPharmacie, form, loadingForm, mutationLoading, setValues } = useForm({ refetch: listResult.refetch });

  const handleOpenDeleteDialog = () => {
    setOpenDeleteDialog(true);
  };

  const handleClickBack = () => {
    push(`/db/${PHARMACIE_URL}`);
    setValues(initialState);
  };

  const handleDeletePharmacies = () => {
    if (selected && selected.length > 0) {
      deletePharmacies({ variables: { ids: selected.map((titulaire: any) => titulaire.id) } });
    }
  };

  const subToolbar = useSubToolbar({
    url: PHARMACIE_URL,
    title: 'Liste des pharmacies',
    selected,
    addBtnLabel: 'Ajouter une pharmacie',
    onDelete: handleOpenDeleteDialog,
  });

  const confirmDeleteDialog = (
    <ConfirmDeleteDialog
      open={openDeleteDialog}
      setOpen={setOpenDeleteDialog}
      title="Supprimer la sélection"
      onClickConfirm={handleDeletePharmacies}
      isLoading={deletingPharmacies.loading}
    />
  );

  if (onForm) {
    return (
      <Form
        createTitle="Ajout d'une nouvelle pharmacie"
        editTitle="Modification d'une pharmacie"
        form={form}
        upsert={upsertPharmacie}
        upsertBtnLabel={id ? 'Modifier' : 'Ajouter'}
        loading={loadingForm}
        mutationLoading={mutationLoading}
        onClickBack={handleClickBack}
      />
    );
  }

  return (
    <div>
      <Helmet>
        <title>Pharmacies</title>
      </Helmet>
      {subToolbar}
      {listPharmacie}
      {confirmDeleteDialog}
    </div>
  );
};

export default Pharmacies;
