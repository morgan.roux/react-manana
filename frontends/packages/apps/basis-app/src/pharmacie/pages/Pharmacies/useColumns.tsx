import { PHARMACIE_URL } from '@lib/common';
import { createStyles, Link, makeStyles, Theme } from '@material-ui/core';
import React, { Fragment } from 'react';
import { useHistory } from 'react-router';

interface UseColumnsInterface {
  deleteMutation?: any;
  refetch?: any;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    linkContainer: {
      '& > span:nth-last-child(1)': {
        display: 'none',
      },
    },
  })
);

const useColumns = (params: UseColumnsInterface) => {
  const { refetch, deleteMutation } = params;

  const { push } = useHistory();

  const classes = useStyles();

  return [
    {
      name: 'cip',
      label: 'CIP',
      renderer: (value: any) => {
        return value.cip ? value.cip : '-';
      },
    },
    {
      name: 'nom',
      label: 'Nom',
      renderer: (value: any) => {
        return value.nom ? value.nom : '-';
      },
    },
    {
      name: 'titulaire.fullName',
      label: 'Titulaire(s)',
      renderer: (value: any) => {
        const handleClick = (titulaireId: string) => () => {
          push(`/db/titulaires-pharmacies/fiche/${titulaireId}`);
        };
        const titulaires: any[] = value.titulaires || [];
        return titulaires.map((t: any, index: number) => {
          const isMultiple: boolean = titulaires.length > 1 && index < titulaires.length - 1;
          if (t && t.sortie === 0) {
            return (
              <Fragment key={`titulaire_link_${index}`}>
                <Link
                  color="secondary"
                  component="button"
                  variant="body2"
                  onClick={handleClick(t.id)}
                  className={classes.linkContainer}
                >
                  {t && `${t.fullName}`}
                  {isMultiple && (
                    <>
                      ,<span style={{ visibility: 'hidden' }}>&nbsp;</span>
                    </>
                  )}
                </Link>
              </Fragment>
            );
          } else {
            return ' ';
          }
        });
      },
    },
    {
      name: 'adresse1',
      label: 'Adresse',
      renderer: (value: any) => {
        return value.adresse1 ? value.adresse1 : '-';
      },
    },
    {
      name: 'cp',
      label: 'Code postal',
      renderer: (value: any) => {
        return value.cp ? value.cp : '-';
      },
    },
    {
      name: 'departement.nom',
      label: 'Département',
      renderer: (value: any) => {
        return value.departement && value.departement.nom ? value.departement.nom : '-';
      },
    },
    {
      name: 'ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value.ville ? value.ville : '-';
      },
    },
    {
      name: 'tele1',
      label: 'Téléphone',
      renderer: (value: any) => {
        return value.contact && value.contact.telProf ? value.contact.telProf : '-';
      },
    },
    {
      name: 'numIvrylab',
      label: 'Code plateforme',
      renderer: (value: any) => {
        return value.numIvrylab ? value.numIvrylab : '-';
      },
    },
    {
      name: 'contrat',
      label: 'Contrat',
      renderer: (value: any) => {
        return value.contrat ? value.contrat : '-';
      },
    },
    {
      name: 'sortie',
      label: 'Sortie',
      renderer: (value: any) => {
        return value.sortie ? (value.sortie === 1 ? 'Oui' : 'Non') : '-';
      },
    },
    {
      name: 'actived',
      label: 'Statut',
      renderer: (value: any) => {
        return value && value.actived !== undefined && value.actived !== null
          ? value.actived
            ? 'Activée'
            : 'Inactive'
          : '-';
      },
    },
    {
      name: '',
      label: '',
      tableActionColumn: {
        baseUrl: PHARMACIE_URL,
        deleteMutation,
        refetch,
        withViewDetails: true,
      },
    },
  ];
};

export default useColumns;
