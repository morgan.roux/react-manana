import { PHARMACIE_URL, useApplicationContext, useDisplayNotification } from '@lib/common';
import { getGroupement } from '@lib/common/src/auth/auth-util';
import {
  ContactInput,
  PharmacieAchatInput,
  PharmacieCapInput,
  PharmacieComptaInput,
  PharmacieConceptInput,
  PharmacieDigitaleInput,
  PharmacieEntreeSortieInput,
  PharmacieInformatiqueInput,
  PharmacieSatisfactionInput,
  PharmacieSegmentationInput,
  PharmacieStatCaInput,
  useContratsQuery,
  useCreate_Update_PharmacieMutation,
  useLogiciels_With_Minim_InfoQuery,
  useMotifs_By_TypeQuery,
  usePharmacieQuery,
} from '@lib/common/src/graphql';
import { ChangeEvent, useState } from 'react';
import { useHistory, useParams } from 'react-router';

export interface PharmacieFormInterface {
  id: string | null;
  cip: string;
  nom: string;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  longitude: string | null;
  latitude: string | null;
  numFiness: string | null;
  sortie: number | null;
  sortieFuture: number | null;
  nbEmploye: number | null;
  nbAssistant: number | null;
  nbAutreTravailleur: number | null;
  uga: string | null;
  commentaire: string | null;
  idGrossistes: string[] | null;
  idGeneriqueurs: string[] | null;
  contact: ContactInput | null;
  entreeSortie: PharmacieEntreeSortieInput | null;
  satisfaction: PharmacieSatisfactionInput | null;
  segmentation: PharmacieSegmentationInput | null;
  compta: PharmacieComptaInput | null;
  informatique: PharmacieInformatiqueInput | null;
  achat: PharmacieAchatInput | null;
  cap: PharmacieCapInput | null;
  concept: PharmacieConceptInput | null;
  chiffreAffaire: PharmacieStatCaInput | null;
  digitales: PharmacieDigitaleInput[] | null;
}

interface UseFormParams {
  refetch: any;
}

export const initialState: PharmacieFormInterface = {
  id: null,
  cip: '',
  nom: '',
  adresse1: null,
  adresse2: null,
  cp: null,
  ville: null,
  pays: null,
  longitude: null,
  latitude: null,
  numFiness: null,
  sortie: null,
  sortieFuture: null,
  nbEmploye: null,
  nbAssistant: null,
  nbAutreTravailleur: null,
  uga: null,
  commentaire: null,
  idGrossistes: null,
  idGeneriqueurs: null,
  contact: null,
  entreeSortie: null,
  satisfaction: null,
  segmentation: null,
  compta: null,
  informatique: null,
  achat: null,
  cap: null,
  concept: null,
  chiffreAffaire: null,
  digitales: null,
};

const useForm = (params: UseFormParams) => {
  const { refetch } = params;
  const { graphql, isGroupePharmacie, currentPharmacie } = useApplicationContext();

  const [values, setValues] = useState<PharmacieFormInterface>(initialState);

  const { id }: any = useParams();

  const { push } = useHistory();

  const displayNotification = useDisplayNotification();

  const { entreeSortie, cip, nom, adresse1, cp, ville, informatique } = values;

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      const entreSortieFields = ['idMotifEntree', 'dateEntree', 'idContrat'];
      const informatiqueFields = ['idLogiciel'];

      if (entreSortieFields.includes(name)) {
        setValues((prevState) => ({
          ...prevState,
          entreeSortie: { ...prevState.entreeSortie, [name]: value },
        }));
      } else if (informatiqueFields.includes(name)) {
        setValues((prevState) => ({
          ...prevState,
          informatique: { ...prevState.informatique, [name]: value },
        }));
      } else {
        setValues((prevState: any) => ({ ...prevState, [name]: value }));
      }
    }
  };

  const loadingPharmacie = usePharmacieQuery({
    client: graphql,
    variables: { id },
    skip: !id,
    fetchPolicy: 'network-only',
    onCompleted: (data) => {
      if (data.pharmacie) {
        setValues({
          ...(data.pharmacie as any),
          entreeSortie: {
            idMotifEntree: data.pharmacie.entreeSortie?.pharmacieMotifEntree?.id,
            idContrat: data.pharmacie.entreeSortie?.contrat?.id,
          },
          informatique: { idLogiciel: data.pharmacie.informatique?.logiciel?.id },
        });
      }
    },
  });

  const loadingMotifs = useMotifs_By_TypeQuery({ client: graphql });

  const loadingContrats = useContratsQuery({ client: graphql });

  const loadingLogiciels = useLogiciels_With_Minim_InfoQuery({ client: graphql });

  const [upsertPharmacie, upsertingPharmacie] = useCreate_Update_PharmacieMutation({
    client: graphql,
    variables: {
      ...values,
      sortie: values.sortie ? 1 : 0,
      sortieFuture: values.sortieFuture ? 1 : 0,
    },
    onCompleted: (data) => {
      if (data.createUpdatePharmacie) {
        displayNotification({ type: 'success', message: `${id ? 'Modification' : 'Ajout'} d'une pharmacie réussi` });
        refetch && refetch();
        setValues(initialState);
        goToHome();
      }
    },
  });

  const form = [
    {
      title: 'Informations personnelles',
      formControls: [
        {
          label: "Motif d'entrée",
          select: {
            label: '',
            list: loadingMotifs.data?.motifs || [],
            listId: 'id',
            index: 'libelle',
            name: 'idMotifEntree',
            value: entreeSortie?.idMotifEntree,
            onChange: handleChange,
            shrink: false,
            placeholder: 'Choisissez le motif',
            withPlaceholder: true,
          },
        },
        {
          label: 'CIP',
          required: true,
          textfield: {
            name: 'cip',
            value: cip,
            onChange: handleChange,
            placeholder: 'Code CIP de la pharmacie',
          },
        },
        {
          label: 'Nom pharmacie',
          required: true,
          textfield: {
            name: 'nom',
            value: nom,
            onChange: handleChange,
            placeholder: 'Nom pharmacie',
          },
        },
        {
          label: "Date d'entrée",
          datepicker: {
            name: 'dateEntree',
            value: entreeSortie?.dateEntree,
            onChange: (date: any, value: any) =>
              handleChange({ target: { value: date, name: 'dateEntree' } } as ChangeEvent<any>),
            placeholder: '00/00/0000',
          },
        },
        {
          label: 'Contrat',
          select: {
            label: '',
            list: loadingContrats.data?.contrats || [],
            listId: 'id',
            index: 'nom',
            name: 'idContrat',
            value: entreeSortie?.idContrat,
            onChange: handleChange,
            shrink: false,
            placeholder: 'Type de contrat',
            withPlaceholder: true,
          },
        },
        {
          label: 'LGO',
          select: {
            label: '',
            list: loadingLogiciels.data?.logiciels || [],
            listId: 'id',
            index: 'nom',
            name: 'idLogiciel',
            value: informatique?.idLogiciel,
            onChange: handleChange,
            shrink: false,
            placeholder: 'Votre LGO',
            withPlaceholder: true,
          },
        },
      ],
    },
    {
      title: 'Coordonnées',
      formControls: [
        {
          label: 'Adresse',
          textfield: {
            name: 'adresse1',
            value: adresse1,
            onChange: handleChange,
            placeholder: 'Son adresse',
          },
        },
        {
          label: 'Code Postale',
          textfield: {
            name: 'cp',
            value: cp,
            onChange: handleChange,
            placeholder: 'Code postal de la pharmacie',
          },
        },
        {
          label: 'Ville',
          textfield: {
            name: 'ville',
            value: ville,
            onChange: handleChange,
            placeholder: 'Ville de la pharmacie',
          },
        },
      ],
    },
  ];

  const goToHome = () => {
    setValues(initialState);
    push(`/db/${PHARMACIE_URL}`);
  };

  return {
    loadingForm:
      loadingPharmacie.loading || loadingMotifs.loading || loadingLogiciels.loading || loadingContrats.loading,
    mutationLoading: upsertingPharmacie.loading,
    upsertPharmacie,
    form,
    setValues,
  };
};

export default useForm;
