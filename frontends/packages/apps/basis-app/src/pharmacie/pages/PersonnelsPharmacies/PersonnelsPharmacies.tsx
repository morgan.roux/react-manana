import {
  PERSONNEL_PHARMACIE_URL,
  useApplicationContext,
  useDisplayNotification,
  useFilters,
  useSubToolbar,
} from '@lib/common';
import CustomContent, { SearchInput } from '@lib/common/src/components/newCustomContent';
import { BuildQuery } from '@lib/common/src/components/withSearch/queryBuilder';
import { useDelete_Soft_PpersonnelsMutation, useSearch_Custom_Content_PpharmacieQuery } from '@lib/common/src/graphql';
import React, { FC, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory, useLocation, useParams } from 'react-router';
import Form from '../../../components/Form';
import FormUser from '../../..//components/FormUser';
import useColumns from './useColumns';
import useStyles from './styles';
import useForm from './useForm';
import { ConfirmDeleteDialog } from '@lib/ui-kit';

const PersonnelsPharmacies: FC = () => {
  const type = 'ppersonnel';

  const { id }: any = useParams();

  const { pathname } = useLocation();

  const { push } = useHistory();

  const { resetSearchFilters } = useFilters();

  const classes = useStyles({});

  const [selected, setSelected] = useState<any>([]);

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const { graphql, user, currentGroupement, pharmacies } = useApplicationContext();

  const displayNotification = useDisplayNotification();

  const onForm: boolean = pathname.includes('/create') || pathname.includes('/edit');

  const onUserForm: boolean = pathname.includes('/user');

  const must: any[] = [{ term: { idGroupement: currentGroupement.id } }, { term: { sortie: 0 } }];

  if (user.role?.groupe === 'PHARMACIE') {
    pharmacies.map((pharmacie) =>
      must.push({
        term: { 'pharmacie.id': pharmacie.id },
      })
    );
  }

  const { query, skip, take } = BuildQuery({ type, optionalMust: must });

  const variables = { take, skip, type: [type], query };

  const listResult = useSearch_Custom_Content_PpharmacieQuery({ client: graphql, variables });

  const [deletePersonnels, deletingPersonnels] = useDelete_Soft_PpersonnelsMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data.deleteSoftPpersonnels) {
        displayNotification({ type: 'success', message: 'Personnel supprimé avec succès' });
        setSelected([]);
        setOpenDeleteDialog(false);
        listResult.refetch();
      }
    },
    onError: () => {
      displayNotification({ type: 'error', message: 'Erreur lors de la suppression du personnel' });
    },
  });

  const columns = useColumns({ deleteMutation: deletePersonnels, refetch: listResult.refetch });

  const handleDeletePersonnels = () => {
    if (selected && selected.length > 0) {
      deletePersonnels({ variables: { ids: selected.map((personnel: any) => personnel.id) } });
    }
  };

  const handleOpenDeleteDialog = () => {
    setOpenDeleteDialog(true);
  };

  const confirmDeleteDialog = (
    <ConfirmDeleteDialog
      open={openDeleteDialog}
      setOpen={setOpenDeleteDialog}
      title="Supprimer la sélection"
      onClickConfirm={handleDeletePersonnels}
      isLoading={deletingPersonnels.loading}
    />
  );

  const listPersonnelsPharmacies = (
    <div className={classes.listTitulaireRoot}>
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher un personnel des pharmacies" />
      </div>
      <CustomContent
        tableRowClassName={classes.tableCell}
        selected={selected}
        setSelected={setSelected}
        {...{ listResult, columns }}
        isSelectable={true}
      />
    </div>
  );

  const { upsertPersonnel, form, loadingForm, mutationLoading, resetValues } = useForm({ refetch: listResult.refetch });

  const handleClickBack = () => {
    push(`/db/${PERSONNEL_PHARMACIE_URL}`);
    resetValues;
  };

  const subToolbar = useSubToolbar({
    url: PERSONNEL_PHARMACIE_URL,
    title: 'Liste des personnels de la pharmacie',
    selected,
    addBtnLabel: 'Ajouter un personnel',
    onDelete: handleOpenDeleteDialog,
  });

  useEffect(() => {
    return () => {
      resetSearchFilters();
    };
  }, []);

  if (onUserForm) {
    return <FormUser />;
  }

  if (onForm) {
    return (
      <Form
        createTitle="Ajout d'un nouveau personnel"
        editTitle="Modification d'un personnel"
        form={form}
        upsert={upsertPersonnel}
        upsertBtnLabel={id ? 'Modifier' : 'Ajouter'}
        loading={loadingForm}
        mutationLoading={mutationLoading}
        onClickBack={handleClickBack}
      />
    );
  }

  // if (id) {
  //   return <DetailsTitulaire />;
  // }
  return (
    <div>
      <Helmet>
        <title>Personnel des pharmacies</title>
      </Helmet>
      {subToolbar}
      {listPersonnelsPharmacies}
      {confirmDeleteDialog}
    </div>
  );
};

export default PersonnelsPharmacies;
