import { PERSONNEL_PHARMACIE_URL, PHARMACIE_URL, TITULAIRE_PHARMACIE_URL } from '@lib/common';
import { CustomAvatar } from '@lib/ui-kit';
import { createStyles, Link, makeStyles, Theme } from '@material-ui/core';
import React, { Fragment } from 'react';
import { useHistory, useLocation } from 'react-router';
import UserStatus from '../../../components/Content/UserStatus';

interface UseColumnsInterface {
  deleteMutation?: any;
  refetch?: any;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    linkContainer: {
      '& > span:nth-last-child(1)': {
        display: 'none',
      },
    },
  })
);

const useColumns = (params: UseColumnsInterface) => {
  const { refetch, deleteMutation } = params;

  const classes = useStyles();

  const { pathname } = useLocation();

  const { push } = useHistory();

  return [
    {
      name: '',
      label: 'Photo',
      renderer: (value: any) => {
        return (
          <CustomAvatar
            name={value && value.fullName}
            url={
              value &&
              value.user &&
              value.user.userPhoto &&
              value.user.userPhoto.fichier &&
              value.user.userPhoto.fichier.publicUrl
            }
          />
        );
      },
    },
    { name: 'civilite', label: 'Civilité' },
    { name: 'fullName', label: 'Nom' },
    {
      name: 'user.anneeNaissance',
      label: 'Année de naissance',
      renderer: (value: any) => (value && value.user && value.user.anneeNaissance) || '-',
    },
    {
      name: 'pharmacie.nom',
      label: 'Pharmacie',
      renderer: (value: any) => {
        const goToPharmacie = () => {
          push(`/db/${PHARMACIE_URL}/fiche/${value.pharmacie.id}`);
        };
        return (
          (value.pharmacie && (
            <Link
              color="secondary"
              variant="body2"
              onClick={goToPharmacie}
              style={{ textAlign: 'left', cursor: 'pointer' }}
            >
              {value.pharmacie.nom}
            </Link>
          )) ||
          '-'
        );
      },
    },
    {
      name: '',
      label: 'Titulaire',
      renderer: (value: any) => {
        const goToTitulaire = (id: string) => () => {
          push(`/db/${TITULAIRE_PHARMACIE_URL}/fiche/${id}`);
        };
        const titulaires: any[] = (value.pharmacie && value.pharmacie.titulaires) || [];
        return titulaires.map((t: any, index: number) => {
          const isMultiple: boolean = titulaires.length > 1 && index < titulaires.length - 1;
          if (t) {
            return (
              <Fragment key={`titulaire_link_${index}`}>
                <Link
                  color="secondary"
                  component="button"
                  variant="body2"
                  onClick={goToTitulaire(t.id)}
                  style={{ textAlign: 'left', cursor: 'pointer' }}
                >
                  {t && `${t.fullName}`}
                  {isMultiple && (
                    <>
                      ,<span style={{ visibility: 'hidden' }}>&nbsp;</span>
                    </>
                  )}
                </Link>
              </Fragment>
            );
          } else {
            return '-';
          }
        });
      },
    },
    {
      name: 'estAmbassadrice',
      label: 'Rôle',
      renderer: (value: any) => {
        return value.user?.role?.nom || '-';
      },
    },
    {
      name: '',
      label: 'Statut',
      renderer: (value: any) => {
        return <UserStatus user={value.user} pathname={pathname} />;
      },
    },
    {
      name: '',
      label: '',
      tableActionColumn: {
        baseUrl: PERSONNEL_PHARMACIE_URL,
        deleteMutation,
        refetch,
        withViewDetails: true,
      },
    },
  ];
};

export default useColumns;
