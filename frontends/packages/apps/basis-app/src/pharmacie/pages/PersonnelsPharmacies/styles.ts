import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listTitulaireRoot: {
      overflowY: 'auto',
      maxHeight: 'calc(100vh - 156px)',
      '& .MuiTableRow-head': {
        '& > th:nth-child(6), & > th:nth-child(7)': {
          minWidth: 116,
        },
        '& > th:nth-child(10), & > th:nth-child(11)': {
          minWidth: 128,
        },
      },
    },
    searchInputBox: {
      padding: '20px 0px 0px 20px',
      display: 'flex',
    },
    tableCell: {
      '& .main-MuiTableCell-root': {
        padding: '6px 12px 6px 12px !important',
      },
    },
  })
);

export default useStyles;
