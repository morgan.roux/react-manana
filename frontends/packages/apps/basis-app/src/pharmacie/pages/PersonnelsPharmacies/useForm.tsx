import { PERSONNEL_PHARMACIE_URL, useApplicationContext, useDisplayNotification } from '@lib/common';
import { getGroupement } from '@lib/common/src/auth/auth-util';
import {
  useCreate_Update_PpersonnelMutation,
  usePersonnel_PharmacyQuery,
  useSearch_All_PharmaciesLazyQuery,
} from '@lib/common/src/graphql';
import { CIVILITE_LIST } from '@lib/common/src/shared/user';
import { debounce } from 'lodash';
import { ChangeEvent, useEffect, useRef, useState } from 'react';
import { useHistory, useParams } from 'react-router';

interface UseFormParams {
  refetch: any;
}
const useForm = (params: UseFormParams) => {
  const { refetch } = params;
  const { graphql, isGroupePharmacie, currentPharmacie, currentGroupement } = useApplicationContext();

  const initialValues = {
    pharmacie: isGroupePharmacie ? currentPharmacie : undefined,
    idPharmacie: isGroupePharmacie ? currentPharmacie.id : undefined,
  };

  const [values, setValues] = useState<any>(initialValues);

  const { id }: any = useParams();

  const { push } = useHistory();

  const groupement = getGroupement();

  const displayNotification = useDisplayNotification();

  const { civilite, prenom, nom, telBureau, telMobile, mail, commentaire, pharmacie, idPharmacie } = values;

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;

      setValues((prevState: any) => ({ ...prevState, [name]: value }));
      if (name != 'nom' && name != 'prenom' && name != 'civilite') {
        setValues((prevState: any) => ({
          ...prevState,
          contact: { ...prevState.contact, [name]: value },
        }));
      }
    }
  };

  useEffect(() => {
    loadPharmacies({ variables: { type: ['pharmacie'], take: 10, skip: 0 } });
  }, []);

  const loadingPpersonnel = usePersonnel_PharmacyQuery({
    client: graphql,
    variables: { id },
    skip: !id,
    fetchPolicy: 'network-only',
    onCompleted: (data) => {
      if (data) {
        setValues(data.ppersonnel);
      }
    },
  });

  const [loadPharmacies, loadingPharmacies] = useSearch_All_PharmaciesLazyQuery({
    client: graphql,
  });

  const [upsertPersonnel, upsertingPersonnel] = useCreate_Update_PpersonnelMutation({
    client: graphql,
    variables: {
      input: {
        id,
        civilite,
        prenom,
        nom,
        telBureau,
        telMobile,
        mail,
        commentaire,
        idPharmacie,
        idGroupement: groupement.id,
      },
    },
    onCompleted: (data) => {
      if (data.createUpdatePpersonnel) {
        displayNotification({ type: 'success', message: `${id ? 'Modification' : 'Ajout'} d'un personnel réussi` });
        refetch && refetch();
        goToHome();
      }
    },
  });

  const handleSearchPharmacies = (searchText: string) => {
    setValues((prev: any) => ({ ...prev, pharmacie: { id: '', nom: searchText } }));
  };

  const debouncedPharmacie = useRef(
    debounce((value: string) => {
      const formated = value.replace(/\s+/g, '\\ ').replace('/', '\\/');
      loadPharmacies({
        variables: {
          type: ['pharmacie'],
          query: {
            query: {
              bool: {
                must: [
                  { term: { idGroupement: currentGroupement.id } },
                  {
                    query_string: {
                      query: formated.startsWith('*') || formated.endsWith('*') ? formated : `*${formated}*`,
                      analyze_wildcard: true,
                    },
                  },
                ],
              },
            },
          },
        },
      });
    }, 1500)
  );

  useEffect(() => {
    values.pharmacie?.nom && debouncedPharmacie.current(values.pharmacie?.nom);
  }, [values.pharmacie]);

  const handlePharmacieAutocompleChange = (pharmacie: any) => {
    setValues((prevState: any) => ({ ...prevState, pharmacie, idPharmacie: pharmacie.id }));
  };

  const resetValues = () => {
    setValues(initialValues);
  };

  const form = [
    {
      formControls: [
        {
          label: 'Pharmacie',
          autocomplete: {
            value: pharmacie,
            inputValue: pharmacie?.nom || '',
            options: loadingPharmacies.data?.search?.data || [],
            optionLabelKey: 'nom',
            search: handleSearchPharmacies,
            onAutocompleteChange: handlePharmacieAutocompleChange,
            placeholder: 'Choisissez une pharmacie',
            loading: loadingPharmacies.loading,
            disabled: isGroupePharmacie,
          },
        },
      ],
    },
    {
      title: 'Informations personnelles',
      formControls: [
        {
          label: 'Civilité',
          select: {
            label: '',
            list: CIVILITE_LIST,
            listId: 'id',
            index: 'value',
            name: 'civilite',
            value: civilite,
            onChange: handleChange,
            shrink: false,
            placeholder: 'Choisissez une civilité',
            withPlaceholder: true,
          },
        },
        {
          label: 'Prénom',
          required: true,
          textfield: {
            name: 'prenom',
            value: prenom,
            onChange: handleChange,
            placeholder: 'Prénom de la personne',
          },
        },
        {
          label: 'Nom',
          required: true,
          textfield: {
            name: 'nom',
            value: nom,
            onChange: handleChange,
            placeholder: 'Nom de la personne',
          },
        },
        {
          label: 'Commentaire',
          textfield: {
            name: 'commentaire',
            value: commentaire,
            onChange: handleChange,
            placeholder: 'Commentaire',
          },
        },
      ],
    },
    {
      title: 'Contact',
      formControls: [
        {
          label: 'Mail',
          textfield: {
            name: 'mail',
            value: mail,
            onChange: handleChange,
            placeholder: 'Son mail',
          },
        },
      ],
    },
    {
      title: 'Numéros de téléphone',
      formControls: [
        {
          label: 'Bureau',
          textfield: {
            name: 'telBureau',
            value: telBureau,
            onChange: handleChange,
            placeholder: 'Son num bureau',
          },
        },
        {
          label: 'Mobile',
          textfield: {
            name: 'telMobile',
            value: telMobile,
            onChange: handleChange,
            placeholder: 'Son num mobile',
          },
        },
      ],
    },
  ];

  const goToHome = () => {
    resetValues();
    push(`/db/${PERSONNEL_PHARMACIE_URL}`);
  };

  return {
    loadingForm: loadingPpersonnel.loading,
    mutationLoading: upsertingPersonnel.loading,
    upsertPersonnel,
    form,
    setValues,
    resetValues,
  };
};

export default useForm;
