import React from 'react';
import loadable from '@loadable/component';
import PresidentIcon from '@material-ui/icons/PersonPin';

import { ModuleDefinition } from '@lib/common';

const presidentRegionModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_PRESIDENTS_REGIONS',
      location: 'PARAMETRE',
      name: 'Présidents des régions',
      to: '/presidents-regions',
      icon: <PresidentIcon />,
      preferredOrder: 250,
      authorize: {
        roles: ['SUPADM', 'GRPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
};

export default presidentRegionModule;
