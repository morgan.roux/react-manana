import { useValueParameterAsBoolean } from '@lib/common';
import { Grid, ListItem } from '@material-ui/core';
import GetAppIcon from '@material-ui/icons/GetApp';
import ListIcon from '@material-ui/icons/List';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import PublishIcon from '@material-ui/icons/Publish';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import MenuItemComponent from './MenuItem';
import useStyles from './style';
import UtilitaireRouter from './UtilitaireRouter';

export interface IMenuItem {
  path: string;
  label: string;
  show: boolean;
  icon?: JSX.Element;
}

const Utilitaire: FC<RouteComponentProps> = ({ history, location: { pathname } }) => {
  const classes = useStyles({});
  // const [menuItems, setMenuItems] = useState<IMenuItem[]>([]);

  const handlePush = (link: string) => {
    history.push(link);
  };

  const helloidParam = useValueParameterAsBoolean('0046');
  const activeDirectoryParam = useValueParameterAsBoolean('0049');
  // const ftpParam = useValueParameterAsBoolean('0050');
  // const applicationsParam = useValueParameterAsBoolean('0051');

  const items: IMenuItem[] = [
    {
      path: '/db/utilitaire/indexation',
      label: 'Indexation',
      show: true,
      icon: <ListIcon className={classes.icon} />,
    },
    {
      path: '/db/utilitaire/scan-email',
      label: 'Facturation : Scan email',
      show: false,
      icon: <PictureAsPdfIcon className={classes.icon} />,
    },
    {
      path: '/db/utilitaire/maj-helloid',
      label: 'Mise à jour HelloID',
      show: helloidParam,
      icon: <PublishIcon className={classes.icon} />,
    },
    {
      path: '/db/utilitaire/maj-active-directory',
      label: 'Mise à jour AD',
      show: activeDirectoryParam,
      icon: <GetAppIcon className={classes.icon} />,
    },
  ];

  const menuItems = items.filter((item) => item.show);

  return (
    <Grid container={true} className={classes.root}>
      <Grid item={true} xs={3} className={classes.leftOperation}>
        {menuItems &&
          menuItems.map((item, index) => {
            const pathName = pathname.split('/')[3];
            const itemPath = item.path.split('/')[3];
            const result: boolean =
              (pathName && itemPath && pathName === itemPath) === true || (index === 0 && !pathName);
            return (
              <ListItem button={true} onClick={(e) => handlePush(item.path)} key={`item-${index}`}>
                <MenuItemComponent icon={item.icon} label={item.label} secondIcon={null} active={result} />
              </ListItem>
            );
          })}
      </Grid>
      <Grid item={true} xs={9}>
        <UtilitaireRouter />
      </Grid>
    </Grid>
  );
};

export default withRouter(Utilitaire);
