import { useApplicationContext } from '@lib/common';
import {
  useDeploy_GroupementMutation,
  useGet_Get_Document_From_EmailLazyQuery,
  usePublish_Produit_ReferencesLazyQuery,
  useUpdate_Item_Scoring_For_All_UsersLazyQuery,
} from '@lib/common/src/federation';
import { useRefresh_IndexesLazyQuery, useReindexLazyQuery } from '@lib/common/src/graphql';
import { Backdrop, CustomSelect } from '@lib/ui-kit';
import { Button, FormControlLabel, Radio, RadioGroup } from '@material-ui/core';
import React, { ChangeEvent, FC, useState } from 'react';
import { withRouter } from 'react-router-dom';
import useStyles from './styles';

const Indexation: FC = () => {
  const classes = useStyles({});
  const [open, setOpen] = React.useState(true);
  const {
    currentGroupement: groupement,
    currentPharmacie: pharmacie,
    notify,
    federation,
    graphql,
  } = useApplicationContext();

  const [checkedRadio, setCheckedRadio] = useState<string>('all');
  const [routingKey, setRoutinkey] = useState<string>('');
  const [loadingMessage, setLoadingMessage] = useState<string>('Indexation en cours, veuillez patienter..."');

  const [getFromEmail, { loading }] = useGet_Get_Document_From_EmailLazyQuery({
    client: federation,
  });

  const [deployGroupemnet, deployingGroupement] = useDeploy_GroupementMutation({ client: federation });

  const [updateItemScoring, updatingItemScoring] = useUpdate_Item_Scoring_For_All_UsersLazyQuery({
    client: federation,
  });

  const [publishProduitReferences, publishingProduitReferences] = usePublish_Produit_ReferencesLazyQuery({
    client: federation,
    onCompleted: (data) => {
      notify({ type: 'success', message: 'Mise à jour terminée' });
    },
  });

  const [refreshIndexes, refreshIndexesResult] = useRefresh_IndexesLazyQuery({
    client: graphql,
    variables: {
      withMapping: false,
    },
    errorPolicy: 'all',
    onCompleted: (data) => {
      notify({ type: 'success', message: 'Réindexation terminée' });
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  const [reindex, reindexResult] = useReindexLazyQuery({
    client: graphql,
    onCompleted: (data) => {
      const routingKeyFromVariables = reindexResult.variables?.routingKey;
      if (routingKey === routingKeyFromVariables) notify({ type: 'success', message: 'Réindexation terminée' });
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  // if (refreshIndexesResult.loading || reindexResult.loading) {
  //   const snackBarData: SnackVariableInterface = {
  //     type: 'INFO',
  //     message: 'Refresh index en cours...',
  //     isOpen: true,
  //   };
  //   displaySnackBar(client, snackBarData);
  // }

  const handleChange = (event: any) => {
    event.preventDefault();
    event.stopPropagation();
    setCheckedRadio(event.target.value);
  };

  const onChange = (event: any) => {
    event.preventDefault();
    event.stopPropagation();
    setRoutinkey(event.target.value);
  };

  const handleSubmit = (event: ChangeEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();

    if (checkedRadio === 'all') {
      setLoadingMessage('Indexation en cours, cela prendra environ 5 à 15 minutes, veuillez patienter...');
      refreshIndexes();
    } else if (routingKey !== '') {
      reindex({ variables: { routingKey } });
    }
  };

  const list = [
    { name: 'Laboratoire', value: 'laboratoires' },
    { name: 'Partenaire', value: 'partenaires' },
    { name: 'Personnel du groupement', value: 'personnels' },
    { name: 'Personnel des pharmacies', value: 'ppersonnels' },
    { name: 'Titulaire', value: 'titulaires' },
    { name: 'Pharmacie', value: 'pharmacies' },
    { name: 'Produit', value: 'produits' },
    { name: 'Produit canal', value: 'produitcanals' },
    { name: 'Marché', value: 'marches' },
    { name: 'Promotion', value: 'promotions' },
    { name: 'Opération', value: 'operations' },
    { name: 'Actualité', value: 'actualites' },
    { name: 'Commande', value: 'commandes' },
  ];
  return (
    <>
      {(refreshIndexesResult.loading || reindexResult.loading) && <Backdrop value={loadingMessage} />}
      <div className={classes.root}>
        <RadioGroup row={true} value={checkedRadio} onChange={handleChange}>
          <FormControlLabel value="all" control={<Radio />} label="Reindexer tous" />
          <FormControlLabel value="one" control={<Radio />} label="Sélectionner les éléments à réindexer" />
        </RadioGroup>

        <div className={classes.input}>
          <CustomSelect
            label="Données à réindexer"
            list={list}
            index="name"
            listId="value"
            name=""
            value={routingKey}
            onChange={(e: any) => onChange(e)}
            disabled={checkedRadio === 'one' ? false : true}
          />
        </div>

        <Button
          className={classes.btn}
          variant="contained"
          color="secondary"
          onClick={handleSubmit}
          disabled={refreshIndexesResult.loading || reindexResult.loading ? true : false}
        >
          Réindexer
        </Button>

        <Button
          onClick={() => getFromEmail()}
          disabled={loading}
          variant="contained"
          color="secondary"
          style={{
            marginTop: 20,
          }}
        >
          Facture : Scan email
        </Button>

        <Button
          onClick={() => updateItemScoring()}
          disabled={updatingItemScoring.loading}
          variant="contained"
          color="secondary"
          style={{
            marginTop: 20,
          }}
        >
          Mettre à jour les scores
        </Button>
        <Button
          onClick={() => publishProduitReferences()}
          disabled={publishingProduitReferences.loading}
          variant="contained"
          color="secondary"
          style={{
            marginTop: 20,
          }}
        >
          Mettre à jour tous les produits référencés
        </Button>
        <Button
          onClick={() => publishProduitReferences({ variables: { idPharmacie: pharmacie.id } })}
          disabled={publishingProduitReferences.loading}
          variant="contained"
          color="secondary"
          style={{
            marginTop: 20,
          }}
        >
          Mettre à jour les produits référencés de la pharmacie
        </Button>

        <Button
          onClick={() => deployGroupemnet({ variables: { idGroupement: groupement.id } })}
          disabled={deployingGroupement.loading}
          variant="contained"
          color="secondary"
          style={{
            marginTop: 20,
          }}
        >
          Deployer groupement
        </Button>
      </div>
    </>
  );
};
export default withRouter(Indexation);
