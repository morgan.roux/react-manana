import { useApplicationContext } from '@lib/common';
import { Backdrop } from '@lib/ui-kit';
import { Button } from '@material-ui/core';
import axios from 'axios';
import React, { ChangeEvent, FC, useState } from 'react';
import useStyles from './styles';

const MajHelloId: FC = () => {
  const classes = useStyles({});
  const { config, currentGroupement: groupement, notify } = useApplicationContext();

  const [loadingUpdate, setLoadingUpdate] = useState<boolean>(false);

  const crud = () => {
    const groupementInfo = {
      groupementId: (groupement && groupement.id) || '',
    };
    setLoadingUpdate(true);
    axios
      .post(`${config.APP_SSO_URL}/create-update-allusers-helloid`, groupementInfo)
      .then(() => {
        setLoadingUpdate(false);
        notify({
          type: 'success',
          message: `Mise à jour terminéé.`,
        });
      })
      .catch(() => {
        setLoadingUpdate(false);
        notify({ type: 'error', message: `Il y a une erreur dans la mise à jour.` });
      });
  };

  const handleSubmit = (event: ChangeEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    crud();
  };

  return (
    <>
      {loadingUpdate && <Backdrop value="Mis à jour d'helloID en cours, veuillez patienter..." />}
      <div className={classes.root}>
        <h3>METTRE A JOUR HELLOID</h3>

        <Button className={classes.btn} variant="contained" color="secondary" onClick={handleSubmit} disabled={false}>
          Mettre à jour
        </Button>
      </div>
    </>
  );
};
export default MajHelloId;
