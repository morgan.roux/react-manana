import { useApplicationContext } from '@lib/common';
import { useGet_Get_Document_From_EmailLazyQuery } from '@lib/common/src/federation';
import { Backdrop } from '@lib/ui-kit';
import { Button } from '@material-ui/core';
import React, { FC } from 'react';
import useStyles from './styles';

const ScanEmail: FC = () => {
  const classes = useStyles({});

  const { federation } = useApplicationContext();

  const [getFromEmail, { loading }] = useGet_Get_Document_From_EmailLazyQuery({ client: federation });

  return (
    <>
      {loading && <Backdrop value="veuillez patienter..." />}
      <div className={classes.root}>
        <h3>Scan email</h3>

        <Button
          className={classes.btn}
          variant="contained"
          color="secondary"
          onClick={() => getFromEmail()}
          disabled={false}
        >
          Scan
        </Button>
      </div>
    </>
  );
};
export default ScanEmail;
