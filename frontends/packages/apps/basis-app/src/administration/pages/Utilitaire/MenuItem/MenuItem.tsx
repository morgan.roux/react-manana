import React, { FC } from 'react';
import { RouteComponentProps, withRouter, Link } from 'react-router-dom';
import { ListItemIcon, ListItemText } from '@material-ui/core';
import useStyles from './styles';
import classnames from 'classnames';

interface MenuListProps {
  active: boolean;
  icon?: any;
  label: string;
  secondIcon: any;
}

const MenuItemComponent: FC<MenuListProps & RouteComponentProps> = ({
  icon,
  secondIcon,
  label,
  active,
}) => {
  const classes = useStyles({});
  return (
    <>
      {icon && (
        <ListItemIcon className={classnames((active && classes.active) || classes.icon)}>
          {icon}
        </ListItemIcon>
      )}
      <ListItemText
        primary={<p className={classnames(classes.itemLabel, active && classes.active)}>{label}</p>}
      />
      {secondIcon}
    </>
  );
};
export default withRouter(MenuItemComponent);
