import { useApplicationContext } from '@lib/common';
import { Backdrop } from '@lib/ui-kit';
import { Button } from '@material-ui/core';
import axios from 'axios';
import React, { ChangeEvent, FC, useState } from 'react';
import useStyles from './styles';

const MajActiveDirectory: FC = () => {
  const classes = useStyles({});
  const { config, currentGroupement: groupement, notify } = useApplicationContext();

  const [loadingUpdate, setLoadingUpdate] = useState<boolean>(false);

  const crud = () => {
    setLoadingUpdate(true);
    axios
      .post(`${config.APP_SSO_URL}/ldap-create-users`, {
        groupementId: (groupement && groupement.id) || '',
      })
      .then(() => {
        setLoadingUpdate(false);
        notify({ type: 'success', message: `Mise à jour terminéé.` });
      })
      .catch(() => {
        setLoadingUpdate(false);
        notify({ type: 'error', message: `Il y a une erreur dans la mise à jour.` });
      });
  };

  const handleSubmit = (event: ChangeEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    crud();
  };

  return (
    <>
      {loadingUpdate && <Backdrop value="Mis à jour via Active Directory, veuillez patienter..." />}
      <div className={classes.root}>
        <h3>METTRE A JOUR VIA ACTIVE DIRECTORY</h3>

        <Button className={classes.btn} variant="contained" color="secondary" onClick={handleSubmit} disabled={false}>
          Mettre à jour
        </Button>
      </div>
    </>
  );
};
export default MajActiveDirectory;
