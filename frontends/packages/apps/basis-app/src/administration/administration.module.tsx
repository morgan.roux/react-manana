import React from 'react';
import loadable from '@loadable/component';
import UtilityIcon from '@material-ui/icons/Build';
import BasisCrudIcon from '@material-ui/icons/Add';

import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';

const UtilitairePage = loadable(() => import('./pages/Utilitaire/Utilitaire'), {
  fallback: <SmallLoading />,
});

const administrationModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_ROLES',
      location: 'PARAMETRE',
      name: 'Gestion de données de base',
      to: '/basis"',
      icon: <BasisCrudIcon />,
      preferredOrder: 280,
      authorize: {
        roles: ['SUPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },
    {
      id: 'FEATURE_PARAMETRE_UTILITAIRE',
      location: 'PARAMETRE',
      name: 'Utilitaire',
      to: '/utilitaire',
      icon: <UtilityIcon />,
      preferredOrder: 30,
      authorize: {
        roles: ['SUPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
  routes: [
    {
      path: [
        '/db/utilitaire',
        '/db/utilitaire/indexation',
        '/db/utilitaire/maj-active-directory',
        '/db/utilitaire/maj-helloid',
      ],
      component: UtilitairePage,
      exact: false,
      attachTo: 'PARAMETRE',
    },
  ],
};

export default administrationModule;
