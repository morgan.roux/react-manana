import { uniqWith } from 'lodash';
import {
  GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes,
  GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_collaborateurResponsables,
  GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables,
  GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables_responsables,
} from './../../../../../federation/demarche-qualite/matrice-tache/types/GET_FONCTIONS_WITH_RESPONSABLE_INFOS';

interface KeyValue {
  [key: string]: any;
}

export const extractResponsablesFromFonctions = (
  fonctions: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes[]
): GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables_responsables[] => {
  return uniqWith(
    fonctions.reduce((responsables, currentFonction) => {
      return [
        ...responsables,
        ...currentFonction.taches.reduce((responsablesTache, currentTache) => {
          return [
            ...responsablesTache,
            ...currentTache.collaborateurResponsables.reduce((collaborateurs, currentResponsable) => {
              return [...collaborateurs, ...currentResponsable.responsables] as any;
            }, []),
          ] as any;
        }, []),
      ];
    }, []),
    (a: any, b: any) => a.id === b.id
  );
};

const formatCollaborateursResponsables = (
  collaborateurResponsables:
    | GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_taches_collaborateurResponsables[]
    | GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes_collaborateurResponsables[]
) => {
  return collaborateurResponsables.reduce((responsableCodes, currentResponsables) => {
    return {
      ...responsableCodes,
      ...currentResponsables.responsables.reduce((listCollaborateurs, currentCollaborateur) => {
        return {
          ...listCollaborateurs,
          [currentCollaborateur.id]: currentResponsables.type.code,
        };
      }, {}),
    };
  }, {});
};

export const formatFonctions = (
  fonctions: GET_FONCTIONS_WITH_RESPONSABLE_INFOS_dQMTFonctions_nodes[],
  niveauMatriceFonctions: number
): KeyValue[] => {
  // const responsables = extractResponsablesFromFonctions(fonctions);

  return fonctions
    .filter((fonction: any) => fonction.active)
    .sort((a, b) => a.ordre - b.ordre)
    .reduce((result, currentFonction, fonctionIndex) => {
      return niveauMatriceFonctions === 1
        ? ([
            ...result,
            {
              fonction: currentFonction.libelle,
              ordreFonction: currentFonction.ordre,
              ...formatCollaborateursResponsables(currentFonction.collaborateurResponsables),
            } as any,
          ] as any)
        : [
            ...result,
            ...currentFonction.taches
              .filter((tache: any) => tache.active)
              .sort((a: any, b: any) => a.ordre - b.ordre)
              .reduce((resultTaches: any, currentTache: any, index: number) => {
                return [
                  ...resultTaches,

                  {
                    fonction: currentFonction.libelle,
                    fonctionFirst: index === 0 ? currentFonction.libelle : '',
                    ordreFonction: currentFonction.ordre,
                    index,
                    nombreTaches: currentFonction.taches.length,
                    tache: currentTache.libelle,
                    ordreTache: currentTache.ordre,
                    ...formatCollaborateursResponsables(currentTache.collaborateurResponsables),
                  },
                ] as any;
              }, []),
          ];
    }, []);
};
