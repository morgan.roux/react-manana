import { useApplicationContext } from '@lib/common';

export const useNiveauMatriceFonctions = (ignorePharmacie?: boolean) => {
  const { useParameterValueAsNumber } = useApplicationContext();
  const groupementNiveauMatriceFonctions = useParameterValueAsNumber('0830');
  const pharmacieNiveauMatriceFonctions = useParameterValueAsNumber('0730');

  return ignorePharmacie
    ? groupementNiveauMatriceFonctions
    : pharmacieNiveauMatriceFonctions || groupementNiveauMatriceFonctions;
};
