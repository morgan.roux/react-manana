import { ModuleDefinition, mergeModules } from '@lib/common';
import administrationModule from './administration/administration.module';
import groupeClientModule from './groupe-client/groupe-client.module';
import pharmacieModule from './pharmacie/pharmacie.module';
import presidentRegionModule from './president-region/president-region.module';
import groupeAmisModule from './groupe-amis/groupe-amis.module';
import prestataireModule from './prestataire/prestataire.module';
import translationModule from './translation/translation.module';
import moment from 'moment';

moment.locale('fr');

const basisModule: ModuleDefinition = mergeModules(
  administrationModule,
  groupeClientModule,
  pharmacieModule,
  presidentRegionModule,
  groupeAmisModule,
  prestataireModule,
  translationModule
);

export default basisModule;
