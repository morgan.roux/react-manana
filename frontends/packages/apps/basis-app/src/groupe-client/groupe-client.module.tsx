import React from 'react';
import loadable from '@loadable/component';

import { ModuleDefinition } from '@lib/common';
import { GroupWork } from '@material-ui/icons';

const groupeClientModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_GROUPES_CLIENTS',
      location: 'PARAMETRE',
      name: 'Groupes de clients',
      to: '/groupes-clients',
      icon: <GroupWork />,
      preferredOrder: 190,
      authorize: {
        roles: ['SUPADM', 'GRPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
};

export default groupeClientModule;
