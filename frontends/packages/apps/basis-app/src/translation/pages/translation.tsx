import { Box } from '@material-ui/core';
import React from 'react';
import { NewCustomButton } from '@lib/ui-kit';
import { useTranslation } from 'react-i18next';
import { showTranslations } from 'translation-check';
import { useTrans } from '@lib/common';

const Translation = () => {
  const { i18n } = useTranslation();
  const t = useTrans();

  return (
    <Box
      style={{
        height: '100%',
        textAlign: 'center',
        padding: 20,
      }}
    >
      <div>{t('welcome', 'Hello', { nom: 'Test' })}</div>
      <NewCustomButton onClick={() => showTranslations(i18n)}>Voir Traduction</NewCustomButton>
    </Box>
  );
};

export default Translation;
