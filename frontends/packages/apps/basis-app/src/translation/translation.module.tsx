import React from 'react';
import loadable from '@loadable/component';
import { ModuleDefinition } from '@lib/common';
import { Translate } from '@material-ui/icons';
import { SmallLoading } from '@lib/ui-kit';

const TranslationPage = loadable(() => import('./pages/translation'), { fallback: <SmallLoading /> });

const groupeAmisModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_TRANSLATION',
      location: 'PARAMETRE',
      name: 'Traduction',
      to: '/translation',
      icon: <Translate />,
      preferredOrder: 1000,
      authorize: {
        roles: ['SUPADM'],
      },
    },
  ],

  routes: [
    {
      component: TranslationPage,
      path: '/translation',
      authorize: {
        roles: ['SUPADM'],
      },
    },
  ],
};

export default groupeAmisModule;
