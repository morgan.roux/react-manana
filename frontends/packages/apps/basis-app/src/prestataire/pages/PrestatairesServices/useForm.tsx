import { PARTENAIRE_SERVICE_URL, useApplicationContext, useDisplayNotification } from '@lib/common';
import {
  PartenaireServiceInput,
  StatutPartenaire,
  TypePartenaire,
  useCreate_Update_Partenaire_ServiceMutation,
  usePartenaire_ServiceQuery,
} from '@lib/common/src/graphql';
import { useGet_SecteursQuery } from '@lib/common/src/federation';
import React, { ChangeEvent, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import { FormStepperInterface } from '../../../components/Form/Form';
import Service from './Stepper/Steps/Service';
import moment from 'moment';
const useForm = (props: any) => {
  const initialState: PartenaireServiceInput = {
    id: '',
    nom: '',
    dateDebut: moment.utc().startOf('year').toDate(),
    dateFin: moment.utc().endOf('year').toDate(),
    idTypeService: '',
    typePartenaire: TypePartenaire.NonPartenaire,
    statutPartenaire: StatutPartenaire.Activer,
    adresse1: '',
    codePostal: '',
    ville: '',
    servicePartenaires: [],
    idSecteur: undefined,
    telephone: '',
    email: '',
    cerclePartenariat: 'PHARMACIE',
    prive: true,
  };

  const [values, setValues] = useState<PartenaireServiceInput>(initialState);

  const { refetch } = props;

  const { id: idPartenaire }: any = useParams();

  const { push } = useHistory();

  const [activeStep, setActiveStep] = useState<number>(0);

  const [selectedPharmacie, setSelectedPharmacie] = useState<any>([]);

  const { graphql, federation, currentPharmacie } = useApplicationContext();
  const loadingSecteurs = useGet_SecteursQuery({
    client: federation,
  });

  const displayNotification = useDisplayNotification();

  const {
    id,
    nom,
    dateDebut,
    dateFin,
    idTypeService,
    typePartenaire,
    statutPartenaire,
    adresse1,
    codePostal,
    ville,
    servicePartenaires,
    idSecteur,
    telephone,
    email,
    cerclePartenariat,
    prive,
  } = values;

  const typePartenaireList = [
    { label: 'Non partenaire', value: 'NON_PARTENAIRE' },
    { label: 'Négociation en cours', value: 'NEGOCIATION_EN_COURS' },
    { label: 'Nouveau référencement', value: 'NOUVEAU_REFERENCEMENT' },
    { label: 'Partenariat actif', value: 'PARTENARIAT_ACTIF' },
  ];

  const statutPartenaireList = [
    { label: 'Activé', value: 'ACTIVER' },
    { label: 'Bloqué', value: 'BLOQUER' },
  ];

  const secteurs = loadingSecteurs.data?.secteurs.nodes || [];

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;

      setValues((prevState: any) => ({ ...prevState, [name]: value }));
    }
  };

  const loadingPartenaireService = usePartenaire_ServiceQuery({
    client: graphql,
    variables: { id: idPartenaire },
    skip: !idPartenaire,
    onCompleted: (data) => {
      if (data.partenaire) {
        const partenaire = data.partenaire;
        const input: PartenaireServiceInput = {
          id: partenaire.id,
          nom: partenaire.nom || '',
          dateDebut: partenaire.partenaireServicePartenaire?.dateDebutPartenaire,
          dateFin: partenaire.partenaireServicePartenaire?.dateFinPartenaire,
          idTypeService: '',
          typePartenaire: partenaire.partenaireServicePartenaire?.typePartenaire as any,
          statutPartenaire: partenaire.partenaireServicePartenaire?.statutPartenaire,
          adresse1: partenaire.partenaireServiceSuite?.adresse1,
          codePostal: partenaire.partenaireServiceSuite?.codePostal,
          ville: partenaire.partenaireServiceSuite?.ville,
          cerclePartenariat: partenaire.cerclePartenariat,
          idSecteur: partenaire.idSecteur,
          prive: !!partenaire.prive,
          servicePartenaires:
            ((partenaire.services || []).map((service) => ({ ...service, __typename: undefined })) as any) || [],
        };
        setValues(input);
      }
    },
  });

  const stepOne = [
    {
      title: 'Informations',
      formControls: [
        {
          label: 'Nom',
          required: true,
          textfield: {
            name: 'nom',
            value: nom,
            onChange: handleChange,
            placeholder: 'Nom de la personne',
          },
        },
        {
          label: 'Date début de partenariat',
          required: true,
          datepicker: {
            placeholder: '',
            name: 'dateDebut',
            value: dateDebut,
            onChange: (date: any, value: any) =>
              handleChange({ target: { value: date, name: 'dateDebut' } } as ChangeEvent<any>),
          },
        },
        {
          label: 'Date fin de partenariat',
          required: true,
          datepicker: {
            placeholder: '',
            name: 'dateFin',
            value: dateFin,
            onChange: (date: any, value: any) =>
              handleChange({ target: { value: date, name: 'dateFin' } } as ChangeEvent<any>),
          },
        },
        {
          label: 'Type',
          select: {
            label: 'Type de partenariat',
            list: typePartenaireList,
            listId: 'value',
            index: 'label',
            name: 'typePartenaire',
            value: typePartenaire,
            onChange: handleChange,
          },
        },
        {
          label: 'Statut de partenariat',
          select: {
            list: statutPartenaireList,
            listId: 'value',
            index: 'label',
            name: 'statutPartenaire',
            value: statutPartenaire,
            onChange: handleChange,
          },
        },
        {
          label: 'Propre à la pharmacie',
          switch: {
            checked: !!prive,
            name: 'prive',
            onChange: (event: React.ChangeEvent<HTMLInputElement>) => {
              setValues((prevState: any) => ({
                ...prevState,
                [event.target.name]: event.target.checked,
              }));
            },
          },
        },

        {
          label: 'Partenariat Labo',
          switch: {
            checked: cerclePartenariat === 'LABORATOIRE',
            name: 'cerclePartenariat',
            onChange: (event: React.ChangeEvent<HTMLInputElement>) => {
              setValues((prevState: any) => ({
                ...prevState,
                [event.target.name]: event.target.checked ? 'LABORATOIRE' : 'PHARMACIE',
              }));
            },
          },
        },

        {
          label: 'Secteur',
          hidden: cerclePartenariat !== 'LABORATOIRE',
          select: {
            list: secteurs,
            listId: 'id',
            index: 'libelle',
            name: 'idSecteur',
            value: idSecteur,
            placeholder: 'Sélectionner le secteur',
            onChange: handleChange,
          },
        },
      ],
    },
    {
      title: 'Coordonnées',
      formControls: [
        {
          label: 'Adresse',
          textfield: {
            name: 'adresse1',
            value: adresse1,
            onChange: handleChange,
            placeholder: 'Son adresse',
          },
        },
        {
          label: 'CP',
          textfield: {
            name: 'codePostal',
            value: codePostal,
            onChange: handleChange,
            placeholder: 'Son code postal',
          },
        },
        {
          label: 'Ville',
          textfield: {
            name: 'ville',
            value: ville,
            onChange: handleChange,
            placeholder: 'Sa ville',
          },
        },
        {
          label: 'Email',
          textfield: {
            name: 'email',
            value: email,
            onChange: handleChange,
            placeholder: 'Adresse email',
          },
        },
        {
          label: 'Téléphone',
          textfield: {
            name: 'telephone',
            value: telephone,
            onChange: handleChange,
            placeholder: 'Numéro téléphone',
          },
        },
      ],
    },
  ];

  const disableNextButton = activeStep === 0 ? !nom || !dateDebut || !dateFin : false; //: activeStep === 2 ? selectedPharmacie.length === 0 : false;

  const [upsertPartenaire, upsertingPartenaire] = useCreate_Update_Partenaire_ServiceMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data.createUpdatePartenaireService) {
        displayNotification({ type: 'success', message: `${id ? 'Modification' : 'Ajout'} d'un partenaire réussi` });
        refetch && refetch();
        setValues(initialState);
        setSelectedPharmacie([]);
        goToHome();
      }
    },
  });

  const finalStep = {
    buttonLabel: id ? 'Modifier' : 'Ajouter',
    loading: upsertingPartenaire.loading,
    action: () => {
      upsertPartenaire({
        variables: {
          input: {
            ...values,
            idPharmacie: values.prive ? currentPharmacie.id : undefined,
          },
        },
      });
    },
  };

  const goToHome = () => {
    setValues(initialState);
    setSelectedPharmacie([]);
    push(`/db/${PARTENAIRE_SERVICE_URL}`);
  };

  const stepper: FormStepperInterface = {
    steps: [
      {
        title: 'Informations personnelles',
        content: stepOne,
      },
      {
        title: 'Liste des services',
        children: <Service handleChange={handleChange} values={values} setValues={setValues} />,
      },
    ],
    goToHome,
    activeStep,
    setActiveStep,
    disableNextButton,
    finalStep,
  };
  return { stepper, loadingForm: loadingPartenaireService.loading, mutationLoading: upsertingPartenaire.loading };
};

export default useForm;
