import { createStyles, makeStyles, Theme } from '@material-ui/core';
import moment from 'moment';
import React, { useState } from 'react';
import { PARTENAIRE_SERVICE_URL } from '@lib/common';
import { Column } from '@lib/common/src/components/newCustomContent/interfaces';

interface UseColumnsInterface {
  deleteMutation?: any;
  refetch?: any;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    linkContainer: {
      '& > span:nth-last-child(1)': {
        display: 'none',
      },
    },
  })
);

const useColumns = (params: UseColumnsInterface): Column[] => {
  const { deleteMutation, refetch } = params;

  const columns: Column[] = [
    {
      name: 'nom',
      label: 'Prestataire de service',
      renderer: (value: any) => {
        return value.nom ? value.nom : '-';
      },
    },
    {
      name: 'adresse1',
      label: 'Adresse',
      renderer: (value: any) => {
        return value.partenaireServiceSuite && value.partenaireServiceSuite.adresse1
          ? value.partenaireServiceSuite.adresse1
          : '-';
      },
    },
    {
      name: 'codePostal',
      label: 'CP',
      renderer: (value: any) => {
        return value.partenaireServiceSuite && value.partenaireServiceSuite.codePostal
          ? value.partenaireServiceSuite.codePostal
          : '-';
      },
    },
    {
      name: 'ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value.partenaireServiceSuite && value.partenaireServiceSuite.ville
          ? value.partenaireServiceSuite.ville
          : '-';
      },
    },
    {
      name: 'partenaireServiceSuite.telephone',
      label: 'Téléphone',
      renderer: (value: any) => {
        return value.partenaireServiceSuite && value.partenaireServiceSuite.telephone
          ? value.partenaireServiceSuite.telephone
          : '-';
      },
    },
    {
      name: 'service',
      label: 'Services',
      //   renderer: (value: any) => {
      //     return value.numIvrylab ? value.numIvrylab : '-';
      //   },
    },
    {
      name: 'dateDebutPartenaire',
      label: 'Date début de Partenariat',
      renderer: (value: any) => {
        return value.partenaireServicePartenaire && value.partenaireServicePartenaire.dateDebutPartenaire
          ? moment(value.partenaireServicePartenaire.dateDebutPartenaire).format('DD/MM/YYYY')
          : '-';
      },
    },
    {
      name: 'dateFinPartenaire',
      label: 'Date fin de Partenariat',
      renderer: (value: any) => {
        return value.partenaireServicePartenaire && value.partenaireServicePartenaire.dateFinPartenaire
          ? moment(value.partenaireServicePartenaire.dateFinPartenaire).format('DD/MM/YYYY')
          : '-';
      },
    },
    {
      name: 'typePartenaire',
      label: 'Type de Partenariat',
      renderer: (value: any) => {
        return value.partenaireServicePartenaire && value.partenaireServicePartenaire.typePartenaire
          ? value.partenaireServicePartenaire.typePartenaire
          : '-';
      },
    },
    {
      name: 'statutPartenaire',
      label: 'Statut de Partenariat',
      renderer: (value: any) => {
        if (value.partenaireServicePartenaire && value.partenaireServicePartenaire.statutPartenaire === 'ACTIVER') {
          return 'Activé';
        } else if (
          value.partenaireServicePartenaire &&
          value.partenaireServicePartenaire.statutPartenaire === 'BLOQUER'
        ) {
          return 'Bloqué';
        } else {
          return '-';
        }
      },
    },
    {
      name: '',
      label: '',
      tableActionColumn: {
        baseUrl: PARTENAIRE_SERVICE_URL,
        deleteMutation,
        refetch,
        withViewDetails: true,
      },
    },
  ];
  return columns;
};
export default useColumns;
