import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    radioGroup: {
      width: 800,
      display: 'flex',
      justifyContent: 'space-around',
    },
    serviceFormcontainer: {
      padding: 33,
      display: 'flex',
      alignItems: 'flex-start',
    },
    serviceFormFirstLine: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'flex-start',
    },
    innerFormItems: {
      marginBottom: 20,
      marginRight: 20,
    },
  })
);

export default useStyles;
