import { CustomDatePicker, CustomFormTextField, CustomTextarea } from '@lib/ui-kit';
import { Typography } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import React, { ChangeEvent } from 'react';
import useStyles from './styles';

const ServicesForm = (props: any) => {
  const { handleChange, removeService, values, handleChangeDate, index } = props;
  const classes = useStyles({});
  const { commentaire, nom, dateDemarrage, nbCollaboQualifie, id } = values;

  const handleClick = () => {
    removeService(id);
  };

  const handleEventChange = (event: ChangeEvent<any>) => {
    handleChange(event, index);
  };

  return (
    <div className={classes.serviceFormcontainer}>
      <div>
        <div className={classes.serviceFormFirstLine}>
          <CustomFormTextField
            label="Services"
            onChange={handleEventChange}
            name="nom"
            value={nom}
            className={classes.innerFormItems}
          />
          <CustomDatePicker
            label="Date démarrage"
            placeholder=""
            value={dateDemarrage}
            name="dateDemarrage"
            onChange={handleChangeDate('dateDemarrage', index)}
            InputLabelProps={{ shrink: true }}
            className={classes.innerFormItems}
          />
          <Typography style={{ fontSize: 10, marginRight: 4 }}>Nbre de collaborateurs qualifiés</Typography>
          <input
            type="number"
            min="0"
            name="nbCollaboQualifie"
            onChange={handleEventChange}
            style={{ width: 39, height: 25 }}
            value={nbCollaboQualifie}
            className={classes.innerFormItems}
          />
        </div>
        <CustomTextarea
          onChangeTextarea={handleEventChange}
          label="Commentaire"
          name="commentaire"
          value={commentaire}
          rows={4}
          rowsMax={4}
          style={{ margin: 18 }}
        />
      </div>
      <Close onClick={handleClick} />
    </div>
  );
};

export default ServicesForm;
