import FormContainer from '@lib/common/src/components/FormContainer';
import { Button } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { FC } from 'react';
import ServicesForm from './servicesForm';

interface ServiceProps {
  values: any;
  setValues: any;
  handleChange: (e: React.ChangeEvent<any>, index: number) => void;
}

const Service: FC<ServiceProps> = (props) => {
  const { values, setValues } = props;
  const { servicePartenaires } = values;

  const getRandomInt = (min: number, max: number) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };
  const initialServices = {
    id: getRandomInt(100000, 9999999),
    nom: '',
    commentaire: '',
    dateDemarrage: null,
    nbCollaboQualifie: 0,
  };

  const pushNewService = () => {
    setValues((prevState: any) => ({
      ...prevState,
      servicePartenaires: [...prevState.servicePartenaires, { ...initialServices, id: getRandomInt(100000, 9999999) }],
    }));
  };

  const removeService = (id: number) => {
    const newServices = servicePartenaires && servicePartenaires.filter((service: any) => service && service.id !== id);
    setValues((prevState: any) => ({ ...prevState, servicePartenaires: newServices }));
  };

  const handleChangeDate = (name: string, itemIndex: number) => (date: any) => {
    const newServices =
      servicePartenaires &&
      servicePartenaires.map((service: any, index: number) => {
        if (itemIndex === index) {
          service[name] = date;
          return service;
        } else {
          return service;
        }
      });
    setValues((prevState: any) => ({ ...prevState, servicePartenaires: newServices }));
  };

  const handleServiceChange = (event: any, itemIndex: number) => {
    const { name, value, type } = event.target;
    const elementValue = type === 'number' ? parseInt(value, 10) : value;
    const newServices =
      servicePartenaires &&
      servicePartenaires.map((service: any, index: number) => {
        if (itemIndex === index) {
          return { ...service, [name]: elementValue };
        } else {
          return service;
        }
      });
    setValues((prevState: any) => ({ ...prevState, servicePartenaires: newServices }));
  };

  const listServices =
    servicePartenaires &&
    servicePartenaires.map((service: any, index: number) => (
      <ServicesForm
        removeService={() => removeService(service.id)}
        values={service}
        handleChange={handleServiceChange}
        handleChangeDate={handleChangeDate}
        index={index}
      />
    ));

  return (
    <div>
      <FormContainer title="Listes des services proposés">{listServices}</FormContainer>
      <Button fullWidth={true} startIcon={<Add />} style={{ border: 'dashed' }} onClick={pushNewService}>
        Ajouter un service
      </Button>
    </div>
  );
};

export default Service;
