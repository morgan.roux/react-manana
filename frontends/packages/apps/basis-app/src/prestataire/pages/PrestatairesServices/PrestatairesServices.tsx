//import Form from '@app/basis/src/components/Form';
import {
  PARTENAIRE_SERVICE_URL,
  useApplicationContext,
  useDisplayNotification,
  useFilters,
  useSubToolbar,
} from '@lib/common';
import CustomContent, { SearchInput } from '@lib/common/src/components/newCustomContent';
import { BuildQuery } from '@lib/common/src/components/withSearch/queryBuilder';
import {
  useDelete_Soft_Partenaire_ServiceMutation,
  useSearch_Custom_Content_PartenaireQuery,
} from '@lib/common/src/graphql';
import { ConfirmDeleteDialog } from '@lib/ui-kit';
import React, { FC, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useLocation, useParams } from 'react-router';
import FormUser from '../../../components/FormUser';
import Form from '../../../components/Form';
import useStyles from './styles';
import useColumns from './useColumns';
import useForm from './useForm';

const PartenaireService: FC = () => {
  const type = 'partenaire';

  const { pathname } = useLocation();

  const displayNotification = useDisplayNotification();

  const { resetSearchFilters } = useFilters();

  const onForm: boolean = pathname.includes('/create') || pathname.includes('/edit');

  const onUserForm: boolean = pathname.includes('/user');

  const classes = useStyles({});

  const [selected, setSelected] = useState<any>([]);

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const { graphql, isSuperAdmin, isAdminGroupement, currentGroupement, currentPharmacie } = useApplicationContext();

  const must = [{ term: { idGroupement: currentGroupement.id } }, { term: { isRemoved: false } }];
  const should = [
    {
      bool: {
        must_not: {
          exists: {
            field: 'idPharmacie',
          },
        },
      },
    },

    {
      bool: {
        must: [
          {
            exists: {
              field: 'idPharmacie',
            },
          },
          {
            term: {
              idPharmacie: currentPharmacie.id,
            },
          },
        ],
      },
    },
  ];

  const { query, skip, take } = BuildQuery({ type, optionalMust: must, optionalShould: should });

  const variables = { take, skip, type: [type], query };

  const listResult = useSearch_Custom_Content_PartenaireQuery({ client: graphql, variables });

  const [deletePartenaire, deletingPartenaires] = useDelete_Soft_Partenaire_ServiceMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data.softDeletePartenaireServices) {
        displayNotification({ type: 'success', message: 'Partenaire supprimé avec succès' });
        setSelected([]);
        setOpenDeleteDialog(false);
        listResult.refetch();
      }
    },
    onError: () => {
      displayNotification({ type: 'error', message: 'Erreur lors de la suppression du partenaire' });
    },
  });

  const handleDeletePartenaires = () => {
    if (selected && selected.length > 0) {
      deletePartenaire({ variables: { ids: selected.map((titulaire: any) => titulaire.id) } });
    }
  };

  const handleOpenDeleteDialog = () => {
    setOpenDeleteDialog(true);
  };

  const columns = useColumns({ deleteMutation: deletePartenaire, refetch: listResult.refetch });

  const listPartenaireService = (
    <div className={classes.listTitulaireRoot}>
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher un partenaire" />
      </div>
      <CustomContent
        tableRowClassName={classes.tableCell}
        selected={selected}
        setSelected={setSelected}
        {...{ listResult, columns }}
        isSelectable={true}
      />
    </div>
  );

  const subToolbar = useSubToolbar({
    url: PARTENAIRE_SERVICE_URL,
    title: 'Liste des partenaires de services',
    selected,
    addBtnLabel: 'Ajouter un partenaire',
    onDelete: handleOpenDeleteDialog,
  });

  const confirmDeleteDialog = (
    <ConfirmDeleteDialog
      open={openDeleteDialog}
      setOpen={setOpenDeleteDialog}
      title="Supprimer la sélection"
      onClickConfirm={handleDeletePartenaires}
      isLoading={deletingPartenaires.loading}
    />
  );

  const { stepper, loadingForm, mutationLoading } = useForm({ refetch: listResult.refetch });

  useEffect(() => {
    return () => {
      resetSearchFilters();
    };
  }, []);

  if (onUserForm) {
    return <FormUser />;
  }

  if (onForm) {
    return (
      <Form
        className={classes.form}
        createTitle="Ajout d'un nouveau partenaire"
        editTitle="Modification d'un partenaire"
        stepper={stepper}
        loading={loadingForm}
        mutationLoading={mutationLoading}
      />
    );
  }

  // if (id) {
  //   return <DetailsTitulaire />;
  // }
  return (
    <div>
      <Helmet>
        <title>Partenaires de Services</title>
      </Helmet>
      {subToolbar}
      {listPartenaireService}
      {confirmDeleteDialog}
    </div>
  );
};

export default PartenaireService;
