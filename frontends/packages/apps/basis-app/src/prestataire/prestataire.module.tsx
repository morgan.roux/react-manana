import React from 'react';
import loadable from '@loadable/component';
import GroupIcon from '@material-ui/icons/Group';
import PharmacieIcon from '@material-ui/icons/LocalHospital';
import TitulaireIcon from '@material-ui/icons/AccountBox';

import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';
import { BusinessCenter } from '@material-ui/icons';

const PrestatairesServicesPage = loadable(() => import('./pages/PrestatairesServices/PrestatairesServices'), {
  fallback: <SmallLoading />,
});

const prestataireModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_PRESTATAIRES',
      location: 'PARAMETRE',
      name: 'Partenaires de Services',
      to: '/partenaires-services',
      icon: <BusinessCenter />,
      preferredOrder: 310,
      /*authorize: {
        roles: ['SUPADM', 'GRPADM'],
      },*/
    },
  ],
  routes: [
    {
      path: [
        '/partenaires-services',
        '/partenaires-services/create',
        '/partenaires-services/edit/:id',
        '/partenaires-services/user/create/:id',
        '/partenaires-services/user/edit/:id/:userId',
      ],
      component: PrestatairesServicesPage,
      exact: false,
      attachTo: 'PARAMETRE',
    },
  ],
};

export default prestataireModule;
