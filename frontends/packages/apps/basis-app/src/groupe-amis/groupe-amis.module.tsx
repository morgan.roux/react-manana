import React from 'react';
import loadable from '@loadable/component';
import { ModuleDefinition } from '@lib/common';
import { GroupAdd } from '@material-ui/icons';

const groupeAmisModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_GROUPE_AMIS',
      location: 'PARAMETRE',
      name: "Groupe d'amis",
      to: '/groupes-amis',
      icon: <GroupAdd />,
      preferredOrder: 110,
      authorize: {
        roles: ['SUPADM', 'GRPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
};

export default groupeAmisModule;
