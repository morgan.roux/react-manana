import { useApplicationContext, useDisplayNotification } from '@lib/common';
import {
  Delete_Soft_Information_LiaisonsMutationVariables,
  useDelete_Soft_Information_LiaisonsMutation,
} from '@lib/common/src/graphql';
import { useState } from 'react';

/**
 *  Create Echange hooks
 */
const useDeleteCahierLiaison = (values: Delete_Soft_Information_LiaisonsMutationVariables) => {
  // const {
  //   content: { variables, operationName },
  // } = useContext<ContentStateInterface>(ContentContext);

  const displayNotification = useDisplayNotification();
  const [mutationDeleteSuccess, setMutationDeleteSuccess] = useState<boolean>(false);
  const { graphql } = useApplicationContext();

  const [deleteCahierLiaison, { loading: mutationLoadingDelete }] = useDelete_Soft_Information_LiaisonsMutation({
    client: graphql,
    variables: {
      ids: values.ids,
    },
    update: (cache, { data }) => {
      if (data && data.softDeleteInformationLiaisons) {
        // if (variables && operationName) {
        //   const req: any = cache.readQuery({
        //     query: operationName,
        //     variables,
        //   });
        //   if (req && req.search && req.search.data) {
        //     cache.writeQuery({
        //       query: operationName,
        //       data: {
        //         search: {
        //           ...req.search,
        //           ...{
        //             total: req.search.total + 1,
        //             data: [...req.search.data, data.softDeleteInformationLiaisons],
        //           },
        //         },
        //       },
        //       variables,
        //     });
        //   }
        // }
      }
    },
    onCompleted: (data) => {
      if (data && data.softDeleteInformationLiaisons) {
        displayNotification({
          type: 'success',
          message: 'Suppression effectuée avec succès',
        });
        setMutationDeleteSuccess(true);
        //client.writeData({ data: { checkedsCahierLiaison: null } });
      }
    },
    onError: (errors) => {
      console.log(errors);
      if (errors.graphQLErrors) {
        errors.graphQLErrors.map((err) => {
          displayNotification({ type: 'error', message: err.message });
        });
      }
    },
  });

  return { useDeleteCahierLiaison, deleteCahierLiaison, mutationDeleteSuccess };
};

export default useDeleteCahierLiaison;
