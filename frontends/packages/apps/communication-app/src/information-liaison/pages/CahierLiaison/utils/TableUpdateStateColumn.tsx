import { useApplicationContext, useUploadFiles } from '@lib/common';
import {
  FichierInput,
  TakeChargeInformationLiaisonInput,
  useUpdate_Information_Liaison_StatusMutation,
} from '@lib/common/src/graphql';
import { Backdrop, ConfirmDeleteDialog } from '@lib/ui-kit';
import { MenuItem, Select, SvgIcon } from '@material-ui/core';
import { SvgIconProps } from '@material-ui/core/SvgIcon';
import { AxiosResponse } from 'axios';
import { uniqBy } from 'lodash';
import React, { FC, ReactNode, useState } from 'react';
import PriseEnCharge from '../DetailsInformation/Modals/PriseEnCharge';
import useTakeChargeInformationLiaison from './useTakeChargeInformationLiaison';

export interface TableActionColumnMenu {
  label: any;
  icon: ReactNode;
  disabled: boolean;
  onClick: (event: any, row: any) => void;
}

export interface TableActionColumnProps {
  row: any;
  baseUrl: string;
  setOpenDeleteDialog?: React.Dispatch<React.SetStateAction<boolean>>;
  withViewDetails?: boolean;
  showResendEmail?: boolean;
  customMenuItems?: TableActionColumnMenu[];
  handleClickDelete?: (row: any) => void;
  handleClickAffectation?: (action: any, row: any) => void;
}

export const ArretSansRemplacementIcon = (props: SvgIconProps) => {
  return (
    <SvgIcon {...props} viewBox="0 0 24 24">
      <g id="delete-24px" width="24" height="24" viewBox="0 0 24 24">
        <path
          id="Tracé_2876"
          data-name="Tracé 2876"
          d="M206.063,71.652h0a.968.968,0,0,1,.978.944.289.289,0,0,0,.292.282h7.4a.288.288,0,0,0,.293-.28V62.641a.289.289,0,0,0-.292-.281h-7.4a.289.289,0,0,0-.293.281.981.981,0,0,1-1.961,0,2.216,2.216,0,0,1,2.253-2.175h7.4a2.217,2.217,0,0,1,2.245,2.17V72.6a2.214,2.214,0,0,1-2.247,2.17h-7.4a2.216,2.216,0,0,1-2.253-2.17A.968.968,0,0,1,206.063,71.652Z"
          transform="translate(-200.243 -60.467)"
          fill="#424242"
        />
        <path
          id="Tracé_2877"
          data-name="Tracé 2877"
          d="M220.179,66.837l-3.032-3.479h0a.816.816,0,0,0-.6-.291h-.013a.8.8,0,0,0-.606.288,1.106,1.106,0,0,0,0,1.419l1.64,1.854h-8.092a1.005,1.005,0,0,0,0,1.991h8.119l-1.606,1.852A1.125,1.125,0,0,0,216,71.895a.832.832,0,0,0,.594.271h.013a.8.8,0,0,0,.606-.291L220.1,68.56A1.354,1.354,0,0,0,220.179,66.837Z"
          transform="translate(-208.604 -60.467)"
          fill="#424242"
        />
      </g>
    </SvgIcon>
  );
};

export const AttributionDepartementIcon = (props: SvgIconProps) => {
  const { color } = props;
  return (
    <SvgIcon {...props} viewBox="0 0 24 24">
      <g width="18.68" height="17" viewBox="0 0 18.68 17">
        <g id="Groupe_13167" data-name="Groupe 13167" transform="translate(-177.231 -57.446)">
          <path
            id="Icon_material-person-pin-circle"
            data-name="Icon material-person-pin-circle"
            d="M183.881,57.446a6.657,6.657,0,0,0-6.65,6.65c0,4.987,6.65,10.35,6.65,10.35s6.65-5.363,6.65-10.35A6.657,6.657,0,0,0,183.881,57.446Zm0,1.9a1.9,1.9,0,1,1-1.9,1.9h0A1.908,1.908,0,0,1,183.881,59.346Zm0,9.5a4.541,4.541,0,0,1-3.8-2.043c.019-1.257,2.537-1.947,3.8-1.947s3.781.694,3.8,1.948A4.541,4.541,0,0,1,183.881,68.846Z"
            fill={color ? color : '#424242'}
          />
          <path
            id="Tracé_2878"
            data-name="Tracé 2878"
            d="M195.437,59.5h-1.581V57.92a.473.473,0,0,0-.473-.474h-.645a.474.474,0,0,0-.473.474V59.5h-1.581a.473.473,0,0,0-.473.473v.645a.473.473,0,0,0,.473.473h1.581v1.581a.473.473,0,0,0,.473.473h.645a.472.472,0,0,0,.473-.473V61.092h1.581a.474.474,0,0,0,.474-.473v-.645A.473.473,0,0,0,195.437,59.5Z"
            fill={color ? color : '#424242'}
          />
        </g>
      </g>
    </SvgIcon>
  );
};

const TableUpdateStateColumn: FC<TableActionColumnProps> = ({ row }) => {
  const { graphql, notify } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const [statutData, setStatutData] = useState([
    {
      label: 'Non lues',
      value: 'NON_LUES',
    },
    {
      label: 'Lues',
      value: 'LUES',
    },
    {
      label: 'Prendre en charge',
      value: 'PRENDRE_EN_CHARGE',
    },
    {
      label: 'Clôturée',
      value: 'CLOTURE',
    },
  ]);

  const [selectStatueValue, setSelectStatueValue] = useState(row.statut);

  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openConfirm, setOpenConfirm] = useState<boolean>(false);

  const [doUpdateStatus, { loading: updateStatusLoading }] = useUpdate_Information_Liaison_StatusMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.updateInformationLiaisonStatut) {
        notify({
          type: 'success',
          message: 'Statut mis à jour avec succès',
        });
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  const handleChangeSelect = (e: any) => {
    const value = e.target.value;
    if (value === 'PRENDRE_EN_CHARGE') {
      setOpenModal(true);
    } else if (value === 'CLOTURE') {
      setOpenConfirm(true);
    } else {
      setSelectStatueValue(value);
    }
  };

  const [loading, setLoading] = useState<boolean>(false);
  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  let files: FichierInput[] = [];
  let uploadResult: AxiosResponse<any> | null = null;

  const [stateTakeCharge, setStateTakeCharge] = useState({
    date: new Date(),
    description: '',
    files,
  });

  const takeCharge: TakeChargeInformationLiaisonInput = {
    id: row && row.id,
    date: stateTakeCharge.date,
    description: stateTakeCharge.description,
    files: stateTakeCharge.files,
  };

  const { takeChargeInformationLiaison, mutationTakeChargeSuccess } = useTakeChargeInformationLiaison(takeCharge);

  const handleAddClickTakeCharge = () => {
    setLoading(true);
    if (selectedFiles && selectedFiles.length > 0) {
      uploadFiles(selectedFiles, {
        directory: 'cahier-liaison',
      })
        .then((uploadedFiles) => {
          files = uniqBy([...files, ...uploadedFiles.map((uploadedFile) => ({ ...uploadedFile, id: '' }))], 'chemin');
          setStateTakeCharge((prevState) => ({ ...prevState, ['files']: files }));

          takeChargeInformationLiaison();
          setLoading(false);
        })
        .catch((error) => {
          notify({
            type: 'error',
            message: error.message,
          });
          setLoading(false);
        });
    } else {
      takeChargeInformationLiaison();
    }
    setSelectStatueValue('CLOTURE');
  };

  React.useEffect(() => {
    setSelectStatueValue(selectStatueValue);
    if (
      selectStatueValue !== row.statut &&
      selectStatueValue !== 'CLOTURE' &&
      selectStatueValue !== 'PRENDRE_EN_CHARGE'
    ) {
      doUpdateStatus({ variables: { id: row.id, statut: selectStatueValue } });
    }
  }, [selectStatueValue]);

  if (row.statut === 'CLOTURE') return <span>Clôturée</span>;

  const DeleteDialogContent = () => {
    return (
      <React.Fragment>
        Etes-vous sûr de vouloir clôturer cette information :{' '}
        <span style={{ fontWeight: 'bold', padding: 0 }}>{row.description}</span> ?
      </React.Fragment>
    );
  };

  return (
    <div>
      {(loading || uploadingFiles.loading || updateStatusLoading) && <Backdrop value={'Opération en cours...'} />}
      <PriseEnCharge
        open={openModal}
        setSelectedFiles={setSelectedFiles}
        selectedFiles={selectedFiles}
        stateTakeCharge={stateTakeCharge}
        setStateTakeCharge={setStateTakeCharge}
        setOpen={setOpenModal}
        handleAddClickTakeCharge={handleAddClickTakeCharge}
      />

      <ConfirmDeleteDialog
        title="Clôture d'information"
        content={<DeleteDialogContent />}
        open={openConfirm}
        setOpen={setOpenConfirm}
        // tslint:disable-next-line: jsx-no-lambda
        onClickConfirm={() => {
          doUpdateStatus({ variables: { id: row.id, statut: 'CLOTURE' } });
          setOpenConfirm(false);
        }}
        confirmBtnLabel="Clôturer"
      />

      <Select
        id={row.id}
        name={row.id}
        value={selectStatueValue}
        onChange={handleChangeSelect}
        MenuProps={{
          getContentAnchorEl: null,
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'left',
          },
        }}
      >
        {statutData.map((statut, index) => {
          return (
            <MenuItem value={statut.value} key={index}>
              {statut.label}
            </MenuItem>
          );
        })}
      </Select>
    </div>
  );
};

export default TableUpdateStateColumn;
