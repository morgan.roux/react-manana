import { useApplicationContext, HtmlTooltip } from '@lib/common';
import { Typography } from '@material-ui/core';
import classnames from 'classnames';
import React, { Fragment } from 'react';
import useStyles from '../../leftContainerItemStyles';
import useOthersStyles from './styles';

interface ColleguesConcerneesProps {
  info: any;
}

const ColleguesConcernees: React.FC<ColleguesConcerneesProps> = ({ info }) => {
  const classes = useStyles();
  const othersClasses = useOthersStyles();

  const { user: currentUser } = useApplicationContext();

  const currentUserId = currentUser.id;
  const currentUserName = currentUser.userName;

  const colleguesConcerneesIds: string[] =
    (info &&
      info.colleguesConcernees &&
      info.colleguesConcernees.length > 0 &&
      info.colleguesConcernees.map((i: any) => i.userConcernee.id)) ||
    [];

  const colleguesConcerneesNames: string[] =
    (info &&
      info.colleguesConcernees &&
      info.colleguesConcernees.length > 0 &&
      info.colleguesConcernees.map((i: any) => i.userConcernee.userName)) ||
    [];

  const isCollegueConcerned: boolean = colleguesConcerneesIds.includes(currentUserId);

  const defaultCollegueConcerneeName = isCollegueConcerned ? currentUserName : colleguesConcerneesNames[0];
  const othersColleguesConcerneesNames: string[] = colleguesConcerneesNames.filter(
    (i) => i !== defaultCollegueConcerneeName
  );

  return defaultCollegueConcerneeName && othersColleguesConcerneesNames.length === 0 ? (
    <Typography
      className={classnames(
        classes.labelContentValue,
        classes.lenghtValueName,
        othersClasses.collegueConcerneeName,
        othersClasses.infoValue
      )}
    >
      {defaultCollegueConcerneeName === currentUserName ? 'Moi' : defaultCollegueConcerneeName}
    </Typography>
  ) : defaultCollegueConcerneeName && othersColleguesConcerneesNames.length > 0 ? (
    <HtmlTooltip
      arrow={true}
      title={
        <Fragment>
          {[defaultCollegueConcerneeName, ...othersColleguesConcerneesNames].map((name, index) => {
            return (
              <Fragment key={index}>
                {name === currentUserName ? `${name} (Moi)` : name}
                <br />
              </Fragment>
            );
          })}
        </Fragment>
      }
    >
      <Typography
        className={classnames(
          classes.labelContentValue,
          classes.lenghtValueName,
          othersClasses.collegueConcerneeName,
          othersClasses.infoValue
        )}
      >
        {`${defaultCollegueConcerneeName === currentUserName ? 'Moi' : defaultCollegueConcerneeName} et ${
          othersColleguesConcerneesNames.length
        } autre(s) personne(s)`}
      </Typography>
    </HtmlTooltip>
  ) : (
    <span>{'-'}</span>
  );
};

export default ColleguesConcernees;
