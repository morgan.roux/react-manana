import { CAHIER_LIAISON_URL, isMobile, useApplicationContext, useDisplayNotification } from '@lib/common';
import {
  Search_Custom_Content_Information_LiaisonDocument,
  useInformation_LiaisonLazyQuery,
  useRead_Information_Liaison_By_User_ConcernedMutation,
  useSearch_Custom_Content_Information_LiaisonLazyQuery,
  useSearch_Custom_Content_Information_LiaisonQuery,
} from '@lib/common/src/graphql';
import { Backdrop, ConfirmDeleteDialog, CustomModal } from '@lib/ui-kit';
import { Box, Hidden, IconButton } from '@material-ui/core';
import { Create } from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import React, { Fragment, useState } from 'react';
import { useEffect } from 'react';
import { useHistory, useLocation, useParams } from 'react-router';
import useCommonStyles from '../commonStyles';
import CahierLiaisonForm from './CahierLiaisonForm';
import { LeftContainer, MainContainer } from './CahierLiaisonMainContainer';
import useStyles from './styles';
import { useDeleteCahierLiaison } from './utils';
import { useButtonHeadAction } from './utils/utils';

interface ICahierLiaisonProps {
  searchTxt?: string;
  statuts?: any[];
  startDate?: any;
  endDate?: any;
  stateCollegeConcernees?: any;
  ckeckedMyInfo: boolean;
  handleSearchTxt: (arg0: any) => void;
  informations: string;
  filters: any;
  refetchCount: () => void;
}

const CahierLiaisonMain: React.FC<ICahierLiaisonProps> = ({
  searchTxt,
  statuts,
  stateCollegeConcernees,
  startDate,
  endDate,
  ckeckedMyInfo,
  handleSearchTxt,
  informations,
  filters,
  refetchCount,
}) => {
  const classes = useStyles();
  const commonClasses = useCommonStyles();

  const {
    user: currentUser,
    currentGroupement: groupement,
    graphql,
    currentPharmacie: pharmacie,
  } = useApplicationContext();

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const { push } = useHistory();
  const { pathname } = useLocation();

  const [openFormModal, setOpenFormModal] = useState<boolean>(false);

  const { idliaison, from }: any = useParams();

  const currentUserId = currentUser.id;

  const displayNotification = useDisplayNotification();

  const isOnCreate = pathname === `${CAHIER_LIAISON_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`${CAHIER_LIAISON_URL}/edit`);
  const isOnList = !isOnCreate && !isOnEdit;

  const [loading, setLoading] = useState<boolean>(false);

  const [activeItem, setActiveItem] = React.useState<any>(null);

  const [showMainContainer, setShowMainContainer] = useState<boolean>(isMobile() ? false : true);

  const handleClickItem = (item: any, event: any) => {
    if (item) {
      const collegueConcernee = item?.colleguesConcernees?.find((i: any) => i.userConcernee.id === currentUserId);

      if (collegueConcernee?.id && collegueConcernee.statut === 'NON_LUES') {
        readInfoByUserConcerned({ variables: { id: collegueConcernee.id, lu: true } });
      }
    }

    if (event) {
      event.stopPropagation();
    }
    setActiveItem(item);

    window.history.pushState(
      null,
      '',
      item
        ? `#${CAHIER_LIAISON_URL}/${informations === 'coupleStatutDeclarant' ? 'emise' : 'recue'}/${item.id}`
        : `#${CAHIER_LIAISON_URL}/${informations === 'coupleStatutDeclarant' ? 'emise' : 'recue'}`
    );
    setShowMainContainer(true);
  };

  const { goToAddInformation, goBack } = useButtonHeadAction(push);

  const DeleteDialogContent = () => {
    return <Fragment>Êtes-vous sûr de vouloir supprimer ces informations de liaison ?</Fragment>;
  };

  const [selected, setSelected] = useState<any[]>([]);

  let ids: any[] = [];
  ids = selected.map((s) => {
    return s.id;
  });
  const { deleteCahierLiaison, mutationDeleteSuccess } = useDeleteCahierLiaison({ ids });

  const defaultMust: any[] = [{ term: { isRemoved: false } }, { term: { 'pharmacie.id': pharmacie.id } }];

  const must: any[] = [{ term: { isRemoved: false } }, { term: { 'pharmacie.id': pharmacie.id } }];
  const should: any[] = [];

  if (searchTxt && searchTxt !== '') {
    searchTxt = searchTxt.replaceAll(' ', '\\ ');
    must.push({
      query_string: {
        query: `*${searchTxt}*`,
        analyze_wildcard: true,
      },
    });
    defaultMust.push({
      query_string: {
        query: `*${searchTxt}*`,
        analyze_wildcard: true,
      },
    });
  }

  if (filters?.idImportances?.length > 0) {
    must.push({ terms: { 'importance.id': filters.idImportances } });
  }
  if (filters?.idUrgences?.length > 0) {
    must.push({ terms: { 'urgence.id': filters.idUrgences } });
  }

  if (startDate && startDate !== '' && endDate && endDate !== '') {
    must.push({
      range: {
        dateCreation: {
          gte: moment(startDate).format('YYYY-MM-DD'),
          lte: moment(endDate).format('YYYY-MM-DD'),
        },
      },
    });
  }

  if (stateCollegeConcernees?.concernedColleaguesIds?.length > 0) {
    (statuts && statuts.length > 0
      ? statuts
      : informations === 'coupleStatutDeclarant'
      ? ['EN_COURS', 'CLOTURE']
      : ['NON_LUES', 'LUES', 'CLOTURE']
    ).map((statut) => {
      should.push({
        terms: {
          [informations]: stateCollegeConcernees?.concernedColleaguesIds.map((id: any) => `${id}-${statut}`),
        },
      });
    });
  }

  // My infos filter
  if (ckeckedMyInfo) {
    must.push({ term: { 'declarant.id': currentUserId } });
  }

  const sort =
    informations === 'coupleStatutDeclarant'
      ? [{ dateCreation: { order: 'desc' } }, { statut: { order: 'desc' } }]
      : [{ priority: { order: 'asc' } }, { 'colleguesConcernees.statut': { order: 'desc' } }];

  const queryVariables: any = {
    type: ['informationliaison'],
    query: {
      query: { bool: should.length > 0 ? { must, should, minimum_should_match: 1 } : { must } },
    },
    take: 10,
    skip: 0,
    sortBy: sort,
  };

  const [readInfoByUserConcerned] = useRead_Information_Liaison_By_User_ConcernedMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.readInformationLiaisonByUserConcerned) {
        handleRefetch(false);
        if (activeItem) loadInformationLiaison({ variables: { id: activeItem.id } });
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const [loadInformationLiaison] = useInformation_LiaisonLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: graphql,
    onCompleted: (data) => {
      if (data.informationLiaison) {
        setActiveItem(data.informationLiaison);
        const collegueConcernee = data.informationLiaison?.colleguesConcernees?.find(
          (i: any) => i.userConcernee.id === currentUserId
        );
      }
    },
  });

  const [loadInformationLiaisons, { data, loading: queryLoading, fetchMore, refetch }] =
    useSearch_Custom_Content_Information_LiaisonLazyQuery({
      client: graphql,
      variables: queryVariables,
      onCompleted: (data) => {
        if (idliaison && data.search?.data) {
          const activeItem: any = data.search.data.find((el: any) => el.id === idliaison);
          if (activeItem) {
            setActiveItem(activeItem);
            const collegueConcernee = activeItem?.colleguesConcernees?.find(
              (i: any) => i.userConcernee.id === currentUserId
            );

            if (collegueConcernee?.id && collegueConcernee.statut === 'NON_LUES') {
              readInfoByUserConcerned({ variables: { id: collegueConcernee.id, lu: true } });
            }
          } else {
            loadInformationLiaison({ variables: { id: idliaison } });
          }
        }
      },
      onError: (error) => {
        error.graphQLErrors.map((err) => {
          displayNotification({ type: 'error', message: err.message });
        });
      },
    });

  useEffect(() => {
    if (isOnList) {
      loadInformationLiaisons();
    }
  }, []);

  useEffect(() => {
    setActiveItem(null);
    refetch && refetch();
  }, [informations]);

  useEffect(() => {
    refetch && refetch();
  }, [stateCollegeConcernees, statuts]);

  const onClickConfirmDelete = () => {
    deleteCahierLiaison();
    setOpenDeleteDialog(false);
  };

  const handleRefetch = (withInformation: boolean = true) => {
    //refetch && refetch();
    refetchCount();
  };

  return (
    <Box className={!isOnList ? classnames(classes.container) : classnames(classes.container, classes.marginTop64)}>
      <Backdrop open={queryLoading} />
      {isOnList ? (
        <Fragment>
          <Box className={classnames(classes.contentMain)}>
            <Box className={commonClasses.root}>
              {(!isMobile() || !showMainContainer) && (
                <Box>
                  <Hidden mdUp={true} implementation="css">
                    <Box className={classes.createButton}>
                      <IconButton onClick={goToAddInformation} color="primary">
                        <Create />
                      </IconButton>
                    </Box>
                  </Hidden>
                  <LeftContainer
                    data={
                      activeItem && !(data?.search?.data || []).some((el: any) => el.id === activeItem.id)
                        ? { search: { data: [activeItem, ...(data?.search?.data || [])] } }
                        : data
                    }
                    activeItem={activeItem}
                    handleClickItem={handleClickItem}
                    fetchMore={fetchMore}
                    queryVariables={queryVariables}
                    refetch={handleRefetch}
                    handleSearchTxt={handleSearchTxt}
                  />
                </Box>
              )}

              {!isMobile() && (
                <MainContainer activeItem={activeItem} refetch={handleRefetch} queryVariables={queryVariables} />
              )}
            </Box>
          </Box>
        </Fragment>
      ) : (
        <Fragment>
          <Box className={classes.contentMain}>
            <CahierLiaisonForm queryVariables={queryVariables} refetch={handleRefetch} />
          </Box>
        </Fragment>
      )}
      <CustomModal
        open={isMobile() && showMainContainer}
        setOpen={setShowMainContainer}
        fullScreen={true}
        closeIcon={true}
        withBtnsActions={false}
        noDialogContent={true}
      >
        <MainContainer
          activeItem={activeItem}
          refetch={handleRefetch}
          handleDrawerToggle={() => setShowMainContainer(false)}
          queryVariables={queryVariables}
        />
      </CustomModal>

      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </Box>
  );
};

export default CahierLiaisonMain;
