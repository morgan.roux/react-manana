import { useApplicationContext, useUploadFiles } from '@lib/common';
import {
  FichierInput,
  InformationLiaisonInput,
  Search_Custom_Content_Information_LiaisonDocument,
  TypeFichier,
  useCreate_Update_Information_LiaisonMutation,
  useInformation_LiaisonQuery,
  useTake_Charge_Information_LiaisonMutation,
} from '@lib/common/src/graphql';
import { useState } from 'react';
import { useHistory, useParams } from 'react-router';

interface FormState {
  tache: any;
  fichiersJoints: File[];
  takeChargeFiles: File[];
  collegues: any;
  declarant: any;
  origineAssocie: any;
}

export const initialState: InformationLiaisonInput = {
  idType: '',
  idItem: undefined,
  idItemAssocie: undefined,
  idOrigine: '',
  idOrigineAssocie: undefined,
  statut: 'EN_COURS',
  titre: '',
  description: '',
  idDeclarant: '',
  idColleguesConcernees: [],
  bloquant: false,
  idFonction: undefined,
  idTache: undefined,
  idUrgence: '',
  idImportance: '',
};

const useCahierLiaisonForm = (props: any) => {
  const { queryVariables, refetch } = props;
  let files: FichierInput[] = [];
  const { push } = useHistory();
  const [input, setInput] = useState<InformationLiaisonInput>(initialState);
  const [state, setState] = useState<FormState>({
    tache: null,
    fichiersJoints: [],
    takeChargeFiles: [],
    collegues: [],
    declarant: null,
    origineAssocie: null,
  });
  const [takeChargeInput, setTakeChargeInput] = useState({
    id: '',
    date: '',
    description: '',
    files,
  });

  const { fichiersJoints } = state;
  const { graphql, notify } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const { idliaison, from }: any = useParams();
  const loadingInformationLiaison = useInformation_LiaisonQuery({
    client: graphql,
    fetchPolicy: 'network-only',
    variables: { id: idliaison },
    skip: !idliaison,
    onCompleted: (data) => {
      const informationLiaison = data.informationLiaison;
      const input: InformationLiaisonInput = {
        id: informationLiaison?.id,
        idType: informationLiaison?.idType || '',
        idItem: informationLiaison?.idItem || '',
        idItemAssocie: informationLiaison?.idItemAssocie,
        idOrigine: informationLiaison?.idOrigine,
        idOrigineAssocie: informationLiaison?.idOrigineAssocie,
        statut: informationLiaison?.statut || '',
        titre: informationLiaison?.titre || '',
        description: informationLiaison?.description || '',
        idDeclarant: informationLiaison?.declarant?.id || '',
        idColleguesConcernees: (informationLiaison?.colleguesConcernees || []).map(
          (collegueConcernee) => collegueConcernee.userConcernee.id
        ),
        bloquant: informationLiaison?.bloquant,
        idFonction: informationLiaison?.idFonction,
        idTache: informationLiaison?.idTache,
        idUrgence: informationLiaison?.urgence?.id || '',
        idImportance: informationLiaison?.importance?.id || '',
        fichiers: (informationLiaison?.fichiersJoints || []).map((fichierJoint) => ({
          type: fichierJoint.fichier.type as TypeFichier,
          nomOriginal: fichierJoint.fichier.nomOriginal,
          chemin: fichierJoint.fichier.chemin,
        })),
      };
      setInput(input);
      setState({
        declarant: informationLiaison?.declarant ? [informationLiaison?.declarant] : [],
        collegues: (informationLiaison?.colleguesConcernees || []).map(
          (collegueConcernee) => collegueConcernee.userConcernee
        ),
        tache: informationLiaison?.idFonction || informationLiaison?.idType,
        fichiersJoints: (informationLiaison?.fichiersJoints || [])?.map(
          (fichierJoint) => ({ name: fichierJoint.fichier.nomOriginal, type: fichierJoint.fichier.type } as File)
        ),
        origineAssocie:
          (informationLiaison?.origineAssocie as any)?.type === 'user'
            ? [informationLiaison?.origineAssocie]
            : informationLiaison?.origineAssocie,
        takeChargeFiles: (informationLiaison?.prisEnCharge?.fichiersJoints || []).map(
          (fichierJoint) => ({ name: fichierJoint.fichier.nomOriginal, type: fichierJoint.fichier.type } as File)
        ),
      });
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  const informationLiaison = loadingInformationLiaison.data?.informationLiaison;

  const [upsertCahierLiaison, upsertingCahierLiaison] = useCreate_Update_Information_LiaisonMutation({
    client: graphql,
    variables: {
      input,
    },
    onCompleted: (data) => {
      if (data && data.createUpdateInformationLiaison) {
        notify({
          type: 'success',
          message:
            (input.isRemoved && 'Suppression effectuée avec succès') ||
            `${input.id ? 'Modification' : 'Création'} réussie`,
        });
        refetch && refetch();
        if (queryVariables) {
          const prevData = graphql.readQuery({
            query: Search_Custom_Content_Information_LiaisonDocument,
            variables: queryVariables,
          });
          if (prevData && !input.id) {
            graphql.writeQuery({
              query: Search_Custom_Content_Information_LiaisonDocument,
              variables: queryVariables,
              data: {
                search: {
                  ...prevData.search,
                  ...{
                    data: [data.createUpdateInformationLiaison, ...prevData.search.data],
                    total: prevData.search.total + 1,
                  },
                },
              },
            });
          } else {
            graphql.writeQuery({
              query: Search_Custom_Content_Information_LiaisonDocument,
              variables: queryVariables,
              data: {
                search: {
                  ...prevData.search,
                  ...{
                    data: prevData.search.data.map((el: any) =>
                      el.id === input.id ? data.createUpdateInformationLiaison : el
                    ),
                    total: prevData.search.total,
                  },
                },
              },
            });
            prevData.search.data.map((el: any) => (el.id === input.id ? data.createUpdateInformationLiaison : el));
          }
        }
        push('/intelligence-collective/cahier-de-liaison/recue');
      }
    },
    onError: (errors) => {
      console.log(errors);
      if (errors.graphQLErrors) {
        errors.graphQLErrors.map((err) => {
          notify({ type: 'error', message: err.message });
        });
      }
    },
  });

  const [takeChargeInformationLiaison, takingChargeInformationLiaison] = useTake_Charge_Information_LiaisonMutation({
    client: graphql,
    variables: {
      input: takeChargeInput,
    },
    update: (cache, { data }) => {
      if (data && data.takeChargeInformationLiaison) {
      }
    },
    onCompleted: (data) => {
      if (data && data.takeChargeInformationLiaison) {
        notify({
          type: 'success',
          message: 'Prise en charge réussie',
        });
      }
    },
    onError: (errors) => {
      console.log(errors);
      if (errors.graphQLErrors) {
        errors.graphQLErrors.map((err) => {
          notify({ type: 'error', message: err.message });
        });
      }
    },
  });

  const createUpdateCahierLiaison = () => {
    if (fichiersJoints && fichiersJoints.length > 0) {
      uploadFiles(fichiersJoints, {
        directory: 'cahier-liaison',
      })
        .then((uploadedFiles) => {
          upsertCahierLiaison({
            variables: { input: { ...input, fichiers: [...uploadedFiles, ...(input.fichiers || [])] } },
          });
        })
        .catch((error) => {
          notify({ type: 'error', message: error.message });
        });
    } else {
      upsertCahierLiaison();
    }
  };

  const prendreEnCharge = () => {
    if (fichiersJoints && fichiersJoints.length > 0) {
      uploadFiles(fichiersJoints, {
        directory: 'cahier-liaison',
      })
        .then((uploadedFiles) => {
          upsertCahierLiaison({
            variables: { input: { ...input, fichiers: [...uploadedFiles, ...(input.fichiers || [])] } },
          });
        })
        .catch((error) => {
          notify({ type: 'error', message: error.message });
        });
    } else {
      takeChargeInformationLiaison();
    }
  };

  const formChange = (event: any) => {
    if (event.target) {
      const { value, name, checked } = event.target;
      // if (name === 'statutId' && value === 'EN_CHARGE')
      if (name === 'statut' && value === 'CLOTURE') {
        //setopenModal(!openModal);
      } else if (name === 'bloquant') {
        setInput((prevState: any) => ({ ...prevState, [name]: checked }));
      } else {
        setInput((prevState: any) => ({ ...prevState, [name]: value }));
      }
    }
  };

  return {
    informationLiaison,
    createUpdateCahierLiaison,
    formChange,
    prendreEnCharge,
    loading:
      loadingInformationLiaison.loading ||
      uploadingFiles.loading ||
      upsertingCahierLiaison.loading ||
      takingChargeInformationLiaison.loading,
    input,
    setInput,
    state,
    setState,
    takeChargeInput,
    setTakeChargeInput,
  };
};

export default useCahierLiaisonForm;
