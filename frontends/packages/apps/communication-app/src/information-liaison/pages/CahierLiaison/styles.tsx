import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    expandBtnContainer: {
      marginRight: 0,
    },
    contentMain: {
      overflowY: 'auto',
      // maxHeight: 'calc(100vh - 156px)',
    },
    subToolbarLeft: {
      '& .MuiTypography-root': {
        textAlign: 'left',
      },
    },
    contentMainTable: {
      '& td.MuiTableCell-root': {
        overflow: 'hidden',
        '-webkit-line-clamp': 1,
        '-webkit-box-orient': 'vertical',
        maxWidth: 300,
        whiteSpace: 'nowrap',
        textOverflow: 'ellipsis',
      },
      width: 'calc(100vw - 340px)',
      [theme.breakpoints.down('md')]: {
        width: '100vw',
      },
    },
    toolbarTitle: {
      fontSize: 20,
      marginLeft: 16,
      fontWeight: 'bold',
    },
    marginTop64: {
      [theme.breakpoints.down('md')]: {
        // marginTop: 64,
      },
    },
    nbrProduits: {
      padding: '2px 6px',
      fontSize: 10,
      background: '#E0E0E0',
      borderRadius: 2,
      height: 20,
      minWidth: 20,
      textAlign: 'center',
      marginRight: 10,
    },
    noStyle: {
      listStyleType: 'none',
      '& .MuiListItem-root': {
        padding: '4px 0',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      },
      '& .MuiRadio-root': {
        padding: '0 8px 0 0!important',
      },
      '& .main-MuiButtonBase-root, & .MuiButtonBase-root': {
        paddingLeft: '0px',
      },
    },
    nopaddingLeft: {
      '& .main-MuiListItem-button, & .MuiListItem-button': {
        padding: '4px 0px',
        '& .main-MuiCheckbox-root, & .MuiCheckbox-root': {
          marginRight: 8,
          padding: 0,
        },
      },
      '&>.main-MuiButtonBase-root, &>.MuiButtonBase-root': {
        paddingRight: '0px',
        justifyContent: 'space-between',
      },
    },
    checkBoxLeftName: {
      color: '#000',
      '& .MuiCheckbox-root, & .main-MuiCheckbox-root': {
        padding: 0,
        color: '#000000',
        marginRight: 8,
      },
      '&$checked': {
        color: '#000000',
      },
    },
    title: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 20,
    },
    childrenRoot: {
      display: 'flex',
      '& > button:not(:nth-last-child(1))': {
        marginRight: 15,
      },
    },
    container: {
      width: '100%',
    },
    containerToolbar: {
      // [theme.breakpoints.down('md')]: {
      //  marginTop: 64,
      // },
    },
    fontSize14: {
      fontSize: 16,
      fontFamily: 'Roboto',
      fontWeight: 600,
    },
    mutationSuccessContainer: {
      width: '100%',
      minHeight: 'calc(100vh - 156px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '& img': {
        width: 350,
        height: 270,
      },

      '& p': {
        textAlign: 'center',
        maxWidth: 600,
      },
      '& > div > p:nth-child(2)': {
        fontSize: 30,
        fontWeight: 'bold',
      },
      '& > div > p:nth-child(3)': {
        fontSize: 16,
        fontWeight: 'normal',
      },
      '& button': {
        marginTop: 30,
      },
    },
    sidebarTitleContainer: {
      display: 'flex',
      alignItems: 'center',
    },
    sidebarTitle: {
      fontSize: 20,
      fontWeight: 'bold',
      fontFamily: 'Montserrat',
      color: theme.palette.secondary.main,
    },
    filterText: {
      fontWeight: 'bold',
      fontSize: '1.1rem',
      marginLeft: 20,
    },
    iconAction: {
      padding: theme.spacing(0.5),
      margin: theme.spacing(0, 0.65, 0, 0),
    },
    createButton: {
      position: 'absolute',
      bottom: 75,
      right: 16,
      background: theme.palette.primary.main,
      borderRadius: '50%',
      '& .main-MuiIconButton-root': {
        color: theme.palette.common.white,
      },
      '& svg': {
        fill: '#fff',
      },
    },
  })
);

export default useStyles;
