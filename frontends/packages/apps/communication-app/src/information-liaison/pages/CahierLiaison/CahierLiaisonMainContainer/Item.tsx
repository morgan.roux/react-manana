import {
  CAHIER_LIAISON_URL,
  isMobile,
  strippedString,
  UrgenceImportanceBadge,
  useApplicationContext,
  useDisplayNotification,
} from '@lib/common';
import { useGet_OriginesQuery, useInformation_Liaison_TypesQuery } from '@lib/common/src/federation';
import {
  FichierInput,
  Search_Custom_Content_Information_LiaisonDocument,
  useDelete_Soft_Information_LiaisonsMutation,
  useUpdate_Information_Liaison_StatusMutation,
  useUpdate_Information_Liaison_Status_By_User_ConcernedMutation,
} from '@lib/common/src/graphql';
import { Backdrop, ConfirmDeleteDialog, CustomAvatar } from '@lib/ui-kit';
import {
  Box,
  CssBaseline,
  Fade,
  Hidden,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Typography,
} from '@material-ui/core';
import {
  ChatBubble,
  Close,
  Delete,
  Edit,
  ErrorOutline,
  Event,
  MoreHoriz,
  Update,
  Visibility,
  VisibilityOff,
} from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import React, { FC } from 'react';
import { useHistory } from 'react-router';
import useStyles from '../../leftContainerItemStyles';

interface ItemProps {
  item: any;
  activeItem: any;
  refetch: any;
  handleClickItem: (item: any, event: any) => void;
  queryVariables?: any;
}

const Item: FC<ItemProps> = ({ item, activeItem, refetch, handleClickItem, queryVariables }) => {
  const classes = useStyles();
  const { push } = useHistory();

  const isActive = item && activeItem && item.id === activeItem.id;

  const { user: currentUser, graphql, federation } = useApplicationContext();

  const displayNotification = useDisplayNotification();

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const [openDeleteDialog, setOpenDeleteDialog] = React.useState<boolean>(false);

  const currentUserId = currentUser && currentUser.id;
  const currentUserName = currentUser && currentUser.userName;

  const EDIT_URL = `${CAHIER_LIAISON_URL}/edit/${item && item.id}`;

  const [deleteInformationLiaisons, { loading }] = useDelete_Soft_Information_LiaisonsMutation({
    client: graphql,
    variables: { ids: [item.id] },
    onCompleted: (data) => {
      if (data && data.softDeleteInformationLiaisons && data.softDeleteInformationLiaisons.length > 0) {
        if (refetch) refetch();
        if (queryVariables) {
          const prevData = graphql.readQuery({
            query: Search_Custom_Content_Information_LiaisonDocument,
            variables: queryVariables,
          });
          if (prevData) {
            graphql.writeQuery({
              query: Search_Custom_Content_Information_LiaisonDocument,
              variables: queryVariables,
              data: {
                search: {
                  ...prevData.search,
                  ...{
                    data: prevData.search.data.filter((el: any) => el.id !== item.id),
                    total: prevData.search.total - 1,
                  },
                },
              },
            });
          }
        }
        setOpenDeleteDialog(false);
        displayNotification({
          type: 'success',
          message: 'Suppression effectuée avec succès',
        });
        push(CAHIER_LIAISON_URL);
      }
    },
    onError: (error) => {
      setOpenDeleteDialog(false);
      if (error && error.graphQLErrors && error.graphQLErrors.length > 0) {
        error.graphQLErrors.map((err) => {
          displayNotification({ type: 'error', message: err.message });
        });
      }
    },
  });

  const [doUpdateStatus, { loading: updateStatusLoading }] = useUpdate_Information_Liaison_StatusMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.updateInformationLiaisonStatut) {
        if (refetch) refetch();
        if (queryVariables) {
          const prevData = graphql.readQuery({
            query: Search_Custom_Content_Information_LiaisonDocument,
            variables: queryVariables,
          });
          if (prevData) {
            graphql.writeQuery({
              query: Search_Custom_Content_Information_LiaisonDocument,
              variables: queryVariables,
              data: {
                search: {
                  ...prevData.search,
                  ...{
                    data: prevData.search.data.map((el: any) =>
                      el.id === item.id ? data.updateInformationLiaisonStatut : el
                    ),
                    total: prevData.search.total,
                  },
                },
              },
            });
          }
        }
        displayNotification({
          type: 'success',
          message: 'Statut mis à jour avec succès',
        });
        handleClickItem(data.updateInformationLiaisonStatut.statut === 'NON_LUES' ? null : item, null);
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const [doUpdateStatusByUserConcerned] = useUpdate_Information_Liaison_Status_By_User_ConcernedMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.updateInformationLiaisonStatutByUserConcerned) {
        if (refetch) refetch();
        if (queryVariables) {
          const prevData = graphql.readQuery({
            query: Search_Custom_Content_Information_LiaisonDocument,
            variables: queryVariables,
          });
          if (prevData) {
            graphql.writeQuery({
              query: Search_Custom_Content_Information_LiaisonDocument,
              variables: queryVariables,
              data: {
                search: {
                  ...prevData.search,
                  ...{
                    data: prevData.search.data.map((el: any) =>
                      el.id === item.id ? data.updateInformationLiaisonStatutByUserConcerned.informationLiaison : el
                    ),
                    total: prevData.search.total,
                  },
                },
              },
            });
          }
        }
        displayNotification({
          type: 'success',
          message: 'Statut mis à jour avec succès',
        });
        handleClickItem(data.updateInformationLiaisonStatutByUserConcerned.statut === 'NON_LUES' ? null : item, null);
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const informationLiaisonTypesQuery = useInformation_Liaison_TypesQuery({
    client: federation,
  });

  const originesQuery = useGet_OriginesQuery({
    client: federation,
  });

  const informationLiaisonTypes = informationLiaisonTypesQuery.data?.informationLiaisonTypes.nodes || [];
  const origines = originesQuery.data?.origines.nodes || [];

  const colleguesConcerneesIds: string[] =
    (item &&
      item.colleguesConcernees &&
      item.colleguesConcernees.length > 0 &&
      item.colleguesConcernees.map((i: any) => i.userConcernee.id)) ||
    [];

  const userCreationId = item && item.userCreation && item.userCreation.id;

  const isCollegueConcerned: boolean = colleguesConcerneesIds.includes(currentUserId);

  const collegueConcernee =
    item &&
    item.colleguesConcernees &&
    item.colleguesConcernees.length > 0 &&
    item.colleguesConcernees.find((i: any) => i.userConcernee.id === currentUserId);

  const activeMenu = (): boolean => {
    if (item && currentUserId) {
      if ((item.userCreation && item.userCreation.id === currentUserId) || isCollegueConcerned) {
        return true;
      }
    }
    return false;
  };

  const activeEditAndDeleteMenu = (): boolean => {
    if (userCreationId === currentUserId) return true;
    return false;
  };

  const handleClickMenu = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClickEdit = (event: React.MouseEvent<HTMLLIElement, MouseEvent>) => {
    event.stopPropagation();
    handleClose();
    push(EDIT_URL);
  };

  const handleClickDelete = (event: React.MouseEvent<HTMLLIElement, MouseEvent>) => {
    event.stopPropagation();
    handleClose();
    setOpenDeleteDialog(true);
  };

  const handleClickUpdateStatus = (statut: string) => (event: React.MouseEvent<HTMLLIElement, MouseEvent>) => {
    event.stopPropagation();
    handleClose();
    if (isCollegueConcerned && collegueConcernee) {
      doUpdateStatusByUserConcerned({
        variables: { id: collegueConcernee.id, statut },
      });
    } else {
      doUpdateStatus({ variables: { id: item.id, statut } });
    }
  };

  const onClickConfirmDelete = () => {
    deleteInformationLiaisons();
  };

  const DeleteDialogContent = () => {
    return <span>Êtes-vous sur de vouloir supprimer ?</span>;
  };

  const menuItem = (
    <Menu
      id="fade-menu"
      anchorEl={anchorEl}
      keepMounted={true}
      open={open}
      onClose={handleClose}
      TransitionComponent={Fade}
      // tslint:disable-next-line: jsx-no-lambda
      onClick={(event) => {
        event.stopPropagation();
      }}
    >
      <MenuItem onClick={handleClickEdit} disabled={!activeEditAndDeleteMenu()}>
        <ListItemIcon>
          <Edit fontSize="small" />
        </ListItemIcon>
        <Typography variant="inherit">Modifier</Typography>
      </MenuItem>
      <MenuItem onClick={handleClickDelete} disabled={!activeEditAndDeleteMenu()}>
        <ListItemIcon>
          <Delete fontSize="small" />
        </ListItemIcon>
        <Typography variant="inherit">Supprimer</Typography>
      </MenuItem>
      {((isCollegueConcerned && collegueConcernee && collegueConcernee.statut === 'NON_LUES') ||
        (currentUserId === userCreationId && item.statut === 'NON_LUES')) && (
        <MenuItem onClick={handleClickUpdateStatus('LUES')}>
          <ListItemIcon>
            <Update fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit">Changer le statut à Lue</Typography>
        </MenuItem>
      )}
      {((isCollegueConcerned && collegueConcernee && collegueConcernee.statut === 'LUES') ||
        (currentUserId === userCreationId && item.statut === 'LUES')) && (
        <MenuItem onClick={handleClickUpdateStatus('NON_LUES')}>
          <ListItemIcon>
            <ErrorOutline fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit">Changer le statut à Non Lue</Typography>
        </MenuItem>
      )}
    </Menu>
  );

  return (
    <>
      <CssBaseline />
      <Hidden mdUp={true} implementation="css">
        <Box className={classes.mobileRoot} onClick={(event) => handleClickItem(item, event)}>
          <Box padding="16px" borderBottom="1.5px solid #E1E1E1">
            <Box display="flex" justifyContent="space-between">
              <Typography className={classes.title} color="primary">
                {(item?.idOrigine && origines.find(({ id }) => id === item.idOrigine)?.libelle) || '-'}
              </Typography>
              <Box display="flex" alignItems="center">
                <Typography className={classnames(classes.dateFromNow)}>
                  {(item && item.dateCreation && moment(item.dateCreation).fromNow()) || '-'}
                </Typography>
                <IconButton
                  size="small"
                  aria-controls="fade-menu"
                  aria-haspopup="true"
                  onClick={handleClickMenu}
                  disabled={!activeMenu()}
                >
                  <MoreHoriz />
                </IconButton>
                {menuItem}
              </Box>
            </Box>
            <Box>
              <Typography>
                {item.titre?.length > 0
                  ? item.titre
                  : item.description.length > 50
                  ? `${strippedString(item.description.substring(0, 50))}...`
                  : strippedString(item.description)}
              </Typography>
            </Box>
            <Box className={classes.spaceBetween} marginTop="16px">
              <Box display="flex" flexDirection="column">
                <Typography className={classes.mobileLabel}>Date :</Typography>
                <Typography className={classes.mobileLabelContent}>
                  {(item && item.dateCreation && moment(item.dateCreation).format('DD/MM/YYYY')) || '-'}
                </Typography>
              </Box>
              <Box>
                {item?.importance && item?.urgence && (
                  <UrgenceImportanceBadge importanceOrdre={item.importance.ordre} urgenceCode={item.urgence.code} />
                )}
              </Box>
            </Box>
          </Box>
          <Box padding="16px">
            <Box className={classes.spaceBetween}>
              <Box display="flex" flexDirection="column">
                <Typography className={classes.mobileLabel}>Déclarant :</Typography>
                <CustomAvatar
                  name={
                    item && item.declarant && item.declarant.id === currentUserId
                      ? currentUserName
                      : item && item.declarant && item.declarant.userName
                  }
                />
              </Box>
              <Box display="flex" flexDirection="column">
                <Typography className={classes.mobileLabel}>Type :</Typography>
                <Typography className={classes.mobileLabelContent}>
                  {(item?.idType && informationLiaisonTypes.find(({ id }) => id === item.idType)?.libelle) || '-'}
                </Typography>
              </Box>
            </Box>
          </Box>
        </Box>
      </Hidden>
      <Hidden smDown={true} implementation="css">
        <Box
          className={isActive ? classnames(classes.root, classes.isActive) : classes.root}
          // tslint:disable-next-line: jsx-no-lambda
          onClick={(event) => handleClickItem(item, event)}
        >
          {loading && <Backdrop value="Suppression en cours..." />}
          {updateStatusLoading && <Backdrop value="Changement de statut en cours..." />}

          <Box className={classnames(classes.flexRow)}>
            <Typography className={classes.title} color="primary">
              {(item?.idOrigine && origines.find(({ id }) => id === item.idOrigine)?.libelle) || '-'}
            </Typography>
            <Box display="flex" alignItems="center">
              <Typography className={classnames(classes.dateFromNow)}>
                {(item && item.dateCreation && moment(item.dateCreation).fromNow()) || '-'}
              </Typography>
              <IconButton
                size="small"
                aria-controls="fade-menu"
                aria-haspopup="true"
                onClick={handleClickMenu}
                disabled={!activeMenu()}
              >
                <MoreHoriz />
              </IconButton>
            </Box>

            {menuItem}
          </Box>

          <Box
            justifyContent="space-between"
            marginBottom="8px"
            marginTop="8px"
            className={classnames(classes.flexRow, classes.titleContainer)}
          >
            <Typography className={classes.subTitle}>
              {item.titre.length > 0
                ? item.titre
                : item.description.length > 50
                ? `${strippedString(item.description.substring(0, 50))}...`
                : strippedString(item.description)}
            </Typography>
            <Box display="flex" alignItems="center">
              <Event />
              <Typography className={classes.date}>
                {(item && item.dateCreation && moment(item.dateCreation).format('DD/MM/YYYY')) || '-'}
              </Typography>
            </Box>
          </Box>

          <Box display="flex" justifyContent="space-between">
            <Box>
              <Box display="flex" alignItems="center">
                <Typography className={classnames(classes.labelContent, classes.widthLabel69)}>Déclarant :</Typography>
                <Typography className={classnames(classes.labelContentValue /*, classes.lenghtValueName*/)}>
                  {item && item.declarant && item.declarant.id === currentUserId
                    ? 'Moi'
                    : (item && item.declarant && item.declarant.userName) || '-'}
                </Typography>
              </Box>
              <Box display="flex" alignItems="center">
                <Typography className={classes.labelContent}>Type:</Typography>
                <Typography className={classes.labelContentValue}>
                  {(item?.idType && informationLiaisonTypes.find(({ id }) => id === item.idType)?.libelle) || '-'}
                </Typography>
              </Box>
            </Box>
            <Box>
              {item?.importance && item?.urgence && (
                <UrgenceImportanceBadge importanceOrdre={item.importance.ordre} urgenceCode={item.urgence.code} />
              )}
            </Box>
          </Box>
          <Box display="flex" justifyContent="space-between" marginTop="16px">
            <Box display="flex">
              <ChatBubble />
              <Typography>{item.nbComment}</Typography>
            </Box>
            <Box>
              {item.nbCollegue > 0 && (
                <Box display="flex">
                  {isCollegueConcerned && collegueConcernee && collegueConcernee.statut === 'NON_LUES' ? (
                    <VisibilityOff />
                  ) : (
                    <Visibility />
                  )}
                  <Typography>{`${item.nbLue}/${item.nbCollegue}`}</Typography>
                </Box>
              )}
            </Box>
            <Box>
              {item.statut === 'CLOTURE' && (
                <Box display="flex">
                  <Close />
                  <Typography>Cloturé</Typography>
                </Box>
              )}
            </Box>
          </Box>
        </Box>
      </Hidden>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </>
  );
};

export default Item;
