import { CAHIER_LIAISON_URL } from '@lib/common';
import { CustomButton, DebouncedSearchInput, NoItemListImage } from '@lib/ui-kit';
import { Box, CssBaseline, Hidden } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import classnames from 'classnames';
import { uniqBy } from 'lodash';
import React, { FC, useState } from 'react';
import { useHistory } from 'react-router';
import useStyles from '../../leftContainerStyles';
import Item from './Item';

export interface LeftContainerProps {
  data: any | undefined;
  activeItem: any;
  fetchMore: any;
  refetch: any;
  queryVariables: any;
  handleClickItem: (item: any, event: any) => void;
  handleSearchTxt: (arg0: any) => void;
}

const LeftContainer: FC<LeftContainerProps> = ({
  data,
  activeItem,
  fetchMore,
  refetch,
  queryVariables,
  handleClickItem,
  handleSearchTxt,
}) => {
  const classes = useStyles();
  const { push } = useHistory();

  const list = (data && data.search && data.search.data) || [];
  const total = (data && data.search && data.search.total) || 0;

  const [searchTxt, setSearchTxt] = useState('');

  const handleSearch = (value: any) => {
    setSearchTxt(value);
    handleSearchTxt(value);
  };

  const handleClickAdd = () => {
    push(`${CAHIER_LIAISON_URL}/create`);
  };

  const realList = uniqBy([...list], 'id');

  if (realList.length > 0 && realList.length < total) {
    const el = document.getElementById('leftContainerId-451');
    if (el && realList.length > 0 && realList.length < total) {
      el.addEventListener('scroll', () => {
        if (el.scrollHeight - el.scrollTop === el.clientHeight) {
          if (fetchMore) {
            fetchMore({
              variables: { ...queryVariables, skip: realList.length },
              updateQuery: (prev: any, { fetchMoreResult }: any) => {
                if (!fetchMoreResult) return prev;
                const prevData = (prev.search && prev.search.data) || [];
                const nextData = (fetchMoreResult && fetchMoreResult.search && fetchMoreResult.search.data) || [];
                const newData = [...prevData, ...nextData];

                return { ...prev, search: { ...prev.search, data: newData } } as any;
              },
            });
          }
        }
      });
    }
  }

  return (
    <Box className={classes.leftContainer}>
      <CssBaseline />
      <Hidden smDown={true} implementation="css">
        <Box
          className={classnames(classes.mainContainerHeader, classes.leftContainerHeader)}
          justifyContent="center !important"
        >
          <CustomButton color="secondary" startIcon={<Add />} onClick={handleClickAdd}>
            Nouvelle information
          </CustomButton>
        </Box>
      </Hidden>
      <Hidden mdUp={true} implementation="css">
        <Box width="100vw" display="flex" justifyContent="center">
          <DebouncedSearchInput
            placeholder={'Rechercher ...'}
            wait={500}
            outlined
            onChange={handleSearch}
            value={searchTxt}
          />
        </Box>
      </Hidden>

      <Box className={classes.itemsContainer} id="leftContainerId-451">
        {realList.length > 0 ? (
          realList.map((item, index) => {
            return (
              <Item
                key={(item as any)?.id || index}
                item={item}
                activeItem={activeItem}
                handleClickItem={handleClickItem}
                refetch={refetch}
                queryVariables={queryVariables}
              />
            );
          })
        ) : (
          <Box minHeight="calc(100vh - 162px)" width="100%" display="flex" alignItems="center" justifyContent="center">
            <NoItemListImage
              title="Aucune information dans la liste"
              subtitle="Crées en une en cliquant sur le bouton d'en haut."
            />
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default LeftContainer;
