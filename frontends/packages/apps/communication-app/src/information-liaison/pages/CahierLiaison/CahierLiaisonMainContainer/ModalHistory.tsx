import { isMobile } from '@lib/common';
import { InformationLiaisonInfoFragment } from '@lib/common/src/graphql';
import { CustomModal } from '@lib/ui-kit';
import { Box, makeStyles, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import moment from 'moment';
import React, { Dispatch, SetStateAction } from 'react';

export interface ModalHistoryProps {
  info: InformationLiaisonInfoFragment;
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
}

const useStyles = makeStyles({
  table: {
    // minWidth: 650,
  },
});

const ModalHistory: React.FC<ModalHistoryProps> = ({ info, open, setOpen }) => {
  const classes = useStyles();

  const data = info.colleguesConcernees || [];

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Historique"
      closeIcon={true}
      withBtnsActions={false}
      headerWithBgColor={true}
      fullWidth={true}
      maxWidth="md"
      fullScreen={isMobile() ? true : false}
    >
      <Box width="100%">
        <Table className={classes.table} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell align="left">Collègue(s)</TableCell>
              <TableCell align="left">Lecture</TableCell>
              <TableCell align="left">Cloture</TableCell>
              <TableCell align="left">Commentaire</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((d) => (
              <TableRow key={d.id}>
                <TableCell align="left">{d.userConcernee.userName || '-'}</TableCell>
                <TableCell align="left">
                  {d.statut === 'LUES'
                    ? `Oui ${d.dateStatutModification ? moment(d.dateStatutModification).format('DD/MM/YYYY') : ''}`
                    : d.statut === 'CLOTURE'
                    ? 'Oui'
                    : 'Non'}
                </TableCell>
                <TableCell align="left">
                  {d.statut === 'CLOTURE'
                    ? `Oui ${d.dateStatutModification ? moment(d.dateStatutModification).format('DD/MM/YYYY') : ''}`
                    : 'Non'}
                </TableCell>
                <TableCell align="left">{d.prisEnCharge?.description || '-'}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Box>
    </CustomModal>
  );
};

export default ModalHistory;
