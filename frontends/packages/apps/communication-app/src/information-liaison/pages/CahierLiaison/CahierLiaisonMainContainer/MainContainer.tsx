import {
  AttachedFiles,
  CAHIER_LIAISON_URL,
  isMobile,
  MobileTopBar,
  nl2br,
  RightContentComment,
  useApplicationContext,
  useUploadFiles,
} from '@lib/common';
import AvatarInput from '@lib/common/src/components/AvatarInput';
import {
  useFonctionQuery,
  useGet_OriginesQuery,
  useInformation_Liaison_TypesQuery,
  useTacheQuery,
} from '@lib/common/src/federation';
import {
  FichierInput,
  InformationLiaisonInfoFragment,
  Search_Custom_Content_Information_LiaisonDocument,
  TakeChargeInformationLiaisonInput,
  useUpdate_Information_Liaison_StatusMutation,
  useUpdate_Information_Liaison_Status_By_User_ConcernedMutation,
} from '@lib/common/src/graphql';
import { Backdrop, CustomButton, NoItemContentImage } from '@lib/ui-kit';
import { Box, CssBaseline, Hidden, IconButton, Menu, MenuItem, Typography } from '@material-ui/core';
import { Close, History, MoreVert } from '@material-ui/icons';
import { AxiosResponse } from 'axios';
import classnames from 'classnames';
import { uniqBy } from 'lodash';
import moment from 'moment';
import React, { FC, Fragment, useState } from 'react';
import { useParams } from 'react-router-dom';
import useCommonStyles from '../../leftContainerItemStyles';
import useStyles from '../../mainContainerStyles';
import DetailsInfoPrisEnCharge from '../DetailsInformation/DetailsInfoPrisEnCharge';
import PriseEnCharge from '../DetailsInformation/Modals/PriseEnCharge';
import useTakeChargeInformationLiaison from '../utils/useTakeChargeInformationLiaison';
import ColleguesConcernees from './ColleguesConcernees';
import ModalHistory from './ModalHistory';
import useOthersStyles from './styles';
export interface MainContainerProps {
  activeItem: any;
  refetch: any;
  handleDrawerToggle?: () => void;
  queryVariables?: any;
}

const MainContainer: FC<MainContainerProps> = ({ activeItem, refetch, handleDrawerToggle, queryVariables }) => {
  const classes = useStyles();
  const othersClasses = useOthersStyles();
  const commonClasses = useCommonStyles();

  const { user: currentUser, graphql, federation, notify } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const currentUserId = currentUser.id;

  const { idliaison }: any = useParams();

  const [realActiveItem, setRealActiveItem] = React.useState<InformationLiaisonInfoFragment | null>(activeItem);

  const [openModalHistory, setOpenModalHistory] = React.useState(false);

  const [openModalTakeCharge, setOpenModalTakeCharge] = React.useState(false);
  const [selectedFiles, setSelectedFiles] = React.useState<File[]>([]);
  const [loading, setLoading] = React.useState(false);

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const open = anchorEl ? true : false;

  let fichiersInputs: FichierInput[] = [];
  let uploadResult: AxiosResponse<any> | null = null;

  const [stateTakeCharge, setStateTakeCharge] = React.useState({
    date: new Date(),
    description: '',
    files: fichiersInputs,
  });

  const informationLiaisonTypesQuery = useInformation_Liaison_TypesQuery({
    client: federation,
  });

  const originesQuery = useGet_OriginesQuery({
    client: federation,
  });

  const informationLiaisonTypes = informationLiaisonTypesQuery.data?.informationLiaisonTypes.nodes || [];
  const origines = originesQuery.data?.origines.nodes || [];

  const takeCharge: TakeChargeInformationLiaisonInput = {
    id: (realActiveItem && realActiveItem.id) || '',
    date: stateTakeCharge.date,
    description: stateTakeCharge.description,
    files: stateTakeCharge.files,
  };

  const collegueConcerneeIds: string[] =
    (realActiveItem &&
      realActiveItem.colleguesConcernees &&
      realActiveItem.colleguesConcernees.length > 0 &&
      realActiveItem.colleguesConcernees.map((i) => i.userConcernee.id)) ||
    [];

  const collegueConcerneeNames: string[] =
    (realActiveItem &&
      realActiveItem.colleguesConcernees &&
      realActiveItem.colleguesConcernees.length > 0 &&
      realActiveItem.colleguesConcernees.map((i) => i.userConcernee.userName || '')) ||
    [];

  const isCollegueConcerned: boolean = collegueConcerneeIds.includes(currentUserId);

  const collegueConcernee = realActiveItem?.colleguesConcernees?.find((i) => i.userConcernee.id === currentUserId);

  const declarantId = realActiveItem && realActiveItem.declarant && realActiveItem.declarant.id;
  const declarantName = realActiveItem && realActiveItem.declarant && realActiveItem.declarant.userName;

  const fichiersJoints: any[] =
    (realActiveItem &&
      realActiveItem.fichiersJoints &&
      realActiveItem.fichiersJoints.length > 0 &&
      realActiveItem.fichiersJoints.map((fichierJoint: any) => fichierJoint.fichier)) ||
    [];

  const userCreationId = realActiveItem && realActiveItem.userCreation && realActiveItem.userCreation.id;

  const type = realActiveItem?.idType && informationLiaisonTypes.find((el) => el.id === realActiveItem.idType)?.libelle;

  const urgence = realActiveItem?.urgence ? `${realActiveItem.urgence.code}:${realActiveItem.urgence.libelle}` : '';
  const importance = realActiveItem?.importance?.libelle || '';

  const origine = realActiveItem?.idOrigine ? origines.find(({ id }) => id === realActiveItem?.idOrigine)?.libelle : '';
  const correspondant = realActiveItem?.origineAssocie
    ? (realActiveItem.origineAssocie as any).userName ||
      (realActiveItem.origineAssocie as any).nomLabo ||
      (realActiveItem.origineAssocie as any).nom
    : '';

  const dateCreation =
    realActiveItem && realActiveItem.dateCreation && moment(realActiveItem.dateCreation).format('DD/MM/YYYY');

  const tacheQuery = useTacheQuery({
    client: federation,
    variables: { id: realActiveItem?.idTache || '' },
    skip: !realActiveItem?.idTache,
  });
  const fonctionQuery = useFonctionQuery({
    client: federation,
    variables: { id: realActiveItem?.idFonction || '' },
    skip: !realActiveItem?.idFonction,
  });

  const tache = tacheQuery.data?.dQMTTache;
  const fonction = fonctionQuery.data?.dQMTFonction;

  const todoAction = realActiveItem && realActiveItem.todoAction ? 'Oui' : 'Non';

  const MORE_INFOS = isMobile()
    ? [
        { id: 3, label: 'Type', value: type },
        //  { id: 4, label: 'Origine', value: origine },
        { id: 4, label: 'Origine', value: origine },
        { id: 16, label: 'Urgence', value: urgence },
        { id: 17, label: 'Importance', value: importance },
        { id: 10, label: 'Date de création', value: dateCreation },
        { id: 12, label: 'Tâche', value: tache?.libelle || '-' },
        { id: 13, label: 'Fonction', value: fonction?.libelle || '-' },
        { id: 15, label: 'Action todo', value: todoAction },
      ]
    : [
        {
          id: 1,
          label: 'Déclarant',
          value: declarantId === currentUserId ? 'Moi' : declarantName,
        },
        {
          id: 2,
          label: 'Collègue(s) Concernée(s)',
          value: (collegueConcerneeNames.length > 0 && collegueConcerneeNames.join(', ')) || '-',
        },
        { id: 3, label: 'Type', value: type },
        { id: 4, label: 'Origine', value: origine },
        { id: 4, label: 'Correspondant', value: correspondant },
        { id: 16, label: 'Urgence', value: urgence },
        { id: 17, label: 'Importance', value: importance },
        { id: 10, label: 'Date de création', value: dateCreation },
        { id: 12, label: 'Tâche', value: tache?.libelle || '-' },
        { id: 13, label: 'Fonction', value: fonction?.libelle || '-' },
        { id: 15, label: 'Action todo', value: todoAction },
      ];

  const { takeChargeInformationLiaison, mutationTakeChargeSuccess, mutationLoading, mutationData } =
    useTakeChargeInformationLiaison(takeCharge, refetch);

  const [doUpdateStatus, { loading: updateStatusLoading }] = useUpdate_Information_Liaison_StatusMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.updateInformationLiaisonStatut) {
        if (refetch) refetch();
        if (queryVariables) {
          const prevData = graphql.readQuery({
            query: Search_Custom_Content_Information_LiaisonDocument,
            variables: queryVariables,
          });
          if (prevData) {
            graphql.writeQuery({
              query: Search_Custom_Content_Information_LiaisonDocument,
              variables: queryVariables,
              data: {
                search: {
                  ...prevData.search,
                  ...{
                    data: prevData.search.data.map((el: any) =>
                      el.id === activeItem?.id ? data.updateInformationLiaisonStatut : el
                    ),
                    total: prevData.search.total,
                  },
                },
              },
            });
          }
        }
        setRealActiveItem(data.updateInformationLiaisonStatut);
        notify({
          type: 'success',
          message: 'Information clôturée avec succès',
        });
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  const [doUpdateStatusByUserConcerned] = useUpdate_Information_Liaison_Status_By_User_ConcernedMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.updateInformationLiaisonStatutByUserConcerned) {
        if (refetch) refetch();
        if (queryVariables) {
          const prevData = graphql.readQuery({
            query: Search_Custom_Content_Information_LiaisonDocument,
            variables: queryVariables,
          });
          if (prevData) {
            graphql.writeQuery({
              query: Search_Custom_Content_Information_LiaisonDocument,
              variables: queryVariables,
              data: {
                search: {
                  ...prevData.search,
                  ...{
                    data: prevData.search.data.map((el: any) =>
                      el.id === activeItem?.id
                        ? data.updateInformationLiaisonStatutByUserConcerned.informationLiaison
                        : el
                    ),
                    total: prevData.search.total,
                  },
                },
              },
            });
          }
        }

        if (collegueConcernee) {
          collegueConcernee.statut = data.updateInformationLiaisonStatutByUserConcerned.statut;
        }
        setRealActiveItem(data.updateInformationLiaisonStatutByUserConcerned.informationLiaison);
        notify({
          type: 'success',
          message: 'Information clôturée avec succès',
        });
      }
    },
    onError: (error) => {
      if (error && error.graphQLErrors) {
        error.graphQLErrors.map((err) => {
          notify({ type: 'error', message: err.message });
        });
      }
    },
  });

  const handleClickPrendreCharge = () => {
    if (isCollegueConcerned && collegueConcernee?.id) {
      doUpdateStatusByUserConcerned({
        variables: { id: collegueConcernee.id, statut: 'CLOTURE' },
      });
    } else {
      setAnchorEl(null);
      setOpenModalTakeCharge(true);
    }
  };

  const handleClickAddTakeCharge = () => {
    setLoading(true);
    if (selectedFiles && selectedFiles.length > 0) {
      uploadFiles(selectedFiles, {
        directory: 'cahier-liaison',
      })
        .then((uploadedFiles) => {
          fichiersInputs = uniqBy(
            [...fichiersInputs, ...uploadedFiles.map((uploadedFile) => ({ ...uploadedFile, id: '' }))],
            'chemin'
          );

          setStateTakeCharge((prevState) => ({
            ...prevState,
            files: fichiersInputs,
          }));

          takeChargeInformationLiaison();
          setLoading(false);
        })
        .catch((error) => {
          setLoading(false);
          notify({
            type: 'error',
            message: error.message,
          });
        });
    } else {
      takeChargeInformationLiaison();
    }
  };

  const handleClickConfirmCloturer = () => {
    if (isCollegueConcerned && collegueConcernee) {
      doUpdateStatusByUserConcerned({
        variables: { id: collegueConcernee.id, statut: 'CLOTURE' },
      });
    } else {
      doUpdateStatus({
        variables: {
          id: (realActiveItem && realActiveItem.id) || '',
          statut: 'CLOTURE',
        },
      });
    }
  };

  const handleClickHistory = () => {
    setAnchorEl(null);
    setOpenModalHistory(true);
  };

  React.useEffect(() => {
    setRealActiveItem(activeItem);
  }, [activeItem]);

  React.useMemo(() => {
    if (mutationTakeChargeSuccess) {
      setLoading(false);
      if (refetch) refetch();
    }
  }, [mutationTakeChargeSuccess]);

  React.useEffect(() => {
    if (mutationData && mutationData.takeChargeInformationLiaison) {
      setRealActiveItem(mutationData.takeChargeInformationLiaison);
      setLoading(false);
    }
  }, [mutationData]);

  const disabledBtnCloturer = (): boolean => {
    if (currentUserId && realActiveItem) {
      // For collegue concerned
      if (isCollegueConcerned && collegueConcernee) {
        if (collegueConcernee.statut === 'CLOTURE') return true;
      }

      // For user creation
      if (userCreationId === currentUserId) {
        if (realActiveItem.statut === 'CLOTURE') return true;
      }
    }
    return false;
  };

  const handleMoreOptionClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    }
  };

  const optionBtn = (
    <>
      <IconButton color="inherit" onClick={handleMoreOptionClick}>
        <MoreVert />
      </IconButton>
      <Menu id="fade-menu" anchorEl={anchorEl} keepMounted={true} open={open}>
        <MenuItem onClick={handleClickHistory}>
          <Typography variant="inherit">Historique</Typography>
        </MenuItem>
        <MenuItem disabled={disabledBtnCloturer()} onClick={handleClickPrendreCharge}>
          <Typography variant="inherit">Clôturer</Typography>
        </MenuItem>
      </Menu>
    </>
  );

  if (window.location.hash === `#${CAHIER_LIAISON_URL}` && realActiveItem) {
    setRealActiveItem(null);
  }

  return (
    <Box className={classes.mainContainer} key={activeItem?.id}>
      {(loading || uploadingFiles.loading || mutationLoading || updateStatusLoading) && (
        <Backdrop value="Opération en cours..." />
      )}

      <Box className={classes.mainContent}>
        <CssBaseline />
        <Hidden mdUp={true} implementation="css">
          <MobileTopBar
            title={`${origine || ''}${realActiveItem?.bloquant ? ` - Bloquante` : ''}`}
            withBackBtn={true}
            handleDrawerToggle={handleDrawerToggle}
            optionBtn={[optionBtn]}
            withTopMargin={false}
            onClickBack={handleDrawerToggle}
          />
        </Hidden>

        {realActiveItem ? (
          <Box className={classes.detailsContainer}>
            <Hidden mdUp={true} implementation="css">
              <Typography
                className={classes.labelContent}
                dangerouslySetInnerHTML={{ __html: nl2br(realActiveItem && realActiveItem.description) } as any}
              />
            </Hidden>
            <Hidden smDown={true} implementation="css">
              <Box display="flex" flexDirection="row" marginBottom="15px">
                <CustomButton color="secondary" startIcon={<History />} onClick={handleClickHistory}>
                  Historique
                </CustomButton>

                <CustomButton
                  color="secondary"
                  startIcon={<Close />}
                  style={{ marginLeft: 10 }}
                  disabled={disabledBtnCloturer()}
                  onClick={handleClickPrendreCharge}
                >
                  Clôturer
                </CustomButton>
              </Box>

              <Box display="flex" justifyContent="space-between" alignItems="center">
                {origine && (
                  <Typography className={classes.title} color="primary">
                    {`${origine || ''}${realActiveItem?.bloquant ? ` - Bloquante` : ''}`}
                  </Typography>
                )}
              </Box>
            </Hidden>

            <Hidden smDown={true} implementation="css">
              <Box className={othersClasses.infoSupContainer}>
                {MORE_INFOS.filter((info) => info.value && info.value !== '-').map((info, index) => {
                  return (
                    <Box key={index} display="flex" alignItems="center">
                      <Typography
                        className={classnames(commonClasses.labelContent)}
                        style={{ minWidth: 'fit-content' }}
                      >
                        {info.label} :
                      </Typography>
                      {info.id === 2 ? (
                        <ColleguesConcernees info={realActiveItem} />
                      ) : (
                        <Typography
                          className={classnames(commonClasses.labelContentValue, othersClasses.infoValue)}
                          style={{ cursor: 'auto' }}
                        >
                          {info.value || '-'}
                        </Typography>
                      )}
                    </Box>
                  );
                })}
              </Box>
            </Hidden>

            <Hidden lgUp={true} implementation="css">
              <Box className={classes.mobileInfoSupContainer}>
                <Box display="flex" justifyContent="space-between" marginBottom="16px">
                  <Typography className={classes.mobileInfoSupLabel}>Déclarant</Typography>
                  <AvatarInput
                    list={(realActiveItem && realActiveItem.declarant && [realActiveItem.declarant]) || []}
                    small={true}
                    standard={true}
                  />
                </Box>
                <Box display="flex" justifyContent="space-between" marginBottom="16px">
                  <Typography className={classes.mobileInfoSupLabel}>Collègue(s) Concernée(s)</Typography>

                  <Typography className={classes.mobileInfoSupValue}>
                    <AvatarInput
                      list={
                        (realActiveItem &&
                          realActiveItem.colleguesConcernees &&
                          realActiveItem.colleguesConcernees.map((collegue) => collegue && collegue.userConcernee)) ||
                        []
                      }
                      small={true}
                      standard={true}
                    />
                  </Typography>
                </Box>
                {MORE_INFOS.filter((info) => info.value && info.value !== '-').map((info, index) => {
                  return (
                    <Box display="flex" justifyContent="space-between" marginBottom="16px">
                      <Typography className={classes.mobileInfoSupLabel}>{info.label}</Typography>

                      <Typography className={classes.mobileInfoSupValue}>{info.value}</Typography>
                    </Box>
                  );
                })}
              </Box>
            </Hidden>

            <Hidden smDown={true} implementation="css">
              <Box display="flex" flexDirection="column">
                {realActiveItem.description && (
                  <Fragment>
                    <Typography className={classes.labelTitle}>Description</Typography>
                    <Typography
                      className={classes.labelContent}
                      dangerouslySetInnerHTML={{ __html: nl2br(realActiveItem.description) } as any}
                    />
                  </Fragment>
                )}
              </Box>
            </Hidden>

            {realActiveItem && realActiveItem.prisEnCharge && <DetailsInfoPrisEnCharge information={realActiveItem} />}

            {fichiersJoints.length > 0 && (
              <Box width="100%" marginTop="10px">
                <AttachedFiles files={fichiersJoints} />
              </Box>
            )}
            <Box width="100%" marginTop="10px">
              <RightContentComment
                listResult={[]}
                codeItem="INFORMATION_LIAISON"
                item={realActiveItem as any}
                refetch={() => refetch(false)}
              />
            </Box>
          </Box>
        ) : (
          <Box minHeight="calc(100vh - 162px)" width="100%" display="flex" alignItems="center" justifyContent="center">
            <NoItemContentImage
              title="Aperçu détaillé de vos informations"
              subtitle="Choisissez une information dans la liste de droite pour l'afficher en détails dans cette partie."
            />
          </Box>
        )}
      </Box>

      <PriseEnCharge
        open={openModalTakeCharge}
        setOpen={setOpenModalTakeCharge}
        setSelectedFiles={setSelectedFiles}
        selectedFiles={selectedFiles}
        stateTakeCharge={stateTakeCharge}
        setStateTakeCharge={setStateTakeCharge}
        // tslint:disable-next-line: jsx-no-lambda
        handleAddClickTakeCharge={() => {
          handleClickAddTakeCharge();
          handleClickConfirmCloturer();
        }}
      />

      {realActiveItem && <ModalHistory info={realActiveItem} open={openModalHistory} setOpen={setOpenModalHistory} />}
    </Box>
  );
};

export default MainContainer;
