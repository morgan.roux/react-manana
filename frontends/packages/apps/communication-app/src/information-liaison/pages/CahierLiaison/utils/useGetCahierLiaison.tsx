import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { InformationLiaisonInfoFragment, useInformation_LiaisonLazyQuery } from '@lib/common/src/graphql';

/**
 *  get information liaison hooks
 */
const useGetCahierLiaison = (
  id: string,
  fetchPolicy?: 'cache-first' | 'network-only' | 'cache-only' | 'no-cache' | 'standby' | 'cache-and-network' | undefined
) => {
  const { graphql } = useApplicationContext();
  const displayNotification = useDisplayNotification();

  const [getCahierLiaison, { data, loading }] = useInformation_LiaisonLazyQuery({
    client: graphql,
    fetchPolicy,
    variables: { id },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        displayNotification({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  const informationLiaison: InformationLiaisonInfoFragment | null = (data && data.informationLiaison) || null;

  return { getCahierLiaison, informationLiaison, loading };
};

export default useGetCahierLiaison;
