import { formatBytes } from '@lib/common';
import { Box, Divider, Paper, TextField, Typography } from '@material-ui/core';
import { PictureAsPdf } from '@material-ui/icons';
import moment from 'moment';
import React, { FC } from 'react';
import useCommonStyles from '../../fromMessageStyles';
import useStyles from './styles';

interface DetailsInfoPrisEnChargeProps {
  information: any;
}

const DetailsInfoPrisEnCharge: FC<DetailsInfoPrisEnChargeProps> = ({ information }) => {
  const classes = useStyles();
  const commonClasses = useCommonStyles();

  const descriptionPrisEnCharge =
    (information && information.prisEnCharge && information.prisEnCharge.description) || '-';
  const datePrisEnCharge = (information && information.prisEnCharge && information.prisEnCharge.date) || '-';

  const fichiersJoints: any[] =
    (information &&
      information.prisEnCharge &&
      information.prisEnCharge.fichiersJoints &&
      information.prisEnCharge.fichiersJoints.length > 0 &&
      information.prisEnCharge.fichiersJoints) ||
    [];

  const InfoTitle: FC<{ label: string }> = ({ label }) => {
    return <Typography className={classes.infoTitle}>{label}</Typography>;
  };

  const InfoTitleContent: FC<{ label: string }> = ({ label }) => {
    return <Typography className={classes.infoTitleContent}>{label}</Typography>;
  };

  const InfoRow: FC<{ label: string; value: string }> = ({ label, value }) => {
    return (
      <Box className={classes.infoRowContainer}>
        <Typography className={classes.infoRowLabel}>{label} : </Typography>
        <Typography className={classes.infoRowValue}>{value}</Typography>
      </Box>
    );
  };

  const openFile = (url: string) => {
    window.open(url, '_blank');
  };

  return (
    <Box width="100%">
      <Paper
        className={classes.infoContainerRightPaper}
        style={{ boxShadow: 'none', maxWidth: '100%', padding: 0, marginBottom: 0, marginTop: 20 }}
      >
        <InfoTitle label="Prise en charge" />
        <Divider />
        <InfoRow label="Date de prise en charge " value={moment(datePrisEnCharge).format('DD/MM/YYYY')} />
        {descriptionPrisEnCharge !== '' && (
          <TextField
            id="outlined-textarea"
            label="Détails"
            placeholder="Placeholder"
            multiline={true}
            variant="outlined"
            rowsMax={8}
            className={classes.infoCommentsContainerDetail}
            value={descriptionPrisEnCharge}
          />
        )}

        {fichiersJoints.length > 0 && (
          <Box className={classes.filesContainer} marginTop="20px">
            <InfoTitleContent label="Fichier(s) joint(s) :" />
            <Box display="flex" flexWrap="wrap">
              {fichiersJoints.map((file, index) => (
                <Box
                  key={`${file.fichier.nomOriginal}_${index}`}
                  className={commonClasses.fileItem}
                  // tslint:disable-next-line: jsx-no-lambda
                  onClick={() => openFile(file.fichier.publicUrl)}
                  style={{ cursor: 'pointer' }}
                >
                  <PictureAsPdf />
                  <Box className={commonClasses.filenameContainer}>
                    <Typography className={classes.nomFile}>{file.fichier.nomOriginal}</Typography>
                    {file.fichier.size && <Typography>{formatBytes(file.fichier.size, 1)}</Typography>}
                  </Box>
                </Box>
              ))}
            </Box>
          </Box>
        )}
      </Paper>
    </Box>
  );
};

export default DetailsInfoPrisEnCharge;
