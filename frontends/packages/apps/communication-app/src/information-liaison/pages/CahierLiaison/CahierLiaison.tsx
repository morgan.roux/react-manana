import { LeftSidebarAndMainPage, useApplicationContext } from '@lib/common';
import { ImportanceInterface } from '@lib/common/src/components/ImportanceFilter/ImportanceFilterList';
import { UrgenceInterface } from '@lib/common/src/components/UrgenceFilter/UrgenceFilterList';
import { useGet_UrgencesQuery, useImportancesQuery } from '@lib/common/src/federation';
import { useCount_Information_LiaisonsQuery } from '@lib/common/src/graphql';
import { Box, IconButton } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router';
import FilterAlt from '../../assets/icon/filter_alt_white.svg';
import CahierLiaisonFilter from './CahierLiaisonFilter';
import CahierLiaisonMain from './CahierLiaisonMain';
import useStyles from './styles';

const CahierLiaison: FC = ({}) => {
  const classes = useStyles({});
  const { push } = useHistory();
  const { pathname } = useLocation();
  const { graphql, user: currentUser, currentPharmacie: pharmacie, federation } = useApplicationContext();

  const isOnEmise = pathname.includes('/emise');

  const informationsList = {
    emise: 'coupleStatutDeclarant',
    recue: 'colleguesConcernees.coupleStatutUserConcernee',
  };

  const [informations, setInformations] = useState<string>(isOnEmise ? informationsList.emise : informationsList.recue);

  const statusInitialState = informations === informationsList.recue ? ['NON_LUES'] : ['EN_COURS'];

  const [searchTxt, setSearchTxt] = useState<any>('');
  const [statuts, setStatuts] = useState<any>(statusInitialState);
  const [startDate, setStartDate] = useState<any>('');
  const [endDate, setEndDate] = useState<any>('');
  const [state, setState] = useState<any>({
    concernedColleagueId: 'all',
    concernedColleagueName: 'all',
    concernedColleaguesIds: [],
  });

  const isInConcernedParticipant = false;

  const [ckeckedMyInfo, setCkeckedMyInfo] = useState<boolean>(false);

  const handleBack = () => {
    push('/');
  };

  const handleClickMyInfos = React.useCallback((value: boolean) => {
    setCkeckedMyInfo(!value);
  }, []);

  const [openDrawer, setOpenDrawer] = React.useState(false);

  const handleDrawerToggle = () => {
    setOpenDrawer(!openDrawer);
  };

  const handleShowFiltersClick = () => {
    setOpenDrawer(!openDrawer);
  };

  const [urgence, setUrgence] = useState<UrgenceInterface[] | null | undefined>();
  const [importance, setImportance] = useState<ImportanceInterface[] | null | undefined>();

  const loadingImportances = useImportancesQuery({ client: federation });
  const loadingUrgences = useGet_UrgencesQuery({ client: federation });

  const importancesData = loadingImportances.data?.importances.nodes || [];
  const urgencesData = loadingUrgences.data?.urgences.nodes || [];

  useEffect(() => {
    if (!loadingUrgences.loading && urgencesData) {
      setUrgence((urgencesData || []) as any);
    }
  }, [loadingUrgences.loading, urgencesData]);

  useEffect(() => {
    if (!loadingImportances.loading && importancesData) {
      setImportance((importancesData || []) as any);
    }
  }, [loadingImportances.loading, importancesData]);

  const idImportances = (importance || []).filter((etiquette) => etiquette.checked).map(({ id }) => id) || [];
  const idUrgences = (urgence || []).filter((urgence) => urgence.checked).map(({ id }) => id) || [];

  const filters = {
    idImportances,
    idUrgences,
  };

  // useEffect(() => {
  //   setStatuts(statusInitialState);
  // }, [informations]);

  const optionBtn = (
    <IconButton
      className={classes.iconAction}
      color="inherit"
      aria-label="settings"
      edge="start"
      onClick={handleShowFiltersClick}
    >
      <img src={FilterAlt} />
    </IconButton>
  );

  const countInformationLiaisonsQuery = useCount_Information_LiaisonsQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
    variables: {
      input: {
        idPharmacie: pharmacie?.id,
        dateDebut: startDate !== '' ? startDate : undefined,
        dateFin: endDate !== '' ? endDate : undefined,
        idsCollegueConcernee: informations === informationsList.recue ? state.concernedColleaguesIds : undefined,
        idsDeclarant: informations === informationsList.emise ? state.concernedColleaguesIds : undefined,
      },
    },
  });

  const countInformationLiaisons = countInformationLiaisonsQuery.data?.countInformationLiaisons;

  return (
    <Box className={classes.root}>
      <LeftSidebarAndMainPage
        drawerTitle="Cahier de liaison"
        drawerBackUrl="/"
        sidebarChildren={
          <CahierLiaisonFilter
            currentUser={currentUser}
            handleSearchTxt={setSearchTxt}
            handleStatus={setStatuts}
            handleEndDate={setEndDate}
            handleStartDate={setStartDate}
            state={state}
            isInConcernedParticipant={isInConcernedParticipant}
            setState={setState}
            handleBack={handleBack}
            handleClickMyInfos={handleClickMyInfos}
            informations={informations}
            setInformations={setInformations}
            urgence={urgence}
            setUrgence={setUrgence}
            etiquette={importance}
            setEtiquette={setImportance}
            countInformationLiaisons={countInformationLiaisons}
          />
        }
        mainChildren={
          <CahierLiaisonMain
            filters={filters}
            searchTxt={searchTxt}
            statuts={statuts}
            startDate={startDate}
            endDate={endDate}
            stateCollegeConcernees={state}
            ckeckedMyInfo={ckeckedMyInfo}
            handleSearchTxt={setSearchTxt}
            informations={informations}
            refetchCount={countInformationLiaisonsQuery.refetch}
          />
        }
        optionBtn={[optionBtn]}
        handleDrawerToggle={handleDrawerToggle}
        openDrawer={openDrawer}
        setOpenDrawer={setOpenDrawer}
      />
    </Box>
  );
};

export default CahierLiaison;
