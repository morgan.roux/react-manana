import { MeUserInfoFragment, GroupementInfoFragment } from '@lib/common/src/federation';
import {
  Search_Information_LiaisonDocument,
  Search_Information_LiaisonQuery,
  Search_Information_LiaisonQueryVariables,
} from '@lib/common/src/graphql';
import { ApolloClient } from '@apollo/client';

export const fetchInformationLiaisonTotalCount = async (context: {
  user: MeUserInfoFragment;
  graphql: ApolloClient<any>;
  currentGroupement: GroupementInfoFragment;
}): Promise<number> => {
  const response = await context.graphql.query<
    Search_Information_LiaisonQuery,
    Search_Information_LiaisonQueryVariables
  >({
    query: Search_Information_LiaisonDocument,
    fetchPolicy: 'network-only',
    variables: {
      type: ['informationliaison'],
      query: {
        query: {
          bool: {
            must: [
              { term: { isRemoved: false } },
              { term: { 'groupement.id': context.currentGroupement.id } },
              //{ term: { bloquant: true } },
              //{ term: { priority: 3 } }, // Bloquant
              {
                terms: {
                  'colleguesConcernees.coupleStatutUserConcernee': [`${context.user.id}-NON_LUES`],
                },
              },
              ,
              //{ terms: { 'colleguesConcernees.lu': [false] } },
              {
                script: {
                  script: {
                    source: "doc['colleguesConcernees.userConcernee.id'].length >= 1",
                    lang: 'painless',
                  },
                },
              },
            ],
          },
        },
      },
      sortBy: [{ dateCreation: { order: 'desc' } }],
    },
  });
  return response.data.search?.total ?? 0;
};
