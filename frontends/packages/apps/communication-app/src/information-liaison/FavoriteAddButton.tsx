import { Add } from '@material-ui/icons';
import React from 'react';
import { useHistory } from 'react-router';

export const FavoriteAddButton = () => {
  const history = useHistory();

  const handleClick = (event: any) => {
    event.preventDefault();
    event.stopPropagation();
    history.push('/intelligence-collective/cahier-de-liaison/create');
  };

  return <Add onClick={handleClick} />;
};
