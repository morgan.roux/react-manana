import React from 'react';
import { SmallLoading } from '@lib/ui-kit';
import loadable from '@loadable/component';
import CahierLiaison from './assets/img/cahier_liaison.png';
import { ModuleDefinition } from '@lib/common';
import { fetchInformationLiaisonTotalCount } from './util/total-count';
import { InfoRounded } from '@material-ui/icons';
import SyncIcon from '@material-ui/icons/Sync';
import { FavoriteAddButton } from './FavoriteAddButton';
import InformationLiaisonAlert from './alert/InformationLiaisonAlert';

const CahierLiaisonPage = loadable(() => import('./pages/CahierLiaison/CahierLiaison'), {
  fallback: <SmallLoading />,
});

const informationLiaisonModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_CAHIER_LIAISON',
      location: 'ACCUEIL',
      name: 'Cahier de liaison',
      to: '/intelligence-collective/cahier-de-liaison/recue',
      icon: CahierLiaison,
      activationParameters: {
        groupement: '0312',
        pharmacie: '0715',
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
      preferredOrder: 150,
      options: {
        totalCount: fetchInformationLiaisonTotalCount,
      },
    },

    {
      id: 'FEATURE_FAVORITE_CAHIER_LIAISON',
      location: 'FAVORITE',
      to: '/intelligence-collective/cahier-de-liaison/recue',
      name: 'Cahier de liaison', // Pharmaceutique
      icon: <SyncIcon />,
      component: <FavoriteAddButton />,

      preferredOrder: 30,
      activationParameters: {
        groupement: '0312',
        pharmacie: '0715',
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_MOBILE_QUICK_ACCESS_INFORMATION_LIAISON',
      location: 'MOBILE_QUICK_ACCESS',
      name: 'Liaison',
      icon: <InfoRounded />,
      to: '/intelligence-collective/cahier-de-liaison',
      options: {
        active: (path) => path.startsWith('/intelligence-collective/cahier-de-liaison'),
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
      preferredOrder: 40,
    },

    {
      id: 'FEATURE_ALERT_INFORMATION_LIAISON',
      location: 'ALERT',
      preferredOrder: 20,
      options: {
        openAlert: (options, onClose) => {
          return <InformationLiaisonAlert open={true} setOpen={() => onClose()} />;
        },
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
  routes: [
    {
      path: [
        '/intelligence-collective/cahier-de-liaison',
        '/intelligence-collective/cahier-de-liaison/create',
        '/intelligence-collective/cahier-de-liaison/edit/:idliaison',
        '/intelligence-collective/cahier-de-liaison/recue/:idliaison?/:from?',
        '/intelligence-collective/cahier-de-liaison/emise/:idliaison?/:from?',
      ],
      component: CahierLiaisonPage,
      exact: true,
      attachTo: 'MAIN',
    },
  ],
};

export default informationLiaisonModule;
