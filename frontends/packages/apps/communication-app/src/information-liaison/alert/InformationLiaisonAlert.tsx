import { CircularProgress, Typography } from '@material-ui/core';
import { ArrowForward } from '@material-ui/icons';
import { Pagination } from '@material-ui/lab';
import { differenceBy } from 'lodash';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useSearch_Information_LiaisonQuery } from '@lib/common/src/graphql';
import { useGet_OriginesQuery } from '@lib/common/src/federation';

import useStyles from './styles';
import { nl2br, trimmedString, useApplicationContext } from '@lib/common';
import { NewCustomButton as CustomButton, CustomModal } from '@lib/ui-kit';

export interface PopupInfoLiaisonProps {
  open: boolean;
  setOpen: (open: boolean) => void;
}

const PopupInfoLiaison: FC<PopupInfoLiaisonProps & RouteComponentProps> = ({ open, setOpen, history: { push } }) => {
  const classes = useStyles({});
  const { currentGroupement, user, notify, graphql, federation } = useApplicationContext();
  const userId = user.id;

  const infoFromStorage = localStorage.getItem('infosBloquantes');

  let infosBloquantes: any[] = [];
  if (infoFromStorage) {
    infosBloquantes = JSON.parse(infoFromStorage);
  }

  const [info, setInfo] = React.useState<any>(null);
  const [filteredData, setFilteredData] = React.useState<any[]>([]);
  const [page, setPage] = React.useState<number>(0);

  const originesResult = useGet_OriginesQuery({
    client: federation,
  });
  const { data, loading } = useSearch_Information_LiaisonQuery({
    client: graphql,
    variables: {
      type: ['informationliaison'],
      query: {
        query: {
          bool: {
            must: [
              { term: { isRemoved: false } },
              { term: { 'groupement.id': currentGroupement.id } },
              { term: { bloquant: true } },
              {
                terms: { 'colleguesConcernees.coupleStatutUserConcernee': [`${userId}-NON_LUES`] },
              },
              {
                script: {
                  script: {
                    source: "doc['colleguesConcernees.userConcernee.id'].length >= 1",
                    lang: 'painless',
                  },
                },
              },
            ],
          },
        },
      },
      sortBy: [{ dateCreation: { order: 'desc' } }],
    },
    fetchPolicy: 'cache-and-network',
    onError: (error) => {
      setOpen(false);
    },
  });

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value - 1);
  };

  const goToInfo = () => {
    const infos = (data && data.search && data.search.data) || [];
    localStorage.setItem('infosBloquantes', JSON.stringify(infos));
    push(`/intelligence-collective/cahier-de-liaison/recue/${info && info.id}`);
  };

  React.useEffect(() => {
    if (data?.search?.data?.length) {
      if (infosBloquantes && infosBloquantes.length > 0) {
        const diff = differenceBy(data.search.data, infosBloquantes, 'id');
        if (diff && diff.length > 0 && diff[page]) {
          setFilteredData(diff);
          setInfo(diff[page]);
        } else {
          setOpen(false);
        }
      } else if (data.search.data[page]) {
        setFilteredData(data.search.data);
        setInfo(data.search.data[page]);
      }
    } else if (!loading) {
      setOpen(false);
    }
  }, [data, page, loading]);

  if (loading) {
    return null;
  }

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={`Vous avez ${filteredData.length} information(s) bloquante(s)`}
      closeIcon={true}
      withBtnsActions={false}
      headerWithBgColor={true}
      fullWidth={true}
      maxWidth="md"
    >
      <div className={classes.popupActuRoot}>
        <div className={classes.actuInfoContainer}>
          <Typography className={classes.origine}>
            Origine :{' '}
            <span>
              {info && info.idOrigine
                ? (originesResult.data?.origines.nodes || []).find(({ id }) => id === info.idOrigine)?.libelle || '-'
                : '-'}
            </span>
            {loading && <CircularProgress size={20} />}
          </Typography>
          <Typography className={classes.description}>{info?.titre}</Typography>
          <CustomButton
            className={classes.btn}
            color="secondary"
            variant="outlined"
            endIcon={<ArrowForward />}
            onClick={goToInfo}
          >
            Consulter
          </CustomButton>
        </div>
        <Pagination
          count={filteredData.length}
          variant="outlined"
          color="primary"
          onChange={handleChange}
          showFirstButton={true}
          showLastButton={true}
        />
      </div>
    </CustomModal>
  );
};

export default withRouter(PopupInfoLiaison);
