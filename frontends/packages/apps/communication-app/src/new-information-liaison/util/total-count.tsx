import { MeUserInfoFragment, GroupementInfoFragment } from '@lib/common/src/federation';
import {
  Search_Information_LiaisonDocument,
  Search_Information_LiaisonQuery,
  Search_Information_LiaisonQueryVariables,
} from '@lib/common/src/federation';
import { ApolloClient } from '@apollo/client';

export const fetchInformationLiaisonTotalCount = async (context: {
  user: MeUserInfoFragment;
  federation: ApolloClient<any>;
  currentGroupement: GroupementInfoFragment;
}): Promise<number> => {
  const response = await context.federation.query<
    Search_Information_LiaisonQuery,
    Search_Information_LiaisonQueryVariables
  >({
    query: Search_Information_LiaisonDocument,
    fetchPolicy: 'network-only',
    variables: {
      index: 'informationliaisons',
      query: {
        query: {
          bool: {
            must: [
              { term: { supprime: false } },
              { term: { 'groupement.id': context.currentGroupement.id } },
              {
                terms: { 'idUsersByStatus.RECUE_NON_LUES': [context.user.id] },
              },
              {
                script: {
                  script: {
                    source: "doc['colleguesConcernees.userConcernee.id'].length >= 1",
                    lang: 'painless',
                  },
                },
              },
            ],
          },
        },
      },
      sortBy: [{ createdAt: { order: 'desc' } }],
    },
  });
  return response.data.search?.total ?? 0;
};
