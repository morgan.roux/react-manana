import { Filter } from './types';

export const mergeFilters = (filters: Filter[]): Filter => {
  return filters.reduce((merged, currentFilter) => {
    let must = currentFilter.must
      ? merged.must
        ? [...merged.must, ...currentFilter.must]
        : currentFilter.must
      : merged.must;

    let should = currentFilter.should
      ? merged.should
        ? [...merged.should, ...currentFilter.should]
        : currentFilter.should
      : merged.should;

    return {
      must,
      should,
    };
  }, {});
};
