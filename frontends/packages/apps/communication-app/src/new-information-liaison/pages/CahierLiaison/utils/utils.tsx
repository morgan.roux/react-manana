import { CAHIER_LIAISON_URL } from '@lib/common';

/**
 *  Subtoolbar button action hooks
 */
export const useButtonHeadAction = (push: any) => {
  const goToAddInformation = () => push(`${CAHIER_LIAISON_URL}/create`);
  const goBack = () => push(`${CAHIER_LIAISON_URL}`);
  return { goToAddInformation, goBack };
};

export const removeEmptyString = (values: Record<string, any>): any => {
  return Object.keys(values).reduce((result, key) => {
    if (typeof values[key] === 'string' && !values[key]) {
      return result;
    }
    return {
      ...result,
      [key]: values[key],
    };
  }, {});
};
