import {
  CustomFilterContent,
  CustomSelectUser,
  ImportanceFilterListByOrder,
  isMobile,
  UrgenceFilterListByCode,
  useApplicationContext,
} from '@lib/common';
import AvatarInput from '@lib/common/src/components/AvatarInput';
import { CustomFormTextField, DebouncedSearchInput } from '@lib/ui-kit';
import { Box, CssBaseline, Hidden, IconButton, ListItem, ListItemText, Radio, Typography } from '@material-ui/core';
import { ArrowBack, ExpandLess, ExpandMore } from '@material-ui/icons';
import React, { Fragment, useState } from 'react';
import { useILURLParams } from './hooks/useILURLParams';
import useStyles from './styles';

interface ICahierLiaisonFilterProps {
  refetchCount?: any;
  handleBack: () => void;
  countInformationLiaisons: any;
}

const CahierLiaisonFilter: React.FC<ICahierLiaisonFilterProps> = ({
  refetchCount,
  handleBack,
  countInformationLiaisons,
}) => {
  const classes = useStyles({});
  const { params, redirectTo } = useILURLParams();
  const { user } = useApplicationContext();

  const findStatutCount = (statut: string) => {
    return (countInformationLiaisons && countInformationLiaisons[statut]) || 0;
  };

  const statusCounts: any[] =
    params.information === 'EMISE'
      ? [findStatutCount('EMISE_EN_COURS'), findStatutCount('EMISE_CLOTURE')]
      : [findStatutCount('RECUE_NON_LUES'), findStatutCount('RECUE_LUES'), findStatutCount('RECUE_CLOTURE')];

  const statusData =
    params.information === 'RECUE'
      ? [
          {
            nom: 'Non lues',
            checked: (params.statut || []).includes('NON_LUES'),
            value: 1,
            id: 'NON_LUES',
          },
          {
            nom: 'Lues',
            checked: (params.statut || []).includes('LUES'),
            value: 2,
            id: 'LUES',
          },
          {
            nom: 'Clôturées',
            checked: (params.statut || []).includes('CLOTURE'),
            value: 3,
            id: 'CLOTURE',
          },
        ]
      : [
          {
            nom: 'En cours',
            checked: (params.statut || []).includes('EN_COURS'),
            value: 1,
            id: 'EN_COURS',
          },
          {
            nom: 'Clôturées',
            checked: (params.statut || []).includes('CLOTURE'),
            value: 2,
            id: 'CLOTURE',
          },
        ];

  const [expandedMore, setExpandedMore] = useState<boolean>(true);
  const [expandedMoreInformations, setExpandedMoreInformations] = useState<boolean>(true);
  const [expandedMoreUrgence, setExpandedMoreUrgence] = useState<boolean>(true);
  const [expandedMoreImportance, setExpandedMoreImportance] = useState<boolean>(true);
  const [openConcernedParticipantModal, setOpenConcernedParticipantModal] = useState<boolean>(false);

  const [isCheckedTeam, setIsCheckedTeam] = useState<boolean>(false);

  const handleOpenConcernedParticipant = (event: any) => {
    event.stopPropagation();
    setOpenConcernedParticipantModal(true);
  };

  const handleStatutData = (value: any) => {
    const status: any[] = [];
    value.map((v: any) => {
      if (v.checked) status.push(v.id);
    });

    if (status && status.length > 0) redirectTo({ ...params, statut: status, id: undefined });
    else redirectTo({ ...params, statut: undefined, id: undefined });
  };

  const collegueConcerneeNames = (params.collegueConcernees || [{ id: user.id, userName: user.fullName }])
    .map((el: any) => (el.id === user.id ? 'Moi' : el.userName))
    .join(', ');

  const handleInformationsClick = (informations: 'EMISE' | 'RECUE') => {
    redirectTo({
      ...params,
      information: informations,
      statut: informations === 'RECUE' ? ['NON_LUES'] : ['EN_COURS'],
      id: undefined,
    });
  };

  return (
    <Fragment>
      <CssBaseline />
      <Hidden smDown={true} implementation="css">
        <Box padding="8px 0 12px" display="flex" justifyContent="space-between" alignItems="center">
          <Box display="flex" alignItems="center">
            <IconButton size="small" onClick={handleBack}>
              <ArrowBack />
            </IconButton>
            <Typography className={classes.toolbarTitle} color="primary">
              Information Liaison
            </Typography>
          </Box>
        </Box>
        <Box display="flex" borderBottom="1px solid #E1E1E1" paddingBottom="16px">
          <DebouncedSearchInput
            wait={1000}
            value={params.searchText || ''}
            onChange={(value) => redirectTo({ ...params, searchText: value || undefined })}
            placeholder="Rechercher une information..."
            fullWidth
          />
        </Box>
      </Hidden>
      <Box>
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          // tslint:disable-next-line: jsx-no-lambda
          onClick={() => setExpandedMoreInformations(!expandedMoreInformations)}
        >
          <Typography className={classes.fontSize14}>Informations</Typography>
          <IconButton size="small">{expandedMore ? <ExpandLess /> : <ExpandMore />}</IconButton>
        </Box>
        {expandedMoreInformations && (
          <Box display="flex" justifyContent="space-between">
            <ListItem role={undefined} dense={true} button={true} onClick={() => handleInformationsClick('RECUE')}>
              <Box display="flex" alignItems="center">
                <Radio checked={params.information === 'RECUE'} tabIndex={-1} disableRipple={true} />
                <ListItemText primary="Reçue(s)" />
              </Box>
            </ListItem>
            <ListItem role={undefined} dense={true} button={true} onClick={() => handleInformationsClick('EMISE')}>
              <Box display="flex" alignItems="center">
                <Radio checked={params.information === 'EMISE'} tabIndex={-1} disableRipple={true} />
                <ListItemText primary="Émise(s)" />
              </Box>
            </ListItem>
          </Box>
        )}
      </Box>

      <Box>
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          // tslint:disable-next-line: jsx-no-lambda
          onClick={() => setExpandedMore(!expandedMore)}
        >
          <Typography className={classes.fontSize14}>Collègue(s) concerné(s)</Typography>
          <IconButton size="small">{expandedMore ? <ExpandLess /> : <ExpandMore />}</IconButton>
        </Box>
        <Box className={classes.noStyle}>
          {expandedMore &&
            (!isMobile() ? (
              <CustomFormTextField
                name="concernedParticipant"
                label=""
                value={collegueConcerneeNames}
                onClick={handleOpenConcernedParticipant}
              />
            ) : (
              <Box onClick={handleOpenConcernedParticipant}>
                <AvatarInput list={params.collegueConcernees || [{ id: user.id, userName: user.fullName }]} />
              </Box>
            ))}
        </Box>
        <CustomSelectUser
          openModal={openConcernedParticipantModal}
          setOpenModal={setOpenConcernedParticipantModal}
          withNotAssigned={false}
          selected={params.collegueConcernees || [{ id: user.id, userName: user.fullName }]}
          setSelected={(selected: any) => {
            redirectTo({
              ...params,
              collegueConcernees:
                selected.length > 0 ? selected.map((el: any) => ({ id: el.id, userName: el.userName })) : undefined,
              id: undefined,
            });
          }}
          title="Collègue(s) concerné(s)"
          searchPlaceholder="Rechercher..."
          assignTeamText="Toute l'équipe"
          withAssignTeam={true}
          singleSelect={false}
          isCheckedTeam={isCheckedTeam}
          setIsCheckedTeam={setIsCheckedTeam}
        />
      </Box>

      <CustomFilterContent
        title="Statut"
        datas={statusData}
        updateDatas={handleStatutData}
        counts={statusCounts}
        containerClassName={classes.nopaddingLeft}
      />
      <Box>
        <Box
          paddingTop={2}
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          onClick={() => setExpandedMoreUrgence(!expandedMoreUrgence)}
        >
          <Typography className={classes.fontSize14}>Urgence</Typography>
          <IconButton size="small">{expandedMoreUrgence ? <ExpandLess /> : <ExpandMore />}</IconButton>
        </Box>
        {expandedMoreUrgence && (
          <Box className={classes.noStyle}>
            <UrgenceFilterListByCode
              codes={params.urgences}
              setUrgences={(urgences: any) => {
                redirectTo({ ...params, urgences: urgences.length > 0 ? urgences : undefined, id: undefined });
              }}
            />
          </Box>
        )}
      </Box>

      <Box>
        <Box
          display="flex"
          justifyContent="space-between"
          alignItems="center"
          onClick={() => setExpandedMoreImportance(!expandedMoreImportance)}
        >
          <Typography className={classes.fontSize14}>Importance</Typography>
          <IconButton size="small">{expandedMoreImportance ? <ExpandLess /> : <ExpandMore />}</IconButton>
        </Box>
        {expandedMoreImportance && (
          <Box className={classes.noStyle}>
            <ImportanceFilterListByOrder
              ordres={(params.importances || []).map((ordre) => parseInt(ordre))}
              setImportances={(importances: any) => {
                redirectTo({ ...params, importances: importances.length > 0 ? importances : undefined, id: undefined });
              }}
            />
          </Box>
        )}
      </Box>
    </Fragment>
  );
};

export default CahierLiaisonFilter;
