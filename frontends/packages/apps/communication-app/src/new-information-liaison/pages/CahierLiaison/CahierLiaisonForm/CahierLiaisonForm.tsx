import {
  CommonFieldsForm,
  CustomSelectUser,
  Dropzone,
  isMobile,
  OrigineInput,
  useApplicationContext,
} from '@lib/common';
import { useInformation_Liaison_TypesQuery } from '@lib/common/src/federation';
import { FichierInput } from '@lib/common/src/graphql';
import { Backdrop, CustomFormTextField, CustomModal, CustomSelect, FormButtons } from '@lib/ui-kit';
import CustomCheckbox from '@lib/ui-kit/src/components/atoms/CustomCheckBox/CustomCheckbox';
import { Box, Typography } from '@material-ui/core';
import classnames from 'classnames';
import React, { FC, Fragment, useState } from 'react';
import ReactQuill from 'react-quill';
import { useHistory, useParams } from 'react-router';
import PriseEnCharge from '../DetailsInformation/Modals/PriseEnCharge';
import useStyles from './styles';
import useCahierLiaisonForm from './useCahierLiaisonForm';

export interface InputInterface {
  name: string;
  value: any;
  autocompleteValue?: any;
  label?: string;
  type?: 'select' | 'date' | 'text' | 'checkbox';
  placeholder?: string;
  inputType?: string;
  selectOptions?: any[];
  selectOptionIdKey?: string;
  selectOptionValueKey?: string;
  required?: boolean;
}

export interface PrestataireInterface {
  id: string;
  nom: string;
}

export interface FormInputInterface {
  title: string;
  inputs: InputInterface[];
}

export interface CahierLiaisonFormProps {
  refetch?: any;
}

export interface IStateForm {
  id: string;
  idType: string;
  idOrigine: string;
  idOrigineAssocie: string;
  idItem: string;
  idItemAssocie: string;
  bloquant: boolean;
  statut: string;
  titre: string;
  description: string;
  idUserDeclarant: string;
  declaringName: string;
  idColleguesConcernees: string[];
  teamAll: boolean;
  fichiers: FichierInput[];
  idImportance: string;
  idUrgence: string;
  idFonction: string;
  idTache: string;
}

const CahierLiaisonForm: FC<CahierLiaisonFormProps> = ({ refetch }) => {
  const { federation } = useApplicationContext();
  const { push } = useHistory();

  const {
    state,
    setState,
    input,
    loading,
    formChange,
    takeChargeInput,
    setTakeChargeInput,
    createUpdateCahierLiaison,
  } = useCahierLiaisonForm({ refetch });

  const { fichiersJoints, tache, collegues, declarant, origineAssocie } = state;

  const { idliaison }: any = useParams();

  const classes = useStyles({});

  const informationLiaisonTypesQuery = useInformation_Liaison_TypesQuery({ client: federation });

  const informationLiaisonTypes = informationLiaisonTypesQuery.data?.informationLiaisonTypes.nodes || [];

  const [openFormModal, setOpenFormModal] = useState<boolean>(isMobile());

  const [openDeclarantModal, setOpenDeclarantModal] = useState<boolean>(false);

  const [isCheckedTeam, setIsCheckedTeam] = useState<boolean>(false);

  const [openModal, setopenModal] = useState<boolean>(false);

  const handleAddClickTakeCharge = () => {};

  const handleOpenDeclarant = (event: any) => {
    event.stopPropagation();
    setOpenDeclarantModal(true);
  };

  const confirmBtnDisabled = !!(
    loading ||
    input.idType === '' ||
    input.idOrigine === '' ||
    input.idUserDeclarant === '' ||
    input.titre === '' ||
    input.idUrgence === '' ||
    input.idImportance === ''
  );

  const goBack = () => {
    push('/intelligence-collective/cahier-de-liaison?statut=NON_LUES&information=RECUE');
  };

  const content = (
    <div className={classes.cahierLiaisonFormRoot}>
      <Backdrop open={loading} value={'Opération en cours...'} />
      <PriseEnCharge
        open={openModal}
        setSelectedFiles={(files) => setState((prev) => ({ ...prev, takeChargeFiles: files }))}
        selectedFiles={state.takeChargeFiles}
        stateTakeCharge={takeChargeInput}
        setStateTakeCharge={setTakeChargeInput}
        setOpen={setopenModal}
        handleAddClickTakeCharge={handleAddClickTakeCharge}
      />
      <div className={classes.formContainer}>
        <Fragment key={`cahier_liaison_form_item_1`}>
          <Typography className={classes.inputTitle} color="primary">
            Information de liaison
          </Typography>

          <CustomSelect
            label="Type"
            list={informationLiaisonTypes}
            listId="id"
            index="libelle"
            name="idType"
            value={input.idType}
            onChange={formChange}
            required={true}
          />

          <OrigineInput
            origine={input.idOrigine}
            origineAssocie={origineAssocie}
            onChangeOrigine={(origine: any) =>
              formChange({
                target: {
                  value: origine?.id,
                  name: 'idOrigine',
                },
              })
            }
            onChangeOrigineAssocie={(origineAssocie: any) => {
              setState((prev) => ({ ...prev, origineAssocie }));
              formChange({
                target: {
                  value: origineAssocie.id || origineAssocie[0]?.id,
                  name: 'idOrigineAssocie',
                },
              });
            }}
          />

          <Box width="100%" marginBottom="5px" marginTop="-10px">
            <CommonFieldsForm
              style={{ padding: 0 }}
              selectedUsers={collegues}
              urgence={input.idUrgence}
              urgenceProps={{
                useCode: false,
              }}
              selectUsersFieldLabel="Collègue(s) concerné(s)"
              usersModalProps={{
                withNotAssigned: false,
                title: 'Collègue(s) concerné(s)',
                searchPlaceholder: 'Rechercher...',
                assignTeamText: "Toute l'équipe",
                withAssignTeam: true,
                singleSelect: false,
                isCheckedTeam: isCheckedTeam,
                setIsCheckedTeam: setIsCheckedTeam,
              }}
              projet={tache?.id || input.idTache || input.idFonction}
              importance={input.idImportance}
              onChangeUsersSelection={(users) => {
                setState((prev) => ({ ...prev, collegues: users }));
                formChange({ target: { name: 'idColleguesConcernees', value: users.map((user: any) => user.id) } });
              }}
              onChangeUrgence={(urgence) => formChange({ target: { name: 'idUrgence', value: urgence?.id } })}
              onChangeImportance={(importance) =>
                formChange({ target: { name: 'idImportance', value: importance?.id } })
              }
              onChangeProjet={(projet) => {
                formChange({ target: { name: 'idTache', value: projet?.idTache } });
                formChange({ target: { name: 'idFonction', value: projet?.idFonction } });
                setState((prev) => ({ ...prev, tache: projet }));
              }}
            />
          </Box>

          <CustomFormTextField name="titre" label="Titre" value={input.titre} onChange={formChange} required={true} />

          <div className={classnames(classes.marginRight, classes.w100)} style={{ marginTop: 7 }}>
            <ReactQuill
              className={classes.customizedReactQuill}
              theme="snow"
              value={input.description}
              onChange={(content) => formChange({ target: { value: content, name: 'description' } })}
            />
          </div>
          <CustomCheckbox
            name="bloquant"
            label="Bloquante"
            value={input.bloquant}
            checked={input.bloquant}
            onChange={formChange}
          />
        </Fragment>

        <Fragment key={`cahier_liaison_form_item_2`}>
          <Typography className={classes.inputTitle}>Intervenants</Typography>

          <CustomSelectUser
            openModal={openDeclarantModal}
            setOpenModal={setOpenDeclarantModal}
            withNotAssigned={false}
            selected={declarant}
            setSelected={(declarant: any) => {
              setState((prev) => ({ ...prev, declarant })),
                formChange({
                  target: {
                    value: declarant[0]?.id,
                    name: 'idUserDeclarant',
                  },
                });
            }}
            title="Déclarant"
            searchPlaceholder="Rechercher..."
            assignTeamText="Toute l'équipe"
            withAssignTeam={false}
            singleSelect={true}
            defaultValueMe={true}
          />

          <CustomFormTextField
            name="declarant"
            label="Déclarant"
            value={declarant && declarant[0] && declarant[0].fullName}
            onClick={handleOpenDeclarant}
            required={true}
          />

          <div
            className={classnames(classes.marginRight, classes.w100)}
            style={{ marginTop: input.idOrigine !== '4' && input.idOrigine !== '' ? 15 : 'auto' }}
          >
            <Dropzone
              contentText="Glissez et déposez vos documents"
              selectedFiles={fichiersJoints}
              setSelectedFiles={(files) => setState((prev) => ({ ...prev, fichiersJoints: files as File[] }))}
              multiple={true}
              // acceptFiles="application/pdf"
            />
            {!isMobile() && (
              <FormButtons
                onClickCancel={goBack}
                onClickConfirm={createUpdateCahierLiaison}
                disableConfirm={confirmBtnDisabled}
              />
            )}
          </div>
        </Fragment>
      </div>
    </div>
  );

  return !isMobile() ? (
    content
  ) : (
    <CustomModal
      title={`${idliaison ? 'Modification' : 'Ajout'} d'information`}
      open={openFormModal}
      setOpen={setOpenFormModal}
      closeIcon={true}
      headerWithBgColor={true}
      withBtnsActions={true}
      fullScreen={true}
      customHandleClose={goBack}
      maxWidth="xl"
      noDialogContent={true}
      actionButton={idliaison ? 'Modifier' : 'Créer'}
      withCancelButton={false}
      onClickConfirm={createUpdateCahierLiaison}
      disabledButton={confirmBtnDisabled}
    >
      {content}
    </CustomModal>
  );
};

export default CahierLiaisonForm;
