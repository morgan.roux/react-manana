import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { TakeChargeInformationLiaisonInput, useTake_Charge_Information_LiaisonMutation } from '@lib/common/src/graphql';
import { useState } from 'react';

/**
 *  Create take charge hooks
 */
const useTakeChargeInformationLiaison = (takeCharge: TakeChargeInformationLiaisonInput, refetch?: () => void) => {
  // const {
  //   content: { variables, operationName },
  // } = useContext<ContentStateInterface>(ContentContext);

  const { graphql } = useApplicationContext();
  const [mutationTakeChargeSuccess, setMutationTakeChargeSuccess] = useState<boolean>(false);

  const displayNotification = useDisplayNotification();

  const [
    takeChargeInformationLiaison,
    { loading: mutationLoading, data: mutationData },
  ] = useTake_Charge_Information_LiaisonMutation({
    client: graphql,
    variables: {
      input: takeCharge,
    },
    update: (cache, { data }) => {
      if (data && data.takeChargeInformationLiaison && takeCharge && !takeCharge.id) {
        // if (variables && operationName) {
        //   const req: any = cache.readQuery({
        //     query: operationName,
        //     variables,
        //   });
        //   if (req && req.search && req.search.data) {
        //     cache.writeQuery({
        //       query: operationName,
        //       data: {
        //         search: {
        //           ...req.search,
        //           ...{
        //             total: req.search.total + 1,
        //             data: [...req.search.data, data.takeChargeInformationLiaison],
        //           },
        //         },
        //       },
        //       variables,
        //     });
        //   }
        // }
      }
    },
    onCompleted: (data) => {
      if (data && data.takeChargeInformationLiaison) {
        displayNotification({
          type: 'success',
          message: 'Prise en charge réussie',
        });
        if (refetch) refetch();
        setMutationTakeChargeSuccess(true);
      }
    },
    onError: (errors) => {
      console.log(errors);
      if (errors.graphQLErrors) {
        errors.graphQLErrors.map((err) => {
          displayNotification({ type: 'error', message: err.message });
        });
      }
    },
  });

  return { takeChargeInformationLiaison, mutationTakeChargeSuccess, mutationLoading, mutationData };
};

export default useTakeChargeInformationLiaison;
