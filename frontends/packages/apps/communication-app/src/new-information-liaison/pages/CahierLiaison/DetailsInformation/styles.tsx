import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { width: '100%', display: 'flex', flexDirection: 'column' },
    rootModal: {
      '& .MuiDialogContent-root': {
        padding: '50px 82px',
        [theme.breakpoints.down('md')]: {
          padding: 16,
        },
      },
    },

    boxwidth: {
      display: 'flex',
      width: '100%',
      flexDirection: 'column',
      '& .MuiDialogActions-root': {
        [theme.breakpoints.down('md')]: {
          justifyContent: 'left',
        },
      },
    },
    infoRowContainer: {
      display: 'flex',
      marginBottom: 10,
    },
    nomFile: {
      margin: 0,
    },
    rootContainer: {
      display: 'flex',
      width: '100%',
      maxWidth: 1238,
      margin: '0 auto',
      padding: '24px',
      justifyContent: 'space-between',
      [theme.breakpoints.down('sm')]: {
        flexWrap: 'wrap',
        justifyContent: 'center',
      },
    },
    transformButton: {
      width: 200,
      height: 50,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 14,
      marginTop: 25,
      marginBottom: 35,
      marginLeft: 25,
      [theme.breakpoints.down('md')]: {
        width: 'auto',
        margin: 0,
      },
    },
    section: {
      margin: '0 0 24px 0 ',
      '& .ql-container.ql-snow': {
        minHeight: 97,
      },
      '& .quill.customized-title': {
        marginBottom: 33,
      },
      '& .MuiMenuItem-root': {
        minHeight: '40px!important',
      },

      '&.MuiListItem-root.Mui-selected': {
        minHeight: '40px !important',
        '&:hover': {
          minHeight: '40px !important',
        },
      },
    },
    sectionDatePicker: {
      margin: '0 0 32px 0 ',

      height: 50,
    },
    inputText: {
      width: '100%',
      marginBottom: 12,
      '& .MuiOutlinedInput-root': {
        '&.Mui-focused': {
          '& .MuiOutlinedInput-notchedOutline': {
            borderColor: 'rgba(0, 0, 0, 0.23)!important',
          },
        },
      },
      '& .MuiSelect-selectMenu': {
        fontSize: '0.875rem',
      },
      '& .MuiOutlinedInput-input': { fontSize: '0.875rem' },
      '& .MuiFormLabel-root': {
        color: '#878787',
        fontSize: '0.875rem',
      },
    },
    infoRowLabel: {
      fontFamily: 'Roboto',
      fontSize: 14,
      color: '#878787',
      minWidth: 141,
      marginRight: 14,
    },
    infoRowValue: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 14,
    },
    infoContainerLeft: {
      width: '100%',
      maxWidth: 537,
      minWidth: 320,
      padding: '20px 25px',
      marginBottom: 24,
      marginRight: 24,
      [theme.breakpoints.down('sm')]: {
        marginRight: 0,
      },
    },
    infoContainerRight: {
      width: '100%',
      maxWidth: 613,
      minWidth: 450,
      padding: '20px 25px',
      marginBottom: 24,
      border: '2px dashed #373435',
      borderRadius: 6,
    },
    infoContainerRightPaper: {
      width: '100%',
      maxWidth: 613,
      minWidth: 450,
      padding: '20px 25px',
      marginBottom: 24,
      borderRadius: 6,
      height: 'fit-content',
      '& .MuiDivider-root': {
        margin: '10px 0 20px',
      },
    },
    filesContainer: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      flexWrap: 'wrap',
      '& .MuiSvgIcon-root': {
        color: '#E53935',
      },
    },
    infoTitle: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 16,
      marginBottom: 12,
    },
    infoTitleContent: {
      fontFamily: 'Roboto',
      fontSize: 14,
      color: '#878787',
      // marginTop: 24,
      // marginBottom: 16,
      marginBottom: 6,
    },
    infoCommentsContainer: {
      width: '100%',
    },
    infoCommentsContainerDetail: {
      width: '100%',
      backgroundColor: '#F2F2F2',
      marginTop: 20,
    },
    smyleyContainer: {
      width: 45,
      height: 45,
      borderRadius: '50%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      right: 24,
    },
    mutationSuccessContainer: {
      width: '100%',
      minHeight: 'calc(100vh - 156px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '& img': {
        width: 350,
        height: 270,
      },

      '& p': {
        textAlign: 'center',
        maxWidth: 600,
      },
      '& > div > p:nth-child(2)': {
        fontSize: 30,
        fontWeight: 'bold',
      },
      '& > div > p:nth-child(3)': {
        fontSize: 16,
        fontWeight: 'normal',
      },
      '& button': {
        marginTop: 30,
      },
    },
  })
);

export default useStyles;
