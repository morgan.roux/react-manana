import { CAHIER_LIAISON_URL, useApplicationContext, useDisplayNotification } from '@lib/common';
import { SearchInformationLiaisonInfoFragment, useSearch_Information_LiaisonQuery } from '@lib/common/src/federation';
import { FichierInput } from '@lib/common/src/graphql';
import { CustomModal } from '@lib/ui-kit';
import { Box, Hidden, IconButton } from '@material-ui/core';
import { Create } from '@material-ui/icons';
import classnames from 'classnames';
import React, { Fragment, useState } from 'react';
import { useHistory, useLocation } from 'react-router';
import useCommonStyles from '../commonStyles';
import CahierLiaisonForm from './CahierLiaisonForm';
import useCahierLiaisonForm from './CahierLiaisonForm/useCahierLiaisonForm';
import { LeftContainer, MainContainer } from './CahierLiaisonMainContainer';
import { useILURLParams } from './hooks/useILURLParams';
import useStyles from './styles';
import { useButtonHeadAction } from './utils/utils';

interface ICahierLiaisonProps {
  refetchCount: () => void;
  countInformationLiaisons: any;
}

export interface IStateForm {
  id: string;
  idType: string;
  idOrigine: string;
  idOrigineAssocie: string;
  idItem: string;
  idItemAssocie: string;
  bloquant: boolean;
  statut: string;
  titre: string;
  description: string;
  idUserDeclarant: string;
  declaringName: string;
  idColleguesConcernees: string[];
  teamAll: boolean;
  fichiers: FichierInput[];
  idImportance: string;
  idUrgence: string;
  idFonction: string;
  idTache: string;
}

const CahierLiaisonMain: React.FC<ICahierLiaisonProps> = ({ refetchCount, countInformationLiaisons }) => {
  const classes = useStyles();
  const commonClasses = useCommonStyles();
  const { params, getInformationLiaisonsQuery, redirectTo } = useILURLParams();

  const { user: currentUser, federation, isMobile } = useApplicationContext();

  const { push } = useHistory();
  const { pathname } = useLocation();

  const currentUserId = currentUser.id;

  const displayNotification = useDisplayNotification();

  const isOnCreate = pathname === `${CAHIER_LIAISON_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`${CAHIER_LIAISON_URL}/edit`);
  const isOnList = !isOnCreate && !isOnEdit;

  const [activeItem, setActiveItem] = React.useState<any>(null);

  const [showMainContainer, setShowMainContainer] = useState<boolean>(isMobile ? false : true);

  const handleClickItem = (item: SearchInformationLiaisonInfoFragment, event: any) => {
    let prevStatutOfSelectedItem: any = undefined;
    if (item) {
      const collegueConcernee = item?.colleguesConcernees?.find((i) => i.userConcernee.id === currentUserId);

      prevStatutOfSelectedItem = collegueConcernee?.statut;
      if (collegueConcernee?.id && collegueConcernee.statut === 'NON_LUES') {
        updateStatus({ variables: { id: item.id, code: 'LUES' } });
      }
    }

    if (event) {
      event.stopPropagation();
    }

    setActiveItem(item);
    redirectTo({ ...params, id: item.id, prevStatutOfSelectedItem });
    setShowMainContainer(true);
  };
  const { goToAddInformation, goBack } = useButtonHeadAction(push);

  const sort =
    params.information === 'EMISE'
      ? [{ createdAt: { order: 'desc' } }, { statut: { order: 'desc' } }]
      : [{ priority: { order: 'asc' } }, { 'colleguesConcernees.statut': { order: 'desc' } }];

  const addFilterById = params.prevStatutOfSelectedItem === 'NON_LUES' && params.id; /*&&
    (((params.statut?.length || 0) > 1 && params?.statut?.includes('NON_LUES')) || !params.statut);
  */
  //&& (countInformationLiaisons?.RECUE_NON_LUES || 0) > 0;

  const queryVariables = {
    index: 'informationliaisons',
    query: addFilterById ? getInformationLiaisonsQuery() : getInformationLiaisonsQuery({ id: undefined }),
    take: 10,
    skip: 0,
    sortBy: sort,
  };

  const loadingInformations = useSearch_Information_LiaisonQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
    variables: queryVariables,
    skip: !isOnList,
    onCompleted: (data) => {
      const informationLiaisons = data.search?.data;
      if (informationLiaisons && params.id) {
        const newActiveItem = informationLiaisons.find(
          (informationLiaison: any) => informationLiaison.id === params.id
        );

        if (params.searchText && !newActiveItem) {
          setActiveItem(null);
        }

        newActiveItem && setActiveItem(newActiveItem);
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const { refetch } = loadingInformations;

  const handleCreateClick = () => {
    goToAddInformation();
  };

  const handleRefetch = (countOnly?: boolean) => {
    if (countOnly) {
      refetchCount();
    } else {
      refetch();
      refetchCount();
    }
  };

  const { updateStatus } = useCahierLiaisonForm({ refetch: handleRefetch, setActiveItem, disableNotify: true });

  if (activeItem && !params.id) {
    setActiveItem(null);
  }

  return (
    <Box className={!isOnList ? classnames(classes.container) : classnames(classes.container, classes.marginTop64)}>
      {/* {(mutationLoading || loading || uploadLoading || queryLoading) && (
        <Backdrop value={mutationLoading || loading ? mutationLoadingMsg : ''} />
      )} */}

      {isOnList ? (
        <Fragment>
          <Box className={classnames(classes.contentMain)}>
            <Box className={commonClasses.root}>
              {(!isMobile || !showMainContainer) && (
                <Box>
                  <Hidden mdUp={true} implementation="css">
                    <Box className={classes.createButton}>
                      <IconButton onClick={handleCreateClick} color="primary">
                        <Create />
                      </IconButton>
                    </Box>
                  </Hidden>
                  <LeftContainer
                    loadingInformations={loadingInformations}
                    activeItem={activeItem}
                    handleClickItem={handleClickItem}
                    refetch={handleRefetch}
                  />
                </Box>
              )}

              {!isMobile && (
                <MainContainer setActiveItem={setActiveItem} activeItem={activeItem} refetch={handleRefetch} />
              )}
            </Box>
          </Box>
        </Fragment>
      ) : (
        <Fragment>
          <Box className={classes.contentMain}>
            <CahierLiaisonForm refetch={handleRefetch} />
          </Box>
        </Fragment>
      )}
      <CustomModal
        open={isMobile && showMainContainer}
        setOpen={setShowMainContainer}
        fullScreen={true}
        closeIcon={true}
        withBtnsActions={false}
        noDialogContent={true}
      >
        <MainContainer
          setActiveItem={setActiveItem}
          activeItem={activeItem}
          refetch={handleRefetch}
          handleDrawerToggle={() => setShowMainContainer(false)}
        />
      </CustomModal>
    </Box>
  );
};

export default CahierLiaisonMain;
