import { useApplicationContext, useUploadFiles } from '@lib/common';
import {
  InformationLiaisonInput,
  useCreate_One_Information_LiaisonMutation,
  useDelete_One_Information_LiaisonMutation,
  useInformation_LiaisonQuery,
  useUpdate_One_Information_LiaisonMutation,
  useUpdate_Information_Liaison_StatutMutation,
} from '@lib/common/src/federation';
import { FichierInput, TypeFichier, useTake_Charge_Information_LiaisonMutation } from '@lib/common/src/graphql';
import { uniqBy } from 'lodash';
import { useState } from 'react';
import { useHistory, useParams } from 'react-router';
import { useILURLParams } from '../hooks/useILURLParams';

interface FormState {
  tache: any;
  fichiersJoints: File[];
  takeChargeFiles: File[];
  collegues: any;
  declarant: any;
  origineAssocie: any;
}

export const initialState: InformationLiaisonInput = {
  idType: '',
  idItem: undefined,
  idItemAssocie: undefined,
  idOrigine: '',
  idOrigineAssocie: undefined,
  statut: 'EN_COURS',
  titre: '',
  description: '',
  idUserDeclarant: '',
  idColleguesConcernees: [],
  bloquant: false,
  idFonction: undefined,
  idTache: undefined,
  idUrgence: '',
  idImportance: '',
};

const useCahierLiaisonForm = (props: any) => {
  const { refetch, setActiveItem, disabledNotify, setOpenDeleteDialog, changedParams } = props;
  const { push } = useHistory();
  const [input, setInput] = useState<InformationLiaisonInput>(initialState);
  const [state, setState] = useState<FormState>({
    tache: null,
    fichiersJoints: [],
    takeChargeFiles: [],
    collegues: [],
    declarant: [],
    origineAssocie: null,
  });
  const [takeChargeInput, setTakeChargeInput] = useState({
    id: '',
    date: '',
    description: '',
    files: [],
  });

  const { fichiersJoints } = state;
  const { graphql, notify, currentGroupement, federation } = useApplicationContext();
  const { params, redirectTo } = useILURLParams();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const { idliaison }: any = useParams();
  const loadingInformationLiaison = useInformation_LiaisonQuery({
    client: federation,
    fetchPolicy: 'network-only',
    variables: { id: idliaison || '' },
    skip: !idliaison,
    onCompleted: (data) => {
      const informationLiaison = data.informationLiaison;
      const input: InformationLiaisonInput = {
        idType: informationLiaison?.idType || '',
        idItem: informationLiaison?.idItem || '',
        idItemAssocie: informationLiaison?.idItemAssocie,
        idOrigine: informationLiaison?.idOrigine,
        idOrigineAssocie: informationLiaison?.idOrigineAssocie,
        statut: informationLiaison?.statut || '',
        titre: informationLiaison?.titre || '',
        description: informationLiaison?.description || '',
        idUserDeclarant: informationLiaison?.declarant?.id || '',
        idColleguesConcernees: (informationLiaison?.colleguesConcernees || []).map(
          (collegueConcernee) => collegueConcernee.userConcernee.id
        ),
        bloquant: informationLiaison?.bloquant,
        idFonction: informationLiaison?.idFonction,
        idTache: informationLiaison?.idTache,
        idUrgence: informationLiaison?.urgence?.id || '',
        idImportance: informationLiaison?.importance?.id || '',
        fichiers: (informationLiaison?.fichiers || []).map((fichierJoint) => ({
          type: fichierJoint.type as TypeFichier,
          nomOriginal: fichierJoint.nomOriginal || '',
          chemin: fichierJoint.chemin || '',
        })),
      };
      setInput(input);
      setState({
        declarant: informationLiaison?.declarant ? [informationLiaison?.declarant] : [],
        collegues: (informationLiaison?.colleguesConcernees || []).map(
          (collegueConcernee) => collegueConcernee.userConcernee
        ),
        tache: informationLiaison?.idFonction || informationLiaison?.idType,
        fichiersJoints: (informationLiaison?.fichiers || [])?.map(
          (fichierJoint) => ({ name: fichierJoint.nomOriginal, type: fichierJoint.type } as File)
        ),
        origineAssocie:
          (informationLiaison?.origineAssocie as any)?.dataType === 'User'
            ? [informationLiaison?.origineAssocie]
            : informationLiaison?.origineAssocie,
        takeChargeFiles: [],
      });
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  const informationLiaison = loadingInformationLiaison.data?.informationLiaison;

  const [createCahierLiaison, creatingCahierLiaison] = useCreate_One_Information_LiaisonMutation({
    client: federation,
    variables: {
      input,
    },
    onCompleted: (data) => {
      if (data && data.createOneInformationLiaison) {
        notify({
          type: 'success',
          message:
            (input.supprime && 'Suppression effectuée avec succès') ||
            `${params.id ? 'Modification' : 'Création'} réussie`,
        });
        refetch && refetch();
        push('/intelligence-collective/cahier-de-liaison?statut=NON_LUES&information=RECUE');
      }
    },
    onError: (errors) => {
      console.log(errors);
      if (errors.graphQLErrors) {
        errors.graphQLErrors.map((err) => {
          notify({ type: 'error', message: err.message });
        });
      }
    },
  });

  const [updateCahierLiaison, updatingCahierLiaison] = useUpdate_One_Information_LiaisonMutation({
    client: federation,
    variables: {
      id: idliaison || '',
      input,
    },
    onCompleted: (data) => {
      if (data && data.updateOneInformationLiaison) {
        notify({
          type: 'success',
          message:
            (input.supprime && 'Suppression effectuée avec succès') ||
            `${idliaison ? 'Modification' : 'Création'} réussie`,
        });
        refetch && refetch();
        push('/intelligence-collective/cahier-de-liaison?statut=NON_LUES&information=RECUE');
      }
    },
    onError: (errors) => {
      console.log(errors);
      if (errors.graphQLErrors) {
        errors.graphQLErrors.map((err) => {
          notify({ type: 'error', message: err.message });
        });
      }
    },
  });

  const [takeChargeInformationLiaison, takingChargeInformationLiaison] = useTake_Charge_Information_LiaisonMutation({
    client: graphql,
    variables: {
      input: takeChargeInput,
    },
    update: (cache, { data }) => {
      if (data && data.takeChargeInformationLiaison) {
      }
    },
    onCompleted: (data) => {
      if (data && data.takeChargeInformationLiaison) {
        notify({
          type: 'success',
          message: 'Prise en charge réussie',
        });
      }
    },
    onError: (errors) => {
      console.log(errors);
      if (errors.graphQLErrors) {
        errors.graphQLErrors.map((err) => {
          notify({ type: 'error', message: err.message });
        });
      }
    },
  });

  const [deleteInformationLiaison, deletingInformationLiaison] = useDelete_One_Information_LiaisonMutation({
    client: federation,
    onCompleted: (data) => {
      if (data && data.deleteOneInformationLiaison) {
        if (refetch) refetch();
        setOpenDeleteDialog && setOpenDeleteDialog(false);
        notify({
          type: 'success',
          message: 'Suppression effectuée avec succès',
        });
      }
    },
    onError: (error) => {
      //setOpenDeleteDialog(false);
      if (error && error.graphQLErrors && error.graphQLErrors.length > 0) {
        error.graphQLErrors.map((err) => {
          notify({ type: 'error', message: err.message });
        });
      }
    },
  });

  const [updateStatus, { loading: updateStatusLoading }] = useUpdate_Information_Liaison_StatutMutation({
    client: federation,
    onCompleted: (data) => {
      if (data && data.updateInformationLiaisonStatut) {
        if (setActiveItem) setActiveItem(data.updateInformationLiaisonStatut);
        if (changedParams) redirectTo({ ...params, ...changedParams });
        if (refetch) refetch();

        if (!disabledNotify) {
          notify({
            type: 'success',
            message: 'Statut mis à jour avec succès',
          });
        }
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  const createUpdateCahierLiaison = () => {
    if (fichiersJoints && fichiersJoints.length > 0) {
      uploadFiles(fichiersJoints, {
        directory: 'cahier-liaison',
      })
        .then((uploadedFiles) => {
          if (idliaison) {
            updateCahierLiaison({
              variables: { id: idliaison || '', input: { ...input, fichiers: uploadedFiles } },
            });
          } else {
            createCahierLiaison({
              variables: { input: { ...input, fichiers: uploadedFiles } },
            });
          }
        })
        .catch((error) => {
          notify({
            type: 'error',
            message: "Erreur lors de l'envoye de(s) fichier(s)",
          });
        });
    } else {
      if (idliaison) {
        updateCahierLiaison({
          variables: { id: idliaison || '', input: { ...input, fichiers: [] } },
        });
      } else {
        createCahierLiaison({
          variables: { input: { ...input, fichiers: [] } },
        });
      }
    }
  };

  const prendreEnCharge = () => {
    if (fichiersJoints && fichiersJoints.length > 0) {
      uploadFiles(fichiersJoints, {
        directory: 'cahier-liaison',
      })
        .then((uploadedFiles) => {
          if (idliaison) {
            updateCahierLiaison({
              variables: { id: idliaison || '', input: { ...input, fichiers: uploadedFiles } },
            });
          } else {
            createCahierLiaison({
              variables: { input: { ...input, fichiers: uploadedFiles } },
            });
          }
        })
        .catch((error) => {
          notify({
            type: 'error',
            message: "Erreur lors de l'envoye de(s) fichier(s)",
          });
        });
    } else {
      takeChargeInformationLiaison();
    }
  };

  const formChange = (event: any) => {
    if (event.target) {
      const { value, name, checked } = event.target;
      // if (name === 'statutId' && value === 'EN_CHARGE')
      if (name === 'statut' && value === 'CLOTURE') {
        //setopenModal(!openModal);
      } else if (name === 'bloquant') {
        setInput((prevState: any) => ({ ...prevState, [name]: checked }));
      } else {
        setInput((prevState: any) => ({ ...prevState, [name]: value }));
      }
    }
  };

  return {
    informationLiaison,
    createUpdateCahierLiaison,
    formChange,
    prendreEnCharge,
    loading:
      loadingInformationLiaison.loading ||
      uploadingFiles.loading ||
      creatingCahierLiaison.loading ||
      updatingCahierLiaison.loading ||
      takingChargeInformationLiaison.loading ||
      deletingInformationLiaison.loading ||
      updateStatusLoading,

    input,
    setInput,
    state,
    setState,
    takeChargeInput,
    setTakeChargeInput,
    updateStatus,
    deleteInformationLiaison,
  };
};

export default useCahierLiaisonForm;
