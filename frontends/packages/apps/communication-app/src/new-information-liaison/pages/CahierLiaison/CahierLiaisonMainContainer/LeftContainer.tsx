import { QueryResult } from '@apollo/client';
import { CAHIER_LIAISON_URL, InfiniteItemList } from '@lib/common';
import {
  SearchInformationLiaisonInfoFragment,
  Search_Information_LiaisonQuery,
  Search_Information_LiaisonQueryVariables,
} from '@lib/common/src/federation';
import { DebouncedSearchInput, NewCustomButton, NoItemListImage } from '@lib/ui-kit';
import { Box, CssBaseline, Hidden } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import classnames from 'classnames';
import React, { FC } from 'react';
import { useHistory } from 'react-router';
import useStyles from '../../leftContainerStyles';
import { useILURLParams } from '../hooks/useILURLParams';
import { Item } from './Item';

export interface LeftContainerProps {
  loadingInformations: QueryResult<Search_Information_LiaisonQuery, Search_Information_LiaisonQueryVariables>;
  activeItem: any;
  refetch: any;
  handleClickItem: (item: any, event: any) => void;
}

const LeftContainer: FC<LeftContainerProps> = ({ loadingInformations, activeItem, refetch, handleClickItem }) => {
  const classes = useStyles();
  const { push } = useHistory();
  const { params, redirectTo } = useILURLParams();

  let listRef = React.useRef();

  const handleSearch = (value: any) => {
    redirectTo({ ...params, searchText: value });
  };

  const handleClickAdd = () => {
    push(`${CAHIER_LIAISON_URL}/create`);
  };

  return (
    <Box className={classes.leftContainer}>
      <CssBaseline />
      <Hidden smDown={true} implementation="css">
        <Box
          className={classnames(classes.mainContainerHeader, classes.leftContainerHeader)}
          justifyContent="center !important"
        >
          <NewCustomButton startIcon={<Add />} onClick={handleClickAdd}>
            Nouvelle information
          </NewCustomButton>
        </Box>
      </Hidden>
      <Hidden mdUp={true} implementation="css">
        <Box width="100vw" display="flex" justifyContent="center">
          <DebouncedSearchInput
            placeholder={'Rechercher ...'}
            wait={500}
            outlined
            onChange={handleSearch}
            value={params.searchText || ''}
          />
        </Box>
      </Hidden>
      {loadingInformations.data?.search?.total === 0 && (
        <Box minHeight="calc(100vh - 162px)" width="100%" display="flex" alignItems="center" justifyContent="center">
          <NoItemListImage
            title="Aucune information dans la liste"
            subtitle="Crées en une en cliquant sur le bouton d'en haut."
          />
        </Box>
      )}
      <InfiniteItemList<
        SearchInformationLiaisonInfoFragment,
        Search_Information_LiaisonQuery,
        Search_Information_LiaisonQueryVariables
      >
        queryResult={loadingInformations}
        refetch={refetch}
        onItemClick={handleClickItem}
        activeItem={activeItem}
        itemRenderer={({
          data,
          style,
          loading,
          refetch: providedRefetch,
          onClick,
          activeItem: providedActiveItem,
          registerChild,
        }) => {
          return (
            <Item
              ref={registerChild}
              style={style}
              item={data}
              activeItem={providedActiveItem}
              loading={loading}
              refetch={providedRefetch}
              handleClickItem={onClick}
            />
          );
        }}
      />
    </Box>
  );
};

export default LeftContainer;
