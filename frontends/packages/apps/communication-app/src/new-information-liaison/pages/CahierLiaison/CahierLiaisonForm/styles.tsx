import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    form: {
      maxWidth: 502,
      '@media (max-width: 1024px)': {
        margin: '0px 20px',
      },
    },
    cahierLiaisonFormRoot: {
      maxWidth: '744px',
      width: '100%',
      padding: '0 24px',
      margin: '25px auto 25px',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    radioButtonBloquant: {
      width: '100%',
      '& .MuiFormControl-root': {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        '& .MuiListItem-root': {
          width: 'auto',
          paddingRight: 0,
          paddingLeft: 0,
          marginRight: 24,
          '&:hover': {
            backgroundColor: 'inherit',
          },
        },
      },
    },
    formContainer: {
      width: '100%',
      display: 'flex',
      paddingBottom: 30,
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
    },
    mutationSuccessContainer: {
      width: '100%',
      minHeight: 'calc(100vh - 156px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '& img': {
        width: 350,
        height: 270,
      },

      '& p': {
        textAlign: 'center',
        maxWidth: 600,
      },
      '& > div > p:nth-child(2)': {
        fontSize: 30,
        fontWeight: 'bold',
      },
      '& > div > p:nth-child(3)': {
        fontSize: 16,
        fontWeight: 'normal',
      },
      '& button': {
        marginTop: 30,
      },
    },
    inputTitle: {
      fontWeight: 'bold',
      fontSize: 18,
      marginBottom: 15,
    },
    inputsContainer: {
      border: '1px solid #DCDCDC',
      borderRadius: 12,
      marginBottom: 25,
      width: '100%',
      '& > div:not(:nth-last-child(1))': {
        borderBottom: '1px solid #DCDCDC',
      },
    },
    customizedReactQuill: {
      marginBottom: 10,
      '& .ql-toolbar.ql-snow': {
        borderRadius: '5px 5px 0px 0px',
      },
      '& .ql-container.ql-snow': {
        minHeight: 110,
        borderRadius: '0px 0px 5px 5px ',
      },
    },
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
    checkboxs: {
      '@media (max-width: 1024px)': {
        justifyContent: 'center',
      },
      '& div': {
        '@media (max-width: 1024px)': {
          width: 'auto',
        },
      },
    },
    autocomplete: {
      width: '100%',
      '& .MuiAutocomplete-inputRoot input': {
        padding: '2.5px 14px !important',
      },
      '& input': {
        color: '#000000',
      },
      '& label': {
        color: '#000000',
      },
      '&:hover label, &.Mui-focused label': {
        color: '#B3D00A',
      },
      '&.Mui-focused fieldset': {
        border: '2px solid #B3D00A !important',
      },
      '& input:invalid + fieldset': {
        borderColor: 'red',
      },
      '& .Mui-focused .MuiIconButton-label svg': {
        color: '#B3D00A !important',
      },
      '& .MuiInput-underline:after': {
        borderBottomColor: '#ffffff',
      },
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: '#000000',
        },
        '&:hover fieldset': {
          borderColor: '#B3D00A',
        },
        '&.Mui-focused fieldset': {
          borderColor: '#B3D00A',
        },
      },
    },
    customAutomcomplete: {
      width: '100%',
      '& .MuiFormLabel-asterisk': {
        color: 'red',
      },
      '& .MuiInputBase-root': {
        padding: 0,
      },
    },
  })
);

export default useStyles;
