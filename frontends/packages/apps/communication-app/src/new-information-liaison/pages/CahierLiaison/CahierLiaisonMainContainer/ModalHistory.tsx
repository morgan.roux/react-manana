import { isMobile } from '@lib/common';
import { CustomModal } from '@lib/ui-kit';
import { Box, makeStyles, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import moment from 'moment';
import React, { Dispatch, SetStateAction } from 'react';
import { SearchInformationLiaisonInfoFragment } from '@lib/common/src/federation';

export interface ModalHistoryProps {
  info: SearchInformationLiaisonInfoFragment;
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
}

const useStyles = makeStyles({
  table: {
    // minWidth: 650,
    paddingTop: '0 !important',
  },
});

const ModalHistory: React.FC<ModalHistoryProps> = ({ info, open, setOpen }) => {
  const classes = useStyles();

  const data = info.colleguesConcernees || [];

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Historique"
      closeIcon={true}
      withBtnsActions={false}
      headerWithBgColor={true}
      fullWidth={true}
      maxWidth="md"
      fullScreen={isMobile() ? true : false}
    >
      <Box width="100%">
        <Table className={classes.table} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell align="left">Collègue(s)</TableCell>
              <TableCell align="left">Lecture</TableCell>
              <TableCell align="left">Clôture</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((d) => (
              <TableRow key={d.id}>
                <TableCell align="left">{d.userConcernee.fullName || '-'}</TableCell>
                <TableCell align="left">
                  {d.statut === 'LUES'
                    ? `Oui ${d.dateStatutModification ? moment.utc(d.dateStatutModification).format('DD/MM/YYYY') : ''}`
                    : d.statut === 'CLOTURE'
                    ? 'Oui'
                    : 'Non'}
                </TableCell>
                <TableCell align="left">
                  {d.statut === 'CLOTURE'
                    ? `Oui ${d.dateStatutModification ? moment.utc(d.dateStatutModification).format('DD/MM/YYYY') : ''}`
                    : 'Non'}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Box>
    </CustomModal>
  );
};

export default ModalHistory;
