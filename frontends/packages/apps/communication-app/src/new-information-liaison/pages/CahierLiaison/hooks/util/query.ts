import { MeUserInfoFragment } from '@lib/common/src/federation';
import { buildCollegueConcerneeFilter } from '../filters/collegue-concernee-filter';
import { buildImportanceFilter } from '../filters/importance-filter';
import { buildSearchTextFilter } from '../filters/search-text-filter';
import { buildUrgenceFilter } from '../filters/urgence-filter';
import { buildSort } from '../sorting/sort';
import { ILURLParams } from '../useILURLParams';
import { commonTerms } from './constants';
import { mergeFilters } from './merge-filter';
import { Filter } from './types';

export const buildQueryFilters = ({ params, user }: BuildILQueryParams): Filter => {
  return mergeFilters([
    { must: commonTerms() },
    buildCollegueConcerneeFilter(params, user),
    buildUrgenceFilter(params),
    buildImportanceFilter(params),
    buildSearchTextFilter(params),
    //buildStatutFilter(params),
  ]);
};

interface BuildILQueryParams {
  user: MeUserInfoFragment;
  params: ILURLParams;
  sorting?: boolean;
}

export const buildILQuery = ({ user, params, sorting }: BuildILQueryParams) => {
  const { should, must } = buildQueryFilters({ user, params, sorting });
  const sortingQuery = buildSort(params);
  const sort = sorting && sortingQuery ? sortingQuery : undefined;

  return {
    sort,
    query: {
      bool: {
        should,
        minimum_should_match: (should?.length || 0) > 0 ? 1 : undefined,
        must,
      },
    },
  };
};
