import { ILURLParams } from '../useILURLParams';
import { Filter } from '../util/types';

export const buildSearchTextFilter = (params: ILURLParams): Filter => {
  if (params.searchText) {
    return {
      must: [
        {
          query_string: {
            query: `*${params.searchText}*`,
            analyze_wildcard: true,
          },
        },
      ],
    };
  }

  return {};
};
