import { ILURLParams } from '../useILURLParams';
import { Filter } from '../util/types';

export const buildStatutFilter = (params: ILURLParams): Filter => {
  if (params.statut) {
    return {
      must: [
        {
          terms: {
            statut: params.statut,
          },
        },
      ],
    };
  } else {
    return {};
  }
};
