import { useApplicationContext } from '@lib/common';

export const commonTerms = () => {
  const { currentPharmacie } = useApplicationContext();
  return [
    {
      term: {
        supprime: false,
      },
    },
    {
      term: {
        'pharmacie.id': currentPharmacie.id,
      },
    },
  ];
};
