import { LeftSidebarAndMainPage, useApplicationContext } from '@lib/common';
import { ImportanceInterface } from '@lib/common/src/components/ImportanceFilter/ImportanceFilterList';
import { UrgenceInterface } from '@lib/common/src/components/UrgenceFilter/UrgenceFilterList';
import { useBulk_CountQuery, useGet_UrgencesQuery, useImportancesQuery } from '@lib/common/src/federation';
import { Box, IconButton } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router';
import FilterAlt from '../../assets/icon/filter_alt_white.svg';
import CahierLiaisonFilter from './CahierLiaisonFilter';
import CahierLiaisonMain from './CahierLiaisonMain';
import { useILURLParams } from './hooks/useILURLParams';
import useStyles from './styles';

const CahierLiaison: FC = ({}) => {
  const classes = useStyles({});
  const { push } = useHistory();
  const { federation } = useApplicationContext();
  const { getInformationLiaisonsQuery, params } = useILURLParams();

  const handleBack = () => {
    push('/');
  };

  const [openDrawer, setOpenDrawer] = React.useState(false);

  const handleDrawerToggle = () => {
    setOpenDrawer(!openDrawer);
  };

  const handleShowFiltersClick = () => {
    setOpenDrawer(!openDrawer);
  };

  const optionBtn = (
    <IconButton
      className={classes.iconAction}
      color="inherit"
      aria-label="settings"
      edge="start"
      onClick={handleShowFiltersClick}
    >
      <img src={FilterAlt} />
    </IconButton>
  );

  const countingIL = useBulk_CountQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
    variables: {
      queries: {
        EMISE_EN_COURS: {
          index: 'informationliaisons',
          ...getInformationLiaisonsQuery({ ...params, statut: ['EN_COURS'], information: 'EMISE', id: undefined }),
        },
        EMISE_CLOTURE: {
          index: 'informationliaisons',
          ...getInformationLiaisonsQuery({ ...params, statut: ['CLOTURE'], information: 'EMISE', id: undefined }),
        },
        RECUE_NON_LUES: {
          index: 'informationliaisons',
          ...getInformationLiaisonsQuery({ ...params, statut: ['NON_LUES'], information: 'RECUE', id: undefined }),
        },
        RECUE_LUES: {
          index: 'informationliaisons',
          ...getInformationLiaisonsQuery({ ...params, statut: ['LUES'], information: 'RECUE', id: undefined }),
        },
        RECUE_CLOTURE: {
          index: 'informationliaisons',
          ...getInformationLiaisonsQuery({ ...params, statut: ['CLOTURE'], information: 'RECUE', id: undefined }),
        },
      },
    },
  });

  const countInformationLiaisons = countingIL.data?.bulkCount;

  return (
    <Box className={classes.root}>
      <LeftSidebarAndMainPage
        drawerTitle="Cahier de liaison"
        drawerBackUrl="/"
        sidebarChildren={
          <CahierLiaisonFilter handleBack={handleBack} countInformationLiaisons={countInformationLiaisons} />
        }
        mainChildren={
          <CahierLiaisonMain countInformationLiaisons={countInformationLiaisons} refetchCount={countingIL.refetch} />
        }
        optionBtn={[optionBtn]}
        handleDrawerToggle={handleDrawerToggle}
        openDrawer={openDrawer}
        setOpenDrawer={setOpenDrawer}
      />
    </Box>
  );
};

export default CahierLiaison;
