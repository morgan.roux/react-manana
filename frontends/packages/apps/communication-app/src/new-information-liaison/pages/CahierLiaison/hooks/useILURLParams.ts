import { useApplicationContext, useURLSearchParams } from '@lib/common';
import { useCallback, useMemo } from 'react';
import { useHistory } from 'react-router';
import { buildILQuery } from './util/query';

const isPrimitive = (value: any): boolean => {
  return value !== Object(value);
};

export interface ILURLParams {
  searchText?: string;
  collegueConcernees?: any[];
  urgences?: string[];
  importances?: string[];
  statut?: Array<'NON_LUES' | 'LUES' | 'EN_COURS' | 'CLOTURE'>;
  sorting?: Record<string, 'asc' | 'desc'>;
  information?: 'EMISE' | 'RECUE';
  id?: string;
  prevStatutOfSelectedItem?: 'NON_LUES' | 'LUES' | 'EN_COURS' | 'CLOTURE'; // previous statut of selected item
}

export interface UseILURLParamsResult {
  params: ILURLParams;
  redirectTo: (params?: ILURLParams) => void;
  getInformationLiaisonsQuery: (changedParams?: ILURLParams, sorting?: boolean) => any;
}
const encodeParam = (param: any, isObjectArray?: boolean): string | undefined => {
  if (Array.isArray(param) && param.length > 0) {
    return isObjectArray
      ? encodeURIComponent(param.map((el) => JSON.stringify(el)).join(','))
      : encodeURIComponent(param.join(','));
  } else if (typeof param === 'string') {
    return encodeURIComponent(param);
  } else if (isPrimitive(param)) {
    return param;
  } else if (param) {
    return encodeURIComponent(JSON.stringify(param));
  }

  return undefined;
};

const decodeParam = (
  params: URLSearchParams,
  name: keyof ILURLParams,
  isArray: boolean,
  isObjectArray?: boolean
): any | undefined => {
  const value = params.get(name);
  if (value) {
    const decodedValue = decodeURIComponent(value);
    if (isObjectArray) {
      return JSON.parse(`[${decodedValue}]`);
    }
    if (isArray) {
      return decodedValue.split(',');
    }
    return decodedValue;
  }

  return undefined;
};

const basePath = '/intelligence-collective/cahier-de-liaison';
export const useILURLParams = (): UseILURLParamsResult => {
  const query = useURLSearchParams();
  const history = useHistory();
  const { user, federation } = useApplicationContext();

  const params: ILURLParams = useMemo(
    () => ({
      collegueConcernees: decodeParam(query, 'collegueConcernees', false, true),
      urgences: decodeParam(query, 'urgences', true),
      importances: decodeParam(query, 'importances', true),
      sorting: decodeParam(query, 'sorting', false),
      statut: decodeParam(query, 'statut', true),
      information: decodeParam(query, 'information', false),
      searchText: decodeParam(query, 'searchText', false),
      id: decodeParam(query, 'id', false),
      prevStatutOfSelectedItem: decodeParam(query, 'prevStatutOfSelectedItem', false),
    }),
    [query]
  );

  const redirectTo = useCallback(
    (changedParams?: ILURLParams) => {
      const newParams: any = {
        ...params,
        ...(changedParams || {}),
      };

      const objectArrayParams: string[] = ['collegueConcernees'];

      const newUrl = Object.keys(newParams).reduce((url, paramName: any) => {
        const newValue = newParams[paramName];
        if (!newValue) {
          return url;
        }
        const encodedValue = encodeParam(newValue, objectArrayParams.includes(paramName) ? true : false);
        if (!encodedValue) {
          return url;
        }

        const keyValue = `${paramName}=${encodedValue}`;

        const toAppend = !url.includes('?') ? `?${keyValue}` : `&${keyValue}`;

        return `${url}${toAppend}`;
      }, `${basePath}`);

      history.push(newUrl);
    },
    [params]
  );

  const getInformationLiaisonsQuery = useCallback(
    (changedParams?: ILURLParams, sorting?: boolean) => {
      return buildILQuery({
        sorting,
        user,
        params: changedParams ? { ...params, ...changedParams } : params,
      });
    },
    [params]
  );

  return {
    params,
    getInformationLiaisonsQuery,
    redirectTo,
  };
};
