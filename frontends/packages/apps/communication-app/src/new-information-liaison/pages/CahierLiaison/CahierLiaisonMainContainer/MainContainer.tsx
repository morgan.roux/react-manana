import { AttachedFiles, isMobile, MobileTopBar, nl2br, RightContentComment, useApplicationContext } from '@lib/common';
import AvatarInput from '@lib/common/src/components/AvatarInput';
import {
  SearchInformationLiaisonInfoFragment,
  useFonctionQuery,
  useGet_OriginesQuery,
  useInformation_Liaison_TypesQuery,
  useTacheQuery,
} from '@lib/common/src/federation';
import { NewCustomButton, NoItemContentImage } from '@lib/ui-kit';
import { Box, CssBaseline, Hidden, IconButton, Menu, MenuItem, Typography } from '@material-ui/core';
import { Close, History, MoreVert } from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import React, { FC, Fragment, useState } from 'react';
import useCommonStyles from '../../leftContainerItemStyles';
import useStyles from '../../mainContainerStyles';
import useCahierLiaisonForm from '../CahierLiaisonForm/useCahierLiaisonForm';
import { useILURLParams } from '../hooks/useILURLParams';
import ColleguesConcernees from './ColleguesConcernees';
import ModalHistory from './ModalHistory';
import useOthersStyles from './styles';
export interface MainContainerProps {
  setActiveItem: any;
  activeItem: SearchInformationLiaisonInfoFragment;
  refetch: any;
  handleDrawerToggle?: () => void;
}

const MainContainer: FC<MainContainerProps> = ({ activeItem, refetch, handleDrawerToggle, setActiveItem }) => {
  const classes = useStyles();
  const othersClasses = useOthersStyles();
  const commonClasses = useCommonStyles();

  const { user: currentUser, federation } = useApplicationContext();

  const { params } = useILURLParams();

  const { updateStatus } = useCahierLiaisonForm({ refetch, changedParams: { id: undefined } });

  const currentUserId = currentUser.id;

  const [openModalHistory, setOpenModalHistory] = React.useState(false);

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const open = anchorEl ? true : false;

  const informationLiaisonTypesQuery = useInformation_Liaison_TypesQuery({
    client: federation,
  });

  const originesQuery = useGet_OriginesQuery({
    client: federation,
  });

  const informationLiaisonTypes = informationLiaisonTypesQuery.data?.informationLiaisonTypes.nodes || [];
  const origines = originesQuery.data?.origines.nodes || [];

  const collegueConcerneeIds: string[] = (activeItem?.colleguesConcernees || []).map((i) => i.userConcernee.id);

  const collegueConcerneeNames: string[] = (activeItem?.colleguesConcernees || []).map(
    (i) => i.userConcernee.fullName || ''
  );

  const isCollegueConcerned: boolean = collegueConcerneeIds.includes(currentUserId);

  const collegueConcernee = activeItem?.colleguesConcernees?.find((i) => i.userConcernee.id === currentUserId);

  const declarantId = activeItem?.declarant?.id;
  const declarantName = activeItem?.declarant?.fullName;

  const fichiersJoints: any[] = activeItem?.fichiers || [];

  const userCreationId = activeItem?.createdBy?.id;

  const type = activeItem?.idType && informationLiaisonTypes.find((el) => el.id === activeItem.idType)?.libelle;

  const urgence = activeItem?.urgence ? `${activeItem.urgence.code}:${activeItem.urgence.libelle}` : '';
  const importance = activeItem?.importance?.libelle || '';

  const origine = activeItem?.idOrigine ? origines.find(({ id }) => id === activeItem?.idOrigine)?.libelle : '';
  const correspondant = activeItem?.origineAssocie
    ? (activeItem.origineAssocie as any).userName ||
      (activeItem.origineAssocie as any).nomLabo ||
      (activeItem.origineAssocie as any).nom
    : '';

  const dateCreation = activeItem?.createdAt && moment(activeItem.createdAt).format('DD/MM/YYYY');

  const tacheQuery = useTacheQuery({
    client: federation,
    variables: { id: activeItem?.idTache || '' },
    skip: !activeItem?.idTache,
  });
  const fonctionQuery = useFonctionQuery({
    client: federation,
    variables: { id: activeItem?.idFonction || '' },
    skip: !activeItem?.idFonction,
  });

  const tache = tacheQuery.data?.dQMTTache;
  const fonction = fonctionQuery.data?.dQMTFonction;

  const todoAction = activeItem?.todoAction ? 'Oui' : 'Non';

  const MORE_INFOS = isMobile()
    ? [
        { id: 3, label: 'Type', value: type },
        //  { id: 4, label: 'Origine', value: origine },
        { id: 4, label: 'Origine', value: origine },
        { id: 16, label: 'Urgence', value: urgence },
        { id: 17, label: 'Importance', value: importance },
        { id: 10, label: 'Date de création', value: dateCreation },
        { id: 12, label: 'Tâche', value: tache?.libelle || '-' },
        { id: 13, label: 'Fonction', value: fonction?.libelle || '-' },
        { id: 15, label: 'Action todo', value: todoAction },
      ]
    : [
        {
          id: 1,
          label: 'Déclarant',
          value: declarantId === currentUserId ? 'Moi' : declarantName,
        },
        {
          id: 2,
          label: 'Collègue(s) Concernée(s)',
          value: (collegueConcerneeNames.length > 0 && collegueConcerneeNames.join(', ')) || '-',
        },
        { id: 3, label: 'Type', value: type },
        { id: 4, label: 'Origine', value: origine },
        { id: 4, label: 'Correspondant', value: correspondant },
        { id: 16, label: 'Urgence', value: urgence },
        { id: 17, label: 'Importance', value: importance },
        { id: 10, label: 'Date de création', value: dateCreation },
        { id: 12, label: 'Tâche', value: tache?.libelle || '-' },
        { id: 13, label: 'Fonction', value: fonction?.libelle || '-' },
        { id: 15, label: 'Action todo', value: todoAction },
      ];

  const handleClickPrendreCharge = () => {
    setAnchorEl(null);
    updateStatus({
      variables: { id: activeItem.id, code: 'CLOTURE', information: params.information },
    });
  };

  const handleClickHistory = () => {
    setAnchorEl(null);
    setOpenModalHistory(true);
  };

  const disabledBtnCloturer = (): boolean => {
    if (currentUserId && activeItem) {
      // For collegue concerned
      if (isCollegueConcerned && collegueConcernee && params.information === 'RECUE') {
        if (collegueConcernee.statut === 'CLOTURE') return true;
      }

      // For user creation
      if (userCreationId === currentUserId && params.information === 'EMISE') {
        if (activeItem.statut === 'CLOTURE') return true;
      }
    }
    return false;
  };

  const handleMoreOptionClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    }
  };

  const optionBtn = (
    <>
      <IconButton color="inherit" onClick={handleMoreOptionClick}>
        <MoreVert />
      </IconButton>
      <Menu id="fade-menu" anchorEl={anchorEl} keepMounted={true} open={open}>
        <MenuItem onClick={handleClickHistory}>
          <Typography variant="inherit">Historique</Typography>
        </MenuItem>
        <MenuItem disabled={disabledBtnCloturer()} onClick={handleClickPrendreCharge}>
          <Typography variant="inherit">Clôturer</Typography>
        </MenuItem>
      </Menu>
    </>
  );

  return (
    <Box className={classes.mainContainer} key={activeItem?.id}>
      <Box className={classes.mainContent}>
        <CssBaseline />
        <Hidden mdUp={true} implementation="css">
          <MobileTopBar
            title={`${origine || ''}${activeItem?.bloquant ? ` - Bloquante` : ''}`}
            withBackBtn={true}
            handleDrawerToggle={handleDrawerToggle}
            optionBtn={[optionBtn]}
            withTopMargin={false}
            onClickBack={handleDrawerToggle}
          />
        </Hidden>

        {activeItem ? (
          <Box className={classes.detailsContainer}>
            <Hidden mdUp={true} implementation="css">
              <Typography
                className={classes.labelContent}
                dangerouslySetInnerHTML={{ __html: nl2br(activeItem?.description) } as any}
              />
            </Hidden>
            <Hidden smDown={true} implementation="css">
              <Box display="flex" flexDirection="row" marginBottom="15px">
                <NewCustomButton startIcon={<History />} onClick={handleClickHistory}>
                  Historique
                </NewCustomButton>

                <NewCustomButton
                  startIcon={<Close />}
                  style={{ marginLeft: 10 }}
                  disabled={disabledBtnCloturer()}
                  onClick={handleClickPrendreCharge}
                >
                  Clôturer
                </NewCustomButton>
              </Box>

              <Box display="flex" justifyContent="space-between" alignItems="center">
                {origine && (
                  <Typography className={classes.title} color="primary">
                    {`${origine || ''}${activeItem?.bloquant ? ` - Bloquante` : ''}`}
                  </Typography>
                )}
              </Box>
            </Hidden>

            <Hidden smDown={true} implementation="css">
              <Box className={othersClasses.infoSupContainer}>
                {MORE_INFOS.filter((info) => info.value && info.value !== '-').map((info, index) => {
                  return (
                    <Box key={index} display="flex" alignItems="center">
                      <Typography
                        className={classnames(commonClasses.labelContent)}
                        style={{ minWidth: 'fit-content' }}
                      >
                        {info.label} :
                      </Typography>
                      {info.id === 2 ? (
                        <ColleguesConcernees info={activeItem} />
                      ) : (
                        <Typography
                          className={classnames(commonClasses.labelContentValue, othersClasses.infoValue)}
                          style={{ cursor: 'auto' }}
                        >
                          {info.value || '-'}
                        </Typography>
                      )}
                    </Box>
                  );
                })}
              </Box>
            </Hidden>

            <Hidden smUp={true} implementation="css">
              <Box className={classes.mobileInfoSupContainer}>
                <Box display="flex" justifyContent="space-between" marginBottom="16px">
                  <Typography className={classes.mobileInfoSupLabel}>Déclarant</Typography>
                  <AvatarInput
                    list={(activeItem?.declarant && [activeItem.declarant]) || []}
                    small={true}
                    standard={true}
                  />
                </Box>
                <Box display="flex" justifyContent="space-between" marginBottom="16px">
                  <Typography className={classes.mobileInfoSupLabel}>Collègue(s) Concernée(s)</Typography>

                  <Typography className={classes.mobileInfoSupValue}>
                    <AvatarInput
                      list={(activeItem.colleguesConcernees || []).map(
                        (collegue) => collegue && collegue.userConcernee
                      )}
                      small={true}
                      standard={true}
                    />
                  </Typography>
                </Box>
                {MORE_INFOS.filter((info) => info.value && info.value !== '-').map((info, index) => {
                  return (
                    <Box display="flex" justifyContent="space-between" marginBottom="16px">
                      <Typography className={classes.mobileInfoSupLabel}>{info.label}</Typography>

                      <Typography className={classes.mobileInfoSupValue}>{info.value}</Typography>
                    </Box>
                  );
                })}
              </Box>
            </Hidden>

            <Hidden smDown={true} implementation="css">
              <Box display="flex" flexDirection="column">
                {activeItem?.description && (
                  <Fragment>
                    <Typography className={classes.labelTitle}>Description</Typography>
                    <Typography
                      className={classes.labelContent}
                      dangerouslySetInnerHTML={{ __html: nl2br(activeItem?.description) } as any}
                    />
                  </Fragment>
                )}
              </Box>
            </Hidden>

            {fichiersJoints.length > 0 && (
              <Box width="100%" marginTop="10px">
                <AttachedFiles files={fichiersJoints} />
              </Box>
            )}
            <Box width="100%" marginTop="10px">
              <RightContentComment
                listResult={[]}
                codeItem="INFORMATION_LIAISON"
                item={activeItem as any}
                refetch={() => refetch(false)}
              />
            </Box>
          </Box>
        ) : (
          <Box minHeight="calc(100vh - 162px)" width="100%" display="flex" alignItems="center" justifyContent="center">
            <NoItemContentImage
              title="Aperçu détaillé de vos informations"
              subtitle="Choisissez une information dans la liste de droite pour l'afficher en détails dans cette partie."
            />
          </Box>
        )}
      </Box>

      {activeItem && <ModalHistory info={activeItem} open={openModalHistory} setOpen={setOpenModalHistory} />}
    </Box>
  );
};

export default MainContainer;
