import { ILURLParams } from '../useILURLParams';
import { Filter } from '../util/types';

export const buildImportanceFilter = (params: ILURLParams): Filter => {
  if (params.importances) {
    return {
      must: [
        {
          terms: {
            'importance.ordre': params.importances,
          },
        },
      ],
    };
  }

  return {};
};
