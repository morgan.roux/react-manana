import { ILURLParams } from '../useILURLParams';

export const buildSort = (params: ILURLParams): any[] | undefined => {
  if (params.sorting) {
    return Object.keys(params.sorting).map((name) => ({
      [name]: {
        order: (params.sorting as any)[name],
      },
    }));
  }
  return undefined;
};
