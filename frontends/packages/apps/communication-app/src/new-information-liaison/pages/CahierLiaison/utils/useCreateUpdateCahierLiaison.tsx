import { useApplicationContext, useDisplayNotification } from '@lib/common';
import {
  Create_Update_Information_LiaisonMutationVariables,
  useCreate_Update_Information_LiaisonMutation,
} from '@lib/common/src/graphql';
import { useState } from 'react';

/**
 *  Create cahier liaison hooks
 */
const useCreateUpdateCahierLiaison = (
  values: Create_Update_Information_LiaisonMutationVariables,
  setOpenFormModal?: (value: boolean) => void,
  setLoading?: (value: boolean) => void
) => {
  // const {
  //   content: { variables, operationName },
  // } = useContext<ContentStateInterface>(ContentContext);

  const { graphql } = useApplicationContext();
  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);
  const displayNotification = useDisplayNotification();

  const [createUpdateCahierLiaison, { loading: mutationLoading }] = useCreate_Update_Information_LiaisonMutation({
    client: graphql,
    variables: {
      input: values.input,
    },
    update: (cache, { data }) => {
      if (data && data.createUpdateInformationLiaison && values && !values.input.id) {
        // if (variables && operationName) {
        //   const req: any = cache.readQuery({
        //     query: operationName,
        //     variables,
        //   });
        //   if (req && req.search && req.search.data) {
        //     cache.writeQuery({
        //       query: operationName,
        //       data: {
        //         search: {
        //           ...req.search,
        //           ...{
        //             total: req.search.total + 1,
        //             data: [data.createUpdateInformationLiaison, ...req.search.data],
        //           },
        //         },
        //       },
        //       variables,
        //     });
        //   }
        // }
      }
    },
    onCompleted: (data) => {
      if (data && data.createUpdateInformationLiaison) {
        displayNotification({
          type: 'success',
          message:
            (values.input.isRemoved && 'Suppression effectuée avec succès') ||
            `${values.input.id ? 'Modification' : 'Création'} réussie`,
        });
        setMutationSuccess(true);
        //client.writeData({ data: { checkedsCahierLiaison: null } });
        if (setOpenFormModal) setOpenFormModal(false);
        if (setLoading) setLoading(false);
      }
    },
    onError: (errors) => {
      console.log(errors);
      if (errors.graphQLErrors) {
        errors.graphQLErrors.map((err) => {
          displayNotification({ type: 'error', message: err.message });
        });

        if (setOpenFormModal) setOpenFormModal(false);
        if (setLoading) setLoading(false);
      }
    },
  });
  return { createUpdateCahierLiaison, mutationSuccess, mutationLoading };
};

export default useCreateUpdateCahierLiaison;
