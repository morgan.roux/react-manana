import { CAHIER_LIAISON_URL, strippedString, UrgenceImportanceBadge, useApplicationContext } from '@lib/common';
import {
  SearchInformationLiaisonInfoFragment,
  useGet_OriginesQuery,
  useInformation_Liaison_TypesQuery,
} from '@lib/common/src/federation';
import { ConfirmDeleteDialog, CustomAvatar } from '@lib/ui-kit';
import { Box, Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import {
  ChatBubble,
  Close,
  Delete,
  Edit,
  ErrorOutline,
  Event,
  MoreHoriz,
  Update,
  Visibility,
  VisibilityOff,
} from '@material-ui/icons';
import { Skeleton } from '@material-ui/lab';
import classnames from 'classnames';
import moment from 'moment';
import React, { CSSProperties } from 'react';
import { useHistory } from 'react-router';
import useStyles from '../../leftContainerItemStyles';
import useCahierLiaisonForm from '../CahierLiaisonForm/useCahierLiaisonForm';

interface ItemProps {
  style: CSSProperties;
  item?: SearchInformationLiaisonInfoFragment;
  activeItem: any;
  loading: boolean;
  refetch: any;
  handleClickItem: (event: any) => void;
}

export const Item = React.forwardRef<HTMLDivElement, ItemProps>(
  ({ item, activeItem, refetch, handleClickItem, loading: loadingSkeleton, style }, containerRef) => {
    const classes = useStyles();
    const { push } = useHistory();
    const isActive = item && activeItem && item.id === activeItem.id;
    const { isMobile } = useApplicationContext();
    const [openDeleteDialog, setOpenDeleteDialog] = React.useState<boolean>(false);
    const { deleteInformationLiaison, updateStatus, loading } = useCahierLiaisonForm({
      refetch,
      isActive,
      setOpenDeleteDialog,
    });

    const { user: currentUser, federation } = useApplicationContext();

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const open = Boolean(anchorEl);

    const currentUserId = currentUser && currentUser.id;
    const currentUserName = currentUser && currentUser.userName;

    const EDIT_URL = `${CAHIER_LIAISON_URL}/edit/${item && item.id}`;

    const informationLiaisonTypesQuery = useInformation_Liaison_TypesQuery({
      client: federation,
    });

    const originesQuery = useGet_OriginesQuery({
      client: federation,
    });

    const informationLiaisonTypes = informationLiaisonTypesQuery.data?.informationLiaisonTypes.nodes || [];
    const origines = originesQuery.data?.origines.nodes || [];

    const colleguesConcerneesIds: string[] =
      (item &&
        item.colleguesConcernees &&
        item.colleguesConcernees.length > 0 &&
        item.colleguesConcernees.map((i: any) => i.userConcernee.id)) ||
      [];

    const userCreationId = item?.createdBy?.id;

    const isCollegueConcerned: boolean = colleguesConcerneesIds.includes(currentUserId);

    const collegueConcernee =
      item &&
      item.colleguesConcernees &&
      item.colleguesConcernees.length > 0 &&
      item.colleguesConcernees.find((i: any) => i.userConcernee.id === currentUserId);

    const activeMenu = (): boolean => {
      if (item && currentUserId) {
        if (item.createdBy?.id === currentUserId || isCollegueConcerned) {
          return true;
        }
      }
      return false;
    };

    const activeEditAndDeleteMenu = (): boolean => {
      if (!item?.idItem && userCreationId === currentUserId) return true;
      return false;
    };

    const handleClickMenu = (event: React.MouseEvent<HTMLElement>) => {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
      setAnchorEl(null);
    };

    const handleClickEdit = (event: React.MouseEvent<HTMLLIElement, MouseEvent>) => {
      event.stopPropagation();
      handleClose();
      push(EDIT_URL);
    };

    const handleClickDelete = (event: React.MouseEvent<HTMLLIElement, MouseEvent>) => {
      event.stopPropagation();
      handleClose();
      setOpenDeleteDialog(true);
    };

    const handleClickUpdateStatus = (statut: string) => (event: React.MouseEvent<HTMLLIElement, MouseEvent>) => {
      event.stopPropagation();
      handleClose();
      if (item) {
        updateStatus({
          variables: { id: item.id, code: statut },
        });
      }
    };

    const onClickConfirmDelete = () => {
      if (item) {
        deleteInformationLiaison({ variables: { id: item.id } });
      }
    };

    const DeleteDialogContent = () => {
      return <span>Êtes-vous sur de vouloir supprimer ?</span>;
    };

    const menuItem = (
      <Menu
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
        // tslint:disable-next-line: jsx-no-lambda
        onClick={(event) => {
          event.stopPropagation();
        }}
      >
        <MenuItem onClick={handleClickEdit} disabled={!activeEditAndDeleteMenu()}>
          <ListItemIcon>
            <Edit fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit">Modifier</Typography>
        </MenuItem>
        <MenuItem onClick={handleClickDelete} disabled={!activeEditAndDeleteMenu()}>
          <ListItemIcon>
            <Delete fontSize="small" />
          </ListItemIcon>
          <Typography variant="inherit">Supprimer</Typography>
        </MenuItem>
        {((isCollegueConcerned && collegueConcernee && collegueConcernee.statut === 'NON_LUES') ||
          (item && currentUserId === userCreationId && item.statut === 'NON_LUES')) && (
          <MenuItem onClick={handleClickUpdateStatus('LUES')}>
            <ListItemIcon>
              <Update fontSize="small" />
            </ListItemIcon>
            <Typography variant="inherit">Changer le statut à Lue</Typography>
          </MenuItem>
        )}
        {((isCollegueConcerned && collegueConcernee && collegueConcernee.statut === 'LUES') ||
          (item && currentUserId === userCreationId && item.statut === 'LUES')) && (
          <MenuItem onClick={handleClickUpdateStatus('NON_LUES')}>
            <ListItemIcon>
              <ErrorOutline fontSize="small" />
            </ListItemIcon>
            <Typography variant="inherit">Changer le statut à Non Lue</Typography>
          </MenuItem>
        )}
      </Menu>
    );

    if (loadingSkeleton) {
      return (
        <div ref={containerRef} className={classes.root}>
          <Skeleton variant="circle" width={35} height={35} animation="wave" />
        </div>
      );
    }

    if (isMobile) {
      return (
        <>
          <Box className={classes.mobileRoot} onClick={handleClickItem} style={style}>
            <Box padding="16px" borderBottom="1.5px solid #E1E1E1">
              <Box display="flex" justifyContent="space-between">
                <Typography className={classes.title} color="primary">
                  {(item?.idOrigine && origines.find(({ id }) => id === item.idOrigine)?.libelle) || '-'}
                </Typography>
                <Box display="flex" alignItems="center">
                  <Typography className={classnames(classes.dateFromNow)}>
                    {(item?.createdAt && moment(item.createdAt).fromNow()) || '-'}
                  </Typography>
                  <IconButton
                    size="small"
                    aria-controls="fade-menu"
                    aria-haspopup="true"
                    onClick={handleClickMenu}
                    disabled={!activeMenu()}
                  >
                    <MoreHoriz />
                  </IconButton>
                  {menuItem}
                </Box>
              </Box>
              <Box>
                <Typography>
                  {item && item.titre?.length > 0
                    ? item.titre
                    : item && item.description.length > 50
                    ? `${strippedString(item.description.substring(0, 50))}...`
                    : strippedString(item?.description || '')}
                </Typography>
              </Box>
              <Box className={classes.spaceBetween} marginTop="16px">
                <Box display="flex" flexDirection="column">
                  <Typography className={classes.mobileLabel}>Date :</Typography>
                  <Typography className={classes.mobileLabelContent}>
                    {(item?.createdAt && moment(item.createdAt).format('DD/MM/YYYY')) || '-'}
                  </Typography>
                </Box>
                <Box>
                  {item?.importance && item?.urgence && (
                    <UrgenceImportanceBadge importanceOrdre={item.importance.ordre} urgenceCode={item.urgence.code} />
                  )}
                </Box>
              </Box>
            </Box>
            <Box padding="16px">
              <Box className={classes.spaceBetween}>
                <Box display="flex" flexDirection="column">
                  <Typography className={classes.mobileLabel}>Déclarant :</Typography>
                  <CustomAvatar
                    name={item?.declarant?.id === currentUserId ? currentUserName : item?.declarant?.fullName}
                  />
                </Box>
                <Box display="flex" flexDirection="column">
                  <Typography className={classes.mobileLabel}>Type :</Typography>
                  <Typography className={classes.mobileLabelContent}>
                    {(item?.idType && informationLiaisonTypes.find(({ id }) => id === item.idType)?.libelle) || '-'}
                  </Typography>
                </Box>
              </Box>
            </Box>
          </Box>
          <ConfirmDeleteDialog
            open={openDeleteDialog}
            setOpen={setOpenDeleteDialog}
            content={<DeleteDialogContent />}
            onClickConfirm={onClickConfirmDelete}
            isLoading={loading}
          />
        </>
      );
    }

    return (
      <>
        <Box
          className={isActive ? classnames(classes.root, classes.isActive) : classes.root}
          // tslint:disable-next-line: jsx-no-lambda
          onClick={handleClickItem}
          style={style}
        >
          <Box className={classnames(classes.flexRow)}>
            <Typography className={classes.title} color="primary">
              {(item?.idOrigine && origines.find(({ id }) => id === item.idOrigine)?.libelle) || '-'}
            </Typography>
            <Box display="flex" alignItems="center">
              <Typography className={classnames(classes.dateFromNow)}>
                {(item?.createdAt && moment(item.createdAt).fromNow()) || '-'}
              </Typography>
              <IconButton
                size="small"
                aria-controls="fade-menu"
                aria-haspopup="true"
                onClick={handleClickMenu}
                disabled={!activeMenu()}
              >
                <MoreHoriz />
              </IconButton>
            </Box>

            {menuItem}
          </Box>

          <Box
            justifyContent="space-between"
            marginBottom="8px"
            marginTop="8px"
            className={classnames(classes.flexRow, classes.titleContainer)}
          >
            <Typography className={classes.subTitle}>
              {item && item.titre.length > 0
                ? item.titre
                : item && item.description.length > 50
                ? `${strippedString(item.description.substring(0, 50))}...`
                : strippedString(item?.description || '')}
            </Typography>
            <Box display="flex" alignItems="center">
              <Event />
              <Typography className={classes.date}>
                {(item && item.createdAt && moment(item.createdAt).format('DD/MM/YYYY')) || '-'}
              </Typography>
            </Box>
          </Box>

          <Box display="flex" justifyContent="space-between">
            <Box>
              <Box display="flex" alignItems="center">
                <Typography className={classnames(classes.labelContent, classes.widthLabel69)}>Déclarant :</Typography>
                <Typography className={classnames(classes.labelContentValue, classes.lenghtValueName)}>
                  {item?.declarant?.id === currentUserId ? 'Moi' : item?.declarant?.fullName || '-'}
                </Typography>
              </Box>
              <Box display="flex" alignItems="center">
                <Typography className={classes.labelContent}>Type:</Typography>
                <Typography className={classes.labelContentValue}>
                  {(item?.idType && informationLiaisonTypes.find(({ id }) => id === item.idType)?.libelle) || '-'}
                </Typography>
              </Box>
            </Box>
            <Box>
              {item?.importance && item?.urgence && (
                <UrgenceImportanceBadge importanceOrdre={item.importance.ordre} urgenceCode={item.urgence.code} />
              )}
            </Box>
          </Box>
          <Box display="flex" justifyContent="space-between" marginTop="16px">
            <Box display="flex">
              <ChatBubble />
              <Typography>{item?.nbComment}</Typography>
            </Box>
            <Box>
              {item && item.nbCollegue > 0 && (
                <Box display="flex">
                  {isCollegueConcerned && collegueConcernee && collegueConcernee.statut === 'NON_LUES' ? (
                    <VisibilityOff />
                  ) : (
                    <Visibility />
                  )}
                  <Typography>{`${item.nbLue}/${item.nbCollegue}`}</Typography>
                </Box>
              )}
            </Box>
            <Box>
              {item?.statut === 'CLOTURE' && (
                <Box display="flex">
                  <Close />
                  <Typography>Cloturé</Typography>
                </Box>
              )}
            </Box>
          </Box>
        </Box>
        <ConfirmDeleteDialog
          open={openDeleteDialog}
          setOpen={setOpenDeleteDialog}
          content={<DeleteDialogContent />}
          onClickConfirm={onClickConfirmDelete}
          isLoading={loading}
        />
      </>
    );
  }
);
