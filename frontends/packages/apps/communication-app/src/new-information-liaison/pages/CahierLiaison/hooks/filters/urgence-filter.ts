import { ILURLParams } from '../useILURLParams';
import { Filter } from '../util/types';

export const buildUrgenceFilter = (params: ILURLParams): Filter => {
  if (params.urgences) {
    return {
      must: [
        {
          terms: {
            'urgence.code': params.urgences,
          },
        },
      ],
    };
  }

  return {};
};
