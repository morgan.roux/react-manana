import { makeStyles, Theme, createStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    infoSupContainer: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'center',
      marginTop: 20,
      padding: 10,
      backgroundColor: '#f2f2f2',
    },
    infoValue: {
      width: 'auto',
      marginRight: 10,
      cursor: 'pointer',
    },
    collegueConcerneeName: {
      width: '100%',
      '&:hover': {
        textDecoration: 'underline',
      },
    },
  }),
);

export default useStyles;
