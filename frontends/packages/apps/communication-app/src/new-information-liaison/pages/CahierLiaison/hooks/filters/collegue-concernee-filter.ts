import { MeUserInfoFragment } from '@lib/common/src/federation';
import { ILURLParams } from '../useILURLParams';
import { Filter } from '../util/types';

export const buildCollegueConcerneeFilter = (params: ILURLParams, user: MeUserInfoFragment): Filter => {
  const collegueConcerneesVariables = (
    params.collegueConcernees
      ? !params.collegueConcernees.some((collegueConcernee) => collegueConcernee.id === user.id)
        ? [{ id: user.id, userName: user.fullName }, ...params.collegueConcernees]
        : params.collegueConcernees
      : [{ id: user.id, userName: user.fullName }]
  )?.map((collegueConcernee) => collegueConcernee.id);

  const statutVariables = (params.statut || []).map((statut) => ({
    terms: {
      [`idUsersByStatus.${params.information}_${statut}`]: collegueConcerneesVariables,
    },
  }));

  if (params.information) {
    return {
      should: params.statut
        ? params.id && params.statut.includes('NON_LUES')
          ? [...statutVariables, { term: { _id: params.id } }]
          : statutVariables
        : [{ terms: { [`idUsersByStatus.${params.information}`]: collegueConcerneesVariables } }],
    };
  } else {
    return { should: [{ terms: { [`idUsersByStatus.RECUE`]: collegueConcerneesVariables } }] };
  }
};
