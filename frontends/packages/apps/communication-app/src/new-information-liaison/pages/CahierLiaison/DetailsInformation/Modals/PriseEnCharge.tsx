import { Dropzone, isMobile } from '@lib/common';
import { CustomButton, CustomDatePicker, CustomModal, NewCustomButton } from '@lib/ui-kit';
import { Box, CssBaseline, DialogActions, Hidden, TextField } from '@material-ui/core';
import React, { FC, useState } from 'react';
import useStyles from '../styles';

interface ChargeProps {
  open: boolean;
  setOpen: (value: boolean) => void;
  handleAddClickTakeCharge: () => void;
  stateTakeCharge: any;
  setStateTakeCharge: (arg0: any) => void;
  setSelectedFiles: (arg0: any) => void;
  selectedFiles: any;
}

export const initialState = {
  dateCharge: new Date(),
  description: '',
};

const PriseEnCharge: FC<ChargeProps> = (props) => {
  const {
    open,
    setOpen,
    handleAddClickTakeCharge,
    stateTakeCharge,
    setStateTakeCharge,
    selectedFiles,
    setSelectedFiles,
  } = props;
  const [dateheure, setdateheure] = useState<any>(new Date());
  const classes = useStyles({});

  const handleChangeDate = (name: string) => (date: any) => {
    if (date && name) {
      setdateheure(date);
      setStateTakeCharge((prevState: any) => ({ ...prevState, ['date']: date }));
    }
  };

  const formChange = (event: any) => {
    const { value, name } = event.target;
    setStateTakeCharge((prevState: any) => ({ ...prevState, [name]: value }));
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChangeData = () => {
    if (stateTakeCharge.date !== '') {
      handleAddClickTakeCharge();
      handleClose();
    }
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Clôturer l'information de liaison"
      withBtnsActions={false}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="md"
      className={classes.rootModal}
      fullWidth={true}
      centerBtns={isMobile() ? false : true}
      actionButton={'Clôturer'}
      fullScreen={isMobile() ? true : false}
    >
      <Box className={classes.boxwidth}>
        <Box className={classes.sectionDatePicker}>
          <CustomDatePicker
            label="Date de prise en charge"
            placeholder="00/00/00"
            onChange={handleChangeDate('dateheure')}
            required={true}
            value={dateheure}
            name="dateheure"
          />
        </Box>
        <Box className={classes.section}>
          <TextField
            id="task-comment"
            label="Détails"
            className={classes.inputText}
            variant="outlined"
            name="description"
            multiline={true}
            rows={4}
            onChange={formChange}
            value={stateTakeCharge.description}
          />
        </Box>
        <Dropzone
          contentText="Glissez et déposez ici votre fichier"
          selectedFiles={selectedFiles}
          setSelectedFiles={setSelectedFiles}
          multiple={true}
          acceptFiles="application/pdf"
        />
        <DialogActions>
          <CssBaseline />
          <Hidden mdDown={true} implementation="css">
            <NewCustomButton theme="transparent" size="large" className={classes.transformButton} onClick={handleClose}>
              Annuler
            </NewCustomButton>
          </Hidden>

          <NewCustomButton size="large" className={classes.transformButton} onClick={handleChangeData}>
            Clôturer
          </NewCustomButton>
        </DialogActions>
      </Box>
    </CustomModal>
  );
};
export default PriseEnCharge;
