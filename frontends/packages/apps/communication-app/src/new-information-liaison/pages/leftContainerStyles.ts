import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
    },
    leftContainer: {
      // flex: '0 0 408px',
      height: 'calc(100vh - 86px)',
      minWidth: 330,
      borderRight: `1px solid #E5E5E5`,
      [theme.breakpoints.down('md')]: {
        marginTop: 64,
        height: 'calc(100vh - 150px)',
        maxWidth: '100vw',
      },
    },
    leftContainerHeader: {
      justifyContent: 'space-between',
      '& .MuiFormControl-root': {
        marginBottom: 0,
      },
      '& .main-MuiButtonBase-root': {
        padding: '6px 16px',
      },
    },

    mainContainerHeader: {
      width: '100%',
      height: 76,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
      borderBottom: '1px solid #E5E5E5',
      padding: '0px 20px',
      position: 'sticky',
      top: 0,
      background: '#fff',
      zIndex: 1,
    },
    mainContainerHeaderBtns: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      '& > button:nth-child(2)': {
        marginLeft: 10,
      },
    },
    itemsContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      height: 'calc(100vh - 162px)',
      overflowY: 'auto',
      [theme.breakpoints.down('md')]: {
        height: 'calc(100vh - 270px)',
      },
    },
    list: {
      width: '100% !important',
      padding: '0px',
      marginBottom: 16,
    },
    autoSizer: {
      width: '100% !important',
    },
  })
);

export default useStyles;
