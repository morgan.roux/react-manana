import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: 400,
      display: 'flex',
      flexDirection: 'column',
      cursor: 'pointer',
      padding: '20px 15px 23px',
      border: '3px solid #fff',
      borderRadius: 6,
      borderBottom: '1px solid #E0E0E0',
    },
    title: {
      // color: theme.palette.secondary.main,
      fontSize: '0.875rem',
      fontWeight: 'bold',
      [theme.breakpoints.down('md')]: {
        fontFamily: 'Roboto',
        fontSize: 14,
        fontWeight: 'bolder',
      },
    },
    subTitle: {
      fontSize: '1rem',
      fontWeight: 'bold',
      color: '#212121',
      marginRight: theme.spacing(1.75),
      [theme.breakpoints.down('md')]: {
        fontSize: 14,
        fontFamily: 'Roboto',
        fontWeight: 'bolder',
      },
    },
    isActive: {
      borderWidth: 1,
      borderStyle: 'solid',
      borderColor: theme.palette.primary.main,
    },
    flexRow: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    titleContainer: {
      display: 'flex',
      alignItems: 'flex-start',
      paddingRight: theme.spacing(3),
    },
    dateFromNow: {
      minWidth: 'fit-content',
      color: '#9E9E9E',
      fontSize: '0.75rem',
      [theme.breakpoints.down('md')]: {
        marginRight: 8,
      },
    },
    stat: {
      display: 'flex',
      alignItems: 'center',
      fontSize: '0.875rem',
      color: '#616161',
      fontWeight: 'bold',
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        marginRight: 4,
        color: '#424242',
      },
    },
    date: {
      justifyContent: 'flex-start',
      fontSize: '0.875rem',
      color: '#616161',
      fontWeight: 'bold',
      '& .MuiSvgIcon-root': {
        fontSize: 18,
        marginRight: 6,
      },
    },
    labelContent: {
      fontSize: '0.75rem',
      color: '#616161',
    },
    labelContentValue: {
      fontSize: '0.75rem',
      fontWeight: 'bold',
      color: '#616161',
      marginLeft: theme.spacing(1),
    },
    widthLabel69: {
      width: 69,
    },
    widthLabel81: {
      width: 81,
    },
    spaceBetween: {
      display: 'flex',
      justifyContent: 'space-between',
      '& > .MuiBox-root': {
        width: '100%',
      },
    },
    mobileRoot: {
      margin: 16,
      borderRadius: 6,
      boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.1)',
      '& .MuiAvatar-root': {
        width: 24,
        height: 24,
      },
    },
    mobileLabel: {
      fontFamily: 'Roboto',
      fontSize: 12,
      color: 'rgba(158, 158, 158, 1)',
    },
    mobileLabelContent: {
      fontFamily: 'Roboto',
      fontSize: 14,
    },
  })
);

export default useStyles;
