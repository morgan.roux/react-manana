import { useApplicationContext } from '@lib/common';
import { useGet_OriginesQuery, useSearch_Information_LiaisonQuery } from '@lib/common/src/federation';
import { CustomModal, NewCustomButton as CustomButton } from '@lib/ui-kit';
import { CircularProgress, Typography } from '@material-ui/core';
import { ArrowForward } from '@material-ui/icons';
import { Pagination } from '@material-ui/lab';
import { differenceBy } from 'lodash';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import useCahierLiaisonForm from '../pages/CahierLiaison/CahierLiaisonForm/useCahierLiaisonForm';
import useStyles from './styles';

export interface PopupInfoLiaisonProps {
  open: boolean;
  setOpen: (open: boolean) => void;
}

const PopupInfoLiaison: FC<PopupInfoLiaisonProps & RouteComponentProps> = ({ open, setOpen, history: { push } }) => {
  const classes = useStyles({});
  const { currentGroupement, user, federation } = useApplicationContext();
  const { updateStatus } = useCahierLiaisonForm({ disabledNotify: true });
  const userId = user.id;

  const infoFromStorage = localStorage.getItem('infosBloquantes');

  let infosBloquantes: any[] = [];
  if (infoFromStorage) {
    infosBloquantes = JSON.parse(infoFromStorage);
  }

  const [info, setInfo] = React.useState<any>(null);
  const [filteredData, setFilteredData] = React.useState<any[]>([]);
  const [page, setPage] = React.useState<number>(0);

  const originesResult = useGet_OriginesQuery({
    client: federation,
  });
  const { data, loading } = useSearch_Information_LiaisonQuery({
    client: federation,
    variables: {
      index: 'informationliaisons',
      query: {
        query: {
          bool: {
            must: [
              { term: { supprime: false } },
              { term: { 'groupement.id': currentGroupement.id } },
              { term: { bloquant: true } },
              {
                terms: { 'idUsersByStatus.RECUE_NON_LUES': [userId] },
              },
              {
                script: {
                  script: {
                    source: "doc['colleguesConcernees.userConcernee.id'].length >= 1",
                    lang: 'painless',
                  },
                },
              },
            ],
          },
        },
      },
      sortBy: [{ createdAt: { order: 'desc' } }],
    },
    fetchPolicy: 'cache-and-network',
    onError: (error) => {
      setOpen(false);
    },
  });

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value - 1);
  };

  const goToInfo = async () => {
    const infos = (data && data.search && data.search.data) || [];
    localStorage.setItem('infosBloquantes', JSON.stringify(infos));
    await updateStatus({ variables: { id: info.id, code: 'LUES' } });
    push(`/intelligence-collective/cahier-de-liaison?statut=NON_LUES&information=RECUE&id=${info.id}`);
  };

  React.useEffect(() => {
    if (data?.search?.data?.length) {
      if (infosBloquantes && infosBloquantes.length > 0) {
        const diff = differenceBy(data.search.data, infosBloquantes, 'id');
        if (diff && diff.length > 0 && diff[page]) {
          setFilteredData(diff);
          setInfo(diff[page]);
        } else {
          setOpen(false);
        }
      } else if (data.search.data[page]) {
        setFilteredData(data.search.data);
        setInfo(data.search.data[page]);
      }
    } else if (!loading) {
      setOpen(false);
    }
  }, [data, page, loading]);

  if (loading) {
    return null;
  }

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={`Vous avez ${filteredData.length} information(s) bloquante(s)`}
      closeIcon={true}
      withBtnsActions={false}
      headerWithBgColor={true}
      fullWidth={true}
      maxWidth="md"
    >
      <div className={classes.popupActuRoot}>
        <div className={classes.actuInfoContainer}>
          <Typography className={classes.origine}>
            Origine :{' '}
            <span>
              {info && info.idOrigine
                ? (originesResult.data?.origines.nodes || []).find(({ id }) => id === info.idOrigine)?.libelle || '-'
                : '-'}
            </span>
            {loading && <CircularProgress size={20} />}
          </Typography>
          <Typography className={classes.description}>{info?.titre}</Typography>
          <CustomButton
            className={classes.btn}
            color="secondary"
            variant="outlined"
            endIcon={<ArrowForward />}
            onClick={goToInfo}
          >
            Consulter
          </CustomButton>
        </div>
        <Pagination
          count={filteredData.length}
          variant="outlined"
          color="primary"
          onChange={handleChange}
          showFirstButton={true}
          showLastButton={true}
        />
      </div>
    </CustomModal>
  );
};

export default withRouter(PopupInfoLiaison);
