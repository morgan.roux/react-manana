import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    popupActuRoot: {
      width: 550,
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    actuInfoContainer: {
      marginBottom: 20,
    },
    title: {
      fontWeight: 'bold',
      fontSize: 22,
      marginBottom: 24,
      color: theme.palette.common.black,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    description: {
      fontSize: 16,
      textAlign: 'justify',
      marginBottom: 15,
    },
    origine: {
      fontSize: 16,
      color: theme.palette.common.black,
      marginBottom: 15,
      '& > span': {
        fontWeight: 'bold',
      },
    },
    btn: {
      textTransform: 'none',
    },
  }),
);

export default useStyles;
