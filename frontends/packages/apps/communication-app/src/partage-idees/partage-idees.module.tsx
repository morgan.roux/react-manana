import React from 'react';
import loadable from '@loadable/component';
import { ModuleDefinition } from '@lib/common';
import { PsychologyWhite } from '@lib/ui-kit';

const partageIdeesModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_PARTAGE_IDEES',
      location: 'PARAMETRE',
      name: 'Partage idées/bonnes pratiques',
      to: '/partage-idee',
      icon: <PsychologyWhite color="inherit" />,
      preferredOrder: 110,
      authorize: {
        roles: ['SUPADM', 'GRPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
};

export default partageIdeesModule;
