import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import { ApolloProvider } from '@apollo/client';
import { initClient, NotificationSnackBar } from '@lib/common';
import module from './module';

const App = () => {
  return (
    <ApolloProvider client={initClient().federation}>
      <NotificationSnackBar />
      <HashRouter>
        <Switch>
          {module.routes?.map((route, index) => (
            <Route
              key={typeof route.path === 'string' ? route.path : `route-${index}`}
              path={route.path}
              component={route.component}
              exact={route.exact}
            />
          ))}
        </Switch>
      </HashRouter>
    </ApolloProvider>
  );
};

export default App;
