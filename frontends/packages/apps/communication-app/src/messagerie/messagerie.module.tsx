import {
  MESSAGE_DELETED_URL,
  MESSAGE_FORWARD_URL,
  MESSAGE_NEW_URL,
  MESSAGE_REPLY_ALL_URL,
  MESSAGE_REPLY_URL,
  MESSAGE_SENDED_URL,
  MESSAGE_URL,
  ModuleDefinition,
} from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';
import loadable from '@loadable/component';
import React from 'react';
import Messagerie from './assets/img/messagerie.png';
import MessagerieBannerButton from './components/MessagerieBannerButton/MessagerieBannerButton';
import { fetchMessagerieTotalCount } from './util/total-count';

const MessageriePage = loadable(() => import('./pages/Messagerie'), {
  fallback: <SmallLoading />,
});

const messagerieModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_MESSAGERIE',
      location: 'ACCUEIL',
      name: 'Messagerie',
      to: '/messagerie/inbox',
      icon: Messagerie,
      activationParameters: {
        groupement: '0317',
        pharmacie: '0720',
      },
      preferredOrder: 140,
      options: {
        totalCount: fetchMessagerieTotalCount,
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_MESSAGERIE_BANNIERE',
      location: 'BANNIERE',
      preferredOrder: 20,
      activationParameters: {
        groupement: '0317',
        pharmacie: '0720',
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
      component: <MessagerieBannerButton />,
    },
  ],
  routes: [
    {
      path: [
        MESSAGE_URL,
        MESSAGE_NEW_URL,
        MESSAGE_SENDED_URL,
        MESSAGE_DELETED_URL,
        `${MESSAGE_URL}/:messageId`,
        `${MESSAGE_SENDED_URL}/:messageId`,
        `${MESSAGE_DELETED_URL}/:messageId`,
        `${MESSAGE_REPLY_URL}/:source/:messageId`,
        `${MESSAGE_REPLY_ALL_URL}/:source/:messageId`,
        `${MESSAGE_FORWARD_URL}/:source/:messageId`,
      ],
      component: MessageriePage,
      exact: true,
      attachTo: 'MAIN',
    },
  ],
};

export default messagerieModule;
