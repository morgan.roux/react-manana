import { useApplicationContext } from '@lib/common';
import { Badge, IconButton } from '@material-ui/core';
import { Email } from '@material-ui/icons';
import React, { FC } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useHistory } from 'react-router';
import { fetchMessagerieTotalCount } from '../../util/total-count';
import { useStyles } from './styles';

const TodoBannerButton: FC<{}> = () => {
  const classes = useStyles({});
  const history = useHistory();
  const { user, graphql } = useApplicationContext();
  const [totalCount, setTotalCount] = useState<number>(0);

  useEffect(() => {
    fetchMessagerieTotalCount({ user, graphql }).then(setTotalCount);
  }, []);
  return (
    <IconButton
      aria-haspopup="true"
      onClick={() => history.push('/messagerie/inbox')}
      aria-label={`show ${totalCount} new messages`}
      className={classes.iconBtn}
    >
      <Badge badgeContent={totalCount} max={999} color="error">
        <Email />
      </Badge>
    </IconButton>
  );
};

export default TodoBannerButton;
