import { ApolloQueryResult } from '@apollo/client';
import { useApolloClient } from '@apollo/client';
import {
  MESSAGE_DELETED_URL,
  MESSAGE_NEW_URL,
  MESSAGE_SENDED_URL,
  useApplicationContext,
  useDisplayNotification,
} from '@lib/common';
import {
  MessagerieInfoFragment,
  MessageriesDocument,
  MessageriesQuery,
  MessageriesQueryVariables,
  MessagerieType,
  Total_Messagerie_Non_LusQuery,
  useDelete_MessagesMutation,
} from '@lib/common/src/graphql';
import { Backdrop, ConfirmDeleteDialog } from '@lib/ui-kit';
import { Box, Button, Divider, IconButton, Typography } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { differenceBy } from 'lodash';
import React, { Dispatch, FC, Fragment, SetStateAction } from 'react';
import { useLocation } from 'react-router';
import Filter from './Filter';
import Message from './Message';
import useStyles from './styles';

export interface ListeMessageProps {
  messageList: any[];
  message: MessagerieInfoFragment | null;
  setMessage: Dispatch<SetStateAction<MessagerieInfoFragment | null>>;
  currentUserId: string;
  queryVariables: MessageriesQueryVariables;
  setQueryVariables: Dispatch<SetStateAction<MessageriesQueryVariables>>;
  selectedMsg: any[];
  setSelectedMsg: Dispatch<SetStateAction<any[]>>;
  refetchUnreadMsg: (
    variables?: Record<string, any> | undefined
  ) => Promise<ApolloQueryResult<Total_Messagerie_Non_LusQuery>>;
}

export const ListeMessage: FC<ListeMessageProps> = ({
  messageList,
  message,
  setMessage,
  currentUserId,
  queryVariables,
  selectedMsg,
  setSelectedMsg,
  setQueryVariables,
  refetchUnreadMsg,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef<HTMLButtonElement>(null);

  const { graphql } = useApplicationContext();
  const { pathname } = useLocation();
  const displayNotification = useDisplayNotification();

  const [openDialog, setOpenDialog] = React.useState<boolean>(false);

  const isOnNew = pathname === MESSAGE_NEW_URL;
  const isOnSended = pathname === MESSAGE_SENDED_URL || pathname.includes(MESSAGE_SENDED_URL);
  const isOnDeleted = pathname === MESSAGE_DELETED_URL || pathname.includes(MESSAGE_DELETED_URL);

  const title = isOnNew || isOnSended ? 'Éléments envoyés' : isOnDeleted ? 'Éléments supprimés' : 'Boîte de réception';

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const onClickDelete = () => {
    if (selectedMsg.length > 0) {
      setOpenDialog(true);
    }
  };

  /**
   * Mutation delete message
   */
  const [doDeleteMsg, { loading: deleteLoading }] = useDelete_MessagesMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.deleteMessages) {
        const req = cache.readQuery<MessageriesQuery, MessageriesQueryVariables>({
          query: MessageriesDocument,
          variables: queryVariables,
        });
        if (req && req.messageries && req.messageries.data) {
          const newData: any[] = differenceBy(req.messageries.data, data.deleteMessages, 'id');
          cache.writeQuery({
            query: MessageriesDocument,
            data: { messageries: { ...req.messageries, ...{ data: newData } } },
            variables: queryVariables,
          });
        }
      }
    },
    onCompleted: (data) => {
      if (data && data.deleteMessages) {
        displayNotification({
          type: 'success',
          message: 'Message(s) supprimé(s) avec succès',
        });
        setMessage(null);
        setSelectedMsg([]);
        if (refetchUnreadMsg) {
          refetchUnreadMsg();
        }
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const onConfirmDelete = () => {
    if (selectedMsg.length > 0) {
      setOpenDialog(false);
      const ids = selectedMsg.map((i) => i && i.id);

      const type: MessagerieType = pathname.includes('inbox') ? MessagerieType.R : MessagerieType.E;
      doDeleteMsg({
        variables: { ids, typeMessagerie: type, permanent: isOnDeleted ? true : false },
      });
    }
  };

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current!.focus();
    }

    prevOpen.current = open;
  }, [open]);

  return (
    <Box className={classes.root}>
      {deleteLoading && <Backdrop value="Suppression en cours..." />}
      <Box className={classes.appBar}>
        <Typography className={classes.bold}>{title}</Typography>
        <Box display="flex">
          <Box display="flex">
            <Button
              className={classes.expendMore}
              ref={anchorRef}
              aria-controls={open ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={handleToggle}
              endIcon={<ExpandMoreIcon />}
            >
              Trier par
            </Button>
            <Filter open={open} setOpen={setOpen} setQueryVariables={setQueryVariables} anchorRef={anchorRef} />
          </Box>

          <Fragment>
            <Divider orientation="vertical" flexItem={true} />
            <IconButton aria-label="delete" className={classes.margin} onClick={onClickDelete}>
              <DeleteIcon fontSize="small" />
            </IconButton>
          </Fragment>
        </Box>
      </Box>
      <Message
        messageList={messageList}
        message={message}
        setMessage={setMessage}
        currentUserId={currentUserId}
        selectedMsg={selectedMsg}
        setSelectedMsg={setSelectedMsg}
        queryVariables={queryVariables}
        setQueryVariables={setQueryVariables}
        refetchUnreadMsg={refetchUnreadMsg}
      />
      <ConfirmDeleteDialog
        open={openDialog}
        setOpen={setOpenDialog}
        onClickConfirm={onConfirmDelete}
        content={
          isOnDeleted
            ? 'Voulez-vous supprimer ce(s) message(s) définitivement?'
            : 'Êtes-vous sûr de vouloir supprimer ces messages ?'
        }
      />
    </Box>
  );
};

export default ListeMessage;
