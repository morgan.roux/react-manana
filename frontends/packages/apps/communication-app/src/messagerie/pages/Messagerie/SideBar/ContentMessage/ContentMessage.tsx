import {
  capitalizeFirstLetter,
  MESSAGE_DELETED_URL,
  MESSAGE_FORWARD_URL,
  MESSAGE_REPLY_ALL_URL,
  MESSAGE_REPLY_URL,
  MESSAGE_SENDED_URL,
  MESSAGE_URL,
  useApplicationContext,
  useDisplayNotification,
} from '@lib/common';
import {
  MessagerieInfoFragment,
  MessageriesDocument,
  MessageriesQuery,
  MessageriesQueryVariables,
  MessagerieType,
  useDelete_MessagesMutation,
  useRestore_MessagesMutation,
} from '@lib/common/src/graphql';
import { Backdrop, ConfirmDeleteDialog, ConfirmDialog } from '@lib/ui-kit';
import { Typography, Box, Button, Divider, CssBaseline, Tooltip, Hidden, IconButton } from '@material-ui/core';
import { Delete, RestoreFromTrash } from '@material-ui/icons';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import LensIcon from '@material-ui/icons/Lens';
import PdfIcon from '@material-ui/icons/PictureAsPdf';
import ReplyIcon from '@material-ui/icons/Reply';
import ReplyAllIcon from '@material-ui/icons/ReplyAll';
import classnames from 'classnames';
import moment from 'moment';
import React, { Dispatch, FC, Fragment, SetStateAction } from 'react';
import { useHistory, useLocation } from 'react-router';
import useStyles from './styles';

export interface ContentMessageProps {
  message: MessagerieInfoFragment | null;
  setMessage: Dispatch<SetStateAction<MessagerieInfoFragment | null>>;
  queryVariables: MessageriesQueryVariables;
  selectedMsg: any[];
  setSelectedMsg: Dispatch<SetStateAction<any[]>>;
  refetchAll?: any;
  setShowMainContent?: (value: boolean) => void;
  setShowFormMessage?: (value: boolean) => void;
}

export const ContentMessage: FC<ContentMessageProps> = ({
  message,
  setMessage,
  queryVariables,
  selectedMsg,
  setSelectedMsg,
  refetchAll,
  setShowMainContent,
  setShowFormMessage,
}) => {
  const classes = useStyles({});

  const { pathname } = useLocation();
  const { push } = useHistory();
  const { graphql } = useApplicationContext();

  const displayNotification = useDisplayNotification();

  const isOnMailbox = pathname === MESSAGE_URL || pathname.includes(MESSAGE_URL);
  // const isOnNewMsg = pathname === MESSAGE_NEW_URL || pathname.includes(MESSAGE_NEW_URL);
  const isOnSended = pathname === MESSAGE_SENDED_URL || pathname.includes(MESSAGE_SENDED_URL);
  const isOnDeleted = pathname === MESSAGE_DELETED_URL || pathname.includes(MESSAGE_DELETED_URL);

  const urlSource: string = isOnSended ? 'sent' : isOnDeleted ? 'trash' : 'inbox';

  const [openDeleteDialog, setOpenDeleteDialog] = React.useState<boolean>(false);

  const nl2br = (str: string | null): string | null => {
    return str ? str.replace(new RegExp('\n', 'g'), '<br/>') : str;
  };

  const [openConfirm, setopenConfirm] = React.useState<boolean>(false);
  const onClickDelete = () => {
    console.log('message :>> ', message);
    if (message) {
      setOpenDeleteDialog(true);
    }
  };

  const onClickRestore = () => {
    if (message) {
      setopenConfirm(true);
    }
  };
  /**
   * Mutation delete message
   */
  const [doDeleteMsg, { loading: deleteLoading }] = useDelete_MessagesMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.deleteMessages) {
        const req = cache.readQuery<MessageriesQuery, MessageriesQueryVariables>({
          query: MessageriesDocument,
          variables: queryVariables,
        });
        if (req && req.messageries && req.messageries.data) {
          cache.writeQuery({
            query: MessageriesDocument,
            data: {
              messageries: {
                ...req.messageries,
                ...{
                  data: [
                    ...req.messageries.data.filter(
                      (m) => m && data.deleteMessages && data.deleteMessages.some((i) => i && m.id !== i.id)
                    ),
                  ],
                },
              },
            },
            variables: queryVariables,
          });
        }
      }
    },
    onCompleted: (data) => {
      if (data && data.deleteMessages) {
        displayNotification({
          type: 'success',
          message: 'Message supprimé avec succès',
        });

        setMessage(null);

        if (selectedMsg.length > 0) {
          const newSelected = selectedMsg.filter(
            (i) => data && data.deleteMessages && data.deleteMessages.some((d) => i && d && i.id !== d.id)
          );
          setSelectedMsg(newSelected);
        }

        if (refetchAll) refetchAll();
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const [doRestoreMessage, { loading: restoreMessageLoading }] = useRestore_MessagesMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.restoreMessages) {
        const req = cache.readQuery<MessageriesQuery, MessageriesQueryVariables>({
          query: MessageriesDocument,
          variables: queryVariables,
        });
        if (req && req.messageries && req.messageries.data) {
          cache.writeQuery({
            query: MessageriesDocument,
            data: {
              messageries: {
                ...req.messageries,
                ...{
                  data: [
                    ...req.messageries.data.filter(
                      (m) => m && data.restoreMessages && data.restoreMessages.some((i) => i && m.id !== i.id)
                    ),
                  ],
                },
              },
            },
            variables: queryVariables,
          });
        }
      }
    },
    onCompleted: (data) => {
      if (data && data.restoreMessages) {
        displayNotification({
          type: 'success',
          message: 'Message restauré avec succès',
        });

        setMessage(null);
        setopenConfirm(false);

        if (selectedMsg.length > 0) {
          const newSelected = selectedMsg.filter(
            (i) => data && data.restoreMessages && data.restoreMessages.some((d) => i && d && i.id !== d.id)
          );
          setSelectedMsg(newSelected);
        }

        if (refetchAll) refetchAll();

        /*  console.log('++++++', isOnMailbox, MESSAGE_URL);
        console.log('++++++', isOnSended, MESSAGE_SENDED_URL);
        console.log('++++++', isOnDeleted, MESSAGE_DELETED_URL);
        if (isOnMailbox) push(`${MESSAGE_URL}`);
        if (isOnSended) push(`${MESSAGE_SENDED_URL}`);
        if (isOnDeleted) push(`${MESSAGE_DELETED_URL}`); */
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const onConfirmDelete = () => {
    if (message) {
      setOpenDeleteDialog(false);

      doDeleteMsg({
        variables: {
          ids: [message.id],
          typeMessagerie: message.typeMessagerie,
          permanent: isOnDeleted ? true : false,
        },
      });
    }
  };

  const confirmRestore = () => {
    console.log('Message >>>>>>>>>>', message);
    if (message) {
      doRestoreMessage({ variables: { ids: [message.id] } });
    }
  };

  const onClickReply = () => {
    if (setShowMainContent) setShowMainContent(false);
    if (setShowFormMessage) setShowFormMessage(true);
    if (message) push(`${MESSAGE_REPLY_URL}/${urlSource}/${message.id}`);
  };

  const onClickReplyAll = () => {
    if (setShowMainContent) setShowMainContent(false);
    if (setShowFormMessage) setShowFormMessage(true);
    if (message) push(`${MESSAGE_REPLY_ALL_URL}/${urlSource}/${message.id}`);
  };

  const onClickForward = () => {
    if (setShowMainContent) setShowMainContent(false);
    if (setShowFormMessage) setShowFormMessage(true);
    if (message) push(`${MESSAGE_FORWARD_URL}/${urlSource}/${message.id}`);
  };

  const onClickAttachement = (url: string) => () => {
    window.open(url);
  };

  const recepteurs: any[] = message
    ? (message.recepteurs &&
        message.recepteurs.length &&
        message.recepteurs.map(
          (recepteur) => recepteur && recepteur.userRecepteur && capitalizeFirstLetter(recepteur.userRecepteur.userName)
        )) ||
      []
    : [];

  return (
    <Box className={classes.root}>
      <CssBaseline />
      {deleteLoading || (restoreMessageLoading && <Backdrop value="Suppression en cours..." />)}
      {message && (
        <>
          <Box className={classes.appBar}>
            <Box display="flex" zIndex="1">
              {!isOnDeleted ? (
                <Box>
                  <Tooltip title="Répondre" placement="bottom">
                    <Button className={classes.expendMore} startIcon={<ReplyIcon />} onClick={onClickReply}>
                      <Typography>Répondre</Typography>
                    </Button>
                  </Tooltip>
                  <Tooltip title="Répondre à tous" placement="bottom">
                    <Button className={classes.expendMore} startIcon={<ReplyAllIcon />} onClick={onClickReplyAll}>
                      <Typography> Répondre à tous</Typography>
                    </Button>
                  </Tooltip>
                  <Tooltip title="Transférer" placement="bottom">
                    <Button className={classes.expendMore} startIcon={<ArrowForwardIcon />} onClick={onClickForward}>
                      <Typography> Transférer</Typography>
                    </Button>
                  </Tooltip>
                </Box>
              ) : (
                <Tooltip title="Restaurer" placement="bottom">
                  <Button className={classes.expendMore} startIcon={<RestoreFromTrash />} onClick={onClickRestore}>
                    <Typography> Restaurer</Typography>
                  </Button>
                </Tooltip>
              )}
            </Box>

            <Hidden smDown={true} implementation="css">
              <Fragment>
                <Divider orientation="vertical" flexItem={true} />
                <Tooltip title="Supprimer" placement="bottom">
                  <IconButton aria-label="delete" className={classes.margin} onClick={onClickDelete}>
                    <Delete fontSize="small" />
                  </IconButton>
                </Tooltip>
              </Fragment>
            </Hidden>
          </Box>
          <Box className={classes.contentDetails}>
            <Box display="flex" alignItems="start" justifyContent="space-between" marginBottom="12px">
              <Box className={classes.paddingSide}>
                <Typography className={classes.titleObjet}>Objet Mail : {message.objet}</Typography>
                <Typography className={classes.titleSender}>
                  De :{' '}
                  {queryVariables && queryVariables.typeMessagerie == MessagerieType.E
                    ? 'Moi'
                    : (message.userEmetteur && message.userEmetteur.userName) || ''}
                </Typography>
                {recepteurs.length ? <Box className={classes.nomRecept}> À: {recepteurs.join(' , ')}</Box> : ''}
              </Box>
              <Box display="flex" flexDirection="column" className={classes.paddingSide}>
                <Hidden mdDown={true} implementation="css">
                  <Box className={classes.iconFilter}>
                    <LensIcon />
                  </Box>
                  <Box className={classes.dateTime}>
                    <Typography style={{ lineHeight: '1.8em' }}>
                      {message.dateCreation ? moment(message.dateCreation).format('Do MMMM') : ''}
                    </Typography>
                    <Typography>{message.dateCreation ? moment(message.dateCreation).format('LT') : ''}</Typography>
                  </Box>
                </Hidden>
                <Hidden mdUp={true} implementation="css">
                  <Typography className={classes.dateCreation}>
                    {moment(message.dateCreation).format('DD MMM HH:mm')}
                  </Typography>
                </Hidden>
              </Box>
            </Box>
            <Divider />
            <Box className={classnames(classes.contentPieceJointe, classes.paddingSide)}>
              <Typography className={classes.titlePiece}>Pièces Jointes :</Typography>
              {message.attachments && message.attachments.length > 0 && (
                <Box className={classes.contentitemPieceJointe}>
                  {message.attachments.map((att, index) => (
                    <Box
                      className={classes.itemPieceJointe}
                      key={`att_${index}`}
                      onClick={onClickAttachement((att && att.fichier && att.fichier.publicUrl) || '')}
                    >
                      <Box className={classes.iconPdf}>
                        <PdfIcon />
                      </Box>
                      <Box>
                        <Typography>
                          <a href={(att && att.fichier && att.fichier.publicUrl) || ''} target="_blank">
                            {(att && att.fichier && att.fichier.nomOriginal) || ''}
                          </a>
                        </Typography>
                        {/* TODO */}
                        {/* <Typography>{pieceJointes.taille}</Typography> */}
                      </Box>
                    </Box>
                  ))}
                </Box>
              )}
            </Box>
            <Divider />
            <Box marginTop="32px" className={classnames(classes.text, classes.paddingSide)}>
              <Typography>
                <Box
                  dangerouslySetInnerHTML={
                    {
                      __html: nl2br(message.message || ''),
                    } as any
                  }
                />
              </Typography>
            </Box>
          </Box>
        </>
      )}

      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={onConfirmDelete}
        content={
          isOnDeleted
            ? 'Voulez-vous supprimer ce(s) message(s) définitivement?'
            : 'Êtes-vous sûr de vouloir supprimer ces messages ?'
        }
      />
      <ConfirmDialog
        open={openConfirm}
        message={'Voulez-vous restaurer ce message?'}
        handleClose={setopenConfirm}
        handleValidate={confirmRestore}
      />
    </Box>
  );
};

export default ContentMessage;
