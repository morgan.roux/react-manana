import {
  ADMINISTRATEUR_GROUPEMENT,
  formatBytes,
  isMobile,
  MESSAGE_FORWARD_URL,
  MESSAGE_NEW_URL,
  MESSAGE_REPLY_ALL_URL,
  MESSAGE_REPLY_URL,
  MobileTopBar,
  SUPER_ADMINISTRATEUR,
  useApplicationContext,
  useUploadFiles,
} from '@lib/common';
import { UserSelect } from '@lib/common/src/components/User/UserSelect';
import {
  Create_Update_MessagerieMutationVariables,
  FichierInput,
  MessageriesDocument,
  MessageriesQuery,
  MessageriesQueryVariables,
  MessagerieType,
  Search_UsersQueryVariables,
  useCreate_Update_MessagerieMutation,
  useMessagerieLazyQuery,
  useMessagerie_ThemesQuery,
  useSearch_UsersLazyQuery,
} from '@lib/common/src/graphql';
import { Autocomplete, Backdrop, CustomAlertDonneesMedicales, CustomModal } from '@lib/ui-kit';
import {
  Box,
  Button,
  CssBaseline,
  debounce,
  Divider,
  Hidden,
  IconButton,
  InputAdornment,
  TextField,
  Typography,
} from '@material-ui/core';
import { AddCircle, Brush, Close, Delete, PictureAsPdf, Send, SupervisorAccount } from '@material-ui/icons';
import { AxiosResponse } from 'axios';
import _ from 'lodash';
import { uniqBy } from 'lodash';
import React, { ChangeEvent, Dispatch, FC, SetStateAction, useRef } from 'react';
import { useDropzone } from 'react-dropzone';
import { useHistory, useLocation, useParams } from 'react-router';
import useStyles from './styles';
import { initialState, MessageInterface } from './useMessageForm';
import ReactQuill from 'react-quill';
export interface FormMessageProps {
  values: MessageInterface;
  setValues: Dispatch<SetStateAction<MessageInterface>>;
  queryVariables: MessageriesQueryVariables;
  currentUserId: string;
  onChangeInput: (e: ChangeEvent<any>) => void;
  onChangeAutocomplete: (name: string) => (e: ChangeEvent<any>, value: any) => void;
  openUserModal?: boolean;
  setOpenUserModal?: (ope: boolean) => void;
  onClickBack?: () => void;
  handleDrawerToggle?: () => void;
}

export interface IFormProps {
  handleChangeMessageContent: (content: string, name: string) => void;
}
const FormMessage: FC<FormMessageProps & IFormProps> = ({
  values,
  setValues,
  onChangeInput,
  onChangeAutocomplete,
  queryVariables,
  currentUserId,
  openUserModal,
  setOpenUserModal,
  handleChangeMessageContent,
  handleDrawerToggle,
  onClickBack,
}) => {
  const classes = useStyles({});
  const { currentGroupement: grp } = useApplicationContext();
  const idGrp = grp.id;

  const { objet, message, id, attachments, recepteursIds, sourceId, themeId, typeMessagerie, recepteurs, theme } =
    values;

  let uploadResult: AxiosResponse<any> | null = null;
  const { pathname } = useLocation();
  const files: FichierInput[] = [];
  const { messageId }: any = useParams();
  const { push } = useHistory();
  const { user: currentUser, currentPharmacie, graphql, isGroupePharmacie, notify } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const isOnReply = pathname.includes(MESSAGE_REPLY_URL);
  const isOnReplyAll = pathname.includes(MESSAGE_REPLY_ALL_URL);
  const isOnForward = pathname.includes(MESSAGE_FORWARD_URL);
  const isOnNewMsg = pathname === MESSAGE_NEW_URL || pathname.includes(MESSAGE_NEW_URL);

  const [loading, setLoading] = React.useState<boolean>(false);
  const [userIds, setUserIds] = React.useState<string[]>([]);
  const [userRecepteurs, setUserRecepteurs] = React.useState<any[]>([]);
  const [filtersModal, setFiltersModal] = React.useState<string[]>(['ALL']);

  const defaultMustNot = [{ term: { _id: currentUserId } }];
  const defaultShould = [{ term: { idGroupement: idGrp } }];
  const [must, setMust] = React.useState<any[]>([]);
  const [mustNot, setMustNot] = React.useState<any[]>(defaultMustNot);
  const [should, setShould] = React.useState<any[]>(defaultShould);

  const [oldFiles, setOldFiles] = React.useState<FichierInput[]>([]);

  const getThemes = useMessagerie_ThemesQuery({ client: graphql });

  const themes =
    getThemes && getThemes.data && getThemes.data.messagerieThemes && getThemes.data.messagerieThemes.length
      ? getThemes.data.messagerieThemes
      : [];

  const sUsersVar: Search_UsersQueryVariables = {
    take: 5,
    query: {
      query: {
        bool: {
          must_not: mustNot,
          should: should,
          must: must,
          minimum_should_match: 1,
        },
      },
    },
  };

  // Get users
  const [searchUsers, { data: userData, loading: userLoading }] = useSearch_UsersLazyQuery({
    client: graphql,
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  const users: any[] =
    (userData &&
      userData.search &&
      userData.search.data &&
      userData.search.data.map((user: any) => {
        return (
          user && {
            email: user.email,
            id: user.id,
            role: user.role,
            status: user.status,
            userName: user.userName,
            __typename: user?.__typename,
          }
        );
      })) ||
    [];

  React.useEffect(() => {
    setUserIds(recepteursIds);
  }, [recepteursIds]);

  React.useEffect(() => {
    setUserRecepteurs(recepteurs);
  }, [recepteurs]);

  React.useEffect(() => {
    if (queryVariables) {
      switch (queryVariables.typeFilter) {
        case 'MY_PHARMACIE':
          if (isGroupePharmacie && currentPharmacie.id) {
            setMustNot(defaultMustNot);
            setMust([
              {
                term: {
                  'pharmacie.id': currentPharmacie.id,
                },
              },
            ]);
          } else {
            setMust([
              {
                term: {
                  email: '',
                },
              },
            ]);
          }
          setFiltersModal(['PHARMACIE', 'MY_PHARMACIE']);
          break;
        case 'OTHER_PHARMACIE':
          if (isGroupePharmacie && currentPharmacie.id) {
            setMustNot([
              ...defaultMustNot,
              ...[
                {
                  term: {
                    'pharmacie.id': currentPharmacie.id,
                  },
                },
              ],
            ]);
            setMust([
              {
                exists: {
                  field: 'pharmacie.id',
                },
              },
            ]);
          } else {
            setMust([
              {
                term: {
                  email: '',
                },
              },
            ]);
          }
          setFiltersModal(['PHARMACIE', 'OTHER_PHARMACIE']);
          break;
        case 'MY_REGION':
          if (isGroupePharmacie && currentPharmacie.departement?.region.id) {
            setMustNot(defaultMustNot);
            setMust([
              {
                term: {
                  'pharmacie.departement.region.id': currentPharmacie.departement.region.id,
                },
              },
            ]);
          } else {
            setMust([
              {
                term: {
                  email: '',
                },
              },
            ]);
          }
          setFiltersModal(['PHARMACIE', 'MY_REGION']);
          break;
        case 'MY_GROUPEMENT':
          setMustNot(defaultMustNot);
          setMust([
            {
              exists: {
                field: 'userPersonnel.id',
              },
            },
          ]);
          setFiltersModal(['MY_GROUPEMENT']);
          break;
        case 'LABORATOIRE':
          setMustNot(defaultMustNot);
          setMust([
            {
              exists: {
                field: 'userLaboratoire.id',
              },
            },
          ]);
          setFiltersModal(['LABORATOIRE']);
          break;
        case 'PARTENAIRE_SERVICE':
          setMustNot(defaultMustNot);
          setMust([
            {
              exists: {
                field: 'userPartenaire.id',
              },
            },
          ]);
          setFiltersModal(['PARTENAIRE_SERVICE']);
          break;
        default:
          setMustNot(defaultMustNot);
          setMust([]);
          setFiltersModal(['ALL']);
          break;
      }
    }
  }, [queryVariables]);

  // Get message on reply, replyAll, forward
  const [getMessage, { data: msgData, loading: msgLoading }] = useMessagerieLazyQuery({
    client: graphql,
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  const onOpenUserAutocomplete = () => {
    searchUsers({ variables: sUsersVar });
  };

  const debouncedSearchUser = useRef(
    debounce((text: string, usersVar: any) => {
      searchUsers({
        variables: {
          ...usersVar,
          filterBy: [{ wildcard: { userName: `*${text}*` } }],
          take: null,
        },
      });
    }, 1000)
  );

  const onChangeUserTxt = (name: string) => (e: ChangeEvent<any>, value: string) => {
    debouncedSearchUser.current(value, sUsersVar);
  };

  const onOpenThemeAutocomplete = () => {
    searchUsers({ variables: sUsersVar });
  };

  const onChangeThemeTxt = (name: string) => (e: ChangeEvent<any>, value: string) => {
    // debouncedSearchUser.current(value);
  };

  const onDrop = (acceptedFiles: File[]) => {
    //('acceptedFiles>>>>>>>>>>>', acceptedFiles);
    setValues((prevState) => ({
      ...prevState,
      selectedFiles: [...selectedFiles, ...acceptedFiles],
    }));
  };
  //('selectedFiles>>>>>>>', values.selectedFiles);

  const removeFile = (file: File) => () => {
    const newFiles = [...selectedFiles];
    newFiles.splice(newFiles.indexOf(file), 1);
    acceptedFiles.splice(acceptedFiles.indexOf(file), 1);
    setValues((prevState) => ({
      ...prevState,
      selectedFiles: newFiles,
    }));
  };

  const initValues = () => {
    setValues(initialState);
  };

  const { getRootProps, getInputProps, open, acceptedFiles } = useDropzone({
    // Disable click and keydown behavior
    noClick: true,
    noKeyboard: true,
    multiple: true,
    onDrop,
  });

  const sendMessageVar: Create_Update_MessagerieMutationVariables = {
    inputs: {
      id,
      message,
      objet,
      recepteursIds,
      attachments,
      sourceId,
      themeId,
      typeMessagerie: typeMessagerie || MessagerieType.R,
    },
  };

  // Do create message
  const [sendMessage, { loading: messageLoading }] = useCreate_Update_MessagerieMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.createUpdateMessagerie) {
        const req = cache.readQuery<MessageriesQuery, MessageriesQueryVariables>({
          query: MessageriesDocument,
          variables: queryVariables,
        });
        if (req && req.messageries && req.messageries.data) {
          cache.writeQuery({
            query: MessageriesDocument,
            data: {
              messageries: {
                ...req.messageries,
                ...{ data: [...req.messageries.data, ...[data.createUpdateMessagerie]] },
              },
            },
            variables: queryVariables,
          });
        }
      }
    },
    onCompleted: (data) => {
      if (data && data.createUpdateMessagerie) {
        // Init form state
        initValues();
        if (isMobile()) {
          push('/messagerie/sent');
          if (onClickBack) {
            onClickBack();
          }
        }

        notify({
          type: 'success',
          message: 'Message envoyé avec succès',
        });
        setLoading(false);
      }
    },
    onError: (error) => {
      setLoading(false);
      error.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  const { selectedFiles } = values;
  //('Somewhere SELECTED FILES', selectedFiles);

  const onClickSend = () => {
    setLoading(true);
    if (recepteursIds.length > 0 && objet && message) {
      if (selectedFiles.length > 0) {
        uploadFiles(selectedFiles, {
          directory: 'messagerie',
        })
          .then((uploadedFiles) => {
            setValues((prevState) => ({
              ...prevState,
              attachments: uniqBy(
                uploadedFiles.map(({ chemin, nomOriginal }) => ({ chemin, nomOriginal })),
                'chemin'
              ),
            }));

            if (recepteursIds.length > 0 && objet && message) {
              sendMessage({
                variables: {
                  inputs: {
                    ...(sendMessageVar.inputs as any),
                    attachments: uniqBy(
                      uploadedFiles.map(({ chemin, nomOriginal }) => ({ chemin, nomOriginal })),
                      'chemin'
                    ),
                  },
                },
              });
            }
          })
          .catch((error) => {
            setLoading(false);
            notify({
              type: 'error',
              message: `Une erreur s'est produite lors de chargement des fichiers`,
            });
          });
      } else {
        sendMessage({ variables: sendMessageVar });
      }
    }
  };
  //('SELECTED FILES +++++++++++', selectedFiles);
  const disabledSendBtn = (): boolean => {
    if (recepteursIds.length === 0 || !objet || !message) {
      return true;
    }
    return false;
  };
  const [focused, setFocused] = React.useState<boolean>(false);
  const handleFocus = () => {
    setFocused(true);
  };
  /**
   * GET MESSAGE IF IS ON REPLY OR REPLY_ALL OR FORWARD
   */
  React.useEffect(() => {
    if (messageId && (isOnReply || isOnReplyAll || isOnForward)) {
      // Execute query get message
      getMessage({ variables: { id: messageId } });
    }
  }, [isOnReply, isOnReplyAll, isOnForward, messageId]);

  /**
   * Set default values if msgData
   */
  React.useMemo(() => {
    if (msgData && msgData.messagerie) {
      const m = msgData.messagerie;
      let newRecepteurs: any[] = [];
      let newRecepteursIds: string[] = [];

      if (isOnReply) {
        if (m.userEmetteur) {
          newRecepteurs = [m.userEmetteur];
          newRecepteursIds = [m.userEmetteur.id];
        }
        if (m && m.userEmetteur && m.userEmetteur.id === currentUserId) {
          newRecepteurs = (m && m.recepteurs?.map((u) => u?.userRecepteur)) || [];
          newRecepteursIds = m && (m.recepteurs?.map((u) => u?.userRecepteur?.id) as any);
          //('CECI doit afficher theophile', newRecepteurs);
          //('ID de theophile', newRecepteursIds);
        }
      }

      if (isOnReplyAll) {
        if (m.recepteurs) {
          const usersRecepteurs = m.recepteurs.map((rc) => rc && rc.userRecepteur);
          if (usersRecepteurs && usersRecepteurs.length > 0) {
            newRecepteurs = newRecepteurs.filter((u) => u && u.id !== currentUserId);
            newRecepteursIds = usersRecepteurs.map((u) => u && u.id) as any;
          }
        }
      }
      const fakeFiles: FichierInput[] = [];

      if (m.attachments) {
        const newFichierPresentation: FichierInput[] = [];
        const fakeFiles: File[] = [];
        if (m && m.attachments) {
          m.attachments.map((item) => {
            if (item && item.fichier && item.fichier.chemin && item.fichier.nomOriginal && item.fichier.__typename) {
              const file: FichierInput = {
                chemin: item.fichier.chemin,
                type: item.fichier.__typename as any,
                nomOriginal: item.fichier.nomOriginal,
              };
              const fakeFile: File = { name: item.fichier.nomOriginal } as any;

              if (!newFichierPresentation.map((elem) => elem.chemin).includes(item.fichier.chemin)) {
                newFichierPresentation.push(file);
              }
              if (!fakeFiles.map((elem) => elem.name).includes(item.fichier.nomOriginal)) {
                fakeFiles.push(fakeFile);
              }
            }
          });
        }
        setValues((prevState) => ({
          ...prevState,
          selectedFiles: fakeFiles,
        }));

        setOldFiles(newFichierPresentation);
      }

      setValues((prevState) => ({
        ...prevState,
        selectedFiles:
          (m &&
            m.attachments &&
            m.attachments.map((file) => {
              return { name: file?.fichier?.nomOriginal || '' } as File;
            })) ||
          [],
      }));

      setValues((prevState) => ({
        ...prevState,
        id: m.id,
        recepteurs: isOnForward ? [] : newRecepteurs,
        recepteursIds: isOnForward ? [] : newRecepteursIds,
        objet: m.objet || '',
        message: m.message || '',
        typeMessagerie: m.typeMessagerie,
        theme: m.messagerieTheme,
        themeId: (m.messagerieTheme && m.messagerieTheme.id) || '',
        sourceId: (m.messagerieSource && m.messagerieSource.id) || '',
        attachments: (m &&
          m.attachments &&
          m.attachments.map((file) => {
            return {
              chemin: file?.fichier?.chemin,
              nomOriginal: file?.fichier?.nomOriginal,
            };
          })) as any,
      }));
    }
  }, [msgData]);

  const handleConfirm = () => {
    if (userIds) {
      setValues((prev) => ({
        ...prev,
        recepteursIds: userIds && userIds.length ? userIds : [],
      }));
    }

    if (userRecepteurs) {
      setValues((prev) => ({
        ...prev,
        recepteurs: userRecepteurs && userRecepteurs.length ? userRecepteurs : [],
      }));
    }
    if (setOpenUserModal) setOpenUserModal(false);
  };

  const handleClose = (open: boolean) => {
    if (userIds) setUserIds(recepteursIds.length ? recepteursIds : []);
    if (userRecepteurs) setUserRecepteurs(recepteurs.length ? recepteurs : []);
    if (setOpenUserModal) setOpenUserModal(open);
  };

  //('------------RECEPTEUS----------------', userRecepteurs);
  //('New recepteur >>>>>>>>>>', values.recepteurs);
  const fileContainer = (
    <Box className={classes.filesContainer}>
      {selectedFiles.map((file: any, index) => (
        <Box key={`${file.name}_${index}`} className={classes.fileItem}>
          <PictureAsPdf />
          <Box className={classes.filenameContainer}>
            <Typography>{file.name || file.nomOriginal}</Typography>
            {file.size && <Typography>{formatBytes(file.size, 1)}</Typography>}
          </Box>
          <IconButton size="small" onClick={removeFile(file)}>
            <Close />
          </IconButton>
        </Box>
      ))}
    </Box>
  );

  const handleChangeAutocomplete = (name: string) => (e: React.ChangeEvent<any>, value: any) => {
    //('-+-++-++--++-+--++-+-', name, _.uniqBy(value, 'id'));
    setValues((prevState) => ({ ...prevState, [name]: _.uniqBy(value, 'id') }));
  };

  const optionBtn = (
    <IconButton color="inherit" onClick={onClickSend} disabled={disabledSendBtn()}>
      <Send />
    </IconButton>
  );

  //(' le recepteurs', recepteurs);
  return (
    <div className={classes.formMessageRoot}>
      {(messageLoading || uploadingFiles.loading || loading || msgLoading) && (
        <Backdrop value={msgLoading ? 'Chargement' : 'Envoi' + ' en cours...'} />
      )}
      <CssBaseline />
      <Hidden lgUp={true} implementation="css">
        <MobileTopBar
          title={
            isOnReply
              ? 'Répondre'
              : isOnReplyAll
              ? 'Répondre à tous'
              : isOnForward
              ? 'Transférer à'
              : isOnNewMsg
              ? 'Nouveau message'
              : ''
          }
          withBackBtn={true}
          onClickBack={onClickBack}
          handleDrawerToggle={handleDrawerToggle}
          optionBtn={[optionBtn]}
        />
      </Hidden>

      <div className={classes.header}>
        <Button
          color="secondary"
          variant="contained"
          size="small"
          startIcon={<Send />}
          onClick={onClickSend}
          disabled={disabledSendBtn()}
        >
          Envoyer
        </Button>
        <Box display="flex">
          <Divider orientation="vertical" flexItem={true} style={{ margin: '20px 0px' }} />
          <IconButton aria-label="delete" className={classes.margin} onClick={initValues}>
            <Delete fontSize="small" />
          </IconButton>
        </Box>
      </div>

      <div className={classes.form}>
        <CustomAlertDonneesMedicales />
        <Autocomplete
          value={recepteurs}
          name="recepteurs"
          multiple={true}
          handleChange={handleChangeAutocomplete}
          handleInputChange={onChangeUserTxt}
          options={users}
          optionLabel="userName"
          startAdornment={<span style={{ fontWeight: 'bold' }}>A : &nbsp;</span>}
          popupIcon={<SupervisorAccount onClick={() => (setOpenUserModal ? setOpenUserModal(true) : '')} />}
          handleOpenAutocomplete={onOpenUserAutocomplete}
          loading={userLoading}
        />
        <TextField
          variant="standard"
          name="objet"
          value={objet}
          onChange={onChangeInput}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <span>Objet : </span>
              </InputAdornment>
            ),
          }}
        />
        <Autocomplete
          value={theme}
          name="theme"
          handleChange={onChangeAutocomplete}
          handleInputChange={onChangeThemeTxt}
          options={themes || []} // TODO
          optionLabel="nom" // TODO
          startAdornment={<span>Thème : &nbsp; </span>}
          popupIcon={<Brush />}
          handleOpenAutocomplete={onOpenThemeAutocomplete}
        />
        <div {...getRootProps({ className: classes.dropzone })}>
          <input {...getInputProps()} />
          <Box display="flex" width="100%">
            <Typography style={{ minWidth: 'fit-content' }}>Pièces Jointes :</Typography>
            {fileContainer}
          </Box>
          <Button variant="contained" endIcon={<AddCircle />} color="default" onClick={open}>
            Sélect. fichiers
          </Button>
        </div>
        <Box width="100%" onFocus={handleFocus} className={classes.mailContent}>
          <ReactQuill
            theme="snow"
            value={message}
            style={{
              height: '75%',
            }}
            className={focused ? 'div-focused' : 'blurred-editor'}
            onChange={(content: any) => {
              handleChangeMessageContent(content, 'message');
            }}
          />
        </Box>
      </div>
      <CustomModal
        open={openUserModal ? openUserModal : false}
        setOpen={handleClose}
        title={'Contact'}
        withBtnsActions={true}
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="lg"
        fullWidth={false}
        withCancelButton={isMobile() ? false : true}
        centerBtns={isMobile() ? false : true}
        onClickConfirm={handleConfirm}
        actionButton="Ajouter"
        className={classes.userSelectRoot}
        fullScreen={isMobile() ? true : false}
      >
        <UserSelect
          userIdsSelected={userIds}
          setUserIdsSelected={setUserIds}
          setUsersSelected={setUserRecepteurs}
          usersSelected={userRecepteurs}
          filtersSelected={filtersModal}
          setFiltersSelected={setFiltersModal}
          disableAllFilter={true}
          activeAllContact={
            currentUser &&
            currentUser.role &&
            currentUser.role.code &&
            [SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT].includes(currentUser.role.code)
              ? true
              : false
          }
        />
      </CustomModal>
    </div>
  );
};

export default FormMessage;
