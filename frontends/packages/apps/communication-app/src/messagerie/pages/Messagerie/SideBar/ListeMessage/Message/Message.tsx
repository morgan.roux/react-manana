import {
  capitalizeFirstLetter,
  MESSAGE_DELETED_URL,
  MESSAGE_NEW_URL,
  MESSAGE_SENDED_URL,
  MESSAGE_URL,
  useApplicationContext,
  useDisplayNotification,
} from '@lib/common';
import {
  MessagerieInfoFragment,
  MessagerieType,
  useMark_Message_As_SeenMutation,
  useMark_Message_As_UnseenMutation,
} from '@lib/common/src/graphql';
import {
  withStyles,
  Box,
  List,
  ListItem,
  ListItemText,
  CssBaseline,
  Hidden,
  Icon,
  IconButton,
  Tooltip,
  AccordionSummary as MuiAccordionSummary,
  Accordion as MuiAccordion,
  AccordionDetails as MuiAccordionDetails,
  Typography,
} from '@material-ui/core';
import { DoneAll, Drafts, Markunread } from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import React, { Dispatch, FC, SetStateAction } from 'react';
import { useHistory, useLocation } from 'react-router';
import { messagerieIcon } from '../../../messagerieIcon';
import MobileMessageItem from '../../Mobile/MobileMessageItem';
import { ListeMessageProps } from '../ListeMessage';
import useStyles from './styles';

export interface MessageProps {
  selectedMsg: any[];
  setSelectedMsg: Dispatch<SetStateAction<any[]>>;
  setShowMainContent?: (value: boolean) => void;
}
const Accordion = withStyles({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    backgroundColor: 'rgba(0, 0, 0, .03)',
    borderBottom: '1px solid rgba(0, 0, 0, .125)',
    marginBottom: -1,
    minHeight: 56,
    '&$expanded': {
      minHeight: 56,
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiAccordionDetails);

export const Message: FC<ListeMessageProps & MessageProps> = ({
  messageList,
  setMessage,
  currentUserId,
  selectedMsg,
  setSelectedMsg,
  queryVariables,
  refetchUnreadMsg,
  // queryVariables,
  setShowMainContent,
}) => {
  const classes = useStyles({});
  const { pathname } = useLocation();
  const { push } = useHistory();
  const { graphql } = useApplicationContext();
  const displayNotification = useDisplayNotification();
  const selectedMsgIds = selectedMsg.map((i) => i && i.id);

  const isOnMailbox = pathname === MESSAGE_URL;
  const isOnSended = pathname === MESSAGE_SENDED_URL;
  const isOnDeleted = pathname === MESSAGE_DELETED_URL;
  const isOnNewMsg = pathname === MESSAGE_NEW_URL;
  const isOnInbox = pathname.startsWith(MESSAGE_URL); // whenever you open a message or not
  const [expanded, setExpanded] = React.useState<string | false>(false);

  React.useEffect(() => {
    const defaultYear =
      messageList.length > 0
        ? Math.max(...[...(messageList || []).map((item: { year: number }) => item.year)])
        : moment().year();
    setExpanded(`${defaultYear}`);
  }, [messageList]);

  const handleChange = (panel: string) => (event: React.ChangeEvent<{}>, newExpanded: boolean) => {
    setExpanded(newExpanded ? panel : false);
  };

  const [markMsgAsSeen] = useMark_Message_As_SeenMutation({
    client: graphql,
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
    onCompleted: (data) => {
      if (data && data.markMessageAsSeen && refetchUnreadMsg) {
        refetchUnreadMsg();
      }
    },
  });

  const [markMessageUnseen] = useMark_Message_As_UnseenMutation({
    client: graphql,
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
    onCompleted: (data) => {
      displayNotification({
        type: 'success',
        message: 'Message marqué comme non lu',
      });
      if (refetchUnreadMsg) {
        refetchUnreadMsg();
      }
    },
  });
  const onClickMessage = (message: MessagerieInfoFragment) => () => {
    setMessage(message);
    setSelectedMsg([message]);
    if (setShowMainContent) setShowMainContent(true);
    if (isOnSended || isOnNewMsg || pathname.includes('sent')) {
      push(`${MESSAGE_SENDED_URL}/${message.id}`);
    }
    if (isOnDeleted || pathname.includes('trash')) {
      push(`${MESSAGE_DELETED_URL}/${message.id}`);
    }
    if (isOnMailbox || pathname.includes('inbox')) {
      push(`${MESSAGE_URL}/${message.id}`);
      // Mark seen message
      if (message && !message.lu && message.recepteurs) {
        const messageHisto = message.recepteurs.find(
          (rc) => rc && rc.userRecepteur && rc.userRecepteur.id === currentUserId
        );

        if (messageHisto) {
          markMsgAsSeen({ variables: { idMessageHisto: messageHisto.id } });
        }
      }
    }
  };

  const onDoubleClickMessage = (message: MessagerieInfoFragment) => () => {
    setMessage(message);

    // Set selected msg

    if (selectedMsgIds.includes(message.id)) {
      const newSelected = selectedMsg.filter((i) => i && i.id !== message.id);
      setSelectedMsg(newSelected);
    } else {
      const newSelected = [...selectedMsg, message];
      setSelectedMsg(newSelected);
    }
  };

  const hadleSetUnread = (message: MessagerieInfoFragment, event: any) => {
    event.stopPropagation();
    if (message && message.lu && message.recepteurs) {
      const messageHisto = message.recepteurs.find(
        (rc) => rc && rc.userRecepteur && rc.userRecepteur.id === currentUserId
      );
      if (messageHisto) {
        markMessageUnseen({ variables: { idMessageHisto: messageHisto.id } });
      }
    }
  };

  const hadleSetRead = (message: MessagerieInfoFragment, event: any) => {
    event.stopPropagation();
    if (message && !message.lu && message.recepteurs) {
      const messageHisto = message.recepteurs.find(
        (rc) => rc && rc.userRecepteur && rc.userRecepteur.id === currentUserId
      );
      if (messageHisto) {
        markMsgAsSeen({ variables: { idMessageHisto: messageHisto.id } });
      }
    }
  };

  const icon = (typeFilter: string) => {
    if (queryVariables && queryVariables.typeFilter && queryVariables.typeMessagerie === MessagerieType.R) {
      return messagerieIcon(queryVariables.typeFilter) || '';
    }
    return messagerieIcon(typeFilter) || '';
  };

  const mobileMessages = messageList && messageList.map((item) => item.messages).flat();

  return (
    <Box className={classes.root}>
      <CssBaseline />
      <Hidden mdUp={true} implementation="css">
        <List>
          {mobileMessages && messageList.length > 0 ? (
            mobileMessages.map((message, index) => (
              <MobileMessageItem key={`${message.id}-item`} message={message} onClick={onClickMessage} />
            ))
          ) : (
            <Box display="flex" justifyContent="center">
              <Typography className={classes.noMessageTxt}>
                {isOnSended ? "Vous n'avez pas de message envoyé" : 'Aucun message'}
              </Typography>
            </Box>
          )}
        </List>
      </Hidden>
      <Hidden smDown={true} implementation="css">
        {messageList.length > 0 ? (
          messageList.map((element: any, index) => {
            if (element) {
              return (
                <Accordion
                  key={`message_${index}`}
                  square={true}
                  expanded={expanded === element.year}
                  onChange={handleChange(element.year)}
                >
                  <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                    <Typography>{element.year}</Typography>
                  </AccordionSummary>
                  <AccordionDetails style={{ display: 'flex', flexDirection: 'column' }}>
                    {element.messages.map((message: MessagerieInfoFragment, msgIndex: number) => {
                      const recepteurs: any[] =
                        (message.recepteurs &&
                          message.recepteurs.length &&
                          message.recepteurs.map(
                            (recepteur) =>
                              recepteur &&
                              recepteur.userRecepteur &&
                              capitalizeFirstLetter(recepteur.userRecepteur.userName)
                          )) ||
                        [];
                      return (
                        <List
                          key={`element_message_${msgIndex}`}
                          className={classnames(classes.messageItem, message.lu ? classes.read : classes.notRead)}
                          component="nav"
                          aria-label="main mailbox folders"
                        >
                          <ListItem
                            className={classes.contentListItem}
                            button={true}
                            onClick={onClickMessage(message)}
                            onDoubleClick={onDoubleClickMessage(message)}
                            id={message.id}
                          >
                            <Box position="relative" style={{ width: '75%' }}>
                              {selectedMsgIds.includes(message.id) && (
                                <Box className={classes.checkedMsgContainer}>
                                  <Icon color="secondary" fontSize="small">
                                    <DoneAll />
                                  </Icon>
                                </Box>
                              )}

                              <ListItemText
                                className={classes.nomSender}
                                primary={
                                  queryVariables && queryVariables.typeMessagerie == MessagerieType.E
                                    ? 'Moi'
                                    : (message.userEmetteur && message.userEmetteur.userName) || ''
                                }
                              />
                              {recepteurs.length ? (
                                <Box className={classes.nomRecept}> À: {recepteurs.join(' , ')}</Box>
                              ) : (
                                ''
                              )}
                              <Box className={classes.objet}>
                                {message.typeFilter && icon(message.typeFilter)}
                                <Typography>Object Mail : {message.objet}</Typography>
                              </Box>
                            </Box>
                            <Box display="flex" alignItems="center">
                              <Box className={classes.date}>
                                <Typography>
                                  {message.dateCreation ? moment(message.dateCreation).format('Do MMMM') : ''}
                                </Typography>
                                <Typography>
                                  {message.dateCreation ? moment(message.dateCreation).format('LT') : ''}
                                </Typography>
                              </Box>
                              {isOnInbox &&
                                (message.lu ? (
                                  <Box className="hoverMessage">
                                    <Tooltip title="Marquer comme non lu">
                                      <IconButton
                                        onClick={(e) => {
                                          hadleSetUnread(message, e);
                                        }}
                                      >
                                        <Drafts />
                                      </IconButton>
                                    </Tooltip>
                                  </Box>
                                ) : (
                                  <Box className="hoverMessage">
                                    <Tooltip title="Marquer comme lu">
                                      <IconButton
                                        onClick={(e) => {
                                          hadleSetRead(message, e);
                                        }}
                                      >
                                        <Markunread />
                                      </IconButton>
                                    </Tooltip>
                                  </Box>
                                ))}
                            </Box>
                          </ListItem>
                        </List>
                      );
                    })}
                  </AccordionDetails>
                </Accordion>
              );
            }
          })
        ) : (
          <Box display="flex" justifyContent="center">
            <Typography className={classes.noMessageTxt}>
              {isOnSended ? "Vous n'avez pas de message envoyé" : 'Aucun message'}
            </Typography>
          </Box>
        )}
      </Hidden>
    </Box>
  );
};

export default Message;
