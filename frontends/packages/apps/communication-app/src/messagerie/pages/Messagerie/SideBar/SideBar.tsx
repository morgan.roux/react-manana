import { ApolloQueryResult } from '@apollo/client';
import { useApolloClient } from '@apollo/client';
import {
  isMd,
  isMobile,
  MESSAGE_DELETED_URL,
  MESSAGE_FORWARD_URL,
  MESSAGE_NEW_URL,
  MESSAGE_REPLY_ALL_URL,
  MESSAGE_REPLY_URL,
  MESSAGE_SENDED_URL,
  MESSAGE_URL,
  MobileTopBar,
  useApplicationContext,
  useDisplayNotification,
} from '@lib/common';
import {
  MessagerieInfoFragment,
  MessageriesDocument,
  MessageriesQuery,
  MessageriesQueryVariables,
  MessagerieType,
  ParamCategory,
  Total_Messagerie_Non_LusQuery,
  useDelete_MessagesMutation,
  useParameters_Groupes_CategoriesQuery,
} from '@lib/common/src/graphql';
import {
  ConfirmDeleteDialog,
  CustomButton,
  CustomModal,
  CustomSelect,
  NewCustomButton,
  SearchInput,
} from '@lib/ui-kit';
import {
  FormControl,
  MenuItem,
  Select,
  Typography,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListSubheader,
  Divider,
  Drawer,
  Hidden,
  IconButton,
  Box,
  Button,
  CssBaseline,
  useTheme,
} from '@material-ui/core';
import { Create, Delete, FilterList } from '@material-ui/icons';
import MailIcon from '@material-ui/icons/Email';
import InboxIcon from '@material-ui/icons/Inbox';
import RestoreFromTrashIcon from '@material-ui/icons/RestoreFromTrash';
import SendIcon from '@material-ui/icons/Send';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import { differenceBy } from 'lodash';
import React, { ChangeEvent, Dispatch, FC, SetStateAction, useState } from 'react';
import { useHistory, useLocation } from 'react-router';
import { messagerieIcon } from '../messagerieIcon';
import ContentMessage from './ContentMessage';
import { hiddenFilter } from './filter';
import FormMessage from './FormMessage';
import { FormMessageProps, IFormProps } from './FormMessage/FormMessage';
import { initialState } from './FormMessage/useMessageForm';
import ListeMessageLeft from './ListeMessage';
import Filter from './ListeMessage/Filter';
import Message from './ListeMessage/Message';
import useStyles from './styles';

interface ResponsiveDrawerProps {
  messageList: any[];
  setQueryVariables: Dispatch<SetStateAction<MessageriesQueryVariables>>;
  searchValue: string;
  setSearchValue: Dispatch<SetStateAction<string>>;
  message: MessagerieInfoFragment | null;
  setMessage: Dispatch<SetStateAction<MessagerieInfoFragment | null>>;
  selectedMsg: any[];
  setSelectedMsg: Dispatch<SetStateAction<any[]>>;
  onChangeSearchValue: (event: ChangeEvent<any>) => void;
  nbMsgNonLus: number;
  refetchUnreadMsg: (
    variables?: Record<string, any> | undefined
  ) => Promise<ApolloQueryResult<Total_Messagerie_Non_LusQuery>>;
  nbMsg: number;
  filterCounts: any[];
  refetchAll?: any;
  handleDrawerToggle: () => void;
  mobileOpen: boolean;
}

const ResponsiveDrawer: FC<ResponsiveDrawerProps & FormMessageProps & IFormProps> = ({
  messageList,
  values,
  setValues,
  onChangeInput,
  onChangeAutocomplete,
  setQueryVariables,
  searchValue,
  setSearchValue,
  onChangeSearchValue,
  message,
  setMessage,
  queryVariables,
  currentUserId,
  selectedMsg,
  setSelectedMsg,
  nbMsgNonLus,
  refetchUnreadMsg,
  nbMsg,
  filterCounts,
  handleChangeMessageContent,
  refetchAll,
  handleDrawerToggle,
  mobileOpen,
}) => {
  const classes = useStyles({});

  const client = useApolloClient();

  const [openFilterModal, setOpenFilterModal] = React.useState<boolean>(false);

  const theme = useTheme();

  const { pathname } = useLocation();

  const { graphql, user: currentUser, isGroupePharmacie } = useApplicationContext();

  const { push } = useHistory();

  const displayNotification = useDisplayNotification();

  const [openFilterPopper, setOpenFilterPopper] = React.useState(false);
  const anchorRef = React.useRef<HTMLButtonElement>(null);

  const isOnMailbox = pathname === MESSAGE_URL || pathname.includes(MESSAGE_URL);
  const isOnNewMsg = pathname === MESSAGE_NEW_URL || pathname.includes(MESSAGE_NEW_URL);
  const isOnSended = pathname === MESSAGE_SENDED_URL || pathname.includes(MESSAGE_SENDED_URL);
  const isOnDeleted = pathname === MESSAGE_DELETED_URL || pathname.includes(MESSAGE_DELETED_URL);

  const isOnReply = pathname.includes(MESSAGE_REPLY_URL);
  const isOnReplyAll = pathname.includes(MESSAGE_REPLY_ALL_URL);
  const isOnForward = pathname.includes(MESSAGE_FORWARD_URL);

  const [openUserModal, setOpenUserModal] = React.useState<boolean>(false);

  const [showMainContent, setShowMainContent] = useState<boolean>(false);

  const [showFormMessage, setShowFormMessage] = useState<boolean>(false);

  const [openDialog, setOpenDialog] = useState<boolean>(false);

  const { data } = useParameters_Groupes_CategoriesQuery({
    client: graphql,
    variables: {
      categories: [ParamCategory.Groupement, ParamCategory.User],
      groupes: ['MESSAGERIE'],
    },
  });

  const initMessage = () => {
    setMessage(null);
    setSearchValue('');
    setValues(initialState);
  };

  const goToNewMsg = () => {
    if (queryVariables && !queryVariables.typeFilter) {
      setOpenFilterModal(true);
    } else {
      initMessage();
      push(MESSAGE_NEW_URL);
      // Show sended message
      setQueryVariables((prevState) => ({
        ...prevState,
        isRemoved: false,
        typeMessagerie: MessagerieType.E,
      }));
    }
  };

  const onClickMailbox = () => {
    push(MESSAGE_URL, { lastPathname: pathname });
    setQueryVariables((prevState) => ({
      ...prevState,
      isRemoved: false,
      typeMessagerie: MessagerieType.R,
    }));
    initMessage();
  };

  const onClickSendedElements = () => {
    push(MESSAGE_SENDED_URL, { lastPathname: pathname });
    setQueryVariables((prevState) => ({
      ...prevState,
      isRemoved: false,
      typeMessagerie: MessagerieType.E,
    }));
    initMessage();
  };

  const onClickDeletedElements = () => {
    push(MESSAGE_DELETED_URL, { lastPathname: pathname });
    setQueryVariables((prevState) => ({ ...prevState, isRemoved: true, typeMessagerie: null }));
    initMessage();
  };

  const handleClickFilter = (type: string) => (e: React.ChangeEvent<any>) => {
    e.preventDefault();
    e.stopPropagation();
    setQueryVariables((prev) => ({
      ...prev,
      typeFilter: (type === queryVariables.typeFilter || type === 'all') && !isOnNewMsg ? '' : type,
    }));
  };

  const [activeListe, setActiveListe] = useState<string>(isOnMailbox ? 'received' : isOnSended ? 'sent' : 'deleted');

  const [activeFilter, setActiveFilter] = useState<any>('MY_PHARMACIE');

  const handleActiveListeChange = (event: ChangeEvent<{ name?: string | undefined; value: unknown }>) => {
    const { value } = event.target;
    setActiveListe(value as string);
    switch (value) {
      case 'received':
        onClickMailbox();
        break;
      case 'sent':
        onClickSendedElements();
        break;
      case 'deleted':
        onClickDeletedElements();
        break;

      default:
        break;
    }
  };

  const handleActiveFilterChange = (event: any) => {
    const { value } = event.target;
    setActiveFilter(value);
    handleClickFilter(value)(event);
  };

  const liste = [
    {
      value: 'received',
      libelle: 'Boîte de réception',
      icon: <InboxIcon />,
      onClick: onClickMailbox,
      active: isOnMailbox || pathname.includes('inbox'),
    },
    {
      value: 'sent',
      libelle: 'Éléments envoyés',
      icon: <SendIcon />,
      onClick: onClickSendedElements,
      active: isOnSended || isOnNewMsg || pathname.includes('sent'),
    },
    {
      value: 'deleted',
      libelle: 'Éléments supprimés',
      icon: <RestoreFromTrashIcon />,
      onClick: onClickDeletedElements,
      active: isOnDeleted || pathname.includes('trash'),
    },
  ];

  const mobileAllFilter = {
    libelle: 'Tout',
    code: 'all',
  };

  const listeFiltre =
    filterCounts && filterCounts.length
      ? filterCounts
          .map((item) => {
            let icon: any = (item && item.code && messagerieIcon(item.code)) || '';
            return {
              libelle: item?.libelle,
              icon,
              onClick: item && item.code ? handleClickFilter(item.code) : () => {},
              nombre: item?.count,
              code: item?.code,
              hidden:
                item && item.code && item.code === 'MY_PHARMACIE'
                  ? false
                  : data && data.parametersGroupesCategories && data.parametersGroupesCategories.length
                  ? hiddenFilter(
                      isGroupePharmacie ? true : false,
                      item && item.code,
                      data.parametersGroupesCategories as any
                    )
                  : true,
            };
          })
          .filter((filter) => !filter.hidden)
      : [];

  const handleClickBack = () => {
    setShowFormMessage(false);
    setShowMainContent(false);
  };

  const handleFilterToggle = () => {
    setOpenFilterPopper((prevOpen) => !prevOpen);
  };

  const handleCreateMessageClick = () => {
    push('/messagerie/new');
    setShowFormMessage(true);
  };

  const handleDeleteMessage = () => {
    if (selectedMsg.length > 0) {
      setOpenDialog(true);
    }
  };

  /**
   * Mutation delete message
   */
  const [doDeleteMsg, { loading: deleteLoading }] = useDelete_MessagesMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.deleteMessages) {
        const req = cache.readQuery<MessageriesQuery, MessageriesQueryVariables>({
          query: MessageriesDocument,
          variables: queryVariables,
        });
        if (req && req.messageries && req.messageries.data) {
          const newData: any[] = differenceBy(req.messageries.data, data.deleteMessages, 'id');
          cache.writeQuery({
            query: MessageriesDocument,
            data: { messageries: { ...req.messageries, ...{ data: newData } } },
            variables: queryVariables,
          });
        }
      }
    },
    onCompleted: (data) => {
      if (data && data.deleteMessages) {
        displayNotification({
          type: 'success',
          message: 'Message(s) supprimé(s) avec succès',
        });
        setMessage(null);
        setSelectedMsg([]);
        if (refetchUnreadMsg) {
          refetchUnreadMsg();
        }
        setShowMainContent(false);
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const onConfirmDelete = () => {
    if (selectedMsg.length > 0) {
      setOpenDialog(false);
      const ids = selectedMsg.map((i) => i && i.id);

      const type: MessagerieType = pathname.includes('inbox') ? MessagerieType.R : MessagerieType.E;
      doDeleteMsg({
        variables: { ids, typeMessagerie: type, permanent: isOnDeleted ? true : false },
      });
    }
  };

  const optionBtn = (
    <IconButton color="inherit" onClick={handleDeleteMessage}>
      <Delete />
    </IconButton>
  );

  const drawer = (
    <Box className={classes.rootDrawer}>
      <CssBaseline />
      <Hidden smDown={true} implementation="css">
        <Box padding="16px">
          <NewCustomButton fullWidth={true} className={classes.addButton} onClick={goToNewMsg}>
            Nouveau Message
          </NewCustomButton>
          <Divider />
        </Box>
        <Box padding="0 16px">
          <List className={classes.rootList}>
            {liste.map((element, index) => (
              <ListItem
                button={true}
                key={`id-liste-${element.libelle}_${index}`}
                onClick={element.onClick}
                className={element.active ? 'active' : ''}
              >
                <ListItemIcon>{element.icon}</ListItemIcon>
                <ListItemText className={classes.textList} primary={element.libelle} />
              </ListItem>
            ))}
          </List>
          <Divider />
        </Box>
        <Box padding="16px">
          <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            subheader={
              <ListSubheader component="div" id="nested-list-subheader">
                Filtrer par
              </ListSubheader>
            }
            className={classes.rootListFilter}
          >
            {listeFiltre.map((elementFiltre) => (
              <ListItem
                button={true}
                key={`id-listeFiltre-${elementFiltre.libelle}`}
                onClick={elementFiltre.onClick}
                className={elementFiltre.code === queryVariables.typeFilter ? 'active' : ''}
              >
                <Box display="flex" alignItems="center">
                  <ListItemIcon className={classes.colorFilterCircle}>{elementFiltre.icon}</ListItemIcon>
                  <ListItemText className={classes.textList} primary={elementFiltre.libelle} />
                </Box>
                <Typography
                  className={`${classes.nombreFiltre} ${
                    elementFiltre.code === queryVariables.typeFilter ? 'active' : ''
                  }`}
                >
                  {elementFiltre.nombre}
                </Typography>
              </ListItem>
            ))}
          </List>
        </Box>
      </Hidden>
      <Hidden mdUp={true} implementation="css">
        <Box width="100%">
          <Box className={classes.mobileTopBar}>
            <Box className={classes.mobileListeFilter}>
              <FormControl>
                <Select value={activeListe} onChange={handleActiveListeChange} className={classes.selectInputMobile}>
                  {liste.map((element, index) => (
                    <MenuItem key={element.value} value={element.value} className={classes.listeMenuItem}>
                      {element.icon}
                      <span>{element.libelle}</span>
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
              <IconButton ref={anchorRef} onClick={handleFilterToggle}>
                <FilterList />
              </IconButton>
              <Filter
                anchorRef={anchorRef}
                open={openFilterPopper}
                setOpen={setOpenFilterPopper}
                setQueryVariables={setQueryVariables}
              />
            </Box>
            <Box padding="16px 16px 0px 16px" width="100">
              <CustomSelect
                label="Filtrer par"
                list={[mobileAllFilter, ...listeFiltre]}
                listId="code"
                index="libelle"
                name="code"
                value={activeFilter}
                onChange={handleActiveFilterChange}
                required={false}
              />
            </Box>
            <Box className={classes.msgNbr}>
              <Typography>{nbMsg || 0} Messages(s)</Typography>
              <Typography>{nbMsgNonLus} Non lu(s)</Typography>
            </Box>
          </Box>

          <Message
            setShowMainContent={setShowMainContent}
            messageList={messageList}
            message={message}
            setMessage={setMessage}
            currentUserId={currentUserId}
            selectedMsg={selectedMsg}
            setSelectedMsg={setSelectedMsg}
            queryVariables={queryVariables}
            setQueryVariables={setQueryVariables}
            refetchUnreadMsg={refetchUnreadMsg}
          />
        </Box>
      </Hidden>
    </Box>
  );

  return (
    <Box className={classes.root}>
      <CssBaseline />
      <Hidden only="md" implementation="css">
        {!showMainContent && (
          <nav className={classes.drawer} aria-label="mailbox folders">
            <Box className={classes.drawerPaper}>{drawer}</Box>
          </nav>
        )}
      </Hidden>

      <Hidden mdUp={true} implementation="css">
        {showMainContent && (
          <nav className={classes.drawer} aria-label="mailbox folders">
            <CustomModal
              open={showMainContent}
              setOpen={setShowMainContent}
              fullScreen={true}
              closeIcon={true}
              withBtnsActions={false}
              noDialogContent={true}
            >
              <Box paddingTop="90px" width="100%">
                <MobileTopBar
                  title={''}
                  withBackBtn={true}
                  onClickBack={handleClickBack}
                  handleDrawerToggle={handleDrawerToggle}
                  optionBtn={[optionBtn]}
                />
                <ContentMessage
                  message={message}
                  setMessage={setMessage}
                  queryVariables={queryVariables}
                  selectedMsg={selectedMsg}
                  setSelectedMsg={setSelectedMsg}
                  refetchAll={refetchAll}
                  setShowMainContent={setShowMainContent}
                  setShowFormMessage={setShowFormMessage}
                />
              </Box>
            </CustomModal>
          </nav>
        )}
        {showFormMessage && (
          <CustomModal
            open={showFormMessage}
            setOpen={setShowFormMessage}
            fullScreen={true}
            closeIcon={true}
            withBtnsActions={false}
            noDialogContent={true}
          >
            <FormMessage
              values={values}
              setValues={setValues}
              onChangeInput={onChangeInput}
              onChangeAutocomplete={onChangeAutocomplete}
              queryVariables={queryVariables}
              currentUserId={currentUserId}
              openUserModal={openUserModal}
              setOpenUserModal={setOpenUserModal}
              handleChangeMessageContent={handleChangeMessageContent}
              onClickBack={handleClickBack}
              handleDrawerToggle={handleDrawerToggle}
            />
          </CustomModal>
        )}
      </Hidden>
      {isMd() && (
        <Drawer
          container={window.document.body}
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <nav className={classes.drawer} aria-label="mailbox folders">
            <Box className={classes.drawerWeb}>{drawer}</Box>
          </nav>
        </Drawer>
      )}
      {(!isMobile() || showMainContent) && (
        <main className={classes.content}>
          <Box className={classes.toolbar} display="flex" justifyContent="space-between">
            <Box className={classes.contentSearch}>
              <SearchInput value={searchValue} onChange={onChangeSearchValue} />
            </Box>
            <Box display="flex">
              <Button className={classes.buttonContent}>
                <Typography>{nbMsg || 0} Messages(s)</Typography>
              </Button>
              <Button className={classes.buttonContent}>
                <Typography>{nbMsgNonLus} Non lu(s)</Typography>
              </Button>
            </Box>
            <Box display="flex">
              <Divider orientation="vertical" flexItem={true} />
              <Button className={classes.buttonContent} onClick={goToNewMsg}>
                <MailIcon />
                <Typography>Messagerie</Typography>
              </Button>
              <Button
                className={classes.buttonContent}
                onClick={() => (isOnNewMsg || isOnReply || isOnReplyAll || isOnForward ? setOpenUserModal(true) : '')}
              >
                <SupervisorAccountIcon />
                <Typography>Contacts</Typography>
              </Button>
            </Box>
          </Box>

          <Box display="flex">
            <ListeMessageLeft
              messageList={messageList}
              message={message}
              setMessage={setMessage}
              currentUserId={currentUserId}
              queryVariables={queryVariables}
              selectedMsg={selectedMsg}
              setSelectedMsg={setSelectedMsg}
              setQueryVariables={setQueryVariables}
              refetchUnreadMsg={refetchUnreadMsg}
            />

            {isOnNewMsg || isOnReply || isOnReplyAll || isOnForward ? (
              <FormMessage
                values={values}
                setValues={setValues}
                onChangeInput={onChangeInput}
                onChangeAutocomplete={onChangeAutocomplete}
                queryVariables={queryVariables}
                currentUserId={currentUserId}
                openUserModal={openUserModal}
                setOpenUserModal={setOpenUserModal}
                handleChangeMessageContent={handleChangeMessageContent}
              />
            ) : (
              <ContentMessage
                message={message}
                setMessage={setMessage}
                queryVariables={queryVariables}
                selectedMsg={selectedMsg}
                setSelectedMsg={setSelectedMsg}
                refetchAll={refetchAll}
              />
            )}
          </Box>
        </main>
      )}
      <CustomModal
        title="Choix du filtre"
        open={openFilterModal}
        setOpen={setOpenFilterModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        maxWidth="xl"
      >
        <div style={{ margin: '15px', fontSize: '15px' }}>Pour envoyer un message, vous devez choisir un filtre</div>
      </CustomModal>
      <ConfirmDeleteDialog
        open={openDialog}
        setOpen={setOpenDialog}
        onClickConfirm={onConfirmDelete}
        content={
          isOnDeleted
            ? 'Voulez-vous supprimer ce(s) message(s) définitivement?'
            : 'Êtes-vous sûr de vouloir supprimer ce(s) message(s) ?'
        }
      />
      <Hidden mdUp={true}>
        <Box className={classes.createMessageButton}>
          <IconButton onClick={handleCreateMessageClick}>
            <Create />
          </IconButton>
        </Box>
      </Hidden>
      {/*isMobile() && (pathname.includes('trash') || pathname.includes('sent') || pathname.includes('inbox')) && (
          <Fab className={classes.fab} color="secondary" variant="round" onClick={goToNewMsg}>
            <Add />
          </Fab>
        )*/}
    </Box>
  );
};

export default ResponsiveDrawer;
