import { MESSAGE_URL } from '@lib/common';
import { MessageriesQueryVariables } from '@lib/common/src/graphql';
import { ClickAwayListener, Grow, MenuItem, MenuList, Paper, Popper } from '@material-ui/core';
import React, { Dispatch, FC, SetStateAction } from 'react';
import { useLocation } from 'react-router';
import useStyles from './styles';

export interface ListeMessageProps {
  setQueryVariables: Dispatch<SetStateAction<MessageriesQueryVariables>>;
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  anchorRef: React.RefObject<HTMLButtonElement>;
}

export const ListeMessage: FC<ListeMessageProps> = ({ setQueryVariables, open, setOpen, anchorRef }) => {
  const classes = useStyles({});

  const { pathname } = useLocation();

  const isOnMailbox = pathname === MESSAGE_URL || pathname.includes('inbox');

  const handleClose = (event: React.MouseEvent<EventTarget>) => {
    if (anchorRef.current && anchorRef.current.contains(event.target as HTMLElement)) {
      return;
    }

    setOpen(false);
  };

  const handleListKeyDown = (event: React.KeyboardEvent) => {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  };

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current!.focus();
    }

    prevOpen.current = open;
  }, [open]);

  const handleOrderBy = (orderBy: string) => () => {
    setQueryVariables((prev) => ({ ...prev, orderBy }));
    setOpen(false);
  };

  return (
    <Popper
      open={open}
      anchorEl={anchorRef.current}
      role={undefined}
      transition={true}
      disablePortal={true}
      className={classes.root}
    >
      {({ TransitionProps, placement }) => (
        <Grow
          {...TransitionProps}
          style={{
            transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
          }}
        >
          <Paper>
            <ClickAwayListener onClickAway={handleClose}>
              <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                <MenuItem onClick={handleOrderBy('dateCreation_DESC')}>Les plus récents</MenuItem>
                <MenuItem onClick={handleOrderBy('dateCreation_ASC')}>Les plus anciens</MenuItem>
                {isOnMailbox && <MenuItem onClick={handleOrderBy('lu_DESC')}>Lus</MenuItem>}
                {isOnMailbox && <MenuItem onClick={handleOrderBy('lu_ASC')}>Non lus</MenuItem>}
                <MenuItem onClick={handleOrderBy('objet_DESC')}>Objet</MenuItem>
              </MenuList>
            </ClickAwayListener>
          </Paper>
        </Grow>
      )}
    </Popper>
  );
};

export default ListeMessage;
