import { createStyles, makeStyles } from '@material-ui/core';
import { Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    avatar: {
      height: 36,
      width: 36,
    },
    spaceBetween: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    receivers: {
      fontFamilty: 'Roboto',
      fontSize: 14,
      fontWeight: 'bold',
    },
    dateCreation: {
      fontFamilty: 'Roboto',
      fontSize: 12,
    },
    objet: {
      fontFamilty: 'Roboto',
      fontSize: 12,
      fontWeight: 'bold',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
      overflow: 'hidden',
    },
    message: {
      fontFamilty: 'Roboto',
      fontSize: 12,
      color: '#27272FAB',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
      overflow: 'hidden',
    },
    messageInfos: {
      [theme.breakpoints.down('sm')]: {
        width: '80%',
      },
      [theme.breakpoints.up('sm')]: {
        width: '90%',
      },
    },
  })
);

export default useStyles;
