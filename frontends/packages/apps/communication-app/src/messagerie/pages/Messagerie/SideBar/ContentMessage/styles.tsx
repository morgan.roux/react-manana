import { createStyles, makeStyles } from '@material-ui/core';
import { Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      height: 'calc(100vh - 184px)',
      [theme.breakpoints.down('lg')]: {
        height: '100%',
        overflow: 'auto',
        display: 'flex',
        flexDirection: 'column',
      },
    },
    margin: {
      margin: theme.spacing(1),
    },
    paper: {
      marginRight: theme.spacing(2),
    },

    appBar: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      padding: '0 6px 0 24px',
      height: 54,
      borderBottom: '1px solid #E0E0E0',
      '& .MuiIconButton-root': {
        padding: 0,
      },
      [theme.breakpoints.only('md')]: {
        marginTop: 168,
      },
      [theme.breakpoints.down('sm')]: {
        position: 'fixed',
        bottom: 12,
        border: 0,
        height: 'auto',
        padding: 0,
        justifyContent: 'center',
        width: '100%',
        '& .MuiButton-root': {
          marginLeft: 0,
        },
      },
    },
    bold: {
      fontWeight: 500,
    },
    expendMore: {
      marginLeft: 16,
      '& .MuiTypography-root': {
        fontSize: '0.75rem',
        textTransform: 'none',
      },
      [theme.breakpoints.down('sm')]: {
        '& .MuiButton-label': {
          flexDirection: 'column',
          border: '1px solid #70707026',
          width: '24vw',
          height: 81,
        },
      },
    },
    contentDetails: {
      padding: '28px 24px',
      overflow: 'auto',
      height: 'calc(100vh - 225px)',
      [theme.breakpoints.only('md')]: {
        marginTop: 16,
      },
      [theme.breakpoints.down('md')]: {
        padding: 0,
      },
    },
    titleObjet: {
      fontWeight: 600,
      fontSize: '1rem',
      [theme.breakpoints.down('lg')]: {
        fontFamily: 'Roboto',
        fontSize: 16,
        fontWeight: 400,
      },
    },
    titleSender: {
      fontSize: '0.875rem',
      [theme.breakpoints.down('lg')]: {
        fontFamily: 'Roboto',
        fontSize: 14,
        fontWeight: 'normal',
        color: '#9E9E9E',
      },
    },
    iconFilter: {
      display: 'flex',
      justifyContent: 'flex-end',
      '& .MuiSvgIcon-root': {
        height: 14,
        width: 14,
      },
    },
    dateTime: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-end',
      width: 110,
      marginTop: 12,
      '& .MuiTypography-root': {
        fontSize: '0.875rem',
        lineHeight: 1,
      },
    },
    text: {
      paddingRight: 32,
      paddingBottom: 32,
      '& .MuiTypography-root': {
        fontSize: '0.875rem',
        textAlign: 'left',
      },
    },
    contentPieceJointe: {
      margin: '12px 0',
      display: 'flex',
      alignItems: 'start',
    },
    titlePiece: {
      fontSize: '0.875rem',
      marginRight: 12,
      minWidth: 102,
    },
    itemPieceJointe: {
      display: 'flex',
      alignItems: 'start',
      border: '1px solid #9E9E9E',
      padding: 4,
      maxWidth: 155,
      marginLeft: 6,
      marginBottom: 6,
      '& .MuiTypography-root': {
        fontSize: '0.75rem',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        '-webkit-line-clamp': 1,
        '-webkit-box-orient': 'vertical',
        whiteSpace: 'nowrap',
        maxWidth: 114,
        width: '100%',
      },
      '&:hover': {
        background: '#e9e9e9',
        cursor: 'pointer',
      },
      '& a': {
        color: 'inherit',
        textDecoration: 'none',
      },
    },
    iconPdf: {
      '& .MuiSvgIcon-root': {
        color: '#E53935',
      },
    },
    contentitemPieceJointe: {
      display: 'flex',
      alignItems: 'start',
      flexWrap: 'wrap',
      overflow: 'auto',
      height: 52,
    },
    nomRecept: {
      marginTop: '15px',
      '& .MuiTypography-root': {
        color: '#212121',
        fontWeight: 100,
        fontSize: '0.875rem',
      },
      [theme.breakpoints.down('lg')]: {
        fontFamily: 'Roboto',
        fontSize: 16,
        fontWeight: 400,
      },
    },
    paddingSide: {
      [theme.breakpoints.down('lg')]: {
        padding: '0px 16px',
      },
    },
    dateCreation: {
      fontFamilty: 'Roboto',
      fontSize: 12,
      color: '#9E9E9E',
    },
  })
);

export default useStyles;
