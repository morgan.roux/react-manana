import { MeUserInfoFragment } from '@lib/common/src/federation';
import {
  Total_Messagerie_Non_LusDocument,
  Total_Messagerie_Non_LusQuery,
  Total_Messagerie_Non_LusQueryVariables,
} from '@lib/common/src/graphql';
import { ApolloClient } from '@apollo/client';

export const fetchMessagerieTotalCount = async (context: {
  user: MeUserInfoFragment;
  graphql: ApolloClient<any>;
}): Promise<number> => {
  const response = await context.graphql.query<Total_Messagerie_Non_LusQuery, Total_Messagerie_Non_LusQueryVariables>({
    query: Total_Messagerie_Non_LusDocument,
    fetchPolicy: 'network-only',
  });
  return response.data.messagerieNonLus?.total ?? 0;
};
