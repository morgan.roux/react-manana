import { ModuleDefinition, mergeModules } from '@lib/common';
import moment from 'moment';
import informationLiaisonModule from './information-liaison/information-liaison.module';
import newInformationLiaisonModule from './new-information-liaison/information-liaison.module';
import messagerieModule from './messagerie/messagerie.module';
import partageIdeesModule from './partage-idees/partage-idees.module';

moment.locale('fr');

const communicationModule: ModuleDefinition = mergeModules(
  newInformationLiaisonModule,
  messagerieModule,
  partageIdeesModule
);

export default communicationModule;
