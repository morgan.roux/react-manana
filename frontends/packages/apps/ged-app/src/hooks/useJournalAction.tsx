import { useApplicationContext } from '@lib/common';
import {
  useCreateEfJournalMutation,
  useDeleteEfJournalMutation,
  useGenerateEfJournalsMutation,
  useUpdateEfJournalMutation,
} from '@lib/common/src/federation';
import { useState } from 'react';
import { JournalSave } from '../pages/comptabilites/Parametrage/Journal/ParametrageJournalForm';

export const useJournalAction = (refetch?: () => void) => {
  const { notify } = useApplicationContext();
  const [saved, setSaved] = useState<boolean>(false);
  const [saving, setSaving] = useState<boolean>(false);

  const [createJournal] = useCreateEfJournalMutation({
    onCompleted: () => {
      setSaved(true);
      setSaving(false);
      refetch && refetch();
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `erreur: ${err.message}`,
      });
      setSaving(false);
    },
  });

  const [updateJournal] = useUpdateEfJournalMutation({
    onCompleted: () => {
      setSaved(true);
      setSaving(false);
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `erreur: ${err.message}`,
      });
      setSaving(false);
    },
  });

  const [deleteJournal] = useDeleteEfJournalMutation({
    onCompleted: () => {
      refetch && refetch();
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `erreur: ${err.message}`,
      });
    },
  });

  const [generateJournals] = useGenerateEfJournalsMutation({
    onCompleted: () => {
      refetch && refetch();
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `Erreur lors de la génération de journal`,
      });
    },
  });

  const handleSave = ({ idType, code, id }: JournalSave) => {
    setSaved(false);
    setSaving(true);

    if (id) {
      updateJournal({
        variables: {
          input: {
            id,
            update: {
              code,
              idType,
            },
          },
        },
      });
    } else {
      createJournal({
        variables: {
          input: {
            eFJournal: {
              idType,
              code,
            },
          },
        },
      });
    }
  };

  const handleDelete = (id?: string) => {
    if (id) {
      deleteJournal({
        variables: {
          input: {
            id,
          },
        },
      });
    }
  };

  const generateJournal = () => {
    generateJournals()
  };

  return {
    handleSave,
    handleDelete,
    saving,
    saved,
    setSaved,
    generateJournal
  };
};
