import {
  FullDocumentCategorieFragment,
  useEfCompteComptablesQuery,
  EfCompteComptableInfoFragment,
  useEfModeleEcrituresLazyQuery,
  useEfModeleEcrituresQuery,
} from '@lib/common/src/federation';
import moment from 'moment';
import { formatMoney } from '@lib/common';

interface FormatFactureInterface {
  factures: FullDocumentCategorieFragment[];
}


const getNumeroCompteComptableTva = (idTva: string, document: FullDocumentCategorieFragment) => {
  const tvaLigne = document.factureProbablyUsedModeleEcriture?.lignes?.find(ligne => ligne.idEfTva === idTva)
  return tvaLigne?.compteComptableTva?.numero ?? ''
}

const formatExportFacture = ({ factures }: FormatFactureInterface) => {
  const facturesToExport = factures.map((facture) => {
    if (!facture.factureLignes?.length) return [];
    const piece = `${facture.factureNumeroPiece || Math.floor(Math.random() * 10000000000)}`;

    return [
      {
        date: moment(facture.factureDate).format('DD/MM/YYYY'),
        dateStatut: moment(facture.createdAt).format('DD/MM/YYYY'),
        journal: 'AC',
        compte: facture.factureCompteComptable?.numero ?? '',
        piece,
        libelle: `${(facture.origineAssocie as any)?.nom || (facture.origineAssocie as any)?.fullName || ''} ${facture.numeroFacture
          }`,
        debit: 0,
        credit: facture.factureTotalTtc ?? 0,
        monnaie: 'E',
      },
      ...facture.factureLignes.map((ligne) => {
        return {
          date: moment(facture.factureDate).format('DD/MM/YYYY'),
          dateStatut: moment(facture.createdAt).format('DD/MM/YYYY'),
          journal: 'AC',
          compte: ligne.compteComptable?.numero ?? '',
          piece,
          libelle: `${(facture.origineAssocie as any)?.nom || (facture.origineAssocie as any)?.fullName || ''} ${facture.numeroFacture
            }`,
          debit: ligne.montantHt,
          credit: 0,
          monnaie: 'E',
        };
      }),
      ...facture.factureLignes.reduce((lignesTva, ligne) => {

        if (!ligne.tva?.id) {
          return lignesTva
        }
        const ligneNumeroCompteTva = getNumeroCompteComptableTva(ligne.tva.id, facture)

        const indexInLignesTva = lignesTva.findIndex((ligneTva) => {
          return ligneTva.compte === ligneNumeroCompteTva
        })

        if (indexInLignesTva < 0) {
          lignesTva.push({
            date: moment(facture.factureDate).format('DD/MM/YYYY'),
            dateStatut: moment(facture.createdAt).format('DD/MM/YYYY'),
            journal: 'AC',
            compte: ligneNumeroCompteTva,
            piece,
            libelle: `${(facture.origineAssocie as any)?.nom || (facture.origineAssocie as any)?.fullName || ''} ${facture.numeroFacture
              }`,
            debit: ligne.montantTva,
            credit: 0,
            monnaie: 'E',

          })
        }
        else {

          lignesTva[indexInLignesTva] = {
            ...lignesTva[indexInLignesTva],
            debit: lignesTva[indexInLignesTva].debit + ligne.montantTva

          }

        }

        return lignesTva
      }, ([] as any[])),
    ];
  });
  return facturesToExport.flat().filter((ligne) => ligne.debit > 0 || ligne.credit > 0).map((ligne) => {

    return {
      ...ligne,
      debit: `${formatMoney(ligne.debit || 0)}`,
      credit: `${formatMoney(ligne.credit || 0)}`
    }

  });
};

export const useExportFacture = () => {
  const getFacturesToExport = (factures: FullDocumentCategorieFragment[]) => {
    return formatExportFacture({ factures });
  };

  return { getFacturesToExport };
};
