import { useApplicationContext } from '@lib/common';
import { useState } from 'react';
import {
  useCreateEfTvaMutation,
  useDeleteEfTvaMutation,
  useGenerateEfTvasMutation,
  useUpdateEfTvaMutation,
} from '../../../../libs/common/src/federation';
import { TvaSave } from '../pages/comptabilites/Parametrage/Tva/ParametrageTvaForm';

export const useTvaAction = (refetch?: () => void) => {
  const { notify } = useApplicationContext();
  const [saved, setSaved] = useState<boolean>(false);
  const [saving, setSaving] = useState<boolean>(false);

  const [createTva] = useCreateEfTvaMutation({
    onCompleted: () => {
      setSaved(true);
      setSaving(false);
      refetch && refetch();
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `erreur: ${err.message}`,
      });
      setSaving(false);
    },
  });

  const [updateTva] = useUpdateEfTvaMutation({
    onCompleted: () => {
      setSaved(true);
      setSaving(false);
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `erreur: ${err.message}`,
      });
      setSaving(false);
    },
  });

  const [deleteTva] = useDeleteEfTvaMutation({
    onCompleted: () => {
      refetch && refetch();
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `erreur: ${err.message}`,
      });
    },
  });

  const [generateTvas] = useGenerateEfTvasMutation({
    onCompleted: () => {
      refetch && refetch();
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `Erreur lors de la generation de tva`,
      });
    },
  });

  const handleSave = ({ id, taux, libelle, dateDebutExercice, dateFinExercice, numeroCompte }: TvaSave) => {
    setSaved(false);
    setSaving(true);

    if (id) {
      updateTva({
        variables: {
          input: {
            id,
            update: {
              taux,
              libelle,
              dateDebutExercice,
              dateFinExercice,
              numeroCompte,
            },
          },
        },
      });
    } else {
      createTva({
        variables: {
          input: {
            taux,
            libelle,
            dateDebutExercice,
            dateFinExercice,
            numeroCompte,
          },
        },
      });
    }
  };

  const handleGenerate = () => {
    generateTvas()
  }

  const handleDelete = (id?: string) => {
    if (id) {
      deleteTva({
        variables: {
          input: {
            id,
          },
        },
      });
    }
  };

  return { handleSave, handleDelete, saving, saved, setSaved, handleGenerate };
};
