import React, { FC } from 'react';

import DocumentPage from './GestionEspaceFacturation/GestionEspaceFacturation';

const EspaceDocumentaire: FC<{}> = () => {
  return <DocumentPage type="AUTRES" />;
};
export default EspaceDocumentaire;
