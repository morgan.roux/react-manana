import { IDocumentSearch, PartenaireSelect } from '@lib/common';
import { CustomDatePicker, CustomSelectMenu, DebouncedSearchInput, NewCustomButton } from '@lib/ui-kit';
import { Box, Radio, Typography } from '@material-ui/core';
import React, { Dispatch, FC, SetStateAction, useState, ChangeEvent } from 'react';
import filter_alt from '../../assets/img/filter_alt.svg';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import ViewStreamIcon from '@material-ui/icons/ViewStream';
import { useStyles } from './styles';
import moment from 'moment';

interface HeaderPageProps {
  title?: string;
  withSearch?: boolean;
  withPartenaireSelect?: boolean;
  withViewButton?: boolean;
  withFacture?: boolean;
  withDetailButton?: boolean;
  // setSearchVariables: Dispatch<SetStateAction<IDocumentSearch>>;
  // searchVariables: IDocumentSearch;
  withPeriodFilter?: boolean;
  withPartenairService?: boolean;
  withMontant?: boolean;
  onClick?: (event: React.MouseEvent<{}>) => void;
}

const HeaderPage: FC<HeaderPageProps> = ({
  title,
  withSearch,
  withPartenaireSelect,
  withViewButton,
  withFacture,
  withDetailButton,
  // setSearchVariables,
  // searchVariables,
  withPeriodFilter,
  withPartenairService,
  withMontant,
  onClick,
}) => {
  const [openFilterModal, setOpenFilterModal] = useState<boolean>(false);
  const [dateDebut, setDateDebut] = useState<Date>(new Date());
  const [dateFin, setDateFin] = useState<Date>(new Date());
  const [montantMinimal, setMontantMinimal] = useState<any>();
  const [montantMaximal, setMontantMaximal] = useState<any>();
  const [searchVariables, setSearchVariables] = useState<any>();

  const iconFilter = <img src={filter_alt} alt="filter" />;
  const classes = useStyles();

  const handleFactureFiltere = (id?: any) => {
    handleFactureFiltere && handleFactureFiltere(id);
    setOpenFilterModal(!openFilterModal);
  };

  const [selectedValue, setSelectedValue] = useState('a');

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSelectedValue(event.target.value);
  };

  const factureFilter = [
    { id: 1, label: 'toutes', name: 'Toutes les facture', code: 'TOUTES' },
    { id: 2, label: 'nouvelle', name: 'Nouvelle acture', code: 'NOUVELLE' },
    { id: 3, label: 'valide', name: 'Facture validées', code: 'VALIDE' },
    { id: 4, label: 'rejets', name: 'Rejets pharmacie', code: 'REJETS' },
  ];

  const periodeFilter = [
    <Box display="flex">
      <CustomDatePicker
        label="De"
        value={dateDebut}
        InputLabelProps={{
          shrink: true,
        }}
        onChange={(value) => {
          // handleChangeSearchVariables({
          //   type: 'FACTURE',
          //   montantMinimal: searchVariables.montantMinimal,
          //   montantMaximal: searchVariables.montantMaximal,
          //   dateDebut: moment(value).toISOString(),
          //   dateFin: searchVariables.dateFin,
          // });
          handleChangeFilterDate('dateDebut', value);
        }}
        fullWidth
        className={classes.dateField}
      />
      <CustomDatePicker
        label="À"
        value={dateFin}
        InputLabelProps={{
          shrink: true,
        }}
        onChange={(value) => {
          // handleChangeSearchVariables({
          //   type: 'FACTURE',
          //   montantMinimal: searchVariables.montantMinimal,
          //   montantMaximal: searchVariables.montantMaximal,
          //   dateDebut: searchVariables.dateDebut,
          //   dateFin: moment(value).toISOString(),
          // });
          handleChangeFilterDate('dateFin', value);
        }}
        fullWidth
        className={classes.dateField}
      />
    </Box>,
  ];

  const montanFilter = [
    <Box display="flex" justifyContent="space-between">
      <Box>
        min : <span className={classes.filterSubtitle}>{(montantMinimal || 0).toFixed(2)}&nbsp;€</span>
      </Box>
      <Box>
        max : <span className={classes.filterSubtitle}>{(montantMaximal || 0).toFixed(2)}&nbsp;€</span>
      </Box>
    </Box>,
  ];

  const partenaireFilter = [
    {
      id: 1,
      name: 'Tous les partenaires de services',
      label: '',
      code: 'TOUS',
      radioGroup: (
        <Radio
          checked={selectedValue === 'a'}
          onChange={handleChange}
          value="a"
          name="radio-button-demo"
          inputProps={{ 'aria-label': 'A' }}
        />
      ),
    },
    {
      id: 2,
      name: 'Mecalux',
      label: '',
      code: 'MECALUX',
      radioGroup: (
        <Radio
          checked={selectedValue === 'b'}
          onChange={handleChange}
          value="b"
          name="radio-button-demo"
          inputProps={{ 'aria-label': 'B' }}
        />
      ),
    },
    {
      id: 3,
      name: 'DHL',
      label: '',
      code: 'DHL',
      radioGroup: (
        <Radio
          checked={selectedValue === 'c'}
          onChange={handleChange}
          value="c"
          name="radio-button-demo"
          inputProps={{ 'aria-label': 'C' }}
        />
      ),
    },
  ];

  const handleChangeFilterDate = (name: string, value: any) => {
    setSearchVariables((prev: any) => ({
      ...prev,
      dateDebut: name === 'dateDebut' ? moment(value).toISOString() : searchVariables.dateDebut,
      dateFin: name === 'dateDebut' ? searchVariables.dateFin : moment(value).toISOString(),
    }));
  };

  return (
    <Box className={classes.root}>
      <Box>
        <Typography style={{ fontSize: '22px', fontFamily: 'Roboto', fontWeight: 600 }}>{title}</Typography>
      </Box>
      <Box style={{ display: 'flex' }}>
        {withSearch ? (
          <Box style={{ marginRight: '16px' }}>
            <DebouncedSearchInput
              wait={500}
              outlined
              onChange={() => {}}
              value={''}
              placeholder="Rechercher"
              className={classes.recherche}
            />
          </Box>
        ) : null}
        <Box>
          {withPartenaireSelect ? (
            <PartenaireSelect
              // noTitle={isMobile}
              filterIcon={<img src={filter_alt} alt="filter" style={{ filter: 'brightness(10) invert(1)' }} />}
              value={''}
              index="laboratoire"
              onChange={() => {
                // redirectTo({
                //   ...params,
                //   idLabo: value?.id ? [value.id] : undefined,
                // });
              }}
            />
          ) : null}
        </Box>

        {withPartenairService ? (
          <Box>
            <PartenaireSelect
              // noTitle={isMobile}
              filterIcon={<img src={filter_alt} alt="filter" style={{ filter: 'brightness(10) invert(1)' }} />}
              value={''}
              index="partenaire"
              onChange={() => {
                // redirectTo({
                //   ...params,
                //   idLabo: value?.id ? [value.id] : undefined,
                // });
              }}
              partenaireClassName={ classes.buttonFacture}
            />
          </Box>
        ) : null}

        <Box>
          {withViewButton ? (
            <NewCustomButton
              // className={classes.vue}
              onClick={onClick}
              startIcon={<FormatListBulletedIcon />}
              theme="transparent"
              shadow={false}
              style={{
                textTransform: 'none',
                fontWeight: 400,
                marginRight: 24,
                paddingInline: '10px !important',
                background: '#F2F2F2',
                height: '50px',
                marginLeft: '16px',
              }}
            >
              Vue liste
            </NewCustomButton>
          ) : null}
        </Box>
        {withFacture ? (
          <Box>
            <CustomSelectMenu
              buttonLabel="Toutes les factures"
              listFilters={factureFilter}
              handleClickItem={handleFactureFiltere}
              iconFilter={iconFilter}
              className={classes.buttonFacture}
            />
          </Box>
        ) : null}

        {withPeriodFilter ? (
          <Box>
            <CustomSelectMenu
              buttonLabel="Période"
              listFilters={periodeFilter}
              handleClickItem={handleFactureFiltere}
              iconFilter={iconFilter}
              className={classes.buttonFacture}
            />
          </Box>
        ) : null}

        {withMontant ? (
          <Box>
            <CustomSelectMenu
              buttonLabel="Montant"
              listFilters={montanFilter}
              handleClickItem={handleFactureFiltere}
              iconFilter={iconFilter}
              className={classes.buttonFacture}
            />
          </Box>
        ) : null}

        {withDetailButton ? (
          <NewCustomButton
            // className={classes.vue}
            onClick={onClick}
            startIcon={<ViewStreamIcon />}
            theme="transparent"
            shadow={false}
            style={{
              textTransform: 'none',
              fontWeight: 400,
              marginRight: 24,
              paddingInline: '10px !important',
              background: '#F2F2F2',
              height: '50px',
              marginLeft: '16px',
            }}
          >
            Vue détail
          </NewCustomButton>
        ) : null}
      </Box>
    </Box>
  );
};

export default HeaderPage;
