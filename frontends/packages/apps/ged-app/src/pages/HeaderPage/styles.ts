import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: '24px',
            [theme.breakpoints.down('md')]: {
                display: 'none',
            }
        },
        recherche: {
            width: '320px',
        },
        buttonFacture: {
            marginLeft: '16px',
            '& .main-MuiButtonBase-root': {
                height: '50px'
            },
        },
        dateField: {
            '& button': {
                padding: '0px !important',
            },
            '& input': {
                fontSize: '14px !important',
            },
            '& div': {
                paddingRight: '4px !important',
            },
        },
        filterSubtitle: {
            fontSize: 14,
            fontFamily: 'Roboto',
            fontWeight: 'bold',
            marginTop: 16,
            marginBottom: 16,
        },
    })
)