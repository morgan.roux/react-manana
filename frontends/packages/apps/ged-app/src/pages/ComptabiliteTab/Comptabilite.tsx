import { AppBar, Box, IconButton, Tab, Typography } from '@material-ui/core';
import { TabContext, TabList, TabPanel, TabPanelProps } from '@material-ui/lab';
import React, { useState, ChangeEvent } from 'react';
import CompteComptable from '../comptabilites/CompteComptable/CompteComptable';
//import ModeleEcritureAutomatique from '../comptabilites/ModeleEcritureAutomatique/ModeleEcritureAutomatique';
import ParametrageComptable from '../comptabilites/Parametrage/ParametrageComptable';
import { useStyles } from './styles';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { useHistory, useParams } from 'react-router';
import { FC } from 'react';
import classnames from 'classnames';

const Comptabilite = () => {
  const [tabValue, setTabValue] = useState<string>('compte-comptable');
  const history = useHistory();
  const { tab: value } = useParams<{ tab: string }>();
  const handleChange = (event: ChangeEvent<{}>, newValue: string) => {
    history.push(`/db/comptabilite/${newValue}`);
    // setValue(newValue);
    setTabValue(newValue);
  };
  const classes = useStyles();
  const CustomTabPanel: FC<TabPanelProps> = ({ children, ...props }) => {
    return (
      <TabPanel {...props} classes={{ root: classes.tabpanelRoot }}>
        {children}
      </TabPanel>
    );
  };
  return (
    <Box className={classnames(classes.root, tabValue === 'compte-comptable' ? classes.comptabiliteClassName : '')}>
      <Box className={classes.header}>
        <IconButton color="inherit" onClick={() => {}}>
          <ArrowBackIcon />
        </IconButton>
        <Typography style={{ margin: 'auto', fontSize: '24px', fontFamily: 'Roboto', fontWeight: 600 }}>
          Comptabilité
        </Typography>
      </Box>
      <Box py={2} px={3}>
        <TabContext value={value || 'compte-comptable'}>
          <AppBar color="transparent" position="static" elevation={0}>
            <TabList onChange={handleChange} aria-label="simple tabs example" className={classes.tablistRoot}>
              <Tab
                classes={{ root: classes.tab, selected: classes.selected }}
                label="Compte Comptable"
                value="compte-comptable"
              />
              <Tab
                classes={{ root: classes.tab, selected: classes.selected }}
                label="Paramétrage Comptable"
                value="parametre-comptable"
              />
            {/*<Tab
                classes={{ root: classes.tab, selected: classes.selected }}
                label="Modèle d'Écriture Automatique"
                value="modele-ecriture-automatique"
            />*/}
            </TabList>
          </AppBar>
          <CustomTabPanel value="compte-comptable" classes={{}}>
            <CompteComptable />
          </CustomTabPanel>
          <CustomTabPanel value="parametre-comptable">
            <ParametrageComptable />
          </CustomTabPanel>
          {/*<CustomTabPanel value="modele-ecriture-automatique">
            <ModeleEcritureAutomatique />
          </CustomTabPanel>*/}
        </TabContext>
      </Box>
    </Box>
  );
};

export default Comptabilite;
