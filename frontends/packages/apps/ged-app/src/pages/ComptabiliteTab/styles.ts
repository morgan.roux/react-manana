import { createStyles, lighten, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    comptabiliteClassName: {
      overflowY: 'hidden',
      height: 'calc(100vh - 88px)',
    },
    header: {
      position: 'sticky',
      top: 0,
      zIndex: 999,
      width: '100%',
      height: 70,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      color: theme.palette.common.white,
      background: lighten(theme.palette.primary.main, 0.1),
    },
    tablistRoot: {
      borderBottom: '1px solid #e0e0e0',
    },
    tab: {
      fontSize: '16px',
      textTransform: 'initial',
      fontFamily: 'Roboto',
      fontWeight: 400,
    },
    selected: {
      color: `${theme.palette.primary.main} !important`,
    },
    tabpanelRoot: {
      padding: 0,
    },
  })
);
