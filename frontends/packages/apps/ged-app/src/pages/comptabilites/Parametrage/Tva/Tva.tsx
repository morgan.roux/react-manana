import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { ConfirmDeleteDialog, NewCustomButton, Table, TableChange, TopBar } from '@lib/ui-kit';
import { ParametrageTvaForm } from './ParametrageTvaForm';
import { useColumnTva } from './useColumn';
import { useStyles } from './styles';
import { EfTvaFilter, EfTvaInfoFragment, useEfTvaLazyQuery } from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';
import { useTvaAction } from '../../../../hooks/useTvaAction';

export interface TvaProps {
  dateDebutExercice: Date;
  dateFinExercice: Date;
}

export const Tva: FC<TvaProps> = ({ dateDebutExercice, dateFinExercice }) => {
  const classes = useStyles();
  const { currentPharmacie } = useApplicationContext();

  const [openEditDialog, setOpenEditDialog] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [mode, setMode] = useState<'ajout' | 'modifier'>('ajout');
  const [tvaToEdit, setTvaToEdit] = useState<EfTvaInfoFragment>();
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(12);

  const [loadTva, loadingTva] = useEfTvaLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const { handleDelete, handleSave, saved, setSaved, saving, handleGenerate } = useTvaAction(loadingTva.refetch);

  useEffect(() => {
    if (saved) {
      setOpenEditDialog(false);
      setSaved(false);
    }
  }, [saved]);

  useEffect(() => {
    handleSearch({ skip, take, searchText: '' });
  }, [dateDebutExercice, dateFinExercice]);

  const handleSearch = ({ skip, take, searchText }: TableChange) => {
    const filterAnd: EfTvaFilter[] = [
      {
        idPharmacie: {
          eq: currentPharmacie.id,
        },
        dateDebutExercice: {
          between: {
            lower: dateDebutExercice,
            upper: dateFinExercice,
          },
        },
      },
    ];
    if (searchText)
      filterAnd.push({
        libelle: {
          iLike: `%${searchText}%`,
        },
      });
    loadTva({
      variables: {
        filter: {
          and: filterAnd,
        },
        paging: {
          offset: skip,
          limit: take,
        },
      },
    });
    setSkip(skip);
    setTake(take);
  };

  const handleOpenAdd = () => {
    setTvaToEdit(undefined);
    setOpenEditDialog(true);
    setMode('ajout');
  };

  const handleClickMenu = (tva: EfTvaInfoFragment) => {
    setTvaToEdit(tva);
  };

  const handleModifier = () => {
    setOpenEditDialog(true);
    setMode('modifier');
  };

  const handleSupprimer = () => {
    setOpenDeleteDialog(true);
  };

  const columns = useColumnTva({ handleClickMenu, handleModifier, handleSupprimer, tvaToEdit: tvaToEdit });
  const data = loadingTva.data?.eFTvas?.nodes || [];

  return (
    <Box mt={2}>
      <Table
        error={loadingTva.error}
        loading={loadingTva.loading}
        search={false}
        data={data}
        selectable={false}
        columns={columns}
        rowsTotal={loadingTva.data?.eFTvas?.totalCount || 0}
        onRunSearch={handleSearch}
        topBarComponent={(searchComponent) => (
          <TopBar
            handleOpenAdd={handleOpenAdd}
            searchComponent={
              <Box display="flex" flexDirection="row">
                {searchComponent}{' '}
                {!data.length && (
                  <NewCustomButton
                    className={`${classes.btn} ${classes.generateBtn}`}
                    onClick={(event) => {
                      event.preventDefault();
                      event.stopPropagation();
                      handleGenerate();
                    }}
                  >
                    Générer TVA
                  </NewCustomButton>
                )}
              </Box>
            }
            titleButton="AJOUTER TVA"
            titleTopBar="Taux de TVA"
            withFilter={false}
            style={'width: auto '}
            className={classes.topbar}
            titleTopBarClassName={classes.titleTobarProps}
          />
        )}
      />

      <ParametrageTvaForm
        mode={mode}
        openTvaForm={openEditDialog}
        setOpenTvaForm={setOpenEditDialog}
        tva={tvaToEdit}
        action={{ handleSave, saving, dateDebutExercice, dateFinExercice }}
      />
      <ConfirmDeleteDialog
        title="Suppression"
        content="Etes-vous sûr de vouloir supprimer ce TVA ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={() => {
          {
            handleDelete(tvaToEdit?.id);
            setOpenDeleteDialog(false);
          }
        }}
      />
    </Box>
  );
};
