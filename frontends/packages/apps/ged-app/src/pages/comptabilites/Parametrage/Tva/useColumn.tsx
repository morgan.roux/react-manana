import { Column } from '@lib/ui-kit';
import { Box, Popover, Typography } from '@material-ui/core';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import React from 'react';
import { useStyles } from './styles';
import { EfTvaInfoFragment } from '@lib/common/src/federation';

interface UseColumnTvaProps {
  handleClickMenu: (tva: EfTvaInfoFragment) => void;
  handleModifier: () => void;
  handleSupprimer: () => void;
  tvaToEdit?: EfTvaInfoFragment;
}

export const useColumnTva = ({ handleClickMenu, handleModifier, handleSupprimer, tvaToEdit }: UseColumnTvaProps) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const handleClick = (event: React.MouseEvent<HTMLElement>, row: EfTvaInfoFragment) => {
    setAnchorEl(event.currentTarget);
    handleClickMenu(row);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClickEdit = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    e.stopPropagation();
    handleClose();
    handleModifier();
  };

  const handleClickDelete = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    e.stopPropagation();
    handleClose();
    handleSupprimer();
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  const columns: Column[] = [
    {
      name: 'libelle',
      label: 'Libellé',
      renderer: (row: EfTvaInfoFragment) => {
        return <Box>{row.libelle}</Box>;
      },
    },
    {
      name: 'tva',
      label: 'Taux de TVA',
      renderer: (row: EfTvaInfoFragment) => {
        return <Box>{row.taux} %</Box>;
      },
    },

    {
      name: 'numeroCompte',
      label: 'Numéro de compte',
      renderer: (row: EfTvaInfoFragment) => {
        return <Box>{row.numeroCompte || 'Non paramétré'}</Box>;
      },
    },
    {
      name: 'action',
      label: '',
      renderer: (row: EfTvaInfoFragment) => {
        return (
          <div>
            <Box aria-describedby={id} onClick={(e) => handleClick(e, row)} textAlign="right">
              <MoreHorizIcon />
            </Box>
            <Popover
              id={id}
              open={open && row?.id === tvaToEdit?.id}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
            >
              <Box className={classes.actions} onClick={handleClickEdit}>
                <Box>
                  <EditIcon />
                </Box>
                <Box>
                  <Typography style={{ fontSize: '16px', fontFamily: 'Roboto', marginLeft: '16px' }}>
                    Modifier
                  </Typography>
                </Box>
              </Box>
              <Box className={classes.actions} onClick={handleClickDelete}>
                <Box>
                  <DeleteIcon />
                </Box>
                <Box>
                  <Typography style={{ fontSize: '16px', fontFamily: 'Roboto', marginLeft: '16px' }}>
                    Supprimer
                  </Typography>
                </Box>
              </Box>
            </Popover>
          </div>
        );
      },
    },
  ];
  return columns;
};
