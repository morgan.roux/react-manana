import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    textDialogue: {
      width: '450px',
    },
    topbar: {
      width: 'auto !important',
      fontWeight: 'bold',
      fontSize: 18,
      fontFamily: 'Roboto',
    },
    titleTobarProps: {
      fontSize: '18px !important',
      fontFamily: 'Roboto !important',
      fontWeight: 600,
    },
    actions: {
      width: '190px',
      display: 'flex',
      alignItems: 'center',
      padding: '16px',
      cursor: 'pointer',
    },
    btn: {
      fontWeight: 'bold',
      margin: 0,
      height: 50,
    },
    generateBtn: {
      background: '#e6e6e6',
      marginLeft: 16,
    },
    button: {
      width: '100%',
      display: 'flex',
      justifyContent: 'flex-end',
    },
  })
);
