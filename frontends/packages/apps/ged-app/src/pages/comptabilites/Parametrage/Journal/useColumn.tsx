import { EfJournalInfoFragment } from '@lib/common/src/federation';
import React from 'react';
import { Column } from '@lib/ui-kit';
import { useStyles } from './styles';
import { Box, Popover, Typography } from '@material-ui/core';
import { Delete, Edit, MoreHoriz } from '@material-ui/icons';

interface useColumnProps {
  handleModifier: () => void;
  handleSupprimer: () => void;
  handleClickMenu: (journal: EfJournalInfoFragment) => void;
  journalToEdit?: EfJournalInfoFragment;
}

export const useColumn = ({ handleModifier, handleClickMenu, handleSupprimer, journalToEdit }: useColumnProps) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);

  const handleClick = (event: React.MouseEvent<HTMLElement>, row: EfJournalInfoFragment) => {
    setAnchorEl(event.currentTarget);
    handleClickMenu(row);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClickEdit = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    e.stopPropagation();
    handleClose();
    handleModifier();
  };

  const handleClickDelete = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    e.stopPropagation();
    handleClose();
    handleSupprimer();
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const columns: Column[] = [
    {
      name: 'code',
      label: 'Code journal',
      renderer: (row: EfJournalInfoFragment) => {
        return <Box>{row.code}</Box>;
      },
    },
    {
      name: 'journal',
      label: 'Journal',
      renderer: (row: EfJournalInfoFragment) => {
        return <Box>{row.type?.libelle}</Box>;
      },
    },
    
    {
      name: 'action',
      label: '',
      renderer: (row: EfJournalInfoFragment) => {
        return (
          <div>
            <Box aria-describedby={id} onClick={(e) => handleClick(e, row)} textAlign="right">
              <MoreHoriz />
            </Box>
            <Popover
              id={id}
              open={open && row?.id === journalToEdit?.id}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
            >
              <Box className={classes.actions} onClick={(e) => handleClickEdit(e)}>
                <Box>
                  <Edit />
                </Box>
                <Box>
                  <Typography style={{ fontSize: '16px', fontFamily: 'Roboto', marginLeft: '16px' }}>
                    Modifier
                  </Typography>
                </Box>
              </Box>
              <Box className={classes.actions} onClick={(e) => handleClickDelete(e)}>
                <Box>
                  <Delete />
                </Box>
                <Box>
                  <Typography style={{ fontSize: '16px', fontFamily: 'Roboto', marginLeft: '16px' }}>
                    Supprimer
                  </Typography>
                </Box>
              </Box>
            </Popover>
          </div>
        );
      },
    },
  ];
  return columns;
};
