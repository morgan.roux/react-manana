import { useApplicationContext } from '@lib/common';
import { CustomFormTextField, CustomModal } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { JournalTypeInput } from '../../../../components/comptabilite/JournalTypeInput';
import { useStyles } from './styles';
import { EfJournalInfoFragment } from '@lib/common/src/federation';
import { useEffect } from 'react';

interface ParametrageJournalFormProps {
  openForm: boolean;
  setOpenForm: (open: boolean) => void;
  mode: 'ajout' | 'modifier';
  journal?: EfJournalInfoFragment;
  action: { handleSave: (journal: JournalSave) => void; saving: boolean };
}

export interface JournalSave {
  idType: string;
  code: string;
  id?: string;
}
const initialValue: JournalSave = {
  idType: '',
  code: '',
};

export const ParametrageJournalForm: FC<ParametrageJournalFormProps> = ({ openForm, setOpenForm, mode, journal, action }) => {
  const { isMobile } = useApplicationContext();
  const classes = useStyles();
  const { handleSave, saving } = action;

  const [values, setValues] = useState<JournalSave>(initialValue);
  const isValid = values.idType && values.code;

  useEffect(() => {
    if (mode === 'modifier' && journal) {
      setValues({ code: journal.code, idType: journal.type?.id || '', id: journal.id });
    } else {
      setValues(initialValue);
    }
  }, [journal, mode]);

  const handleChange = (event: any) => {
    setValues((prev) => ({ ...prev, [event.target.name]: event.target.value }));
  };

  return (
    <CustomModal
      open={openForm}
      setOpen={setOpenForm}
      title={mode === 'modifier' ? 'Modification du Code Journal' : 'Nouveau Code Journal'}
      withBtnsActions
      closeIcon
      headerWithBgColor
      fullScreen={!!isMobile}
      disabledButton={!isValid || saving}
      onClickConfirm={() => handleSave(values)}
      maxWidth="md"
    >
      <div>
        <Box className={classes.root}>
          <JournalTypeInput idType={values.idType} onChange={handleChange} />
        </Box>
        <Box>
          <CustomFormTextField
            type="text"
            label="Code Journal"
            name="code"
            placeholder="Code Journal"
            onChange={handleChange}
            value={values.code}
            className={classes.textDialogue}
          />
        </Box>
      </div>
    </CustomModal>
  );
};
