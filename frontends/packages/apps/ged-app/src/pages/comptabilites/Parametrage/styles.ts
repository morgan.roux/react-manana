import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    titre: {
      padding: '20px',
      marginLeft: '-20px',
    },
    date: {
      display: 'flex',
      flexDirection: 'column',
    },
    dateItem: {
      display: 'flex',
    },
    datePicker: {
      width: '570px !important',
      marginRight: '30px',
      marginBottom: 24,
    },
    periode: {
      display: 'flex',
      width: '100%',
    },
    customFormText: {
      width: '80px',
    },
    cloture: {
      display: 'flex',
      alignItems: 'center',
    },
    topbar: {
      width: 'auto !important',
      fontWeight: 'bold',
      fontSize: 18,
      fontFamily: 'Roboto',
    },
    titleTobarProps: {
      fontSize: '18px !important',
      fontFamily: 'Roboto !important',
      fontWeight: 600,
    },
    actions: {
      width: '190px',
      display: 'flex',
      alignItems: 'center',
      padding: '16px',
      cursor: 'pointer',
    },
  })
);
