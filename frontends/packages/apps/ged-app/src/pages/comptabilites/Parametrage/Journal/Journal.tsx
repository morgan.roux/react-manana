import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { ConfirmDeleteDialog, NewCustomButton, Table, TableChange, TopBar } from '@lib/ui-kit';
import { ParametrageJournalForm } from './ParametrageJournalForm';
import { useColumn } from './useColumn';
import { useStyles } from './styles';
import { EfJournalFilter, EfJournalInfoFragment, useEfJournalLazyQuery } from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';
import { useJournalAction } from '../../../../hooks/useJournalAction';

export interface JournalProps {}

export const Journal: FC<JournalProps> = () => {
  const classes = useStyles();
  const { currentPharmacie } = useApplicationContext();

  const [journalToEdit, setJournalToEdit] = useState<EfJournalInfoFragment>();
  const [openEditDialog, setOpenEditDialog] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [mode, setMode] = useState<'ajout' | 'modifier'>('ajout');

  const [loadJournal, loadingJournal] = useEfJournalLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const { handleDelete, saved, setSaved, handleSave, saving, generateJournal } = useJournalAction(
    loadingJournal.refetch
  );

  useEffect(() => {
    if (saved) {
      setOpenEditDialog(false);
      setSaved(false);
    }
  }, [saved]);

  const handleSearch = ({ skip, take, searchText }: TableChange) => {
    const filterAnd: EfJournalFilter[] = [
      {
        idPharmacie: {
          eq: currentPharmacie.id,
        },
      },
    ];
    if (searchText)
      filterAnd.push({
        code: {
          iLike: `%${searchText}%`,
        },
      });
    loadJournal({
      variables: {
        filter: {
          and: filterAnd,
        },
        paging: {
          offset: skip,
          limit: take,
        },
      },
    });
  };

  const toggleOpenEditDialog = () => {
    if (!openEditDialog) {
      setJournalToEdit(undefined);
    }
    setOpenEditDialog((prev) => !prev);
  };

  const handleOpenAdd = () => {
    toggleOpenEditDialog();
    setMode('ajout');
  };

  const handleClickMenu = (journal: EfJournalInfoFragment) => {
    setJournalToEdit(journal);
  };

  const handleModifier = () => {
    setOpenEditDialog(true);
    setMode('modifier');
  };

  const handleSupprimer = () => {
    setOpenDeleteDialog(true);
  };

  const columns = useColumn({ handleModifier, handleClickMenu, handleSupprimer, journalToEdit: journalToEdit });
  const data = loadingJournal.data?.eFJournals?.nodes || [];
  console.log('datajournal', data);

  return (
    <Box>
      <Table
        error={loadingJournal.error}
        loading={loadingJournal.loading}
        search={false}
        data={loadingJournal.data?.eFJournals?.nodes || []}
        selectable={false}
        columns={columns}
        rowsTotal={loadingJournal.data?.eFJournals?.totalCount || 0}
        onRunSearch={handleSearch}
        topBarComponent={(searchComponent) => (
          <TopBar
            handleOpenAdd={handleOpenAdd}
            searchComponent={
              <Box display="flex" flexDirection="row">
                {searchComponent}{' '}
                {!data.length && (
                  <NewCustomButton
                    className={`${classes.btn} ${classes.generateBtn}`}
                    // tslint:disable-next-line: jsx-no-lambda
                    onClick={(event) => {
                      event.preventDefault();
                      event.stopPropagation();
                      generateJournal();
                    }}
                  >
                    Générer Journal
                  </NewCustomButton>
                )}
              </Box>
            }
            titleButton="AJOUTER CODE JOURNALE"
            titleTopBar="Code des journaux"
            withFilter={false}
            style={'width: auto '}
            className={classes.topbar}
            titleTopBarClassName={classes.titleTobarProps}
          />
        )}
      />

      <ParametrageJournalForm
        mode={mode}
        openForm={openEditDialog}
        setOpenForm={setOpenEditDialog}
        journal={journalToEdit}
        action={{ handleSave, saving }}
      />
      <ConfirmDeleteDialog
        title="Suppression"
        content="Etes-vous sûr de vouloir supprimer ce journal ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={() => {
          handleDelete(journalToEdit?.id);
          setOpenDeleteDialog(false);
        }}
      />
    </Box>
  );
};
