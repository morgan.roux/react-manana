import { CustomDatePicker, CustomFormTextField, CustomSelect, Table, TopBar } from '@lib/ui-kit';
import { Box, Typography } from '@material-ui/core';
import moment from 'moment';
import React, { useState } from 'react';
import { Journal } from './Journal';
import { useStyles } from './styles';
import { Tva } from './Tva';

const ParametrageComptable = () => {
  const classes = useStyles();
  const [selectedDateDebut, setSelectedDateDebut] = useState<Date>(moment().startOf('year').toDate());
  const [selectedDateFin, setSelectedDateFin] = useState<Date>(moment().endOf('year').toDate());
  // const [repetition, setRepetition] = useState<number>(1);
  // const [periode, setPeriode] = useState<string>('');
  // const [mode, setMode] = useState<'ajout' | 'modifier' | 'supprimer'>('ajout');

  const handleDateDebutChange = (date: Date | null) => {
    setSelectedDateDebut(date || moment().startOf('year').toDate());
  };

  const handleDateFinChange = (date: Date | null) => {
    setSelectedDateFin(date || moment().endOf('year').toDate());
  };

  // const handleFrequenceChange = (event: any) => {
  //   setIdType(event.target.value);
  // };

  // const handlePeriodeChange = (event: any) => {
  //   setPeriode(event.target.value);
  // };

  // const listFrequence = [
  //   { id: 1, name: 'mois', label: 'Mois', code: 'MOIS' },
  //   { id: 2, name: 'semaine', label: 'Semaine', code: 'SEMAINE' },
  //   { id: 3, name: 'jour', label: 'Jour', code: 'JOUR' },
  // ];

  // const listPeriode = [
  //   { id: 1, name: 'annuelle', label: 'Annuelle', code: 'ANNUELLE' },
  //   { id: 2, name: 'mensuel', label: 'Mensuel', code: 'MENSUEL' },
  //   { id: 3, name: 'journalie', label: 'Journalié', code: 'JOURNALIE' },
  // ];

  return (
    <div className={classes.root}>
      <Box className={classes.date}>
        <Box className={classes.titre}>
          <Typography style={{ fontSize: '18px', fontFamily: 'Roboto', fontWeight: 700 }}>
            Options comptables
          </Typography>
        </Box>
        <Box className={classes.dateItem}>
          <Box>
            <CustomDatePicker
              defaultValue={new Date()}
              label="Date debut d'exercice"
              placeholder="Date début"
              onChange={handleDateDebutChange}
              name="dateDebut"
              value={selectedDateDebut}
              disablePast={false}
              className={classes.datePicker}
            />
          </Box>
          <Box>
            <CustomDatePicker
              defaultValue={new Date()}
              label="Date fin d'exercice"
              placeholder="Date fin"
              onChange={handleDateFinChange}
              name="dateDebut"
              value={selectedDateFin}
              disablePast={false}
              className={classes.datePicker}
            />
          </Box>
        </Box>
        {/* <Box className={classes.titre}>
          <Typography style={{ fontSize: '16px', fontFamily: 'Roboto', fontWeight: 400 }}>Période comptable</Typography>
        </Box>
        <Box className={classes.periode}>
          <CustomFormTextField
            type="number"
            onChange={(event: any) => {
              const nouveauRepetition = event.target.value ? parseInt(event.target.value, 10) : 1;
              if (nouveauRepetition > 0) {
                setRepetition(nouveauRepetition);
              }
            }}
            value={repetition}
            className={classes.customFormText}
          />
          <Box className={classes.cloture}>
            <Box>
              <CustomSelect
                listId="id"
                index="label"
                value={idType}
                onChange={handleFrequenceChange}
                list={listFrequence}
                style={{ textAlign: 'left', marginLeft: '16px', width: '220px' }}
                defaultValue="Mois"
              />
            </Box>
            <Box>
              <Typography
                style={{
                  marginLeft: '16px',
                  fontSize: '16px',
                  fontFamily: 'Roboto',
                  fontWeight: 400,
                  marginBottom: '12px',
                }}
              >
                après la fréquence de clôture
              </Typography>
            </Box>
          </Box>

          <Box className={classes.cloture}>
            <Box>
              <CustomSelect
                listId="id"
                index="label"
                value={periode}
                onChange={handlePeriodeChange}
                list={listPeriode}
                style={{ textAlign: 'left', marginLeft: '16px', width: '220px' }}
                defaultValue="Mois"
              />
            </Box>
            <Box>
              <Typography
                style={{
                  marginLeft: '16px',
                  fontSize: '16px',
                  fontFamily: 'Roboto',
                  fontWeight: 400,
                  marginBottom: '12px',
                }}
              >
                pour traiter la période
              </Typography>
            </Box>
          </Box>
        </Box> */}

        <Journal />
        <Tva dateDebutExercice={selectedDateDebut} dateFinExercice={selectedDateFin} />
      </Box>
    </div>
  );
};

export default ParametrageComptable;
