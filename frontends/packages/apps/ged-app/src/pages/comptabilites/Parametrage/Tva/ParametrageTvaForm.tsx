import { useApplicationContext } from '@lib/common';
import { CustomFormTextField, CustomModal, MontantInputField } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { EfTvaInfoFragment } from '../../../../../../../libs/common/src/federation';
import { useStyles } from './styles';
interface ParametrageTvaFormProps {
  openTvaForm: boolean;
  setOpenTvaForm: (open: boolean) => void;
  mode: 'ajout' | 'modifier';
  tva?: EfTvaInfoFragment;
  action: { handleSave: (tva: TvaSave) => void; saving: boolean; dateDebutExercice: Date; dateFinExercice: Date };
}

export interface TvaSave {
  taux: number;
  libelle: string;
  id?: string;
  dateDebutExercice: Date;
  dateFinExercice: Date;
  numeroCompte?: string;
}

export const ParametrageTvaForm: FC<ParametrageTvaFormProps> = ({ openTvaForm, setOpenTvaForm, mode, tva, action }) => {
  const classes = useStyles();
  const { isMobile } = useApplicationContext();
  const { handleSave, saving, dateDebutExercice, dateFinExercice } = action;

  const initialValue: TvaSave = {
    taux: 2.1,
    libelle: '',
    dateDebutExercice,
    dateFinExercice,
    numeroCompte: ''
  };

  const [values, setValues] = useState<TvaSave>(initialValue);

  useEffect(() => {
    if (mode === 'modifier' && tva) {
      setValues({...tva, numeroCompte: tva.numeroCompte || ''});
    } else {
      setValues(initialValue);
    }
  }, [tva, mode]);

  const handleChange = (name: string, values?: any) => {
    setValues((prev) => ({ ...prev, [name]: values }));
  };

  const isValid = values.libelle && (values.taux || values.taux === 0);
  return (
    <CustomModal
      open={openTvaForm}
      setOpen={setOpenTvaForm}
      title={mode === 'modifier' ? 'Modification TVA' : 'Nouveau TVA'}
      withBtnsActions
      closeIcon
      headerWithBgColor
      fullScreen={!!isMobile}
      disabledButton={!isValid || saving}
      onClickConfirm={(e) => {
        e.preventDefault();
        e.stopPropagation();
        handleSave(values);
      }}
      maxWidth="md"
    >
      <div>
        <Box className={classes.root}>
          <MontantInputField value={values.taux} onChangeValue={(value) => handleChange('taux', value)} suffix="%" />
        </Box>
        <Box>
          <CustomFormTextField
            type="text"
            label="Libellé"
            onChange={(event) => handleChange('libelle', event.target.value)}
            value={values.libelle}
            className={classes.textDialogue}
          />
        </Box>
        <Box>
          <CustomFormTextField
            type="text"
            label="Numéro de compte"
            onChange={(event) => handleChange('numeroCompte', event.target.value)}
            value={values.numeroCompte}
            className={classes.textDialogue}
          />
        </Box>
      </div>
    </CustomModal>
  );
};
