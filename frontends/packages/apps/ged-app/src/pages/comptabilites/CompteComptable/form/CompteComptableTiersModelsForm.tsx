import { useApplicationContext } from '@lib/common';
import { CustomFormTextField, CustomModal, SmallLoading } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import {
  EfCompteComptableInfoFragment,
  EfTvaInfoFragment,
  useEfModeleEcrituresLazyQuery,
  useEfModeleEcrituresQuery,
} from '@lib/common/src/federation';
import { useStyles } from './CompteComptableTiersModelsForm.styles';
import { ModeReglementInput } from '@lib/common/src/components/EspaceDocumentaire/NewGedForm/ModeReglementInput';
import ModelEctitureTable, {
  ModeleCompteLigne,
  ModeleCompteSave,
} from './ModelEcritureTiers/ModeleEcritureAutomatiqueForm';

export interface ModeleCompteFormValues {
  id?: string;
  titre: string;
  idReglementMode: string;
  withLocation: boolean;
  achats: CompteComptableLigne[];
  locations: CompteComptableLigne[];
  idOrigine?: string;
  idOrigineAssocie?: string;
  idCompteFournisseur?: string;
}

interface CompteComptableLigne {
  libelle: string;
  numeroCompte: string;
  numeroCompteTva: string;
  idEfTva: string;
}

/* const defaultValues: ModeleCompteFormValues = {
  titre: 'Modèle',
  idReglementMode: '',
  withLocation: false,
  achats: [],
  locations: [],
}; */

interface CompteComptableTiersModelsFormProps {
  tiersCompteComptable: EfCompteComptableInfoFragment;
  open: boolean;
  setOpen: (open: boolean) => void;
  action: { onSaveModeleCompte: (compte: ModeleCompteSave) => void; saving: boolean };
}

const initialLigne: ModeleCompteLigne = {
  isContrepartie: false,
  sens: 'DEBIT',
  compteComptable: undefined,
  libelle: '',
  categorie: 'ACHAT',
  idEfTva: ''
};

const initialValue: ModeleCompteSave = {
  titre: '',
  idModeReglement: '',
  lignes: [{ ...initialLigne }],
};

const CompteComptableTiersModelsForm: FC<CompteComptableTiersModelsFormProps> = ({
  tiersCompteComptable,
  open,
  setOpen,
  action,
}) => {
  const classes = useStyles();
  const { currentPharmacie } = useApplicationContext();
  const { saving, onSaveModeleCompte } = action;

  // const [values, setValues] = useState<ModeleCompteFormValues>(defaultValues);
  const [value, setValue] = useState<ModeleCompteSave>(initialValue);

  /*  const [loadTva, loadingTva] = useEfTvaLazyQuery({
    fetchPolicy: 'cache-and-network',
  }); */

  const loadingModeleEcriture = useEfModeleEcrituresQuery({
    fetchPolicy: 'network-only',
    variables: {
      filter: {
        idCompteFournisseur: {
          eq: tiersCompteComptable.id,
        },
        idPharmacie: {
          eq: currentPharmacie.id,
        },
      },
      paging: {
        offset: 0,
        limit: 1,
      },
    },
    onCompleted: (result) => {
      if (result.eFModeleEcritures.nodes.length) {
        const modeleEcriture = result?.eFModeleEcritures?.nodes[0];

        const lignes: ModeleCompteLigne[] = (modeleEcriture?.lignes || []).map(
          ({ compteComptable, categorie, libelle, tva, isContrepartie, sens,compteComptableTva   }) => {
            return {
              libelle: libelle || ``,
              idEfTva: tva.id,
              categorie: categorie || 'ACHAT',
              isContrepartie: isContrepartie || false,
              sens: sens || 'DEBIT',
              compteComptable: compteComptable || undefined,
              compteComptableTva: compteComptableTva || undefined,
            };
          }
        );

        setValue({
          id: modeleEcriture?.id,
          titre: modeleEcriture?.titre || `Modèle ${tiersCompteComptable.numero}`,
          idModeReglement: modeleEcriture?.idModeReglement || '',
          idCompteFournisseur: tiersCompteComptable?.id,
          lignes: lignes.length ? lignes : [{ ...initialLigne }],
        });
      }
    },
  });

  const handleChange = (name: string, value?: any) => {
    setValue((prev) => ({ ...prev, [name]: value }));
  };
  const handleChangeEvent = (e: ChangeEvent<HTMLInputElement>) => handleChange(e.target.name, e.target.value);

  /** label?: string;
  idReglementMode?: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  name?: string;
  required?: boolean; */

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={`Modèle écriture ${tiersCompteComptable.numero} / ${tiersCompteComptable.libelle}`}
      withBtnsActions
      closeIcon
      headerWithBgColor
      fullScreen={true}
      onClickConfirm={() => {
        onSaveModeleCompte({
          ...value,
          idCompteFournisseur: tiersCompteComptable?.id,
        });
      }}
      disabledButton={saving}
    >
      {loadingModeleEcriture.loading ? (
        <SmallLoading />
      ) : (
        <div className={classes.root}>
          <Box>
            <CustomFormTextField
              label="Titre du modèle"
              name="titre"
              placeholder="Titre du modèle"
              required
              type="text"
              value={value.titre}
              onChange={handleChangeEvent}
              className={classes.formulaire}
            />
          </Box>
          <Box>
            <ModeReglementInput
              name="idModeReglement"
              idReglementMode={value.idModeReglement}
              onChange={handleChangeEvent}
            />
          </Box>
          <ModelEctitureTable tiersCompteComptable={tiersCompteComptable} value={value} setValue={setValue} />


        </div>
      )}
    </CustomModal>
  );
};


export default CompteComptableTiersModelsForm;
