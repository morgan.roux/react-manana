import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      width: 'auto',
    },
    searchTopBar: {
      marginLeft: 0,
    },
    titleTobarProps: {
      fontSize: '18px !important',
      fontFamily: 'Roboto !important',
      fontWeight: 600,
    },
    pagination:{
      position: 'sticky',
      bottom: 0,
      background: '#fff',
    },
    tableContainerProps: {
      height: 'calc(100vh - 360px)',
    },
  })
);
