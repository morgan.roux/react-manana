import { MouseEvent } from 'react';
import { EfCompteComptableInfoFragment } from '@lib/common/src/federation';

export interface MenuActionProps {
  anchorEl: Element | ((element: Element) => Element) | null | undefined;
  open: boolean;
  handleClose: () => void;
  handleEdit: (compteComptable: EfCompteComptableInfoFragment) => void;
  handleShowMenu: (compteComptable: EfCompteComptableInfoFragment, event: MouseEvent<HTMLElement>) => void;
  compteComptable?: EfCompteComptableInfoFragment;
  compteComptableToEdit?: EfCompteComptableInfoFragment;
  setOpenDeleteDialog: (boolean: true) => void;
  handleShowModelEcriture :  (compteComptable: EfCompteComptableInfoFragment) => void;
  compteComptableToViewModel?: EfCompteComptableInfoFragment;
  handleShowCreateTiers :  (compteComptable: EfCompteComptableInfoFragment,tiersType: 'PRESTATAIRE_SERVICE' | 'LABORATOIRE' ) => void;
}

export interface DefaultValueCompteComptableProps {
  id?: string;
  numero: string;
  libelle: string;
  actif?: boolean;
  sens: string;
  suivi: string;
  lettrage?: boolean;
  exclureLettrage?: boolean;
  idTva?: string;
  type?: string;
  origine?: any;
  origineAssocie?: any;
}

export interface CompteComptableFormProps {
  mode: 'creation' | 'modification';
  open: boolean;
  setOpen: (open: boolean) => void;
  compteComptable?: EfCompteComptableInfoFragment;
  action: { onRequestSave: (compte: DefaultValueCompteComptableProps) => void; saving: boolean };
}
