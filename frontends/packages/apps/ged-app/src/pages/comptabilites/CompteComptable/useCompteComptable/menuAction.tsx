import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Add, Delete, Edit, MoreHoriz, Visibility } from '@material-ui/icons';
import React from 'react';
import { MenuActionProps } from '../interface';

export const menuAction = ({
  anchorEl,
  open,
  handleClose,
  handleEdit,
  handleShowMenu,
  compteComptable,
  setOpenDeleteDialog,
  compteComptableToEdit,
  handleShowModelEcriture,
  handleShowCreateTiers
}: MenuActionProps) => {


  const isTiersOrFournisseur = compteComptable?.numero.startsWith('401') || compteComptable?.numero.startsWith('411')

  return (
    <div key={`row-${compteComptable?.id}`}>
      <IconButton
        aria-controls="simple-menu"
        aria-haspopup="true"
        // eslint-disable-next-line react/jsx-no-bind
        onClick={handleShowMenu.bind(null, compteComptable as any)}
      >
        <MoreHoriz />
      </IconButton>
      <Menu
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open && compteComptable?.id === compteComptableToEdit?.id}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {isTiersOrFournisseur && (
          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              handleClose();
              handleShowModelEcriture(compteComptable as any);
            }}
          >
            <ListItemIcon>
              <Visibility />
            </ListItemIcon>
            <Typography variant="inherit">Voir modèle</Typography>
          </MenuItem>
        )}

        {isTiersOrFournisseur && !compteComptable?.idOrigineAssocie && (
        <>
        <MenuItem
          onClick={(event) => {
            event.stopPropagation();
            handleClose();
            handleShowCreateTiers(compteComptable as any,'PRESTATAIRE_SERVICE');
          }}
        >
          <ListItemIcon>
            <Add />
          </ListItemIcon>
          <Typography variant="inherit">Créer en tant que Prestataire de service</Typography>
        </MenuItem>
        <MenuItem
          onClick={(event) => {
            event.stopPropagation();
            handleClose();
            handleShowCreateTiers(compteComptable as any,'LABORATOIRE');
          }}
        >
          <ListItemIcon>
            <Add />
          </ListItemIcon>
          <Typography variant="inherit">Créer en tant que Laboratoire</Typography>
        </MenuItem>
        </>
        )}


        <MenuItem
          onClick={(event) => {
            event.stopPropagation();
            handleClose();
            handleEdit(compteComptable as any);
          }}
        >
          <ListItemIcon>
            <Edit />
          </ListItemIcon>
          <Typography variant="inherit">Modifier</Typography>
        </MenuItem>

        <MenuItem
          onClick={(event) => {
            event.stopPropagation();
            handleClose();
            setOpenDeleteDialog(true);
          }}
        >
          <ListItemIcon>
            <Delete />
          </ListItemIcon>
          <Typography variant="inherit">Supprimer</Typography>
        </MenuItem>
      </Menu>
    </div>
  );
};
