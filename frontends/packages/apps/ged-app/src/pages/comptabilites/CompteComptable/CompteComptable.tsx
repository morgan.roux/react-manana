import { ConfirmDeleteDialog, Table, TableChange, TopBar } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC, useState, MouseEvent, useEffect } from 'react';
import CompteComptableForm from './form/CompteComptableForm';
import { useColumnCompteComptable } from './useCompteComptable/useColumnCompteComptable';
import { useStyles } from './style';
import { useCompteComptable } from './hooks/useCompteComptable';
import {
  EfCompteComptableInfoFragment,
  useGet_OriginesQuery,
} from '@lib/common/src/federation';
import CompteComptableTiersModels from './form/CompteComptableTiersModelsForm';
import { PartenaireForm } from '@lib/common';

const CompteComptable: FC<{}> = () => {
  const styles = useStyles();

  const [mode, setMode] = useState<'creation' | 'modification'>('creation');
  const [openForm, setOpenForm] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [openModelEcriture, setOpenModelEcriture] = useState<boolean>(false);
  const [openModelCreateTiers, setOpenModelCreateTiers] = useState<boolean>(false);
  const [createTiersType, setCreateTiersType] = useState<'PRESTATAIRE_SERVICE' | 'LABORATOIRE'>();

  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(10);
  const [searchText, setSearchText] = useState<string>('');

  const [compteComptableSelected, setCompteComptableSelected] = useState<EfCompteComptableInfoFragment>();
  const loadingOrigines = useGet_OriginesQuery();

  const open = Boolean(anchorEl);

  const {
    saving,
    handleSearch: onRequestSearch,
    handleDelete: onRequestDelete,
    loadingCompteComptable,
    saved,
    setSaved,
    handleSave: onRequestSave,
    handleSaveModeleCompte: onSaveModeleCompte,
  } = useCompteComptable();

  useEffect(() => {
    onRequestSearch({
      skip,
      take,
      searchText,
    });
  }, [skip, take, searchText]);

  useEffect(() => {
    if (saved) {
      setCompteComptableSelected(undefined);
      setOpenForm(false);
      setSaved(false);
    }
  }, [saved]);

  const toggleOpenEditDialog = () => {
    setOpenForm((prev) => !prev);
  };

  const handleOpenAdd = () => {
    toggleOpenEditDialog();
    setMode('creation');
    setCompteComptableSelected(undefined);
  };

  const handleShowMenu = (compteComptable: any, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
    setCompteComptableSelected(compteComptable);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const handleEdit = (compteComptable: any) => {
    toggleOpenEditDialog();
    setMode('modification');
    setAnchorEl(null);
  };

  const handleSearch = ({ skip, take, searchText: search }: TableChange) => {
    setSkip(skip);
    setTake(take);
    setSearchText(search);
  };

  const handleSortTable = (column: string, direction: 'DESC' | 'ASC') => {
    // history.push()
  };

  const handleConfirmDelete = () => {
    onRequestDelete(compteComptableSelected?.id);
    setOpenDeleteDialog(false);
    setAnchorEl(null);
  };

  const handleShowModelEcriture = (selected: EfCompteComptableInfoFragment) => {
    setCompteComptableSelected(selected);
    setOpenModelEcriture(true);
    setAnchorEl(null);
  };

  const handleShowCreateTiers = (
    selected: EfCompteComptableInfoFragment,
    tiersType: 'PRESTATAIRE_SERVICE' | 'LABORATOIRE'
  ) => {
    setCompteComptableSelected(selected);
    setOpenModelCreateTiers(true);
    setCreateTiersType(tiersType);
    setAnchorEl(null);
  };

  const columns = useColumnCompteComptable({
    anchorEl,
    open,
    handleClose,
    handleEdit,
    handleShowMenu,
    setOpenDeleteDialog,
    compteComptableToEdit: compteComptableSelected,
    handleShowModelEcriture: handleShowModelEcriture,
    handleShowCreateTiers: handleShowCreateTiers,
  });
  return (
    <Box>
      <Table
        loading={loadingCompteComptable.loading || false}
        error={loadingCompteComptable.error}
        columns={columns}
        data={loadingCompteComptable.data?.eFCompteComptables?.nodes || []}
        search={false}
        selectable={false}
        rowsTotal={loadingCompteComptable.data?.eFCompteComptables?.totalCount || 0}
        topBarComponent={(searchComponent) => (
          <TopBar
            handleOpenAdd={handleOpenAdd}
            searchComponent={searchComponent}
            titleButton="AJOUTER COMPTE COMPTABLE"
            titleTopBar="Liste des comptes comptables"
            withFilter={false}
            searchContainerClassName={styles.searchTopBar}
            style={'width: auto'}
            className={styles.content}
            titleTopBarClassName={styles.titleTobarProps}
          />
        )}
        onRunSearch={handleSearch}
        onSortColumn={handleSortTable}
        notablePagination={false}
        paginationPropsClassName={styles.pagination}
        tableContainerProps={styles.tableContainerProps}
      />
      <CompteComptableForm
        mode={mode}
        open={openForm}
        setOpen={setOpenForm}
        action={{ onRequestSave, saving }}
        compteComptable={compteComptableSelected}
      />
      <ConfirmDeleteDialog
        title="Suppression du compte comptable"
        content="Voulez-vous supprimez ce compte comptable"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        confirmBtnLabel="Supprimer"
        onClickConfirm={handleConfirmDelete}
      />
      {compteComptableSelected && openModelEcriture && (
        <CompteComptableTiersModels
          open={openModelEcriture}
          setOpen={setOpenModelEcriture}
          tiersCompteComptable={compteComptableSelected}
          action={{
            saving, onSaveModeleCompte: (values) => {
              onSaveModeleCompte(values)
              setOpenModelEcriture(false)
            }
          }}
        />
      )}

      {compteComptableSelected && createTiersType && !loadingOrigines.loading && (
        <PartenaireForm
          mode="creation"
          partenaireType={createTiersType}
          open={openModelCreateTiers}
          setOpen={setOpenModelCreateTiers}
          defaultValues={{
            nom: compteComptableSelected.libelle,
          }}
          onCreated={(created) => {
            const origine = loadingOrigines.data?.origines.nodes.find(({ code }) => code === createTiersType);
            onRequestSave({
              ...compteComptableSelected,
              type: 'FOURNISSEUR',
              actif: compteComptableSelected.isActif || false,
              idTva: compteComptableSelected.tva?.id || '',
              suivi: compteComptableSelected.suiviCompte,
              lettrage: compteComptableSelected.isLettrage || false,
              exclureLettrage: compteComptableSelected.isExclureLettrage || false,
              origine: origine,
              origineAssocie: created,
            });
          }}
        />
      )}
    </Box>
  );
};

export default CompteComptable;
