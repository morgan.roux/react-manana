import { useApplicationContext, TvaInput } from '@lib/common';
import { CustomModal, CustomFormTextField } from '@lib/ui-kit';
import CustomCheckbox from '@lib/ui-kit/src/components/atoms/CustomCheckBox/CustomCheckbox';
import { Box, FormControl } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { ChangeEvent } from 'react';
import { SensInput, SuiviCompteInput,  TypeInput } from '../../../../components/comptabilite';
import { CompteComptableFormProps, DefaultValueCompteComptableProps } from '../interface';

const defaultValue: DefaultValueCompteComptableProps = {
  numero: '',
  libelle: '',
  actif: false,
  sens: 'DEBIT',
  lettrage: false,
  exclureLettrage: false,
  idTva: '',
  type: '',
  suivi: 'AUCUN',
};

const CompteComptableForm: FC<CompteComptableFormProps> = ({ mode, open, setOpen, compteComptable, action }) => {
  const { isMobile } = useApplicationContext();
  const [value, setValue] = useState<DefaultValueCompteComptableProps>(defaultValue);

  const { onRequestSave, saving } = action;

  useEffect(() => {
    if (open && mode === 'creation') {
      setValue(defaultValue);
    }
  }, [open, mode]);

  useEffect(() => {
    if (compteComptable && mode === 'modification') {
      setValue({
        ...compteComptable,
        type: compteComptable.type || undefined,
        actif: compteComptable.isActif || false,
        idTva: compteComptable.tva?.id || '',
        suivi: compteComptable.suiviCompte,
        lettrage: compteComptable.isLettrage || false,
        exclureLettrage: compteComptable.isExclureLettrage || false,
        origine: compteComptable.idOrigine,
        origineAssocie: compteComptable.idOrigineAssocie,
      });
    }
  }, [compteComptable, mode]);

  console.log('type', compteComptable);

  const handleChangeCompte = (event: ChangeEvent<HTMLInputElement>) => {
    setValue({ ...value, [event.target.name]: event.target.value });
  };

  const handleChange = (name: string, value?: any) => {
    setValue((prev) => ({ ...prev, [name]: value, type: 'FOURNISSEUR' }));
    if (name === 'type' && value && value !== 'AUTRE') setValue((prev) => ({ ...prev, idTva: '' }));
  };

  const handleChangeCheckedCompte = (event: ChangeEvent<HTMLInputElement>) => {
    setValue({ ...value, [event.target.name]: event.target.checked });
  };

  const isValid = () => {
    return (
      value.numero &&
      value.libelle &&
      value.sens &&
      value.suivi 
     // &&(value.idTva || value.type)
      // &&(value.type !== 'FOURNISSEUR' || (value.origine && value.origineAssocie && value.origineAssocie.length !== 0))
    );
  };

  const handleSave = () => {
    onRequestSave(value);
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={mode === 'modification' ? 'Modifier le compte comptable' : 'Nouveau compte comptable'}
      withBtnsActions
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      fullWidth
      fullScreen={!!isMobile}
      disabledButton={!isValid() || saving}
      onClickConfirm={handleSave}
    >
      <Box display="flex" flexDirection="column" width="100%">
        <CustomFormTextField
          label="Numéro"
          name="numero"
          placeholder="Saisir le numéro"
          required
          type="text"
          value={value.numero}
          onChange={handleChangeCompte}
        />
        <CustomFormTextField
          label="Libellé"
          name="libelle"
          placeholder="Sasir le libéllé"
          required
          type="text"
          value={value.libelle}
          onChange={handleChangeCompte}
        />
        <FormControl>
          <CustomCheckbox
            name="actif"
            checked={value.actif}
            onClick={(e: any) => e.stopPropagation()}
            onChange={handleChangeCheckedCompte}
            label="Actif"
          />
        </FormControl>
        <SensInput onChange={handleChangeCompte} sens={value.sens} />
        <SuiviCompteInput onChange={handleChangeCompte} suivi={value.suivi} />
        <FormControl>
          <CustomCheckbox
            name="lettrage"
            checked={value.lettrage}
            onClick={(e: any) => e.stopPropagation()}
            onChange={handleChangeCheckedCompte}
            label="Lettrage"
          />
        </FormControl>
        <FormControl>
          <CustomCheckbox
            name="exclureLettrage"
            checked={value.exclureLettrage}
            onClick={(e: any) => e.stopPropagation()}
            onChange={handleChangeCheckedCompte}
            label="Exclure lettrage automatique"
          />
        </FormControl>
        <TvaInput
          onChange={handleChangeCompte}
          idTva={value.idTva}
        />
        <TypeInput onChange={handleChange} value={value} />
      </Box>
    </CustomModal>
  );
};

export default CompteComptableForm;
