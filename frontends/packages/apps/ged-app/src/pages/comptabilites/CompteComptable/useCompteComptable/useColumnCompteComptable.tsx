import { Column } from '@lib/ui-kit';
import { EfCompteComptableInfoFragment } from '@lib/common/src/federation';
import { MenuActionProps } from '../interface';
import { menuAction } from './menuAction';
import React from 'react';
import { Box } from '@material-ui/core';

export const useColumnCompteComptable = ({
  anchorEl,
  open,
  handleClose,
  handleEdit,
  handleShowMenu,
  setOpenDeleteDialog,
  compteComptableToEdit,
  handleShowModelEcriture,
  compteComptableToViewModel,
  handleShowCreateTiers
}: MenuActionProps) => {
  const columns: Column[] = [
    {
      name: 'numero',
      label: 'Numero',
      // sortable: true,
      renderer: (row: EfCompteComptableInfoFragment) => {
        return <Box>{row.numero}</Box>;
      },
    },
    {
      name: 'libelle',
      label: 'Libellé',
      // sortable: true,
      renderer: (row: EfCompteComptableInfoFragment) => {
        return <Box>{row.libelle}</Box>;
      },
      scope: 'row',
      component: 'th',
    },
    {
      name: 'type',
      label: 'Type',
      // sortable: true,
      renderer: (row: EfCompteComptableInfoFragment) => {
        if (row.idOrigineAssocie) {
          return 'FOURNISSEUR'
        }
        if (row.tva?.id) {
          return 'TVA'
        }

        return <Box>{row.type}</Box>;
      },
    },
    {
      name: 'isLettrable',
      label: 'Lettrable',
      renderer: (row: EfCompteComptableInfoFragment) => {
        return <Box>{row.isLettrage ? 'Oui' : 'Non'}</Box>;
      },
    },
    {
      name: 'suiviCompte',
      label: 'Bancaire',
      // sortable: true,
      renderer: (row: EfCompteComptableInfoFragment) => {
        return <Box>{row.suiviCompte === 'BANQUE' ? 'Oui' : 'Non'}</Box>;
      },
    },
    {
      name: 'isActif',
      label: 'Actif',
      // sortable: true,
      renderer: (row: EfCompteComptableInfoFragment) => {
        return <Box>{row.isActif ? 'Oui' : 'Non'}</Box>;
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: EfCompteComptableInfoFragment) => {
        return menuAction({
          anchorEl,
          open,
          handleClose,
          handleEdit,
          handleShowMenu,
          compteComptable: row,
          setOpenDeleteDialog,
          compteComptableToEdit,
          handleShowModelEcriture,
          compteComptableToViewModel,
          handleShowCreateTiers
        });
      },
    },
  ];
  return columns;
};
