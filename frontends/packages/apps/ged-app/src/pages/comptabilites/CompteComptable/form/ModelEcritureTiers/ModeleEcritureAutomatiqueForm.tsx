import { CustomFormTextField } from '@lib/ui-kit';
import { Box, Radio, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import CloseIcon from '@material-ui/icons/Close';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './styles';
import { EfCompteComptableInfoFragment, EfTvaFilter, useEfTvaQuery } from '@lib/common/src/federation';
import { CompteComptableInput, useApplicationContext, TvaInput } from '@lib/common';
import { SensInput } from '../../../../../components/comptabilite';
import { TypeChargeInput } from '../../../../../components/comptabilite/TypeCharge';
import moment from 'moment';

export interface ModeleCompteLigne {
  id?: string;
  isContrepartie?: boolean;
  sens?: string;
  libelle: string;
  idEfTva: string;
  compteComptable?: EfCompteComptableInfoFragment;
  compteComptableTva?: EfCompteComptableInfoFragment;
  categorie: string;
}
export interface ModeleCompteSave {
  id?: string;
  titre: string;
  idModeReglement: string;
  idCompteFournisseur?: string;
  lignes: ModeleCompteLigne[];
}

interface ModeleEcritureAutomatiqueFormProps {
  tiersCompteComptable: EfCompteComptableInfoFragment;
  value: ModeleCompteSave;
  setValue: React.Dispatch<React.SetStateAction<ModeleCompteSave>>;
}

const initialLigne: ModeleCompteLigne = {
  isContrepartie: false,
  sens: 'DEBIT',
  compteComptable: undefined,
  libelle: '',
  idEfTva: '',
  categorie: 'ACHAT',
};

const ModeleEcritureAutomatiqueForm: FC<ModeleEcritureAutomatiqueFormProps> = ({
  tiersCompteComptable,
  value,
  setValue,
}) => {
  const classes = useStyles();

  const { currentPharmacie } = useApplicationContext();

  const filterAnd: EfTvaFilter[] = [
    {
      idPharmacie: {
        eq: currentPharmacie.id,
      },
      dateDebutExercice: {
        between: {
          lower: moment().startOf('year').toDate(),
          upper: moment().endOf('year').toDate(),
        },
      },
    },
  ];
  const loadTva = useEfTvaQuery({
    fetchPolicy: 'cache-and-network',
    variables: {
      filter: {
        and: filterAnd,
      },
      paging: {
        offset: 0,
        limit: 25,
      },
    },
  });

  console.log('modele', value);

  const handleChangeLigne = (index: number, name: string, value?: any) => {
    setValue((prev) => ({
      ...prev,
      lignes: prev.lignes?.map((ligne, idx) => {
        if (index === idx) {
          return { ...ligne, [name]: value };
        }
        return ligne;
      }),
    }));
  };

  const handleChangeCompte = (index: number, compte?: EfCompteComptableInfoFragment) => {

    handleChangeLigne(index, 'compteComptable', compte);
    handleChangeLigne(index, 'sens', compte?.sens || 'DEBIT');
    handleChangeLigne(index, 'libelle', compte?.libelle || '')
    handleChangeLigne(index, 'idEfTva', compte?.tva?.id)


    const tva = (loadTva.data?.eFTvas.nodes || []).find(({ id }) => id === compte?.tva?.id);
  };

  const handleAddRow = () => {
    setValue((prev) => ({ ...prev, lignes: [...(prev.lignes || []), initialLigne] }));
  };
  const handleRemoveRow = (idx: any) => {
    setValue((prev) => ({ ...prev, lignes: prev.lignes?.filter((_, index) => index !== idx) }));
  };

  return (
    <Box style={{ marginTop: '16px' }}>
      <Table className="table table-bordered table-hover" id="tab_logic">
        <TableHead>
          <TableRow>
            <TableCell className={classes.tableCellTitle}> Contrepartie </TableCell>
            <TableCell className={classes.tableCellTitle} style={{ minWidth: 100 }} align="center">
              Sens
            </TableCell>
            <TableCell className={classes.tableCellTitle} align="center" style={{ minWidth: 150 }} >
              Compte de charge
            </TableCell>
            <TableCell className={classes.tableCellTitle} style={{ minWidth: 150 }} align="center">
              Type de charge
            </TableCell>
            <TableCell className={classes.tableCellTitle} align="center">
              Libellé
            </TableCell>
            <TableCell className={classes.tableCellTitle} align="center" style={{ minWidth: 130 }} >
              Taux TVA
            </TableCell>
            <TableCell className={classes.tableCellTitle} align="center">
              N°Compte TVA
            </TableCell>
            <TableCell className={classes.tableCellTitle} />
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <TableCell className={classes.tableCell} align="center">
              <Radio checked={true} inputProps={{ 'aria-label': 'A' }} readOnly />
            </TableCell>
            <TableCell className={classes.tableCell} align="center">
              <Box className={classes.sensContainer}>
                <SensInput sens="CREDIT" label="" required={false} onChange={() => { }} disabled />
              </Box>
            </TableCell>
            <TableCell className={classes.tableCell} align="center">
              <Box pl={2} className={classes.compteContainer}>
                <CompteComptableInput value={tiersCompteComptable} onChange={() => { }} type="TIERS" disabled />
              </Box>
            </TableCell>
            <TableCell className={classes.tableCell} align="center" />
            <TableCell className={classes.tableCell} align="center">
              <Box pl={2}>
                <CustomFormTextField type="text" value={tiersCompteComptable.libelle} fullWidth disabled />
              </Box>
            </TableCell>
            <TableCell className={classes.tableCell} align="center" />
            <TableCell className={classes.tableCell} align="center" />
            <TableCell className={classes.tableCell} align="center" />
          </TableRow>

          {value.lignes &&
            value.lignes.map((modeleLigne, idx) => (
              <TableRow id="addr0" key={idx}>
                <TableCell className={classes.tableCell} align="center">
                  <Radio checked={false} readOnly onClick={() => { }} inputProps={{ 'aria-label': 'A' }} />
                </TableCell>
                <TableCell className={classes.tableCell} align="center">
                  <Box className={classes.sensContainer}>
                    <SensInput
                      onChange={(e: any) => handleChangeLigne(idx, 'sens', e.target.value)}
                      sens={modeleLigne.sens}
                      label=""
                      required={false}
                      disabled
                    />
                  </Box>
                </TableCell>
                <TableCell className={classes.tableCell} align="center">
                  <Box pl={2} className={classes.compteContainer}>
                    <CompteComptableInput
                      value={modeleLigne.compteComptable}
                      onChange={(compte) => {
                        handleChangeCompte(idx, compte)
                      }}
                      type="CLASS_6"
                    // idComptes={idComptes}
                    />
                  </Box>
                </TableCell>
                <TableCell className={classes.tableCell} align="center">
                  <Box pl={2} className={classes.compteContainer}>
                    <TypeChargeInput
                      onChange={(e: any) => handleChangeLigne(idx, 'categorie', e.target.value)}
                      sens={modeleLigne.categorie}
                      label=""
                      required={false}
                    />
                  </Box>
                </TableCell>

                <TableCell className={classes.tableCell} align="center">
                  <Box pl={2}>
                    <CustomFormTextField
                      type="text"
                      onChange={(e) => handleChangeLigne(idx, 'libelle', e.target.value)}
                      value={modeleLigne.libelle}
                      fullWidth
                    />
                  </Box>
                </TableCell>
                <TableCell className={classes.tableCell} align="center">
                  <Box pl={2}>
                    <TvaInput
                      onChange={(event) => {
                        handleChangeLigne(idx, 'idEfTva', event.target.value)
                      }}
                      idTva={modeleLigne.idEfTva}
                    />
                  </Box>
                </TableCell>
                <TableCell className={classes.tableCell} align="center">
                  <Box pl={2}>
                    <CompteComptableInput
                      value={modeleLigne.compteComptableTva}
                      onChange={(compte) => {
                        handleChangeLigne(idx, 'compteComptableTva', compte)
                      }}
                      type="TVA"
                    // idComptes={idComptes}
                    />
                  </Box>
                </TableCell>

                <TableCell className={classes.tableCell} align="center">
                  {idx !== 0 && (
                    <Box
                      className={classes.actionDelete}
                      onClick={() => {
                        handleRemoveRow(idx);
                      }}
                    >
                      <Box>
                        <CloseIcon />
                      </Box>
                      <Typography>Supprimer</Typography>
                    </Box>
                  )}
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
      <Box onClick={handleAddRow} className={classes.actionAdd}>
        <Box>
          <AddIcon />
        </Box>
        <Typography>Ajouter une ligne</Typography>
      </Box>
    </Box>
  );
};

export default ModeleEcritureAutomatiqueForm;
