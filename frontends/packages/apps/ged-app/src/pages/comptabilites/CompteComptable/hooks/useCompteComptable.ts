import { useState } from 'react';
import {
  EfCompteComptableFilter,
  SortDirection,
  useCreateEfCompteComptableMutation,
  useCreateEfModeleCompteComptableMutation,
  useCreateEfModeleEcritureMutation,
  useDeleteEfCompteComptableMutation,
  useEfCompteComptablesLazyQuery,
  useUpdateEfCompteComptableMutation,
  useUpdateEfModeleCompteComptableMutation,
  useUpdateEfModeleEcritureMutation,
} from '@lib/common/src/federation';
import { DefaultValueCompteComptableProps } from '../interface';
import { useApplicationContext } from '@lib/common';
import { TableChange } from '@lib/ui-kit';
import { ModeleCompteSave } from '../form/ModelEcritureTiers/ModeleEcritureAutomatiqueForm';

export const useCompteComptable = () => {
  const { currentPharmacie, notify } = useApplicationContext();

  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [mode, setMode] = useState<'creation' | 'modification'>('creation');
  const [openForm, setOpenForm] = useState<boolean>(false);

  const [loadCompteComptable, loadingCompteComptable] = useEfCompteComptablesLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [createCompteComptable] = useCreateEfCompteComptableMutation({
    onCompleted: () => {
      setSaved(true);
      setSaving(false);
      loadingCompteComptable.refetch && loadingCompteComptable.refetch();
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `erreur: ${err.message}`,
      });
      setSaving(false);
    },
  });

  const [updateCompteComptable] = useUpdateEfCompteComptableMutation({
    onCompleted: () => {
      setSaved(true);
      setSaving(false);
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `erreur: ${err.message}`,
      });
      setSaving(false);
    },
  });

  const [deleteCompteComptable] = useDeleteEfCompteComptableMutation({
    onCompleted: () => {
      loadingCompteComptable.refetch && loadingCompteComptable.refetch();
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `erreur: ${err.message}`,
      });
    },
  });

  const [createModeleCompte] = useCreateEfModeleEcritureMutation({
    onCompleted: () => {
      setSaved(true);
      setSaving(false);
      loadingCompteComptable.refetch && loadingCompteComptable.refetch();

      notify({
        type:'success',
        message: 'Le modèle a été bien enregistré'
      })
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `Erreur lors de la création de modèle`,
      });
      setSaving(false);
    },
  });

  const [updateModeleCompte] = useUpdateEfModeleEcritureMutation({
    onCompleted: () => {
      setSaved(true);
      setSaving(false);



      notify({
        type:'success',
        message: 'Le modèle a été bien enregistré'
      })
    },
    onError: (err) => {
      notify({
        type: 'error',
        message: `Erreur lors de la création de modèle`,
      });
      setSaving(false);
    },
  });

  const handleSearch = ({ skip, take, searchText, sortDirection, sortBy }: TableChange) => {
    const filterAnd: EfCompteComptableFilter[] = [
      {
        idPharmacie: {
          eq: currentPharmacie.id,
        },
      },
    ];
    if (searchText)
      filterAnd.push({
        or: [
          {
            numero: {
              iLike: `%${searchText}%`,
            },
          },
          {
            libelle: {
              iLike: `%${searchText}%`,
            },
          },
          {
            sens: {
              iLike: `%${searchText}%`,
            },
          },
          {
            type: {
              iLike: `%${searchText}%`,
            },
          },
          {
            suiviCompte: {
              iLike: `%${searchText}%`,
            },
          },
        ],
      });

    const sorting = sortDirection && sortBy ? [{ field: sortBy, direction: sortDirection }] : undefined;

    const sortTableFilter = sorting || [
      {
        field: 'numero',
        direction: SortDirection.Asc,
      },
    ];
    loadCompteComptable({
      variables: {
        filter: {
          and: filterAnd,
        },
        paging: {
          offset: skip,
          limit: take,
        },
        sorting: sortTableFilter,
      },
    });
  };

  const handleSave = ({
    id,
    numero,
    libelle,
    actif,
    sens,
    suivi,
    lettrage,
    exclureLettrage,
    idTva,
    type,
    origine,
    origineAssocie,
  }: DefaultValueCompteComptableProps) => {
    setSaving(true);
    setSaved(false);

    const idOrigine = typeof origine==='string'? origine:  origine?.id
    const idOrigineAssocie = typeof origineAssocie==='string'? origineAssocie: origineAssocie?.id;

    if (id) {


      updateCompteComptable({
        variables: {
          input: {
            id,
            update: {
              numero,
              libelle,
              isActif: actif,
              sens,
              suiviCompte: suivi,
              isLettrage: lettrage,
              isExclureLettrage: exclureLettrage,
              idEfTva: idOrigineAssocie? undefined : idTva,
              type,
              idOrigine: idOrigineAssocie ? idOrigine : null,
              idOrigineAssocie: idOrigineAssocie? idOrigineAssocie : null,
            },
          },
        },
      });
    } else {
      createCompteComptable({
        variables: {
          input: {
            eFCompteComptable: {
              numero,
              libelle,
              isActif: actif,
              sens,
              suiviCompte: suivi,
              isLettrage: lettrage,
              isExclureLettrage: exclureLettrage,
              idEfTva: origineAssocie?.id ? undefined : idTva,
              type,
              idOrigine,
              idOrigineAssocie,
            },
          },
        },
      });
    }
  };

  const handleDelete = (id?: string) => {
    if (id) {
      deleteCompteComptable({
        variables: {
          input: {
            id,
          },
        },
      });
    }
  };

  const handleSaveModeleCompte = ({
    id,
    titre,
    idModeReglement,
    lignes,
    idCompteFournisseur,
  }: ModeleCompteSave) => {
    setSaving(true);
    setSaved(false);

    if (id) {
      updateModeleCompte({
        variables: {
          id,
          input: {
            titre,
            idModeReglement,
            idCompteFournisseur,
            lignes: [
              ...((lignes || [])
                .filter(({ compteComptable }) => compteComptable?.id)
                .map(({ libelle,idEfTva, compteComptable, compteComptableTva, categorie }) => ({
                  libelle,
                  idEfTva,
                  idCompteComptableTva: compteComptableTva?.id,
                  categorie,
                  idCompteComptable: compteComptable?.id || ''
                }))),
            ],
          },
        },
      });
    } else {
      createModeleCompte({
        variables: {
          input: {
            titre,
            idModeReglement,
            idCompteFournisseur,
            lignes: [
              ...((lignes || [])
                .filter(({ compteComptable }) => compteComptable?.id)
                .map(({ libelle,idEfTva, compteComptable, compteComptableTva, categorie }) => ({
                  libelle,idEfTva,
                  idCompteComptableTva:compteComptableTva?.id,
                  categorie,
                  idCompteComptable: compteComptable?.id || ''
                }))),
            ],
          },
        },
      });
    }
  };

  return {
    saving,
    setSaving,
    saved,
    setSaved,
    mode,
    setMode,
    openForm,
    setOpenForm,
    handleSave,
    handleSearch,
    handleDelete,
    loadingCompteComptable,
    handleSaveModeleCompte,
  };
};
