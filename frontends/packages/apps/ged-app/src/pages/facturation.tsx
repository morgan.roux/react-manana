import React, { FC } from 'react';

import DocumentPage from './GestionEspaceFacturation/GestionEspaceFacturation';

const EspaceFacturation: FC<{}> = () => {
  return <DocumentPage type="FACTURE" />;
};

export default EspaceFacturation;
