import React, { useRef, useState } from 'react';
import { Column, CustomAvatar, CustomDatePicker, CustomFormTextField, OptionSelect } from '@lib/ui-kit';
import { Box, Button, IconButton, Popover, Typography } from '@material-ui/core';
import { useStyles } from './styles';
import { Cancel, Delete, Edit, MoreHoriz, Visibility } from '@material-ui/icons';
import moment from 'moment';
import { FullDocumentCategorieFragment } from '@lib/common/src/federation';
import { FactureStatut } from '../../components/facture/FactureStatut';
import { FactureExport, MenuExport } from '../../components/facture/menu-export';
import { formatMoney } from '@lib/common';

interface UseVueListeColumnProps {
  handleMenuClick: (event: React.MouseEvent<HTMLElement>, facture: FullDocumentCategorieFragment) => void;
  handleCloseMenu: () => void;
  handleDetail: () => void;
  handleModifier: () => void;
  handleSupprimer: () => void;
  handleExportPdf: () => void;
  loading: boolean;
  factureToExport: FactureExport[];
  anchorEl: HTMLElement | null;
  type: 'FACTURE' | 'AUTRES';
  activeItem?: FullDocumentCategorieFragment;
}

export const useVueListeColumn = ({
  handleMenuClick,
  handleDetail,
  handleModifier,
  handleSupprimer,
  handleExportPdf,
  loading,
  factureToExport,
  handleCloseMenu,
  anchorEl,
  type,
  activeItem,
}: UseVueListeColumnProps): Column[] => {
  const classes = useStyles();

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const [facture, setFacture] = useState<string>('20210106');
  const [commande, setCommande] = useState<string>('20210106');
  const [totalTtc, setTotalTtc] = useState<string>('3 220,25 €');
  const [totalHt, setTotalHt] = useState<string>('3 000,00€');
  const [tva, setTva] = useState<string>('220,25 €');
  const [selectedDateFacture, setSelectedDateFacture] = useState<Date | null>(new Date());
  const [selectedDateReglement, setSelectedDateReglement] = useState<Date | null>(new Date());
  const [showInputFacture, setShowInputFacture] = useState<boolean>(false);
  const [showInputCommande, setShowInputCommande] = useState<boolean>(false);
  const [showInputTtc, setShowInputTtc] = useState<boolean>();
  const [showInputHt, setShowInputHt] = useState<boolean>();
  const [showInputTva, setShowInputTva] = useState<boolean>();
  const [showDateFacture, setShowDateFacture] = useState<boolean>();
  const [showDateReglement, setShowDateReglement] = useState<boolean>();

  const handleDateFactureChange = (event: React.MouseEvent<{}>) => {
    // setShowDateFacture(true);
  };

  const inputEl = useRef<HTMLInputElement>();

  const hideNumeroInput = (event: React.MouseEvent<{}>) => {
    setShowInputFacture(false);
  };
  const hideNumeroCommande = (event: React.MouseEvent<{}>) => {
    setShowInputCommande(false);
  };

  const hideDateFacture = (event: React.MouseEvent<{}>) => {
    setShowDateFacture(false);
  };

  const hideDatereglement = (event: React.MouseEvent<{}>) => {
    setShowDateReglement(false);
  };

  const hideTotalTtc = (event: React.MouseEvent<{}>) => {
    setShowInputTtc(false);
  };

  const hideTotalHt = (event: React.MouseEvent<{}>) => {
    setShowInputHt(false);
  };

  const hideTotalTva = (event: React.MouseEvent<{}>) => {
    setShowInputTva(false);
  };

  const showNumeroInputFacture = (event: React.MouseEvent<{}>) => {
    // setShowInputFacture(true);
  };

  const showNumeroInputCommande = (event: React.MouseEvent<{}>) => {
    // setShowInputCommande(true);
  };

  const handleTTCChange = (event: React.MouseEvent<{}>) => {
    // setShowInputTtc(true);
  };

  const handleHTChange = (event: React.MouseEvent<{}>) => {
    // setShowInputHt(true);
  };

  const handleTvaChange = (event: React.MouseEvent<{}>) => {
    // setShowInputTva(true);
  };

  const handleDateReglementChenge = (event: React.MouseEvent<{}>) => {
    // setShowDateReglement(true);
  };

  const handleDateFacture = (date: Date | null) => {
    setSelectedDateFacture(date);
  };

  const handleDateReglement = (date: Date | null) => {
    setSelectedDateReglement(date);
  };

  if (type === 'AUTRES') {
    return [
      {
        name: 'description',
        label: 'Description',
        sortable: true,
        renderer: (row: FullDocumentCategorieFragment) => {
          return row.description ? <Box dangerouslySetInnerHTML={{ __html: row.description }} /> : null;
        },
      },
      {
        name: 'origine',
        label: 'Origine',
        sortable: true,
        renderer: (row: any) => {
          return row.origineAssocie ? <Box>{row.origineAssocie.nom || row.origineAssocie.fullName}</Box> : null;
        },
      },
      {
        name: 'espace',
        label: 'Espace documentaire',
        renderer: (row: FullDocumentCategorieFragment) => {
          return <Box>{row.categorie?.libelle || '-'}</Box>;
        },
      },

      {
        name: 'sous-espace',
        label: 'Sous-espace documentaire',
        renderer: (row: FullDocumentCategorieFragment) => {
          return <Box>{row.sousCategorie?.libelle || '-'}</Box>;
        },
      },
    ];
  }

  return [
    {
      name: 'numero_facture',
      label: 'Numéro de facture',
      sortable: true,
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box>
            {showInputFacture ? (
              <Box style={{ display: 'flex' }}>
                <CustomFormTextField
                  type="text"
                  onChange={(event: any) => {
                    setFacture(event.target.value);
                    console.log('******Num facture******', event.target.value);
                  }}
                  value={facture}
                  className={classes.textDialogue}
                  inputRef={inputEl}
                />
                <IconButton onClick={hideNumeroInput}>
                  <Cancel />
                </IconButton>
              </Box>
            ) : (
              <Typography onClick={showNumeroInputFacture}>{row.numeroFacture}</Typography>
            )}
          </Box>
        );
      },
    },
    {
      name: 'numero_commande',
      label: 'Numéro de commande',
      sortable: true,
      style: {
        minWidth: 150,
        textAlign: 'right',
      },
      headerStyle: {
        textAlign: 'right',
      },
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box>
            {showInputCommande ? (
              <Box style={{ display: 'flex' }}>
                <CustomFormTextField
                  type="text"
                  onChange={(event: any) => {
                    setCommande(event.target.value);
                    console.log('******Num commande******', event.target.value);
                  }}
                  value={commande}
                  className={classes.textDialogue}
                />
                <IconButton onClick={hideNumeroCommande}>
                  <Cancel />
                </IconButton>
              </Box>
            ) : (
              <Typography onClick={showNumeroInputCommande}>{row.numeroCommande}</Typography>
            )}
          </Box>
        );
      },
    },
    {
      name: 'facture_date',
      label: 'Date de facture',
      sortable: true,
      style: {
        minWidth: 150,
        textAlign: 'right',
      },
      headerStyle: {
        textAlign: 'right',
      },
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box>
            {showDateFacture ? (
              <Box style={{ display: 'flex' }}>
                <CustomDatePicker
                  value={selectedDateFacture}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={handleDateFacture}
                  fullWidth
                  required
                  className={classes.datePicker}
                />
                <IconButton onClick={hideDateFacture}>
                  <Cancel />
                </IconButton>
              </Box>
            ) : (
              <Typography onClick={handleDateFactureChange}>
                {row.factureDate ? `${moment(row.factureDate).format('DD/MM/YYYY')}` : `-`}
              </Typography>
            )}
          </Box>
        );
      },
    },
    {
      name: 'facture_date_reglement',
      label: 'Date de règlement',
      sortable: true,
      style: {
        minWidth: 150,
        textAlign: 'right',
      },
      headerStyle: {
        textAlign: 'right',
      },
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box>
            {showDateReglement ? (
              <Box style={{ display: 'flex' }}>
                <CustomDatePicker
                  value={selectedDateReglement}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  onChange={handleDateReglement}
                  fullWidth
                  required
                  className={classes.datePicker}
                />
                <IconButton onClick={hideDatereglement}>
                  <Cancel />
                </IconButton>
              </Box>
            ) : (
              <Typography onClick={handleDateReglementChenge}>
                {row.factureDateReglement ? `${moment(row.factureDateReglement).format('DD/MM/YYYY')}` : `-`}
              </Typography>
            )}
          </Box>
        );
      },
    },
    {
      name: 'facture_total_ht',
      label: 'Total TTC',
      sortable: true,
      style: {
        minWidth: 150,
        textAlign: 'right',
      },
      headerStyle: {
        textAlign: 'right',
      },
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box>
            {showInputTtc ? (
              <Box style={{ display: 'flex' }}>
                <CustomFormTextField
                  type="string"
                  onChange={(event: any) => {
                    const nouveauTTC = event.target.value;
                    if (nouveauTTC) {
                      setTotalTtc(nouveauTTC);
                      console.log('*************TTC****************', nouveauTTC);
                    }
                  }}
                  value={totalTtc}
                  className={classes.textDialogue}
                  inputRef={inputEl}
                />
                <IconButton onClick={hideTotalTtc}>
                  <Cancel />
                </IconButton>
              </Box>
            ) : ( 
              <Typography onClick={handleTTCChange}>{`${formatMoney(row.factureTotalTtc || 0)} €`}</Typography>
            )}
          </Box>
        );
      },
    },
    {
      name: 'facture_tva',
      label: 'Total HT',
      sortable: true,
      style: {
        minWidth: 150,
        textAlign: 'right',
      },
      headerStyle: {
        textAlign: 'right',
      },
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box>
            {showInputHt ? (
              <Box style={{ display: 'flex' }}>
                <CustomFormTextField
                  type="string"
                  onChange={(event: any) => {
                    const nouveauHt = event.target.value;
                    if (nouveauHt) {
                      setTotalHt(nouveauHt);
                      console.log('*************HT****************', nouveauHt);
                    }
                  }}
                  value={totalHt}
                  className={classes.textDialogue}
                  inputRef={inputEl}
                />
                <IconButton onClick={hideTotalHt}>
                  <Cancel />
                </IconButton>
              </Box>
            ) : (
              <Typography onClick={handleHTChange}>{`${formatMoney(row.factureTotalHt || 0)} €`}</Typography>
            )}
          </Box>
        );
      },
    },
    {
      name: 'facture_total_ttc',
      label: 'Total TVA',
      sortable: true,
      style: {
        minWidth: 150,
        textAlign: 'right',
      },
      headerStyle: {
        textAlign: 'right',
      },
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box>
            {showInputTva ? (
              <Box style={{ display: 'flex' }}>
                <CustomFormTextField
                  type="string"
                  onChange={(event: any) => {
                    const nouveauTva = event.target.value;
                    if (nouveauTva) {
                      setTva(nouveauTva);
                      console.log('*************TVA****************', nouveauTva);
                    }
                  }}
                  value={tva}
                  className={classes.textDialogue}
                  inputRef={inputEl}
                />
                <IconButton onClick={hideTotalTva}>
                  <Cancel />
                </IconButton>
              </Box>
            ) : (
              <Typography onClick={handleTvaChange}>{`${formatMoney(row.factureTva || 0)} €`}</Typography>
            )}
          </Box>
        );
      },
    },
    {
      name: 'etat',
      label: 'Etat facturation',
      style: {
        minWidth: 150,
        textAlign: 'right',
      },
      headerStyle: {
        textAlign: 'right',
      },
      renderer: (row: FullDocumentCategorieFragment) => {
        return <FactureStatut facture={row} />;
      },
    },
    {
      name: 'valide',
      label: 'Validée/Rejetée par',
      style: {
        minWidth: 150,
        textAlign: 'right',
      },
      headerStyle: {
        textAlign: 'right',
      },
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box style={{ display: 'flex', alignItems: 'center' }}>
            {row.dernierChangementStatut && (
              <CustomAvatar url={row.dernierChangementStatut?.updatedBy?.photo?.publicUrl} />
            )}
            <Typography style={{ marginLeft: '8px' }}>{row.dernierChangementStatut?.updatedBy?.fullName}</Typography>
          </Box>
        );
      },
    },
    {
      name: '',
      label: '',
      sortable: false,
      style: {
        minWidth: 150,
        textAlign: 'right',
      },
      headerStyle: {
        textAlign: 'center',
      },
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box>
            <Button onClick={(e: React.MouseEvent<HTMLElement>) => handleMenuClick(e, row)}>
              <MoreHoriz />
            </Button>
            <Popover
              id={id}
              open={open && activeItem?.id === row.id}
              anchorEl={anchorEl}
              onClose={handleCloseMenu}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
            >
              <Box
                className={classes.popover}
                onClick={(e: React.MouseEvent<HTMLElement>) => {
                  e?.stopPropagation();
                  e?.preventDefault();
                }}
              >
                <Box className={classes.popoverItem} onClick={handleDetail}>
                  <Visibility />
                  <Typography className={classes.typography}>Voir détail</Typography>
                </Box>
                <Box className={classes.popoverItem}>
                  <MenuExport
                    facturesToExport={factureToExport}
                    handleExportPdf={handleExportPdf}
                    loading={loading}
                    handleCloseMenu={handleCloseMenu}
                  />
                </Box>
                {/* <Box className={classes.popoverItem}>
                <AttachFile />
                <Typography className={classes.typography}>Associer à une commande</Typography>
              </Box> */}
                {(!row.dernierChangementStatut || row.dernierChangementStatut?.status === 'EN_ATTENTE_APPROBATION') && (
                  <Box className={classes.popoverItem} onClick={handleModifier}>
                    <Edit />
                    <Typography className={classes.typography}>Modifier</Typography>
                  </Box>
                )}
                <Box className={classes.popoverItem} onClick={handleSupprimer}>
                  <Delete />
                  <Typography className={classes.typography}>Supprimer</Typography>
                </Box>
              </Box>
            </Popover>
          </Box>
        );
      },
    },
  ];
};
