import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      // padding: '24px',
    },
    body: {
      padding: '0px 24px 24px 24px',
    },
    popover: {
      display: 'flex',
      flexDirection: 'column',
      padding: '10px',
    },
    popoverItem: {
      display: 'flex',
      padding: '6px',
    },
    typography: {
      marginLeft: '16px',
      cursor: 'pointer',
    },
    titletolbarClassName: {
      fontSize: '18px',
      fontFamily: 'Roboto',
      fontWeight: 600,
    },
    filterContainer: {
      width: 'auto',
      display: 'flex',
    },
    buttonExport: {
      display: 'flex',
      alignItems: 'center',
      background: '#FAFAFA',
      '& .main-MuiSvgIcon-root': {
        color: '#8f8f8fed',
      },
    },
    datePicker: {
      maxWidth: 190,
    },
    textDialogue: {
      minWidth: '90px',
      marginBottom: 0,
    },
    exportTableCell: {
      fontSize: 14,
      fontFamily: 'Roboto',
    },
  })
);
