import { ConfirmDeleteDialog, NewCustomButton, Table, TopBar } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import { LazyQueryResult } from '@apollo/client';
import { Add } from '@material-ui/icons';
import React, { FC, useRef } from 'react';
import { useStyles } from './styles';
import { useVueListeColumn } from './useVueListeColumn';
import {
  FullDocumentCategorieFragment,
  SortDirection,
  Search_DocumentQuery,
  Search_DocumentQueryVariables,
} from '@lib/common/src/federation';
import { useHistory } from 'react-router';
import { useState } from 'react';
import { useDocumentAction } from '@lib/common/src';
import { useFactureParams } from '../GestionEspaceFacturation/useFactureParams';
import { MenuExport } from '../../components/facture/menu-export';
import { useExportFacture } from '../../hooks/useExportFacture';
import { useMemo } from 'react';
import { TableExport } from '../../components/facture/TableExport';
import { PDFExport } from '@progress/kendo-react-pdf';

interface VueListeProps {
  documents: LazyQueryResult<Search_DocumentQuery, Search_DocumentQueryVariables>;
  refetch: () => void;
  titleTopBar?: string;
  titleAddButton?: string;
  titleExportButton?: string;
  type: 'FACTURE' | 'AUTRES';
}

const VueListe: FC<VueListeProps> = ({ documents, refetch, titleTopBar, titleAddButton, titleExportButton, type }) => {
  const history = useHistory();
  const { handleDelete } = useDocumentAction(refetch);
  const { params, redirectTo } = useFactureParams(type);
  const componentToPrint = useRef<PDFExport>(null);
  const { getFacturesToExport } = useExportFacture();

  const [documentToShow, setDocumentToShow] = useState<FullDocumentCategorieFragment>();
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [factureSelected, setFactureSelected] = useState<FullDocumentCategorieFragment[]>([]);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);

  const facturesToExport = useMemo(() => {
    return getFacturesToExport(factureSelected);
  }, [factureSelected]);

  const factureToExport = useMemo(() => {
    if (documentToShow) return getFacturesToExport([documentToShow]);
    return [];
  }, [documentToShow]);

  const handleClearSelection = () => {
    setFactureSelected([]);
  };
  const handleSelectChange = (dataSelected: any) => {
    setFactureSelected(dataSelected);
  };

  const handleSortTable = (column: any, direction: SortDirection) => {
    redirectTo({ ...params, sorting: [{ field: column, direction }] });
  };

  const handleAddfacture = () => {
    history.push('/espace-facturation/form');
  };
  const handleMenuClick = (event: React.MouseEvent<HTMLElement>, facture: FullDocumentCategorieFragment) => {
    event.preventDefault();
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
    setDocumentToShow(facture);
  };
  const handleCloseMenu = () => {
    setAnchorEl(null);
  };
  const handleDetail = () => {
    if (documentToShow?.id) history.push(`/laboratoires/facture-detail?id=${documentToShow.id}`);
  };
  const handleModifier = () => {
    if (documentToShow?.id) history.push(`/espace-facturation/form?id=${documentToShow.id}`);
  };
  const handleSupprimer = () => {
    if (documentToShow?.id) setOpenDeleteDialog(true);
  };

  const handleConfirmDelete = () => {
    if (documentToShow?.id) handleDelete(documentToShow.id);
    setOpenDeleteDialog(false);
  };

  const handleExportPdf = () => {
    // makePdf(componentToPrint.current, `Facture`, {
    //   margin: 24,
    //   // paperSize: 'A4',
    // });
    if (componentToPrint?.current?.save) {
      componentToPrint?.current?.save();
    }
  };

  const columns = useVueListeColumn({
    anchorEl,
    handleMenuClick,
    handleCloseMenu,
    handleDetail,
    handleModifier,
    handleSupprimer,
    loading:false,
    factureToExport,
    handleExportPdf,
    type,
    activeItem: documentToShow,
  });

  const topBarComponent = (searchComponent: any) => (
    <TopBar
      // handleOpenAdd={handleOpenAdd}
      searchComponent={false}
      titleButton=" "
      titleTopBar={titleTopBar}
      withMoreButton
      buttons={
        type === 'FACTURE'
          ? [
              {
                component: (
                  <Box mr={2}>
                    <NewCustomButton onClick={handleAddfacture}>
                      <Add />
                      {titleAddButton}
                    </NewCustomButton>
                  </Box>
                ),
              },

              {
                component: (
                  <MenuExport facturesToExport={facturesToExport} handleExportPdf={handleExportPdf} loading={false} />
                ),
              },
            ]
          : undefined
      }
      filterClassName={classes.filterContainer}
      withFilter={false}
      titleTopBarClassName={classes.titletolbarClassName}
    />
  );

  const classes = useStyles();
  return (
    <>
      <Box height={1}>
        <Box className={classes.body}>
          <Table
            showSelectedNotice={false}
            style={{ marginTop: 25 }}
            error={documents.error}
            loading={documents?.loading}
            search={false}
            data={documents.data?.searchGedDocuments.data || []}
            selectable={type === 'FACTURE'}
            columns={columns}
            rowsTotal={documents.data?.searchGedDocuments.total || 0}
            // onRowClick={handleRowClick}
            onSortColumn={handleSortTable}
            topBarComponent={topBarComponent}
            onRowsSelectionChange={handleSelectChange}
            onClearSelection={handleClearSelection}
            rowsSelected={factureSelected}
            noTableToolbarSearch
          />
        </Box>
        <ConfirmDeleteDialog
          title="Suppression de la Facture"
          content="Voulez-vous supprimez ce compte comptable"
          open={openDeleteDialog}
          setOpen={setOpenDeleteDialog}
          confirmBtnLabel="Supprimer"
          onClickConfirm={handleConfirmDelete}
        />
      </Box>
      <div
        style={{
          position: 'absolute',
          left: '-1000px',
          top: 0,
        }}
      >
        <PDFExport paperSize="A4" margin="1cm" ref={componentToPrint}>
          <TableExport facturesToExport={anchorEl ? factureToExport : facturesToExport} />
        </PDFExport>
      </div>
    </>
  );
};

export default VueListe;
