import {
  EspaceDocumentaire,
  FactureStatusSelect,
  FournisseurSelect,
  IDocumentSearch,
  OrigineInput,
  PageContainerWithDetail,
  PartenaireSelect,
  PARTENAIRE_SERVICE,
  TopSectionContainer,
  uploadToS3,
  useApplicationContext,
  useUploadFiles,
} from '@lib/common';
import {
  useCreate_ActiviteMutation,
  useCreate_Document_CategorieMutation,
  useDelete_Document_CategorieMutation,
  useGed_Document_FournisseursQuery,
  useGet_Document_CategorieLazyQuery,
  useGet_Ged_Categories_Annee_MoisQuery,
  useGet_Mes_CategoriesQuery,
  useMax_Facture_Total_TtcLazyQuery,
  useSearch_DocumentLazyQuery,
  useToggle_Document_To_FavoriteMutation,
  useUpdate_Document_CategorieMutation,
  FullDocumentCategorieFragment,
} from '@lib/common/src/federation';
import {
  Smyley,
  useCommentsLazyQuery,
  useCreate_Put_Pesigned_UrlMutation,
  useCreate_User_SmyleyMutation,
  useTvasQuery,
} from '@lib/common/src/graphql';
import { CustomDatePicker, NewCustomButton, SmallLoading } from '@lib/ui-kit';
// import HeaderPage from '@lib/ui-kit/src/components/atoms/HeaderPageTopSection/HeaderPage';
import React, { FC, useEffect, useState, ChangeEvent, MouseEvent } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory, useParams } from 'react-router';
import VueListe from '../VueListe/VueListe';
import filterIcon from '../../assets/img/filter_alt.svg';
import laboFilterIcon from '../../assets/img/science_black_24dp.svg';
import { Cancel, FormatListBulleted, ViewStream } from '@material-ui/icons';
import { useFactureParams } from './useFactureParams';
import { Box, Button, IconButton, Menu, Slider, Typography } from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import { useStyles } from './styles';
import filter_alt from '../../assets/img/filter_alt.svg';
import RestoreIcon from '@material-ui/icons/Restore';
import { ModeReglementInput } from '../../../../../libs/common/src/components/EspaceDocumentaire/NewGedForm/ModeReglementInput';

interface RechercheAvance {
  accesRapide?: any;
  idReglementMode?: string;
  origine?: any;
  origineAssocie?: any;
  dateDebut?: string;
  dateFin?: string;
  montantMinimal?: number;
  montantMaximal?: number;
}

interface GestionEspaceFacturationProps {
  type: 'AUTRES' | 'FACTURE';
  page?: string;
}

const GestionEspaceFacturation: FC<GestionEspaceFacturationProps> = ({ type, page }) => {
  const classes = useStyles();
  const history = useHistory();

  const { federation, notify, user, graphql, uploadFiles } = useApplicationContext();

  const userPartenaireId = user?.role?.code === PARTENAIRE_SERVICE ? user.prestataireService?.id : undefined;
  const isPrestataire = !!user?.prestataireService?.id;

  const [loadMaxFactureTtc, loadingMaxFactureTtc] = useMax_Facture_Total_TtcLazyQuery({
    fetchPolicy: 'network-only',
  });
  const maxFactureTotalTtc = (Math.round((loadingMaxFactureTtc.data?.maxFactureTotalTtc || 0) / 100) + 1) * 100;
  const initialRecherche: RechercheAvance | undefined = isPrestataire
    ? { accesRapide: { APPROUVE: true }, montantMaximal: maxFactureTotalTtc }
    : { montantMaximal: maxFactureTotalTtc };

  const take = 12;
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [mode, setMode] = useState<'creation' | 'remplacement'>('creation');
  const [skip, setSkip] = useState<number>(0);
  const [showRechercheAvance, setShowRechercheAvance] = useState<boolean>(isPrestataire);
  const [recherches, setRecherches] = useState<RechercheAvance | undefined>(initialRecherche);
  const [textToSearch, setTextToSearch] = useState<string>('');

  useEffect(() => {
    if (
      isPrestataire &&
      (!recherches?.accesRapide || !Object.keys(recherches?.accesRapide).some((key) => recherches?.accesRapide[key]))
    ) {
      setRecherches((prev) => ({ ...prev, accesRapide: initialRecherche?.accesRapide }));
    }
  }, [recherches?.accesRapide]);

  useEffect(() => {
    if (type === 'FACTURE' && maxFactureTotalTtc && recherches?.montantMaximal === 100) {
      setRecherches((prev) => ({ ...prev, montantMaximal: maxFactureTotalTtc }));
    }
    if (type === 'FACTURE' && maxFactureTotalTtc && searchParameters?.montantMaximal === 100) {
      setSearchParameters((prev: any) => ({ ...prev, montantMaximal: maxFactureTotalTtc }));
    }
  }, [maxFactureTotalTtc]);

  const {
    params: { vue, searchText, sorting, id: idDocument },
    redirectTo,
  } = useFactureParams(type);

  const showVueDetail = vue !== 'list';

  const { loading: loadingTva, error: errorTva, data: tvas } = useTvasQuery({ client: graphql });

  const [doLike] = useCreate_User_SmyleyMutation({
    client: graphql,
  });

  const [searchParameters, setSearchParameters] = useState<any>({
    proprietaire: 'TOUT',
    accesRapide: userPartenaireId ? ['APPROUVE'] : undefined,
    type,
    dateDebut: null,
    dateFin: null,
    montantMaximal: maxFactureTotalTtc || 0,
  });

  const {
    loading: categoriesLoading,
    error: categoriesError,
    data: categoriesData,
    refetch: refetchCategories,
  } = useGet_Mes_CategoriesQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const facturesLaboratoires = useGed_Document_FournisseursQuery({
    client: federation,
    variables: {
      input: {
        type: 'FACTURE',
        validation: userPartenaireId ? 'PRESTATAIRE_SERVICE' : 'INTERNE',
        idPartenaireValidateur: user.prestataireService?.idUser,
        typeFournisseur: 'LABORATOIRE',
      },
    },
    fetchPolicy: 'cache-and-network',
    skip: type !== 'FACTURE',
  });

  const facturesFournisseurs = useGed_Document_FournisseursQuery({
    client: federation,
    variables: {
      input: {
        type: 'FACTURE',
        validation: userPartenaireId ? 'PRESTATAIRE_SERVICE' : 'INTERNE',
        idPartenaireValidateur: user.prestataireService?.idUser,
        typeFournisseur: 'PRESTATAIRE_SERVICE',
      },
    },
    fetchPolicy: 'cache-and-network',
    skip: type !== 'FACTURE',
  });

  const facturesRejeteesInterneCategories = useGet_Ged_Categories_Annee_MoisQuery({
    client: federation,
    variables: {
      input: {
        statut: 'REFUSES',
        validation: 'INTERNE',
        type: 'FACTURE',
        idPartenaireValidateur: user.prestataireService?.idUser,
      },
    },

    skip: type !== 'FACTURE',
  });

  const facturesRejeteesPrestataireServiceCategories = useGet_Ged_Categories_Annee_MoisQuery({
    client: federation,
    variables: {
      input: {
        statut: 'REFUSES',
        validation: 'PRESTATAIRE_SERVICE',
        type: 'FACTURE',
        idPartenaireValidateur: user.prestataireService?.idUser,
      },
    },
    skip: type !== 'FACTURE',
  });

  const [
    getCommentaires,
    { loading: commentsLoading, error: commentsError, data: comments, refetch: refetchComments },
  ] = useCommentsLazyQuery({
    client: graphql,
  });

  const handleRefetchComments = () => {
    refetchComments && refetchComments();
    refetchDocument && refetchDocument();
  };

  const handleRefetchSmyleys = () => {
    refetchDocument && refetchDocument();
  };

  const handleRechercheAvanceClick = () => {
    setShowRechercheAvance(!showRechercheAvance);
  };

  const [getDocument, { loading: documentLoading, error: documentError, data: document, refetch: refetchDocument }] =
    useGet_Document_CategorieLazyQuery({
      client: federation,
      fetchPolicy: 'network-only',
    });

  const [addActivite] = useCreate_ActiviteMutation({
    client: federation,
    onCompleted: () => {
      refetchDocument && refetchDocument();
    },
  });

  const [searchDocuments, searchingDocuments] = useSearch_DocumentLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const variablesInput = {
    ...(searchParameters as any),
    libelleSousEspace: undefined,
    dateDebut: searchParameters.dateDebut ?? undefined,
    dateFin: searchParameters.dateFin ?? undefined,
    idPartenaireValidateur: userPartenaireId,
    limit: take,
    offset: skip,
  };

  const [addDocumentCategorie] = useCreate_Document_CategorieMutation({
    onCompleted: () => {
      setSaving(false);
      setSaved(true);
      handleRefetchAll(true);

      notify({
        message: 'Le document a été ajouté avec succès !',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout du document!",
        type: 'error',
      });

      setSaving(false);
    },
    client: federation,
  });

  const [toggleDocumentFavorite, { loading: toggleLoading, error: toggleError, data: toggle }] =
    useToggle_Document_To_FavoriteMutation({
      onError: () => {
        notify({
          message: "Des erreurs se sont survenues pendant l'ajout du document aux favoris!",
          type: 'error',
        });
      },

      onCompleted: () => {
        searchingDocuments.refetch && searchingDocuments.refetch();
      },

      client: federation,
    });

  useEffect(() => {
    if (toggle?.toggleToGedDocumentFavoris && !toggleLoading && !toggleError) {
      notify({
        message: `Le document a été ${
          toggle?.toggleToGedDocumentFavoris?.favoris ? 'ajouté aux' : 'retiré des'
        } favoris!`,
        type: 'success',
      });
    }
  }, [toggle, toggleLoading, toggleError]);

  const [updateDocumentCategorie] = useUpdate_Document_CategorieMutation({
    onCompleted: () => {
      setSaving(false);
      setSaved(true);
      notify({
        message: 'La facture a été remplacée avec succès !',
        type: 'success',
      });

      handleRefetchAll(true);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant le remplacement de la facture!',
        type: 'error',
      });
      setSaving(false);
    },
    client: federation,
  });

  const [deleteDocumentCategorie] = useDelete_Document_CategorieMutation({
    onCompleted: () => {
      notify({
        message: 'La facture a été supprimée avec succès !',
        type: 'success',
      });

      handleRefetchAll(true);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression de la facture !',
        type: 'error',
      });
    },
    client: federation,
  });

  useEffect(() => {
    if (sorting && sorting[0]) {
      setSearchParameters((prev: any) => ({
        ...prev,
        searchText,
        sortBy: sorting[0].field,
        sortDirection: sorting[0].direction,
      }));
    } else {
      setSearchParameters((prev: any) => ({
        ...prev,
        searchText,
        sortBy: undefined,
        sortDirection: undefined,
      }));
    }
  }, [searchText, sorting && sorting[0] && sorting[0].field, sorting && sorting[0] && sorting[0].direction]);

  useEffect(() => {
    if (toggle?.toggleToGedDocumentFavoris && !toggleLoading && !toggleError) {
      notify({
        message: `La facture a été ${
          toggle?.toggleToGedDocumentFavoris?.favoris ? 'ajoutée aux' : 'retirée des'
        } favoris!`,
        type: 'success',
      });
    }
  }, [toggle, toggleLoading, toggleError]);

  useEffect(() => {
    searchDocuments({
      variables: {
        input: variablesInput,
      },
    });
  }, [searchParameters]);

  useEffect(() => {
    loadMaxFactureTtc({
      variables: {
        input: { ...variablesInput, montantMaximal: undefined },
      },
    });
  }, [searchParameters.accesRapide]);

  useEffect(() => {
    if (idDocument) {
      getDoc(idDocument);
    }
  }, [idDocument]);

  const handleRefetchAll = (withSearch: boolean = false) => {
    if (withSearch) {
      searchingDocuments.refetch && searchingDocuments.refetch();
    }
    loadingMaxFactureTtc.refetch && loadingMaxFactureTtc.refetch();
    refetchCategories();
    facturesLaboratoires.refetch();
    facturesFournisseurs.refetch();
    facturesRejeteesInterneCategories.refetch();
    facturesRejeteesPrestataireServiceCategories.refetch();
  };

  const handleToggleDocumentToOrFromFavorite = (document: FullDocumentCategorieFragment): void => {
    toggleDocumentFavorite({
      variables: {
        idDocument: document?.id as any,
      },
    });
  };

  const handleDeleteDocument = (document: FullDocumentCategorieFragment): void => {
    deleteDocumentCategorie({
      variables: {
        input: {
          id: document?.id as any,
        },
      },
    });
  };

  const handleDetailsDocument = (document: FullDocumentCategorieFragment): void => {
    redirectTo({ id: document.id });
  };

  const handleOnRequestDocument = (id: string) => {
    getDoc(id);
  };

  const getDoc = (id: string) => {
    getDocument({
      variables: {
        id: id,
      },
    });

    addActivite({
      variables: {
        input: {
          activiteTypeCode: 'VISIT',
          itemCode: 'GED_DOCUMENT',
          idItemAssocie: id,
        },
      },
    });

    getCommentaires({
      variables: {
        codeItem: 'GED_DOCUMENT',
        idItemAssocie: id,
      },
    });
  };

  const upsertDocument = ({
    id,
    idSousCategorie,
    description,
    nomenclature,
    numeroVersion,
    idUserVerificateur,
    idUserRedacteur,
    dateHeureParution,
    dateHeureDebutValidite,
    dateHeureFinValidite,
    motCle1,
    motCle2,
    motCle3,
    fichier,
    idOrigine,
    idOrigineAssocie,
    dateFacture,
    hT,
    tva,
    ttc,
    type,

    numeroFacture,
    factureDateReglement,
    idReglementMode,
    numeroCommande,
    isGenererCommande,
    avoirType,
    avoirCorrespondants,
    nombreJoursPreavis,
    isRenouvellementTacite,
    isMultipleTva,
    factureTvas,
  }: any): void => {
    if (!id) {
      setMode('creation');
      addDocumentCategorie({
        variables: {
          input: {
            idSousCategorie: idSousCategorie,
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: new Date(dateHeureParution),
            dateHeureDebutValidite: new Date(dateHeureDebutValidite),
            dateHeureFinValidite: new Date(dateHeureFinValidite),
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
            nombreJoursPreavis,
            isRenouvellementTacite,
            isMultipleTva,
            facturesTva: factureTvas,
          },
        },
      }).then(() => {
        searchingDocuments.refetch && searchingDocuments.refetch();
        loadingMaxFactureTtc.refetch && loadingMaxFactureTtc.refetch();
        refetchCategories();
      });
    } else {
      setMode('remplacement');
      updateDocumentCategorie({
        variables: {
          input: {
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: dateHeureParution,
            dateHeureDebutValidite: dateHeureDebutValidite,
            dateHeureFinValidite: dateHeureFinValidite,
            idSousCategorie: idSousCategorie,
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
            nombreJoursPreavis,
            isRenouvellementTacite,
            isMultipleTva,
            facturesTva: factureTvas,
          },
          id: id,
        },
      }).then(() => {
        refetchCategories();
        loadingMaxFactureTtc.refetch && loadingMaxFactureTtc.refetch();
      });
    }
  };

  const showError = () => {
    notify({
      message: `Une erreur est survenue pendant le chargement de la facture`,
      type: 'error',
    });
  };
  const handleSave = (doc: any): void => {
    setSaving(true);
    setSaved(false);
    if (doc.launchUpload) {
      uploadFiles([doc.selectedFile], {
        directory: 'ged',
      })
        .then((uploadedFiles) => {
          if (uploadedFiles.length > 0) {
            upsertDocument({
              id: doc.id,
              idSousCategorie: doc.idSousCategorie,
              description: doc.description,
              nomenclature: doc.nomenclature,
              numeroVersion: doc.numeroVersion,
              idUserVerificateur: doc.idUserVerificateur,
              idUserRedacteur: doc.idUserRedacteur,
              dateHeureParution: doc.dateHeureParution,
              dateHeureDebutValidite: doc.dateHeureDebutValidite,
              dateHeureFinValidite: doc.dateHeureFinValidite,
              motCle1: doc.motCle1,
              motCle2: doc.motCle2,
              motCle3: doc.motCle3,
              fichier: uploadedFiles[0],
              idOrigine: doc.origine?.id || doc.origine,
              idOrigineAssocie: doc.correspondant?.id || doc.origineAssocie?.id,
              dateFacture: doc.dateFacture,
              hT: doc.hT ? parseFloat(doc.hT) : undefined,
              tva: doc.tva ? parseFloat(doc.tva) : undefined,
              ttc: doc.ttc ? parseFloat(doc.ttc) : undefined,
              type: doc.type,

              numeroFacture: doc?.numeroFacture,
              factureDateReglement: doc?.factureDateReglement,
              idReglementMode: doc?.idReglementMode,
              numeroCommande: doc?.numeroCommande,
              isGenererCommande: doc?.isGenererCommande,
              avoirType: doc?.avoirType,
              avoirCorrespondants: doc?.avoirCorrespondants,
              nombreJoursPreavis: doc?.nombreJoursPreavis,
              isRenouvellementTacite: doc?.isRenouvellementTacite,
              isMultipleTva: doc?.isMultipleTva,
              factureTvas: doc?.factureTvas,
            });
          }
        })
        .catch((error) => {
          showError();
        });
    } else {
      const { nomOriginal, chemin, type } = doc.previousFichier || {};

      upsertDocument({
        id: doc.id,
        idSousCategorie: doc.idSousCategorie,
        description: doc.description,
        nomenclature: doc.nomenclature,
        numeroVersion: doc.numeroVersion,
        idUserVerificateur: doc.idUserVerificateur,
        idUserRedacteur: doc.idUserRedacteur,
        dateHeureParution: doc.dateHeureParution,
        dateHeureDebutValidite: doc.dateHeureDebutValidite,
        dateHeureFinValidite: doc.dateHeureFinValidite,
        motCle1: doc.motCle1,
        motCle2: doc.motCle2,
        motCle3: doc.motCle3,
        fichier: doc.previousFichier ? { nomOriginal, chemin, type } : undefined,
        idOrigine: doc.origine?.id || doc.origine,
        idOrigineAssocie: doc.correspondant?.id,
        dateFacture: 'FACTURE' === doc.type ? doc.dateFacture : undefined,
        hT: 'FACTURE' === doc.type && doc.hT ? parseFloat(doc.hT) : undefined,
        tva: 'FACTURE' === doc.type && doc.tva ? parseFloat(doc.tva) : undefined,
        ttc: 'FACTURE' === doc.type && doc.ttc ? parseFloat(doc.ttc) : undefined,
        type: doc.type,

        numeroFacture: doc?.numeroFacture,
        factureDateReglement: doc?.factureDateReglement,
        idReglementMode: doc?.idReglementMode,
        numeroCommande: doc?.numeroCommande,
        isGenererCommande: doc?.isGenererCommande,
        avoirType: doc?.avoirType,
        avoirCorrespondants: doc?.avoirCorrespondants,
        nombreJoursPreavis: doc?.nombreJoursPreavis,
        isRenouvellementTacite: doc?.isRenouvellementTacite,
        isMultipleTva: doc?.isMultipleTva,
        factureTvas: doc?.factureTvas,
      });
    }
  };

  const handleLike = (document: FullDocumentCategorieFragment, smyley: Smyley | null): void => {
    doLike({
      variables: {
        codeItem: 'GED_DOCUMENT',
        idSource: document?.id as any,
        idSmyley: smyley?.id as any,
      },
    });
  };

  const handleCompleteDownload = (document: FullDocumentCategorieFragment): void => {
    addActivite({
      variables: {
        input: {
          activiteTypeCode: 'DOWNLOAD',
          itemCode: 'GED_DOCUMENT',
          idItemAssocie: document.id as any,
        },
      },
    });
  };

  const handleRefresh = (): void => {
    searchingDocuments.refetch && searchingDocuments.refetch();
    setSearchParameters({
      searchText: undefined,
      accesRapide: undefined,
      proprietaire: 'TOUT',
      idSousCategorie: undefined,
      filterBy: undefined,
      sortBy: undefined,
      sortDirection: undefined,
      type: undefined,
      validation: 'TOUT',
      idOrigine: undefined,
      idOrigineAssocie: undefined,
      dateDebut: undefined,
      dateFin: undefined,
    });
    history.push(type === 'FACTURE' ? `/espace-facturation` : `/espace-documentaire`);
  };

  const handleSearchVariables = (variables: IDocumentSearch) => {
    // history.push(`/espace-facturation`);
    if (idDocument) {
      redirectTo({ id: undefined });
    }
    setSearchParameters(variables);
  };

  const handleDeplace = (document: FullDocumentCategorieFragment): void => {};

  const handleFetchMoreComments = (document: FullDocumentCategorieFragment): void => {};

  const handleGoBack = () => {
    history.push('/');
  };

  const handleSearchTopSection = (value: any) => {
    // redirectTo({ vue: vue, sorting, searchText: value });
    setTextToSearch(value);
  };
  console.log('vuesearch2', vue);
  useEffect(() => {
    redirectTo({ vue: vue, sorting, searchText: textToSearch });
  }, [textToSearch]);

  if (
    categoriesLoading ||
    facturesLaboratoires.loading ||
    facturesFournisseurs.loading ||
    facturesRejeteesInterneCategories.loading ||
    facturesRejeteesPrestataireServiceCategories.loading
  )
    return <SmallLoading />;

  const onChangePartenaireSelect = (value: any) => {};

  const handleChangeSearchVariables = (newValues: IDocumentSearch): void => {
    setSearchParameters((prev: any) => ({
      ...prev,
      idSousCategorie: undefined,
      filterBy: 'TOUT',
      sortBy: undefined,
      type: undefined,
      validation: 'TOUT',
      idOrigine: undefined,
      idOrigineAssocie: undefined,
      idReglementMode: undefined,
      dateDebut: undefined,
      dateFin: undefined,
      libelleSousEspace: undefined,
      accesRapide: undefined,
      montantMinimal: undefined,
      montantMaximal: undefined,
      searchText: undefined,
      ...newValues,
    }));
  };

  const handleChangeFilterDate = (name: string, value?: any) => {
    setRecherches((prev) => ({
      ...prev,
      [name]: value,
    }));
  };
  const handleChangeFilterMontant = (_event: any, newValue: any) => {
    if (newValue?.length === 2) {
      setRecherches((prev) => ({
        ...prev,
        montantMinimal: newValue[0],
        montantMaximal: newValue[1],
      }));
    }
  };

  const handleChangeAcces = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRecherches((prev) => ({
      ...prev,
      accesRapide: { ...(prev?.accesRapide || {}), [event.target.name]: event.target.checked },
    }));
  };
  const handleChangeRecherche = (name: string, value?: any) => {
    setRecherches((prev) => ({ ...prev, [name]: value }));
  };

  const handleSearch = (e: React.MouseEvent<HTMLElement>) => {
    e.stopPropagation();
    e.preventDefault();

    const accesRapide = recherches?.accesRapide
      ? Object.entries(recherches?.accesRapide)
          .filter((entry) => entry[1])
          .map((entry) => entry[0])
      : [];
    handleChangeSearchVariables({
      accesRapide: accesRapide as any,
      idReglementMode: recherches?.idReglementMode,
      idOrigine: recherches?.origine?.id,
      idOrigineAssocie: recherches?.origineAssocie?.id,
      montantMaximal: recherches?.montantMaximal,
      montantMinimal: recherches?.montantMinimal,
      type,
    });
  };
  const handleReinitialSearch = (e: React.MouseEvent<HTMLElement>) => {
    e.stopPropagation();
    e.preventDefault();

    setRecherches(initialRecherche);
    handleChangeSearchVariables({
      type,
    });
  };

  const title = type === 'FACTURE' ? 'Espace Facturation' : 'Espace Documentaire';

  console.log('************** Document Document ************', idDocument, 'doc', document);

  return (
    <>
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <Box className={classes.root}>
        <TopSectionContainer
          showPartenaireSelectLabos={false}
          defaultMobile
          onChangePartenaireSelect={onChangePartenaireSelect}
          laboFilterIcon={laboFilterIcon}
          laboFilterPrestataireIcon={filterIcon}
          filterIcon={filterIcon}
          title={title}
          withSearch
          searchValue={searchText}
          onRunSearch={handleSearchTopSection}
          // withPrestataireServices={showVueDetail}
          // onChangePrestataireSelect={onChangePrestataireSelect}
          additionnalFilter={
            <>
              {!showVueDetail ? (
                type === 'FACTURE' ? (
                  <Button onClick={handleRechercheAvanceClick} className={classes.rechercheAvance}>
                    <Typography className={classes.rechercheTypography}>Recherche avancée</Typography>
                    <Box className={classes.rechercheIcons}>
                      {showRechercheAvance ? <CancelIcon /> : <AddCircleIcon />}
                    </Box>
                  </Button>
                ) : null
              ) : (
                ''
              )}
              <NewCustomButton
                onClick={() => redirectTo({ vue: vue === 'list' ? 'detail' : 'list' })}
                startIcon={showVueDetail ? <FormatListBulleted /> : <ViewStream />}
                theme="transparent"
                shadow={false}
                style={{
                  textTransform: 'none',
                  background: '#F2F2F2',
                  marginLeft: '8px',
                }}
              >
                {showVueDetail ? 'Vue liste' : 'Vue détail'}
              </NewCustomButton>
            </>
          }
        />
        <Box className={classes.container}>
          {showVueDetail ? (
            <>
              <EspaceDocumentaire
                facturesRejeteesInterneCategories={
                  facturesRejeteesInterneCategories.data?.gedCategoriesAnneeMois || ([] as any)
                }
                facturesRejeteesPrestataireServiceCategories={
                  facturesRejeteesPrestataireServiceCategories.data?.gedCategoriesAnneeMois || ([] as any)
                }
                onGoback={handleGoBack}
                categories={{
                  loading: categoriesLoading,
                  error: categoriesError as any,
                  data: categoriesData?.gedMesCategories as any,
                }}
                documents={searchingDocuments}
                content={{
                  loading: documentLoading,
                  error: documentError as any,
                  data: document?.gedDocument as any,
                }}
                commentaires={{
                  loading: commentsLoading,
                  error: false as any,
                  data: comments?.comments as any,
                }}
                mode={mode}
                activeDocument={idDocument ? (document?.gedDocument as any) : undefined}
                saving={saving}
                saved={saved}
                setSaved={() => setSaved(false)}
                searchVariables={searchParameters}
                onRequestCreateDocument={handleSave}
                onRequestAddDocumentToFavorite={handleToggleDocumentToOrFromFavorite}
                onRequestDeleteDocument={handleDeleteDocument}
                onRequestDetailsDocument={handleDetailsDocument}
                onRequestRemoveDocumentFromFavorite={handleToggleDocumentToOrFromFavorite}
                onRequestReplaceDocument={handleSave}
                onRequestGetComments={() => {}}
                onRequestFetchMoreComments={handleFetchMoreComments}
                onRequestLike={handleLike}
                onRequestRefresh={handleRefresh}
                onRequestDeplace={handleDeplace}
                setSearchVariables={(parameters: any) => handleSearchVariables(parameters as any)}
                refetchComments={handleRefetchComments}
                refetchSmyleys={handleRefetchSmyleys}
                onCompleteDownload={handleCompleteDownload}
                refetchDocument={refetchDocument}
                onRequestDocument={handleOnRequestDocument}
                onRequestFetchAll={handleRefetchAll}
                espaceType={type === 'FACTURE' ? 'espace-facture' : 'espace-documentaire'}
                factureLaboratoires={facturesLaboratoires.data?.gedDocumentFournisseurs || []}
                facturePartenaires={facturesFournisseurs.data?.gedDocumentFournisseurs || []}
                tvas={{
                  loading: loadingTva,
                  error: errorTva as any,
                  data: tvas?.tvas || [],
                }}
                maxTTC={maxFactureTotalTtc}
              />
            </>
          ) : (
            <>
              {showRechercheAvance && (
                <Box className={classes.rechercheAvanceList}>
                  <Box className={classes.slider}>
                    <Box>
                      <Box className={classes.filterSubtitle}>Par période</Box>
                      <Box className={classes.flex}>
                        <CustomDatePicker
                          label="De"
                          value={recherches?.dateDebut}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          onChange={(value) => {
                            handleChangeFilterDate('dateDebut', value);
                          }}
                          fullWidth
                          className={classes.dateField}
                        />
                        <CustomDatePicker
                          label="À"
                          value={recherches?.dateFin}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          onChange={(value) => {
                            handleChangeFilterDate('dateFin', value);
                          }}
                          fullWidth
                          className={classes.dateField}
                        />
                      </Box>
                    </Box>
                    <Box className={classes.filter}>
                      <Box className={classes.customSelectCheckBox}>
                        <FactureStatusSelect
                          accesRapide={recherches?.accesRapide}
                          onChange={handleChangeAcces}
                          isPrestataire={isPrestataire}
                        />
                      </Box>
                      <Box className={classes.customSelectCheckBox} ml={1}>
                        <ModeReglementInput
                          label="Modalité de paiement"
                          idReglementMode={recherches?.idReglementMode || ''}
                          onChange={(e: ChangeEvent<HTMLInputElement>) =>
                            handleChangeRecherche('idReglementMode', e.target.value)
                          }
                        />
                      </Box>
                    </Box>
                  </Box>
                  <Box className={classes.slider}>
                    <Box className={classes.sliderItem}>
                      <Typography className={classes.filterSubtitle} id="range-slider">
                        Par montant
                      </Typography>
                      <Box display="flex" justifyContent="space-between">
                        <Box>
                          min :{' '}
                          <span className={classes.filterSubtitle}>
                            {(recherches?.montantMinimal || 0).toFixed(2)}&nbsp;€
                          </span>
                        </Box>
                        <Box>
                          max :{' '}
                          <span className={classes.filterSubtitle}>
                            {(recherches?.montantMaximal || 0).toFixed(2)}&nbsp;€
                          </span>
                        </Box>
                      </Box>
                      <Slider
                        value={[recherches?.montantMinimal || 0, recherches?.montantMaximal || 0]}
                        onChange={handleChangeFilterMontant}
                        valueLabelDisplay="auto"
                        aria-labelledby="range-slider"
                        min={0}
                        max={maxFactureTotalTtc}
                      />
                    </Box>
                    <Box className={classes.filterWithSearch}>
                      <Box className={classes.partenaireClassName}>
                        <OrigineInput
                          onChangeOrigine={(origine: any) => {
                            handleChangeRecherche('origine', origine);
                          }}
                          origine={recherches?.origine}
                          origineAssocie={recherches?.origineAssocie}
                          onChangeOrigineAssocie={(origineAssocie) => {
                            handleChangeRecherche('origineAssocie', origineAssocie);
                          }}
                          enabledCodes={['CONFRERE', 'LABORATOIRE', 'PRESTATAIRE_SERVICE']}
                          withSelectAll
                        />
                      </Box>
                      <Box
                        style={{ display: 'flex', justifyContent: 'space-between', marginBottom: '16px', width: '63%' }}
                      >
                        <Box marginLeft="16px" onClick={handleSearch}>
                          <NewCustomButton>Rechercher</NewCustomButton>
                        </Box>
                        <Button onClick={handleReinitialSearch} className={classes.reinitialise}>
                          <RestoreIcon />
                        </Button>
                      </Box>
                    </Box>
                  </Box>
                </Box>
              )}
              <VueListe
                titleExportButton="Exporter des factures"
                titleAddButton="NOUVELLE FACTURE"
                titleTopBar={type === 'FACTURE' ? 'Historique des factures' : 'Liste des documents'}
                documents={searchingDocuments}
                refetch={() => handleRefetchAll(true)}
                type={type}
              />
            </>
          )}
        </Box>
      </Box>
    </>
  );
};

export default GestionEspaceFacturation;
