import { createStyles, lighten, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      height: '100%',
    },

    container: {
      flexGrow: 1,
    },
    rechercheAvance: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      // marginBottom: '24px',
      cursor: 'pointer',
      '& .main-MuiButton-label': {
        flexDirection: 'row-reverse',
      },
    },
    rechercheTypography: {
      color: theme.palette.primary.main,
      fontSize: '16px',
      fontFamily: 'Roboto',
      fontWeight: 500,
      textTransform: 'initial',
      marginLeft: '4px',
    },
    rechercheIcons: {
      '& .main-MuiSvgIcon-root': {
        color: theme.palette.primary.main,
        marginTop: '4px',
      },
    },
    partenaireClassName: {
      minWidth: '546px',
      marginLeft: 8,
    },
    reinitialise: {
      display: 'flex',
      alignItems: 'center',
      // border: '1px solid #000',
      marginLeft: '16px',
      // '&:not(:hover)': {
      //   '& #reinitial_id': {
      //     opacity: 0,
      //   },
      // },
      // '& #reinitial_id': {
      //   opacity: 1,
      // },
    },
    classNameSelectCheckBoxMenu: {
      display: 'flex',
      marginLeft: '8px',
      // textAlign: 'left',
      // width: '100%',
      // '&.main-MuiSelect-select' : {
      //     width: '220px'
      // },
    },
    filter: {
      display: 'flex',
      alignItems: 'center',
      width: '100%',
      marginLeft: '16px',
      marginTop: '70px',
    },
    filterWithSearch: {
      display: 'flex',
      width: '100%',
      marginLeft: '19px',
      marginTop: '40px',
    },
    filterSubtitle: {
      fontSize: 14,
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      marginTop: 16,
      marginBottom: 16,
    },
    flex: {
      display: 'flex',
      alignItems: 'center',
      '&>div': {
        flex: '1 1 calc(50% - 16px)',
        maxWidth: 'calc(50% - 8px)',
      },
      '&>div:last-of-type': {
        marginLeft: theme.spacing(2),
      },
    },
    slider: {
      display: 'flex',
      alignItems: 'center',
      marginLeft: '24px',
    },
    sliderItem: {
      minWidth: '388px',
    },
    dateField: {
      '& button': {
        padding: '0px !important',
      },
      '& input': {
        fontSize: '14px !important',
        height: '20px',
      },
      '& div': {
        paddingRight: '4px !important',
      },
    },
    customSelectCheckBox: {
      '& .main-MuiSelect-root': {
        width: '240px',
      },
    },
    rangeBox: {
      display: 'flex',
      justifyContent: 'space-between',
      padding: 8,
      paddingTop: 0,
      paddingBottom: 16,
      borderBottom: 'solid 1px #2121211A',
    },
    textRange: {
      fontSize: 14,
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      color: '#21212133',
    },
    buttonPeriode: {
      display: 'flex',
      padding: '11px 34px',
      background: '#F2F2F2',
      alignItems: 'center',
      borderRadius: '4px !important',
      textTransform: 'none',
      justifyContent: 'space-between',
    },
    dateIcon: {
      marginRight: 12,
    },
    rechercheAvanceList: {
      background: lighten(theme.palette.primary.main, 0.9),
      width: '98%',
      margin: 'auto',
    },
  })
);
