import { useURLSearchParams } from '@lib/common';
import { useCallback, useMemo } from 'react';
import { useHistory } from 'react-router';
import { SortDirection, SortNulls } from '@lib/common/src/federation';

const isPrimitive = (value: any): boolean => {
  return value !== Object(value);
};

export interface FactureParams {
  id?: string;
  vue?: 'list' | 'detail';
  searchText?: string;
  sorting?: Array<{
    field: any;
    direction: SortDirection;
    nulls?: SortNulls;
  }>;
}

export interface UseFactureParamsResult {
  params: FactureParams;
  redirectTo: (params?: FactureParams) => void;
}
const encodeParam = (param: any, isObjectArray?: boolean): string | undefined => {
  if (Array.isArray(param) && param.length > 0) {
    return isObjectArray
      ? encodeURIComponent(param.map((el) => JSON.stringify(el)).join(','))
      : encodeURIComponent(param.join(','));
  } else if (typeof param === 'string') {
    return encodeURIComponent(param);
  } else if (isPrimitive(param)) {
    return param;
  } else if (param) {
    return encodeURIComponent(JSON.stringify(param));
  }

  return undefined;
};

const decodeParam = (
  params: URLSearchParams,
  name: keyof FactureParams,
  isArray: boolean,
  isObjectArray?: boolean
): any | undefined => {
  const value = params.get(name);
  if (value) {
    const decodedValue = decodeURIComponent(value);
    if (isObjectArray) {
      return JSON.parse(`[${decodedValue}]`);
    }
    if (isArray) {
      return decodedValue.split(',');
    }
    return decodedValue;
  }

  return undefined;
};

const espaceFacturationBasePath = '/espace-facturation';
const espaceDocumentaireBasePath = 'espace-documentaire';
export const useFactureParams = (type: 'FACTURE' | 'AUTRES'): UseFactureParamsResult => {
  const query = useURLSearchParams();
  const history = useHistory();

  const params: FactureParams = useMemo(
    () => ({
      id: query.get('id') as any,
      vue: decodeParam(query, 'vue', false),
      searchText: decodeParam(query, 'searchText', false),
      sorting: decodeParam(query, 'sorting', true, true),
    }),
    [query]
  );

  const redirectTo = useCallback(
    (changedParams?: FactureParams) => {
      const newParams: any = {
        ...params,
        ...(changedParams || {}),
      };

      const objectArrayParams = ['sorting'];
      const newUrl = Object.keys(newParams).reduce((url, paramName: any) => {
        const newValue = newParams[paramName];
        if (!newValue) {
          return url;
        }
        const encodedValue = encodeParam(newValue, objectArrayParams.includes(paramName) ? true : false);
        if (!encodedValue) {
          return url;
        }

        const keyValue = `${paramName}=${encodedValue}`;

        const toAppend = !url.includes('?') ? `?${keyValue}` : `&${keyValue}`;

        return `${url}${toAppend}`;
      }, `${type === 'FACTURE' ? espaceFacturationBasePath : espaceDocumentaireBasePath}`);

      history.push(newUrl);
    },
    [params]
  );

  return {
    params,
    redirectTo,
  };
};
