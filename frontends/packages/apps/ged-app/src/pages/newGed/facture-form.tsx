import React, { FC, useEffect, useState } from 'react';
import { FactureSave, NewGedForm } from '@lib/common';
import { useDocumentAction } from '@lib/common';
import { useLocation } from 'react-router';
import { FullDocumentCategorieFragment, useGet_Document_CategorieLazyQuery } from '@lib/common/src/federation';

const FactureForm: FC<{}> = () => {
  const { handleCreateDocument, saved, saving, setSaved } = useDocumentAction();
  const { search } = useLocation();
  const id = new URLSearchParams(search).get('id');
  const statusParams = new URLSearchParams(search).get('status');
  const [documentToEdit, setDocumentToEdit] = useState<FullDocumentCategorieFragment>();
  const mode = statusParams || (id ? 'modification' : 'creation');

  const [loadDocument] = useGet_Document_CategorieLazyQuery({
    fetchPolicy: 'network-only',
    onCompleted: (result) => {
      if (result.gedDocument) setDocumentToEdit(result.gedDocument);
    },
  });

  useEffect(() => {
    if (id) {
      loadDocument({
        variables: {
          id,
        },
      });
    }
  }, [id]);

  const createDocument = (documentToSave: FactureSave) => {
    if (documentToSave.files?.length && documentToSave.files[0]) {
      const doc: any = {
        description: documentToSave.description,
        origine: documentToSave.origine,
        correspondant: documentToSave.correspondant,
        dateFacture: documentToSave.factureDate,
        hT: documentToSave.factureTotalHt,
        tva: documentToSave.factureTva,
        ttc: documentToSave.factureTotalTtc,
        type: 'FACTURE',

        numeroFacture: documentToSave.numeroFacture,
        factureDateReglement: documentToSave.factureDateReglement,
        idReglementMode: documentToSave.idReglementMode,
        numeroCommande: parseInt(documentToSave.numeroCommande || '0'),
        isGenererCommande: documentToSave.isGenererCommande,
        avoirCorrespondants: [],
        factureLignes: documentToSave?.factureLignes?.map(({ montantHt, tva, compteComptable, libelle, montantTva }) => ({
          idEfTva: tva?.id,
          idCompteComptable: compteComptable?.id,
          libelle,
          montantHt,
          montantTva,
        })),
      };
      const dataToSave =
        mode === 'creation'
          ? {
            ...doc,
            selectedFile: documentToSave.files[0],
            launchUpload: !!(documentToSave.files[0] && documentToSave.files[0].size),
          }
          : {
            ...doc,
            id: documentToEdit?.id,
            selectedFile: documentToSave.files[0],
            previousFichier: documentToEdit?.fichier,
            launchUpload: !!(documentToSave.files.length > 0 && documentToSave.files[0].size),
          };
      handleCreateDocument(dataToSave, mode);
    }
  };
  return (
    <NewGedForm
      onSave={createDocument}
      saved={saved}
      saving={saving}
      setSaved={setSaved}
      documentToEdit={documentToEdit}
      mode={mode}
    />
  );
};

export default FactureForm;
