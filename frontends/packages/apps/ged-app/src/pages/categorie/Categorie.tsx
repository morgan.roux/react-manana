import React, { FC, useState } from 'react';
import { CategoriePage, ICategorie, ISousCategorie } from '@lib/ui-kit';
import { NoItemContentImage } from '@lib/ui-kit';
import { PartenaireInput, SUPER_ADMINISTRATEUR, useApplicationContext } from '@lib/common';
import { useCreate_CategorieMutation, useCreate_Sous_CategorieMutation, useDelete_CategorieMutation, useDelete_Sous_CategorieMutation, useGet_CategorieLazyQuery, useGet_CategoriesQuery, useUpdate_CategorieMutation, useUpdate_Sous_CategorieMutation } from '@lib/common/src/federation';
import UserInput from '@lib/common/src/components/UserInput';

const workflows = [
  {
    code: 'INTERNE',
    libelle: 'Interne',
  },
  {
    code: 'UTILISATEUR',
    libelle: 'Utilisateur',
  },
  {
    code: 'PARTENAIRE_SERVICE',
    libelle: 'Prestataire Service',
  },
];

export interface PrestataireInterface {
  id: string;
  nom: string;
}

const Categorie: FC<{ user: any }> = ({ user }) => {
  const { currentPharmacie: pharmacie, federation, notify } = useApplicationContext();

  // const partenaireType = ['partenaire'];
  const codeParameter = '0831';

  const [openContact, setOpenContact] = useState<boolean>(false);
  const [usersSelected, setUsersSelected] = useState<any[]>([]);
  const [prestataire, setPrestataire] = useState<any>('');
  // const [valueAuto, setValueAuto] = useState<any>('');

  const isSuperAdmin = user && user.role && user.role.code && user.role.code === SUPER_ADMINISTRATEUR;

  const { loading, error, data, refetch: refetchCategories } = useGet_CategoriesQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
    variables: {
      filter: isSuperAdmin
        ? {
            or: [{ idPharmacie: { is: null } }, { idPharmacie: { eq: pharmacie.id } }],
          }
        : pharmacie
        ? {
            idPharmacie: {
              eq: pharmacie.id,
            },
          }
        : {},
    },
  });

  // const [
  //   getCollaborateurs,
  //   { loading: getCollaborateursLoading, error: getCollaborateursError, data: collaborateurs },
  // ] = useLazyQuery<GET_COLLABORATEURS_TYPE>(GET_COLLABORATEURS, {
  //   client: federation,
  // });

  const [
    getCategorie,
    { loading: getCategorieLoading, error: getCategorieError, data: categorie, refetch: refetchCategorie },
  ] = useGet_CategorieLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  const [addCategorie] = useCreate_CategorieMutation({
    onCompleted: () => {
      notify({
        message: 'La catégorie a été ajoutée avec succès.',
        type: 'success',
      });

      // TODO : Update cache
      refetchCategories();
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'enregistrement de la catégorie",
        type: 'success',
      });
    },
    client: federation,
  });

  const [editCategorie] = useUpdate_CategorieMutation({
    onCompleted: () => {
      notify({
        message: 'La catégorie a été modifiée avec succès !',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification de la catégorie',
        type: 'error',
      });
    },
    client: federation,
  });

  const [deleteCategorie] = useDelete_CategorieMutation({
    onCompleted: () => {
      notify({
        message: 'La catégorie a été supprimée avec succès !',
        type: 'success',
      });

      refetchCategories();
    },
    onError: () => {
      notify({
        message: 'Des erreus se sont survenues pendant la suppression de la catégorie',
        type: 'error',
      });
    },
    client: federation,
  });

  const [createSousCategorie] = useCreate_Sous_CategorieMutation({
      onCompleted: () => {
        notify({
          message: 'La sous-catégorie a été ajoutée avec succès!',
          type: 'success',
        });

        refetchCategorie && refetchCategorie();
      },
      onError: () => {
        notify({
          message: "Des erreurs se sont survenues pendant l'ajout de la sous-catégorie",
          type: 'success',
        });
      },
      client: federation,
    }
  );

  const [updateSousCategorie] = useUpdate_Sous_CategorieMutation({
      onCompleted: () => {
        notify({
          message: 'La sous-catégorie a été modifiée avec succès!',
          type: 'success',
        });

        refetchCategorie && refetchCategorie();
      },
      onError: () => {
        notify({
          message: 'Des erreurs se sont survenues pendant la modification de la sous-catégorie',
          type: 'success',
        });
      },
      client: federation,
    }
  );

  const [deleteSousCategorie] = useDelete_Sous_CategorieMutation({
      onCompleted: () => {
        notify({
          message: 'La sous-catégorie a été supprimée avec succès !',
          type: 'success',
        });

        refetchCategorie && refetchCategorie();
      },
      onError: () => {
        notify({
          message: 'Des erreus se sont survenues pendant la suppression de la sous-catégorie',
          type: 'error',
        });
      },
      client: federation,
    }
  );

  const handleDelete = (categorie: ICategorie): void => {
    deleteCategorie({
      variables: {
        input: {
          id: categorie.id as any,
        },
      },
    });
  };

  const handleSaveCategorie = (categorie: ICategorie): void => {
    if (categorie?.id) {
      editCategorie({
        variables: {
          id: categorie?.id,
          categoryInput: {
            libelle: categorie?.libelle as any,
            type: categorie?.type as any,
            validation: categorie?.validation as any,
            idUserParticipants: categorie?.participants?.map(({ id }) => id),
            idPartenaireValidateur: categorie?.partenaireValidateur?.id || undefined,
            workflowValidation: categorie?.workflowValidation || undefined,
          },
        },
      });
    } else {
      addCategorie({
        variables: {
          categoryInput: {
            libelle: categorie?.libelle as any,
            type: categorie?.type as any,
            validation: categorie?.validation as any,
            idUserParticipants: categorie?.participants?.map(({ id }) => id),
            idPartenaireValidateur: categorie?.partenaireValidateur?.id || undefined,
            workflowValidation: categorie?.workflowValidation || undefined,
          },
        },
      });
    }
  };

  const handleSaveSousCategorie = (sousCategorie: ISousCategorie): void => {
    if (!sousCategorie.id) {
      createSousCategorie({
        variables: {
          input: {
            idCategorie: sousCategorie?.parent?.id as any,
            libelle: sousCategorie?.libelle as any,
            validation: sousCategorie?.validation as any,
            idUserParticipants: sousCategorie?.participants?.map(({ id }) => id),
            idPartenaireValidateur: sousCategorie?.partenaireValidateur?.id || undefined,
            workflowValidation: sousCategorie?.workflowValidation || undefined,
          },
        },
      });
    } else {
      updateSousCategorie({
        variables: {
          id: sousCategorie.id as any,
          input: {
            idCategorie: sousCategorie?.parent?.id as any,
            libelle: sousCategorie?.libelle as any,
            validation: sousCategorie?.validation as any,
            idUserParticipants: sousCategorie?.participants?.map(({ id }) => id),
            idPartenaireValidateur: sousCategorie?.partenaireValidateur?.id || undefined,
            workflowValidation: sousCategorie?.workflowValidation || undefined,
          },
        },
      });
    }
    setPrestataire('');
  };

  const handleDeleteSousCategorie = (sousCategorie: ISousCategorie) => {
    if (sousCategorie.id) {
      deleteSousCategorie({
        variables: {
          input: {
            id: sousCategorie.id,
          },
        },
      });
    }
  };

  const handleRequestShowDetail = (categorie: ICategorie): void => {
    getCategorie({
      variables: {
        id: categorie.id as any,
      },
    });
  };

  const PartageComponent = (
    <UserInput
      openModal={openContact}
      setOpenModal={setOpenContact}
      selected={usersSelected}
      setSelected={setUsersSelected}
      codeParameter={codeParameter}
    />
  );

  return (
    <CategoriePage
      currentIdPharmacie={pharmacie?.id}
      loading={loading}
      error={error as any}
      emptyListComponent={
        <NoItemContentImage
          title="Aucune catégorie trouvée"
          subtitle="Ajouter-en une en cliquant sur le bouton de création d'en haut"
        />
      }
      noCategorieSelected={
        <NoItemContentImage
          title="Affichage de catégorie"
          subtitle="Cliquer sur une catégorie de la liste de gauche pour voir la description dans cette partie"
        />
      }
      categories={data?.gedCategories.nodes as any}
      onRequestDelete={handleDelete}
      onRequestSave={handleSaveCategorie}
      onRequestSaveSousCategorie={handleSaveSousCategorie}
      onRequestDeleteSousCategorie={handleDeleteSousCategorie}
      onRequestShowDetail={handleRequestShowDetail}
      categorie={{
        loading: getCategorieLoading,
        error: getCategorieError as any,
        data: categorie?.gedCategorie as any,
      }}
      PartageComponent={PartageComponent}
      workflows={workflows}
      participants={usersSelected}
      setParticipants={setUsersSelected}
      partenaireValidateur={typeof prestataire === 'string' ? { id: prestataire } : prestataire}
      setPartenaireValidateur={setPrestataire}
      SelectPartenaireComponent={
        <PartenaireInput index="partenaire" label="Prestataire" value={prestataire} onChange={setPrestataire} />
      }
    />
  );
};

export default Categorie;
