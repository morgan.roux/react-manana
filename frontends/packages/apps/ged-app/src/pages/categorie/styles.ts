import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    actionModalContact: {
      width: '100%',
      display: 'flex',
      justifyContent: 'flex-end',
      marginTop: 16,
    },
  })
);

export default useStyles;
