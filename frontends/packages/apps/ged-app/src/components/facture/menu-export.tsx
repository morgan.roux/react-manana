import { Button, CircularProgress, Fade, Menu, MenuItem, Typography } from '@material-ui/core';
import { SaveAlt } from '@material-ui/icons';
import React, { FC, useState } from 'react';
import { CSVLink } from 'react-csv';
import { exportExcel } from '@lib/common';

export interface FactureExport {
  date: string;
  dateStatut: string;
  journal: string;
  compte: string;
  piece: number;
  libelle: string;
  debit: number;
  credit: number;
  monnaie: string;
}

export interface MenuExportProps {
  facturesToExport: FactureExport[];
  handleExportPdf: () => void;
  loading: boolean;
  handleCloseMenu?: () => void;
}

export const MenuExport: FC<MenuExportProps> = ({ facturesToExport, handleExportPdf, loading, handleCloseMenu }) => {
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    if (facturesToExport.length) setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
    handleCloseMenu && handleCloseMenu();
  };

  const handleExportExcel = () => {
    const dataExport = facturesToExport.map(({ date, journal, compte, piece, libelle, debit, credit, monnaie }) => {
      return [date, journal, compte, piece, libelle, debit, credit, monnaie];
    });
    exportExcel({
      data: [['Date', 'Journal', 'Compte', 'Pièce', 'Libellé', 'Débit', 'Crédit', 'Monnaie'], ...dataExport],
      title: 'Factures',
      filename: 'factures',
    });
  };

  return loading ? (
    <CircularProgress color="secondary" />
  ) : (
    <>
      <div>
        <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
          <SaveAlt style={facturesToExport.length ? undefined : { color: '#8f8f8fed' } } />
          <Typography
            style={{
              marginLeft: '16px',
              textTransform: 'initial',
              fontSize: '16px',
              fontFamily: 'Roboto',
              fontWeight: 400,
              color: facturesToExport.length ? '#212121' :  '#8f8f8fed',
            }}
          >
            Exporter des factures
          </Typography>
        </Button>
        <Menu
          id="fade-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open}
          onClose={handleClose}
          TransitionComponent={Fade}
        >
          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              handleClose();
              handleExportExcel();
            }}
          >
            <Typography variant="inherit" color="primary">
              Excel (.xlsx)
            </Typography>
          </MenuItem>
          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              handleClose();
            }}
          >
            <CSVLink data={facturesToExport || ''} style={{ textDecoration: 'none' }} filename="Factures.csv">
              <Typography variant="inherit" color="primary">
                CSV (.csv)
              </Typography>
            </CSVLink>
          </MenuItem>

          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              handleClose();
              handleExportPdf();
            }}
          >
            <Typography variant="inherit" color="primary">
              PDF (.pdf)
            </Typography>
          </MenuItem>
        </Menu>
      </div>
    </>
  );
};
