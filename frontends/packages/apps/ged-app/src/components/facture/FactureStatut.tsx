import React, { FC } from 'react';
import { OptionSelect } from '@lib/ui-kit';
import { FullDocumentCategorieFragment } from '@lib/common/src/federation';

export interface FactureStatutProps {
  facture?: FullDocumentCategorieFragment;
}

const options = [
  { id: '1', code: 'EN_ATTENTE_APPROBATION', libelle: 'Nouvelle facture', background: '#e9f5fa', color: '#63B8DD' },
  { id: '2', code: 'APPROUVE', libelle: 'Facture validée', background: '#d5eddf', color: '#00A745' },
  { id: '3', code: 'REFUSE', libelle: 'Rejet pharmacie', background: '#fde7e9', color: '#F05365' },
  { id: '4', code: 'REFUSE_COMPTABLE', libelle: 'Rejet comptable', background: '#f8eed6', color: '#FBB104' },
  { id: '5', code: 'APPROUVE_COMPTABLE', libelle: 'Comptabilisée', background: '#f8ee5e', color: '#FBB104' },
  { id: '6', code: 'PAYE', libelle: 'Facture payée', background: '#f8e5f5', color: '#CB48B7' },
];

const getStatutCode = (facture?: FullDocumentCategorieFragment) => {
  return facture?.dernierChangementStatut?.status;
};

export const FactureStatut: FC<FactureStatutProps> = ({ facture }) => {
  const handleChangeEtatFacture = (status?: string) => {};
  return (
    <OptionSelect
      options={options}
      onChange={handleChangeEtatFacture}
      useDefaultStyle={false}
      value={options.find((option) => option.code === getStatutCode(facture))}
      disabled
    />
  );
};
