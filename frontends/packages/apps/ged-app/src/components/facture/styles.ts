import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    exportTableCell: {
      fontSize: 12,
      textAlign: 'center'
    },
    exportTableCellMontant: {
      fontSize: 12,
      textAlign: 'right'
    },
    exportTableLabelMontant: {
      fontSize: 12,
      textAlign: 'right',
      '&.main-MuiTableCell-sizeSmall': {
        padding: '0px',
      },
    },
    noTableCellPadding: {
      '&.main-MuiTableCell-sizeSmall': {
        padding: '0px',
      },
    },
  })
);
