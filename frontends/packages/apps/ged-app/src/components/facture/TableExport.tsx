import React, { FC } from 'react';
import { Table } from '@lib/ui-kit';
import { FactureExport } from './menu-export';
import { Typography } from '@material-ui/core';
import { useStyles } from './styles';

export interface TableExportProps {
  facturesToExport: FactureExport[];
}

export const TableExport: FC<TableExportProps> = ({ facturesToExport }) => {
  const classes = useStyles();

  return (
    <>
      <Table
        search={false}
        data={facturesToExport}
        columns={[
          {
            name: 'date',
            label: 'Date',
            className: classes.noTableCellPadding,
            renderer: (row: FactureExport) => <Typography className={classes.exportTableCell}>{row.date}</Typography>,
          },
          /*{
          name: 'dateStatut',
          label: 'Date Statut',
          renderer: (row: FactureExport) => (
            <Typography className={classes.exportTableCell}>{row.dateStatut}</Typography>
          ),
        },*/
          {
            name: 'journal',
            label: 'Journal',
            className: classes.noTableCellPadding,
            renderer: (row: FactureExport) => (
              <Typography className={classes.exportTableCell}>{row.journal}</Typography>
            ),
          },
          {
            name: 'compte',
            label: 'Compte',
            className: classes.noTableCellPadding,
            renderer: (row: FactureExport) => <Typography className={classes.exportTableCell}>{row.compte}</Typography>,
          },
          {
            name: 'piece',
            label: 'Piece',
            className: classes.noTableCellPadding,
            renderer: (row: FactureExport) => <Typography className={classes.exportTableCell}>{row.piece}</Typography>,
          },
          {
            name: 'libelle',
            label: 'Libellé',
            className: classes.noTableCellPadding,
            renderer: (row: FactureExport) => (
              <Typography className={classes.exportTableCell} style={{ minWidth: 100 }}>
                {row.libelle}
              </Typography>
            ),
          },
          {
            style: { textAlign: 'right' },
            name: 'debit',
            label: <div className={classes.exportTableLabelMontant}>Débit</div>,
            className: classes.noTableCellPadding,
            headerClassName: classes.noTableCellPadding,
            renderer: (row: FactureExport) => (
              <Typography className={classes.exportTableCellMontant}>{row.debit}</Typography>
            ),
          },
          {
            style: { textAlign: 'right' },
            name: 'credit',
            label: <div className={classes.exportTableLabelMontant}>Crédit</div>,
            className: classes.noTableCellPadding,
            headerClassName: classes.noTableCellPadding,
            renderer: (row: FactureExport) => (
              <Typography className={classes.exportTableCellMontant}>{row.credit}</Typography>
            ),
          },
          {
            name: 'monnaie',
            label: 'Monnaie',
            className: classes.noTableCellPadding,
            renderer: (row: FactureExport) => (
              <Typography className={classes.exportTableCell}>{row.monnaie}</Typography>
            ),
          },
        ]}
        rowsTotal={0}
        noTableToolbarSearch
        notablePagination
      />
    </>
  );
};
