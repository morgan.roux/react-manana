import React, { FC } from 'react';
import { CustomSelect } from '@lib/ui-kit';

export interface TypeChargeInputProps {
  sens?: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  label?: string;
  name?: string;
  required?: boolean;
  disabled?: boolean;
}

const listTypeCharges = [
  { id: 'ACHAT', libelle: 'Achat' },
  { id: 'LOCATION', libelle: 'Location' },
  { id: 'FRAIS_BANCAIRE', libelle: 'Fais bancaire' },
  { id: 'AUTRE', libelle: 'Autre' },
];

export const TypeChargeInput: FC<TypeChargeInputProps> = ({ sens, onChange, name, label, required = true, disabled }) => {
  return (
    <CustomSelect
      list={listTypeCharges}
      loading={false}
      error={false}
      index="libelle"
      listId="id"
      label={label ?? 'Choisir le sens'}
      name={name || 'sens'}
      value={sens}
      onChange={onChange}
      required={required}
      withPlaceholder
      placeholder="Choisir le sens"
      disabled={disabled}
    />
  );
};
