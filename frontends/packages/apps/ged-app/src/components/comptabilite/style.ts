import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    buttonJournal: {
      '& .main-MuiButtonBase-root': {
        background: '#FAFAFA'
      },
    }
  })
);
