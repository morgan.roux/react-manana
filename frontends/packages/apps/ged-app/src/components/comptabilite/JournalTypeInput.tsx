import React, { FC } from 'react';
import { CustomSelect } from '@lib/ui-kit';
import { useEfJournalTypeQuery } from '@lib/common/src/federation';
import { Box } from '@material-ui/core';

export interface JournalTypeInputProps {
  label?: string;
  idType?: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  name?: string;
  required?: boolean
}

export const JournalTypeInput: FC<JournalTypeInputProps> = ({ label, idType, onChange, name, required = true }) => {
  const journalTypes = useEfJournalTypeQuery().data?.eFJournalTypes?.nodes || [];

  return journalTypes.length ? (
    <CustomSelect
      list={journalTypes}
      listId="id"
      index="libelle"
      label={label || 'Journal'}
      name={name || 'idType'}
      value={idType}
      onChange={onChange}
      style={{ width: '450px' }}
      required={required}
    />
  ) : (
    <Box>Aucun journal</Box>
  );
};
