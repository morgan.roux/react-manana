import React, { FC } from 'react';
import { CustomSelect } from '@lib/ui-kit';

export interface SensInputProps {
  sens?: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  label?: string;
  name?: string;
  required?: boolean;
  disabled?: boolean;
}

const listSens = [
  { id: 'DEBIT', libelle: 'Débit' },
  { id: 'CREDIT', libelle: 'Crédit' },
];

export const SensInput: FC<SensInputProps> = ({ sens, onChange, name, label, required = true, disabled }) => {
  return (
    <CustomSelect
      list={listSens}
      loading={false}
      error={false}
      index="libelle"
      listId="id"
      label={label ?? 'Choisir le sens'}
      name={name || 'sens'}
      value={sens}
      onChange={onChange}
      required={required}
      withPlaceholder
      placeholder="Choisir le sens"
      disabled={disabled}
    />
  );
};
