import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import { useEfJournalTypeQuery } from '@lib/common/src/federation';
import { CustomSelectMenu } from '@lib/ui-kit';
import filter_alt from '../../assets/img/filter_alt.svg';
import { useStyles } from './style';

export interface JournalTypeFilterProps {
  onFilter: (idJournalType?: string) => void;
  label?: string;
}

export const JournalTypeFilter: FC<JournalTypeFilterProps> = ({ label, onFilter }) => {
  const journalTypes = useEfJournalTypeQuery().data?.eFJournalTypes?.nodes || [];

  const classes = useStyles();
  return (
    <Box display="flex" flexDirection="row" alignItems="center" px={2}>
      <CustomSelectMenu
        buttonLabel={label ?? 'Journal'}
        listFilters={[
          {
            id: 'Tous',
            name: 'Tous',
          },
          ...journalTypes.map((type) => ({ id: type.id, name: type.libelle })),
        ]}
        handleClickItem={onFilter}
        iconFilter={<img src={filter_alt} />}
        className={classes.buttonJournal}
      />
    </Box>
  );
};
