import React, { FC, useState } from 'react';
import { CustomSelect } from '@lib/ui-kit';
import { OrigineInput } from '@lib/common';

export interface TypeInputProps {
  value?: { type?: string; origine?: any; origineAssocie?: any };
  onChange: (name: string, value?: any) => void;
  label?: string;
}

const types = [
  { id: 'CLIENT', libelle: 'Clients' },
  { id: 'FOURNISSEUR', libelle: 'Fournisseurs' },
  { id: 'GESTION', libelle: 'Gestion' },
  { id: 'BILAN', libelle: 'Bilan' },
  { id: 'AUTRE', libelle: 'Autres Tiers' },
];

export const TypeInput: FC<TypeInputProps> = ({ value, onChange, label }) => {
  return (
    <>
     {/* <CustomSelect
        list={types}
        loading={false}
        error={false}
        index="libelle"
        listId="id"
        label={label || 'Choisir le type'}
        value={value?.type}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChange('type', e.target.value)}
        required
        withPlaceholder
        placeholder="Choisir le type"
     />*/}

     
        <OrigineInput
          onChangeOrigine={(origine: any) => {
            onChange('origine', origine);
          }}
          origine={value?.origine}
          origineAssocie={value?.origineAssocie}
          onChangeOrigineAssocie={(origineAssocie) => {
            onChange('origineAssocie', origineAssocie);
          }}
          enabledCodes={['LABORATOIRE','PRESTATAIRE_SERVICE','CONFRERE']}
        />
    </>
  );
};
