import React, { FC } from 'react';
import { CustomSelect } from '@lib/ui-kit';

export interface SuiviCompteInputProps {
  suivi?: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  label?: string;
  name?: string;
  required?: string;
}

const listSuiviCompte = [
  { id: 'AUCUN', libelle: 'Aucun' },
  { id: 'BANQUE', libelle: 'Banque' },
];

export const SuiviCompteInput: FC<SuiviCompteInputProps> = ({ suivi, onChange, name, label, required = true }) => {
  return (
    <CustomSelect
      list={listSuiviCompte}
      loading={false}
      error={false}
      index="libelle"
      listId="id"
      label={label || 'Suivi de compte'}
      name={name || 'suivi'}
      value={suivi || 'AUCUN'}
      onChange={onChange}
      required={required}
    />
  );
};
