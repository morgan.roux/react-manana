import { createContext } from 'react';
import { ThemeName } from './config';

type Context = [ThemeName, (newTheme: ThemeName | ((previousTheme: ThemeName) => ThemeName)) => void];

const ThemeContext = createContext<Context>(['angel', () => {}]);

export default ThemeContext;
