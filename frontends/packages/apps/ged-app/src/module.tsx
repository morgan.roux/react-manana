import React from 'react';
import { SmallLoading } from '@lib/ui-kit';
import loadable from '@loadable/component';
import EspaceDocumentaire from './assets/img/ged.png';
import EspaceFacturation from './assets/img/espace_facture.png';

import { ModuleDefinition } from '@lib/common';
import { Category, Receipt } from '@material-ui/icons';
import moment from 'moment';
moment.locale('fr');

const EspaceDocumentairePage = loadable(() => import('./pages/ged'), {
  fallback: <SmallLoading />,
});

const EspaceFacturationPage = loadable(() => import('./pages/facturation'), {
  fallback: <SmallLoading />,
});

const Categorie = loadable(() => import('./pages/categorie/Categorie'), {
  fallback: <SmallLoading />,
});

const CompteComptable = loadable(() => import('./pages/comptabilites/CompteComptable/CompteComptable'), {
  fallback: <SmallLoading />,
});



const Comptabilite = loadable(() => import('./pages/ComptabiliteTab/Comptabilite'), {
  fallback: <SmallLoading />,
});

const NewGedForm = loadable(() => import('./pages/newGed/facture-form'), {
  fallback: <SmallLoading />,
});

const gedModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_GED',
      location: 'ACCUEIL',
      name: 'Espace documentaire',
      to: '/espace-documentaire',
      icon: EspaceDocumentaire,
      activationParameters: {
        groupement: '0314',
        pharmacie: '0717',
      },
      preferredOrder: 180,
      authorize: {
        rolesDenied: ['PRTSERVICE'],
      },
    },

    {
      id: 'FEATURE_FACTURATION',
      location: 'ACCUEIL',
      name: 'Espace facturation',
      to: '/espace-facturation',
      icon: EspaceFacturation,
      activationParameters: {
        groupement: '0331',
        pharmacie: '0751',
      },
      preferredOrder: 170,
    },

    {
      id: 'FEATURE_PARAMETRE_GED',
      location: 'PARAMETRE',
      name: 'Espace documentaire',
      to: '/categorie',
      icon: <Category />,
      preferredOrder: 170,
      authorize: {
        roles: ['SUPADM', 'GRPADM', 'PRMTIT'],
      },
    },
    {
      id: 'FEATURE_COMPTE_COMPTABLE',
      location: 'PARAMETRE',
      name: 'Comptabilité',
      to: '/comptabilite',
      icon: <Receipt />,
      preferredOrder: 170,
      authorize: {
        roles: ['SUPADM', 'GRPADM', 'PRMTIT', 'PRTSERVICE'],
      },
    },
  ],
  routes: [
    {
      attachTo: 'MAIN',
      path: '/espace-documentaire',
      component: EspaceDocumentairePage,
      mobileOptions: {
        topBar: {
          title: 'Espace documentaire',
          withBackBtn: true,
          withTopMargin: true,
          //onGoBack={onRequestGoBack}
        },
      },
    },
    {
      attachTo: 'MAIN',
      path: '/espace-facturation/form',
      component: NewGedForm,
    },

    {
      attachTo: 'MAIN',
      path: '/espace-facturation',
      component: EspaceFacturationPage,
      mobileOptions: {
        topBar: {
          title: 'Espace facturation',
          withBackBtn: true,
          withTopMargin: true,
          //onGoBack={onRequestGoBack}
        },
      },
    },

    {
      attachTo: 'PARAMETRE',
      path: '/categorie',
      component: Categorie,
    },
    {
      attachTo: 'PARAMETRE',
      path: '/comptabilite/:tab?',
      component: Comptabilite,
    },
    {
      attachTo: 'PARAMETRE',
      path: '/compte-comptable',
      component: CompteComptable,
    }
  ],
};

export default gedModule;
