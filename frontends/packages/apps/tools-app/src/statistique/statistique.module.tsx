import React from 'react';
import { Equalizer } from 'mdi-material-ui';
import loadable from '@loadable/component';
import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';
import StatistiqueBannerButton from './components/StatistiqueBannerButton/StatistiqueBannerButton';

const PilotagePage = loadable(() => import('./pages/Pilotage'), {
  fallback: <SmallLoading />,
});

const activationParameters = { groupement: '0313', pharmacie: '0716' };

const statistiqueModule: ModuleDefinition = {
  features: [],

  routes: [
    {
      attachTo: 'MAIN',
      path: '/pilotage-statistiques/:codeItem?',
      component: PilotagePage,
      activationParameters,
    },
  ],
};

export default statistiqueModule;
