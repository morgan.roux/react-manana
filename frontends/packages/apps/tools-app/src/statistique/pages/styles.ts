import { createStyles, makeStyles } from '@material-ui/core';
const useStyles = makeStyles(() =>
  createStyles({
    selectUserBox: {
      paddingRight: 16,
      paddingLeft: 24,
      minWidth: 242,
    }
  }),
);

export default useStyles;
