import { IconButton } from '@material-ui/core';
import { Equalizer } from 'mdi-material-ui';
import React, { FC } from 'react';
import { useHistory } from 'react-router';
import { useStyles } from './styles';

const StatistiqueBannerButton: FC<{}> = () => {
  const classes = useStyles({});
  const history = useHistory();

  return (
    <IconButton className={classes.iconBtn} onClick={() => history.push(`/pilotage-statistiques`)}>
      <Equalizer />
    </IconButton>
  );
};

export default StatistiqueBannerButton;
