import { Box, FormControlLabel } from "@material-ui/core"
import { Stop } from "@material-ui/icons"
import React, { FC } from "react"


export interface ChartLegendItem {
    id?: string;
    libelle: string;
    pilotageCouleur: string;
}

interface ChartLegendProps {
    items?: ChartLegendItem[]
}


const ChartLegend: FC<ChartLegendProps> = ({ items = [] }) => {
    return (
        <Box display="flex" width={1} justifyContent="center">
            <Box textAlign="center" width={1} maxWidth={600}>
                {
                    items.map((item, index) => <FormControlLabel
                        key={item.id || index}
                        style={{
                            margin: 10
                        }}
                        control={<Stop style={{ color: item.pilotageCouleur }} />} name="checkedH"
                        label={item.libelle}
                    />)
                }

            </Box>
        </Box>
    )

}

export default ChartLegend