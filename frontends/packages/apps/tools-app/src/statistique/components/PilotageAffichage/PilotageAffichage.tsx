import { Box, Typography } from '@material-ui/core';
import {
  Chart,
  ChartCategoryAxis,
  ChartCategoryAxisItem,
  ChartLegend,
  ChartSeries,
  ChartSeriesItem,
  ChartSeriesLabels,
} from '@progress/kendo-react-charts';
import React, { FC, ReactNode } from 'react';
import { Statistique } from '../types';
import 'hammerjs';
import CustomChartLegend, { ChartLegendItem as CustomChartLegendItem } from '../ChartLegend';

import useStyles from './styles';

export interface RapportAffichageProps {
  type: string;
  SelectionComponent?: ReactNode;
  ComparaisonComponent?: ReactNode;
  data?: Statistique;
  legendItems?: CustomChartLegendItem[];
}

const RapportAffichage: FC<RapportAffichageProps> = ({
  type,
  SelectionComponent,
  data,
  ComparaisonComponent,
  legendItems,
}) => {
  const classes = useStyles();
  return (
    <Box>
      <Box display="flex" flexDirection="row" pb={2}>
        <Typography className={classes.subtitle}>{data?.titre}</Typography>
        <Box className={classes.selectionBox} justifyContent="center">
          {SelectionComponent}
        </Box>
        <Box className={classes.comparaisonBox}>{ComparaisonComponent}</Box>
      </Box>

      {(type === 'bar' || type === 'column') && (
        <Chart style={{ fontSize: 16 }}>
          <ChartLegend visible={false} position="bottom" orientation="horizontal" />
          <ChartCategoryAxis>
            <ChartCategoryAxisItem categories={data?.categorie} />
          </ChartCategoryAxis>
          <ChartSeries>
            {data?.series?.map((serie, index) => {
              return (
                <ChartSeriesItem
                  key={`serie${index}`}
                  type={type}
                  gap={type === 'bar' ? 1 : 5}
                  stack={type === 'column'}
                  spacing={0.25}
                  name={serie.nom}
                  data={serie.valeur}
                  color={serie.color}
                />
              );
            })}
          </ChartSeries>
        </Chart>
      )}
      {type === 'donut' && (
        <Chart>
          <ChartSeries>
            <ChartSeriesItem
              type="donut"
              data={data?.items}
              categoryField="nom"
              field="valeur"
              // color={(e: any) => e.dataItem.color}
            >
              <ChartSeriesLabels color="#fff" background="none" content={(e: any) => `${(e?.value).toFixed(0)}%`} />
            </ChartSeriesItem>
          </ChartSeries>
          <ChartLegend visible={false} position="bottom" orientation="horizontal" />
        </Chart>
      )}
      <Box pt={2}>
        <CustomChartLegend items={legendItems} />
      </Box>
    </Box>
  );
};

export default RapportAffichage;
