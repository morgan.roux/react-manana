import { createStyles, makeStyles } from '@material-ui/core';
const useStyles = makeStyles(() =>
  createStyles({
    subtitle: {
      fontFamily: 'Roboto',
      textAlign: 'left',
      color: '#212121',
      fontSize: 18,
      fontWeight: '500' as any,
      width: '100%',
    },
    selectionBox: {
      display: 'flex',
      width: '100%',
    },
    comparaisonBox: {
      display: 'flex',
      justifyContent: 'flex-end',
      width: '100%',
    },
  }),
);

export default useStyles;
