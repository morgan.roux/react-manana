import { createStyles, Theme, makeStyles, lighten } from '@material-ui/core';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    coursBox: {
      width: '100%',
      paddingLeft: 8,
      paddingRight: 8,
      paddingTop: 16,
      paddingBottom: 16,
      borderTop: 'solid 1px #e0e0e0',
    },
    subtitle: {
      fontFamily: 'Roboto',
      textAlign: 'left',
      color: '#27272F',
      fontSize: 20,
      fontWeight: 'bold',
      width: '100%',
      paddingTop: 8,
      paddingBottom: 16,
      marginTop: 24,
      marginBottom: 16,
    },
    subtitle2: {
      fontFamily: 'Roboto',
      textAlign: 'center',
      color: '#FFFFFF',
      fontSize: 20,
      fontWeight: 'bold',
      width: '100%',
      paddingTop: 8,
      paddingBottom: 16,
    },
    subtitle3: {
      fontFamily: 'Roboto',
      textAlign: 'left',
      color: '#212121',
      fontSize: 18,
      fontWeight: '500' as any,
      width: '100%',
    },
    selectBox: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    itemBox: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    periodeBox: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
    },
    buttonPeriode: {
      paddingLeft: 16,
      paddingRight: 16,
      paddingTop: 8,
      paddingBottom: 8,
    },
    container: {
      paddingLeft: 16,
      paddingRight: 32,
      paddingTop: 16,
      paddingBottom: 16,
      overflow: 'auto',
      height: '85%',
    },
    topBox: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      background: lighten(theme.palette.primary.main, 0.1),
      paddingTop: 16,
      paddingBottom: 8,
    },
    arrowBox: {
      paddingLeft: 16,
      cursor: 'pointer',
    },
    arrow: {
      color: '#FFF',
    },
    activeButton: {
      backgroundColor: theme.palette.primary.main,
      textTransform: 'none',
      color: '#ffffff',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 16,
      height: 50,
      '& .root': {
        padding: 0,
      },
    },
    defaultButton: {
      textTransform: 'none',
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 16,
      color: '#212121 !important',
      boxShadow: 'unset',
      background: '#F8F8F8 !important',
      height: 50,
      '& .root': {
        padding: 0,
      },
    },
    select: {
      minWidth: 150,
      paddingLeft: 24,
    },
  })
);

export default useStyles;
