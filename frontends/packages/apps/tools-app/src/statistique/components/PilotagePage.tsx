import { Box, Typography } from '@material-ui/core';
import React, { ChangeEvent, FC, ReactNode, useEffect, useState } from 'react';
import RapportAffichage from './PilotageAffichage/PilotageAffichage';
import { Rapport, Statistique, StatistiqueComparaison, StatistiqueType } from './types';
import useStyles from './styles';
import { SmallLoading, NewCustomButton as CustomButton, CustomSelect, ErrorPage } from '@lib/ui-kit';
import { ArrowBack } from '@material-ui/icons';

export interface RapportData {
  loading: boolean;
  error: boolean;
  data: Rapport;
}

export interface PeriodeItem {
  // code: string;
  ordre: number;
  libelle: string;
}

export interface PeriodeList {
  anneePrecedentes?: PeriodeItem[];
  mois?: PeriodeItem[];
  moisPrecedents?: PeriodeItem[];
  semaines?: PeriodeItem[];
  semainePrecedentes?: PeriodeItem[];
}

export interface Periode {
  cours?: number;
  compare?: number;
  type?: number;
}

export interface Option {
  code: string;
  libelle: string;
}

export interface ItemProps {
  loading?: boolean;
  error?: boolean;
  data?: Option[];
}

export interface ChangePeriodeProps {
  type: string;
  periode: string;
  ordre: number;
}

const calculMois = (listMois: PeriodeItem[], mois: number) => {
  const result = listMois.filter((value) => value.ordre === mois).map((value) => value.libelle);
  if (result && result.length > 0) {
    return result[0];
  }
  return '';
};

const calculDataComparaison = (data?: StatistiqueComparaison[], titre?: string): Statistique | undefined => {
  return data?.reduce(
    (previous: Statistique, current: StatistiqueComparaison) => {
      return {
        series:
          current.importances?.map((importance, index) => {
            return {
              nom: importance.libelle,
              valeur: [
                ...(previous?.series && previous?.series.length > index ? previous?.series[index].valeur : []),
                importance.pilotageOccurences,
              ],
              color: importance.pilotageCouleur,
            };
          }) || previous.series,
        categorie: [...(previous.categorie || []), current.categorie],
        items: [],
        titre,
      };
    },
    {
      series: [],
      categorie: [],
      items: [],
      titre,
    }
  );
};

const calculDataType = (data?: StatistiqueType[], titre?: string): Statistique | undefined => {
  return data?.reduce(
    (previous: Statistique, current: StatistiqueType) => {
      return {
        series:
          current.statuts?.map((statut, index) => {
            return {
              nom: statut.libelle,
              valeur: [
                ...(previous?.series && previous?.series.length > index ? previous?.series[index].valeur : []),
                statut.pilotageOccurences,
              ],
              color: statut.pilotageCouleur,
            };
          }) || previous.series,
        categorie: [...(previous.categorie || []), current.libelle],
        items: [
          ...(previous.items || []),
          {
            nom: current.libelle,
            valeur: current.pilotagePercentage,
            color: current.pilotageCouleur,
          },
        ],
        titre,
      };
    },
    {
      series: [],
      categorie: [],
      items: [],
      titre,
    }
  );
};

const afficherType = (data?: StatistiqueType[]) => {
  return data?.some((item) => item.pilotagePercentage !== 0);
};

export interface RapportPageProps {
  titre: string;
  rapport?: RapportData;
  selectedPeriode: string;
  selectedItem?: string;
  periodes: PeriodeList;
  annee?: Periode;
  mois?: Periode;
  semaine?: Periode;
  items?: ItemProps;

  SelectUserComponent?: ReactNode;

  onChangePeriode?: (change: ChangePeriodeProps) => void;
  onClickPeriode?: (periode: string) => void;
  onChangeItem?: (periode: string) => void;
  onGoBack?: () => void;
}

const RapportPage: FC<RapportPageProps> = ({
  titre,
  rapport,
  selectedPeriode,
  periodes,
  annee,
  mois,
  semaine,
  SelectUserComponent,
  items,
  selectedItem,

  onChangePeriode,
  onClickPeriode,
  onChangeItem,
  onGoBack,
}) => {
  const classes = useStyles();

  const [comparaison, setComparaison] = useState<boolean>(false);
  const [dataCours, setDataCours] = useState<Statistique | undefined>(
    calculDataComparaison(rapport?.data?.cours, rapport?.data?.titreCours)
  );
  const [dataComparaison, setDataComparaison] = useState<Statistique | undefined>(
    calculDataComparaison(rapport?.data?.compare, rapport?.data?.titreCompare)
  );
  const [dataType, setDataType] = useState<Statistique | undefined>(
    calculDataType(rapport?.data?.type, rapport?.data?.titreType)
  );

  useEffect(() => {
    setDataCours(calculDataComparaison(rapport?.data?.cours, rapport?.data?.titreCours));
    setDataComparaison(calculDataComparaison(rapport?.data?.compare, rapport?.data?.titreCompare));
    setDataType(calculDataType(rapport?.data?.type, rapport?.data?.titreType));
  }, [rapport]);

  const toggleComparaison = () => {
    setComparaison((prev) => !prev);
  };
  // console.log(
  //   '----------------------calcul evolutions',
  //   calculDataComparaison(rapport?.data?.cours, rapport?.data?.titreCours),
  // );
  // console.log(
  //   '----------------------calcul item',
  //   calculDataType(rapport?.data?.type, rapport?.data?.titreType),
  // );

  if (rapport?.loading) {
    return <SmallLoading />;
  }

  if (rapport?.error) {
    return <ErrorPage />;
  }

  return (
    <>
      <Box className={classes.topBox}>
        <Box onClick={onGoBack} className={classes.arrowBox}>
          <ArrowBack className={classes.arrow} />
        </Box>
        <Box className={classes.subtitle2}>{titre}</Box>
      </Box>
      <Box className={classes.container}>
        <Box className={classes.selectBox}>
          <Box className={classes.itemBox}>
            {/* <CustomSelect
            list={items?.data}
            index="libelle"
            listId="code"
            value={selectedItem}
            onChange={(event: ChangeEvent<HTMLInputElement>) => {
              onChangeItem && onChangeItem(event.target.value);
            }}
          /> */}
            {items?.data?.map((item) => {
              return (
                <Box px={1} key={`item${item.code}`}>
                  <CustomButton
                    className={selectedItem === item.code ? classes.activeButton : classes.defaultButton}
                    onClick={(event) => {
                      event.preventDefault();
                      event.stopPropagation();
                      onChangeItem && onChangeItem(item.code);
                    }}
                  >
                    <Box className={classes.buttonPeriode}>{item.libelle}</Box>
                  </CustomButton>
                </Box>
              );
            })}
          </Box>
          <Box className={classes.periodeBox}>
            <Box px={1}>
              <CustomButton
                className={selectedPeriode === 'SEMAINE' ? classes.activeButton : classes.defaultButton}
                onClick={(event) => {
                  event.preventDefault();
                  event.stopPropagation();
                  onClickPeriode && onClickPeriode('SEMAINE');
                }}
              >
                <Box className={classes.buttonPeriode}>Semaine</Box>
              </CustomButton>
            </Box>
            <Box px={1}>
              <CustomButton
                className={selectedPeriode === 'MOIS' ? classes.activeButton : classes.defaultButton}
                color="default"
                onClick={(event) => {
                  event.preventDefault();
                  event.stopPropagation();
                  onClickPeriode && onClickPeriode('MOIS');
                }}
              >
                <Box className={classes.buttonPeriode}>Mois</Box>
              </CustomButton>
            </Box>
            <Box px={1}>
              <CustomButton
                className={selectedPeriode === 'ANNEE' ? classes.activeButton : classes.defaultButton}
                onClick={(event) => {
                  event.preventDefault();
                  event.stopPropagation();
                  onClickPeriode && onClickPeriode('ANNEE');
                }}
              >
                <Box className={classes.buttonPeriode}>Année</Box>
              </CustomButton>
            </Box>
            {SelectUserComponent}
          </Box>
        </Box>
        <Box className={classes.coursBox}>
          <RapportAffichage
            legendItems={
              rapport?.data?.cours && rapport?.data?.cours?.length > 0 ? rapport?.data?.cours[0].importances : []
            }
            data={dataCours}
            type="column"
            SelectionComponent={
              <Box display="flex" flexDirection="row" alignItems="flex-end">
                {selectedPeriode === 'SEMAINE' && (
                  <Typography className={classes.buttonPeriode}>{`S${(semaine?.cours || 0) < 10 ? '0' : ''}${
                    semaine?.cours
                  }`}</Typography>
                )}
                {(selectedPeriode === 'SEMAINE' || selectedPeriode === 'MOIS') && (
                  <Typography className={classes.buttonPeriode}>
                    {calculMois(periodes.mois || [], mois?.cours || 0)}
                  </Typography>
                )}

                <Typography className={classes.buttonPeriode}>{annee?.cours}</Typography>
              </Box>
            }
            ComparaisonComponent={
              <Typography
                color="secondary"
                onClick={(event) => {
                  event.preventDefault();
                  event.stopPropagation();
                  toggleComparaison();
                }}
                style={{
                  cursor: 'pointer',
                }}
              >
                {comparaison
                  ? `Annuler la comparaison`
                  : `Comparer avec ${
                      selectedPeriode === 'SEMAINE'
                        ? 'la semaine précedente'
                        : selectedPeriode === 'MOIS'
                        ? 'le mois précedent'
                        : selectedPeriode === 'ANNEE'
                        ? "l'année dérnière"
                        : ''
                    }`}
              </Typography>
            }
          />
        </Box>
        {comparaison && (
          <Box className={classes.coursBox}>
            <RapportAffichage
              legendItems={
                rapport?.data?.compare && rapport?.data?.compare?.length > 0
                  ? rapport?.data?.compare[0].importances
                  : []
              }
              data={dataComparaison}
              type="column"
              SelectionComponent={
                <Box display="flex" flexDirection="row" alignItems="center">
                  {/* <Box className={classes.buttonPeriode}>
                  <CustomSelect
                    list={
                      selectedPeriode === 'SEMAINE'
                        ? periodes.semainePrecedentes
                        : selectedPeriode === 'MOIS'
                        ? periodes.moisPrecedents
                        : selectedPeriode === 'ANNEE'
                        ? periodes.anneePrecedentes
                        : []
                    }
                    index="libelle"
                    listId="ordre"
                    label={selectedPeriode.toLowerCase()}
                    variant="standard"
                    value={
                      selectedPeriode === 'SEMAINE'
                        ? semaine?.compare
                        : selectedPeriode === 'MOIS'
                        ? mois?.compare
                        : selectedPeriode === 'ANNEE'
                        ? annee?.compare
                        : undefined
                    }
                    onChange={(event: ChangeEvent<HTMLInputElement>) => {
                      onChangePeriode &&
                        onChangePeriode({
                          type: 'compare',
                          periode: selectedPeriode,
                          ordre: parseInt(event.target.value),
                        });
                    }}
                  />
                </Box> */}
                  {selectedPeriode === 'SEMAINE' && (
                    <Box className={classes.select}>
                      <CustomSelect
                        list={periodes.semaines}
                        index="libelle"
                        listId="ordre"
                        label="Semaine"
                        value={semaine?.compare}
                        onChange={(event: ChangeEvent<HTMLInputElement>) => {
                          onChangePeriode &&
                            onChangePeriode({
                              type: 'compare',
                              periode: 'SEMAINE',
                              ordre: parseInt(event.target.value),
                            });
                        }}
                      />
                    </Box>
                  )}
                  {(selectedPeriode === 'SEMAINE' || selectedPeriode === 'MOIS') && (
                    <Box className={classes.select}>
                      <CustomSelect
                        list={periodes.mois}
                        index="libelle"
                        listId="ordre"
                        label="Mois"
                        disabled={selectedPeriode === 'SEMAINE'}
                        value={mois?.compare}
                        onChange={(event: ChangeEvent<HTMLInputElement>) => {
                          onChangePeriode &&
                            onChangePeriode({
                              type: 'compare',
                              periode: 'MOIS',
                              ordre: parseInt(event.target.value),
                            });
                        }}
                      />
                    </Box>
                  )}
                  <Box className={classes.select}>
                    <CustomSelect
                      list={periodes.anneePrecedentes}
                      index="libelle"
                      listId="ordre"
                      label="Année"
                      value={annee?.compare}
                      onChange={(event: ChangeEvent<HTMLInputElement>) => {
                        onChangePeriode &&
                          onChangePeriode({
                            type: 'compare',
                            periode: 'ANNEE',
                            ordre: parseInt(event.target.value),
                          });
                      }}
                    />
                  </Box>
                </Box>
              }
            />
          </Box>
        )}
        <Box className={classes.coursBox}>
          {afficherType(rapport?.data?.type) ? (
            <RapportAffichage
              legendItems={(rapport?.data?.type?.length || 0) === 0 ? [] : rapport?.data.type || []}
              data={dataType}
              type="donut"
              SelectionComponent={
                <Box display="flex" flexDirection="row" alignItems="center">
                  {/* {selectedPeriode === 'SEMAINE' && (
                <Box className={classes.buttonPeriode}>
                  <CustomSelect
                    list={periodes.semaines}
                    index="libelle"
                    listId="ordre"
                    label="Semaine"
                    variant="standard"
                    value={semaine?.type}
                    onChange={(event: ChangeEvent<HTMLInputElement>) => {
                      onChangePeriode &&
                        onChangePeriode({
                          type: 'type',
                          periode: 'SEMAINE',
                          ordre: parseInt(event.target.value),
                        });
                    }}
                  />
                </Box>
              )}
              {(selectedPeriode === 'SEMAINE' || selectedPeriode === 'MOIS') && (
                <Box className={classes.buttonPeriode}>
                  <CustomSelect
                    list={periodes.mois}
                    index="libelle"
                    listId="ordre"
                    label="Mois"
                    variant="standard"
                    value={mois?.type}
                    onChange={(event: ChangeEvent<HTMLInputElement>) => {
                      onChangePeriode &&
                        onChangePeriode({
                          type: 'type',
                          periode: 'MOIS',
                          ordre: parseInt(event.target.value),
                        });
                    }}
                  />
                </Box>
              )} */}
                  {selectedPeriode === 'SEMAINE' && (
                    <Typography className={classes.buttonPeriode}>{`S${(semaine?.cours || 0) < 10 ? '0' : ''}${
                      semaine?.cours
                    }`}</Typography>
                  )}
                  {(selectedPeriode === 'SEMAINE' || selectedPeriode === 'MOIS') && (
                    <Typography className={classes.buttonPeriode}>
                      {calculMois(periodes.mois || [], mois?.cours || 0)}
                    </Typography>
                  )}
                  <Typography className={classes.buttonPeriode}>{annee?.cours}</Typography>
                </Box>
              }
            />
          ) : (
            <Typography className={classes.subtitle3}>Aucun type</Typography>
          )}
        </Box>
        <Box className={classes.coursBox}>
          <RapportAffichage
            legendItems={rapport?.data?.type && rapport?.data?.type?.length > 0 ? rapport?.data?.type[0].statuts : []}
            data={calculDataType(rapport?.data?.type, rapport?.data?.titreHorizontal)}
            type="bar"
          />
        </Box>
        <Box> </Box>
      </Box>
    </>
  );
};

export default RapportPage;
