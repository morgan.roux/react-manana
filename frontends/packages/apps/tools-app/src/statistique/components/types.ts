export interface Serie {
  nom: string;
  valeur: number[];
  color: string;
}

export interface SerieItem {
  nom: string;
  valeur: number;
  color: string;
}

export interface PilotageImportance {
  id?: string;
  libelle: string;
  pilotageCouleur: string;
  pilotageOccurences: number;
}

export interface StatistiqueComparaison {
  categorie: string;
  importances?: PilotageImportance[];
}

export interface PilotageStatut {
  id?: string;
  libelle: string;
  pilotageCouleur: string;
  pilotageOccurences: number;
}

export interface StatistiqueType {
  id?: string;
  code: string;
  libelle: string;
  pilotageCouleur: string;
  pilotagePercentage: number;
  statuts?: PilotageStatut[];
}

export interface Statistique {
  series?: Serie[];
  categorie?: string[];
  items?: SerieItem[];
  titre?: string;
}

export interface Rapport {
  cours?: StatistiqueComparaison[];
  compare?: StatistiqueComparaison[];
  type?: StatistiqueType[];
  titreCours?: string;
  titreCompare?: string;
  titreType?: string;
  titreHorizontal?: string;
}
