import { useApplicationContext } from '@lib/common';
import {
  DqFicheType,
  useGet_Fiche_Types_With_Item_Scoring_OrdreQuery,
  useUpdate_Item_Scoring_For_All_UsersLazyQuery,
} from '@lib/common/src/federation';
import { CardTheme, ErrorPage, NoItemContentImage, SmallLoading } from '@lib/ui-kit';
import { Box, CircularProgress, IconButton, Typography } from '@material-ui/core';
import { Cached, Edit } from '@material-ui/icons';
import React, { FC, useState } from 'react';
import { Helmet } from 'react-helmet';
import ItemScoringForm from '../../components/ItemScoringForm/ItemScoringForm';
import useSaveOneItemScoringPersonnalisation from '../../utils/useSaveOneItemScoringPersonnalisation';
import { useStyles } from './style';

const ParametreItemScoring: FC<{}> = () => {
  const classes = useStyles({});

  const { currentPharmacie: pharmacie, federation, notify } = useApplicationContext();
  // const isTitulairePharmacie = user?.role?.code === TITULAIRE_PHARMACIE;
  const { loading, error, data, refetch } = useGet_Fiche_Types_With_Item_Scoring_OrdreQuery({
    client: federation,
    variables: {
      idPharmacie: pharmacie.id,
    },
  });
  const [updateItemScoring, updatingItemScoring] = useUpdate_Item_Scoring_For_All_UsersLazyQuery({
    client: federation,
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'Mise à jour des scores terminé',
      });
    },
  });
  const [savePersonnalisation, savingPersonnalisation] = useSaveOneItemScoringPersonnalisation();

  const [openEditModal, setOpenEditModal] = useState<boolean>(false);
  const [ficheTypeToEdit, setFicheTypeToEdit] = useState<DqFicheType>();
  const [ordre, setOrdre] = useState<number>();

  const handleUpdateItemScoring = () => {
    updateItemScoring();
  };

  const handleSavePersonnalisation = () => {
    if (ordre && ficheTypeToEdit?.id) {
      savePersonnalisation(
        {
          ordre,
          idTypeAssocie: ficheTypeToEdit?.id,
          type: 'DQ_FICHE_TYPE',
        },
        ficheTypeToEdit?.itemScoringIdPersonnalisation as any
      ).then(() => {
        refetch();
        setOpenEditModal(false);
      });
    }
  };

  if (loading) return <SmallLoading />;

  if (error) {
    return <ErrorPage />;
  }
  return (
    <>
      <Helmet>
        <title>Score des Fiches d'Incidents</title>
      </Helmet>
      <ItemScoringForm
        open={openEditModal}
        setOpen={setOpenEditModal}
        libelle={ficheTypeToEdit?.libelle || ''}
        ordre={ordre}
        setOrdre={setOrdre}
        saving={savingPersonnalisation.loading || false}
        onSubmit={handleSavePersonnalisation}
      />
      {data?.dQFicheTypes.nodes.length || 0 > 0 ? (
        <Box>
          <div className={classes.headerContent}>
            <Typography>Types des fiches d'incident</Typography>

            {updatingItemScoring.loading ? (
              <CircularProgress style={{ color: '#FFF' }} className={classes.iconReload} />
            ) : (
              <IconButton
                onClick={handleUpdateItemScoring}
                disabled={updatingItemScoring.loading}
                className={classes.iconReload}
              >
                <Cached color={updatingItemScoring.loading ? 'disabled' : undefined} />
              </IconButton>
            )}
          </div>
          <Box style={{ textAlign: 'left' }}>
            {[...(data?.dQFicheTypes?.nodes || [])]
              .sort((a, b) => (a?.itemScoringOrdre || 0) - (b?.itemScoringOrdre || 0))
              .map((item: any) => {
                return (
                  <CardTheme
                    applyMinHeight={false}
                    key={item.id}
                    title={item.libelle}
                    littleComment={`Score : ${item.itemScoringOrdre ? `${item.itemScoringOrdre}` : '-'}`}
                    setCurrentId={console.log}
                    onClick={console.log}
                    moreOptions={[
                      {
                        menuItemLabel: {
                          title: `Modifier le score`,
                          icon: <Edit />,
                        },
                        onClick: (event) => {
                          event.preventDefault();
                          event.stopPropagation();
                          setFicheTypeToEdit(item);
                          setOpenEditModal(true);
                          setOrdre(item.itemScoringOrdre || undefined);
                        },
                      },
                    ]}
                  />
                );
              })}
          </Box>
        </Box>
      ) : (
        <NoItemContentImage title="Aucun type des fiches d'incident" subtitle="" />
      )}
    </>
  );
};

export default ParametreItemScoring;
