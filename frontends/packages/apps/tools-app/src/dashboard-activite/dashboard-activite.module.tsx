import React from 'react';
import loadable from '@loadable/component';
import { ModuleDefinition } from '@lib/common';
import { TableChart, Grade } from '@material-ui/icons';
import { SmallLoading } from '@lib/ui-kit';

const DashboardPage = loadable(() => import('./pages/dashboard/Dashboard'), {
  fallback: <SmallLoading />,
});

const ParametrePage = loadable(() => import('./pages/parametre-item-scoring/ParametreItemScoring'), {
  fallback: <SmallLoading />,
});

const dashboardActiviteModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_ITEM_SCORING',
      location: 'PARAMETRE',
      name: "Score des Fiches d'Incidents",
      to: '/dashboard-item-scoring',
      icon: <Grade />,
      preferredOrder: 240,
      authorize: {
        roles: ['SUPADM', 'GRPADM', 'PRMTIT'],
        rolesDenied: ['PRTSERVICE']
      },
    },
    {
      id: 'FEATURE_OUTIL_DASHBORD',
      location: 'FAVORITE',
      to: '/dashboard-activite',
      name: "Dashboard d'activité",
      icon: <TableChart />,
      preferredOrder: 10,
      activationParameters: {
        groupement: '0210',
        pharmacie: '0745',
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
  routes: [
    {
      attachTo: 'PARAMETRE',
      path: '/dashboard-item-scoring',
      component: ParametrePage,
    },
    {
      attachTo: 'MAIN',
      path: '/dashboard-activite',
      component: DashboardPage,
      mobileOptions: {
        topBar: {
          title: "Mon Dashboard d'activité",
          withBackBtn: true,
          withTopMargin: true,
          //onGoBack={onRequestGoBack}
        },
      },
    },
  ],
};

export default dashboardActiviteModule;
