import { useApplicationContext } from '@lib/common';
import {
  ItemScoringPersonnalisationInfoFragment,
  ItemScoringPersonnalisationInput,
  useCreate_One_Item_Scoring_PersonnalisationMutation,
  useUpdate_One_Item_Scoring_PersonnalisationMutation,
} from '@lib/common/src/federation';
import { useState } from 'react';

const useSaveOneItemScoringPersonnalisation = (): [
  (input: ItemScoringPersonnalisationInput, id?: string) => Promise<any>,
  { loading?: boolean; error?: Error; data?: ItemScoringPersonnalisationInfoFragment }
] => {
  const { federation } = useApplicationContext();
  const [mode, setMode] = useState<'creation' | 'modification'>('creation');
  const [create, creation] = useCreate_One_Item_Scoring_PersonnalisationMutation({
    client: federation,
  });
  const [update, modification] = useUpdate_One_Item_Scoring_PersonnalisationMutation({
    client: federation,
  });

  const upsert = (input: ItemScoringPersonnalisationInput, id?: string) => {
    if (id) {
      setMode('modification');
      return update({
        variables: {
          id,
          input: {
            type: input.type,
            idTypeAssocie: input.idTypeAssocie,
            ordre: input.ordre,
          },
        },
      });
    } else {
      setMode('creation');
      return create({
        variables: {
          input: {
            type: input.type,
            idTypeAssocie: input.idTypeAssocie,
            ordre: input.ordre,
          },
        },
      });
    }
  };

  return [
    upsert,
    {
      loading: mode === 'creation' ? creation.loading : modification.loading,
      error: mode === 'creation' ? creation.error : modification.error,
      data:
        mode === 'creation'
          ? creation.data?.createOneItemScoringPersonnalisation
          : modification.data?.updateOneItemScoringPersonnalisation,
    },
  ];
};

export default useSaveOneItemScoringPersonnalisation;
