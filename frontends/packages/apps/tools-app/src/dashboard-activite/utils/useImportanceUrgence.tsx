import { useEffect } from 'react';
import { SortDirection, UrgenceSortFields, useGet_UrgencesLazyQuery, useImportancesLazyQuery } from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';

export const useImportanceUrgence = () => {
  const { federation } = useApplicationContext();
  const [loadUrgences, loadingUrgences] = useGet_UrgencesLazyQuery({
    client: federation,
    variables: {
      sorting: [{ field: UrgenceSortFields.Code, direction: SortDirection.Desc }],
    },
  });
  const [loadImportances, loadingImportances] = useImportancesLazyQuery({ client: federation }
  );

  useEffect(() => {
    loadUrgences();
    loadImportances();
  }, []);

  const importances = {
    data: loadingImportances.data?.importances.nodes,
    error: loadingImportances.error,
    loading: loadingImportances.loading,
  };

  const urgences = {
    data: loadingUrgences.data?.urgences.nodes,
    error: loadingUrgences.error,
    loading: loadingUrgences.loading,
  };

  return [importances, urgences];
};
