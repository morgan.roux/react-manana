import { useApplicationContext } from '@lib/common';
import {
  useUpdate_Action_Operationnelle_StatutMutation,
  useUpdate_Fiche_Amelioration_StatutMutation,
  useUpdate_Fiche_Incident_StatutMutation,
  useUpdate_Information_Liaison_StatutMutation,
  useUpdate_Todo_Action_StatutMutation,
} from '@lib/common/src/federation';
import { Item } from '@lib/ui-kit/src/components/pages/DashboardPage/types';
export interface UpdateReunionProps {
  item: Item;
  idItemAssocie: string;
  codeStatut: string;
}

const useReunion = (loading: any, onCompletChangeStatut: () => void) => {
  const { notify, federation } = useApplicationContext();

  const [updateFicheAmeliorationStatut] = useUpdate_Fiche_Amelioration_StatutMutation({
    onCompleted: () => {
      notify({
        message: `Le statut a été modifié avec succès !`,
        type: 'success',
      });
      loading && loading.refetch();
      onCompletChangeStatut();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du statut de la fiche amélioration',
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateFicheIncidentStatut] = useUpdate_Fiche_Incident_StatutMutation({
    onCompleted: () => {
      notify({
        message: `Le statut a été modifié avec succès !`,
        type: 'success',
      });
      loading && loading.refetch();
      onCompletChangeStatut();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du statut de la fiche incident',
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateActionOperationnelleStatut] = useUpdate_Action_Operationnelle_StatutMutation({
    onCompleted: () => {
      notify({
        message: `Le statut a été modifié avec succès !`,
        type: 'success',
      });
      loading && loading.refetch();
      onCompletChangeStatut();
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant la modification du statut de l'action opérationnelle",
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateTodoActionStatut] = useUpdate_Todo_Action_StatutMutation({
    onCompleted: () => {
      notify({
        message: `Le statut de la To-do action a été modifié avec succès !`,
        type: 'success',
      });
      loading && loading.refetch();
      onCompletChangeStatut();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du statut de la To-Do Action',
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateInformationLiaisonStatut] = useUpdate_Information_Liaison_StatutMutation({
    onCompleted: () => {
      notify({
        message: `Le statut de l'information de liaison a été modifié avec succès !`,
        type: 'success',
      });
      loading && loading.refetch();
      onCompletChangeStatut();
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant la modification du statut de l'information de liaison",
        type: 'error',
      });
    },
    client: federation,
  });

  const updateReunion = ({ item, idItemAssocie, codeStatut }: UpdateReunionProps) => {
    if (item.codeItem === 'SA') {
      updateFicheAmeliorationStatut({
        variables: {
          id: idItemAssocie,
          code: codeStatut,
        },
      });
    } else if (item.codeItem === 'SB') {
      updateFicheIncidentStatut({
        variables: {
          id: idItemAssocie,
          code: codeStatut,
        },
      });
    } else if (item.codeItem === 'SC') {
      updateActionOperationnelleStatut({
        variables: {
          id: idItemAssocie,
          code: codeStatut,
        },
      });
    } else if (item.codeItem === 'L') {
      updateTodoActionStatut({
        variables: {
          id: idItemAssocie,
          code: codeStatut,
        },
      });
    } else if (item.codeItem === 'Q') {
      updateInformationLiaisonStatut({
        variables: {
          id: idItemAssocie,
          code: codeStatut,
        },
      });
    }
  };

  return { updateReunion };
};

export default useReunion;
