import { useApplicationContext } from '@lib/common';
import { useItem_StatistiquesQuery } from '@lib/common/src/federation';
import { SeriesItem } from '@lib/ui-kit/src/components/pages/DashboardPage/DashboardRepartition';

export const useStatistique = () => {
  const { federation, user } = useApplicationContext();
  const loadStatistique = useItem_StatistiquesQuery({
    fetchPolicy: 'cache-and-network',
    variables: { idUser: user.id },
    client: federation,
  });

  const dashboardFilter = {
    data: (loadStatistique?.data?.itemStatistiques || []).map((itemStatistique) => {
      return {
        ...itemStatistique.item,
        occurence: itemStatistique.occurence,
      };
    }),
    loading: loadStatistique.loading,
    error: loadStatistique.error,
  };

  let statistiques = loadStatistique?.data?.itemStatistiques?.map((itemStatistique) => {
    return {
      categorie: itemStatistique.item.name,
      valeur: itemStatistique.percentage,
    };
  });

  statistiques = [...(statistiques || [])].sort((a: SeriesItem, b: SeriesItem) => b.valeur - a.valeur);

  const series = statistiques?.filter((statistique, index) => statistique.valeur !== 0 && index < 3);

  const otherSerie = statistiques
    ?.filter((_statistique, index) => index > 1)
    .reduce(
      (previousSerie: SeriesItem, currentSerie: SeriesItem) => {
        return { categorie: 'Autres', valeur: previousSerie.valeur + currentSerie.valeur };
      },
      { categorie: 'Autres', valeur: 0 }
    );

  series && series.length > 2 && (series[2] = otherSerie || { categorie: 'Autres', valeur: 0 });

  return {
    series,
    dashboardFilter,
    loadStatistique,
  };
};
