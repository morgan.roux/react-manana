import moment from 'moment';
export const startTime = (date: any): string | null => {
  if (!date) {
    return null;
  }

  const m = moment(date).utcOffset(0);
  m.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
  return m.toISOString();
};

export const isToday = (date: any) => {
  return startTime(moment()) === startTime(date);
};

export const isTodayBetween = (dateDebut: any, dateFin: any) => {
  return moment().isBetween(dateDebut, dateFin);
};
