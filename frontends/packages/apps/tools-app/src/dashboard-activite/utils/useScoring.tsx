import { ApolloError, LazyQueryResult } from '@apollo/client';
import { useApplicationContext } from '@lib/common';
import {
  Get_Item_ScoringsQuery,
  Get_Item_ScoringsQueryVariables,
  Get_Item_Scorings_AggregatesQuery,
  Get_Item_Scorings_AggregatesQueryVariables,
  ItemScoringFilter,
  ItemScoringFragment,
  ItemScoringSort,
  ItemScoringSortFields,
  SortDirection,
  useGet_Item_ScoringsLazyQuery,
  useGet_Item_Scorings_AggregatesLazyQuery,
} from '@lib/common/src/federation';
import moment from 'moment';
import { useCallback, useEffect } from 'react';

import { isToday, isTodayBetween } from './util';

export const startTime = (date: any): string | null => {
  if (!date) {
    return null;
  }

  const m = moment(date).utcOffset(0);
  m.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
  return m.toISOString();
};

export const endTime = (date: any): string | null => {
  if (!date) {
    return null;
  }

  const m = moment(date).utcOffset(0);
  m.set({ hour: 23, minute: 59, second: 59, millisecond: 59 });
  return m.toISOString();
};

export const useScoring = (
  filters: any,
  user: any,
  pharmacie: any
): [
  ItemScoringFragment[],
  boolean,
  ApolloError | undefined,
  number | undefined | null,
  LazyQueryResult<Get_Item_ScoringsQuery, Get_Item_ScoringsQueryVariables>,
  LazyQueryResult<Get_Item_Scorings_AggregatesQuery, Get_Item_Scorings_AggregatesQueryVariables>
] => {
  const { federation } = useApplicationContext();
  const [loadScoring, loadingScoring] = useGet_Item_ScoringsLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  const [countScoring, countingScoring] = useGet_Item_Scorings_AggregatesLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  const getSort = useCallback(() => {
    console.log(filters);
    const sort: ItemScoringSort[] = [];
    if (filters && filters.order) {
      if (filters.order.field === 'importance') {
        sort.push({
          field: ItemScoringSortFields.CoefficientImportance,
          direction: filters.order.direction,
        });
      } else if (filters.order.field === 'urgence') {
        sort.push({
          field: ItemScoringSortFields.CoefficientUrgence,
          direction: filters.order.direction,
        });
      } else if (filters.order.field === 'statut') {
        sort.push({
          field: ItemScoringSortFields.OrdreStatut,
          direction: filters.order.direction,
        });
      } else if (filters.order.field === 'source') {
        sort.push({
          field: ItemScoringSortFields.NomItem,
          direction: filters.order.direction,
        });
      } else if (filters.order.field === 'retard') {
        sort.push({
          field: ItemScoringSortFields.NombreJoursRetard,
          direction: filters.order.direction,
        });
      } else if (filters.order.field === 'score') {
        sort.push({
          field: ItemScoringSortFields.Score,
          direction: filters.order.direction,
        });
      }
    }
    if (sort.length === 0) {
      sort.push({ field: ItemScoringSortFields.Score, direction: SortDirection.Desc });
    }
    return sort;
  }, [filters]);

  useEffect(() => {
    const filterList: ItemScoringFilter[] = [
      {
        idUser: {
          eq: user.id,
        },
      },
      {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
    ];

    if (filters.items && filters.items.length > 0) {
      filterList.push({
        idItem: {
          in: (filters.items || []).map((element: any) => {
            return element.id;
          }),
        },
      });
    }

    if (filters.searchText) {
      filterList.push({
        description: {
          iLike: `%${filters.searchText}%`,
        },
      });
    }

    if (filters.importance) {
      filterList.push({
        idImportance: {
          eq: filters.importance.id,
        },
      });
    }

    if (filters.urgence) {
      filterList.push({
        idUrgence: {
          eq: filters.urgence.id,
        },
      });
    }

    if (filters.date) {
      if (isToday(filters.date)) {
        // Importer les informations en retard
        filterList.push({
          dateEcheance: {
            lte: endTime(filters.date),
          },
        });
      } else {
        filterList.push({
          dateEcheance: {
            between: {
              lower: startTime(filters.date),
              upper: endTime(filters.date),
            },
          },
        });
      }
    }

    if (filters.dateDebut && filters.dateFin) {
      if (isTodayBetween(filters.dateDebut, filters.dateFin)) {
        // importer les informations en retard (ignorer la date début)
        filterList.push({
          dateEcheance: {
            lte: endTime(filters.dateFin),
          },
        });
      } else {
        filterList.push({
          dateEcheance: {
            between: {
              lower: startTime(filters.dateDebut),
              upper: endTime(filters.dateFin),
            },
          },
        });
      }
    }

    loadScoring({
      variables: {
        filter: {
          and: filterList,
        },
        paging: {
          offset: filters.paging.offset,
          limit: filters.paging.limit,
        },
        sorting: getSort(),
      },
    });
    countScoring({
      variables: {
        filter: {
          and: filterList,
        },
      },
    });
  }, [filters]);

  return [
    loadingScoring.data?.itemScorings.nodes || [],
    loadingScoring.loading || countingScoring.loading,
    loadingScoring.error || countingScoring.error,
    (countingScoring.data?.itemScoringAggregate.length || 0) > 0
      ? countingScoring.data?.itemScoringAggregate[0].count?.id
      : 0,
    loadingScoring,
    countingScoring,
  ];
};
