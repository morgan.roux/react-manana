import { useApplicationContext } from '@lib/common';
import {
  useGet_Rendez_VousLazyQuery,
  useGet_Row_Rendez_VousLazyQuery,
  useUpdate_Rendez_VousMutation,
  PrtRendezVousInfoFragment,
} from '@lib/common/src/federation';
import _ from 'lodash';
import { useEffect, useState } from 'react';

interface OnRequestNextRdvProps {
  take: number;
  skip: number;
}

export const useRendezVous = (): [
  (paging: OnRequestNextRdvProps) => void,
  (data: any) => void,
  PrtRendezVousInfoFragment[] | undefined,
  boolean,
  Error | undefined,
  number
] => {
  const [data, setData] = useState<PrtRendezVousInfoFragment[]>([]);
  const { federation, notify, currentPharmacie: pharmacie } = useApplicationContext();

  const [loadRdv, loadingRdv] = useGet_Rendez_VousLazyQuery({
    client: federation,
  });

  useEffect(() => {
    if (loadingRdv.data?.pRTRendezVous?.nodes) {
      const temp = loadingRdv.data.pRTRendezVous.nodes;
      setData((prev) =>
        Object.values(
          (prev || [])
            .concat(temp)
            .reduce((acc, obj) => ((acc[obj.id] = obj), acc), {} as Record<string, PrtRendezVousInfoFragment>)
        )
      );
    }
  }, [loadingRdv.data]);

  const [loadRowRdv, loadingRowRdv] = useGet_Row_Rendez_VousLazyQuery({
    client: federation,
  });

  const [updateRdv] = useUpdate_Rendez_VousMutation({
    client: federation,
    onCompleted: (updated) => {
      setData((prev) =>
        (prev || []).map((item) => {
          if (item.id === updated.updateOnePRTRendezVous.id) {
            return updated.updateOnePRTRendezVous;
          }
          return item;
        })
      );
      notify({
        message: 'Le rendez-vous est reporté',
        type: 'success',
      });
    },
    onError: (error) => {
      notify({
        message: error.message,
        type: 'error',
      });
    },
  });

  const handleOnRequest = (paging: OnRequestNextRdvProps) => {
    loadRdv({
      variables: {
        filter: {
          statut: {
            notIn: ['REALISE', 'ANNULE'],
          },
          idPharmacie: {
            eq: pharmacie.id,
          },
        },
        paging: {
          offset: paging.skip,
          limit: paging.take,
        },
      },
    });
    loadRowRdv({
      variables: {
        filter: {
          statut: {
            notIn: ['REALISE', 'ANNULE'],
          },
          idPharmacie: {
            eq: pharmacie.id,
          },
        },
      },
    });
  };

  const handleSave = (data: any) => {
    updateRdv({
      variables: {
        id: data.id,
        input: {
          typeReunion: data.typeReunion,
          dateRendezVous: data.dateRendezVous,
          heureDebut: data.heureDebut,
          heureFin: data.heureFin,
          idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
          partenaireType: data.partenaireType,
          idUserParticipants: (data.participants || []).map(({ id }: any) => id),
          idInvites: (data.invites || []).map(({ id }: any) => id) as any,
          idSubject: data.idSubject,
          statut: 'REPORTER',
        },
      },
    });
  };

  return [
    handleOnRequest,
    handleSave,
    data,
    loadingRdv.loading,
    loadingRdv.error,
    loadingRowRdv.data?.pRTRendezVousAggregate.length || 0 > 0
      ? loadingRowRdv.data?.pRTRendezVousAggregate[0].count?.id || 0
      : 0,
  ];
};
