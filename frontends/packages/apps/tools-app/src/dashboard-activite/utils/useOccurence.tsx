import { LazyQueryResult } from '@apollo/client';
import { useApplicationContext } from '@lib/common';
import {
  Get_Count_ScoringQuery,
  Get_Count_ScoringQueryVariables,
  ItemScoringCountFragment,
  useGet_Count_ScoringLazyQuery,
} from '@lib/common/src/federation';
import { useEffect, useState } from 'react';

interface FilterOccurence {
  date?: Date;
  dateDebut?: Date;
  dateFin?: Date;
  idItems?: string[];
}

export const useOccurence = (): [
  React.Dispatch<React.SetStateAction<FilterOccurence | undefined>>,
  ItemScoringCountFragment[],
  LazyQueryResult<Get_Count_ScoringQuery, Get_Count_ScoringQueryVariables>,
  boolean,
  Error?
] => {
  const { federation } = useApplicationContext();
  const [countScoringUser, countingScoringUser] = useGet_Count_ScoringLazyQuery({
    client: federation,
  });
  const [filter, setFilter] = useState<FilterOccurence>();

  useEffect(() => {
    countScoringUser({
      variables: {
        idItems: filter?.idItems || [],
        dateEcheance: filter?.date,
        dateDebut: filter?.dateDebut,
        dateFin: filter?.dateFin,
      },
    });
  }, [filter]);

  return [
    setFilter,
    countingScoringUser.data?.itemScoringsCount || [],
    countingScoringUser,
    countingScoringUser.loading,
    countingScoringUser.error,
  ];
};
