import { CommentsQuery, useCommentsLazyQuery } from '@lib/common/src/graphql';

export const useComment = (): [any, boolean?, Error?, CommentsQuery?,(() => void)?] => {
  const [
    getCommentaires,
    { loading: commentsLoading, error: commentsError, data: commentaires, refetch: refetchComments },
  ] = useCommentsLazyQuery();

  return [getCommentaires, commentsLoading, commentsError, commentaires, refetchComments];
};
