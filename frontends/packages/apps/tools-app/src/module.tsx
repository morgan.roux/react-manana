import { ModuleDefinition, mergeModules } from '@lib/common';
import commandeOraleModule from './commande-orale/commande-orale.module';
import dashboardActiviteModule from './dashboard-activite/dashboard-activite.module';
import statistiqueModule from './statistique/statistique.module';
import suiviAppelsModule from './suivi-appels/suivi-appels.module';
import releveTemperaturesModule from './releve-temperatures/releve-temperatures.module';
import moment from 'moment';
moment.locale('fr');

const toolsModule: ModuleDefinition = mergeModules(
  commandeOraleModule,
  dashboardActiviteModule,
  statistiqueModule,
  suiviAppelsModule,
  releveTemperaturesModule
);

export default toolsModule;
