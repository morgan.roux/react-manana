import React from 'react';
import loadable from '@loadable/component';
import SuiviAppelIcon from '@material-ui/icons/ContactPhone';
import { ModuleDefinition } from '@lib/common';

const suiviAppelsModule: ModuleDefinition = {
  features: [
    /*{
      id: 'FEATURE_FAVORITE_SUIVI_APPEL',
      location: 'FAVORITE',
      name: 'Suivi des appels',
      to: '/outils-digitaux/suivi-appels',
      icon: <SuiviAppelIcon />,
      preferredOrder: 30,
      activationParameters: {
        groupement: '0824',
        pharmacie: '0723',
      },
    },*/
  ],
};

export default suiviAppelsModule;
