import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  useGet_RelevesLazyQuery,
  useGet_Row_ReleveLazyQuery,
  useGet_FrigosQuery,
  useGet_List_Releve_StatusQuery,
  useCreate_ReleveMutation,
} from '@lib/common/src/federation';
import { MobileTopBar, useApplicationContext } from '@lib/common';
import { ReleveTemperaturePage } from '../../components/ReleveTemperaturePage';
import { useTheme } from '@material-ui/core';

const ReleveTemperature: FC<RouteComponentProps> = ({ history }) => {
  const [relevesListType, setRelevesListType] = useState<'ALL' | 'STATS'>('ALL');

  const { currentPharmacie: pharmacie, federation, notify, isMobile } = useApplicationContext();
  const theme = useTheme();

  /**
   * GET ALL FRIGOS
   */
  const loadFrigos = useGet_FrigosQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
    variables: {
      filter: {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
    },
  });

  const loadStatus = useGet_List_Releve_StatusQuery({
    client: federation,
  });

  const [loadReleves, loadingReleves] = useGet_RelevesLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });
  const [loadAggregate, loadingAggregate] = useGet_Row_ReleveLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  /**
   * MUTATION FOR CREATE RELEVE
   */
  const [createReleve, creatingReleve] = useCreate_ReleveMutation({
    onCompleted: (data) => {
      if (data.createOneRTReleve.status.code === '002') {
        notify({
          message: 'Le rélevé du frigo a été créé avec succès !',
          type: 'success',
        });
      }

      loadFrigos.refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la création du rélevé',
        type: 'error',
      });
    },
    client: federation,
  });

  /** SUBMIT FORM CREATE RELEVE */

  const handleRequestSave = (releve: any) => {
    createReleve({
      variables: {
        input: {
          ...releve,
        },
      },
    });
  };
  const handleRequestGoBack = () => {
    history.push('/');
  };

  const handleRequestSearch = (frigo: any, change: any) => {
    setRelevesListType('ALL');
    loadReleves({
      variables: {
        filter: {
          idFrigo: {
            eq: frigo.id,
          },
        },
        paging: {
          offset: change.skip,
          limit: change.take,
        },
      },
    });

    loadAggregate({
      variables: {
        filter: {
          idFrigo: {
            eq: frigo.id,
          },
        },
      },
    });
  };

  const handleRequestStatsReleves = (frigo: any, change: any) => {
    setRelevesListType('STATS');
    loadReleves({
      variables: {
        filter: {
          and: [
            {
              idFrigo: {
                eq: frigo.id,
              },
            },
            {
              createdAt: {
                gte: change.start,
              },
            },
            {
              createdAt: {
                lte: change.end,
              },
            },
          ],
        },
        paging: {
          offset: 0,
          limit: 500,
        },
      },
    });
  };

  const total =
    relevesListType === 'ALL'
      ? (loadingAggregate.data?.rTReleveAggregate.length || 0) > 0
        ? loadingAggregate.data?.rTReleveAggregate[0].count?.id
        : 0
      : loadingReleves.data?.rTReleves.nodes.length;

  console.log('***********************************************Theme ReleveTemperaturePage', theme);

  return (
    <>
      <MobileTopBar
        breakpoint="sm"
        title="Relevé des Températures"
        withBackBtn={true}
        withTopMargin={true}
        backUrl="/"
      />
      <ReleveTemperaturePage
        isMobile={isMobile}
        releves={{
          loading:
            relevesListType === 'ALL' ? loadingReleves.loading && loadingAggregate.loading : loadingReleves.loading,
          error: loadingReleves.error || loadingAggregate.error,
          data: loadingReleves.data?.rTReleves.nodes as any,
          total: total || 0,
        }}
        saving={creatingReleve.loading}
        errorSaving={creatingReleve.error as any}
        onRequestStatsReleves={handleRequestStatsReleves}
        onRequestSearch={handleRequestSearch}
        onRequestSave={handleRequestSave}
        frigos={loadFrigos.data?.rTFrigos.nodes as any}
        loading={loadFrigos.loading}
        error={loadFrigos.error}
        statusFrigo={{
          ...loadStatus,
          data: loadStatus.data?.rTStatuses.nodes || [],
        }}
        onRequestGoBack={handleRequestGoBack}
      />
    </>
  );
};

export default withRouter(ReleveTemperature);
