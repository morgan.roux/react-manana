import React, { FC, useState } from 'react';
import {
  useCreate_FrigoMutation,
  useUpdate_FrigoMutation,
  useDelete_One_FrigoMutation,
  useGet_Row_FrigoQuery,
  useGet_FrigosQuery,
  useGet_Traitement_Automatiques_TypesQuery,
  useCreate_Many_Traitements_AutomatiquesMutation,
  useDelete_Many_Traitements_AutomatiquesMutation,
  Get_FrigosDocument,
  Get_FrigosQuery,
  Get_FrigosQueryVariables,
} from '@lib/common/src/federation';

import { useApplicationContext /*, CommonFieldsForm*/ } from '@lib/common';
import { FrigoPage } from '../../components/ReleveTemperaturePage/FrigoPage';

const ParametreFrigo: FC<{}> = () => {
  const { currentPharmacie: pharmacie, federation, notify } = useApplicationContext();

  const [state, setState] = useState<any>({});
  const [frigoToEdit, setFrigoToEdit] = React.useState<any>(undefined);
  const [frigoToSave, setFrigoTosave] = React.useState<any>(undefined);
  const [saved, setSaved] = useState<boolean>(false);
  const [saving, setSaving] = useState<boolean>(false);

  const filterFrigo = {
    filter: {
      idPharmacie: {
        eq: pharmacie.id,
      },
    },
  };

  const loadingFrigos = useGet_FrigosQuery({
    variables: filterFrigo,
    client: federation,
  });

  const loadFrigoAggregate = useGet_Row_FrigoQuery({
    variables: {
      filter: {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
    },
    client: federation,
  });

  const loadTraitementTypes = useGet_Traitement_Automatiques_TypesQuery({
    client: federation,
  });

  const [updateFrigo, updatingFrigo] = useUpdate_FrigoMutation({
    onCompleted: () => {
      notify({
        message: 'Le Frigo a été modifié avec succès !',
        type: 'success',
      });

      setState([]);

      setSaved(true);
      setSaving(false);
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du Frigo',
        type: 'error',
      });
      setSaving(false);
    },
    client: federation,
  });

  const [createFrigo, creatingFrigo] = useCreate_FrigoMutation({
    onCompleted: () => {
      notify({
        message: 'Le Frigo a été créé avec succès !',
        type: 'success',
      });

      setState([]);
      setSaved(true);
      setSaving(false);
    },
    update: (cache, result) => {
      if (result.data?.createOneRTFrigo) {
        const previousList = cache.readQuery<Get_FrigosQuery, Get_FrigosQueryVariables>({
          query: Get_FrigosDocument,
          variables: filterFrigo,
        })?.rTFrigos;

        cache.writeQuery<Get_FrigosQuery, Get_FrigosQueryVariables>({
          query: Get_FrigosDocument,
          variables: filterFrigo,
          data: {
            rTFrigos: {
              ...(previousList || {}),
              nodes: [...(previousList?.nodes || []), result.data.createOneRTFrigo],
            } as any,
          },
        });
      }
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la création du Frigo',
        type: 'error',
      });
      setSaving(false);
    },
    client: federation,
  });

  const [deleteFrigo, deletingFrigo] = useDelete_One_FrigoMutation({
    onCompleted: () => {
      notify({
        message: 'Le Frigo a été supprimé avec succès !',
        type: 'success',
      });
    },
    update: (cache, result) => {
      if (result.data?.deleteOneRTFrigo) {
        const previousList = cache.readQuery<Get_FrigosQuery, Get_FrigosQueryVariables>({
          query: Get_FrigosDocument,
          variables: filterFrigo,
        })?.rTFrigos;

        cache.writeQuery<Get_FrigosQuery, Get_FrigosQueryVariables>({
          query: Get_FrigosDocument,
          variables: filterFrigo,
          data: {
            rTFrigos: {
              ...(previousList || {}),
              nodes: previousList?.nodes?.filter((frigo) => frigo.id !== result.data?.deleteOneRTFrigo.id) || [],
            } as any,
          },
        });
      }
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression du Frigo',
        type: 'error',
      });
    },
    client: federation,
  });

  const handleChangeTache = (value: any) => {
    handleChange({ target: { name: 'projet', value } });
  };

  const [createManyTraitement, creatingManyTraitement] = useCreate_Many_Traitements_AutomatiquesMutation({
    onCompleted: (result) => {
      notify({
        message: 'Le traitement automatique a été créé avec succès !',
        type: 'success',
      });

      if (frigoToSave.id) {
        const idTraitements = frigoToSave.traitements
          .filter((traitement: any) => traitement.id)
          .map((traitement: any) => traitement.id);
        updateFrigo({
          variables: {
            id: frigoToSave.id,
            input: {
              ...frigoToSave,
              id: undefined,
              temporisation: frigoToSave.temporisation || 30,
              traitements: undefined,
              idTraitements: [...idTraitements, ...result.createManyTATraitements.map((traitement) => traitement.id)],
            },
          },
        });
      } else {
        createFrigo({
          variables: {
            input: {
              ...frigoToSave,
              temporisation: frigoToSave.temporisation || 30,
              traitements: undefined,
              idTraitements: result.createManyTATraitements.map((traitement) => traitement.id),
            },
          },
        });
      }
      setState([]);
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la création du traitement automatique',
        type: 'error',
      });
    },
    client: federation,
  });

  const [deleteManyTraitement, deletingManyTraitement] = useDelete_Many_Traitements_AutomatiquesMutation({
    onCompleted: () => {
      notify({
        message: 'Le traitement automatique a été supprimé avec succès !',
        type: 'success',
      });
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression du traitement automatique',
        type: 'error',
      });
    },
    client: federation,
  });

  const handleChange = (event: any) => {
    if (event.target) {
      const { value, name } = event.target;
      setState((prevState: any) => ({ ...prevState, [name]: value }));
    }
  };

  const handleRequestEdit = (frigo: any) => {
    setFrigoToEdit(frigo);
  };

  const handleRequestDelete = (frigo: any) => {
    deleteFrigo({
      variables: {
        input: { id: frigo.id },
      },
    });
    deleteManyTraitement({
      variables: {
        ids: frigo.traitements.map((traitement: any) => traitement.id),
        //   input: [...traitementsToDelete.map(traitement => traitement.id )]
      },
    });
  };

  const handleRequestSaveTraitement = (traitement: any): any => {
    console.log('---------------------------Traitement frigo-----------------------------: ', traitement);
    return {
      ...traitement,
      idUrgence: traitement.idUrgence,
      idFonction: traitement?.idFonction,
      idTache: traitement?.idTache,
      idImportance: traitement.idImportance,
      idUserParticipants: (traitement.assignedUsers || []).map(({ id }: any) => id),
    };
  };

  const handleRequestSave = (frigo: any) => {
    setSaved(false);
    setSaving(true);
    if (
      frigo.typeReleve === 'MANUEL' &&
      ((frigo?.traitements && frigo?.traitements?.length > 0) ||
        (frigoToEdit?.traitements && frigoToEdit?.traitements?.length > 0))
    ) {
      // console.log('frigoTraitement', frigo.traitements);
      // console.log('frigoto Edit Traitement', frigoToEdit?.traitements);
      const traitementsToAdd = frigo.traitements.filter(
        (traitement: any) =>
          !frigoToEdit?.traitements.some((traitementEdit: any) => traitementEdit.id === traitement.id)
      );
      const traitementsToDelete = frigoToEdit?.traitements.filter(
        (traitementEdit: any) => !frigo.traitements.some((traitement: any) => traitementEdit.id === traitement.id)
      );

      setFrigoTosave(frigo);

      if (traitementsToAdd && traitementsToAdd.length !== 0) {
        createManyTraitement({
          variables: {
            input: [...traitementsToAdd.map((traitement: any) => ({ ...traitement, id: undefined }))],
          },
        });
      }

      if (traitementsToDelete && traitementsToDelete.length !== 0) {
        const idTraitements = frigo.traitements
          .filter((traitement: any) => traitement.id)
          .map((traitement: any) => traitement.id);
        // console.log('idTraitements', idTraitements);
        deleteManyTraitement({
          variables: {
            ids: traitementsToDelete.map((traitement: any) => traitement.id),
          },
        }).then(() =>
          updateFrigo({
            variables: {
              id: frigo.id,
              input: {
                ...frigo,
                id: undefined,
                temporisation: frigo.temporisation || 30,
                traitements: undefined,
                idTraitements,
              },
            },
          })
        );
      }
    } else {
      if (frigo.id) {
        updateFrigo({
          variables: {
            id: frigo.id,
            input: {
              nom: frigo.nom,
              temperatureHaute: frigo.temperatureHaute,
              temperatureBasse: frigo.temperatureBasse,
              temporisation: frigo.temporisation || 30,
              typeReleve: frigo.typeReleve,
              capteurs: frigo.capteurs,
            },
          },
        });
      } else {
        createFrigo({
          variables: {
            input: {
              nom: frigo.nom,
              temperatureHaute: frigo.temperatureHaute,
              temperatureBasse: frigo.temperatureBasse,
              temporisation: frigo.temporisation || 30,
              typeReleve: frigo.typeReleve,
              capteurs: frigo.capteurs,
            },
          },
        });
      }
    }
  };

  return (
    <FrigoPage
      saved={saved}
      setSaved={setSaved}
      saving={saving}
      onRequestSaveTraitement={handleRequestSaveTraitement}
      loading={loadingFrigos.loading}
      error={loadingFrigos.error}
      data={(loadingFrigos.data?.rTFrigos.nodes || []) as any}
      rowsTotal={
        (loadFrigoAggregate.data?.rTFrigoAggregate.length || 0) > 0
          ? loadFrigoAggregate.data?.rTFrigoAggregate[0].count?.id || 0
          : 0
      }
      onRequestEdit={handleRequestEdit}
      onRequestSave={handleRequestSave}
      onRequestDelete={handleRequestDelete}
      frigoToEdit={frigoToEdit}
      traitementTypes={{
        loading: loadTraitementTypes.loading,
        error: loadTraitementTypes.error,
        data: (loadTraitementTypes.data?.tATraitementTypes.nodes || []) as any,
      }}
    />
  );
};

export default ParametreFrigo;
