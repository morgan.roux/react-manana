import React from 'react';
import { FridgeOutline } from 'mdi-material-ui';
import loadable from '@loadable/component';
import ReleveTemperatures from './assets/img/releve_temperature.png';
import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';

const ParametreFrigo = loadable(() => import('./pages/ParametreFrigo/ParametreFrigo'), {
  fallback: <SmallLoading />,
});

const ReleveTemperaturePage = loadable(() => import('./pages/ReleveTemperatures/ReleveTemperature'), {
  fallback: <SmallLoading />,
});

const releveTemperaturesModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_TOOLS_RELEVE_TEMPERATURES',
      location: 'ACCUEIL',
      name: 'Relevé des températures',
      to: '/releve-des-temperatures',
      icon: ReleveTemperatures,
      activationParameters: {
        groupement: '0825',
        pharmacie: '0724',
      },
      preferredOrder: 210,
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_PARAMETRE_FRIGO',
      location: 'PARAMETRE',
      name: 'Paramètres Frigo',
      to: '/parametre-frigo',
      icon: <FridgeOutline />,
      preferredOrder: 170,
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
  routes: [
    {
      attachTo: 'MAIN',
      path: '/releve-des-temperatures',
      component: ReleveTemperaturePage,
      mobileOptions: {
        topBar: {
          title: 'Relevé des Températures',
          withBackBtn: true,
          withTopMargin: true,
          //onGoBack={onRequestGoBack}
        },
      },
    },
    {
      attachTo: 'PARAMETRE',
      path: '/parametre-frigo',
      component: ParametreFrigo,
    },
  ],
};

export default releveTemperaturesModule;
