import React, { FC } from 'react';

interface ThermoIconProps {
  color?: string;
}

export const ThermoIcon: FC<ThermoIconProps> = ({ color }) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
      <g transform="translate(0 -0.01)">
        <rect fill="#f11957" opacity="0" width="36" height="36" transform="translate(0 0.01)" />
        <g transform="translate(10.668 4.885)">
          <path
            fill={color}
            d="M124.385,14.191V4.7a4.7,4.7,0,1,0-9.4,0v9.492a7.186,7.186,0,1,0,9.4,0Zm-4.7,10.961a5.525,5.525,0,0,1-3.364-9.911l.324-.249V4.7a3.04,3.04,0,0,1,6.081,0V14.992l.324.249a5.525,5.525,0,0,1-3.364,9.911Z"
            transform="translate(-112.5)"
          />
          <path
            fill={color}
            d="M186.646,169.929V162.5h-1.658v7.429a3.317,3.317,0,1,0,1.658,0Z"
            transform="translate(-178.63 -153.517)"
          />
        </g>
      </g>
    </svg>
  );
};
