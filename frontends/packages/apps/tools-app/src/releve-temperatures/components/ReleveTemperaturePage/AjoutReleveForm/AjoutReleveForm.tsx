import { Typography, Box } from '@material-ui/core';
import React, { FC, useState, useEffect } from 'react';
import NotificationImportantIcon from '@material-ui/icons/NotificationImportant';
import WarningIcon from '@material-ui/icons/Warning';
import { Thermometer } from 'mdi-material-ui';
import useStyles from './style';
import NumberFormat from 'react-number-format';
import { RTFrigo, RTStatus } from '@lib/types';
import { computeColor } from '../ReleveTemperaturePage';
import moment from 'moment';
import { NewCustomButton, CustomModal } from '@lib/ui-kit';

export interface AjoutReleveProps {
  open: boolean;
  nom: string;
  frigo?: RTFrigo;
  statusFrigo: {
    loading?: boolean;
    error?: Error;
    data?: RTStatus[];
  }; // 001(en mache) 002 (en panne)
  saving?: boolean;
  errorSaving?: boolean;
  setOpen: (open: boolean) => void;
  onRequestSave: (data?: RTReleveRequestSaving) => void;
}

export interface RTReleveRequestSaving {
  temperature: number | undefined;
  type: string;
  idStatus: string;
  idFrigo: string;
}

const AjoutReleve: FC<AjoutReleveProps> = ({
  open,
  nom,
  frigo,
  saving,
  errorSaving,
  statusFrigo,
  setOpen,
  onRequestSave,
}) => {
  const classes = useStyles();
  const [temperature, setTemperature] = useState<string>('');

  // CALCUL PROCHAIN RELEVE
  const traitements = frigo?.traitements || [];
  let prochainement = '';
  if (traitements.length > 0) {
    let dateProchainement = traitements[0].execution?.prochainement;
    traitements?.forEach((traitement) => {
      if (
        traitement?.execution?.prochainement &&
        dateProchainement &&
        moment(traitement.execution.prochainement).isBefore(dateProchainement)
      ) {
        dateProchainement = traitement.execution.prochainement;
      }
    });
    prochainement = moment(dateProchainement).format('DD/MM/YYYY à HH:mm');
  }

  useEffect(() => {
    if (errorSaving) {
      setOpen(true);
    }
  }, [errorSaving]);

  useEffect(() => {
    if (!open) {
      setTemperature('');
    }
  }, [open]);

  const handleTemperatureChange = (event: any) => {
    setTemperature(event.currentTarget.value);
  };

  const handleSubmit = () => {
    if (frigo) {
      const newReleves: RTReleveRequestSaving = {
        temperature: parseFloat(temperature),
        type: 'MANUEL',
        idFrigo: frigo?.id || '',
        idStatus: statusFrigo.data?.find(({ code }) => code === '002')?.id || '',
      };
      onRequestSave(newReleves);
      setOpen(false);
    }
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Ajout d'un relevé"
      withBtnsActions={false}
      closeIcon
      headerWithBgColor
      maxWidth="xs"
      fullWidth
    >
      <form
        onSubmit={(event) => {
          event.stopPropagation();
          event.preventDefault();
          handleSubmit();
        }}
        style={{ width: '100%' }}
      >
        <Box>
          <Typography className={classes.textBlack}>{nom}</Typography>
        </Box>
        <Box display="flex" flexDirection="row">
          <Box className={classes.temperatureBox} style={{ marginRight: 20 }}>
            <Typography className={classes.textGrey}>Température min.</Typography>
            <Typography className={classes.textBlack}>{frigo?.temperatureBasse}°c</Typography>
          </Box>
          <Box className={classes.temperatureBox}>
            <Typography className={classes.textGrey}>Température max.</Typography>
            <Typography className={classes.textBlack}>{frigo?.temperatureHaute}°c</Typography>
          </Box>
        </Box>
        <Box display="flex" flexDirection="row">
          <Box>
            <NotificationImportantIcon style={{ color: computeColor(parseFloat(temperature), frigo) }} />
          </Box>
          <Box className={classes.fieldBox}>
            <Typography className={classes.textBlack}>Température actuelle</Typography>
            <Typography className={classes.textGrey}>Veuillez saisir la température actuelle</Typography>
            <NumberFormat
              value={temperature}
              onChange={handleTemperatureChange}
              className={classes.temperatureField}
            />{' '}
            °c
          </Box>
        </Box>
        {frigo && parseFloat(temperature) > frigo.temperatureHaute ? (
          <Box display="flex" flexDirection="row" className={classes.warningBox}>
            <WarningIcon className={classes.warningIcon} />{' '}
            <Typography className={classes.textNormal}>
              La température a depassée le maximum. Il faut informer le service concerné. <br />
              <Box display="flex" flexDirection="row">
                <Thermometer style={{ color: computeColor(parseFloat(temperature), frigo) }} />
                <Typography className={classes.textNormal}>&gt; {frigo.temperatureHaute}°c</Typography>
              </Box>
            </Typography>
          </Box>
        ) : (
          ''
        )}
        {frigo && parseFloat(temperature) < frigo.temperatureBasse ? (
          <Box display="flex" flexDirection="row" className={classes.warningBox}>
            <WarningIcon className={classes.warningIcon} />{' '}
            <Typography className={classes.textNormal} style={{ paddingLeft: 10 }}>
              La température a depassée le minimum. Il faut informer le service concerné. <br />
              <Thermometer style={{ color: computeColor(parseFloat(temperature), frigo) }} /> &lt;{' '}
              {frigo.temperatureBasse}°c
            </Typography>
          </Box>
        ) : (
          ''
        )}

        {prochainement && (
          <Typography className={classes.textNormal} style={{ marginBottom: 20 }}>
            Le prochain relevé est prévu pour le :{' ' + prochainement}
          </Typography>
        )}
        <Box display="flex" justifyContent="flex-end">
          <NewCustomButton type="submit" disabled={saving || statusFrigo.loading || temperature === ''}>
            Enregistrer
          </NewCustomButton>
        </Box>
      </form>
    </CustomModal>
  );
};

export default AjoutReleve;
