// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable react/jsx-no-bind */
import {
  Box,
  Card,
  CardActions,
  CardContent,
  FormControlLabel,
  FormGroup,
  Grid,
  IconButton,
  Switch,
  Typography,
} from '@material-ui/core';
import React, { FC, ChangeEvent, useState, useEffect } from 'react';
import { ThermoIcon } from '../ThermoIcon/ThermoIcon';
import { ThermoLogo } from '../ThermoIcon/ThermoLogo';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import NotificationImportantIcon from '@material-ui/icons/NotificationImportant';
import ReplayIcon from '@material-ui/icons/Replay';
import SaveIcon from '@material-ui/icons/Save';
import TimelineIcon from '@material-ui/icons/Timeline';
import moment from 'moment';
import classnames from 'classnames';
import { RTFrigo, RTReleve, RTStatus } from '@lib/types';
import { computeColor } from '../ReleveTemperaturePage';
import useStyles from '../style';
import { RTReleveRequestSaving } from '../AjoutReleveForm/AjoutReleveForm';
import { ConfirmDialog } from '@lib/ui-kit';

interface ReleveFrigoProps {
  frigo: RTFrigo;
  releveFrigo: RTReleve | undefined;
  statusFrigo: {
    loading?: boolean;
    error?: Error;
    data?: RTStatus[];
  }; // 001(en mache) 002 (en panne)
  setToggleOpenHistorique: (releveFrigos: RTFrigo | undefined) => void;
  setToggleOpenStatistique: (releveFrigos: RTFrigo | undefined) => void;
  setToggleOpenAdd: (releveFrigos: RTFrigo | undefined) => void;
  onRequestSave: (data?: RTReleveRequestSaving) => void;
}

const ReleveFrigo: FC<ReleveFrigoProps> = ({
  frigo,
  releveFrigo,
  statusFrigo,
  setToggleOpenHistorique,
  setToggleOpenStatistique,
  setToggleOpenAdd,
  onRequestSave,
}) => {
  const classes = useStyles();

  const [status, setStatus] = useState<RTStatus>();
  const [openConfirmChangementStatutDialog, setOpenConfirmChangementStatutDialog] = useState<boolean>(false);

  const handleChangeStateFrigo = (e: ChangeEvent<HTMLInputElement>) => {
    e.stopPropagation();
    if (e.target.checked) {
      setStatus(statusFrigo.data?.find(({ code }) => code === '002'));
    } else {
      setOpenConfirmChangementStatutDialog(true);
    }
  };

  const handleConfirmChangementStatutFrigo = () => {
    setStatus(statusFrigo.data?.find(({ code }) => code === '001'));
    onRequestSave({
      temperature: undefined,
      type: 'MANUEL',
      idFrigo: frigo?.id || '',
      idStatus: statusFrigo.data?.find(({ code }) => code === '001')?.id || '',
    });

    setOpenConfirmChangementStatutDialog(false);
  };

  useEffect(() => {
    if (frigo?.dernierReleve) {
      setStatus(frigo.dernierReleve?.status);
    }
  }, [frigo?.dernierReleve]);

  return (
    <Box className={classes.cards}>
      <ConfirmDialog
        title="Confirmation"
        message="Etes-vous sûr de vouloir changer le statut de ce frigo ?"
        open={openConfirmChangementStatutDialog}
        handleClose={() => setOpenConfirmChangementStatutDialog(false)}
        handleValidate={handleConfirmChangementStatutFrigo}
      />
      <Card
        className={classnames(classes.card, classes.equalHeight)}
        style={{ background: computeColor(releveFrigo?.temperature, frigo) }}
      >
        <CardContent>
          <Box display="flex" flexDirection="row" justifyContent="space-between">
            <Box display="flex" flexDirection="column" alignContent="space-between" className={classes.equalHeight}>
              <Box>
                <Typography>{frigo.nom || ''}</Typography>
              </Box>
              <Box>
                <Typography className={releveFrigo?.temperature ? classes.temperature : classes.small}>
                  {releveFrigo?.temperature ? `${releveFrigo.temperature} °C` : 'Inconnue'}
                </Typography>
              </Box>
              <Box>
                <Typography style={{ fontSize: '0.8em' }}>
                  Température Actuelle <ArrowUpwardIcon />
                </Typography>
              </Box>
            </Box>
            <Box>
              <Typography className={classes.verySmall}>{frigo.typeReleve}</Typography>
            </Box>
            <Box display="flex" justifyContent="center" width={1 / 4} height="100%" alignItems="center">
              <ThermoLogo />
            </Box>
          </Box>
        </CardContent>
      </Card>
      <Card className={classes.details}>
        <CardContent className={classes.detailsContent}>
          <Grid container alignItems="stretch">
            <Grid item xs={6} md={6} className={classes.Item}>
              <Typography className={classes.textBlack}>
                <Box display="flex" justifyContent="center">
                  <FormGroup>
                    <FormControlLabel
                      classes={{
                        label: classes.releveFrigoLabelStatut,
                      }}
                      control={
                        <Switch
                          checked={status?.code === '002'}
                          size="small"
                          onChange={handleChangeStateFrigo}
                          color={status?.code === '001' ? 'secondary' : 'default'}
                        />
                      }
                      label={status?.libelle || 'En arret'}
                      labelPlacement="start"
                    />
                  </FormGroup>
                </Box>
              </Typography>
              <Typography className={classes.textGrey}>Statut</Typography>
            </Grid>
            <Grid item xs={6} md={6} className={classes.Item}>
              <Typography className={classes.textBlack}>
                <Box display="flex" justifyContent="center">
                  <ThermoIcon color={computeColor(releveFrigo?.temperature, frigo)} />
                  {frigo?.temperatureBasse}°c - {frigo?.temperatureHaute}°c
                </Box>
              </Typography>
              <Typography className={classes.textGrey}>Température limite</Typography>
            </Grid>
            <Grid item xs={6} md={6} className={classes.Item}>
              <NotificationImportantIcon
                className={classes.notificationIcon}
                style={{ color: computeColor(releveFrigo?.temperature, frigo) }}
              />{' '}
              <br />
              <br />
              <Typography className={classes.textGrey}>Alerte</Typography>
            </Grid>
            <Grid item xs={6} md={6} className={classes.Item}>
              <Box display="flex" justifyContent="space-between" flexDirection="column" alignItems="space-between">
                <Box>
                  <Typography className={classes.textBlack}>
                    {releveFrigo ? moment(releveFrigo?.createdAt).format('DD/MM/YYYY HH:mm:ss') : '-'}
                  </Typography>
                </Box>
                <Box>
                  <Typography className={classes.textGrey}>Date de Relevé</Typography>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </CardContent>
        <CardActions>
          <Grid container>
            <Grid item xs={4}>
              <IconButton onClick={setToggleOpenStatistique.bind(null, frigo)}>
                <TimelineIcon className={classes.buttonAction} />
              </IconButton>
            </Grid>
            <Grid item xs={4}>
              <IconButton onClick={setToggleOpenHistorique.bind(null, frigo)}>
                <ReplayIcon className={classes.buttonAction} />
              </IconButton>
            </Grid>
            <Grid item xs={4}>
              <IconButton
                onClick={setToggleOpenAdd.bind(null, frigo)}
                disabled={frigo.typeReleve === 'AUTOMATIQUE' || status?.code === '001'}
              >
                <SaveIcon
                  className={
                    frigo.typeReleve !== 'AUTOMATIQUE' && status?.code === '002' ? classes.buttonAction : undefined
                  }
                />
              </IconButton>
            </Grid>
          </Grid>
        </CardActions>
      </Card>
    </Box>
  );
};

export default ReleveFrigo;
