import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    cell: {
      width: 100,
    },
  })
);

export default useStyles;
