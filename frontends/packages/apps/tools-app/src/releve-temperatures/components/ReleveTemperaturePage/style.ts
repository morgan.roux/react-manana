import { makeStyles, Theme, createStyles } from '@material-ui/core';
import GREY from '@material-ui/core/colors/grey';
import PINK from '@material-ui/core/colors/pink';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    rooty: {
      [theme.breakpoints.down('sm')]: {
        height: 'calc(100vh - 155px)',
        width: '100%',
        padding: 0,
        overflowY: 'auto',
        overflowX: 'auto',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 50,
      },
    },
    content: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      flexWrap: 'wrap',
      [theme.breakpoints.down('sm')]: {
        overflowX: 'auto',
        flexWrap: 'nowrap',
      },
    },
    card: {
      color: theme.palette.common.white,
      maxWidth: 350,
      marginBottom: 20,
      [theme.breakpoints.between('xs', 'sm')]: {
        width: '100vh',
      },
    },
    cards: {
      background: 'none',
      border: 'none',
      boxShadow: 'none',
      display: 'flex',
      flexDirection: 'column',
      [theme.breakpoints.up('sm')]: {
        padding: 5,
      },
      [theme.breakpoints.down('sm')]: {
        padding: 5,
        marginBottom: 25,
        display: 'inline-block',
        position: 'relative',
        left: '50%',
        top: '3%',
        transform: 'translateX(-50%)',
      },
    },
    details: {
      maxWidth: 350,
      paddingTop: 30,
      textAlign: 'center',
      border: 'solid 1px',
      borderColor: GREY[200],
      flexGrow: 1,
    },
    detailsContent: {
      borderBottom: 'solid 1px',
      borderBottomColor: GREY[200],
      marginLeft: 20,
      marginRight: 20,
      [theme.breakpoints.down('md')]: {
        padding: 0,
      },
    },
    notificationIcon: {
      minHeight: 30,
    },
    textBlack: {
      fontWeight: 'bold',
      color: GREY[700],
      fontSize: '0.9em',
      marginBottom: 10,
    },
    textGrey: {
      color: GREY[400],
      fontSize: '0.8em',
      marginBottom: 10,
    },
    buttonAction: {
      color: PINK[400],
    },
    thermometerTitle: {
      color: theme.palette.common.white,
      fontSize: 80,
    },
    Item: {
      padding: 10,
      height: '100%',
    },
    small: {
      fontSize: '1.5em',
    },
    temperature: {
      fontSize: '2.5em',
    },
    equalHeight: {
      height: '30%',
    },
    verySmall: {
      fontSize: '0.7em',
    },
    equalHeight2: {
      height: '60%',
    },
    dots: {
      position: 'absolute',
      bottom: '15px',
      left: '50%',
      width: '100%',
    },
    swiperFrigo: {
      overflow: 'auto',
      display: 'flex',
      alignItems: 'center',
      marginRight: '0 !important',
    },

    releveFrigoLabelStatut: {
      width: 80,
    },
  })
);

export default useStyles;
