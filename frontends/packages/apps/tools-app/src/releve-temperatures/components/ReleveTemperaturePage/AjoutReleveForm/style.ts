import { createStyles, makeStyles } from '@material-ui/core';
import GREY from '@material-ui/core/colors/grey';
import AMBER from '@material-ui/core/colors/amber';

const useStyles = makeStyles(() =>
  createStyles({
    cell: {
      width: 100,
    },
    temperatureBox: {
      border: 'solid 1px',
      borderRadius: 6,
      borderColor: GREY[100],
      paddingTop: 20,
      paddingBottom: 10,
      textAlign: 'center',
      width: '100%',
      marginBottom: 20,
    },
    textBlack: {
      fontWeight: 'bold',
      color: GREY[700],
      fontSize: '0.9em',
      marginBottom: 10,
    },
    textGrey: {
      color: GREY[400],
      fontSize: '0.8em',
      marginBottom: 10,
    },
    fieldBox: {
      marginLeft: 10,
      marginBottom: 20,
    },
    temperatureField: {
      maxWidth: 40,
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 5,
      border: 'solid 1px',
      borderRadius: 4,
      borderColor: GREY[400],
    },
    textNormal: {
      fontSize: '0.8em',
    },
    warningBox: {
      backgroundColor: GREY[100],
      padding: 10,
      marginBottom: 20,
    },
    warningIcon: {
      color: AMBER[300],
    },
  })
);

export default useStyles;
