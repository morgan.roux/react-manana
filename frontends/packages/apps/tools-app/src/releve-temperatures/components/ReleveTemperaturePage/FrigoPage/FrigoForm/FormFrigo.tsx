import React, { FC, useState, ChangeEvent, FormEvent, Fragment, useEffect, ReactNode, MouseEvent } from 'react';
import { Box, Grid, ListItemSecondaryAction, Typography, List, ListItemText, ListItem } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import { TATraitementType, RTFrigo } from '@lib/types';
import styles from './style';
import {
  CustomButton,
  CustomFormTextField,
  CustomModal,
  CustomSelect,
  FieldGroupValue,
  FieldsGroup,
  NewCustomButton,
} from '@lib/ui-kit';
import {
  TraitementAutomatiqueForm,
  TraitementAutomatiqueRequestSaving,
} from '../../../../../../../todo-app/src/traitement-recurrent/components/TraitementAutomatiquePage/TraitementAutomatiqueForm';

/** ********************************* INTERFACES ************************************************* */

export interface TATraitementRequestSaving {
  id?: string;
  description: string;
  idType: string;

  idFonction?: string;
  idTache?: string;
  idImportance?: string;
  idUrgence?: string;
  idUserParticipants?: string[];

  repetition: number | undefined;
  repetitionUnite: string;
  heure: string;
  joursSemaine?: number[];
  jourMois?: number;
  moisAnnee?: number;
  dateHeureFin?: Date;
  nombreOccurencesFin?: number;
  dateDebut?: Date;
}

export interface RTRequestCapteurSaving {
  nom: string;
  numeroSerie: string;
  pointAcces: {
    nom: string;
    numeroSerie: string;
  };
}

export interface RTRequestFrigoSaving {
  id?: string;
  typeReleve: string;
  nom: string;
  temporisation?: number;
  temperatureBasse: number;
  temperatureHaute: number;
  traitements: TATraitementRequestSaving[];
  capteurs: RTRequestCapteurSaving[];
}

interface FrigoTypes {
  id: string;
  value: string;
  libelle: string;
}

interface FormFrigoInterface {
  mode: 'creation' | 'modification';
  listFrigoTypes: FrigoTypes[];
  open: boolean;
  errorSaving?: boolean;
  saving?: boolean;
  saved?: boolean;
  setSaved?: (newSaved: boolean) => void;
  setOpen: (open: boolean) => void;
  onRequestSave: (data: any) => void;
  onRequestSaveTraitement?: (data: TraitementAutomatiqueRequestSaving) => TATraitementRequestSaving;
  frigo?: RTFrigo;
  traitementTypes: TATraitementType[];
  // commonFieldsComponent: ReactNode;

  // collaborateurs?: any[];
  // idTache?: string;
  // idImportance?: string;
  // idUrgence?: string;
}

/** * *****************************COMPONENT FORMFRIGO ******************************************** */

const FormFrigo: FC<FormFrigoInterface> = ({
  mode,
  open,
  listFrigoTypes,
  errorSaving,
  saving,
  setSaved,
  saved,
  setOpen,
  onRequestSave,
  onRequestSaveTraitement,
  frigo,
  // commonFieldsComponent,
  traitementTypes,

  // idTache,
  // idImportance,
  // idUrgence,
}) => {
  /** ********************* USE STYLE *************************************** */
  const classes = styles();
  /** **************************END USE STYLE ****************************************************** */

  const [openTA, setOpenTA] = useState<boolean>(false);
  const handleOpenTAModal = () => {
    setOpenTA(!openTA);
  };

  /** ************************ DATA CONTROLLED ******************************************************** */
  const [typeReleve, setTypeReleve] = useState<string>('MANUEL');
  const [nom, setNom] = useState<string>('');
  const [numerSeriePointAcces, setNumeroSeriePointAcces] = useState<string>('');
  const [numeroSeriesCapteurs, setNumeroSeriesCapteurs] = useState<FieldGroupValue[]>([]);
  const [temperatureBasse, setTemperatureBasse] = useState<number | undefined>(undefined);
  const [temperatureHaute, setTemperatureHaute] = useState<number | undefined>(undefined);
  const [temporisation, setTemporisation] = useState<number>(0);
  const [traitementFrigo, setTraitementFrigo] = useState<TATraitementRequestSaving[]>([]);
  const [state, setState] = useState<any>({});
  const [isPrivate, setIsPrivate] = useState<boolean>(false);

  const handleChangeNumeroSeriePointAcces = (e: ChangeEvent<HTMLInputElement>) => {
    setNumeroSeriePointAcces(e.target.value);
  };
  const handleChangeFrigoType = (e: ChangeEvent<HTMLInputElement>) => {
    setTraitementFrigo([]);
    setTypeReleve(e.target.value);
  };
  const handleChangenom = (e: ChangeEvent<HTMLInputElement>) => {
    setNom(e.target.value);
  };
  const handleChangetemperatureBasse = (e: ChangeEvent<HTMLInputElement>) => {
    setTemperatureBasse(parseFloat(e.target.value));
  };
  const handleChangetemperatureHaute = (e: ChangeEvent<HTMLInputElement>) => {
    setTemperatureHaute(parseFloat(e.target.value));
  };
  const handleChangeTemporisation = (e: ChangeEvent<HTMLInputElement>) => {
    setTemporisation(parseInt(e.target.value));
  };
  const handleSubmitTraitementAutomatique = (data: TraitementAutomatiqueRequestSaving) => {
    const dataSaved: TATraitementRequestSaving = onRequestSaveTraitement ? onRequestSaveTraitement(data) : data;
    data ? setTraitementFrigo([...traitementFrigo, dataSaved]) : setTraitementFrigo(traitementFrigo);
    setOpenTA(false);
  };
  const handleRemoveTraitement = (item: number, e: MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
    e.preventDefault();
    const newTraitementFrigo = traitementFrigo.filter((_value, index, _arr) => {
      return index !== item;
    });
    setTraitementFrigo(newTraitementFrigo);
  };
  /** **************************END DATA CONTROLLED ****************************************************** */

  /** **************************SUBMIT DATA ****************************************************** */
  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.stopPropagation();
    e.preventDefault();
    const data: RTRequestFrigoSaving = {
      id: mode === 'modification' ? frigo?.id : undefined,
      typeReleve,
      nom,
      temporisation: temporisation,
      temperatureBasse: temperatureBasse || 0,
      temperatureHaute: temperatureHaute || 0,
      traitements: traitementFrigo,
      capteurs: numeroSeriesCapteurs.map((numeroSerieCapteur) => ({
        nom: numeroSerieCapteur.value,
        numeroSerie: numeroSerieCapteur.value,
        pointAcces: { nom: numerSeriePointAcces, numeroSerie: numerSeriePointAcces },
      })),
    };
    onRequestSave(data);
    // setOpen(false);
  };

  /** **************************END SUBMIT DATA ****************************************************** */

  /** **************************** Open modal if error saving ****************************************** */
  useEffect(() => {
    if (errorSaving) {
      setOpen(true);
    }
  }, [errorSaving]);
  /** ****************************END Open modal if error saving ****************************************** */

  useEffect(() => {
    if (frigo && mode === 'modification') {
      setTypeReleve(frigo.typeReleve);
      setNom(frigo.nom);
      setTemperatureBasse(frigo.temperatureBasse);
      setTemperatureHaute(frigo.temperatureHaute);
      setTraitementFrigo(frigo.traitements || []);
      setTemporisation(frigo.temporisation);
      setNumeroSeriePointAcces((frigo.capteurs && frigo.capteurs[0]?.pointAcces?.numeroSerie) || '');
      setNumeroSeriesCapteurs(
        (frigo.capteurs || []).map((capteur, index) => ({
          ordre: index,
          name: capteur.id,
          value: capteur.numeroSerie,
          deletable: true,
        }))
      );
    } else if (open && mode === 'creation') {
      setTypeReleve('MANUEL');
      setNom('');
      setTemperatureBasse(undefined);
      setTemperatureHaute(undefined);
      setTemporisation(0);
      setTraitementFrigo([]);
      setNumeroSeriePointAcces('');
      setNumeroSeriesCapteurs([]);
    }
  }, [frigo]);

  useEffect(() => {
    if (saved) {
      setOpen(false);
      if (setSaved) {
        setSaved(false);
      }
    }
  }, [saved]);

  /** ****************************Empty field if open modal creation ou fermer la modification ****************************************** */

  const isTemperaturesValid = () => {
    return !(
      typeof temperatureBasse === 'undefined' ||
      typeof temperatureHaute === 'undefined' ||
      temperatureBasse >= temperatureHaute
    );
  };

  const isFieldNull = () => {
    const emptyCommonFields = typeReleve === '' || nom === '';
    return typeReleve === 'AUTOMATIQUE'
      ? numerSeriePointAcces === '' || numeroSeriesCapteurs.length === 0 || emptyCommonFields
      : emptyCommonFields;
  };

  /* ***************************** END CHECK EMPTY FIELD ****************************************************************************** */

  // DEFAULT TYPE
  const defaultTypeId = traitementTypes.find((type) => type.code === 'RELEVE_TEMPERATURE')?.id;

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={mode === 'creation' ? 'Ajout du frigo' : 'Modification du frigo'}
      withBtnsActions={true}
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      fullWidth
      disableBackdropClick
      className={classes.root}
      onClickConfirm={handleSubmit}
      disabledButton={saving || !isTemperaturesValid() || isFieldNull()}
    >
      <form onSubmit={handleSubmit} style={{ width: '100%', padding: '20px 0' }}>
        <CustomSelect
          list={listFrigoTypes}
          index="libelle"
          listId="value"
          label="Type de rélevé"
          value={typeReleve}
          onChange={handleChangeFrigoType}
          defaultValue={typeReleve}
          required
        />
        <CustomFormTextField
          type="text"
          onChange={handleChangenom}
          required
          placeholder="Entrer le nom du frigo"
          label="Nom du frigo"
          value={nom}
        />
        {typeReleve === 'AUTOMATIQUE' && (
          <>
            <CustomFormTextField
              type="text"
              required
              onChange={handleChangeNumeroSeriePointAcces}
              placeholder="Numéro de série"
              label="Numéro de Série du Modem"
              value={numerSeriePointAcces}
            />
            <Typography>Liste de capteurs</Typography>
            <FieldsGroup
              style={{ marginBottom: 20 }}
              multiline={false}
              placeholder="Numéro de série du capteur"
              addFieldLabel="Ajouter un capteur"
              values={numeroSeriesCapteurs}
              onChange={setNumeroSeriesCapteurs}
            />
          </>
        )}
        <Grid container xs={12}>
          <Grid item xs={6}>
            <Typography component="div">
              <Box fontWeight="bold">Température min.</Box>
            </Typography>
            <Box display="flex" flexDirection="row" justifyContent="flex-start" alignItems="center">
              <CustomFormTextField
                type="number"
                className={classes.numberField}
                onChange={handleChangetemperatureBasse}
                value={temperatureBasse}
              />
              <Typography component="div">
                <Box fontWeight="bold">°C</Box>
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Typography component="div">
              <Box fontWeight="bold">Température max.</Box>
            </Typography>
            <Box display="flex" flexDirection="row" justifyContent="flex-start" alignItems="center">
              <CustomFormTextField
                type="number"
                className={classes.numberField}
                onChange={handleChangetemperatureHaute}
                value={temperatureHaute}
              />
              <Typography component="div">
                <Box fontWeight="bold">°C</Box>
              </Typography>
            </Box>
          </Grid>
        </Grid>
        {typeReleve === 'MANUEL' && (
          <Fragment>
            <Grid>
              <Typography component="div">
                <Box fontWeight="bold" pb={3}>
                  Frequence de rélevé de la température
                </Box>
                <Box>Répéter:</Box>
              </Typography>
            </Grid>
            <Grid>
              <List>
                {traitementFrigo.map((item, index) => {
                  // eslint-disable-next-line no-unused-expressions
                  const recurrence =
                    item.repetition === 1
                      ? `Tous les jours, à ${item.heure}`
                      : `Tous les ${item.repetition} jours à ${item.heure}`;
                  return (
                    <ListItem key={index}>
                      <ListItemText primary={recurrence} className={classes.listItem} />
                      <ListItemSecondaryAction>
                        <NewCustomButton
                          variant="outlined"
                          theme="transparent"
                          // eslint-disable-next-line react/jsx-no-bind
                          onClick={handleRemoveTraitement.bind(null, index)}
                        >
                          <RemoveIcon />
                        </NewCustomButton>
                      </ListItemSecondaryAction>
                    </ListItem>
                  );
                })}
                <ListItem>
                  <ListItemSecondaryAction>
                    <NewCustomButton theme="transparent" variant="outlined" onClick={handleOpenTAModal}>
                      <AddIcon />
                    </NewCustomButton>
                  </ListItemSecondaryAction>
                </ListItem>
              </List>
            </Grid>
          </Fragment>
        )}
        <Grid container xs={12}>
          <Grid item xs={6}>
            <Typography component="div">
              <Box fontWeight="bold">Temporisation</Box>
            </Typography>
            <Box display="flex" flexDirection="row" justifyContent="flex-start" alignItems="center">
              <CustomFormTextField
                type="number"
                className={classes.numberField}
                onChange={handleChangeTemporisation}
                value={temporisation}
              />
              <Typography component="div">
                <Box fontWeight="bold">mn</Box>
              </Typography>
            </Box>
          </Grid>
        </Grid>
      </form>
      {openTA && (
        <TraitementAutomatiqueForm
          open={openTA}
          setOpen={handleOpenTAModal}
          mode="creation"
          filterRecurrence="Jour"
          onGetData={handleSubmitTraitementAutomatique}
          saving={false}
          // idImportance={idImportance}
          // idUrgence={idUrgence}
          // idTache={idTache}
          // commonFieldsComponent={commonFieldsComponent}
          traitementTypes={traitementTypes}
          defaultTypeId={defaultTypeId}
          isPrivate={isPrivate}
          setIsPrivate={setIsPrivate}
          state={state}
          setState={setState}
        />
      )}
    </CustomModal>
  );
};

export default FormFrigo;
