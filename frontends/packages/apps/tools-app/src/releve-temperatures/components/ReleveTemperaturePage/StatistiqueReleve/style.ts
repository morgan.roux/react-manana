import { createStyles, Theme, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    chartToolbar: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-end',
      flexWrap: 'wrap',
      [theme.breakpoints.down('sm')]: {
        overflowX: 'auto',
        flexWrap: 'nowrap',
        flexDirection: 'column',
        alignItems: 'center',
      },
    },
  })
);

export default useStyles;
