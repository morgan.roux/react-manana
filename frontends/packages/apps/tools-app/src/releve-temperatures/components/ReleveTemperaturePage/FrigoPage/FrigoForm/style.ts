import { makeStyles, createStyles } from '@material-ui/core';

const style = makeStyles(() =>
  createStyles({
    root: {
      '& .main-MuiDialogContent-root': {
        padding: '24px',
      },
    },
    listItem: {
      background: 'whitesmoke',
      padding: '10px 5px',
      marginRight: 50,
      borderRadius: 5,
    },
    numberField: {
      width: 80,
      margin: '10px 10px 10px 0',
    },
    btnSubmit: {
      marginLeft: 'auto',
      marginTop: 15,
    },
    fontWeight: {
      fontWeight: 'bold',
    },
  })
);

export default style;
