import { Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { RTReleve } from '@lib/types';
import moment from 'moment';
import { Column, CustomModal, Table, TableChange } from '@lib/ui-kit';

// import useStyles from './style';

export interface HistoriqueReleveProps {
  open: boolean;
  loading?: boolean;
  error?: Error;
  data?: RTReleve[];
  rowsTotal: number;
  nom: string;
  setOpen: (open: boolean) => void;
  onRequestSearch: (change: TableChange) => void;
}

const HistoriqueReleve: FC<HistoriqueReleveProps> = ({
  open,
  loading,
  error,
  data,
  rowsTotal,
  nom,
  setOpen,
  onRequestSearch,
}) => {
  // const classes = useStyles();
  const columns: Column[] = [
    {
      name: 'temperature',
      label: 'Température du frigo',
      renderer: (row: RTReleve) => {
        return row.status?.code === '001' ? row.status.libelle : row?.temperature ? row.temperature + '°c' : '-';
      },
    },
    {
      name: 'dateReleve',
      label: 'Date de relevé',
      colSpan: 3,
      renderer: (row: RTReleve) => {
        return row?.createdAt ? moment(row.createdAt).format('DD/MM/YYYY HH:mm:ss') : '-';
      },
    },
  ];

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Historique de relevé"
      withBtnsActions={false}
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      fullWidth
      fullScreen={true}
    >
      <Box width="100%">
        <Typography style={{ padding: 10 }}>{nom}</Typography>
        <Table
          error={error}
          loading={loading}
          search={false}
          data={data || []}
          selectable={false}
          columns={columns}
          rowsTotal={rowsTotal}
          onRunSearch={onRequestSearch}
        />
      </Box>
    </CustomModal>
  );
};

export default HistoriqueReleve;
