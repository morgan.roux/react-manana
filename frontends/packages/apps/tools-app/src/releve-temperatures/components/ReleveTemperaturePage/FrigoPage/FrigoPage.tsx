import React, { FC, useState, ReactNode, MouseEvent, useEffect } from 'react';
import { RTFrigo, TATraitementType } from '@lib/types';
import { Box, Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Edit, Delete, Add, MoreHoriz } from '@material-ui/icons';
import FrigoForm, { RTRequestFrigoSaving, TATraitementRequestSaving } from './FrigoForm/FormFrigo';
import TopBar from '@lib/ui-kit/src/components/templates/ParameterContainer/TopBar';
import { Backdrop, Column, ConfirmDeleteDialog, NewCustomButton, ErrorPage, Table } from '@lib/ui-kit';
import { TraitementAutomatiqueRequestSaving } from '../../../../../../todo-app/src/traitement-recurrent/components/TraitementAutomatiquePage/TraitementAutomatiqueForm';

export interface FrigoPageProps {
  loading?: boolean;
  error?: Error;
  data?: RTFrigo[];
  rowsTotal: number;
  frigoToEdit?: RTFrigo;

  traitementTypes: {
    loading?: boolean;
    error?: Error;
    data?: TATraitementType[];
  };
  // collaborateurs?: any[];
  // idTache?: string;
  // idImportance?: string;
  // idUrgence?: string;

  errorSaving?: boolean;
  saving?: boolean;
  saved?: boolean;
  setSaved?: (newSaved: boolean) => void;

  onRequestSaveTraitement?: (data: TraitementAutomatiqueRequestSaving) => TATraitementRequestSaving;

  // commonFieldsComponent: ReactNode;
  onRequestEdit: (frigoToEdit: RTFrigo | undefined) => void;
  onRequestSave: (frigo: RTRequestFrigoSaving) => void;
  onRequestDelete: (frigo: RTFrigo) => void;
}

const FRIGOTYPES = [
  {
    id: '1',
    value: 'MANUEL',
    libelle: 'Manuel',
  },
  {
    id: '2',
    value: 'AUTOMATIQUE',
    libelle: 'Automatique',
  },
];

const FrigoPage: FC<FrigoPageProps> = ({
  loading,
  error,
  data,
  rowsTotal,
  frigoToEdit,
  traitementTypes,

  // collaborateurs,
  // idTache,
  // idImportance,
  // idUrgence,

  errorSaving,
  saving,
  setSaved,
  saved,
  // commonFieldsComponent,
  onRequestEdit,
  onRequestSave,
  onRequestDelete,

  onRequestSaveTraitement,
}) => {
  const [openEditDialog, setOpenEditDialog] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const open = Boolean(anchorEl);

  useEffect(() => {
    if (saved) {
      setOpenEditDialog(false);
      if (setSaved) {
        setSaved(false);
      }
    }
  }, [saved]);

  const toggleOpenEditDialog = () => {
    if (!openEditDialog) {
      // if closed, set frigo to edit undefid
      onRequestEdit(undefined);
    }
    setOpenEditDialog((prev) => !prev);
  };

  const handleOpenAdd = () => {
    toggleOpenEditDialog();
    onRequestEdit(undefined);
  };

  const handleEdit = (frigo: RTFrigo) => {
    toggleOpenEditDialog();
    onRequestEdit(frigo);
  };

  const handleConfirmDeleteOne = (): void => {
    if (frigoToEdit) {
      onRequestDelete(frigoToEdit);
      setOpenDeleteDialog(false);
    }
  };

  const handleShowMenuClick = (reunion: RTFrigo, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    onRequestEdit(reunion);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const columns: Column[] = [
    {
      name: 'nom',
      label: 'Nom du Frigo',
      renderer: (row: RTFrigo) => {
        return row?.nom ? row.nom : '-';
      },
    },
    {
      name: 'typeReleve',
      label: 'Type de relevé',
      renderer: (row: RTFrigo) => {
        return row?.typeReleve ? row.typeReleve : '-';
      },
    },
    {
      name: 'temperatureLimite',
      label: 'Température Limite',
      renderer: (row: RTFrigo) => {
        return row ? row.temperatureBasse + '°c' + ' - ' + row.temperatureHaute + '°c' : '-';
      },
    },
    {
      name: 'frequenceReleve',
      label: 'Fréquence du Relevé',
      renderer: (row: RTFrigo) => {
        return row?.traitements ? (
          <div>
            {row.traitements.map((item, index) => {
              return (
                <p key={(row.id || '') + index}>
                  {item.repetition === 1
                    ? `Tous les jours, à ${item.heure}`
                    : `Tous les ${item.repetition} jours à ${item.heure}`}
                </p>
              );
            })}
          </div>
        ) : (
          ''
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: RTFrigo) => {
        return (
          <div key={`row-${row.id}`}>
            <IconButton
              aria-controls="simple-menu"
              aria-haspopup="true"
              // eslint-disable-next-line react/jsx-no-bind
              onClick={handleShowMenuClick.bind(null, row)}
            >
              <MoreHoriz />
            </IconButton>
            <Menu
              id="fade-menu"
              anchorEl={anchorEl}
              keepMounted
              open={open && row.id === frigoToEdit?.id}
              onClose={handleClose}
              TransitionComponent={Fade}
            >
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  handleEdit(row);
                }}
              >
                <ListItemIcon>
                  <Edit />
                </ListItemIcon>
                <Typography variant="inherit">Modifier</Typography>
              </MenuItem>

              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  setOpenDeleteDialog(true);
                }}
              >
                <ListItemIcon>
                  <Delete />
                </ListItemIcon>
                <Typography variant="inherit">Supprimer</Typography>
              </MenuItem>
            </Menu>
          </div>
        );
      },
    },
  ];

  if (loading) {
    return <Backdrop open={loading} value="Récupération de données..." />;
  }

  if (error) {
    return <ErrorPage />;
  }

  return (
    <>
      <Box>
        <TopBar
          title="Liste des Frigos"
          actionButtons={[
            <NewCustomButton key={`action-button-${0}`} onClick={handleOpenAdd} startIcon={<Add />}>
              AJOUTER UN FRIGO
            </NewCustomButton>,
          ]}
        />
        <Table
          error={error}
          loading={loading}
          search={false}
          data={data || []}
          selectable={false}
          columns={columns}
          rowsTotal={rowsTotal}
        />
      </Box>

      {openEditDialog && (
        <FrigoForm
          open={openEditDialog}
          setOpen={toggleOpenEditDialog}
          mode={frigoToEdit ? 'modification' : 'creation'}
          listFrigoTypes={FRIGOTYPES}
          frigo={frigoToEdit}
          onRequestSave={onRequestSave}
          traitementTypes={traitementTypes.data || []}
          // commonFieldsComponent={commonFieldsComponent}
          // collaborateurs={collaborateurs}
          // idImportance={idImportance}
          // idUrgence={idUrgence}
          saving={saving}
          errorSaving={errorSaving}
          // idTache={idTache}
          onRequestSaveTraitement={onRequestSaveTraitement}
          saved={saved}
          setSaved={setSaved}
        />
      )}
      <ConfirmDeleteDialog
        title="Suppression de Frigo"
        content="Etes-vous sûr de vouloir supprimer ce Frigo ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDeleteOne}
      />
    </>
  );
};

export default FrigoPage;
