import React, { FC, useEffect, useState } from 'react';
import '@progress/kendo-theme-default/dist/all.css';
import moment, { Moment } from 'moment';
import { RTFrigo, RTReleve } from '@lib/types';
import {
  Chart,
  ChartTitle,
  ChartSeries,
  ChartSeriesItem,
  ChartCategoryAxis,
  ChartCategoryAxisItem,
  ChartTooltip,
} from '@progress/kendo-react-charts';
import { Box, Button, ButtonGroup, IconButton } from '@material-ui/core';
import { KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons';
import { WeekStartDay } from '@progress/kendo-react-charts/dist/npm/common/property-types';
import useStyle from './style';
import { CustomModal, ErrorPage, Loader } from '@lib/ui-kit';

const GROUPES = [
  {
    code: 'hours',
    libelle: 'Heure',
  },
  {
    code: 'days',
    libelle: 'Jour',
  },
  {
    code: 'weeks',
    libelle: 'Semaine',
  },
];

export interface Criteria {
  offset: number;
  start: string;
  end: string;
  group: string;
}
export interface StatistiqueReleveProps {
  open: boolean;
  loading?: boolean;
  error?: Error;
  releves?: RTReleve[];
  nom: string;
  frigo?: RTFrigo;
  setOpen: (open: boolean) => void;
  onChangeCriteria: (frigo: RTFrigo, change: Criteria) => void;
}

const StatistiqueReleve: FC<StatistiqueReleveProps> = ({
  open,
  releves,
  loading,
  error,
  nom,
  frigo,
  setOpen,
  onChangeCriteria,
}) => {
  // 15 derniers releves
  const dernierReleves = [...(releves || [])].sort(
    (a, b) => moment(a.createdAt).valueOf() - moment(b.createdAt).valueOf()
  );
  const [group, setGroup] = useState<string>('hours');
  const [offset, setOffset] = useState<number>(-1);
  const [start, setStart] = useState<Moment>(moment().add(-1, 'hours'));
  const [end, setEnd] = useState<Moment>(moment());

  const classes = useStyle();

  useEffect(() => {
    if (frigo) {
      const now = moment();
      const start = moment(now).add(offset, group as any);
      setStart(start);
      setEnd(now);
      onChangeCriteria(frigo, {
        offset,
        group,
        start: start.toISOString(),
        end: now.toISOString(),
      });
    }
  }, [group, offset]);

  const handlePrev = () => {
    setOffset((prev) => prev - 1);
  };

  const handleNext = () => {
    setOffset((prev) => prev + 1);
  };

  const handleChangeGroup = (code: string) => {
    setOffset(-1);
    setGroup(code);
  };

  const renderTooltip = (props: any) => {
    const { point } = props;

    return (
      <span>
        {moment(point.category).format('HH:mm')} ({point.value}&deg;C)
      </span>
    );
  };

  const formatDate = (date?: Moment): string => {
    return date ? date.format('ddd DD à HH:mm') : '';
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Statistique des relevés"
      withBtnsActions={false}
      closeIcon
      headerWithBgColor
      maxWidth="md"
      fullWidth
      fullScreen={true}
    >
      <Box width={1} height={1}>
        <Box className={classes.chartToolbar}>
          <ButtonGroup color="primary" aria-label="outlined primary button group">
            {GROUPES.map(({ code, libelle }) => (
              <Button
                key={code}
                variant={group === code ? 'contained' : undefined}
                onClick={() => handleChangeGroup(code)}
              >
                {libelle}
              </Button>
            ))}
          </ButtonGroup>
          <ButtonGroup>
            <IconButton onClick={handlePrev}>
              <KeyboardArrowLeft />
            </IconButton>
            <IconButton onClick={handleNext} disabled={offset === -1}>
              <KeyboardArrowRight />
            </IconButton>
          </ButtonGroup>
        </Box>
        <Box width={1} display="flex" justifyContent="center" alignItems="center">
          {loading ? (
            <Loader />
          ) : error ? (
            <ErrorPage />
          ) : (
            <Chart zoomable={true} style={{ width: '100%' }}>
              <ChartTitle text={nom} />
              <ChartSeries>
                <ChartSeriesItem
                  zIndex={5}
                  type="line"
                  field="temperature"
                  categoryField="Date"
                  data={dernierReleves.map((releve) => ({
                    ...releve,
                    Date: moment(releve.createdAt).toDate(),
                  }))}
                  color="#00A745"
                  style="smooth"
                  markers={{ visible: false }}
                />
                <ChartSeriesItem
                  markers={{ visible: false }}
                  type="line"
                  field="temperature"
                  zIndex={0}
                  categoryField="Date"
                  data={dernierReleves.map((releve) => ({
                    ...releve,
                    Date: moment(releve.createdAt).toDate(),
                    temperature: frigo?.temperatureBasse,
                  }))}
                  color="blue"
                />

                <ChartSeriesItem
                  markers={{ visible: false }}
                  type="line"
                  field="temperature"
                  zIndex={0}
                  categoryField="Date"
                  data={dernierReleves.map((releve) => ({
                    ...releve,
                    Date: moment(releve.createdAt).toDate(),
                    temperature: frigo?.temperatureHaute,
                  }))}
                  color="#E34168"
                />
              </ChartSeries>
              <ChartCategoryAxis>
                <ChartCategoryAxisItem
                  type="date"
                  baseUnit="minutes"
                  maxDivisions={24}
                  weekStartDay={WeekStartDay.Monday}
                  title={{ text: `${formatDate(start)} - ${formatDate(end)}` }}
                />
              </ChartCategoryAxis>
              <ChartTooltip render={renderTooltip} />
            </Chart>
          )}
        </Box>
      </Box>
    </CustomModal>
  );
};

export default StatistiqueReleve;
