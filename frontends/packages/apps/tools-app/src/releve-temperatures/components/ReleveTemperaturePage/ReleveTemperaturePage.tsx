// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable react/jsx-no-bind */
import React, { FC, useState } from 'react';
// import { CardTheme } from '../../atoms';
import { Box } from '@material-ui/core';
import { HistoriqueReleve } from './HistoriqueReleve';
import { StatistiqueReleve, Criteria } from './StatistiqueReleve';
import BLUE from '@material-ui/core/colors/blue';
import { AjoutReleveForm } from './AjoutReleveForm';
import { RTFrigo, RTReleve, RTStatus } from '@lib/types';
import { RTReleveRequestSaving } from './AjoutReleveForm/AjoutReleveForm';
import { ReleveFrigo } from './ReleveFrigo';
import SwiperCore, { Pagination } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import useStyles from './style';
import 'swiper/swiper.min.css';
import 'swiper/components/navigation/navigation.min.css';
import 'swiper/components/pagination/pagination.min.css';
import moment from 'moment';
import { ErrorPage, SmallLoading, TableChange } from '@lib/ui-kit';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';

SwiperCore.use([Pagination]);

export const computeColor = (temperature: number | undefined, frigo: RTFrigo | undefined) => {
  if ((temperature || 0) > (frigo?.temperatureHaute || 0)) {
    return '#E34168';
  }
  if ((temperature || 0) < (frigo?.temperatureBasse || 0)) {
    return BLUE[500];
  }
  return '#00A745';
};

export interface Releve {
  temperature: number;
  dateReleve: Date;
}

interface ReleveTemperaturePageProps {
  loading?: boolean;
  error?: Error;
  frigos?: RTFrigo[];
  statusFrigo: {
    loading?: boolean;
    error?: Error;
    data?: RTStatus[];
  };
  releves: {
    loading?: boolean;
    error?: Error;
    data?: RTReleve[];
    total?: number;
  };
  isMobile: boolean;
  onRequestSave: (releve?: RTReleveRequestSaving) => void;
  onRequestGoBack: () => void;
  onRequestSearch: (frigo: RTFrigo, change: TableChange) => void;
  onRequestStatsReleves: (frigo: RTFrigo, criteria: Criteria) => void; // Used to request the n last releves to display in stats
  errorSaving?: boolean;
  saving?: boolean;
}

const ReleveTemperaturePage: FC<ReleveTemperaturePageProps> = ({
  frigos,
  loading,
  error,
  statusFrigo,
  errorSaving,
  saving,
  releves,
  isMobile,
  onRequestSave,
  onRequestGoBack,
  onRequestStatsReleves,
  onRequestSearch,
}) => {
  const classes = useStyles();
  const [openHistorique, setOpenHistorique] = useState<boolean>(false);
  const [openStatistique, setOpenStatistique] = useState<boolean>(false);
  const [selectedFrigo, setSelectedFrigo] = useState<RTFrigo | undefined>();
  const [openAdd, setOpenAdd] = useState<boolean>(false);

  const toggleOpenHistorique = (releveFrigos: RTFrigo | undefined) => {
    setSelectedFrigo(releveFrigos);
    setOpenHistorique(!openHistorique);
  };
  const toggleOpenStatistique = (releveFrigos: RTFrigo | undefined) => {
    setSelectedFrigo(releveFrigos);
    setOpenStatistique(!openStatistique);
    if (releveFrigos) {
      onRequestStatsReleves(releveFrigos, {
        start: moment().add(-1, 'hours').toISOString(),
        end: moment().toISOString(),
        offset: -1,
        group: 'hour',
      });
    }
  };
  const toggleOpenAdd = (releveFrigos: RTFrigo | undefined) => {
    setSelectedFrigo(releveFrigos);
    setOpenAdd(!openAdd);
  };

  if (loading || statusFrigo.loading) {
    return <SmallLoading />;
  }
  if (error || statusFrigo.error) {
    return <ErrorPage />;
  }

  const frigoList = (
    <>
      {frigos && frigos.length > 0
        ? frigos?.map((frigo, index) => {
            const releveFrigo = frigo.dernierReleve;
            return isMobile ? (
              <SwiperSlide key={frigo.id || index} className={classes.swiperFrigo}>
                <ReleveFrigo
                  releveFrigo={releveFrigo}
                  setToggleOpenHistorique={toggleOpenHistorique}
                  setToggleOpenStatistique={toggleOpenStatistique}
                  setToggleOpenAdd={toggleOpenAdd}
                  frigo={frigo}
                  statusFrigo={statusFrigo}
                  onRequestSave={onRequestSave}
                />
              </SwiperSlide>
            ) : (
              <ReleveFrigo
                releveFrigo={releveFrigo}
                setToggleOpenHistorique={toggleOpenHistorique}
                setToggleOpenStatistique={toggleOpenStatistique}
                setToggleOpenAdd={toggleOpenAdd}
                frigo={frigo}
                statusFrigo={statusFrigo}
                onRequestSave={onRequestSave}
              />
            );
          })
        : 'Liste des frigos est vide'}
    </>
  );

  return (
    <>
      <CustomContainer
        bannerBack
        filled
        bannerTitle="Relevé des Températures"
        onBackClick={onRequestGoBack}
        bannerContentStyle={{
          justifyContent: 'unset',
        }}
        bannerTitleStyle={{
          textAlign: 'center',
          flexGrow: 1,
        }}
        breakpoint="sm"
        contentStyle={isMobile ? { padding: 0, flexDirection: 'row' } : { padding: 0, margin: 'auto' }}
      >
        <Box className={classes.rooty}>
          <Box className={classes.content}>
            {isMobile ? (
              <Swiper spaceBetween={50} pagination={{ type: 'bullets', el: '.page' }}>
                {frigoList}
              </Swiper>
            ) : (
              frigoList
            )}

            <HistoriqueReleve
              onRequestSearch={(change) => selectedFrigo && onRequestSearch(selectedFrigo, change)}
              loading={releves.loading}
              error={releves.error}
              data={releves.data}
              rowsTotal={releves.total || 0}
              open={openHistorique}
              setOpen={toggleOpenHistorique.bind(null, selectedFrigo)}
              nom={selectedFrigo?.nom || 'Frigo'}
            />
            <StatistiqueReleve
              frigo={selectedFrigo as any}
              loading={releves.loading}
              error={releves.error}
              releves={releves.data}
              open={openStatistique}
              setOpen={toggleOpenStatistique.bind(null, selectedFrigo)}
              nom={selectedFrigo?.nom || 'Frigo'}
              onChangeCriteria={onRequestStatsReleves}
            />
            <AjoutReleveForm
              open={openAdd}
              setOpen={toggleOpenAdd.bind(null, selectedFrigo)}
              nom={selectedFrigo?.nom || 'Frigo'}
              frigo={selectedFrigo}
              onRequestSave={onRequestSave}
              statusFrigo={statusFrigo}
              errorSaving={errorSaving}
              saving={saving}
            />
          </Box>
        </Box>
      </CustomContainer>
      <Box display="flex" width="100%" alignItems="center" justifyContent="center">
        <div className={`page ${classes.dots}`} />
      </Box>
    </>
  );
};

export default ReleveTemperaturePage;
