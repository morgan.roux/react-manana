import React from 'react';
import loadable from '@loadable/component';
import CommandeOraleIcon from '@material-ui/icons/ShoppingBasket';
import { ModuleDefinition } from '@lib/common';
import { LocalMall } from '@material-ui/icons';

const commandeOraleModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_COMMANDE_ORALE',
      location: 'PARAMETRE',
      name: 'Commande orale',
      to: '/commande-orale',
      icon: <LocalMall />,
      preferredOrder: 200,
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },

    /* {
      id: 'FEATURE_FAVORITE_COMMANDE_ORALE',
      location: 'FAVORITE',
      name: 'La gestion des commandes orales',
      to: '/outils-digitaux/commandes-orales',
      icon: <CommandeOraleIcon />,
      preferredOrder: 10,
      activationParameters: {
        groupement: '0821',
        pharmacie: '0721',
      },
    },*/
  ],
};

export default commandeOraleModule;
