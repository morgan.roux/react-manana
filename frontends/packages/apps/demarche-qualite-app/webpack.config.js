const HtmlWebpackPlugin = require('html-webpack-plugin');
const ModuleFederationPlugin = require('webpack').container.ModuleFederationPlugin;
const path = require('path');
const deps = require('./package.json').dependencies;

const singletonDeps = Object.keys(deps).reduce((singletonResult, name) => {
  return {
    ...singletonResult,
    [name]: {
      singleton: true,
      requiredVersion: deps[name],
    },
  };
}, {});

module.exports = {
  entry: './src/index',
  mode: 'development',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    port: 3003,
  },
  watchOptions: {
    ignored: /node_modules/,
  },
  output: {
    publicPath: 'auto',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
  },
  module: {
    rules: [
      {
        test: /bootstrap\.tsx$/,
        loader: 'bundle-loader',
        options: {
          lazy: true,
        },
      },
      {
        test: /\.tsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: [
            '@babel/preset-react',
            '@babel/preset-typescript',
            ['@babel/preset-env', { useBuiltIns: 'usage', corejs: 3, targets: '> 0.25%, not dead', modules: false }],
          ],
          plugins: [
            '@babel/proposal-class-properties',
            '@babel/transform-runtime' /*, {
            "regenerator": true,
            "corejs": 3
          }]*/,
          ],
        },
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new ModuleFederationPlugin({
      name: 'demarche_qualite',
      filename: 'remoteEntry.js',
      library: { type: 'var', name: 'demarche_qualite' },
      exposes: {
        './module': './src/module',
      },
      shared: {
        ...singletonDeps,
        react: {
          eager: true,
          singleton: true,
          requiredVersion: deps.react,
        },
        'react-dom': {
          eager: true,
          singleton: true,
          requiredVersion: deps['react-dom'],
        },
        '@material-ui/core': {
          eager: true,
          singleton: true,
          requiredVersion: deps['@material-ui/core'],
        },
        '@material-ui/styles': {
          eager: true,
          singleton: true,
        },
        '@lib/ui-kit': {
          singleton: true,
        },
        '@lib/common': {
          singleton: true,
        },
      },
    }),
    new HtmlWebpackPlugin({
      favicon: './public/digital4win.png',
      template: './public/index.ejs',
    }),
  ],
};
