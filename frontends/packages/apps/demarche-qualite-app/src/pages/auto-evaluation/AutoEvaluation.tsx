import { NewCustomButton, NoItemContentImage } from '@lib/ui-kit';
import React, { FC } from 'react';
import { RouteComponentProps } from 'react-router';

const AutoEvaluation: FC<RouteComponentProps> = ({ history }) => {
  const title: string = 'Auto Evaluation';
  const subTitle: string = 'Fonction non encore disponible';

  const handleClick = (): void => {
    history.push(`/demarche-qualite`);
  };

  return (
    <NoItemContentImage
      title={title}
      subtitle={subTitle}
      children={
        <NewCustomButton theme="transparent" onClick={handleClick}>
          Retour
        </NewCustomButton>
      }
    />
  );
};

export default AutoEvaluation;
