import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      [theme.breakpoints.up('xs')]: {
        borderRadius: 6,
        background: '#F8F8F8',
        border: 'none',
      },
    },
  })
);
