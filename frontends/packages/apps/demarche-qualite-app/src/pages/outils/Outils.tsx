import React, { FC } from 'react';
import { SelectBoxItem, SelectBoxItemProps, SelectBoxPage, Banner } from '@lib/ui-kit';
import affichesIcon from '../../assets/img/demarche-qualite/outils-qualites/Affiches.svg';
import enregistrementsIcon from '../../assets/img/demarche-qualite/outils-qualites/Enregistrements.svg';
import documentsIcon from '../../assets/img/demarche-qualite/outils-qualites/Documents.svg';
import checkListIcon from '../../assets/img/demarche-qualite/outils-qualites/Check-list.svg';
import memosIcon from '../../assets/img/demarche-qualite/outils-qualites/Memos.svg';
import proceduresIcon from '../../assets/img/demarche-qualite/outils-qualites/Procedures.svg';
import compterenduIcon from '../../assets/img/demarche-qualite/outils-qualites/Compte-rendu.svg';
import ficheincidentsIcon from '../../assets/img/demarche-qualite/outils-qualites/Ficheincidents.svg';
import ficheameliorationIcon from '../../assets/img/demarche-qualite/outils-qualites/Ficheamelioration.svg';
import { useApplicationContext } from '@lib/common';
import { useHistory } from 'react-router';
import { useStyles } from './styles';

const Outils: FC<{}> = () => {
  const classes = useStyles();
  const title: string = '';
  const subTitle: string = '';
  // Le kit d'Outils Qualité rassemble des documents destinés à faciliter la mise en oeuvre des exigences du Référentiel Qualité

  const { isMobile } = useApplicationContext();

  const history = useHistory();

  const handleGoBack = (): void => {
    history.push('/demarche-qualite');
  };

  const handleRedirect = (value: string): void => {
    history.push(`/demarche-qualite/outils/${value}`);
  };

  const handleFicheRedirect = (value: string): void => {
    history.push(`/demarche-qualite/${value}`);
  };

  let data: SelectBoxItemProps[] = [
    {
      image: {
        src: `${ficheincidentsIcon}`,
        alt: 'Signalement d’Incidents',
      },

      text: 'Signalement d’Incidents',
      onClick: handleFicheRedirect.bind(null, 'fiche-incident'),
    },
    {
      image: {
        src: `${ficheameliorationIcon}`,
        alt: 'Demande d’Améliorations',
      },

      text: 'Demande d’Améliorations',
      onClick: handleFicheRedirect.bind(null, 'fiche-amelioration'),
    },

    {
      image: {
        src: `${ficheincidentsIcon}`,
        alt: 'Actions Opérationnelles',
      },

      text: 'Actions Opérationnelles',
      onClick: handleFicheRedirect.bind(null, 'action-operationnelle'),
    },
    {
      image: {
        src: `${compterenduIcon}`,
        alt: 'Suivi des Réunions',
      },

      text: 'Suivi des Réunions',
      onClick: handleFicheRedirect.bind(null, 'compte-rendu'),
    },
  ];

  // if (!isMobile) {
  //   data = [
  //     ...data,
  //     {
  //       image: {
  //         src: `${checkListIcon}`,
  //         alt: 'Check-list Button Picture',
  //       },

  //       text: 'Check-list',
  //       onClick: handleRedirect.bind(null, 'checklist'),
  //     },

  //     {
  //       image: {
  //         src: `${affichesIcon}`,
  //         alt: 'Affiches Button Picture',
  //       },

  //       text: 'Affiches',
  //       onClick: handleRedirect.bind(null, 'affiche'),
  //     },
  //     {
  //       image: {
  //         src: `${enregistrementsIcon}`,
  //         alt: 'Enregistrement Button Picture',
  //       },

  //       text: 'Enregistrements',
  //       onClick: handleRedirect.bind(null, 'enregistrement'),
  //     },
  //     {
  //       image: {
  //         src: `${documentsIcon}`,
  //         alt: 'Documents Button Picture',
  //       },

  //       text: 'Documents',
  //       onClick: handleRedirect.bind(null, 'document'),
  //     },
  //     {
  //       image: {
  //         src: `${memosIcon}`,
  //         alt: 'Mémos Button Picture',
  //       },

  //       text: 'Mémos',
  //       onClick: handleRedirect.bind(null, 'memo'),
  //     },
  //     {
  //       image: {
  //         src: `${proceduresIcon}`,
  //         alt: 'Procédures Button Picture',
  //       },

  //       text: 'Procédures',
  //       onClick: handleRedirect.bind(null, 'procedure'),
  //     },
  //   ];
  // }

  return (
    <SelectBoxPage
      title={title}
      subTitle={subTitle}
      // banner={
      //   <Banner
      //     style={{ zIndex: 1 }}
      //     titleStyle={{ width: '100%', textAlign: 'center' }}
      //     title="Outils qualités"
      //     onClick={handleGoBack}
      //   />
      // }
      innerStyle={{ maxWidth: 640 }}
    >
      <div style={{ height: 50, width: '100%' }}></div>
      {data.map((currentData: SelectBoxItemProps, index: number) => (
        <SelectBoxItem
          key={index}
          image={currentData.image}
          text={currentData.text}
          onClick={currentData.onClick}
          className={classes.root}
        />
      ))}
    </SelectBoxPage>
  );
};

export default Outils;
