import { strippedString, useApplicationContext } from '@lib/common';
import PdfIcon from '@lib/common/src/assets/img/pdfIcon';
import { useDelete_Action_OperationnelleMutation, useGet_Action_OperationnelleQuery } from '@lib/common/src/federation';
import { ConfirmDeleteDialog, CustomModal, NewCustomButton } from '@lib/ui-kit';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import { Box, Card, Chip, Divider, TextField } from '@material-ui/core';
import React, { FC, useState, MouseEvent } from 'react';
import { RouteComponentProps } from 'react-router';
import FicheIncidentForm from './../../../pages/fiche-incident/fiche-incident-form/FicheIncidentForm';
import { useDqParams } from './../../../hooks/useDqParams';
import useStyles from './style';
import { LineDetail } from '../../../components/DemarcheQualite';

const DetailsActionOperationnelleComponent: FC<RouteComponentProps> = ({ history: { push } }) => {
  const classes = useStyles();
  const { params, redirect } = useDqParams();
  const actionOperationnelleId = (params as any).id;
  const { notify, federation } = useApplicationContext();

  const { loading, data, error } = useGet_Action_OperationnelleQuery({
    variables: {
      id: actionOperationnelleId,
    },
    client: federation,
    fetchPolicy: 'network-only',
  });

  const [deleteActionOperationnelle, deletionActionOperationnelle] = useDelete_Action_OperationnelleMutation({
    onCompleted: () => {
      notify({
        message: "L'action a été supprimée avec succès !",
        type: 'success',
      });

      push('/demarche-qualite/action-operationnelle');
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant la suppression de l'action",
        type: 'error',
      });
    },
    client: federation,
  });

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [openFormModal, setOpenFormModal] = useState<boolean>(false);

  const handleDelete = (e: MouseEvent<any>) => {
    if (data?.dQActionOperationnelle?.id) {
      deleteActionOperationnelle({
        variables: {
          input: {
            id: data?.dQActionOperationnelle?.id,
          },
        },
      });
    }
  };

  const handleEdit = (e: MouseEvent<any>): void => {
    setOpenFormModal(true);
  };

  const handleCloseFormModal = (value: boolean) => {
    redirect({ action: undefined, id: undefined, view: params.view }, 'action-operationnelle');
    setOpenFormModal(value);
  };

  const handleGoBack = (): void => {
    redirect(
      {
        id: undefined,
      },
      'action-operationnelle'
    );
  };

  return (
    <>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<span>Êtes-vous sur de vouloir supprimer cette action ? </span>}
        onClickConfirm={handleDelete}
      />
      <CustomContainer
        bannerBack={true}
        filled={true}
        bannerTitle="Détails d'une action"
        onBackClick={handleGoBack}
        bannerContentStyle={{
          justifyContent: 'unset',
        }}
        bannerTitleStyle={{
          textAlign: 'center',
          flexGrow: 1,
        }}
      >
        <Box className={classes.detailsContent}>
          <Card className={classes.infoBox}>
            <Box>
              <span className={classes.span}>Informations générales</span>
              <LineDetail label="Auteur" value={data?.dQActionOperationnelle?.auteur?.fullName || '-'} />
              <LineDetail label="Suivi par" value={data?.dQActionOperationnelle?.suiviPar?.fullName || '-'} />
            </Box>
            {data?.dQActionOperationnelle?.fichiers && (
              <Box style={{ marginBottom: 20 }}>
                <span className={classes.fichierJointLabel}>Fichier(s) joint(s)</span>
                <Box>
                  {(data?.dQActionOperationnelle?.fichiers || []).map((fiche) => {
                    return (
                      <Chip
                        className={classes.ficheBox}
                        key={fiche?.publicUrl as any}
                        icon={<PdfIcon />}
                        clickable={true}
                        label={fiche?.nomOriginal as any}
                        component="a"
                        target="_blank"
                        href={fiche?.publicUrl as any}
                      />
                    );
                  })}
                </Box>
              </Box>
            )}
            <Divider />
            <Box style={{ marginBottom: 12 }}>
              <span className={classes.span}>Description</span>
              <TextField
                className={classes.descriptionField}
                fullWidth={true}
                multiline={true}
                disabled={true}
                variant="outlined"
                label="Description"
                value={strippedString(data?.dQActionOperationnelle?.description || '')}
              />
            </Box>
            <Box className={classes.buttonContainer}>
              <NewCustomButton
                className={classes.buttonContainerButton}
                onClick={() => setOpenDeleteDialog(true)}
                disabled={!!data?.dQActionOperationnelle?.reunion?.id}
              >
                Supprimer
              </NewCustomButton>
              <NewCustomButton className={classes.buttonContainerButton} onClick={handleEdit}>
                Modifier
              </NewCustomButton>
            </Box>
          </Card>
        </Box>
      </CustomContainer>
      <CustomModal
        title={params.action !== 'edit' ? 'Modification action opérationnelle' : 'Nouvelle action opérationnelle'}
        open={openFormModal}
        setOpen={handleCloseFormModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        fullScreen={true}
        maxWidth="xl"
        noDialogContent={true}
      >
        <FicheIncidentForm actionToEdit={data?.dQActionOperationnelle as any} setOpenFormModal={handleCloseFormModal} />
      </CustomModal>
    </>
  );
};

export default DetailsActionOperationnelleComponent;
