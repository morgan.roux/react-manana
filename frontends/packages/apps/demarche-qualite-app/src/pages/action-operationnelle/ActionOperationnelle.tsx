import { useApplicationContext } from '@lib/common';
import {
  useDelete_Action_OperationnelleMutation,
  useGet_Actions_OperationnellesLazyQuery,
  useGet_Action_OperationnelleLazyQuery,
  useGet_OriginesQuery,
  useGet_Statuts_Order_By_Nombre_Action_OperationnellesQuery,
  useImportancesQuery,
  useUpdate_Action_OperationnelleMutation,
  useUpdate_Action_Operationnelle_ImportanceMutation,
  useUpdate_Action_Operationnelle_StatutMutation,
  ActionOperationnelleFragment,
  Get_Actions_OperationnellesQuery,
  Get_Actions_OperationnellesQueryVariables,
} from '@lib/common/src/federation';
import { CustomModal, TableChange } from '@lib/ui-kit';
import React, { FC, useCallback, useMemo, useState } from 'react';
import { RouteComponentProps, useHistory } from 'react-router';
import { sortStatuts } from '../../utils/util';
import { endTime, importancesToImpacts, startTime } from '@lib/common';
import FicheIncidentForm from '../fiche-incident/fiche-incident-form/FicheIncidentForm';
import { CustomContainerPage } from '../../components/CustomContainerPage';
import moment from 'moment';
import { useDqParams } from './../../hooks/useDqParams';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { useCommentsLazyQuery, useUser_SmyleysLazyQuery } from '@lib/common/src/graphql';
import { uniqBy } from 'lodash';
import { ActionOperationnelle } from '../../components/DemarcheQualite';

const ActionOperationnelleComponent: FC<RouteComponentProps> = ({}) => {
  const { push } = useHistory();
  const { redirect, params } = useDqParams();
  const { currentPharmacie: pharmacie, federation, notify, graphql } = useApplicationContext();
  const idPharmacie = pharmacie.id;

  const [actionToEdit, setActionToEdit] = useState<any>();

  const loadingImportances = useImportancesQuery();

  const [loadCommentaires, loadingCommentaire] = useCommentsLazyQuery({ client: graphql });

  const [searchActionsOperationnelles, searchingActionsOperationnelles] = useGet_Actions_OperationnellesLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [loadActionOperationnelleAccueil, loadingActionOperationnelleAccueil] = useGet_Action_OperationnelleLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  const origines = useGet_OriginesQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const statuts = useGet_Statuts_Order_By_Nombre_Action_OperationnellesQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [updateActionOperationnelle] = useUpdate_Action_OperationnelleMutation({
    onCompleted: () => {
      notify({
        message: "L'action opération a été modifiée avec succès !",
        type: 'success',
      });

      statuts.refetch && statuts.refetch();
      searchingActionsOperationnelles.refetch && searchingActionsOperationnelles.refetch();
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant la modification de l'action opérationnelle",
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateActionOperationnelleImportance] = useUpdate_Action_Operationnelle_ImportanceMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: `L'impact de l'opération a été modifié`,
        type: 'success',
      });
      searchingActionsOperationnelles.refetch && searchingActionsOperationnelles.refetch();
    },
    onError: () => {
      notify({
        message: `Une erreur est survenue lors du changement de l'impact`,
        type: 'error',
      });
    },
  });

  const [updateActionOperationnelleStatut] = useUpdate_Action_Operationnelle_StatutMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: `Le statut de l'opération a été modifié`,
        type: 'success',
      });
      searchingActionsOperationnelles.refetch && searchingActionsOperationnelles.refetch();
    },
    onError: () => {
      notify({
        message: `Une erreur est survenue lors du changement du statut`,
        type: 'error',
      });
    },
  });

  const [loadUserSmyleys, loadingUserSmyleys] = useUser_SmyleysLazyQuery({ client: graphql });

  const [openFormModal, setOpenFormModal] = useState<boolean>(false);

  const importancesData = useMemo(() => {
    return importancesToImpacts(loadingImportances.data?.importances.nodes);
  }, [loadingImportances.data?.importances.nodes]);

  useDeepCompareEffect(() => {
    handleSearch();
    const data = searchingActionsOperationnelles?.data?.dQActionOperationnelles?.nodes?.length || 0;
    if (params.doc || data > 0) {
      loadCommentaires({
        variables: {
          codeItem: 'DQ_ACTION_OPERATIONNELLE',
          idItemAssocie:
            params?.doc || searchingActionsOperationnelles?.data?.dQActionOperationnelles?.nodes[1]?.id || '',
        },
      });
      loadUserSmyleys({
        variables: {
          codeItem: 'DQ_ACTION_OPERATIONNELLE',
          idSource: params.doc || '',
        },
      });
    }
    if (params.action === 'create') {
      setOpenFormModal(true);
    } else {
      setOpenFormModal(false);
    }
    if (params.doc) {
      loadActionOperationnelleAccueil({
        variables: {
          id: params.doc,
        },
      });
    }
  }, [params]);

  const handleRefetch = () => {
    searchingActionsOperationnelles.refetch && searchingActionsOperationnelles.refetch();
  };

  const [deleteActionOperationnelle] = useDelete_Action_OperationnelleMutation({
    onCompleted: () => {
      notify({
        message: "L'action opérationnelle a été supprimée avec succès !",
        type: 'success',
      });
      searchingActionsOperationnelles.refetch && searchingActionsOperationnelles.refetch();
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant la suppression de l'action opérationnelle",
        type: 'error',
      });
    },
    client: federation,
  });

  const handleGoBack = (): void => {
    push('/demarche-qualite/outils');
  };

  const handleShowDetails = (action: ActionOperationnelle): void => {
    redirect(
      {
        id: action.id,
      },
      'action-operationnelle-detail'
    );
  };

  const handleEdit = (action: ActionOperationnelle): void => {
    setActionToEdit(action);
    setOpenFormModal(true);
  };

  const handleRequestStatutChange = (currentAction?: ActionOperationnelle): void => {
    const idStatutCloture = statuts.data?.dQStatutsOrderByNombreActionOperationnelles.find((statut) => {
      return statut.code === 'CLOTURE' && statut.type === 'OPERATION';
    })?.id;
    if (currentAction) {
      updateActionOperationnelle({
        variables: {
          id: currentAction.id as any,
          update: {
            idOrigine: currentAction.origine.id,
            description: currentAction.description,
            idStatut: idStatutCloture,
            idUserAuteur: currentAction.auteur?.id as any,
            idUserSuivi: currentAction.suiviPar?.id as any,
            idImportance: currentAction.importance.id,
            idTache: currentAction.tache?.id,
            idFonction: currentAction.fonction?.id,
            idOrigineAssocie: currentAction.idOrigineAssocie,
            dateAction: currentAction.dateAction,
            idType: currentAction.type.id,
            idCause: currentAction.cause.id,
            isPrivate: currentAction.isPrivate ?? false,
            idUserParticipants: [
              ...(currentAction.participants || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_EN_CHARGE' })),
              ...(currentAction.participantsAInformer || []).map(({ id }) => ({
                idUser: id,
                type: 'PARTICIPANT_A_INFORMER',
              })),
            ],
          } as any,
        },
      });
    }
  };

  const handleRequestChangeImpact = ({ data, idItem }: any) => {
    if (data) {
      updateActionOperationnelleImportance({
        variables: {
          id: data.id,
          idItem,
        },
      });
    }
  };

  const handleRequestChangeStatut = ({ data, idItem }: any) => {
    if (data) {
      updateActionOperationnelleStatut({
        variables: {
          id: data.id,
          idItem,
        },
      });
      redirect(
        {
          doc: undefined,
        },
        'action-operationnelle'
      );
    }
  };

  const handleDelete = (action: ActionOperationnelle): void => {
    deleteActionOperationnelle({
      variables: {
        input: {
          id: action.id as any,
        },
      },
    });
  };

  const handleSearch = (): void => {
    if (params) {
      let filterList: any[] | undefined = [
        {
          idPharmacie: {
            eq: idPharmacie,
          },
        },
      ];
      if (params.idImportance) {
        filterList = [
          ...(filterList || []),
          {
            idImportance: {
              eq: params.idImportance,
            },
          },
        ];
      }

      if (params.searchText) {
        filterList = [
          ...(filterList || []),
          {
            description: {
              iLike: `%${params.searchText}%`,
            },
          },
        ];
      }
      if (params.date) {
        filterList = [
          ...(filterList || []),
          {
            dateAction: {
              between: {
                lower: startTime(moment.utc(new Date(params.date))),
                upper: endTime(moment.utc(new Date(params.date))),
              },
            },
          },
        ];
      }

      if (params.idStatut) {
        filterList = [
          ...(filterList || []),
          {
            idStatut: {
              eq: params.idStatut,
            },
          },
        ];
      }

      const parameters = filterList
        ? {
            variables: {
              filter: {
                and: filterList,
              },
              paging: {
                limit: params.take ? parseInt(params.take) : 12,
                offset: params.skip ? parseInt(params.skip) : 0,
              },
            },
          }
        : {};

      searchActionsOperationnelles(parameters);
    }
  };

  const handleCloseFormModal = (value: boolean) => {
    redirect({ action: undefined, id: undefined, view: params.view }, 'action-operationnelle');
    setOpenFormModal(value);
  };

  const handleCreateClick = () => {
    redirect(
      {
        id: undefined,
        action: 'create',
      },
      'action-operationnelle'
    );
    setActionToEdit(undefined);
  };
  const onRunSearch = (value: string) => {
    redirect(
      {
        doc: undefined,
        searchText: value,
      },
      'action-operationnelle'
    );
  };

  const handleClickFilter = (type: string, id: string) => {
    redirect(
      {
        doc: undefined,
        [type]: id,
      },
      'action-operationnelle'
    );
  };

  const onUserSelectChange = (value: any) => {
    //setFilterChange((prevFilter) => ({ ...prevFilter, idUser: value?.id }));
  };

  const handleDateFilter = (date: any, value?: any) => {
    const realDate = moment.utc(date).isValid() ? moment.utc(date).format('YYYY-MM-DD') : undefined;
    redirect(
      {
        doc: undefined,
        date: realDate,
      },
      'action-operationnelle'
    );
  };

  const handleClickListItem = (id: string) => {
    redirect(
      {
        doc: id,
      },
      'action-operationnelle'
    );
  };

  const handleSearchTable = (change: TableChange) => {
    redirect(
      {
        skip: change.skip.toString(),
        take: change.take.toString(),
      },
      'action-operationnelle'
    );
  };

  const handleGoPilotage = () => {
    push(`/pilotage-statistiques/SC`);
  };

  const handleRefecthComments = useCallback(() => {
    loadingCommentaire?.refetch && loadingCommentaire.refetch();
  }, [loadingCommentaire]);

  const handleRefecthSmyleys = () => () => {
    loadingUserSmyleys?.refetch && loadingUserSmyleys.refetch();
  };

  const commentaires = {
    total: loadingCommentaire.data?.comments?.total || 0,
    data: loadingCommentaire.data?.comments?.data as any,
    loading: loadingCommentaire.loading,
  };

  const userSmyleys = {
    data: loadingUserSmyleys.data?.userSmyleys?.data as any,
    loading: loadingUserSmyleys.loading,
  };

  return (
    <>
      <CustomContainerPage<
        ActionOperationnelleFragment,
        Get_Actions_OperationnellesQuery,
        Get_Actions_OperationnellesQueryVariables
      >
        buildVariables={(queryResult, range) => {
          return {
            ...queryResult.variables,
            paging: {
              offset: range.startIndex,
              limit: range.stopIndex + 1,
            },
          };
        }}
        updateQuery={(prev, { fetchMoreResult }) => {
          if (prev?.dQActionOperationnelles?.nodes && fetchMoreResult?.dQActionOperationnelles?.nodes) {
            const { nodes: currentData } = prev.dQActionOperationnelles;
            return {
              ...prev,
              dQActionOperationnelles: {
                ...prev.dQActionOperationnelles,
                nodes: uniqBy([...currentData, ...fetchMoreResult.dQActionOperationnelles.nodes], 'id'),
                totalCount: fetchMoreResult.dQActionOperationnelles.totalCount,
              },
            };
          }
          return prev;
        }}
        refetch={searchingActionsOperationnelles.refetch}
        queryResult={searchingActionsOperationnelles}
        extractItems={(queryResult) => queryResult.data?.dQActionOperationnelles.nodes || []}
        extractItemsTotalCount={(queryResult) => queryResult.data?.dQActionOperationnelles.totalCount || 0}
        listFilterImpact={importancesData}
        listFilterStatut={
          {
            ...statuts,
            data: sortStatuts(
              (statuts.data?.dQStatutsOrderByNombreActionOperationnelles || []).filter(
                ({ type }: any) => type === 'OPERATION'
              ) as any
            ),
          } as any
        }
        origine={{
          ...origines,
          data: origines.data?.origines.nodes || [],
        }}
        title="Actions Opérationnelles"
        onClickItemListMenu={handleClickListItem}
        onRequestGoBack={handleGoBack}
        onRunSearch={onRunSearch}
        onUserSelectChange={onUserSelectChange}
        addButtonLabel="Nouvelle action"
        dataListMenuDetail={{
          results: searchingActionsOperationnelles.data?.dQActionOperationnelles.nodes as any,
          total: searchingActionsOperationnelles.data?.dQActionOperationnelles.totalCount || 0,
          loading: searchingActionsOperationnelles.loading,
          error: searchingActionsOperationnelles.error as any,
        }}
        documentSeleceted={
          params.doc ? (loadingActionOperationnelleAccueil.data?.dQActionOperationnelle as any) : undefined
        }
        idSourceComment={
          params.doc
            ? loadingActionOperationnelleAccueil.data?.dQActionOperationnelle?.id
            : searchingActionsOperationnelles.data?.dQActionOperationnelles.nodes[1]?.id
        }
        codeItemComment={'DQ_ACTION_OPERATIONNELLE'}
        loading={searchingActionsOperationnelles.loading}
        error={!!searchingActionsOperationnelles.error}
        labelTypeTableListe="Type action"
        labelCauseTableListe="Cause action"
        labelDescriptionTableListe="Déscription"
        labelDeclarantInListMenu="Déclarant"
        labelImpactTableListe="Impact"
        labelStatusTableListe="Statut"
        labelDateDebutTableListe="Date"
        labelDateResolutionTableListe="Date de resolution"
        typeLabelDetail="Type"
        causeLabelDetail="Cause"
        titleButtonTableListe="Nouvelle action"
        labelAddMenuAction="Nouvelle action"
        titleOrigineDetail="Qui est à l'origine de cette action ?"
        titleTopBarTableListe="Liste des actions opérationnelles"
        onClickAddButton={handleCreateClick}
        onClickCloturer={handleRequestStatutChange}
        handleEditMenuAction={handleEdit}
        handleVoirDetailMenuAction={handleShowDetails as any}
        handleDeleteMenuAction={handleDelete}
        handleClickItemImpact={handleRequestChangeImpact}
        handleClickItemStatut={handleRequestChangeStatut}
        handleClickFilter={handleClickFilter}
        onChangeDatePickers={handleDateFilter}
        handleSearchTable={handleSearchTable}
        refetchComments={handleRefecthComments}
        dateFilter={params.date ? moment.utc(new Date(params.date)).toDate() : moment.utc().toDate()}
        commentaires={commentaires}
        refetchSmyleys={handleRefecthSmyleys}
        userSmyleys={userSmyleys}
        handleGoPilotage={handleGoPilotage}
        page="action"
      />
      <CustomModal
        title={params.action !== 'edit' ? 'Modification action opérationnelle' : 'Nouvelle action opérationnelle'}
        open={openFormModal}
        setOpen={handleCloseFormModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        fullScreen={true}
        noDialogContent={true}
      >
        <FicheIncidentForm
          actionToEdit={actionToEdit}
          setOpenFormModal={handleCloseFormModal}
          refetch={handleRefetch}
        />
      </CustomModal>
    </>
  );
};

export default ActionOperationnelleComponent;
