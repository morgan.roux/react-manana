import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    createButton: {
      position: 'absolute',
      bottom: 75,
      right: 16,
      background: theme.palette.primary.main,
      borderRadius: '50%',
      '& .MuiIconButton-root': {
        color: theme.palette.common.white,
      },
      '& svg': {
        fill: '#fff',
      },
    },
  })
);

export default useStyles;
