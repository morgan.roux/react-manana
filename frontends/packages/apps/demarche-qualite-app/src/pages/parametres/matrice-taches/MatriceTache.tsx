import React, { FC, useState, useEffect } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import useStyles from './style';
import { Box, IconButton, Typography } from '@material-ui/core';
import { CardTheme, CardThemeList, NoItemContentImage } from '@lib/ui-kit';
import { Add, Delete, Edit } from '@material-ui/icons';
import { TwoColumnsParameters, Backdrop, ConfirmDeleteDialog, ErrorPage } from '@lib/ui-kit';

import MatriceForm from '../../../components/parametres/MatriceTache/MatriceTacheForm/MatriceTacheForm';
import MatriceTacheFonctionDetails from '../../../components/parametres/MatriceTache/MatriceTacheFonctionDetails/MatriceTacheFonctionDetails';
import { useNiveauMatriceFonctions } from '../../../components/parametres/MatriceTache/hooks';
import { TITULAIRE_PHARMACIE, useApplicationContext } from '@lib/common';
import {
  Get_FonctionsDocument,
  Get_FonctionsQuery,
  Get_FonctionsQueryVariables,
  useCreate_FonctionMutation,
  useDelete_FonctionMutation,
  useGet_FonctionLazyQuery,
  useGet_FonctionsQuery,
  usePersonnaliser_FonctionMutation,
  useToggle_One_Fonction_ActivationMutation,
  useUpdate_FonctionMutation,
} from '@lib/common/src/federation';

const MatriceTache: FC<RouteComponentProps> = ({ history: { push }, location: { pathname }, match: { params } }) => {
  const classes = useStyles({});

  const { user, currentPharmacie: pharmacie, auth, federation, notify } = useApplicationContext();
  const isTitulairePharmacie = user?.role?.code === TITULAIRE_PHARMACIE;

  const [show, setShow] = useState<boolean>(false);
  const [showDeleteButton, setShowDeleteButton] = useState<boolean>(false);
  const [fonctionToDeleteId, setFonctionToDeleteId] = useState<string>('');
  const [mode, setMode] = useState<'creation' | 'modification'>('creation');
  const [ordre, setOrdre] = useState<number | undefined>();
  const [libelle, setLibelle] = useState<string>('');
  const [currentFonctionId, setCurrentFonctionId] = useState<string>('');
  const niveauMatriceFonctions = useNiveauMatriceFonctions(auth.isSupAdminOrIsGpmAdmin);

  const selectedFonctionId = (params as any).id;

  const { loading, error, data, refetch } = useGet_FonctionsQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
    variables: {
      idPharmacie: pharmacie.id,
      paging: {
        offset: 0,
        limit: 50,
      },
    },
  });

  const [loadFonction, loadingFonction] = useGet_FonctionLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network', // TODO : Use cache
  });

  const [addFonction, creationFonction] = useCreate_FonctionMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une s'est produite lors de l'ajout de la nouvelle fonction de Base`,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      notify({
        type: 'success',
        message: 'La fonction de Base a été ajoutée avec succès !',
      });

      refetch();
    },
    update: (cache, creationResult) => {
      if (creationResult?.data?.createOneDQMTFonction) {
        const previousList = cache.readQuery<Get_FonctionsQuery, Get_FonctionsQueryVariables>({
          query: Get_FonctionsDocument,
        })?.dQMTFonctions;

        cache.writeQuery<Get_FonctionsQuery, Get_FonctionsQueryVariables>({
          query: Get_FonctionsDocument,
          data: {
            dQMTFonctions: {
              ...(previousList || {}),
              nodes: [creationResult.data.createOneDQMTFonction, ...(previousList?.nodes || [])],
            } as any,
          },
        });
      }
    },
    client: federation,
  });

  const [updateFonction, modificationFonction] = useUpdate_FonctionMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une s'est produite lors de la mise à jour de la fonction de Base`,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      notify({
        type: 'success',
        message: 'La fonction de Base a été modifiée avec succès !',
      });
      refetch();
    },
    client: federation,
  });

  const [personaliserFonction, personnalisationFonction] = usePersonnaliser_FonctionMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de la modification de la fonction.`,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      notify({
        type: 'success',
        message: 'La fonction a été modifiée avec succès !',
      });
      refetch();
    },
    client: federation,
  });

  const [deleteFonction] = useDelete_FonctionMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une s'est produite lors de la suppression de la fonction de Base`,
      });
    },
    onCompleted: (deleteResult) => {
      setShowDeleteButton(false);
      notify({
        type: 'success',
        message: 'La fonction de Base a été supprimée avec succès !',
      });

      refetch();
      if (selectedFonctionId && selectedFonctionId === deleteResult.deleteOneDQMTFonction?.id) {
        push('/db/matrice_taches/');
      }
    },
    update: (cache, deletionResult) => {
      if (deletionResult.data?.deleteOneDQMTFonction?.id) {
        const previousList = cache.readQuery<Get_FonctionsQuery, Get_FonctionsQueryVariables>({
          query: Get_FonctionsDocument,
        })?.dQMTFonctions;

        const deleteId = deletionResult.data.deleteOneDQMTFonction.id;

        cache.writeQuery<Get_FonctionsQuery, Get_FonctionsQueryVariables>({
          query: Get_FonctionsDocument,
          data: {
            dQMTFonctions: {
              ...(previousList || {}),
              nodes: (previousList?.nodes || []).filter((item) => item?.id !== deleteId),
            } as any,
          },
        });

        if (deleteId === currentFonctionId) {
          push('db/demarche_qualite');
        }
      }
    },
    client: federation,
  });

  const [toggleFonctionActivation] = useToggle_One_Fonction_ActivationMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une s'est produite lors de la mise à jour de la fonction de Base`,
      });
    },
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'La fonction de Base a été modifiée avec succès !',
      });
    },
    client: federation,
  });

  const handleIdChange = (item: any) => {
    loadFonction({
      variables: {
        idPharmacie: pharmacie.id,
        id: item.id,
      },
    });
  };

  useEffect(() => {
    if (loadingFonction.called && loadingFonction.data?.dQMTFonction?.id) {
      push({
        pathname: `/db/matrice_taches/${loadingFonction.data.dQMTFonction.id}`,
        state: { fonction: loadingFonction.data?.dQMTFonction },
      });
    }
  }, [loadingFonction.data]);

  const handleClick = (id: string): void => {
    setFonctionToDeleteId(id);
    setShowDeleteButton(!showDeleteButton);
  };

  const reInit = () => {
    setOrdre(undefined);
    setLibelle('');
  };

  const handleChangeShow = (mode: 'creation' | 'modification', element: any = null): void => {
    setShow(!show);
    setMode(mode);
    if (element) {
      setCurrentFonctionId(element.id);
      setOrdre(element.ordre);
      setLibelle(element.libelle);
    }
  };

  const handleSubmit = (): void => {
    if (ordre) {
      if (mode === 'creation') {
        addFonction({
          variables: {
            idPharmacie: pharmacie.id,
            input: {
              dQMTFonction: {
                ordre,
                libelle,
              },
            },
          },
        });
      } else {
        if (isTitulairePharmacie) {
          personaliserFonction({
            variables: {
              idFonction: currentFonctionId,
              idPharmacie: pharmacie.id,
              input: {
                ordre,
                libelle,
              },
            },
          });
        } else {
          updateFonction({
            variables: {
              idPharmacie: pharmacie.id,
              input: {
                id: currentFonctionId,
                update: {
                  ordre,
                  libelle,
                },
              },
            },
          });
        }
      }
    }
  };

  const handleDelete = (): void => {
    deleteFonction({
      variables: {
        input: {
          id: fonctionToDeleteId,
        },
      },
    });
  };

  const handleToggleFonctionActivation = ({ id }: any): void => {
    toggleFonctionActivation({
      variables: {
        idFonction: id,
        idPharmacie: pharmacie.id,
      },
    });
  };

  if (loading || loadingFonction.loading) return <Backdrop value="Récuperation de données..." open={true} />;

  if (error) {
    return <ErrorPage />;
  }

  const fonctionHeader = (
    <div className={classes.headerContent}>
      <div>
        <Typography color="primary" style={{ fontWeight: 'bold' }}>
          Liste des fonctions de Base
        </Typography>
      </div>
      {!isTitulairePharmacie && (
        <IconButton onClick={handleChangeShow.bind(null, 'creation')}>
          <Add color="primary" />
        </IconButton>
      )}
    </div>
  );

  const listFonctions =
    data?.dQMTFonctions.nodes.length || 0 > 0 ? (
      <Box>
        {niveauMatriceFonctions === 1 && fonctionHeader}
        <CardThemeList>
          {[...(data?.dQMTFonctions.nodes || [])]
            .sort((a, b) => (a?.ordre || 0) - (b?.ordre || 0))
            .map((item: any, index: number) => {
              const result: boolean = (selectedFonctionId && item && selectedFonctionId === item.id) === true;

              return (
                <CardTheme
                  applyMinHeight={false}
                  key={item.id}
                  listItemFields={{
                    url: '/db/matrice_taches/',
                  }}
                  title={`${item.ordre}-${item.libelle}${item.active ? '' : '(désactivée)'}`}
                  setCurrentId={setCurrentFonctionId}
                  littleComment={niveauMatriceFonctions === 1 ? ' ' : `Nbre de fonctions : ${item.nombreTaches}`}
                  active={result}
                  onClick={handleIdChange.bind(null, item)}
                  moreOptions={
                    isTitulairePharmacie
                      ? [
                          {
                            menuItemLabel: {
                              title: item.active ? 'Désactiver' : 'Activer',
                              icon: <Edit />,
                            },
                            onClick: (event) => handleToggleFonctionActivation(item),
                          },

                          {
                            menuItemLabel: {
                              title: 'Modifier',
                              icon: <Edit />,
                            },
                            onClick: handleChangeShow.bind(null, 'modification', item),
                          },
                        ]
                      : [
                          {
                            menuItemLabel: {
                              title: 'Modifier',
                              icon: <Edit />,
                            },
                            onClick: handleChangeShow.bind(null, 'modification', item),
                          },
                          {
                            menuItemLabel: {
                              title: 'Supprimer',
                              icon: <Delete />,
                            },
                            onClick: handleClick.bind(null, item.id),
                          },
                        ]
                  }
                />
              );
            })}
        </CardThemeList>
        <ConfirmDeleteDialog
          key="delete-fonction"
          open={showDeleteButton}
          setOpen={setShowDeleteButton}
          onClickConfirm={handleDelete}
        />
      </Box>
    ) : (
      <NoItemContentImage
        title="Aucune fonction de Base à afficher"
        subtitle="Ajouter-en une en cliquant sur le bouton d'en haut"
      />
    );

  return (
    <>
      <MatriceForm
        saving={
          mode === 'modification'
            ? isTitulairePharmacie
              ? personnalisationFonction.loading
              : modificationFonction.loading
            : creationFonction.loading
        }
        title={mode == 'creation' ? 'Ajout de fonction de Base' : 'Modification de fonction de Base'}
        show={show}
        setShow={setShow}
        onSubmit={handleSubmit}
        ordre={ordre}
        libelle={libelle}
        setLibelle={setLibelle}
        setOrdre={setOrdre}
      />
      {niveauMatriceFonctions === 1 ? (
        listFonctions
      ) : (
        <TwoColumnsParameters leftHeaderContent={fonctionHeader} rightContent={<MatriceTacheFonctionDetails />}>
          {listFonctions}
        </TwoColumnsParameters>
      )}
    </>
  );
};

export default withRouter(MatriceTache);
