import React, { FC, useState, useEffect, ChangeEvent } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import useStyles from './style';
import { Box, IconButton, Typography } from '@material-ui/core';
import { CardTheme, CardThemeList, ITheme, NoItemContentImage } from '@lib/ui-kit';
import DetailsTheme from '../../../components/parametres/Theme/Theme';

import { Add, Delete, Edit } from '@material-ui/icons';
import { TwoColumnsParameters, Backdrop, ConfirmDeleteDialog, ErrorPage } from '@lib/ui-kit';
import ThemeAdd from '../../../components/parametres/ThemeAdd/ThemeAdd';
import { useApplicationContext } from '@lib/common';
import {
  Get_ThemesDocument,
  Get_ThemesQuery,
  Get_ThemesQueryVariables,
  useCreate_ThemeMutation,
  useDelete_ThemeMutation,
  useGet_ThemeLazyQuery,
  useGet_ThemesQuery,
  useUpdate_ThemeMutation,
} from '@lib/common/src/federation';

const DemarcheQualite: FC<RouteComponentProps> = ({ history: { push }, location: { pathname }, match: { params } }) => {
  const classes = useStyles({});

  const [item, setItem] = useState<any>();
  const [show, setShow] = useState<boolean>(false);
  const [showDeleteButton, setShowDeleteButton] = useState<boolean>(false);
  const [themeToDeleteId, setThemeToDeleteId] = useState<string>('');
  const [mode, setMode] = useState<'creation' | 'modification'>('creation');
  const [numeroOrdre, setNumeroOrdre] = useState<number | undefined>();
  const [nom, setNom] = useState<string>('');
  const [currentId, setCurrentId] = useState<string>('');

  const { notify, federation } = useApplicationContext();

  const handleNumeroOrdreChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNumeroOrdre(parseInt((event.target as HTMLInputElement).value, 10));
  };

  const handleNomChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNom((event.target as HTMLInputElement).value);
  };

  const { loading, error, data } = useGet_ThemesQuery({
    client: federation,
  });

  const [loadTheme, { called, loading: load, data: themeData, error: errorQuery }] = useGet_ThemeLazyQuery({
    client: federation,
    fetchPolicy: 'cache-first', // TODO : Use cache
  });

  const [addTheme, creationTheme] = useCreate_ThemeMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une s'est produite lors de l'ajout du nouveau thème`,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      notify({
        type: 'success',
        message: 'Le thème a été ajouté avec succès !',
      });
    },
    update: (cache, creationResult) => {
      if (creationResult?.data?.createDQTheme) {
        const previousList = cache.readQuery<Get_ThemesQuery, Get_ThemesQueryVariables>({
          query: Get_ThemesDocument,
        })?.dqThemes;

        cache.writeQuery<Get_ThemesQuery, Get_ThemesQueryVariables>({
          query: Get_ThemesDocument,
          data: {
            dqThemes: [creationResult.data.createDQTheme as any, ...(previousList || [])],
          },
        });
      }
    },
    client: federation,
  });

  const [updateTheme, modificationTheme] = useUpdate_ThemeMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une s'est produite lors de la mise à jour du thème`,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      notify({
        type: 'success',
        message: 'Le thème a été modifié avec succès !',
      });
    },
    client: federation,
  });

  const [deleteTheme] = useDelete_ThemeMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une s'est produite lors de la suppression du thème`,
      });
    },
    onCompleted: (deleteResult) => {
      setShowDeleteButton(false);
      notify({
        type: 'success',
        message: 'Le thème a été supprimé avec succès !',
      });

      const selectedThemeId = (params as any).id;

      if (selectedThemeId && selectedThemeId === deleteResult.deleteDQTheme?.id) {
        push('/db/demarche_qualite/');
      }
    },
    update: (cache, deletionResult) => {
      if (deletionResult.data && deletionResult.data.deleteDQTheme && deletionResult.data.deleteDQTheme.id) {
        const previousList = cache.readQuery<Get_ThemesQuery, Get_ThemesQueryVariables>({
          query: Get_ThemesDocument,
        })?.dqThemes;

        const deleteId = deletionResult.data.deleteDQTheme.id;

        cache.writeQuery<Get_ThemesQuery, Get_ThemesQueryVariables>({
          query: Get_ThemesDocument,
          data: {
            dqThemes: (previousList || []).filter((item) => item?.id !== deleteId),
          },
        });

        // If currentTheme is the selected theme, then back to home

        if (deleteId === currentId) {
          push('db/demarche_qualite');
        }
      }
    },
    client: federation,
  });

  const handleIdChange = (item: ITheme) => {
    loadTheme({
      variables: {
        id: item.id,
      },
    });
  };

  useEffect(() => {
    if (called) {
      if (themeData && themeData.dqTheme) {
        setItem(themeData.dqTheme);
        push({
          pathname: `/db/demarche_qualite/${themeData.dqTheme.id}`,
          state: { theme: themeData.dqTheme },
        });
      }
    }
  }, [themeData]);

  const handleClick = (id: string): void => {
    setThemeToDeleteId(id);
    setShowDeleteButton(!showDeleteButton);
  };

  const reInit = () => {
    setNumeroOrdre(undefined);
    setNom('');
  };

  const handleChangeShow = (mode: 'creation' | 'modification', element: any = null): void => {
    setShow(!show);
    setMode(mode);
    if (element) {
      setCurrentId(element.id);
      setNumeroOrdre(element.ordre);
      setNom(element.nom);
    }
  };

  // Submit ajout theme
  const handleSubmit = (): void => {
    if (mode === 'creation') {
      addTheme({
        variables: {
          themeInput: {
            ordre: numeroOrdre,
            nom: nom,
          },
        },
      });
    } else {
      updateTheme({
        variables: {
          id: currentId,
          themeInput: {
            ordre: numeroOrdre,
            nom: nom,
          },
        },
      });
    }
  };

  const handleDelete = (): void => {
    deleteTheme({
      variables: {
        id: themeToDeleteId,
      },
    });
  };

  if (loading || load) return <Backdrop value="Récuperation de données..." open={loading || load} />;

  if (error) {
    return <ErrorPage />;
  }

  return (
    <TwoColumnsParameters
      leftHeaderContent={
        <div>
          <div className={classes.headerContent}>
            <div>
              <Typography color="primary" style={{ fontWeight: 'bold' }}>
                Liste des thèmes
              </Typography>
            </div>
            <IconButton onClick={handleChangeShow.bind(null, 'creation')}>
              <Add color="primary" />
            </IconButton>
          </div>
          <ThemeAdd
            saving={mode === 'modification' ? modificationTheme.loading : creationTheme.loading}
            title={mode == 'creation' ? 'Ajout de thème' : 'Modification de thème'}
            show={show}
            setShow={setShow}
            onSubmit={handleSubmit}
            value1={numeroOrdre}
            value2={nom}
            setValue1={handleNumeroOrdreChange}
            setValue2={handleNomChange}
          />
        </div>
      }
      rightContent={<DetailsTheme />}
    >
      {!loading &&
        !error &&
        (data && data.dqThemes && data.dqThemes.length > 0 ? (
          <Box>
            <CardThemeList>
              {[...(data.dqThemes || [])]
                .sort((a, b) => (a?.ordre || 0) - (b?.ordre || 0))
                .map((item: any, index: number) => {
                  const pathName = pathname.split('/')[3];
                  const result: boolean = (pathName && item && pathName === item.id) === true;

                  return (
                    <>
                      <CardTheme
                        key={item.id}
                        listItemFields={{
                          url: '/db/demarche_qualite/',
                        }}
                        title={`${item.ordre}-${item.nom}`}
                        setCurrentId={setCurrentId}
                        littleComment={`Nbre de sous-thèmes : ${item.nombreSousThemes}`}
                        active={result}
                        onClick={handleIdChange.bind(null, item)}
                        moreOptions={[
                          {
                            menuItemLabel: {
                              title: 'Modifier',
                              icon: <Edit />,
                            },
                            onClick: handleChangeShow.bind(null, 'modification', item),
                          },
                          {
                            menuItemLabel: {
                              title: 'Supprimer',
                              icon: <Delete />,
                            },
                            onClick: handleClick.bind(null, item.id),
                          },
                        ]}
                      />
                    </>
                  );
                })}
            </CardThemeList>
            <ConfirmDeleteDialog
              key="delete-theme"
              open={showDeleteButton}
              setOpen={setShowDeleteButton}
              onClickConfirm={handleDelete}
            />
          </Box>
        ) : (
          <NoItemContentImage
            title="Aucun thème à afficher"
            subtitle="Ajouter-en un en cliquant sur le bouton d'en haut"
          />
        ))}
    </TwoColumnsParameters>
  );
};

export default withRouter(DemarcheQualite);
