import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { flexWrap: 'nowrap' },
    leftOperation: {
      minWidth: 280,
      padding: 24,
      borderRight: '2px solid #E5E5E5',
      height: 'calc(100vh - 86px)',
      overflowY: 'auto',
    },
    nested: {
      paddingLeft: theme.spacing(4),
    },
    icon: {
      fontSize: 20,
    },

    leftOperationHeader: {
      padding: 15,
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },

    leftOperationHeaderTitle: {
      display: 'block',
    },

    addIconContent: {
      display: 'block',
    },

    listItemContent: {
      marginTop: 15,
    },
    headerContent: {
      padding: '10px 10px 10px 20px',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      fontWeight: 'bold',
      alignItems: 'center',
    },

    textField: {
      display: 'block',
      marginBottom: 15,
    },

    modalContent: {
      display: 'flex',
      flexDirection: 'column',
    },
  })
);

export default useStyles;
