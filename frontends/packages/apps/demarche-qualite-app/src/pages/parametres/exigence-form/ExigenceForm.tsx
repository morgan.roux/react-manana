import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Button } from '@material-ui/core';
import { IExigence, NoItemContentImage } from '@lib/ui-kit';
import Image from '../../../assets/img/mutation-success.png';
import { useApplicationContext } from '@lib/common';
import {
  Get_ThemeDocument,
  Get_ThemeQuery,
  Get_ThemeQueryVariables,
  useCreate_ExigenceMutation,
  useGet_ExigenceLazyQuery,
  useGet_Minimal_Sous_ThemeQuery,
  useGet_Outils_Grouped_By_TypologieQuery,
  useGet_ThemeQuery,
  useUpdate_ExigenceMutation,
} from '@lib/common/src/federation';
import { ExigenceFormPage, RequestSavingExigence } from '../../../components/DemarcheQualite/ExigenceFormPage';

const ExigenceForm: FC<RouteComponentProps> = ({ match: { params }, history }) => {
  const { notify, federation } = useApplicationContext();

  const exigenceId = (params as any).id;
  const themeId = (params as any).idTheme;
  const sousThemeId = (params as any).idSousTheme;
  const isNewExigence = 'new' === exigenceId;

  const [exigence, setExigence] = useState<IExigence>();
  const [displayNotificationPanel, setDisplayNotificationPanel] = useState<boolean>(false);

  const loadTheme = useGet_ThemeQuery({
    variables: {
      id: themeId,
    },
    client: federation,
  });

  const loadSousTheme = useGet_Minimal_Sous_ThemeQuery({
    variables: {
      id: sousThemeId,
    },
    client: federation,
  });

  const loadOutils = useGet_Outils_Grouped_By_TypologieQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  const [loadExigence, { called, loading, data, error }] = useGet_ExigenceLazyQuery({
    variables: {
      id: exigenceId,
    },
    client: federation,
    fetchPolicy: 'network-only',
  });

  const [createExigence, creationExigence] = useCreate_ExigenceMutation({
    onError: () => {
      notify({
        type: 'success',
        message: isNewExigence
          ? `Une erreur s'est produite lors de l'ajout d'exigence.`
          : `Une erreur s'est produite lors de modification d'exigence.`,
      });
    },
    onCompleted: () => {
      setDisplayNotificationPanel(true);
    },
    update: (cache, creationResult) => {
      if (creationResult.data && creationResult.data.createDQExigence) {
        const previousTheme = cache.readQuery<Get_ThemeQuery, Get_ThemeQueryVariables>({
          query: Get_ThemeDocument,
          variables: {
            id: themeId,
          },
        })?.dqTheme;

        const updatedTheme = {
          ...previousTheme,
          sousThemes: ((previousTheme as any).sousThemes || []).map((sousTheme: any) => {
            if (sousTheme.id === sousThemeId) {
              return {
                ...sousTheme,
                nombreExigences: (sousTheme.nombreExigences || 0) + 1,
                exigences: [creationResult.data?.createDQExigence, ...(sousTheme.exigences || [])],
              };
            }
            return sousTheme;
          }),
        };

        cache.writeQuery<Get_ThemeQuery, Get_ThemeQueryVariables>({
          query: Get_ThemeDocument,
          variables: {
            id: themeId,
          },
          data: {
            dqTheme: updatedTheme as any,
          },
        });
      }
    },
    client: federation,
  });

  const [updateExigence, modificationExigence] = useUpdate_ExigenceMutation({
    client: federation,
    onCompleted: () => {
      setDisplayNotificationPanel(true);
    },
  });

  useEffect(() => {
    if (!isNewExigence) {
      if (!called) {
        loadExigence();
      } else if (data?.dqExigence) {
        setExigence(data.dqExigence as any); // FIXME : Fix graphql schema
      }
    }
  }, [exigenceId, loading]);

  const handleSave = (exigenceToSave: RequestSavingExigence) => {
    if (isNewExigence) {
      createExigence({
        variables: {
          idSousTheme: sousThemeId,
          exigenceInput: exigenceToSave,
        },
      });
    } else if (exigenceToSave.id) {
      updateExigence({
        variables: {
          id: exigenceToSave.id,
          exigenceInput: {
            ...(exigenceToSave as any),
            id: undefined,
          },
        },
      });
    }
  };

  const handleGoBack = () => {
    history.push(`/db/demarche_qualite/${themeId}`);
  };

  const isLoading = loadTheme.loading || loadSousTheme.loading || loadOutils.loading || (!isNewExigence && loading);
  const queryError = loadTheme.error || loadSousTheme.error || loadOutils.error || (!isNewExigence ? error : undefined);

  const isSaving = isNewExigence ? creationExigence.loading : modificationExigence.loading;
  return displayNotificationPanel ? (
    <NoItemContentImage
      src={Image}
      title={isNewExigence ? `Ajout d'exigence réussi` : `Modification d'exigence réussie`}
      subtitle={
        isNewExigence
          ? `L'exigence que vous venez de créer est maintenant dans la liste`
          : `L'exigence que vous venez de modifier est à jour.`
      }
    >
      <Button onClick={handleGoBack}>Retour</Button>
    </NoItemContentImage>
  ) : (
    <ExigenceFormPage
      key={Math.random()}
      mode={isNewExigence ? 'creation' : 'modification'}
      loading={isLoading}
      saving={isSaving}
      error={queryError}
      theme={loadTheme.data?.dqTheme as any}
      sousTheme={loadSousTheme.data?.dqSousTheme as any}
      exigence={isNewExigence ? undefined : (exigence as any)}
      outils={loadOutils.data?.dqOutilsGroupedByTypologie as any}
      onRequestSave={handleSave}
      onRequestGoBack={handleGoBack}
    />
  );
};

export default withRouter(ExigenceForm);
