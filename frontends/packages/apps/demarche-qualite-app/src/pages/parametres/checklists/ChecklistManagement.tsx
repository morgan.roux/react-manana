import React, { FC } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { useApplicationContext } from '@lib/common';
import {
  Get_ChecklistsDocument,
  Get_ChecklistsQuery,
  Get_ChecklistsQueryVariables,
  useDelete_ChecklistMutation,
  useGet_ChecklistsQuery,
} from '@lib/common/src/federation';
import { Checklist, ChecklistPage } from '../../../components/DemarcheQualite/ChecklistPage';

const ChecklistManagement: FC<RouteComponentProps> = ({ history }) => {
  const { auth, currentPharmacie: pharmacie, federation, notify } = useApplicationContext();

  const variables = !auth.isSupAdminOrIsGpmAdmin
    ? { idPharmacie: pharmacie.id, onlyChecklistsPharmacie: true }
    : undefined;

  const { loading, error, data } = useGet_ChecklistsQuery({
    client: federation,
    variables,
  });

  const [deleteChecklist, deletionChecklist] = useDelete_ChecklistMutation({
    client: federation,
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de la suppréssion du checklist`,
      });
    },

    onCompleted: () => {
      notify({
        type: 'success',
        message: 'Le checklist a été supprimé avec succès !',
      });
    },
    update: (cache, deletionResult) => {
      if (deletionResult?.data?.deleteDQChecklist?.id) {
        const previousList = cache.readQuery<Get_ChecklistsQuery, Get_ChecklistsQueryVariables>({
          query: Get_ChecklistsDocument,
          variables,
        })?.dqChecklists;

        const deletedId = deletionResult.data.deleteDQChecklist.id;

        cache.writeQuery<Get_ChecklistsQuery, Get_ChecklistsQueryVariables>({
          query: Get_ChecklistsDocument,
          variables,
          data: {
            dqChecklists: (previousList || []).filter((item) => item?.id !== deletedId),
          },
        });
      }
    },
  });

  const handleAddChecklist = () => {
    history.push(`/db/check-list-qualite/new`);
  };
  const handleEditChecklist = (checklistToEdit: Checklist) => {
    history.push(`/db/check-list-qualite/${checklistToEdit.id}`);
  };
  const handleDeleteChecklist = (checklistToDelete: Checklist) => {
    if (checklistToDelete.id) {
      deleteChecklist({
        variables: {
          id: checklistToDelete.id,
        },
      });
    }
  };

  return (
    <ChecklistPage
      onRequestClose={() => console.log('Close')}
      loading={loading || deletionChecklist.loading}
      error={error}
      data={(data?.dqChecklists || []) as any}
      onRequestAdd={handleAddChecklist}
      onRequestEdit={handleEditChecklist}
      onRequestDelete={handleDeleteChecklist}
    />
  );
};

export default withRouter(ChecklistManagement);
