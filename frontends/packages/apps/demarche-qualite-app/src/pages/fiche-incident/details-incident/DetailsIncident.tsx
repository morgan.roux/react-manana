import { useMutation } from '@apollo/client';
import { useApplicationContext } from '@lib/common';
import {
  Get_Fiche_IncidentsDocument,
  Get_Fiche_IncidentsQuery,
  Get_Fiche_IncidentsQueryVariables,
  useCollaborateursQuery,
  useCreate_SolutionMutation,
  useDelete_Fiche_IncidentMutation,
  useGet_Fiche_IncidentQuery,
  useGet_SolutionLazyQuery,
} from '@lib/common/src/federation';
import React, { FC, useEffect } from 'react';
import { RouteComponentProps } from 'react-router';
import { useDqParams } from '../../../hooks/useDqParams';
import Image from '../../../assets/img/demarche-qualite/ficheAmelioration/success.png';
import FicheIncidentForm from '../fiche-incident-form/FicheIncidentForm';
import { DetailsFicheIncident, FicheIncident } from '../../../components/DemarcheQualite';

const DetailsIncident: FC<RouteComponentProps> = ({ history: { push } }) => {
  const { notify, federation, currentPharmacie: pharmacie } = useApplicationContext();

  const { params, redirect } = useDqParams();
  const ficheIncidentId = (params as any).id;

  const [loadSolution, loadingSolution] = useGet_SolutionLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  const loadFicheIncident = useGet_Fiche_IncidentQuery({
    onCompleted: (ficheResult) => {
      if (ficheResult.dQFicheIncident?.idSolution) {
        loadSolution({
          variables: {
            id: ficheResult.dQFicheIncident.idSolution,
          },
        });
      }
    },
    client: federation,
    variables: {
      id: ficheIncidentId,
    },
    fetchPolicy: 'network-only',
  });

  const collaborateurs = useCollaborateursQuery({
    client: federation,
  });

  const [createSolution, creationSolution] = useCreate_SolutionMutation({
    onCompleted: (result) => {
      notify({
        message: 'La solution a été ajoutée avec succès',
        type: 'success',
      });

      loadSolution({
        variables: {
          id: result.createOneDQSolution.id,
        },
      });
      redirect({}, 'fiche-incident-detail');
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout de la solution",
      });
    },
    client: federation,
  });

  const [deleteFicheIncident, deletionFicheIncident] = useDelete_Fiche_IncidentMutation({
    onCompleted: () => {
      notify({
        message: "La fiche d'incident a été supprimée avec succès !",
        type: 'success',
      });

      redirect({ id: undefined, doc: undefined }, 'fiche-incident');
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant la suppression de la fiche d'incident",
        type: 'error',
      });
    },
    client: federation,
  });

  useEffect(() => {
    if (loadFicheIncident.data?.dQFicheIncident?.idSolution) {
      loadSolution({
        variables: {
          id: loadFicheIncident.data.dQFicheIncident.idSolution,
        },
      });
    }
  }, [loadFicheIncident.data]);

  const handleGoBack = (): void => {
    redirect(
      {
        id: undefined,
      },
      'fiche-incident'
    );
  };

  const handleDelete = (incident: FicheIncident): void => {
    deleteFicheIncident({
      variables: {
        input: {
          id: incident.id,
        },
      },
    });
  };

  const handleSaveSolution = ({ id }: FicheIncident, { idUser, description }: any): void => {
    createSolution({
      variables: {
        idFicheIncident: id,
        solution: {
          idUser,
          description,
        },
      },
    });
  };

  const handleEdit = (incident: FicheIncident): void => {
    redirect(
      {
        id: incident.id,
        action: 'edit',
      },
      'fiche-incident'
    );
  };

  return (
    <>
      <DetailsFicheIncident
        loading={loadFicheIncident.loading || collaborateurs.loading || deletionFicheIncident.loading}
        error={loadFicheIncident.error || collaborateurs.error}
        noContentActionImageSrc={Image}
        ficheIncident={loadFicheIncident.data?.dQFicheIncident as any}
        solution={{
          ...loadingSolution,
          data: loadingSolution.data?.dQSolution as any,
          saving: creationSolution.loading,
          errorSaving: creationSolution.error,
        }}
        onRequestGoBack={handleGoBack}
        onRequestDelete={handleDelete}
        onRequestSaveSolution={handleSaveSolution}
        onRequestEdit={handleEdit}
        collaborateurs={collaborateurs?.data?.collaborateurs as any}
      />
    </>
  );
};

export default DetailsIncident;
