import { useMutation } from '@apollo/client';
import { Backdrop } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { Redirect, RouteComponentProps, withRouter } from 'react-router';
import AssignTaskUserModal from '@lib/common/src/components/CustomSelectUser/CustomSelectUser';
import { CommonFieldsForm, OrigineInput, useApplicationContext, useUploadFiles } from '@lib/common';
import {
  Create_Fiche_AmeliorationDocument,
  Create_Fiche_IncidentDocument,
  Create_Fiche_IncidentMutation,
  useCreate_Action_OperationnelleMutation,
  useCreate_Fiche_AmeliorationMutation,
  useFiche_CausesQuery,
  useFiche_TypesQuery,
  useGet_Actions_OperationnellesQuery,
  useGet_Action_OperationnelleLazyQuery,
  useGet_Fiche_AmeliorationLazyQuery,
  useGet_Fiche_IncidentLazyQuery,
  useGet_Origines_Order_By_Nombre_Fiche_IncidentsQuery,
  useUpdate_Action_OperationnelleMutation,
  useUpdate_Fiche_AmeliorationMutation,
  useUpdate_Fiche_IncidentMutation,
} from '@lib/common/src/federation';
import { FichierInput } from '@lib/common/src/graphql';
import AvatarInput from '@lib/common/src/components/AvatarInput';
import { useDqParams } from './../../../hooks/useDqParams';
import {
  ActionOperationnelle,
  FicheAmelioration,
  FicheIncident,
  FicheIncidentFormPage,
  RequestSavingFicheIncident,
} from '../../../components/DemarcheQualite';

interface CreateIncidentProps {
  match: {
    params: {
      id: string | undefined;
      idIncident?: string;
    };
  };
  incidentToEdit?: FicheIncident;
  ameliorationToEdit?: FicheAmelioration;
  actionToEdit?: ActionOperationnelle;
  setOpenFormModal?: (value: boolean) => void;
  refetch?: () => void;
}

const FicheIncidentForm: FC<CreateIncidentProps & RouteComponentProps> = ({
  history: { push },
  location: { pathname },
  match: {
    params: { idIncident },
  },
  incidentToEdit,
  ameliorationToEdit,
  actionToEdit,
  setOpenFormModal,
  refetch,
}) => {
  const { user: currentUser, federation, notify } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const type = pathname.includes('fiche-amelioration')
    ? 'amelioration'
    : pathname.includes('action-operationnelle')
    ? 'action'
    : 'incident';

  const { redirect, params } = useDqParams();
  const { id } = params;

  const [ficheIncident, setFicheIncident] = useState<FicheIncident | undefined>();
  const [ficheAmelioration, setFicheAmelioration] = useState<FicheAmelioration | undefined>();
  const [ficheAction, setFicheAction] = useState<ActionOperationnelle | undefined>();
  const [mutationLoading, setMutationLoading] = useState<boolean>(false);
  const [idImportance, setIdImportance] = useState<string>('');
  const [tache, setTache] = useState<any>();
  const [tacheAInformer, setTacheAInformer] = useState<any>();
  const [auteur, setAuteur] = useState<any>([currentUser]);
  const [userSuivi, setUserSuivi] = useState<any>([]);
  const [userParticipants, setUserParticipants] = useState<any[]>([]);
  const [userParticipantsAInformer, setUserParticipantsAInformer] = useState<any[]>([]);
  const [openAuteurModal, setOpenAuteurModal] = useState<boolean>(false);
  const [openSuiviParModal, setOpenSuiviParModal] = useState<boolean>(false);
  const [idFicheIncident, setIdFicheIncident] = useState<string | undefined>();
  const [origine, setOrigine] = useState<any>();
  const [origineAssocie, setOrigineAssocie] = useState<any>();
  const [isPrivate, setIsPrivate] = useState<boolean>(false);

  const isNewFiche =
    ficheAction?.id ||
    ficheAmelioration?.id ||
    ficheIncident?.id ||
    incidentToEdit?.id ||
    ameliorationToEdit?.id ||
    actionToEdit?.id
      ? false
      : !id || id == 'new';

  const [loadFicheIncident, incidentQuery] = useGet_Fiche_IncidentLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  const originesQuery = useGet_Origines_Order_By_Nombre_Fiche_IncidentsQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const origines = originesQuery.data?.origines.nodes || [];

  const causesQuery = useFiche_CausesQuery({
    client: federation,
  });

  const typesQuery = useFiche_TypesQuery({
    client: federation,
  });

  const [loadFicheAmelioration, ameliorationQuery] = useGet_Fiche_AmeliorationLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  const [loadAction, actionQuery] = useGet_Action_OperationnelleLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  const loading =
    type === 'incident'
      ? incidentQuery.loading
      : type === 'action'
      ? actionQuery.loading
      : idIncident
      ? ameliorationQuery.loading || incidentQuery.loading
      : ameliorationQuery.loading;

  const called =
    type === 'incident'
      ? incidentQuery.called
      : type === 'action'
      ? actionQuery.called
      : idIncident
      ? ameliorationQuery.called || incidentQuery.called
      : ameliorationQuery.called;

  useEffect(() => {
    if (!loading) {
      if (!isNewFiche) {
        if (!called) {
          if (incidentToEdit?.id || (id && type === 'incident')) {
            loadFicheIncident({
              variables: {
                id: incidentToEdit?.id || id || '',
              },
            });
          }
          if (type === 'amelioration' && (id || ameliorationToEdit?.id)) {
            loadFicheAmelioration({
              variables: {
                id: ameliorationToEdit?.id || id || '',
              },
            });
          }
          if (type === 'action' && (id || actionToEdit?.id)) {
            loadAction({
              variables: {
                id: actionToEdit?.id || id || '',
              },
            });
          }
        } else if (
          incidentQuery.data?.dQFicheIncident ||
          ameliorationQuery.data?.dQFicheAmelioration ||
          actionQuery.data?.dQActionOperationnelle
        ) {
          const incident = incidentQuery.data?.dQFicheIncident as any;
          const amelioration = ameliorationQuery.data?.dQFicheAmelioration as any;
          const action = actionQuery.data?.dQActionOperationnelle as any;
          const data = incident || amelioration || action;
          const idFicheIncident = incident?.id;
          const tache = data?.tache;
          const fonction = data?.fonction;
          const tacheAInformer = data?.tacheAInformer;
          const fonctionAInformer = data?.fonctionAInformer;
          const declarant = data?.declarant || data?.auteur;
          const userParticipants = data?.participants;
          const userParticiapantsAInformer = data?.participantsAInformer;
          const importance = data?.importance;
          const userSuivi = data?.suiviPar;
          const origineAssocie = data?.origineAssocie;
          const origine = (origines || []).find((origine) => origine.id === data?.origine?.id);
          const isPrivate =
            incidentQuery.data?.dQFicheIncident?.isPrivate ||
            // ameliorationQuery.data?.dQFicheAmelioration?.isPrivate ||
            // actionQuery.data?.dQActionOperationnelle?.isPrivate ||
            false;

          setIsPrivate(isPrivate);
          if (action) {
            setFicheAction(action);
          }
          if (incident) setFicheIncident(incident);
          if (amelioration) {
            setFicheAmelioration(amelioration);
          }
          if (declarant) {
            setAuteur([declarant]);
          }
          if (userParticipants) {
            setUserParticipants(userParticipants);
          }
          if (userParticiapantsAInformer) {
            setUserParticipantsAInformer(userParticiapantsAInformer);
          }

          if (tache || fonction) {
            setTache({
              ...(tache || {}),
              idFonction: fonction?.id,
              idTache: tache?.id,
              id: tache?.idProjet || fonction?.idProjet,
            });
          }
          if (tacheAInformer || fonctionAInformer) {
            setTacheAInformer({
              ...(tacheAInformer || {}),
              idFonction: fonctionAInformer?.id,
              idTache: tacheAInformer?.id,
              id: tacheAInformer?.idProjet || fonctionAInformer?.idProjet,
            });
          }

          if (importance) {
            setIdImportance(importance.id);
          }
          if (idFicheIncident /*&& ameliorationToCreate*/) {
            setIdFicheIncident(idFicheIncident);
          }
          if (userSuivi) {
            setUserSuivi([userSuivi]);
          }
          if (origineAssocie) {
            setOrigineAssocie(origine?.code === 'INTERNE' ? [origineAssocie] : origineAssocie);
          }
          if (origine) {
            setOrigine(origine);
          }
        }
      } else if (type === 'amelioration' && idIncident) {
        if (!called) {
          loadFicheIncident({
            variables: {
              id: idIncident,
            },
          });
        } else {
          const result = incidentQuery.data?.dQFicheIncident as any;
          if (result) {
            setFicheIncident(result);

            const tache = result?.tache;
            const fonction = result?.fonction;
            const tacheAInformer = result?.tacheAInformer;
            const fonctionAInformer = result?.fonctionAInformer;

            const declarant = result?.declarant || result?.auteur;
            const userParticipants = result?.participants;
            const userParticipantsAInformer = result?.participantsAInformer;
            const importance = result?.importance;
            const origineAssocie = result?.origineAssocie;
            const origine = (origines || []).find((origine) => origine.id === result?.origine?.id);
            if (declarant) {
              setAuteur([declarant]);
            }
            if (userParticipants) {
              setUserParticipants(userParticipants);
            }
            if (userParticipantsAInformer) {
              setUserParticipantsAInformer(userParticipantsAInformer);
            }
            if (tache || fonction) {
              setTache({
                ...(tache || {}),
                idFonction: fonction?.id,
                idTache: tache?.id,
                id: tache?.idProjet || fonction?.idProjet,
              });
            }
            if (tacheAInformer || fonctionAInformer) {
              setTacheAInformer({
                ...(tacheAInformer || {}),
                idFonction: fonctionAInformer?.id,
                idTache: tacheAInformer?.id,
                id: tacheAInformer?.idProjet || fonctionAInformer?.idProjet,
              });
            }

            if (importance) {
              setIdImportance(importance.id);
            }
            if (origineAssocie) {
              setOrigineAssocie(origine?.code === 'INTERNE' ? [origineAssocie] : origineAssocie);
            }
            if (origine) {
              setOrigine(origine);
            }
          }
        }
      }
    }
  }, [id, idIncident, loading, origines, incidentToEdit, ameliorationToEdit, actionToEdit /*, ameliorationToCreate*/]);
  const [addFicheIncident] = useMutation<Create_Fiche_IncidentMutation, any>(
    /* ameliorationToCreate ? Create_Fiche_AmeliorationDocument :*/ Create_Fiche_IncidentDocument,
    {
      onCompleted: () => {
        notify({
          // message: `La fiche incident a été ${ameliorationToCreate ? 'transformée' : 'crée'} avec succès !`,
          message: `La fiche incident a été crée avec succès !`,
          type: 'success',
        });
        setMutationLoading(false);
        if (setOpenFormModal && refetch) {
          refetch();
          setOpenFormModal(false);
        }
        redirect({ id: undefined, action: undefined, view: params.view }, 'fiche-incident');
      },
      onError: () => {
        notify({
          message: `Des erreurs se sont survenues pendant l'ajout de la fiche d'incident'`,
          type: 'error',
        });
        setMutationLoading(false);
      },
      client: federation as any,
    }
  );

  const [addFicheAmelioration] = useCreate_Fiche_AmeliorationMutation({
    onCompleted: () => {
      notify({
        message: "La fiche d'amélioration a été crée avec succès",
        type: 'success',
      });
      setMutationLoading(false);
      if (setOpenFormModal && refetch) {
        refetch();
        setOpenFormModal(false);
      }
      redirect({ id: undefined, action: undefined }, 'fiche-amelioration');
    },
    onError: () => {
      notify({
        message: `Des erreurs se sont survenues pendant l'ajout de la fiche d'amélioration'`,
        type: 'error',
      });
      setMutationLoading(false);
    },
    client: federation,
  });

  const [addActionOperationnelle] = useCreate_Action_OperationnelleMutation({
    onCompleted: () => {
      notify({
        message: "L'action opérationnelle a été crée avec succès",
        type: 'success',
      });
      setMutationLoading(false);
      if (setOpenFormModal && refetch) {
        refetch();
        setOpenFormModal(false);
      }
      redirect({ action: undefined, id: undefined, view: params.view }, 'action-operationnelle');
    },
    onError: () => {
      notify({
        message: `Des erreurs se sont survenues pendant l'ajout de l'action opérationnelle'`,
        type: 'error',
      });
      setMutationLoading(false);
    },
    client: federation,
  });
  useGet_Actions_OperationnellesQuery;
  const [updateFicheIncident] = useUpdate_Fiche_IncidentMutation({
    onCompleted: () => {
      notify({
        message: `La fiche incident a été modifiée avec succès !`,
        type: 'success',
      });
      if (setOpenFormModal && refetch) {
        refetch();
        setOpenFormModal(false);
      }
      setMutationLoading(false);
      redirect({ id: undefined, action: undefined, view: params.view }, 'fiche-incident');
    },
    onError: () => {
      notify({
        message: `Des erreurs se sont survenues pendant la modification de la fiche d'incident`,
        type: 'error',
      });
      setMutationLoading(false);
    },
    client: federation,
  });

  const [updateFicheAmelioration] = useUpdate_Fiche_AmeliorationMutation({
    onCompleted: () => {
      notify({
        message: `La fiche d'amélioration a été modifiée avec succès !`,
        type: 'success',
      });
      if (setOpenFormModal && refetch) {
        refetch();
        setOpenFormModal(false);
      }
      setMutationLoading(false);
      redirect({ id: undefined, action: undefined, view: params.view }, 'fiche-amelioration');
    },
    onError: () => {
      notify({
        message: `Des erreurs se sont survenues pendant la modification de la fiche d'amélioration`,
        type: 'error',
      });
      setMutationLoading(false);
    },
    client: federation,
  });

  const [updateActionOperationnelle] = useUpdate_Action_OperationnelleMutation({
    onCompleted: () => {
      notify({
        message: `L' action opérationnelle a été modifiée avec succès !`,
        type: 'success',
      });
      if (setOpenFormModal && refetch) {
        refetch();
        setOpenFormModal(false);
      }
      setMutationLoading(false);
      redirect({ id: undefined, view: params.view }, 'action-operationnelle');
    },
    onError: () => {
      notify({
        message: `Des erreurs se sont survenues pendant la modification de l'action opérationnelle'`,
        type: 'success',
      });
      setMutationLoading(false);
    },
    client: federation,
  });

  const handleSave = (ficheToSave: RequestSavingFicheIncident) => {
    setMutationLoading(true);
    const fichiers = (ficheToSave.fichiers || [])
      .filter((file) => !file.size)
      .map((fichier: any) => ({
        chemin: fichier.chemin,
        nomOriginal: fichier.name,
        type: fichier.type,
      }));
    const fileToUpload = (ficheToSave.fichiers || []).filter((file) => !!file.size);

    if (fileToUpload.length > 0) {
      handleUploadFiles(fileToUpload, ficheToSave, fichiers);
    } else {
      upsertFicheIncident(ficheToSave, fichiers);
    }
  };

  const handleUploadFiles = (
    fileToUpload: File[],
    ficheToSave: RequestSavingFicheIncident,
    fichiers: FichierInput[]
  ): void => {
    uploadFiles(fileToUpload, {
      directory: 'demarche-qualite',
    })
      .then((uploadedFiles) => {
        upsertFicheIncident(ficheToSave, [...uploadedFiles, ...fichiers]);
      })
      .catch(() =>
        notify({
          message: `Des erreurs se sont survenues pendant l'upload des fichiers''`,
          type: 'error',
        })
      );
  };

  const upsertFicheIncident = (ficheToSave: Omit<RequestSavingFicheIncident, 'fichiers'>, fichiers: FichierInput[]) => {
    console.log('*************upsertFicheIncident*********************', idFicheIncident, isNewFiche, id);
    if (idFicheIncident && isNewFiche) {
      addFicheIncident({
        variables: {
          idFicheIncident,
          input: {
            description: ficheToSave.description,
            idOrigine: ficheToSave.idOrigine,
            idUserAuteur: ficheToSave.idUserDeclarant,
            fichiers,
            idImportance,
            dateIncident: ficheToSave.dateFiche,
            idFonction: tache?.idFonction,
            idTache: tache?.idTache,
            idUserParticipants: [
              ...idUserParticipants.map((idUser) => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
              ...idUserParticipantsAInformer.map((idUser) => ({
                idUser,
                type: 'PARTICIPANT_A_INFORMER',
              })),
            ],
            idFonctionAInformer: tacheAInformer?.idFonction,
            idTacheAInformer: tacheAInformer?.idTache,
            isPrivate,
          },
        },
      });
    } else {
      if (isNewFiche || id === 'new') {
        if (type === 'incident') {
          addFicheIncident({
            variables: {
              input: {
                idType: ficheToSave.idType,
                idCause: ficheToSave.idCause,
                description: ficheToSave.description,
                idOrigine: ficheToSave.idOrigine,
                idOrigineAssocie: ficheToSave.idOrigineAssocie,
                fichiers,
                idImportance,
                idFonction: tache?.idFonction,
                idTache: tache?.idTache,
                idUserDeclarant: ficheToSave.idUserDeclarant,
                idFonctionAInformer: tacheAInformer?.idFonction,
                idTacheAInformer: tacheAInformer?.idTache,
                idUserParticipants: [
                  ...idUserParticipants.map((idUser) => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
                  ...idUserParticipantsAInformer.map((idUser) => ({
                    idUser,
                    type: 'PARTICIPANT_A_INFORMER',
                  })),
                ],
                dateIncident: ficheToSave.dateFiche,
                dateFiche: undefined,
                isPrivate,
              },
            },
          });
        }
        if (type === 'amelioration') {
          addFicheAmelioration({
            variables: {
              idFicheIncident: idIncident,
              input: {
                description: ficheToSave.description,
                idOrigine: ficheToSave.idOrigine,
                idOrigineAssocie: ficheToSave.idOrigineAssocie,
                idType: ficheToSave.idType,
                idCause: ficheToSave.idCause,
                fichiers,
                idImportance,
                idFonction: tache?.idFonction,
                idUserAuteur: ficheToSave.idUserDeclarant,
                idTache: tache?.idTache,
                idUserParticipants: [
                  ...idUserParticipants.map((idUser) => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
                  ...idUserParticipantsAInformer.map((idUser) => ({
                    idUser,
                    type: 'PARTICIPANT_A_INFORMER',
                  })),
                ],
                idFonctionAInformer: tacheAInformer?.idFonction,
                idTacheAInformer: tacheAInformer?.idTache,
                dateAmelioration: ficheToSave.dateFiche,
                isPrivate,
              },
            },
          });
        }
        if (type === 'action') {
          addActionOperationnelle({
            variables: {
              input: {
                // ...(ficheToSave as any),
                description: ficheToSave.description,
                idOrigine: ficheToSave.idOrigine,
                idOrigineAssocie: ficheToSave.idOrigineAssocie,
                idType: ficheToSave.idType,
                idCause: ficheToSave.idCause,
                fichiers,
                idImportance,
                idFonction: tache?.idFonction,
                idUserSuivi: ficheToSave.idUserDeclarant,
                idUserAuteur: ficheToSave.idUserDeclarant,
                idTache: tache?.idTache,
                idUserParticipants: [
                  ...idUserParticipants.map((idUser) => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
                  ...idUserParticipantsAInformer.map((idUser) => ({
                    idUser,
                    type: 'PARTICIPANT_A_INFORMER',
                  })),
                ],
                idFonctionAInformer: tacheAInformer?.idFonction,
                idTacheAInformer: tacheAInformer?.idTache,
                dateAction: ficheToSave.dateFiche,
                isPrivate,
              },
            },
          });
        }
      } else {
        if (type === 'incident') {
          updateFicheIncident({
            variables: {
              id: ficheToSave.id as any,
              update: {
                idType: ficheToSave.idType,
                idCause: ficheToSave.idCause,
                description: ficheToSave.description,
                idOrigine: ficheToSave.idOrigine,
                idOrigineAssocie: ficheToSave.idOrigineAssocie,
                fichiers,
                idImportance,
                idFonction: tache?.idFonction,
                idTache: tache?.idTache,
                idUserDeclarant: ficheToSave.idUserDeclarant,
                idFonctionAInformer: tacheAInformer?.idFonction,
                idTacheAInformer: tacheAInformer?.idTache,
                idUserParticipants: [
                  ...idUserParticipants.map((idUser) => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
                  ...idUserParticipantsAInformer.map((idUser) => ({
                    idUser,
                    type: 'PARTICIPANT_A_INFORMER',
                  })),
                ],
                dateIncident: ficheToSave.dateFiche,
                isPrivate,
              },
            },
          });
          redirect({ action: undefined, view: params.view }, 'fiche-incident');
        }
        if (type === 'amelioration') {
          updateFicheAmelioration({
            variables: {
              id: ficheToSave.id as any,
              update: {
                description: ficheToSave.description,
                idOrigine: ficheToSave.idOrigine,
                idType: ficheToSave.idType,
                idCause: ficheToSave.idCause,
                idImportance: ficheToSave.idImportance,
                idUserParticipants: [
                  ...idUserParticipants.map((idUser) => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
                  ...idUserParticipantsAInformer.map((idUser) => ({
                    idUser,
                    type: 'PARTICIPANT_A_INFORMER',
                  })),
                ],
                fichiers,
                idUserAuteur: (ficheToSave as any).idUserAuteur || ficheToSave.idUserDeclarant,
                dateAmelioration: ficheToSave.dateFiche,
                isPrivate,
              },
            },
          });
        }
        if (type === 'action') {
          updateActionOperationnelle({
            variables: {
              id: ficheToSave.id as any,
              update: {
                description: ficheToSave.description,
                idOrigine: ficheToSave.idOrigine,
                idOrigineAssocie: ficheToSave.idOrigineAssocie,
                idType: ficheToSave.idType,
                idCause: ficheToSave.idCause,
                idImportance: ficheToSave.idImportance,
                idUserParticipants: [
                  ...idUserParticipants.map((idUser) => ({ idUser, type: 'PARTICIPANT_EN_CHARGE' })),
                  ...idUserParticipantsAInformer.map((idUser) => ({
                    idUser,
                    type: 'PARTICIPANT_A_INFORMER',
                  })),
                ],
                fichiers,
                idUserAuteur: ficheToSave.idUserDeclarant,
                idUserSuivi: ficheToSave.idUserDeclarant,
                dateAction: ficheToSave.dateFiche,
                isPrivate,
              },
            },
          });
        }
      }
    }
  };

  const handleGoBack = (): void => {
    setOpenFormModal && setOpenFormModal(false);
    type === 'action'
      ? redirect(
          {
            action: undefined,
            view: params.view,
          },
          'action-operationnelle'
        )
      : type === 'amelioration'
      ? redirect(
          {
            action: undefined,
          },
          'fiche-amelioration'
        )
      : type === 'incident'
      ? redirect({ action: undefined }, 'fiche-incident')
      : redirect({ action: undefined }, 'fiche-incident');
  };

  const handleOpenAuteur = (event: any) => {
    event.stopPropagation();
    setOpenAuteurModal(true);
  };

  const handleOpenSuiviPar = (event: any) => {
    event.stopPropagation();
    setOpenSuiviParModal(true);
  };

  const handleSelectedAuteur = (selected: any) => {
    setAuteur(selected);
  };

  const handleSelectedUserSuivi = (selected: any) => {
    setUserSuivi(selected);
  };

  const idUserDeclarant = auteur && auteur[0]?.id;
  const idUserSuivi = userSuivi && userSuivi[0]?.id;
  const idUserParticipants = userParticipants.map((userParticipant) => userParticipant.id);
  const idUserParticipantsAInformer = userParticipantsAInformer.map((userParticipant) => userParticipant.id);

  const auteurComponent = (
    <Box onClick={handleOpenAuteur} width="100%">
      <AvatarInput list={auteur} label={type === 'amelioration' ? 'Déclarant' : 'Auteur'} />
      <AssignTaskUserModal
        openModal={openAuteurModal}
        setOpenModal={setOpenAuteurModal}
        withNotAssigned={false}
        selected={auteur}
        setSelected={handleSelectedAuteur}
        title="Auteur"
        searchPlaceholder="Rechercher..."
        withAssignTeam={false}
        singleSelect={true}
      />
    </Box>
  );

  const suiviParComponent = (
    <Box onClick={handleOpenSuiviPar} width="100%">
      <AvatarInput list={userSuivi} label="Suivi Par" />
      <AssignTaskUserModal
        openModal={openSuiviParModal}
        setOpenModal={setOpenSuiviParModal}
        withNotAssigned={false}
        selected={userSuivi}
        setSelected={handleSelectedUserSuivi}
        title="Suivi Par"
        searchPlaceholder="Rechercher..."
        withAssignTeam={false}
        singleSelect={true}
      />
    </Box>
  );

  const origineInput = (
    <OrigineInput
      onChangeOrigine={setOrigine}
      onChangeOrigineAssocie={setOrigineAssocie}
      origineAssocie={origineAssocie}
      origine={origine}
    />
  );

  return (
    <>
      <Backdrop
        open={mutationLoading}
        value={`${isNewFiche || id === 'new' ? 'Création' : 'Modification'} en cours ...`}
      />
      <FicheIncidentFormPage
        type={type}
        mode={isNewFiche || id === 'new' ? 'creation' : 'modification'}
        loading={loading}
        saving={mutationLoading}
        causes={{
          data: causesQuery.data?.dQFicheCauses.nodes || [],
          error: causesQuery.error as any,
          loading: causesQuery.loading,
        }}
        types={{
          data: typesQuery.data?.dQFicheTypes.nodes || [],
          error: typesQuery.error as any,
          loading: typesQuery.loading,
        }}
        idUserSuivi={idUserSuivi}
        idFicheIncident={idFicheIncident}
        originInput={origineInput}
        idOrigine={origine ? (typeof origine === 'string' ? origine : origine?.id) : ''}
        idOrigineAssocie={
          origineAssocie
            ? typeof origineAssocie === 'string'
              ? origineAssocie
              : Array.isArray(origineAssocie)
              ? origineAssocie[0]?.id
              : origineAssocie.id
            : ''
        }
        idImportance={idImportance}
        idUserDeclarant={idUserDeclarant}
        idFonction={tache?.idFonction}
        idTache={tache?.idTache}
        idUserParticipants={idUserParticipants}
        selectAuteurComponent={auteurComponent}
        commonFieldsComponent={
          <CommonFieldsForm
            selectedUsers={userParticipants}
            urgence={null}
            urgenceProps={{
              useCode: false,
            }}
            usersModalProps={{
              withNotAssigned: false,
              searchPlaceholder: 'Rechercher...',
              withAssignTeam: false,
              singleSelect: false,
              title: 'Choix de collaborateurs',
            }}
            style={{ padding: 0 }}
            isPrivate={isPrivate}
            onChangePrivate={setIsPrivate}
            selectUsersFieldLabel="Collaborateurs"
            projet={tache}
            onChangeUsersSelection={setUserParticipants}
            onChangeProjet={(projet) => {
              setTache(projet);
            }}
            showUserInput={false}
            showSelectFoction={false}
          />
        }
        commonFieldsParticipantsAInformerComponent={
          <CommonFieldsForm
            selectedUsers={userParticipantsAInformer}
            urgence={null}
            urgenceProps={{
              useCode: false,
            }}
            style={{ padding: 0 }}
            usersModalProps={{
              withNotAssigned: false,
              searchPlaceholder: 'Rechercher...',
              withAssignTeam: false,
              singleSelect: false,
            }}
            projet={tacheAInformer}
            importance={idImportance}
            onChangeUsersSelection={setUserParticipantsAInformer}
            onChangeProjet={(projet: any) => {
              setTacheAInformer(projet);
            }}
            onChangeImportance={(importance: any) => setIdImportance(importance.id)}
            radioImportance={true}
          />
        }
        selectSuiviParComponent={suiviParComponent}
        ficheIncident={!isNewFiche || (type === 'amelioration' && idIncident) ? ficheIncident : undefined}
        ficheAmelioration={!isNewFiche ? ficheAmelioration : undefined}
        ficheAction={!isNewFiche ? ficheAction : undefined}
        onRequestGoBack={handleGoBack}
        onRequestSave={handleSave}
      />
    </>
  );
};

export default withRouter(FicheIncidentForm);
