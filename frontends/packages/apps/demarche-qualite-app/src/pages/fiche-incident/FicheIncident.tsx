import { statutsWithStyles, useApplicationContext } from '@lib/common';
import { CustomModal, TableChange } from '@lib/ui-kit';
import React, { FC, useCallback, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import FicheIncidentForm from './fiche-incident-form/FicheIncidentForm';
import {
  FicheIncidentInfoFragment,
  Get_Fiche_IncidentsQuery,
  Get_Fiche_IncidentsQueryVariables,
  useCreate_SolutionMutation,
  useDelete_Fiche_IncidentMutation,
  useGet_Fiche_IncidentLazyQuery,
  useGet_Fiche_IncidentsLazyQuery,
  useGet_Origines_Order_By_Nombre_Fiche_IncidentsQuery,
  useGet_Statuts_Order_By_Nombre_Fiche_IncidentsQuery,
  useImportancesQuery,
  useUpdate_Fiche_IncidentMutation,
  useUpdate_Fiche_Incident_StatutMutation,
} from '@lib/common/src/federation';
import { CustomContainerPage } from '../../components/CustomContainerPage';
import { useDqParams } from '../../hooks/useDqParams';
import { sortStatuts } from '../../utils/util';
import { endTime, importancesToImpacts, startTime } from '@lib/common';
import moment from 'moment';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { Solution } from '../../components/Solution/Solution';
import { SolutionRequestSaving } from '../../components/Solution/interface';
import { useCommentsLazyQuery, useUser_SmyleysLazyQuery } from '@lib/common/src/graphql';
import { uniqBy } from 'lodash';
import { useMemo } from 'react';
import { FicheIncident } from '../../components/DemarcheQualite';

const FicheIncidentManagement: FC<RouteComponentProps> = ({ history: { push } }) => {
  const { redirect, params } = useDqParams();

  const { currentPharmacie: pharmacie, federation, notify, graphql } = useApplicationContext();
  const idPharmacie = pharmacie.id;

  const loadingImportances = useImportancesQuery({
    client: federation,
  });

  const [ficheToEdit, setFicheToEdit] = useState<FicheIncident>();
  const [openCreateSolutionDialog, setOpenCreateSolutionDialog] = useState<boolean>(false);

  const importancesData = useMemo(() => {
    return importancesToImpacts(loadingImportances.data?.importances.nodes);
  }, [loadingImportances.data?.importances.nodes]);

  useDeepCompareEffect(() => {
    handleSearch();
    const data = searchingFichesIncident?.data?.dQFicheIncidents?.nodes?.length || 0;
    if (params.doc || data > 0) {
      loadCommentaires({
        variables: {
          codeItem: 'DQ_INCIDENT',
          idItemAssocie: params?.doc || searchingFichesIncident.data?.dQFicheIncidents.nodes[0].id || '',
        },
      });
      loadUserSmyleys({
        variables: {
          codeItem: 'DQ_INCIDENT',
          idSource: params.doc || '',
        },
      });
    }
    if (params.doc) {
      loadFicheIncident({
        variables: {
          id: params.doc,
        },
      });
    }
  }, [params]);

  // useEffect(() => {
  //   handleRequestSearch();
  // }, [filterChange, urgence, importances]);

  const [searchFichesIncident, searchingFichesIncident] = useGet_Fiche_IncidentsLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [loadFicheIncident, loadingFicheIncident] = useGet_Fiche_IncidentLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  const origines = useGet_Origines_Order_By_Nombre_Fiche_IncidentsQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const statuts = useGet_Statuts_Order_By_Nombre_Fiche_IncidentsQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [loadCommentaires, loadingCommentaire] = useCommentsLazyQuery({ client: graphql });
  const [loadUserSmyleys, loadingUserSmyleys] = useUser_SmyleysLazyQuery({ client: graphql });

  const handleRefetch = () => {
    origines.refetch && origines.refetch();
    statuts.refetch && statuts.refetch();
    searchingFichesIncident.refetch && searchingFichesIncident.refetch();
  };

  const [deleteFicheIncident] = useDelete_Fiche_IncidentMutation({
    onCompleted: () => {
      notify({
        message: "La fiche d'incident a été supprimée avec succès !",
        type: 'success',
      });
      handleRefetch();
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant la suppression de la fiche d'incident",
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateFicheIncident] = useUpdate_Fiche_IncidentMutation({
    onCompleted: () => {
      notify({
        message: 'La fiche incident a été modifiée avec succès !',
        type: 'success',
      });

      statuts.refetch && statuts.refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification de la fiche incident !',
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateFicheIncidentStatut] = useUpdate_Fiche_Incident_StatutMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: `Le statut du fiche d'incident a été modifié`,
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: 'Une erreur est survenue lors du changement du statut',
        type: 'error',
      });
    },
  });

  const [createSolution] = useCreate_SolutionMutation({
    onCompleted: () => {
      notify({
        message: 'La solution a été ajoutée avec succès',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout de la solution",
      });
    },
    client: federation,
  });

  const handleRequestGoBack = (): void => {
    push(`/demarche-qualite/outils`);
  };

  const handleShowDetails = (fiche: FicheIncident): void => {
    redirect(
      {
        doc: fiche.id,
        view: 'detail',
      },
      'fiche-incident'
    );
  };

  const handleClickApporterSolution = (data: any) => {
    setOpenCreateSolutionDialog(true);
    redirect({ doc: data?.id }, 'fiche-incident');
  };

  const handleRequestDelete = (fiche: FicheIncident): void => {
    deleteFicheIncident({
      variables: {
        input: {
          id: fiche.id,
        },
      },
    });
  };

  const handleEdit = (incident: FicheIncident): void => {
    redirect(
      {
        id: incident.id,
        action: 'edit',
      },
      'fiche-incident'
    );
  };

  const handleSearch = (): void => {
    if (params) {
      let filterList: any[] | undefined = [
        {
          idPharmacie: {
            eq: idPharmacie,
          },
        },
      ];
      if (params.idImportance) {
        filterList = [
          ...(filterList || []),
          {
            idImportance: {
              eq: params.idImportance,
            },
          },
        ];
      }

      if (params.searchText) {
        filterList = [
          ...(filterList || []),
          {
            description: {
              iLike: `%${params.searchText}%`,
            },
          },
        ];
      }
      if (params.date) {
        filterList = [
          ...(filterList || []),
          {
            dateIncident: {
              between: {
                lower: startTime(moment.utc(new Date(params.date))),
                upper: endTime(moment.utc(new Date(params.date))),
              },
            },
          },
        ];
      }

      if (params.idStatut) {
        filterList = [
          ...(filterList || []),
          {
            idStatut: {
              eq: params.idStatut,
            },
          },
        ];
      }

      const parameters = filterList
        ? {
            variables: {
              filter: {
                and: filterList,
              },
              paging: {
                limit: params.take ? parseInt(params.take) : 12,
                offset: params.skip ? parseInt(params.skip) : 0,
              },
            },
          }
        : {};

      searchFichesIncident(parameters);
    }
  };

  const handleImportanceChange = (currentIncident: FicheIncident, newId: string): void => {
    updateFicheIncident({
      variables: {
        id: currentIncident.id,
        update: {
          idStatut: currentIncident.statut.id,
          idOrigine: currentIncident.origine.id,
          idUserDeclarant: currentIncident.declarant.id,
          description: currentIncident.description,
          idImportance: newId,
          idFonction: currentIncident.fonction?.id,
          idTache: currentIncident.tache?.id,
          idUserParticipants: [
            ...(currentIncident.participants || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_EN_CHARGE' })),
            ...(currentIncident.participantsAInformer || []).map(({ id }) => ({
              idUser: id,
              type: 'PARTICIPANT_A_INFORMER',
            })),
          ],
          dateIncident: currentIncident.dateIncident,
          idOrigineAssocie: currentIncident.idOrigineAssocie,
          idType: currentIncident.type.id,
          idCause: currentIncident.cause.id,
          //TODO:
          isPrivate: currentIncident.isPrivate,
        },
      },
    });
  };

  // const handleRequestOpenFormModal = (fiche: FicheIncident | undefined) => {
  //   if (fiche) {
  //     setFicheToEdit(fiche);
  //     setOpenFormModal(true);
  //   }
  // };

  // const handleRequestOpenAmeliorationFormModal = (fiche: FicheIncident | undefined) => {
  //   if (fiche) {
  //     setAmeliorationToCreate(fiche);
  //     setOpenFormModal(true);
  //   }
  // };

  const commentaires = {
    total: loadingCommentaire.data?.comments?.total || 0,
    data: loadingCommentaire.data?.comments?.data as any,
    loading: loadingCommentaire.loading,
  };
  const userSmyleys = {
    data: loadingUserSmyleys.data?.userSmyleys?.data as any,
    loading: loadingUserSmyleys.loading,
  };

  // New pages
  const handleRefecthComments = useCallback(() => {
    loadingCommentaire?.refetch && loadingCommentaire.refetch();
  }, [loadingCommentaire]);

  const handleRefecthSmyleys = () => () => {
    loadingUserSmyleys?.refetch && loadingUserSmyleys.refetch();
  };

  const handleCreateClick = () => {
    redirect(
      {
        id: undefined,
        action: 'create',
      },
      'fiche-incident'
    );
    setFicheToEdit(undefined);
  };

  const handleSaveSolution = (data: SolutionRequestSaving): void => {
    createSolution({
      variables: {
        idFicheIncident: data.idItem,
        solution: {
          idUser: data.idUser,
          description: data.description,
        },
      },
    });
  };

  const handleCloseFormModal = (value: boolean) => {
    redirect({ action: undefined, id: undefined, view: params.view }, 'fiche-incident');
    setFicheToEdit(undefined);
  };
  const onRunSearch = (value: string) => {
    redirect(
      {
        searchText: value,
      },
      'fiche-incident'
    );
  };

  const handleClickListItem = (id: string) => {
    redirect(
      {
        doc: id,
      },
      'fiche-incident'
    );
  };

  const handleDateFilter = (date: any, value?: any) => {
    const realDate = moment.utc(date).isValid() ? moment.utc(date).format('YYYY-MM-DD') : undefined;
    redirect(
      {
        date: realDate,
      },
      'fiche-incident'
    );
  };

  const handleClickFilter = (type: string, id: string) => {
    redirect(
      {
        [type]: id,
      },
      'fiche-incident'
    );
  };

  const handleSearchTable = (value: TableChange) => {
    redirect(
      {
        skip: value.skip.toString(),
        take: value.take.toString(),
      },
      'fiche-incident'
    );
  };

  const handleRequestChangeImpact = ({ data, idItem }: any) => {
    if (idItem && data) {
      handleImportanceChange(data, idItem);
    }
  };

  const handleRequestChangeStatut = ({ data, idItem }: any) => {
    const statutCode = (statuts.data?.dQStatutsOrderByNombreFicheIncidents || []).find((el) => {
      return el.id === idItem;
    })?.code;
    if (idItem && data && statutCode) {
      updateFicheIncidentStatut({
        variables: {
          id: data.id,
          code: statutCode,
        },
      });
      // redirect(
      //   {
      //     doc: undefined,
      //   },
      //   'fiche-incident'
      // );
    }
  };

  const handleRequestChangeCloture = (data?: FicheIncident, id?: string) => {
    if (id && data) {
      updateFicheIncidentStatut({
        variables: {
          id: data.id,
          code: 'CLOTURE',
        },
      });
    }
  };

  const handleGoPilotage = () => {
    push(`/pilotage-statistiques/SB`);
  };

  return (
    <>
      <CustomContainerPage<FicheIncidentInfoFragment, Get_Fiche_IncidentsQuery, Get_Fiche_IncidentsQueryVariables>
        buildVariables={(queryResult, range) => {
          return {
            ...queryResult.variables,
            paging: {
              offset: range.startIndex,
              limit: range.stopIndex + 1,
            },
          };
        }}
        updateQuery={(prev, { fetchMoreResult }) => {
          if (prev?.dQFicheIncidents?.nodes && fetchMoreResult?.dQFicheIncidents?.nodes) {
            const { nodes: currentData } = prev.dQFicheIncidents;
            return {
              ...prev,
              dQFicheIncidents: {
                ...prev.dQFicheIncidents,
                nodes: uniqBy([...currentData, ...fetchMoreResult.dQFicheIncidents.nodes], 'id'),
                totalCount: fetchMoreResult.dQFicheIncidents.totalCount,
              },
            };
          }
          return prev;
        }}
        refetch={searchingFichesIncident.refetch}
        queryResult={searchingFichesIncident}
        extractItems={(queryResult) => queryResult.data?.dQFicheIncidents.nodes || []}
        extractItemsTotalCount={(queryResult) => queryResult.data?.dQFicheIncidents.totalCount || 0}
        listFilterImpact={importancesData}
        listFilterStatut={
          {
            ...statuts,
            data: sortStatuts(
              statutsWithStyles(statuts.data?.dQStatutsOrderByNombreFicheIncidents || []).filter(
                ({ type }: any) => type === 'INCIDENT'
              ) as any
            ),
          } as any
        }
        title="Signalement d'Incidents"
        onRequestGoBack={handleRequestGoBack}
        onRunSearch={onRunSearch}
        addButtonLabel="Nouvel incident"
        dataListMenuDetail={{
          results: searchingFichesIncident.data?.dQFicheIncidents.nodes as any,
          loading: searchingFichesIncident.loading,
          total: searchingFichesIncident.data?.dQFicheIncidents.totalCount || 0,
          error: searchingFichesIncident.error as any,
        }}
        documentSeleceted={params.doc ? (loadingFicheIncident.data?.dQFicheIncident as any) : undefined}
        loading={searchingFichesIncident.loading}
        error={searchingFichesIncident.error as any}
        labelTypeTableListe="Type incident"
        labelCauseTableListe="Cause incident"
        labelDescriptionTableListe="Description"
        labelDeclarantInListMenu="Déclarant"
        labelImpactTableListe="Impact"
        labelStatusTableListe="Statut"
        labelDateDebutTableListe="Date"
        labelDateResolutionTableListe="Date de résolution"
        titleButtonTableListe="Nouvel incident"
        titleTopBarTableListe="Liste des incidents"
        onClickAddButton={handleCreateClick}
        origine={{
          ...origines,
          data: origines.data?.origines.nodes || [],
        }}
        labelAddMenuAction="Ajouter nouvel incident"
        handleVoirDetailMenuAction={handleShowDetails as any}
        handleEditMenuAction={handleEdit as any}
        handleDeleteMenuAction={handleRequestDelete}
        dateFilter={params.date ? moment.utc(new Date(params.date)).toDate() : moment.utc().toDate()}
        onChangeDatePickers={handleDateFilter}
        handleClickFilter={handleClickFilter}
        onClickCloturer={handleRequestChangeCloture}
        handleApporterSolutionMenuAction={handleClickApporterSolution}
        handleClickItemImpact={handleRequestChangeImpact}
        handleClickItemStatut={handleRequestChangeStatut}
        onClickItemListMenu={handleClickListItem}
        codeItemComment={'DQ_INCIDENT'}
        refetchComments={handleRefecthComments}
        commentaires={commentaires}
        idSourceComment={params.doc ? loadingFicheIncident.data?.dQFicheIncident?.id : undefined}
        refetchSmyleys={handleRefecthSmyleys}
        userSmyleys={userSmyleys}
        handleGoPilotage={handleGoPilotage}
        handleSearchTable={handleSearchTable}
        page="incident"
      />

      <CustomModal
        title={
          /* ameliorationToCreate ? 'Transformation incident' :*/ ficheToEdit
            ? 'Modification incident'
            : 'Nouvel incident'
        }
        open={params.action === 'create' || params.action === 'edit'}
        setOpen={handleCloseFormModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        fullScreen={true}
        noDialogContent={true}
      >
        <FicheIncidentForm
          // ameliorationToCreate={ameliorationToCreate}
          setOpenFormModal={handleCloseFormModal}
          refetch={handleRefetch}
        />
      </CustomModal>
      <Solution
        openCreateSolutionDialog={openCreateSolutionDialog}
        setOpenCreateSolutionDialog={setOpenCreateSolutionDialog}
        onRequestSaveSolution={handleSaveSolution}
        idItem={params.doc}
        type="amelioration"
      />
    </>
  );
};

export default withRouter(FicheIncidentManagement);
