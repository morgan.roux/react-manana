import { useApplicationContext } from '@lib/common';
import { useGet_Full_Sous_ThemeLazyQuery, useGet_Full_ThemesQuery } from '@lib/common/src/federation';
import { ISousTheme, NoItemContentImage } from '@lib/ui-kit';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ExigenceComment } from '../../components/ExigenceComment';
import FilterAlt from '../../assets/img/demarche-qualite/filter_alt_white.svg';
import { ReferentielQualitePage } from '../../components/DemarcheQualite/ReferentielQualitePage';
const ReferentielQualite: FC<RouteComponentProps> = ({ history }) => {
  const { federation } = useApplicationContext();
  const { loading, error, data } = useGet_Full_ThemesQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  const [loadSousTheme, loadingSousTheme] = useGet_Full_Sous_ThemeLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  const onRequestGoBack = (): void => {
    history.push('/demarche-qualite');
  };

  const onRequestExigences = (sousTheme: ISousTheme) => {
    loadSousTheme({
      variables: {
        id: sousTheme.id,
      },
    });
  };

  const handleOutilClick = (typologie: string, id: string) => {
    if (typologie === 'checklist') {
      history.push(`/demarche-qualite/outils/checklist`);
    } else {
      history.push(`/demarche-qualite/outils/${typologie}/${id}`);
    }
  };

  console.log('+++data', data?.dqThemes || ([] as any));

  return (
    <ReferentielQualitePage
      exigenceCommentComponentRenderer={(exigence) => <ExigenceComment exigenceId={exigence.id} />}
      loadingThemes={loading}
      themes={data?.dqThemes || ([] as any)}
      errorLoadingThemes={error}
      exigences={loadingSousTheme.data?.dqSousTheme?.exigences || ([] as any)}
      loadingExigences={loadingSousTheme.called && loadingSousTheme.loading}
      errorLoadingExigences={loadingSousTheme.called ? loadingSousTheme.error : undefined}
      onRequestGoBack={onRequestGoBack}
      onRequestExigences={onRequestExigences}
      onOutilClick={handleOutilClick}
      filterIcon={<img src={FilterAlt} alt="filter icon" />}
      noItemSelectedComponent={
        <NoItemContentImage
          title="Aucun élément séléctionné."
          subtitle="Séléctionner un sous-thème de la partie gauche pour voir le contenu dans cette partie."
        />
      }
      emptyListComponent={
        <NoItemContentImage
          title="Aucun thème à afficher."
          subtitle="Ajouter-en un en allant dans la page de création de celui-ci."
        />
      }
    />
  );
};

export default withRouter(ReferentielQualite);
