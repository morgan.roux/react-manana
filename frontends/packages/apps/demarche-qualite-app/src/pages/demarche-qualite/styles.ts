import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      [theme.breakpoints.up('xs')]: {
        boxShadow: '0px 0px 4px 0px rgba(0,0,0,0.25)',
      },
    },
  })
);
