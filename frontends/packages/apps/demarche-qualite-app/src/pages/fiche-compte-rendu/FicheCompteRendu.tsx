import { endTime, startTime, useApplicationContext } from '@lib/common';
import {
  DqReunionFilter,
  Get_ReunionsDocument,
  Get_ReunionsQuery,
  Get_ReunionsQueryVariables,
  ReunionInfoFragment,
  useDelete_ReunionMutation,
  useGet_OriginesQuery,
  useGet_ReunionLazyQuery,
  useGet_ReunionsLazyQuery,
  useGet_Reunion_AggregateQuery,
} from '@lib/common/src/federation';
import { CustomModal, TableChange } from '@lib/ui-kit';
import React, { FC, useCallback, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import FilterAlt from '../../assets/img/demarche-qualite/filter_alt_white.svg';
import FicheCompteRenduMobileForm from './fiche-compte-rendu-mobile-form/FicheCompteRenduMobileForm';
import { ImportanceInterface } from '../../components/ImportanceFilter';
import { UrgenceInterface } from '../../components/UrgenceFilter';
import { CustomContainerPage } from '../../components/CustomContainerPage';
import { CompteRenduTable } from './CompteRenduTable';
import { useDqParams } from './../../hooks/useDqParams';
import moment from 'moment';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { uniqBy } from 'lodash';
import { Reunion } from '../../components/DemarcheQualite/CompteRenduFormPage/types';
import { useCommentsLazyQuery, useUser_SmyleysLazyQuery } from '@lib/common/src/graphql';

const FicheCompteRendu: FC<RouteComponentProps> = ({ history: { push } }) => {
  const { federation, notify, isMobile, graphql } = useApplicationContext();
  const { redirect, params } = useDqParams();

  const [urgence, setUrgence] = useState<UrgenceInterface[] | null | undefined>();

  const [etiquette, setEtiquette] = useState<ImportanceInterface[] | null | undefined>();

  const [openFormModal, setOpenFormModal] = useState<boolean>(false);

  const [ficheToEdit, setFicheToEdit] = useState<Reunion>();

  const [currentReunions, setCurrentReunions] = useState<any[]>();

  const [loadUserSmyleys, loadingUserSmyleys] = useUser_SmyleysLazyQuery({ client: graphql });
  const [loadCommentaires, loadingCommentaire] = useCommentsLazyQuery({ client: graphql });

  useEffect(() => {
    const urgenceIds = urgence
      ?.filter((urgence) => urgence && urgence.id && urgence.checked)
      .map((urgence) => urgence && urgence.id) as any;
    const etiquetteIds = etiquette?.filter((etiquette) => etiquette.checked).map((etiquette) => etiquette.id);

    let variables = searchingReunions.variables;

    //urgence filters
    if (urgenceIds && urgenceIds.length > 0) {
      const filterList = addFilter(
        variables?.filter?.and,
        { 'typeAssocie.idUrgence': { in: urgenceIds } },
        'idUrgence'
      );
      variables = { ...variables, filter: { and: filterList } };
    } else {
      const cleared = clearFilter(variables?.filter?.and, 'idUrgence');
      variables = { ...variables, filter: { and: cleared } };
    }

    //etiquette filters
    if (etiquetteIds && etiquetteIds.length > 0) {
      const filterList = addFilter(variables?.filter?.and, { idEtiquette: { in: etiquetteIds } }, 'idEtiquette');
      variables = { ...variables, filter: { and: filterList } };
    } else {
      const cleared = clearFilter(variables?.filter?.and, 'idEtiquette');
      variables = { ...variables, filter: { and: cleared } };
    }

    searchReunions({ variables });
  }, [urgence, etiquette]);

  const clearFilter = (filterList: DqReunionFilter[] | null | undefined, key: string) => {
    if (filterList && filterList.length > 0) {
      return filterList.filter((filterItem) => !(filterItem as any)[key]);
    }
  };

  const addFilter = (filterList: DqReunionFilter[] | null | undefined, object: any, key: string) => {
    if (filterList && filterList.length > 0) {
      const cleared = clearFilter(filterList, key);
      return cleared?.concat(object) || [];
    } else {
      return [object];
    }
  };

  const origines = useGet_OriginesQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [searchReunions, searchingReunions] = useGet_ReunionsLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const { data: reunionAgregateData, refetch: reunionAgregateRefetch } = useGet_Reunion_AggregateQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [deleteCompteRendu] = useDelete_ReunionMutation({
    onCompleted: () => {
      notify({
        message: 'La réunion a été supprimé avec succès',
        type: 'success',
      });

      searchingReunions.refetch && searchingReunions.refetch();
      reunionAgregateRefetch();
      redirect(
        {
          doc: undefined,
        },
        'compte-rendu'
      );
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression de la réunion !',
        type: 'error',
      });
    },
    update: (cache, deletionResult) => {
      if (deletionResult?.data?.deleteOneDQReunion?.id) {
        const previousList = cache.readQuery<Get_ReunionsQuery, Get_ReunionsQueryVariables>({
          query: Get_ReunionsDocument,
        })?.dQReunions;

        const deletedId = deletionResult.data.deleteOneDQReunion.id;

        cache.writeQuery<Get_ReunionsQuery>({
          query: Get_ReunionsDocument,
          data: {
            dQReunions: {
              ...(previousList || {}),
              nodes: (previousList?.nodes || []).filter((element) => element.id !== deletedId),
            } as any,
          },
        });
      }
    },
    client: federation,
  });

  const [loadReunionAccueil, loadingReunionAccuueil] = useGet_ReunionLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  useDeepCompareEffect(() => {
    handleSearch();
    if (params.action === 'create' && isMobile) {
      setOpenFormModal(true);
    } else {
      setOpenFormModal(false);
    }
    if (params?.doc) {
      loadReunionAccueil({
        variables: {
          id: params.doc,
        },
      });

      loadCommentaires({
        variables: {
          codeItem: 'DQ_REUNION',
          idItemAssocie: params.doc,
        },
      });

      loadUserSmyleys({
        variables: {
          codeItem: 'DQ_REUNION',
          idSource: params.doc,
        },
      });
    }
  }, [params]);

  useEffect(() => {
    if (!searchingReunions.loading && !searchingReunions.error && searchingReunions.data?.dQReunions) {
      setCurrentReunions(searchingReunions.data?.dQReunions.nodes);
    }
  }, [searchingReunions.loading, searchingReunions.error, searchingReunions.data]);

  const handleGoBack = (): void => {
    push(`/demarche-qualite/outils`);
  };

  const handleAdd = (): void => {
    redirect(
      {
        id: undefined,
        action: 'create',
      },
      'compte-rendu-form'
    );
    setFicheToEdit(undefined);
  };

  const handleEdit = (fiche: any): void => {
    redirect(
      {
        id: fiche.id,
      },
      'compte-rendu-form'
    );
    if (isMobile) {
      setOpenFormModal(true);
    }
    setFicheToEdit(fiche);
  };

  const handleShowDetails = (fiche: any): void => {
    redirect(
      {
        id: fiche.id,
      },
      'compte-rendu-detail'
    );
  };

  const handleDelete = (fiche: Reunion): void => {
    if (fiche.id)
      deleteCompteRendu({
        variables: {
          input: {
            id: fiche.id,
          },
        },
      });
  };

  const handleSearch = (): void => {
    if (params) {
      let filterList: any[] | undefined = undefined;

      const urgenceIds = urgence
        ?.filter((urgence) => urgence && urgence.id && urgence.checked)
        .map((urgence) => urgence && urgence.id) as any;
      const etiquetteIds = etiquette?.filter((etiquette) => etiquette.checked).map((etiquette) => etiquette.id);

      if (urgenceIds && urgenceIds.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idUrgence: {
              in: urgenceIds,
            },
          },
        ];
      }

      if (etiquetteIds && etiquetteIds.length > 0) {
        filterList = [
          ...(filterList || []),
          {
            idEtiquette: {
              in: etiquetteIds,
            },
          },
        ];
      }

      if (params.searchText) {
        filterList = [
          ...(filterList || []),
          {
            description: {
              iLike: `%${params.searchText}%`,
            },
          },
        ];
      }
      if (params.date) {
        filterList = [
          ...(filterList || []),
          {
            createdAt: {
              between: {
                lower: startTime(moment.utc(new Date(params.date))),
                upper: endTime(moment.utc(new Date(params.date))),
              },
            },
          },
        ];
      }

      if (params.idUser) {
        filterList = [
          ...(filterList || []),
          {
            idResponsable: {
              eq: params.idUser,
            },
          },
        ];
      }

      const parameters = filterList
        ? {
            variables: {
              filter: {
                and: filterList,
              },
            },
          }
        : {};

      searchReunions(parameters);
    }
  };

  const handleRequestOpenFormModal = (fiche: Reunion | undefined) => {
    if (fiche) {
      setFicheToEdit(fiche);
      setOpenFormModal(true);
    }
  };

  const handleCloseFormModal = (value: boolean) => {
    setOpenFormModal(value);
    setFicheToEdit(undefined);
  };

  const onRunSearch = (value: string) => {
    redirect(
      {
        searchText: value,
        doc: undefined,
      },
      'compte-rendu'
    );
  };
  const onUserSelectChange = (value: any) => {
    redirect(
      {
        doc: undefined,
        idUser: value ? value.id : undefined,
      },
      'compte-rendu'
    );
  };

  const handleDateFilter = (date: any, value?: any) => {
    const realDate = moment.utc(date).isValid() ? moment.utc(date).format('YYYY-MM-DD') : undefined;
    redirect(
      {
        doc: undefined,
        date: realDate,
      },
      'compte-rendu'
    );
  };

  const handleClickListItem = (id: string) => {
    redirect(
      {
        doc: id,
        action: undefined,
      },
      'compte-rendu'
    );
  };

  const handleSearchTable = (value: TableChange) => {
    redirect(
      {
        skip: value.skip.toString(),
        take: value.take.toString(),
      },
      'compte-rendu'
    );
  };

  const handleRefecthComments = useCallback(() => {
    loadingCommentaire?.refetch && loadingCommentaire.refetch();
  }, [loadingCommentaire]);

  const handleRefecthSmyleys = () => () => {
    loadingUserSmyleys?.refetch && loadingUserSmyleys.refetch();
  };

  const commentaires = {
    total: loadingCommentaire.data?.comments?.total || 0,
    data: loadingCommentaire.data?.comments?.data as any,
    loading: loadingCommentaire.loading,
  };

  const userSmyleys = {
    data: loadingUserSmyleys.data?.userSmyleys?.data as any,
    loading: loadingUserSmyleys.loading,
  };

  return (
    <>
      <CustomContainerPage<ReunionInfoFragment, Get_ReunionsQuery, Get_ReunionsQueryVariables>
        buildVariables={(queryResult, range) => {
          return {
            ...queryResult.variables,
            paging: {
              offset: range.startIndex,
              limit: range.stopIndex + 1,
            },
          };
        }}
        updateQuery={(prev, { fetchMoreResult }) => {
          if (prev?.dQReunions?.nodes && fetchMoreResult?.dQReunions?.nodes) {
            const { nodes: currentData } = prev.dQReunions;
            return {
              ...prev,
              dQFicheIncidents: {
                ...prev.dQReunions,
                nodes: uniqBy([...currentData, ...fetchMoreResult.dQReunions.nodes], 'id'),
                totalCount: fetchMoreResult.dQReunions.totalCount,
              },
            };
          }
          return prev;
        }}
        commentaires={commentaires}
        userSmyleys={userSmyleys}
        refetchComments={handleRefecthComments}
        refetchSmyleys={handleRefecthSmyleys}
        refetch={searchingReunions.refetch}
        queryResult={searchingReunions}
        extractItems={(queryResult) => queryResult.data?.dQReunions.nodes || []}
        extractItemsTotalCount={(queryResult) => queryResult.data?.dQReunions.totalCount || 0}
        title="Suivi des Réunions"
        toogleViewTop
        impactFilterTop={false}
        statusFilterTop={false}
        onRequestGoBack={handleGoBack}
        onRunSearch={onRunSearch}
        onUserSelectChange={onUserSelectChange}
        addButtonLabel="Nouvelle réunion"
        dataListMenuDetail={{
          results: searchingReunions.data?.dQReunions.nodes as any,
          total: searchingReunions.data?.dQReunions.totalCount || 0,
          loading: searchingReunions.loading,
          error: searchingReunions.error as any,
        }}
        codeItemComment="DQ_REUNION"
        idSourceComment={params?.doc}
        documentSeleceted={params.doc ? loadingReunionAccuueil.data?.dQReunion : undefined}
        loading={searchingReunions.loading}
        error={!!searchingReunions.error}
        labelTypeTableListe="Type réunion"
        labelCauseTableListe="Cause réunion"
        labelDescriptionTableListe="Déscription"
        labelImpactTableListe="Impact"
        labelStatusTableListe="Statut"
        labelDateDebutTableListe="Date"
        labelDateResolutionTableListe="Date de resolution"
        titleButtonTableListe="Nouvelle réunion"
        labelAddMenuAction="Nouvelle réunion"
        titleTopBarTableListe="Liste des Réunions"
        onClickAddButton={handleAdd}
        handleDeleteMenuAction={handleDelete}
        handleEditMenuAction={handleEdit}
        onChangeDatePickers={handleDateFilter}
        onClickItemListMenu={handleClickListItem}
        handleVoirDetailMenuAction={handleShowDetails}
        handleSearchTable={handleSearchTable}
        page="reunion"
        dateLabelDetail="Date de réunion"
        origineLabelDetail="Animateur"
        correspondantLabelDetail="Responsable"
        titleOrigineDetail="Qui est l'animateur de cette réunion"
        tableSuiviReunion={
          <CompteRenduTable
            data={(loadingReunionAccuueil?.data?.dQReunion?.actions as any) || []}
            error={loadingReunionAccuueil.error}
            loading={loadingReunionAccuueil.loading}
            // onRequestAdd={handleAdd}
            // onRequestDelete={handleDelete}
            // onRequestEdit={handleEdit}
            // onRequestGoBack={handleGoBack}
            // onRequestSearch={handleSearch}
            // onRequestShowDetail={handleShowDetails}
            rowsTotal={
              (reunionAgregateData?.dQReunionAggregate.length || 0) > 0
                ? (reunionAgregateData?.dQReunionAggregate[0].count?.id as any)
                : 0
            }
            onRequestOpenFormModal={handleRequestOpenFormModal}
            filterIcon={<img src={FilterAlt} />}
            origines={origines.data?.origines.nodes}
          />
        }
      />
      <CustomModal
        title={ficheToEdit ? 'Modification réunion' : 'Nouvelle réunion'}
        open={openFormModal}
        setOpen={handleCloseFormModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        fullScreen={true}
        maxWidth="xl"
        noDialogContent={true}
      >
        <FicheCompteRenduMobileForm reunionToEdit={ficheToEdit} setOpenFormModal={handleCloseFormModal} />
      </CustomModal>
    </>
  );
};

export default FicheCompteRendu;
