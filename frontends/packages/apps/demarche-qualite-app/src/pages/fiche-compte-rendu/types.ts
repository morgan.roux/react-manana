import { User } from '@lib/ui-kit';
import { ReunionAction } from '@lib/ui-kit/src/components/pages/DemarcheQualite/CompteRenduFormPage/types';

// A coupler ave le Back graphql

export interface ReunionChange {
  dateCreationFiltersSelected: any;
  searchText: string;
  skip: number;
  take: number;
}

export interface FullReunion {
  id: string;
  description: string;
  idUserAnimateur: string;
  idResponsable: string;
  createdAt: string;
  updatedAt: string;
  resposable: {
    id: string;
    fullName: string;
  };
  animateur: {
    id: string;
    fullName: string;
  };
  participants: User[];
  actions: ReunionAction[];
}
