import { CompteRenduMobileFormPage } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

import AssignTaskUserModal from '@lib/common/src/components/CustomSelectUser/CustomSelectUser';
import {
  RequestSavingCompteRendu,
  Reunion,
} from '@lib/ui-kit/src/components/pages/DemarcheQualite/CompteRenduFormPage/types';
import { useApplicationContext } from '@lib/common';
import {
  useCreate_ReunionMutation,
  useGet_Encours_Action_OperationnellesQuery,
  useGet_Encours_Fiche_AmeliorationsQuery,
  useGet_Encours_Fiche_IncidentsQuery,
  useGet_OriginesQuery,
  useGet_ReunionLazyQuery,
  useGet_UrgencesQuery,
  useImportancesQuery,
  useUpdate_ReunionMutation,
} from '@lib/common/src/federation';
import AvatarInput from '@lib/common/src/components/AvatarInput';
import moment from 'moment';

interface CompteRenduMobileFormProps {
  match: {
    params: {
      id: string | undefined;
    };
  };
  reunionToEdit?: Reunion;
  setOpenFormModal?: (value: boolean) => void;
}

const CompteRenduMobileForm: FC<RouteComponentProps & CompteRenduMobileFormProps> = ({
  history: { push },
  match: {
    params: { id },
  },
  reunionToEdit,
  setOpenFormModal,
}) => {
  const importances = useImportancesQuery();
  const urgences = useGet_UrgencesQuery();

  const { notify, federation } = useApplicationContext();

  const [userParticipants, setUserParticipants] = useState<any[]>([]);
  const [reunion, setReunion] = useState<Reunion | undefined>();
  const [animateur, setAnimateur] = useState<any>([]);
  const [responsable, setResponsable] = useState<any>([]);
  const [openConcernedParticipantModal, setOpenConcernedParticipantModal] = useState<boolean>(false);
  const [openAnimateurModal, setOpenAnimateurModal] = useState<boolean>(false);
  const [openResponsableModal, setOpenResponsableModal] = useState<boolean>(false);
  const [mutationLoading, setMutationLoading] = useState<boolean>(false);

  const isNewReunion = reunion?.id || reunionToEdit ? false : true;

  const [loadReunion, reunionQuery] = useGet_ReunionLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  const origines = useGet_OriginesQuery({
    client: federation,
  });

  useEffect(() => {
    if (!isNewReunion && !reunionQuery.loading) {
      if (!reunionQuery.client) {
        if (id || reunionToEdit?.id) {
          loadReunion({
            variables: {
              id: reunionToEdit?.id || id || '',
            },
          });
        }
      } else if (reunionQuery.data?.dQReunion) {
        const reunion = reunionQuery.data?.dQReunion;
        const animateur = reunion?.animateur;
        const responsable = reunion?.responsable;
        const userParticipants = reunion?.participants;
        if (reunion) {
          setReunion(reunion as any);
        }
        if (animateur) {
          setAnimateur([animateur]);
        }
        if (responsable) {
          setResponsable([responsable]);
        }
        if (userParticipants) {
          setUserParticipants(userParticipants);
        }
      }
    }
  }, [id, reunionQuery.loading]);

  const loadingEncoursActionOperationnelles = useGet_Encours_Action_OperationnellesQuery({
    fetchPolicy: 'network-only',
    client: federation,
  });

  const loadingEncoursFicheAmeliorations = useGet_Encours_Fiche_AmeliorationsQuery({
    fetchPolicy: 'network-only',
    client: federation,
  });

  const loadingEncoursFicheIncidents = useGet_Encours_Fiche_IncidentsQuery({
    fetchPolicy: 'network-only',
    client: federation,
  });

  const handleOpenConcernedParticipant = (event: any) => {
    event.stopPropagation();
    setOpenConcernedParticipantModal(true);
  };

  const handleOpenAnimateurModal = (event: any) => {
    event.stopPropagation();
    setOpenAnimateurModal(true);
  };

  const handleOpenResponsableModal = (event: any) => {
    event.stopPropagation();
    setOpenResponsableModal(true);
  };

  const handleSelectedCollaborateurs = (selected: any) => {
    setUserParticipants(selected);
  };

  const handleSelectedAnimateur = (selected: any) => {
    console.log('+++ selected : ', selected);
    setAnimateur(selected);
  };

  const handleSelectedResponsable = (selected: any) => {
    setResponsable(selected);
  };

  const handleGoBack = (): void => {
    push(`/demarche-qualite/compte-rendu`);
  };

  const [addReunion] = useCreate_ReunionMutation({
    onCompleted: () => {
      notify({
        message: 'La réunion a été ajoutée avec succès !',
        type: 'success',
      });
      setMutationLoading(false);
      if (setOpenFormModal) {
        setOpenFormModal(false);
      }
      push('/demarche-qualite/compte-rendu');
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout de la réunion",
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateReunion] = useUpdate_ReunionMutation({
    onCompleted: () => {
      notify({
        message: 'La réunion a été modifié avec succès !',
        type: 'success',
      });
      setMutationLoading(false);
      if (setOpenFormModal) {
        setOpenFormModal(false);
      }
      push('/demarche-qualite/compte-rendu');
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification de la réunion',
        type: 'error',
      });
    },

    client: federation,
  });

  const upsertReunion = (reunionToSave: RequestSavingCompteRendu) => {
    if (isNewReunion) {
      addReunion({
        variables: {
          input: {
            idUserAnimateur: reunionToSave.idUserAnimateur,
            idUserParticipants: reunionToSave.idUserParticipants,
            idResponsable: reunionToSave.idResponsable,
            description: reunionToSave?.description,
            actions: reunionToSave.actions.map((action) => ({ ...action, id: undefined })),
            dateReunion: moment.utc(reunionToSave.dateReunion),
          },
        },
      });
    } else {
      updateReunion({
        variables: {
          id: reunionToSave?.id || '',
          input: {
            idUserAnimateur: reunionToSave.idUserAnimateur,
            idUserParticipants: reunionToSave.idUserParticipants,
            idResponsable: reunionToSave.idResponsable,
            description: reunionToSave?.description,
            actions: reunionToSave.actions.map((action) => ({ ...action, id: undefined })),
            dateReunion: moment.utc(reunionToSave.dateReunion),
          },
        },
      });
    }
  };

  const handleSave = (ficheToSave: RequestSavingCompteRendu) => {
    setMutationLoading(true);
    upsertReunion(ficheToSave);
  };

  const collaborateurComponent = (
    <Box onClick={handleOpenConcernedParticipant} width="100%">
      <AvatarInput list={userParticipants} label="Presence" />
      <AssignTaskUserModal
        openModal={openConcernedParticipantModal}
        setOpenModal={setOpenConcernedParticipantModal}
        withNotAssigned={false}
        selected={userParticipants}
        setSelected={handleSelectedCollaborateurs}
        title="Presence"
        searchPlaceholder="Rechercher..."
        withAssignTeam={false}
      />
    </Box>
  );

  const idUserParticipants = userParticipants.map((userParticipant) => userParticipant.id);
  const idUserAnimateur = animateur[0]?.id;
  const idResponsable = responsable[0]?.id;

  console.log('+++ : iNew', isNewReunion);

  const animateurComponent = (
    <Box onClick={handleOpenAnimateurModal} width="100%">
      <AvatarInput list={animateur} label="Animateur" />
      <AssignTaskUserModal
        openModal={openAnimateurModal}
        setOpenModal={setOpenAnimateurModal}
        withNotAssigned={false}
        selected={animateur}
        setSelected={handleSelectedAnimateur}
        title="Animateur"
        searchPlaceholder="Rechercher..."
        withAssignTeam={false}
        singleSelect={true}
      />
    </Box>
  );

  const responsableComponent = (
    <Box onClick={handleOpenResponsableModal} width="100%">
      <AvatarInput list={responsable} label="Resonsable Compte Rendu" />
      <AssignTaskUserModal
        openModal={openResponsableModal}
        setOpenModal={setOpenResponsableModal}
        withNotAssigned={false}
        selected={responsable}
        setSelected={handleSelectedResponsable}
        title="Resonsable Compte Rendu"
        searchPlaceholder="Rechercher..."
        withAssignTeam={false}
        singleSelect={true}
      />
    </Box>
  );

  return (
    <CompteRenduMobileFormPage
      origines={{
        ...origines,
        data: origines.data?.origines.nodes || [],
      }}
      importances={importances.data?.importances.nodes || []}
      urgences={urgences.data?.urgences.nodes || []}
      reunion={reunion}
      mode={isNewReunion ? 'creation' : 'modification'}
      loading={reunionQuery.loading || importances.loading || urgences.loading}
      onParticipantsClicked={() => console.log('clicked')}
      onChangeImportance={() => console.log('importance')}
      onChangeUrgence={() => console.log('urgence')}
      saving={mutationLoading}
      idUserParticipants={idUserParticipants}
      selectCollaborateurComponent={collaborateurComponent}
      selectAnimateurComponent={animateurComponent}
      selectResponsableComponent={responsableComponent}
      onRequestGoBack={handleGoBack}
      onRequestSave={handleSave}
      idUserAnimateur={idUserAnimateur}
      idResponsable={idResponsable}
      ficheAmeliorations={{
        ...loadingEncoursFicheAmeliorations,
        data: loadingEncoursFicheAmeliorations.data?.dQEncoursFicheAmeliorations as any,
      }}
      ficheIncidents={{
        ...loadingEncoursFicheIncidents,
        data: loadingEncoursFicheIncidents.data?.dQEncoursFicheIncidents as any,
      }}
      actionOperationnelles={{
        ...loadingEncoursActionOperationnelles,
        data: loadingEncoursActionOperationnelles.data?.dQEncoursActionOperationnelles as any,
      }}
    />
  );
};

export default withRouter(CompteRenduMobileForm);
