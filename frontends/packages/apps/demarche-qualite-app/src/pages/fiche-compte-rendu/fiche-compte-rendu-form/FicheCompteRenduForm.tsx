import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import _ from 'lodash';
import moment from 'moment';
import useReunionAction from '../../../utils/useReunionAction';
import {
  useCollaborateursQuery,
  useCreate_ReunionMutation,
  useGet_Encours_Action_OperationnellesQuery,
  useGet_Encours_Fiche_AmeliorationsQuery,
  useGet_Encours_Fiche_IncidentsQuery,
  useGet_OriginesQuery,
  useGet_ReunionLazyQuery,
  useGet_UrgencesQuery,
  useImportancesQuery,
  useUpdate_ReunionMutation,
} from '@lib/common/src/federation';
import { CustomSelectUser, useApplicationContext } from '@lib/common';
import UserInput from '@lib/common/src/components/UserInput';
import { useDqParams } from './../../../hooks/useDqParams';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { RequestSavingCompteRendu, ReunionAction } from '../../../components/DemarcheQualite/CompteRenduFormPage/types';
import { CompteRenduFormPage } from '../../../components/DemarcheQualite';

interface FicheCompteRenduFormProps {
  match: {
    params: {
      id: string;
    };
  };
}

const FicheCompteRenduForm: FC<FicheCompteRenduFormProps & RouteComponentProps> = ({ history: { push } }) => {
  const { redirect, params } = useDqParams();
  const isNewFicheCompteRendu = params.action === 'create';

  const { notify, federation } = useApplicationContext();

  const [openModalAnimateur, setOpenModalAnimateur] = useState<boolean>(false);
  const [openModalResponsable, setOpenModalResponsable] = useState<boolean>(false);
  const [openModalParticipants, setOpenModalParticipants] = useState<boolean>(false);
  const [reunionActionToEdit, setReunionActionToEdit] = useState<ReunionAction>();
  const [participantTypeToEdit, setParticipantTypeToEdit] = useState<string>();

  const [selectedUserAnimateurs, setSelectedUserAnimateurs] = useState<any>([]);
  const [selectedUserResponsables, setSelectedUserResponsables] = useState<any>([]);
  const importances = useImportancesQuery();
  const urgences = useGet_UrgencesQuery();
  const { updateReunionAction } = useReunionAction();

  const origines = useGet_OriginesQuery({
    client: federation,
  });

  const {
    loading,
    error,
    data: collaborateurs,
  } = useCollaborateursQuery({
    client: federation,
  });

  const loadingEncoursActionOperationnelles = useGet_Encours_Action_OperationnellesQuery({
    fetchPolicy: 'network-only',
    client: federation,
  });

  const loadingEncoursFicheAmeliorations = useGet_Encours_Fiche_AmeliorationsQuery({
    fetchPolicy: 'network-only',
    client: federation,
  });

  const loadingEncoursFicheIncidents = useGet_Encours_Fiche_IncidentsQuery({
    fetchPolicy: 'network-only',
    client: federation,
  });

  const [loadReunion, loadingReunion] = useGet_ReunionLazyQuery({
    fetchPolicy: 'network-only',
    client: federation,
  });

  const [addReunion, creationReunion] = useCreate_ReunionMutation({
    onCompleted: () => {
      notify({
        message: 'La réunion a été ajoutée avec succès !',
        type: 'success',
      });
      // if (setOpenFormModal) {
      //   setOpenFormModal(false);
      // }
      redirect(
        {
          id: undefined,
          action: undefined,
        },
        'compte-rendu'
      );
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout de la réunion",
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateReunion, modificationReunion] = useUpdate_ReunionMutation({
    onCompleted: () => {
      notify({
        message: 'La réunion a été modifié avec succès !',
        type: 'success',
      });
      redirect(
        {
          id: undefined,
          action: undefined,
        },
        'compte-rendu'
      );
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification de la réunion',
        type: 'error',
      });
    },

    client: federation,
  });

  const handleRequestSave = (dataToSave: RequestSavingCompteRendu) => {
    console.log('**Data to save**', dataToSave);
    if (isNewFicheCompteRendu) {
      addReunion({
        variables: {
          input: {
            idUserAnimateur: dataToSave.idUserAnimateur,
            idUserParticipants: dataToSave.idUserParticipants,
            idResponsable: dataToSave.idResponsable,
            description: dataToSave?.description,
            actions: dataToSave.actions.map((action) => ({ ...action, id: undefined })),
            dateReunion: moment.utc(dataToSave.dateReunion),
          },
        },
      });
    } else {
      updateReunion({
        variables: {
          id: params.id || '',
          input: {
            idUserAnimateur: dataToSave.idUserAnimateur,
            idUserParticipants: dataToSave.idUserParticipants,
            idResponsable: dataToSave.idResponsable,
            description: dataToSave?.description,
            actions: dataToSave.actions.map((action) => ({ ...action, id: undefined })),
            dateReunion: moment.utc(dataToSave.dateReunion),
          },
        },
      });
    }
  };

  const handleGoBack = () => {
    redirect(
      {
        id: undefined,
        action: undefined,
      },
      'compte-rendu'
    );
  };

  useDeepCompareEffect(() => {
    if (params.id) {
      loadReunion({
        variables: {
          id: params.id,
        },
      });
    }
  }, [params]);

  useEffect(() => {
    if (isNewFicheCompteRendu) {
      setSelectedUserAnimateurs([]);
      setSelectedUserResponsables([]);
    } else if (loadingReunion?.data?.dQReunion) {
      setSelectedUserAnimateurs([loadingReunion?.data.dQReunion.animateur]);
      setSelectedUserResponsables([loadingReunion?.data.dQReunion.responsable]);
    }
  }, [loadingReunion?.data?.dQReunion]);

  const handleParticipantsClicked = (reunionAction: ReunionAction, type: string) => {
    setReunionActionToEdit(reunionAction);
    setParticipantTypeToEdit(type);
    setOpenModalParticipants(true);
  };
  const handleChangeParticipants = (selected: any) => {
    setOpenModalParticipants(false);
    if (reunionActionToEdit && selected) {
      const typeAssocie = reunionActionToEdit.typeAssocie;
      const previousIdUserParticipants = (typeAssocie.participants || []).map(({ id }) => id);
      const previousIdUserParticipantsAInformer = (typeAssocie.participantsAInformer || []).map(({ id }) => id);

      const newParticipants = selected.map(({ id }: any) => ({
        idUser: id,
        type: participantTypeToEdit,
      }));

      const idUserParticipants =
        participantTypeToEdit === 'PARTICIPANT_EN_CHARGE'
          ? [
              ...previousIdUserParticipantsAInformer.map((idUser) => ({
                idUser,
                type: 'PARTICIPANT_A_INFORMER',
              })),
              ...newParticipants,
            ]
          : [
              ...previousIdUserParticipants.map((idUser) => ({
                idUser,
                type: 'PARTICIPANT_EN_CHARGE',
              })),
              ...newParticipants,
            ];

      updateReunionAction(reunionActionToEdit, { idUserParticipants });
    }
  };

  const computeDateEcheanceFromUrgence = (code: 'A' | 'B' | 'C'): string => {
    switch (code) {
      case 'A':
        return moment().toString();

      case 'B':
        return moment().endOf('isoWeek').add(1, 'week').toISOString();
      default:
        return moment().startOf('isoWeek').add(2, 'week').toISOString();
    }
  };

  const handleChangeUrgence = (reunionAction: ReunionAction, urgence: any) => {
    if (reunionAction.type === 'AMELIORATION') {
      updateReunionAction(reunionAction, {
        dateAmelioration: computeDateEcheanceFromUrgence(urgence.code),
        idUrgence: undefined,
      });
    } else if (reunionAction.type === 'INCIDENT') {
      updateReunionAction(reunionAction, {
        dateIncident: computeDateEcheanceFromUrgence(urgence.code),
        idUrgence: undefined,
      });
    } else {
      updateReunionAction(reunionAction, {
        dateAction: computeDateEcheanceFromUrgence(urgence.code),
        idUrgence: undefined,
      });
    }
  };

  const handleChangeImportance = (reunionAction: ReunionAction, importance: any) => {
    updateReunionAction(reunionAction, { idImportance: importance.id, idUrgence: undefined });
  };

  return (
    <>
      <CustomSelectUser
        withNotAssigned={false}
        selected={
          participantTypeToEdit === 'PARTICIPANT_EN_CHARGE'
            ? reunionActionToEdit?.typeAssocie.participants
            : reunionActionToEdit?.typeAssocie.participantsAInformer
        }
        setSelected={handleChangeParticipants}
        openModal={openModalParticipants}
        setOpenModal={setOpenModalParticipants}
      />
      <CompteRenduFormPage
        origines={{
          ...origines,
          data: origines.data?.origines.nodes || [],
        }}
        importances={importances.data?.importances.nodes || []}
        urgences={urgences.data?.urgences.nodes || []}
        key={params.id}
        loading={
          loading ||
          loadingReunion.loading ||
          loadingEncoursFicheAmeliorations.loading ||
          loadingEncoursFicheIncidents.loading ||
          loadingEncoursActionOperationnelles.loading ||
          importances.loading ||
          urgences.loading
        }
        onParticipantsClicked={handleParticipantsClicked}
        onChangeImportance={handleChangeImportance}
        onChangeUrgence={handleChangeUrgence}
        animateurComponent={
          <UserInput
            openModal={openModalAnimateur}
            withNotAssigned={false}
            singleSelect={true}
            setOpenModal={setOpenModalAnimateur}
            selected={selectedUserAnimateurs}
            setSelected={setSelectedUserAnimateurs}
            label="Animateur"
            defaultValueMe={true}
          />
        }
        responsableComponent={
          <UserInput
            openModal={openModalResponsable}
            withNotAssigned={false}
            singleSelect={true}
            setOpenModal={setOpenModalResponsable}
            selected={selectedUserResponsables}
            setSelected={setSelectedUserResponsables}
            label="Responsable Compte Rendu"
            defaultValueMe={true}
          />
        }
        selectedUserAnimateur={selectedUserAnimateurs ? selectedUserAnimateurs[0] : undefined}
        selectedUserResponsable={selectedUserResponsables ? selectedUserResponsables[0] : undefined}
        saving={isNewFicheCompteRendu ? creationReunion.loading : modificationReunion.loading}
        mode={isNewFicheCompteRendu ? 'creation' : 'modification'}
        reunion={loadingReunion.data?.dQReunion as any}
        error={loadingReunion.error}
        onRequestSave={handleRequestSave}
        onRequestGoBack={handleGoBack}
        presence={{
          loading: loading,
          error: error,
          data: collaborateurs?.collaborateurs as any,
        }}
        ficheAmeliorations={{
          ...loadingEncoursFicheAmeliorations,
          data: loadingEncoursFicheAmeliorations.data?.dQEncoursFicheAmeliorations as any,
        }}
        ficheIncidents={{
          ...loadingEncoursFicheIncidents,
          data: loadingEncoursFicheIncidents.data?.dQEncoursFicheIncidents as any,
        }}
        actionOperationnelles={{
          ...loadingEncoursActionOperationnelles,
          data: loadingEncoursActionOperationnelles.data?.dQEncoursActionOperationnelles as any,
        }}
      />
    </>
  );
};

export default FicheCompteRenduForm;
