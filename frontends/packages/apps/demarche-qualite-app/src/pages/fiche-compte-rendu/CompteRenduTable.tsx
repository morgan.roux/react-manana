/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable react/jsx-no-bind */
import { ReunionChange } from './types';
import { CssBaseline, Fade, Hidden, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Add, Delete, Edit, MoreHoriz, MoreVert, Visibility } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, MouseEvent, useEffect, useState } from 'react';
import { Reunion } from '@lib/ui-kit/src/components/pages/DemarcheQualite/CompteRenduFormPage/types';
import useStyles from './styles';
import { importanceToImpact, strippedString, useApplicationContext } from '@lib/common';
import { Backdrop, Column, CustomAvatarGroup, ImportanceLabel, Table } from '@lib/ui-kit';
import { DqReunionAction, OrigineInfoFragment } from '@lib/common/src/federation';
export interface CompteRenduPageProps {
  loading?: boolean;
  error?: Error;
  data?: Reunion[];
  rowsTotal: number;

  onRequestGoBack?: () => void;
  onRequestAdd?: () => void;
  onRequestShowDetail?: (fiche: Reunion) => void;
  onRequestEdit?: (fiche: Reunion) => void;
  onRequestDelete?: (fiches: Reunion[]) => void;
  onRequestSearch?: (change: ReunionChange) => void;
  onRequestOpenFormModal?: (fiche?: Reunion) => void;

  //urgenceFilter: ReactNode;
  // importanceFilter: ReactNode;

  filterIcon: any;
  origines?: OrigineInfoFragment[];
}

export const CompteRenduTable: FC<CompteRenduPageProps> = ({
  loading,
  error,
  data,
  rowsTotal,
  filterIcon,
  origines,
}) => {
  const classes = useStyles();
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(12);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [reunionToEdit, setReunionToEdit] = useState<Reunion | undefined>();
  const [rowsSelected, setRowsSelected] = useState<Reunion[]>([]);
  const { isMobile } = useApplicationContext();
  const open = Boolean(anchorEl);

  // useEffect(() => {
  //   onRequestSearch({
  //     skip,
  //     take,
  //     searchText,
  //     dateCreationFiltersSelected,
  //   });
  // }, [skip, take, searchText, dateCreationFiltersSelected]);

  const handleShowMenuClick = (reunion: Reunion, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    setReunionToEdit(reunion);
    setAnchorEl(event.currentTarget);
  };

  // const consultReunion = (): void => {
  //   if (reunionToEdit) {
  //     onRequestShowDetail(reunionToEdit);
  //   }
  // };

  // const editReunion = (): void => {
  //   if (!isMobile && reunionToEdit) {
  //     onRequestEdit(reunionToEdit);
  //   } else {
  //     onRequestOpenFormModal(reunionToEdit);
  //   }
  // };

  // const handleConfirmDeleteOne = (): void => {
  //   if (reunionToEdit) {
  //     onRequestDelete([reunionToEdit]);
  //     setOpenDeleteDialog(false);
  //   }
  // };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const actionBtn = (row: any) => {
    if (row) {
      return (
        <>
          <CssBaseline />
          <IconButton
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={handleShowMenuClick.bind(null, row)}
            color="inherit"
            id={`fiche-menu-icon-${row?.id}`}
          >
            {isMobile ? <MoreVert /> : <MoreHoriz />}
          </IconButton>
          <Menu
            id={`fiche-incident-menu-${row?.id}`}
            anchorEl={anchorEl}
            keepMounted
            open={open && row?.id === reunionToEdit?.id}
            onClose={handleClose}
            TransitionComponent={Fade}
          >
            <Hidden smDown={true} implementation="css">
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                  handleClose();
                  // consultReunion();
                }}
                disabled={false}
              >
                <ListItemIcon>
                  <Visibility />
                </ListItemIcon>
                <Typography variant="inherit">Voir détails</Typography>
              </MenuItem>
            </Hidden>

            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                // editReunion();
              }}
              disabled={row.statut?.code === 'CLOTURE'}
            >
              <ListItemIcon>
                <Edit />
              </ListItemIcon>
              <Typography variant="inherit">Modifier</Typography>
            </MenuItem>

            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                setOpenDeleteDialog(true);
              }}
              disabled={row.statut?.code === 'CLOTURE'}
            >
              <ListItemIcon>
                <Delete />
              </ListItemIcon>
              <Typography variant="inherit">Supprimer</Typography>
            </MenuItem>
          </Menu>
        </>
      );
    } else return <></>;
  };

  const columns: Column[] = [
    {
      name: 'type',
      label: 'Type',
      renderer: (row: DqReunionAction) => {
        return row.type ? row.type : '-';
      },
    },
    {
      name: 'description',
      label: 'Description',
      renderer: (row: DqReunionAction) => {
        return <div dangerouslySetInnerHTML={{ __html: row.typeAssocie?.description || '-' }} />;
      },
    },
    {
      name: 'createdAt',
      label: 'Date de création',
      renderer: (row: DqReunionAction) => {
        return row?.typeAssocie?.createdAt ? moment.utc(row.typeAssocie.createdAt).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'collaborateurs',
      label: 'Collaborateurs concernés',
      renderer: (row: DqReunionAction) => {
        return <CustomAvatarGroup max={3} users={(row.typeAssocie?.participants || []) as any} />;
      },
    },
    {
      name: 'participants',
      label: 'Origine',
      renderer: (row: DqReunionAction) => {
        return (
          (origines || []).find((el) => {
            return el.id === row?.typeAssocie?.origine.id;
          })?.libelle || '-'
        );
      },
    },
    {
      name: 'impact',
      label: 'Impact',
      renderer: (row: DqReunionAction) => {
        return <ImportanceLabel importance={importanceToImpact(row?.typeAssocie?.importance as any) as any} />;
      },
    },
    // {
    //   name: '',
    //   label: '',
    //   renderer: (row: DqReunionAction) => {
    //     return <div key={`row-${row.id}`}>{actionBtn(row)}</div>;
    //   },
    // },
  ];

  const [openDrawer, setOpenDrawer] = useState(false);

  const handleDrawerToggle = () => {
    setOpenDrawer(!openDrawer);
  };

  const handleShowFiltersClick = () => {
    setOpenDrawer(!openDrawer);
  };

  const fiche = data && data[0];

  const optionBtn = [
    <IconButton
      className={classes.iconAction}
      color="inherit"
      aria-label="settings"
      edge="start"
      onClick={handleShowFiltersClick}
    >
      {filterIcon}
    </IconButton>,
    actionBtn(fiche),
  ];

  const handleSwipePrevious = () => {
    if (skip > 0) {
      setSkip(skip - 1);
    }
  };

  const handleSwipeNext = () => {
    if (skip + 1 < rowsTotal) setSkip(skip + 1);
  };

  return (
    <>
      <Typography>Listes des actions</Typography>
      <Table
        style={{ padding: 24 }}
        error={error}
        search={false}
        data={data || []}
        selectable={false}
        rowsSelected={rowsSelected}
        onRowsSelectionChange={(newRowsSelected) => setRowsSelected(newRowsSelected as Reunion[])}
        columns={columns}
        onRunSearch={({ skip, take }) => {
          setSkip(skip);
          setTake(take);
        }}
        rowsTotal={rowsTotal}
      />
    </>
  );
};
