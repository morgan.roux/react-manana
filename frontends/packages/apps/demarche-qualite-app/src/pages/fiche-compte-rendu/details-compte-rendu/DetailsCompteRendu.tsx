import React, { FC } from 'react';
import { RouteComponentProps } from 'react-router';
import useReunionAction from '../../../utils/useReunionAction';
import { useGet_ReunionLazyQuery, useGet_StatutsQuery } from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';
import { sortStatuts } from '../../../utils/util';
import { useDqParams } from './../../../hooks/useDqParams';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { ReunionAction } from '../../../components/DemarcheQualite/CompteRenduFormPage/types';
import { DetailsCompteRendu } from '../../../components/DemarcheQualite';
interface DetailsFicheCompteRenduProps {
  match: {
    params: {
      id: string;
    };
  };
}

const DetailsFicheCompteRendu: FC<RouteComponentProps & DetailsFicheCompteRenduProps> = ({ history: { push } }) => {
  const { updateReunionAction } = useReunionAction();
  const { federation } = useApplicationContext();
  const { redirect, params } = useDqParams();

  const [loadReunion, loadingReunion] = useGet_ReunionLazyQuery({
    fetchPolicy: 'network-only',
    client: federation,
  });

  const loadingStatuts = useGet_StatutsQuery({
    client: federation,
    variables: {
      paging: {
        offset: 0,
        limit: 50,
      },
    },
  });

  useDeepCompareEffect(() => {
    if (params.id) {
      loadReunion({
        variables: {
          id: params.id,
        },
      });
    }
  }, [params]);

  const handleGoBack = (): void => {
    redirect(
      {
        id: undefined,
        doc: undefined,
      },
      'compte-rendu'
    );
  };

  const handleAdd = (): void => {
    redirect(
      {
        id: undefined,
        doc: undefined,
        action: 'create',
      },
      'compte-rendu-form'
    );
  };

  const handleSeeAction = (action: any): void => {
    console.log('**Action**', action);
  };

  const handleGoToTodo = (action: any): void => {
    console.log('**Action**', action);
  };

  const handleChangeStatus = (action: ReunionAction, idStatut: string): void => {
    updateReunionAction(action, { idStatut, idUrgence: undefined });
  };

  return (
    <DetailsCompteRendu
      compteRendu={{
        loading: loadingReunion.loading,
        error: loadingReunion.error as any,
        data: loadingReunion.data?.dQReunion as any,
      }}
      collaborateurs={{
        loading: loadingReunion.loading,
        error: loadingReunion.error as any,
        data: loadingReunion.data?.dQReunion?.participants as any,
      }}
      statuts={{
        ...loadingStatuts,
        data: sortStatuts((loadingStatuts.data?.dQStatuts.nodes || []) as any),
      }}
      onRequestChangeStatus={handleChangeStatus}
      onRequestAdd={handleAdd}
      onRequestGoBack={handleGoBack}
      onRequestSeeAction={handleSeeAction}
      onRequestGoToTodo={handleGoToTodo}
    />
  );
};

export default DetailsFicheCompteRendu;
