import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { NoItemContentImage } from '@lib/ui-kit';
import { useApplicationContext, useUploadFiles } from '@lib/common';
import {
  Get_OutilsDocument,
  Get_OutilsQuery,
  Get_OutilsQueryVariables,
  useCreate_OutilMutation,
  useDelete_OutilMutation,
  useGet_OutilLazyQuery,
  useGet_OutilsQuery,
  useUpdate_OutilMutation,
} from '@lib/common/src/federation';
import { FichePage, noContentValues } from '../../components/DemarcheQualite';

interface ToolsProps {
  match: {
    params: {
      typologie: string;
      id: string;
    };
  };
}

const Tools: FC<ToolsProps & RouteComponentProps> = ({
  history: { push },
  match: {
    params: { typologie, id },
  },
}) => {
  const { notify, federation } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();
  const [mutationLoading, setMutationLoading] = useState<boolean>(false);
  useEffect(() => {
    if (id) {
      getOutil({
        variables: {
          typologie: typologie,
          id: id,
        },
      });
    }
  }, [id]);

  // Get all tools by typologie
  const { loading, data, error } = useGet_OutilsQuery({
    variables: {
      typologie: typologie,
    },
    client: federation,
  });

  // const [
  //   getOutils,
  //   { loading: getOutilsLoading, data: getOutilsData, error: getOutilsError },
  // ] = useLazyQuery<GET_OUTILS_TYPE, GET_OUTILSVariables>(GET_OUTILS, {
  //   client: federation,
  // });

  const [getOutil, { loading: getOutilLoading, data: getOutilData, error: getOutilError }] = useGet_OutilLazyQuery({
    client: federation,
  });

  const showError = (message: string = "Des erreurs se sont survenues pendant l'ajout de l'outil"): void => {
    notify({
      type: 'error',
      message: message,
    });
  };

  const [addOutil, creationOutil] = useCreate_OutilMutation({
    onCompleted: () => {
      setMutationLoading(false);
      notify({
        type: 'success',
        message: "L'outil a été ajouté avec succès !",
      });
    },
    onError: () => {
      setMutationLoading(false);
      notify({
        type: 'error',
        message: "Des erreurs se sont survenues pendant l'ajout de l'outil",
      });
    },
    update: (cache, creationResult) => {
      if (creationResult.data && creationResult.data.createDQOutil) {
        const previousList = cache.readQuery<Get_OutilsQuery, Get_OutilsQueryVariables>({
          query: Get_OutilsDocument,
          variables: {
            typologie: typologie,
          },
        })?.dqOutils;

        cache.writeQuery<Get_OutilsQuery, Get_OutilsQueryVariables>({
          query: Get_OutilsDocument,
          variables: {
            typologie: typologie,
          },
          data: {
            dqOutils: [...(previousList || []), creationResult.data.createDQOutil],
          },
        });
      }
    },
    client: federation,
  });

  useGet_OutilsQuery;

  const [updateOutil, modificationOutil] = useUpdate_OutilMutation({
    onCompleted: () => {
      setMutationLoading(false);
      notify({
        type: 'success',
        message: "L'outil a été modifié avec succès !",
      });
    },
    onError: () => {
      notify({
        type: 'error',
        message: "Des erreurs se sont survenues pendant la modification de l'outil !",
      });
    },
    client: federation,
  });

  const [deleteOutil] = useDelete_OutilMutation({
    onCompleted: () => {
      notify({
        type: 'success',
        message: "L'outil a été supprimé avec succès !",
      });
    },
    onError: () => {
      notify({
        type: 'error',
        message: "Des erreurs se sont survenues pendant la suppression de l'outil !",
      });
    },
    update: (cache, deletionResult) => {
      if (deletionResult.data && deletionResult.data.deleteDQOutil && deletionResult.data.deleteDQOutil.id) {
        const previousList = cache.readQuery<Get_OutilsQuery, Get_OutilsQueryVariables>({
          query: Get_OutilsDocument,
          variables: {
            typologie: typologie,
          },
        })?.dqOutils;

        const deletedId = deletionResult.data.deleteDQOutil.id;

        cache.writeQuery<Get_OutilsQuery, Get_OutilsQueryVariables>({
          query: Get_OutilsDocument,
          variables: {
            typologie: typologie,
          },
          data: {
            dqOutils: (previousList || []).filter((item) => item?.id !== deletedId),
          },
        });
      }
    },
    client: federation,
  });

  const handleGoBack = (): void => {
    push(`/demarche-qualite/outils`);
  };

  const upsertOutil = ({ id, ordre, libelle, fichier }: any) => {
    if (id) {
      updateOutil({
        variables: {
          id: id,
          typologie: typologie,
          outilInput: fichier
            ? { ordre, libelle, fichier }
            : {
                ordre,
                libelle,
              },
        },
      });
    } else {
      addOutil({
        variables: {
          typologie: typologie,
          outilInput: {
            ordre,
            libelle,
            fichier,
          },
        },
      });
    }
  };

  const handleSave = ({ id, ordre, libelle, selectedFile, launchUpload }: any): void => {
    setMutationLoading(true);
    if (launchUpload) {
      uploadFiles([selectedFile], {
        directory: 'demarche-qualite',
      })
        .then((uploadedFiles) => {
          if (uploadFiles.length > 0) {
            upsertOutil({ id, ordre, libelle, fichier: uploadedFiles[0] });
          }
        })
        .catch(() => showError());
    } else {
      upsertOutil({ id, ordre, libelle });
    }
  };

  const handleDelete = (fiche: any): void => {
    deleteOutil({
      variables: {
        id: fiche.id,
        typologie: typologie,
      },
    });
  };

  const handleShowDetail = (fiche: any): void => {
    push(`/demarche-qualite/outils/${typologie}/${fiche.id}`);
    getOutil({
      variables: {
        typologie: typologie,
        id: fiche.id,
      },
    });
  };

  return (
    <FichePage
      typologie={typologie}
      saving={mutationLoading}
      idFromUrl={id}
      savingCompleted={!creationOutil.loading && !modificationOutil.loading && !mutationLoading}
      fiches={{
        loading: loading,
        error: error as any,
        data: data?.dqOutils as any,
      }}
      detailsRenderer={{
        loading: getOutilLoading,
        error: getOutilError as any,
        currentData: getOutilData?.dqOutil as any,
      }}
      emptyListComponent={<NoItemContentImage {...noContentValues(typologie).list} />}
      noItemSelectedComponent={<NoItemContentImage {...noContentValues(typologie).content} />}
      onRequestDelete={handleDelete}
      onRequestGoBack={handleGoBack}
      onRequestSave={handleSave}
      onRequestShowDetail={handleShowDetail}
    />
  );
};

export default Tools;
