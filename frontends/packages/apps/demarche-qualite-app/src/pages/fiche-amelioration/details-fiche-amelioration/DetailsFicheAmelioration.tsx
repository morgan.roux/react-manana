import React, { FC, useState, ChangeEvent, useEffect } from 'react';
import {
  ConfirmDeleteDialog,
  CustomButton,
  CustomDatePicker,
  CustomEditorText,
  CustomModal,
  ErrorPage,
  LoaderSmall,
  NoItemContentImage,
} from '@lib/ui-kit';

import Image from '../../../assets/img/demarche-qualite/ficheAmelioration/success.png';
import {} from '@lib/common';
import { Dropzone, useUploadFiles, strippedString, useApplicationContext } from '@lib/common';
import {
  Get_Fiche_AmeliorationsDocument,
  Get_Fiche_AmeliorationsQuery,
  Get_Fiche_AmeliorationsQueryVariables,
  useCreate_Fiche_Amelioration_ActionMutation,
  useDelete_Fiche_AmeliorationMutation,
  useGet_Fiche_AmeliorationQuery,
  useGet_Fiche_AmeliorationsLazyQuery,
  useGet_Fiche_Amelioration_ActionLazyQuery,
  useGet_OriginesQuery,
} from '@lib/common/src/federation';
import { Box, Card, Chip, Divider, TextField } from '@material-ui/core';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import moment from 'moment';
import PdfIcon from '@lib/common/src/assets/img/pdfIcon';
import useStyles from './style';
import { useDqParams } from './../../../hooks/useDqParams';
import FicheIncidentForm from './../../fiche-incident/fiche-incident-form/FicheIncidentForm';
import { LineDetail } from '../../../components/DemarcheQualite';

const DetailsFicheAmelioration: FC<{}> = ({}) => {
  const classes = useStyles();
  const { notify, federation } = useApplicationContext();
  const { redirect, params } = useDqParams();
  const [uploadFiles, uploadingFiles] = useUploadFiles();
  const [savingAmeliorationAction, setSavingAmeliorationAction] = useState<boolean>(false);
  const [openCreateDialog, setOpenCreateDialog] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [openFormModal, setOpenFormModal] = useState<boolean>(false);
  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  const [description, setDescription] = useState<string>('');
  const [dateHeureMiseEnplace, setDateHeureMiseEnplace] = useState<any | null>(new Date());

  const showError = (message: string = "Des erreurs se sont survenues pendant l'ajout de l'action"): void => {
    notify({
      type: 'error',
      message: message,
    });
  };

  const [loadAction, loadingAction] = useGet_Fiche_Amelioration_ActionLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  const { loading, data, error } = useGet_Fiche_AmeliorationQuery({
    onCompleted: (ficheResult) => {
      if (ficheResult.dQFicheAmelioration?.idAction) {
        loadAction({
          variables: {
            id: ficheResult.dQFicheAmelioration.idAction,
          },
        });
      }
    },
    variables: {
      id: params.id || '',
    },
    client: federation,
    fetchPolicy: 'network-only',
  });

  const origines = useGet_OriginesQuery({
    client: federation,
    variables: {
      filter: {
        and: [
          {
            id: {
              eq: data?.dQFicheAmelioration?.origine.id,
            },
          },
        ],
      },
    },
  });

  const [createFicheAmeliorationAction] = useCreate_Fiche_Amelioration_ActionMutation({
    onCompleted: (result) => {
      notify({
        message: "L'action d'amélioration a été ajoutée avec succès !",
        type: 'success',
      });

      // Reload action
      loadAction({
        variables: {
          id: result.createOneDQFicheAmeliorationAction.id,
        },
      });

      setSavingAmeliorationAction(false);
    },

    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout de l'action d'amélioration",
        type: 'error',
      });
    },
    client: federation,
  });

  const [deleteFicheAmelioration, deletionFicheAmelioration] = useDelete_Fiche_AmeliorationMutation({
    onCompleted: () => {
      notify({
        message: 'La fiche amélioration a été supprimée avec succès !',
        type: 'success',
      });

      redirect(
        {
          id: undefined,
        },
        'fiche-amelioration'
      );
    },
    update: (cache, deletionResult) => {
      if (
        deletionResult.data &&
        deletionResult.data.deleteOneDQFicheAmelioration &&
        deletionResult.data.deleteOneDQFicheAmelioration.id
      ) {
        const previousList = cache.readQuery<Get_Fiche_AmeliorationsQuery, Get_Fiche_AmeliorationsQueryVariables>({
          query: Get_Fiche_AmeliorationsDocument,
        })?.dQFicheAmeliorations;

        const deletedId = deletionResult.data.deleteOneDQFicheAmelioration.id;

        cache.writeQuery<Get_Fiche_AmeliorationsQuery, Get_Fiche_AmeliorationsQueryVariables>({
          query: Get_Fiche_AmeliorationsDocument,
          data: {
            dQFicheAmeliorations: {
              ...(previousList || {}),
              nodes: (previousList?.nodes || []).filter((item) => item?.id !== deletedId),
            } as any,
          },
        });
      }
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression de la fiche amélioration',
        type: 'error',
      });
    },
    client: federation,
  });

  useEffect(() => {
    if (data?.dQFicheAmelioration?.idAction) {
      loadAction({
        variables: {
          id: data.dQFicheAmelioration.idAction,
        },
      });
    }
  }, [data]);

  useEffect(() => {
    setDateHeureMiseEnplace(new Date());
    setDescription('');
    setSelectedFiles([]);
  }, [openCreateDialog]);

  const handleCloseFormModal = () => {
    setOpenFormModal(false);
  };

  const handleConfirmDelete = () => {
    if (data?.dQFicheAmelioration) {
      deleteFicheAmelioration({
        variables: {
          input: {
            id: data?.dQFicheAmelioration.id,
          },
        },
      });
    }
  };

  const handleSaveAction = () => {
    setSavingAmeliorationAction(true);
    if (selectedFiles.length > 0) {
      uploadFiles(selectedFiles, {
        directory: 'demarche-qualite',
      })
        .then((uploadedFiles) => {
          createFicheAmeliorationAction({
            variables: {
              idFicheAmelioration: data?.dQFicheAmelioration?.id || '',
              action: {
                description,
                dateHeureMiseEnplace,
                fichiers: uploadedFiles,
              },
            },
          });
        })
        .catch(() => showError());
    } else {
      createFicheAmeliorationAction({
        variables: {
          idFicheAmelioration: data?.dQFicheAmelioration?.id || '',
          action: {
            description,
            dateHeureMiseEnplace,
            fichiers: [],
          },
        },
      });
      setOpenCreateDialog(false);
    }
  };

  const handleEdit = () => {
    setOpenFormModal(true);
  };

  const handleGoBack = (): void => {
    redirect(
      {
        id: undefined,
      },
      'fiche-amelioration'
    );
  };

  const isValidFormAction = () => {
    return description && description !== '' && dateHeureMiseEnplace;
  };

  return (
    <>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<span>Êtes-vous sur de vouloir supprimer cette fiche d'amélioration ? </span>}
        onClickConfirm={handleConfirmDelete}
      />
      <CustomModal
        open={openCreateDialog}
        setOpen={setOpenCreateDialog}
        closeIcon={true}
        headerWithBgColor={true}
        fullWidth={true}
        maxWidth="sm"
        title={"Ajouter une action d'amélioration"}
        withBtnsActions={true}
        disabledButton={savingAmeliorationAction || uploadingFiles.loading || !isValidFormAction()}
        onClickConfirm={handleSaveAction}
      >
        <Box className={classes.actionForms}>
          <Box className={classes.actionForm}>
            <CustomDatePicker
              label="Date de mise en place"
              placeholder="Date début"
              onChange={(event) => setDateHeureMiseEnplace(event)}
              name="date"
              value={dateHeureMiseEnplace}
              disablePast={false}
              className={classes.actionDateInput}
            />
          </Box>
          <Box className={classes.actionForm}>
            <CustomEditorText
              value={description}
              placeholder="Action(*)"
              onChange={(value) => setDescription(value)}
              className={classes.actionFormAction}
            />
          </Box>
          <Box className={classes.actionForm}>
            <Dropzone selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles as any} />
          </Box>
        </Box>
      </CustomModal>

      <CustomContainer
        filled={true}
        bannerBack={true}
        bannerTitle="Détails d'une amélioration"
        onBackClick={handleGoBack}
        bannerContentStyle={{
          justifyContent: 'unset',
        }}
        bannerTitleStyle={{
          textAlign: 'center',
          flexGrow: 1,
        }}
      >
        <Box className={classes.detailsContent}>
          <Card className={classes.infoBox}>
            <Box>
              <span className={classes.span}>Informations générales</span>
              <LineDetail label="Auteur" value={data?.dQFicheAmelioration?.auteur?.fullName || '-'} />
              <LineDetail label="Origine" value={origines.data?.origines?.nodes[0]?.libelle || '-'} />
              <LineDetail
                label="Date de déclaration"
                value={
                  data?.dQFicheAmelioration?.createdAt
                    ? moment(data?.dQFicheAmelioration?.createdAt).format('DD/MM/YYYY')
                    : '-'
                }
              />
            </Box>
            {data?.dQFicheAmelioration?.fichiers && (
              <Box style={{ marginBottom: 20 }}>
                <span className={classes.fichierJointLabel}>Fichier(s) joint(s)</span>
                <Box>
                  {data?.dQFicheAmelioration?.fichiers.map((fiche: any) => {
                    return (
                      <Chip
                        className={classes.ficheBox}
                        key={fiche.id}
                        icon={<PdfIcon />}
                        clickable={true}
                        label={fiche.nomOriginal}
                        component="a"
                        target="_blank"
                        href={fiche.publicUrl}
                      />
                    );
                  })}
                </Box>
              </Box>
            )}

            <Divider />
            <Box>
              <span className={classes.span}>Actions d'amélioration</span>
              {!loadingAction?.data ? (
                <LineDetail
                  label="Action"
                  value={'Non'}
                  link={{
                    label: 'Ajouter une action',
                    onClick: () => setOpenCreateDialog(true),
                  }}
                />
              ) : (
                <LineDetail
                  label="Action"
                  value={strippedString(loadingAction.data.dQFicheAmeliorationAction?.description || '')}
                />
              )}
              <LineDetail label="Réunion d'équipe" value="Non" />
            </Box>
            <Divider />
            <Box style={{ marginBottom: 12 }}>
              <span className={classes.span}></span>

              <TextField
                className={classes.descriptionField}
                fullWidth={true}
                multiline={true}
                disabled={true}
                variant="outlined"
                label="Description du problème"
                value={strippedString(data?.dQFicheAmelioration?.description || '')}
              />
            </Box>
            <Box className={classes.buttonContainer}>
              <CustomButton
                className={classes.buttonContainerButton}
                onClick={() => setOpenDeleteDialog(true)}
                disabled={!!data?.dQFicheAmelioration?.reunion?.id}
              >
                Supprimer
              </CustomButton>
              <CustomButton className={classes.buttonContainerButton} color="secondary" onClick={handleEdit}>
                Modifier
              </CustomButton>
            </Box>
          </Card>
          <Card
            className={loadingAction?.data?.dQFicheAmeliorationAction ? classes.actionBox : classes.actionBoxBorder}
          >
            {loadingAction.loading ? (
              <LoaderSmall />
            ) : loadingAction?.error ? (
              <ErrorPage />
            ) : loadingAction?.data?.dQFicheAmeliorationAction ? (
              <Box>
                <span className={classes.span}>Action d'amélioration</span>
                <Divider className={classes.marginDevider} />
                <Box>
                  <LineDetail
                    label="Date de mise en place"
                    value={
                      loadingAction?.data?.dQFicheAmeliorationAction.dateHeureMiseEnplace
                        ? moment(new Date(loadingAction?.data?.dQFicheAmeliorationAction.dateHeureMiseEnplace)).format(
                            'DD/MM/YYYY'
                          )
                        : '-'
                    }
                  />

                  <TextField
                    className={(classes.descriptionField, classes.bgDecriptionField)}
                    fullWidth={true}
                    multiline={true}
                    disabled={true}
                    variant="outlined"
                    label="Description"
                    value={strippedString(loadingAction?.data.dQFicheAmeliorationAction?.description || '')}
                  />

                  {loadingAction?.data?.dQFicheAmeliorationAction.fichiers && (
                    <Box>
                      <span className={classes.problemeLabel}>Fichier(s) joint(s)</span>

                      <Box>
                        {(loadingAction?.data?.dQFicheAmeliorationAction.fichiers || []).map((fiche: any) => {
                          return (
                            <Chip
                              className={classes.ficheBox}
                              key={fiche?.id}
                              icon={<PdfIcon />}
                              clickable={true}
                              label={fiche?.nomOriginal}
                              component="a"
                              target="_blank"
                              href={fiche?.publicUrl}
                            />
                          );
                        })}
                      </Box>
                    </Box>
                  )}
                </Box>
              </Box>
            ) : (
              <NoItemContentImage
                title="Ajout d'action d'amélioration"
                subtitle="Vous pouvez ajouter une action à tout moment pour résoudre le problème"
              >
                <CustomButton
                  className={classes.addActionButton}
                  color="secondary"
                  onClick={() => setOpenCreateDialog(true)}
                >
                  Apporter une action
                </CustomButton>
              </NoItemContentImage>
            )}
          </Card>
        </Box>
        <CustomModal
          title={data?.dQFicheAmelioration ? 'Modification action opérationnelle' : 'Nouvelle action opérationnelle'}
          open={openFormModal}
          setOpen={handleCloseFormModal}
          closeIcon={true}
          headerWithBgColor={true}
          withBtnsActions={false}
          fullScreen={true}
          maxWidth="xl"
          noDialogContent={true}
        >
          <FicheIncidentForm
            ameliorationToEdit={data?.dQFicheAmelioration as any}
            setOpenFormModal={handleCloseFormModal}
          />
        </CustomModal>
      </CustomContainer>
    </>
  );
};

export default DetailsFicheAmelioration;
