import { endTime, startTime, useApplicationContext } from '@lib/common';
import {
  useCreate_One_Fiche_Amelioration_SolutionMutation,
  useDelete_Fiche_AmeliorationMutation,
  useGet_Fiche_AmeliorationLazyQuery,
  useGet_Fiche_AmeliorationsLazyQuery,
  useGet_OriginesQuery,
  useGet_Statuts_Order_By_Nombre_Fiche_AmeliorationsQuery,
  useGet_UrgencesQuery,
  useImportancesQuery,
  useUpdate_Fiche_Amelioration_ImportanceMutation,
  useUpdate_Fiche_Amelioration_StatutMutation,
  FicheAmeliorationInfoFragment,
  Get_Fiche_AmeliorationsQueryVariables,
  Get_Fiche_AmeliorationsQuery,
} from '@lib/common/src/federation';
import { CustomModal, TableChange } from '@lib/ui-kit';
import React, { FC, useCallback, useEffect, useMemo, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import { sortStatuts } from '../../utils/util';
import { importancesToImpacts } from '@lib/common';

import FicheIncidentForm from '../fiche-incident/fiche-incident-form/FicheIncidentForm';
import { CustomContainerPage } from '../../components/CustomContainerPage';
import { Solution } from './../../components/Solution/Solution';
import { SolutionRequestSaving } from './../../components/Solution/interface';
import { useDqParams } from './../../hooks/useDqParams';
import moment from 'moment';
import useDeepCompareEffect from 'use-deep-compare-effect';
import { useCommentsLazyQuery, useUser_SmyleysLazyQuery } from '@lib/common/src/graphql';
import { uniqBy } from 'lodash';
import { FicheAmelioration } from '../../components/DemarcheQualite';

const FicheAmeliorationManagement: FC<RouteComponentProps> = ({ history: { push } }) => {
  const { currentPharmacie: pharmacie, notify, federation, graphql } = useApplicationContext();
  const { redirect, params } = useDqParams();
  const idPharmacie = pharmacie.id;

  const loadingImportances = useImportancesQuery();
  const loadingUrgences = useGet_UrgencesQuery();

  const [openFormModal, setOpenFormModal] = useState<boolean>(false);
  const [ficheToEdit, setFicheToEdit] = useState<FicheAmelioration>();
  const [openCreateSolutionDialog, setOpenCreateSolutionDialog] = useState<boolean>(false);

  const importancesData = useMemo(() => {
    return importancesToImpacts(loadingImportances.data?.importances.nodes);
  }, [loadingImportances.data?.importances.nodes]);

  const origines = useGet_OriginesQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const statuts = useGet_Statuts_Order_By_Nombre_Fiche_AmeliorationsQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  //LazyQuery
  const [searchFicheAmeliorations, searchingFicheAmeliorations] = useGet_Fiche_AmeliorationsLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [updateFicheAmeliorationStatut] = useUpdate_Fiche_Amelioration_StatutMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: `Le statut du fiche amélioration a été modifié`,
        type: 'success',
      });
      searchingFicheAmeliorations.refetch && searchingFicheAmeliorations.refetch();
    },
    onError: () => {
      notify({
        message: `Une erreur est survenue lors du changement du statut`,
        type: 'error',
      });
    },
  });

  const [updateFicheAmeliorationInmportance] = useUpdate_Fiche_Amelioration_ImportanceMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: `L'impact du fiche amélioration a été modifié`,
        type: 'success',
      });
      searchingFicheAmeliorations.refetch && searchingFicheAmeliorations.refetch();
    },
    onError: () => {
      notify({
        message: `Une erreur est survenue lors du changement de l'impact`,
        type: `error`,
      });
    },
  });

  const [createFicheAmeliorationSolution] = useCreate_One_Fiche_Amelioration_SolutionMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: `L'ajout du solution a été ajouté`,
        type: 'success',
      });
      searchingFicheAmeliorations.refetch && searchingFicheAmeliorations.refetch();
    },
    onError: () => {
      notify({
        message: `Une erreur est survenue lors de l'ajout du solution`,
        type: 'error',
      });
    },
  });

  const [loadFicheAmeliorationAccueil, loadingFicheAmeliorationAccueil] = useGet_Fiche_AmeliorationLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
  });

  const [loadCommentaires, loadingCommentaire] = useCommentsLazyQuery({ client: graphql });

  const [loadUserSmyleys, loadingUserSmyleys] = useUser_SmyleysLazyQuery({ client: graphql });

  useDeepCompareEffect(() => {
    handleRequestSearch();
    const data = searchingFicheAmeliorations?.data?.dQFicheAmeliorations?.nodes?.length || 0;
    if (params.doc || data > 0) {
      loadCommentaires({
        variables: {
          codeItem: 'DQ_AMELIORATION',
          idItemAssocie: params?.doc || searchingFicheAmeliorations?.data?.dQFicheAmeliorations?.nodes[1]?.id || '',
        },
      });
      loadUserSmyleys({
        variables: {
          codeItem: 'DQ_AMELIORATION',
          idSource: params.doc || '',
        },
      });
    }
    if (params.action === 'create') {
      setOpenFormModal(true);
    } else {
      setOpenFormModal(false);
    }
    if (params.doc) {
      loadFicheAmeliorationAccueil({
        variables: {
          id: params.doc,
        },
      });
    }
  }, [params]);

  const handleRefetch = () => {
    origines.refetch && origines.refetch();
    searchingFicheAmeliorations.refetch && searchingFicheAmeliorations.refetch();
  };

  const [deleteFicheAmelioration] = useDelete_Fiche_AmeliorationMutation({
    onCompleted: () => {
      notify({
        message: 'La fiche amélioration a été supprimée avec succès !',
        type: 'success',
      });
      handleRefetch();
    },
    /*
    TODO : Update cache then remove all refetches
    
   update: (cache, deletionResult) => {
      if (
        deletionResult.data &&
        deletionResult.data.deleteOneDQFicheAmelioration &&
        deletionResult.data.deleteOneDQFicheAmelioration.id
      ) {
        const previousList = cache.readQuery<
          GET_FICHE_AMELIORATIONS_TYPE,
          GET_FICHE_AMELIORATIONSVariables
        >({
          query: GET_FICHE_AMELIORATIONS,
        })?.dQFicheAmeliorations;

        const deletedId = deletionResult.data.deleteOneDQFicheAmelioration.id;

        cache.writeData<GET_FICHE_AMELIORATIONS_TYPE>({
          data: {
            dQFicheAmeliorations: (previousList || []).filter((item) => item?.id !== deletedId),
          },
        });
      }
    },*/
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression de la fiche amélioration',
        type: 'error',
      });
    },
    client: federation,
  });

  const onRequestGoBack = (): void => {
    push('/demarche-qualite/outils');
  };

  const onRequestAdd = (): void => {
    //push(`/demarche-qualite/fiche-amelioration/new`);
    redirect(
      {
        id: undefined,
        action: 'create',
      },
      'fiche-amelioration'
    );
    setFicheToEdit(undefined);
  };

  const handleRequestShowDetail = (fiche: FicheAmelioration): void => {
    redirect(
      {
        id: fiche.id,
      },
      'fiche-amelioration-detail'
    );
  };

  const handleRequestDelete = (value: FicheAmelioration): void => {
    deleteFicheAmelioration({
      variables: {
        input: {
          id: value.id,
        },
      },
    });
  };

  const handleRequestEdit = (fiche: FicheAmelioration): void => {
    setOpenFormModal(true);
    setFicheToEdit(fiche);
  };

  const handleRequestSearch = (): void => {
    if (params) {
      let filterList: any[] | undefined = [
        {
          idPharmacie: {
            eq: idPharmacie,
          },
        },
      ];

      if (params.idImportance) {
        filterList = [
          ...(filterList || []),
          {
            idImportance: {
              eq: params.idImportance,
            },
          },
        ];
      }

      if (params.date) {
        filterList = [
          ...(filterList || []),
          {
            dateAmelioration: {
              between: {
                lower: startTime(moment.utc(new Date(params.date))),
                upper: endTime(moment.utc(new Date(params.date))),
              },
            },
          },
        ];
      }

      if (params.idStatut) {
        filterList = [
          ...(filterList || []),
          {
            idStatut: {
              eq: params.idStatut,
            },
          },
        ];
      }

      if (params.searchText) {
        filterList = [
          ...(filterList || []),
          {
            description: {
              iLike: `%${params.searchText}%`,
            },
          },
        ];
      }

      const parameters = filterList
        ? {
            variables: {
              filter: {
                and: filterList,
              },
              paging: {
                limit: params.take ? parseInt(params.take) : 12,
                offset: params.skip ? parseInt(params.skip) : 0,
              },
            },
          }
        : {};

      searchFicheAmeliorations(parameters);
    }
  };

  const handleCloseFormModal = (value: boolean) => {
    redirect(
      {
        action: undefined,
      },
      'fiche-amelioration'
    );
    setOpenFormModal(false);
    setFicheToEdit(undefined);
  };

  const handleClickListItem = (id: string) => {
    redirect(
      {
        doc: id,
        action: undefined,
      },
      'fiche-amelioration'
    );
  };

  const onRunSearch = (value: string) => {
    redirect(
      {
        searchText: value,
        doc: undefined,
      },
      'fiche-amelioration'
    );
  };

  const handleRequestChangeCloture = (currentAction?: FicheAmelioration) => {
    const idStatutCloture = statuts.data?.dQStatutsOrderByNombreFicheAmeliorations.find((statut) => {
      return statut.code === 'CLOTURE' && statut.type === 'OPERATION';
    })?.id;
    if (currentAction && idStatutCloture) {
      updateFicheAmeliorationStatut({
        variables: {
          id: currentAction.id as any,
          idItem: idStatutCloture,
        },
      });
    }
  };

  const handleRequestChangeStatut = ({ data, idItem }: any) => {
    if (data) {
      updateFicheAmeliorationStatut({
        variables: {
          id: data.id,
          idItem,
        },
      });
    }
  };
  const handleRequestChangeImpact = ({ data, idItem }: any) => {
    if (data) {
      updateFicheAmeliorationInmportance({
        variables: {
          id: data.id,
          idItem,
        },
      });
    }
  };
  const handleSaveSolution = (data: SolutionRequestSaving) => {
    createFicheAmeliorationSolution({
      variables: {
        input: {
          description: data.description,
          idUser: data.idUser,
          idItem: data.idItem,
        },
      },
    });
  };
  const handleClickApporterSolution = (data: any) => {
    setOpenCreateSolutionDialog(true);
    redirect({ doc: data?.id }, 'fiche-amelioration');
  };

  const handleClickFilter = (type: string, id: string) => {
    redirect(
      {
        doc: undefined,
        [type]: id,
      },
      'fiche-amelioration'
    );
  };

  const handleDateFilter = (date: any, value?: any) => {
    const realDate = moment.utc(date).isValid() ? moment.utc(date).format('YYYY-MM-DD') : undefined;
    redirect(
      {
        doc: undefined,
        date: realDate,
      },
      'fiche-amelioration'
    );
  };

  const handleSearchTable = (value: TableChange) => {
    redirect(
      {
        skip: value.skip.toString(),
        take: value.take.toString(),
      },
      'fiche-amelioration'
    );
  };

  const handleGoPilotage = () => {
    push(`/pilotage-statistiques/SA`);
  };

  const handleRefecthComments = useCallback(() => {
    loadingCommentaire?.refetch && loadingCommentaire.refetch();
  }, [loadingCommentaire]);

  const handleRefecthSmyleys = () => () => {
    loadingUserSmyleys?.refetch && loadingUserSmyleys.refetch();
  };

  const commentaires = {
    total: loadingCommentaire.data?.comments?.total || 0,
    data: loadingCommentaire.data?.comments?.data as any,
    loading: loadingCommentaire.loading,
  };

  const userSmyleys = {
    data: loadingUserSmyleys.data?.userSmyleys?.data as any,
    loading: loadingUserSmyleys.loading,
  };

  return (
    <>
      <CustomContainerPage<
        FicheAmeliorationInfoFragment,
        Get_Fiche_AmeliorationsQuery,
        Get_Fiche_AmeliorationsQueryVariables
      >
        buildVariables={(queryResult, range) => {
          return {
            ...queryResult.variables,
            paging: {
              offset: range.startIndex,
              limit: range.stopIndex + 1,
            },
          };
        }}
        updateQuery={(prev, { fetchMoreResult }) => {
          if (prev?.dQFicheAmeliorations?.nodes && fetchMoreResult?.dQFicheAmeliorations?.nodes) {
            const { nodes: currentData } = prev.dQFicheAmeliorations;
            return {
              ...prev,
              dQFicheAmeliorations: {
                ...prev.dQFicheAmeliorations,
                nodes: uniqBy([...currentData, ...fetchMoreResult.dQFicheAmeliorations.nodes], 'id'),
                totalCount: fetchMoreResult.dQFicheAmeliorations.totalCount,
              },
            };
          }
          return prev;
        }}
        refetch={searchingFicheAmeliorations.refetch}
        queryResult={searchingFicheAmeliorations}
        extractItems={(queryResult) => queryResult.data?.dQFicheAmeliorations.nodes || []}
        extractItemsTotalCount={(queryResult) => queryResult.data?.dQFicheAmeliorations.totalCount || 0}
        listFilterImpact={importancesData}
        listFilterStatut={
          {
            ...statuts,
            data: sortStatuts(
              (statuts.data?.dQStatutsOrderByNombreFicheAmeliorations || []).filter(
                ({ type }: any) => type === 'AMELIORATION'
              ) as any
            ),
          } as any
        }
        title="Demande d’Améliorations"
        onClickItemListMenu={handleClickListItem}
        onRequestGoBack={onRequestGoBack}
        onRunSearch={onRunSearch}
        addButtonLabel="Nouvelle amélioration"
        dataListMenuDetail={{
          results: searchingFicheAmeliorations.data?.dQFicheAmeliorations.nodes as any,
          loading: searchingFicheAmeliorations.loading,
          error: searchingFicheAmeliorations.error as any,
          total: searchingFicheAmeliorations.data?.dQFicheAmeliorations.totalCount || 0,
        }}
        documentSeleceted={params.doc ? (loadingFicheAmeliorationAccueil.data?.dQFicheAmelioration as any) : undefined}
        idSourceComment={
          params.doc
            ? loadingFicheAmeliorationAccueil.data?.dQFicheAmelioration?.id
            : searchingFicheAmeliorations.data?.dQFicheAmeliorations.nodes[1]?.id
        }
        codeItemComment={'DQ_AMELIORATION'}
        loading={searchingFicheAmeliorations.loading}
        error={searchingFicheAmeliorations.error as any}
        labelTypeTableListe="Type amélioration"
        labelCauseTableListe="Cause amélioration"
        labelDescriptionTableListe="Description"
        labelDeclarantInListMenu="Déclarant"
        labelImpactTableListe="Impact"
        labelStatusTableListe="Statut"
        labelDateDebutTableListe="Date"
        titleButtonTableListe="Nouvelle amélioration"
        labelDateResolutionTableListe="Date de résolution"
        titleTopBarTableListe="Liste des demandes d'améliorations"
        onClickAddButton={onRequestAdd}
        onClickCloturer={handleRequestChangeCloture}
        handleVoirDetailMenuAction={handleRequestShowDetail as any}
        handleEditMenuAction={handleRequestEdit}
        handleClickItemImpact={handleRequestChangeImpact}
        handleClickItemStatut={handleRequestChangeStatut}
        handleDeleteMenuAction={handleRequestDelete}
        handleClickFilter={handleClickFilter}
        onChangeDatePickers={handleDateFilter}
        dateFilter={params.date ? moment.utc(new Date(params.date)).toDate() : moment.utc().toDate()}
        handleApporterSolutionMenuAction={handleClickApporterSolution}
        refetchComments={handleRefecthComments}
        commentaires={commentaires}
        refetchSmyleys={handleRefecthSmyleys}
        userSmyleys={userSmyleys}
        handleGoPilotage={handleGoPilotage}
        handleSearchTable={handleSearchTable}
        origine={{
          ...origines,
          data: origines.data?.origines.nodes || [],
        }}
        page="amelioration"
        labelAddMenuAction="Nouvelle fiche amélioration"
      />
      <CustomModal
        title={ficheToEdit ? 'Modification amélioration' : 'Nouvelle amélioration'}
        open={openFormModal}
        setOpen={handleCloseFormModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        fullScreen={true}
        noDialogContent={true}
      >
        <FicheIncidentForm
          ameliorationToEdit={ficheToEdit}
          setOpenFormModal={handleCloseFormModal}
          refetch={handleRefetch}
        />
      </CustomModal>
      <Solution
        openCreateSolutionDialog={openCreateSolutionDialog}
        setOpenCreateSolutionDialog={setOpenCreateSolutionDialog}
        onRequestSaveSolution={handleSaveSolution}
        idItem={params.doc}
        type="amelioration"
      />
    </>
  );
};

export default FicheAmeliorationManagement;
