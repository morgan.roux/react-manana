import React from 'react';
import loadable from '@loadable/component';
import DemarcheQualite from './assets/img/demarche_qualite.png';
import { ModuleDefinition } from '@lib/common';
import { Face, ViewModule, WarningRounded } from '@material-ui/icons';
import { SmallLoading } from '@lib/ui-kit';
import FicheIncidentForm from './pages/fiche-incident/fiche-incident-form/FicheIncidentForm';
import moment from 'moment';
moment.locale('fr');

const DemarcheQualitePage = loadable(() => import('./pages/demarche-qualite/DemarcheQualite'), {
  fallback: <SmallLoading />,
});

const AutoEvaluationPage = loadable(() => import('./pages/auto-evaluation/AutoEvaluation'), {
  fallback: <SmallLoading />,
});

const ReferentielQualitePage = loadable(() => import('./pages/referentiel-qualite/ReferentielQualite'), {
  fallback: <SmallLoading />,
});

const OutilsPage = loadable(() => import('./pages/outils/Outils'), {
  fallback: <SmallLoading />,
});

const ToolsPage = loadable(() => import('./pages/tools/Tools'), {
  fallback: <SmallLoading />,
});

const ChecklistEvaluationPage = loadable(() => import('./components/ChecklilstEvaluation/ChecklistEvaluation'), {
  fallback: <SmallLoading />,
});

const ActionOperationnellePage = loadable(() => import('./pages/action-operationnelle/ActionOperationnelle'), {
  fallback: <SmallLoading />,
});

const DetailsActionOperationnellePage = loadable(
  () => import('./pages/action-operationnelle/details-action-operationnelle/DetailsActionOperationnelle'),
  {
    fallback: <SmallLoading />,
  }
);

const FicheIncidentPage = loadable(() => import('./pages/fiche-incident/FicheIncident'), {
  fallback: <SmallLoading />,
});

const FicheIncidentFormPage = loadable(() => import('./pages/fiche-incident/fiche-incident-form/FicheIncidentForm'), {
  fallback: <SmallLoading />,
});

const DetailsIncidentPage = loadable(() => import('./pages/fiche-incident/details-incident/DetailsIncident'), {
  fallback: <SmallLoading />,
});

const FicheAmeliorationPage = loadable(() => import('./pages/fiche-amelioration/FicheAmelioration'), {
  fallback: <SmallLoading />,
});

const DetailsAmeliorationPage = loadable(
  () => import('./pages/fiche-amelioration/details-fiche-amelioration/DetailsFicheAmelioration'),
  {
    fallback: <SmallLoading />,
  }
);

const FicheCompteRenduPage = loadable(() => import('./pages/fiche-compte-rendu/FicheCompteRendu'), {
  fallback: <SmallLoading />,
});

const FicheCompteRenduForm = loadable(
  () => import('./pages/fiche-compte-rendu/fiche-compte-rendu-form/FicheCompteRenduForm'),
  {
    fallback: <SmallLoading />,
  }
);

const DetailsCompteRenduPage = loadable(
  () => import('./pages/fiche-compte-rendu/details-compte-rendu/DetailsCompteRendu'),
  {
    fallback: <SmallLoading />,
  }
);

const MatricheTachePage = loadable(() => import('./pages/parametres/matrice-taches/MatriceTache'), {
  fallback: <SmallLoading />,
});

const ParametreDemarcheQualitePage = loadable(() => import('./pages/parametres/demarche-qualite/DemarcheQualite'), {
  fallback: <SmallLoading />,
});

const ExigenceFormPage = loadable(() => import('./pages/parametres/exigence-form/ExigenceForm'), {
  fallback: <SmallLoading />,
});

const ChecklistForm = loadable(() => import('./pages/parametres/checklists/ChecklistForm'), {
  fallback: <SmallLoading />,
});

const ChecklistManagementPage = loadable(() => import('./pages/parametres/checklists/ChecklistManagement'), {
  fallback: <SmallLoading />,
});

const activationParameters = {
  groupement: '0313',
  pharmacie: '0716',
};

const demarcheQualiteModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_DEMARCHE_QUALITE',
      location: 'ACCUEIL',
      name: 'Démarche qualité',
      to: '/demarche-qualite/outils',
      icon: DemarcheQualite,
      activationParameters,
      preferredOrder: 160,
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
    {
      id: 'FEATURE_PARAMETRE_MATRICE_BASE',
      location: 'PARAMETRE',
      name: 'Matrice des fonctions de Base',
      to: '/matrice_taches',
      icon: <ViewModule />,
      preferredOrder: 130,
      authorize: {
        roles: ['SUPADM', 'GRPADM', 'PRMTIT'],
        rolesDenied: ['PRTSERVICE']
      },
      device: 'WEB',
    },

    {
      id: 'FEATURE_PARAMETRE_DEMARCHE_QUALITE',
      location: 'PARAMETRE',
      name: 'Démarche qualité',
      to: '/demarche_qualite',
      icon: <Face />,
      preferredOrder: 140,
      authorize: {
        roles: ['SUPADM', 'GRPADM'],
        rolesDenied: ['PRTSERVICE']
      },
      device: 'WEB',
    },

    {
      id: 'FEATURE_PARAMETRE_CHECKLIST',
      location: 'PARAMETRE',
      name: 'Check-list qualité',
      to: '/check-list-qualite',
      icon: <Face />,
      preferredOrder: 150,
      authorize: {
        roles: ['SUPADM', 'GRPADM', 'PRMTIT'],
        rolesDenied: ['PRTSERVICE']
      },
      device: 'WEB',
    },

    {
      id: 'FEATURE_MOBILE_QUICK_ACCESS_INCIDENT',
      location: 'MOBILE_QUICK_ACCESS',
      name: 'Incident',
      icon: <WarningRounded />,
      to: '/demarche-qualite/fiche-incident',
      options: {
        active: (path) => path.startsWith('/demarche-qualite/fiche-incident'),
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
      preferredOrder: 40,
    },
  ],
  routes: [
    {
      attachTo: 'MAIN',
      path: '/demarche-qualite',
      component: DemarcheQualitePage,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/auto-evaluation',
      component: AutoEvaluationPage,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/referentiel-qualite',
      component: ReferentielQualitePage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/outils',
      component: OutilsPage,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/outils/checklist/:id?',
      component: ChecklistEvaluationPage,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/outils/:typologie/:id?',
      component: ToolsPage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/action-operationnelle/:params?',
      component: ActionOperationnellePage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/action-operationnelle/:id?',
      component: FicheIncidentForm,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/action-operationnelle-detail/:params?',
      component: DetailsActionOperationnellePage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/fiche-incident',
      component: FicheIncidentPage,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/fiche-incident/:id',
      component: FicheIncidentFormPage,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/fiche-incident-detail',
      component: DetailsIncidentPage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/fiche-amelioration',
      component: FicheAmeliorationPage,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/fiche-amelioration/:id/:idIncident?',
      component: FicheIncidentFormPage,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/fiche-amelioration-detail/:id?',
      component: DetailsAmeliorationPage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/compte-rendu',
      component: FicheCompteRenduPage,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/compte-rendu-form/:params?',
      component: FicheCompteRenduForm,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/demarche-qualite/compte-rendu-detail/:params?',
      component: DetailsCompteRenduPage,
      activationParameters,
    },

    {
      attachTo: 'PARAMETRE',
      path: '/matrice_taches/:id?',
      component: MatricheTachePage,
    },
    {
      attachTo: 'PARAMETRE',
      path: '/check-list-qualite',
      component: ChecklistManagementPage,
    },
    {
      attachTo: 'PARAMETRE',
      path: '/check-list-qualite/:id',
      component: ChecklistForm,
    },

    {
      attachTo: 'PARAMETRE',
      path: '/demarche_qualite/exigence/:idTheme/:idSousTheme/:id',
      component: ExigenceFormPage,
    },
    {
      attachTo: 'PARAMETRE',
      path: '/demarche_qualite/:id?',
      component: ParametreDemarcheQualitePage,
    },
  ],
};

export default demarcheQualiteModule;
