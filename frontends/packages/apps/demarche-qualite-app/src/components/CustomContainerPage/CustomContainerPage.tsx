import { PageContainerWithDetail, TopSectionContainer } from '@lib/common';
import { CustomSelectMenu, NewCustomButton, OptionSelect, TableChange } from '@lib/ui-kit';
import { Box, Typography } from '@material-ui/core';
import React, { FC, PropsWithChildren, ReactNode, useState } from 'react';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import filterIconSVG from '../../assets/img/filter_alt.svg';
import { FormatListBulleted, ViewStream } from '@material-ui/icons';
import { CustomViewDetailPage } from './CustomViewDetailPage';
import { PageListe } from './CustomViewListe';
import { useDqParams } from './../../hooks/useDqParams';
import {
  Identifiable,
  InfiniteItemListProps,
  OffsetPaginationBasedVariables,
} from '@lib/common/src/components/InfiniteItemList/InfiniteItemList';
import moment from 'moment';

interface CustomContainerPageProps<ITem extends Identifiable, TData = any, TVariables = OffsetPaginationBasedVariables>
  extends Omit<InfiniteItemListProps<ITem, TData, TVariables>, 'itemRenderer'> {
  title?: string;
  onRequestGoBack?: () => void;
  onRunSearch?: (value: string) => void;
  onUserSelectChange?: (value: any) => void;
  onChangeDatePickers?: (date: any, value?: string | null) => void;
  handleClickItemImpact?: ({ data, idItem }: any) => void;
  handleClickItemStatut?: ({ data, idItem }: any) => void;
  listFilterImpact?: any;
  listFilterStatut?: any;
  addButtonLabel?: string;
  dataListMenuDetail?: {
    loading: boolean;
    results: any[];
    error: boolean;
    total: number;
  };
  dateFilterTop?: boolean;
  impactFilterTop?: boolean;
  statusFilterTop?: boolean;
  toogleViewTop?: boolean;
  loading?: boolean;
  error?: boolean;
  onClickItemListMenu?: (id: string) => void;
  documentSeleceted?: any;
  labelDeclarantInListMenu?: string;
  onClickAddButton?: () => void;
  onClickCloturer?: (data?: any, id?: string) => void;
  onClickNext?: (currentID: string) => void;
  onClickPrev?: (currentID: string) => void;
  dateLabelDetail?: string;
  typeLabelDetail?: string;
  causeLabelDetail?: string;
  origineLabelDetail?: string;
  correspondantLabelDetail?: string;
  impactLabelDetail?: string;
  descriptionLabelDetail?: string;
  titleOrigineDetail?: string;
  refetchComments?: () => void;
  refetchSmyleys?: () => void;
  activeDocument?: any;
  codeItemComment?: string;
  idSourceComment?: string;
  tableSuiviReunion?: ReactNode;
  labelTypeTableListe?: string;
  labelCauseTableListe?: string;
  labelDescriptionTableListe?: string;
  labelImpactTableListe?: string;
  labelStatusTableListe?: string;
  labelDateDebutTableListe?: string;
  labelDateResolutionTableListe?: string;
  titleButtonTableListe?: string;
  titleTopBarTableListe?: string;
  impactTableListe?: ReactNode;
  statusTableListe?: ReactNode;
  origine?: any;
  labelAddMenuAction?: string;
  handleVoirDetailMenuAction?: (data?: string) => void;
  handleEditMenuAction?: (data?: any, id?: string) => void;
  handleDeleteMenuAction?: (data?: any, id?: string) => void;
  handleApporterSolutionMenuAction?: (data?: any, id?: string) => void;
  page?: string;
  handleClickFilter?: (type: string, id: string) => void;
  dateFilter?: Date;
  handleSearchTable?: (change: TableChange) => void;
  commentaires?: {
    total: number;
    data: any[];
    loading: boolean;
  };
  userSmyleys?: {
    data: any[];
    loading: boolean;
  };
  handleGoPilotage?: () => void;
}
const FilterIcon = <img src={filterIconSVG} alt="filter" />;
export const CustomContainerPage = <
  ITem extends Identifiable,
  TData = any,
  TVariables = OffsetPaginationBasedVariables
>({
  title,
  onRequestGoBack,
  onRunSearch,
  onUserSelectChange,
  onChangeDatePickers,
  handleClickItemImpact,
  handleClickItemStatut,
  listFilterImpact,
  listFilterStatut,
  addButtonLabel,
  dataListMenuDetail,
  loading,
  error,
  onClickItemListMenu,
  documentSeleceted,
  labelDeclarantInListMenu,
  onClickAddButton,
  onClickCloturer,
  onClickNext,
  onClickPrev,
  causeLabelDetail,
  correspondantLabelDetail,
  dateLabelDetail,
  descriptionLabelDetail,
  impactLabelDetail,
  origineLabelDetail,
  typeLabelDetail,
  titleOrigineDetail,
  refetchComments,
  refetchSmyleys,
  codeItemComment,
  idSourceComment,
  tableSuiviReunion,
  labelTypeTableListe,
  labelCauseTableListe,
  labelDescriptionTableListe,
  labelImpactTableListe,
  labelStatusTableListe,
  labelDateDebutTableListe,
  labelDateResolutionTableListe,
  titleButtonTableListe,
  titleTopBarTableListe,
  handleVoirDetailMenuAction,
  labelAddMenuAction,
  handleEditMenuAction,
  handleDeleteMenuAction,
  handleApporterSolutionMenuAction,
  origine,
  page,
  dateFilterTop = true,
  impactFilterTop = true,
  statusFilterTop = true,
  toogleViewTop = true,
  handleClickFilter,
  dateFilter,
  handleSearchTable,
  commentaires,
  userSmyleys,
  handleGoPilotage,
  ...rest
}: PropsWithChildren<CustomContainerPageProps<ITem, TData, TVariables>>) => {
  const { redirect, params } = useDqParams();
  const tabActive = () => {
    if (page === 'action') {
      return 'action-operationnelle';
    }
    if (page === 'amelioration') {
      return 'fiche-amelioration';
    }
    if (page === 'reunion') {
      return 'compte-rendu';
    }
    return 'fiche-incident';
  };

  const handleChangeView = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    redirect(
      {
        view: params.view === 'list' ? 'detail' : 'list',
      },
      tabActive() as any //action-operationnelle
    );
  };

  const AdditionnalFilter = () => {
    return (
      <>
        {dateFilterTop && (
          <Box mr={1}>
            <CustomSelectMenu
              buttonLabel={!params.date ? 'Date' : moment.utc(params.date).format('DD/MM/YYYY')}
              iconFilter={FilterIcon}
              type="date"
              onChangeDatePickers={onChangeDatePickers}
              valueDatePickers={dateFilter}
            />
          </Box>
        )}
        {impactFilterTop && (
          <Box mr={1}>
            <CustomSelectMenu
              buttonLabel={
                !params.idImportance
                  ? 'Impact'
                  : (listFilterImpact || []).find((el: any) => el?.id === params.idImportance)?.libelle
              }
              iconFilter={FilterIcon}
              handleClickItem={(id) => handleClickFilter && handleClickFilter('idImportance', id)}
              listFilters={[{ id: undefined, libelle: 'Tout' }].concat(listFilterImpact || [])}
            />
          </Box>
        )}
        {statusFilterTop && (
          <Box mr={1}>
            <CustomSelectMenu
              buttonLabel={
                !params.idStatut
                  ? 'Statut'
                  : (listFilterStatut?.data || []).find((el: any) => el?.id === params.idStatut)?.libelle
              }
              iconFilter={FilterIcon}
              handleClickItem={(id) => handleClickFilter && handleClickFilter('idStatut', id)}
              listFilters={[{ id: undefined, libelle: 'Tout' }].concat(listFilterStatut?.data || [])}
            />
          </Box>
        )}
        {toogleViewTop && (
          <Box>
            <NewCustomButton
              startIcon={params.view === 'list' ? <FormatListBulleted /> : <ViewStream />}
              onClick={handleChangeView}
              theme="transparent"
              style={{ background: '#F2F2F2', textTransform: 'none' }}
              shadow={false}
            >
              {params.view === 'list' ? 'Vue liste' : 'Vue détail'}
            </NewCustomButton>
          </Box>
        )}
      </>
    );
  };

  return (
    <Box style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
      <TopSectionContainer
        title={title}
        onRequestGoBack={onRequestGoBack}
        onRunSearch={onRunSearch}
        withSearch
        onUserSelectChange={onUserSelectChange}
        withUserSelect={page ? !['amelioration', 'action'].includes(page) : true}
        elementNextToSearch={
          <Box>
            <NewCustomButton
              onClick={handleGoPilotage}
              startIcon={<EqualizerIcon />}
              theme="transparent"
              style={{ marginRight: 8, background: '#F2F2F2', textTransform: 'none' }}
              shadow={false}
            >
              Pilotage
            </NewCustomButton>
          </Box>
        }
        additionnalFilter={<AdditionnalFilter />}
        searchValue={params.searchText}
      />
      <Box style={{ flexGrow: 1 }}>
        {params.view !== 'list' ? (
          <PageContainerWithDetail
            page={page}
            addButtonLabel={addButtonLabel}
            loading={loading}
            error={error}
            onClickItem={onClickItemListMenu}
            documentSeleceted={documentSeleceted}
            onClickCloturer={onClickCloturer}
            onClickNext={onClickNext}
            onClickPrev={onClickPrev}
            causeLabelDetail={causeLabelDetail}
            correspondantLabelDetail={correspondantLabelDetail}
            dateLabelDetail={dateLabelDetail}
            descriptionLabelDetail={descriptionLabelDetail}
            impactLabelDetail={impactLabelDetail}
            origineLabelDetail={origineLabelDetail}
            typeLabelDetail={typeLabelDetail}
            titleOrigineDetail={titleOrigineDetail}
            refetchComments={refetchComments}
            refetchSmyleys={refetchSmyleys}
            activeDocument={documentSeleceted}
            codeItemComment={codeItemComment}
            idSourceComment={idSourceComment}
            onClickAddButton={onClickAddButton}
            tableSuiviReunion={tableSuiviReunion}
            labelDeclarantInListMenu={labelDeclarantInListMenu}
            origine={origine}
            handleDeleteMenuAction={handleDeleteMenuAction}
            handleEditMenuAction={handleEditMenuAction}
            handleVoirDetailMenuAction={handleVoirDetailMenuAction}
            labelAddMenuAction={labelAddMenuAction}
            handleApporterSolutionMenuAction={handleApporterSolutionMenuAction}
            commentaires={commentaires}
            userSmyleys={userSmyleys}
            {...rest}
          />
        ) : (
          <PageListe
            listFilterImpact={listFilterImpact}
            listFilterStatut={listFilterStatut}
            handleClickItemStatut={handleClickItemStatut}
            handleClickItemImpact={handleClickItemImpact}
            onClickAddTableListe={onClickAddButton}
            titleTopBarTableListe={titleTopBarTableListe}
            titleButtonTableListe={titleButtonTableListe}
            labelTypeTableListe={labelTypeTableListe}
            labelCauseTableListe={labelCauseTableListe}
            labelDescriptionTableListe={labelDescriptionTableListe}
            labelImpactTableListe={labelImpactTableListe}
            labelStatusTableListe={labelStatusTableListe}
            labelDateDebutTableListe={labelDateDebutTableListe}
            labelDateResolutionTableListe={labelDateResolutionTableListe}
            data={dataListMenuDetail}
            page={page}
            handleAddMenuAction={onClickAddButton}
            handleDeleteMenuAction={handleDeleteMenuAction}
            handleEditMenuAction={handleEditMenuAction}
            handleVoirDetailMenuAction={handleVoirDetailMenuAction}
            labelAddMenuAction={labelAddMenuAction}
            handleApporterSolutionMenuAction={handleApporterSolutionMenuAction}
            handleSearchTable={handleSearchTable}
          />
        )}
      </Box>
    </Box>
  );
};
