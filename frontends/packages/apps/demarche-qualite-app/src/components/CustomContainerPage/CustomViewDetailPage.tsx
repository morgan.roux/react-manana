import { PageContainerWithDetail } from '@lib/common';
import React, { ReactNode } from 'react';
import { FC } from 'react';

interface CustomViewDetailPageProps {
  addButtonLabel?: string;
  dataListMenuDetail?: any;
  loading?: boolean;
  error?: boolean;
  onClickItem?: (id: string) => void;
  documentSeleceted?: any;
  labelDeclarantInListMenu?: string;
  onClickAddButton?: () => void;
  onClickCloturer?: (data?: any, id?: string) => void;
  onClickNext?: (currentID: string) => void;
  onClickPrev?: (currentID: string) => void;
  dateLabelDetail?: string;
  typeLabelDetail?: string;
  causeLabelDetail?: string;
  origineLabelDetail?: string;
  correspondantLabelDetail?: string;
  impactLabelDetail?: string;
  descriptionLabelDetail?: string;
  titleOrigineDetail?: string;
  refetchComments?: () => void;
  refetchSmyleys?: () => void;
  activeDocument?: any;
  tableSuiviReunion?: ReactNode;
  codeItemComment?: string;
  idSourceComment?: string;
  origine?: any;
  page?: string;
  labelAddMenuAction?: string;
  handleVoirDetailMenuAction?: (id?: string) => void;
  handleEditMenuAction?: (data?: any, id?: string) => void;
  handleDeleteMenuAction?: (data?: any, id?: string) => void;
  handleApporterSolutionMenuAction?: (data?: any, id?: string) => void;
  commentaires?: {
    total: number;
    data: any[];
    loading: boolean;
  };
  userSmyleys?: {
    data: any[];
    loading: boolean;
  };
}
export const CustomViewDetailPage: FC<CustomViewDetailPageProps> = ({
  addButtonLabel,
  dataListMenuDetail,
  loading,
  error,
  onClickItem,
  documentSeleceted,
  labelDeclarantInListMenu,
  onClickAddButton,
  onClickCloturer,
  onClickNext,
  onClickPrev,
  causeLabelDetail,
  correspondantLabelDetail,
  dateLabelDetail,
  descriptionLabelDetail,
  impactLabelDetail,
  origineLabelDetail,
  typeLabelDetail,
  titleOrigineDetail,
  refetchComments,
  refetchSmyleys,
  codeItemComment,
  idSourceComment,
  tableSuiviReunion,
  origine,
  handleVoirDetailMenuAction,
  labelAddMenuAction,
  handleEditMenuAction,
  handleDeleteMenuAction,
  handleApporterSolutionMenuAction,
  page,
  commentaires,
  userSmyleys,
}) => {
  return <></>;
  // return (
  //   <PageContainerWithDetail
  //     page={page}
  //     addButtonLabel={addButtonLabel}
  //     dataListMenuDetail={dataListMenuDetail}
  //     loading={loading}
  //     error={error}
  //     onClickItem={onClickItem}
  //     documentSeleceted={documentSeleceted}
  //     onClickCloturer={onClickCloturer}
  //     onClickNext={onClickNext}
  //     onClickPrev={onClickPrev}
  //     causeLabelDetail={causeLabelDetail}
  //     correspondantLabelDetail={correspondantLabelDetail}
  //     dateLabelDetail={dateLabelDetail}
  //     descriptionLabelDetail={descriptionLabelDetail}
  //     impactLabelDetail={impactLabelDetail}
  //     origineLabelDetail={origineLabelDetail}
  //     typeLabelDetail={typeLabelDetail}
  //     titleOrigineDetail={titleOrigineDetail}
  //     refetchComments={refetchComments}
  //     refetchSmyleys={refetchSmyleys}
  //     activeDocument={documentSeleceted}
  //     codeItemComment={codeItemComment}
  //     idSourceComment={idSourceComment}
  //     onClickAddButton={onClickAddButton}
  //     tableSuiviReunion={tableSuiviReunion}
  //     labelDeclarantInListMenu={labelDeclarantInListMenu}
  //     origine={origine}
  //     handleDeleteMenuAction={handleDeleteMenuAction}
  //     handleEditMenuAction={handleEditMenuAction}
  //     handleVoirDetailMenuAction={handleVoirDetailMenuAction}
  //     labelAddMenuAction={labelAddMenuAction}
  //     handleApporterSolutionMenuAction={handleApporterSolutionMenuAction}
  //     commentaires={commentaires}
  //     userSmyleys={userSmyleys}
  //   />
  // );
};
