import { ActionMenuButton, statutsWithStyles } from '@lib/common';
import { Column, CustomAvatarGroup, CustomSelectMenu, FicheIncident, OptionSelect } from '@lib/ui-kit';
import { Box, Typography } from '@material-ui/core';
import moment from 'moment';
import React from 'react';

interface useColumnsProps {
  labelTypeTableListe?: string;
  labelCauseTableListe?: string;
  labelDescriptionTableListe?: string;
  labelDateResolutionTableListe?: string;
  labelImpactTableListe?: string;
  labelStatusTableListe?: string;
  labelDateDebutTableListe?: string;
  page?: string;
  handleAddMenuAction?: () => void;
  handleVoirDetailMenuAction?: (data?: string) => void;
  handleEditMenuAction?: (data?: any, id?: string) => void;
  handleDeleteMenuAction?: (data?: any, id?: string) => void;
  handleApporterSolutionMenuAction?: (data?: any, id?: string) => void;
  handleClickItemImpact?: ({ data, idItem }: any) => void;
  handleClickItemStatut?: ({ data, idItem }: any) => void;
  listFilterImpact?: any;
  listFilterStatut?: any;
  labelAddMenuAction?: string;
}

export const useColumns = ({
  labelTypeTableListe,
  labelCauseTableListe,
  labelDescriptionTableListe,
  labelDateResolutionTableListe,
  labelImpactTableListe,
  labelStatusTableListe,
  labelDateDebutTableListe,
  page,
  handleClickItemImpact,
  listFilterImpact,
  handleClickItemStatut,
  listFilterStatut,
  handleAddMenuAction,
  handleDeleteMenuAction,
  handleEditMenuAction,
  handleVoirDetailMenuAction,
  labelAddMenuAction,
  handleApporterSolutionMenuAction,
}: useColumnsProps) => {
  let columns: Column[] = [];
  if (page !== 'reunion') {
    columns = [
      {
        name: 'type.libelle',
        label: labelTypeTableListe || '-',
        renderer: (row) => {
          return <Typography>{row.type?.libelle || ''}</Typography>;
        },
      },
      {
        name: 'type.libelle',
        label: labelCauseTableListe || '-',
        renderer: (row) => {
          return <Typography>{row.cause?.libelle || ''}</Typography>;
        },
      },
      {
        name: 'type.libelle',
        label: labelDescriptionTableListe || '-',
        renderer: (row) => {
          return <Typography dangerouslySetInnerHTML={{ __html: row.description || '' }} />;
        },
      },
      {
        name: 'type.libelle',
        label: labelDateDebutTableListe || '-',
        renderer: (row) => {
          if (page === 'action') {
            return row.dateAction ? moment(row.dateAction).format('DD/MM/YYYY') : '-';
          }
          if (page === 'amelioration') {
            return row.dateAmelioration ? moment(row.dateAmelioration).format('DD/MM/YYYY') : '-';
          }
          return row.dateIncident ? moment(row.dateIncident).format('DD/MM/YYYY') : '-';
        },
      },
    ];

    if (page !== 'action' && page !== 'reunion') {
      const moreColumns: Column[] = [
        {
          name: 'type.libelle',
          label: labelDateResolutionTableListe || '-',
          renderer: (row: FicheIncident) => {
            return row.solution && row.solution?.createdAt
              ? moment(new Date(row.solution?.createdAt)).format('DD/MM/YYYY')
              : '-';
          },
        },
      ];
      columns = [...columns, ...moreColumns];
    }
    columns = [
      ...columns,
      {
        name: 'type.libelle',
        label: labelImpactTableListe || '-',
        renderer: (row) => {
          return (
            <OptionSelect
              options={listFilterImpact}
              value={row.importance}
              onChange={(id) => handleClickItemImpact && handleClickItemImpact({ data: row, idItem: id })}
              useDefaultStyle={false}
            />
          );

          /* return (
            <CustomSelectMenu
              handleClickItem={(id) => handleClickItemImpact && handleClickItemImpact({ data: row, idItem: id })}
              listFilters={listFilterImpact}
              currentSelectedData={row.importance}
            />
          );*/
        },
      },
      {
        name: 'type.libelle',
        label: labelStatusTableListe || '-',
        renderer: (row) => {
          return (
            <OptionSelect
              options={statutsWithStyles(listFilterStatut?.data || [])}
              value={row.statut}
              onChange={(id) => handleClickItemStatut && handleClickItemStatut({ data: row, idItem: id })}
              useDefaultStyle={false}
            />
          );

          /*(
            <CustomSelectMenu
              handleClickItem={(id) => handleClickItemStatut && handleClickItemStatut({ data: row, idItem: id })}
              listFilters={listFilterStatut?.data}
              currentSelectedData={row.statut}
            />
          );*/
        },
      },
      {
        name: '',
        label: '',
        renderer: (row) => {
          return (
            <Box>
              <ActionMenuButton
                page={page}
                data={row}
                handleAddMenuAction={handleAddMenuAction}
                handleDeleteMenuAction={handleDeleteMenuAction}
                handleEditMenuAction={handleEditMenuAction}
                handleVoirDetailMenuAction={handleVoirDetailMenuAction}
                labelAddMenuAction={labelAddMenuAction}
                handleApporterSolutionMenuAction={handleApporterSolutionMenuAction}
              />
            </Box>
          );
        },
      },
    ];
    return columns;
  }
  if (page === 'reunion') {
    columns = [
      {
        name: 'date',
        label: 'Date de réunion',
        renderer: (row: any) => {
          console.log('------------REUNION----------: ', row);
          return row.dateReunion ? moment(row.dateReunion).format('DD/MM/YYYY') : '-';
        },
      },
      {
        name: 'animateur',
        label: 'Animateur',
        renderer: (row: any) => {
          return <Typography>{row.animateur ? row.animateur?.fullName : '-'}</Typography>;
        },
      },
      {
        name: 'responsable',
        label: 'Responsable',
        renderer: (row: any) => {
          return <Typography>{row.responsable ? row.responsable?.fullName : '-'}</Typography>;
        },
      },
      {
        name: 'description',
        label: 'Description',
        renderer: (row: any) => {
          return <div dangerouslySetInnerHTML={{ __html: row.description }} />;
        },
      },
      {
        name: 'collaborateur',
        label: 'Collaborateurs invités',
        renderer: (row: any) => {
          return <CustomAvatarGroup max={3} users={(row.participants || []) as any} />;
        },
      },
      {
        name: 'statistique',
        label: 'Statistique',
        renderer: (row: any) => {
          return `${row.nombreTotalClotureActions || 0}/${row.nombreTotalActions || 0}`;
        },
      },
      {
        name: '',
        label: '',
        renderer: (row: any) => {
          return (
            <Box>
              <ActionMenuButton
                page={page}
                data={row}
                handleAddMenuAction={handleAddMenuAction}
                handleDeleteMenuAction={handleDeleteMenuAction}
                handleEditMenuAction={handleEditMenuAction}
                handleVoirDetailMenuAction={handleVoirDetailMenuAction}
                labelAddMenuAction={labelAddMenuAction}
                //handleApporterSolutionMenuAction={handleApporterSolutionMenuAction}
              />
            </Box>
          );
        },
      },
    ];
    return columns;
  }
  return columns;
};
