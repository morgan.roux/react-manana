import { Box } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { TableChange, TopBar } from '@lib/ui-kit';
import { NewCustomButton, OptionSelect, Table } from '@lib/ui-kit';
import { Add } from '@material-ui/icons';
import { useStyles } from './styles';
import { useColumns } from './column';

interface PageListeProps {
  titleTopBarTableListe?: string;
  titleButtonTableListe?: string;
  onClickAddTableListe?: () => void;
  labelTypeTableListe?: string;
  labelCauseTableListe?: string;
  labelDescriptionTableListe?: string;
  labelImpactTableListe?: string;
  labelStatusTableListe?: string;
  labelDateDebutTableListe?: string;
  labelDateResolutionTableListe?: string;
  page?: string;
  data?: {
    loading: boolean;
    results: any[];
    error: boolean;
    total: number;
  };
  labelAddMenuAction?: string;
  labelVoirDetailMenuAction?: string;
  handleAddMenuAction?: () => void;
  handleVoirDetailMenuAction?: (data?: string) => void;
  handleEditMenuAction?: (data?: any, id?: string) => void;
  handleDeleteMenuAction?: (data?: any, id?: string) => void;
  handleApporterSolutionMenuAction?: (data?: any, id?: string) => void;
  paddingBlock?: number;
  listFilterStatut?: any;
  listFilterImpact?: any;
  handleClickItemStatut?: ({ data, idItem }: any) => void;
  handleClickItemImpact?: ({ data, idItem }: any) => void;
  handleSearchTable?: (change: TableChange) => void;
}

export const PageListe: FC<PageListeProps> = ({
  titleTopBarTableListe,
  titleButtonTableListe,
  onClickAddTableListe,
  labelTypeTableListe,
  labelCauseTableListe,
  labelDescriptionTableListe,
  labelImpactTableListe,
  labelStatusTableListe,
  labelDateDebutTableListe,
  labelDateResolutionTableListe,
  page,
  data,
  handleVoirDetailMenuAction,
  handleAddMenuAction,
  labelAddMenuAction,
  handleEditMenuAction,
  handleDeleteMenuAction,
  handleApporterSolutionMenuAction,
  listFilterStatut,
  listFilterImpact,
  handleClickItemStatut,
  handleClickItemImpact,
  handleSearchTable,
}) => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const columns = useColumns({
    labelTypeTableListe,
    labelCauseTableListe,
    labelDescriptionTableListe,
    labelDateResolutionTableListe,
    labelImpactTableListe,
    labelStatusTableListe,
    labelDateDebutTableListe,
    page,
    handleAddMenuAction,
    handleVoirDetailMenuAction,
    handleEditMenuAction,
    handleDeleteMenuAction,
    handleApporterSolutionMenuAction,
    handleClickItemImpact,
    handleClickItemStatut,
    listFilterImpact,
    listFilterStatut,
    labelAddMenuAction,
  });

  const topBarComponent = (searchComponent: any) => (
    <TopBar
      // handleOpenAdd={handleOpenAdd}
      searchComponent={false}
      titleButton=" "
      titleTopBar={titleTopBarTableListe}
      withMoreButton
      buttons={[
        {
          component: (
            <Box mr={2}>
              <NewCustomButton onClick={onClickAddTableListe}>
                <Add />
                {titleButtonTableListe}
              </NewCustomButton>
            </Box>
          ),
        },
      ]}
      filterClassName={classes.filterContainer}
      withFilter={false}
      titleTopBarClassName={classes.titletolbarClassName}
    />
  );
  return (
    <Box padding="24px">
      <Table
        style={{ marginTop: 25 }}
        error={data?.error as Error | undefined}
        loading={data?.loading}
        search={false}
        data={data?.results || []}
        selectable={false}
        columns={columns}
        rowsTotal={data?.total || 0}
        // onRowClick={handleRowClick}
        onRunSearch={handleSearchTable}
        //   onSortColumn={handleSort}
        topBarComponent={topBarComponent}
      />
    </Box>
  );
};
