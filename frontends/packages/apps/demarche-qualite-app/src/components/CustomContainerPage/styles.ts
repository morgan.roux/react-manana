import { createStyles, makeStyles } from "@material-ui/core";

export const useStyles = makeStyles(() =>
    createStyles({
        titletolbarClassName: {
            fontSize: '18px',
            fontFamily: 'Roboto',
            fontWeight: 600,
        },
        filterContainer: {
            width: 'auto',
            display: 'flex',
        },
    })
)