import { Box, List, ListItem, ListItemText, Checkbox, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { useStyles } from './styles';
import { ExpandMore, Error } from '@material-ui/icons';
export interface ImportanceInterface {
  checked: boolean;
  __typename?: 'Importance';
  id?: string;
  ordre?: number;
  libelle?: string;
  couleur?: string;
}

interface UrgenceFilterListProps {
  importances: ImportanceInterface[] | null | undefined;
  setImportances: (importance: ImportanceInterface[] | null | undefined) => void;
}

const ImportanceFilterList: FC<UrgenceFilterListProps & RouteComponentProps> = ({
  importances,
  setImportances,
}) => {
  const classes = useStyles({});

  const handleChange = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    item: ImportanceInterface,
  ) => {
    setImportances(
      (importances || []).map(currentItem => {
        if (currentItem.id === item.id) {
          currentItem.checked = !currentItem.checked;
        }

        return currentItem;
      }),
    );
  };


  return (
    <List>
      {[...(importances || [])]
        ?.sort((a: any, b: any) => a.ordre - b.ordre)
        .map(
          (item, index) =>
            item && (
              <ListItem
                dense={true}
                button={true}
                key={`${item.id}-${item.checked}`}
                onClick={event => handleChange(event, item)}
              >
                <Box display="flex" alignItems="center">
                  <Checkbox checked={item.checked} tabIndex={-1} disableRipple={true} />
                  <ListItemText
                    primary={
                      <Box display="flex" alignItems="center">
                        <Error style={{ color: item.couleur || 'black' }} />
                        <Typography
                          className={classes.title}
                          style={{ color: item.couleur || 'black' }}
                        >
                          {item.libelle}
                        </Typography>
                      </Box>
                    }
                  />
                </Box>
                {/*
                      <Box>
                        <Typography className={classes.nbrProduits}>{item.count}</Typography>
                      </Box>
                    */}
              </ListItem>
            ),
        )}
    </List>
  );
};

export default withRouter(ImportanceFilterList);
