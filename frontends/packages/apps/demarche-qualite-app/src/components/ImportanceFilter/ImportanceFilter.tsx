import { Accordion, AccordionDetails, AccordionSummary, Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { useStyles } from './styles';
import { ExpandMore } from '@material-ui/icons';
import ImportanceFilterList, { ImportanceInterface } from './ImportanceFilterList';

interface ImportanceFilterProps {
  importances: ImportanceInterface[] | null | undefined;
  setImportances: (urgence: ImportanceInterface[] | null | undefined) => void;
  withDropdown?: boolean;
}

const ImportanceFilter: FC<ImportanceFilterProps & RouteComponentProps> = ({
  importances,
  setImportances,
  withDropdown = true,
}) => {
  const classes = useStyles({});

  return withDropdown ? (
    <Box className={classes.root}>
      <Accordion defaultExpanded={false}>
        <AccordionSummary expandIcon={<ExpandMore />}>
          <Typography className={classes.heading}>Importance</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <ImportanceFilterList importances={importances} setImportances={setImportances} />
        </AccordionDetails>
      </Accordion>
    </Box>
  ) : (
    <ImportanceFilterList importances={importances} setImportances={setImportances} />
  );
};

export default withRouter(ImportanceFilter);
