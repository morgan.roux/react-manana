import React, { FC } from 'react';
import { Comment } from '@lib/common';
import usesStyles from './styles';
import { useCommentsQuery, useSmyleysQuery } from '@lib/common/src/graphql';
import { useApplicationContext } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';
import CommentList from '@lib/common/src/components/Comment/CommentList';

interface ExigenceCommentProps {
  exigenceId: string;
}

const ExigenceComment: FC<ExigenceCommentProps> = ({ exigenceId }) => {
  const classes = usesStyles();

  const {notify, graphql, currentGroupement: groupement} = useApplicationContext();
  const getComments = useCommentsQuery({
    client: graphql,
    variables: {
      codeItem: 'EXIGENCE',
      idItemAssocie: exigenceId,
      take: 10,
      skip: 0,
    },
    fetchPolicy: 'cache-and-network',
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  // Get smyleys by groupement
  const getSmyleys = useSmyleysQuery({
    variables: {
      idGroupement: groupement && groupement.id,
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  const nbComment =
    (getComments &&
      getComments.data &&
      getComments.data.comments &&
      getComments.data.comments.total) ||
    0;

  const fetchMoreComments = () => {
    if (getComments.data && getComments.data.comments && getComments.data.comments.data) {
      const { fetchMore } = getComments;
      fetchMore({
        variables: {
          codeItem: 'EXIGENCE',
          idItemAssocie: exigenceId,
          skip: getComments.data.comments.data.length,
          take: 10,
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          if (!fetchMoreResult) return prev;
          return {
            ...prev,
            data: [
              ...((fetchMoreResult && fetchMoreResult.comments && fetchMoreResult.comments.data) ||
                []),
              ...((prev && prev.comments && prev.comments.data) || []),
            ],
          };
        },
      });
    }
  };

  return (
    <div className={classes.root}>
      {getComments.loading && !getComments.data && <SmallLoading />}
      {getComments.data && getComments.data.comments && (
        <CommentList
          key={`comment_list_${exigenceId}`}
          comments={getComments.data.comments.data}
          loading={getComments.loading && !getComments.data}
          fetchMoreComments={fetchMoreComments}
        />
      )}
      <Comment codeItem="EXIGENCE" idSource={exigenceId} />
    </div>
  );
};

export default ExigenceComment;
