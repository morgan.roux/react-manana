import { Collaborateur } from '@lib/ui-kit';

export interface SolutionProps {
  openCreateSolutionDialog: boolean;
  setOpenCreateSolutionDialog: (open: boolean) => void;
  solution?: {
    loading?: boolean;
    saving?: boolean;
    error?: Error;
    errorSaving?: Error;
    data?: any;
  };
  onRequestSaveSolution: (data: SolutionRequestSaving) => void;
  type: string;
  idItem?: string;
}

export interface SolutionRequestSaving {
  idItem: string;
  type: string;
  idUser: string;
  description: string;
}
