import UserInput from '@lib/common/src/components/UserInput';
import { CustomEditorText, CustomModal } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { SolutionProps } from './interface';
import useStyles from './style';

export const Solution: FC<SolutionProps> = ({
  openCreateSolutionDialog,
  setOpenCreateSolutionDialog,
  solution,
  onRequestSaveSolution,
  type,
  idItem,
}) => {
  const classes = useStyles();

  const [openModalCollaborateur, setOpenModalCollaborateur] = useState<boolean>(false);
  const [userParticipants, setUserParticipants] = useState<any[]>([]);
  const [description, setDescription] = useState<string>('');

  useEffect(() => {
    if (!openCreateSolutionDialog) {
      setUserParticipants([]);
      setDescription('');
    }
  }, [openCreateSolutionDialog]);

  const addAction = (): void => {
    if (userParticipants.length > 0 && idItem) {
      setOpenCreateSolutionDialog(false);
      onRequestSaveSolution({
        idItem,
        type,
        idUser: userParticipants[0].id,
        description,
      });
    }
  };

  const handleDescriptionChange = (value: string): void => {
    setDescription(value);
  };

  const isEmptyFields = (): boolean => userParticipants.length === 0 || !description;
  return (
    <>
      <CustomModal
        open={openCreateSolutionDialog}
        setOpen={setOpenCreateSolutionDialog}
        closeIcon={true}
        headerWithBgColor={true}
        fullWidth={true}
        maxWidth="sm"
        title={'Apporter une solution'}
        withBtnsActions={true}
        onClickConfirm={addAction}
        disabledButton={solution?.saving || isEmptyFields()}
        centerBtns={true}
      >
        <Box className={classes.actionForms}>
          <Box className={classes.actionForm}>
            <UserInput
              openModal={openModalCollaborateur}
              withNotAssigned={false}
              setOpenModal={setOpenModalCollaborateur}
              selected={userParticipants}
              setSelected={setUserParticipants}
              label="Apporté par*"
              key="collaborateur"
              singleSelect
            />
          </Box>
          <Box className={classes.actionForm}>
            <CustomEditorText value={description} placeholder="Description" onChange={handleDescriptionChange} />
          </Box>
        </Box>
      </CustomModal>
    </>
  );
};
