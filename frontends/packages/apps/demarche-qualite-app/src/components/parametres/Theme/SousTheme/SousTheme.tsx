import React, { ChangeEvent, FC, useState, MouseEvent } from 'react';
import { Box } from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import useStyles from './style';
import { Theme, ConfirmDeleteDialog, IExigence, Loader, CustomFullScreenModal } from '@lib/ui-kit';
import Exigences from './Exigences/Exigences';
import DetailsExigence from './../../DetailsExigence/DetailsExigence';
import { useApplicationContext } from '@lib/common';
import {
  Get_ThemeDocument,
  Get_ThemeQuery,
  Get_ThemeQueryVariables,
  useDeleteDqSousThemeMutation,
  useDelete_ExigenceMutation,
  useGet_Full_Sous_ThemeLazyQuery,
  useUpdate_Sous_ThemeMutation,
} from '@lib/common/src/federation';
import { Add, Delete, Edit } from '@material-ui/icons';
import ThemeAdd from '../../ThemeAdd/ThemeAdd';

interface SousThemeComponentProps {
  sousThemes: any[];
  themeId: string;
}

const SousThemeComponent: FC<SousThemeComponentProps & RouteComponentProps> = ({ sousThemes, themeId, history }) => {
  const classes = useStyles({});
  const [show, setShow] = useState<boolean>(false);
  const [showDeleteButton, setShowDeleteButton] = useState<boolean>(false);
  const [current, setCurrent] = useState<any>();
  const [numeroOrdre, setNumeroOrdre] = useState<number | undefined>();
  const [nom, setNom] = useState<string>('');
  const [currentId, setCurrentId] = useState<string>('');

  const [openDeleteExigenceDialog, setOpenDeleteExigenceDialog] = useState<boolean>(false);
  const [exigenceToDelete, setExigenceToDelete] = useState<IExigence | null>(null);

  const [openDetailExigenceDialog, setOpenDetailExigenceDialog] = useState<boolean>(false);
  const [exigenceToShowDetail, setExigenceToShowDetail] = useState<IExigence | null>(null);
  const [exigenceToShowDetailSousTheme, setExigenceToShowDetailSousTheme] = useState<IExigence | null>(null);

  const reInit = () => {
    setNumeroOrdre(undefined);
    setNom('');
  };

  const { federation, notify } = useApplicationContext();
  const handleNumeroOrdreChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNumeroOrdre(parseInt((event.target as HTMLInputElement).value, 10));
  };

  const handleNomChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNom((event.target as HTMLInputElement).value);
  };

  const [loadSousTheme, loadingSousTheme] = useGet_Full_Sous_ThemeLazyQuery({
    client: federation,
    fetchPolicy: 'network-only', // TODO : Use cache
  });

  const [deleteSousTheme, deletionSousTheme] = useDeleteDqSousThemeMutation({
    onError: () => {
      notify({
        type: 'success',
        message: `Une erreur s'est produite lors de la suppression du sous-thème.`,
      });
    },

    onCompleted: () => {
      setShowDeleteButton(false);
      notify({
        type: 'success',
        message: 'Le sous-thème a été supprimé avec succès !',
      });
    },
    update: (cache, deletionResult) => {
      if (deletionResult.data && deletionResult.data.deleteDQSousTheme && deletionResult.data.deleteDQSousTheme.id) {
        const previousTheme = cache.readQuery<Get_ThemeQuery, Get_ThemeQueryVariables>({
          query: Get_ThemeDocument,
          variables: {
            id: themeId,
          },
        })?.dqTheme;

        const deleteId = deletionResult.data.deleteDQSousTheme.id;

        const deletedSousTheme = {
          ...previousTheme,
          nombreSousThemes: ((previousTheme as any)?.sousThemes?.length || 0) - 1,
          sousThemes: sousThemes.filter((item) => item.id !== deleteId),
        };

        cache.writeQuery<Get_ThemeQuery, Get_ThemeQueryVariables>({
          query: Get_ThemeDocument,
          data: {
            dqTheme: deletedSousTheme,
          } as any,
        });
      }
    },

    client: federation,
  });

  const [updateSousTheme, modificationSousTheme] = useUpdate_Sous_ThemeMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de la modification du sous-thème.`,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      notify({
        type: 'success',
        message: 'Le sous-thème a été modifié avec succès !',
      });
    },
    client: federation,
  });

  const [deleteExigence, deletionExigence] = useDelete_ExigenceMutation({
    client: federation,
    onError: () => {
      notify({
        type: 'error',
        message: `Une s'est produite lors de la suppression de l'exigence`,
      });
    },
    onCompleted: () => {
      setExigenceToDelete(null);
      setOpenDeleteExigenceDialog(false);

      notify({
        type: 'success',
        message: "L'exigence a été supprimée avec succès !",
      });
    },
    update: (cache, deletionExigenceResult) => {
      const deletedExigenceId = deletionExigenceResult.data?.deleteDQExigence?.id;
      if (deletedExigenceId) {
        const previousTheme = cache.readQuery<Get_ThemeQuery, Get_ThemeQueryVariables>({
          query: Get_ThemeDocument,
          variables: {
            id: themeId,
          },
        })?.dqTheme;

        const updatedTheme = {
          ...previousTheme,
          sousThemes: ((previousTheme as any).sousThemes || []).map((sousTheme: any) => {
            const foundExigenceInSousTheme = (sousTheme.exigences || []).some(
              ({ id }: any) => id === deletedExigenceId
            );

            return {
              ...sousTheme,
              nombreExigences: foundExigenceInSousTheme
                ? (sousTheme.nombreExigences || 0) - 1
                : sousTheme.nombreExigences,
              exigences: foundExigenceInSousTheme
                ? (sousTheme.exigences || []).filter(({ id }: any) => id !== deletedExigenceId)
                : sousTheme.exigences,
            };
          }),
        };

        cache.writeQuery<Get_ThemeQuery, Get_ThemeQueryVariables>({
          query: Get_ThemeDocument,
          variables: {
            id: themeId,
          },
          data: {
            dqTheme: updatedTheme as any,
          },
        });
      }
    },
  });

  const handleLoadSousTheme = (id: string, event: MouseEvent, expended: boolean): void => {
    if (expended) {
      loadSousTheme({
        variables: {
          id,
        },
      });
    }
  };

  const handleChange = (element: any): void => {
    setCurrent(element);
    setShowDeleteButton(!showDeleteButton);
  };

  const handleAddExigence = (sousTheme: any) => {
    history.push(`/db/demarche_qualite/exigence/${themeId}/${sousTheme.id}/new`);
  };

  const handleUpdate = (sousTheme: any) => {
    setCurrent(sousTheme);
    setCurrentId(sousTheme.id); // A supprimer peut-être ?
    setNumeroOrdre(sousTheme.ordre);
    setNom(sousTheme.nom);
    setShow(!show);
  };

  const handleRequestEditExigence = (sousTheme: any, exigence: IExigence) => {
    history.push(`/db/demarche_qualite/exigence/${themeId}/${sousTheme.id}/${exigence.id}`);
  };

  const handleRequestDeleteExigence = (exigence: IExigence) => {
    setExigenceToDelete(exigence);
    setOpenDeleteExigenceDialog(true);
  };

  const handleRequestDetailExigence = (sousTheme: any, exigence: IExigence) => {
    setExigenceToShowDetail(exigence);
    setExigenceToShowDetailSousTheme(sousTheme);
    setOpenDetailExigenceDialog(true);
  };

  const handleOutilClick = (typologie: string, id: string) => {
    if (typologie === 'checklist') {
      history.push(`/demarche-qualite/outils/checklist`);
    } else {
      history.push(`/demarche-qualite/outils/${typologie}/${id}`);
    }
  };

  const doUpdate = (): void => {
    updateSousTheme({
      variables: {
        id: currentId,
        sousThemeInput: {
          ordre: numeroOrdre,
          nom: nom,
        },
      },
    });
  };

  const doDelete = (): void => {
    deleteSousTheme({
      variables: {
        id: current.id,
      },
    });
  };

  const doDeleteExigence = (): void => {
    if (exigenceToDelete?.id) {
      deleteExigence({
        variables: {
          id: exigenceToDelete.id,
        },
      });
    }
  };

  return (
    <Box className={classes.content}>
      {sousThemes &&
        [...(sousThemes || [])]
          .sort((a, b) => a.ordre - b.ordre)
          .map((sousTheme: any) => (
            <Theme
              key={sousTheme.id}
              theme={sousTheme}
              summarySubContent={
                <Box className={classes.nombreExigences}>{`Nombre d'exigence : ${sousTheme.nombreExigences}`}</Box>
              }
              checkList={true}
              onClick={handleLoadSousTheme.bind(null, sousTheme.id)}
              textStyle={{ fontWeight: 'bold', color: '#000' }}
              moreOptions={[
                {
                  menuItemLabel: {
                    title: 'Ajouter exigence',
                    icon: <Add />,
                  },
                  onClick: handleAddExigence.bind(null, sousTheme),
                },
                {
                  menuItemLabel: {
                    title: 'Modifier sous-thème',
                    icon: <Edit />,
                  },
                  onClick: handleUpdate.bind(null, sousTheme),
                },
                {
                  menuItemLabel: {
                    title: 'Supprimer sous-thème',
                    icon: <Delete />,
                  },
                  onClick: handleChange.bind(null, sousTheme),
                },
              ]}
            >
              <Exigences
                onRequestAdd={handleAddExigence.bind(null, sousTheme)}
                onRequestDelete={handleRequestDeleteExigence}
                onRequestEdit={handleRequestEditExigence.bind(null, sousTheme)}
                onRequestDetail={handleRequestDetailExigence.bind(null, sousTheme)}
                error={loadingSousTheme.error}
                loading={loadingSousTheme.loading}
                data={(loadingSousTheme?.data?.dqSousTheme?.exigences as any) || []}
              />
            </Theme>
          ))}
      <ThemeAdd
        title={'Modification de sous-thème'}
        saving={modificationSousTheme.loading}
        show={show}
        setShow={setShow}
        value1={numeroOrdre}
        value2={nom}
        setValue1={handleNumeroOrdreChange}
        setValue2={handleNomChange}
        onSubmit={doUpdate}
      />
      <ConfirmDeleteDialog
        open={openDeleteExigenceDialog}
        setOpen={setOpenDeleteExigenceDialog}
        onClickConfirm={doDeleteExigence}
        isLoading={deletionExigence.loading}
      />

      <ConfirmDeleteDialog
        open={showDeleteButton}
        setOpen={setShowDeleteButton}
        onClickConfirm={doDelete}
        isLoading={deletionSousTheme.loading}
      />
      {/* TODO: Change*/}
      <CustomFullScreenModal
        title="Détails d'exigence"
        actionButton="Modifier"
        onClickConfirm={() => {
          if (exigenceToShowDetail) {
            handleRequestEditExigence(exigenceToShowDetailSousTheme, exigenceToShowDetail);
          }
        }}
        open={openDetailExigenceDialog}
        setOpen={setOpenDetailExigenceDialog}
      >
        {exigenceToShowDetail && exigenceToShowDetailSousTheme ? (
          <DetailsExigence
            onRequestEdit={() => handleRequestEditExigence(exigenceToShowDetailSousTheme, exigenceToShowDetail)}
            onRequestGoBack={() => setOpenDetailExigenceDialog(false)}
            onOutilClick={handleOutilClick}
            exigence={exigenceToShowDetail}
            sousTheme={exigenceToShowDetailSousTheme as any}
          />
        ) : (
          <Loader />
        )}
      </CustomFullScreenModal>
    </Box>
  );
};

export default withRouter(SousThemeComponent);
