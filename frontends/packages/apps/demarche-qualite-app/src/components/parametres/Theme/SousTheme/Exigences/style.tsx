import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
    },

    header: {
      padding: 10,
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginBottom: 20,
    },

    content: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      padding: 5,
      marginTop: 8,
    },

    title: {
      display: 'block',
    },

    span: {
      color: '#E34168',
    },

    spanBold: {
      fontWeight: 'bold',
    },

    btn: {
      textTransform: 'none',
      marginRight: 8,
    },
    exigenceContent: {
      padding: 15,
    },

    modalContent: {
      display: 'flex',
      flexDirection: 'column',
    },
  })
);

export default useStyles;
