import { Box, Divider, Typography } from '@material-ui/core';
import React, { FC, useState, ChangeEvent } from 'react';
import useStyles from './style';
import { CustomButton, NewCustomButton, NoItemContentImage } from '@lib/ui-kit';
import AddIcon from '@material-ui/icons/Add';
import { RouteComponentProps, withRouter, useLocation } from 'react-router';
import ThemeAdd from '../ThemeAdd/ThemeAdd';
import { useApplicationContext } from '@lib/common';
import {
  Get_ThemeDocument,
  Get_ThemeQuery,
  Get_ThemeQueryVariables,
  useCreate_Sous_ThemeMutation,
} from '@lib/common/src/federation';
import SousThemeComponent from './SousTheme/SousTheme';

const DetailsTheme: FC<RouteComponentProps> = ({ history }) => {
  const classes = useStyles({});
  const location: any = useLocation();
  const state = location.state;
  const item = state && state.theme; // Type thème avec sous-thèmes

  const { notify, federation } = useApplicationContext();

  const [show, setShow] = useState<boolean>(false);
  const [numeroOrdre, setNumeroOrdre] = useState<number | undefined>();
  const [nom, setNom] = useState<string>('');
  const reInit = () => {
    setNumeroOrdre(undefined);
    setNom('');
  };

  const handleNumeroOrdreChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNumeroOrdre(parseInt((event.target as HTMLInputElement).value, 10));
  };

  const handleNomChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNom((event.target as HTMLInputElement).value);
  };

  const [addSousTheme, creationSousTheme] = useCreate_Sous_ThemeMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une s'est produite lors de l'ajout du nouveau sous-thème`,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      notify({
        type: 'success',
        message: 'Le sous-thème a été ajouté avec succès !',
      });
    },
    update: (cache, creationResult) => {
      if (creationResult.data && creationResult.data.createDQSousTheme) {
        const previousTheme = cache.readQuery<Get_ThemeQuery, Get_ThemeQueryVariables>({
          query: Get_ThemeDocument,
          variables: {
            id: item.id,
          },
        })?.dqTheme;

        const updatedTheme = {
          ...previousTheme,
          nombreSousThemes: ((previousTheme as any)?.sousThemes?.length || 0) + 1,
          sousThemes: [creationResult.data.createDQSousTheme, ...((previousTheme as any)?.sousThemes || [])],
        };

        cache.writeQuery<Get_ThemeQuery, Get_ThemeQueryVariables>({
          query: Get_ThemeDocument,
          variables: {
            id: item.id,
          },
          data: {
            dqTheme: updatedTheme,
          } as any,
        });
      }
    },
    client: federation,
  });

  const handleChangeShow = () => {
    setShow(!show);
  };

  const handleSubmit = (): void => {
    addSousTheme({
      variables: {
        idTheme: item.id,
        sousThemeInput: {
          ordre: numeroOrdre,
          nom: nom,
        },
      },
    });
  };

  return (
    <>
      {state ? (
        <Box className={classes.root}>
          <Box className={classes.header}>
            <div>
              <Typography color="primary" className={classes.spanBold}>{`${item.ordre}-${item.nom}`}</Typography>
            </div>
            <div>
              <NewCustomButton
                startIcon={<AddIcon />}
                children="Ajouter un sous-thème"
                className={classes.btn}
                onClick={handleChangeShow}
              />
              <ThemeAdd
                title={'Ajout de sous-thème'}
                saving={creationSousTheme.loading}
                show={show}
                setShow={setShow}
                onSubmit={handleSubmit}
                value1={numeroOrdre}
                value2={nom}
                setValue1={handleNumeroOrdreChange}
                setValue2={handleNomChange}
              />
            </div>
          </Box>
          <Divider />
          {item.sousThemes && item.sousThemes.length > 0 && (
            <SousThemeComponent sousThemes={item.sousThemes} themeId={item.id} />
          )}
        </Box>
      ) : (
        <NoItemContentImage
          title="Aucun thème selectionné"
          subtitle="Choisissez un en cliquant sur la partie gauche pour voir le contenu dans cette partie"
        />
      )}
    </>
  );
};

export default withRouter(DetailsTheme);
