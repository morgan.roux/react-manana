import { ApolloProvider } from '@apollo/client';
import { useApplicationContext } from '@lib/common';
import React, { FC } from 'react';
import ChecklistManagement from './ChecklistManagement';

// FIXME : Temporary solution
const WithApolloFederationClient: FC<{}> = ({}) => {
  const { federation } = useApplicationContext();
  return (
    <ApolloProvider client={federation}>
      <ChecklistManagement />
    </ApolloProvider>
  );
};

export default WithApolloFederationClient;
