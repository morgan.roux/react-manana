import { ApolloProvider } from '@apollo/client';
import { useApplicationContext } from '@lib/common';
import React, { FC } from 'react';
import ChecklistForm from './ChecklistForm';

// FIXME : Temporary solution
const WithApolloFederationClient: FC<{}> = ({}) => {
  const { federation } = useApplicationContext();

  return (
    <ApolloProvider client={federation}>
      <ChecklistForm />
    </ApolloProvider>
  );
};

export default WithApolloFederationClient;
