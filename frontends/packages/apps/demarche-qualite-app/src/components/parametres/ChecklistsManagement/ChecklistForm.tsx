import React, { FC, useEffect, useState } from 'react';
import { Button } from '@material-ui/core';
import Image from './../../../../assets/img/mutation-success.png';
import { useLazyQuery, useMutation, useApolloClient } from '@apollo/client';
import { Checklist, ChecklistFormPage, NoItemContentImage } from '@lib/ui-kit';

import { withRouter, RouteComponentProps } from 'react-router-dom';
import { useApplicationContext } from '@lib/common';
import {
  Get_ChecklistsDocument,
  Get_ChecklistsQuery,
  Get_ChecklistsQueryVariables,
  useCreate_ChecklistMutation,
  useGet_ChecklistLazyQuery,
  useGet_ChecklistsQuery,
  useUpdate_ChecklistMutation,
} from '@lib/common/src/federation';

const ChecklistForm: FC<RouteComponentProps> = ({ match: { params }, history }) => {
  const client = useApolloClient();

  const { auth, currentPharmacie: pharmacie, federation, notify } = useApplicationContext();
  const idPharmacie = pharmacie.id;
  const variables = !auth.isSupAdminOrIsGpmAdmin ? { idPharmacie, onlyChecklistsPharmacie: true } : undefined;

  const checklistId = (params as any).id;
  const isNewChecklist = 'new' === checklistId;
  const [checklist, setCheckList] = useState<Checklist>();
  const [displayNotificationPanel, setDisplayNotificationPanel] = useState<boolean>(false);

  const [loadChecklist, { called, loading, data }] = useGet_ChecklistLazyQuery({
    variables: {
      id: checklistId,
    },
    client: federation,
    fetchPolicy: 'network-only',
  });

  const [createChecklist, creationChecklist] = useCreate_ChecklistMutation({
    onError: () => {
      notify({
        type: 'success',
        message: isNewChecklist
          ? `Une erreur s'est produite lors de l'ajout de checklist.`
          : `Une erreur s'est produite lors de modification de checklist.`,
      });
    },
    onCompleted: () => {
      setDisplayNotificationPanel(true);
    },
    update: (cache, creationResult) => {
      if (creationResult.data && creationResult.data.createDQChecklist) {
        const previousList = cache.readQuery<Get_ChecklistsQuery, Get_ChecklistsQueryVariables>({
          query: Get_ChecklistsDocument,
          variables,
        })?.dqChecklists;
        cache.writeQuery<Get_ChecklistsQuery, Get_ChecklistsQueryVariables>({
          query: Get_ChecklistsDocument,
          variables,
          data: {
            dqChecklists: [creationResult.data.createDQChecklist, ...(previousList || [])],
          },
        });
      }
    },
    client: federation,
  });

  const [updateChecklist, modificationChecklist] = useUpdate_ChecklistMutation({
    client: federation,
    onCompleted: () => {
      setDisplayNotificationPanel(true);
    },
  });

  useEffect(() => {
    if (!isNewChecklist) {
      if (!called) {
        loadChecklist();
      } else if (data?.dqChecklist) {
        setCheckList(data.dqChecklist as any); // FIXME : Fix graphql schema
      }
    }
  }, [checklistId, loading]);

  const handleSave = (checklistToSave: Checklist) => {
    if (isNewChecklist) {
      createChecklist({
        variables: {
          checklistInput: {
            ...(checklistToSave as any),
            idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? idPharmacie : undefined,
          },
        },
      });
    } else if (checklistToSave.id) {
      updateChecklist({
        variables: {
          id: checklistToSave.id,
          checklistInput: {
            ...(checklistToSave as any),
            id: undefined,
          },
        },
      });
    }
  };

  const handleGoBack = () => {
    history.push('/db/check-list-qualite');
  };

  return displayNotificationPanel ? (
    <NoItemContentImage
      src={Image}
      title={isNewChecklist ? `Ajout de check-list réussi` : `Modification de check-list réussie`}
      subtitle={
        isNewChecklist
          ? `Le check-list que vous venez de créer est maintenant dans la liste`
          : `Le check-list que vous venez de modifier est à jour.`
      }
    >
      <Button onClick={handleGoBack}>Retour</Button>
    </NoItemContentImage>
  ) : (
    <ChecklistFormPage
      key={Math.random()}
      loading={loading}
      saving={creationChecklist.loading || modificationChecklist.loading}
      onRequestGoBack={handleGoBack}
      onRequestSave={handleSave}
      checklist={!isNewChecklist ? checklist : undefined}
      mode={isNewChecklist ? 'creation' : 'modification'}
    />
  );
};

export default withRouter(ChecklistForm);
