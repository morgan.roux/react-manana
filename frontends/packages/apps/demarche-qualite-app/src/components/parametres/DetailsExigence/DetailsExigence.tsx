import { Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import {
  Exigence,
  IExigence,
  ISousTheme,
  ExigenceTab,
} from '@lib/ui-kit';
import { RouteComponentProps, withRouter } from 'react-router';
import useStyles from './style';
import { ExigenceComment } from '../../ExigenceComment';

interface DetailsExigenceProps {
  sousTheme: ISousTheme;
  exigence: IExigence;
  onRequestEdit: () => void;
  onRequestGoBack: () => void;
  onOutilClick: (typologie: string, id: string) => void;
}

// Prend en paramètre une exigence
const DetailsExigence: FC<DetailsExigenceProps & RouteComponentProps> = ({
  exigence,
  sousTheme,
  onOutilClick,
}) => {
  const classes = useStyles({});

  return (
    <Box className={classes.root}>
      {/*<CustomContainer
        bannerBack={true}
        filled={true}
        onBackClick={onRequestGoBack}
        bannerTitle="Détails d'exigence"
        bannerRight={
          <CustomButton
            children="Modifier"
            color="primary"
            className={classes.btn}
            onClick={onRequestEdit}
          />
        }
      />*/}
      <Box className={classes.content}>
        <Box className={classes.textContent}>
          <Typography color="secondary">{`${sousTheme.ordre}-${sousTheme.nom}`}</Typography>
        </Box>
        <Exigence exigence={exigence} />
        <ExigenceTab
          commentComponentRenderer={exigence => <ExigenceComment exigenceId={exigence.id} />}
          onOutilClick={onOutilClick}
          exigence={exigence}
        />
      </Box>
    </Box>
  );
};

export default withRouter(DetailsExigence);
