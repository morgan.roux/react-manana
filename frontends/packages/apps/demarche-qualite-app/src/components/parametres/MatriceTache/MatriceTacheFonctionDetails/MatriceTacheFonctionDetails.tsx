import { Box, Divider, Typography } from '@material-ui/core';
import React, { FC, useState, ChangeEvent } from 'react';
import useStyles from './style';
import { CustomButton, NewCustomButton, NoItemContentImage } from '@lib/ui-kit';
import AddIcon from '@material-ui/icons/Add';
import { RouteComponentProps, withRouter, useLocation } from 'react-router';
import MatriceTacheForm from '../MatriceTacheForm/MatriceTacheForm';

import TacheComponent from './Tache/Tache';
import { TITULAIRE_PHARMACIE, useApplicationContext } from '@lib/common';
import {
  Get_FonctionDocument,
  Get_FonctionQuery,
  Get_FonctionQueryVariables,
  useCreate_TacheMutation,
  useGet_FonctionQuery,
} from '@lib/common/src/federation';

const DetailsTheme: FC<RouteComponentProps> = ({ history }) => {
  const classes = useStyles({});
  const location: any = useLocation();
  const state = location.state;
  const item = state && state.fonction;

  const { user, currentPharmacie: pharmacie, notify, federation } = useApplicationContext();
  const isTitulairePharmacie = user?.role?.code === TITULAIRE_PHARMACIE;

  const [show, setShow] = useState<boolean>(false);
  const [ordre, setOrdre] = useState<number | undefined>();
  const [libelle, setLibelle] = useState<string>('');

  const reInit = () => {
    setOrdre(undefined);
    setLibelle('');
  };

  const [addTache, creationTache] = useCreate_TacheMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une s'est produite lors de l'ajout de la nouvelle fonction`,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      notify({
        type: 'success',
        message: 'La fonction a été ajoutée avec succès !',
      });
    },
    update: (cache, creationResult) => {
      if (creationResult.data?.createOneDQMTTache) {
        const previousFonction = cache.readQuery<Get_FonctionQuery, Get_FonctionQueryVariables>({
          query: Get_FonctionDocument,
          variables: {
            idPharmacie: pharmacie.id,
            id: item.id,
          },
        })?.dQMTFonction;

        const updatedFonction = {
          ...previousFonction,
          nombreTaches: ((previousFonction as any)?.nombreTaches || 0) + 1,
          taches: [creationResult.data.createOneDQMTTache, ...((previousFonction as any)?.taches || [])],
        };

        useGet_FonctionQuery;

        cache.writeQuery<Get_FonctionQuery, Get_FonctionQueryVariables>({
          query: Get_FonctionDocument,
          variables: {
            idPharmacie: pharmacie.id,
            id: item.id,
          },
          data: {
            dQMTFonction: updatedFonction,
          } as any,
        });
      }
    },
    client: federation,
  });

  const toggleShow = () => {
    setShow(!show);
  };

  const handleSave = (): void => {
    if (ordre) {
      addTache({
        variables: {
          input: {
            dQMTTache: {
              idFonction: item.id as any,
              ordre,
              libelle,
            },
          },
        },
      });
    }
  };

  return (
    <>
      {state ? (
        <Box className={classes.root}>
          <Box className={classes.header}>
            <div>
              <Typography className={classes.spanBold} color="primary">{`${item.ordre}-${item.libelle}`}</Typography>
            </div>
            <div>
              {!isTitulairePharmacie && (
                <NewCustomButton
                  startIcon={<AddIcon />}
                  children="Ajouter une fonction"
                  className={classes.btn}
                  onClick={toggleShow}
                />
              )}
              <MatriceTacheForm
                saving={creationTache.loading}
                title={'Ajout de fonction'}
                show={show}
                setShow={setShow}
                onSubmit={handleSave}
                ordre={ordre}
                libelle={libelle}
                setLibelle={setLibelle}
                setOrdre={setOrdre}
              />
            </div>
          </Box>
          <Divider />
          <TacheComponent fonctionId={item.id} />
        </Box>
      ) : (
        <NoItemContentImage
          title="Aucune fonction de Base selectionnée"
          subtitle="Choisissez un en cliquant sur la partie gauche pour voir le contenu dans cette partie"
        />
      )}
    </>
  );
};

export default withRouter(DetailsTheme);
