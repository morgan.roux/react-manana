import { GridPDFExport } from '@progress/kendo-react-pdf';
import { ExcelExport, ExcelExportColumn } from '@progress/kendo-react-excel-export';
import { Grid, GridColumn } from '@progress/kendo-react-grid';
import '@progress/kendo-theme-default/dist/all.css';
import React, { useState } from 'react';
import { Backdrop } from '@lib/ui-kit';
import { formatFonctions } from './export-matrice';
import './kendo-styles.css';

import moment from 'moment';
import { useNiveauMatriceFonctions } from '../hooks';
import { useApplicationContext } from '@lib/common';
import {
  useFull_CollaborateursLazyQuery,
  useGet_Fonctions_With_Responsable_InfosLazyQuery,
} from '@lib/common/src/federation';
export interface Pharmacie {
  id: string;
  nom: string;
}

const couleurs = [
  {
    id: 'ckgqbdfpvvacy07441e1ynzvj',
    code: '#C1CAD6',
    libelle: 'Gris',
    isRemoved: false,
    __typename: 'Couleur',
  },
  {
    id: 'ckgqbdgbxvad30744rxkvwffy',
    code: '#7F2CCB',
    libelle: 'Violet',
    isRemoved: false,
    __typename: 'Couleur',
  },
  {
    id: 'ckgqbdhbivad80744f9rtqbjx',
    code: '#1982C4',
    libelle: 'Bleu',
    isRemoved: false,
    __typename: 'Couleur',
  },
  {
    id: 'ckgqbdhxtvadu0744u27c0h3i',
    code: '#8AC926',
    libelle: 'Vert',
    isRemoved: false,
    __typename: 'Couleur',
  },
  {
    id: 'ckgqbdij8vadz07448u2yduiy',
    code: '#FFCA3A',
    libelle: 'Jaune',
    isRemoved: false,
    __typename: 'Couleur',
  },
  {
    id: 'ckgqbdj3qvae40744jddmr16k',
    code: '#E57A44',
    libelle: 'Orange',
    isRemoved: false,
    __typename: 'Couleur',
  },
  {
    id: 'ckgqbdkcjvae90744u4ftrbl2',
    code: '#8E6E53',
    libelle: 'Marron',
    isRemoved: false,
    __typename: 'Couleur',
  },
  {
    id: 'ckgqbdl9tvaee0744x3eza04u',
    code: '#FF3A20',
    libelle: 'Rouge',
    isRemoved: false,
    __typename: 'Couleur',
  },
  {
    id: 'ckgqbdlvvvaej07446jh244ml',
    code: '#F374AE',
    libelle: 'Rose',
    isRemoved: false,
    __typename: 'Couleur',
  },
  {
    id: 'ckhc14qgqwidj079480qsltuu',
    code: '#c01818',
    libelle: 'Rouge',
    isRemoved: true,
    __typename: 'Couleur',
  },
];

const ExportMatriceTache = (): {
  component: React.ReactNode;
  launch: (pharmacie: Pharmacie, format: 'pdf' | 'xlsx') => void;
} => {
  const { notify, federation } = useApplicationContext();

  let exportPdfEl: any = null;
  let exportExcelEl: any = null;
  const [exportFormat, setExportFormat] = useState<'pdf' | 'xlsx'>('pdf');
  const [exportPharmacie, setExportPharmacie] = useState<Pharmacie>();
  const niveauMatriceFonctions = useNiveauMatriceFonctions();

  const [loadCollaborateurs, loadingCollaborateurs] = useFull_CollaborateursLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de l'export de la matrice des fonctions de Base`,
      });
    },
    onCompleted: () => {
      // Call load
      if (exportPharmacie) {
        loadMatricePharmacie({
          variables: {
            idPharmacie: exportPharmacie.id,
          },
        });
      }
    },
  });

  const [loadMatricePharmacie, loadingMatricePharmacie] = useGet_Fonctions_With_Responsable_InfosLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de l'export de la matrice des fonctions de Base`,
      });
    },
    onCompleted: (result) => {
      if (exportFormat === 'pdf' && exportPdfEl) {
        exportPdfEl.save(formatFonctions(result.dQMTFonctions.nodes as any, niveauMatriceFonctions));
      } else if (exportExcelEl) {
        exportExcelEl.save(formatFonctions(result.dQMTFonctions.nodes as any, niveauMatriceFonctions));
      }
    },
  });

  const handleRequestExportMatricePharmacie = (pharmacie: Pharmacie, format: 'pdf' | 'xlsx'): void => {
    setExportFormat(format);
    setExportPharmacie(pharmacie);
    loadCollaborateurs({
      variables: {
        idPharmacie: pharmacie.id,
      },
    });
  };

  const fonctionHeader = () => {
    return <p>{niveauMatriceFonctions === 1 ? 'Fonctions' : ''}</p>;
  };

  const twoDigits = (value: number): string => (value < 10 ? `0${value}` : `${value}`);

  const currentDateTime = () => {
    const d = new Date();
    return `${twoDigits(d.getDate())}/${twoDigits(d.getMonth() + 1)}/${d.getFullYear()} à ${twoDigits(
      d.getHours()
    )}h${twoDigits(d.getMinutes())}`;
  };
  const tacheHeader = (props: any) => {
    const style = {
      fontSize: 'x-small',
      border: 0,
      margin: 0,
      textAlign: 'center' as 'center',
    };

    return (
      <div>
        <p style={style}>{props.title}</p>
        <p style={style}>{exportPharmacie?.nom || ''}</p>
        <p style={style}>Le {currentDateTime()}</p>
      </div>
    );
  };

  const cellFonction = (props: any) => {
    const index: number = props.dataItem.ordreFonction % couleurs.length;
    const inTheMiddle = props.dataItem.index === Math.round(props.dataItem.nombreTaches / 2);
    const onLast = props.dataItem.index >= props.dataItem.nombreTaches - 1;

    const style = {
      backgroundColor: couleurs[index].code,
      fontSize: 'x-small',
      borderBottom: niveauMatriceFonctions === 1 || onLast ? 'ridge 1px' : undefined,
      borderRight: 'ridge 1px',
      minHeight: '200px',
      width: '50px',
      color: 'white',
    };

    const pstyle = {
      transform: 'rotate(-90deg)',
      paddingTop: '30px',
      paddingBottom: '30px',
    };

    if (niveauMatriceFonctions === 1 || inTheMiddle) {
      return (
        <td style={style}>
          <p style={niveauMatriceFonctions === 1 ? undefined : pstyle}>{props.dataItem[props.field]}</p>
        </td>
      );
    } else {
      return (
        <td
          style={{
            backgroundColor: couleurs[index].code,
            borderBottom: onLast ? 'ridge 1px' : undefined,
            borderRight: 'ridge 1px',
            color: 'white',
          }}
        >
          <p></p>
        </td>
      );
    }
  };

  const cellTache = (props: any) => {
    const index = props.dataItem.ordreFonction % couleurs.length;

    const style = {
      backgroundColor: couleurs[index].code,
      fontSize: 'x-small',
      borderBottom: 'ridge 1px',
      borderRight: 'ridge 1px',
      paddingLeft: '3px',
      textAlign: 'left' as any,
      color: 'white',
      paddingRight: '3px',
    };

    return <td style={style}>{props.dataItem[props.field]}</td>;
  };

  const cellCollaborateur = (props: any) => {
    const index: number = props.dataItem.ordreFonction % couleurs.length;
    const style = {
      backgroundColor: couleurs[index].code,
      fontSize: 'x-small',
      borderBottom: 'ridge 1px',
      borderRight: 'ridge 1px',
      paddingLeft: '3px',
      color: 'white',
      textAlign: 'center' as any,
      paddingRight: '3px',
    };

    return <td style={style}>{props.dataItem[props.field]}</td>;
  };

  const PDFExportPageTemplate = (props: any) => {
    const style = {
      position: 'absolute' as any,
      bottom: 10,
      right: 40,
    };
    return (
      <p style={style}>
        {props.pageNum}/{props.totalPages}
      </p>
    );
  };

  const collaborateurs = [...(loadingCollaborateurs.data?.collaborateurs || [])].sort((a, b) =>
    a?.role?.code && b?.role?.code ? (a?.role?.code < b?.role?.code ? -1 : a?.role?.code > b?.role?.code ? 1 : 0) : 0
  );
  const datetime = moment().format('DD-MM-YYYY-HHmm');
  const exportFilename = `Matrice-Fonctions-${(exportPharmacie?.nom || '').replaceAll(' ', '-')}-${datetime}.${
    exportFormat || ''
  }`;

  const headerCellOptions: any = {
    background: '#FFF',
    color: '#000',
    textAlign: 'center',
    wrap: true,
    borderRight: {
      color: '#000',
      size: 1,
    },
    borderBottom: {
      color: '#000',
      size: 1,
    },
  };

  const cellOptions: any = {
    borderRight: {
      color: '#000',
      size: 1,
    },
    borderBottom: {
      color: '#000',
      size: 1,
    },
  };
  return {
    launch: handleRequestExportMatricePharmacie,
    component: (
      <>
        <Backdrop open={loadingCollaborateurs.loading || loadingMatricePharmacie.loading} />
        <GridPDFExport
          ref={(pdfExport) => (exportPdfEl = pdfExport)}
          paperSize="A4"
          margin="1cm"
          fileName={exportFilename}
          pageTemplate={PDFExportPageTemplate}
        >
          <Grid
            style={{ display: 'none' }}
            data={formatFonctions(
              loadingMatricePharmacie.data?.dQMTFonctions.nodes || ([] as any),
              niveauMatriceFonctions
            )}
          >
            <GridColumn
              field="fonction"
              title={niveauMatriceFonctions === 1 ? 'Matrice des Fonctions' : 'Fonctions'}
              cell={cellFonction}
              width="100px"
              className="verticalTableHeader"
              headerCell={niveauMatriceFonctions === 1 ? tacheHeader : fonctionHeader}
            />
            {niveauMatriceFonctions !== 1 && (
              <GridColumn field="tache" title="Matrice des Fonctions" cell={cellTache} headerCell={tacheHeader} />
            )}
            {collaborateurs.map((responsable: any, index) => (
              <GridColumn
                field={responsable.id as any}
                title={responsable.fullName || ''}
                key={responsable.id || `responsable-${index}`}
                width="50px"
                headerClassName="collaborateurHeader"
                cell={cellCollaborateur}
              />
            ))}
          </Grid>
        </GridPDFExport>
        <ExcelExport ref={(excelExport) => (exportExcelEl = excelExport)} fileName={exportFilename}>
          <ExcelExportColumn
            field={niveauMatriceFonctions === 1 ? 'fonction' : 'fonctionFirst'}
            title={
              niveauMatriceFonctions === 1
                ? `Matrice des Fonctions \r\n${exportPharmacie?.nom || ''} \r\nLe ${currentDateTime()}`
                : ''
            }
            headerCellOptions={{ ...headerCellOptions, color: niveauMatriceFonctions === 1 ? undefined : '#FFF' }}
            cellOptions={{ ...cellOptions, bold: true }}
          />
          {niveauMatriceFonctions !== 1 && (
            <ExcelExportColumn
              field="tache"
              title={`Matrice des Fonctions \r\n${exportPharmacie?.nom || ''} \r\nLe ${currentDateTime()}`}
              headerCellOptions={headerCellOptions}
              cellOptions={cellOptions}
            />
          )}
          {collaborateurs.map((responsable: any, index) => (
            <ExcelExportColumn
              field={responsable.id as any}
              title={`${responsable.fullName || ''}\r\n${responsable?.role?.nom}`}
              key={responsable.id || `responsable-${index}`}
              headerCellOptions={headerCellOptions}
              cellOptions={{
                ...cellOptions,
                textAlign: 'center',
              }}
            />
          ))}
        </ExcelExport>
      </>
    ),
  };
};

export default ExportMatriceTache;
