import React, { FC, useState } from 'react';
import { Box } from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import useStyles from './style';
import { ConfirmDeleteDialog, LoaderSmall, ErrorPage, CardThemeList, CardTheme } from '@lib/ui-kit';
import { Delete, Edit } from '@material-ui/icons';
import MatriceTacheForm from '../../MatriceTacheForm/MatriceTacheForm';
import { TITULAIRE_PHARMACIE, useApplicationContext } from '@lib/common';
import {
  Get_FonctionDocument,
  Get_FonctionQuery,
  Get_FonctionQueryVariables,
  useDelete_TacheMutation,
  useGet_FonctionQuery,
  usePersonnaliser_TacheMutation,
  useToggle_One_Tache_ActivationMutation,
  useUpdate_TacheMutation,
} from '@lib/common/src/federation';

interface TacheComponentProps {
  fonctionId: string;
}

const TacheComponent: FC<TacheComponentProps & RouteComponentProps> = ({ fonctionId, history }) => {
  const classes = useStyles({});

  const { user, currentPharmacie: pharmacie, notify, federation } = useApplicationContext();

  const isTitulairePharmacie = user?.role?.code === TITULAIRE_PHARMACIE;

  const [show, setShow] = useState<boolean>(false);
  const [showDeleteButton, setShowDeleteButton] = useState<boolean>(false);
  const [toDeleteTache, setToDeleteTache] = useState<any>();
  const [toEditTache, setToEditTache] = useState<any>();
  const [ordre, setOrdre] = useState<number | undefined>();
  const [libelle, setLibelle] = useState<string>('');

  const reInit = () => {
    setOrdre(undefined);
    setLibelle('');
  };

  const loadingFonction = useGet_FonctionQuery({
    client: federation,
    fetchPolicy: 'network-only',
    variables: {
      idPharmacie: pharmacie.id,
      id: fonctionId,
    },
  });

  const [deleteTache, deletionTache] = useDelete_TacheMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de la suppression de la fonction.`,
      });
    },

    onCompleted: () => {
      setShowDeleteButton(false);
      notify({
        type: 'error',
        message: 'La fonction a été supprimée avec succès !',
      });
    },
    update: (cache, deletionResult) => {
      if (deletionResult.data?.deleteOneDQMTTache?.id) {
        const previousFonction = cache.readQuery<Get_FonctionQuery, Get_FonctionQueryVariables>({
          query: Get_FonctionDocument,
          variables: {
            idPharmacie: pharmacie.id,
            id: fonctionId,
          },
        })?.dQMTFonction;

        const deleteId = deletionResult.data.deleteOneDQMTTache.id;

        const deletedTache = {
          ...previousFonction,
          nombreTaches: ((previousFonction as any)?.nombreTaches || 0) - 1,
          taches: (loadingFonction.data?.dQMTFonction?.taches || []).filter((item) => item.id !== deleteId),
        };

        cache.writeQuery<Get_FonctionQuery, Get_FonctionQueryVariables>({
          query: Get_FonctionDocument,
          data: {
            dQMTFonction: deletedTache,
          } as any,
        });
      }
    },

    client: federation,
  });

  const [updateTache, modificationTache] = useUpdate_TacheMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de la modification de la fonction.`,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      notify({
        type: 'success',
        message: 'La fonction a été modifiée avec succès !',
      });
    },
    client: federation,
  });

  const [personaliserTache, personnalisationTache] = usePersonnaliser_TacheMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de la modification de la fonction.`,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      notify({
        type: 'success',
        message: 'La fonction a été modifiée avec succès !',
      });
    },
    client: federation,
  });

  const [toggleTacheActivation, loadingTacheActivation] = useToggle_One_Tache_ActivationMutation({
    onError: () => {
      notify({
        type: 'error',
        message: `Une s'est produite lors de la mise à jour de la fonction`,
      });
    },
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'La fonction a été modifiée avec succès !',
      });
    },
    client: federation,
  });

  const handleDelete = (element: any): void => {
    setToDeleteTache(element);
    setShowDeleteButton(!showDeleteButton);
  };

  const handleUpdate = (tache: any) => {
    setToEditTache(tache);
    setOrdre(tache.ordre);
    setLibelle(tache.libelle);
    setShow(!show);
  };

  const doUpdate = (): void => {
    if (ordre) {
      if (isTitulairePharmacie) {
        personaliserTache({
          variables: {
            idPharmacie: pharmacie.id,
            idTache: toEditTache.id,
            input: {
              idFonction: fonctionId,
              ordre,
              libelle,
            },
          },
        });
      } else {
        updateTache({
          variables: {
            input: {
              id: toEditTache.id,
              update: {
                idFonction: fonctionId,
                ordre,
                libelle,
              },
            },
          },
        });
      }
    }
  };

  const doDelete = (): void => {
    if (toDeleteTache?.id) {
      deleteTache({
        variables: {
          input: {
            id: toDeleteTache.id,
          },
        },
      });
    }
  };

  const handleToggleTacheActivation = ({ id }: any): void => {
    toggleTacheActivation({
      variables: {
        idTache: id,
        idPharmacie: pharmacie.id,
      },
    });
  };

  return (
    <Box className={classes.content}>
      {loadingFonction.loading ? (
        <LoaderSmall />
      ) : loadingFonction.error ? (
        <ErrorPage />
      ) : (
        <CardThemeList>
          {[...(loadingFonction.data?.dQMTFonction?.taches || [])]
            .sort((a, b) => a.ordre - b.ordre)
            .map((currentTache: any) => (
              <CardTheme
                applyMinHeight={false}
                key={currentTache.id}
                title={`${currentTache.ordre}-${currentTache.libelle}${
                  currentTache.active && loadingFonction.data?.dQMTFonction?.active ? '' : '(désactivée)'
                }`}
                littleComment=" "
                moreOptions={
                  isTitulairePharmacie
                    ? [
                        {
                          menuItemLabel: {
                            title: currentTache.active ? 'Désactiver' : 'Activer',
                            icon: <Edit />,
                          },
                          onClick: () => handleToggleTacheActivation(currentTache),
                        },

                        {
                          menuItemLabel: {
                            title: 'Modifier fonction',
                            icon: <Edit />,
                          },
                          onClick: handleUpdate.bind(null, currentTache),
                        },
                      ]
                    : [
                        {
                          menuItemLabel: {
                            title: 'Modifier fonction',
                            icon: <Edit />,
                          },
                          onClick: handleUpdate.bind(null, currentTache),
                        },
                        {
                          menuItemLabel: {
                            title: 'Supprimer fonction',
                            icon: <Delete />,
                          },
                          onClick: handleDelete.bind(null, currentTache),
                        },
                      ]
                }
              />
            ))}
        </CardThemeList>
      )}
      <MatriceTacheForm
        title={'Modification de fonction'}
        saving={isTitulairePharmacie ? personnalisationTache.loading : modificationTache.loading}
        show={show}
        setShow={setShow}
        ordre={ordre}
        libelle={libelle}
        setLibelle={setLibelle}
        setOrdre={setOrdre}
        onSubmit={doUpdate}
      />
      <ConfirmDeleteDialog
        open={showDeleteButton}
        setOpen={setShowDeleteButton}
        onClickConfirm={doDelete}
        isLoading={deletionTache.loading}
      />
    </Box>
  );
};

export default withRouter(TacheComponent);
