import {
  DqmtFonction,
  DqmtTacheResponsableCollaborateur,
  useGet_Fonctions_With_Responsable_InfosLazyQuery,
} from '@lib/common/src/federation';
import { uniqWith } from 'lodash';

interface KeyValue {
  [key: string]: any;
}

export const extractResponsablesFromFonctions = (fonctions: DqmtFonction[]): DqmtTacheResponsableCollaborateur[] => {
  return uniqWith(
    fonctions.reduce((responsables, currentFonction) => {
      return [
        ...responsables,
        ...currentFonction.taches.reduce((responsablesTache, currentTache) => {
          return [
            ...responsablesTache,
            ...currentTache.collaborateurResponsables.reduce((collaborateurs, currentResponsable) => {
              return [...collaborateurs, ...currentResponsable.responsables] as any;
            }, []),
          ] as any;
        }, []),
      ];
    }, []),
    (a: any, b: any) => a.id === b.id
  );
};

export const formatFonctions = (fonctions: DqmtFonction[]): KeyValue[] => {
  // const responsables = extractResponsablesFromFonctions(fonctions);

  return fonctions
    .filter((fonction: any) => fonction.active)
    .sort((a, b) => a.ordre - b.ordre)
    .reduce((result, currentFonction) => {
      return [
        ...result,
        ...currentFonction.taches
          .filter((tache: any) => tache.active)
          .sort((a, b) => a.ordre - b.ordre)
          .reduce((resultTaches, currentTache, index) => {
            return [
              ...resultTaches,

              {
                fonction: currentFonction.libelle,
                ordreFonction: currentFonction.ordre,
                index,
                nombreTaches: currentFonction.taches.length,
                tache: currentTache.libelle,
                ordreTache: currentTache.ordre,
                ...currentTache.collaborateurResponsables.reduce((responsableCodes, currentResponsables) => {
                  return {
                    ...responsableCodes,
                    ...currentResponsables.responsables.reduce((listCollaborateurs, currentCollaborateur) => {
                      return {
                        ...listCollaborateurs,
                        [currentCollaborateur.id]: currentResponsables.type.code,
                      };
                    }, {}),
                  };
                }, {}),
              },
            ] as any;
          }, []),
      ];
    }, []);
};
