import { DqmtFonction, DqmtTacheResponsableCollaborateur, useGet_Fonctions_With_Responsable_InfosQuery, User } from '@lib/common/src/federation';
import { uniqWith } from 'lodash';
interface KeyValue {
  [key: string]: any;
}

useGet_Fonctions_With_Responsable_InfosQuery;

export const extractResponsablesFromFonctions = (fonctions: DqmtFonction[]): User[] => {
  return uniqWith(
    fonctions.reduce((responsables, currentFonction) => {
      return [
        ...responsables,
        ...currentFonction.taches.reduce((responsablesTache, currentTache) => {
          return [
            ...responsablesTache,
            ...currentTache.collaborateurResponsables.reduce((collaborateurs, currentResponsable) => {
              return [...collaborateurs, ...currentResponsable.responsables] as any;
            }, []),
          ] as any;
        }, []),
      ];
    }, []),
    (a: any, b: any) => a.id === b.id
  );
};

const formatCollaborateursResponsables = (collaborateurResponsables: DqmtTacheResponsableCollaborateur[]) => {
  return collaborateurResponsables.reduce((responsableCodes, currentResponsables) => {
    return {
      ...responsableCodes,
      ...currentResponsables.responsables.reduce((listCollaborateurs, currentCollaborateur) => {
        return {
          ...listCollaborateurs,
          [currentCollaborateur.id]: currentResponsables.type.code,
        };
      }, {}),
    };
  }, {});
};

export const formatFonctions = (fonctions: DqmtFonction[], niveauMatriceFonctions: number): KeyValue[] => {
  // const responsables = extractResponsablesFromFonctions(fonctions);

  return fonctions
    .filter((fonction: any) => fonction.active)
    .sort((a, b) => a.ordre - b.ordre)
    .reduce((result, currentFonction, fonctionIndex) => {
      return niveauMatriceFonctions === 1
        ? ([
            ...result,
            {
              fonction: currentFonction.libelle,
              ordreFonction: currentFonction.ordre,
              ...formatCollaborateursResponsables(currentFonction.collaborateurResponsables),
            } as any,
          ] as any)
        : [
            ...result,
            ...currentFonction.taches
              .filter((tache: any) => tache.active)
              .sort((a, b) => a.ordre - b.ordre)
              .reduce((resultTaches, currentTache, index) => {
                return [
                  ...resultTaches,

                  {
                    fonction: currentFonction.libelle,
                    fonctionFirst: index === 0 ? currentFonction.libelle : '',
                    ordreFonction: currentFonction.ordre,
                    index,
                    nombreTaches: currentFonction.taches.length,
                    tache: currentTache.libelle,
                    ordreTache: currentTache.ordre,
                    ...formatCollaborateursResponsables(currentTache.collaborateurResponsables),
                  },
                ] as any;
              }, []),
          ];
    }, []);
};
