import React, { FC, useState, ChangeEvent, MouseEvent } from 'react';
import { CustomModal, CustomFormTextField } from '@lib/ui-kit';
import useStyles from './style';
import { Box } from '@material-ui/core';

interface ThemeAddProps {
  title: string;
  show: boolean;
  saving: boolean;
  value1?: number;
  value2: string;
  setValue1: (event: any) => void;
  setValue2: (event: any) => void;
  setShow: (open: boolean) => void;
  onSubmit: (event: MouseEvent) => void;
}

const ThemeAdd: FC<ThemeAddProps> = ({
  title,
  show,
  value1,
  value2,
  saving,
  setValue1,
  setValue2,
  setShow,
  onSubmit,
}) => {
  const classes = useStyles({});

  return (
    <CustomModal
      title={title}
      open={show}
      setOpen={setShow}
      headerWithBgColor={true}
      fullWidth={true}
      maxWidth="sm"
      closeIcon={true}
      withBtnsActions={true}
      disabledButton={!value2 || !value2 || saving}
      onClickConfirm={onSubmit}
      centerBtns={true}
    >
      <Box className={classes.formsContent}>
        <Box>
          <CustomFormTextField
            type="number"
            placeholder="Numéro d'ordre"
            value={value1}
            onChange={setValue1}
            label="Numéro d'ordre"
          />
        </Box>
        <Box>
          <CustomFormTextField
            placeholder="Nom du thème"
            value={value2}
            onChange={setValue2}
            label="Nom"
          />
        </Box>
      </Box>
    </CustomModal>
  );
};

export default ThemeAdd;
