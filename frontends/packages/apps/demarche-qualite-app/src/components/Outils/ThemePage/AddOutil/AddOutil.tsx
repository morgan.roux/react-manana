import { CustomModal, OutilForms } from '@lib/ui-kit';
import React, { FC, ChangeEvent } from 'react';

interface AddOutilProps {
  title: string;
  open: boolean;
  selectedFile: File[];
  numeroOrdre: string;
  intitule: string;
  loading: boolean;
  setSelectedFile: (file: File[]) => void;
  setOpen: (open: boolean) => void;
  setNumeroOrdre: (event: ChangeEvent<HTMLInputElement>) => void;
  setIntitule: (event: ChangeEvent<HTMLInputElement>) => void;
  onSubmit: () => void;
}

const AddOutil: FC<AddOutilProps> = ({
  title,
  open,
  selectedFile,
  numeroOrdre,
  intitule,
  loading,
  setOpen,
  setSelectedFile,
  setIntitule,
  setNumeroOrdre,
  onSubmit,
}) => {
  return (
    <CustomModal
      title={title}
      open={open}
      setOpen={setOpen}
      withBtnsActions={true}
      closeIcon={true}
      centerBtns={true}
      headerWithBgColor={true}
      fullWidth={true}
      maxWidth="md"
      onClickConfirm={onSubmit}
      disabledButton={loading}
    >
      <OutilForms
        key={Math.random()}
        style={{ width: '100%' }}
        numeroOrdre={numeroOrdre}
        intitule={intitule}
        setNumeroOrdre={setNumeroOrdre}
        setIntitule={setIntitule}
        selectedFile={selectedFile}
        setSelectedFile={setSelectedFile}
      />
    </CustomModal>
  );
};

export default AddOutil;
