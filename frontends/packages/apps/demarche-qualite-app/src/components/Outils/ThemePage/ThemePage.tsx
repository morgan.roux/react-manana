import React, { FC } from 'react';
/*import ItemContainer from '../../../../../Common/ItemContainer';
import MoreOptions from '../../../../../Common/MainContainer/TwoColumnsContainer/LeftListContainer/MoreOptions/More';
import { LayoutType } from '../../../../../Common/MainContainer/MainContainer';
import { Details } from '@lib/common';
*/
export interface IContent {
  title: string;
  subtitle: string;
}

export interface INoContentValues {
  list: IContent;
  content: IContent;
}

export interface ThemePageProps {
  typologie: string;
  noContentValues: INoContentValues;
  outilsData: any[];
  addButton: any;
  listItemFields: any;
}

const ThemePage: FC<ThemePageProps> = ({ typologie, noContentValues, outilsData, addButton, listItemFields }) => {
  /* return (
    <ItemContainer
      typologie={typologie}
      moreOptionsItem={<MoreOptions typologie={typologie} listResult={outilsData} />}
      noContentValues={noContentValues}
      layout={LayoutType.TwoColumns}
      listResult={outilsData}
      listItemFields={listItemFields}
      rightContentChildren={<Details listResult={outilsData} />}
      headerButton={addButton}
    />
  );*/

  return <div>Theme</div>;
};

export default ThemePage;
