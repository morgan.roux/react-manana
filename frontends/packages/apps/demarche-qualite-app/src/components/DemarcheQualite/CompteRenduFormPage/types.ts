import { FicheAmelioration } from '../FicheAmeliorationPage/types';

import { FicheIncident } from '../FicheIncidentPage/types';
import { ActionOperationnelle } from '../ActionOperationnellePage/types';
import { User } from '@lib/types';

export interface DQReunionActionInput {
  id?: string;
  type: 'INCIDENT' | 'AMELIORATION' | 'OPERATION';
  idTypeAssocie: string;
  idTodoAction: string;
}

export interface RequestSavingCompteRendu {
  id?: string;
  idUserAnimateur: string;
  idResponsable: string;
  description: string;
  idUserParticipants?: string[];
  actions: DQReunionActionInput[];
  dateReunion: Date;
}

export interface Reunion {
  id?: string;
  description: string;
  idUserAnimateur: string;
  idResponsable: string;
  createdAt: string;
  updatedAt: string;
  animateur: User;
  responsable: User;
  participants: User[];
  nombreTotalActions?: number;
  nombreTotalClotureActions?: number;
  actions?: ReunionAction[];
  dateReunion?: Date;
}

export interface ReunionAction {
  id?: string;
  type: 'INCIDENT' | 'AMELIORATION' | 'OPERATION';
  idTypeAssocie: string;
  typeAssocie: FicheAmelioration | FicheIncident | ActionOperationnelle;
  idTodoAction?: string;
  idReunion?: string;
  createdAt?: string;
  updatedAt?: string;
}
