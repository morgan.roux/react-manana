import React, { FC } from 'react';
import { PersonAdd } from '@material-ui/icons';
import { Table, Column, CustomAvatarGroup, OptionSelect } from '@lib/ui-kit/src/components/atoms';
import { ReunionAction } from '../types';
import useStyles from './styles';
import moment from 'moment';
import { isMobile, strippedColumnStyle, strippedString } from '@lib/ui-kit/src/services/Helpers';
import { IconButton } from '@material-ui/core';
import { Origine } from '../../FicheAmeliorationPage/types';
import { importancesToImpacts } from '@lib/common';

export interface ActionsProps {
  actionsData: ReunionAction[];
  selectedReunionActions: ReunionAction[];
  onShowSelectedReunionsActions: () => void;
  showSelectedReunionsActions: boolean;
  onChangeReunionActionsSelection: (action: ReunionAction[]) => void;
  onParticipantsClicked: (action: ReunionAction, type: 'PARTICIPANT_A_INFORMER' | 'PARTICIPANT_EN_CHARGE') => void;
  onChangeUrgence: (action: ReunionAction, urgence: any) => void;
  onChangeImportance: (action: ReunionAction, importance: any) => void;
  importances: any[];
  urgences: any[];

  origines: {
    loading?: boolean;
    error?: Error;
    data?: Origine[];
  };
}

const Actions: FC<ActionsProps> = ({
  actionsData,
  selectedReunionActions,
  showSelectedReunionsActions,
  onChangeReunionActionsSelection,
  onShowSelectedReunionsActions,
  onParticipantsClicked,
  onChangeUrgence,
  onChangeImportance,
  importances,
  urgences,
  origines,
}) => {
  const classes = useStyles();

  const findOrigine = (id?: string): any | undefined => {
    if (!id) {
      return undefined;
    }

    return (origines?.data || []).find((o) => o.id === id);
  };

  const columns: Column[] = isMobile()
    ? [
        {
          name: '',
          label: 'Type',

          renderer: (row: ReunionAction) => {
            if (row.type === 'AMELIORATION') return 'Amélioration';
            else if (row.type === 'INCIDENT') return 'Incident';
            else return 'Action opérationnelle';
          },
        },

        {
          name: 'description',
          label: 'Description',
          renderer: (row: ReunionAction) => {
            return row?.typeAssocie?.description ? strippedString(row.typeAssocie.description) : '-';
          },

          style: strippedColumnStyle,
        },
      ]
    : [
        {
          name: '',
          label: 'Type',

          renderer: (row: ReunionAction) => {
            if (row.type === 'AMELIORATION') return 'Amélioration';
            else if (row.type === 'INCIDENT') return 'Incident';
            else return 'Action opérationnelle';
          },
        },

        {
          name: 'description',
          label: 'Description',
          renderer: (row: ReunionAction) => {
            return row?.typeAssocie?.description ? strippedString(row.typeAssocie.description) : '-';
          },
          style: strippedColumnStyle,
        },
        {
          name: 'origine',
          label: 'Origine',
          renderer: (row: ReunionAction) => {
            return findOrigine(row?.typeAssocie?.origine?.id)?.libelle || '-';
          },
        },

        {
          name: 'correspondant',
          label: 'Correspondant',
          renderer: (row: ReunionAction) => {
            if (!row?.typeAssocie?.origineAssocie || !row?.typeAssocie?.origine?.id) {
              return '-';
            }
            const origine = findOrigine(row.typeAssocie.origine.id);
            return origine
              ? origine.code === 'INTERNE'
                ? row.typeAssocie.origineAssocie.fullName
                : row.typeAssocie.origineAssocie.nom
              : '-';
          },
        },
        {
          name: 'dateCreation',
          label: 'Date de création',
          renderer: (row: ReunionAction) => {
            return row?.createdAt ? moment(row.createdAt).format('DD/MM/YYYY') : '-';
          },
        },
        {
          name: 'participants',
          label: 'Collaborateurs concernés',
          renderer: (row: ReunionAction) =>
            (row.typeAssocie.participants?.length || 0) > 0 ? (
              <CustomAvatarGroup
                onClick={() => onParticipantsClicked(row, 'PARTICIPANT_EN_CHARGE')}
                max={3}
                sizes="30px"
                users={(row.typeAssocie.participants || []) as any}
              />
            ) : (
              <IconButton onClick={() => onParticipantsClicked(row, 'PARTICIPANT_EN_CHARGE')}>
                <PersonAdd />
              </IconButton>
            ),
        },
        {
          name: 'participantsAInformer',
          label: 'Collaborateurs à informer',
          renderer: (row: ReunionAction) =>
            (row.typeAssocie.participantsAInformer?.length || 0) > 0 ? (
              <CustomAvatarGroup
                onClick={() => onParticipantsClicked(row, 'PARTICIPANT_A_INFORMER')}
                max={3}
                sizes="30px"
                users={(row.typeAssocie.participantsAInformer || []) as any}
              />
            ) : (
              <IconButton onClick={() => onParticipantsClicked(row, 'PARTICIPANT_A_INFORMER')}>
                <PersonAdd />
              </IconButton>
            ),
        },
        /*{
          name: 'urgence',
          label: 'Urgence',
          renderer: (row: ReunionAction) => (
            <UrgenceInput
              noBorder={true}
              noLabel={true}
              noMinHeight={true}
              onChange={(newUrgence) => onChangeUrgence(row, newUrgence)}
              value={row.typeAssocie.urgence?.id}
              options={urgences}
            />
          ),
        },*/

        {
          name: 'importance',
          label: 'Impact',
          renderer: (row: ReunionAction) => (
            <OptionSelect
              options={importancesToImpacts(importances)}
              value={row.typeAssocie.importance as any}
              onChange={(id) => onChangeImportance(row, { id })}
              useDefaultStyle={false}
            />

            /* <ImportanceInput
              noBorder={true}
              noLabel={true}
              noMinHeight={true}
              onChange={(newImportance) => onChangeImportance(row, newImportance)}
              value={row.typeAssocie.importance?.id}
              options={importances}
           />*/
          ),
        },
      ];

  const handleSelectAll = () => {
    if (actionsData.length === selectedReunionActions.length) {
      onChangeReunionActionsSelection([]);
    } else {
      onChangeReunionActionsSelection(actionsData);
    }
  };

  return (
    <div className={classes.root}>
      <Table
        dataIdName="idTypeAssocie"
        showSelectedRows={showSelectedReunionsActions}
        onShowSelected={onShowSelectedReunionsActions}
        columns={columns}
        data={actionsData.filter((action) => !!action?.typeAssocie)}
        rowsSelected={selectedReunionActions}
        onRowsSelectionChange={(selectedRows) => onChangeReunionActionsSelection(selectedRows as any)}
        onClearSelection={() => onChangeReunionActionsSelection([])}
        onSelectAll={handleSelectAll}
        style={{ alignItems: 'start' }}
        selectable
      />
    </div>
  );
};

export default Actions;
