import React, { FC } from 'react';
import { Backdrop } from '@lib/ui-kit/src/components/atoms';
import { CollaboratorList, CollaboratorListProps } from '@lib/ui-kit/src/components/atoms/CollaboratorList';
import { SidedFieldLabel } from '@lib/ui-kit/src/components/templates';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import useStyles from './styles';

export interface PresenceProps {
  loading?: boolean;
  error?: Error;
  title: string;
}

const Presence: FC<CollaboratorListProps & PresenceProps> = ({
  title = 'Fiche de présence',
  data,
  rowsSelected,
  onSelectionChange,
  selectable,
  filterable,
  loading,
  error,
  multipleSelection,
}) => {
  const classes = useStyles();

  if (error) {
    return <ErrorPage />;
  }

  if (loading) {
    return <Backdrop />;
  }

  return (
    <div className={classes.root}>
      <SidedFieldLabel label={title}>
        <CollaboratorList
          data={data}
          rowsSelected={rowsSelected}
          onSelectionChange={onSelectionChange}
          selectable={selectable}
          filterable={filterable}
          multipleSelection={multipleSelection}
        />
      </SidedFieldLabel>
    </div>
  );
};

export default Presence;
