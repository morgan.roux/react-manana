import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      width: '100%',
      maxWidth: 800,
    },
    formContainer: {
      width: '100%',
      marginTop: 30,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      '& > div': {
        width: '100%',
      },
    },
  })
);

export default useStyles;
