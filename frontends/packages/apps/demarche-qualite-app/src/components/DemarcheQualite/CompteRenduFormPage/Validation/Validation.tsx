import { Box } from '@material-ui/core';
import React, { FC, ReactNode } from 'react';
import { CustomDatePicker, CustomEditorText } from '@lib/ui-kit';
import { SidedFieldLabel } from '@lib/ui-kit/src/components/templates';
import useStyles from './styles';

interface ValidationProps {
  date: Date;
  description: string;
  animateurComponent: ReactNode;
  responsableComponent: ReactNode;
  onDescriptionChange: (description: string) => void;
  onDateRendezVousChange: (date: Date) => void;
}

const Validation: FC<ValidationProps> = ({
  date,
  description,
  animateurComponent,
  responsableComponent,
  onDescriptionChange,
  onDateRendezVousChange,
}) => {
  const classes = useStyles();

  const handleDescr = (value: string): void => {
    onDescriptionChange(value);
  };

  const handleDateFicheChange = (date: any) => {
    onDateRendezVousChange(date);
  };

  return (
    <div className={classes.root}>
      <SidedFieldLabel label="Informations générales">
        <div className={classes.formContainer}>
          {animateurComponent}
          {responsableComponent}
          <CustomEditorText
            value={description}
            placeholder="Description (*)"
            onChange={(value) => handleDescr(value)}
          />
          <Box py={1}>
            <CustomDatePicker value={date} label={`Date du reunion`} onChange={handleDateFicheChange} required={true} />
          </Box>
        </div>
      </SidedFieldLabel>
    </div>
  );
};

export default Validation;
