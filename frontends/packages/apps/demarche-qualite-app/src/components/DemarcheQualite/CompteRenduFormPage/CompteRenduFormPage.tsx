import { unionWith } from 'lodash';
import React, { FC, ReactNode, useEffect, useState } from 'react';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import { Backdrop } from '@lib/ui-kit/src/components/atoms';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import { Origine } from '../FicheAmeliorationPage/types';
import { Stepper } from '@lib/ui-kit/src/components/atoms/Stepper';
import { ActionOperationnelle } from '../ActionOperationnellePage/types';
import { FicheAmelioration } from '../FicheAmeliorationPage/types';
import { FicheIncident } from '../FicheIncidentPage/types';
import { Actions } from './Actions';
import { Presence } from './Presence';
import { RequestSavingCompteRendu, Reunion, ReunionAction } from './types';
import { Validation } from './Validation';
import { User } from '@lib/ui-kit/src/components/pages/DashboardPage';

export interface CompteRenduFormPageProps {
  mode: 'creation' | 'modification';
  loading?: boolean;
  loadingText?: string;
  saving?: boolean;
  error?: Error;
  reunion?: Reunion;

  origines: {
    loading?: boolean;
    error?: Error;
    data?: Origine[];
  };

  presence: {
    data: User[];
    loading?: boolean;
    error?: Error;
  };

  ficheAmeliorations: {
    data?: FicheAmelioration[];
    loading?: boolean;
    error?: Error;
  };

  ficheIncidents: {
    data?: FicheIncident[];
    loading?: boolean;
    error?: Error;
  };

  actionOperationnelles: {
    data?: ActionOperationnelle[];
    loading?: boolean;
    error?: Error;
  };

  animateurComponent: ReactNode;
  responsableComponent: ReactNode;

  selectedUserAnimateur?: User;
  selectedUserResponsable?: User;

  importances: any[];
  urgences: any[];

  onChangeUrgence: (action: ReunionAction, urgence: any) => void;
  onChangeImportance: (action: ReunionAction, importance: any) => void;

  onParticipantsClicked: (action: ReunionAction, type: 'PARTICIPANT_A_INFORMER' | 'PARTICIPANT_EN_CHARGE') => void;
  onRequestSave: (toSavingComteRendu: RequestSavingCompteRendu) => void;
  onRequestGoBack: () => void;
}

const CompteRenduFormPage: FC<CompteRenduFormPageProps> = ({
  mode,
  origines,
  ficheAmeliorations,
  ficheIncidents,
  actionOperationnelles,
  onRequestSave,
  onRequestGoBack,
  saving,
  error,
  loading,
  loadingText,
  presence,
  reunion,
  animateurComponent,
  responsableComponent,
  selectedUserAnimateur,
  selectedUserResponsable,
  importances,
  urgences,
  onChangeUrgence,
  onChangeImportance,
  onParticipantsClicked,
}) => {
  const [activeStep, setActiveStep] = React.useState<number | undefined>(0);
  const [description, setDescription] = useState<string>('');
  const [userParticipants, setUserParticipants] = useState<User[]>([]);
  const [selectedReunionActions, setSelectedReunionActions] = useState<ReunionAction[]>([]);
  const [reunionActions, setReunionActions] = useState<ReunionAction[]>([]);
  const [showSelectedReunionActions, setShowSelectedReunionActions] = useState<boolean>(false);
  const [dateReunion, setDateRendezVous] = useState<Date>(new Date());

  useEffect(() => {
    const tableau: ReunionAction[] = [...((reunion?.actions || []) as any)];

    (ficheAmeliorations.data || []).forEach((currentData: FicheAmelioration) => {
      if (!tableau.some((item) => item.idTypeAssocie === currentData.id && item.type === 'AMELIORATION')) {
        tableau.push({
          type: 'AMELIORATION',
          idTypeAssocie: currentData.id,
          createdAt: currentData.createdAt,
          updatedAt: currentData.updatedAt,
          typeAssocie: currentData,
        });
      }
    });

    (ficheIncidents.data || []).forEach((currentData: FicheIncident) => {
      if (!tableau.some((item) => item.idTypeAssocie === currentData.id && item.type === 'INCIDENT')) {
        tableau.push({
          type: 'INCIDENT',
          idTypeAssocie: currentData.id,
          createdAt: currentData.createdAt,
          updatedAt: currentData.updatedAt,
          typeAssocie: currentData,
        });
      }
    });

    (actionOperationnelles.data || []).forEach((currentData: ActionOperationnelle) => {
      if (!tableau.some((item) => item.idTypeAssocie === currentData.id && item.type === 'OPERATION')) {
        tableau.push({
          type: 'OPERATION',
          idTypeAssocie: currentData.id as any,
          createdAt: currentData.createdAt,
          typeAssocie: currentData,
        });
      }
    });

    setReunionActions(unionWith(tableau, (a, b) => a.type === b.type && a.idTypeAssocie === b.idTypeAssocie));

    if (reunion && mode === 'modification') {
      setDescription(reunion.description);
      setUserParticipants(reunion.participants as any);
      setSelectedReunionActions(reunion.actions || ([] as any));
      setDateRendezVous(reunion.dateReunion || new Date());
    }
  }, [ficheAmeliorations.data, ficheIncidents.data, actionOperationnelles.data, reunion]);

  // TODO
  // const isNextButtonActive = false;
  const isNextButtonDisabled = (): boolean => {
    console.log(
      '*****************************!selectedUserAnimateur || !selectedUserResponsable || !description',
      activeStep,
      selectedUserAnimateur,
      selectedUserResponsable,
      description
    );

    if (activeStep === 0) return userParticipants.length === 0;
    else if (activeStep === 1) return selectedReunionActions.length === 0;
    else return !selectedUserAnimateur || !selectedUserResponsable || !description;
  };

  const handleSaveReunion = (): void => {
    if (selectedUserAnimateur && selectedUserResponsable) {
      onRequestSave({
        id: mode === 'modification' ? reunion?.id : undefined,
        description,
        idUserAnimateur: selectedUserAnimateur?.id,
        idResponsable: selectedUserResponsable?.id || '',
        idUserParticipants: userParticipants.map(({ id }) => id),
        actions: selectedReunionActions.map((action) => ({
          type: action.type,
          idTypeAssocie: action.idTypeAssocie,
          idTodoAction: action.idTodoAction,
        })) as any,
        dateReunion,
      });
    }
  };

  if (loading || ficheAmeliorations.loading || ficheIncidents.loading || actionOperationnelles.loading) {
    return <Backdrop value={loadingText} />;
  }

  if (error || ficheAmeliorations.error || ficheIncidents.error || actionOperationnelles.error) {
    return <ErrorPage />;
  }

  return (
    <CustomContainer
      filled
      bannerContentStyle={{
        justifyContent: 'center',
      }}
      bannerBack
      onBackClick={onRequestGoBack}
      bannerTitle={mode === 'creation' ? 'Nouvelle réunion' : 'Modification de réunion'}
      contentStyle={{
        marginTop: 24,
        width: '100%',
        maxWidth: 1200,
      }}
      onlyBackAndTitle={true}
    >
      <Stepper
        contentStyle={{ width: '100%' }}
        onStepChange={setActiveStep}
        onCancel={onRequestGoBack}
        onValidate={handleSaveReunion}
        loading={saving}
        disableNextBtn={isNextButtonDisabled()}
        disableValidateBtn={isNextButtonDisabled()}
        steps={[
          {
            title: 'Présence',
            content: (
              <Presence
                title="Fiche de présence"
                multipleSelection
                rowsSelected={userParticipants}
                onSelectionChange={setUserParticipants}
                data={presence?.data || []}
                selectable
                filterable
                loading={presence?.loading}
                error={presence?.error}
              />
            ),
          },
          {
            title: "Plan d'Actions",
            content: (
              <Actions
                importances={importances}
                origines={origines}
                urgences={urgences}
                onChangeImportance={onChangeImportance}
                onChangeUrgence={onChangeUrgence}
                onParticipantsClicked={onParticipantsClicked}
                actionsData={reunionActions}
                selectedReunionActions={selectedReunionActions}
                onChangeReunionActionsSelection={setSelectedReunionActions}
                showSelectedReunionsActions={showSelectedReunionActions}
                onShowSelectedReunionsActions={() => setShowSelectedReunionActions(!showSelectedReunionActions)}
              />
            ),
          },
          {
            title: 'Validation',
            content: (
              <Validation
                description={description}
                responsableComponent={responsableComponent}
                animateurComponent={animateurComponent}
                onDescriptionChange={setDescription}
                onDateRendezVousChange={setDateRendezVous}
                date={dateReunion}
              />
            ),
          },
        ]}
      />
    </CustomContainer>
  );
};

export default CompteRenduFormPage;
