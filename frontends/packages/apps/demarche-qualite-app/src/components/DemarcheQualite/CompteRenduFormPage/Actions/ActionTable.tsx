import React, { Dispatch, FC, SetStateAction, useState } from 'react';
import { Column, Table, TableChange } from '@lib/ui-kit/src/components/atoms';
import { ReunionAction } from '../types';
import moment from 'moment';
import { strippedString } from '@lib/ui-kit/src/services/Helpers';

// TODO: Types

interface ActionTableProps {
  data: any[];
  rowsTotal: number;
  rowsSelected: any[];
  setRowsSelected: Dispatch<SetStateAction<any[]>>;
  loading?: boolean;
  error?: Error;
  handleSearch?: (change: TableChange) => void;
}

// TODO : Name

const columns: Column[] = [
  {
    name: '',
    label: 'Type',

    renderer: (row: ReunionAction) => {
      return row.type;
    },
  },

  {
    name: 'description',
    label: 'Description',
    renderer: (row: ReunionAction) => {
      return row.typeAssocie.description ? strippedString(row.typeAssocie.description) : '-';
    },
  },
  {
    name: 'dateCreation',
    label: 'Date de création',
    renderer: (row: ReunionAction) => {
      return row?.createdAt ? moment(new Date(row.createdAt)).format('DD/MM/YYYY') : '-';
    },
  },
  {
    name: 'dateUpdate',
    label: 'Date de modification',
    renderer: (row: ReunionAction) => {
      return row?.updatedAt ? moment(new Date(row.updatedAt)).format('DD/MM/YYYY') : '-';
    },
  },
  {
    name: '',
    label: 'Statut',
    renderer: (row: ReunionAction) => {
      return row.typeAssocie?.statut?.libelle;
    },
  },
];

export const ActionTable: FC<ActionTableProps> = ({
  rowsSelected,
  setRowsSelected,
  data,
  loading,
  rowsTotal,
  handleSearch,
  error,
}) => {
  const [showSelected, setShowSelected] = useState<boolean>(false);

  const onClearSelection = () => {
    setRowsSelected([]);
  };

  const onShowSelected = () => {
    setShowSelected(!showSelected);
  };

  const onSelectAll = () => {
    if (rowsSelected.length === data.length) {
      setRowsSelected([]);
    } else {
      setRowsSelected(data);
    }
  };

  return (
    <Table
      selectable
      rowsSelected={rowsSelected}
      onRowsSelectionChange={setRowsSelected}
      columns={columns}
      data={showSelected ? rowsSelected : data}
      onClearSelection={onClearSelection}
      onShowSelected={onShowSelected}
      showSelected={showSelected}
      onSelectAll={onSelectAll}
      rowsTotal={rowsTotal}
      search={false}
      loading={loading}
      onRunSearch={handleSearch}
      error={error}
    />
  );
};
