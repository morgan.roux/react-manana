import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginTop: 30,
      width: '100%',
      maxWidth: 1200,
      '& .MuiPaper-root': {
        boxShadow: '0px 2px 4px 0px rgba(0,0,0,0.25)',
      },
      '& header': {
        width: 'fit-content',
      },
      '& > div': {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
    },
    tablePaper: {
      marginTop: 25,
      padding: 25,
      width: '100%',
      minWidth: 700,
    },
    title: {
      fontSize: 20,
      fontWeight: 'bold',
      fontFamily: 'Roboto',
      letterSpacing: 0,
      color: theme.palette.common.black,
    },
    tableHead: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 25,
    },
    tableHeadBtnContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      '& > button:nth-child(2)': {
        marginLeft: 15,
      },
    },
    formControlLabel: {
      '& .MuiFormControlLabel-label': {
        fontFamily: 'Montserrat',
        fontSize: 14,
        fontWeight: 'normal',
        letterSpacing: 0.24,
        '& > span': {
          marginLeft: 10,
          fontWeight: 'bold',
        },
      },
    },
    forms: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
    },

    actionDateInput: {
      marginBottom: 10,
    },

    form: {},
  })
);

export default useStyles;
