import React, { FC } from 'react';
import { Box, Typography } from '@material-ui/core';
import useStyles from './style';

interface InfoLineProps {
  titre: string;
  libelle: string;
}

const InfoLine: FC<InfoLineProps> = ({ titre, libelle }) => {
  const classes = useStyles();
  
  return (
    <Box className={classes.infoLine}>
      <Typography>{titre} : </Typography>
      <Typography>{libelle}</Typography>
    </Box>
  );
};

export default InfoLine;
