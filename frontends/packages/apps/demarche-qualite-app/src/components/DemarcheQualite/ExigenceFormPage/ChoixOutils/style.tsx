import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    title: {
      marginBottom: '10px',
    },
    info: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      marginBottom: '10px',
    },
    infoLineBox: {
      width: '100%',
      background: '#E0E0E0',
      padding: 10,
      marginRight: 5,
    },
    infolineItem: {
      fontWeight: 'bold',
    },
    formBox: {
      width: '15%',
    },
    textField: {
      width: '100%',
    },
    infoLine: {
      display: 'flex',
      flexDirection: 'row',
    },
  })
);

export default useStyles;
