import React, { FC, useState, useEffect } from 'react';
import { Box } from '@material-ui/core';
import { IExigenceOutils } from '@lib/ui-kit/src/components/atoms/Exigence/Exigence';
import InfoLine from './InfoLine';
import { Table, Column } from '@lib/ui-kit/src/components/atoms/Table';
import useStyles from './style';

interface ChoixOutilsProps {
  titre: string;
  themeNom: string;
  themeOrdre: number;
  sousThemeNom: string;
  sousThemeOrdre: number;
  exigenceTitre: string;
  outils: IExigenceOutils;
  selectedOutils: any[];
  onChangeSelectionOutils: (selectedOutils: any[]) => void;
  typologies: ('affiche' | 'document' | 'enregistrement' | 'memo' | 'procedure' | 'checklist')[];
}

const COLUMNS: Column[] = [
  { name: 'code', label: 'Code' },
  { name: 'libelle', label: 'Nom' },
  { name: 'typologie', label: 'Type' },
];

const ChoixOutils: FC<ChoixOutilsProps> = ({
  titre,
  themeNom,
  sousThemeNom,
  exigenceTitre,
  outils,
  typologies,
  selectedOutils,
  onChangeSelectionOutils,
}) => {
  const [filteredOutilsByTypologies, setFilteredOutilsByTypologies] = useState<any[]>([]);
  const [showSelectedRows, setShowSelectedRows] = useState<boolean>(false);
  // const [value, setValue] = useState<string>('Tous');

  const classes = useStyles();

  // const data: string[] = ['Tous', 'Sélectionné', 'Non séléctionné'];

  /*const handleValueChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setValue((event.target as HTMLInputElement).value);
  };*/

  const handleSelectAll = () => {
    if (filteredOutilsByTypologies.length === selectedOutils.length) {
      onChangeSelectionOutils([]);
    } else {
      onChangeSelectionOutils(filteredOutilsByTypologies);
    }
  };

  useEffect(() => {
    if (outils && typologies) {
      const filteredOutils = (typologies || []).reduce((acc: any[], currentTypologie) => {
        if (currentTypologie === 'checklist') {
          return [...acc, ...outils.checklists.map((checklist) => ({ ...checklist, typologie: 'Checklist' }))];
        }

        const others =
          currentTypologie === 'affiche'
            ? outils.affiches
            : currentTypologie === 'document'
            ? outils.documents
            : currentTypologie === 'enregistrement'
            ? outils.enregistrements
            : currentTypologie === 'memo'
            ? outils.memos
            : currentTypologie === 'procedure'
            ? outils.procedures
            : [];

        return [
          ...acc,
          ...others.map((outil) => ({
            ...outil,
            typologie: `${outil.typologie.charAt(0).toUpperCase()}${outil.typologie.substring(1)}`,
          })),
        ];
      }, []);

      setFilteredOutilsByTypologies(filteredOutils);
    }
  }, [outils, typologies]);
  return (
    <Box style={{ width: 800 }}>
      <Box className={classes.title}>{titre}</Box>
      <Box className={classes.info}>
        <Box className={classes.infoLineBox}>
          <InfoLine titre="Thème" libelle={themeNom} />
          <InfoLine titre="Sous-thème" libelle={sousThemeNom} />
          <InfoLine titre="Exigence" libelle={exigenceTitre} />
        </Box>
        {/*<Box className={classes.formBox}>
          <TextField
            id="outlined-select-currency-native"
            select={true}
            label="Affichage"
            value={value}
            onChange={handleValueChange}
            SelectProps={{
              native: true,
            }}
            variant="outlined"
            className={classes.textField}
          >
            {data.map((option: string, index: number) => (
              <option key={index} value={option}>
                {option}
              </option>
            ))}
          </TextField>
            </Box>*/}
      </Box>
      <Box>
        <Table
          dataIdName="id"
          selectable={true}
          onRowsSelectionChange={onChangeSelectionOutils}
          onClearSelection={() => onChangeSelectionOutils([])}
          onSelectAll={handleSelectAll}
          rowsSelected={selectedOutils}
          showSelectedRows={showSelectedRows}
          onShowSelected={() => setShowSelectedRows(!showSelectedRows)}
          columns={COLUMNS}
          data={filteredOutilsByTypologies}
        />
      </Box>
    </Box>
  );
};

export default ChoixOutils;
