import React, { FC } from 'react';
import { Box } from '@material-ui/core';
import { CustomFormSection } from '@lib/ui-kit/src/components/templates/CustomFormSection';
import { CustomFormTextField } from '@lib/ui-kit/src/components/atoms/CustomTextField';
import { SidedFieldLabel } from '@lib/ui-kit/src/components/templates/SidedFieldLabel';
import { FieldsGroup } from '@lib/ui-kit/src/components/atoms/FieldsGroup';

export interface ChecklistFormPageProps {
  mode: 'creation' | 'modification';
  values: {
    [name: string]: any;
  };
  onChange: (name: string, value: any) => void;
}

const ChecklistFormPage: FC<ChecklistFormPageProps> = ({ values, onChange }) => {
  return (
    <Box style={{ width: 800 }}>
      <CustomFormSection title="Informations de base">
        <SidedFieldLabel label="Numéro d'ordre">
          <CustomFormTextField
            name="ordre"
            placeholder="Ordre"
            type="number"
            value={values.ordre}
            onChange={(event) => onChange('ordre', event.target.value)}
          />
        </SidedFieldLabel>
      </CustomFormSection>

      <CustomFormTextField
        name="titre"
        label="Titre de l'éxigence"
        value={values.titre}
        onChange={(event) => onChange('titre', event.target.value)}
        multiline={true}
        required={true}
      />

      <CustomFormTextField
        name="finalite"
        label="Finalité"
        value={values.finalite}
        onChange={(event) => onChange('finalite', event.target.value)}
        multiline={true}
        required={true}
      />

      <CustomFormSection title="Question à se poser">
        <FieldsGroup
          placeholder="votre question..."
          addFieldLabel="Ajouter une question"
          values={values.questions}
          onChange={(value) => onChange('questions', value)}
        />
      </CustomFormSection>

      <CustomFormSection title="Exemple de pratiques et preuves">
        <FieldsGroup
          placeholder="votre exemple de pratiques et de preuves..."
          addFieldLabel="Ajouter un exemple de pratiques et de preuves"
          values={values.exemples}
          onChange={(value) => onChange('exemples', value)}
        />
      </CustomFormSection>
    </Box>
  );
};

export default ChecklistFormPage;
