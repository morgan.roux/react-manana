import React, { FC, useState, useEffect } from 'react';
import { Stepper } from '@lib/ui-kit/src/components/atoms/Stepper';
import { DefinitionBaseForm } from './DefinitionBaseForm';
import { ChoixOutils } from './ChoixOutils';
import { IExigence, ITheme, ISousTheme, IExigenceOutils } from '@lib/ui-kit/src/components/atoms/Exigence/Exigence';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import { Backdrop } from '@lib/ui-kit/src/components/atoms/Backdrop';

export interface RequestSavingExigence {
  id: string | undefined;
  ordre: number;
  titre: string;
  finalite: string;
  exemples: {
    ordre: number;
    exemple: string;
  }[];
  questions: {
    ordre: number;
    question: string;
  }[];
  outils: {
    idOutilAssocie: string;
    typologie: string;
  }[];
}

export interface ExigenceFormPageProps {
  mode: 'creation' | 'modification';
  exigence?: IExigence;
  theme: ITheme;
  sousTheme: ISousTheme;
  outils: IExigenceOutils;
  loading?: boolean;
  saving?: boolean;
  error?: Error;
  onRequestSave: (toSaveExigence: RequestSavingExigence) => void;
  onRequestGoBack: () => void;
}

const ExigenceFormPage: FC<ExigenceFormPageProps> = ({
  mode,
  exigence,
  sousTheme,
  theme,
  outils,
  loading,
  error,
  saving,
  onRequestSave,
  onRequestGoBack,
}) => {
  const [definitionBaseValues, setDefinitionBaseValues] = useState<any>({
    questions: [],
    exemples: [],
  });
  const [outilsAssocies, setOutilsAssocies] = useState<any>([]);
  const [documentsAssocies, setDocumentsAssocies] = useState<any>([]);
  const [activeStep, setActiveStep] = useState<number | undefined>(0);

  const isDefinitionBaseValuesNotEmpty = (): boolean => {
    console.log(
      'Active step',
      !definitionBaseValues.titre,
      !definitionBaseValues.ordre,
      !definitionBaseValues.finalite
    );

    return !definitionBaseValues.titre || !definitionBaseValues.ordre || !definitionBaseValues.finalite;
  };

  const [isNextButtonActive, setIsNextButtonActive] = useState<boolean>(isDefinitionBaseValuesNotEmpty());

  /*const isOutilsAssociesEmpty = () : boolean => {
      return outilsAssocies.length === 0;
  };

  const isDocumentsAssociesEmpty = () : boolean => {
      return documentsAssocies.length === 0 ;
  };*/

  useEffect(() => {
    let active: boolean = false;
    if (activeStep === 0) {
      active = isDefinitionBaseValuesNotEmpty();
    }
    /*else if(activeStep === 1)
      {
        active = isOutilsAssociesEmpty();
      }
      else{
        active = isDocumentsAssociesEmpty();
      }*/

    console.log('Active step', activeStep, active);
    setIsNextButtonActive(active);
  }, [activeStep, definitionBaseValues]);

  useEffect(() => {
    if (exigence && mode === 'modification') {
      const newState = {
        ...exigence,
        questions: exigence.questions.map((question) => {
          return {
            name: question.id,
            ordre: question.ordre,
            value: question.question,
            deletable: true,
          };
        }),

        exemples: exigence.exemples.map((exemple) => {
          return {
            name: exemple.id,
            ordre: exemple.ordre,
            value: exemple.exemple,
            deletable: true,
          };
        }),
      };

      const exigenceOutils = exigence.outils;
      const newOutilsAssocies = [
        ...(exigenceOutils?.affiches || []),
        ...(exigenceOutils?.memos || []),
        ...(exigenceOutils?.enregistrements || []),
        ...(exigenceOutils?.procedures || []),
        ...(exigenceOutils?.checklists || []).map((checklist) => ({
          ...checklist,
          typologie: 'checklist',
        })),
      ];
      const newDocumentsAssocies = exigenceOutils?.documents || [];

      setDefinitionBaseValues(newState as any);
      setDocumentsAssocies(newDocumentsAssocies);
      setOutilsAssocies(newOutilsAssocies);
    }
  }, [exigence]);

  const handleChangeDefinitionBase = (name: string, value: any) => {
    setDefinitionBaseValues({
      ...definitionBaseValues,
      [name]: value,
    });
  };

  const handleSave = () => {
    onRequestSave({
      id: exigence?.id,
      ordre: parseInt(`${definitionBaseValues.ordre}`, 10),
      titre: definitionBaseValues.titre,
      finalite: definitionBaseValues.finalite,
      exemples: (definitionBaseValues.exemples || []).map(({ value, ordre }: any) => ({
        ordre,
        exemple: value,
      })),
      questions: (definitionBaseValues.questions || []).map(({ value, ordre }: any) => ({
        ordre,
        question: value,
      })),
      outils: [...outilsAssocies, ...documentsAssocies].map(({ typologie, id }: any) => ({
        idOutilAssocie: id,
        typologie: typologie.toLowerCase(),
      })),
    });
  };

  if (error) {
    return <ErrorPage />;
  }

  if (loading) {
    return <Backdrop />;
  }

  console.log('***isNextButtonActive*****', isNextButtonActive);
  return (
    <Stepper
      onStepChange={setActiveStep}
      onCancel={onRequestGoBack}
      onValidate={handleSave}
      loading={saving}
      disableNextBtn={isNextButtonActive}
      steps={[
        {
          title: 'Définition de base',
          content: (
            <DefinitionBaseForm values={definitionBaseValues} onChange={handleChangeDefinitionBase} mode={mode} />
          ),
        },
        {
          title: 'Choix des outils associés',
          content: (
            <ChoixOutils
              selectedOutils={outilsAssocies}
              onChangeSelectionOutils={setOutilsAssocies}
              themeNom={theme.nom}
              themeOrdre={theme.ordre}
              sousThemeNom={sousTheme.nom}
              sousThemeOrdre={sousTheme.ordre}
              outils={outils}
              typologies={['affiche', 'checklist', 'enregistrement', 'memo', 'procedure']}
              titre="Choix des outils associés"
              exigenceTitre={definitionBaseValues.titre}
            />
          ),
        },
        {
          title: 'Choix des documents associés',
          content: (
            <ChoixOutils
              selectedOutils={documentsAssocies}
              onChangeSelectionOutils={setDocumentsAssocies}
              themeNom={theme.nom}
              themeOrdre={theme.ordre}
              sousThemeNom={sousTheme.nom}
              sousThemeOrdre={sousTheme.ordre}
              outils={outils}
              typologies={['document']}
              titre="Choix des documents associés"
              exigenceTitre={definitionBaseValues.titre}
            />
          ),
        },
      ]}
    />
  );
};

export default ExigenceFormPage;
