import { Checklist } from './../ChecklistPage';

export const filtered = (checklist: Checklist, searchText: string): boolean => {
  const label = `${checklist.code}. ${checklist.libelle}`;
  return label.toLocaleLowerCase().indexOf(searchText.toLocaleLowerCase()) >= 0;
};
