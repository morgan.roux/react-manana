import { createStyles, makeStyles } from '@material-ui/core';
// import { start } from 'repl';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: 'flex',
      alignContent: 'start',
      alignItems: 'start',
      maxHeight: '100%',

      '& > *': {
        font: 'Roboto',
      },
    },
    rootCardTheme: {
      width: '100%',
    },
    boxLeft: {
      maxWidth: '326px',
      height: 'calc(100vh - 86px)',
      paddingLeft: 5,
      paddingRight: 5,
    },
    scrollList: {
      overflowY: 'auto',
      padding: '16px 24px 0px',
      height: 'calc(100vh - 230px)',
    },
    boxRight: {
      width: '100%',
      padding: '24px 0 !important',
    },
    checklistToDisp: {
      overflowY: 'hidden',
    },
    textItemcheckistToDisp: {
      alignSelf: 'baseline',
    },
  })
);

export default useStyles;
