import { Fichier } from '@lib/types';
import { Checklist } from './../ChecklistPage';

export interface ChecklistEvaluationSectionItem {
  id: string;
  ordre: number;
  libelle: string;
  idChecklistEvaluationSection: string;
  idChecklistSectionItem: string;
  idChecklist: string;
  verifie?: boolean;
}

export interface ChecklistEvaluationSection {
  id: string;
  ordre: number;
  libelle: string;
  items: ChecklistEvaluationSectionItem[];
}

export interface ChecklistEvaluation {
  id: string;
  ordre: number;
  libelle: string;
  createdAt: string;
  updatedAt: string;
  sections: ChecklistEvaluationSection[];
  cloture?: {
    fichiers?: Fichier[];
    commentaire?: string;
    createdAt?: string;
    updatedAt?: string;
  };
  updatedBy?: {
    id: string;
    fullName: string;
  };

  checklist?: Checklist; //
}
