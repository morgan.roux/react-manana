import { Dropzone } from '@lib/common';
import { Box } from '@material-ui/core';
import React, { FC, CSSProperties } from 'react';
import ReactQuill from 'react-quill';

export interface ClotureFormProps {
  style?: CSSProperties;
  selectedFile: File[];
  commentaire: string;
  setSelectedFile: (file: File[]) => void;
  setCommentaire: (content: string) => void;
}

const ClotureForm: FC<ClotureFormProps> = ({
  style,
  selectedFile,
  commentaire,
  setSelectedFile,
  setCommentaire,
}) => {

  return (
    <Box style={style}>
      <Dropzone
        contentText="Glissez et déposez les fichiers-joints ici"
        selectedFiles={selectedFile}
        setSelectedFiles={setSelectedFile}
        multiple={false}
      />
      <ReactQuill
        style={{ marginTop: 20 }}
        theme="snow"
        placeholder="Commentaire"
        value={commentaire}
        onChange={setCommentaire}
      />
    </Box>
  );
};

export default ClotureForm;
