import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      marginLeft: 20,
      paddingTop: '10px',
    },

    header: {
      marginBottom: 30,
    },
    evaluationTitle: {
      fontWeight: 'bold',
    },

    list: {
      width: '100%',
      boxSizing: 'border-box',
    },
    listItem: {
      border: '1px solid #E3E3E3',
      borderRadius: 5,
      marginBottom: 10,
      width: '100%',
      boxSizing: 'border-box',
    },
  })
);

export default useStyles;
