import React, { FC } from 'react';
import { Box, Typography, List, ListItem, ListItemIcon, ListItemText, Checkbox } from '@material-ui/core';
import CircleCheckedFilled from '@material-ui/icons/CheckCircle';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';

import useStyles from './styles';
import { Theme } from '@lib/ui-kit/src/components/atoms/Theme';
import { ChecklistEvaluation, ChecklistEvaluationSectionItem } from '../types';
import { Checklist } from './../../ChecklistPage';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import { Loader } from '@lib/ui-kit/src/components/atoms/Loader';
import { ClotureInfo } from './ClotureInfo';

interface EvaluationDetailProps {
  loading?: boolean;
  error?: Error;
  togglingEvaluationSectionItemIds?: string[];
  errorTogglingEvaluationSectionItemIds?: string[];
  checklist?: Checklist;
  evaluation?: ChecklistEvaluation;
  onEvaluationSectionItemClick: (
    evaluation: ChecklistEvaluation,
    evaluationSectionItem: ChecklistEvaluationSectionItem
  ) => void;
}

const EvaluationDetail: FC<EvaluationDetailProps> = ({
  onEvaluationSectionItemClick,
  evaluation,
  checklist,
  loading,
  error,
  togglingEvaluationSectionItemIds,
  errorTogglingEvaluationSectionItemIds,
}) => {
  const classes = useStyles();

  if (loading) {
    return <Loader />;
  }
  if (error || (errorTogglingEvaluationSectionItemIds && errorTogglingEvaluationSectionItemIds.length > 0)) {
    return <ErrorPage />;
  }

  if (!checklist || !evaluation) {
    return null;
  }

  return (
    <Box className={classes.root}>
      <Box className={classes.header}>
        <Typography variant="h5" className={classes.evaluationTitle}>
          {evaluation.libelle}
        </Typography>
        <Typography>
          Modèle : <span style={{ fontWeight: 'bold' }}>{checklist.libelle}</span>
        </Typography>
      </Box>
      {(evaluation.cloture?.commentaire || (evaluation.cloture?.fichiers?.length || 0) > 0) && (
        <ClotureInfo checklistEvaluation={evaluation} />
      )}
      <Box>
        {evaluation.sections.map((section) => {
          return (
            <Theme
              checkList
              key={`${evaluation.id}-${section.id}`}
              expended
              theme={{
                nom: section.libelle,
              }}
            >
              <List className={classes.list}>
                {section.items.map((item) => {
                  return (
                    <ListItem
                      className={classes.listItem}
                      key={`${evaluation.id}-${section.id}-${item.id}`}
                      role={undefined}
                      dense
                      button
                      disabled={
                        !!evaluation.cloture ||
                        (togglingEvaluationSectionItemIds && togglingEvaluationSectionItemIds.includes(item.id))
                      }
                      onClick={() => onEvaluationSectionItemClick(evaluation, item)}
                    >
                      <ListItemIcon>
                        <Checkbox
                          icon={<CircleUnchecked />}
                          checkedIcon={<CircleCheckedFilled />}
                          edge="start"
                          checked={item.verifie}
                          tabIndex={-1}
                          disableRipple
                          inputProps={{ 'aria-labelledby': item.id }}
                        />
                      </ListItemIcon>
                      <ListItemText id={item.id} primary={item.libelle} />
                    </ListItem>
                  );
                })}
              </List>
            </Theme>
          );
        })}
      </Box>
    </Box>
  );
};

export default EvaluationDetail;
