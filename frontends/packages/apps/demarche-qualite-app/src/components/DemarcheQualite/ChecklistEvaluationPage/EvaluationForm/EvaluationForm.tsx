import React, { FC } from 'react';
import { Box, Typography } from '@material-ui/core';
import { CustomFormTextField } from '@lib/ui-kit/src/components/atoms/CustomTextField';
import { Checklist } from './../../ChecklistPage';
import useStyles from './styles';

interface EvaluationFormProps {
  checklist?: Checklist;
  defaultValue?: string;
  value?: string;
  onChange: (value: string) => void;
}

const EvaluationForm: FC<EvaluationFormProps> = ({ checklist, value, defaultValue, onChange }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Typography className={classes.checklist}>
        Source: <span style={{ fontWeight: 'bold' }}>{checklist?.libelle}</span>
      </Typography>
      <CustomFormTextField
        label="Intitulé"
        onChange={(event) => onChange(event.target.value)}
        defaultValue={defaultValue}
        value={value}
        multiline={true}
      />
    </Box>
  );
};

export default EvaluationForm;
