import React, { FC, MouseEvent, CSSProperties } from 'react';
import { Box } from '@material-ui/core';
import useStyles from './styles';
import { Banner } from '@lib/ui-kit/src/components/atoms/Banner';
import { DebouncedSearchInput } from '@lib/ui-kit/src/components/atoms/DebouncedSearchInput';

interface SearchBannerProps {
  style?: CSSProperties;
  title?: string;
  searchValue: string;
  placeholder?: string;
  onChangeSearchValue: (value: string) => void;
  onRequestGoBack: (event: MouseEvent) => void;
}

const SearchBanner: FC<SearchBannerProps> = ({
  style,
  title,
  placeholder,
  searchValue,
  onChangeSearchValue,
  onRequestGoBack,
}) => {
  const classes = useStyles();

  return (
    <Box className={classes.root} style={style}>
      <Banner withDivider={false} title={title || 'Check-list'} onClick={onRequestGoBack} />
      <Box>
        <DebouncedSearchInput
          wait={100}
          fullWidth={false}
          dark={true}
          onChange={onChangeSearchValue}
          value={searchValue}
          placeholder={placeholder}
        />
      </Box>
    </Box>
  );
};

export default SearchBanner;
