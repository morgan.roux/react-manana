import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {},
    checklist: {
      marginTop: 24,
      marginBottom: 24,
    },
  })
);

export default useStyles;
