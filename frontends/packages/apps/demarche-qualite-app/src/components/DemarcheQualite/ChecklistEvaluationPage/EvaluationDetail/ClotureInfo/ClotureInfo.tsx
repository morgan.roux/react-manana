import { Box, Divider, Chip } from '@material-ui/core';
import PdfIcon from '@material-ui/icons/PictureAsPdf';
import React, { FC } from 'react';
import useStyles from './style';
import { ChecklistEvaluation } from '../..';

interface ClotureInfoProps {
  checklistEvaluation: ChecklistEvaluation;
}

const ClotureInfo: FC<ClotureInfoProps> = ({ checklistEvaluation }) => {
  const classes = useStyles();

  return (
    <Box>
      <Divider className={classes.marginDevider} />
      <Box>
        {checklistEvaluation.cloture?.commentaire && (
          <Box>
            <span className={classes.problemeLabel}>Commentaire</span>
            <Box dangerouslySetInnerHTML={{ __html: checklistEvaluation.cloture?.commentaire }} />
          </Box>
        )}

        {(checklistEvaluation?.cloture?.fichiers?.length || 0) > 0 && (
          <Box>
            <span className={classes.problemeLabel}>Fichier(s) joint(s)</span>
            <Box>
              {(checklistEvaluation.cloture?.fichiers || []).map((fiche) => {
                return (
                  <Chip
                    className={classes.ficheBox}
                    key={fiche.id}
                    icon={<PdfIcon />}
                    clickable={true}
                    label={fiche.nomOriginal}
                    component="a"
                    target="_blank"
                    href={fiche.publicUrl}
                  />
                );
              })}
            </Box>
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default ClotureInfo;
