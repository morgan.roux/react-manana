import React, { FC, MouseEvent, useState, useEffect, ReactNode } from 'react';
import { Box, ListItem, ListItemIcon, ListItemText, List, Divider } from '@material-ui/core';
import ChecklistIcon from '@material-ui/icons/FiberManualRecord';
import AddIcon from '@material-ui/icons/Add';
import { Checklist } from '../ChecklistPage';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import { ItemContainer } from '@lib/ui-kit/src/components/templates/ItemContainer';
import { SearchBanner } from './SearchBanner';
import { Backdrop } from '@lib/ui-kit/src/components/atoms/Backdrop';
import { NewCustomButton } from '@lib/ui-kit/src/components/atoms/CustomButton';
import { filtered } from './util';
import useStyles from './styles';
import { ChecklistEvaluation, ChecklistEvaluationSectionItem } from './types';
import moment from 'moment';
import { CardTheme } from '@lib/ui-kit/src/components/atoms/CardTheme';
import { Close, Delete, Edit } from '@material-ui/icons';
import { EvaluationDetail } from './EvaluationDetail';
import { EvaluationForm } from './EvaluationForm';
import { ClotureForm } from './ClotureForm';
import { CustomModal } from '@lib/ui-kit/src/components/atoms/CustomModal';
import { ConfirmDeleteDialog } from '@lib/ui-kit/src/components/atoms';

// ATTENTION : Pour les checklist evaluation encours/clôturés, il faut charger les checklist(s), (Evaluation.checklist)

export interface ChecklistEvaluationPageProps {
  // components
  emptyListComponent: ReactNode;
  noItemSelectedComponent: ReactNode;

  loadingChecklists?: boolean;
  errorLoadingChecklists?: Error;
  checklists?: Checklist[];

  loadingEvaluations?: boolean;
  errorLoadingEvaluations?: Error;
  evaluations?: ChecklistEvaluation[];

  togglingEvaluationSectionItemIds?: string[];
  errorTogglingEvaluationSectionItemIds?: string[];

  // selected ids
  selectedChecklistId?: string;
  selectedEvaluationId?: string;

  // creation/modification/deletion evaluation states
  savingEvaluation?: boolean;
  errorSavingEvaluation?: Error;

  // loading selected evaluation
  loadedEvaluation?: ChecklistEvaluation;
  loadingEvaluation?: boolean;
  errorLoadingEvaluation?: Error;

  idFromUrl?: string;

  onRequestGoBack: (event: MouseEvent) => void;
  onRequestSearch?: (text: string) => void;
  onRequestEvaluations: (checklist: Checklist) => void;
  onRequestEvaluationsByStatus: (cloture: boolean) => void;
  onRequestToggleItem: (evaluation: ChecklistEvaluation, evaluationSectionItem: ChecklistEvaluationSectionItem) => void;

  // Update if id is undefined
  onRequestSaveEvaluation: (id: string | undefined, ordre: number, libelle: string) => void;
  onRequestDeleteEvaluation: (evaluation: ChecklistEvaluation) => void;
  onRequestEvaluation: (id: string) => void;
  onRequestCloseEvaluation: (params: {
    evaluation: ChecklistEvaluation;
    fichiers?: File[];
    commentaire?: string;
  }) => void;

  cloture: {
    saving?: boolean;
    saved?: boolean;
    setSaved?: (newValue: boolean) => void;
  };
}

const searchChecklist = (checklists: Checklist[], searchText: string): Checklist[] => {
  if (searchText) {
    return checklists.filter((checklist) => filtered(checklist, searchText));
  }
  return checklists;
};

const ChecklistEvaluationPage: FC<ChecklistEvaluationPageProps> = ({
  emptyListComponent,
  noItemSelectedComponent,
  onRequestSearch,
  onRequestGoBack,
  onRequestEvaluations,
  onRequestEvaluationsByStatus,
  onRequestSaveEvaluation,
  onRequestDeleteEvaluation,
  onRequestEvaluation,
  onRequestToggleItem,
  onRequestCloseEvaluation,
  idFromUrl,
  selectedChecklistId,
  selectedEvaluationId,
  loadingChecklists,
  errorLoadingChecklists,
  checklists,
  loadedEvaluation,
  loadingEvaluation,
  errorLoadingEvaluation,
  loadingEvaluations,
  errorLoadingEvaluations,
  evaluations,
  togglingEvaluationSectionItemIds,
  cloture,
}) => {
  const classes = useStyles();
  const [searchValue, setSearchValue] = useState<string>('');
  const [originalChecklists, setOriginalChecklists] = useState<Checklist[]>([]);
  const [displayChecklists, setDisplayChecklists] = useState<Checklist[]>([]);
  const [selectedChecklist, setSelectedChecklist] = useState<Checklist>();
  const [originalEvaluations, setOriginalEvaluations] = useState<ChecklistEvaluation[]>([]);
  const [selectedEvaluation, setSelectedEvaluation] = useState<ChecklistEvaluation>();
  const [selectedAccessRapide, setSelectedAccessRapide] = useState<'encours' | 'cloture'>();

  // Form
  const [openFormDialog, setOpenFormDialog] = useState<boolean>(false);
  const [evaluationLibelle, setEvaluationLibelle] = useState<string>();

  const [mode, setMode] = useState<'creation' | 'modification'>('creation');
  const [toEditEvaluation, setToEditEvaluation] = useState<ChecklistEvaluation>();
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [toDeleteEvaluation, setToDeleteEvaluation] = useState<ChecklistEvaluation>();

  // Clôture
  const [clotureCommentaire, setClotureCommentaire] = useState<string>('');
  const [clotureSelectedFile, setClotureSelectedFile] = useState<File[]>([]);
  const [openClotureDialog, setOpenClotureDialog] = useState<boolean>(false);
  const [clotureEvaluation, setClotureEvaluation] = useState<ChecklistEvaluation>();

  // 1. Update OriginalChecklist from checklists props
  useEffect(() => {
    if (checklists) {
      setOriginalChecklists(checklists);

      // Default checklist
      if (checklists && selectedChecklistId && !selectedChecklist) {
        const defaultChecklist = checklists.find(({ id }) => id === selectedChecklistId);
        if (defaultChecklist) {
          setSelectedChecklist(defaultChecklist);
          onRequestEvaluations(defaultChecklist);
        }
      }
    }
  }, [loadingChecklists, checklists]);

  // 2. Update displayChecklists when orginal list or searchValue changed.
  useEffect(() => {
    setDisplayChecklists(searchChecklist(originalChecklists || [], searchValue));
  }, [searchValue, originalChecklists]);

  // 3. Make selectedChecklist undefined when it doest exist on displayChecklists
  useEffect(() => {
    if (
      displayChecklists &&
      selectedChecklist &&
      !displayChecklists.some((checklist) => checklist.id === selectedChecklist.id)
    ) {
      setSelectedChecklist(undefined);
    }
  }, [selectedChecklist, displayChecklists]);

  // 4. Evaluation

  useEffect(() => {
    if (evaluations) {
      setOriginalEvaluations(evaluations);

      // Default evaluation
      if (selectedEvaluationId && !selectedEvaluation) {
        const defaultEvaluation = evaluations.find(({ id }) => id === selectedEvaluationId);
        if (defaultEvaluation) {
          setSelectedEvaluation(defaultEvaluation);
        }
      }
    }
  }, [evaluations]);

  // 5. loading evaluation

  useEffect(() => {
    if (loadedEvaluation) {
      setSelectedEvaluation(loadedEvaluation);
    }
  }, [loadedEvaluation]);

  useEffect(() => {
    if (cloture.saved) {
      setOpenClotureDialog(false);
      setClotureCommentaire('');
      setClotureSelectedFile([]);
      setClotureEvaluation(undefined);

      if (cloture.setSaved) {
        cloture.setSaved(false);
      }
    }
  }, [cloture.saved]);

  const handleChangeSearchValue = (value: string) => {
    if (onRequestSearch) {
      onRequestSearch(value);
    }
    setSearchValue(value);
  };

  const handleSelectedChecklist = (checklist: Checklist) => {
    setSelectedChecklist(checklist);
    setSelectedAccessRapide(undefined);
    setSelectedEvaluation(undefined);
    onRequestEvaluations(checklist);
  };

  const handleSelectedAccessRapide = (accessRapide: 'encours' | 'cloture') => {
    setSelectedChecklist(undefined);
    setSelectedAccessRapide(accessRapide);
    setSelectedEvaluation(undefined);
    onRequestEvaluationsByStatus(accessRapide === 'cloture');
  };

  const handleSelectedEvaluation = (evaluation: ChecklistEvaluation) => {
    // Access rapide : Set checklist
    if (selectedAccessRapide) {
      setSelectedChecklist(evaluation.checklist);
    }

    setSelectedEvaluation(evaluation);
    onRequestEvaluation(evaluation.id);
  };

  const handleClickAddEvaluation = () => {
    setEvaluationLibelle('');
    setOpenFormDialog(true);
    setMode('creation');
  };

  const handleClickEditEvaluation = (evaluation: ChecklistEvaluation) => {
    setEvaluationLibelle(evaluation.libelle);
    setMode('modification');
    setToEditEvaluation(evaluation);
    setOpenFormDialog(true);
  };

  const handleClickDeleteEvaluation = (evaluation: ChecklistEvaluation) => {
    setToDeleteEvaluation(evaluation);
    setOpenDeleteDialog(true);
  };

  const handleConfirmDelete = () => {
    if (toDeleteEvaluation) {
      setOpenDeleteDialog(false);
      onRequestDeleteEvaluation(toDeleteEvaluation);

      if (toDeleteEvaluation.id === selectedEvaluation?.id) {
        setSelectedEvaluation(undefined);
      }
    }
  };

  const handleClickSaveEvaluation = () => {
    if (evaluationLibelle) {
      setOpenFormDialog(false);
      onRequestSaveEvaluation(
        mode === 'modification' ? toEditEvaluation?.id : undefined,
        (mode === 'modification' ? toEditEvaluation?.ordre : checklists?.length) || 1,
        evaluationLibelle
      );
    }
  };

  const handleClickCloseEvaluation = (evaluation: ChecklistEvaluation): void => {
    setClotureCommentaire('');
    setClotureSelectedFile([]);
    setOpenClotureDialog(true);
    setClotureEvaluation(evaluation);
  };

  const handleConfirmCloseEvaluation = () => {
    if (clotureEvaluation) {
      onRequestCloseEvaluation({
        commentaire: clotureCommentaire,
        evaluation: clotureEvaluation,
        fichiers: clotureSelectedFile,
      });
    }
  };

  if (loadingChecklists) {
    return <Backdrop />;
  }

  if (errorLoadingChecklists) {
    return <ErrorPage />;
  }

  return (
    <Box className={classes.root}>
      <Box className={classes.boxLeft}>
        <SearchBanner
          style={{
            padding: '16px 24px 0px',
          }}
          onRequestGoBack={onRequestGoBack}
          onChangeSearchValue={handleChangeSearchValue}
          searchValue={searchValue}
        />
        <Divider style={{ marginTop: 23 }} />

        <List className={classes.scrollList}>
          <ListItem
            className={classes.checklistToDisp}
            key="acces-rapide-cl-encours"
            button
            selected={selectedAccessRapide === 'encours'}
            onClick={() => handleSelectedAccessRapide('encours')}
          >
            <ListItemText className={classes.textItemcheckistToDisp} primary="En cours" />
          </ListItem>

          <ListItem
            className={classes.checklistToDisp}
            key="acces-rapide-cl-cloture"
            button
            selected={selectedAccessRapide === 'cloture'}
            onClick={() => handleSelectedAccessRapide('cloture')}
          >
            <ListItemText className={classes.textItemcheckistToDisp} primary="Clôturés" />
          </ListItem>

          {displayChecklists.map((checklistToDisplay, index: number) => {
            return (
              <ListItem
                className={classes.checklistToDisp}
                key={checklistToDisplay.id || index}
                button
                selected={
                  !!(selectedChecklist?.id && checklistToDisplay.id === selectedChecklist?.id) ||
                  !!(idFromUrl === checklistToDisplay.id)
                }
                onClick={() => handleSelectedChecklist(checklistToDisplay)}
              >
                <ListItemIcon>
                  <ChecklistIcon />
                </ListItemIcon>
                <ListItemText
                  className={classes.textItemcheckistToDisp}
                  primary={`${checklistToDisplay.code}. ${checklistToDisplay.libelle}`}
                />
              </ListItem>
            );
          })}
        </List>
      </Box>
      <Divider orientation="vertical" flexItem />
      <Box className={classes.boxRight}>
        <ItemContainer
          loading={loadingEvaluations}
          error={errorLoadingEvaluations}
          items={originalEvaluations}
          emptyListComponent={emptyListComponent}
          listBanner={
            <Box style={{ textAlign: 'center' }}>
              <NewCustomButton
                disabled={!selectedChecklist}
                onClick={() => handleClickAddEvaluation()}
                startIcon={<AddIcon />}
              >
                Créer une nouvelle copie
              </NewCustomButton>
              <Divider style={{ marginTop: 10 }} />
            </Box>
          }
          listItemRenderer={(evaluation: any, _index: number) => {
            const creationMessage = evaluation.createdAt ? `${moment(evaluation.createdAt).format('DD/MM/YYYY')}` : ``;
            const modificationMessage = evaluation.updatedAt
              ? `${moment(evaluation.updatedAt).format('DD/MM/YYYY')}`
              : '';

            const infoToDisplay: any = evaluation?.cloture
              ? {
                  littleComment: `Clôturé par ${evaluation?.updatedBy?.fullName}`,
                }
              : {
                  createDate: creationMessage,
                  updateDate: modificationMessage,
                };

            return (
              <CardTheme
                onClick={() => handleSelectedEvaluation(evaluation)}
                title={evaluation.libelle}
                active={selectedEvaluation?.id === evaluation.id}
                currentId={evaluation.id}
                {...infoToDisplay}
                moreOptions={[
                  {
                    menuItemLabel: {
                      title: 'Modifier',
                      icon: <Edit />,
                    },
                    disabled: evaluation?.cloture as any,
                    onClick: () => {
                      handleClickEditEvaluation(evaluation);
                    },
                  },
                  {
                    menuItemLabel: {
                      title: 'Clôturer',
                      icon: <Close />,
                    },
                    disabled: evaluation?.cloture as any,
                    onClick: () => {
                      handleClickCloseEvaluation(evaluation);
                    },
                  },
                  {
                    menuItemLabel: {
                      title: 'Supprimer',
                      icon: <Delete />,
                    },
                    disabled: evaluation?.cloture as any,
                    onClick: () => {
                      handleClickDeleteEvaluation(evaluation);
                    },
                  },
                ]}
              />
            );
          }}
          detailRenderer={() =>
            selectedEvaluation ? (
              <EvaluationDetail
                key={selectedEvaluation.id}
                togglingEvaluationSectionItemIds={togglingEvaluationSectionItemIds}
                onEvaluationSectionItemClick={onRequestToggleItem}
                loading={loadingEvaluation}
                error={errorLoadingEvaluation}
                checklist={selectedChecklist}
                evaluation={selectedEvaluation}
              />
            ) : (
              noItemSelectedComponent
            )
          }
        />
      </Box>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<span>Êtes-vous sur de vouloir supprimer ce check-list ? </span>}
        onClickConfirm={handleConfirmDelete}
      />

      <CustomModal
        headerWithBgColor
        closeIcon
        title="Créer une nouvelle copie"
        open={openFormDialog}
        actionButton={mode === 'creation' ? 'Ajouter' : 'Enregistrer'}
        setOpen={setOpenFormDialog}
        onClickConfirm={() => handleClickSaveEvaluation()}
      >
        <EvaluationForm value={evaluationLibelle} onChange={setEvaluationLibelle} checklist={selectedChecklist} />
      </CustomModal>
      <CustomModal
        title={'Clôture'}
        open={openClotureDialog}
        setOpen={setOpenClotureDialog}
        withBtnsActions
        closeIcon
        centerBtns={false}
        headerWithBgColor
        fullWidth
        maxWidth="sm"
        onClickConfirm={handleConfirmCloseEvaluation}
        disabledButton={cloture.saving}
      >
        <ClotureForm
          style={{ width: '100%' }}
          commentaire={clotureCommentaire}
          setCommentaire={setClotureCommentaire}
          selectedFile={clotureSelectedFile}
          setSelectedFile={setClotureSelectedFile}
        />
      </CustomModal>
    </Box>
  );
};

export default ChecklistEvaluationPage;
