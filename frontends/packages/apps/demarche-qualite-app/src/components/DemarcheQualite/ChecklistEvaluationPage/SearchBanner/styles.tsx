import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      position: 'sticky',
      top: 0,
      zIndex: 50,
      background: '#FFF',
      marginBottom: 23,
    },
  })
);

export default useStyles;
