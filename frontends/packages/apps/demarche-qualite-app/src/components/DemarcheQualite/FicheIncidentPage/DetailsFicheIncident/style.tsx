import { createStyles, makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() =>
  createStyles({
    detailsContent: {
      width: '80%',
      margin: '0 auto',
      minHeight: '80%',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginTop: 30,
    },
    infoBox: {
      padding: 15,
      width: '43%',
      flex: 1,
      display: 'block',
      marginRight: 15, // A enlever une fois le positionnement reglé !!
    },
    fichierJointLabel: {
      display: 'block',
      textAlign: 'left',
      font: 'normal normal normal 14px/17px Roboto',
      letterSpacing: 0.47,
      color: '#878787',
      opacity: 1,
      marginBottom: 10,
    },
    actionBox: {
      width: '57%',
      flex: 1,
      display: 'block',
      padding: 15,
      height: 'fit-content',
    },
    actionBoxBorder: {
      border: '1px dashed #373435',
      boxShadow: 'none',
      width: '57%',
      flex: 1,
      display: 'block',
      padding: 15,
    },
    span: {
      display: 'block',
      fontWeight: 'bold',
      marginBottom: 20,
      marginTop: 8,
    },
    problemeLabel: {
      display: 'block',
    },
    probleme: {
      position: 'relative',
      padding: 10,
    },
    buttonContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-around',
    },

    buttonContainerButton: {
      flexGrow: 1,
      height: 50,
      margin: 5,
    },

    addActionButton: {
      marginTop: 25,
    },

    actionForms: {
      paddingTop: 20,
      display: 'block',
      flexDirection: 'column',
      width: '100%',
    },
    actionForm: {
      marginBottom: 5,
      // '& .main-MuiInputBase-root': {
      //   minHeight: '80px',
      // },
    },
    actionFormDescription: {
      marginBottom: 5,
      '& .main-MuiInputBase-root': {
        minHeight: '85px',
      },
    },

    actionDateInput: {
      marginBottom: 10,
    },

    descriptionField: {
      color: 'black',
      marginTop: 24,
      marginBottom: 10,
    },
    marginDevider: {
      marginBottom: 15,
    },
    bgDecriptionField: {
      background: '#F2F2F2 0% 0% no-repeat padding-box',
    },
    ficheBox: {
      background: 'var(--texte-et-forme-6) 0% 0% no-repeat padding-box',
      borderRadius: 2,
      border: '1px solid #9E9E9E',
      opacity: 1,
      width: 150,
      height: 31,
      marginLeft: 3,
    },
  })
);

export default useStyles;
