import { Theme, createStyles, makeStyles } from '@material-ui/core';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    iconAction: {
      padding: theme.spacing(0.5),
      margin: theme.spacing(0, 0.65, 0, 0),
    },
  }),
);

export default useStyles;
