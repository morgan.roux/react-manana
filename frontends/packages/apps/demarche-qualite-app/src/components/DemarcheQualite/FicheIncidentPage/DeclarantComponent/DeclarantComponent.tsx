import { Box, FormControlLabel, Radio } from '@material-ui/core';
import React, { FC, CSSProperties, ChangeEvent, useState } from 'react';
import { CustomFormTextField /*, CustomSelect*/ } from '@lib/ui-kit/src/components/atoms';
import Collaborateur from './Collaborateur/Collaborateur';
import useStyles from './style';
import { Collaborateur as ICollaborateur } from '../types';

interface DeclarantComponentProps {
  style?: CSSProperties;
  data: ICollaborateur[];
  onCheckRequest: (collaborateur: ICollaborateur) => void;
}

const DeclarantComponent: FC<DeclarantComponentProps> = ({ style, data, onCheckRequest }) => {
  const classes = useStyles();

  const [searchValue, setSearchValue] = useState<string>('');
  // const [typeValue, setTypeValue] = useState<string>('');
  const [checkedRadios, setCheckedRadios] = useState<string[]>([]);
  const [currentData, setCurrentData] = useState<ICollaborateur[]>(data);

  /*
  const filterType = (tab: ICollaborateur[]): string[] => {
    let typeTab: string[] = [];
    tab.map((currentData: ICollaborateur) => typeTab.push(currentData.type));

    return [...new Set(typeTab)];
  };
  */

  const handleSearchValueChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const currentValue: string = (event.target as HTMLInputElement).value;
    setSearchValue(currentValue);
    setCurrentData(
      data.filter((element) => element.fullName === currentValue) // A remedier
    );
  };

  /*
  const handleTypeValueChange = (event: ChangeEvent<HTMLSelectElement>): void => {
    const currentType = (event.target as HTMLSelectElement).value;
    setTypeValue(currentType);
    setCurrentData(data.filter((element) => element.type === currentType));
  };
  */

  const handleRadioClick = (collaborateur: ICollaborateur): void => {
    let tab: string[] = [];
    if (checkedRadios.find((element) => element === collaborateur.id)) {
      tab = checkedRadios.filter((element) => element !== collaborateur.id);
      setCheckedRadios(tab);
    } else {
      tab.push(collaborateur.id);
      setCheckedRadios(tab);
    }

    onCheckRequest(collaborateur);
  };

  return (
    <Box className={classes.root} style={style}>
      <Box className={classes.formsContent}>
        <Box className={classes.searchInputContent}>
          <CustomFormTextField
            fullWidth={true}
            value={searchValue}
            onChange={handleSearchValueChange}
            placeholder="Recherche..."
          />
        </Box>
        {/*<Box className={classes.typeSelectContent}>
                    <CustomSelect
                        label="Type"
                        list={filterType(data)}
                        value={typeValue}
                        onChange={handleTypeValueChange}
                    />
    </Box>*/}
      </Box>
      {currentData && currentData.length > 0 ? (
        <Box>
          {currentData.map((data: ICollaborateur) => (
            <Box className={classes.dataContent} key={`data-content-${data.id}`}>
              <Box>
                <Collaborateur key={data.id} collaborateur={data} />
              </Box>
              <Box>
                <FormControlLabel
                  control={
                    <Radio
                      onClick={() => handleRadioClick(data)}
                      checked={typeof checkedRadios.find((element) => element === data.id) === 'string'}
                    />
                  }
                  label=""
                />
              </Box>
            </Box>
          ))}
        </Box>
      ) : (
        <Box className={classes.noValue}>Aucune correspondance</Box>
      )}
    </Box>
  );
};

export default DeclarantComponent;
