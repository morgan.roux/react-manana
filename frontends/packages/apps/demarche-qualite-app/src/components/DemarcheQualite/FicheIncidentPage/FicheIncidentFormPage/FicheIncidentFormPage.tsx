import { Dropzone, useApplicationContext } from '@lib/common';
import { Box, CssBaseline, /* FormControl, FormControlLabel, Radio, RadioGroup,*/ Typography } from '@material-ui/core';
import React, { FC, MouseEvent, ReactNode, useEffect, useState } from 'react';
import ReactQuill from 'react-quill';
import { CustomDatePicker, CustomSelect } from '@lib/ui-kit/src/components/atoms';
import { FormButtons } from '@lib/ui-kit/src/components/atoms/FormButtons';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import { ActionOperationnelle } from '../../ActionOperationnellePage/types';
import { FicheIncident } from '../types';
import { Backdrop } from '@lib/ui-kit/src/components/atoms/Backdrop';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import { FicheAmelioration, FicheCause, FicheType } from '../../FicheAmeliorationPage/types';
import useStyles from './styles';

export interface RequestSavingFicheIncident {
  id?: string;
  description: string;
  idOrigine: string;
  idType: string;
  idCause: string;
  dateFiche: any;
  idOrigineAssocie: string;
  idUserDeclarant: string;
  idUserSuivi?: string;
  idImportance: string;
  idFonction: string;
  idTache: string;
  fichiers: File[];
  idUserParticipants: string[];
}

interface FicheIncidentFormPageProps {
  type: 'amelioration' | 'incident' | 'action';
  mode: 'creation' | 'modification';
  ficheIncident?: FicheIncident;
  ficheAmelioration?: FicheAmelioration;
  ficheAction?: ActionOperationnelle;
  loading?: boolean;
  saving?: boolean;
  error?: Error;

  types?: {
    loading?: boolean;
    error?: Error;
    data?: FicheType[];
  };

  causes?: {
    loading?: boolean;
    error?: Error;
    data?: FicheCause[];
  };

  idUserSuivi?: string;
  idFicheIncident?: string;
  idOrigine: string;
  idOrigineAssocie: string;
  idImportance: string;
  idFonction: string;
  idTache: string;
  idUserDeclarant: string;
  idUserParticipants: string[];

  commonFieldsComponent: ReactNode;
  selectAuteurComponent: ReactNode;
  selectSuiviParComponent: ReactNode;
  originInput: ReactNode;
  commonFieldsParticipantsAInformerComponent: ReactNode;

  onRequestGoBack: (event: MouseEvent) => void;
  onRequestSave: (toSave: RequestSavingFicheIncident) => void;
}

const FicheIncidentFormPage: FC<FicheIncidentFormPageProps> = ({
  type,
  mode,
  ficheIncident,
  ficheAmelioration,
  ficheAction,
  loading,
  error,
  saving,
  types,
  causes,
  //selectAuteurComponent,
  selectSuiviParComponent,
  //commonFieldsComponent,
  commonFieldsParticipantsAInformerComponent,
  //idFicheIncident,
  idOrigine,
  idOrigineAssocie,
  idImportance,
  idFonction,
  idTache,
  idUserSuivi,
  idUserDeclarant,
  idUserParticipants,
  originInput,
  onRequestGoBack,
  onRequestSave,
}) => {
  const classes = useStyles();
  const [description, setDescription] = useState<string>('');
  const [idType, setIdType] = useState<string>('');
  const [idCause, setIdCause] = useState<string>('');
  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  const [dateFiche, setDateFiche] = useState<any>();

  console.log('', selectSuiviParComponent);

  const origineSectionTitle = `Qui est à l'origine de ${
    type === 'incident' ? 'cet incident' : type === 'action' ? 'cette action' : 'cette amélioration'
  } ?`;
  /*const participantsEnChargeSectionTitle = `Qui est en charge de ${
    type === 'incident' ? 'cet incident' : type === 'action' ? 'cette action' : 'cette amélioration'
  } ?`;
  const dateEcheanceSectionTitle = 'Quelle sera la date de prise en charge ?';
  const participantsAInformerSectionTitle = 'Qui doit-on informer ? ';
*/

  /*const [value, setValue] = React.useState('moyen');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue((event.target as HTMLInputElement).value);
  };*/

  const disabledConfirmButton = (): boolean => {
    return description === '' || idImportance === '' || idOrigine === '' || idType === '' || idCause === '';
  };

  useEffect(() => {
    if (mode === 'modification') {
      const fiche = ficheAction || ficheIncident || (ficheAmelioration as any);
      if (fiche) {
        setDescription(fiche.description);
        setIdType(fiche.type?.id);
        setIdCause(fiche.cause?.id);
        setDateFiche(fiche.dateAction || fiche.dateIncident || fiche.dateAmelioration || null);
        if (fiche.fichiers) {
          setSelectedFiles(fiche.fichiers.map((fichier: any) => ({ ...fichier, name: fichier.nomOriginal })));
        }
      }
    } else {
      setDateFiche(new Date());
    }
  }, [mode, loading, ficheIncident, ficheAmelioration, ficheAction]);

  const handleSaveClick = () => {
    if (idOrigine && idUserDeclarant) {
      onRequestSave({
        description,
        idOrigine,
        idUserDeclarant,
        idOrigineAssocie,
        dateFiche,
        idType,
        idCause,
        id: type === 'incident' ? ficheIncident?.id : type === 'amelioration' ? ficheAmelioration?.id : ficheAction?.id,
        fichiers: selectedFiles,
        idImportance,
        idFonction,
        idTache,
        idUserParticipants,
        idUserSuivi,
      });
    }
  };

  const handleDescriptionChange = (content: string) => {
    setDescription(content);
  };

  /*const handleDateEcheanceChange = (date: any) => {
    setDateEcheance(date);
  };*/

  const handleDateFicheChange = (date: any) => {
    setDateFiche(date);
  };

  if (error) {
    return <ErrorPage />;
  }

  if (loading) {
    return <Backdrop />;
  }
  const { isMobile } = useApplicationContext();

  const titleContainerSwitcher = (mode: string, type: string) => {
    switch (type) {
      case 'amelioration':
        return mode === 'modification' ? 'Modification fiche amélioration' : 'Nouvelle fiche amélioration';

      case 'action':
        return mode === 'modification' ? 'Modification action opérationnelle' : 'Nouvelle action opérationnelle';

      case 'incident':
        return mode === 'modification' ? 'Modification incident' : 'Nouvel incident';

      case 'reunion':
        return mode === 'modification' ? 'Modification réunion' : 'Nouvelle réunion';
      default:
        return undefined; //eslint-desable-line
    }
  };

  return (
    <CustomContainer
      filled
      bannerBack
      onBackClick={onRequestGoBack}
      onlyBackAndTitle
      bannerContentStyle={{
        justifyContent: 'center',
      }}
      bannerTitle={titleContainerSwitcher(mode, type)}
      contentStyle={{
        marginTop: 24,
        width: '100%',
        maxWidth: 785,
        padding: isMobile ? 20 : '',
        paddingBottom: isMobile ? 90 : '',
      }}
    >
      <Box width="100%">
        <CssBaseline />
        <Box className={classes.inlineContainer} style={{ marginBottom: '16px' }}>
          {/* {selectAuteurComponent} */}
          <CustomDatePicker
            value={dateFiche}
            label={`Date ${type}`}
            onChange={handleDateFicheChange}
            required={true}
            className={`${classes.heightOfInput}`}
          />
        </Box>
        <Box className={classes.inlineContainer} marginTop="8px">
          <CustomSelect
            disabled={types?.loading}
            error={types?.error}
            listId="id"
            index="libelle"
            label={`Type ${type}`}
            value={idType}
            onChange={(event: any) => setIdType(event.target.value)}
            list={types?.data || []}
            required={true}
            placeholder="Sélectionner le type incident"
          />
          <CustomSelect
            disabled={causes?.loading}
            error={causes?.error}
            listId="id"
            index="libelle"
            label={`Cause ${type}`}
            value={idCause}
            onChange={(event: any) => setIdCause(event.target.value)}
            list={causes?.data || []}
            required={true}
            classes={{ formControl: classes.desktopFlexSpacing }}
            placeholder="Sélectionner la cause incident"
          />
        </Box>
        <Box>
          <Typography variant="body1" className={classes.sectionTitle}>
            {origineSectionTitle}
          </Typography>
          {originInput}
        </Box>
        {/* <Box>
          <Typography variant="body1" className={classes.sectionTitle}>
            {participantsEnChargeSectionTitle}
          </Typography>
          {commonFieldsComponent}
        </Box> */}
        {/*type === 'action' && selectSuiviParComponent*/}
        {/* <Box>
          <Typography variant="body1" className={classes.sectionTitle}>
            {dateEcheanceSectionTitle}
          </Typography>
          <Box marginBottom="16px">
            <CustomDatePicker
              value={dateEcheance}
              label="Date de prise en charge"
              onChange={handleDateEcheanceChange}
              required={true}
            />
          </Box>
        </Box> */}

        <Box>
          {/* <Typography variant="body1" className={classes.sectionTitle}>
            {participantsAInformerSectionTitle}
          </Typography> */}
          {commonFieldsParticipantsAInformerComponent}
        </Box>

        <ReactQuill
          theme="snow"
          placeholder="description"
          className={classes.customReactQuill}
          value={description}
          onChange={handleDescriptionChange}
        />

        <Dropzone multiple setSelectedFiles={setSelectedFiles} selectedFiles={selectedFiles} />
        <FormButtons
          onClickCancel={onRequestGoBack}
          onClickConfirm={handleSaveClick}
          disableConfirm={disabledConfirmButton() || saving}
        />
      </Box>
    </CustomContainer>
  );
};

export default FicheIncidentFormPage;
