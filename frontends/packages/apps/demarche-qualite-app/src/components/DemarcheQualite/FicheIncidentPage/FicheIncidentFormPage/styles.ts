import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customReactQuill: {
      width: '100%',
      outline: 'none',
      marginBottom: 18,
      marginTop: 25,
      border: '1px solid rgba(0, 0, 0, 0.23)',
      borderRadius: 4,
      '& .ql-toolbar.ql-snow': {
        border: '0px',
        borderBottom: '1px solid rgba(0, 0, 0, 0.23)',
      },
      '& .ql-container': {
        minHeight: 200,
        border: '0px',
      },
      '&:hover': {
        borderColor: theme.palette.primary.main,
      },
      '&:focus': {
        outline: `auto ${theme.palette.primary.main}`,
      },
    },
    actionBtns: {
      position: 'sticky',
      bottom: 16,
      display: 'flex',
      width: '100%',
      justifyContent: 'flex-end',
    },
    inlineContainer: {
      [theme.breakpoints.down('xs')]: {
        display: 'block',
        '&>div': {
          marginLeft: 0,
        },
      },
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginBottom: '0 !important',
      '& .MuiFormControl-root:nth-child(2)': {
        marginLeft: 16,
      },
    },
    sectionTitle: {
      fontWeight: 400,
      marginBottom: 10,
    },
    heightOfInput: {
      height: 60,
      '& .main-MuiInputBase-root': {
        height: 48,
      },
    },
    desktopFlexSpacing: {
      marginLeft: 15,
    },
    radioGroup: {
      width: '100%',
      display: 'flex',
    },
    radioGroupItem: {
      display: 'contents'
    },
  })
);

export default useStyles;
