import FicheIncidentPage, { FicheIncidentChange } from './FicheIncidentPage';
import FicheIncidentFormPage ,{ RequestSavingFicheIncident } from './FicheIncidentFormPage/FicheIncidentFormPage';
import DetailsFicheIncident from './DetailsFicheIncident/DetailsFicheIncident';

export { FicheIncidentPage, DetailsFicheIncident, FicheIncidentFormPage , FicheIncidentChange , RequestSavingFicheIncident };
export * from './types';
