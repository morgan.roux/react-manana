import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'start',
      marginBottom: 13,
    },
    labelBox: {
      color: 'gray',
      width: 200,
    },
    valueBox: {
      fontWeight: 'bold',
    },
  }),
);

export default useStyles;
