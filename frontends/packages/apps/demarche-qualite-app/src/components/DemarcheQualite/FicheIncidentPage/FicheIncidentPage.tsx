// import { User } from '@lib/types';
import { CssBaseline, Fade, Hidden, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Add, Delete, Edit, EventNote, MoreHoriz, MoreVert, Visibility, WbIncandescent } from '@material-ui/icons';
import moment from 'moment';
import React, { ChangeEvent, FC, MouseEvent, ReactNode, useEffect, useState } from 'react';
import { isMobile, strippedColumnStyle, strippedString } from '@lib/ui-kit/src/services/Helpers';
import {
  Column,
  CustomAvatarGroup,
  CustomSelect,
  ImportanceLabel,
  Table,
  UrgenceLabel,
  ConfirmDeleteDialog,
  NewCustomButton,
} from '@lib/ui-kit/src/components/atoms';
import SwipeableContainer from '@lib/ui-kit/src/components/molecules/SwipeableContainer/SwipeableContainer';
import { ParameterContainer } from '@lib/ui-kit/src/components/templates/ParameterContainer';
import { Origine } from '../FicheAmeliorationPage';
import MobileMainContent from './MobileMainContent';
import useStyles from './style';
import { FicheIncident, Statut } from './types';
import { importanceToImpact } from '../../../utils/util';

export interface FicheIncidentChange {
  originesFiltersSelected: Origine[];
  statutsFiltersSelected: Statut[];
  hasFicheAmeliorationFilterSelected: 'ALL' | 'NO' | 'YES';
  searchText: string;
  searchTextChanged: boolean;
  skip: number;
  take: number;
}

interface FicheAmeliotationStat {
  id: 'ALL' | 'YES' | 'NO';
  libelle?: string;
  total?: number;
}

interface FicheIncidentPageProps {
  loading?: boolean;
  error?: Error;
  data?: FicheIncident[];
  rowsTotal: number;

  ficheAmeliorationStats: {
    loading?: boolean;
    error?: Error;
    data?: FicheAmeliotationStat[];
  };

  origines: {
    loading?: boolean;
    error?: Error;
    data?: Origine[];
  };

  statuts: {
    loading?: boolean;
    error?: Error;
    data?: Statut[];
  };

  urgenceFilter: ReactNode;
  importanceFilter: ReactNode;
  statutsInput: ReactNode;

  filterIcon: any;

  onRequestStatutChange: (fiche: FicheIncident, value: string) => void;
  onRequestAddFicheAmelioration: (fiche: FicheIncident) => void;
  onRequestGoBack: () => void;
  onRequestAdd: () => void;
  onRequestShowDetails: (fiche: FicheIncident) => void;
  onRequestShowFicheAmeliorationDetails: (fiche: FicheIncident) => void;
  onRequestEdit: (fiche: FicheIncident) => void;
  onRequestDelete: (fiches: FicheIncident[]) => void;
  onRequestSearch: (change: FicheIncidentChange) => void;
  onRequestOpenFormModal: (fiche?: FicheIncident) => void;
  onRequestOpenAmeliorationFormModal: (fiche?: FicheIncident) => void;
  onRequestGoReunion: (action: FicheIncident) => void;
}

const FicheIncidentPage: FC<FicheIncidentPageProps> = ({
  loading,
  error,
  data,
  origines,
  statuts,
  rowsTotal,
  ficheAmeliorationStats,
  onRequestStatutChange,
  onRequestAddFicheAmelioration,
  onRequestGoBack,
  onRequestAdd,
  onRequestShowDetails,
  onRequestShowFicheAmeliorationDetails,
  onRequestEdit,
  onRequestDelete,
  onRequestSearch,
  onRequestOpenFormModal,
  onRequestOpenAmeliorationFormModal,
  onRequestGoReunion,
  urgenceFilter,
  importanceFilter,
  statutsInput,
  filterIcon,
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [ficheIncidentToEdit, setFicheIncidentToEdit] = useState<FicheIncident | undefined>();
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(isMobile() ? 1 : 12);
  const [searchText, setSearchText] = useState<string>('');

  const [originesFiltersSelected, setOriginesFiltersSelected] = useState<Origine[]>([]);
  const [statutsFiltersSelected, setStatutsFiltersSelected] = useState<Statut[]>([]);
  const [ficheAmeliorationFiltersSelected, setFicheAmeliorationsFiltersSelected] = useState<FicheAmeliotationStat[]>([
    { id: 'ALL' },
  ]);

  const [rowsSelected, setRowsSelected] = useState<FicheIncident[]>([]);
  const open = Boolean(anchorEl);

  useEffect(() => {
    if (statuts.data) {
      setStatutsFiltersSelected(statuts.data.filter((element) => element.code !== 'CLOTURE'));
    }
  }, [statuts.loading]);

  useEffect(() => {
    onRequestSearch({
      skip,
      take,
      searchText,
      searchTextChanged: false,
      statutsFiltersSelected,
      originesFiltersSelected,
      hasFicheAmeliorationFilterSelected: ficheAmeliorationFiltersSelected[0].id,
    });
  }, [skip, take, statutsFiltersSelected, originesFiltersSelected, ficheAmeliorationFiltersSelected]);

  useEffect(() => {
    onRequestSearch({
      skip,
      take,
      searchText,
      searchTextChanged: true,
      statutsFiltersSelected,
      originesFiltersSelected,
      hasFicheAmeliorationFilterSelected: ficheAmeliorationFiltersSelected[0].id,
    });
  }, [searchText]);

  const handleShowMenuClick = (fiche: FicheIncident, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    setFicheIncidentToEdit(fiche);
    setAnchorEl(event.currentTarget);
  };

  const handleConfirmDeleteOne = (): void => {
    if (ficheIncidentToEdit) {
      onRequestDelete([ficheIncidentToEdit]);
      setOpenDeleteDialog(false);
    }
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const addFicheAmelioration = (): void => {
    if (!isMobile() && ficheIncidentToEdit) {
      onRequestAddFicheAmelioration(ficheIncidentToEdit);
    } else {
      onRequestOpenAmeliorationFormModal(ficheIncidentToEdit);
    }
  };

  const showFicheAmelioration = (): void => {
    if (ficheIncidentToEdit) {
      onRequestShowFicheAmeliorationDetails(ficheIncidentToEdit);
    }
  };

  const consultFicheIncident = (): void => {
    if (ficheIncidentToEdit) {
      onRequestShowDetails(ficheIncidentToEdit);
    }
  };

  const editFicheIncident = (): void => {
    if (!isMobile() && ficheIncidentToEdit) {
      onRequestEdit(ficheIncidentToEdit);
    } else {
      onRequestOpenFormModal(ficheIncidentToEdit);
    }
  };

  const changeStatut = (event: ChangeEvent<HTMLSelectElement>, fiche: FicheIncident): void => {
    if (fiche) {
      onRequestStatutChange(fiche, (event.target as HTMLSelectElement).value);
    }
  };

  const actionBtn = (row: any, hasFicheAmelioration: any) => {
    if (row) {
      return (
        <>
          <CssBaseline />
          <IconButton
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={handleShowMenuClick.bind(null, row)}
            color="inherit"
            id={`fiche-menu-icon-${row?.id}`}
          >
            {isMobile() ? <MoreVert /> : <MoreHoriz />}
          </IconButton>
          <Menu
            id={`fiche-incident-menu-${row?.id}`}
            anchorEl={anchorEl}
            keepMounted
            open={open && row?.id === ficheIncidentToEdit?.id}
            onClose={handleClose}
            TransitionComponent={Fade}
          >
            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                if (hasFicheAmelioration) {
                  showFicheAmelioration();
                } else {
                  addFicheAmelioration();
                }
              }}
              disabled={false}
            >
              <ListItemIcon>{hasFicheAmelioration ? <Visibility /> : <Add />}</ListItemIcon>
              <Typography variant="inherit">
                {hasFicheAmelioration ? "Consulter la fiche d'amélioration" : " Ajouter une fiche d'amélioration"}
              </Typography>
            </MenuItem>

            <Hidden smDown={true} implementation="css">
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                  handleClose();
                  consultFicheIncident();
                }}
                disabled={false}
              >
                <ListItemIcon>
                  <Visibility />
                </ListItemIcon>
                <Typography variant="inherit">Voir détails</Typography>
              </MenuItem>
            </Hidden>

            <Hidden smDown={true} implementation="css">
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                  handleClose();
                }}
                disabled={false}
              >
                <ListItemIcon>
                  <WbIncandescent />
                </ListItemIcon>
                <Typography variant="inherit">Apporter une solution</Typography>
              </MenuItem>
            </Hidden>

            {/* <Hidden smDown={true} implementation="css">
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                  handleClose();
                  onRequestGoReunion(row);
                }}
                disabled={!row.reunion?.id}
              >
                <ListItemIcon>
                  <EventNote />
                </ListItemIcon>
                <Typography variant="inherit">Consulter la réunion</Typography>
              </MenuItem>
            </Hidden> */}

            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                editFicheIncident();
              }}
              disabled={row.statut?.code === 'CLOTURE'}
            >
              <ListItemIcon>
                <Edit />
              </ListItemIcon>
              <Typography variant="inherit">Modifier</Typography>
            </MenuItem>

            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                setOpenDeleteDialog(true);
              }}
              disabled={row.statut?.code === 'CLOTURE' || row.reunion?.id}
            >
              <ListItemIcon>
                <Delete />
              </ListItemIcon>
              <Typography variant="inherit">Supprimer</Typography>
            </MenuItem>
          </Menu>
        </>
      );
    } else return <></>;
  };

  const findOrigine = (id?: string): any | undefined => {
    if (!id) {
      return undefined;
    }

    return (origines?.data || []).find((o) => o.id === id);
  };

  const columns: Column[] = [
    {
      name: 'type.libelle',
      label: 'Type incident',
      renderer: (row: FicheIncident) => {
        return row.type ? row.type.libelle : '-';
      },
    },
    {
      name: 'cause.libelle',
      label: 'Cause incident',
      renderer: (row: FicheIncident) => {
        return row.cause ? row.cause.libelle : '-';
      },
    },
    {
      name: 'description',
      label: 'Déscription',
      renderer: (row: FicheIncident) => {
        return row.description ? strippedString(row.description) : '-';
      },
      style: strippedColumnStyle,
    },
    {
      name: 'origine',
      label: 'Origine',
      renderer: (row: FicheIncident) => {
        return findOrigine(row?.origine?.id)?.libelle || '-';
      },
    },
    {
      name: 'correspondant',
      label: 'Correspondant',
      renderer: (row: FicheIncident) => {
        if (!row.origineAssocie || !row?.origine?.id) {
          return '-';
        }
        const origine = findOrigine(row.origine.id);
        return origine ? (origine.code === 'INTERNE' ? row.origineAssocie.fullName : row.origineAssocie.nom) : '-';
      },
    },
    {
      name: 'idFicheAmelioration',
      label: "Fiche d'amélioration",
      renderer: (row: any) => {
        return row.idFicheAmelioration || row.ficheAmelioration ? 'Oui' : 'Non';
      },
    },
    {
      name: 'createAt',
      label: 'Date',
      renderer: (row: FicheIncident) => {
        return row.createdAt ? moment(new Date(row.createdAt)).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'dateResolution',
      label: 'Date de résolution',
      renderer: (row: FicheIncident) => {
        return row.solution && row.solution.createdAt
          ? moment(new Date(row.solution.createdAt)).format('DD/MM/YYYY')
          : '-';
      },
    },

    {
      name: 'importance',
      label: 'Impact',
      renderer: (row: FicheIncident) => <ImportanceLabel importance={importanceToImpact(row.importance as any)} />,
    },
    {
      name: 'statut',
      label: 'Statut',
      renderer: (row: FicheIncident) => {
        return (
          <CustomSelect
            key={row.id}
            disabled={statuts.loading || statuts.error}
            list={statuts?.data || []}
            listId="id"
            index="libelle"
            value={row.statut.id}
            variant="standard"
            onChange={(e: any) => changeStatut(e, row)}
          />
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: any) => {
        const hasFicheAmelioration = !!(row.idFicheAmelioration || row.ficheAmelioration);
        return <div key={`row-${row.id}`}>{actionBtn(row, hasFicheAmelioration)}</div>;
      },
    },
  ];

  const [openDrawer, setOpenDrawer] = useState(false);

  const handleDrawerToggle = () => {
    setOpenDrawer(!openDrawer);
  };

  const handleShowFiltersClick = () => {
    setOpenDrawer(!openDrawer);
  };

  const fiche = data && data[0];

  const optionBtn = [
    <IconButton
      className={classes.iconAction}
      color="inherit"
      aria-label="settings"
      edge="start"
      onClick={handleShowFiltersClick}
    >
      {filterIcon}
    </IconButton>,
    actionBtn(fiche, !!fiche?.ficheAmelioration),
  ];

  const handleSwipePrevious = () => {
    if (skip > 0) {
      setSkip(skip - 1);
    }
  };

  const handleSwipeNext = () => {
    if (skip + 1 < rowsTotal) setSkip(skip + 1);
  };

  return (
    <>
      <ParameterContainer
        sideNavProps={{
          title: 'Fiches incidents',
          searchPlaceholder: 'Rechercher ...',
          onRequestGoBack: () => onRequestGoBack(),
          onRequestSearchText: setSearchText,
          dropdownFilters: [
            {
              title: 'Origine',
              filterList: origines,
              dataIdName: 'id',
              dataLabelName: 'libelle',
              dataCountName: 'nombreFicheIncidents',
              filtersSelected: originesFiltersSelected,
              onFiltersSelectionChange: (filters) => setOriginesFiltersSelected(filters as Origine[]),
            },
            {
              title: 'Statut',
              filterList: statuts,
              dataIdName: 'id',
              dataLabelName: 'libelle',
              dataCountName: 'nombreFicheIncidents',
              filtersSelected: statutsFiltersSelected,
              onFiltersSelectionChange: (filters) => setStatutsFiltersSelected(filters as Statut[]),
            },

            {
              title: "Fiche d'amélioration",
              dataIdName: 'id',
              dataLabelName: 'libelle',
              dataCountName: 'total',
              radio: true,
              filterList: ficheAmeliorationStats,
              filtersSelected: ficheAmeliorationFiltersSelected,
              onFiltersSelectionChange: (filters) => {
                setFicheAmeliorationsFiltersSelected(filters as FicheAmeliotationStat[]);
              },
            },
            {
              title: 'Urgence',
              children: urgenceFilter,
            },
            {
              title: 'Importance',
              children: importanceFilter,
            },
          ],
        }}
        topBarProps={{
          title: 'Liste des incidents',
          actionButtons: [
            <NewCustomButton key={`action-button-${0}`} onClick={onRequestAdd} startIcon={<Add />}>
              NOUVEL INCIDENT
            </NewCustomButton>,
          ],
        }}
        mainContent={
          isMobile() ? (
            <SwipeableContainer onNext={handleSwipeNext} onPrevious={handleSwipePrevious}>
              <MobileMainContent fiche={fiche} statutsInput={statutsInput} loading={loading} />
            </SwipeableContainer>
          ) : (
            <div style={{ paddingRight: 25 }}>
              <Table
                style={{ padding: 24 }}
                error={error}
                loading={loading}
                search={false}
                data={data || []}
                selectable={false}
                rowsSelected={rowsSelected}
                onRowsSelectionChange={(newRowsSelected) => setRowsSelected(newRowsSelected as FicheIncident[])}
                columns={columns}
                onRunSearch={({ skip, take }) => {
                  setSkip(skip);
                  setTake(take);
                }}
                rowsTotal={rowsTotal}
              />
            </div>
          )
        }
        optionBtn={[optionBtn]}
        handleDrawerToggle={handleDrawerToggle}
        openDrawer={openDrawer}
        drawerTitle={`Fiche incidents ${rowsTotal !== 0 ? `(${skip + 1}/${rowsTotal})` : ``}`}
        onGoBack={onRequestGoBack}
      />
      <ConfirmDeleteDialog
        title="Suppression de fiche d'incident"
        content="Etes-vous sûr de vouloir supprimer cette fiche ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDeleteOne}
      />
    </>
  );
};

export default FicheIncidentPage;
