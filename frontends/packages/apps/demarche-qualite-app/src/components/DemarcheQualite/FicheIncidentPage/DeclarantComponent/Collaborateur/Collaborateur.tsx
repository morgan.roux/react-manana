import React, { FC } from 'react';
import { Presentation } from '@lib/ui-kit/src/components/atoms/Presentation';
import { Collaborateur as ICollaborateur } from '../../types';

interface CollaborateurProps {
  collaborateur: ICollaborateur;
}

const Collaborateur: FC<CollaborateurProps> = ({ collaborateur }) => {
  return <Presentation presentation={collaborateur} />;
};

export default Collaborateur;
