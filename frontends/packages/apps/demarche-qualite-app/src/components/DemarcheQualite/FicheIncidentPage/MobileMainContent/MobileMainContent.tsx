import { Box, CircularProgress, Typography } from '@material-ui/core';
import { Error } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, ReactNode } from 'react';
import { strippedString } from '@lib/ui-kit/src/services/Helpers';
import AvatarInput from '@lib/ui-kit/src/components/atoms/AvatarInput';
import InfoList, { InfoListItem } from '@lib/ui-kit/src/components/atoms/InfoList/InfoList';
import { FicheIncident } from '../types';
interface MobileMainContentProps {
  loading?: boolean;
  fiche?: FicheIncident;
  statutsInput: ReactNode;
}

const MobileMainContent: FC<MobileMainContentProps> = ({ fiche, statutsInput, loading }) => {
  const ImportanceErrorIcon = (
    <Error
      htmlColor={fiche?.importance?.couleur || 'black'}
      style={{
        marginRight: '13px',
        marginLeft: '10px',
      }}
    />
  );

  const UrgenceErrorIcon = (
    <Error
      htmlColor={fiche?.urgence?.couleur || 'black'}
      style={{
        marginRight: '13px',
        marginLeft: '10px',
      }}
    />
  );

  const infosGenerale: InfoListItem = {
    title: 'Informations générales',
    content: [
      {
        label: 'Type',
        value: fiche?.type?.libelle || '-',
      },
      {
        label: 'Cause',
        value: fiche?.cause?.libelle || '-',
      },
      {
        label: 'Déclarant',
        children: <AvatarInput list={[fiche?.declarant] || []} small={true} standard={true} />,
      },
      {
        label: 'Fonction',
        value: fiche?.fonction?.libelle || '-',
      },
      {
        label: 'Tâche',
        value: fiche?.tache?.libelle || '-',
      },
      {
        label: 'Collaborateurs',
        children: <AvatarInput list={fiche?.participants || []} small={true} standard={true} />,
      },
      {
        label: 'Urgence',
        value: `${fiche?.urgence?.code}:${fiche?.urgence?.libelle}`,
        color: fiche?.urgence?.couleur,
        icon: UrgenceErrorIcon,
      },
      {
        label: 'Impact',
        value: fiche?.importance?.libelle,
        color: fiche?.importance?.couleur,
        icon: ImportanceErrorIcon,
      },
      {
        label: 'Date de création',
        value: (fiche?.createdAt && moment(fiche.createdAt).format('DD/MM/YYYY')) || '-',
      },
      {
        label: 'Statut',
        children: statutsInput,
      },
    ],
  };

  const actionsAmelioration: InfoListItem = {
    title: "Actions d'améliorations",
    content: [
      {
        label: 'Fiche amélioration',
        value: fiche?.ficheAmelioration ? 'Oui' : 'Non',
      },
      {
        label: 'Fiche de réclamation',
        value: 'Non',
      },
    ],
  };

  const details: InfoListItem = {
    title: 'Description',
    children: <Typography>{strippedString(fiche?.description || '')}</Typography>,
  };

  if (loading) {
    return (
      <Box width="100%" height="100%" justifyContent="center" alignItems="center" display="flex">
        <CircularProgress color="secondary" />
      </Box>
    );
  } else if (!fiche) {
    return (
      <Box width="100%" height="100%" justifyContent="center" alignItems="center" display="flex">
        <Typography>Aucun résultat</Typography>
      </Box>
    );
  } else {
    return (
      <Box padding="16px">
        <InfoList info={infosGenerale} />
        <InfoList info={actionsAmelioration} />
        <InfoList info={details} />
      </Box>
    );
  }
};

export default MobileMainContent;
