import { Box, Card, Chip, Divider, TextField } from '@material-ui/core';
import PdfIcon from '@material-ui/icons/PictureAsPdf';
import moment from 'moment';
import React, { ChangeEvent, FC, MouseEvent, useEffect, useState } from 'react';
import { strippedString } from '@lib/ui-kit/src/services/Helpers';
import {
  CustomButton,
  CustomFormTextField,
  CustomModal,
  LoaderSmall,
  NoItemContentImage,
  ConfirmDeleteDialog,
  CustomEditorText,
} from '@lib/ui-kit/src/components/atoms';
import { Backdrop } from '@lib/ui-kit/src/components/atoms/Backdrop';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import DeclarantComponent from '../DeclarantComponent/DeclarantComponent';
import { Collaborateur, FicheIncident, FicheIncidentSolution } from '../types';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import LineDetail from './LineDetail/LineDetail';
import useStyles from './style';

export interface RequestSavingFicheAmeliorationSolution {
  description: string;
  idUser: string;
}

interface DetailsIncidentProps {
  ficheIncident: FicheIncident;
  loading?: boolean;
  error?: Error;
  noContentActionImageSrc: string;
  collaborateurs: Collaborateur[];

  solution?: {
    loading?: boolean;
    saving?: boolean;
    error?: Error;
    errorSaving?: Error;
    data?: FicheIncidentSolution;
  };

  onRequestGoBack: (event: MouseEvent) => void;
  onRequestDelete: (incident: FicheIncident) => void;
  onRequestEdit: (incident: FicheIncident) => void;
  onRequestSaveSolution: (fiche: FicheIncident, tosave: RequestSavingFicheAmeliorationSolution) => void;
}

const DetailsIncident: FC<DetailsIncidentProps> = ({
  ficheIncident,
  noContentActionImageSrc,
  collaborateurs,
  solution,
  error,
  loading,
  onRequestGoBack,
  onRequestDelete,
  onRequestEdit,
  onRequestSaveSolution,
}) => {
  const classes = useStyles();

  const [openCreateSolutionDialog, setOpenCreateSolutionDialog] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const [declarantOpen, setDeclarantOpen] = useState<boolean>(false);

  const [collaborateur, setCollaborateur] = useState<Collaborateur>();
  const [description, setDescription] = useState<string>('');

  useEffect(() => {
    if (solution?.errorSaving) {
      setOpenCreateSolutionDialog(true);
    }
  }, [solution?.errorSaving]);

  const handleConfirmDelete = () => {
    if (ficheIncident) {
      onRequestDelete(ficheIncident);
      setOpenDeleteDialog(false);
    }
  };

  const handleDescriptionChange = (value: string): void => {
    setDescription(value);
  };

  const handleOpenChange = (): void => {
    setOpenCreateSolutionDialog(!openCreateSolutionDialog);
  };

  const openDeclarantDialog = (): void => {
    setDeclarantOpen(!declarantOpen);
  };

  const onCheckRequest = (collaborateur: Collaborateur): void => {
    setDeclarantOpen(false);
    setCollaborateur(collaborateur);
  };

  const addAction = (): void => {
    if (collaborateur) {
      setOpenCreateSolutionDialog(false);
      onRequestSaveSolution(ficheIncident, { idUser: collaborateur.id, description });
    }
  };

  const editIncident = (): void => {
    onRequestEdit(ficheIncident);
  };

  const isEmptyFields = (): boolean => !collaborateur || !description;

  if (error) {
    return <ErrorPage />;
  }

  if (loading || solution?.saving) {
    return <Backdrop />;
  }

  return (
    <>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<span>Êtes-vous sur de vouloir supprimer cette fiche incident ? </span>}
        onClickConfirm={handleConfirmDelete}
      />

      <CustomModal
        open={openCreateSolutionDialog}
        setOpen={setOpenCreateSolutionDialog}
        closeIcon={true}
        headerWithBgColor={true}
        fullWidth={true}
        maxWidth="sm"
        title={'Apporter une solution'}
        withBtnsActions={true}
        onClickConfirm={addAction}
        disabledButton={solution?.saving || isEmptyFields()}
        centerBtns={true}
      >
        <Box className={classes.actionForms}>
          <Box className={classes.actionForm}>
            <CustomFormTextField
              label="Apporté par"
              value={collaborateur?.fullName || ''}
              onClick={openDeclarantDialog}
            />
          </Box>
          <Box className={classes.actionForm}>
            <CustomEditorText
              value={description}
              placeholder="Description"
              onChange={(value) => handleDescriptionChange(value)}
              className={classes.actionFormDescription}
            />
          </Box>
        </Box>
      </CustomModal>
      <CustomModal
        title="Déclarant"
        open={declarantOpen}
        setOpen={setDeclarantOpen}
        closeIcon={true}
        withBtnsActions={true}
        centerBtns={true}
      >
        <DeclarantComponent data={collaborateurs || []} onCheckRequest={onCheckRequest} />
      </CustomModal>

      <CustomContainer
        bannerBack={true}
        filled={true}
        bannerTitle="Description de l'incident"
        onBackClick={onRequestGoBack}
        bannerContentStyle={{
          justifyContent: 'unset',
        }}
        bannerTitleStyle={{
          textAlign: 'center',
          flexGrow: 1,
        }}
      >
        <Box className={classes.detailsContent}>
          <Card className={classes.infoBox}>
            <Box>
              <span className={classes.span}>Informations générales</span>
              <LineDetail
                label="Auteur"
                value={
                  ficheIncident.declarant && ficheIncident.declarant.fullName ? ficheIncident.declarant.fullName : '-'
                }
              />
              <LineDetail
                label="Origine"
                value={ficheIncident.origine && ficheIncident.origine.libelle ? ficheIncident.origine.libelle : '-'}
              />
              <LineDetail
                label="Date de création"
                value={ficheIncident.createdAt ? moment(new Date(ficheIncident.createdAt)).format('DD/MM/YYYY') : '-'}
              />
              <LineDetail
                label="Statut"
                value={ficheIncident.statut && ficheIncident.statut.libelle ? ficheIncident.statut.libelle : '-'}
              />
            </Box>
            {ficheIncident?.fichiers && (
              <Box style={{ marginBottom: 20 }}>
                <span className={classes.fichierJointLabel}>Fichier(s) joint(s)</span>
                <Box>
                  {ficheIncident?.fichiers.map((fiche) => {
                    return (
                      <Chip
                        className={classes.ficheBox}
                        key={fiche.id}
                        icon={<PdfIcon />}
                        clickable={true}
                        label={fiche.nomOriginal}
                        component="a"
                        target="_blank"
                        href={fiche.publicUrl}
                      />
                    );
                  })}
                </Box>
              </Box>
            )}

            <Divider />
            <Box>
              <span className={classes.span}>Actions d'amélioration</span>
              <LineDetail label="Fiche amélioration" value={ficheIncident.ficheAmelioration ? 'Oui' : 'Non'} />
              <LineDetail label="Fiche de réclamation" value={ficheIncident.idTicketReclamation ? 'Oui' : 'Non'} />
            </Box>
            <Divider />
            <Box style={{ marginBottom: 12 }}>
              <span className={classes.span}>Description</span>
              <TextField
                className={classes.descriptionField}
                fullWidth={true}
                multiline={true}
                disabled={true}
                variant="outlined"
                label="Description"
                value={strippedString(ficheIncident.description || '')}
              />
            </Box>
            <Box className={classes.buttonContainer}>
              <CustomButton
                className={classes.buttonContainerButton}
                onClick={() => setOpenDeleteDialog(true)}
                disabled={!!ficheIncident.reunion?.id}
              >
                Supprimer
              </CustomButton>
              <CustomButton className={classes.buttonContainerButton} color="secondary" onClick={editIncident}>
                Modifier
              </CustomButton>
            </Box>
          </Card>
          <Card className={solution?.data ? classes.actionBox : classes.actionBoxBorder}>
            {solution?.loading ? (
              <LoaderSmall />
            ) : solution?.error ? (
              <ErrorPage />
            ) : solution?.data ? (
              <Box>
                <span className={classes.span}>Solution apportée par</span>
                <Divider className={classes.marginDevider} />
                <Box>
                  <LineDetail label="Nom et prénom(s)" value={solution.data.user.fullName || '-'} />
                  <LineDetail
                    label="Date"
                    value={solution.data.createdAt ? moment(solution.data.createdAt).format('DD/MM/YYYY') : '-'}
                  />
                  <TextField
                    className={(classes.descriptionField, classes.bgDecriptionField)}
                    fullWidth={true}
                    multiline={true}
                    disabled={true}
                    variant="outlined"
                    label="Description"
                    value={strippedString(solution.data.description || '')}
                  />
                </Box>
              </Box>
            ) : (
              <NoItemContentImage
                src={noContentActionImageSrc}
                title="Ajout de solution"
                subtitle="Vous pouvez apporter une solution à tout moment pour clôturer l'incident."
              >
                <CustomButton color="secondary" onClick={handleOpenChange}>
                  Apporter une solution
                </CustomButton>
              </NoItemContentImage>
            )}
          </Card>
        </Box>
      </CustomContainer>
    </>
  );
};

export default DetailsIncident;
