import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import useStyles from './style';

interface LineDetailProps {
  label: string;
  value: string;
}

const LineDetail: FC<LineDetailProps> = ({ label, value }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.labelBox}>{label}</Box>
      <Box className={classes.valueBox}>{value}</Box>
    </Box>
  );
};

export default LineDetail;
