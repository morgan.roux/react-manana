import { DQMTFonction, DQMTTache, Fichier, Importance, Urgence, User } from '@lib/types';
import { Reunion } from '../CompteRenduFormPage/types';
import { Activite, FicheAmelioration, FicheCause, FicheType, Origine } from '../FicheAmeliorationPage/types';

export interface PersonnelService {
  id: string;
  nom: string;
}

export interface Statut {
  id: string;
  libelle: string;
  type?: 'INCIDENT' | 'AMELIORATION' | 'PROMOTION';
  code?: string;
}

export interface Motif {
  id: string;
  libelle: string;
}

export interface FicheIncidentSolution {
  id: string;
  description: string;
  createdAt: string;
  updatedAt: string;
  user: User;
}

export interface Tache {
  id: string;
}

export interface Fonction {
  id: string;
}

export interface FicheIncident {
  id: string;
  description: string;
  idTicketReclamation: string;
  idSolution: string;
  idOrigineAssocie: string;
  origineAssocie?: any;
  dateIncident: string;
  createdAt: string;
  updatedAt: string;
  solution?: FicheIncidentSolution;
  ficheAmelioration?: FicheAmelioration;
  type: FicheType;
  cause: FicheCause;
  urgence: Urgence;
  importance: Importance;
  activite: Activite;
  tache: DQMTTache;
  fonction: DQMTFonction;
  origine: Origine;
  statut: Statut;
  declarant: User;
  fichiers?: Fichier[];
  participants?: User[];
  participantsAInformer?: User[];
  reunion?: Reunion;
  isPrivate: boolean;
}

export interface Collaborateur {
  id: string;
  type: string;
  fullName: string;
  photo: Fichier;
}
