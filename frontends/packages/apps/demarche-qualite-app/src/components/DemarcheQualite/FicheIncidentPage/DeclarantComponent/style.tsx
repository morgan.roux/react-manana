import { createStyles, makeStyles } from "@material-ui/styles";

const useStyles = makeStyles( () => 
    createStyles({
        root : {
            display : 'flex',
            flexDirection : 'column',
            width : '100%',
            padding : 10
        },

        formsContent : {
            display : 'flex',
            flexDirection : 'row',
            justifyContent : 'space-around',
            width : '100%',
        },

        searchInputContent : {
            width : '100%'
        },


        noValue : {
            width : '60%',
            margin : '0 auto'
        },

        dataContent : {
            display : 'flex',
            flexDirection : 'row',
            justifyContent : 'space-between'
        }
    })
)

export default useStyles;