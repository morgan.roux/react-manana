import { Box, Card, Chip, Divider, TextField } from '@material-ui/core';
import PdfIcon from '@material-ui/icons/PictureAsPdf';
import React, { FC, MouseEvent, useState } from 'react';
import { strippedString } from '@lib/ui-kit/src/services/Helpers';
import { ConfirmDeleteDialog, NewCustomButton } from '@lib/ui-kit/src/components/atoms';
import { Backdrop } from '@lib/ui-kit/src/components/atoms/Backdrop';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import { ActionOperationnelle } from '../types';
import LineDetail from './LineDetail/LineDetail';
import useStyles from './style';

interface DetailsActionOperationnelleProps {
  action: ActionOperationnelle;
  loading?: boolean;
  error?: Error;

  onRequestGoBack: (event: MouseEvent) => void;
  onRequestDelete: (action: ActionOperationnelle) => void;
  onRequestEdit: (action: ActionOperationnelle) => void;
}

const DetailsActionOperationnelle: FC<DetailsActionOperationnelleProps> = ({
  action,
  error,
  loading,
  onRequestGoBack,
  onRequestDelete,
  onRequestEdit,
}) => {
  const classes = useStyles();

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const handleConfirmDelete = () => {
    if (action) {
      onRequestDelete(action);
      setOpenDeleteDialog(false);
    }
  };

  const editActionOperationnelle = (): void => {
    onRequestEdit(action);
  };

  if (error) {
    return <ErrorPage />;
  }

  if (loading) {
    return <Backdrop />;
  }

  return (
    <>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<span>Êtes-vous sur de vouloir supprimer cette action ? </span>}
        onClickConfirm={handleConfirmDelete}
      />
      <CustomContainer
        bannerBack={true}
        filled={true}
        bannerTitle="Détails d'une action"
        onBackClick={onRequestGoBack}
        bannerContentStyle={{
          justifyContent: 'unset',
        }}
        bannerTitleStyle={{
          textAlign: 'center',
          flexGrow: 1,
        }}
      >
        <Box className={classes.detailsContent}>
          <Card className={classes.infoBox}>
            <Box>
              <span className={classes.span}>Informations générales</span>
              <LineDetail label="Auteur" value={action.auteur?.fullName || '-'} />
              <LineDetail label="Suivi par" value={action.suiviPar?.fullName || '-'} />
            </Box>
            {action?.fichiers && (
              <Box style={{ marginBottom: 20 }}>
                <span className={classes.fichierJointLabel}>Fichier(s) joint(s)</span>
                <Box>
                  {action?.fichiers.map((fiche) => {
                    return (
                      <Chip
                        className={classes.ficheBox}
                        key={fiche.id}
                        icon={<PdfIcon />}
                        clickable={true}
                        label={fiche.nomOriginal}
                        component="a"
                        target="_blank"
                        href={fiche.publicUrl}
                      />
                    );
                  })}
                </Box>
              </Box>
            )}
            <Divider />
            <Box style={{ marginBottom: 12 }}>
              <span className={classes.span}>Description</span>
              <TextField
                className={classes.descriptionField}
                fullWidth={true}
                multiline={true}
                disabled={true}
                variant="outlined"
                label="Description"
                value={strippedString(action.description || '')}
              />
            </Box>
            <Box className={classes.buttonContainer}>
              <NewCustomButton
                className={classes.buttonContainerButton}
                onClick={() => setOpenDeleteDialog(true)}
                disabled={!!action.reunion?.id}
              >
                Supprimer
              </NewCustomButton>
              <NewCustomButton className={classes.buttonContainerButton} onClick={editActionOperationnelle}>
                Modifier
              </NewCustomButton>
            </Box>
          </Card>
        </Box>
      </CustomContainer>
    </>
  );
};

export default DetailsActionOperationnelle;
