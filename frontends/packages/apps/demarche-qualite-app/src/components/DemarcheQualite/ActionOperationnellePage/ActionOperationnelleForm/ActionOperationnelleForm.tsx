import React, { FC, MouseEvent, useState, useEffect } from 'react';
import { CustomSelect, CustomFormTextField, NewCustomButton } from '@lib/ui-kit';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import { CustomFormSection } from '@lib/ui-kit/src/components/templates/CustomFormSection';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import { Backdrop } from '@lib/ui-kit/src/components/atoms/Backdrop';
import { CustomDatePicker } from '@lib/ui-kit/src/components/atoms/CustomDateTimePicker';
import { ActionOperationnelle } from '../types';
import { Dropzone } from '@lib/common';
import { User } from '@lib/ui-kit/src/components/pages/DashboardPage';

export interface RequestSavingActionOperationnelle {
  id?: string;
  description: string;
  idUserAuteur: string;
  idUserSuivi: string;
  priorite: number;
  dateHeureAction: Date;
  fichiers: File[];
}

interface ActionFormPageProps {
  mode: 'creation' | 'modification';
  action?: ActionOperationnelle;
  loading?: boolean;
  saving?: boolean;
  error?: Error;
  collaborateurs?: {
    loading?: boolean;
    error?: Error;
    data?: User[];
  };

  onRequestGoBack: (event: MouseEvent) => void;
  onRequestSave: (toSave: RequestSavingActionOperationnelle) => void;
}

const ActionOperationnelleFormPage: FC<ActionFormPageProps> = ({
  mode,
  loading,
  error,
  saving,
  action,
  collaborateurs,
  onRequestGoBack,
  onRequestSave,
}) => {
  const [description, setDescription] = useState<string>('');
  const [priorite, setPriorite] = useState<string>('');
  const [idUserAuteur, setIdUserAuteur] = useState<string>('');
  const [idUserSuivi, setIdUserSuivi] = useState<string>('');
  const [dateHeureAction, setDateHeureAction] = useState<any | null>(null);
  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);

  const isActivedAddButton = (): boolean => {
    return priorite === '' || idUserAuteur === '' || idUserSuivi === '';
  };

  useEffect(() => {
    if (action) {
      setDescription(action.description);
      setIdUserAuteur(action.auteur?.id || '');
      setIdUserSuivi(action.suiviPar?.id || '');

      if (action.fichiers) {
        setSelectedFiles(
          action.fichiers.map((fichier: any) => ({
            ...fichier,
            name: fichier.nomOriginal,
          }))
        );
      }
    }
  }, [loading, action]);

  const handleSaveClick = () => {
    if (priorite && idUserAuteur && idUserSuivi) {
      const data = {
        description,
        idUserAuteur,
        idUserSuivi,
        priorite: parseInt(`${priorite}`, 10),
        dateHeureAction,
      };
      const dataToSave =
        mode === 'creation'
          ? {
              ...data,
              fichiers: selectedFiles,
            }
          : {
              id: action?.id,
              ...data,
              fichiers: selectedFiles,
            };
      onRequestSave(dataToSave as any);
    }
  };

  if (error) {
    return <ErrorPage />;
  }

  if (loading) {
    return <Backdrop />;
  }

  return (
    <CustomContainer
      filled
      bannerBack
      bannerTitle={`${mode === 'modification' ? 'Modification' : 'Ajout'} d'action`}
      onBackClick={onRequestGoBack}
      bannerRight={
        <NewCustomButton disabled={isActivedAddButton() || saving} onClick={handleSaveClick}>
          {`${mode === 'modification' ? 'Enregistrer' : 'Créer'}`}
        </NewCustomButton>
      }
      contentStyle={{
        marginTop: 24,
        width: '100%',
        maxWidth: 785,
      }}
    >
      <CustomFormSection title="Informations générales" bordered={false}>
        <Dropzone multiple selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles} />
        <CustomSelect
          disabled={collaborateurs?.loading}
          error={collaborateurs?.error}
          listId="id"
          index="fullName"
          label="Auteur"
          value={idUserAuteur}
          onChange={(event: any) => setIdUserAuteur(event.target.value)}
          list={collaborateurs?.data || []}
          required
        />
        <CustomSelect
          disabled={collaborateurs?.loading}
          error={collaborateurs?.error}
          listId="id"
          index="fullName"
          label="Suivi Par"
          value={idUserSuivi}
          onChange={(event: any) => setIdUserSuivi(event.target.value)}
          list={collaborateurs?.data || []}
          required
        />
        <CustomSelect
          listId="value"
          index="libelle"
          label="Priorité"
          value={priorite}
          onChange={(event: any) => setPriorite(event.target.value)}
          list={[
            { value: 1, libelle: 'Priorité 1' },
            { value: 2, libelle: 'Priorité 2' },
            { value: 3, libelle: 'Priorité 3' },
            { value: 4, libelle: 'Priorité 4' },
          ]}
          required
        />
        <CustomFormTextField
          multiline
          fullWidth
          variant="outlined"
          label="Détails"
          value={description}
          onChange={(event) => setDescription(event.target.value)}
          required
        />
        <CustomDatePicker
          label="Date d'action"
          value={dateHeureAction}
          onChange={(value) => setDateHeureAction(value)}
        />
      </CustomFormSection>
    </CustomContainer>
  );
};

export default ActionOperationnelleFormPage;
