import { CssBaseline, Fade, Hidden, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Add, Delete, Edit, EventNote, MoreHoriz, MoreVert, Visibility, WbIncandescent } from '@material-ui/icons';
import React, { ChangeEvent, FC, MouseEvent, ReactNode, useEffect, useState } from 'react';
import { Origine } from '..';
import SwipeableContainer from '@lib/ui-kit/src/components/molecules/SwipeableContainer';
import { ParameterContainer } from '@lib/ui-kit/src/components/templates/ParameterContainer';
import { isMobile, strippedColumnStyle, strippedString } from '@lib/ui-kit/src/services/Helpers';
import {
  Column,
  CustomAvatarGroup,
  CustomButton,
  CustomSelect,
  ImportanceLabel,
  Table,
  UrgenceLabel,
  ConfirmDeleteDialog,
  NewCustomButton,
} from '@lib/ui-kit/src/components/atoms';
import { CustomDatePicker } from '@lib/ui-kit/src/components/atoms/CustomDateTimePicker';
import { Statut } from '../FicheIncidentPage/types';
import MobileMainContent from './MobileMainContent';
import useStyles from './style';
import { ActionOperationnelle } from './types';
import { importanceToImpact } from '@lib/common';

export interface ActionOperationnelleChange {
  dateCreationFiltersSelected?: any;
  idImportance?: string;
  idStatut?: string;
  searchText?: string;
  searchTextChanged?: boolean;
  skip?: number;
  take?: number;
}

export interface ActionOperationnellePageProps {
  loading?: boolean;
  error?: Error;
  data?: ActionOperationnelle[];
  rowsTotal: number;

  origines: {
    loading?: boolean;
    error?: Error;
    data?: Origine[];
  };

  statuts: {
    loading?: boolean;
    error?: Error;
    data?: Statut[];
  };

  urgenceFilter: ReactNode;
  importanceFilter: ReactNode;
  statutsInput: ReactNode;

  filterIcon: any;

  onRequestGoBack: () => void;
  onRequestAdd: () => void;
  onRequestShowDetail: (action: ActionOperationnelle) => void;
  onRequestEdit: (action: ActionOperationnelle) => void;
  onRequestStatutChange: (fiche: ActionOperationnelle, value: string) => void;
  onRequestDelete: (actions: ActionOperationnelle[]) => void;
  onRequestSearch: (change: ActionOperationnelleChange) => void;
  onRequestOpenFormModal: (fiche?: ActionOperationnelle) => void;
  onRequestGoReunion: (action: ActionOperationnelle) => void;
}

const ActionOperationnellePage: FC<ActionOperationnellePageProps> = ({
  loading,
  error,
  data,
  rowsTotal,
  statuts,
  origines,
  onRequestStatutChange,
  onRequestAdd,
  onRequestDelete,
  onRequestEdit,
  onRequestGoBack,
  onRequestSearch,
  onRequestShowDetail,
  onRequestOpenFormModal,
  onRequestGoReunion,
  urgenceFilter,
  importanceFilter,
  statutsInput,
  filterIcon,
}) => {
  const classes = useStyles();
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(12);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [actionToEdit, setActionToEdit] = useState<ActionOperationnelle | undefined>();
  const [searchText, setSearchText] = useState<string>('');
  const [dateCreationFiltersSelected, setDateCreationFiltersSelected] = useState<any | null>(null);
  const [rowsSelected, setRowsSelected] = useState<ActionOperationnelle[]>([]);
  const [originesFiltersSelected, setOriginesFiltersSelected] = useState<Origine[]>([]);
  const [statutsFiltersSelected, setStatutsFiltersSelected] = useState<Statut[]>([]);

  const open = Boolean(anchorEl);

  useEffect(() => {
    if (statuts.data) {
      setStatutsFiltersSelected(statuts.data.filter((element) => element.code !== 'CLOTURE'));
    }
  }, [statuts.loading]);

  useEffect(() => {
    onRequestSearch({
      skip,
      take,
      searchText,
      searchTextChanged: false,
      idStatut: statutsFiltersSelected,
      dateCreationFiltersSelected,
      idImportance: originesFiltersSelected,
    });
  }, [skip, take, originesFiltersSelected, dateCreationFiltersSelected, statutsFiltersSelected]);

  useEffect(() => {
    onRequestSearch({
      skip,
      take,
      searchText,
      searchTextChanged: true,
      statutsFiltersSelected,
      originesFiltersSelected,
      dateCreationFiltersSelected,
    });
  }, [searchText]);

  const handleShowMenuClick = (reunion: ActionOperationnelle, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    setActionToEdit(reunion);
    setAnchorEl(event.currentTarget);
  };

  const handleConfirmDeleteOne = (): void => {
    if (actionToEdit) {
      onRequestDelete([actionToEdit]);
      setOpenDeleteDialog(false);
    }
  };

  const changeStatut = (event: ChangeEvent<HTMLSelectElement>, fiche: ActionOperationnelle): void => {
    if (fiche) {
      onRequestStatutChange(fiche, (event.target as HTMLSelectElement).value);
    }
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const consultAction = (): void => {
    if (actionToEdit) {
      onRequestShowDetail(actionToEdit);
    }
  };

  const editAction = (): void => {
    if (!isMobile() && actionToEdit) {
      onRequestEdit(actionToEdit);
    } else {
      onRequestOpenFormModal(actionToEdit);
    }
  };

  const actionBtn = (row: any) => {
    if (row) {
      return (
        <>
          <CssBaseline />
          <IconButton
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={handleShowMenuClick.bind(null, row)}
            color="inherit"
            id={`fiche-menu-icon-${row?.id}`}
          >
            {isMobile() ? <MoreVert /> : <MoreHoriz />}
          </IconButton>
          <Menu
            id={`fiche-incident-menu-${row?.id}`}
            anchorEl={anchorEl}
            keepMounted
            open={open && row?.id === actionToEdit?.id}
            onClose={handleClose}
            TransitionComponent={Fade}
          >
            <Hidden mdUp={true} implementation="css">
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                  handleClose();
                }}
                disabled={false}
              >
                <ListItemIcon>
                  <WbIncandescent />
                </ListItemIcon>
                <Typography variant="inherit">Apporter une solution</Typography>
              </MenuItem>
            </Hidden>
            <Hidden smDown={true} implementation="css">
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                  handleClose();
                  consultAction();
                }}
                disabled={false}
              >
                <ListItemIcon>
                  <Visibility />
                </ListItemIcon>
                <Typography variant="inherit">Voir détails</Typography>
              </MenuItem>
            </Hidden>

            <Hidden smDown={true} implementation="css">
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                  handleClose();
                  onRequestGoReunion(row);
                }}
                disabled={!row.reunion?.id}
              >
                <ListItemIcon>
                  <EventNote />
                </ListItemIcon>
                <Typography variant="inherit">Consulter la réunion</Typography>
              </MenuItem>
            </Hidden>

            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                editAction();
              }}
              disabled={row.statut?.code === 'CLOTURE'}
            >
              <ListItemIcon>
                <Edit />
              </ListItemIcon>
              <Typography variant="inherit">Modifier</Typography>
            </MenuItem>

            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                setOpenDeleteDialog(true);
              }}
              disabled={row.statut?.code === 'CLOTURE' || row.reunion?.id}
            >
              <ListItemIcon>
                <Delete />
              </ListItemIcon>
              <Typography variant="inherit">Supprimer</Typography>
            </MenuItem>
          </Menu>
        </>
      );
    } else return <></>;
  };

  const findOrigine = (id?: string): any | undefined => {
    if (!id) {
      return undefined;
    }

    return (origines?.data || []).find((o) => o.id === id);
  };

  const columns: Column[] = [
    {
      name: 'type.libelle',
      label: 'Type',
      renderer: (row: ActionOperationnelle) => {
        return row.type ? row.type.libelle : '-';
      },
    },
    {
      name: 'cause.libelle',
      label: 'Cause',
      renderer: (row: ActionOperationnelle) => {
        return row.cause ? row.cause.libelle : '-';
      },
    },
    {
      name: 'description',
      label: 'Description',
      renderer: (row: ActionOperationnelle) => {
        return row.description ? strippedString(row.description) : '-';
      },
      style: strippedColumnStyle,
    },

    {
      name: 'origine',
      label: 'Origine',
      renderer: (row: ActionOperationnelle) => {
        return findOrigine(row?.origine?.id)?.libelle || '-';
      },
    },

    {
      name: 'correspondant',
      label: 'Correspondant',
      renderer: (row: ActionOperationnelle) => {
        if (!row.origineAssocie || !row?.origine?.id) {
          return '-';
        }
        const origine = findOrigine(row.origine.id);
        return origine ? (origine.code === 'INTERNE' ? row.origineAssocie.fullName : row.origineAssocie.nom) : '-';
      },
    },

    {
      name: 'auteur',
      label: 'Auteur',
      renderer: (row: ActionOperationnelle) => {
        return row?.auteur?.fullName ? row.auteur.fullName : '-';
      },
    },
    {
      name: 'suiviPar',
      label: 'Suivi Par',
      renderer: (row: ActionOperationnelle) => {
        return row?.suiviPar?.fullName ? row.suiviPar.fullName : '-';
      },
    },
    {
      name: 'participants',
      label: 'Collaborateurs concernés',
      renderer: (row: ActionOperationnelle) => (
        <CustomAvatarGroup max={3} sizes="30px" users={(row.participants || []) as any} />
      ),
    },
    {
      name: 'participantsAInformer',
      label: 'Collaborateurs à informer',
      renderer: (row: ActionOperationnelle) => (
        <CustomAvatarGroup max={3} sizes="30px" users={(row.participantsAInformer || []) as any} />
      ),
    },
    {
      name: 'urgence',
      label: 'Urgence',
      renderer: (row: ActionOperationnelle) => <UrgenceLabel urgence={row.urgence} />,
    },

    {
      name: 'importance',
      label: 'Impact',
      renderer: (row: ActionOperationnelle) => (
        <ImportanceLabel importance={importanceToImpact(row.importance as any) as any} />
      ),
    },
    {
      name: 'statut',
      label: 'Statut',
      renderer: (row: ActionOperationnelle) => {
        return (
          <CustomSelect
            key={row.id}
            disabled={statuts.loading || statuts.error}
            list={statuts?.data || []}
            listId="id"
            index="libelle"
            value={row?.statut?.id}
            variant="standard"
            onChange={(e: any) => changeStatut(e, row)}
          />
        );
      },
    },

    {
      name: '',
      label: '',
      renderer: (row: ActionOperationnelle) => {
        return <div key={`row-${row.id}`}>{actionBtn(row)}</div>;
      },
    },
  ];

  const [openDrawer, setOpenDrawer] = useState(false);

  const handleDrawerToggle = () => {
    setOpenDrawer(!openDrawer);
  };

  const handleShowFiltersClick = () => {
    setOpenDrawer(!openDrawer);
  };

  const fiche = data && data[0];

  const optionBtn = [
    <IconButton
      className={classes.iconAction}
      color="inherit"
      aria-label="settings"
      edge="start"
      onClick={handleShowFiltersClick}
    >
      {filterIcon}
    </IconButton>,
    actionBtn(fiche),
  ];

  const handleSwipePrevious = () => {
    if (skip > 0) {
      setSkip(skip - 1);
    }
  };

  const handleSwipeNext = () => {
    if (skip + 1 < rowsTotal) setSkip(skip + 1);
  };

  return (
    <>
      <ParameterContainer
        sideNavProps={{
          title: 'Action',
          searchPlaceholder: 'Rechercher ...',
          onRequestGoBack: () => onRequestGoBack(),
          onRequestSearchText: setSearchText,
          dropdownFilters: [
            {
              title: 'Origine',
              filterList: origines,
              dataIdName: 'id',
              dataLabelName: 'libelle',
              dataCountName: 'nombreFicheAmeliorations',
              filtersSelected: originesFiltersSelected,
              onFiltersSelectionChange: (filters) => setOriginesFiltersSelected(filters as Origine[]),
            },
            {
              title: 'Statut',
              filterList: statuts,
              dataIdName: 'id',
              dataLabelName: 'libelle',
              dataCountName: 'nombreActionOperationnelles',
              filtersSelected: statutsFiltersSelected,
              onFiltersSelectionChange: (filters) => setStatutsFiltersSelected(filters as Statut[]),
            },

            {
              title: "Date d'action",
              children: (
                <CustomDatePicker
                  placeholder="Date"
                  onChange={(value: any) => setDateCreationFiltersSelected(value)}
                  name="date"
                  value={dateCreationFiltersSelected}
                  disablePast={false}
                />
              ),
            },
            {
              title: 'Urgence',
              children: urgenceFilter,
            },
            {
              title: 'Importance',
              children: importanceFilter,
            },
          ],
        }}
        topBarProps={{
          title: 'Liste des actions',
          actionButtons: [
            <NewCustomButton key={`action-button-${0}`} onClick={onRequestAdd} startIcon={<Add />}>
              AJOUTER UNE ACTION
            </NewCustomButton>,
          ],
        }}
        mainContent={
          isMobile() ? (
            <SwipeableContainer onNext={handleSwipeNext} onPrevious={handleSwipePrevious}>
              <MobileMainContent fiche={fiche} statutsInput={statutsInput} loading={loading} />
            </SwipeableContainer>
          ) : (
            <Table
              style={{ padding: 24 }}
              error={error}
              loading={loading}
              search={false}
              data={data || []}
              selectable={false}
              rowsSelected={rowsSelected}
              onRowsSelectionChange={(newRowsSelected) => setRowsSelected(newRowsSelected as ActionOperationnelle[])}
              columns={columns}
              onRunSearch={({ skip, take }) => {
                setSkip(skip);
                setTake(take);
              }}
              rowsTotal={rowsTotal}
            />
          )
        }
        optionBtn={[optionBtn]}
        handleDrawerToggle={handleDrawerToggle}
        openDrawer={openDrawer}
        drawerTitle={`Actions opérationnelles ${rowsTotal !== 0 ? `(${skip + 1}/${rowsTotal})` : ``}`}
        onGoBack={onRequestGoBack}
      />
      <ConfirmDeleteDialog
        title="Suppression d'action"
        content="Etes-vous sûr de vouloir supprimer cette action ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDeleteOne}
      />
    </>
  );
};

export default ActionOperationnellePage;
