import { DQMTFonction, DQMTTache, Fichier, Importance, Urgence, User } from '@lib/types';
import { Reunion } from '../CompteRenduFormPage/types';
import { FicheCause, FicheType, Origine } from '../FicheAmeliorationPage/types';
import { Statut } from '../FicheIncidentPage/types';

export interface ActionOperationnelle {
  id?: string;
  auteur?: {
    id: string;
    fullName: string;
  };
  suiviPar?: {
    id: string;
    fullName: string;
  };
  isPrivate: boolean;
  dateAction: string;
  idOrigineAssocie: string;
  origineAssocie?: any;
  type: FicheType;
  cause: FicheCause;
  statut: Statut;
  description: string;
  urgence: Urgence;
  importance: Importance;
  tache: DQMTTache;
  fonction: DQMTFonction;
  origine: Origine;
  declarant: User;
  fichiers?: Fichier[];
  participants?: User[];
  participantsAInformer?: User[];
  createdAt?: string;
  reunion?: Reunion;
}
