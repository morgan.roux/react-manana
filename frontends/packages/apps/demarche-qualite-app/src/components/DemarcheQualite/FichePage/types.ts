import { FichierInfoFragment } from '@lib/common/src/federation';

export interface Outil {
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  fichier: FichierInfoFragment;
  createdAt: string;
  updatedAt: string;
}
