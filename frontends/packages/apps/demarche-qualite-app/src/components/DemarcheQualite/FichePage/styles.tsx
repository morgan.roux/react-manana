import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) =>
  createStyles({
    root: {
      display: 'flex',
      width: '100%',
      height: '100%',
    },
    boxLeft: {
      flex: '0 0 330px',
      padding: theme.spacing(0, 2),
      borderRight: '1px solid #E6E6E6',
    },
    scrollList: {
      overflowY: 'auto',
      height: 'calc(100vh - 194px)',
    },
    boxRight: {
      width: '100%',
    },
    checklistToDisp: {
      overflowY: 'hidden',
    },
    textItemcheckistToDisp: {
      alignSelf: 'baseline',
    },
    rightContent: {
      display: 'flex',
      flexDirection: 'column',
      padding: 10,
    },
    title: {
      marginTop: 10,
      marginBottom: 25,
      fontWeight: 'bold',
    },
    formsContent: {
      display: 'flex',
      flexDirection: 'column',
    },
    formContent: {
      marginBottom: 15,
    },
    radioContainer: {
      marginTop: 25,
    },
    triageTitle: {
      marginBottom: 15,
      fontWeight: 'bold',
    },
  })
);

export default useStyles;
