import { Box, Button, FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import { Add, Delete, Edit } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, useState, ReactNode, ChangeEvent, useEffect } from 'react';
import { CustomModal, Loader, CardTheme } from '@lib/ui-kit/src/components/atoms';
import { filtered } from '@lib/ui-kit/src/components/atoms/Table/util';
import { ItemContainer } from '@lib/ui-kit/src/components/templates/ItemContainer';
import { SearchBanner } from '../ChecklistEvaluationPage/SearchBanner';
import useStyles from './styles';
import { Outil } from './types';
import { OutilForms } from '@lib/ui-kit/src/components/molecules/OutilForms';
//FIXME : Move to demarche qualite app
import { FileViewer } from '@lib/common';

export interface Contenu {
  title: string;
  subtitle: string;
}

export interface NoContenu {
  list: Contenu;
  content: Contenu;
}

export const end = (typologie: string): string => {
  let end = '';
  if (typologie === 'affiche' || typologie === 'procedure') end = 'e';

  return end;
};

export const noContentValues = (typologie: string): NoContenu => {
  const terminaison: string = end(typologie);

  return {
    list: {
      title: `Aucun${terminaison} ${typologie} dans la liste.`,
      subtitle: `Créez-en un${terminaison} en cliquant sur le bouton d'en haut`,
    },

    content: {
      title: `Aperçu détaillé des ${typologie}s`,
      subtitle: `Choisissez un${terminaison} ${typologie} dans la liste de gauche pour voir son contenu dans cette partie`,
    },
  };
};

interface FichePageProps {
  onRequestGoBack: () => void;
  onRequestSave: (fiche: any) => void;
  onRequestDelete: (fiche: any) => void;
  onRequestShowDetail: (fiche: any) => void;
  saving?: boolean;
  savingCompleted?: boolean;

  typologie: string;
  fiches: {
    loading?: boolean;
    error?: Error;
    data: Outil[];
  };
  detailsRenderer: {
    loading?: boolean;
    error?: Error;
    currentData?: Outil;
  };
  idFromUrl?: string;
  emptyListComponent: ReactNode;
  noItemSelectedComponent: ReactNode;
}

const FichePage: FC<FichePageProps> = ({
  onRequestGoBack,
  onRequestSave,
  onRequestDelete,
  onRequestShowDetail,
  saving,
  savingCompleted = false,
  typologie,
  idFromUrl,
  fiches,
  detailsRenderer,
  emptyListComponent,
  noItemSelectedComponent,
}) => {
  const classes = useStyles();

  const [searchText, setSearchText] = useState<string>('');
  const [openUpsertDialog, setOpenUpsertDialog] = useState<boolean>(false);

  const [numeroOrdre, setNumeroOrdre] = useState<string>('');
  const [intitule, setIntitule] = useState<string>('');
  const [selectedFile, setSelectedFile] = useState<File[]>([]);

  const [selectedFiche, setSelectedFiche] = useState<any | undefined>(undefined);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [mode, setMode] = useState<'creation' | 'modification'>('creation');
  const [ficheToEdit, setFicheToEdit] = useState<any>();
  const [id, setId] = useState<string>('');
  const [orderBy, setOrderBy] = useState<'ordre' | 'libelle'>('ordre');

  const [filteredOutils, setFilteredOutils] = useState<Outil[]>(fiches.data || []);
  const handleSort = (_: React.ChangeEvent<{}>, value: any) => setOrderBy(value);

  useEffect(() => {
    if (fiches.data) {
      const resultSearch = fiches.data.filter((fiche) => filtered(fiche, searchText));

      const resultOrdering = resultSearch.sort((a, b) => {
        const valueA = a[orderBy];
        const valueB = b[orderBy];

        if (valueA < valueB) {
          return -1;
        }
        if (valueA > valueB) {
          return 1;
        }
        return 0;
      });

      setFilteredOutils(resultOrdering);
    }
  }, [fiches?.data, searchText, orderBy]);

  const reInit = (): void => {
    setSelectedFile([]);
    setNumeroOrdre('');
    setIntitule('');
  };

  useEffect(() => {
    if (savingCompleted) {
      reInit();
      setOpenUpsertDialog(false);
    }
  }, [savingCompleted]);

  const handleSearchValueChange = (text: string): void => {
    setSearchText(text);
  };

  const handleNumberoOrdreChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNumeroOrdre((event.target as HTMLInputElement).value);
  };

  const handleIntituleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setIntitule((event.target as HTMLInputElement).value);
  };

  const handleOpenAddModal = (): void => {
    reInit();
    setMode('creation');
    setOpenUpsertDialog(!openUpsertDialog);
  };

  const showDetailFiche = (fiche: any): void => {
    setSelectedFiche(fiche);
    onRequestShowDetail(fiche);
  };

  const beginByUpperCase = (typologie: string): string => {
    const tab = Array.from(typologie);
    let value = '';
    tab.map((letter: string, index: number) => {
      value += index === 0 ? letter.toUpperCase() : letter;
    });

    return value;
  };

  const end = (typologie: string): string => {
    let end = '';
    if (typologie === 'affiche' || typologie === 'procedure') end = 'e';

    return end;
  };

  const saveFiche = (): void => {
    const toSave =
      mode === 'creation'
        ? {
            selectedFile: selectedFile[0],
            ordre: parseInt(numeroOrdre, 10),
            libelle: intitule,
            launchUpload: !!selectedFile[0],
          }
        : {
            id: id,
            ordre: parseInt(numeroOrdre, 10),
            libelle: intitule,
            selectedFile: selectedFile[0],
            launchUpload: !!(selectedFile.length > 0 && selectedFile[0].size),
          };
    onRequestSave(toSave);
  };

  const updateInfo = (fiche: Outil): void => {
    setMode('modification');
    setOpenUpsertDialog(true);
    setFicheToEdit(fiche);

    setId(fiche.id);
    setNumeroOrdre(`${fiche.ordre}`);
    setIntitule(fiche.libelle);
    setSelectedFile([{ ...fiche.fichier, name: fiche.fichier.nomOriginal } as any]);
  };

  const handleSelectedFiles = (files: any[]) => {
    if (files) {
      setSelectedFile([files[files.length - 1]]);
    }
  };

  const openDeleteFicheDialog = (fiche: any): void => {
    setOpenDeleteDialog(true);
    setFicheToEdit(fiche);
  };

  const deleteFiche = (): void => {
    onRequestDelete(ficheToEdit);
    setOpenDeleteDialog(false);
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.boxLeft}>
        <SearchBanner
          title={beginByUpperCase(typologie)}
          onRequestGoBack={onRequestGoBack}
          onChangeSearchValue={handleSearchValueChange}
          searchValue={searchText}
        />
        <Box mt={3}>
          <Typography gutterBottom>Trier par</Typography>
          <RadioGroup value={orderBy} onChange={handleSort}>
            <FormControlLabel label="Code" control={<Radio color="secondary" value="ordre" />} />
            <FormControlLabel label="Nom" control={<Radio color="secondary" value="libelle" />} />
          </RadioGroup>
        </Box>
      </Box>
      <ItemContainer
        loading={fiches?.loading}
        error={fiches?.error}
        items={filteredOutils}
        emptyListComponent={emptyListComponent}
        listBanner={
          <Button variant="contained" onClick={handleOpenAddModal} color="secondary" startIcon={<Add />}>
            {`Créer un${end(typologie)} ${typologie}`}
          </Button>
        }
        listItemRenderer={(fiche: any, index: number) => {
          const creationDate = fiche.createdAt ? `${moment(fiche.createdAt).format('DD/MM/YYYY')}` : ``;
          const modificationDate = fiche.updatedAt ? `${moment(fiche.updatedAt).format('DD/MM/YYYY')}` : '';

          return (
            <CardTheme
              key={index}
              onClick={() => showDetailFiche(fiche)}
              title={fiche.libelle}
              active={selectedFiche?.id === fiche.id || idFromUrl === fiche.id}
              currentId={fiche.id}
              createDate={creationDate}
              updateDate={modificationDate}
              moreOptions={[
                {
                  menuItemLabel: {
                    title: 'Modifier',
                    icon: <Edit />,
                  },
                  onClick: () => {
                    updateInfo(fiche);
                  },
                },
                {
                  menuItemLabel: {
                    title: 'Supprimer',
                    icon: <Delete />,
                  },
                  onClick: () => {
                    openDeleteFicheDialog(fiche);
                  },
                },
              ]}
            />
          );
        }}
        detailRenderer={() =>
          detailsRenderer.loading ? (
            <Loader />
          ) : detailsRenderer.currentData ? (
            <Box p={2} height={1}>
              <Typography variant="h5">
                {`${typologie.charAt(0).toUpperCase()}${detailsRenderer.currentData.ordre}.${
                  detailsRenderer.currentData.libelle
                }`}
              </Typography>
              <FileViewer fileUrl={detailsRenderer.currentData.fichier.publicUrl} />
            </Box>
          ) : (
            noItemSelectedComponent
          )
        }
      />
      <CustomModal
        title={
          mode === 'creation'
            ? `Ajout d'un${end(typologie)} ${typologie}`
            : `Modification d'un${end(typologie)} ${typologie}`
        }
        open={openUpsertDialog}
        setOpen={setOpenUpsertDialog}
        withBtnsActions
        closeIcon
        centerBtns={false}
        headerWithBgColor
        fullWidth
        maxWidth="sm"
        onClickConfirm={saveFiche}
        disabledButton={saving}
      >
        <OutilForms
          style={{ width: '100%' }}
          numeroOrdre={numeroOrdre}
          intitule={intitule}
          setNumeroOrdre={handleNumberoOrdreChange}
          setIntitule={handleIntituleChange}
          selectedFile={selectedFile}
          setSelectedFile={handleSelectedFiles}
        />
      </CustomModal>

      <CustomModal
        title={`Suppression d'un${end(typologie)} ${typologie}`}
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        withBtnsActions
        closeIcon
        headerWithBgColor
        fullWidth
        maxWidth="sm"
        onClickConfirm={deleteFiche}
        disabledButton={saving}
      >
        <Typography>Etes-vous sûr de vouloir poursuivre la suppression ? Cette action est irréversible</Typography>
      </CustomModal>

      {/* <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={deleteFiche}
        content="Etes-vous sûr de vouloir poursuivre la suppression ? Cette action est irréversible."
      /> */}
    </Box>
  );
};

export default FichePage;
