import { createStyles, makeStyles, lighten, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    btn: {
      textTransform: 'none',
      marginRight: 8,
    },

    textContent: {
      color: '#E34168',
      textAlign: 'center',
    },
    allExigenceContent: {
      marginLeft: 10,
    },
    content: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      marginLeft: 10,
    },
    title: {
      fontWeight: 'bold',
      marginBottom: 20,
    },
    headerDetails: {
      background: lighten(theme.palette.primary.main, 0.1),
      color: '#FFFFFF',
      padding: 30,
      marginBottom: 20,
    },
  })
);

export default useStyles;
