import { ITheme, IExigence } from '../../../atoms';

export const filtered = (exigence: IExigence, searchText: string): boolean => {
  const toLowerCased = searchText.toLowerCase();
  return (
    (exigence.titre && exigence.titre.toLowerCase().indexOf(toLowerCased) >= 0) ||
    (exigence.finalite && exigence.finalite.toLowerCase().indexOf(toLowerCased) >= 0) ||
    (exigence.exemples || []).some(
      ({ exemple }) => exemple && exemple.toLowerCase().indexOf(toLowerCased) >= 0,
    ) ||
    (exigence.questions || []).some(
      ({ question }) => question && question.toLowerCase().indexOf(toLowerCased) >= 0,
    )
  );
};

export const filteredThemes = (themes: ITheme[], searchText: string): ITheme[] => {
  const toLowerCased = searchText.toLowerCase();

  console.log(themes);
  const themeFiltered = themes.reduce((previousThemes: ITheme[], currentTheme) => {
    if (currentTheme.nom && currentTheme.nom.toLowerCase().indexOf(toLowerCased) >= 0) {
      return [...previousThemes, currentTheme];
    } else {
      const sousThemes = (currentTheme.sousThemes || []).filter(
        (sousTheme) =>
          (sousTheme.nom && sousTheme.nom.toLowerCase().indexOf(toLowerCased) >= 0) ||
          sousTheme.exigences.some(
            (exigence) =>
              (exigence.titre && exigence.titre.toLowerCase().indexOf(toLowerCased) >= 0) ||
              (exigence.finalite && exigence.finalite.toLowerCase().indexOf(toLowerCased) >= 0) ||
              (exigence.exemples || []).some(
                (exemple) =>
                  exemple.exemple && exemple.exemple.toLowerCase().indexOf(toLowerCased) >= 0,
              ) ||
              (exigence.questions || []).some(
                ({ question }) => question && question.toLowerCase().indexOf(toLowerCased) >= 0,
              ),
          ),
      );
      if (sousThemes.length > 0) {
        return [...previousThemes, { ...currentTheme, sousThemes }];
      }
    }
    return previousThemes;
  }, []);
  return themeFiltered;
  // return themes.filter(
  //   (theme) =>
  //     (theme.nom && theme.nom.toLowerCase().indexOf(toLowerCased) >= 0) ||
  //     (theme.sousThemes || []).some(
  //       (sousTheme) =>
  //         (sousTheme.nom && sousTheme.nom.toLowerCase().indexOf(toLowerCased) >= 0) ||
  //         sousTheme.exigences.some(
  //           (exigence) =>
  //             (exigence.titre && exigence.titre.toLowerCase().indexOf(toLowerCased) >= 0) ||
  //             (exigence.finalite && exigence.finalite.toLowerCase().indexOf(toLowerCased) >= 0) ||
  //             (exigence.exemples || []).some(
  //               (exemple) =>
  //                 exemple.exemple && exemple.exemple.toLowerCase().indexOf(toLowerCased) >= 0,
  //             ) ||
  //             (exigence.questions || []).some(
  //               ({ question }) => question && question.toLowerCase().indexOf(toLowerCased) >= 0,
  //             ),
  //         ),
  //     ),
  // );
};
