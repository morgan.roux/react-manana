import { Box } from '@material-ui/core';
import { FiberManualRecord } from '@material-ui/icons';
import React, { FC, ReactNode, useState, useEffect, MouseEvent } from 'react';
import { ItemContainer } from '@lib/ui-kit/src/components/templates';
import { ITheme, IExigence, ISousTheme } from '@lib/ui-kit';
import { DetailsExigence } from './DetailsExigence';
import useStyles from './style';

import { filteredThemes } from './DetailsExigence/util';

import DropdownFilter from '@lib/ui-kit/src/components/templates/ParameterContainer/SideNav/DropdownFilter/DropdownFilter';
import { useApplicationContext } from '@lib/common';

interface ReferentielQualitePageProps {
  emptyListComponent: ReactNode;
  noItemSelectedComponent: ReactNode;

  exigenceCommentComponentRenderer: (exigence: IExigence) => ReactNode;

  loadingThemes?: boolean;
  errorLoadingThemes?: Error;
  themes?: ITheme[];

  loadingExigences?: boolean;
  errorLoadingExigences?: Error;
  exigences?: IExigence[];

  selectedThemeId?: string;
  selectedSousThemeId?: string;

  onOutilClick: (typologie: string, id: string) => void;
  onRequestExigences: (sousTheme: ISousTheme) => void;
  onRequestGoBack: (event: MouseEvent) => void;
  filterIcon?: any;
}

const ReferentielQualitePage: FC<ReferentielQualitePageProps> = ({
  emptyListComponent,
  noItemSelectedComponent,
  exigenceCommentComponentRenderer,
  exigences,
  themes,
  selectedThemeId,
  selectedSousThemeId,
  loadingThemes,
  errorLoadingThemes,
  loadingExigences,
  errorLoadingExigences,
  onRequestGoBack,
  onRequestExigences,
  onOutilClick,
  filterIcon,
}) => {
  const classes = useStyles();
  const [filterShowList, setFilterShowList] = useState<boolean>(false);
  const [filterThemes, setFilterThemes] = useState<ITheme[]>([]);
  const [selectedSousTheme, setSelectedSousTheme] = useState<ISousTheme>();
  const [selectedTheme, setSelectedTheme] = useState<ITheme>();
  const [textToSearch, setTextToSearch] = useState<string>('');

  const { isMobile } = useApplicationContext();
  console.log('+++ filterThemes : ', filterThemes, 'themes : ', themes);

  useEffect(() => {
    if (!loadingThemes && themes) {
      setFilterThemes(themes);
      console.log('themesUseEffect', themes);

      // default theme
      if (selectedThemeId && !selectedTheme) {
        const defaultTheme = themes.find(({ id }) => id === selectedThemeId);
        if (defaultTheme) {
          setSelectedTheme(defaultTheme);

          // change sous-theme
          if (selectedSousThemeId) {
            const defaultSousTheme = defaultTheme.sousThemes.find(({ id }) => id === selectedSousThemeId);
            if (defaultSousTheme) {
              setSelectedSousTheme(defaultSousTheme);
            }
          }
        }
      }
    }
  }, [loadingThemes]);

  useEffect(() => {
    console.log('+++ onSearchChange themes : ', themes, textToSearch);
    if (themes && textToSearch.length >= 2) {
      const filterThemes = filteredThemes(themes, textToSearch);
      setFilterThemes(filterThemes);
    } else if (textToSearch.length === 0) {
      setFilterThemes(themes || []);
    }
  }, [textToSearch]);

  useEffect(() => {
    if (selectedSousTheme) {
      onRequestExigences(selectedSousTheme);
    }
  }, [selectedSousTheme]);

  const handleChange = (sousTheme: ISousTheme, theme: ITheme): void => {
    setSelectedTheme(theme);
    setSelectedSousTheme(sousTheme);
  };

  return (
    <Box className={classes.root}>
      <ItemContainer
        banner={{
          withDivider: false,
          title: 'Référentiel Qualité',
          onClick: onRequestGoBack,
        }}
        onSearchChange={setTextToSearch}
        isMenu
        style={{ width: '100%' }}
        menuStyle={{ width: 326 }}
        emptyListComponent={emptyListComponent}
        items={filterThemes}
        loading={loadingThemes}
        error={errorLoadingThemes}
        mobileFilter={isMobile}
        filterIcon={filterIcon}
        filterState={filterShowList}
        setFilterState={setFilterShowList}
        listItemRenderer={(theme: any) => {
          return (
            <DropdownFilter
              title={`${theme.ordre}. ${theme.nom}`}
              filterList={{
                data: theme.sousThemes,
              }}
              numbered
              dataIdName="id"
              dataLabelName="nom"
              dataNumberName="ordre"
              prefixNumber={theme.ordre}
              dataIcon={<FiberManualRecord />}
              countable={false}
              selectable={false}
              onClickItem={() => {
                setFilterShowList(!filterShowList);
                console.log('showwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwlist', filterShowList);
              }}
              filtersSelected={selectedSousTheme ? [selectedSousTheme] : undefined}
              onItemClick={(sousTheme) => handleChange(sousTheme as ISousTheme, theme)}
            />
          );
        }}
        detailRenderer={() =>
          selectedSousTheme ? (
            <DetailsExigence
              key={selectedSousTheme.id}
              commentComponentRenderer={exigenceCommentComponentRenderer}
              onOutilClick={onOutilClick}
              loading={loadingExigences}
              error={errorLoadingExigences}
              exigences={exigences || []}
              sousTheme={selectedSousTheme}
              theme={selectedTheme}
            />
          ) : (
            noItemSelectedComponent
          )
        }
      />
    </Box>
  );
};

export default ReferentielQualitePage;
