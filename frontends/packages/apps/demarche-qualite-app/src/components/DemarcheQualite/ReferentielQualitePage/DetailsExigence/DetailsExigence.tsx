import { Box } from '@material-ui/core';
import React, { FC, ReactNode, useState, useEffect } from 'react';
import { Loader, ErrorPage } from '@lib/ui-kit';
import { Banner, Exigence, IExigence, ISousTheme, ITheme, ExigenceTab } from '@lib/ui-kit';
import useStyles from './style';

interface DetailsExigence {
  loading?: boolean;
  error?: Error;

  exigences: IExigence[];
  sousTheme: ISousTheme;
  commentComponentRenderer: (exigence: IExigence) => ReactNode;

  theme?: ITheme;
  onOutilClick: (typologie: string, id: string) => void;
}

const DetailsExigence: FC<DetailsExigence> = ({
  exigences,
  theme,
  sousTheme,
  loading,
  error,
  onOutilClick,
  commentComponentRenderer,
}) => {
  const classes = useStyles();
  const [showDetails, setShowDetails] = useState<boolean>(false);
  const [currentExigence, setCurrentExigence] = useState<IExigence>();
  const [originalExigences, setOriginalExigences] = useState<IExigence[]>([]);
  const [displayExigences, setDisplayExigences] = useState<IExigence[]>([]);

  useEffect(() => {
    if (typeof exigences !== 'undefined' && exigences != null) {
      setOriginalExigences(exigences);
    }
  }, [exigences]);

  useEffect(() => {
    if (originalExigences) {
      setDisplayExigences(originalExigences);
    } else {
      setDisplayExigences(originalExigences || []);
    }
  }, [originalExigences]);

  const handleShowDetails = (exigence: IExigence) => {
    setCurrentExigence(exigence);
    setShowDetails(true);
  };

  const handleShow = () => {
    setShowDetails(false);
  };

  if (loading) {
    return <Loader />;
  }
  if (error) {
    return <ErrorPage />;
  }

  if (!theme || !sousTheme) {
    return null;
  }

  return (
    <Box paddingTop={5}>
      {/* <Box className={classes.headerDetails}>
        <DebouncedSearchInput
          placeholder="Rechercher une exigence"
          fullWidth={false}
          value={exigenceToSearch}
          wait={100}
          onChange={setExigenceToSearch}
        />
      </Box> */}
      {showDetails && currentExigence && (
        <Box className={classes.allExigenceContent}>
          <Banner title="Retour" onClick={handleShow} titleStyle={{ color: 'black', fontSize: 13 }} />
          <Exigence key={currentExigence.id} exigence={currentExigence} />
          <br />
          <ExigenceTab
            key={currentExigence.id}
            commentComponentRenderer={commentComponentRenderer}
            onOutilClick={onOutilClick}
            exigence={currentExigence}
          />
        </Box>
      )}
      <Box className={classes.content}>
        {!showDetails && (
          <Box>
            <Box className={classes.title}>{`${theme.ordre}.${sousTheme.ordre}. ${sousTheme.nom}`}</Box>
            {displayExigences.map((exigence: any) => (
              <Exigence key={exigence.id} exigence={exigence} onClick={() => handleShowDetails(exigence)} />
            ))}
          </Box>
        )}
      </Box>
    </Box>
  );
};

export default DetailsExigence;
