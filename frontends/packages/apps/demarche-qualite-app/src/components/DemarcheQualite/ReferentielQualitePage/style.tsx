import { createStyles, Theme, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      height: 'calc(100vh - 86px)',
      '& > *': {
        font: 'Roboto',
      },
    },

    left: {
      width: 274,
    },

    selectedThemeIcon: {
      color: theme.palette.common.white,
    },
  })
);

export default useStyles;
