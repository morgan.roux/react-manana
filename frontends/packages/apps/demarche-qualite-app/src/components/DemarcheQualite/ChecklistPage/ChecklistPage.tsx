/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable react/jsx-no-bind */
import React, { FC, useState } from 'react';
import { Delete, Edit, MoreHoriz, Add, Close } from '@material-ui/icons';
import { IconButton, Menu, MenuItem, Typography, ListItemIcon, Fade, Box } from '@material-ui/core';
import moment from 'moment';

import { CustomContainer } from '@lib/ui-kit/src/components/templates/CustomContainer';
import { NewCustomButton, Backdrop, ErrorPage, Table, ConfirmDeleteDialog } from '@lib/ui-kit';

export interface Checklist {
  id?: string;
  code?: string;
  ordre: number;
  libelle: string;
  createdAt?: any;
  updatedAt?: any;
  nombreEvaluations?: number;
  sections: ChecklistSection[];
  cloture?: boolean;
  updatedBy?: {
    id: string;
    fullName: string;
  };
}

export interface ChecklistSection {
  id?: string;
  ordre: number;
  libelle: string;
  items: ChecklistSectionItem[];
}

export interface ChecklistSectionItem {
  id?: string;
  ordre: number;
  libelle: string;
  nombreEvaluations?: number;
}

export interface ChecklistPageProps {
  loading?: boolean;
  error?: Error;
  data?: Checklist[];
  onRequestAdd: () => void;
  onRequestClose: (checkList: Checklist) => void;
  onRequestDelete: (checklist: Checklist) => void;
  onRequestEdit: (checklist: Checklist) => void;
}

const ChecklistPage: FC<ChecklistPageProps> = ({
  onRequestAdd,
  onRequestEdit,
  onRequestDelete,
  onRequestClose,
  loading,
  error,
  data,
}) => {
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [checklistToDelete, setChecklistToDelete] = useState<Checklist | null>(null);
  const [checklistToEdit, setChecklistToEdit] = useState<Checklist | null>(null);

  const open = Boolean(anchorEl);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleShowMenuClick = (checklistToEdit: Checklist, event: React.MouseEvent<HTMLElement>) => {
    event.preventDefault();
    event.stopPropagation();
    setChecklistToEdit(checklistToEdit);
    setAnchorEl(event.currentTarget);
  };

  const handleConfirmDelete = () => {
    if (checklistToDelete) {
      onRequestDelete(checklistToDelete);
      setOpenDeleteDialog(false);
    }
  };

  const handleEdit = () => {
    if (checklistToEdit) {
      onRequestEdit(checklistToEdit);
    }
  };

  const handleCloseFiche = (): void => {
    if (checklistToEdit) {
      onRequestClose(checklistToEdit);
    }
  };

  if (loading) {
    return <Backdrop />;
  }
  if (error) {
    return <ErrorPage />;
  }
  const tableData: Checklist[] = data || [];

  return (
    <>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<span>Êtes-vous sur de vouloir supprimer ce check-list ? </span>}
        onClickConfirm={handleConfirmDelete}
      />
      <CustomContainer
        filled={false}
        bannerTitle="Liste des checklist"
        bannerRight={
          <NewCustomButton startIcon={<Add />} onClick={onRequestAdd}>
            Ajouter un check-list
          </NewCustomButton>
        }
        contentStyle={{
          padding: 20,
        }}
      >
        <Table
          dataIdName="id"
          data={tableData}
          search={false}
          topBarComponent={(searchComponent) => {
            return (
              <Box paddingBottom="10px" display="flex" justifyContent="flex-end">
                <Box maxWidth={400}>{searchComponent}</Box>
              </Box>
            );
          }}
          columns={[
            { name: 'code', label: 'Code' },
            { name: 'libelle', label: 'Nom' },
            { name: 'nombreEvaluations', label: "Nombre d'évaluation" },
            {
              name: 'createdAt',
              label: 'Date de création',

              renderer: (row: Checklist) => {
                return row.createdAt ? moment(row.createdAt).format('DD/MM/YYYY') : '-';
              },
            },
            {
              name: 'updatedAt',
              label: 'Date de modification',
              renderer: (row: Checklist) => {
                return row.updatedAt ? moment(row.updatedAt).format('DD/MM/YYYY') : '-';
              },
            },
            {
              name: '',
              label: '',
              renderer: (row: Checklist) => {
                return (
                  <div key={`row-${row.id}`}>
                    <IconButton
                      aria-controls="simple-menu"
                      aria-haspopup="true"
                      onClick={handleShowMenuClick.bind(null, row)}
                    >
                      <MoreHoriz />
                    </IconButton>
                    <Menu
                      id="fade-menu"
                      anchorEl={anchorEl}
                      keepMounted
                      open={open && row.id === checklistToEdit?.id}
                      onClose={handleClose}
                      TransitionComponent={Fade}
                    >
                      <MenuItem
                        onClick={(event) => {
                          event.stopPropagation();
                          event.preventDefault();
                          handleClose();
                          handleEdit();
                        }}
                        disabled={row?.cloture}
                      >
                        <ListItemIcon>
                          <Edit />
                        </ListItemIcon>
                        <Typography variant="inherit">Modifier</Typography>
                      </MenuItem>

                      <MenuItem
                        onClick={(event) => {
                          event.stopPropagation();
                          event.preventDefault();
                          handleClose();
                          handleCloseFiche();
                        }}
                        disabled={row?.cloture}
                      >
                        <ListItemIcon>
                          <Close />
                        </ListItemIcon>
                        <Typography variant="inherit">Clôturer</Typography>
                      </MenuItem>

                      <MenuItem
                        onClick={(event) => {
                          event.stopPropagation();
                          event.preventDefault();
                          handleClose();
                          setChecklistToDelete(row);
                          setOpenDeleteDialog(true);
                        }}
                        disabled={row?.cloture || (row?.nombreEvaluations as any) > 0}
                      >
                        <ListItemIcon>
                          <Delete />
                        </ListItemIcon>
                        <Typography variant="inherit">Supprimer</Typography>
                      </MenuItem>
                    </Menu>
                  </div>
                );
              },
            },
          ]}
        />
      </CustomContainer>
    </>
  );
};

export default ChecklistPage;
