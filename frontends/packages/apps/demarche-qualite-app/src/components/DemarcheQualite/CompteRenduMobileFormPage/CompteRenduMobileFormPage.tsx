import { Box, CssBaseline, Hidden } from '@material-ui/core';
import { unionWith } from 'lodash';
import React, { ChangeEvent, FC, MouseEvent, ReactNode, useEffect, useState } from 'react';
import { CustomDatePicker, CustomFormTextField, NewCustomButton } from '@lib/ui-kit/src/components/atoms';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import { ActionOperationnelle } from '../ActionOperationnellePage/types';
import { FicheIncident } from '../FicheIncidentPage';
import { Actions } from '../CompteRenduFormPage/Actions';
import { RequestSavingCompteRendu, Reunion, ReunionAction } from '../CompteRenduFormPage/types';
import { Backdrop } from '@lib/ui-kit/src/components/atoms/Backdrop';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import { FicheAmelioration, Origine } from '../FicheAmeliorationPage/types';
import useStyles from './styles';

interface CompteRenduMobileFormProps {
  mode: 'creation' | 'modification';
  loading?: boolean;
  saving?: boolean;
  error?: Error;

  reunion?: Reunion;

  origines: {
    loading?: boolean;
    error?: Error;
    data?: Origine[];
  };

  ficheAmeliorations: {
    data?: FicheAmelioration[];
    loading?: boolean;
    error?: Error;
  };

  ficheIncidents: {
    data?: FicheIncident[];
    loading?: boolean;
    error?: Error;
  };

  actionOperationnelles: {
    data?: ActionOperationnelle[];
    loading?: boolean;
    error?: Error;
  };

  idUserParticipants: string[];
  idUserAnimateur: string;
  idResponsable: string;

  selectAnimateurComponent: ReactNode;
  selectResponsableComponent: ReactNode;
  selectCollaborateurComponent: ReactNode;

  importances: any[];
  urgences: any[];

  onChangeUrgence: (action: ReunionAction, urgence: any) => void;
  onChangeImportance: (action: ReunionAction, importance: any) => void;

  onParticipantsClicked: (action: ReunionAction) => void;

  onRequestGoBack: (event: MouseEvent) => void;
  onRequestSave: (toSave: RequestSavingCompteRendu) => void;
}

const CompteRenduMobileFormPage: FC<CompteRenduMobileFormProps> = ({
  mode,
  origines,
  reunion,
  loading,
  error,
  saving,
  ficheAmeliorations,
  ficheIncidents,
  actionOperationnelles,
  selectAnimateurComponent,
  selectResponsableComponent,
  selectCollaborateurComponent,
  idUserParticipants,
  idUserAnimateur,
  idResponsable,
  importances,
  urgences,
  onParticipantsClicked,
  onChangeUrgence,
  onChangeImportance,
  onRequestGoBack,
  onRequestSave,
}) => {
  const classes = useStyles();
  const [description, setDescription] = useState<string>('');
  const [selectedReunionActions, setSelectedReunionActions] = useState<ReunionAction[]>([]);
  const [reunionActions, setReunionActions] = useState<ReunionAction[]>([]);
  const [showSelectedReunionActions, setShowSelectedReunionActions] = useState<boolean>(false);
  const [dateReunion, setDateRendezVous] = useState<Date>(new Date());

  const isActivedAddButton = (): boolean => {
    return description === '';
  };

  useEffect(() => {
    const tableau: ReunionAction[] = [...((reunion?.actions || []) as any)];

    (ficheAmeliorations.data || []).forEach((currentData: FicheAmelioration) => {
      if (!tableau.some((item) => item.idTypeAssocie === currentData.id && item.type === 'AMELIORATION')) {
        tableau.push({
          type: 'AMELIORATION',
          idTypeAssocie: currentData.id,
          createdAt: currentData.createdAt,
          updatedAt: currentData.updatedAt,
          typeAssocie: currentData,
        });
      }
    });

    (ficheIncidents.data || []).forEach((currentData: FicheIncident) => {
      if (!tableau.some((item) => item.idTypeAssocie === currentData.id && item.type === 'INCIDENT')) {
        tableau.push({
          type: 'INCIDENT',
          idTypeAssocie: currentData.id,
          createdAt: currentData.createdAt,
          updatedAt: currentData.updatedAt,
          typeAssocie: currentData,
        });
      }
    });

    (actionOperationnelles.data || []).forEach((currentData: ActionOperationnelle) => {
      if (!tableau.some((item) => item.idTypeAssocie === currentData.id && item.type === 'OPERATION')) {
        tableau.push({
          type: 'OPERATION',
          idTypeAssocie: currentData.id as any,
          createdAt: currentData.createdAt,
          typeAssocie: currentData,
        });
      }
    });

    setReunionActions(unionWith(tableau, (a, b) => a.type === b.type && a.idTypeAssocie === b.idTypeAssocie));

    if (reunion && mode === 'modification') {
      setDescription(reunion.description);
      setSelectedReunionActions(reunion.actions || ([] as any));
    }
  }, [ficheAmeliorations.data, ficheIncidents.data, actionOperationnelles.data, reunion]);

  const handleSaveClick = () => {
    if (idResponsable && idUserAnimateur)
      onRequestSave({
        id: mode === 'modification' ? reunion?.id : undefined,
        description,
        idUserAnimateur,
        idResponsable,
        idUserParticipants,
        actions: selectedReunionActions.map((action) => ({
          type: action.type,
          idTypeAssocie: action.idTypeAssocie,
          idTodoAction: action.idTodoAction,
        })) as any,
        dateReunion,
      });
  };

  const handleDescriptionChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setDescription((event.target as HTMLInputElement).value);
  };

  const handleDateRendezVousChange = (date: any) => {
    setDateRendezVous(date);
  };

  if (error) {
    return <ErrorPage />;
  }

  if (loading) {
    return <Backdrop />;
  }

  return (
    <CustomContainer
      filled
      bannerBack
      bannerTitle={`${mode === 'modification' ? 'Modification' : 'Ajout'} de réunion`}
      onBackClick={onRequestGoBack}
      bannerRight={
        <NewCustomButton disabled={isActivedAddButton() || saving} onClick={handleSaveClick}>
          {`${mode === 'modification' ? 'Enregistrer' : 'Créer'}`}
        </NewCustomButton>
      }
      contentStyle={{
        marginTop: 24,
        width: '100%',
        maxWidth: 785,
        padding: 20,
      }}
    >
      <Box width="100%">
        <CssBaseline />
        {selectCollaborateurComponent}
        {selectAnimateurComponent}
        {selectResponsableComponent}
        <CustomFormTextField
          label="Description"
          value={description}
          name="description"
          onChange={handleDescriptionChange}
          rows={4}
          multiline
        />
        <Actions
          origines={origines}
          importances={importances}
          urgences={urgences}
          onChangeImportance={onChangeImportance}
          onChangeUrgence={onChangeUrgence}
          onParticipantsClicked={onParticipantsClicked}
          actionsData={reunionActions}
          selectedReunionActions={selectedReunionActions}
          onChangeReunionActionsSelection={setSelectedReunionActions}
          showSelectedReunionsActions={showSelectedReunionActions}
          onShowSelectedReunionsActions={() => setShowSelectedReunionActions(!showSelectedReunionActions)}
        />
        <Box py={2}>
          <CustomDatePicker
            value={dateReunion}
            label={`Date du reunion`}
            onChange={handleDateRendezVousChange}
            required={true}
          />
        </Box>
        <Hidden mdUp={true} implementation="css">
          <Box className={classes.actionBtns}>
            <NewCustomButton disabled={isActivedAddButton() || saving} onClick={handleSaveClick}>
              {`${mode === 'modification' ? 'Enregistrer modification' : 'Créer une réunion'}`}
            </NewCustomButton>
          </Box>
        </Hidden>
      </Box>
    </CustomContainer>
  );
};

export default CompteRenduMobileFormPage;
