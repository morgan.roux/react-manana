import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    customReactQuill: {
      width: '100%',
      outline: 'none',
      marginBottom: 18,
      marginTop: 25,
      border: '1px solid rgba(0, 0, 0, 0.23)',
      borderRadius: 4,
      '& .ql-toolbar.ql-snow': {
        border: '0px',
        borderBottom: '1px solid rgba(0, 0, 0, 0.23)',
      },
      '& .ql-container': {
        minHeight: 200,
        border: '0px',
      },
      '&:hover': {
        borderColor: theme.palette.primary.main,
      },
      '&:focus': {
        outline: `auto ${theme.palette.primary.main}`,
      },
    },
    actionBtns: {
      position: 'sticky',
      bottom: 16,
      display: 'flex',
      width: '100%',
      justifyContent: 'flex-end',
    },
  }),
);

export default useStyles;
