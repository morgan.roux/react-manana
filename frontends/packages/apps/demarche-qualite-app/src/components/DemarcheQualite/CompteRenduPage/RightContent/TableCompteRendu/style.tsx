import { createStyles, makeStyles } from "@material-ui/styles";

const useStyles = makeStyles( () =>
    createStyles({
        root : {

        },
        tableCaption : {
            width : '100%',
            display : 'flex',
            flexDirection : 'row',
            justifyContent : 'space-between',
            marginBottom : 10
        },
        dataContainer : {
            display : 'flex',
            flexDirection : 'column',
            marginTop : 8
        },
        selectedElement : {
            fontWeight : 'bold'
        },
        tableContent : {

        },
    })
)

export default useStyles;