import { Box, TextField } from '@material-ui/core';
import React, { FC, useState, ChangeEvent } from 'react';
import { DebouncedSearchInput } from '@lib/ui-kit/src/components/atoms';
import Comment from '@lib/ui-kit/src/components/atoms/Comment/Comment';
import useStyles from './style';

interface PresenceProps {
  data: any[];
}

const Presence: FC<PresenceProps> = ({ data }) => {
  const classes = useStyles();

  const [currentSearch, setCurrentSearch] = useState<string>('');
  const [type, setType] = useState<string>('');
  const [currentId, setCurrentId] = useState<string>('');

  setCurrentId('XXX');

  const handleTypeChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setType((event.target as HTMLInputElement).value);
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.title}>Fiche de présence</Box>
      <Box className={classes.forms}>
        <Box className={classes.searchFormBox}>
          <DebouncedSearchInput fullWidth={true} value={currentSearch} onChange={setCurrentSearch} />
        </Box>
        <Box className={classes.typeFormBox}>
          <TextField label="Type" select={true} value={type} onChange={handleTypeChange} className={classes.textField}>
            <option>Tous</option>
          </TextField>
        </Box>
      </Box>
      <Box className={classes.presenceWithCheck}>
        {data.map((currentData: any) => (
          <>
            <Box>
              <Comment
                comment={{
                  user: {
                    userName: currentData.name,
                  },
                  content: currentData.content,
                }}
              />
            </Box>
            <Box>
              <input type="checkbox" value={currentData.id} checked={currentData.id === currentId} />
            </Box>
          </>
        ))}
      </Box>
    </Box>
  );
};

export default Presence;
