import { Box } from '@material-ui/core';
import React, { FC, useState, ReactNode, useEffect } from 'react';
import useStyles from './style';
import EnhancedTable from '@lib/ui-kit/src/components/atoms/Table/Table';
import { TableRowData, Column } from '@lib/ui-kit/src/components/atoms';
import { MoreHoriz } from '@material-ui/icons';

interface TableCompteRenduProps {
  isDetailsTable?: boolean;
  data: any[];
  rightContent: ReactNode;
  leftLabel: any;
  rowsPerPageOptions?: number[];
}

const TableCompteRendu: FC<TableCompteRenduProps> = ({
  isDetailsTable,
  data,
  rightContent,
  leftLabel,
  rowsPerPageOptions,
}) => {
  const classes = useStyles();

  const [rowsSelected, setRowsSelected] = useState<TableRowData[]>([]);
  const [realData, setRealData] = useState<TableRowData[]>([]);

  const columns: Column[] = [
    {
      name: 'dateReunion',
      label: 'Date de réunion',
    },
    {
      name: 'animateur',
      label: 'Animateur',
    },
    {
      name: 'resume',
      label: 'Resumé',
    },
    {
      name: 'statistique',
      label: 'Statistique',
    },
    {
      name: 'more',
      label: '',
    },
  ];

  const detailsColumns: Column[] = [
    {
      name: 'type',
      label: 'Type',
    },
    {
      name: 'dateAction',
      label: "Date d'action",
    },
    {
      name: 'auteur',
      label: 'Auteur',
    },
    {
      name: 'activite',
      label: 'Activité',
    },
    {
      name: 'details',
      label: 'Détails',
    },
    {
      name: 'priorite',
      label: 'Priorité',
    },
    {
      name: 'suiviPar',
      label: 'Suivi Par',
    },
    {
      name: 'statutTodo',
      label: 'Statut Todo',
    },
    {
      name: 'more',
      label: '',
    },
  ];

  useEffect(() => {
    if (data) {
      const tab: any[] = [];
      data.map((currentData: any) => {
        let object: any = {};
        if (isDetailsTable) {
          object.type = currentData.type;
          object.dateAction = currentData.dateAction;
          object.auteur = currentData.auteur;
          object.activite = currentData.activite;
          object.details = currentData.details;
          object.priorite = currentData.priorite;
          object.suiviPar = currentData.suiviPar;
          object.statutTodo = currentData.statutTodo;
        } else {
          object.dateReunion = currentData.dateReunion;
          object.animateur = currentData.animateur;
          object.resume = currentData.resume;
          object.statistique = currentData.statistique;
        }
        object.more = () => {
          return <MoreHoriz onClick={() => console.log('Current data', currentData)} />;
        };

        tab.push(object);
      });

      setRealData(tab);
    }
  }, [data]);

  return (
    <Box className={classes.dataContainer}>
      <Box className={classes.tableCaption}>
        <Box className={classes.selectedElement}>{leftLabel}</Box>
        <Box>{rightContent}</Box>
      </Box>
      <Box className={classes.tableContent}>
        <EnhancedTable
          columns={isDetailsTable ? detailsColumns : columns}
          rowsSelected={rowsSelected}
          onRowsSelectionChange={setRowsSelected}
          data={realData}
          selectable={true}
          rowsPerPageOptions={rowsPerPageOptions ? rowsPerPageOptions : [10, 20]}
        />
      </Box>
    </Box>
  );
};

export default TableCompteRendu;
