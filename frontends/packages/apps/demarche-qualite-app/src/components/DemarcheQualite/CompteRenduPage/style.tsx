import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
    },
    left: {
      maxWidth: 276,
      width: 276,
      paddingLeft: 25,
      paddingRight: 25,
      borderRight: '1px solid black',
    },
    right: {
      width: 'auto',
    },
    iconAction: {
      padding: theme.spacing(0.5),
      margin: theme.spacing(0, 0.65, 0, 0),
    },
    createButton: {
      position: 'absolute',
      bottom: 80,
      right: 16,
      background: theme.palette.primary.main,
      borderRadius: '50%',
      '& .MuiIconButton-root': {
        color: theme.palette.common.white,
      },
      '& svg': {
        fill: '#fff',
      },
    },
  })
);

export default useStyles;
