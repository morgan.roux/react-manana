import { createStyles, makeStyles } from "@material-ui/styles";
import { Theme } from '@material-ui/core';

const useStyles = makeStyles( (theme : Theme) =>
    createStyles({
        root : {
            display : 'flex',
            flexDirection : 'column',
        },
        title : {
            fontWeight : 'bold',
            color : theme.palette.text.secondary,
            marginBottom : 10
        },
        forms : {
            display : 'flex',
            flexDirection : 'row',
            width : '100%'
        },
        searchFormBox : {
            width : '60%',
            marginRight : 5
        },
        typeFormBox : {
            width : '40%'
        },
        textField : {
            width : '100%'
        },
        presenceWithCheck : {
            display : 'flex',
            flexDirection : 'row',
            justifyContent : 'space-between'
        }
    })
)

export default useStyles;