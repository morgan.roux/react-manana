import React, { FC } from 'react';
import { Stepper } from '@lib/ui-kit';
import Action from './Action/Action';
import Presence from './Presence/Presence';
import Validation from './Validation/Validation';

interface AjoutCompteRenduProps {
  saving?: boolean;
  onRequestSave: () => void;
  onRequestGoBack: () => void;
}

const AjoutCompteRendu: FC<AjoutCompteRenduProps> = ({ saving, onRequestSave, onRequestGoBack }) => {
  const handleSave = (): void => {
    onRequestSave();
  };

  return (
    <Stepper
      onValidate={handleSave}
      loading={saving}
      onCancel={onRequestGoBack}
      disableNextBtn={false}
      steps={[
        {
          title: 'Présence',
          content: <Presence data={[]} />,
        },
        {
          title: 'Actions',
          content: <Action />,
        },
        {
          title: 'Validation',
          content: <Validation />,
        },
      ]}
    />
  );
};

export default AjoutCompteRendu;
