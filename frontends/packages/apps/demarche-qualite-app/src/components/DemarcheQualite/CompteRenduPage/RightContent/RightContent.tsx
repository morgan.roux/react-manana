import { Box, Divider } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { FC } from 'react';
import { CustomButton } from '@lib/ui-kit/src/components/atoms';
import useStyles from './style';
import TableCompteRendu from './TableCompteRendu/TableCompteRendu';

export interface RightContentProps {
  data: any[];
}

const RightContent: FC<RightContentProps> = ({ data }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.header}>
        <Box className={classes.headerTitle}>Liste des réunions</Box>
        <Box>
          <CustomButton startIcon={<Add />}>Ajouter une réunion</CustomButton>
        </Box>
      </Box>
      <Divider />
      <Box className={classes.dataContainer}>
        <Box className={classes.tableContent}>
          <TableCompteRendu
            rowsPerPageOptions={[5, 10]}
            data={data}
            leftLabel="5/520 séléctionnées(520)"
            rightContent={<CustomButton>Supprimer ma séléction</CustomButton>}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default RightContent;
