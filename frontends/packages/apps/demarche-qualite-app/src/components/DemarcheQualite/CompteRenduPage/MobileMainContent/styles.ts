import { createStyles, makeStyles } from '@material-ui/core';
const useStyles = makeStyles(() =>
  createStyles({
    descriptionFieldset: {
      borderRadius: 4,
      padding: 16,
      border: '1px solid #9E9E9E',
      '& legend': {
        color: '#9E9E9E',
        fontFamily: 'Roboto',
        fontSize: 12,
      },
      '& .MuiTypography-root': {
        fontFamily: 'Roboto',
        fontSize: 16,
      },
    },
  }),
);

export default useStyles;
