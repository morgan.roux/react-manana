import { Box, InputLabel, OutlinedInput, FormControl } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { FC, MouseEvent, useState } from 'react';
import { CustomButton } from '@lib/ui-kit/src/components/atoms/CustomButton';
import { Column } from '@lib/ui-kit/src/components/atoms/Table';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import useStyles from './style';
import {
  Backdrop,
  CustomModal,
  Table,
  CustomSelect,
  CustomAvatarGroup,
  UrgenceLabel,
  ImportanceLabel,
} from '@lib/ui-kit/src/components/atoms';
import { Reunion, ReunionAction } from '../../../CompteRenduFormPage/types';
import moment from 'moment';
import { CollaboratorList } from '@lib/ui-kit/src/components/atoms/CollaboratorList';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import { Statut } from '../../../FicheIncidentPage/types';
import { strippedColumnStyle, strippedString } from '@lib/ui-kit/src/services/Helpers';

interface DetailsCompteRenduProps {
  collaborateurs: {
    loading?: boolean;
    error: Error;
    data: any[];
  };
  compteRendu: {
    loading?: boolean;
    error: Error;
    data: Reunion;
  };

  statuts: {
    loading?: boolean;
    error?: Error;
    data?: Statut[];
  };

  onRequestGoBack: (event: MouseEvent) => void;
  onRequestAdd: (event: MouseEvent) => void;
  onRequestSeeAction: (action: ReunionAction) => void;
  onRequestGoToTodo: (action: ReunionAction) => void;
  onRequestChangeStatus: (Action: ReunionAction, idStatut: string) => void;
}

const DetailsCompteRendu: FC<DetailsCompteRenduProps> = ({
  collaborateurs,
  compteRendu,
  statuts,
  onRequestGoBack,
  onRequestAdd,
  onRequestChangeStatus,
}) => {
  const classes = useStyles();
  const [openListePresenceDialog, setOpenListePresenceDialog] = useState<boolean>(false);

  const columns: Column[] = [
    {
      name: 'type',
      label: 'Type',
      renderer: (row: ReunionAction) => {
        if (row.type === 'INCIDENT') return 'Incident';
        else if (row.type === 'AMELIORATION') return 'Amélioration';
        else return 'Action opérationnelle';
      },
    },
    {
      name: 'description',
      label: 'Description',
      renderer: (row: ReunionAction) => {
        return row.typeAssocie.description ? strippedString(row.typeAssocie.description) : '-';
      },
      style: strippedColumnStyle,
    },
    {
      name: 'origine',
      label: 'Origine',
      renderer: (row: ReunionAction) => {
        return row.typeAssocie.origine?.libelle || '-';
      },
    },

    {
      name: 'createdAt',
      label: "Date d'action",
      renderer: (row: ReunionAction) => {
        return row.typeAssocie?.createdAt ? moment(row.typeAssocie.createdAt).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'participants',
      label: 'Collaborateurs concernés',
      renderer: (row: ReunionAction) => (
        <CustomAvatarGroup max={3} sizes="30px" users={(row.typeAssocie.participants || []) as any} />
      ),
    },
    {
      name: 'urgence',
      label: 'Urgence',
      renderer: (row: ReunionAction) => <UrgenceLabel urgence={row.typeAssocie.urgence} />,
    },

    {
      name: 'importance',
      label: 'Importance',
      renderer: (row: ReunionAction) => <ImportanceLabel importance={row.typeAssocie.importance as any} />,
    },

    {
      name: '',
      label: 'Statut',
      renderer: (row: ReunionAction) => {
        return (
          <CustomSelect
            key={row.id}
            list={(statuts.data || []).filter(({ type }) => type === row.type)}
            listId="id"
            index="libelle"
            value={row.typeAssocie.statut.id}
            onChange={(e: any) => onRequestChangeStatus(row, e.target.value)}
          />
        );
      },
    },
  ];

  const toggleOpenListePresence = (): void => {
    setOpenListePresenceDialog(!openListePresenceDialog);
  };

  if (compteRendu.loading || collaborateurs.loading || statuts.loading) {
    return <Backdrop value="Récupération de données..." />;
  }

  if (compteRendu.error || collaborateurs.error || statuts.error) {
    return <ErrorPage />;
  }

  return (
    <Box className={classes.root}>
      <CustomContainer
        bannerBack
        onBackClick={onRequestGoBack}
        bannerTitle="Détails de la réunion"
        filled
        bannerRight={
          <CustomButton startIcon={<Add />} onClick={onRequestAdd} color="secondary">
            AJOUTER UNE REUNION
          </CustomButton>
        }
      >
        <Box className={classes.content}>
          <Box className={classes.firstContent}>
            <Box className={classes.enum}>
              <Box className={classes.line}>
                <Box className={classes.oneLine}>
                  <span className={classes.label}>Date de réunion : </span>
                  <span className={classes.value}>{`${moment(compteRendu.data?.createdAt).format('L')}`}</span>
                </Box>
                <Box className={classes.oneLine}>
                  <span className={classes.label}>Auteur : </span>
                  <span className={classes.value}>{`${compteRendu.data?.animateur.fullName || '-'}`}</span>
                </Box>
                <Box className={classes.oneLine}>
                  <span className={classes.label}>Nbre de présent : </span>
                  <span className={classes.value}>{`${compteRendu.data?.participants.length || 0}`}</span>
                </Box>
                <Box className={classes.toPresentList} onClick={toggleOpenListePresence}>
                  Voir la liste de présence
                </Box>
              </Box>
            </Box>
            <Box className={classes.resume}>
              <FormControl className={classes.resumeForm} variant="outlined">
                <InputLabel htmlFor="component-outlined">Résumé </InputLabel>
                <OutlinedInput
                  id="component-outlined"
                  value={strippedString(compteRendu.data?.description || '')}
                  multiline
                  label="Résumé"
                />
              </FormControl>
            </Box>
          </Box>
          <Box className={classes.lastContent}>
            <Table
              style={{ padding: 24 }}
              columns={columns}
              data={compteRendu?.data?.actions || ([] as any)}
              search={false}
            />
          </Box>
          <Box />
        </Box>
      </CustomContainer>
      <CustomModal
        withBtnsActions={false}
        closeIcon
        headerWithBgColor
        fullWidth
        maxWidth="sm"
        title="Liste de présence"
        open={openListePresenceDialog}
        setOpen={setOpenListePresenceDialog}
      >
        <CollaboratorList {...(collaborateurs as any)} selectable={false} />
      </CustomModal>
    </Box>
  );
};

export default DetailsCompteRendu;
