import { Box, CircularProgress, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { strippedString } from '@lib/ui-kit/src/services/Helpers';
import InfoList, { InfoListItem } from '@lib/ui-kit/src/components/atoms/InfoList/InfoList';
import { Reunion } from '../../CompteRenduFormPage/types';
import useStyles from './styles';
interface MobileMainContentProps {
  fiche?: Reunion;
}

const MobileMainContent: FC<MobileMainContentProps> = ({ fiche }) => {
  const classes = useStyles();

  const presence: InfoListItem = {
    title: 'Présence',
    content: fiche?.participants?.map((participant) => ({
      label: 'Nom',
      value: participant?.fullName,
    })),
  };

  const action: InfoListItem = {
    title: "Plan d'Actions",
    content: fiche?.actions?.map((action) => ({
      label: action?.type,
      value: strippedString(action?.typeAssocie?.description || ''),
    })),
  };

  const description: InfoListItem = {
    title: 'Description',
    children: (
      <fieldset className={classes.descriptionFieldset}>
        <legend>Description</legend>
        <Typography>{strippedString(fiche?.description || '')}</Typography>
      </fieldset>
    ),
  };

  if (!fiche) {
    return (
      <Box width="100%" height="100%" justifyContent="center" alignItems="center" display="flex">
        <CircularProgress color="secondary" />
      </Box>
    );
  }

  return (
    <Box padding="16px">
      <InfoList info={presence} />
      <InfoList info={action} />
      <InfoList info={description} />
    </Box>
  );
};

export default MobileMainContent;
