import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import { Comment } from '@lib/ui-kit/src/components/atoms/Comment';
import useStyles from './style';

interface ListePresenceProps {
  data: any[];
}

const ListePresence: FC<ListePresenceProps> = ({ data }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      {data.map((currentData: any, index: number) => (
        <Comment
          key={currentData.id || index}
          comment={{
            user: {
              userName: currentData.name,
            },
            content: currentData.comment,
          }}
        />
      ))}
    </Box>
  );
};

export default ListePresence;
