/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable react/jsx-no-bind */
import { CssBaseline, Fade, Hidden, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Add, Delete, Edit, MoreHoriz, MoreVert, Visibility } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, MouseEvent, useEffect, useState } from 'react';
import SwipeableContainer from '@lib/ui-kit/src/components/molecules/SwipeableContainer';
import { ParameterContainer } from '@lib/ui-kit/src/components/templates/ParameterContainer';
import { isMobile, strippedString } from '@lib/ui-kit/src/services/Helpers';
import {
  Backdrop,
  Column,
  CustomAvatarGroup,
  CustomButton,
  Table,
  ConfirmDeleteDialog,
} from '@lib/ui-kit/src/components/atoms';
import { Reunion } from '../CompteRenduFormPage/types';
import { CustomDatePicker } from '@lib/ui-kit/src/components/atoms/CustomDateTimePicker';
import MobileMainContent from './MobileMainContent';
import useStyles from './style';
import { ReunionChange } from './types';
export interface CompteRenduPageProps {
  loading?: boolean;
  error?: Error;
  data?: Reunion[];
  rowsTotal: number;

  onRequestGoBack: () => void;
  onRequestAdd: () => void;
  onRequestShowDetail: (fiche: Reunion) => void;
  onRequestEdit: (fiche: Reunion) => void;
  onRequestDelete: (fiches: Reunion[]) => void;
  onRequestSearch: (change: ReunionChange) => void;
  onRequestOpenFormModal: (fiche?: Reunion) => void;

  //urgenceFilter: ReactNode;
  // importanceFilter: ReactNode;

  filterIcon: any;
}

const CompteRenduPage: FC<CompteRenduPageProps> = ({
  loading,
  error,
  data,
  rowsTotal,

  onRequestAdd,
  onRequestDelete,
  onRequestEdit,
  onRequestGoBack,
  onRequestSearch,
  onRequestShowDetail,
  onRequestOpenFormModal,
  filterIcon,
}) => {
  const classes = useStyles();
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(12);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [reunionToEdit, setReunionToEdit] = useState<Reunion | undefined>();
  const [searchText, setSearchText] = useState<string>('');
  const [dateCreationFiltersSelected, setDateCreationFiltersSelected] = useState<any | null>(null);
  const [rowsSelected, setRowsSelected] = useState<Reunion[]>([]);

  const open = Boolean(anchorEl);

  useEffect(() => {
    onRequestSearch({
      skip,
      take,
      searchText,
      dateCreationFiltersSelected,
    });
  }, [skip, take, searchText, dateCreationFiltersSelected]);

  const handleShowMenuClick = (reunion: Reunion, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    setReunionToEdit(reunion);
    setAnchorEl(event.currentTarget);
  };

  const consultReunion = (): void => {
    if (reunionToEdit) {
      onRequestShowDetail(reunionToEdit);
    }
  };

  const editReunion = (): void => {
    if (!isMobile() && reunionToEdit) {
      onRequestEdit(reunionToEdit);
    } else {
      onRequestOpenFormModal(reunionToEdit);
    }
  };

  const handleConfirmDeleteOne = (): void => {
    if (reunionToEdit) {
      onRequestDelete([reunionToEdit]);
      setOpenDeleteDialog(false);
    }
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const actionBtn = (row: any) => {
    if (row) {
      return (
        <>
          <CssBaseline />
          <IconButton
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={handleShowMenuClick.bind(null, row)}
            color="inherit"
            id={`fiche-menu-icon-${row?.id}`}
          >
            {isMobile() ? <MoreVert /> : <MoreHoriz />}
          </IconButton>
          <Menu
            id={`fiche-incident-menu-${row?.id}`}
            anchorEl={anchorEl}
            keepMounted
            open={open && row?.id === reunionToEdit?.id}
            onClose={handleClose}
            TransitionComponent={Fade}
          >
            <Hidden smDown={true} implementation="css">
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                  handleClose();
                  consultReunion();
                }}
                disabled={false}
              >
                <ListItemIcon>
                  <Visibility />
                </ListItemIcon>
                <Typography variant="inherit">Voir détails</Typography>
              </MenuItem>
            </Hidden>

            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                editReunion();
              }}
              disabled={row.statut?.code === 'CLOTURE'}
            >
              <ListItemIcon>
                <Edit />
              </ListItemIcon>
              <Typography variant="inherit">Modifier</Typography>
            </MenuItem>

            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                setOpenDeleteDialog(true);
              }}
              disabled={row.statut?.code === 'CLOTURE'}
            >
              <ListItemIcon>
                <Delete />
              </ListItemIcon>
              <Typography variant="inherit">Supprimer</Typography>
            </MenuItem>
          </Menu>
        </>
      );
    } else return <></>;
  };

  const columns: Column[] = [
    {
      name: 'createdAt',
      label: 'Date de réunion',
      renderer: (row: Reunion) => {
        return row.createdAt ? moment(new Date(row.createdAt)).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'animateur.fullName',
      label: 'Animateur',
      renderer: (row: Reunion) => {
        return row?.animateur?.fullName ? row.animateur.fullName : '-';
      },
    },
    {
      name: 'responsable.fullName',
      label: 'Responsable',
      renderer: (row: Reunion) => {
        return row?.responsable?.fullName ? row.responsable.fullName : '-';
      },
    },
    {
      name: 'description',
      label: 'Description',
      renderer: (row: Reunion) => {
        return row?.description ? strippedString(row?.description) : '-';
      },
    },
    {
      name: 'participants',
      label: 'Collaborateurs invités',
      renderer: (row: Reunion) => <CustomAvatarGroup max={3} users={(row.participants || []) as any} />,
    },
    {
      name: '',
      label: 'Statistique',
      renderer: (reunion: Reunion) => {
        return `${reunion.nombreTotalClotureActions || 0}/${reunion.nombreTotalActions || 0}`;
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: Reunion) => {
        return <div key={`row-${row.id}`}>{actionBtn(row)}</div>;
      },
    },
  ];

  const [openDrawer, setOpenDrawer] = useState(false);

  const handleDrawerToggle = () => {
    setOpenDrawer(!openDrawer);
  };

  const handleShowFiltersClick = () => {
    setOpenDrawer(!openDrawer);
  };

  const fiche = data && data[0];

  const optionBtn = [
    <IconButton
      className={classes.iconAction}
      color="inherit"
      aria-label="settings"
      edge="start"
      onClick={handleShowFiltersClick}
    >
      {filterIcon}
    </IconButton>,
    actionBtn(fiche),
  ];

  const handleSwipePrevious = () => {
    if (skip > 0) {
      setSkip(skip - 1);
    }
  };

  const handleSwipeNext = () => {
    if (skip + 1 < rowsTotal) setSkip(skip + 1);
  };

  return (
    <>
      <ParameterContainer
        sideNavProps={{
          title: 'Réunion',
          searchPlaceholder: 'Rechercher ...',
          onRequestGoBack: () => onRequestGoBack(),
          onRequestSearchText: setSearchText,
          dropdownFilters: [
            {
              title: 'Date de réunion',
              children: (
                <CustomDatePicker
                  placeholder="Date"
                  onChange={(value: any) => setDateCreationFiltersSelected(value)}
                  name="date"
                  value={dateCreationFiltersSelected}
                  disablePast={false}
                />
              ),
            } /*,
            {
              title: 'Urgence',
              children: urgenceFilter,
            },
            {
              title: 'Importance',
              children: importanceFilter,
            },*/,
          ],
        }}
        topBarProps={{
          title: 'Liste des réunions',
          actionButtons: [
            <CustomButton key={`action-button-${0}`} color="secondary" onClick={onRequestAdd} startIcon={<Add />}>
              AJOUTER UNE REUNION
            </CustomButton>,
          ],
        }}
        mainContent={
          loading ? (
            <Backdrop open={loading} value="Récupération de données..." />
          ) : isMobile() ? (
            <SwipeableContainer onNext={handleSwipeNext} onPrevious={handleSwipePrevious}>
              <MobileMainContent fiche={fiche} />
            </SwipeableContainer>
          ) : (
            <Table
              style={{ padding: 24 }}
              error={error}
              loading={loading}
              search={false}
              data={data || []}
              selectable={false}
              rowsSelected={rowsSelected}
              onRowsSelectionChange={(newRowsSelected) => setRowsSelected(newRowsSelected as Reunion[])}
              columns={columns}
              onRunSearch={({ skip, take }) => {
                setSkip(skip);
                setTake(take);
              }}
              rowsTotal={rowsTotal}
            />
          )
        }
        optionBtn={[optionBtn]}
        handleDrawerToggle={handleDrawerToggle}
        openDrawer={openDrawer}
        drawerTitle={`Réunions ${rowsTotal !== 0 ? `(${skip + 1}/${rowsTotal})` : ``}`}
        onGoBack={onRequestGoBack}
      />
      <ConfirmDeleteDialog
        title="Suppression de réunion"
        content="Etes-vous sûr de vouloir supprimer cette réunion?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDeleteOne}
      />
    </>
  );
};

export default CompteRenduPage;
