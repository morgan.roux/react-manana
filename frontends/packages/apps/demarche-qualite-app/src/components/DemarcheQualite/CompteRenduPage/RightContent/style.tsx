import { createStyles, makeStyles } from "@material-ui/styles";

const useStyles = makeStyles( () => 
    createStyles({
        root : {

        },
        header : {
            width : '100%',
            display : 'flex',
            flexDirection : 'row',
            justifyContent : 'space-between',
            padding : 10
        },
        headerTitle : {
            fontWeight : 'bold'
        },
        tableCaption : {
            width : '100%',
            display : 'flex',
            flexDirection : 'row',
            justifyContent : 'space-between',
            marginBottom : 10
        },
        dataContainer : {
            display : 'flex',
            flexDirection : 'column',
            marginTop : 8
        },
        tableContent : {

        },
        selectedElement : {
            fontWeight : 'bold'
        }
    })
);

export default useStyles;