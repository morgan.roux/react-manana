import CompteRenduPage from './CompteRenduPage';
import DetailsCompteRendu from './RightContent/DetailsCompteRendu/DetailsCompteRendu';
export { ReunionChange } from './types';
export { CompteRenduPage, DetailsCompteRendu };
