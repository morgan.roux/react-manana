import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {},
    content: {
      width: '80%',
      margin: '0 auto',
      display: 'flex',
      flexDirection: 'column',
    },
    firstContent: {
      display: 'flex',
      flexDirection: 'column',
      marginBottom: 10,
      background: '#FEFEFE 0% 0% no-repeat padding-box',
      boxShadow: '0px 2px 12px #14141429',
      borderRadius: 6,
      marginTop: '20px',
      padding: 20,
    },
    line: {
      display: 'flex',
      flexDirection: 'row',
    },
    resumeTitle: {
      display: 'block',
      position: 'relative',
      top: '-20px',
      background: '#fff',
      width: '9%',
    },
    toPresentList: {
      fontWeight: 'bold',
      color: '#E34168',
      cursor: 'pointer',
      textDecoration: 'underline',
    },
    lastContent: {},
    enum: {},
    resume: {
      position: 'relative',
      marginTop: 15,
      marginBottom: 10,
      padding: 10,
    },
    resumeForm: {
      width: '-webkit-fill-available',
    },
    label: {
      color: 'gray',
      marginRight: 8,
    },
    value: {
      fontWeight: 'bold',
    },
    statutTodoLabel: {
      marginRight: 7,
    },
    todoLink: {
      fontWeight: 'bold',
      textDecoration: 'underline',
      cursor: 'pointer',
    },
    oneLine: {
      marginRight: 18,
    },

    visibilityIcon: {
      cursor: 'pointer',
    },
  })
);

export default useStyles;
