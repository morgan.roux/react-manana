import { makeStyles } from '@material-ui/core';

export default makeStyles((theme) => ({
  containerCheckList: {
    width: '75%',
    margin: '32px auto 0',
  },
  borderBox: {
    margin: theme.spacing(2, 0),
    padding: theme.spacing(1.5),
    border: '1px solid #E6E6E6',
    borderRadius: 6,
  },
  flex: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  inputSection: {
    '& .main-MuiOutlinedInput-multiline': {
      padding: '15px 0px 0px 20px',
    },
  },
}));
