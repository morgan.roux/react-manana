import React, { FC, useState, MouseEvent, useEffect } from 'react';
import { CustomContainer } from '@lib/ui-kit/src/components/templates/CustomContainer';
import { Backdrop } from '@lib/ui-kit';
import { MultiSectionsFieldsGroup, SectionFieldsGroupValue } from '@lib/ui-kit';
import { Checklist, ChecklistSection } from '../ChecklistPage/ChecklistPage';
import { Box, TextField, Typography } from '@material-ui/core';
import style from './style';
import classnames from 'classnames';
import { FormButtons } from '@lib/ui-kit';

export interface ChecklistFormPageProps {
  mode: 'creation' | 'modification';
  loading?: boolean;
  saving?: boolean;
  checklist?: Checklist;
  onRequestSave: (checklist: Checklist) => void;
  onRequestGoBack: (event: MouseEvent) => void;
}

const toSectionValues = (sections?: ChecklistSection[]): SectionFieldsGroupValue[] => {
  if (sections) {
    return sections.map(({ id, ordre, libelle, items }) => {
      return {
        name: id || '',
        ordre,
        value: libelle,
        fieldValues: (items || []).map((item) => {
          return {
            name: item.id || '',
            ordre: item.ordre,
            value: item.libelle,
            deletable: item.nombreEvaluations != undefined && item.nombreEvaluations > 0,
          };
        }),
      };
    });
  }

  return [];
};

const fromSectionValues = (sectionsValues: SectionFieldsGroupValue[]): ChecklistSection[] => {
  if (sectionsValues) {
    return sectionsValues.map(({ name, ordre, value, fieldValues }) => {
      return {
        id: name,
        ordre,
        libelle: value,
        items: (fieldValues || []).map((fieldValue) => {
          return {
            id: fieldValue.name,
            ordre: fieldValue.ordre,
            libelle: fieldValue.value,
          };
        }),
      };
    });
  }

  return [];
};

const ChecklistFormPage: FC<ChecklistFormPageProps> = ({
  checklist,
  mode,
  onRequestSave,
  onRequestGoBack,
  loading,
  saving,
}) => {
  const classes = style();

  const [ordre, setOrdre] = useState<number | undefined>();
  const [libelle, setLibelle] = useState<string>('');
  const [sectionValues, setSectionValues] = useState<SectionFieldsGroupValue[]>([]);

  useEffect(() => {
    if (checklist) {
      setLibelle(checklist.libelle);
      setOrdre(checklist.ordre);

      setSectionValues(toSectionValues(checklist.sections));
    }
  }, [checklist]);

  const emptyFields = (): boolean => !ordre || !libelle;

  const handleSaveClick = () => {
    onRequestSave({
      id: checklist?.id,
      ordre: ordre || 1,
      libelle,
      sections: fromSectionValues(sectionValues),
    });
  };

  if (loading) {
    return <Backdrop />;
  }

  return (
    <CustomContainer
      filled={true}
      onBackClick={onRequestGoBack}
      onlyBackAndTitle
      bannerContentStyle={{
        justifyContent: 'center',
      }}
      bannerBack={true}
      bannerTitle={mode === 'creation' ? 'Ajout de check-list' : 'Modification de check-list'}
    >
      <Box className={classes.containerCheckList}>
        <Typography variant="h5" color="secondary">
          Informations de base
        </Typography>

        <Box className={classnames(classes.borderBox, classes.flex)}>
          <Typography>Numéros d'ordre</Typography>
          <TextField
            variant="outlined"
            name="ordre"
            value={ordre}
            onChange={(event) => setOrdre(parseInt(event.target.value, 10))}
            placeholder="Ordre"
            type="number"
          />
        </Box>

        <TextField
          variant="outlined"
          name="nom"
          label="Nom de l'outil"
          onChange={(event) => setLibelle(event.target.value)}
          value={libelle}
          rows={1}
          inputProps={{
            style: { resize: 'both' },
          }}
          InputLabelProps={{ shrink: true }}
          multiline
          fullWidth
          required
        />

        <Typography variant="h5" color="secondary" style={{ marginTop: 15 }}>
          Check-list
        </Typography>

        <Box className={classes.borderBox}>
          <MultiSectionsFieldsGroup
            addFieldLabel="Ajouter un check-list"
            addSectionLabel="Ajouter une section"
            fieldPlaceholder="Votre question"
            sectionPlaceholder="Titre de la section"
            values={sectionValues}
            onChange={setSectionValues}
            inputClassName={classes.inputSection}
          />
        </Box>
        <FormButtons
          onClickCancel={onRequestGoBack}
          onClickConfirm={handleSaveClick}
          disableConfirm={saving || emptyFields()}
        />
      </Box>
    </CustomContainer>
  );
};

export default ChecklistFormPage;
