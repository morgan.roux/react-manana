import { DQMTFonction, DQMTTache, Importance, Urgence } from '@lib/types';
import { Fichier, User } from '@lib/ui-kit/src/components/pages/DashboardPage';
import { Reunion } from '../CompteRenduFormPage/types';
import { Statut } from '../FicheIncidentPage';

export interface Activite {
  id: string;
  libelle: string;
}

export interface Perception {
  id: string;
  libelle: string;
}

export interface Origine {
  id: string;
  libelle: string;
}

export interface Correspondant {
  value: string;
}

export interface FicheAmeliorationAction {
  id: string;
  description: string;
  dateHeureMiseEnplace: string;
  createdAt: string;
  updatedAt: string;
  fichiers?: Fichier[];
}

export interface FicheType {
  id: string;
  libelle: string;
}

export interface FicheCause {
  id: string;
  libelle: string;
}

export interface FicheAmelioration {
  id: string;
  description: string;
  createdAt: string;
  dateAmelioration: string;
  idOrigineAssocie: string;
  origineAssocie?: any;
  updatedAt: string;
  action?: FicheAmeliorationAction;
  origine: Origine;
  fichiers?: Fichier[];
  type: FicheType;
  cause: FicheCause;
  auteur: User;
  statut: Statut;
  urgence: Urgence;
  importance: Importance;
  tache: DQMTTache;
  fonction: DQMTFonction;
  participants?: User[];
  participantsAInformer?: User[];
  isPrivate: boolean;
  updatedBy?: {
    id: string;
    fullName: string;
  };
  reunion?: Reunion;
}
