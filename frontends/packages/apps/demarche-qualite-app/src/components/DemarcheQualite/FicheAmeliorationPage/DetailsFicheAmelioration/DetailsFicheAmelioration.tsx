import { Dropzone } from '@lib/common';
import { Box, Card, Divider, TextField, Chip } from '@material-ui/core';
import PdfIcon from '@material-ui/icons/PictureAsPdf';
import moment from 'moment';
import React, { FC, MouseEvent, useEffect, useState } from 'react';
import { strippedString } from '@lib/ui-kit/src/services/Helpers';
import {
  CustomButton,
  CustomFormTextField,
  CustomModal,
  NoItemContentImage,
  ConfirmDeleteDialog,
} from '@lib/ui-kit/src/components/atoms';
import { Backdrop } from '@lib/ui-kit/src/components/atoms/Backdrop';
import { LoaderSmall } from '@lib/ui-kit/src/components/atoms/Loader';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import { CustomDatePicker } from '@lib/ui-kit/src/components/atoms/CustomDateTimePicker';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import { FicheAmelioration, FicheAmeliorationAction } from '../types';
import LineDetail from './LineDetail/LineDetail';
import useStyles from './style';

export interface RequestSavingFicheAmeliorationAction {
  description: string;
  dateHeureMiseEnplace: string;
  fichiers: File[];
}

interface DetailsFicheAmeliorationProps {
  noContentActionImageSrc: string;
  ficheAmelioration?: FicheAmelioration;
  loading?: boolean;
  error?: Error;

  ficheAmeliorationAction?: {
    loading?: boolean;
    saving?: boolean;
    error?: Error;
    errorSaving?: Error;
    data?: FicheAmeliorationAction;
  };

  onRequestGoBack: (event: MouseEvent) => void;
  onRequestDelete: (ficheAmelioration: FicheAmelioration) => void;
  onRequestEdit: (ficheAmelioration: FicheAmelioration) => void;
  onRequestSaveAction: (
    ficheAmeliotation: FicheAmelioration,
    toSaveAction: RequestSavingFicheAmeliorationAction
  ) => void;
}

const DetailsFicheAmelioration: FC<DetailsFicheAmeliorationProps> = ({
  noContentActionImageSrc,
  ficheAmelioration,
  loading,
  error,
  ficheAmeliorationAction,
  onRequestGoBack,
  onRequestEdit,
  onRequestDelete,
  onRequestSaveAction,
}) => {
  const classes = useStyles();

  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  const [description, setDescription] = useState<string>('');
  const [dateHeureMiseEnplace, setDateHeureMiseEnplace] = useState<any | null>(null);

  const [openCreateDialog, setOpenCreateDialog] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  useEffect(() => {
    if (ficheAmeliorationAction?.errorSaving) {
      setOpenCreateDialog(true);
    }
  }, [ficheAmeliorationAction?.errorSaving]);

  const handleRequestEdit = () => {
    if (ficheAmelioration) {
      onRequestEdit(ficheAmelioration);
    }
  };

  const handleConfirmDelete = () => {
    if (ficheAmelioration) {
      onRequestDelete(ficheAmelioration);
      setOpenDeleteDialog(false);
    }
  };

  const handleConfirmAddAction = () => {
    if (ficheAmelioration) {
      setOpenCreateDialog(false);
      onRequestSaveAction(ficheAmelioration, {
        fichiers: selectedFiles,
        description,
        dateHeureMiseEnplace: moment(dateHeureMiseEnplace).toISOString(),
      });
    }
  };

  const isEmptyFields = (): boolean => !description || !dateHeureMiseEnplace;

  if (error) {
    return <ErrorPage />;
  }

  if (loading || ficheAmeliorationAction?.saving) {
    return <Backdrop />;
  }

  return (
    <>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<span>Êtes-vous sur de vouloir supprimer cette fiche d'amélioration ? </span>}
        onClickConfirm={handleConfirmDelete}
      />
      <CustomModal
        open={openCreateDialog}
        setOpen={setOpenCreateDialog}
        closeIcon={true}
        headerWithBgColor={true}
        fullWidth={true}
        maxWidth="sm"
        title={"Ajouter une action d'amélioration"}
        withBtnsActions={true}
        disabledButton={ficheAmeliorationAction?.saving || isEmptyFields()}
        onClickConfirm={handleConfirmAddAction}
      >
        <Box className={classes.actionForms}>
          <Box className={classes.actionForm}>
            <CustomDatePicker
              label="Date de mise en place"
              placeholder="Date début"
              onChange={(event) => setDateHeureMiseEnplace(event)}
              name="date"
              value={dateHeureMiseEnplace}
              disablePast={false}
              className={classes.actionDateInput}
            />
          </Box>
          <Box className={classes.actionForm}>
            <CustomFormTextField
              multiline={true}
              label="Action"
              value={description}
              onChange={(event) => setDescription((event.target as any).value)}
              className={classes.actionFormAction}
            />
          </Box>
          <Box className={classes.actionForm}>
            <Dropzone selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles} />
          </Box>
        </Box>
      </CustomModal>

      <CustomContainer
        filled={true}
        bannerBack={true}
        bannerTitle="Détails d'une amélioration"
        onBackClick={onRequestGoBack}
        bannerContentStyle={{
          justifyContent: 'unset',
        }}
        bannerTitleStyle={{
          textAlign: 'center',
          flexGrow: 1,
        }}
      >
        <Box className={classes.detailsContent}>
          <Card className={classes.infoBox}>
            <Box>
              <span className={classes.span}>Informations générales</span>
              <LineDetail label="Auteur" value={ficheAmelioration?.auteur?.fullName || '-'} />
              <LineDetail label="Origine" value={ficheAmelioration?.origine?.libelle || '-'} />
              <LineDetail
                label="Date de déclaration"
                value={ficheAmelioration?.createdAt ? moment(ficheAmelioration?.createdAt).format('DD/MM/YYYY') : '-'}
              />
            </Box>
            {ficheAmelioration?.fichiers && (
              <Box style={{ marginBottom: 20 }}>
                <span className={classes.fichierJointLabel}>Fichier(s) joint(s)</span>
                <Box>
                  {ficheAmelioration?.fichiers.map((fiche) => {
                    return (
                      <Chip
                        className={classes.ficheBox}
                        key={fiche.id}
                        icon={<PdfIcon />}
                        clickable={true}
                        label={fiche.nomOriginal}
                        component="a"
                        target="_blank"
                        href={fiche.publicUrl}
                      />
                    );
                  })}
                </Box>
              </Box>
            )}

            <Divider />
            <Box>
              <span className={classes.span}>Actions d'amélioration</span>
              {!ficheAmeliorationAction?.data ? (
                <LineDetail
                  label="Action"
                  value={'Non'}
                  link={{
                    label: 'Ajouter une action',
                    onClick: () => setOpenCreateDialog(true),
                  }}
                />
              ) : (
                <LineDetail label="Action" value={strippedString(ficheAmeliorationAction.data.description || '')} />
              )}
              <LineDetail label="Réunion d'équipe" value="Non" />
            </Box>
            <Divider />
            <Box style={{ marginBottom: 12 }}>
              <span className={classes.span}></span>

              <TextField
                className={classes.descriptionField}
                fullWidth={true}
                multiline={true}
                disabled={true}
                variant="outlined"
                label="Description du problème"
                value={strippedString(ficheAmelioration?.description || '')}
              />
            </Box>
            <Box className={classes.buttonContainer}>
              <CustomButton
                className={classes.buttonContainerButton}
                onClick={() => setOpenDeleteDialog(true)}
                disabled={!!ficheAmelioration?.reunion?.id}
              >
                Supprimer
              </CustomButton>
              <CustomButton className={classes.buttonContainerButton} color="secondary" onClick={handleRequestEdit}>
                Modifier
              </CustomButton>
            </Box>
          </Card>
          <Card className={ficheAmeliorationAction?.data ? classes.actionBox : classes.actionBoxBorder}>
            {ficheAmeliorationAction?.loading ? (
              <LoaderSmall />
            ) : ficheAmeliorationAction?.error ? (
              <ErrorPage />
            ) : ficheAmeliorationAction?.data ? (
              <Box>
                <span className={classes.span}>Action d'amélioration</span>
                <Divider className={classes.marginDevider} />
                <Box>
                  <LineDetail
                    label="Date de mise en place"
                    value={
                      ficheAmeliorationAction?.data?.dateHeureMiseEnplace
                        ? moment(new Date(ficheAmeliorationAction.data.dateHeureMiseEnplace)).format('DD/MM/YYYY')
                        : '-'
                    }
                  />

                  <TextField
                    className={(classes.descriptionField, classes.bgDecriptionField)}
                    fullWidth={true}
                    multiline={true}
                    disabled={true}
                    variant="outlined"
                    label="Description"
                    value={strippedString(ficheAmeliorationAction.data.description || '')}
                  />

                  {ficheAmeliorationAction?.data?.fichiers && (
                    <Box>
                      <span className={classes.problemeLabel}>Fichier(s) joint(s)</span>

                      <Box>
                        {ficheAmeliorationAction.data.fichiers.map((fiche) => {
                          return (
                            <Chip
                              className={classes.ficheBox}
                              key={fiche.id}
                              icon={<PdfIcon />}
                              clickable={true}
                              label={fiche.nomOriginal}
                              component="a"
                              target="_blank"
                              href={fiche.publicUrl}
                            />
                          );
                        })}
                      </Box>
                    </Box>
                  )}
                </Box>
              </Box>
            ) : (
              <NoItemContentImage
                src={noContentActionImageSrc}
                title="Ajout d'action d'amélioration"
                subtitle="Vous pouvez ajouter une action à tout moment pour résoudre le problème"
              >
                <CustomButton
                  className={classes.addActionButton}
                  color="secondary"
                  onClick={() => setOpenCreateDialog(true)}
                >
                  Apporter une action
                </CustomButton>
              </NoItemContentImage>
            )}
          </Card>
        </Box>
      </CustomContainer>
    </>
  );
};

export default DetailsFicheAmelioration;
