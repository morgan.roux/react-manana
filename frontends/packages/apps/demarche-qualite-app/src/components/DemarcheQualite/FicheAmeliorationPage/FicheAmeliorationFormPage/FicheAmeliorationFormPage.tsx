import { Dropzone, User } from '@lib/common';
import { Button, TextField } from '@material-ui/core';
import React, { FC, MouseEvent, useEffect, useState } from 'react';
import { CustomSelect } from '@lib/ui-kit/src/components/atoms';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import { Backdrop } from '@lib/ui-kit/src/components/atoms/Backdrop';
import { CustomFormSection } from '@lib/ui-kit/src/components/templates/CustomFormSection';
import { ErrorPage } from '@lib/ui-kit/src/components/pages/ErrorPage';
import { FicheAmelioration, Origine } from '../types';

export interface RequestSavingFicheAmelioration {
  id?: string;
  description: string;
  idOrigine: string;
  idUserAuteur: string;
  fichiers: File[];
}

interface FicheAmeliorationFormPageProps {
  mode: 'creation' | 'modification';
  ficheAmelioration?: FicheAmelioration;
  loading?: boolean;
  saving?: boolean;
  error?: Error;

  origines?: {
    loading?: boolean;
    error?: Error;
    data?: Origine[];
  };

  collaborateurs?: {
    loading?: boolean;
    error?: Error;
    data?: User[];
  };

  onRequestGoBack: (event: MouseEvent) => void;
  onRequestSave: (toSaveAmelioration: RequestSavingFicheAmelioration) => void;
}

const FicheAmeliorationFormPage: FC<FicheAmeliorationFormPageProps> = ({
  mode,
  ficheAmelioration,
  loading,
  saving,
  error,
  origines,
  collaborateurs,
  onRequestGoBack,
  onRequestSave,
}) => {
  const [description, setDescription] = useState<string>('');
  const [idOrigine, setIdOrigine] = useState<string>('');
  const [idUserAuteur, setIdUserAuteur] = useState<string>('');
  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);

  useEffect(() => {
    if (ficheAmelioration) {
      setDescription(ficheAmelioration.description);
      setIdOrigine(ficheAmelioration.origine.id);
      setIdUserAuteur(ficheAmelioration.auteur.id);
      if (ficheAmelioration.fichiers) {
        setSelectedFiles(
          ficheAmelioration.fichiers.map((fichier: any) => ({
            ...fichier,
            name: fichier.nomOriginal,
          }))
        );
      }
    }
  }, [loading, ficheAmelioration]);

  const handleSaveClick = () => {
    if (idOrigine && idUserAuteur) {
      onRequestSave({
        description,
        idOrigine,
        idUserAuteur,
        id: ficheAmelioration?.id,
        fichiers: selectedFiles,
      });
    }
  };

  if (error) {
    return <ErrorPage />;
  }

  if (loading) {
    return <Backdrop />;
  }

  return (
    <CustomContainer
      bannerBack
      filled
      bannerTitle={`${mode === 'modification' ? 'Modification' : 'Ajout'} d'amélioration`}
      onBackClick={onRequestGoBack}
      bannerRight={
        <Button variant="contained" disabled={saving} color="secondary" onClick={handleSaveClick}>
          {`${mode === 'modification' ? 'Enregistrer' : 'Créer'}`}
        </Button>
      }
      contentStyle={{
        marginTop: 24,
        width: '100%',
        maxWidth: 785,
      }}
    >
      <CustomFormSection title="Informations générales" bordered={false}>
        <CustomSelect
          disabled={collaborateurs?.loading}
          error={collaborateurs?.error}
          listId="id"
          index="fullName"
          label="Auteur"
          value={idUserAuteur}
          onChange={(event: any) => setIdUserAuteur(event.target.value)}
          list={collaborateurs?.data || []}
        />

        <CustomSelect
          disabled={origines?.loading}
          error={origines?.error}
          key="origines"
          listId="id"
          index="libelle"
          label="Origine"
          value={idOrigine}
          onChange={(event: any) => setIdOrigine(event.target.value)}
          list={origines?.data || []}
        />
        <Dropzone multiple setSelectedFiles={setSelectedFiles} selectedFiles={selectedFiles} />
      </CustomFormSection>
      <CustomFormSection title="Incident potentiel" bordered={false}>
        <TextField
          variant="outlined"
          label="Description du problème"
          value={description}
          onChange={(event) => setDescription(event.target.value)}
          multiline
          fullWidth
          InputLabelProps={{ shrink: true }}
          rows={4}
        />
      </CustomFormSection>
    </CustomContainer>
  );
};

export default FicheAmeliorationFormPage;
