import { Box, FormControlLabel, Radio } from '@material-ui/core';
import React, { FC } from 'react';
import { CustomCheckBox } from '@lib/ui-kit/src/components/atoms';
import useStyles from './style';

interface LineFilterProps {
  type: string;
  label: string;
  nb: number;
  checked?: boolean;
  value?: string;
  setChecked?: () => void;
  handleChange?: (event: any, id: any) => void;
}

const LineFilter: FC<LineFilterProps> = ({ type, label, nb, checked, value, setChecked, handleChange }) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      {type === 'checkbox' ? (
        <Box>
          <CustomCheckBox checked={checked} label={label} onClick={setChecked} />
        </Box>
      ) : (
        <FormControlLabel control={<Radio onChange={handleChange} checked={checked} value={value} />} label={label} />
      )}
      <Box>{nb}</Box>
    </Box>
  );
};

export default LineFilter;
