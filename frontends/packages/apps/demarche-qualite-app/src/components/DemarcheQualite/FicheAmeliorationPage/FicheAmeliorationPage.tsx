/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable react/jsx-no-bind */
import { CssBaseline, Fade, Hidden, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Add, Delete, Edit, EventNote, MoreHoriz, MoreVert, Visibility, WbIncandescent } from '@material-ui/icons';
import moment from 'moment';
import React, { ChangeEvent, FC, MouseEvent, ReactNode, useEffect, useState } from 'react';
import SwipeableContainer from '@lib/ui-kit/src/components/molecules/SwipeableContainer';
import { ParameterContainer } from '@lib/ui-kit/src/components/templates/ParameterContainer';
import { isMobile, strippedColumnStyle, strippedString } from '@lib/ui-kit/src/services/Helpers';
import {
  Column,
  CustomAvatarGroup,
  CustomButton,
  CustomSelect,
  ImportanceLabel,
  Table,
  UrgenceLabel,
  ConfirmDeleteDialog,
} from '@lib/ui-kit/src/components/atoms';
import { Statut } from '../FicheIncidentPage/types';
import MobileMainContent from './MobileMainContent';
import useStyles from './style';
import { FicheAmelioration, Origine } from './types';
import { importanceToImpact } from '@lib/common';

export interface FicheAmeliorationChange {
  originesFiltersSelected: Origine[];
  statutsFiltersSelected: Statut[];
  hasActionFilterSelected: 'ALL' | 'NO' | 'YES';
  searchText: string;
  searchTextChanged: boolean;
  skip: number;
  take: number;
}

interface FicheAmeliotationActionStat {
  id: 'ALL' | 'YES' | 'NO';
  libelle?: string;
  total?: number;
}

interface FicheAmeliorationPageProps {
  loading?: boolean;
  error?: Error;
  data?: FicheAmelioration[];
  rowsTotal: number;

  actionStats: {
    loading?: boolean;
    error?: Error;
    data?: FicheAmeliotationActionStat[];
  };

  origines: {
    loading?: boolean;
    error?: Error;
    data?: Origine[];
  };

  statuts: {
    loading?: boolean;
    error?: Error;
    data?: Statut[];
  };

  urgenceFilter: ReactNode;
  importanceFilter: ReactNode;
  statutsInput: ReactNode;

  filterIcon: any;

  onRequestGoBack: () => void;
  onRequestAdd: () => void;
  onRequestShowDetail: (fiche: FicheAmelioration) => void;
  onRequestEdit: (fiche: FicheAmelioration) => void;
  onRequestDelete: (fiches: FicheAmelioration[]) => void;
  onRequestStatutChange: (fiche: FicheAmelioration, value: string) => void;
  onRequestSearch: (change: FicheAmeliorationChange) => void;
  onRequestOpenFormModal: (fiche?: FicheAmelioration) => void;
  onRequestGoReunion: (action: FicheAmelioration) => void;
}

const FicheAmeliorationPage: FC<FicheAmeliorationPageProps> = ({
  loading,
  error,
  data,
  rowsTotal,
  origines,
  actionStats,
  statuts,
  onRequestGoBack,
  onRequestAdd,
  onRequestShowDetail,
  onRequestDelete,
  onRequestEdit,
  onRequestSearch,
  onRequestStatutChange,
  onRequestOpenFormModal,
  onRequestGoReunion,
  urgenceFilter,
  importanceFilter,
  statutsInput,
  filterIcon,
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [ficheAmeliorationToEdit, setFicheAmeliorationToEdit] = useState<FicheAmelioration | undefined>();
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(12);
  const [searchText, setSearchText] = useState<string>('');
  const [originesFiltersSelected, setOriginesFiltersSelected] = useState<Origine[]>([]);
  const [statutsFiltersSelected, setStatutsFiltersSelected] = useState<Statut[]>([]);
  const [actionsFiltersSelected, setActionsFiltersSelected] = useState<FicheAmeliotationActionStat[]>([{ id: 'ALL' }]);

  const [rowsSelected, setRowsSelected] = useState<FicheAmelioration[]>([]);

  useEffect(() => {
    if (statuts.data) {
      setStatutsFiltersSelected(statuts.data.filter((element) => element.code !== 'CLOTURE'));
    }
  }, [statuts.loading]);

  useEffect(() => {
    onRequestSearch({
      skip,
      take,
      searchText,
      searchTextChanged: false,
      originesFiltersSelected,
      statutsFiltersSelected,
      hasActionFilterSelected: actionsFiltersSelected[0].id,
    });
  }, [skip, take, originesFiltersSelected, actionsFiltersSelected, statutsFiltersSelected]);

  useEffect(() => {
    onRequestSearch({
      skip,
      take,
      searchText,
      searchTextChanged: true,
      originesFiltersSelected,
      statutsFiltersSelected,
      hasActionFilterSelected: actionsFiltersSelected[0].id,
    });
  }, [searchText]);

  const open = Boolean(anchorEl);

  const handleShowMenuClick = (fiche: FicheAmelioration, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    setFicheAmeliorationToEdit(fiche);
    setAnchorEl(event.currentTarget);
  };

  const handleConfirmDeleteOne = (): void => {
    if (ficheAmeliorationToEdit) {
      onRequestDelete([ficheAmeliorationToEdit]);
      setOpenDeleteDialog(false);
    }
  };

  const changeStatut = (event: ChangeEvent<HTMLSelectElement>, fiche: FicheAmelioration): void => {
    if (fiche) {
      onRequestStatutChange(fiche, (event.target as HTMLSelectElement).value);
    }
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const consultFicheAmelioration = (): void => {
    if (ficheAmeliorationToEdit) {
      onRequestShowDetail(ficheAmeliorationToEdit);
    }
  };

  const editFicheAmelioration = (): void => {
    if (!isMobile() && ficheAmeliorationToEdit) {
      onRequestEdit(ficheAmeliorationToEdit);
    } else {
      onRequestOpenFormModal(ficheAmeliorationToEdit);
    }
  };

  const actionBtn = (row: any) => {
    if (row) {
      return (
        <>
          <CssBaseline />
          <IconButton
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={handleShowMenuClick.bind(null, row)}
            color="inherit"
            id={`fiche-menu-icon-${row?.id}`}
          >
            {isMobile() ? <MoreVert /> : <MoreHoriz />}
          </IconButton>
          <Menu
            id={`fiche-incident-menu-${row?.id}`}
            anchorEl={anchorEl}
            keepMounted
            open={open && row?.id === ficheAmeliorationToEdit?.id}
            onClose={handleClose}
            TransitionComponent={Fade}
          >
            <Hidden mdUp={true} implementation="css">
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                  handleClose();
                }}
                disabled={false}
              >
                <ListItemIcon>
                  <WbIncandescent />
                </ListItemIcon>
                <Typography variant="inherit">Apporter une solution</Typography>
              </MenuItem>
            </Hidden>
            <Hidden smDown={true} implementation="css">
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                  handleClose();
                  consultFicheAmelioration();
                }}
                disabled={false}
              >
                <ListItemIcon>
                  <Visibility />
                </ListItemIcon>
                <Typography variant="inherit">Voir détails</Typography>
              </MenuItem>
            </Hidden>

            <Hidden smDown={true} implementation="css">
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                  handleClose();
                  onRequestGoReunion(row);
                }}
                disabled={!row.reunion?.id}
              >
                <ListItemIcon>
                  <EventNote />
                </ListItemIcon>
                <Typography variant="inherit">Consulter la réunion</Typography>
              </MenuItem>
            </Hidden>

            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                editFicheAmelioration();
              }}
              disabled={row.statut?.code === 'CLOTURE'}
            >
              <ListItemIcon>
                <Edit />
              </ListItemIcon>
              <Typography variant="inherit">Modifier</Typography>
            </MenuItem>

            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                event.preventDefault();
                handleClose();
                setOpenDeleteDialog(true);
              }}
              disabled={row.statut?.code === 'CLOTURE' || row.reunion?.id}
            >
              <ListItemIcon>
                <Delete />
              </ListItemIcon>
              <Typography variant="inherit">Supprimer</Typography>
            </MenuItem>
          </Menu>
        </>
      );
    } else return <></>;
  };

  const findOrigine = (id?: string): any | undefined => {
    if (!id) {
      return undefined;
    }

    return (origines?.data || []).find((o) => o.id === id);
  };

  const columns: Column[] = [
    {
      name: 'type.libelle',
      label: 'Type',
      renderer: (row: FicheAmelioration) => {
        return row.type ? row.type.libelle : '-';
      },
    },
    {
      name: 'cause.libelle',
      label: 'Cause',
      renderer: (row: FicheAmelioration) => {
        return row.cause ? row.cause.libelle : '-';
      },
    },
    {
      name: 'description',
      label: 'Description du problème',
      renderer: (row: FicheAmelioration) => {
        return row.description ? strippedString(row.description) : '-';
      },
      style: strippedColumnStyle,
    },
    {
      name: 'origine',
      label: 'Origine',
      renderer: (row: FicheAmelioration) => {
        return findOrigine(row?.origine?.id)?.libelle || '-';
      },
    },
    {
      name: 'origine',
      label: 'Origine',
      renderer: (row: FicheAmelioration) => {
        return row?.origine?.id ? (origines?.data || []).find((o) => o.id === row.origine.id)?.libelle || '-' : '-';
      },
    },
    {
      name: 'correspondant',
      label: 'Correspondant',
      renderer: (row: FicheAmelioration) => {
        if (!row?.origineAssocie?.id || !row?.origine?.id) {
          return '-';
        }
        const origine = findOrigine(row.origine.id);
        return origine ? (origine.code === 'INTERNE' ? row.origineAssocie.fullName : row.origineAssocie.nom) : '-';
      },
    },

    {
      name: 'actionAmelioration',
      label: "Action d'amélioration",
      renderer: (row: any) => {
        return row.idAction || row.action ? 'Oui' : 'Non';
      },
    },
    {
      name: 'dateHeureDeclaration',
      label: 'Date de déclaration',
      renderer: (row: FicheAmelioration) => {
        return row.createdAt ? moment(row.createdAt).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'participants',
      label: 'Collaborateurs concernés',
      renderer: (row: FicheAmelioration) => (
        <CustomAvatarGroup max={3} sizes="30px" users={(row.participants || []) as any} />
      ),
    },
    {
      name: 'participantsAInformer',
      label: 'Collaborateurs à informer',
      renderer: (row: FicheAmelioration) => (
        <CustomAvatarGroup max={3} sizes="30px" users={(row.participantsAInformer || []) as any} />
      ),
    },
    {
      name: 'urgence',
      label: 'Urgence',
      renderer: (row: FicheAmelioration) => <UrgenceLabel urgence={row.urgence} />,
    },

    {
      name: 'importance',
      label: 'Impact',
      renderer: (row: FicheAmelioration) => (
        <ImportanceLabel importance={importanceToImpact(row.importance as any) as any} />
      ),
    },
    {
      name: 'statut',
      label: 'Statut',
      renderer: (row: FicheAmelioration) => {
        return (
          <CustomSelect
            key={row.id}
            disabled={statuts.loading || statuts.error}
            list={statuts?.data || []}
            listId="id"
            index="libelle"
            value={row?.statut?.id}
            variant="standard"
            onChange={(e: any) => changeStatut(e, row)}
          />
        );
      },
    },

    {
      name: '',
      label: '',
      renderer: (row: FicheAmelioration) => {
        return <div key={`row-${row.id}`}>{actionBtn(row)}</div>;
      },
    },
  ];

  const [openDrawer, setOpenDrawer] = useState(false);

  const handleDrawerToggle = () => {
    setOpenDrawer(!openDrawer);
  };

  const handleShowFiltersClick = () => {
    setOpenDrawer(!openDrawer);
  };

  const fiche = data && data[0];

  const optionBtn = [
    <IconButton
      className={classes.iconAction}
      color="inherit"
      aria-label="settings"
      edge="start"
      onClick={handleShowFiltersClick}
    >
      {filterIcon}
    </IconButton>,
    actionBtn(fiche),
  ];

  const handleSwipePrevious = () => {
    if (skip > 0) {
      setSkip(skip - 1);
    }
  };

  const handleSwipeNext = () => {
    if (skip + 1 < rowsTotal) setSkip(skip + 1);
  };

  return (
    <>
      <ParameterContainer
        sideNavProps={{
          title: 'Fiches améliorations',
          searchPlaceholder: 'Rechercher ...',
          onRequestGoBack: () => onRequestGoBack(),
          onRequestSearchText: setSearchText,
          dropdownFilters: [
            {
              title: 'Origine',
              filterList: origines,
              dataIdName: 'id',
              dataLabelName: 'libelle',
              dataCountName: 'nombreFicheAmeliorations',
              filtersSelected: originesFiltersSelected,
              onFiltersSelectionChange: (filters) => setOriginesFiltersSelected(filters as Origine[]),
            },
            {
              title: 'Statut',
              filterList: statuts,
              dataIdName: 'id',
              dataLabelName: 'libelle',
              dataCountName: 'nombreFicheAmeliorations',
              filtersSelected: statutsFiltersSelected,
              onFiltersSelectionChange: (filters) => setStatutsFiltersSelected(filters as Statut[]),
            },

            {
              title: "Action d'amélioration",
              dataIdName: 'id',
              dataLabelName: 'libelle',
              dataCountName: 'total',
              radio: true,
              filterList: actionStats,
              filtersSelected: actionsFiltersSelected,
              onFiltersSelectionChange: (filters) => {
                setActionsFiltersSelected(filters as FicheAmeliotationActionStat[]);
              },
            },
            {
              title: 'Urgence',
              children: urgenceFilter,
            },
            {
              title: 'Importance',
              children: importanceFilter,
            },
          ],
        }}
        topBarProps={{
          title: 'Liste des améliorations',
          actionButtons: [
            <CustomButton key={`action-button-${0}`} color="secondary" onClick={onRequestAdd} startIcon={<Add />}>
              AJOUTER UNE AMELIORATION
            </CustomButton>,
          ],
        }}
        mainContent={
          isMobile() ? (
            <SwipeableContainer onNext={handleSwipeNext} onPrevious={handleSwipePrevious}>
              <MobileMainContent fiche={fiche} statutsInput={statutsInput} loading={loading} />
            </SwipeableContainer>
          ) : (
            <Table
              style={{ padding: 24 }}
              error={error}
              loading={loading}
              search={false}
              data={data || []}
              selectable={false}
              rowsSelected={rowsSelected}
              onRowsSelectionChange={(newRowsSelected) => setRowsSelected(newRowsSelected as FicheAmelioration[])}
              columns={columns}
              onRunSearch={({ skip, take }) => {
                setSkip(skip);
                setTake(take);
              }}
              rowsTotal={rowsTotal}
            />
          )
        }
        optionBtn={[optionBtn]}
        handleDrawerToggle={handleDrawerToggle}
        openDrawer={openDrawer}
        drawerTitle={`Fiche améliorations ${rowsTotal !== 0 ? `(${skip + 1}/${rowsTotal})` : ``}`}
        onGoBack={onRequestGoBack}
      />
      <ConfirmDeleteDialog
        title="Suppression de fiche d'amélioration"
        content="Etes-vous sûr de vouloir supprimer cette fiche ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDeleteOne}
      />
    </>
  );
};

export default FicheAmeliorationPage;
