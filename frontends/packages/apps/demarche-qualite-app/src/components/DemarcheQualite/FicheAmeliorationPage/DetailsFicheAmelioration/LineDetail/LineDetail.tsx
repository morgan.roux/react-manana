import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import useStyles from './style';

interface LineDetailProps {
    label : string
    value : string
    link? : {
        label : string
        onClick : () => void
    }
};

const LineDetail : FC<LineDetailProps> = ({
    label,
    value,
    link
}) => {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <Box className={classes.labelBox}>
                {label}
            </Box>
            <Box className={classes.valueBox}>
                {value}
            </Box>
            {
                link && (
                    <Box className={classes.link} onClick={link.onClick} >
                        { link.label }
                    </Box>
                )
            }
        </Box>
    );
};

export default LineDetail;
