import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginBottom: 13,
    },
    rootWithLink: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginBottom: 13,
    },
    labelBox: {
      color: 'gray',
    },
    valueBox: {
      fontWeight: 'bold',
    },
    link: {
      color: theme.palette.secondary.main,
      textDecoration: 'underline',
      fontWeight: 'bold',
      cursor: 'pointer',
    },
  }),
);

export default useStyles;
