import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  ChecklistEvaluationPage,
  Checklist,
  ChecklistEvaluation,
  ChecklistEvaluationSectionItem,
  NoItemListImage,
  NoItemContentImage,
} from '@lib/ui-kit';
import { useApplicationContext, useUploadFiles } from '@lib/common';
import {
  Checklist_EvaluationsDocument,
  Checklist_EvaluationsQuery,
  Checklist_EvaluationsQueryVariables,
  useChecklist_EvaluationLazyQuery,
  useChecklist_EvaluationsLazyQuery,
  useChecklist_EvaluationsQuery,
  useCloture_Checklist_EvaluationMutation,
  useCloture_Checklist_EvaluationsLazyQuery,
  useCreate_Checklist_EvaluationMutation,
  useDelete_Checklist_EvaluationMutation,
  useEncours_Checklist_EvaluationsLazyQuery,
  useGet_ChecklistsQuery,
  useToggle_Checklist_Section_Item_EvaluationMutation,
  useUpdate_Checklist_EvaluationMutation,
} from '@lib/common/src/federation';

interface ChecklistEvaluationComponentProps {
  match: {
    params: {
      id: string;
    };
  };
}

const ChecklistEvaluationComponent: FC<ChecklistEvaluationComponentProps & RouteComponentProps> = ({
  history,
  match,
}) => {
  const [togglingEvaluationSectionItemIds, setTogglingEvaluationSectionItemIds] = useState<string[]>([]);
  const [selectedChecklistId, setSelectedChecklistId] = useState<string>();
  const [selectedEvaluationId, setSelectedEvaluationId] = useState<string>();
  const [loadedEvalutionsType, setLoadedEvaluationsType] = useState<
    'checklist_evaluations' | 'encours_evalutions' | 'cloture_evalutions'
  >('checklist_evaluations');

  const [clotureSaving, setClotureSaving] = useState<boolean>(false);
  const [clotureSaved, setClotureSaved] = useState<boolean>(false);

  const { auth, currentPharmacie: pharmacie, federation, notify } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const variables = !auth.isSupAdminOrIsGpmAdmin
    ? { idPharmacie: pharmacie.id, onlyChecklistsPharmacie: false }
    : undefined;

  const id = match?.params?.id;

  const { loading, error, data } = useGet_ChecklistsQuery({
    variables,
    client: federation,
  });

  const [loadEvaluations, loadingEvaluations] = useChecklist_EvaluationsLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  React.useEffect(() => {
    if (id) {
      setSelectedEvaluationId(id);
      loadEvaluations({
        variables: {
          idChecklist: id,
        },
      });
    }
  }, [id]);

  const [loadEncoursEvaluations, loadingEncoursEvaluations] = useEncours_Checklist_EvaluationsLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [loadClotureEvaluations, loadingClotureEvaluations] = useCloture_Checklist_EvaluationsLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [loadEvaluation, loadingEvaluation] = useChecklist_EvaluationLazyQuery({
    client: federation,
  });

  const [toggleEvaluationSectionItem] = useToggle_Checklist_Section_Item_EvaluationMutation({
    client: federation,
    onCompleted: (togglingResult) => {
      if (togglingResult.toggleDQChecklistSectionItemEvaluation?.id) {
        setTogglingEvaluationSectionItemIds(
          // TODO : Filter the valid id
          togglingEvaluationSectionItemIds.slice(1)
        );
      }
    },
  });
  const [createChecklistEvaluation, creationChecklistEvaluation] = useCreate_Checklist_EvaluationMutation({
    update: (cache, creationResult) => {
      if (selectedChecklistId && creationResult.data && creationResult.data.createDQChecklistEvaluation) {
        const previousList = cache.readQuery<Checklist_EvaluationsQuery, Checklist_EvaluationsQueryVariables>({
          query: Checklist_EvaluationsDocument,
          variables: {
            idChecklist: selectedChecklistId,
          },
        })?.dqChecklistEvaluations;

        cache.writeQuery<Checklist_EvaluationsQuery, Checklist_EvaluationsQueryVariables>({
          query: Checklist_EvaluationsDocument,
          variables: {
            idChecklist: selectedChecklistId,
          },
          data: {
            dqChecklistEvaluations: [creationResult.data.createDQChecklistEvaluation, ...(previousList || [])],
          },
        });
      }
    },
    client: federation,
  });

  const [updateChecklistEvaluation, modificationChecklistEvaluation] = useUpdate_Checklist_EvaluationMutation({
    client: federation,
    onCompleted: () => {},
  });

  const [clotureChecklistEvaluation] = useCloture_Checklist_EvaluationMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'La checklist a été clôturée avec succès!',
        type: 'success',
      });

      setClotureSaved(true);
      setClotureSaving(false);
    },

    onError: () => {
      showError();
      setClotureSaving(false);
    },
  });

  const [deleteChecklistEvaluation, deletionChecklistEvaluation] = useDelete_Checklist_EvaluationMutation({
    client: federation,
  });

  const handleGoBack = () => {
    history.push('/demarche-qualite/outils');
  };

  const handleRequestEvaluations = ({ id }: Checklist) => {
    if (id) {
      history.push(`/demarche-qualite/outils/checklist/${id}`);
      setLoadedEvaluationsType('checklist_evaluations');
      loadEvaluations({
        variables: {
          idChecklist: id,
        },
      });

      setSelectedChecklistId(id);
    }
  };

  const handleRequestEvaluationsByStatus = (cloture: boolean) => {
    if (cloture) {
      setLoadedEvaluationsType('cloture_evalutions');
      loadClotureEvaluations();
    } else {
      setLoadedEvaluationsType('encours_evalutions');
      loadEncoursEvaluations();
    }
  };

  const handleRequestEvaluation = (id: string) => {
    if (id) {
      loadEvaluation({
        variables: {
          id,
        },
      });
    }
  };

  const handleRequestSaveEvaluation = (id: string | undefined, ordre: number, libelle: string) => {
    if (id) {
      updateChecklistEvaluation({
        variables: {
          id,
          checklistEvaluationInput: {
            ordre,
            libelle,
          },
        },
      });
    } else if (selectedChecklistId) {
      createChecklistEvaluation({
        variables: {
          idChecklist: selectedChecklistId,
          checklistEvaluationInput: {
            ordre,
            libelle,
          },
        },
      });
    }
  };

  const handleDeleteEvaluation = (evaluation: ChecklistEvaluation) => {
    deleteChecklistEvaluation({
      variables: {
        id: evaluation.id,
      },

      update: (cache, deletionResult) => {
        if (selectedChecklistId && deletionResult?.data?.deleteDQChecklistEvaluation?.id) {
          loadingEncoursEvaluations.refetch && loadingEncoursEvaluations.refetch();
          loadingClotureEvaluations.refetch && loadingClotureEvaluations.refetch();
          loadingEvaluations.refetch && loadingEvaluations.refetch();

          /*const previousList = cache.readQuery<
            CHECKLIST_EVALUATIONS_TYPE,
            CHECKLIST_EVALUATIONSVariables
          >({
            query: CHECKLIST_EVALUATIONS,
            variables: {
              idChecklist: selectedChecklistId,
            },
          })?.dqChecklistEvaluations;

          cache.writeQuery<CHECKLIST_EVALUATIONS_TYPE, CHECKLIST_EVALUATIONSVariables>({
            query: CHECKLIST_EVALUATIONS,
            variables: {
              idChecklist: selectedChecklistId,
            },
            data: {
              dqChecklistEvaluations: (previousList || []).filter(
                (evaluation) =>
                  evaluation?.id !==
                  deletionChecklistEvaluation.data?.deleteDQChecklistEvaluation?.id,
              ),
            },
          });*/
        }
      },
    });
  };

  const handleToggleItem = (evaluation: ChecklistEvaluation, evaluationSectionItem: ChecklistEvaluationSectionItem) => {
    setTogglingEvaluationSectionItemIds([...togglingEvaluationSectionItemIds, evaluationSectionItem.id]);

    toggleEvaluationSectionItem({
      variables: {
        idChecklistEvaluation: evaluation.id,
        idChecklistSectionItem: evaluationSectionItem.idChecklistSectionItem,
      },
    });
  };

  const showError = (message: string = 'Des erreurs se sont survenues pendant la clôture de la checklist'): void => {
    notify({
      type: 'error',
      message: message,
    });
  };

  const doClose = ({ evaluation, commentaire, fichiers }: any) => {
    clotureChecklistEvaluation({
      variables: {
        id: evaluation.id,
        checklistEvaluationInput: {
          commentaire,
          fichiers,
        },
      },
    });
  };

  const handleCloseEvaluation = ({ evaluation, commentaire, fichiers }: any): void => {
    setClotureSaved(false);
    setClotureSaving(true);
    if (fichiers) {
      uploadFiles(fichiers, {
        directory: 'demarche-qualite',
      })
        .then((uploadedFiles) => {
          doClose({ evaluation, commentaire, fichiers: uploadedFiles });
        })
        .catch(() => showError());
    } else {
      doClose({ evaluation, commentaire });
    }
  };

  return (
    <ChecklistEvaluationPage
      selectedChecklistId={selectedChecklistId}
      selectedEvaluationId={selectedEvaluationId}
      loadingChecklists={loading}
      errorLoadingChecklists={error}
      idFromUrl={id}
      checklists={data?.dqChecklists as any}
      loadingEvaluations={
        loadedEvalutionsType === 'checklist_evaluations'
          ? loadingEvaluations.loading
          : loadedEvalutionsType === 'encours_evalutions'
          ? loadingEncoursEvaluations.loading
          : loadedEvalutionsType === 'cloture_evalutions'
          ? loadingEncoursEvaluations.loading
          : undefined
      }
      errorLoadingEvaluations={
        loadedEvalutionsType === 'checklist_evaluations'
          ? loadingEvaluations.error
          : loadedEvalutionsType === 'encours_evalutions'
          ? loadingEncoursEvaluations.error
          : loadedEvalutionsType === 'cloture_evalutions'
          ? loadingClotureEvaluations.error
          : undefined
      }
      evaluations={
        loadedEvalutionsType === 'checklist_evaluations'
          ? (loadingEvaluations.data?.dqChecklistEvaluations as any)
          : loadedEvalutionsType === 'encours_evalutions'
          ? loadingEncoursEvaluations.data?.dqEncoursChecklistEvaluations
          : loadedEvalutionsType === 'cloture_evalutions'
          ? loadingClotureEvaluations.data?.dqClotureChecklistEvaluations
          : undefined
      }
      togglingEvaluationSectionItemIds={togglingEvaluationSectionItemIds}
      loadingEvaluation={loadingEvaluation.called && loadingEvaluation.loading}
      errorLoadingEvaluation={loadingEvaluation.called ? loadingEvaluation.error : undefined}
      loadedEvaluation={loadingEvaluation.called ? (loadingEvaluation.data?.dqChecklistEvaluation as any) : undefined}
      savingEvaluation={
        creationChecklistEvaluation.loading ||
        modificationChecklistEvaluation.loading ||
        deletionChecklistEvaluation.loading
      }
      errorSavingEvaluation={
        creationChecklistEvaluation.error || modificationChecklistEvaluation.error || deletionChecklistEvaluation.error
      }
      emptyListComponent={<NoItemListImage />}
      noItemSelectedComponent={<NoItemContentImage title="" subtitle="" />}
      onRequestGoBack={handleGoBack}
      onRequestEvaluations={handleRequestEvaluations}
      onRequestEvaluationsByStatus={handleRequestEvaluationsByStatus}
      onRequestEvaluation={handleRequestEvaluation}
      onRequestToggleItem={handleToggleItem}
      onRequestSaveEvaluation={handleRequestSaveEvaluation}
      onRequestDeleteEvaluation={handleDeleteEvaluation}
      onRequestCloseEvaluation={handleCloseEvaluation}
      cloture={{
        setSaved: () => setClotureSaved(false),
        saved: clotureSaved,
        saving: clotureSaving,
      }}
    />
  );
};

export default withRouter(ChecklistEvaluationComponent);
