import { useURLSearchParams } from '@lib/common';
import { FC, useCallback } from 'react';
import { useMemo } from 'react';
import { useHistory, useLocation, useParams } from 'react-router';
import { DQParams, Tab } from './interface';

const basePath = 'demarche-qualite';

const isPrimitive = (value: any): boolean => {
  return value !== Object(value);
};

const encodeParam = (param: any, isObjectArray?: boolean): string | undefined => {
  if (Array.isArray(param) && param.length > 0) {
    return isObjectArray
      ? encodeURIComponent(param.map((el) => JSON.stringify(el)).join(','))
      : encodeURIComponent(param.join(','));
  } else if (typeof param === 'string') {
    return encodeURIComponent(param);
  } else if (isPrimitive(param)) {
    return param;
  } else if (param) {
    return encodeURIComponent(JSON.stringify(param));
  }

  return undefined;
};

const decodeParams = (params: URLSearchParams, name: keyof DQParams) => {
  const value = params.get(name);
  if (value) {
    return decodeURIComponent(value);
  }
  return undefined;
};

const extractTabFromLocationPathname = (pathname: string) => {
  if (pathname && pathname.startsWith(basePath)) {
    const omitBasePath = pathname.substr(basePath.length + 1);
    return omitBasePath;
  }

  return undefined;
};

export const useDqParams = () => {
  const query = useURLSearchParams();
  const history = useHistory();
  const location = useLocation();
  const { tab: tabFromParam } = useParams<{ tab: string }>();

  const params: DQParams = useMemo(() => {
    return {
      id: decodeParams(query, 'id'),
      idType: decodeParams(query, 'idType'),
      idImportance: decodeParams(query, 'idImportance'),
      idStatut: decodeParams(query, 'idStatut'),
      view: decodeParams(query, 'view'),
      take: decodeParams(query, 'take'),
      date: decodeParams(query, 'date'),
      back: decodeParams(query, 'back'),
      skip: decodeParams(query, 'skip'),
      action: decodeParams(query, 'action'),
      doc: decodeParams(query, 'doc'),
      searchText: decodeParams(query, 'searchText'),
      idUser: decodeParams(query, 'idUser'),
    };
  }, [query]);

  const redirect = useCallback(
    (changeParams: DQParams, tab: Tab) => {
      const targetTab = tab || extractTabFromLocationPathname(location.pathname);
      const newParams = {
        ...params,
        ...changeParams,
      };
      const newUrl = Object.entries(newParams)
        .filter((el) => {
          return el[1] !== undefined && el[1] !== '';
        })
        .map((element) => {
          const encodedValue = encodeParam(element[1], false);
          return `${element[0]}=${encodedValue}`;
        })
        .join('&');

      history.push(`${targetTab}?${newUrl}`);
    },
    [params]
  );

  return {
    params,
    redirect,
    tab: tabFromParam || extractTabFromLocationPathname(location.pathname) || '',
  };
};
