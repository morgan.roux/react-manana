export interface DQParams {
  id?: string;
  idType?: string;
  idStatut?: string;
  idImportance?: string;
  searchText?: string;
  view?: string;
  take?: string;
  skip?: string;
  date?: string;
  back?: string;
  action?: string;
  doc?: string;
  idUser?: string;
}

export type Tab =
  | 'action-operationnelle'
  | 'action-operationnelle-detail'
  | 'fiche-amelioration'
  | 'fiche-amelioration-detail'
  | 'compte-rendu'
  | 'compte-rendu-form'
  | 'compte-rendu-detail'
  | 'fiche-incident'
  | 'fiche-incident-detail';
