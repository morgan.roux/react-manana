const STATUTS_CODE_ORDER_MAP: any = {
  NOUVEAU: 1,
  EN_COURS: 2,
  CLOTURE: 3,
};

export const sortStatuts = (statuts: any[]): any[] => {
  if (!statuts) {
    return statuts;
  }

  return [...statuts].sort((a: any, b: any) => STATUTS_CODE_ORDER_MAP[a.code] - STATUTS_CODE_ORDER_MAP[b.code]);
};
