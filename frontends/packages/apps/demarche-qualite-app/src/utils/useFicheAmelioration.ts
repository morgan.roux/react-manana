import { useApplicationContext } from '@lib/common';
import {
  Get_Fiche_AmeliorationsDocument,
  Get_Fiche_AmeliorationsQuery,
  Get_Fiche_AmeliorationsQueryVariables,
  useCreate_Fiche_AmeliorationMutation,
  useGet_Fiche_AmeliorationsQuery,
  useUpdate_Fiche_AmeliorationMutation,
} from '@lib/common/src/federation';
import { FicheAmelioration } from '../components/DemarcheQualite';

interface UseFicheAmeliorationOptions {
  onCreated?: (fiche: FicheAmelioration) => void;
  onCreateError?: () => void;
  onUpdated?: (fiche: FicheAmelioration) => void;
  onUpdateError?: () => void;
}

const useFicheAmelioration = (options?: UseFicheAmeliorationOptions) => {
  const { notify, federation } = useApplicationContext();
  const [createFicheAmelioration, creationFicheAmelioration] = useCreate_Fiche_AmeliorationMutation({
    onCompleted: (result) => {
      notify({
        message: "La fiche d'amélioration a été crée avec succès",
        type: 'success',
      });

      if (options?.onCreated) {
        options.onCreated(result.createOneDQFicheAmelioration as any);
      }
    },
    onError: () => {
      notify({
        message: `Des erreurs se sont survenues pendant l'ajout de la fiche d'amélioration'`,
        type: 'error',
      });

      if (options?.onCreateError) {
        options.onCreateError();
      }
    },
    update: (cache, dataResult) => {
      if (dataResult.data && dataResult.data.createOneDQFicheAmelioration) {
        const previousList = cache.readQuery<Get_Fiche_AmeliorationsQuery, Get_Fiche_AmeliorationsQueryVariables>({
          query: Get_Fiche_AmeliorationsDocument,
        })?.dQFicheAmeliorations;

        cache.writeQuery<Get_Fiche_AmeliorationsQuery, Get_Fiche_AmeliorationsQueryVariables>({
          query: Get_Fiche_AmeliorationsDocument,
          data: {
            dQFicheAmeliorations: {
              ...(previousList || {}),
              nodes: [...(previousList?.nodes || []), dataResult.data.createOneDQFicheAmelioration],
            } as any,
          },
        });
      }
    },
    client: federation,
  });

  const [updateFicheAmelioration, updatingFicheAmelioration] = useUpdate_Fiche_AmeliorationMutation({
    onCompleted: (result) => {
      notify({
        message: `La fiche d'amélioration a été modifiée avec succès !`,
        type: 'success',
      });

      if (options?.onUpdated) {
        options.onUpdated(result.updateOneDQFicheAmelioration as any);
      }
    },
    onError: () => {
      notify({
        message: `Des erreurs se sont survenues pendant la modification de la fiche d'amélioration`,
        type: 'error',
      });

      if (options?.onUpdateError) {
        options.onUpdateError();
      }
    },
    client: federation,
  });

  return {
    createFicheAmelioration,
    creationFicheAmelioration,
    updateFicheAmelioration,
    updatingFicheAmelioration,
  };
};

export default useFicheAmelioration;
