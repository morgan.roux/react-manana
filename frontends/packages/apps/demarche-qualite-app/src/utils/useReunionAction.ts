import { useApplicationContext } from '@lib/common';
import {
  useUpdate_Action_OperationnelleMutation,
  useUpdate_Fiche_AmeliorationMutation,
  useUpdate_Fiche_IncidentMutation,
} from '@lib/common/src/federation';
import { ActionOperationnelle, FicheAmelioration, FicheIncident } from '../components/DemarcheQualite';
import { ReunionAction } from '../components/DemarcheQualite/CompteRenduFormPage/types';

const useReunionAction = () => {
  const { notify, federation } = useApplicationContext();

  const [updateFicheAmelioration] = useUpdate_Fiche_AmeliorationMutation({
    onCompleted: () => {
      notify({
        message: 'La fiche amélioration a été modifiée avec succès',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification de la fiche amélioration',
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateActionOperationnelle] = useUpdate_Action_OperationnelleMutation({
    onCompleted: () => {
      notify({
        message: "L'action opération a été modifiée avec succès !",
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant la modification de l'action opérationnelle",
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateFicheIncident] = useUpdate_Fiche_IncidentMutation({
    onCompleted: () => {
      notify({
        message: `La fiche incident a été modifiée avec succès !`,
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: `Des erreurs se sont survenues pendant la modification de la fiche d'incident`,
        type: 'error',
      });
    },
    client: federation,
  });

  const updateReunionAction = (reunionAction: ReunionAction, newValues: any) => {
    console.log(
      '**************updateReunionActionupdateReunionActionupdateReunionAction*****************',
      reunionAction,
      newValues
    );
    if (reunionAction.type === 'AMELIORATION') {
      const fiche: FicheAmelioration = reunionAction.typeAssocie as FicheAmelioration;
      updateFicheAmelioration({
        variables: {
          id: fiche.id,
          update: {
            description: fiche.description,
            idOrigine: fiche.origine.id,
            idUserAuteur: fiche.auteur.id,
            fichiers: fiche.fichiers?.map(({ chemin, nomOriginal, type }) => ({ chemin, nomOriginal, type })),
            idImportance: fiche.importance.id,
            idFonction: fiche.fonction?.id,
            idTache: fiche.tache?.id,
            idStatut: fiche.statut.id,
            idUserParticipants: [
              ...(fiche.participants || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_EN_CHARGE' })),
              ...(fiche.participantsAInformer || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_A_INFORMER' })),
            ],
            idCause: fiche.cause?.id,
            idOrigineAssocie: fiche.idOrigineAssocie,
            idType: fiche.type?.id,
            dateAmelioration: fiche.dateAmelioration,
            isPrivate: fiche.isPrivate ?? false,
            ...newValues,
          },
        },
      });
    } else if (reunionAction.type === 'INCIDENT') {
      const fiche: FicheIncident = reunionAction.typeAssocie as FicheIncident;
      updateFicheIncident({
        variables: {
          id: fiche.id,
          update: {
            description: fiche.description,
            idOrigine: fiche.origine.id,
            idUserDeclarant: fiche.declarant.id,
            fichiers: fiche.fichiers?.map(({ chemin, nomOriginal, type }) => ({ chemin, nomOriginal, type })),
            idImportance: fiche.importance.id,
            idFonction: fiche.fonction?.id,
            idTache: fiche.tache?.id,
            idStatut: fiche.statut.id,
            idUserParticipants: [
              ...(fiche.participants || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_EN_CHARGE' })),
              ...(fiche.participantsAInformer || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_A_INFORMER' })),
            ],
            idCause: fiche.cause?.id,
            idOrigineAssocie: fiche.idOrigineAssocie,
            idType: fiche.type?.id,
            dateIncident: fiche.dateIncident,
            isPrivate: fiche.isPrivate ?? false,
            ...newValues,
          },
        },
      });
    } else if (reunionAction.type === 'OPERATION') {
      const fiche: ActionOperationnelle = reunionAction.typeAssocie as ActionOperationnelle;

      updateActionOperationnelle({
        variables: {
          id: fiche.id || '',
          update: {
            description: fiche.description,
            idOrigine: fiche.origine.id,
            idUserAuteur: fiche.auteur?.id || '',
            idUserSuivi: fiche.suiviPar?.id || '',
            fichiers: fiche.fichiers?.map(({ chemin, nomOriginal, type }) => ({ chemin, nomOriginal, type })),
            idImportance: fiche.importance.id,
            idFonction: fiche.fonction?.id,
            idTache: fiche.tache?.id,
            idStatut: fiche.statut.id,
            idUserParticipants: [
              ...(fiche.participants || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_EN_CHARGE' })),
              ...(fiche.participantsAInformer || []).map(({ id }) => ({ idUser: id, type: 'PARTICIPANT_A_INFORMER' })),
            ],
            idCause: fiche.cause?.id,
            idOrigineAssocie: fiche.idOrigineAssocie,
            idType: fiche.type?.id,
            dateAction: fiche.dateAction,
            isPrivate: fiche.isPrivate ?? false,
            ...newValues,
          },
        },
      });
    }
  };

  return { updateReunionAction };
};

export default useReunionAction;
