import { ModeleOperationMarketingComponent } from './../components';
import { useApplicationContext, CommonFieldsForm } from '@lib/common';
import React, { FC, useState } from 'react';
import { ChromePicker } from 'react-color';
import noContentActionImageSrc from './../assets/image_default.png';
import {
  PlanMarketingTypeActionSortFields,
  SortDirection,
  useChange_Plan_Marketin_Type_Action_Activation_StatusMutation,
  useChange_Plan_Marketin_Type_Activation_StatusMutation,
  useCreate_Modele_Operation_MarketingMutation,
  useCreate_Plan_Marketing_TypeMutation,
  useDelete_Modele_Operation_MarketingMutation,
  useDelete_One_Plan_Marketing_TypeMutation,
  useGet_List_Plan_Marketing_TypesQuery,
  useGet_Modele_Operation_MarketingsLazyQuery,
  usePersonnalise_One_Plan_Marketin_TypeMutation,
  usePersonnalise_One_Plan_Marketin_Type_ActionMutation,
  useUpdate_Modele_Operation_MarketingMutation,
  useUpdate_One_Plan_Marketin_TypeMutation,
} from '@lib/common/src/federation';
import { useParams } from 'react-router';

const ModeleOperationMarketing: FC<{}> = ({}) => {
  const { isSuperAdmin, isAdminGroupement, currentGroupement, currentPharmacie, federation, notify } =
    useApplicationContext();
  const isSuperAdminOrAdminGroupement = isSuperAdmin || isAdminGroupement;

  const { idModele } = useParams<{ idModele: string }>();
  const [commonFields, setCommonFields] = useState<any>({
    userParticipants: [],
    tache: null,
    importance: null,
  });
  const [couleur, setCouleur] = useState<any>();
  const pharmacie = currentPharmacie;
  const groupement = currentGroupement;
  const [idPlanMarketingType, setIdPlanMarketingType] = useState<string | undefined>(idModele);

  // To-Do
  const [loadOperationMarketingActions, loadingOperationMarketingActions] = useGet_Modele_Operation_MarketingsLazyQuery(
    {
      client: federation,
    }
  );

  const [createOperationMarketingAction, creatingOperationMarketingAction] =
    useCreate_Modele_Operation_MarketingMutation({
      onCompleted: () => {
        notify({
          message: 'La To-Do a été créee avec success',
          type: 'success',
        });
        loadingOperationMarketingActions?.refetch && loadingOperationMarketingActions.refetch();
      },
      onError: () => {
        notify({
          message: 'Des erreurs sont survenues pendant la création de la To-Do',
          type: 'error',
        });
      },
      client: federation,
    });

  const [updateOperationMarketingAction, updatingOperationMarketingAction] =
    useUpdate_Modele_Operation_MarketingMutation({
      onCompleted: () => {
        notify({
          message: 'La To-Do a été modifiéé avec success',
          type: 'success',
        });
        loadingOperationMarketingActions?.refetch && loadingOperationMarketingActions.refetch();
      },
      onError: () => {
        notify({
          message: 'Des erreurs sont survenues pendant la modification de la to-Do',
          type: 'error',
        });
      },
      client: federation,
    });

  const [deleteOperationmarketingAction, deletingOperationMarketingAction] =
    useDelete_Modele_Operation_MarketingMutation({
      onCompleted: () => {
        notify({
          message: 'La To-Do a été supprimé avec succès !',
          type: 'success',
        });
        loadingOperationMarketingActions?.refetch && loadingOperationMarketingActions.refetch();
      },
      onError: () => {
        notify({
          message: 'Des erreurs se sont survenues pendant la suppression de la To-Do',
          type: 'error',
        });
      },
      client: federation,
    });

  // Modele
  const planMarketingTypes = useGet_List_Plan_Marketing_TypesQuery({
    fetchPolicy: 'cache-and-network',
    variables: {
      idPharmacie: !isSuperAdminOrAdminGroupement ? pharmacie.id : undefined,
      filter: { idGroupement: { eq: groupement.id } },
      paging: {
        offset: 0,
        limit: 100,
      },
    },
    client: federation,
    onCompleted: (data) => {
      if (data.pRTPlanMarketingTypes) {
        idPlanMarketingType &&
          loadOperationMarketingActions({
            variables: {
              idPharmacie: !isSuperAdminOrAdminGroupement ? pharmacie.id : undefined,
              filter: {
                and: [
                  {
                    idPharmacie: {
                      eq: pharmacie.id,
                    },
                  },
                  {
                    idPlanMarketingType: {
                      eq: idPlanMarketingType,
                    },
                  },
                ],
              },
              sorting: [
                {
                  field: PlanMarketingTypeActionSortFields.JourLancement,
                  direction: SortDirection.Asc,
                },
              ],
            },
          });
      }
    },
  });

  const [createModele, creatingModele] = useCreate_Plan_Marketing_TypeMutation({
    onCompleted: () => {
      notify({
        message: 'La modèle a été créee avec success',
        type: 'success',
      });
      planMarketingTypes.refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs sont survenues pendant la création de la modèle',
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateModele, updatingModele] = useUpdate_One_Plan_Marketin_TypeMutation({
    onCompleted: () => {
      notify({
        message: 'La modèle a été modifiéé avec success',
        type: 'success',
      });
      planMarketingTypes.refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs sont survenues pendant la modification de la modèle',
        type: 'error',
      });
    },
    client: federation,
  });
  const [personnaliseModele, personnalisingModele] = usePersonnalise_One_Plan_Marketin_TypeMutation({
    onCompleted: () => {
      notify({
        message: 'Le modèle a été personnalisé avec succès',
        type: 'success',
      });
      planMarketingTypes.refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs sont survenues pendant la personnalisation du modèle',
        type: 'error',
      });
    },
    client: federation,
  });

  const [personnaliseTask, personnalisingTask] = usePersonnalise_One_Plan_Marketin_Type_ActionMutation({
    onCompleted: () => {
      notify({
        message: "L'action a été personnalisée avec succès",
        type: 'success',
      });
      planMarketingTypes.refetch();
    },
    onError: () => {
      notify({
        message: "Des erreurs sont survenues pendant la personnalisation de l'action",
        type: 'error',
      });
    },
    client: federation,
  });
  const [changePlanMarketingTypeActivationStatus, changingPlanMarketingTypeActivationStatus] =
    useChange_Plan_Marketin_Type_Activation_StatusMutation({
      onCompleted: () => {
        notify({
          message: 'Le modèle a été activé avec succès',
          type: 'success',
        });
        planMarketingTypes.refetch();
      },
      onError: () => {
        notify({
          message: "Des erreurs sont survenues pendant l'activation du modèle",
          type: 'error',
        });
      },
      client: federation,
    });

  const [changePlanMarketingTypeActionActivationStatus, changingPlanMarketingTypeActionActivationStatus] =
    useChange_Plan_Marketin_Type_Action_Activation_StatusMutation({
      onCompleted: () => {
        notify({
          message: 'Le modèle a été activé avec succès',
          type: 'success',
        });
        loadingOperationMarketingActions?.refetch && loadingOperationMarketingActions.refetch();
      },
      onError: () => {
        notify({
          message: "Des erreurs sont survenues pendant l'activation du modèle",
          type: 'error',
        });
      },
      client: federation,
    });

  const [deletePlanMarketingType, deletingPlanMarketingType] = useDelete_One_Plan_Marketing_TypeMutation({
    onCompleted: () => {
      notify({
        message: 'La modèle a été supprimé avec succès !',
        type: 'success',
      });
      planMarketingTypes.refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression de la modèle',
        type: 'error',
      });
    },
    client: federation,
  });

  const handeleRequestTaskSave = (data: any) => {
    if (!data.id) {
      console.log('-----------------idPlanMarketingType: ', data);
      createOperationMarketingAction({
        variables: {
          input: {
            jourLancement: data.jourLancement,
            idParticipants: data.idParticipants,
            description: data.description,
            idPlanMarketingType: idPlanMarketingType || '',
            idFonction: commonFields.tache?.idFonction,
            idTache: commonFields.tache?.idTache,
            idImportance: data.idImportance,
            pendant: data.pendant,
          },
        },
      });
      return;
    }
    if (data.id) {
      updateOperationMarketingAction({
        variables: {
          id: data.id,
          input: {
            jourLancement: data.jourLancement,
            idParticipants: data.idParticipants,
            description: data.description,
            idPlanMarketingType: idPlanMarketingType || '',
            idFonction: data.idfonction,
            idTache: data.idTache,
            idImportance: data.idImportance,
            pendant: data.pendant,
          },
        },
      });
    }
  };

  const handeleRequestTaskPersonnalisation = (task: any) => {
    const { jourLancement, pendant, description } = task;
    personnaliseTask({
      variables: {
        idPharmacie: pharmacie.id,
        idPlanMarketingTypeAction: task.id,
        input: {
          description,
          jourLancement,
          pendant,
          idPlanMarketingType: idPlanMarketingType || '',
          idFonction: commonFields.tache?.idFonction,
          idTache: commonFields.tache?.idTache,
          idImportance: task.idImportance,
          idParticipants: task.idParticipants,
        },
      },
    });
  };

  const handleRequestSaveModele = (data: any) => {
    if (!data.id) {
      createModele({
        variables: {
          input: {
            pRTPlanMarketingType: {
              idGroupement: groupement.id,
              libelle: data.libelle,
              code: data.code,
              couleur, //: data.couleur,
              idCategorie: data.idCategorie,
              idReglementType: data.idReglementType,
              idReglementLieuDeclaration: data.idReglementLieuDeclaration,
              idReglementModeDeclaration: data.idReglementModeDeclaration,
            },
          },
        },
      });
      return;
    }
    if (data.id) {
      updateModele({
        variables: {
          input: {
            id: data.id,
            update: {
              idGroupement: groupement.id,
              libelle: data.libelle,
              code: data.code,
              couleur, //: data.couleur,
              idCategorie: data.idCategorie,
              idReglementType: data.idReglementType,
              idReglementLieuDeclaration: data.idReglementLieuDeclaration,
              idReglementModeDeclaration: data.idReglementModeDeclaration,
            },
          },
        },
      });
    }
  };

  const handleRequestPlanMarketingTypePersonnalisation = (planMarketingType: any) => {
    const { libelle, code } = planMarketingType;
    personnaliseModele({
      variables: {
        idPharmacie: pharmacie.id,
        idPlanMarketingType: planMarketingType.id,
        input: {
          libelle,
          code,
          couleur,
          idGroupement: groupement.id,
          idReglementType: planMarketingType.idReglementType,
          idReglementLieuDeclaration: planMarketingType.idReglementLieuDeclaration,
          idReglementModeDeclaration: planMarketingType.idReglementModeDeclaration,
        },
      },
    });
  };

  const handleRequestDeletePlanMarketingType = (planMarketingType: any) => {
    deletePlanMarketingType({
      variables: { id: planMarketingType.id },
    });
  };

  const handleRequestDeleteTask = (data: any) => {
    deleteOperationmarketingAction({
      variables: {
        id: data.id,
      },
    });
  };

  const handleOpenTaskForm = (task: any) => {
    setCommonFields({
      handlePlanMarketingTypeClick: task.participants || [],
      importance: task.importance,
      tache: { id: task.idTask, idFonction: task.idFonction, idTask: task.idTask },
    });
  };

  const handleRequestChangeTaskStatus = (task: any) => {};

  const handleRequestChangeTaskActivationStatus = (task: any) => {
    changePlanMarketingTypeActionActivationStatus({
      variables: {
        active: !task.active,
        idPlanMarketingTypeAction: task.id,
        idPharmacie: pharmacie.id,
      },
    });
  };

  const handleRequestChangePlanMarketingTypeActivationStatus = (planMarketingType: any) => {
    changePlanMarketingTypeActivationStatus({
      variables: {
        active: !planMarketingType.active,
        idPlanMarketingType: planMarketingType.id,
        idPharmacie: pharmacie.id,
      },
    });
  };

  const handlePlanMarketingTypeClick = (planMarketingType: any) => {
    window.history.pushState(null, '', `/#/db/modele-operation-marketing/${planMarketingType.id}`);
    setIdPlanMarketingType(planMarketingType.id);

    loadOperationMarketingActions({
      variables: {
        idPharmacie: !isSuperAdminOrAdminGroupement ? pharmacie.id : undefined,
        filter: {
          and: [
            {
              idGroupement: {
                eq: groupement.id,
              },
            },
            {
              idPlanMarketingType: {
                eq: planMarketingType.id,
              },
            },
          ],
        },
        sorting: [
          {
            field: PlanMarketingTypeActionSortFields.JourLancement,
            direction: SortDirection.Asc,
          },
        ],
      },
    });
  };

  return (
    <>
      <ModeleOperationMarketingComponent
        isAdmin={isSuperAdminOrAdminGroupement}
        idPlanMarketingType={idPlanMarketingType}
        noContentActionImageSrc={noContentActionImageSrc}
        onRequestTaskSave={handeleRequestTaskSave}
        tasks={{
          data: loadingOperationMarketingActions.data?.planMarketingTypeActions.nodes as any,
          loading: loadingOperationMarketingActions.loading,
        }}
        savingTask={
          creatingOperationMarketingAction.loading ||
          updatingOperationMarketingAction.loading ||
          personnalisingTask.loading
        }
        savingPlanMarketingType={creatingModele.loading || updatingModele.loading || personnalisingTask.loading}
        commonFieldsIds={{
          idImportance: commonFields.importance?.id,
          idTache: commonFields.tache?.idTache,
          idFonction: commonFields.tache?.idFonction,
          idParticipants: commonFields.userParticipants?.map((user: any) => user.id) as any,
        }}
        commonFieldComponent={
          <CommonFieldsForm
            selectedUsers={commonFields.userParticipants}
            urgence={null}
            hideUserInput={true} // Pas de selection de collaborateurs
            urgenceProps={{
              useCode: false,
            }}
            usersModalProps={{
              withNotAssigned: false,
              searchPlaceholder: 'Rechercher...',
              withAssignTeam: false,
              singleSelect: false,
              title: 'Choix de collaborateurs',
            }}
            selectUsersFieldLabel="Collaborateurs"
            projet={commonFields.tache}
            onChangeUsersSelection={(users) =>
              setCommonFields((prevState: any) => ({ ...prevState, userParticipants: users }))
            }
            /*onChangeProjet={projet =>
              setCommonFields(prevState => ({ ...prevState, tache: projet }))
            }*/
            importance={commonFields.importance}
            onChangeImportance={(importance) => setCommonFields((prevState: any) => ({ ...prevState, importance }))}
          />
        }
        planMarketingTypes={{
          data: planMarketingTypes.data?.pRTPlanMarketingTypes.nodes as any,
          loading: planMarketingTypes.loading,
        }}
        couleur={couleur}
        setCouleur={setCouleur}
        colorPicker={
          <fieldset style={{ display: 'flex', padding: 16, justifyContent: 'center' }}>
            <legend>
              <span style={{ fontSize: '0.75rem' }}>Couleur</span>
            </legend>
            <ChromePicker
              //className={classes.colorPicker}
              color={couleur}
              onChangeComplete={(color: any) => setCouleur(color?.hex || '')}
            />
          </fieldset>
        }
        onOpenTaskForm={handleOpenTaskForm}
        onPlanMarketingTypeClick={handlePlanMarketingTypeClick}
        onRequestPlanMarketingTypeSave={handleRequestSaveModele}
        onRequestDeletePlanMarketingType={handleRequestDeletePlanMarketingType}
        onRequestDeleteTask={handleRequestDeleteTask}
        onFetchMoreType={() => {}}
        onRequestPlanMarketingTypePersonnalisation={handleRequestPlanMarketingTypePersonnalisation}
        onRequestChangePlanMarketingTypeActivationStatus={handleRequestChangePlanMarketingTypeActivationStatus}
        onRequestChangeTaskStatus={handleRequestChangeTaskStatus}
        onRequestChangeTaskActivationStatus={handleRequestChangeTaskActivationStatus}
        onRequestTaskPersonnalisation={handeleRequestTaskPersonnalisation}
        idModele={idModele}
      />
    </>
  );
};

export default ModeleOperationMarketing;
