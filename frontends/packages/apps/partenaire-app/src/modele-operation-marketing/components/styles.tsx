import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    sidenav: {
      '& .MuiTypography-root.MuiTypography-caption.MuiTypography-colorTextSecondary': {
        display: 'none !important',
      },
    },
    infinityScroll: {
      height: 'calc(100vh - 140px)',
      width: '100%',
      overflowY: 'scroll',
      scrollbarWidth: 'thin',
      '&::-webkit-scrollbar': {
        // width: '0.2em',
        // background: theme.palette.secondary.main,
      },
    },
  })
);
