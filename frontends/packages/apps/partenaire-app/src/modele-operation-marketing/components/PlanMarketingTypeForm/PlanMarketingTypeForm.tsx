import { Box } from '@material-ui/core';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { CustomFormTextField, CustomModal, CustomSelect } from '@lib/ui-kit';
import { PrtPlanMarketingTypeInfoFragment, usePlan_Marketing_CategoriesQuery } from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';
import { ReglementTypeInput } from '../../../laboratoire/components/ReglementTypeInput/ReglementTypeInput';
import { LieuDeclarationInput } from '../../../laboratoire/components/LieuDeclarationInput/LieuDeclarationInput';
import { ModeDeclarationInput } from '../../../laboratoire/components/ModeDeclarationInput/ModeDeclarationInput';

export interface PlanMarketingTypeFormProps {
  open?: boolean;
  setOpen: (open: boolean) => void;
  mode: 'creation' | 'modification' | 'personnalisation';
  saving: boolean;
  planMarketingType?: PrtPlanMarketingTypeInfoFragment;
  colorPicker: any;
  couleur: any;
  setCouleur: (couleur: string) => void;
  onRequestSave: (data: PRTPlanMarketingTypeRequestSaving) => void;
  onRequestPersonnalise: (data: PRTPlanMarketingTypeRequestSaving) => void;
}

export interface PRTPlanMarketingTypeRequestSaving {
  id?: string;
  code: string;
  libelle: string;
  idCategorie: string;
  couleur?: string;
  idReglementType?: string;
  idReglementModeDeclaration?: string;
  idReglementLieuDeclaration?: string;
}

export const PlanMarketingTypeForm: FC<PlanMarketingTypeFormProps> = ({
  open,
  setOpen,
  mode,
  saving,
  planMarketingType,
  onRequestSave,
  onRequestPersonnalise,
  colorPicker,
  couleur,
  setCouleur,
}) => {
  const { federation, auth } = useApplicationContext();

  const loadingCategories = usePlan_Marketing_CategoriesQuery({
    client: federation,
    variables: {
      paging: {
        offset: 0,
        limit: 50,
      },
    },
  });

  const [values, setValues] = useState<PRTPlanMarketingTypeRequestSaving>({
    code: '',
    libelle: '',
    idCategorie: '',
  });

  const { code, libelle, idCategorie, idReglementType, idReglementLieuDeclaration, idReglementModeDeclaration } =
    values;

  useEffect(() => {
    if (planMarketingType && (mode === 'modification' || mode === 'personnalisation')) {
      const { code, libelle, couleur, idCategorie, reglementLieuDeclaration, reglementModeDeclaration, reglementType } =
        planMarketingType;
      setValues({
        code,
        libelle,
        idCategorie: idCategorie as any,
        idReglementType: reglementType?.id,
        idReglementLieuDeclaration: reglementLieuDeclaration?.id,
        idReglementModeDeclaration: reglementModeDeclaration?.id,
      });
      if (couleur) setCouleur(couleur);
    }
  }, [planMarketingType, mode]);

  useEffect(() => {
    if ((open && mode === 'creation') || (!open && (mode === 'modification' || mode === 'personnalisation'))) {
      setValues({
        code: '',
        libelle: '',
        idCategorie: '',
      });
    }
  }, [open]);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value, name } = e.target;

    setValues((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = () => {
    const data: PRTPlanMarketingTypeRequestSaving = {
      id:
        (mode === 'modification' || mode === 'personnalisation') && planMarketingType
          ? planMarketingType?.id
          : undefined,
      code,
      libelle,
      couleur,
      idCategorie,
      idReglementType,
      idReglementLieuDeclaration,
      idReglementModeDeclaration,
    };
    mode !== 'personnalisation' && onRequestSave(data);
    mode === 'personnalisation' && onRequestPersonnalise(data);
    setOpen && setOpen(false);
  };

  const isFormValid = () => {
    return code && libelle;
  };

  return (
    <CustomModal
      open={open || false}
      setOpen={setOpen || ((_open: boolean) => {})}
      title={
        mode === 'creation'
          ? 'Nouveau modèle'
          : mode === 'modification'
          ? 'Modification du modèle'
          : 'Personnalisation du modèle'
      }
      withBtnsActions
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      disabledButton={!isFormValid() || saving}
      onClickConfirm={handleSubmit}
    >
      <Box display="flex" flexDirection="column">
        <Box width={500}>
          <CustomFormTextField
            type="text"
            onChange={handleChange}
            required={true}
            name="code"
            placeholder="Code"
            label="code"
            value={code}
          />
        </Box>
        <Box width={500}>
          <CustomFormTextField
            type="text"
            onChange={handleChange}
            required={true}
            name="libelle"
            placeholder="Libelle"
            label="libelle"
            value={libelle}
          />
        </Box>
        <Box width={500}>
          <ReglementTypeInput onChange={handleChange} idReglement={idReglementType} name="idReglementType" />
          <ModeDeclarationInput
            onChange={handleChange}
            idModeDeclaration={idReglementModeDeclaration}
            name="idReglementModeDeclaration"
          />
          <LieuDeclarationInput
            onChange={handleChange}
            idLieuDeclaration={idReglementLieuDeclaration}
            name="idReglementLieuDeclaration"
          />
        </Box>
        {auth.isSupAdminOrIsGpmAdmin && (
          <Box width={500} style={{ marginBottom: 10 }}>
            <CustomSelect
              disabled={loadingCategories.loading}
              error={loadingCategories.error}
              listId="id"
              index="libelle"
              label="Type"
              value={idCategorie}
              name="idCategorie"
              onChange={(event: any) => {
                handleChange(event);
              }}
              list={loadingCategories.data?.pRTPlanMarketingCategories.nodes || []}
              required={true}
            />
          </Box>
        )}
        <Box width={500}>{colorPicker}</Box>
      </Box>
    </CustomModal>
  );
};
