import { Dispatch, SetStateAction, useState } from 'react';
import { ModeleOperationMarketingInfoFragment } from '@lib/common/src/federation';

const useTask = (
  setMode: Dispatch<SetStateAction<'creation' | 'modification' | 'personnalisation'>>,
  onOpenTaskForm: (task: ModeleOperationMarketingInfoFragment) => void
) => {
  const [activeTask, setActiveTask] = useState<ModeleOperationMarketingInfoFragment>();

  const [openTaskDeleteDialog, setOpenTaskDeleteDialog] = useState<boolean>(false);

  const [openTaskFormDialog, setOpenTaskFormDialog] = useState<boolean>(false);

  const handlePersonnaliseTask = (task: ModeleOperationMarketingInfoFragment) => {
    setActiveTask(task);
    setMode('personnalisation');
    onOpenTaskForm(task);
    setOpenTaskFormDialog(true);
  };

  const handleCreateTask = () => {
    setMode('creation');
    setOpenTaskFormDialog(true);
  };

  const handleEditTask = (task: ModeleOperationMarketingInfoFragment) => {
    setActiveTask(task);
    setMode('modification');
    onOpenTaskForm(task);
    setOpenTaskFormDialog(true);
  };

  const handleDeletetask = (task: ModeleOperationMarketingInfoFragment) => {
    setActiveTask(task);
    setOpenTaskDeleteDialog(true);
  };
  return {
    activeTask,
    openTaskDeleteDialog,
    openTaskFormDialog,
    setOpenTaskDeleteDialog,
    setOpenTaskFormDialog,
    handleCreateTask,
    handleDeletetask,
    handleEditTask,
    handlePersonnaliseTask,
  };
};
export default useTask;
