import { Dispatch, SetStateAction, useState } from 'react';
import { PrtPlanMarketingTypeInfoFragment } from '@lib/common/src/federation';

const usePlanMarketingType = (setMode: Dispatch<SetStateAction<'creation' | 'modification' | 'personnalisation'>>) => {
  const [activePlanMarketingType, setActivePlanMarketingType] = useState<PrtPlanMarketingTypeInfoFragment>();

  const [openPlanMarketingTypeDeleteDialog, setOpenPlanMarketingTypeDeleteDialog] = useState<boolean>(false);

  const [openPlanMarketingTypeFormDialog, setOpenPlanMarketingTypeFormDialog] = useState<boolean>(false);

  const handlePersonnalisePlanMarketingType = (planMarketingType: PrtPlanMarketingTypeInfoFragment) => {
    setActivePlanMarketingType(planMarketingType);
    setMode('personnalisation');
    setOpenPlanMarketingTypeFormDialog(true);
  };

  const handleCreatePlanMarketingType = () => {
    setMode('creation');
    setOpenPlanMarketingTypeFormDialog(true);
  };

  const handleEditPlanMarketingType = (planMarketingType: PrtPlanMarketingTypeInfoFragment) => {
    setActivePlanMarketingType(planMarketingType);
    setMode('modification');
    setOpenPlanMarketingTypeFormDialog(true);
  };

  const handleDeletePlanMarketingType = (planMarketingType: PrtPlanMarketingTypeInfoFragment) => {
    setActivePlanMarketingType(planMarketingType);
    setOpenPlanMarketingTypeDeleteDialog(true);
  };

  console.log('*************Active activePlanMarketingType', activePlanMarketingType);
  return {
    activePlanMarketingType,
    setActivePlanMarketingType,
    openPlanMarketingTypeDeleteDialog,
    openPlanMarketingTypeFormDialog,
    setOpenPlanMarketingTypeDeleteDialog,
    setOpenPlanMarketingTypeFormDialog,
    handleCreatePlanMarketingType,
    handleDeletePlanMarketingType,
    handleEditPlanMarketingType,
    handlePersonnalisePlanMarketingType,
  };
};
export default usePlanMarketingType;
