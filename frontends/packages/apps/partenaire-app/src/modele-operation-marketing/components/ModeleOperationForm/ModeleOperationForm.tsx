import { Box, FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import React, { ChangeEvent, FC, ReactNode, useEffect, useState } from 'react';
import { ModeleOperationMarketingInfoFragment } from '@lib/common/src/federation';
import { CustomEditorText, CustomFormTextField, CustomModal } from '@lib/ui-kit';
import { useWindowDimensions } from '@lib/ui-kit/src/components/atoms/MobileTopBar/MobileTopBar';

export interface ModeleOperationFormProps {
  open?: boolean;
  setOpen?: (open: boolean) => void;
  mode: 'creation' | 'modification' | 'personnalisation';
  saving: boolean;
  commonFieldComponent: ReactNode;
  task?: ModeleOperationMarketingInfoFragment;
  onRequestSave: (data: TaskRequestSaving) => void;
  onRequestPersonnalisation: (data: TaskRequestSaving) => void;
  idImportance?: string;
  idTache?: string;
  idFonction?: string;
  idPlanMarketingType?: string;
}

export interface TaskRequestSaving {
  id?: string;
  description: string;
  jourLancement?: number;
  idPlanMarketingType: string;
  idImportance?: string;
  idTache?: string;
  idFonction?: string;
  pendant?: number;
}

export const ModeleOperationForm: FC<ModeleOperationFormProps> = ({
  open,
  setOpen,
  mode,
  saving,
  commonFieldComponent,
  task,
  onRequestSave,
  onRequestPersonnalisation,
  idImportance,
  idTache,
  idFonction,
  idPlanMarketingType,
}) => {
  const [values, setValues] = useState({
    description: '',
    avantApres: 'AVANT',
    jourLancement: 0,
  });

  const { description, avantApres, jourLancement } = values;

  const dimension = useWindowDimensions();

  useEffect(() => {
    if (task && (mode === 'modification' || mode === 'personnalisation')) {
      const { description, jourLancement, pendant } = task;
      setValues({
        description: description || '',
        avantApres: pendant ? 'PENDANT' : jourLancement && jourLancement > 0 ? 'APRES' : 'AVANT',
        jourLancement: (jourLancement as any) || 0,
      });
    }
  }, [task, mode]);

  useEffect(() => {
    if ((open && mode === 'creation') || (!open && (mode === 'modification' || mode === 'personnalisation'))) {
      setValues({
        description: '',
        avantApres: 'AVANT',
        jourLancement: 0,
      });
    }
  }, [open]);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    const numberFields = ['jourLancement'];
    if (numberFields.includes(name)) {
      setValues((prevState) => ({
        ...prevState,
        [name]: parseInt(value),
      }));
    } else {
      setValues((prevState) => ({
        ...prevState,
        [name]: value,
      }));
    }
  };
  const isFormValid = () => {
    return avantApres && jourLancement && description && idImportance;
  };
  const handleSubmit = () => {
    const data = {
      id: mode !== 'creation' ? task?.id : undefined,
      description,
      jourLancement: avantApres === 'AVANT' ? -1 * jourLancement : jourLancement,
      idPlanMarketingType: idPlanMarketingType || '',
      avantApres,
      idImportance,
      idTache,
      idFonction,
      pendant: avantApres === 'PENDANT' ? jourLancement : undefined,
    };
    mode !== 'personnalisation' && onRequestSave(data);
    mode === 'personnalisation' && onRequestPersonnalisation(data);
  };

  useEffect(() => {
    if (!saving && open) {
      setOpen && setOpen(false);
    }
  }, [saving]);

  return (
    <CustomModal
      open={open || false}
      setOpen={setOpen || ((_open: boolean) => {})}
      title={
        mode === 'creation'
          ? 'Nouvelle tâche'
          : mode === 'modification'
          ? 'Modification de la tâche'
          : 'Personnalisation de la tâche'
      }
      withBtnsActions
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      disabledButton={!isFormValid() || saving}
      onClickConfirm={handleSubmit}
      fullWidth={dimension === 'isSm'}
    >
      <Box display="flex" flexDirection="column" width={700}>
        <Box py={1}>
          <CustomEditorText
            value={description}
            placeholder="Description"
            name="description"
            onChangeWithName={handleChange}
          />
        </Box>
        <Box mt={1.5}> {commonFieldComponent}</Box>
        <Box>
          <Box>
            <Typography>Cette tâche sera ajoutée automatiquement :</Typography>
          </Box>
          <Box>
            <RadioGroup aria-label="avant-apres" name="avantApres" value={avantApres} onChange={handleChange}>
              <Box display="flex" flexDirection="row">
                <Box>
                  <FormControlLabel value="AVANT" control={<Radio />} label="Avant" />
                </Box>
                <Box>
                  <FormControlLabel value="PENDANT" control={<Radio />} label="Pendant" />
                </Box>
                <Box>
                  <FormControlLabel value="APRES" control={<Radio />} label="Après" />
                </Box>
              </Box>
            </RadioGroup>
          </Box>
        </Box>
        <Box display="flex" flexDirection="row" mt={1.5}>
          <Box>
            {' '}
            <CustomFormTextField
              onChange={handleChange}
              value={avantApres === 'PENDANT' ? jourLancement : jourLancement >= 0 ? jourLancement : -1 * jourLancement}
              type="number"
              inputProps={{
                min: avantApres === 'PENDANT' ? undefined : 0,
                step: 1,
                style: { textAlign: 'right' },
              }}
              name="jourLancement"
              style={{ width: 80 }}
            />
          </Box>
          <Box ml={3} mt={1.9}>
            <Typography>jour(s) du lancement de l'operation marketing</Typography>
          </Box>
        </Box>
      </Box>
    </CustomModal>
  );
};
