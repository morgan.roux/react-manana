import { Box, Button, Grid, Typography } from '@material-ui/core';
import { Add, Cancel, CheckCircle, Delete, Edit } from '@material-ui/icons';
import React, { FC, ReactNode, useEffect, useState } from 'react';
import { BottomScrollListener } from 'react-bottom-scroll-listener';
import { CardTheme, Loader, NoItemContentImage, CardThemeList, ConfirmDeleteDialog, ModeleTask } from '@lib/ui-kit';
import { ModeleOperationForm, TaskRequestSaving } from './ModeleOperationForm/ModeleOperationForm';
import {
  PlanMarketingTypeForm,
  PRTPlanMarketingTypeRequestSaving,
} from './PlanMarketingTypeForm/PlanMarketingTypeForm';
import { useStyles } from './styles';
import usePlanMarketingType from './usePlanMarketingType';
import useTask from './useTask';
import { ModeleOperationMarketingInfoFragment, PrtPlanMarketingTypeInfoFragment } from '@lib/common/src/federation';
import { ParameterContainer } from '@lib/ui-kit/src/components/templates';

export interface ModeleOperationMarketingPageProps {
  isAdmin: boolean;
  idPlanMarketingType?: string;
  noContentActionImageSrc: string;
  savingTask: boolean;
  savingPlanMarketingType: boolean;
  commonFieldComponent: ReactNode;
  tasks: {
    data?: ModeleOperationMarketingInfoFragment[];
    loading?: boolean;
    error?: Error;
  };
  planMarketingTypes: {
    data?: PrtPlanMarketingTypeInfoFragment[];
    loading?: boolean;
    error?: Error;
  };
  commonFieldsIds?: {
    idImportance?: string;
    idTache?: string;
    idFonction?: string;
    idParticipants?: string;
  };
  colorPicker: any;
  couleur: any;
  setCouleur: (couleur: string) => void;

  onRequestDeleteTask: (task: ModeleOperationMarketingInfoFragment) => void;
  onRequestTaskSave: (data: TaskRequestSaving) => void;
  onRequestTaskPersonnalisation: (data: TaskRequestSaving) => void;
  onRequestChangeTaskStatus?: (id: string, statut: 'DONE' | 'ACTIVE') => void;
  onFetchMoreType: (data: number) => void;
  onRequestChangeTaskActivationStatus: (task: ModeleOperationMarketingInfoFragment) => void;
  onRequestChangePlanMarketingTypeActivationStatus: (planMarketingType: PrtPlanMarketingTypeInfoFragment) => void;
  onRequestPlanMarketingTypePersonnalisation: (data: PRTPlanMarketingTypeRequestSaving) => void;
  onRequestPlanMarketingTypeSave: (data: PRTPlanMarketingTypeRequestSaving) => void;
  onRequestDeletePlanMarketingType: (planMarketingType: PrtPlanMarketingTypeInfoFragment) => void;
  onPlanMarketingTypeClick: (planMarketingType: PrtPlanMarketingTypeInfoFragment) => void;
  onOpenTaskForm: (task: ModeleOperationMarketingInfoFragment) => void;
  idModele?: string;
}

export const ModeleOperationMarketingComponent: FC<ModeleOperationMarketingPageProps> = ({
  isAdmin,
  idPlanMarketingType,
  noContentActionImageSrc,
  savingTask,
  savingPlanMarketingType,
  commonFieldComponent,
  tasks,
  onRequestTaskSave,
  onRequestTaskPersonnalisation,
  planMarketingTypes,
  commonFieldsIds,
  colorPicker,
  couleur,
  setCouleur,
  onRequestDeleteTask,
  onFetchMoreType,
  onRequestChangeTaskActivationStatus,
  onRequestChangePlanMarketingTypeActivationStatus,
  onRequestPlanMarketingTypePersonnalisation,
  onRequestPlanMarketingTypeSave,
  onRequestDeletePlanMarketingType,
  onPlanMarketingTypeClick,
  onOpenTaskForm,
  idModele,
}) => {
  const [mode, setMode] = useState<'creation' | 'modification' | 'personnalisation'>('creation');

  const {
    activeTask,
    openTaskDeleteDialog,
    openTaskFormDialog,
    setOpenTaskDeleteDialog,
    setOpenTaskFormDialog,
    handleCreateTask,
    handleDeletetask,
    handleEditTask,
    handlePersonnaliseTask,
  } = useTask(setMode, onOpenTaskForm);

  const {
    activePlanMarketingType,
    setActivePlanMarketingType,
    openPlanMarketingTypeDeleteDialog,
    openPlanMarketingTypeFormDialog,
    setOpenPlanMarketingTypeDeleteDialog,
    setOpenPlanMarketingTypeFormDialog,
    handleCreatePlanMarketingType,
    handleDeletePlanMarketingType,
    handleEditPlanMarketingType,
    handlePersonnalisePlanMarketingType,
  } = usePlanMarketingType(setMode);

  const classes = useStyles();
  const handleFetchOnBottom = () => {
    onFetchMoreType(10);
  };

  const handleClickPlanMarketingType = (planMarketingType: PrtPlanMarketingTypeInfoFragment) => {
    onPlanMarketingTypeClick(planMarketingType);
  };

  const handleRequestDeleteTask = () => {
    setOpenTaskDeleteDialog(false);
    onRequestDeleteTask(activeTask as any);
  };

  useEffect(() => {
    if (idPlanMarketingType && planMarketingTypes.data?.length) {
      const planMarketingType = planMarketingTypes.data.find(
        (planMarketingType) => planMarketingType.id === idPlanMarketingType
      );
      planMarketingType && setActivePlanMarketingType(planMarketingType);
    }
  }, [planMarketingTypes]);

  const userPharmacieButtons = (planMarketingType: PrtPlanMarketingTypeInfoFragment) => [
    {
      menuItemLabel: {
        title: 'Personnaliser',
        icon: <Edit />,
      },
      onClick: () => handlePersonnalisePlanMarketingType(planMarketingType),
    },
    {
      menuItemLabel: {
        title: planMarketingType.active ? 'Desactiver' : 'Activer',
        icon: planMarketingType.active ? <Cancel /> : <CheckCircle />,
      },
      onClick: () => onRequestChangePlanMarketingTypeActivationStatus(planMarketingType),
    },
  ];

  const userAdminButtons = (planMarketingType: PrtPlanMarketingTypeInfoFragment) =>
    planMarketingType.operationMarketings
      ? [
          {
            menuItemLabel: {
              title: 'Modifier',
              icon: <Edit />,
            },
            onClick: () => handleEditPlanMarketingType(planMarketingType),
          },
        ]
      : [
          {
            menuItemLabel: {
              title: 'Modifier',
              icon: <Edit />,
            },
            onClick: () => handleEditPlanMarketingType(planMarketingType),
          },
          {
            menuItemLabel: {
              title: 'Supprimer',
              icon: <Delete />,
            },
            onClick: () => handleDeletePlanMarketingType(planMarketingType),
          },
        ];

  const content = tasks.loading ? (
    <Loader />
  ) : (
    <>
      <Box display="flex" flexDirection="row" justifyContent="space-between" height="100%">
        <Box>
          <Typography style={{ fontSize: '18px', fontWeight: 'bold' }}>Liste tâche à faire</Typography>
        </Box>
        <Box mt={1}>
          {isAdmin && idModele && (
            <Button variant="contained" color="secondary" onClick={handleCreateTask}>
              <Add />
              AJOUTER UNE TO-DO
            </Button>
          )}
        </Box>
      </Box>
      <Box height="100%">
        {(tasks.data || []).length > 0
          ? (tasks.data || []).map((task) => (
              <ModeleTask
                isAdmin={isAdmin}
                task={task as any}
                handlePersonnaliseTask={handlePersonnaliseTask as any}
                handleChangeTaskStatus={onRequestChangeTaskActivationStatus as any}
                handleEditTask={handleEditTask as any}
                handleDeletetask={handleDeletetask as any}
              />
            ))
          : activePlanMarketingType?.idPharmacie && (
              <NoItemContentImage
                src={noContentActionImageSrc}
                title="Ajout d'une TO-DO"
                subtitle="Vous pouvez ajouter une TO-DO à tout moment pour résoudre le problème"
              />
            )}
      </Box>
      <ModeleOperationForm
        open={openTaskFormDialog}
        setOpen={setOpenTaskFormDialog}
        mode={mode}
        saving={savingTask}
        commonFieldComponent={commonFieldComponent}
        onRequestSave={onRequestTaskSave}
        onRequestPersonnalisation={onRequestTaskPersonnalisation}
        task={activeTask}
        idPlanMarketingType={idModele}
        {...commonFieldsIds}
      />
      <ConfirmDeleteDialog
        title="Suppression d'une tâche"
        content="Etes-vous sûr de vouloir supprimer cette tâche ?"
        open={openTaskDeleteDialog}
        setOpen={setOpenTaskDeleteDialog}
        onClickConfirm={handleRequestDeleteTask}
      />
    </>
  );

  const sidenav = (
    <Box className={classes.sidenav}>
      <Box mt={2}>
        <Typography style={{ fontSize: '18px', fontWeight: 'bold', textAlign: 'center' }}>
          Opération Trade Marketing
        </Typography>
      </Box>
      {isAdmin && (
        <Box mt={4} mb={1} ml={7}>
          <Button
            variant="contained"
            color="secondary"
            onClick={handleCreatePlanMarketingType}
            disabled={tasks.loading}
          >
            <Add />
            AJOUTER UN MODELE
          </Button>
        </Box>
      )}
      <BottomScrollListener onBottom={handleFetchOnBottom}>
        {(scrollRef) => (
          <Grid
            item
            id="listLaboGrid"
            className={classes.infinityScroll}
            ref={scrollRef as any}
            style={isAdmin ? { height: 'calc(100vh - 230px)' } : {}}
          >
            {planMarketingTypes.error && <Box textAlign="center">Une erreur est survenue</Box>}
            {planMarketingTypes.loading ? (
              <Loader />
            ) : (
              (planMarketingTypes.data || []).map((planMarketingType) => {
                return (
                  <Box mt={3} width={310} onClick={() => handleClickPlanMarketingType(planMarketingType)} px={2}>
                    <CardThemeList>
                      <CardTheme
                        icon={planMarketingType.active && <CheckCircle color="secondary" />}
                        title={planMarketingType.libelle}
                        moreOptions={
                          !isAdmin ? userPharmacieButtons(planMarketingType) : userAdminButtons(planMarketingType)
                        }
                        active={planMarketingType?.id === activePlanMarketingType?.id}
                      />
                    </CardThemeList>
                  </Box>
                );
              })
            )}
          </Grid>
        )}
      </BottomScrollListener>
      <PlanMarketingTypeForm
        open={openPlanMarketingTypeFormDialog}
        setOpen={setOpenPlanMarketingTypeFormDialog}
        mode={mode}
        saving={savingPlanMarketingType}
        couleur={couleur}
        colorPicker={colorPicker}
        setCouleur={setCouleur}
        onRequestSave={onRequestPlanMarketingTypeSave}
        onRequestPersonnalise={onRequestPlanMarketingTypePersonnalisation}
        planMarketingType={activePlanMarketingType}
      />
      <ConfirmDeleteDialog
        title="Suppression d'un modèle"
        content="Etes-vous sûr de vouloir supprimer ce modèle ?"
        open={openPlanMarketingTypeDeleteDialog}
        setOpen={setOpenPlanMarketingTypeDeleteDialog}
        onClickConfirm={() => onRequestDeletePlanMarketingType}
      />
    </Box>
  );
  return (
    <ParameterContainer
      sideNavCustomContent={<Box>{sidenav}</Box>}
      mainContent={
        <Box mr={3} ml={3} mt={3}>
          {content}
        </Box>
      }
    />
  );
};
