import React from 'react';
import DashboardIcon from '@material-ui/icons/Dashboard';
import loadable from '@loadable/component';

import { ModuleDefinition } from '@lib/common';

const ModeleOperationMarketingPage = loadable(() => import('./pages/ModeleOperationMarketing'));

const modeleOperationMarketingModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_MODELE_OPERATION_MARKETING',
      location: 'PARAMETRE',
      name: 'Opération Trade Marketing',
      to: '/modele-operation-marketing',

      icon: <DashboardIcon />,
      preferredOrder: 170,
      authorize: {
        roles: ['SUPADM', 'GRPADM', 'PRMTIT'],
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
  routes: [
    {
      attachTo: 'PARAMETRE',
      path: '/modele-operation-marketing/:idModele?',
      component: ModeleOperationMarketingPage,
    },
  ],
};

export default modeleOperationMarketingModule;
