import { makeVar } from '@apollo/client';
export const searchTextVar = makeVar<string>('');

export const refetchStatsVar = makeVar<boolean>(false);
