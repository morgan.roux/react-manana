import moment from 'moment';

export const round2Decimals = (num?: number) => {
  return num ? Math.round((num + Number.EPSILON) * 100) / 100 : 0;
};

export const formatMoney = (num?: number | null, defaultReturn: string = '0,00'): string => {
  return num ? round2Decimals(num).toFixed(2).replace('.', ',') : defaultReturn;
};

export const computeHourPlus = (value: string, minutes: number = 30) => {
  const debut = value.split(':');

  const date = moment().minute(parseInt(debut[1])).hour(parseInt(debut[0]));

  const fin = date.add(minutes, 'minutes');

  let minute = fin.get('minutes');
  let heure = fin.get('hours');

  if (minute > 59) {
    minute = minute - 60;
    heure = heure + 1;
  }

  if (heure > 23) {
    heure = 0;
  }

  const heureFin = `${heure > 9 ? heure : `0${heure}`}:${minute > 9 ? minute : `0${minute}`}`;

  return heureFin;
};

export const typeReunions = [
  {
    id: '1',
    code: 'VISIO',
    libelle: 'Rendez-Vous en VISIO',
  },
  {
    id: '2',
    code: 'PRESENTIELLE',
    libelle: 'Rendez-Vous en Présentiel',
  },
];
