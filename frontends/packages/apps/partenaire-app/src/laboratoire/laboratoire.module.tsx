import React from 'react';
import loadable from '@loadable/component';
import Laboratoire from './assets/img/laboratoire.png';
import LaboIcon from '@material-ui/icons/WbIncandescent';
import ReceiptIcon from '@material-ui/icons/Receipt';
import SwapHorizontalCircleIcon from '@material-ui/icons/SwapHorizontalCircle';
import ClassIcon from '@material-ui/icons/Class';
import EventIcon from '@material-ui/icons/Event';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import RemiseArriereIcon from './assets/img/remise-arriere.svg';
import BarChartIcon from '@material-ui/icons/BarChart';
import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';
import { FavoriteAddButton } from './FavoriteAddButton';
import { Add } from '@material-ui/icons';
import { openAddActionOperationnelle } from './modules/action-operationnel/pages/reactive-vars';
import QuickAddActionOperationnelle from './modules/action-operationnel/pages/quick-add-action-operationnelle';

import { openAddRendezVous } from './modules/rendezVous/pages/reactive-vars';
import QuickAddRendezVous from './modules/rendezVous/pages/rendez-vous/quick-add-rendez-vous';
import { fetchRendezVousTotalCount } from './total-count';

const RELATION_LABORATOIRE_FAVORITE_GROUP = 'Relation avec les labos';
const activationParameters = {
  groupement: '0305',
  pharmacie: '0708',
};

const NewPagesLabo = loadable(() => import('./pages/NewPages/Page'), {
  fallback: <SmallLoading />,
});

const DetailContratPage = loadable(() => import('./modules/contrat/pages/details-contrat/DetailContrat'), {
  fallback: <SmallLoading />,
});

const DetailCompteRenduPage = loadable(() => import('./modules/compte-rendu/pages/compte-rendu-detail'), {
  fallback: <SmallLoading />,
});

const CompteRenduFormPage = loadable(() => import('./modules/compte-rendu/pages/compte-rendu-form'), {
  fallback: <SmallLoading />,
});

const DetailActionOperationnelle = loadable(
  () => import('./modules/action-operationnel/pages/action-operationnelle-detail'),
  {
    fallback: <SmallLoading />,
  }
);

const ContactPage = loadable(() => import('./modules/contact/pages/contact/Contact'), {
  fallback: <SmallLoading />,
});

const RemunerationPage = loadable(() => import('./modules/remuneration/pages/Remuneration'), {
  fallback: <SmallLoading />,
});

const PilotagePage = loadable(() => import('./modules/pilotage/pages/Pilotage'), {
  fallback: <SmallLoading />,
});

const PilotagePlanningPage = loadable(() => import('./modules/pilotage/pages/pilotage-plan-marketing'), {
  fallback: <SmallLoading />,
});

const PilotageRemunerationPage = loadable(() => import('./modules/pilotage/pages/pilotage-remuneration'), {
  fallback: <SmallLoading />,
});

const DetailsConditionCommerciale = loadable(
  () => import('./modules/condition-commerciale/pages/condition-commerciale-detail/ConditionCommercialeDetail'),
  {
    fallback: <SmallLoading />,
  }
);

const TradeMarketingFormPage = loadable(() => import('./modules/trade-marketing/pages/plan-form'), {
  fallback: <SmallLoading />,
});

const DetailsFacture = loadable(() => import('./modules/historique-facture/pages/details-facture/DetailFacture'), {
  fallback: <SmallLoading />,
});

const CatalogueProduitPage = loadable(() => import('./modules/catalogue-produit/pages/CatalogueProduit'), {
  fallback: <SmallLoading />,
});

const ParametreLaboratoire = loadable(() => import('./pages/Parametre-laboratoire/Laboratoire'), {
  fallback: <SmallLoading />,
});

const laboratoireModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARTENAIRE_LABORATOIRE',
      location: 'ACCUEIL',
      name: 'Laboratoires',
      to: '/laboratoires/plan',
      icon: Laboratoire,
      activationParameters,
      preferredOrder: 50,
      options: {
        totalCount: fetchRendezVousTotalCount,
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_PARAMETRE_LABORATOIRE',
      location: 'PARAMETRE',
      name: 'Laboratoires',
      to: '/laboratoires',
      icon: <LaboIcon />,
      preferredOrder: 260,
      authorize: {
        roles: ['SUPADM', 'GRPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },
    {
      id: 'FEATURE_FAVORITE_PLAN_MARKETING',
      location: 'FAVORITE',
      name: 'Plan Trade',
      to: '/laboratoires/plan',
      icon: <ReceiptIcon />,
      activationParameters,
      preferredOrder: 10,
      component: (
        <FavoriteAddButton
          url={'/laboratoires/plan-form?id=new&backParams=%7B"estRemiseArriere"%3Afalse%2C"take"%3A"12"%7D'}
        />
      ),
      options: {
        group: RELATION_LABORATOIRE_FAVORITE_GROUP,
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
    {
      id: 'FEATURE_FAVORITE_REMISE_ARRIERE',
      location: 'FAVORITE',
      name: 'Remise Arrière',
      to: '/laboratoires/remise?take=12',
      icon: <img style={{ width: 24, height: 24 }} src={RemiseArriereIcon} alt="Remise Arrière" />,
      activationParameters,
      preferredOrder: 10,
      component: (
        <FavoriteAddButton
          url={
            '/laboratoires/plan-form?id=new&estRemiseArriere=true&backParams=%7B"estRemiseArriere"%3Afalse%2C"take"%3A"12"%7D'
          }
        />
      ),
      options: {
        group: RELATION_LABORATOIRE_FAVORITE_GROUP,
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
    {
      id: 'FEATURE_FAVORITE_LITIGE',
      location: 'FAVORITE',
      name: 'Gestion Litiges',
      to: '/laboratoires/litige',
      icon: <SwapHorizontalCircleIcon />,
      activationParameters,
      preferredOrder: 20,
      component: (
        <>
          <Add
            onClick={(event) => {
              event.preventDefault();
              event.stopPropagation();
              openAddActionOperationnelle(true);
            }}
          />
          <QuickAddActionOperationnelle />
        </>
      ),
      options: {
        group: RELATION_LABORATOIRE_FAVORITE_GROUP,
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_FAVORITE_COMPTE_RENDU',
      location: 'FAVORITE',
      name: 'Compte Rendu',
      to: '/laboratoires/compte-rendu',
      icon: <ClassIcon />,
      activationParameters,
      preferredOrder: 30,
      component: <FavoriteAddButton url="/laboratoires/compte-rendu-form" />,
      options: {
        group: RELATION_LABORATOIRE_FAVORITE_GROUP,
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_FAVORITE_RENDEZ_VOUS',
      location: 'FAVORITE',
      name: 'Rendez-vous',
      to: '/laboratoires/rendez-vous',
      icon: <EventIcon />,
      activationParameters,
      preferredOrder: 40,
      component: (
        <>
          <Add
            onClick={(event) => {
              event.preventDefault();
              event.stopPropagation();
              openAddRendezVous(true);
            }}
          />
          <QuickAddRendezVous />
        </>
      ),
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
      options: {
        group: RELATION_LABORATOIRE_FAVORITE_GROUP,
      },
    },
    {
      id: 'FEATURE_FAVORITE_TODO',
      location: 'FAVORITE',
      name: 'To-Do Labo',
      to: '/laboratoires/to-do?todoActionStatus=ACTIVE',
      icon: <DoneOutlineIcon />,
      activationParameters: [activationParameters, '0887'],
      preferredOrder: 60,
      options: {
        group: RELATION_LABORATOIRE_FAVORITE_GROUP,
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
    {
      id: 'FEATURE_FAVORITE_PILOTAGE',
      location: 'FAVORITE',
      name: 'Pilotage',
      to: '/laboratoires/pilotages/PILOTAGES',
      icon: <BarChartIcon />,
      activationParameters,
      preferredOrder: 80,
      options: {
        group: RELATION_LABORATOIRE_FAVORITE_GROUP,
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_FAVORITE_CONTACT',
      location: 'FAVORITE',
      name: 'Contact Labo',
      to: '/laboratoires/contact',
      icon: <AccountBoxIcon />,
      activationParameters,
      preferredOrder: 90,
      //  component: <Add />,
      options: {
        group: RELATION_LABORATOIRE_FAVORITE_GROUP,
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],

  routes: [
    {
      attachTo: 'MAIN',
      path: '/laboratoires/plan-form',
      component: TradeMarketingFormPage,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/laboratoires/detail-contrat',
      component: DetailContratPage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/laboratoires/detail-compte-rendu',
      component: DetailCompteRenduPage,
      activationParameters,
    },

    // {
    //   attachTo: 'MAIN',
    //   path: '/laboratoires/contact',
    //   component: ContactPage,
    //   activationParameters,
    // },

    {
      attachTo: 'MAIN',
      path: '/laboratoires/compte-rendu-form',
      component: CompteRenduFormPage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/laboratoires/detail-action-operationnelle',
      component: DetailActionOperationnelle,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/laboratoires/remuneration',
      component: RemunerationPage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: ['/laboratoires/pilotages/PLAN_MARKETING_GLOBAL'],
      component: PilotagePlanningPage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: ['/laboratoires/pilotages/PILOTAGE_REMUNERATIONS'],
      component: PilotageRemunerationPage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: ['/laboratoires/pilotages/:view'],
      component: PilotagePage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/laboratoires/condition-detail',
      component: DetailsConditionCommerciale,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/laboratoires/facture-detail',
      component: DetailsFacture,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/laboratoires/catalogue/:view',
      component: CatalogueProduitPage,
      activationParameters,
    },

    {
      attachTo: 'MAIN',
      path: '/laboratoires/:tab?',
      component: NewPagesLabo,
    },

    {
      attachTo: 'PARAMETRE',
      path: '/laboratoires',
      component: ParametreLaboratoire,
    },
  ],
};

export default laboratoireModule;
