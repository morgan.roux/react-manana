import { Add } from '@material-ui/icons';
import React, { FC } from 'react';
import { useHistory } from 'react-router';

interface FavoriteAddButtonProps {
  url: string;
}

export const FavoriteAddButton: FC<FavoriteAddButtonProps> = ({ url }) => {
  const history = useHistory();

  const handleClick = (event: any) => {
    event.preventDefault();
    event.stopPropagation();
    history.push(url);
  };

  return <Add onClick={handleClick} />;
};
