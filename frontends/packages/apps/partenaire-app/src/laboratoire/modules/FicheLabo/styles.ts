import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      paddingTop: 30,
      width: 'calc(100% + 40px)',
      marginLeft: '-20px',
      padding: 30,
      background: '#f8f8f8',
    },
    title: {
      fontSize: 18,
      fontWeight: 500,
    },
    swiperSlider: {
      display: 'flex',
      overflow: 'auto',
      flexWrap: 'nowrap',
    },
  })
);
