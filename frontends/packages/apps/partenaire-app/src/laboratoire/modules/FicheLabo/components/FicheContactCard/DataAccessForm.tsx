import React, { FC } from 'react';
import { Box, createStyles, makeStyles, Theme } from '@material-ui/core/';
import { TextField } from '@material-ui/core';
import { CustomModal } from '@lib/ui-kit';
import { PartenaireAcces } from '@lib/common/src/federation';
import { useEffect } from 'react';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: theme.spacing(1),
      minWidth: 300,
    },
    formfullWidth: {
      width: '100%',
    },
  })
);

interface DataAccessFormProps {
  mode: 'creation' | 'modification';
  open: boolean;
  setOpen: (open: boolean) => void;
  loading: boolean;
  accesToEdit?: PartenaireAcces;
  onRequestSave: (value: Partial<PartenaireAcces>) => void;
}
export const DataAccessForm: FC<DataAccessFormProps> = ({
  mode,
  accesToEdit,
  open,
  setOpen,
  loading,
  onRequestSave,
}) => {
  const classes = useStyles();
  const [values, setValues] = React.useState<Partial<PartenaireAcces>>({
    url: '',
    commentaire: '',
  });

  useEffect(() => {
    if (open) {
      if (mode == 'creation') {
        setValues({
          url: '',
          commentaire: '',
        });
      } else if (accesToEdit) {
        setValues({
          url: accesToEdit.url || '',
          commentaire: accesToEdit.commentaire || '',
        });
      }
    }
  }, [open, mode, accesToEdit]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    setValues({ ...values, [event.target.name]: event.target.value });
  };
  const handleSubmit = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    onRequestSave(values);
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Accès aux données"
      onClickConfirm={handleSubmit}
      disabledButton={loading || (!values.url && !values.commentaire)}
      headerWithBgColor
      closeIcon
    >
      <form className={classes.root} noValidate autoComplete="off">
        <Box mb={2}>
          <TextField
            className={classes.formfullWidth}
            label="Lien site web"
            onChange={handleChange}
            name="url"
            value={values.url}
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Box>
        <Box>
          <TextField
            className={classes.formfullWidth}
            label="Commentaire"
            variant="outlined"
            name="commentaire"
            multiline
            value={values.commentaire}
            rows={4}
            onChange={handleChange}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Box>
      </form>
    </CustomModal>
  );
};
