import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    svg: {
      fill: theme.palette.secondary.light,
    },
  })
);
