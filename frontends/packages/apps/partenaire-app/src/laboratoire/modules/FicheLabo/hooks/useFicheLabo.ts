import { useApplicationContext } from '@lib/common';
import {
  PrtChiffreAffaireSortFields,
  SortDirection,
  useCompute_Prt_Chiffre_AffaireLazyQuery,
  useCompute_Prt_Chiffre_AffairesLazyQuery,
  useCreate_PartenariatMutation,
  useGet_LaboratoireLazyQuery,
  useGet_List_Partenariat_StatutsLazyQuery,
  useGet_List_Partenariat_TypesLazyQuery,
  usePrt_Compte_RendusLazyQuery,
  useUpdate_PartenariatMutation,
} from '@lib/common/src/federation';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { useLaboratoireParams } from './../../../hooks/useLaboratoireParams';

export const useFicheLabo = () => {
  const { federation, currentPharmacie, currentGroupement, notify } = useApplicationContext();
  const {
    params: { idLabo, view },
  } = useLaboratoireParams('plan');
  const [savingCreatePartenariat, setSavingCreatePartenariat] = useState<boolean>(false);
  const [savedCreatePartenariat, setSavedCreatePartenariat] = useState<boolean>(false);
  const [sortTable, setSortTable] = useState<any>({
    column: 'annee',
    direction: 'DESC',
  });
  const [paginate, setPaginate] = useState<any>({
    skip: 0,
    limit: 12,
  });

  const [loadLaboratoire, loadingLaboratoire] = useGet_LaboratoireLazyQuery({ client: federation });

  const [computeChiffreAffaire, computingChiffreAffaire] = useCompute_Prt_Chiffre_AffaireLazyQuery({
    client: federation,
  });

  const [computeChiffreAffaires, computingChiffreAffaires] = useCompute_Prt_Chiffre_AffairesLazyQuery({
    client: federation,
  });

  const [createPartenariat, creatingPartenariat] = useCreate_PartenariatMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Le partenariat a été modifié avec succès!',
        type: 'success',
      });
      loadingLaboratoire?.refetch && loadingLaboratoire.refetch();
      setSavingCreatePartenariat(false);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du partenariat',
        type: 'error',
      });
    },
  });

  const [updatePartenariat, updatingPartenariat] = useUpdate_PartenariatMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Le partenariat a été modifié avec succès!',
        type: 'success',
      });
      loadingLaboratoire?.refetch && loadingLaboratoire.refetch();
      setSavingCreatePartenariat(false);
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'enregistrement de partenariat",
        type: 'error',
      });
    },
  });
  const [loadTypePartenariats, loadingTypePartenariats] = useGet_List_Partenariat_TypesLazyQuery({
    client: federation,
  });
  const [loadStatutPartenariats, loadingStatutPartenariats] = useGet_List_Partenariat_StatutsLazyQuery({
    client: federation,
  });

  const [loadCompteRendu, loadingCompteRendu] = usePrt_Compte_RendusLazyQuery({
    client: federation,
  });

  const idLaboSelected = idLabo && idLabo.length > 0 ? idLabo[0] : undefined;

  useEffect(() => {
    loadTypePartenariats();
    loadStatutPartenariats();
  }, []);

  useEffect(() => {
    if (idLaboSelected) {
      computeChiffreAffaires({
        variables: {
          filter: {
            and: [
              {
                idPartenaireTypeAssocie: {
                  eq: idLaboSelected,
                },
              },
              {
                partenaireType: {
                  eq: 'LABORATOIRE',
                },
              },
              {
                idPharmacie: {
                  eq: currentPharmacie.id,
                },
              },
            ],
          },
          sorting: sortTable.column ? [{ field: sortTable.column, direction: sortTable.direction }] : undefined,
          // paging: {
          //   limit: paginate.take,
          //   offset: paginate.skip,
          // },
        },
      });
    }
  }, [sortTable, idLaboSelected, paginate]);

  useEffect(() => {
    if (idLaboSelected) {
      loadLaboratoire({
        variables: {
          id: idLaboSelected,
          idPharmacie: currentPharmacie.id,
        },
      });
      computeChiffreAffaire({
        variables: {
          input: {
            idPartenaireTypeAssocie: idLaboSelected,
            partenaireType: 'LABORATOIRE',
          },
        },
      });
      loadCompteRendu({
        variables: {
          filter: {
            and: [
              {
                idPharmacie: {
                  eq: currentPharmacie.id,
                },
              },
              {
                idGroupement: {
                  eq: currentGroupement.id,
                },
              },
              {
                idPartenaireTypeAssocie: {
                  eq: idLaboSelected,
                },
              },
              {
                partenaireType: {
                  eq: 'LABORATOIRE',
                },
              },
            ],
          },
          paging: {
            limit: 1,
          },
        },
      });
    }
  }, [idLaboSelected]);

  const handleRequestSavePartenariat = (data: any) => {
    if (idLaboSelected) {
      setSavingCreatePartenariat(true);
      setSavedCreatePartenariat(false);
      if (data.id) {
        updatePartenariat({
          variables: {
            id: data.id,
            update: {
              idPartenaireTypeAssocie: idLaboSelected,
              partenaireType: 'LABORATOIRE',
              ...data,
              dateDebut: moment(data.dateDebut).startOf('day').utc(true).format(),
              dateFin: data.dateFin ? moment(data.dateFin).endOf('day').utc(true).format() : undefined,
              id: undefined,
              idLabo: undefined,
              typeReunion: data.typeReunion,
              optionNotifications: data.optionNotifications || [],
              correspondants: (data.correspondants || [])
                .map((correspondant: any) => {
                  return (correspondant.value || []).map((value: any) => {
                    return {
                      code: correspondant.code,
                      idContact: value.id,
                    };
                  });
                })
                .flat(),
            },
          },
        });
      } else {
        createPartenariat({
          variables: {
            input: {
              idPartenaireTypeAssocie: idLaboSelected,
              partenaireType: 'LABORATOIRE',
              ...data,
              dateDebut: moment(data.dateDebut).startOf('day').utc(true).format(),
              dateFin: data.dateFin ? moment(data.dateFin).endOf('day').utc(true).format() : undefined,
              idLabo: undefined,
              typeReunion: data.typeReunion,
              optionNotifications: data.optionNotifications || [],
              correspondants: (data.correspondants || [])
                .map((correspondant: any) => {
                  return (correspondant.value || []).map((value: any) => {
                    return {
                      code: correspondant.code,
                      idContact: value.id,
                    };
                  });
                })
                .flat(),
            },
          },
        });
      }
    }
  };

  const handleRequestFilterHistorique = ({ sortTable, paginate }: any) => {
    console.log('-----------------------sortTable: ', sortTable, paginate);
    setSortTable(sortTable);
    setPaginate(paginate);
  };

  const laboratoire = {
    data: loadingLaboratoire.data?.laboratoire,
    error: loadingLaboratoire.error,
    loading: loadingLaboratoire.loading,
  };

  const chiffreAffaire = {
    currentYear: computingChiffreAffaire.data?.computeChiffreAffaire.currentYear,
    previousYear: computingChiffreAffaire.data?.computeChiffreAffaire.previousYear,
    pourcentage: computingChiffreAffaire.data?.computeChiffreAffaire.pourcentage,
  };

  const historiqueCA = {
    data: computingChiffreAffaires.data?.pRTChiffreAffaires.nodes,
    loading: computingChiffreAffaires.loading,
    error: computingChiffreAffaires.error,
    rowsTotal: computingChiffreAffaires.data?.pRTChiffreAffaires.totalCount,
    refetch: () => {
      computingChiffreAffaires?.refetch && computingChiffreAffaires.refetch();
      computingChiffreAffaire.refetch && computingChiffreAffaire.refetch();
    },
  } as any;

  const typePartenariats = {
    data: loadingTypePartenariats.data?.pRTPartenariatTypes.nodes as any,
    loading: loadingTypePartenariats.loading,
    error: loadingTypePartenariats.error,
  } as any;

  const statutPartenariats = {
    data: loadingStatutPartenariats.data?.pRTPartenariatStatuts.nodes as any,
    loading: loadingStatutPartenariats.loading,
    error: loadingStatutPartenariats.error,
  } as any;

  const compteRendu = {
    count: loadingCompteRendu.data?.pRTCompteRendus.totalCount || 0,
    data: loadingCompteRendu.data?.pRTCompteRendus.nodes,
  };

  return {
    laboratoire,
    chiffreAffaire,
    historiqueCA,
    handleRequestSavePartenariat,
    typePartenariats,
    statutPartenariats,
    compteRendu,
    handleRequestFilterHistorique,
  };
};
