import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      marginTop: 20,
      height: 'calc(100% - 47px)',
    },
    iconContact: {
      position: 'relative',
      top: 5,
      marginRight: 8,
    },
    titleLabo: {
      fontSize: 20,
      fontWeight: 500,
    },
    laboInfos: {
      fontSize: 14,
    },
    dateLabel: {
      color: '#42424261',
    },
    dateValue: {
      fontWeight: 500,
    },
    btn: {
      cursor: 'pointer',
    },
  })
);
