import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { CustomModal, CustomTextField } from '@lib/ui-kit';
import { useStyles } from './styles';
import { useSet_Prt_Chiffre_AffaireMutation } from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';

interface ChiffreAffaireFormProps {
  refetch?: () => void;
  open: boolean;
  setOpen: (open: boolean) => void;
  partenaireType: 'LABORATOIRE' | 'PRESTATAIRE_SERVICE';
  idPartenaireTypeAssocie: string;
}

interface FormValue {
  annee: number;
  montant: number;
}

const initialValues: FormValue = {
  annee: new Date().getFullYear(),
  montant: 0,
};

export const ChiffreAffaireForm: FC<ChiffreAffaireFormProps> = ({
  open,
  setOpen,
  partenaireType,
  idPartenaireTypeAssocie,
  refetch,
}) => {
  const classes = useStyles();

  const { federation, currentPharmacie } = useApplicationContext();
  const [values, setValues] = useState<FormValue>(initialValues);

  const [updateChiffreAffaire, modificationChiffreAffaire] = useSet_Prt_Chiffre_AffaireMutation({
    client: federation,
    onCompleted: () => {
      refetch && refetch();
      setOpen(false);
    },
  });

  useEffect(() => {
    if (!open) {
      setValues((prev) => ({ ...prev, montant: 0 }));
    }
  }, [open]);

  const handleChange = (name: keyof FormValue, value: any) => {
    setValues((prev) => ({ ...prev, [name]: value }));
  };

  const handleSubmit = () => {
    updateChiffreAffaire({
      variables: {
        input: {
          partenaireType,
          idPartenaireTypeAssocie,
          annee: parseInt(`${values.annee}`, 10),
          montant: parseFloat(`${values.montant}`.replace(',', '.')),
          idPharmacie: currentPharmacie.id,
        },
      },
    });
  };

  return (
    <CustomModal
      open={open}
      closeIcon
      headerWithBgColor
      maxWidth="xl"
      setOpen={setOpen}
      title="Chiffre d'affaire"
      onClickConfirm={handleSubmit}
      disabledButton={modificationChiffreAffaire.loading || !values.annee || !values.montant}
      withBtnsActions
    >
      <Box className={classes.textFieldAjout}>
        <CustomTextField
          variant="outlined"
          label="Année"
          type="number"
          style={{ marginBottom: '16px' }}
          value={values.annee}
          onChange={(event) => handleChange('annee', event.target.value)}
          className={classes.customText}
        />
        <CustomTextField
          variant="outlined"
          label="Chiffre d'affaire"
          type="number"
          style={{ marginBottom: '16px' }}
          value={values.montant}
          onChange={(event) => handleChange('montant', event.target.value)}
          className={classes.customText}
        />
      </Box>
    </CustomModal>
  );
};
