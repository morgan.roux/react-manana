import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    card: {
      height: '100%',
      minWidth: 320,
      '&:hover': {
        '& #actionBtn': {
          opacity: 0.6,
          transition: '.5s',
        },
      },
    },
    actionBtnClass: {
      [theme.breakpoints.up('sm')]: {
        opacity: 0,
      },
      transition: '.5s',
    },
    fullname: {
      fontSize: 16,
      fontWeight: 500,
    },
    posttitle: {
      fontSize: 14,
    },
    contact: {
      color: '#8e8e8e',
      fontSize: 14,
    },
    spnIcon: {
      position: 'relative',
      top: 6,
      marginRight: 5,
    },
    btnVp: {
      textTransform: 'none',
    },
    cardcontent: {
      height: 'calc(100% - 116px)',
    },
    flexCenter: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      height: '100%',
    },

    spnNbAccess: {
      fontSize: 12,
      padding: 5,
      background: '#ccc',
      borderRadius: 5,
      marginLeft: 15,
    },
    dataAccessItem: {
      background: '#f8f8f8',
      padding: 10,
      borderRadius: 10,
      marginBottom: 15,
    },
    boxDataAccess: {
      overflow: 'auto',
      height: 100,
      '&::-webkit-scrollbar': {
        width: 0,
      },
    },
    btnDelete: {
      color: '#f00',
    },
    withTwoBtn: {
      '& button': {
        padding: 5,
      },
    },
    rootContactCard: {
      [theme.breakpoints.down('sm')]: {
        width: 'calc(33.33% - 20px)',
      },
      [theme.breakpoints.down('xs')]: {
        width: '100%',
      },
      width: 'calc(25% - 20px)',
      display: 'inline-block',
      marginRight: 20,
    },
  })
);
