import React, { FC, useState, ChangeEvent, useEffect } from 'react';
import { CustomFormTextField, CustomModal, CustomSelect } from '@lib/ui-kit';

import { Box, FormControl, Typography } from '@material-ui/core';
import { useStyles } from './styles';
import { Autocomplete } from '@material-ui/lab';
import { PartenaireInput, useApplicationContext, CIVILITE_LIST, useUploadFiles } from '@lib/common';
import {
  PrtContactFullInfoFragment,
  Secteur,
  useDepartementsQuery,
  useFonctionsQuery,
  DepartementSortFields,
  SortDirection,
  useCreate_Prt_ContactMutation,
  useUpdate_Prt_ContactMutation,
  FichierInput,
  useGet_LaboratoireQuery,
  useGet_Prestataire_ServiceQuery,
} from '@lib/common/src/federation';
import { SelectAvatar } from '../select-avatar/SelectAvatar';
import { TypeFichier } from '@lib/common/src/graphql';
import CustomCheckbox from '@lib/ui-kit/src/components/atoms/CustomCheckBox/CustomCheckbox';
import { useLaboratoireParams } from './../../../../hooks/useLaboratoireParams';
import { isEmailValid } from '@lib/common';

interface ContactFormProps {
  mode: 'creation' | 'modification';
  partenaireType: 'LABORATOIRE' | 'PRESTATAIRE_SERVICE';
  idPartenaireTypeAssocie: string;
  open: boolean;
  idSecteur?: string;
  setOpen: (open: boolean) => void;
  contactToEdit?: PrtContactFullInfoFragment;
  refetch?: () => void;
}

interface ContactValueProps {
  adresseEmail: string;
  pageWeb?: string;
  adresseEmailProf?: string;
  bureauTelephone?: string;
  domicileTelephone?: string;
  mobileTelephone?: string;
  linkedIn?: string;
  facebook?: string;
  messenger?: string;
  whatsApp?: string;
  youTube?: string;
}

interface PrtContactValueProps {
  civilite?: string;
  nom?: string;
  prenom?: string;
  idFonction: string;
  adresse?: string;
  ville?: string;
  cp?: string;
  idDepartements?: string[];
  partenaireTypeAssocie: any;
  prive?: boolean;
}

const ContactValue = {
  adresseEmail: '',
  pageWeb: '',
  adresseEmailProf: '',
  bureauTelephone: '',
  domicileTelephone: '',
  mobileTelephone: '',
  linkedIn: '',
  facebook: '',
  messenger: '',
  whatsApp: '',
  youTube: '',
};

const PrtContactValue = {
  civilite: '',
  idFonction: '',
  nom: '',
  prenom: '',
  adresse: '',
  ville: '',
  cp: '',
  idDepartements: [] as string[],
  partenaireTypeAssocie: {},
  prive: true,
};

const ContactForm: FC<ContactFormProps> = ({
  mode,
  open,
  setOpen,
  contactToEdit,
  refetch,
  idSecteur,
  partenaireType,
  idPartenaireTypeAssocie,
}) => {
  const classes = useStyles();

  const { isMobile, currentPharmacie, federation, notify, graphql } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();
  const [saving, setSaving] = useState<boolean>(false);
  const [photo, setPhoto] = useState<any>(null);

  const {
    params: { idPrestataireService },
  } = useLaboratoireParams();

  const loadingLaboratoire = useGet_LaboratoireQuery({
    client: federation,
    variables: {
      id: idPartenaireTypeAssocie,
      idPharmacie: currentPharmacie.id,
    },
  });

  const loadingPrestataire = useGet_Prestataire_ServiceQuery({
    client: federation,
    variables: {
      id: idPartenaireTypeAssocie,
    },
  });

  const loadingDepartements = useDepartementsQuery({
    client: federation,
    variables: {
      paging: { offset: 0, limit: 1000 },
      sorting: [
        {
          field: DepartementSortFields.Code,
          direction: SortDirection.Asc,
        },
      ],
    },
  });

  const loadingFonctions = useFonctionsQuery({
    client: federation,
    variables: {
      paging: {
        offset: 0,
        limit: 1000,
      },
    },
  });

  const [createContact] = useCreate_Prt_ContactMutation({
    onCompleted: () => {
      notify({
        message: ' Le contact a été crée avec succés !',
        type: 'success',
      });

      refetch && refetch();

      setSaving(false);
      setOpen(false);
    },
    onError: () => {
      notify({
        message: 'Erreurs lors de la création du Contact.',
        type: 'error',
      });
    },
  });

  const [updateContact] = useUpdate_Prt_ContactMutation({
    onCompleted: () => {
      notify({
        message: 'Le Contact a été modifié avec succès !',
        type: 'success',
      });

      refetch && refetch();
      setSaving(false);
      setOpen(false);
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du contact de laboratoire',
        type: 'error',
      });
    },
  });

  // STATE

  const [prtContact, setPrtContact] = useState<PrtContactValueProps>(PrtContactValue);
  const [contact, setContact] = useState<ContactValueProps>(ContactValue);
  const [emailError, setEmailError] = useState<string>('');
  const [activeStep, setActiveStep] = useState<number>(0);

  /** ****************************Empty field if open modal creation ou fermer la modification ****************************************** */
  useEffect(() => {
    if ((open && mode === 'creation') || (!open && mode === 'modification')) {
      setPrtContact(PrtContactValue);
      setContact(ContactValue);
      setActiveStep(0);
    }
  }, [open]);

  /** ****************************END Open modal if error saving ****************************************** */
  useEffect(() => {
    if (contactToEdit && mode === 'modification') {
      setPrtContact({
        civilite: contactToEdit.civilite || undefined,
        idFonction: contactToEdit.idFonction || '',
        idDepartements: ((contactToEdit as any).departements || []).map(({ id }: any) => id),
        nom: contactToEdit.nom,
        prenom: contactToEdit.prenom ? contactToEdit.prenom : '',
        adresse: contactToEdit.contact?.adresse1 ? contactToEdit.contact.adresse1 : '',
        ville: contactToEdit.contact?.ville ? contactToEdit.contact.ville : '',
        cp: contactToEdit.contact?.cp ? contactToEdit.contact?.cp : '',
        partenaireTypeAssocie: {
          id: contactToEdit.partenaireTypeAssocie?.id,
          nom: contactToEdit.partenaireTypeAssocie?.nom,
        },
        prive: contactToEdit.prive || false,
      });
      setContact({
        adresseEmail: contactToEdit.contact?.mailPerso ? contactToEdit.contact.mailPerso : '',
        pageWeb: contactToEdit.contact?.sitePerso ? contactToEdit.contact.sitePerso : '',
        adresseEmailProf: contactToEdit.contact?.mailProf ? contactToEdit.contact.mailProf : '',
        bureauTelephone: contactToEdit.contact?.telProf ? contactToEdit.contact.telProf : '',
        domicileTelephone: contactToEdit.contact?.telPerso ? contactToEdit.contact.telPerso : '',
        mobileTelephone: contactToEdit.contact?.telMobPerso ? contactToEdit.contact.telMobPerso : '',
        linkedIn: contactToEdit.contact?.urlLinkedInPerso ? contactToEdit.contact.urlLinkedInPerso : '',
        facebook: contactToEdit.contact?.urlFacebookPerso ? contactToEdit.contact.urlFacebookPerso : '',
        messenger: contactToEdit.contact?.urlMessenger ? contactToEdit.contact?.urlMessenger : '',
        whatsApp: contactToEdit.contact?.whatsappMobPerso ? contactToEdit.contact.whatsappMobPerso : '',
        youTube: contactToEdit.contact?.urlYoutube ? contactToEdit.contact?.urlYoutube : '',
      });
    } else if (mode === 'creation') {
      setPrtContact((prev) => ({
        ...(prev || {}),
        partenaireTypeAssocie: idPrestataireService
          ? {
              id: loadingPrestataire.data?.prestataireService?.id,
              nom: loadingPrestataire.data?.prestataireService?.nom,
            }
          : (loadingLaboratoire.data?.laboratoire as any),
        idDepartements: currentPharmacie.idDepartement ? [currentPharmacie.idDepartement] : [],
      }));
    }
  }, [contactToEdit, open]);

  const handleChangePrtContact = (e: ChangeEvent<HTMLInputElement>) => {
    if (e.target.name === 'prive') {
      setPrtContact({ ...prtContact, [e.target.name]: e.target.checked });
    } else {
      setPrtContact({ ...prtContact, [e.target.name]: e.target.value });
    }
  };
  const handleChangeContact = (e: ChangeEvent<HTMLInputElement>) => {
    setContact({ ...contact, [e.target.name]: e.target.value });
  };
  const handleChangePartenaireTypeAssocie = (value: any) => {
    setPrtContact({ ...prtContact, partenaireTypeAssocie: value });
  };

  /** **************************SUBMIT DATA ****************************************************** */

  const saveContact = (contactInput: any, userPhoto: any) => {
    // console.log('detection', userPhoto);
    const contact = contactInput.contact;
    setSaving(true);
    if (contactInput.id) {
      updateContact({
        variables: {
          id: contactInput.id,
          input: {
            idSecteur: idSecteur,
            civilite: contactInput.civilite,
            idFonction: contactInput.idFonction,
            nom: contactInput.nom,
            prenom: contactInput.prenom,
            idDepartements: contactInput.idDepartements,
            prive: contactInput.prive,
            contact: {
              adresse1: contact.adresse1,
              mailPerso: contact.adresseEmail,
              mailProf: contact.adresseEmailProf,
              telProf: contact.bureauTelephone,
              cp: contact.cp,
              telPerso: contact.domicileTelephone,
              telMobPerso: contact.mobileTelephone,
              urlFacebookPerso: contact.facebook,
              urlLinkedInPerso: contact.linkedIn,
              urlMessenger: contact.messenger,
              sitePerso: contact.pageWeb,
              ville: contact.ville,
              whatsappMobPerso: contact.whatsApp,
              urlYoutube: contact.youTube,
            },
            photo: userPhoto ? userPhoto[0] : undefined,
            partenaireType,
            idPartenaireTypeAssocie,
          },
        },
      });
    } else {
      createContact({
        variables: {
          input: {
            idSecteur: idSecteur,
            civilite: contactInput.civilite,
            idFonction: contactInput.idFonction,
            idDepartements: contactInput.idDepartements,
            nom: contactInput.nom,
            prenom: contactInput.prenom,
            prive: contactInput.prive,
            contact: {
              adresse1: contact.adresse1,
              mailPerso: contact.adresseEmail,
              mailProf: contact.adresseEmailProf,
              telProf: contact.bureauTelephone,
              cp: contact.cp,
              telPerso: contact.domicileTelephone,
              telMobPerso: contact.mobileTelephone,
              urlFacebookPerso: contact.facebook,
              urlLinkedInPerso: contact.linkedIn,
              urlMessenger: contact.messenger,
              sitePerso: contact.pageWeb,
              ville: contact.ville,
              whatsappMobPerso: contact.whatsApp,
              urlYoutube: contact.youTube,
            },
            photo: userPhoto ? userPhoto[0] : undefined,
            partenaireType,
            idPartenaireTypeAssocie,
          },
        },
      });
    }
  };

  const handleSaveContact = (contactInput: any) => {
    if (photo && photo.size) {
      uploadFiles([photo], {
        directory: 'relation-fournisseurs/contact',
      })
        .then((uploadedFiles) => {
          saveContact(contactInput, uploadedFiles);
        })
        .catch((error) => {
          notify({
            type: 'error',
            message: `Une erreur s'est produite lors de chargement du fichier`,
          });
        });
    } else {
      let userPhoto: FichierInput | null = null;
      if (photo && photo.id) {
        userPhoto = {
          chemin: photo.chemin,
          nomOriginal: photo.nomOriginal,
          type: TypeFichier.Avatar,
        };
        saveContact(contactInput, [userPhoto]);
      } else {
        saveContact(contactInput, null);
      }
    }
  };

  const handleSubmit = () => {
    if (contact.adresseEmail && !isEmailValid(contact.adresseEmail)) {
      setEmailError('Adresse Email invalide');
    } else {
      const data: any = {
        id: mode === 'modification' ? contactToEdit?.id : undefined,
        civilite: prtContact.civilite,
        nom: prtContact.nom,
        prenom: prtContact.prenom,
        idFonction: prtContact.idFonction,
        idDepartements: prtContact.idDepartements?.length ? prtContact.idDepartements : undefined,
        prive: new Boolean(prtContact.prive),
        contact: {
          ...contact,
          cp: prtContact.cp,
          ville: prtContact.ville,
          adresse1: prtContact.adresse,
        },
        idPartenaireTypeAssocie: prtContact.partenaireTypeAssocie?.id,
      };
      handleSaveContact(data);
      setActiveStep(0);
    }
  };
  /** **************************END SUBMIT DATA ****************************************************** */

  /** **************************rADIO GROUP ****************************************************** */

  /** ****************************Empty field if open modal creation ou fermer la modification ****************************************** */

  /*const isEmail = () => {
    return contact.adresseEmail ? contact.adresseEmail && isEmailValid(contact.adresseEmail) : true;
  };*/

  const isFormValid = () => {
    return prtContact.idFonction && prtContact.partenaireTypeAssocie; // && isEmail();
  };

  const isNextButtonDisabledStepper = () => {
    if (activeStep === 0) {
      return !(prtContact.civilite === '' || prtContact.nom === '' || prtContact.idFonction === '');
    } else {
      return isFormValid();
    }
  };

  const showDepartement = prtContact?.idFonction
    ? (loadingFonctions.data?.fonctions.nodes || []).find((fonction) => fonction.id === prtContact.idFonction)
        ?.competenceTerritoriale
    : false;

  console.log('----------------------partenaire associe----------------: ', prtContact.partenaireTypeAssocie);

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={mode === 'modification' ? 'Modification de Contact' : 'Ajout de Contact'}
      withBtnsActions
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      fullWidth
      fullScreen={!!isMobile}
      disabledButton={!isFormValid()}
      onClickConfirm={handleSubmit}
    >
      <Box display="flex" flexDirection="column">
        <SelectAvatar photo={photo} setPhoto={setPhoto} />
        <Box my={2} className={classes.infoPersonnelle}>
          <Typography className={classes.textTitle}>Informations Personnelles</Typography>
          <CustomSelect
            list={CIVILITE_LIST}
            index="libelle"
            listId="id"
            label="Civilité"
            name="civilite"
            value={prtContact.civilite}
            onChange={handleChangePrtContact}
            className={classes.selectPadding10}
          />
          <CustomFormTextField
            type="text"
            placeholder="Prénom"
            label="Prénom"
            name="prenom"
            value={prtContact.prenom}
            onChange={handleChangePrtContact}
          />
          <CustomFormTextField
            type="text"
            placeholder="Nom"
            label="Nom"
            name="nom"
            value={prtContact.nom}
            onChange={handleChangePrtContact}
          />
          <Box mb={2}>
            <PartenaireInput
              onChange={handleChangePartenaireTypeAssocie}
              value={prtContact.partenaireTypeAssocie}
              index={partenaireType === 'LABORATOIRE' ? 'laboratoire' : 'partenaire'}
              label={partenaireType === 'LABORATOIRE' ? 'Labo' : 'Prestaire de service'}
              // disabled={idPartenaireTypeAssocie ? true : false}
            />
          </Box>
          <CustomSelect
            list={loadingFonctions.data?.fonctions.nodes || []}
            loading={loadingFonctions.loading}
            error={loadingFonctions.error}
            index="libelle"
            listId="id"
            label="Fonction"
            name="idFonction"
            value={prtContact.idFonction}
            onChange={handleChangePrtContact}
            required
          />
          {showDepartement && partenaireType === 'LABORATOIRE' && (
            <Autocomplete
              loading={loadingDepartements.loading}
              multiple
              options={loadingDepartements.data?.departements.nodes || []}
              getOptionLabel={(option: any) => (option?.code ? `${option.code} - ${option.nom}` : option?.nom)}
              onChange={(_event, newValue) => {
                setPrtContact((prev) => ({
                  ...prev,
                  idDepartements: newValue.map(({ id }) => id),
                }));
              }}
              value={(loadingDepartements.data?.departements.nodes || []).filter(({ id }: any) =>
                prtContact.idDepartements?.includes(id)
              )}
              renderInput={(params) => (
                <CustomFormTextField
                  {...params}
                  variant="standard"
                  // label="Départements"
                  placeholder="Les départements aux quels est rattaché ce contact"
                />
              )}
            />
          )}

          <FormControl className={classes.checkbox} style={{ marginBottom: '16px', marginTop: '10px', width: '100%' }}>
            <CustomCheckbox
              value={prtContact.prive}
              name="prive"
              checked={prtContact.prive}
              onClick={(e: any) => e.stopPropagation()}
              onChange={handleChangePrtContact}
              label="Contact propre à la pharmacie"
            />
          </FormControl>
        </Box>
        <Box my={2} className={classes.infoPersonnelle}>
          <Typography className={classes.textTitle}>Coordonnées</Typography>
          <CustomFormTextField
            type="text"
            placeholder="Adresse de la Personne"
            label="Adresse"
            name="adresse"
            value={prtContact.adresse}
            onChange={handleChangePrtContact}
          />
          <Box display="flex" flexDirection="row">
            <Box mr={1} width={1} style={{ width: '60%' }}>
              <CustomFormTextField
                type="text"
                placeholder="Ville"
                label="Ville"
                name="ville"
                value={prtContact.ville}
                onChange={handleChangePrtContact}
              />
            </Box>
            <Box ml={1} width={1} style={{ width: '40%' }}>
              <CustomFormTextField
                type="text"
                placeholder="Code Postal"
                label="Code Postal"
                name="cp"
                value={prtContact.cp}
                onChange={handleChangePrtContact}
              />
            </Box>
          </Box>
          <CustomFormTextField
            type="text"
            placeholder="Mobile de la personne"
            label="Mobile"
            name="mobileTelephone"
            value={contact.mobileTelephone}
            onChange={handleChangeContact}
          />
          <CustomFormTextField
            error={emailError !== ''}
            type="email"
            placeholder="Adresse de Messagerie de la Personne"
            label="Adresse de messagerie"
            name="adresseEmail"
            value={contact.adresseEmail}
            onChange={handleChangeContact}
            helperText={emailError}
          />
        </Box>
      </Box>
    </CustomModal>
  );
};

{
  /* <Stepper
        disableNextBtn={!isNextButtonDisabledStepper()}
        onStepChange={setActiveStep}
        onCancel={() => setOpen(false)}
        onValidate={handleSubmit}
        disableValidateBtn={!isFormValid()}
        loading={saving}
        steps={[
          {
            title: 'Informations',
            content: (
              <div>
                <SelectAvatar photo={photo} setPhoto={setPhoto} />
                <Box my={2} className={classes.infoPersonnelle}>
                  <Typography className={classes.textTitle}>Informations Personnelles</Typography>
                  <CustomSelect
                    list={listCivilites}
                    index="libelle"
                    listId="id"
                    label="Civilité"
                    name="civilite"
                    value={prtContact.civilite}
                    onChange={handleChangePrtContact}
                    required
                    className={classes.selectPadding10}
                  />
                  <CustomFormTextField
                    type="text"
                    required
                    placeholder="Nom"
                    label="Nom"
                    name="nom"
                    value={prtContact.nom}
                    onChange={handleChangePrtContact}
                  />
                  <CustomFormTextField
                    type="text"
                    placeholder="Prénom"
                    label="Prénom"
                    name="prenom"
                    value={prtContact.prenom}
                    onChange={handleChangePrtContact}
                  />
                  <PartenaireInput
                    onChange={handleChangePartenaireTypeAssocie}
                    value={prtContact.partenaireTypeAssocie}
                    index="laboratoire"
                    label="Labo"
                  />
                  <CustomSelect
                    list={fonctions.data || []}
                    loading={fonctions.loading}
                    error={fonctions.error}
                    index="libelle"
                    listId="id"
                    label="Fonction"
                    name="idFonction"
                    value={prtContact.idFonction}
                    onChange={handleChangePrtContact}
                    required
                  />
                  {showDepartement && (
                    <Autocomplete
                      loading={departements.loading}
                      multiple
                      options={departements.data || []}
                      getOptionLabel={(option: any) => (option?.code ? `${option.code} - ${option.nom}` : option?.nom)}
                      onChange={(_event, newValue) => {
                        setPrtContact((prev) => ({
                          ...prev,
                          idDepartements: newValue.map(({ id }) => id),
                        }));
                      }}
                      value={(departements.data || []).filter(({ id }: any) => prtContact.idDepartements.includes(id))}
                      renderInput={(params) => (
                        <CustomFormTextField
                          {...params}
                          variant="standard"
                          // label="Départements"
                          placeholder="Les départements aux quels est rattaché ce contact"
                        />
                      )}
                    />
                  )}
                  <FormControl
                    className={classes.checkbox}
                    style={{ marginBottom: '16px', marginTop: '10px', width: '100%' }}
                  >
                    <CustomCheckbox
                      value={prtContact.prive}
                      name="prive"
                      checked={prtContact.prive}
                      onClick={(e: any) => e.stopPropagation()}
                      onChange={handleChangePrtContact}
                      label="Contact propre à la pharmacie"
                    />
                  </FormControl>
                </Box>

                <Box my={2} className={classes.infoPersonnelle}>
                  <Typography className={classes.textTitle}>Coordonnées</Typography>
                  <CustomFormTextField
                    type="text"
                    placeholder="Adresse de la Personne"
                    label="Adresse"
                    name="adresse"
                    value={prtContact.adresse}
                    onChange={handleChangePrtContact}
                  />
                  <Box display="flex" flexDirection="row">
                    <Box mr={1} width={1} style={{ width: '60%' }}>
                      <CustomFormTextField
                        type="text"
                        placeholder="Ville"
                        label="Ville"
                        name="ville"
                        value={prtContact.ville}
                        onChange={handleChangePrtContact}
                      />
                    </Box>
                    <Box ml={1} width={1} style={{ width: '40%' }}>
                      <CustomFormTextField
                        type="text"
                        placeholder="Code Postal"
                        label="Code Postal"
                        name="cp"
                        value={prtContact.cp}
                        onChange={handleChangePrtContact}
                      />
                    </Box>
                  </Box>
                </Box>
              </div>
            ),
          },
          {
            title: 'Contact',
            content: (
              <div>
                
                <Box my={2}>
                  <Typography className={classes.textTitle}>Internet</Typography>
                  <CustomFormTextField
                    error={emailError !== ''}
                    type="text"
                    required
                    placeholder="Adresse de Messagerie de la Personne"
                    label="Adresse de messagerie"
                    name="adresseEmail"
                    value={contact.adresseEmail}
                    onChange={handleChangeContact}
                    helperText={emailError}
                  />
                  <CustomFormTextField
                    type="text"
                    placeholder="Page Web de la Personne"
                    label="Page Web"
                    name="pageWeb"
                    value={contact.pageWeb}
                    onChange={handleChangeContact}
                  />
                  <CustomFormTextField
                    error={messagerieError !== ''}
                    type="text"
                    placeholder="Adresse de messagerie instantannée de la personne"
                    label="Adresse de messagerie instantannée"
                    name="adresseEmailProf"
                    value={contact.adresseEmailProf}
                    onChange={handleChangeContact}
                    helperText={messagerieError}
                    required={true}
                  />
                </Box>
                <Box my={2}>
                  <Typography className={classes.textTitle}>Numéros de Téléphone</Typography>
                  <CustomFormTextField
                    type="text"
                    placeholder="Bureau de la Personne"
                    label="Bureau"
                    name="bureauTelephone"
                    value={contact.bureauTelephone}
                    onChange={handleChangeContact}
                  />
                  <CustomFormTextField
                    type="text"
                    placeholder="Domicile de la Personne"
                    label="Domicile"
                    name="domicileTelephone"
                    value={contact.domicileTelephone}
                    onChange={handleChangeContact}
                  />
                  <CustomFormTextField
                    type="text"
                    placeholder="Mobile de la personne"
                    label="Mobile"
                    name="mobileTelephone"
                    value={contact.mobileTelephone}
                    onChange={handleChangeContact}
                  />
                </Box>
                <Box my={2}>
                  <Typography className={classes.textTitle}>Réseaux Sociaux</Typography>
                  <CustomFormTextField
                    type="text"
                    placeholder="LinkedIn"
                    label="LinkedIn"
                    name="linkedIn"
                    value={contact.linkedIn}
                    onChange={handleChangeContact}
                  />
                  <CustomFormTextField
                    type="text"
                    placeholder="Facebook"
                    label="Facebook"
                    name="facebook"
                    value={contact.facebook}
                    onChange={handleChangeContact}
                  />
                  <CustomFormTextField
                    type="text"
                    placeholder="WhatsApp"
                    label="WhatsApp"
                    name="whatsApp"
                    value={contact.whatsApp}
                    onChange={handleChangeContact}
                  />
                  <CustomFormTextField
                    type="text"
                    placeholder="Messenger"
                    label="Messenger"
                    name="messenger"
                    value={contact.messenger}
                    onChange={handleChangeContact}
                  />
                  <CustomFormTextField
                    type="text"
                    placeholder="Youtube"
                    label="Youtube"
                    name="youTube"
                    value={contact.youTube}
                    onChange={handleChangeContact}
                  />
                </Box>
              </div>
            ),
          },
        ]}
      /> */
}

export default ContactForm;
