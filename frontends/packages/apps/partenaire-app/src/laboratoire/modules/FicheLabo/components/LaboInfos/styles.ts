import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      marginTop: 20,
      height: 'calc(100% - 47px)',
      '&:not(:hover)': {
        '& #historique_id': {
          opacity: 0,
        },
      },
      '& #historique_id': {
        opacity: 0.5,
      },
    },
    img: {
      objectFit: 'cover',
      objectPosition: 'center',
      width: 80,
      height: 80,
      [theme.breakpoints.down('sm')]: {
        width: 52,
        height: 52,
      },
    },
    iconContact: {
      position: 'relative',
      top: 5,
      marginRight: 8,
    },
    titleLabo: {
      fontSize: 20,
      fontWeight: 500,
    },
    laboInfos: {
      fontSize: 14,
    },
    titleChiffre: {
      fontSize: 14,
      color: '#9E9E9E',
      padding: 0,
      border: 'none',
    },
    valueChiffre: {
      fontSize: 28,
      fontWeight: 500,
      padding: 0,
      border: 'none',
    },
    percentageIcon: {
      marginTop: 15,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    chiffreroot: {
      background: '#F8F8F8',
      padding: 10,
      borderRadius: 15,
      height: '100%',
    },
    modalRoot: {
      width: '100%',
      display: 'flex',
      padding: '10px',
      justifyContent: 'center',
      '& .main-MuiAvatar-root': {
        marginRight: '16px'
      },
    },
    modalText: {
      display: 'flex',
      flexDirection: 'column',
    },
    textField: {
      display: 'flex',
      flexDirection: 'column',
      margin: 'auto',
      padding: '10px',
    },
    parameter: {
      float: 'right',
      alignItems: 'baseline',
      marginBottom: '24px',
    },
  })
);
