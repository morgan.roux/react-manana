import React, { useState } from 'react';
import { FC } from 'react';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';

import { PrtChiffreAffaireInfoFragment, Laboratoire } from '@lib/common/src/federation';
import { useStyles } from './styles';
import {
  Card,
  CardContent,
  Grid,
  Box,
  Typography,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Link,
} from '@material-ui/core';
import { Place, Phone, Language } from '@material-ui/icons';
import { Historique } from './Historique/Historique';
import { PartenaireForm } from '@lib/common';
import { ChiffreAffaireForm } from './ChiffreAffaireForm/ChiffreAffaireForm';
import { CustomAvatar } from '@lib/ui-kit';
import { useFicheLabo } from '../../hooks/useFicheLabo';
interface LaboInfosProps {
  laboratoire?: Laboratoire;
  goCompteRendu?: (event: React.MouseEvent<{}>) => void;
  onEditLabo?: (event: React.MouseEvent<{}>) => void;
  chiffreAffaire?: any;
  historiqueCA?: {
    data: PrtChiffreAffaireInfoFragment[];
    loading: boolean;
    error: boolean;
    rowsTotal: number;
    refetch?: () => void;
  };
  hasCompteRendu?: boolean;
}
export const LaboInfos: FC<LaboInfosProps> = ({
  laboratoire,
  onEditLabo,
  chiffreAffaire,
  historiqueCA,
  goCompteRendu,
  hasCompteRendu,
}) => {
  const classes = useStyles();
  const [openPartenaireForm, setOpenPartenaireForm] = useState<boolean>(false);
  const [openCAForm, setOpenCAForm] = useState<boolean>(false);

  const currentYear = new Date().getFullYear();
  const previousYear = currentYear - 1;

  const { handleRequestFilterHistorique } = useFicheLabo();

  const laboPhone = laboratoire?.laboratoireSuite?.telephone;
  const laboWeb = laboratoire?.laboratoireSuite?.webSiteUrl;
  const isHttpUrl = (url: string) => {
    const isHttp = url.startsWith('http', 0);
    return isHttp ? url : `https://${url}`;
  };
  return (
    <>
      <Card className={classes.root} elevation={2}>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item md={6} xs={12}>
              <div>
                <EditIcon
                  style={{ float: 'right', fontSize: '16px', cursor: 'pointer', marginRight: '10px' }}
                  onClick={() => {
                    setOpenPartenaireForm(true);
                  }}
                  id="historique_id"
                />
              </div>
              <Box display="flex">
                <Box mr={3}>
                  <CustomAvatar
                    name={laboratoire?.nom || ''}
                    className={classes.img}
                    url={laboratoire?.photo?.publicUrl as any}
                    alt="laboratoire"
                  />
                </Box>

                <Box width="100%">
                  <Box display="flex" justifyContent="space-between" alignItems="center">
                    <Box>
                      <Typography className={classes.titleLabo}>{laboratoire?.nom || 'N/A'}</Typography>
                    </Box>
                    {/* <Box>
                  <IconButton onClick={onEditLabo}>
                    <EditIcon />
                  </IconButton>
                </Box> */}
                  </Box>
                  <Box>
                    <Box pb={1}>
                      <Typography className={classes.laboInfos}>
                        <Place className={classes.iconContact} />
                        &nbsp;{laboratoire?.laboratoireSuite?.adresse || 'N/A'}
                      </Typography>
                    </Box>
                    <Box pb={1}>
                      <Typography className={classes.laboInfos}>
                        {laboPhone ? (
                          <Link href={`tel:${laboPhone}`}>
                            <Phone className={classes.iconContact} />
                            Tél:&nbsp;
                            {laboPhone}
                          </Link>
                        ) : (
                          <>
                            <Phone className={classes.iconContact} />
                            Tél:&nbsp; N/A
                          </>
                        )}
                      </Typography>
                    </Box>
                    <Box pb={1}>
                      <Typography className={classes.laboInfos}>
                        {laboWeb ? (
                          <Link href={isHttpUrl(laboWeb)} target="_blank" rel="noreferrer">
                            <Language className={classes.iconContact} />
                            {laboWeb}
                          </Link>
                        ) : (
                          <>
                            <Language className={classes.iconContact} />
                            N/A
                          </>
                        )}
                      </Typography>
                    </Box>
                    {hasCompteRendu && (
                      <Box>
                        <Typography
                          className={classes.laboInfos}
                          onClick={goCompteRendu}
                          color="primary"
                          style={{ cursor: 'pointer' }}
                        >
                          Dernier compte rendu
                        </Typography>
                      </Box>
                    )}
                  </Box>
                </Box>
              </Box>
            </Grid>
            <Grid item md={6} xs={12}>
              <div className={classes.chiffreroot}>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell align="center" className={classes.titleChiffre}>
                        Chiffre d'affaire {previousYear}
                      </TableCell>
                      <TableCell align="center" className={classes.titleChiffre}>
                        Chiffre d'affaire {currentYear}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" className={classes.valueChiffre}>
                        {(chiffreAffaire.previousYear?.montant || 0).toFixed(2)} €
                      </TableCell>
                      <TableCell align="center" className={classes.valueChiffre}>
                        {(chiffreAffaire.currentYear?.montant || 0).toFixed(2)} €
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
                {chiffreAffaire.pourcentage ? (
                  <div
                    className={classes.percentageIcon}
                    style={{
                      color: `${
                        chiffreAffaire.previousYear?.montant < chiffreAffaire.currentYear?.montant
                          ? '#34a745'
                          : '#F05365'
                      }`,
                    }}
                  >
                    {chiffreAffaire.previousYear?.montant < chiffreAffaire.currentYear?.montant ? (
                      <>
                        <ArrowUpwardIcon />
                        <>+</>
                      </>
                    ) : (
                      <>
                        <ArrowDownwardIcon />
                      </>
                    )}
                    {chiffreAffaire.pourcentage.toFixed(2)} %
                  </div>
                ) : null}
                <Box className={classes.parameter}>
                  <Historique historiques={historiqueCA} onRequestFilter={handleRequestFilterHistorique} />
                  <AddIcon
                    style={{
                      float: 'right',
                      fontSize: '20px',
                      cursor: 'pointer',
                      marginRight: '12px',
                      marginTop: '4px',
                    }}
                    onClick={() => {
                      setOpenCAForm(true);
                    }}
                    id="historique_id"
                  />
                </Box>
              </div>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
      <PartenaireForm
        partenaireAssocieToEdit={laboratoire}
        open={openPartenaireForm}
        setOpen={setOpenPartenaireForm}
        partenaireType="LABORATOIRE"
        mode="modification"
      />
      {laboratoire?.id && (
        <ChiffreAffaireForm
          refetch={historiqueCA?.refetch}
          open={openCAForm}
          setOpen={setOpenCAForm}
          partenaireType="LABORATOIRE"
          idPartenaireTypeAssocie={laboratoire.id}
        />
      )}
    </>
  );
};
