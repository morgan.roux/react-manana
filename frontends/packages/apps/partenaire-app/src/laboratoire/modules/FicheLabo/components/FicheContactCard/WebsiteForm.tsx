import React, { FC, useEffect } from 'react';
import { Box, createStyles, makeStyles, Theme } from '@material-ui/core/';
import { TextField } from '@material-ui/core';
import { CustomModal } from '@lib/ui-kit';
import { PrtContactCredential } from '@lib/common/src/federation';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: theme.spacing(1),
      minWidth: 300,
    },
    formfullWidth: {
      width: '100%',
      '& input': {
        padding: '14.5px',
      },
    },
  })
);

interface FormValues {
  url: string;
  nomService: string;
  commentaire: string;
}

const initialValues: FormValues = {
  url: '',
  nomService: '',
  commentaire: '',
};

interface WebsiteFormProps {
  mode: 'creation' | 'modification';
  open: boolean;
  setOpen: (open: boolean) => void;
  loading: boolean;
  contactToEdit?: PrtContactCredential;
  onRequestSave: (value: Partial<PrtContactCredential>) => void;
  idSecteur: string;
}
export const WebsiteForm: FC<WebsiteFormProps> = ({
  mode,
  contactToEdit,
  open,
  setOpen,
  loading,
  onRequestSave,
  idSecteur,
}) => {
  const classes = useStyles();
  const [values, setValues] = React.useState<FormValues>(initialValues);

  useEffect(() => {
    if (open) {
      if (mode == 'creation') {
        setValues(initialValues);
      } else if (contactToEdit) {
        setValues({
          url: contactToEdit.url || '',
          commentaire: contactToEdit.commentaire || '',
          nomService: contactToEdit.nomService || '',
        });
      }
    }
  }, [open, mode, contactToEdit]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    setValues({ ...values, [event.target.name]: event.target.value });
  };
  const handleSubmit = (event: React.MouseEvent<{}>) => {
    event.preventDefault();

    onRequestSave({ ...values, nomService: values.nomService || values.url, idSecteur });
  };

  console.log('***********************handleSubmit*******', values);

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={mode === 'modification' ? 'Modification site web' : 'Ajouter site web'}
      onClickConfirm={handleSubmit}
      disabledButton={loading || (!values.url && !values.commentaire)}
      headerWithBgColor
      closeIcon
    >
      <form className={classes.root} noValidate autoComplete="off">
        <Box mb={2}>
          <TextField
            className={classes.formfullWidth}
            label="Nom"
            onChange={handleChange}
            name="nomService"
            InputLabelProps={{
              shrink: true,
            }}
            value={values.nomService}
            variant="outlined"
          />
        </Box>
        <Box mb={2}>
          <TextField
            className={classes.formfullWidth}
            label="Lien du site web"
            onChange={handleChange}
            name="url"
            value={values.url}
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Box>
        <Box>
          <TextField
            className={classes.formfullWidth}
            label="Commentaire"
            variant="outlined"
            name="commentaire"
            multiline
            value={values.commentaire}
            rows={4}
            onChange={handleChange}
            InputLabelProps={{
              shrink: true,
            }}
          />
        </Box>
      </form>
    </CustomModal>
  );
};
