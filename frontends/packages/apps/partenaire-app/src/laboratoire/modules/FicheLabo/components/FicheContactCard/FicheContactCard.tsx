import React, { FC } from 'react';
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  IconButton,
  Link,
  Typography,
} from '@material-ui/core';
import { Add, Email, Phone, Settings, Room, Edit, Language, Delete } from '@material-ui/icons';
import { CustomAvatar } from '@lib/ui-kit';
import { useStyles } from './styles';
import {
  PartenaireContactUnion,
  PrtContact,
  PrestataireService,
  Secteur,
  PrtContactCredential,
  PartenaireAcces,
} from '@lib/common/src/federation';
import { nl2br, PartenaireSelect } from '@lib/common';
import classnames from 'classnames';
const isHttpUrl = (url: string) => {
  const isHttp = url.startsWith('http', 0);
  return isHttp ? url : `https://${url}`;
};

interface FicheContactCardProps {
  secteur: Secteur;
  contacts?: PartenaireContactUnion[];
  onClickAddContact: (secteur: Secteur) => void;
  onClickEditContact: (secteur: Secteur, contact: PartenaireContactUnion) => void;
  onClickVoirPlus: (secteur?: Secteur, prestataireService?: PrestataireService) => void;
  onChangePartenariatSelect: (value: any) => void;
  onRequestDeletePrestataireServiceContact: (secteur: Secteur) => void;
  onClickAddAcces: (prestataireService: PrestataireService) => void;
  onClickAddWebsite?: (secteur: Secteur) => void;
  onClickEditDataAccess: (acces: PartenaireAcces, prestataireService?: PrestataireService) => void;
  onClickDeleteDataAccess: (id: string) => void;
  onClickDeleteWebsite: (id: string) => void;
}
export const FicheContactCard: FC<FicheContactCardProps> = ({
  secteur,
  contacts,
  onClickAddContact,
  onClickVoirPlus,
  onChangePartenariatSelect,
  onClickEditContact,
  onClickAddAcces,
  onRequestDeletePrestataireServiceContact,
  onClickAddWebsite,
  onClickDeleteDataAccess,
  onClickDeleteWebsite,
  onClickEditDataAccess,
}) => {
  const classes = useStyles();
  const [prestataireService, setPrestataireService] = React.useState<any>();
  const [switchOn, setSwitchOn] = React.useState<boolean>(false);

  const selectedPrestaireService = (contacts || []).find((contact) => contact.dataType === 'PrestataireService');
  const isWebsiteContact = (contacts || []).some((contact) => contact.dataType === 'PRTContactCredential');

  React.useEffect(() => {
    setSwitchOn(!!selectedPrestaireService);
    setPrestataireService(selectedPrestaireService);
  }, [selectedPrestaireService]);

  const handleSwitchOn = (on: boolean) => {
    if (selectedPrestaireService && !on) {
      onRequestDeletePrestataireServiceContact(secteur);
    } else {
      setSwitchOn(on);
    }
  };

  return (
    <Grid item md={3}>
      <Card className={classes.card} elevation={3}>
        <CardHeader
          title={secteur?.libelle || '-'}
          action={
            <PartenaireSelect
              noTitle
              customFilterIcon={<Settings className={classes.actionBtnClass} id="actionBtn" />}
              iconDefaultStyle
              onChange={setPrestataireService}
              value={prestataireService}
              onClickValidate={() => onChangePartenariatSelect(prestataireService)}
              index="partenaire"
              withSwitcher
              switchOn={switchOn}
              onSwitchChange={handleSwitchOn}
              switcherLabel="Services externalisés"
              withSelectAll={false}
              key={secteur?.id}
              idSecteur={secteur?.id}
              cerclePartenariat="PHARMACIE"
              onCreated={(newPrestataireService) => onChangePartenariatSelect(newPrestataireService)}
              mustFilter={
                secteur?.id
                  ? [
                      {
                        term: {
                          idSecteur: secteur.id,
                        },
                      },
                    ]
                  : undefined
              }
            />
          }
        />
        <CardContent className={classes.cardcontent}>
          {contacts?.length ? (
            contacts.slice(0, 2).map((item) => {
              const isWebsite = item.dataType === 'PRTContactCredential';

              if (isWebsite) {
                const website = item as PrtContactCredential;

                return (
                  <Box display="flex" justifyContent="space-around">
                    <Box mr={2}>
                      <Typography className={classes.fullname}>{website.nomService}</Typography>

                      <Box width={1}>
                        <Typography>
                          <span className={classes.contact}>Site : </span>
                          <Link href={isHttpUrl(website.url || '')} target="_blank" rel="noreferrer">
                            {' '}
                            {website.url}
                          </Link>
                        </Typography>
                        <Typography>
                          <span className={classes.contact}>commentaire :</span>
                          <p dangerouslySetInnerHTML={{ __html: nl2br(website.commentaire || '') || '' }} />
                        </Typography>
                      </Box>
                    </Box>
                    <Box className={classes.withTwoBtn}>
                      <IconButton
                        className={classes.actionBtnClass}
                        id="actionBtn"
                        onClick={() => onClickEditContact(secteur, item)}
                      >
                        <Edit />
                      </IconButton>
                      <IconButton
                        className={classnames(classes.actionBtnClass, classes.btnDelete)}
                        id="actionBtn"
                        onClick={() => onClickDeleteWebsite(item.id)}
                      >
                        <Delete />
                      </IconButton>
                    </Box>
                  </Box>
                );
              }

              const contact: PrestataireService | PrtContact = item as PrestataireService | PrtContact;

              const isPrestataireService = contact.dataType === 'PrestataireService';
              const fullname = isPrestataireService
                ? (contact as PrestataireService).nom
                : `${(contact as PrtContact).prenom} ${contact.nom}`;
              const postTitle = isPrestataireService
                ? 'Prestataire de service'
                : (contact as PrtContact).fonction?.libelle;

              const phone = isPrestataireService
                ? (contact as PrestataireService).prestataireServiceSuite?.telephone
                : (contact as PrtContact).contact?.telProf ||
                  (contact as PrtContact).contact?.telMobProf ||
                  (contact as PrtContact).contact?.telPerso ||
                  (contact as PrtContact).contact?.telMobPerso;

              const dataAccesCount = isPrestataireService ? (contact as PrestataireService)?.acces?.length || 0 : 0;
              const dataAcces = isPrestataireService
                ? ((contact as PrestataireService)?.acces || []).map((access) => (
                    <Box className={classes.dataAccessItem}>
                      <Box display="flex" justifyContent="space-between">
                        <div>
                          <Typography>
                            <span className={classes.contact}>Site :</span>
                            <Link href={isHttpUrl(access.url || '')} target="_blank" rel="noreferrer">
                              {access.url}
                            </Link>
                          </Typography>
                          <Typography>
                            <span className={classes.contact}>commentaire :</span> {access.commentaire}
                          </Typography>
                        </div>
                        <Box className={classes.withTwoBtn}>
                          <IconButton
                            className={classes.actionBtnClass}
                            id="actionBtn"
                            onClick={() => onClickEditDataAccess(access, selectedPrestaireService as any)}
                          >
                            <Edit />
                          </IconButton>
                          <IconButton
                            className={classnames(classes.actionBtnClass, classes.btnDelete)}
                            id="actionBtn"
                            onClick={() => onClickDeleteDataAccess(access.id)}
                          >
                            <Delete />
                          </IconButton>
                        </Box>
                      </Box>
                    </Box>
                  ))
                : null;

              const adresses = isPrestataireService
                ? (contact as PrestataireService).prestataireServiceSuite?.adresse1
                : undefined;

              const email = isPrestataireService
                ? (contact as PrestataireService).prestataireServiceSuite?.email
                : (contact as PrtContact).contact?.mailProf || (contact as PrtContact).contact?.mailPerso;

              const webSiteUrl = isPrestataireService
                ? (contact as PrestataireService).prestataireServiceSuite?.webSiteUrl
                : (contact as PrtContact).contact?.sitePerso;

              return (
                <>
                  <Box display="flex" mb={3} justifyContent="space-around">
                    <Box mr={2}>
                      <CustomAvatar name={contact.nom} url={contact.photo?.publicUrl as any} />
                    </Box>
                    <Box mr={2}>
                      <Typography className={classes.fullname}>{fullname}</Typography>
                      <Typography className={classes.posttitle}>{postTitle}</Typography>
                      {adresses && (
                        <Typography className={classes.contact}>
                          <span className={classes.spnIcon}>
                            <Room />
                          </span>
                          {adresses}
                        </Typography>
                      )}
                      {phone && (
                        <Typography className={classes.contact}>
                          <Link href={`tel:${phone}`}>
                            <span className={classes.spnIcon}>
                              <Phone />
                            </span>
                            {phone}
                          </Link>
                        </Typography>
                      )}
                      {email && (
                        <Typography className={classes.contact}>
                          <Link href={`mailto:${email}`}>
                            <span className={classes.spnIcon}>
                              <Email />
                            </span>
                            {email}
                          </Link>
                        </Typography>
                      )}
                      {webSiteUrl && (
                        <Typography className={classes.contact}>
                          <Link href={isHttpUrl(webSiteUrl)} target="_blank" rel="noreferrer">
                            <span className={classes.spnIcon}>
                              <Language />
                            </span>
                            {webSiteUrl}
                          </Link>
                        </Typography>
                      )}
                    </Box>
                    <Box>
                      <IconButton
                        className={classes.actionBtnClass}
                        id="actionBtn"
                        onClick={() => onClickEditContact(secteur, contact)}
                      >
                        <Edit />
                      </IconButton>
                    </Box>
                  </Box>
                  {isPrestataireService && (
                    <>
                      <Box>
                        <Divider />
                      </Box>
                      <Box display="flex" justifyContent="space-between" alignItems="center">
                        <Typography style={{ fontWeight: 500 }}>
                          Accès aux données : <span className={classes.spnNbAccess}>{dataAccesCount}</span>
                        </Typography>
                        <IconButton onClick={() => onClickAddAcces(contact as any)}>
                          <Add />
                        </IconButton>
                      </Box>

                      <Box className={classes.boxDataAccess}>{dataAcces}</Box>
                    </>
                  )}
                </>
              );
            })
          ) : (
            <Box className={classes.flexCenter}>
              <Box textAlign="center">
                <Typography>Aucun contact pour le moment</Typography>
                <Box my={2}>
                  <Button color="primary" startIcon={<Add />} onClick={() => onClickAddContact(secteur)}>
                    Ajouter un contact
                  </Button>
                </Box>
                {/*<Box my={2}>
                  <Button color="primary" startIcon={<Add />} onClick={handleOpenPartenaireForm}>
                    Ajouter un prestataire de service
                  </Button>
          </Box>*/}
                {secteur.code === 'TRADE' && (
                  <Box my={2}>
                    <Button
                      color="primary"
                      startIcon={<Add />}
                      onClick={(event) => {
                        event.preventDefault();
                        event.stopPropagation();
                        onClickAddWebsite && onClickAddWebsite(secteur);
                      }}
                    >
                      Ajouter un site web
                    </Button>
                  </Box>
                )}
              </Box>
            </Box>
          )}
        </CardContent>
        {!isWebsiteContact && contacts?.length ? (
          <CardActions>
            <Box textAlign="center" width="100%">
              <Button
                onClick={() => onClickVoirPlus(secteur, selectedPrestaireService as any)}
                className={classes.btnVp}
              >
                Voir plus
              </Button>
            </Box>
          </CardActions>
        ) : null}
      </Card>
    </Grid>
  );
};
