import { makeStyles, createStyles } from '@material-ui/core';
import GREY from '@material-ui/core/colors/grey';

export const useStyles = makeStyles((theme) =>
  createStyles({
    root: {},
    textTitle: {
      fontSize: 18,
      fontWeight: 'bold',
      color: GREY[600],
      marginBottom: '16px !important',
    },
    line: {
      width: '30%',
      borderBottom: 'solid 1px',
      position: 'relative',
      top: -30,
      borderBottomColor: GREY[600],
    },
    textNormal: {
      color: GREY[400],
      fontSize: 12,
    },
    selectPadding10: {
      '& .MuiSelect-root': {
        padding: 6,
      },
    },
    information: {
      // display: 'flex',
      width: '100%',
      border: '1px solid #9E9E9E',
      padding: '20px',
      borderRadius: '4px',
    },
    infoPersonnelle: {
      [theme.breakpoints.down('sm')]: {
        marginLeft: 'auto !important',
        marginRight: 'auto !important',
      },
    },
    informationUser: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    motscles: {
      width: '100%',
      display: 'flex',
      '& .MuiFormControl-root': {
        // maxWidth: '302px',
        minWidth: '302px',
        width: '100%',
      },
      '& .MuiSelect-root': {
        height: '0',
      },
      [theme.breakpoints.down('xs')]: {
        '& .MuiFormControl-root': {
          // maxWidth: '302px',
          minWidth: '150px',
          width: '100%',
        },
        '& .MuiSelect-root': {
          height: '0',
        },
      },
    },
    sexe: {
      width: '100%',
      marginLeft: '16px',
      display: 'flex',
      '& .MuiFormControl-root': {
        width: '100%',
        // maxWidth: '149px',
        minWidth: '149px',
      },
      '& .MuiSelect-root': {
        height: '0',
      },
      [theme.breakpoints.down('xs')]: {
        '& .MuiFormControl-root': {
          width: '100%',
          // maxWidth: '149px',
          minWidth: '75px',
        },
        '& .MuiSelect-root': {
          height: '0',
        },
      },
    },
    avatar: {
      marginTop: '16px',
      marginLeft: '16px',
      '& .MuiSvgIcon-root': {
        background: '#E0E0E0',
        marginLeft: '16px',
        borderRadius: '20px',
        padding: '2px',
        width: '25px',
      },
    },
    avatarTypography: {
      '& .MuiTypography-root': {
        fontSize: '17px',
        top: '545px',
        left: '733px',
        /* width: 90px; */
        height: '17px',
        color: '#878787',
        fontFamily: 'Roboto,Regular',
      },
    },
    sectionPhoto: {
      width: '100%',
    },
    radiogroup: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      [theme.breakpoints.down('sm')]: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
    },
    checkbox: {
      marginBottom: '16px',
      display: 'flex',
    },
  })
);
