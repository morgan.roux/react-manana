import { Box, Grid, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import { useLaboratoireParams } from '../../hooks/useLaboratoireParams';
import { useHistory } from 'react-router';

import { FicheLaboPartenariat } from './components/FicheLaboPartenariat/FicheLaboPartenariat';
import { LaboInfos } from './components/LaboInfos/LaboInfos';
import { useFicheLabo } from './hooks/useFicheLabo';
import { useStyles } from './styles';
import { UseContactCard } from './UseContactCard';
import ContactPage from '../contact/pages/contact/Contact';
import { CustomModal } from '@lib/ui-kit';
import { PartenaireInput, useApplicationContext } from '@lib/common';

export const FicheLabo = () => {
  const classes = useStyles();
  const { params, redirectTo } = useLaboratoireParams();
  const { idSecteur, idLabo } = params;
  const { isMobile } = useApplicationContext();
  const [partenaireAssocie, setPartenaireAssocie] = useState<any>();
  // const [searchText, setSearchText] = useState<string>('');

  const {
    laboratoire,
    chiffreAffaire,
    historiqueCA,
    typePartenariats,
    statutPartenariats,
    compteRendu,
    handleRequestSavePartenariat,
  } = useFicheLabo();

  const history = useHistory();

  const handleToCompteRendu = () => {
    if (compteRendu.data) {
      const withIdLabo = idLabo ? `&idLabo=${idLabo[0]}` : '';
      history.push(`/laboratoires/detail-compte-rendu?id=${compteRendu.data[0].id}${withIdLabo}`);
    }
  };
  console.log(<UseContactCard />);
  return !idLabo?.length ? (
    <>
      <CustomModal
        open={!idLabo || (idLabo && !idLabo[0])}
        setOpen={() => {}}
        title="Choix du labo"
        closeIcon
        headerWithBgColor
        maxWidth="sm"
        fullScreen={!!isMobile}
        actionButtonTitle="Choisir"
        onClickConfirm={() => redirectTo({ ...params, idLabo: [partenaireAssocie.id] })}
        customHandleClose={() => history.goBack()}
        disabledButton={!partenaireAssocie}
      >
        <Box display="flex" flexDirection="column">
          <Box mb={3}>
            <Typography>Veuillez choisir un laboratoire</Typography>
          </Box>
          <PartenaireInput
            index="laboratoire"
            onChange={(value: any) => setPartenaireAssocie(value)}
            value={partenaireAssocie}
            label="laboratoire"
          />
        </Box>
      </CustomModal>
    </>
  ) : idSecteur ? (
    <ContactPage />
  ) : (
    <div className={classes.root}>
      {/* <Box maxWidth="300px" paddingBottom={1}>
        <DebouncedSearchInput value={searchText} placeholder="Rechercher" fullWidth={false} onChange={setSearchText} />
      </Box> */}
      <Grid container spacing={3}>
        <Grid item lg={6} md={6} xs={12}>
          <Typography className={classes.title}>Infos labo</Typography>
          <LaboInfos
            laboratoire={laboratoire.data as any}
            chiffreAffaire={chiffreAffaire}
            historiqueCA={historiqueCA}
            goCompteRendu={handleToCompteRendu}
            hasCompteRendu={compteRendu && compteRendu?.count > 0}
          />
        </Grid>
        <Grid item lg={6} md={6} xs={12}>
          <Typography className={classes.title}>Partenariat</Typography>
          <FicheLaboPartenariat
            laboratoire={laboratoire.data as any}
            typePartenariats={typePartenariats}
            statutPartenariats={statutPartenariats}
            handleRequestSavePartenariat={handleRequestSavePartenariat}
          />
        </Grid>
      </Grid>
      <div>
        <Box my={2}>
          <Box mb={2}>
            <Typography className={classes.title}>Contacts</Typography>
          </Box>
          <UseContactCard />
        </Box>
      </div>
    </div>
  );
};
