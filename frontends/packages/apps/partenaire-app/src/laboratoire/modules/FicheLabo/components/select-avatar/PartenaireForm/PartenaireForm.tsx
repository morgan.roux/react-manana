import { Box } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { CustomModal, CustomTextField } from '@lib/ui-kit';
import {
  Laboratoire,
  PrestataireService,
  PrestataireServiceSuite,
  LaboratoireSuite,
  useUpdate_One_LaboratoireMutation,
  useUpdate_One_Prestataire_ServiceMutation,
} from '@lib/common/src/federation';
import { useStyles } from './partenaire-form-styles';
import { Dropzone, UploadedFile, useApplicationContext } from '@lib/common';
import { useEffect } from 'react';

interface PartenaireFormProps {
  mode: 'creation' | 'modification';
  partenaireType: 'PRESTATAIRE_SERVICE' | 'LABORATOIRE';
  partenaireAssocieToEdit?: Laboratoire | PrestataireService;

  open: boolean;
  setOpen: (open: boolean) => void;
}

interface FormValues {
  files: (File | UploadedFile)[]; // Logo

  nom: string;
  adresse: string;
  codePostal: string;
  ville: string;
  telephone: string;
  email: string;
  webSiteUrl: string;
}

const initialValues: FormValues = {
  files: [],
  nom: '',
  adresse: '',
  codePostal: '',
  ville: '',
  telephone: '',
  email: '',
  webSiteUrl: '',
};

export const PartenaireForm: FC<PartenaireFormProps> = ({
  mode,
  partenaireType,
  partenaireAssocieToEdit,
  open,
  setOpen,
}) => {
  const classes = useStyles();
  const { federation, uploadFiles, notify, currentPharmacie } = useApplicationContext();
  const [values, setValues] = useState<FormValues>(initialValues);
  const [saving, setSaving] = useState<boolean>(false);

  const [updateLaboratoire] = useUpdate_One_LaboratoireMutation({
    client: federation,
    onCompleted: () => {
      setSaving(false);
      setOpen(false);
    },
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de modification de laboratoire`,
      });
      setSaving(false);
    },
  });

  const [updatePrestataireService] = useUpdate_One_Prestataire_ServiceMutation({
    client: federation,
    onCompleted: () => {
      setSaving(false);
      setOpen(false);
    },
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite lors de modification de prestataire de service`,
      });
      setSaving(false);
    },
  });

  useEffect(() => {
    if (open) {
      if (mode === 'modification') {
        if (partenaireAssocieToEdit) {
          const partenaireSuite: PrestataireServiceSuite | LaboratoireSuite | undefined | null =
            partenaireType === 'LABORATOIRE'
              ? (partenaireAssocieToEdit as Laboratoire).laboratoireSuite
              : (partenaireAssocieToEdit as PrestataireService).prestataireServiceSuite;

          const adresse =
            partenaireType === 'LABORATOIRE'
              ? (partenaireSuite as LaboratoireSuite)?.adresse
              : (partenaireSuite as PrestataireServiceSuite)?.adresse1;

          setValues({
            files: partenaireAssocieToEdit.photo?.chemin ? ([partenaireAssocieToEdit.photo] as any) : [],
            nom: partenaireAssocieToEdit.nom,
            adresse: adresse || '',
            codePostal: partenaireSuite?.codePostal || '',
            ville: partenaireSuite?.ville || '',
            telephone: partenaireSuite?.telephone || '',
            email: partenaireSuite?.email || '',
            webSiteUrl: partenaireSuite?.webSiteUrl || '',
          });
        }
      } else {
        setValues(initialValues);
      }
    }
  }, [partenaireAssocieToEdit, mode, open]);

  const handleChange = (name: keyof FormValues, value: any) => {
    setValues((prev) => ({ ...prev, [name]: value }));
  };

  const handleSave = () => {
    if (partenaireAssocieToEdit) {
      setSaving(true);
      uploadFiles(values.files, {
        directory: partenaireType === 'LABORATOIRE' ? `laboratoires` : `prestataires_service`,
      })
        .then((filesInput) => {
          const input = {
            photo: filesInput.length > 0 ? filesInput[0] : undefined,
            nom: partenaireAssocieToEdit.nom,
            adresse: values.adresse,
            codePostal: values.codePostal,
            ville: values.ville,
            telephone: values.telephone,
            email: values.email,
            webSiteUrl: values.webSiteUrl,
          };

          if (partenaireType === 'LABORATOIRE') {
            updateLaboratoire({
              variables: {
                id: partenaireAssocieToEdit.id,
                idPharmacie: currentPharmacie.id,
                input,
              },
            });
          } else {
            updatePrestataireService({
              variables: {
                id: partenaireAssocieToEdit.id,
                input,
              },
            });
          }
        })
        .catch((error) => {
          notify({
            type: 'error',
            message: `Une erreur s'est produite lors de chargement du fichier`,
          });
          setSaving(false);
        });
    }
  };

  const partenaireTitle = partenaireType === 'LABORATOIRE' ? 'Laboratoire' : 'Prestataire service';

  return (
    <CustomModal
      open={open}
      closeIcon
      headerWithBgColor
      maxWidth="xl"
      setOpen={setOpen}
      onClickConfirm={handleSave}
      disabledButton={saving || !values.nom}
      title={mode === 'modification' ? `Modification de ${partenaireTitle}` : `Création de ${partenaireTitle}`}
      withBtnsActions
    >
      <Box className={classes.root}>
        <Box className={classes.textField}>
          <Dropzone
            multiple={false}
            acceptFiles="image/*"
            selectedFiles={values.files}
            setSelectedFiles={(value) => handleChange('files', value)}
            className={classes.dropZone}
          />
        </Box>
        <CustomTextField
          className={classes.textField}
          placeholder="Nom"
          variant="outlined"
          label="Nom"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.nom}
          onChange={(event) => handleChange('nom', event.target.value)}
        />
        <CustomTextField
          className={classes.textField}
          placeholder="Adresse"
          variant="outlined"
          label="Adresse"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.adresse}
          onChange={(event) => handleChange('adresse', event.target.value)}
        />
        <CustomTextField
          className={classes.textField}
          placeholder="Code postal"
          variant="outlined"
          label="CP"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.codePostal}
          onChange={(event) => handleChange('codePostal', event.target.value)}
        />
        <CustomTextField
          className={classes.textField}
          placeholder="Ville"
          variant="outlined"
          label="Ville"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.ville}
          onChange={(event) => handleChange('ville', event.target.value)}
        />
        <CustomTextField
          className={classes.textField}
          placeholder="Email"
          variant="outlined"
          label="Email"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.email}
          onChange={(event) => handleChange('email', event.target.value)}
        />
        <CustomTextField
          className={classes.textField}
          placeholder="Téléphone"
          variant="outlined"
          label="Téléphone"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.telephone}
          onChange={(event) => handleChange('telephone', event.target.value)}
        />

        <CustomTextField
          className={classes.textField}
          placeholder="Site Web"
          variant="outlined"
          label="Site Web"
          style={{ marginBottom: '16px' }}
          InputLabelProps={{
            shrink: true,
          }}
          value={values.webSiteUrl}
          onChange={(event) => handleChange('webSiteUrl', event.target.value)}
        />
      </Box>
    </CustomModal>
  );
};
