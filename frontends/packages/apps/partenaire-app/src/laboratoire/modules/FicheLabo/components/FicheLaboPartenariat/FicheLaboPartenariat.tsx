import React, { useState } from 'react';
import { Typography, IconButton, Button, Grid, Divider } from '@material-ui/core';
import { FC } from 'react';
import { Box, Card, CardContent } from '@material-ui/core';
import { useStyles } from './styles';
import { SvgPartenaire } from './../../assets/svgPartenaire';
import { SvgGroupement } from './../../assets/svgGroupement';
import moment from 'moment';
import { PartenariatForm } from './../../../partenariat/pages/PartenariatForm';
import { PartenariatTypeInfoFragment, PartenariatStatutInfoFragment } from '@lib/common/src/federation';
interface FicheLaboPartenariatProps {
  laboratoire?: any;
  handleRequestSavePartenariat?: (data: any) => void;
  typePartenariats?: {
    data: PartenariatTypeInfoFragment[];
    loading: boolean;
    error: boolean;
  };
  statutPartenariats?: {
    data: PartenariatStatutInfoFragment[];
    loading: boolean;
    error: boolean;
  };
}
export const FicheLaboPartenariat: FC<FicheLaboPartenariatProps> = ({
  laboratoire,
  handleRequestSavePartenariat,
  typePartenariats,
  statutPartenariats,
}) => {
  const classes = useStyles();
  const [openEditPartenariat, setOpenEditPartenariat] = useState<boolean>(false);
  const handleOpenModalEditPartenariat = () => {
    setOpenEditPartenariat((prev) => !prev);
  };

  return (
    <Card className={classes.root} elevation={2}>
      <CardContent
        style={!laboratoire ? { minHeight: 205, display: 'flex', justifyContent: 'center', alignItems: 'center' } : {}}
      >
        {laboratoire?.partenariat ? (
          <>
            <Box width="100%">
              <Box display="flex" justifyContent="space-between" alignItems="center">
                <Box>
                  {laboratoire?.partenariat?.type?.code === 'GROUPEMENT' ? <SvgGroupement /> : <SvgPartenaire />}
                </Box>
                <Box>
                  <Typography
                    className={classes.btn}
                    variant="button"
                    onClick={handleOpenModalEditPartenariat}
                    color="primary"
                  >
                    Modifier le partenariat
                  </Typography>
                </Box>
              </Box>
              <Divider style={{ marginBottom: 30 }} />
              <Grid container spacing={4}>
                <Grid item md={6} xs={12}>
                  <Box display="flex" justifyContent="space-between" mb={3}>
                    <Typography className={classes.dateLabel}>Date début :</Typography>
                    <Typography className={classes.dateValue}>
                      {laboratoire?.partenariat?.dateDebut
                        ? moment(laboratoire?.partenariat?.dateDebut).utc(true).startOf('day').format('DD/MM/YYYY')
                        : '-'}
                    </Typography>
                  </Box>
                  <Box display="flex" justifyContent="space-between">
                    <Typography className={classes.dateLabel}>Date fin :</Typography>
                    <Typography className={classes.dateValue}>
                      {laboratoire?.partenariat?.dateFin
                        ? moment.utc(laboratoire?.partenariat?.dateFin).format('DD/MM/YYYY')
                        : '-'}
                    </Typography>
                  </Box>
                </Grid>
                <Grid item md={6} xs={12}>
                  <Box display="flex" justifyContent="space-between" mb={3}>
                    <Typography className={classes.dateLabel}>Status partenariat :</Typography>
                    <Typography className={classes.dateValue}>
                      {laboratoire?.partenariat?.statut?.libelle || '-'}
                    </Typography>
                  </Box>
                  <Box display="flex" justifyContent="space-between">
                    <Typography className={classes.dateLabel}>Type Partenariat :</Typography>
                    <Typography className={classes.dateValue}>
                      {laboratoire?.partenariat?.type?.libelle || '-'}
                    </Typography>
                  </Box>
                </Grid>
              </Grid>
            </Box>
          </>
        ) : (
          <>
            <Box textAlign="center">
              <Typography>Aucun partenariat pour le moment</Typography>
              <Box mt={2}>
                <Typography
                  className={classes.btn}
                  variant="button"
                  color="primary"
                  onClick={handleOpenModalEditPartenariat}
                >
                  Définir un partenariat
                </Typography>
              </Box>
            </Box>
          </>
        )}
        {openEditPartenariat && (
          <PartenariatForm
            open={openEditPartenariat}
            setOpen={handleOpenModalEditPartenariat}
            onSave={handleRequestSavePartenariat}
            partenariat={laboratoire?.partenariat as any}
            idLabo={laboratoire?.id || ''}
            typesPartenariats={typePartenariats?.data as any}
            statutsPartenariats={statutPartenariats?.data as any}
          />
        )}
      </CardContent>
    </Card>
  );
};
