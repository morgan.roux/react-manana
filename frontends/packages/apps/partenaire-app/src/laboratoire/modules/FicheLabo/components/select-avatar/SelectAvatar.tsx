import { Dropzone, useApplicationContext } from '@lib/common';
import ChoiceAvatarForm from '@lib/common/src/components/ChoiceAvatarForm';
import { CustomModal, CustomReactEasyCrop, NewCustomButton } from '@lib/ui-kit';
import { Box, FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import { last } from 'lodash';
import React, { Dispatch, FC, SetStateAction, useCallback, useEffect, useState } from 'react';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera';
import useStyles from './styles';

import { useTheme } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';

interface SelectAvatarProps {
  photo: any;
  setPhoto: Dispatch<SetStateAction<any>>;
}

export const SelectAvatar: FC<SelectAvatarProps> = ({ photo, setPhoto }) => {
  const classes = useStyles({});

  const { config } = useApplicationContext();

  const [openResize, setOpenResize] = useState<boolean>(false);
  const [croppedImgUrl, setCroppedImgUrl] = useState<any>('');
  const [savedCroppedImage, setSavedCroppedImage] = useState<boolean>(false);

  const [imgType, setImgType] = useState<'AVATAR' | 'IMPORT'>('AVATAR');
  const [selectedAvatar, setSelectedAvatar] = useState<any>(null);
  const [avatarList, setAvatarList] = useState<any[]>([]);

  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  const [croppedFiles, setCroppedFiles] = useState<File[]>([]);
  const [imageUrl, setImageUrl] = useState<string | null | undefined>(undefined);

  const [userPhotoSrc, setUserPhotoSrc] = useState<any>(null);
  const [show, setShow] = useState<boolean>(false);

  useEffect(() => {
    if (photo && !photo.avatar) {
      setImgType('IMPORT');
    } else {
      setSelectedAvatar(photo && photo.avatar);
    }
  }, [photo]);

  // const handleChangeRadioGroup = (event: React.ChangeEvent<HTMLInputElement>) => {
  //   setImgType((event.target as HTMLInputElement).value as any);
  //   setSelectedFiles([]);
  //   setCroppedFiles([]);
  //   setPhoto(null);
  // };

  const handleAvatarChange = (event: React.MouseEvent<{}>) => {
    if (imgType === 'AVATAR' || !show) {
      setShow(!show);
    }
    setImgType('AVATAR');
    setSelectedFiles([]);
    setCroppedFiles([]);
    setPhoto(null);
  };

  const handleImporteChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (imgType === 'IMPORT' || !show) {
      setShow(!show);
    }
    setImgType((event.target as HTMLInputElement).value as any);
    setImgType('IMPORT');
    setSelectedFiles([]);
    setCroppedFiles([]);
  };

  const deleteAvatar = () => {};

  const handleClickResize = useCallback(() => {
    setOpenResize(true);
    // Reset cropped image
    setSavedCroppedImage(false);
    setCroppedImgUrl(null);
  }, []);

  const onClickAvatar = (avatar: any) => {
    if (avatar) {
      const photo = { ...avatar.fichier, idAvatar: avatar.id };
      if (photo) setPhoto(photo as any);
    }
  };

  const saveResizedImage = () => {
    setOpenResize(false);
    setSavedCroppedImage(true);
  };

  /**
   * Set set userPhotoSrc
   */
  useEffect(() => {
    const image = selectedFiles && selectedFiles.length > 0 && last(selectedFiles);
    const photo = imageUrl || (image && URL.createObjectURL(image));
    if (photo) {
      setUserPhotoSrc(photo);
    }
  }, [imageUrl, selectedFiles]);

  /**
   * Set user photo variables
   */
  useEffect(() => {
    let newPhoto: File | undefined;
    if (croppedFiles && croppedFiles.length > 0) {
      newPhoto = last(croppedFiles);
    } else if (selectedFiles && selectedFiles.length > 0) {
      newPhoto = last(selectedFiles);
    }
    if (newPhoto) {
      setPhoto(newPhoto);
    }
  }, [croppedFiles, selectedFiles]);

  /**
   * Reset preview
   */
  useEffect(() => {
    if (selectedFiles && selectedFiles.length > 0) {
      // Reset cropped image
      setCroppedImgUrl(null);
      // Open Resize
      handleClickResize();
    }
  }, [selectedFiles]);

  /**
   * Set image url
   */
  useEffect(() => {
    if (photo && photo.chemin) {
      setImageUrl(`${config.STORAGE_PUBLIC_BASE_PATH}/${photo.chemin}`);
    }

    if (selectedFiles && selectedFiles.length > 0) {
      const file = last(selectedFiles);
      if (file) {
        const url = URL.createObjectURL(file);
        setImageUrl(url);
      }
    }
  }, [photo, selectedFiles]);

  const theme = useTheme();
  const match = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Box maxWidth="816px" width="100%">
      <div className={classes.imgFormContainer}>
        <Typography className={classes.inputTitle}>Photo</Typography>
        <Box className={classes.buttonRoot}>
          <NewCustomButton
            // className={classes.vue}
            onClick={handleAvatarChange}
            startIcon={<AccountCircleIcon />}
            theme="transparent"
            shadow={false}
            style={{
              textTransform: 'none',
              fontWeight: 400,
              paddingInline: '10px !important',
            }}
          >
            Choisir un avatar
          </NewCustomButton>
          <NewCustomButton
            // className={classes.vue}
            onClick={handleImporteChange}
            startIcon={<PhotoCameraIcon />}
            theme="transparent"
            shadow={false}
            style={{
              textTransform: 'none',
              fontWeight: 400,
              marginRight: 24,
              paddingInline: '10px !important',
            }}
          >
            Importer une photo
          </NewCustomButton>
        </Box>
        {/* <RadioGroup
          row={true}
          aria-label="imgType"
          name="imgType"
          value={imgType}
          onChange={handleChangeRadioGroup}
          className={classes.radioGroup}
        >
          {match ? (
            <>
              <FormControlLabel
                value="AVATAR"
                control={<Radio color="primary" />}
                label="Sélectionner un avatar"
                className={classes.formControl}
              />
              <Typography>ou</Typography>
              <FormControlLabel
                value="IMPORT"
                control={<Radio color="primary" />}
                label="Télécharger une photo"
                className={classes.formControl}
              />
            </>
          ) : (
            <Box className={classes.radioGroupItem}>
              <FormControlLabel
                value="AVATAR"
                control={<Radio color="primary" />}
                label="Sélectionner un avatar"
                className={classes.formControl}
              />
              <Box className={classes.separateur}>
                <Typography>ou</Typography>
              </Box>
              <FormControlLabel
                value="IMPORT"
                control={<Radio color="primary" />}
                label="Télécharger une photo"
                className={classes.formControl}
              />
            </Box>
          )}
        </RadioGroup> */}
        {show ? (
          imgType === 'AVATAR' ? (
            <ChoiceAvatarForm
              selectedAvatar={selectedAvatar}
              setSelectedAvatar={setSelectedAvatar}
              onClickAvatar={onClickAvatar}
              avatarList={avatarList}
              setAvatarList={setAvatarList}
            />
          ) : (
            <Dropzone
              contentText="Glissez et déposez ici votre photo"
              selectedFiles={selectedFiles}
              setSelectedFiles={setSelectedFiles}
              multiple={false}
              acceptFiles="image/*"
              withFileIcon={false}
              where="inUserSettings"
              withImagePreview={true}
              withImagePreviewCustomized={true}
              onClickDelete={deleteAvatar}
              fileAlreadySetUrl={croppedImgUrl || (imageUrl as any)}
              onClickResize={handleClickResize}
            />
          )
        ) : null}
      </div>

      {/* Modal crop image */}
      <CustomModal
        open={openResize}
        setOpen={setOpenResize}
        title="Redimensionnement"
        onClickConfirm={saveResizedImage}
        actionButton="Enregistrer"
        closeIcon={true}
        centerBtns={true}
      >
        {userPhotoSrc ? (
          <CustomReactEasyCrop
            src={userPhotoSrc}
            withZoom={true}
            withRotate={true}
            croppedImage={croppedImgUrl}
            setCroppedImage={setCroppedImgUrl}
            setCropedFiles={setCroppedFiles}
            savedCroppedImage={savedCroppedImage}
          />
        ) : (
          <div>Aucune Image</div>
        )}
      </CustomModal>
    </Box>
  );
};
