import {
  Box,
  Popover,
  // Table,
  TableCell,
  Typography,
} from '@material-ui/core';
import React, { FC, useState, MouseEvent, useEffect } from 'react';
import UpdateIcon from '@material-ui/icons/Update';
import { withStyles, Theme, createStyles, makeStyles } from '@material-ui/core';
import { AnyObject } from 'yup/lib/types';
import { Column, Table } from '@lib/ui-kit';
import { PrtChiffreAffaireInfoFragment } from '@lib/common/src/federation';
import { useStyles } from './styles';

const StyledTableCell = withStyles((theme: Theme) =>
  createStyles({
    head: {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  })
)(TableCell);

interface HistoriqueProps {
  historiques?: {
    data: PrtChiffreAffaireInfoFragment[];
    loading: boolean;
    error: boolean;
    rowsTotal: number;
  };
  onRequestFilter?: ({ sortable, paginate }: any) => void;
}

export const Historique: FC<HistoriqueProps> = ({ historiques, onRequestFilter }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const [sortTable, setSortTable] = useState<any>({
    column: null,
    direction: 'DESC',
  });
  const [paginate, setPaginate] = useState<any>({
    skip: 0,
    take: 12,
  });

  useEffect(() => {
    onRequestFilter && onRequestFilter({ sortTable, paginate });
  }, [sortTable, paginate]);

  const handleHistoriqueClick = (event: MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSortTable = (column: string, direction: 'ASC' | 'DESC') => {
    setSortTable({ column, direction });
  };

  const handleSearchTable = ({ skip, take }: any) => {
    setPaginate({
      skip,
      take,
    });
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const columns: Column[] = [
    {
      name: 'annee',
      label: 'Année',
      sortable: false,
      renderer: (row: PrtChiffreAffaireInfoFragment) => {
        return row.annee ? <Typography>{row.annee}</Typography> : '';
      },
    },
    {
      name: 'montant',
      label: "Chiffre d'affaire",
      sortable: false,
      style: {
        minWidth: 150,
        textAlign: 'right',
      },
      headerStyle: {
        textAlign: 'right',
      },
      renderer: (row: PrtChiffreAffaireInfoFragment) => {
        return row.montant ? <Typography>{row.montant.toFixed(2)} €</Typography> : '0 €';
      },
    },
  ];

  return (
    <Box>
      <Box onClick={handleHistoriqueClick}>
        <UpdateIcon
          style={{
            transform: 'rotateY(170deg)',
            cursor: 'pointer',
            display: 'flex',
            float: 'right',
            marginRight: '10px',
          }}
          id="historique_id"
        />
      </Box>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        PaperProps={{
          style: { padding: 15 },
        }}
      >
        <Box className={classes.root}>
          <UpdateIcon style={{ transform: 'rotateY(170deg)', fontWeight: 500, fontSize: 18, marginRight: 6 }} />
          <Typography style={{ fontSize: '14', fontWeight: 500 }}>Historique</Typography>
        </Box>
        <Table
          rowsTotal={historiques?.rowsTotal || 0}
          columns={columns}
          data={historiques?.data || []}
          loading={historiques?.loading}
          error={historiques?.error as any}
          onSortColumn={handleSortTable}
          onRunSearch={handleSearchTable}
          search={false}
          notablePagination={true}
        />
      </Popover>
    </Box>
  );
};
