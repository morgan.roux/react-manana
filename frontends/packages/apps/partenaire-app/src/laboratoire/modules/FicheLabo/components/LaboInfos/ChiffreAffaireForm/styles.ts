import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
    createStyles({
        textFieldAjout: {
            display: 'flex',
            flexDirection: 'column',
            margin: 'auto',
            padding: '10px',
        },
        customText: {
            '& .main-MuiFormLabel-root': {
              color: '#999 !important'
            },
            '& .main-MuiInputBase-input': {
              color: '#000 !important'
            },
          },
    })
)