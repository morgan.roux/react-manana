import { Box, Grid } from '@material-ui/core';
import React, { useState } from 'react';
import { FicheContactCard } from './components/FicheContactCard/FicheContactCard';
import {
  useGet_Fiche_LaboratoireLazyQuery,
  Secteur,
  useLaboratoire_Set_Prestataire_Service_As_Secteur_ContactMutation,
  useLaboratoire_Delete_Prestataire_Service_As_Secteur_ContactMutation,
  PartenaireContactUnion,
  useCreate_One_Partenaire_AccesMutation,
  useUpdate_One_Partenaire_AccesMutation,
  useDelete_One_Partenaire_AccesMutation,
  useCreate_One_Contact_CredentialMutation,
  useUpdate_One_Contact_CredentialMutation,
  useDelete_One_Contact_CredentialMutation,
  PartenaireAcces,
  PrestataireService,
  PrtContactCredential,
} from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';
import { useLaboratoireParams } from '../../hooks/useLaboratoireParams';
import { useEffect } from 'react';
import { ConfirmDeleteDialog, SmallLoading } from '@lib/ui-kit';
import ContactForm from './components/contact-form/ContactForm';
import { DataAccessForm } from './components/FicheContactCard/DataAccessForm';
import { WebsiteForm } from './components/FicheContactCard/WebsiteForm';
import { PartenaireForm } from '@lib/common';
import { SwiperSlide } from 'swiper/react';
import { useStyles } from './styles';

export const UseContactCard = () => {
  const classes = useStyles();
  const { federation, currentPharmacie, isMd, isSm } = useApplicationContext();
  const [openAccessForm, setOpenAccessForm] = React.useState<boolean>(false);
  const [accesToEdit, setAccesToEdit] = useState<PartenaireAcces>();
  const [accesToPrestataireService, setAccesToPrestataireService] = useState<PrestataireService>();

  const [openForm, setOpenForm] = useState<boolean>(false);
  const [contactToEdit, setContactToEdit] = useState<PartenaireContactUnion>();
  const [secteur, setSecteur] = useState<Secteur>();
  const { params, redirectTo } = useLaboratoireParams();

  const [openAddWebsite, setOpenAddWebsite] = useState(false);
  const [openConfirmationDeleteAccess, setOpenConfirmatioDeleteAccess] = useState<boolean>(false);
  const [openConfirmationDeleteWebsite, setOpenConfirmatioDeleteWebsite] = useState<boolean>(false);
  const [idToDeleteOrEditDataAccess, setIdToDeleteOrEditDataAccess] = useState<string>();
  const [idToDeleteWebsite, setIdToDeleteWebsite] = useState<string>();
  const [loadFicheLaboratoire, loadingFicheLaboratoire] = useGet_Fiche_LaboratoireLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  const [setPrestataireServiceAsSecteurContact, settingPrestataireServiceAsSecteurContact] =
    useLaboratoire_Set_Prestataire_Service_As_Secteur_ContactMutation({
      client: federation,
      onCompleted: () => {
        loadingFicheLaboratoire.refetch && loadingFicheLaboratoire.refetch();
      },
    });

  const [deletePrestataireServiceAsSecteurContact, deletingPrestataireServiceAsSecteurContact] =
    useLaboratoire_Delete_Prestataire_Service_As_Secteur_ContactMutation({
      client: federation,
      onCompleted: () => {
        loadingFicheLaboratoire.refetch && loadingFicheLaboratoire.refetch();
      },
    });

  const [createPartenaireAcces, creationPartenaireAcces] = useCreate_One_Partenaire_AccesMutation({
    client: federation,
    onCompleted: () => {
      setOpenAccessForm(false);
      loadingFicheLaboratoire.refetch && loadingFicheLaboratoire.refetch();
    },
  });

  const [updatePartenaireAcces, modificationPartenaireAcces] = useUpdate_One_Partenaire_AccesMutation({
    client: federation,
    onCompleted: () => {
      setOpenAccessForm(false);
    },
  });

  const [deletePartenaireAcces] = useDelete_One_Partenaire_AccesMutation({
    client: federation,
    onCompleted: () => {
      setOpenConfirmatioDeleteAccess(false);
      loadingFicheLaboratoire.refetch && loadingFicheLaboratoire.refetch();
    },
  });

  const [createContactCredential, creationContactCredential] = useCreate_One_Contact_CredentialMutation({
    client: federation,
    onCompleted: () => {
      setOpenAddWebsite(false);
      loadingFicheLaboratoire.refetch && loadingFicheLaboratoire.refetch();
    },
  });

  const [updateContactCredential, modificationContactCredential] = useUpdate_One_Contact_CredentialMutation({
    client: federation,
    onCompleted: () => {
      setOpenAddWebsite(false);
      loadingFicheLaboratoire.refetch && loadingFicheLaboratoire.refetch();
    },
  });

  const [deleteContactCredential] = useDelete_One_Contact_CredentialMutation({
    client: federation,
    onCompleted: () => {
      setOpenConfirmatioDeleteWebsite(false);
      loadingFicheLaboratoire.refetch && loadingFicheLaboratoire.refetch();
    },
  });

  const firstLaboratoire = params.idLabo?.length ? params.idLabo[0] : undefined;

  useEffect(() => {
    if (firstLaboratoire) {
      loadFicheLaboratoire({
        variables: {
          id: firstLaboratoire,
          idPharmacie: currentPharmacie.id,
        },
      });
    }
  }, [firstLaboratoire]);

  const handleAddContact = (newSecteur?: Secteur) => {
    setSecteur(newSecteur);
    setContactToEdit(undefined);
    setOpenForm(true);
  };

  const handleEditContact = (newSecteur: Secteur, contact: PartenaireContactUnion) => {
    setSecteur(newSecteur);
    setContactToEdit(contact as any);

    const isWebsiteContact = contact?.dataType === 'PRTContactCredential';

    if (isWebsiteContact) {
      setOpenAddWebsite(true);
    } else {
      setOpenForm(true);
    }
  };

  const handleVoirPlus = (secteur?: Secteur, prestataireService?: PrestataireService) => {
    redirectTo({ idSecteur: secteur?.id, idPrestataireService: prestataireService?.id });
  };

  const handleChangeSecteurContactPrestataireService = (secteur: Secteur, prestataireService: any) => {
    if (firstLaboratoire) {
      setPrestataireServiceAsSecteurContact({
        variables: {
          idLaboratoire: firstLaboratoire,
          idSecteur: secteur.id,
          idPrestataireService: prestataireService.id,
          idPharmacie: currentPharmacie.id,
        },
      });
    }
  };

  const handleDeleteSecteurContactPrestataireService = (secteur: Secteur) => {
    if (firstLaboratoire) {
      deletePrestataireServiceAsSecteurContact({
        variables: {
          idLaboratoire: firstLaboratoire,
          idPharmacie: currentPharmacie.id,
          idSecteur: secteur.id,
        },
      });
    }
  };

  const handleOpenDataAccessForm = (prestataireService: PrestataireService) => {
    setAccesToPrestataireService(prestataireService);
    setOpenAccessForm(true);
  };

  const handleSaveAcces = (acces: Partial<PartenaireAcces>) => {
    if (accesToPrestataireService?.id) {
      if (accesToEdit) {
        updatePartenaireAcces({
          variables: {
            input: {
              id: accesToEdit.id,
              update: {
                url: acces.url,
                commentaire: acces.commentaire,
                idPartenaireTypeAssocie: accesToPrestataireService?.id,
                partenaireType: 'PRESTATAIRE_SERVICE',
              },
            },
          },
        });
      } else {
        createPartenaireAcces({
          variables: {
            input: {
              partenaireAcces: {
                url: acces.url || '',
                commentaire: acces.commentaire,
                idPartenaireTypeAssocie: accesToPrestataireService?.id,
                partenaireType: 'PRESTATAIRE_SERVICE',
              },
            },
          },
        });
      }
    }
  };

  const handleSaveContactCredential = (contactCredential: Partial<PrtContactCredential>) => {
    if (firstLaboratoire && contactCredential.idSecteur) {
      if (contactToEdit) {
        updateContactCredential({
          variables: {
            input: {
              id: contactToEdit.id,
              update: {
                url: contactCredential.url,
                idSecteur: contactCredential.idSecteur,
                nomService: contactCredential.nomService,
                commentaire: contactCredential.commentaire,
                idPartenaireTypeAssocie: firstLaboratoire,
                partenaireType: 'LABORATOIRE',
              },
            },
          },
        });
      } else {
        createContactCredential({
          variables: {
            input: {
              pRTContactCredential: {
                url: contactCredential.url,
                idSecteur: contactCredential.idSecteur,
                commentaire: contactCredential.commentaire,
                nomService: contactCredential.nomService,
                idPartenaireTypeAssocie: firstLaboratoire,
                partenaireType: 'LABORATOIRE',
              },
            },
          },
        });
      }
    }
  };

  const isPrestataireServiceContact = contactToEdit?.dataType === 'PrestataireService';
  const isWebsiteContact = contactToEdit?.dataType === 'PRTContactCredential';

  const handleOpenAddWebsite = (secteur: Secteur) => {
    setSecteur(secteur);
    setContactToEdit(undefined);
    setOpenAddWebsite(true);
  };
  const handleClickDeleteDataAccess = (id: string) => {
    setIdToDeleteOrEditDataAccess(id);
    setOpenConfirmatioDeleteAccess(true);
  };
  const handleClickEditDataAccess = (acces: PartenaireAcces, prestataireService?: PrestataireService) => {
    setAccesToEdit(acces);
    setAccesToPrestataireService(prestataireService);
    setOpenAccessForm(true);
  };
  const handleClickDeleteWebsite = (id: string) => {
    setIdToDeleteWebsite(id);
    setOpenConfirmatioDeleteWebsite(true);
  };
  const handleConfirmDeleteAccess = (event: React.MouseEvent<{}>) => {
    if (idToDeleteOrEditDataAccess) {
      deletePartenaireAcces({
        variables: {
          input: {
            id: idToDeleteOrEditDataAccess,
          },
        },
      });
    }
  };
  const handleConfirmDeleteWebsite = (event: React.MouseEvent<{}>) => {
    if (idToDeleteWebsite) {
      deleteContactCredential({
        variables: {
          input: {
            id: idToDeleteWebsite,
          },
        },
      });
    }
  };
  return (
    <SwiperSlide>
      <Grid container spacing={3} className={classes.swiperSlider}>
        {loadingFicheLaboratoire.loading ? (
          <SmallLoading />
        ) : (
          (loadingFicheLaboratoire.data?.laboratoire?.secteurContacts || []).map((secteurContact) => {
            return (
              <FicheContactCard
                onChangePartenariatSelect={(ps) => {
                  handleChangeSecteurContactPrestataireService(secteurContact.secteur as any, ps);
                }}
                secteur={secteurContact.secteur as any}
                contacts={(secteurContact.contacts || []) as any}
                onClickAddContact={handleAddContact}
                onClickVoirPlus={handleVoirPlus}
                onClickEditContact={handleEditContact}
                onClickAddAcces={handleOpenDataAccessForm}
                onClickAddWebsite={handleOpenAddWebsite}
                onRequestDeletePrestataireServiceContact={handleDeleteSecteurContactPrestataireService}
                onClickDeleteDataAccess={handleClickDeleteDataAccess}
                onClickEditDataAccess={handleClickEditDataAccess}
                onClickDeleteWebsite={handleClickDeleteWebsite}
              />
            );
          })
        )}
        {firstLaboratoire && !isPrestataireServiceContact && !isWebsiteContact && (
          <ContactForm
            open={openForm}
            setOpen={setOpenForm}
            contactToEdit={contactToEdit as any}
            partenaireType="LABORATOIRE"
            idPartenaireTypeAssocie={firstLaboratoire}
            mode={contactToEdit ? 'modification' : 'creation'}
            idSecteur={secteur?.id}
            refetch={loadingFicheLaboratoire.refetch}
          />
        )}
        {firstLaboratoire && isPrestataireServiceContact && (
          <PartenaireForm
            partenaireAssocieToEdit={contactToEdit as any}
            open={openForm}
            setOpen={setOpenForm}
            partenaireType="PRESTATAIRE_SERVICE"
            mode="modification"
          />
        )}

        <DataAccessForm
          loading={creationPartenaireAcces.loading || modificationPartenaireAcces.loading}
          open={openAccessForm}
          accesToEdit={accesToEdit}
          mode={accesToEdit ? 'modification' : 'creation'}
          setOpen={setOpenAccessForm}
          onRequestSave={handleSaveAcces}
        />
        <WebsiteForm
          idSecteur={secteur?.id || ''}
          open={openAddWebsite}
          setOpen={setOpenAddWebsite}
          contactToEdit={isWebsiteContact ? (contactToEdit as any) : undefined}
          mode={contactToEdit && isWebsiteContact ? 'modification' : 'creation'}
          loading={creationContactCredential.loading || modificationContactCredential.loading}
          onRequestSave={handleSaveContactCredential}
        />
        {/* <FicheContactCard /> */}
        <ConfirmDeleteDialog
          open={openConfirmationDeleteAccess}
          setOpen={setOpenConfirmatioDeleteAccess}
          onClickConfirm={handleConfirmDeleteAccess}
          title="Suppression accès aux données"
        />
        <ConfirmDeleteDialog
          open={openConfirmationDeleteWebsite}
          setOpen={setOpenConfirmatioDeleteWebsite}
          onClickConfirm={handleConfirmDeleteWebsite}
          title="Supression site web"
        />
      </Grid>
    </SwiperSlide>
  );
};
