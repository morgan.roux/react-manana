import React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Card, CardHeader, CardContent, CardActions, IconButton } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { FC } from 'react';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 345,
    },
  })
);
interface SimpleCardProps {
  title?: string;
  action?: any;
  empty?: any;
  content?: React.ReactNode;
  voirPlus?: boolean;
  onClickVoirPlus?: (event: React.MouseEvent<{}>) => void;
}
export const SimpleCard: FC<SimpleCardProps> = ({ title, action, content, voirPlus, onClickVoirPlus }) => {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card className={classes.root}>
      <CardHeader
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={title}
      />
      <CardContent>{content} </CardContent>
      <CardActions disableSpacing>
        {voirPlus && (
          <IconButton onClick={onClickVoirPlus} aria-label="voir-plus">
            <ExpandMoreIcon />
          </IconButton>
        )}
      </CardActions>
    </Card>
  );
};
