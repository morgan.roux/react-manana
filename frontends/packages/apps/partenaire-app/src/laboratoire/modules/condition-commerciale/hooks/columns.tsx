import React from 'react';
import moment from 'moment';
import { Box, Typography } from '@material-ui/core';
import { MoreHoriz } from '@material-ui/icons';
import { useStyles } from './styles';
import { Column } from '@lib/ui-kit';
import { ConditionCommandeItem } from '../pages/condition-commerciale/ConditionCommerciale';

export const useColumns = (
  // listStatusConditionCommande: ConditionStatus[],
  // handleChangeStatus: (id: string, value: string) => void,
  handleClickAction: (event: React.MouseEvent<HTMLSpanElement, MouseEvent>, itemChoosen: ConditionCommandeItem) => void,
  handleRowDetail: (event: React.MouseEvent<HTMLSpanElement, MouseEvent>, itemChoosen: ConditionCommandeItem) => void,
  isFacture?: boolean
) => {
  const styles = useStyles();
  const columns: Column[] = [
    {
      name: 'idType',
      label: 'Type',
      sortable: true,
      renderer: (row: ConditionCommandeItem) => {
        return row?.type?.libelle ? (
          <Box onClick={(e) => handleRowDetail(e, row)}>
            <Typography className={styles.tableText}>{row.type.libelle}</Typography>
          </Box>
        ) : (
          ''
        );
      },
    },
    {
      name: 'titre',
      label: 'Titre',
      sortable: true,
      renderer: (row: ConditionCommandeItem) => {
        return (
          <Box onClick={(e) => handleRowDetail(e, row)}>
            <Typography className={styles.tableText}>{row.libelle}</Typography>
          </Box>
        );
      },
    },
    {
      name: 'labo',
      label: 'Labo',
      renderer: (row: ConditionCommandeItem) => {
        return (
          <Box onClick={(e) => handleRowDetail(e, row)}>
            <Typography className={styles.tableText}>{row.typeAssocie?.nom}</Typography>
          </Box>
        );
      },
    },
    {
      name: 'periode',
      label: 'Période',
      sortable: true,
      renderer: (row: ConditionCommandeItem) => {
        return (
          <Box onClick={(e) => handleRowDetail(e, row)}>
            <Typography className={styles.tableText}>
              Du {moment.utc(row.periode.debut).format('DD/MM/YYYY')} - Au{' '}
              {moment.utc(row.periode.fin).format('DD/MM/YYYY')}
            </Typography>
          </Box>
        );
      },
    },
    {
      name: 'statut',
      label: 'Statut',
      renderer: (row: ConditionCommandeItem) => {
        return (
          <Box onClick={(e) => handleRowDetail(e, row)}>
            <Typography className={styles.tableText}>{row?.status?.libelle || '-'}</Typography>
          </Box>
        );
        // ) : (
        //   <SelectComp
        //     id={row.id || ''}
        //     value={row.status.id}
        //     statusList={listStatusConditionCommande}
        //     onChange={handleChangeStatus}
        //   />
        // );
      },
    },
    {
      name: 'action',
      label: '',
      renderer: (row: ConditionCommandeItem) => {
        return isFacture ? (
          ' '
        ) : (
          <span onClick={(e) => handleClickAction(e, row)}>
            <MoreHoriz />
          </span>
        );
      },
    },
  ];
  return columns;
};
