// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import React, { FC, useEffect, useState } from 'react';
import { CustomFormTextField, CustomModal, CustomEditorText, CustomDatePicker } from '@lib/ui-kit';
import { Box, FormControl, InputLabel, MenuItem, Select } from '@material-ui/core';
import moment from 'moment';
import { CanalCommandeType, TypeConditionCommande } from '../condition-commerciale/ConditionCommerciale';
import { Dropzone, PartenaireInput, useApplicationContext } from '@lib/common';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import { LaboratoireInfoFragment } from '@lib/common/src/federation';
// import { useStyles } from './styles';

export interface FormConditionCommercialeInput {
  id?: string;
  idType?: string;
  // idCanal?: string;
  libelle?: string;
  details?: string;
  fichiers?: any[];
  periode?: {
    debut?: any;
    fin?: any;
  };
  idPartenaireTypeAssocie?: string;
}

export interface FormConditionCommercialeProps {
  value?: FormConditionCommercialeInput;
  open: boolean;
  setOpen: (open: boolean) => void;
  onSave: (data: FormConditionCommercialeOutput, mode?: 'ajout' | 'modification') => void;
  saving?: boolean;
  mode: 'ajout' | 'modification';
  canalCommandes: CanalCommandeType[];
  typesConditionCommande: TypeConditionCommande[];
  partenaireAssocie?: LaboratoireInfoFragment;
}

export interface FormConditionCommercialeOutput {
  id?: string;
  idLaboratoire?: string;
  idType: string;
  // idCanal: string;
  details?: string;
  periode: {
    debut: any;
    fin: any;
  };
  fichiers: any[] | null;
  libelle: string;
  idPartenaireTypeAssocie?: string;
}

const ConditionCommercialeValue: FormConditionCommercialeInput = {
  periode: {
    debut: moment(new Date()).startOf('year').toDate(),
    fin: moment(new Date()).endOf('year').toDate(),
  },
};

export const FormConditionCommerciale: FC<FormConditionCommercialeProps> = ({
  open,
  setOpen,
  mode,
  saving,
  value,
  typesConditionCommande,
  onSave,
  partenaireAssocie,
}) => {
  const { isMobile } = useApplicationContext();

  const { params } = useLaboratoireParams('condition');
  const { idLabo } = params;

  const [input, setInput] = useState<FormConditionCommercialeInput | undefined>(ConditionCommercialeValue);
  const [errorDateDebut, setErrorDateDebut] = useState<boolean>(false);
  const [errorDateFin, setErrorDateFin] = useState<boolean>(false);
  const [fichiersJoints, setFichiersJoints] = useState<any[]>([]);
  const [partenaire, setPartenaire] = useState<any>();

  // const styles = useStyles();

  useEffect(() => {
    setInput(value);
    if (value?.fichiers) {
      setFichiersJoints(value.fichiers);
    } else {
      setFichiersJoints([]);
    }
    setPartenaire(value?.idPartenaireTypeAssocie);
  }, [value]);

  useEffect(() => {
    if ((open && mode === 'ajout') || (!open && mode === 'modification')) {
      setInput(ConditionCommercialeValue);
      setFichiersJoints([]);
      if (partenaireAssocie) setPartenaire(partenaireAssocie);
    }
    if (!open) {
      setPartenaire(undefined);
    }
  }, [open]);

  useEffect(() => {
    if (!partenaireAssocie) {
      setPartenaire(undefined);
    }
  }, [partenaireAssocie]);

  const handleChange = (event: any) => {
    if (event.target.name) {
      setInput({ ...input, [event.target.name]: event.target.value });
    }
  };

  const handleChangeSelect = (
    event: React.ChangeEvent<{
      name?: string | undefined;
      value: unknown;
    }>,
    __child: React.ReactNode
  ) => {
    if (event.target.name) {
      setInput({ ...input, [event.target.name]: event.target.value });
    }
  };

  const handleDateDebutChange = (date: Date | null, __value: string | undefined | null) => {
    if (date) {
      setInput({ ...input, periode: { ...input?.periode, debut: date } });
      if (input && input.periode && input.periode.fin && date > input.periode.fin) {
        setErrorDateDebut(true);
      } else {
        setErrorDateDebut(false);
      }
    }
  };

  const handleDateFinChange = (date: Date | null, __value: string | undefined | null) => {
    if (date) {
      setInput({ ...input, periode: { ...input?.periode, fin: date } });
      if (input && input.periode && input.periode.debut && date < input.periode.debut) {
        setErrorDateFin(true);
      } else {
        setErrorDateFin(false);
      }
    }
  };

  const handleChangeDetails = (content: string) => {
    setInput({ ...input, details: content });
  };

  const handleChangeLaboratoire = (laboratoire: any) => {
    setPartenaire(laboratoire);
  };

  const handleSave = () => {
    if (
      input &&
      input.idType &&
      input.libelle &&
      // input.periode &&
      // input.periode.debut &&
      // input.periode.fin &&
      !errorDateDebut &&
      !errorDateFin
    ) {
      onSave(
        {
          id: input.id,
          libelle: input.libelle,
          idType: input.idType,
          periode: {
            debut: input.periode?.debut || moment(new Date()).startOf('year').toDate(),
            fin: input.periode?.fin || moment(new Date()).endOf('year').toDate(),
          },
          fichiers: fichiersJoints,
          details: input.details,
          idPartenaireTypeAssocie: partenaire.id,
        },
        mode
      );
      setInput(undefined);
    }
  };

  const isValid = () => {
    return (
      input &&
      input.idType &&
      input.libelle &&
      // input.periode &&
      // input.periode.debut &&
      // input.periode.fin &&
      !errorDateDebut &&
      !errorDateFin &&
      partenaire
    );
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={mode === 'ajout' ? 'Nouvelle condition commerciale' : "Modification d'une condition commerciale"}
      withBtnsActions
      closeIcon
      headerWithBgColor
      fullScreen={!!isMobile}
      disabledButton={!isValid() || saving}
      onClickConfirm={handleSave}
      maxWidth="md"
    >
      <Box width={1} maxWidth={500} paddingTop={1} paddingBottom={3}>
        <Box>
          <PartenaireInput
            key="laboratoire"
            index="laboratoire"
            label="Laboratoire"
            value={partenaire}
            onChange={handleChangeLaboratoire}
            defaultValue={partenaireAssocie}
            //disabled={partenaireAssocie ? true : false}
          />
        </Box>
        <FormControl style={{ width: '100%', margin: '0 0 8px 0', marginTop: '16px' }} variant="outlined">
          <InputLabel id="type">Type *</InputLabel>
          <Select
            labelId="type"
            id="TypeId"
            value={input?.idType || '0'}
            onChange={handleChangeSelect}
            label="Type *"
            name="idType"
            required
            style={{ height: 48 }}
          >
            <MenuItem value="0">Sélectionner le type</MenuItem>
            {(typesConditionCommande || []).map((item) => (
              <MenuItem key={item.id} value={item.id}>
                {item.libelle}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <h4 style={{ margin: '16px 0 16px 0' }}>Période</h4>
        <Box display="flex" flexDirection="row" py={1}>
          <Box mr={2}>
            <CustomDatePicker
              style={{ width: '100%' }}
              value={input?.periode?.debut || moment(new Date()).startOf('year').toDate()}
              onChange={handleDateDebutChange}
              required
              label="Date de début"
            />
          </Box>
          <Box ml={2}>
            <CustomDatePicker
              style={{ width: '100%' }}
              value={input?.periode?.fin || moment(new Date()).endOf('year').toDate()}
              onChange={handleDateFinChange}
              required
              label="Date de fin"
            />
          </Box>
        </Box>
        <Box pt={2}>
          <CustomFormTextField
            style={{ marginBottom: 16 }}
            value={input?.libelle}
            name="libelle"
            label="Titre"
            onChange={handleChange}
            required
            placeholder="Titre condition commerciale"
          />
        </Box>
        <CustomEditorText
          style={{ marginBottom: 16 }}
          value={input?.details || ''}
          placeholder="Description détaillée"
          onChange={handleChangeDetails}
        />
        <Dropzone
          contentText="Glissez et déposez vos documents"
          selectedFiles={fichiersJoints}
          setSelectedFiles={setFichiersJoints}
          multiple
        />
        {/* <FormControl style={{ width: '100%' }} variant="outlined">
          <InputLabel id="type">Canal de commandes *</InputLabel>
          <Select
            labelId="type"
            id="TypeId"
            value={input?.idCanal || '0'}
            onChange={handleChangeSelect}
            label="Canal de commandes *"
            name="idCanal"
            required
            style={{ height: 56 }}
          >
            <MenuItem value="0">Canal de commande</MenuItem>
            {(canalCommandes || []).map((item) => (
              <MenuItem key={item.id} value={item.id}>
                {item.libelle}
              </MenuItem>
            ))}
          </Select>
        </FormControl> */}
      </Box>
    </CustomModal>
  );
};
