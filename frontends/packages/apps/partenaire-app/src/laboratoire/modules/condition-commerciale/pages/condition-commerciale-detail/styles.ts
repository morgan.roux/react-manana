import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      padding: 24,
      paddingTop: 0,
      backgroundColor: theme.palette.common.white,
    },
    line: {
      [theme.breakpoints.down('xs')]: {
        display: 'block',
      },
      display: 'flex',
      flexDirection: 'row',
      backgroundColor: '#f8f8f8',
      marginTop: 24,
      padding: 16,
    },
    label: {
      color: 'gray',
      marginRight: 8,
    },
    value: {
      fontWeight: 'bold',
    },
    oneLine: {
      marginRight: 24,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    descriptionBox: {
      paddingTop: 16,
      paddingBottom: 8,
    },
    subtitle: {
      fontFamily: 'Roboto',
      fontSize: 18,
      fontWeight: 'bold',
      color: '#27272f',
      paddingBottom: 8,
    },
    text: {
      fontFamily: 'Roboto',
      fontSize: 16,
      fontWeight: 'normal',
      color: '#27272f',
    },
    likeBox: {
      paddingTop: 16,
      marginTop: 24,
      paddingBottom: 24,
      borderTop: 'solid #eeeeee 1px',
    },
    likeShow: {
      display: 'flex',
      flexDirection: 'row',
      paddingBottom: 16,
    },
    likeNumber: {
      fontFamily: 'Roboto',
      fontSize: 16,
      fontWeight: 'normal',
      color: '#27272f',
      paddingLeft: 16,
    },
    likeButton: {
      fontFamily: 'Roboto',
      fontSize: 16,
      fontWeight: 'normal',
      color: '#27272f',
      textTransform: 'none',
    },
    commentBox: {
      width: '100%',
      marginTop: 24,
      paddingTop: 24,
      borderTop: `2px solid ${theme.palette.grey[300]}`,
    },
    textCommentaire: {
      fontFamily: 'Roboto',
      fontSize: 16,
      fontWeight: 'normal',
      color: '#27272f',
      textAlign: 'center',
      paddingTop: 16,
    },
  })
);
