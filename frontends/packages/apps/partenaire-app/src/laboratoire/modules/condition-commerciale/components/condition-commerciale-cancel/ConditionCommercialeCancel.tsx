import { Box } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { CustomModal, CustomEditorText } from '@lib/ui-kit';
export interface ConditionCommercialeCancelProps {
  open: boolean;
  setOpen: () => void;
  saving?: boolean;
  onRequestCancel?: (comment: string) => void;
}

export const ConditionCommercialeCancel: FC<ConditionCommercialeCancelProps> = ({
  open,
  setOpen,
  saving,
  onRequestCancel,
}) => {
  const [detail, setDetail] = useState<string>('');

  const handleSave = (e: React.FormEvent<HTMLInputElement>) => {
    e.preventDefault();
    e.stopPropagation();
    onRequestCancel && onRequestCancel(detail);
  };

  const isValid = () => detail !== '';

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Annuler"
      withBtnsActions
      closeIcon
      headerWithBgColor
      disabledButton={!isValid() || saving}
      onClickConfirm={handleSave}
      withCancelButton={false}
      actionButtonTitle="Annuler"
      maxWidth="md"
    >
      <Box>
        <CustomEditorText
          style={{ marginBottom: 16 }}
          value={detail}
          placeholder="Détail annulation"
          onChange={(value) => setDetail(value)}
        />
      </Box>
    </CustomModal>
  );
};
