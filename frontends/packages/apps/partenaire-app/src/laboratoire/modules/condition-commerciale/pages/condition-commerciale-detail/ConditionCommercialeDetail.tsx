// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import { CommentList, /* UserAction */ Comment, useApplicationContext } from '@lib/common';
import { useGet_Condition_CommercialeLazyQuery } from '@lib/common/src/federation';
import { useCommentsQuery, useSmyleysQuery, useUser_SmyleysLazyQuery } from '@lib/common/src/graphql';
import { ErrorPage, Loader, NewCustomButton } from '@lib/ui-kit';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import { Box, Typography } from '@material-ui/core';
import { Cancel } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, useEffect } from 'react';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import { useConditionCommerciale } from '../../hooks/useConditionCommerciale';
import { useStyles } from './styles';
import { AttachedFiles } from '@lib/common';

export interface ConditionCommercialeDetailProps {
  saving?: boolean;
  saved?: boolean;
  setSaved?: (newSaved: boolean) => void;

  onRequestCloture?: (idCondition?: string) => void;
  onRequestRestaure?: {
    request?: (idCondition?: string) => void;
    saving?: boolean;
    saved?: boolean;
    setSaved?: (newDaved: boolean) => void;
  };

  // onRequestLike?: (idCondition?: string) => void;
  // onRequestComment?: (idCondition?: string, comment?: string) => void;
}

const ConditionCommercialeDetail: FC<{}> = ({}) => {
  const classes = useStyles();
  // const [commentaire, setCommentaire] = useState<string>('');

  const { redirectTo, params, goBack } = useLaboratoireParams('condition');
  const { graphql, notify, currentGroupement: groupement } = useApplicationContext();

  const [
    onRequestSave,
    onRequestSearch,
    onRequestDelete,
    handleChangeStatus,
    handleFetchMoreConditions,
    { type: typesConditionCommande, status: listStatusConditionCommande, canal: canalCommandes },
    total,
    loadingConditions,
    setSaved,
    saving,
    saved,
    onCancel,
    onRequestCloture,
    onRequestDetail,
    onRequestRestaure,
  ] = useConditionCommerciale();

  useEffect(() => {
    if (saved) {
      if (setSaved) setSaved(false);
      handleGoBack();
    }
  }, [saved]);

  const [loadConditionCommerciale, loadingConditionCommerciale] = useGet_Condition_CommercialeLazyQuery();

  useEffect(() => {
    if (params.id) {
      loadConditionCommerciale({
        variables: { id: params.id },
      });
    }
  }, [params.id]);

  const handleGoBack = () => {
    goBack('condition');
  };

  const item = loadingConditionCommerciale.data?.pRTConditionCommerciale;
  /*const [loadUserSmyleys, loadingUserSmyles] = useUser_SmyleysLazyQuery({ client: graphql });

  const getSmyleys = useSmyleysQuery({
    client: graphql,
    variables: {
      idGroupement: groupement.id,
    },

    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });*/

  const conditionCommerciale = {
    id: item?.id,
    canal: item?.canal,
    type: item?.type,
    status: item?.statut,
    libelle: item?.titre,
    fichiers: item?.fichiers || null,
    details: item?.description || '',
    periode: {
      debut: item?.dateDebut,
      fin: item?.dateFin,
    },
  };

  const loadingCommentaire = useCommentsQuery({
    variables: {
      codeItem: 'CONDITION_COMMERCIALE',
      idItemAssocie: conditionCommerciale.id || '',
    },
    client: graphql,
  });

  /*const handleRefecthSmyles = () => () => {
    loadingUserSmyles.refetch && loadingUserSmyles.refetch();
  };*/

  const handleFecthMoreComments = () => {};

  const handleRefecthComments = () => {
    loadingCommentaire.refetch && loadingCommentaire.refetch();
  };

  return (
    <CustomContainer
      filled
      bannerContentStyle={{
        justifyContent: 'center',
      }}
      bannerBack
      onBackClick={handleGoBack}
      bannerTitle="Détail condition commerciale"
      contentStyle={{
        marginTop: 24,
        width: '100%',
      }}
      onlyBackAndTitle
    >
      <Box className={classes.container}>
        <Box>
          <NewCustomButton
            startIcon={<Cancel />}
            disabled={saving || onRequestRestaure?.saving}
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              conditionCommerciale?.status?.code !== 'CLOTURER' && onRequestCloture(conditionCommerciale?.id);
              conditionCommerciale?.status?.code === 'CLOTURER' &&
                onRequestRestaure?.request &&
                onRequestRestaure.request(conditionCommerciale?.id);
            }}
          >
            {conditionCommerciale?.status?.code === 'CLOTURER' ? 'RESTAURER' : 'CLÔTURER'}
          </NewCustomButton>
        </Box>
        <Box className={classes.line}>
          <Box className={classes.oneLine}>
            <span className={classes.label}>Type: </span>
            <span className={classes.value}>{conditionCommerciale?.type?.libelle}</span>
          </Box>
          <Box className={classes.oneLine}>
            <span className={classes.label}>Titre: </span>
            <span className={classes.value}>{conditionCommerciale?.libelle}</span>
          </Box>
          <Box className={classes.oneLine}>
            <span className={classes.label}>Période: </span>
            <span className={classes.value}>
              Du {moment.utc(conditionCommerciale?.periode?.debut).format('DD/MM/YYYY')} - Au{' '}
              {moment.utc(conditionCommerciale?.periode?.fin).format('DD/MM/YYYY')}
            </span>
          </Box>
          <Box className={classes.oneLine}>
            <span className={classes.label}>Statut: </span>
            <span className={classes.value}>{conditionCommerciale?.status?.libelle}</span>
          </Box>
        </Box>
        {conditionCommerciale?.details && (
          <Box className={classes.descriptionBox}>
            <Typography className={classes.subtitle}>Description</Typography>
            <span dangerouslySetInnerHTML={{ __html: conditionCommerciale?.details }} className={classes.text} />
          </Box>
        )}
        {(conditionCommerciale.fichiers?.length || 0) > 0 ? (
          <AttachedFiles files={conditionCommerciale.fichiers as any} />
        ) : null}

        <Box className={classes.commentBox}>
          {
            <>
              {/* <UserAction
                refetchSmyleys={handleRefecthSmyles}
                codeItem="CONDITION_COMMERCIALE"
                idSource={conditionCommerciale.id || ''}
                // nbComment={conditionCommercialeToShow.nombreCommentaires}
                // nbSmyley={conditionCommercialeToShow.nombreReaction}
                userSmyleys={loadingUserSmyles.data?.userSmyleys as any}
                smyleys={getSmyleys && getSmyleys.data && getSmyleys.data.smyleys}
                hidePartage
                hideRecommandation
              /> */}
              {loadingCommentaire.loading ? (
                <Loader />
              ) : loadingCommentaire.error ? (
                <ErrorPage />
              ) : (
                <CommentList
                  key={`comment_list`}
                  comments={loadingCommentaire.data?.comments?.data || []}
                  fetchMoreComments={handleFecthMoreComments}
                />
              )}
              <Comment
                refetch={handleRefecthComments}
                codeItem="CONDITION_COMMERCIALE"
                idSource={conditionCommerciale?.id || ''}
              />
            </>
          }
        </Box>

        {/* <Box className={classes.likeBox}>
        <Box className={classes.likeShow}>
          <ThumbUp color="secondary" />
          <Typography className={classes.likeNumber}>{conditionCommerciale?.like || 0}</Typography>
        </Box>
        <Box>
          <CustomButton
            startIcon={<ThumbUp />}
            variant="outlined"
            className={classes.likeButton}
            onClick={(e) => {
              e.preventDefault();
              e.stopPropagation();
              onRequestLike && onRequestLike(conditionCommerciale?.id);
            }}
          >
            j'aime
          </CustomButton>
        </Box>
      </Box>

      <Box>
        <FormControl style={{ width: '100%' }}>
          <Box className={classes.commentBox}>
            <CustomFormTextField
              style={{ marginBottom: 16 }}
              value={commentaire}
              name="commentaire"
              onChange={(event) => {
                setCommentaire(event.target.value);
              }}
              multiline
              fullWidth
              rows={4}
            />
            <IconButton
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
                onRequestComment && onRequestComment(conditionCommerciale?.id, commentaire);
              }}
              disabled={!commentaire}
            >
              <PlayArrow fontSize="large" />
            </IconButton>
          </Box>
        </FormControl>
        <Typography className={classes.textCommentaire}> 0 commentaires</Typography>
      </Box> */}
      </Box>
    </CustomContainer>
  );
};

export default ConditionCommercialeDetail;
