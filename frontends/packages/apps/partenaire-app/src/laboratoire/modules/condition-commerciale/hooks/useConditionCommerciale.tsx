import { useState } from 'react';
import { useApplicationContext, useUploadFiles } from '@lib/common';
import moment from 'moment';
import {
  CanalInfoFragment,
  ConditionTypeInfoFragment,
  LaboratoireInfoFragment,
  PrtConditionCommercialeFilter,
  SortDirection,
  StatusConditionInfoFragment,
  useChange_Status_ConditionMutation,
  useCreate_ConditionMutation,
  useDelete_ConditionMutation,
  useGet_CanalsQuery,
  useGet_ConditionsLazyQuery,
  useGet_LaboratoireLazyQuery,
  useGet_Status_ConditionQuery,
  useGet_Types_ConditionQuery,
  useUpdate_ConditionMutation,
} from '@lib/common/src/federation';
import { ConditionCommandeItem } from '../pages/condition-commerciale/ConditionCommerciale';
import { FichierInput } from '@lib/common/src/graphql';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import { useEffect } from 'react';

export interface TypeCanalStatusCondition {
  type?: ConditionTypeInfoFragment[];
  status?: StatusConditionInfoFragment[];
  canal?: CanalInfoFragment[];
}

export const useConditionCommerciale = (): [
  (data: any) => void,
  (search: { skip?: string; take?: string; searchText?: string; filters: any[]; sorting?: any }) => void,
  (id: string) => void,
  (id: string, value: string) => void,
  () => void,
  TypeCanalStatusCondition,
  number,
  any,
  (newValue: boolean) => any,
  boolean,
  boolean,
  {
    request: (condition: ConditionCommandeItem, comment: string) => void;
    saving?: boolean;
    saved?: boolean;
    setSaved?: (newSaved: boolean) => void;
  },
  (idCondition?: string) => void,
  (item?: ConditionCommandeItem) => void,
  {
    request: (idCondition?: string) => void;
    saving?: boolean;
    saved?: boolean;
    setSaved?: (newSaved: boolean) => void;
  },
  LaboratoireInfoFragment
] => {
  const { currentPharmacie: pharmacie, notify, graphql } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();
  const { params } = useLaboratoireParams('condition');
  const { idLabo, idStatut, idType } = params;
  const idLaboExist = idLabo ? idLabo[0] : undefined;
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [savingConditionCancel, setSavingConditionCancel] = useState<boolean>(false);
  const [savedConditionCloture, setSavedConditionCloture] = useState<boolean>(false);
  const [savingRestaureCondition, setSavingRestaureCondition] = useState<boolean>(false);
  const [savedRestaureCondition, setSavedRestaureCondition] = useState<boolean>(false);
  const [savedConditionCancel, setSavedConditionCancel] = useState<boolean>(false);
  const [conditionCommercialeToShow, setConditionCommercialeToShow] = useState<any>();

  const [loadLabo, loadingLabo] = useGet_LaboratoireLazyQuery();

  const [changeStatus] = useChange_Status_ConditionMutation({
    onCompleted: () => {
      notify({
        message: 'Le changement de statut a été effectué avec succès.',
        type: 'success',
      });
      loadingConditions.refetch && loadingConditions.refetch();
      // loadingRowConditions.refetch();
      setSavedConditionCloture(true);
      setSaving(false);
      setSaved(true);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant le changement de statut.',
        type: 'error',
      });
    },
  });

  const loadCanal = useGet_CanalsQuery();

  const loadType = useGet_Types_ConditionQuery();
  const loadStatus = useGet_Status_ConditionQuery();
  const [loadCondtions, loadingConditions] = useGet_ConditionsLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [createCondtion] = useCreate_ConditionMutation({
    onCompleted: () => {
      notify({
        message: 'Cette condition commerciale a été ajoutée avec succès.',
        type: 'success',
      });
      loadingConditions.refetch && loadingConditions.refetch();
      setSaving(false);
      setSaved(true);
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout de cette condition commerciale.",
        type: 'error',
      });
    },
  });

  const [deleteCondition] = useDelete_ConditionMutation({
    onCompleted: () => {
      notify({
        message: 'Cette condition commerciale a été suprimée avec succès.',
        type: 'success',
      });
      loadingConditions.refetch && loadingConditions.refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la supression de cette condition commerciale.',
        type: 'error',
      });
    },
  });

  const [updateCondition, updatingCondition] = useUpdate_ConditionMutation({
    onCompleted: () => {
      notify({
        message: 'Cette condition commerciale a été modifiée avec succès.',
        type: 'success',
      });
      setSaving(false);
      setSaved(true);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification de cette condition commerciale.',
        type: 'error',
      });
    },
  });

  const [updateConditionStatut] = useUpdate_ConditionMutation({
    onCompleted: () => {
      notify({
        message: 'Cette condition commerciale a été annulée avec succès.',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification de statut de la condition commerciale.',
        type: 'error',
      });
    },
  });

  useEffect(() => {
    if (idLaboExist) {
      loadLabo({
        variables: {
          id: idLaboExist,
          idPharmacie: pharmacie.id,
        },
      });
    }
  }, [idLaboExist]);

  const handleFetchMoreConditions = () => {
    const fetchMore = loadingConditions.fetchMore;
    const variables = loadingConditions.variables;
    const paging = variables?.paging;
    if (loadingConditions.data?.pRTConditionCommerciales.nodes) {
      fetchMore &&
        fetchMore({
          variables: {
            ...variables,
            paging: {
              ...paging,
              offset: loadingConditions.data?.pRTConditionCommerciales.nodes,
            },
          },

          updateQuery: (prev: any, { fetchMoreResult }) => {
            if (prev?.pRTConditionCommerciales && fetchMoreResult?.pRTConditionCommerciales.nodes) {
              return {
                ...prev,
                pRTConditionCommerciales: {
                  ...prev.pRTConditionCommerciales,
                  nodes: [...prev.pRTConditionCommerciales.nodes, ...fetchMoreResult.pRTConditionCommerciales.nodes],
                },
              };
            }

            return prev;
          },
        });
    }
  };

  const handleRequestSearch = ({ skip, take, searchText, sorting }: any) => {
    const filterAnd: PrtConditionCommercialeFilter[] = [
      {
        partenaireType: {
          eq: 'LABORATOIRE',
        },
      },
      {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
      {
        or: searchText
          ? [
              {
                titre: {
                  iLike: `%${searchText}%`,
                },
              },
              {
                description: {
                  iLike: `%${searchText}%`,
                },
              },
            ]
          : undefined,
      },
    ];

    if (idLabo?.length)
      filterAnd.push({
        idPartenaireTypeAssocie: {
          in: idLabo,
        },
      });

    if (idStatut?.length) filterAnd.push({ idStatut: { in: idStatut } });

    if (idType?.length) filterAnd.push({ idType: { in: idType } });

    const sortTableFilter = sorting || [
      {
        field: 'dateDebut',
        direction: SortDirection.Desc,
      },
    ];

    loadCondtions({
      variables: {
        filter: {
          and: filterAnd,
        },
        paging: {
          offset: skip ? parseInt(skip) : 0,
          limit: take ? parseInt(take) : 12,
        },
        sorting: sortTableFilter,
      },
    });
  };

  const handleOnRequestSave = (data: any) => {
    setSaving(false);
    setSaved(true);
    if (data.fichiers && data.fichiers.length > 0) {
      uploadFiles(data.fichiers, {
        directory: 'relation-fournisseurs/conditions-commerciales',
      })
        .then((uploadedFiles) => {
          saveCondition(data, uploadedFiles);
        })
        .catch((error) => {
          setSaving(false);
          setSaved(false);
          notify({
            type: 'error',
            message: `Une erreur s'est produite lors de chargement des fichiers`,
          });
        });
    } else {
      saveCondition(data, undefined);
    }
  };

  const saveCondition = (data: any, fichiers: FichierInput[] | undefined) => {
    if (data.id) {
      updateCondition({
        variables: {
          id: data.id,
          input: {
            idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
            partenaireType: 'LABORATOIRE',
            titre: data.libelle,
            description: data.details,
            dateDebut: moment(data.periode.debut).startOf('day').utc(true).format(),
            dateFin: moment(data.periode.fin).startOf('day').utc(true).format(),
            fichiers,
            // idCanal: data.idCanal,
            idType: data.idType,
          },
        },
      });
    } else {
      createCondtion({
        variables: {
          input: {
            idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
            partenaireType: 'LABORATOIRE',
            titre: data.libelle,
            description: data.details,
            dateDebut: moment(data.periode.debut).startOf('day').utc(true).format(),
            dateFin: moment(data.periode.fin).startOf('day').utc(true).format(),
            fichiers,
            // idCanal: data.idCanal,
            idType: data.idType,
          },
        },
      });
    }
  };

  const handleDelete = (id: string) => {
    deleteCondition({ variables: { id } });
  };

  const handleChangeStatus = (id: string, value: string) => {
    changeStatus({
      variables: {
        id,
        idStatut: value,
      },
    });
  };

  const handleCancelCondition = (condition: ConditionCommandeItem, comment: string) => {
    setSavedConditionCancel(false);
    setSavingConditionCancel(true);
    const annulerStatut = loadStatus.data?.pRTConditionCommercialeStatuts?.nodes.filter(
      (statut) => statut.code === 'ANNULER'
    );
    if (annulerStatut?.length && condition.id && idLabo) {
      updateConditionStatut({
        variables: {
          id: condition.id,
          input: {
            idPartenaireTypeAssocie: condition.idPartenaireTypeAssocie as any,
            partenaireType: 'LABORATOIRE',
            titre: condition.libelle,
            description: comment,
            dateDebut: condition.periode.debut,
            dateFin: condition.periode.fin,
            fichiers: condition.fichiers,
            // idCanal: condition.canal.id,
            idType: condition.type.id,
            idStatut: annulerStatut.length ? annulerStatut[0].id : '',
          },
        },
      })
        .then(() => {
          setSavedConditionCancel(true);
        })
        .finally(() => {
          setSavingConditionCancel(false);
        });
    } else {
      setSavingConditionCancel(false);
    }
  };

  const handleClotureCondition = (idCondition?: string) => {
    setSavedConditionCloture(false);
    const cloturerStatut = loadStatus.data?.pRTConditionCommercialeStatuts?.nodes.filter(
      (statut) => statut.code === 'CLOTURER'
    );
    if (cloturerStatut?.length) {
      changeStatus({
        variables: {
          id: idCondition || '',
          idStatut: cloturerStatut?.length ? cloturerStatut[0].id : '',
        },
      });
    }
  };

  const handleRestaureCondition = (idCondition?: string) => {
    setSavedRestaureCondition(false);
    const restaureStatut = loadStatus.data?.pRTConditionCommercialeStatuts?.nodes.filter(
      (statut) => statut.code === 'EN_COURS'
    );
    if (restaureStatut?.length) {
      changeStatus({
        variables: {
          id: idCondition || '',
          idStatut: restaureStatut?.length ? restaureStatut[0].id : '',
        },
      });
    }
  };

  return [
    handleOnRequestSave,
    handleRequestSearch,
    handleDelete,
    handleChangeStatus,
    handleFetchMoreConditions,
    {
      type: loadType.data?.pRTConditionCommercialeTypes.nodes,
      status: loadStatus.data?.pRTConditionCommercialeStatuts.nodes,
      canal: loadCanal.data?.canals.nodes,
    },
    loadingConditions.data?.pRTConditionCommerciales.totalCount || 0,
    loadingConditions,
    setSaved,
    saving,
    saved,
    {
      request: handleCancelCondition,
      saving: savingConditionCancel,
      saved: savedConditionCancel,
      setSaved: setSavingConditionCancel,
    },
    handleClotureCondition,
    (item) => {
      setConditionCommercialeToShow(item);
    },
    {
      request: handleRestaureCondition,
      // saving: false,
      // saved: savedRestaureCondition,
      // setSaved: setSavedRestaureCondition,
    },
    loadingLabo.data?.laboratoire as any,
  ];
};
