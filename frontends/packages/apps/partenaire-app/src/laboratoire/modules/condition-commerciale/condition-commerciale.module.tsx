import React from 'react';
import loadable from '@loadable/component';

import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';

const ConditionCommercialePage = loadable(() => import('./pages/condition-commerciale/ConditionCommerciale'), {
  fallback: <SmallLoading />,
});

const DetailsConditionCommerciale = loadable(
  () => import('./pages/condition-commerciale-detail/ConditionCommercialeDetail'),
  {
    fallback: <SmallLoading />,
  }
);

const conditionCommercialeModule: ModuleDefinition = {
  routes: [
   
    {
      attachTo: 'MAIN',
      path: '/laboratoires/condition',
      component: ConditionCommercialePage,
      activationParameters: {
        groupement: '0305',
        pharmacie: '0708',
      },
    },
  ],
};

export default conditionCommercialeModule;
