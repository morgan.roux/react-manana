import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    dateContainer: {
      display: 'flex',
      flexDirection: 'row',
      width: '100%',
      justifyContent: 'space-between',
      flexWrap: 'wrap',
    },
    dateField: {
      width: '48.3%',
      margin: '0 0 16px 0',
      minWidth: 200,
      [theme.breakpoints.down('sm')]: {
        width: '100%',
      },
    },
  })
);
