// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable prettier/prettier */
import { useApplicationContext, ConsultFichierModal } from '@lib/common';
import { PrtConditionInfoFragment, SortDirection } from '@lib/common/src/federation';
import { ConfirmDeleteDialog, CustomAvatar, ErrorPage, NewCustomButton, Table, TableChange, TopBar } from '@lib/ui-kit';
import { Box, Fab, Grid, IconButton, MenuItem, Select, Typography } from '@material-ui/core';
import { Add, Event, MoreHoriz } from '@material-ui/icons';
import classNames from 'classnames';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { BottomScrollListener } from 'react-bottom-scroll-listener';
import { useHistory } from 'react-router';
import { useLaboratoireParams } from '../../../../../laboratoire/hooks/useLaboratoireParams';
import { ConditionCommercialeCancel } from '../../components/condition-commerciale-cancel';
import { MenuEl } from '../../components/condition-commerciale/MenuEl';
import { useColumns } from '../../hooks/columns';
import { useConditionCommerciale } from '../../hooks/useConditionCommerciale';
import {
  FormConditionCommerciale,
  FormConditionCommercialeInput,
  FormConditionCommercialeOutput,
} from '../condition-commerciale-form/ConditionCommercialeForm';
import { useStyles } from './styles';

export interface CanalCommandeType {
  id: string;
  libelle: string;
  code: string;
}

export interface ConditionStatus {
  id: string;
  libelle: string;
  code: string;
}

export interface TypeConditionCommande {
  id: string;
  code: string;
  libelle: string;
}

interface Fichier {
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string;
}

export interface ConditionCommandeItem {
  id?: string;
  type: TypeConditionCommande;
  canal: CanalCommandeType;
  status: ConditionStatus;
  libelle: string;
  details?: string;
  fichiers?: Fichier[];
  periode: {
    debut: Date;
    fin: Date;
  };
  like?: number;
  idPartenaireTypeAssocie?: string;
  typeAssocie?: { id: string; nom: string };
}

interface SelectCompProps {
  id: string;
  value: string;
  statusList: ConditionStatus[];
  onChange: (id: string, value: string) => void;
}

export const SelectComp: FC<SelectCompProps> = ({ value, id, onChange, statusList }) => {
  const [idCurrent, setIdCurent] = useState<string | undefined>();

  useEffect(() => {
    setIdCurent(id);
  }, [id]);

  const handleChange = (
    event: React.ChangeEvent<{
      name?: string | undefined;
      value: unknown;
    }>,
    __child: React.ReactNode
  ) => {
    event.stopPropagation();
    if (idCurrent) onChange(idCurrent, event.target.value as string);
  };

  return (
    <>
      <Select style={{ minWidth: 20 }} variant="standard" value={value} onChange={handleChange} displayEmpty>
        {statusList.map(({ id, libelle }) => (
          <MenuItem key={id} value={id}>
            {libelle}
          </MenuItem>
        ))}
      </Select>
    </>
  );
};

interface ListCompProps {
  item: any;
  onClick: (event: React.MouseEvent<HTMLSpanElement, MouseEvent>, item: ConditionCommandeItem) => void;
}

const ListComp: FC<ListCompProps> = ({ item, onClick }) => {
  const styles = useStyles();

  const handleClick = (event: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
    onClick(event, item);
  };

  return (
    <div className={styles.conditionItemContainer}>
      <div className={classNames(styles.conditionItemTop, styles.conditionContent)}>
        <span>
          <h3>{item.libelle}</h3>
        </span>
        <IconButton onClick={handleClick}>
          <MoreHoriz />
        </IconButton>
      </div>
      <div className={classNames(styles.conditionItemBottom, styles.conditionContent)}>
        <Box>
          <Box>
            <Typography className={styles.typoFlex} style={{ color: '#6d6d6d' }}>
              <Event style={{ marginRight: 10 }} />{' '}
              {` ${moment(item.periode.debut).format('DD/MM/YYY')} - ${moment(item.periode.fin).format('DD/MM/YYYY')} `}
            </Typography>
          </Box>
          <Box display="flex" alignItems="center">
            <CustomAvatar
              name={item.typeAssocie?.nom || '-'}
              url={item.typeAssocie?.photo?.publicUrl as any}
              style={{ width: 30, height: 30, marginRight: 5 }}
            />
            <Typography style={{ fontSize: 13 }}>{item.typeAssocie?.nom}</Typography>
          </Box>
        </Box>
        <span>{item.status.libelle}</span>
      </div>
    </div>
  );
};

export interface ConditionProps {
  filterIcon?: any;
  onSideNavListClick?: (item: string) => void;
  activeItem?: string;

  isFacture?: boolean;
  onRequestSelected?: (conditions: ConditionCommandeItem[]) => void;
  selectedItemsIds?: string[];
  onRequestDetail?: (item?: ConditionCommandeItem) => void;
  onRequestGoBack?: () => void;
  onRestaurer?: {
    request: (idCondition?: string) => void;
    saving?: boolean;
    saved?: boolean;
    setSaved?: (newDaved: boolean) => void;
  };
}

const Condition: FC<ConditionProps> = ({ isFacture, onRequestSelected, selectedItemsIds }) => {
  const styles = useStyles();

  const { isMobile } = useApplicationContext();

  const { params, redirectTo } = useLaboratoireParams('condition');
  const { idLabo, idStatut, idType, skip, take, searchText, sorting } = params;

  const [anchorEL, setAnchorEl] = useState<HTMLElement | null>(null);
  const [item, setItem] = useState<ConditionCommandeItem | undefined>();
  const [modeModal, setModeModal] = useState<'ajout' | 'modification'>('ajout');
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [filter, setFilter] = useState<any[]>([]);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [openCancelDialog, setOpenCancelDialog] = useState<boolean>(false);

  const [conditionSelected, setConditionSelected] = useState<ConditionCommandeItem[]>([]);
  const [showSelected, setShowSelected] = useState<boolean>(false);
  const [showFichier, setShowFichier] = useState<boolean>(false);

  const handleOpenModal = () => {
    setOpenModal((prev) => !prev);
  };

  const toggleOpenCancelDialog = () => {
    setOpenCancelDialog((prev) => !prev);
  };

  const [
    onRequestSave,
    onRequestSearch,
    onRequestDelete,
    handleChangeStatus,
    handleFetchMoreConditions,
    { type: typesConditionCommande, status: listStatusConditionCommande, canal: canalCommandes },
    total,
    loadingConditions,
    setSaved,
    saving,
    saved,
    onCancel,
    handleClotureCondition,
    onRequestDetail,
    onRequestRestaure,
    laboratoire,
  ] = useConditionCommerciale();

  const data: ConditionCommandeItem[] = (loadingConditions.data?.pRTConditionCommerciales.nodes || []).map(
    (item: PrtConditionInfoFragment) => ({
      id: item.id,
      canal: item.canal,
      type: item.type,
      status: item.statut,
      libelle: item.titre,
      fichiers: item.fichiers || null,
      details: item.description || '',
      periode: {
        debut: item.dateDebut,
        fin: item.dateFin,
      },
      idPartenaireTypeAssocie: item.idPartenaireTypeAssocie,
      typeAssocie: item.typeAssocie,
    })
  );

  useEffect(() => {
    if (saved) {
      setOpenModal(false);
      if (setSaved) setSaved(false);
    }
  }, [saved]);

  useEffect(() => {
    onRequestSearch({ skip, take, searchText, filters: filter, sorting });
  }, [
    skip,
    take,
    searchText,
    JSON.stringify(idLabo),
    JSON.stringify(idStatut),
    JSON.stringify(idType),
    sorting && sorting[0] && sorting[0].field,
    sorting && sorting[0] && sorting[0].direction,
  ]);

  useEffect(() => {
    if (selectedItemsIds?.length && (data || []).length > 0) {
      const selectedRows = (data || []).filter((item) => selectedItemsIds.includes(item.id || ''));
      setConditionSelected(selectedRows);
    }
  }, [selectedItemsIds, data]);

  useEffect(() => {
    if (onCancel?.saved) {
      setOpenCancelDialog(false);
      if (onCancel?.setSaved) onCancel.setSaved(false);
    }
  }, [onCancel?.saved]);

  const onShowSelected = () => {
    setShowSelected(!showSelected);
  };

  const onCloseMenu = () => {
    setAnchorEl(null);
  };

  const handleConfirmDeleteOne = (): void => {
    if (item && item.id) {
      onRequestDelete && onRequestDelete(item.id);
      setOpenDeleteDialog(false);
    }
  };

  const handleClickAction = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>,
    itemChoosen: ConditionCommandeItem
  ) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget); // open Menu
    setItem(itemChoosen);
  };

  const handleDeleteClick = () => {
    setAnchorEl(null); // close Menu
    setOpenDeleteDialog(true);
  };

  const handleEditClick = () => {
    setModeModal('modification');
    handleOpenModal();
    setAnchorEl(null); // close Menu
  };

  const handleSearchTable = ({ skip, take, searchText, sortDirection, sortBy }: TableChange) => {
    redirectTo({
      ...params,
      skip: skip ? skip.toString() : undefined,
      take: take ? take.toString() : undefined,
      searchText,
      sorting: sortDirection && sortBy ? [{ field: sortBy, direction: sortDirection }] : undefined,
    });
  };

  const handleAddClick = () => {
    handleOpenModal();
    setItem(undefined);
    setModeModal('ajout');
  };

  const handleSaveData = (data: FormConditionCommercialeOutput) => {
    if (onRequestSave) {
      onRequestSave({ ...data });
    }
  };

  const handleChangeFilter = (filter: any) => {
    setFilter(filter);
  };

  const handleDetail = () => {
    if (item?.id) {
      //push(`/laboratoires/condition/${item?.id}`);
      redirectTo({ id: item.id, backParams: params }, 'condition-detail');
      setAnchorEl(null);
    }
    // close Menu
    // onRequestDetail && onRequestDetail(item);
  };

  const handleRowDetail = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>,
    itemChoosen: ConditionCommandeItem
  ) => {
    handleDetail();
    event.stopPropagation();
    // setAnchorEl(event.currentTarget); // open Menu
    setItem(itemChoosen);
  };

  const handleCancel = () => {
    setAnchorEl(null); // close Menu
    setOpenCancelDialog(true);
  };

  const handleConfirmCancelOne = (comment: string) => {
    if (item) {
      onCancel && onCancel.request(item, comment);
    }
  };

  const columns = useColumns(handleClickAction, handleRowDetail, isFacture);

  const handleClearSelection = () => {
    setConditionSelected([]);
  };

  const handleSelectAll = () => {
    setConditionSelected(data || []);
  };

  const handleSelectChange = (dataSelected: any) => {
    setConditionSelected(dataSelected);
  };

  const handleSortTable = (column: any, direction: SortDirection) => {
    redirectTo({ ...params, sorting: [{ field: column, direction }] });
  };

  const handleFichierVueClick = () => {
    setShowFichier(true);
  };

  return (
    <>
      {isMobile ? (
        <div className={styles.rootMobile}>
          <BottomScrollListener onBottom={handleFetchMoreConditions}>
            {(scrollRef) => (
              <Grid
                id="infinity-condition-commerciale"
                item
                ref={scrollRef as any}
                className={styles.infiniteScrollContainer}
              >
                {(data || []).map((item) => {
                  console.log('---------------condition commerciale---------------', item);
                  return <ListComp key={item.id} item={item} onClick={handleClickAction} />;
                })}
              </Grid>
            )}
          </BottomScrollListener>
          <Fab className={styles.fab} onClick={handleAddClick} variant="round" color="secondary">
            <Add />
          </Fab>
          {loadingConditions.error && <ErrorPage />}
        </div>
      ) : (
        <div className={styles.rootWeb}>
          <div className={styles.tableContainer}>
            <Table
              loading={loadingConditions.loading || false}
              columns={columns}
              error={loadingConditions.error}
              data={data || []}
              topBarComponent={(searchComponent: any) => (
                <TopBar
                  handleOpenAdd={handleAddClick}
                  searchComponent={searchComponent}
                  titleButton="AJOUTER UNE CONDITION COMMERCIALE"
                  // titleTopBar="Liste des conditions commerciales"
                  withFilter
                  noAddBtn={isFacture}
                />
              )}
              selectable={isFacture}
              noTableToolbarSearch={isFacture}
              onClearSelection={isFacture ? handleClearSelection : undefined}
              onSelectAll={isFacture ? handleSelectAll : undefined}
              rowsSelected={isFacture ? conditionSelected : undefined}
              onRowsSelectionChange={isFacture ? handleSelectChange : undefined}
              onShowSelected={onShowSelected}
              showSelected={showSelected}
              search={false}
              rowsTotal={total}
              onRunSearch={handleSearchTable}
              onSortColumn={handleSortTable}
            />
            {isFacture && (
              <Box display="flex" justifyContent="flex-end">
                <NewCustomButton
                  onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
                    event.preventDefault();
                    event.stopPropagation();
                    onRequestSelected && onRequestSelected(conditionSelected);
                  }}
                  className={styles.joindreButton}
                  disabled={!conditionSelected?.length}
                >
                  Joindre
                </NewCustomButton>
              </Box>
            )}
          </div>
        </div>
      )}
      <ConsultFichierModal
        open={showFichier}
        setOpen={setShowFichier}
        title={`Fichier ci-joint (${item?.libelle})`}
        files={item?.fichiers || []}
      />
      <MenuEl
        anchorEl={anchorEL}
        onClose={onCloseMenu}
        onDeleteClick={handleDeleteClick}
        onEditClick={handleEditClick}
        onDetailClick={handleDetail}
        onCancelClick={handleCancel}
        onFichierVueClick={handleFichierVueClick}
      />
      <FormConditionCommerciale
        partenaireAssocie={laboratoire}
        mode={modeModal}
        open={openModal}
        setOpen={handleOpenModal}
        canalCommandes={canalCommandes || []}
        typesConditionCommande={typesConditionCommande || []}
        onSave={handleSaveData}
        saving={saving}
        value={
          item
            ? ({
                id: item.id,
                idType: item.type.id,
                libelle: item.libelle,
                details: item.details,
                periode: item.periode,
                fichiers: item.fichiers,
                idPartenaireTypeAssocie: item.idPartenaireTypeAssocie,
              } as FormConditionCommercialeInput)
            : undefined
        }
      />
      <ConditionCommercialeCancel
        open={openCancelDialog}
        setOpen={toggleOpenCancelDialog}
        saving={onCancel?.saving}
        onRequestCancel={handleConfirmCancelOne}
      />
      <ConfirmDeleteDialog
        title="Suppression"
        content="Etes-vous sûr de vouloir supprimer cette condition commerciale ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDeleteOne}
      />
    </>
  );
};

export default Condition;
