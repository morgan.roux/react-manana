import { Menu, MenuItem, Typography } from '@material-ui/core';
import { Delete, Description, Edit, Visibility } from '@material-ui/icons';
import React, { FC } from 'react';

interface MenuElprops {
  onEditClick: () => void;
  onDeleteClick: () => void;
  anchorEl: HTMLElement | null;
  onClose: () => void;
  onDetailClick?: () => void;
  onSaisirClick?: () => void;
  onCancelClick?: () => void;
  onFichierVueClick?: () => void;
}

export const MenuEl: FC<MenuElprops> = ({
  anchorEl,
  onClose,
  onDeleteClick,
  onEditClick,
  onDetailClick,
  onFichierVueClick,
}) => {
  return (
    <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={onClose}>
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          onFichierVueClick && onFichierVueClick();
        }}
      >
        <Description />
        &nbsp;
        <Typography variant="inherit">Consulter le fichier ci-joint</Typography>
      </MenuItem>
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          onDetailClick && onDetailClick();
        }}
      >
        <Visibility />
        &nbsp;
        <Typography variant="inherit">Voir détail</Typography>
      </MenuItem>
      {/* <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          onSaisirClick && onSaisirClick();
        }}
      >
        <AttachMoney />
        &nbsp;
        <Typography variant="inherit">Saisir rémunération</Typography>
      </MenuItem> */}
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          onEditClick();
        }}
      >
        <Edit />
        &nbsp;
        <Typography variant="inherit">Modifier</Typography>
      </MenuItem>
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          onDeleteClick();
        }}
      >
        <Delete />
        &nbsp;
        <Typography variant="inherit">Suprimer</Typography>
      </MenuItem>
      {/* <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          onCancelClick && onCancelClick();
        }}
      >
        <Cancel />
        &nbsp;
        <Typography variant="inherit">Annuler</Typography>
      </MenuItem> */}
    </Menu>
  );
};
