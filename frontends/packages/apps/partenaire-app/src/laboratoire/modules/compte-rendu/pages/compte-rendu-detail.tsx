import React, { FC } from 'react';
import { Box } from '@material-ui/core';
import { useCompteRenduClient } from './utils/useCompteRenduClient';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import { useGet_LaboratoireLazyQuery, usePrt_Compte_RenduQuery } from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';
import { useEffect } from 'react';
import { SmallLoading } from '@lib/ui-kit';
import { CompteRenduDetail } from './CompteRenduDetail/CompteRenduDetail';
import { usePharmacieLazyQuery } from '@lib/common/src/graphql';

const CompteRenduComp: FC<{}> = ({}): JSX.Element => {
  const { federation, currentPharmacie, graphql } = useApplicationContext();
  const { params } = useLaboratoireParams('compte-rendu');
  const { loading: loadingCompteRendu, data } = usePrt_Compte_RenduQuery({
    client: federation,
    variables: {
      id: params.id || '',
      idPharmacie: currentPharmacie.id,
    },
  });

  const { id: idCompteRendu, idLabo } = params;
  const { onCompteRenduDetail, rendezVous, handleSendMail, isSendingEmail } = useCompteRenduClient(
    idCompteRendu || null
  );
  const [loadLabo, { data: infoLabo, loading: loadingLabo }] = useGet_LaboratoireLazyQuery({
    client: federation,
  });
  const [loadPharmacie, loadingPharmacie] = usePharmacieLazyQuery({ client: graphql });

  useEffect(() => {
    if (data?.pRTCompteRendu && onCompteRenduDetail) {
      if (idLabo) {
        loadLabo({
          variables: {
            id: idLabo[0],
            idPharmacie: currentPharmacie.id,
          },
        });
        loadPharmacie({ variables: { id: currentPharmacie.id } });
      }
    }
  }, [loadingCompteRendu, idLabo && idLabo[0]]);

  const laboratoireInformation = {
    nom: infoLabo?.laboratoire?.nom,
    adresse: infoLabo?.laboratoire?.laboratoireSuite?.adresse,
    email: infoLabo?.laboratoire?.laboratoireSuite?.email,
    telephone: infoLabo?.laboratoire?.laboratoireSuite?.telephone,
    representantLabo: {
      nom:
        data?.pRTCompteRendu?.responsables &&
        data.pRTCompteRendu.responsables.length > 0 &&
        data.pRTCompteRendu.responsables[0].nom,
      telephone:
        data?.pRTCompteRendu?.responsables &&
        data.pRTCompteRendu.responsables.length > 0 &&
        (data.pRTCompteRendu.responsables[0]?.contact?.telPerso ||
          data?.pRTCompteRendu.responsables[0]?.contact?.telProf ||
          ''),
    },
  } as any;

  const pharmacieInformation =
    loadingPharmacie.data && loadingPharmacie.data.pharmacie
      ? {
          nom: loadingPharmacie.data?.pharmacie?.nom || '',
          adresse: loadingPharmacie.data?.pharmacie?.adresse1 || '',
          telephone:
            loadingPharmacie.data?.pharmacie?.contact?.telMobPerso ||
            loadingPharmacie.data?.pharmacie?.contact?.telMobProf ||
            '',
          email:
            loadingPharmacie.data?.pharmacie?.contact?.mailPerso ||
            loadingPharmacie.data?.pharmacie?.contact?.mailProf ||
            '',
        }
      : undefined;

  return loadingCompteRendu ? (
    <SmallLoading />
  ) : (
    <CompteRenduDetail
      compteRenduValue={data?.pRTCompteRendu as any}
      laboratoireInformation={laboratoireInformation}
      pharmaciInformation={pharmacieInformation}
      dateProchainRendezVous={rendezVous?.rendezVous?.dateRendezVous}
      onRequestSendMail={handleSendMail}
      sendingEmail={isSendingEmail}
      loading={loadingLabo && loadingPharmacie.loading && loadingCompteRendu}
    />
  );
};

export default CompteRenduComp;
