import React, { useCallback, useEffect, useState } from 'react';
import { contentEmailCR } from './file/contentEmailCR';
import {
  FichierInput,
  PrtConditionInfoFragment,
  PrtPlanMarketingInfoFragment,
  PrtRendezVousSortFields,
  SortDirection,
  useCreate_ConditionMutation,
  useCreate_Plan_MarketingMutation,
  useCreate_Prt_Compte_RenduMutation,
  useCreate_Rendez_VousMutation,
  useDelete_One_Prt_Compte_RenduMutation,
  useGet_LaboratoireLazyQuery,
  useGet_List_PartenariatsLazyQuery,
  useGet_List_Plan_Marketing_TypesLazyQuery,
  useGet_Rendez_VousLazyQuery,
  useGet_Types_ConditionLazyQuery,
  useNotification_LogsMutation,
  usePrt_Compte_RendusLazyQuery,
  usePrt_Condition_Commerciale_Non_RattacheLazyQuery,
  usePrt_Plan_Marketing_Non_RattacheLazyQuery,
  useSend_EmailMutation,
  useUpdate_Prt_Compte_RenduMutation,
  useUpdate_Prt_Compte_Rendu_Notification_LogsMutation,
  useUpdate_Rendez_VousMutation,
  usePrt_Compte_RenduLazyQuery,
  useDelete_ConditionMutation,
  useUpdate_ConditionMutation,
  useDelete_One_Plan_MarketingMutation,
  useUpdate_Plan_MarketingMutation,
  useSend_Email_Compte_RenduLazyQuery,
} from '@lib/common/src/federation';
import { startTime, useApplicationContext, useUploadFiles } from '@lib/common';
import {
  Search_Plan_Marketing_Produit_CanalsQueryVariables,
  usePharmacieLazyQuery,
  useSearch_Plan_Marketing_Produit_CanalsLazyQuery,
} from '@lib/common/src/graphql';
import { useHistory } from 'react-router';
import UserInput from '@lib/common/src/components/UserInput';
import useStyles from './styles';
import { CompteRendu, OnCompteRenduDetailProps, SendEmailInput } from '../interface';
import moment from 'moment';
import { CompteRenduFormOuptput } from '../../components/interface';
import { CompteRenduDetail } from '../CompteRenduDetail/CompteRenduDetail';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import { TableChange } from '@lib/ui-kit';

export const useCompteRenduClient = (idCompteRendu: string | null) => {
  const {
    user,
    currentPharmacie: pharmacie,
    currentGroupement: groupement,
    auth,
    federation,
    graphql,
    notify,
  } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const { params, redirectTo } = useLaboratoireParams('compte-rendu');
  const history = useHistory();
  const { view, idLabo, id, take, skip, searchText, sorting } = params;

  const classes = useStyles();
  const [success, setSuccess] = useState<boolean>(false);
  const [successRdv, setSuccessRdv] = useState<boolean>(false);
  const [openCollaborateurRdv, setOpenCollaborateurRdv] = useState<boolean>(false);
  const [collaborateursRdv, setCollaborateursRdv] = useState<any[]>([]);
  const [responsableRdv, setResponsablesRdv] = useState<any[]>([]);
  const [isSendingEmail, setSendingEmail] = useState<boolean>(false);
  const [compteRenduToEmail, setCompteRenduToEmail] = useState<any>();
  const [selectSuiviOperation, setSelectSuiviOperation] = useState<any[]>();

  const [detail, setDetail] = useState<OnCompteRenduDetailProps>();
  const [useCataloguePharmacie, setUseCataloguePharmacie] = useState<boolean>(true);
  const [saved, setSaved] = useState<boolean>(false);

  const [loadCompteRendu, loadingCompteRendu] = usePrt_Compte_RendusLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [deleteCompteRendu, deletingCompteRendu] = useDelete_One_Prt_Compte_RenduMutation({
    onCompleted: () => {
      setSuccess(true);
      notify({
        message: 'Le compte rendu a été supprimé avec succès!',
        type: 'success',
      });
      loadingCompteRendu?.refetch && loadingCompteRendu.refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression du compte rendu',
        type: 'error',
      });
    },
    client: federation,
  });

  const [updateCompteRendu, updatingCompteRendu] = useUpdate_Prt_Compte_RenduMutation({
    onCompleted: () => {
      setSuccess(true);
      notify({
        message: 'Le compte rendu a été modifié avec succès!',
        type: 'success',
      });
      loadingCompteRendu?.refetch && loadingCompteRendu.refetch();
      setSaved(true);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du compte rendu',
        type: 'error',
      });
    },
    client: federation,
  });

  const [createCompteRendu, creatingCompteRendu] = useCreate_Prt_Compte_RenduMutation({
    onCompleted: () => {
      setSuccess(true);
      notify({
        message: 'Le compte rendu a été ajouté avec succès!',
        type: 'success',
      });
      loadingCompteRendu?.refetch && loadingCompteRendu.refetch();
      setSaved(true);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la création du compte rendu',
        type: 'error',
      });
    },
    client: federation,
  });

  const [loadPlanMarketingTypes, loadingPlanMarketingTypes] = useGet_List_Plan_Marketing_TypesLazyQuery({
    client: federation,
    variables: {
      idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
      filter: { idGroupement: { eq: groupement.id } },
    },
  });

  const handleRequestDelete = (id: string) => {
    deleteCompteRendu({
      variables: { id },
    });
  };

  const [createRdv, creatingRdv] = useCreate_Rendez_VousMutation({
    client: federation,
    onCompleted: () => {
      setSuccessRdv(true);
      loadingRendezVous?.refetch && loadingRendezVous.refetch();
      notify({
        message: 'Rendez-vous planifié.',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout du rendez-vous",
        type: 'error',
      });
    },
  });

  const [updateRdv, updatingRdv] = useUpdate_Rendez_VousMutation({
    onCompleted: () => {
      setSuccessRdv(true);
      notify({
        message: 'Le rendez-vous a été reporté avec succès !',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du rendez-vous',
        type: 'error',
      });
    },
    client: federation,
  });

  const [loadConditionTypes, loadingConditionTypes] = useGet_Types_ConditionLazyQuery({ client: federation });

  const [loadRendezVous, loadingRendezVous] = useGet_Rendez_VousLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [loadCompteRenduToEdit, loadingCompteRenduToEdit] = usePrt_Compte_RenduLazyQuery({ client: federation });

  useEffect(() => {
    setSuccess(false);
  }, [loadingCompteRendu.data?.pRTCompteRendus]);

  useEffect(() => {
    if (idCompteRendu) {
      loadCompteRenduToEdit({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          id: idCompteRendu,
        },
      });
    }
  }, [idCompteRendu]);

  const [loadConditionCommerciale, loadingConditionCommerciale] = usePrt_Condition_Commerciale_Non_RattacheLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [loadPlanMarketingProduits, loadingPlanMarketingProduits] = useSearch_Plan_Marketing_Produit_CanalsLazyQuery({
    client: graphql,
  });

  const [createCondtion, creatingCondition] = useCreate_ConditionMutation({
    client: federation,
    onCompleted: () => {
      loadingConditionCommerciale?.refetch && loadingConditionCommerciale.refetch();
      notify({
        message: 'Cette condition commerciale a été ajoutée avec succès.',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout de cette condition commerciale.",
        type: 'error',
      });
    },
  });

  const [updateCondition, updatingCondition] = useUpdate_ConditionMutation({
    onCompleted: () => {
      loadingConditionCommerciale?.refetch && loadingConditionCommerciale.refetch();
      notify({
        message: 'Cette condition commerciale a été modifiée avec succès.',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification de cette condition commerciale.',
        type: 'error',
      });
    },
  });

  const [deleteCondition] = useDelete_ConditionMutation({
    onCompleted: () => {
      notify({
        message: 'Cette condition commerciale a été suprimée avec succès.',
        type: 'success',
      });
      loadingConditionCommerciale?.refetch && loadingConditionCommerciale.refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la supression de cette condition commerciale.',
        type: 'error',
      });
    },
  });

  const [createPlan, creatingPlan] = useCreate_Plan_MarketingMutation({
    onCompleted: () => {
      loadingPlanMarketing?.refetch && loadingPlanMarketing.refetch();
      loadingRemiseArriere?.refetch && loadingRemiseArriere.refetch();
      notify({
        message: ' Le plan Trade a été crée avec succés !',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: 'Erreurs lors de la création du plan Trade ',
        type: 'error',
      });
    },
    client: federation,
  });

  const [updatePlanMarketing] = useUpdate_Plan_MarketingMutation({
    onCompleted: () => {
      notify({
        message: 'Le plan Marketing a été modifié avec succès !',
        type: 'success',
      });
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du plan Marketing',
        type: 'error',
      });
    },
  });

  const [deletePlanMarketing] = useDelete_One_Plan_MarketingMutation({
    onCompleted: () => {
      notify({
        message: 'Le plan Marketing a été supprimé avec succès !',
        type: 'success',
      });
      loadingPlanMarketing.refetch && loadingPlanMarketing.refetch();
      loadingRemiseArriere?.refetch && loadingRemiseArriere.refetch();
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression du plan Marketing',
        type: 'error',
      });
    },
  });

  const [loadPlanMarketing, loadingPlanMarketing] = usePrt_Plan_Marketing_Non_RattacheLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [loadRemiseArriere, loadingRemiseArriere] = usePrt_Plan_Marketing_Non_RattacheLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const [createNotificationLogs, creatingNotificationLogs] = useNotification_LogsMutation({
    client: federation,
    onCompleted: (data) => {
      if (idLabo?.length) {
        updateCompteRenduNotification({
          variables: {
            input: {
              idNotificationLogs: data.createOneNotificationLogs.id,
              compteRendu: {
                id: compteRenduToEmail.id,
                idPartenaireTypeAssocie: idLabo[0],
                idPharmacie: compteRenduToEmail.idPharmacie,
              },
            },
          },
        });
      }
      setCompteRenduToEmail(undefined);
    },
  });

  const [updateCompteRenduNotification, updatingCompteRenduNotification] =
    useUpdate_Prt_Compte_Rendu_Notification_LogsMutation({
      client: federation,
    });

  const [loadPartenariat, loadingPartenariat] = useGet_List_PartenariatsLazyQuery({ client: federation });

  const [loadProduits, loadingProduits] = useSearch_Plan_Marketing_Produit_CanalsLazyQuery({ client: graphql });

  const [loadPharmacie, loadingPharmacie] = usePharmacieLazyQuery({ client: graphql });

  const [loadLabo, loadingLabo] = useGet_LaboratoireLazyQuery({
    client: federation,
  });

  const [sendEmail, sendingEmail] = useSend_Email_Compte_RenduLazyQuery({
    client: federation,
    onCompleted: () => {
      notify({
        type: sendingEmail.data?.sendEmailCompteRendu.statut as any,
        message: sendingEmail.data?.sendEmailCompteRendu.message,
      });
    },
    onError: () => {
      notify({
        type: 'error',
        message: sendingEmail.data?.sendEmailCompteRendu.message,
      });
    },
  });

  useEffect(() => {
    if (sendingEmail.loading) {
      notify({
        type: 'success',
        message: `En cours d'envoi d'email`,
      });
    }
  }, [sendingEmail]);

  const saveCondition = (data: PrtConditionInfoFragment, fichiers: FichierInput[] | undefined) => {
    if (!data.id) {
      createCondtion({
        variables: {
          input: {
            idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
            partenaireType: 'LABORATOIRE',
            titre: data.titre,
            description: data.description,
            dateDebut: moment(data.dateDebut).startOf('day').utc(true).format(),
            dateFin: moment(data.dateFin).startOf('day').utc(true).format(),
            fichiers,
            // idCanal: data.idCanal,
            idType: data.idType,
          },
        },
      });
    } else {
      updateCondition({
        variables: {
          id: data.id,
          input: {
            idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
            partenaireType: 'LABORATOIRE',
            titre: data.titre,
            description: data.description,
            dateDebut: moment(data.dateDebut).startOf('day').utc(true).format(),
            dateFin: moment(data.dateFin).startOf('day').utc(true).format(),
            fichiers,
            // idCanal: data.idCanal,
            idType: data.idType,
          },
        },
      });
    }
  };

  const handleSaveCondition = async (data: any) => {
    if (data.fichiers && data.fichiers.length > 0) {
      uploadFiles(data.fichiers, {
        directory: 'relation-fournisseurs/compte-rendu',
      })
        .then((uploadedFiles) => {
          saveCondition(data, uploadedFiles);
        })
        .catch((error) => {
          notify({
            type: 'error',
            message: `Une erreur s'est produite lors de chargement des fichiers`,
          });
        });
    } else {
      saveCondition(data, undefined);
    }
  };

  const handleDeleteCondition = (condition: PrtConditionInfoFragment) => {
    deleteCondition({ variables: { id: condition.id } });
  };

  const savePlan = (planMarketing: any, fichiers: any) => {
    if (!planMarketing.id) {
      createPlan({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          input: {
            titre: planMarketing.titre,
            description: planMarketing.description,
            dateDebut: moment(planMarketing.dateDebut).startOf('day').utc(true).format(),
            dateFin: moment(planMarketing.dateFin).startOf('day').utc(true).format(),
            partenaireType: 'LABORATOIRE',
            idType: planMarketing.idType,
            fichiers,
            //idStatut: '',
            idMiseAvants: planMarketing?.miseAvants,
            produits: planMarketing?.produits,
            idPartenaireTypeAssocie: planMarketing?.idPartenaireTypeAssocie,
            typeRemuneration: planMarketing?.typeRemuneration,
            //idProduits: planMarketing?.idProduit || null,
            montant: planMarketing?.montant,
            montantStatutRealise: planMarketing?.montantStatutRealise,
            remise: planMarketing?.remise,
            estRemiseArriere: planMarketing?.estRemiseArriere,
          } as any,
        },
      });
    } else {
      updatePlanMarketing({
        variables: {
          id: planMarketing.id,
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          input: {
            titre: planMarketing.titre,
            description: planMarketing.description,
            dateDebut: moment(planMarketing.dateDebut).startOf('day').utc(true).format(),
            dateFin: moment(planMarketing.dateFin).startOf('day').utc(true).format(),
            partenaireType: 'LABORATOIRE',
            idType: planMarketing.idType,
            fichiers,
            //idStatut: '',
            idMiseAvants: planMarketing?.miseAvants,
            produits: planMarketing?.produits,
            idPartenaireTypeAssocie: planMarketing?.idPartenaireTypeAssocie,
            typeRemuneration: planMarketing?.typeRemuneration,
            //idProduits: planMarketing?.idProduit || null,
            montant: planMarketing?.montant,
            montantStatutRealise: planMarketing?.montantStatutRealise,
            remise: planMarketing?.remise,
            estRemiseArriere: planMarketing?.estRemiseArriere,
          },
        },
      });
    }
  };

  const handleSavePlan = (data: PrtPlanMarketingInfoFragment) => {
    if (data.fichiers && data.fichiers.length > 0) {
      uploadFiles(data.fichiers as any, {
        directory: 'relation-fournisseurs/compte-rendu',
      })
        .then((uploadedFiles) => {
          savePlan(data, uploadedFiles);
        })
        .catch((error) => {
          notify({
            type: 'error',
            message: `Une erreur s'est produite lors de chargement des fichiers`,
          });
        });
    } else {
      savePlan(data, undefined);
    }
  };

  const handleDeleteTrade = (trade: PrtPlanMarketingInfoFragment) => {
    deletePlanMarketing({
      variables: {
        input: { id: trade.id },
      },
    });
  };

  const handleRequestSave = async (data: CompteRenduFormOuptput) => {
    setSaved(false);

    if (data.id) {
      updateCompteRendu({
        variables: {
          id: data.id,
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          input: {
            includesAllNoClosedItems: false,
            idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
            partenaireType: 'LABORATOIRE',
            titre: data.titre,
            remiseEchantillon: data.remiseEchantillon,
            gestionPerime: data.gestionPerime,
            conclusion: data.conclusion,
            rapportVisite: data.rapportVisite,
            idResponsables: data.idResponsables,
            idUserParticipants: data.idUserParticipants,
            idSuiviOperationnels: data.idSuiviOperationnels,
            conditionCommerciales: await Promise.all(
              (data.conditionCommerciales || []).map(async (condition: any) => ({
                ...condition,
                idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
                partenaireType: 'LABORATOIRE',
                fichiers: await getFile(condition),
              }))
            ),
            planMarketings: await Promise.all(
              (data.planMarketings || []).map(async (plan: any) => ({
                ...plan,
                idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
                partenaireType: 'LABORATOIRE',
                fichiers: await getFile(plan),
                typeRemuneration: 'euro', //plan.typeRemuneration,
                remuneration: 'forfaitaire', //plan.remuneration,
              }))
            ),
            dateRendezVous: data.dateRendezVous,
            heureDebut: data.heureDebut,
            heureFin: data.heureFin,
            typeReunion: data.typeReunion,
          },
        },
      });
    } else {
      createCompteRendu({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          input: {
            includesAllNoClosedItems: true,
            idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
            partenaireType: 'LABORATOIRE',
            titre: data.titre,
            remiseEchantillon: data.remiseEchantillon,
            gestionPerime: data.gestionPerime,
            conclusion: data.conclusion,
            rapportVisite: data.rapportVisite,
            idResponsables: data.idResponsables,
            idUserParticipants: data.idUserParticipants,
            idSuiviOperationnels: data.idSuiviOperationnels,
            conditionCommerciales: await Promise.all(
              (data.conditionCommerciales || []).map(async (condition: any) => ({
                ...condition,
                idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
                partenaireType: 'LABORATOIRE',
                fichiers: await getFile(condition),
              }))
            ),
            planMarketings: await Promise.all(
              (data.planMarketings || []).map(async (plan: any) => ({
                ...plan,
                idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
                partenaireType: 'LABORATOIRE',
                fichiers: await getFile(plan),
                typeRemuneration: 'euro', //plan.typeRemuneration,
                remuneration: 'forfaitaire', //plan.remuneration,
              }))
            ),
            dateRendezVous: data.dateRendezVous,
            heureDebut: data.heureDebut,
            heureFin: data.heureFin,
            typeReunion: data.typeReunion,
          },
        },
      }).then((result) => {
        if (result?.data?.createOnePRTCompteRendu && data.isImprimable) {
          // localhost:3050/#/laboratoires/detail-compte-rendu?id=1ygu1ozr9hjjmsgz8q40tzawx&idLabo=ckf6k11x6f8yz0784p47zjbt2
          history.push(
            `/laboratoires/detail-compte-rendu?id=${result.data.createOnePRTCompteRendu.id}&idLabo=${result.data.createOnePRTCompteRendu.idPartenaireTypeAssocie}&isAutoDownload=true`
          );
        }
      });
    }
  };

  const handleLoadCompteRendu = () => {
    const sortTableFilter = sorting || [];
    if (idLabo?.length) {
      loadCompteRendu({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          filter: {
            idPharmacie: {
              eq: pharmacie.id,
            },
            idPartenaireTypeAssocie: {
              eq: idLabo[0],
            },
            rapportVisite: searchText
              ? {
                  iLike: `%${searchText}%`,
                }
              : undefined,
          },
          paging: {
            limit: take ? parseInt(take) : 12,
            offset: skip ? parseInt(skip) : 0,
          },
          sorting: sortTableFilter,
        },
      });
    } else {
      loadCompteRendu({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          filter: {
            idPharmacie: {
              eq: pharmacie.id,
            },
            rapportVisite: searchText
              ? {
                  iLike: `%${searchText}%`,
                }
              : undefined,
          },
          paging: {
            limit: take ? parseInt(take) : 12,
            offset: skip ? parseInt(skip) : 0,
          },
          sorting: sortTableFilter,
        },
      });
    }
  };

  const handleRequestSearch = ({ skip, take, searchText, filter, sorting }: any) => {
    redirectTo({ ...params, skip, take, searchText, sorting });
  };

  useEffect(() => {
    if (view === 'creation' || view === 'modification') {
      if (idLabo?.length) {
        loadLabo({
          variables: {
            id: idLabo[0],
            idPharmacie: pharmacie.id,
          },
        });
      }
      loadPharmacie({ variables: { id: pharmacie.id } });
      loadConditionTypes();
      loadPlanMarketingTypes({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          filter: { idGroupement: { eq: groupement.id } },
        },
      });
      loadRendezVous({
        variables: {
          filter: {
            and: [
              {
                idPartenaireTypeAssocie: {
                  eq: idLabo?.length ? idLabo[0] : undefined,
                },
              },
              {
                idPharmacie: {
                  eq: pharmacie.id,
                },
              },
              {
                or: [
                  {
                    statut: { eq: 'REPORTE' },
                  },
                  {
                    statut: { eq: 'PLANIFIE' },
                  },
                ],
              },
              {
                dateRendezVous: {
                  gte: startTime(moment(new Date())),
                },
              },
            ],
          },
          sorting: [{ field: PrtRendezVousSortFields.DateRendezVous, direction: SortDirection.Asc }],
        },
      });
      loadConditionCommerciale({
        variables: {
          input: {
            idPartenaireTypeAssocie: idLabo?.length ? idLabo[0] : undefined,
            partenaireType: 'LABORATOIRE',
          },
        },
      });
      loadPlanMarketing({
        variables: {
          input: {
            idPartenaireTypeAssocie: idLabo?.length ? idLabo[0] : undefined,
            partenaireType: 'LABORATOIRE',
            estRemiseArriere: false,
          },
        },
      });
      loadRemiseArriere({
        variables: {
          input: {
            idPartenaireTypeAssocie: idLabo?.length ? idLabo[0] : undefined,
            partenaireType: 'LABORATOIRE',
            estRemiseArriere: true,
          },
        },
      });
    }
    handleLoadCompteRendu();
  }, [idLabo && idLabo[0]]);

  useEffect(() => {
    handleLoadCompteRendu();
  }, [
    skip,
    take,
    searchText,
    sorting && sorting[0] && sorting[0].field,
    sorting && sorting[0] && sorting[0].direction,
  ]);

  const getFile = async (data: any) => {
    if (data.fichiers && data.fichiers.length > 0) {
      return uploadFiles(data.fichiers, {
        directory: 'relation-fournisseurs/compte-rendu',
      });
    } else {
      return [];
    }
  };

  const compteRenduFormProps = {
    /* conditionCommercialeTypes: (loadingConditionTypes.data?.pRTConditionCommercialeTypes.nodes || []).map((item) => ({
      id: item.id,
      libelle: item.libelle,
      code: item.code,
    })),*/
    planMarketingTypes: (loadingPlanMarketingTypes.data?.pRTPlanMarketingTypes.nodes || []).map((item) => ({
      id: item.id,
      libelle: item.libelle,
      code: item.code,
    })),
    selectedSuiviOperationnelIds:
      selectSuiviOperation ||
      (loadingCompteRenduToEdit.data?.pRTCompteRendu?.suiviOperationnels || []).map((el) => {
        return el.id;
      }) ||
      [],
  };

  const handleSaveRdv = useCallback(
    (rdv: any) => {
      if (rdv.id) {
        updateRdv({
          variables: {
            id: rdv.id,
            input: {
              typeReunion: rdv.typeReunion,
              idPartenaireTypeAssocie: rdv.idPartenaireTypeAssocie,
              dateRendezVous: rdv.dateRendezVous,
              heureDebut: rdv.heureDebut,
              heureFin: rdv.heureFin,
              //idSubject: rdv.idSubject,
              idInvites: rdv.idInvites,
              idUserParticipants: rdv.idUserParticipants,
              note: rdv.note,
              partenaireType: 'LABORATOIRE',
              statut: rdv.statut,
            },
          },
        });
      } else {
        createRdv({
          variables: {
            input: {
              typeReunion: rdv.typeReunion,
              idPartenaireTypeAssocie: rdv.idPartenaireTypeAssocie,
              dateRendezVous: rdv.dateRendezVous,
              heureDebut: rdv.heureDebut,
              heureFin: rdv.heureFin,
              //idSubject: rdv.idSubject,
              idInvites: rdv.idInvites,
              idUserParticipants: rdv.idUserParticipants,
              note: rdv.note,
              partenaireType: 'LABORATOIRE',
              statut: rdv.statut,
            },
          },
        });
      }
    },
    [idLabo]
  );

  const handleSendMail = (email: SendEmailInput) => {
    setSendingEmail(true);
    if (email.compteRendu?.id && email.attachment) {
      sendEmail({
        variables: {
          input: {
            idCompteRendu: email.compteRendu?.id,
            attachment: email.attachment,
          },
        },
      });
    }
  };

  const handleGoBack = () => {
    redirectTo(view ? { view: undefined } : undefined, view ? undefined : 'compte-rendu');
  };

  const compteRenduDetail = (
    <CompteRenduDetail
      compteRenduValue={detail?.compteRenduValue}
      laboratoireInformation={detail?.laboratoireInformation}
      pharmaciInformation={detail?.pharmacieInformation}
      dateProchainRendezVous={detail?.dateProchainRendezVous}
      onRequestSendMail={handleSendMail}
      sendingEmail={isSendingEmail}
    />
  );

  const handleCompteRenduDetail = (props?: OnCompteRenduDetailProps) => {
    //redirectTo({ view: 'detail-compte-rendu', id: props?.compteRenduValue?.id });

    loadPartenariat({
      variables: {
        paging: {
          limit: 1,
        },
        filter: {
          and: [
            {
              idPartenaireTypeAssocie: {
                eq: idLabo?.length ? idLabo[0] : undefined,
              },
            },
            {
              partenaireType: {
                eq: 'LABORATOIRE',
              },
            },
            {
              idPharmacie: {
                eq: props?.compteRenduValue?.idPharmacie,
              },
            },
          ],
        },
      },
    });
    setDetail(props);
  };

  const handleEditCompteRendu = (compteRendu: CompteRendu) => {
    loadCompteRenduToEdit({
      variables: {
        id: compteRendu.id,
        idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
      },
    });
  };

  const handleRequestSearchProduits = (
    change?: TableChange | null,
    ids?: string[],
    cataloguePharmacie?: boolean,
    idPartenaireTypeAssocie?: string
  ) => {
    console.log('****************Request search');
    let must: any = [
      {
        term: {
          'commandeCanal.code': 'PFL',
        },
      },
      {
        term: {
          'produit.produitTechReg.laboExploitant.id': idPartenaireTypeAssocie || '',
        },
      },
    ];

    if (typeof cataloguePharmacie === 'undefined' || cataloguePharmacie) {
      must.push({ terms: { 'produit.pharmacieReferences': [pharmacie.id] } });
      setUseCataloguePharmacie(true);
    } else {
      setUseCataloguePharmacie(false);
    }

    if (ids && ids.length) {
      must.push({
        terms: {
          _id: ids,
        },
      });
    }

    if (change) {
      const searchText = change.searchText.replace(/\s+/g, '\\ ').replace('/', '\\/');
      if (searchText) {
        must.push({
          query_string: {
            query: searchText.startsWith('*') || searchText.endsWith('*') ? searchText : `*${searchText}*`,
            fields: [],
            analyze_wildcard: true,
          },
        });
      }
    }

    const variables: Search_Plan_Marketing_Produit_CanalsQueryVariables = {
      type: ['produitcanal'],
      query: {
        query: {
          bool: {
            must,
          },
        },
      },
      take: change?.take || 12,
      skip: change?.skip || 0,
    };

    if (ids && ids.length) {
      loadPlanMarketingProduits({ variables });
    }
    //if (change) {
    loadProduits({ variables });
    //}
  };

  const handleRequestFetchMore = () => {
    const fetchMore = loadingCompteRendu.fetchMore;
    const variables = loadingCompteRendu.variables;
    const paging = variables?.paging;
    if (loadingCompteRendu.data?.pRTCompteRendus.nodes) {
      fetchMore &&
        fetchMore({
          variables: {
            ...variables,
            paging: {
              ...paging,
              offset: loadingCompteRendu.data?.pRTCompteRendus.nodes.length || 0,
            },
          },

          updateQuery: (prev: any, { fetchMoreResult }) => {
            if (prev?.pRTCompteRendus && fetchMoreResult?.pRTCompteRendus.nodes) {
              return {
                ...prev,
                pRTCompteRendus: {
                  ...prev.pRTCompteRendus,
                  nodes: [...prev.pRTCompteRendus.nodes, ...fetchMoreResult.pRTCompteRendus.nodes],
                },
              };
            }

            return prev;
          },
        });
    }
  };

  const produits = {
    error: loadingProduits.error as any,
    loading: loadingProduits.loading as any,
    data: loadingProduits.data?.search?.data,
    rowsTotal: loadingProduits.data?.search?.total || 0,
    onRequestSearch: handleRequestSearchProduits,
    planMarketingProduits: loadingPlanMarketingProduits.data?.search?.data || [],
    pharmacieCatalogue: useCataloguePharmacie,
  };

  const compteRenduProps = {
    laboratoire: loadingLabo.data?.laboratoire as any,
    compteRendus: {
      loading: loadingCompteRendu.loading,
      error: loadingCompteRendu.error as any,
      data: (loadingCompteRendu.data?.pRTCompteRendus.nodes || []).map((item) => ({
        id: item.id,
        titre: item.titre,
        remiseEchantillon: item.remiseEchantillon,
        gestionPerime: item.gestionPerime || undefined,
        responsables: item.responsables || [],
        collaborateurs: item.collaborateurs,
        planMarketings: (item.planMarketings || []).map((plan) => ({
          ...plan,
          idType: plan.type.id,
        })),
        suiviOperationnelIds: (item.suiviOperationnels || []).map((d) => d.id),
        createdAt: item.createdAt,
        conditionsCommerciales: (item.conditionsCommerciales || []) as any,
        suiviOperationnels: item.suiviOperationnels || [],
        conclusion: item.conclusion || undefined,
        rapportVisite: item.rapportVisite || undefined,
        notificationLogs: item.notificationLogs,
        idPharmacie: item.idPharmacie,
        partenaireTypeAssocie: item.partenaireTypeAssocie,
        dateRendezVous: item.dateRendezVous,
        heureDebut: item.heureDebut,
        heureFin: item.heureFin,
        typeReunion: item.typeReunion,
      })) as any,
    },
    rowsTotal:
      (loadingCompteRendu.data?.pRTCompteRendus.totalCount || 0) > 0
        ? loadingCompteRendu.data?.pRTCompteRendus.totalCount || 0
        : 0,
    onRequestSave: handleRequestSave,
    onRequestDelete: handleRequestDelete,
    onRequestSearch: handleRequestSearch,
    compteRenduForm: compteRenduFormProps,
    saving: {
      error: updatingCompteRendu.error || creatingCompteRendu.error,
      loading: updatingCompteRendu.loading || creatingCompteRendu.loading,
      success,
    },
    deletingSuccess: success,
    rendezVous: {
      onRequestSave: handleSaveRdv,
      rendezVous: loadingRendezVous.data?.pRTRendezVous.nodes[0] as any,
      saving: {
        loading: updatingRdv.loading || creatingRdv.loading,
        error: updatingRdv.error || creatingRdv.error,
        success: successRdv,
      },
      selectedInvites: responsableRdv,
      selectedCollaborateurs: collaborateursRdv,
      collaborateurPiker: (
        <UserInput
          openModal={openCollaborateurRdv}
          withNotAssigned={false}
          setOpenModal={setOpenCollaborateurRdv}
          selected={collaborateursRdv}
          setSelected={setCollaborateursRdv}
          label="Collaborateur"
        />
      ),
      changeSuccess: () => {
        setSuccessRdv(false);
      },
      setSelectedInvites: (selecteds: any) => {
        setResponsablesRdv(selecteds);
      },
      setSelectedParticipants: (selecteds: any) => {
        setCollaborateursRdv(selecteds);
      },
    },
    onRequestGoBack: handleGoBack,
    planMarketings: {
      onSavePlan: handleSavePlan,
      saving: {
        loading: creatingPlan.loading,
        error: creatingPlan.error,
        success,
      },
      data: (loadingPlanMarketing.data?.compteRenduPlanMarketing as any) || [],
      loading: loadingPlanMarketing.loading,
      error: loadingPlanMarketing.error as any,
      onRequestDeleteTrade: handleDeleteTrade,
    },
    remiseArrieres: {
      onSavePlan: handleSavePlan,
      saving: {
        loading: creatingPlan.loading,
        error: creatingPlan.error,
        success,
      },
      data: (loadingRemiseArriere.data?.compteRenduPlanMarketing as any) || [],
      loading: loadingRemiseArriere.loading,
      error: loadingRemiseArriere.error as any,
      onRequestDeleteRemiseArriere: handleDeleteTrade,
    },
    conditionCommerciales: {
      onSaveCondition: handleSaveCondition,
      saving: {
        loading: creatingCondition.loading,
        error: creatingCondition.error,
        success,
      },
      data: (loadingConditionCommerciale.data?.compteRenduConditionCommerciale as any) || [],
      loading: loadingConditionCommerciale.loading,
      error: loadingConditionCommerciale.error as any,
      onRequestDeleteCondition: handleDeleteCondition,
    },
    conditionCommercialeTypes: (loadingConditionTypes.data?.pRTConditionCommercialeTypes.nodes || []).map((item) => ({
      id: item.id,
      libelle: item.libelle,
      code: item.code,
    })),
    onCompteRenduDetail: handleCompteRenduDetail,
    compteRenduDetail,
    setSelectSuiviOperation,
    selectedSuiviOperationnelIds: selectSuiviOperation,
    onRequestEdit: handleEditCompteRendu,
    compteRendu: loadingCompteRenduToEdit.data?.pRTCompteRendu as any,
    refetchCompteRendu: loadingCompteRendu?.refetch,
    handleSendMail,
    isSendingEmail,
    loadingCompteRendu: loadingCompteRenduToEdit.loading,
    produits,
    savedCompteRendu: saved,
    setSavedCompteRendu: setSaved,
    onRequestFetchMore: handleRequestFetchMore,
  };
  return compteRenduProps;
};
