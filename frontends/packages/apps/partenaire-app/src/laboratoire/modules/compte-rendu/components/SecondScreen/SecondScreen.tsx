import { useApplicationContext } from '@lib/common';
import { PrtConditionInfoFragment, PrtPlanMarketingInfoFragment } from '@lib/common/src/federation';
import { ConfirmDeleteDialog, CustomEditorText, CustomModal, NewCustomButton, Table } from '@lib/ui-kit';
import { useWindowDimensions } from '@lib/ui-kit/src/components/atoms/MobileTopBar/MobileTopBar';
import { Box, Checkbox, FormControl, FormControlLabel, IconButton, Typography } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import moment from 'moment';
import React, { FC, useCallback, useEffect, useState } from 'react';
import {
  FormConditionCommerciale,
  FormConditionCommercialeOutput,
} from './../../../condition-commerciale/pages/condition-commerciale-form/ConditionCommercialeForm';
import { useStyles } from './styles';
import { SecondFormulaireCompteRenduProps } from './types';
import { useColumnConditionCommerciale } from './useColumnCondtionCommerciale';
import { useLaboratoireParams } from './../../../../hooks/useLaboratoireParams';
interface Form {
  open: boolean;
  mode: 'ajout' | 'modification';
  saved: boolean;
}

export const SecondFormulaireCompteRendu: FC<SecondFormulaireCompteRenduProps> = ({
  valueToSave,
  setValueToSave,
  conditionSelected,
  setConditionSelected,
  onRequestSelectedCondition,
  partenaireAssocie,
  conditionCommercialeTypes,
  conditionCommerciales,
}) => {
  const styles = useStyles();
  const dimension = useWindowDimensions();
  const { isMobile } = useApplicationContext();
  const [conditionCommerciale, setConditionCommerciale] = useState<PrtConditionInfoFragment | undefined>();
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [anchor, setAnchor] = useState<any>();

  const [conditionCommercialeForm, setConditionCommercialeForm] = useState<Form>({
    open: false,
    mode: 'ajout',
    saved: false,
  });

  const {
    params: { id },
  } = useLaboratoireParams();

  useEffect(() => {
    if (!conditionCommercialeForm.open) {
      setConditionCommerciale(undefined);
    }
  }, [conditionCommercialeForm.open]);

  const handleClickMenuConditionCommerciale = (e: any, row: PrtConditionInfoFragment | undefined) => {
    setAnchor(e.currentTarget);
    setConditionCommerciale(row);
  };

  const handleOpenFormConditionCommerciale = () => {
    setConditionCommercialeForm((prev) => ({ ...prev, open: !prev.open }));
  };

  const handleClearSelectionCondition = () => {
    setConditionSelected && setConditionSelected([]);
  };

  const handleSelectAllCondition = () => {
    setConditionSelected && setConditionSelected(valueToSave?.conditionsCommerciales as any);
  };

  const handleSaveConditionCommerciale = useCallback(
    (data: FormConditionCommercialeOutput, mode?: 'ajout' | 'modification') => {
      const _condifitionCommerciale: PrtConditionInfoFragment = {
        id: data.id,
        titre: data.libelle,
        dateDebut: data.periode.debut,
        dateFin: data.periode.fin,
        idType: data.idType,
        fichiers: data.fichiers || [],
        description: data.details || '',
        type: conditionCommercialeTypes.find((e) => e.id === data.idType) as any,
        idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
      } as any;

      conditionCommerciales?.onSaveCondition(_condifitionCommerciale);

      if (mode === 'ajout') {
        setValueToSave({
          ...valueToSave,
          conditionsCommerciales: [...((valueToSave?.conditionsCommerciales as any) || []), _condifitionCommerciale],
        });
      } else {
        if (conditionCommerciale) {
          const keyFinding = ((valueToSave?.conditionsCommerciales as any) || []).indexOf(conditionCommerciale);

          if (keyFinding >= 0 && valueToSave && valueToSave.conditionsCommerciales) {
            setValueToSave({
              ...valueToSave,
              conditionsCommerciales: valueToSave.conditionsCommerciales.map((item: any, key: number) => {
                if (key === keyFinding) {
                  return _condifitionCommerciale;
                } else {
                  return item;
                }
              }),
            });
            setAnchor(null);
          }
        }
      }
      setConditionCommercialeForm({
        ...conditionCommercialeForm,
        open: false,
        saved: false,
        mode: 'ajout',
      });
      setConditionCommerciale(undefined);
    },
    [valueToSave, conditionCommercialeForm, conditionCommerciale, conditionCommercialeTypes]
  );

  const handleDelete = (row: PrtConditionInfoFragment) => {
    setOpenDeleteDialog(true);
    setAnchor(null);
  };

  const handleConfirmDelete = () => {
    if (conditionCommerciale) {
      conditionCommerciales?.onRequestDeleteCondition(conditionCommerciale);
    }
    setOpenDeleteDialog(false);
  };

  const handleEdit = (row: PrtConditionInfoFragment) => {
    setConditionCommercialeForm({
      mode: 'modification',
      open: true,
      saved: false,
    });
    setAnchor(null);
  };

  const handleCloseMenu = () => {
    setAnchor(null);
  };

  const columnsConditionCommerciale = useColumnConditionCommerciale({
    onMenuClick: handleClickMenuConditionCommerciale,
    onDeleteClick: handleDelete,
    onEditClick: handleEdit,
    onCloseMenu: handleCloseMenu,
    anchor,
    conditionCommerciale,
  });

  const handleChangeRemiseEchatillon = useCallback(
    (__e: any, checked: boolean) => {
      setValueToSave && setValueToSave({ ...valueToSave, remiseEchantillon: checked });
    },
    [valueToSave]
  );

  const handleChangeGestionPerime = useCallback(
    (text: string) => {
      setValueToSave && setValueToSave({ ...valueToSave, gestionPerime: text });
    },
    [valueToSave]
  );

  const handleSelectChangeCondition = (dataSelected: any) => {
    setConditionSelected && setConditionSelected(dataSelected);
    onRequestSelectedCondition && onRequestSelectedCondition(dataSelected);
  };

  return (
    <Box className={styles.root}>
      <Box className={styles.container}>
        <h2 className={styles.title}>Nouveautés</h2>
        <FormControl component="fieldset" className={styles.formControlCheckbox}>
          <FormControlLabel
            control={
              <Checkbox
                checked={valueToSave?.remiseEchantillon}
                onChange={handleChangeRemiseEchatillon}
                name="remiseEchantillon"
              />
            }
            label="Remise d'échantillon"
          />
        </FormControl>
        <Box className={styles.editorTextContainer}>
          <CustomEditorText
            className={styles.editorText}
            value={valueToSave?.gestionPerime || ''}
            onChange={handleChangeGestionPerime}
            placeholder="Gestion des perimés"
          />
        </Box>

        <Box className={styles.tableConditionCommercialeContainer}>
          <Box className={styles.headerTable}>
            {isMobile ? (
              <IconButton style={{ float: 'right', padding: 0 }} onClick={handleOpenFormConditionCommerciale}>
                <Add />
              </IconButton>
            ) : (
              ''
            )}
            <h2 className={styles.title}>Conditions Commerciales</h2>
            {dimension !== 'isSm' && (
              <NewCustomButton
                theme="flatPrimary"
                variant="text"
                onClick={handleOpenFormConditionCommerciale}
                startIcon={<Add />}
              >
                AJOUTER UNE CONDITION
              </NewCustomButton>
            )}
          </Box>
          {isMobile ? (
            <>
              {conditionCommerciales?.data.map((item, index) => {
                return (
                  <div key={index}>
                    <Typography style={{ fontWeight: 600 }}>{item.titre} </Typography>
                    <Typography variant="caption">
                      {`Du ${moment(item.dateDebut).format('DD/MM/YYYY')} - Au ${moment(item.dateFin).format(
                        'DD/MM/YYYY'
                      )}`}{' '}
                      <FiberManualRecordIcon style={{ width: 7, height: 7 }} /> <span>{item.type.libelle} </span>
                    </Typography>
                  </div>
                );
              })}
            </>
          ) : (
            <>
              <Table
                search={false}
                noTableToolbarSearch
                columns={columnsConditionCommerciale}
                data={conditionCommerciales?.data || []}
                error={conditionCommerciales?.error as any}
                loading={conditionCommerciales?.loading}
                selectable={id ? true : false}
                onClearSelection={handleClearSelectionCondition}
                onSelectAll={handleSelectAllCondition}
                rowsSelected={conditionSelected}
                onRowsSelectionChange={handleSelectChangeCondition}
                dataIdName="id"
              />
              {dimension === 'isSm' && (
                <Box width={1} display="flex" justifyContent="flex-end" marginTop={2}>
                  <NewCustomButton
                    theme="flatPrimary"
                    variant="text"
                    onClick={handleOpenFormConditionCommerciale}
                    startIcon={<Add />}
                  >
                    AJOUTER UNE CONDITION
                  </NewCustomButton>
                </Box>
              )}
            </>
          )}
        </Box>
      </Box>
      <FormConditionCommerciale
        partenaireAssocie={partenaireAssocie}
        mode={conditionCommercialeForm.mode}
        open={conditionCommercialeForm.open}
        setOpen={handleOpenFormConditionCommerciale}
        canalCommandes={[]}
        typesConditionCommande={conditionCommercialeTypes}
        onSave={handleSaveConditionCommerciale}
        saving={false}
        // saved={conditionCommercialeForm.saved}
        value={
          conditionCommerciale
            ? ({
                id: conditionCommerciale.id,
                idType: conditionCommerciale.idType,
                libelle: conditionCommerciale.titre,
                periode: {
                  debut: conditionCommerciale.dateDebut,
                  fin: conditionCommerciale.dateFin,
                },
                details: conditionCommerciale.description,
                fichiers: conditionCommerciale.fichiers,
                idPartenaireTypeAssocie: conditionCommerciale.idPartenaireTypeAssocie,
              } as any)
            : undefined
        }
      />
      <ConfirmDeleteDialog
        title="Suppression"
        content="Etes-vous sûr de vouloir supprimer cette condition commerciale ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDelete}
      />
    </Box>
  );
};
