import { Column } from '@lib/ui-kit';
import moment from 'moment';

export const conditionAndPlanColumn: Column[] = [
  {
    name: 'type',
    label: 'Type',
    renderer: (row: any) => {
      return row.type ? row.type.libelle : '';
    },
  },
  {
    name: 'titre',
    label: 'Titre',
    renderer: (row: any) => {
      return row.titre ? row.titre : '';
    },
  },
  {
    name: 'periode',
    label: 'Période',
    renderer: (row: any) => {
      return row.dateDebut && row.dateFin
        ? `Du ${moment(row.dateDebut).format('DD-MM-YYYY')} - Au ${moment(row.dateFin).format('DD-MM-YYYY')}`
        : '';
    },
  },
];
