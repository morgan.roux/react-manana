import React, { FC, useCallback, useEffect, useMemo, useState } from 'react';
import { Box, Typography } from '@material-ui/core';
import { SecondFormulaireCompteRendu, TradeCompteRendu, RemiseCompteRendu } from './SecondScreen';
import { ThirdFormulaireCompteRendu } from './ThirdScreen';
import { CustomAvatar, CustomEditorText, Stepper } from '@lib/ui-kit';
import { useParams } from 'react-router';
import { useStyles } from './styles';
import { CompteRenduFormProps } from './interface';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import { useRemise_Arriere_TypesQuery } from '@lib/common/src/federation';
import ActionOperationnel from '../../action-operationnel/pages/ActionOperationnel';
import classNames from 'classnames';
import moment from 'moment';

const CompteRenduForm: FC<CompteRenduFormProps> = ({
  onChangeStep,
  onSideNavListClick,
  selectedSuiviOperationnelIds,
  onReturn,
  saving,
  value,
  conditionCommercialeTypes,
  conditionCommerciales,
  planMarketingTypes,
  planMarketings,
  rendezVous,
  onRequestSave,
  produits,
  setPlanSelected,
  setConditionSelected,
  planSelected,
  conditionSelected,
  partenaireAssocie,
  userParticipants,
  setUserParticipants,
  responsables,
  setResponsables,
  laboratoire,
  setValueToSave,
  remiseArrieres,
  remiseSelected,
  setRemiseSelected,
}) => {
  const valueToSave = value;
  const [isImprimable, setIsImprimable] = useState<boolean>(false);
  const { view: activeItem } = useParams<{ view: string }>();

  const { params, goBack } = useLaboratoireParams();
  const { id: idCompteRendu } = params;

  const handleCancelStepper = () => {
    goBack('compte-rendu');
  };

  const remiseArriereTypes = useRemise_Arriere_TypesQuery().data?.pRTRemiseArriereTypes.nodes || [];

  const handlePrev = useCallback(() => {
    onReturn && onReturn(selectedSuiviOperationnelIds || []);
  }, [selectedSuiviOperationnelIds]);

  const handleSubmit = useCallback(() => {
    console.log('---------------------------valueToSave-----------------: ', valueToSave);
    onRequestSave &&
      onRequestSave({
        id: valueToSave?.id,
        titre:
          valueToSave?.typeReunion && valueToSave?.dateRendezVous
            ? `Rendez-Vous en ${valueToSave?.typeReunion} le ${moment(valueToSave.dateRendezVous).format(
                'DD/MM/YYYY'
              )} ${
                valueToSave?.heureDebut
                  ? `à partir de ${valueToSave?.heureDebut} ${
                      valueToSave?.heureFin ? `jusqu'à ${valueToSave?.heureFin}` : ''
                    }`
                  : ''
              }`
            : '',
        remiseEchantillon: valueToSave?.remiseEchantillon || false,
        idResponsables: valueToSave?.responsablesIds || [],
        idUserParticipants: valueToSave?.collaborateurIds || [],
        planMarketings: [...(valueToSave?.planMarketings || []), ...(valueToSave?.remiseArrieres || [])].map(
          (plan) => ({
            id: plan?.id,
            titre: plan.titre,
            dateDebut: plan.dateDebut,
            dateFin: plan.dateFin,
            idStatut: plan.statut?.id,
            idMiseAvants: (plan.miseAvants || []).map((item) => item.id),
            idType: plan.type.id,
            description: plan.description || '',
            fichiers: plan.fichiers as any[],
            estRemiseArriere: plan.estRemiseArriere,
          })
        ),
        idSuiviOperationnels: valueToSave?.suiviOperationIds || [],
        conditionCommerciales: (valueToSave?.conditionsCommerciales || []).map((condition) => ({
          id: condition?.id,
          titre: condition.titre,
          idType: condition.idType,
          idStatut: condition?.idStatut,
          dateDebut: condition.dateDebut,
          dateFin: condition.dateFin,
          description: condition.description,
          fichiers: condition.fichiers as any[],
        })) as any,
        conclusion: valueToSave?.conclusion,
        rapportVisite: valueToSave?.rapportVisite,
        gestionPerime: valueToSave?.gestionPerime,
        idPartenaireTypeAssocie: partenaireAssocie?.id,
        dateRendezVous: valueToSave?.dateRendezVous,
        heureDebut: valueToSave?.heureDebut,
        heureFin: valueToSave?.heureFin,
        typeReunion: valueToSave?.typeReunion,
        isImprimable: isImprimable,
      });
    setValueToSave((prev: any) => ({
      ...prev,
      suiviOperationIds: [''],
      conditionsCommerciales: [],
      planMarketings: [],
      collaborateurIds: [],
      responsablesIds: [],
      dateRendezVous: valueToSave?.dateRendezVous,
      heureDebut: valueToSave?.heureDebut,
      heureFin: valueToSave?.heureFin,
      typeReunion: valueToSave?.typeReunion,
    }));
  }, [valueToSave, isImprimable]);

  useEffect(() => {
    if (saving && saving.success) {
      onSideNavListClick && onSideNavListClick('compte-rendu');
    }
  }, [saving]);

  const isValid = useMemo(() => {
    return (
      valueToSave &&
      valueToSave.rapportVisite &&
      valueToSave.rapportVisite !== '' &&
      valueToSave.collaborateurIds &&
      valueToSave.collaborateurIds.length > 0 &&
      valueToSave.responsablesIds &&
      valueToSave.responsablesIds.length > 0
    );
  }, [valueToSave]);

  const classes = useStyles();
  const handleChangeRapport = (value: string) => {
    setValueToSave((prev: any) => ({ ...prev, rapportVisite: value === '<p><br></p>' ? undefined : value }));
  };

  console.log('------------------------COMPTE RENDU EDIT---------------: ', valueToSave);

  return (
    <Box margin={3} display="flex" flexDirection="column" marginBottom="70px !important">
      <Typography className={classes.nouveauCR}>
        {activeItem === 'ajout-compte-rendu'}
        <span className={classes.titreCR}>{idCompteRendu ? 'Modification compte rendu' : 'Nouveau compte rendu'}</span>

        <span className={classes.titrePartenaire}>
          <CustomAvatar
            url={partenaireAssocie?.photo?.publicUrl}
            name={partenaireAssocie?.nom}
            className={classes.small}
          />
          <span style={{ marginRight: '8px', marginLeft: '8px' }}>{partenaireAssocie?.nom}</span>
        </span>
      </Typography>
      <Stepper
        contentStyle={{ width: '100%' }}
        steps={[
          {
            title: 'Litiges',
            content: (
              <Box width="100%" marginTop={3}>
                <ActionOperationnel
                  onRequestSelectedIds={(ids) => setValueToSave((prev: any) => ({ ...prev, suiviOperationIds: ids }))}
                  suiviOperationnelSelected={valueToSave?.suiviOperationIds || []}
                  partenaireAssocie={laboratoire}
                />
              </Box>
            ),
          },
          {
            title: 'Actions',
            content: (
              <Box width="100%" marginTop={3}>
                <ActionOperationnel
                  onRequestSelectedIds={(ids) => setValueToSave((prev: any) => ({ ...prev, suiviOperationIds: ids }))}
                  suiviOperationnelSelected={valueToSave?.suiviOperationIds || []}
                  partenaireAssocie={laboratoire}
                  categorie="ACTION"
                />
              </Box>
            ),
          },
          {
            title: 'Plan Trade',
            content: (
              <TradeCompteRendu
                planMarketingTypes={planMarketingTypes as any}
                valueToSave={valueToSave}
                setValueToSave={setValueToSave}
                conditionCommercialeTypes={conditionCommercialeTypes}
                produits={produits}
                planMarketings={planMarketings}
                planSelected={(planSelected || []).filter((p) => !p.estRemiseArriere)}
                setPlanSelected={setPlanSelected as any}
                partenaireAssocie={partenaireAssocie}
              />
            ),
          },
          {
            title: 'Remise Arriere',
            content: (
              <RemiseCompteRendu
                planMarketingTypes={remiseArriereTypes as any}
                valueToSave={valueToSave}
                setValueToSave={setValueToSave}
                conditionCommercialeTypes={conditionCommercialeTypes}
                produits={produits}
                remiseArrieres={remiseArrieres}
                remiseSelected={(remiseSelected || []).filter((p) => p.estRemiseArriere)}
                setRemiseSelected={setRemiseSelected as any}
                partenaireAssocie={partenaireAssocie}
              />
            ),
          },
          {
            title: 'Accords',
            content: (
              <SecondFormulaireCompteRendu
                planMarketingTypes={planMarketingTypes as any}
                valueToSave={valueToSave}
                setValueToSave={setValueToSave}
                conditionCommercialeTypes={conditionCommercialeTypes}
                produits={produits}
                conditionCommerciales={conditionCommerciales}
                planSelected={planSelected}
                conditionSelected={conditionSelected}
                setConditionSelected={setConditionSelected as any}
                setPlanSelected={setPlanSelected as any}
                partenaireAssocie={partenaireAssocie}
              />
            ),
          },
          {
            title: 'Rapports',
            content: (
              <ThirdFormulaireCompteRendu
                valueToSave={valueToSave}
                setValueToSave={setValueToSave}
                rendezVous={rendezVous}
                laboratoire={partenaireAssocie}
                userParticipants={userParticipants}
                setUserParticipants={setUserParticipants}
                responsables={responsables}
                setResponsables={setResponsables}
                imprimable={isImprimable}
                setIsImprimable={setIsImprimable}
              />
            ),
          },
        ]}
        fullWidth
        disableNextBtn={false}
        disableValidateBtn={!isValid || saving?.loading}
        onClickPrev={handlePrev}
        onCancel={handleCancelStepper}
        onValidate={handleSubmit}
        onStepChange={onChangeStep}
      >
        <Box className={classNames(classes.containerForm)}>
          <CustomEditorText
            className={classes.editorText}
            value={valueToSave?.rapportVisite || ''}
            onChange={handleChangeRapport}
            placeholder="Rapport de visite*"
          />
        </Box>
      </Stepper>
    </Box>
  );
};

export default CompteRenduForm;
