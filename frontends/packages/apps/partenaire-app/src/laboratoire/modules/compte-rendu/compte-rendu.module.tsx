import React from 'react';
import loadable from '@loadable/component';

import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';

const CompteRenduPage = loadable(() => import('./pages/CompteRendu'), {
  fallback: <SmallLoading />,
});

const CompteRenduDetailPage = loadable(() => import('./pages/compte-rendu-detail'), {
  fallback: <SmallLoading />,
});

const compteRenduModule: ModuleDefinition = {
  routes: [
    {
      attachTo: 'MAIN',
      path: '/laboratoires/compte-rendu',
      component: CompteRenduPage,
      activationParameters: {
        groupement: '0305',
        pharmacie: '0708',
      },
      exact: false,
    },
    {
      attachTo: 'MAIN',
      path: '/laoratoires/detail-compte-rendu?id=:id&idLabo=:idLabo&isAutoDownload=:autoDownload',
      component: CompteRenduDetailPage,
      activationParameters: {
        groupement: '0305',
        pharmacie: '0708',
      },
      exact: false,
    },
  ],
};

export default compteRenduModule;
