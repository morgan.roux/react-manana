import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      margin: '24px 0',
      display: 'flex',
      width: '100%',
      justifyContent: 'center',
    },
    container: {
      width: '75%',
      [theme.breakpoints.up('sm')]: { minWidth: 700 },
      [theme.breakpoints.down('sm')]: {
        width: '100%',
      },
    },
    title: {
      fontFamily: 'Roboto',
      fontSize: 18,
      margin: '0 0 16px 0',
      fontWeight: 500 as any,
      [theme.breakpoints.down('md')]: {
        fontSize: 16,
      },
    },
    formControlCheckbox: {
      marginBottom: 24,
    },
    editorText: {
      minHeight: 92,
    },
    editorTextContainer: {
      minHeight: 132,
      width: '100%',
      marginBottom: 60,
      [theme.breakpoints.up('sm')]: {
        marginBottom: 32,
      },
    },
    tableConditionCommercialeContainer: {
      width: '100%',
      marginBottom: 32,
    },
    headerTable: {
      [theme.breakpoints.down('md')]: {
        display: 'block',
      },
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginBottom: 8,
    },
  }),
);
