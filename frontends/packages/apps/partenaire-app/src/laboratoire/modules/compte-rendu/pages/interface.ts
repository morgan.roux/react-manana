import { ReactNode } from 'react';
import { PRTContact, PRTPlanMarketing, PRTRendezVous, PRTSuiviOperationnel, User } from '@lib/types';
import { TableChange } from '@lib/ui-kit';
import {
  LaboratoireInfoFragment,
  PrtPlanMarketingInfoFragment,
  PrtConditionInfoFragment,
  ConditionTypeInfoFragment,
  PrtCompteRenduInfoFragment,
} from '@lib/common/src/federation';
import { RendezVousFormOutput } from '../../rendezVous/pages/rendez-vous-form/interface';
import { CompteRenduFormOuptput, CompteRenduFormProps } from '../components/interface';
export interface NotificationLogs {
  id?: string;
  titre?: string;
  message?: string;
  type?: string;
  expediteur?: string;
  recepteurs?: string[];
  createdAt?: Date;
  updatedAt?: Date;
}

export interface PharmacieInformation {
  nom: string;
  adresse: string;
  email?: string;
  telephone?: string;
}

export interface LaboratoireInformation {
  nom: string;
  adresse: string;
  email?: string;
  telephone?: string;
  representantLabo?: {
    nom: string;
    telephone?: string;
  };
}

export interface SendEmailInput {
  subject?: string;
  to: string[];
  from?: string;
  cc?: string;
  html?: string;
  templateId?: string;
  templateData?: any;
  attachment?: AttachmentInput;
  compteRendu?: CompteRendu;
}

export interface AttachmentInput {
  content: string;
  filename: string;
  type: string;
}

export interface CompteRenduDetailProps {
  compteRenduValue?: CompteRendu;
  laboratoireInformation?: LaboratoireInformation;
  pharmaciInformation?: PharmacieInformation;
  dateProchainRendezVous?: Date;
  onRequestSendMail?: (email: SendEmailInput) => void;
  onSideNavListClick?: (route: string) => void;
  sendingEmail?: boolean;
}

export interface CompteRendu {
  id: string;
  titre: string;
  createdAt: Date;
  remiseEchantillon: boolean;
  gestionPerime?: string;
  conclusion?: string;
  rapportVisite?: string;
  conditionsCommerciales: PrtConditionInfoFragment[];
  planMarketings: PrtPlanMarketingInfoFragment[];
  collaborateurs: User[];
  responsables: PRTContact[];
  suiviOperationnelIds: string[];
  suiviOperationnels?: PRTSuiviOperationnel[];
  notificationLogs?: NotificationLogs;
  idPharmacie: string;
  partenaireTypeAssocie: any;
  dateRendezVous?: any;
  heureDebut?: string;
  heureFin?: string;
  typeReunion?: string;
}

export interface CompteRenduComponentProps {
  compteRendu?: PrtCompteRenduInfoFragment;
  onRequestEdit: (compteRendu: CompteRendu) => void;
  laboratoire?: LaboratoireInfoFragment;
  selectedSuiviOperationnelIds?: string[];
  onRequestSelected?: (suivis: PRTSuiviOperationnel[]) => void;
  activeItem?: string;
  onSideNavListClick?: (item: string) => void;
  compteRendus?: {
    loading?: boolean;
    error?: boolean;
    data?: CompteRendu[];
  };
  selectedSuiviId?: string[];
  compteRenduForm?: CompteRenduFormProps;
  rowsTotal?: number;
  onRequestSave?: (data: CompteRenduFormOuptput) => void;
  onRequestDelete?: (id: string) => void;
  onRequestSearch?: ({ skip, take, searchText, filter, sortTable }: any) => void;
  onRequestChangeStatut?: (id: string, value: string) => void;
  onRequestFetchMore?: () => void;
  filterIcon?: any;
  suiviComponent?: ReactNode;
  setSelectSuiviOperation?: (ids: string[], idCompteRendu?: string) => void;
  saving?: {
    loading?: boolean;
    success?: boolean;
    error?: Error;
  };
  deletingSuccess?: boolean;
  conditionCommercialeTypes?: ConditionTypeInfoFragment[];
  onMount?: () => void;
  rendezVous?: {
    onRequestSave: (rdv: RendezVousFormOutput) => void;
    rendezVous: PRTRendezVous;
    saving: {
      loading?: boolean;
      error?: Error;
      success?: boolean;
    };
    selectedInvites: any[];
    selectedCollaborateurs: any[];
    collaborateurPiker: ReactNode;
    changeSuccess: () => void;
    setSelectedParticipants?: (selecteds: any[]) => void;
    setSelectedInvites?: (selecteds: any[]) => void;
  };
  detailinformtionCompteRendu?: CompteRenduDetailProps;
  produits?: {
    error?: Error;
    loading?: boolean;
    data?: any;
    rowsTotal?: number;
    onRequestSearch?: (change?: TableChange | null, ids?: string[]) => void;
    planMarketingProduits?: any;
  };
  onRequestGoBack?: () => void;
  planMarketings?: {
    onSavePlan: (data: PrtPlanMarketingInfoFragment) => void;
    saving: {
      loading?: boolean;
      error?: Error;
      success?: boolean;
    };
    data: PRTPlanMarketing[];
    loading?: boolean;
    error?: boolean;
  };
  conditionCommerciales?: {
    onSaveCondition: (data: PrtConditionInfoFragment) => void;
    saving: {
      loading?: boolean;
      error?: Error;
      success?: boolean;
    };
    data: PrtConditionInfoFragment[];
    loading?: boolean;
    error?: boolean;
  };

  compteRenduDetail?: ReactNode;
  onCompteRenduDetail?: (props?: OnCompteRenduDetailProps) => void;
  refetchCompteRendu?: () => void;
}

export interface OnCompteRenduDetailProps {
  compteRenduValue?: CompteRendu;
  laboratoireInformation?: LaboratoireInformation;
  pharmacieInformation?: PharmacieInformation;
  dateProchainRendezVous?: Date;
  onRequestSendMail?: (email: SendEmailInput) => void;
}
