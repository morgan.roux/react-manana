import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      margin: '24px 0',
      width: '100%',
    },
    title: {
      fontFamily: 'Roboto',
      fontSize: 18,
      margin: '0 0 16px 0',
      fontWeight: 500 as any,
    },
    container: {
      width: '100%',
      marginBottom: 24,
    },
    rendezVousContainer: {
      display: 'flex',
      alignItems: 'center',
      '& span': {
        fontFamily: 'Roboto',
        fontWeight: 500 as any,
        fontSize: 16,
      },
    },
    editorText: {
      minHeight: 92,
    },
    editorTextContainer: {
      width: '100%',
      marginBottom: 32,
    },
    containerForm: {
      width: '75%',
      [theme.breakpoints.up('sm')]: {
        minWidth: 700,
      },
      [theme.breakpoints.down('sm')]: {
        width: '100%',
      },
    },
    userInputCompteRendu: {
      marginBottom: -24,
    },
  }),
);
