import React, { useState, FC, useEffect } from 'react';
import { CustomModal } from '@lib/ui-kit';
import CompteRenduForm from './../components/CompteRenduForm';

import { Box, Typography } from '@material-ui/core';
import { PrtPlanMarketingInfoFragment, PrtConditionInfoFragment } from '@lib/common/src/federation';
import { PartenaireInput, useApplicationContext } from '@lib/common';
import { useCompteRenduClient } from './utils/useCompteRenduClient';
import { ActionOperationnel } from '../../action-operationnel/pages';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import { CompteRenduFormValue } from '../components/interface';
import { useLaboratoireCorrespondants } from '../../../hooks/useLaboratoireCorrespondants';
import useDeepCompareEffect from 'use-deep-compare-effect';

const CompteRenduComp: FC<{}> = ({}): JSX.Element => {
  const { isMobile, user } = useApplicationContext();
  const [loadCorrespondants, responsablesCorrespondants] = useLaboratoireCorrespondants('COMPTERENDU_CORRESPONDANT');

  const [conditionSelected, setConditionSelected] = useState<PrtConditionInfoFragment[]>();
  const [planSelected, setPlanSelected] = useState<PrtPlanMarketingInfoFragment[]>();
  const [remiseSelected, setRemiseSelected] = useState<PrtPlanMarketingInfoFragment[]>();
  const [partenaireAssocie, setPartenaireAssocie] = useState<any>();
  const [userParticipants, setUserParticipants] = useState<any[]>([]);
  const [responsables, setResponsables] = useState<any[]>([]);
  const [valueToSave, setValueToSave] = useState<CompteRenduFormValue | undefined>();

  const { params, redirectTo } = useLaboratoireParams('compte-rendu');
  const { id: idCompteRendu, idLabo } = params;
  const {
    laboratoire,
    planMarketings,
    conditionCommerciales,
    compteRenduForm,
    onRequestGoBack,
    onRequestSave,
    saving,
    rendezVous,
    setSelectSuiviOperation,
    conditionCommercialeTypes,
    produits,
    compteRendu,
    savedCompteRendu,
    setSavedCompteRendu,
    remiseArrieres,
  } = useCompteRenduClient(idCompteRendu || null);

  useEffect(() => {
    if (laboratoire) {
      setPartenaireAssocie(laboratoire);
    }
  }, [laboratoire]);

  useEffect(() => {
    if (partenaireAssocie?.id) {
      loadCorrespondants(partenaireAssocie.id);
    }
  }, [partenaireAssocie?.id]);

  useEffect(() => {
    setResponsables(responsablesCorrespondants);
  }, [JSON.stringify(responsablesCorrespondants)]);

  useEffect(() => {
    setUserParticipants([user]);
  }, []);

  useEffect(() => {
    if (savedCompteRendu) {
      setValueToSave(undefined);
      setSavedCompteRendu(false);
    }
  }, [savedCompteRendu]);

  useEffect(() => {
    const data = compteRendu
      ? ({
          id: compteRendu.id,
          titre: compteRendu.titre,
          remiseEchantillon: compteRendu.remiseEchantillon,
          gestionPerime: compteRendu.gestionPerime as any,
          responsablesIds: (compteRendu.responsables || []).map((item: any) => item.id) as any[],
          collaborateurIds: (compteRendu.collaborateurs || []).map((item: any) => item.id),
          planMarketings: (compteRendu.planMarketings || []).filter(
            (p: PrtPlanMarketingInfoFragment) => !p.estRemiseArriere
          ),
          conditionsCommerciales: compteRendu.conditionsCommerciales,
          conclusion: compteRendu.conclusion as any,
          suiviOperationIds: (compteRendu.suiviOperationnels || []).map((el: any) => {
            return el.id;
          }),
          rapportVisite: compteRendu.rapportVisite,
          dateRendezVous: compteRendu.dateRendezVous,
          heureDebut: compteRendu.heureDebut,
          heureFin: compteRendu.heureFin,
          typeReunion: compteRendu.typeReunion,
          remiseArrieres: (compteRendu.planMarketings || []).filter(
            (p: PrtPlanMarketingInfoFragment) => p.estRemiseArriere
          ),
        } as any)
      : undefined;
    console.log('----------compte rendu edit: ', compteRendu);
    setValueToSave(data);
    setUserParticipants(compteRendu?.collaborateurs || []);
    setResponsables(compteRendu?.responsables || []);
  }, [compteRendu]);

  useEffect(() => {
    if (planMarketings?.data || remiseArrieres?.data) {
      const newPlanMarketings = (planMarketings?.data || []).map((element: any) => {
        return { ...element, idType: element.type.id };
      });
      const newRemiseArrieres = (remiseArrieres?.data || []).map((element: any) => {
        return { ...element, idType: element.type.id };
      });
      valueToSave?.planMarketings?.concat(newPlanMarketings);
      valueToSave?.remiseArrieres?.concat(newRemiseArrieres);
    }
    setPlanSelected && setPlanSelected(valueToSave?.planMarketings as any);
    setRemiseSelected(valueToSave?.remiseArrieres);
  }, [planMarketings?.data, valueToSave?.planMarketings]);

  useEffect(() => {
    if (conditionCommerciales?.data) {
      valueToSave?.conditionsCommerciales?.concat(conditionCommerciales.data);
    }
    setConditionSelected(valueToSave?.conditionsCommerciales);
  }, [conditionCommerciales?.data, valueToSave?.conditionsCommerciales]);

  // useEffect(() => {
  //   setValueToSave({
  //     ...valueToSave,
  //     planMarketings: planSelected,
  //     conditionsCommerciales: conditionSelected,
  //     remiseArrieres: remiseSelected,
  //   });
  // }, [planSelected, conditionSelected, remiseSelected]);

  useEffect(() => {
    setValueToSave((prev) => ({ ...prev, collaborateurIds: userParticipants?.map((user: any) => user?.id) || [] }));
  }, [userParticipants]);

  useEffect(() => {
    setValueToSave((prev) => ({ ...prev, responsablesIds: responsables?.map((user: any) => user?.id) || [] }));
  }, [responsables]);

  const handleReturn = (ids: string[]) => {
    setSelectSuiviOperation && setSelectSuiviOperation(ids);
  };

  const handleGoBack = () => {
    onRequestGoBack && onRequestGoBack();
  };

  return (
    <>
      {idLabo && idLabo[0] ? (
        <CompteRenduForm
          laboratoire={laboratoire}
          rendezVous={rendezVous as any}
          saving={saving}
          onReturn={handleReturn}
          suiviOperationnels={compteRendu?.suiviOperationnels}
          selectedSuiviOperationnelIds={compteRenduForm?.selectedSuiviOperationnelIds || []}
          onRequestSave={onRequestSave}
          planMarketingTypes={compteRenduForm?.planMarketingTypes || []}
          planMarketings={{
            ...planMarketings,
            data: [
              ...(planMarketings?.data || []),
              ...(compteRendu?.planMarketings || []).filter((p: PrtPlanMarketingInfoFragment) => !p.estRemiseArriere),
            ],
          }}
          remiseArrieres={{
            ...remiseArrieres,
            data: [
              ...(remiseArrieres?.data || []),
              ...(compteRendu?.planMarketings || []).filter((p: PrtPlanMarketingInfoFragment) => p.estRemiseArriere),
            ],
          }}
          userParticipants={userParticipants}
          setUserParticipants={setUserParticipants}
          responsables={responsables}
          setResponsables={setResponsables}
          value={valueToSave}
          setValueToSave={setValueToSave}
          produits={produits}
          setConditionSelected={setConditionSelected}
          setPlanSelected={setPlanSelected}
          planSelected={planSelected}
          conditionSelected={conditionSelected}
          onSideNavListClick={handleGoBack}
          partenaireAssocie={partenaireAssocie}
          conditionCommercialeTypes={conditionCommercialeTypes}
          conditionCommerciales={
            compteRendu
              ? {
                  ...conditionCommerciales,
                  data: [...(conditionCommerciales?.data || []), ...compteRendu.conditionsCommerciales],
                }
              : conditionCommerciales
          }
          remiseSelected={remiseSelected}
          setRemiseSelected={setRemiseSelected}
        />
      ) : (
        <>
          <CustomModal
            open={!idLabo || (idLabo && !idLabo[0])}
            setOpen={() => {}}
            title="Choix du labo"
            closeIcon
            headerWithBgColor
            maxWidth="sm"
            fullScreen={!!isMobile}
            actionButtonTitle="Choisir"
            onClickConfirm={() => redirectTo({ ...params, idLabo: [partenaireAssocie.id] }, 'compte-rendu-form')}
            customHandleClose={handleGoBack}
            disabledButton={!partenaireAssocie}
          >
            <Box display="flex" flexDirection="column">
              <Box mb={3}>
                <Typography>Veuillez choisir un labo avant d'ajouter un compte rendu</Typography>
              </Box>
              <PartenaireInput
                index="laboratoire"
                onChange={(value: any) => setPartenaireAssocie(value)}
                value={partenaireAssocie}
                label="labo"
              />
            </Box>
          </CustomModal>
        </>
      )}
    </>
  );
};

export default CompteRenduComp;
