import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      fontFamily: 'Roboto',
      width: '100%',
      padding: '0px 20px 24px 12px',
    },
    tableText: {
      fontWeight: 'normal',
    },
    gridSuiviMobile: {
      borderBottom: '1px solid #ccc',
    },
    textTitle: {
      color: '#424242',
      fontWeight: 'bold',
      textAlign: 'left',
      fontSize: 14,
    },
    textAction: {
      fontWeight: 'normal',
      color: theme.palette.primary.main,
    },
    topText: {
      padding: 16,
      fontFamily: 'Roboto',
      fontSize: 16,
      fontWeight: 500,
    },
    listPlanMobile: {
      background: '#f5f5f5',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginBottom: 10,
      position: 'relative',
    },
    typeAssocie: {
      display: 'flex',
      alignItems: 'center',
      fontSize: 14,
    },
    logoTypeAssocie: {
      width: 30,
      height: 30,
      marginRight: 10,
    },
    caption: {
      fontFamily: 'Roboto',
      color: '#9E9E9E',
      fontWeight: 'normal',
      fontSize: 12,
      display: 'flex',
      alignItems: 'center',
    },
    wrappedColumn: {
      whiteSpace: 'break-spaces !important' as any,
    },
    btnDetails: {
      position: 'absolute',
      top: '50%',
      transform: 'translateY(-50%)',
      right: 0,
    },
  })
);
