import React, { FC, memo, useCallback, useEffect, useRef, useState } from 'react';
import { useStyles } from './styles';
import { Clear, GetApp, Reply } from '@material-ui/icons';
import 'moment/locale/fr';
import moment from 'moment';
import { Avatar, Box, IconButton } from '@material-ui/core';
import { AvatarGroup } from '@material-ui/lab';
import { Column, NewCustomButton, Table } from '@lib/ui-kit';
import { drawDOM, exportPDF } from '@progress/kendo-drawing';
import { PDFExportProps, savePDF } from '@progress/kendo-react-pdf';
import '@progress/kendo-theme-default/dist/all.css';
import { CompteRendu } from '../interface';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import {
  useGet_List_PartenariatsLazyQuery,
  useGet_List_Suivi_Operationnel_TypesQuery,
} from '@lib/common/src/federation';
import { strippedString, useApplicationContext } from '@lib/common';

const conditionAndPlanColumn: Column[] = [
  {
    name: 'type',
    label: 'Type',
  },
  {
    name: 'titre',
    label: 'Titre',
  },
  {
    name: 'periode',
    label: 'Période',
  },
  {
    name: 'description',
    label: 'Déscription',
  },
];

const litigeColumn: Column[] = [
  {
    name: 'type',
    label: 'Type Litige',
  },
  {
    name: 'date',
    label: 'Date',
  },
  {
    name: 'titre',
    label: 'Titre',
  },
  {
    name: 'montant',
    label: 'Montant',
  },
  {
    name: 'statut',
    label: 'Statut',
  },
];

const suiviColumn: Column[] = [
  {
    name: 'type',
    label: 'Type',
  },
  {
    name: 'titre',
    label: 'Titre',
  },
  {
    name: 'participants',
    label: 'Collaborateurs',
  },
  {
    name: 'responsables',
    label: 'Représentant Labo',
  },
  {
    name: 'date',
    label: 'Date',
  },
];

const PageTemplate = (props: { pageNum: number; totalPages: number }) => (
  <span>
    Page {props.pageNum} sur {props.totalPages}
  </span>
);

export const makePdf = (html: any, fileName: string, option?: PDFExportProps) => {
  return savePDF(html, {
    paperSize: 'auto',
    fileName: `${fileName}.pdf`,
    margin: option?.margin || 3,
    pageTemplate: PageTemplate,
  });
};

const getBase64pdf = (element: any) => {
  return drawDOM(element, {
    paperSize: 'auto',
  })
    .then((group) => {
      return exportPDF(group);
    })
    .then((dataUri) => {
      return dataUri.split(';base64,')[1];
    });
};

export interface PharmacieInformation {
  nom: string;
  adresse: string;
  email?: string;
  telephone?: string;
}

export interface LaboratoireInformation {
  nom: string;
  adresse: string;
  email?: string;
  telephone?: string;
  representantLabo?: {
    nom: string;
    telephone?: string;
  };
}

export interface SendEmailInput {
  subject?: string;
  to: string[];
  from?: string;
  cc?: string;
  html?: string;
  templateId?: string;
  templateData?: any;
  attachment?: AttachmentInput;
  compteRendu: CompteRendu;
}

export interface AttachmentInput {
  content: string;
  filename: string;
  type: string;
}

export interface CompteRenduDetailProps {
  compteRenduValue?: CompteRendu;
  laboratoireInformation?: LaboratoireInformation;
  pharmaciInformation?: PharmacieInformation;
  dateProchainRendezVous?: Date;
  onRequestSendMail?: (email: SendEmailInput) => void;
  onSideNavListClick?: (route: string) => void;
  sendingEmail?: boolean;
  isEnvoiEmail?: boolean;
  loading?: boolean;
}
const keyTel = 'Tél';
const keyTel2 = 'Tél#';
const showVide = true;

const CompteRenduDetailComponnent: FC<CompteRenduDetailProps> = ({
  compteRenduValue,
  laboratoireInformation,
  pharmaciInformation,
  dateProchainRendezVous,
  onRequestSendMail,
  loading,
  sendingEmail,
}) => {
  const { redirectTo, goBack } = useLaboratoireParams('compte-rendu');
  const { federation } = useApplicationContext();

  const { params } = useLaboratoireParams('compte-rendu-detail');

  const classes = useStyles();
  const [pharmacieInformationValue, setPharmacieInformation] = useState<any | undefined>();
  const [laboInfo, setLaboInfo] = useState<any | undefined>();
  const [compteRendu, setCompteRendu] = useState<CompteRendu | undefined>();
  const [loadPartenariat, loadingPartenariat] = useGet_List_PartenariatsLazyQuery({ client: federation });

  const componentToPrint = useRef(null);

  useEffect(() => {
    moment.locale('fr');
    setCompteRendu(compteRenduValue);
    setLaboInfo({
      Laboratoire: laboratoireInformation?.nom,
      Adresse: laboratoireInformation?.adresse || '',
      Email: laboratoireInformation?.email || '',
      [keyTel]: laboratoireInformation?.telephone || '',
      'Représentant Labo':
        compteRendu &&
        compteRendu.responsables &&
        compteRendu.responsables.length > 0 &&
        compteRendu.responsables[0].nom,
      [keyTel2]:
        (compteRendu &&
          compteRendu.responsables &&
          compteRendu.responsables.length > 0 &&
          compteRendu.responsables[0]?.contact?.telPerso) ||
        compteRendu?.responsables[0]?.contact?.telProf ||
        '',
    });
    setPharmacieInformation({
      Adresse: pharmaciInformation?.adresse || '',
      Email: pharmaciInformation?.email || '',
      [keyTel]: pharmaciInformation?.telephone || '',
    });
    if (compteRendu) {
      loadPartenariat({
        variables: {
          paging: {
            limit: 1,
          },
          filter: {
            and: [
              {
                idPartenaireTypeAssocie: {
                  eq: compteRendu.partenaireTypeAssocie.id,
                },
              },
              {
                partenaireType: {
                  eq: 'LABORATOIRE',
                },
              },
              {
                idPharmacie: {
                  eq: compteRendu.idPharmacie,
                },
              },
            ],
          },
        },
      });
    }
  }, [laboratoireInformation, compteRendu, pharmaciInformation, compteRenduValue]);

  useEffect(() => {
    if (!loading && params.isAutoDownload) {
      setTimeout(() => {
        makePdf(componentToPrint.current, `compteRendu${moment(compteRendu?.createdAt).format('DD-MM-YYYY')}`);
        setTimeout(() => {
          redirectTo({ idLabo: params.idLabo }, 'compte-rendu');
        }, 1000);
      }, 2000);
    }
  }, [loading, componentToPrint, params.isAutoDownload]);

  const idLitigeTypes = (
    useGet_List_Suivi_Operationnel_TypesQuery({
      variables: {
        filter: {
          categorie: {
            eq: 'LITIGE',
          },
        },
      },
    }).data?.pRTSuiviOperationnelTypes?.nodes || []
  ).map(({ id }) => id);

  const idActionTypes = (
    useGet_List_Suivi_Operationnel_TypesQuery({
      variables: {
        filter: {
          categorie: {
            eq: 'ACTION',
          },
        },
      },
    }).data?.pRTSuiviOperationnelTypes?.nodes || []
  ).map(({ id }) => id);

  const handleDownloadClick = useCallback(() => {
    makePdf(componentToPrint.current, `compteRendu${moment(compteRendu?.createdAt).format('DD-MM-YYYY')}`);
  }, [componentToPrint, compteRendu]);

  const handleSendMail = useCallback(
    async (compteRendu) => {
      const base64pdf = await getBase64pdf(componentToPrint.current);
      onRequestSendMail &&
        onRequestSendMail({
          subject: 'Compte rendu du visite de laboratoire',
          to: [
            compteRendu.responsables[0]?.contact?.mailPerso || '',
            compteRendu.responsables[0]?.contact?.mailProf || '',
          ].filter((e) => e !== ''),
          html: compteRendu.rapportVisite,
          attachment: {
            type: 'application/pdf',
            content: base64pdf,
            filename: `compte-rendu-${moment(compteRendu.createdAt).format('DD-MM-YYYY')}`,
          },
          compteRendu,
        });
    },
    [compteRendu, componentToPrint]
  );

  const isEnvoiEmail = () => {
    const partenariat = loadingPartenariat.data?.pRTPartenariats.nodes;
    const compteRenduOption =
      (partenariat &&
        partenariat.length > 0 &&
        (partenariat[0].optionNotifications || [])?.find((el) => {
          return el.code === 'COMPTERENDU';
        })?.valeur) ||
      'NONE';
    if (compteRenduOption !== 'NONE' && compteRendu?.responsables && compteRendu?.responsables.length > 0) {
      return true;
    }
    return false;
  };

  const litiges = (compteRendu?.suiviOperationnels || []).filter((action) => idLitigeTypes.includes(action.type.id));
  const actions = (compteRendu?.suiviOperationnels || []).filter((action) => idActionTypes.includes(action.type.id));

  return (
    <div>
      <div style={!params.isAutoDownload ? { display: 'none' } : {}}>En cours de téléchargement</div>
      <div style={params.isAutoDownload ? { opacity: 0 } : {}}>
        <div className={classes.top}>
          <div>
            <h2 className={classes.titre3}>Détail Compte Rendu</h2>
          </div>
          <div onClick={() => goBack('compte-rendu')} style={{ cursor: 'pointer' }}>
            <Clear />
          </div>
        </div>
        <div className={classes.root}>
          <div className={classes.btnContainer}>
            <NewCustomButton style={{ marginRight: 24 }} onClick={handleDownloadClick} startIcon={<GetApp />}>
              TELECHARGER
            </NewCustomButton>
            {compteRendu && compteRendu.responsables && compteRendu.responsables.length > 0 && isEnvoiEmail() && (
              <NewCustomButton
                onClick={() => {
                  handleSendMail(compteRendu);
                }}
                startIcon={<Reply style={{ transform: 'scaleX(-1)' }} />}
              >
                ENVOYER PAR EMAIL
              </NewCustomButton>
            )}
          </div>
          <div className={classes.page}>
            <div className={classes.pdfContainer} ref={componentToPrint}>
              <h1>COMPTE RENDU DU VISITE DE LABORATOIRE</h1>
              {compteRenduValue?.typeReunion && compteRenduValue.dateRendezVous && (
                <h1>{`Rendez-Vous en ${compteRenduValue?.typeReunion} le ${moment(
                  compteRenduValue.dateRendezVous
                ).format('LL')}`}</h1>
              )}
              <div className={classes.headerPage}>
                <div className={classes.pharmacieInformation}>
                  <div className={classes.bordered}>
                    <h2>{pharmaciInformation?.nom}</h2>
                    {pharmacieInformationValue &&
                      Object.entries(pharmacieInformationValue).map(([label, value]) => {
                        return value || showVide ? (
                          <div key={label}>
                            <span className={classes.label}>{label}&nbsp;:</span>&nbsp;&nbsp;
                            <span className={classes.value}>{value as any}</span>
                          </div>
                        ) : (
                          ''
                        );
                      })}
                  </div>
                  <div className={classes.dateContainer}>Le {moment(compteRendu?.createdAt).format('LL')}</div>
                </div>
                <div className={classes.laboratoireInformation}>
                  {laboInfo &&
                    Object.entries(laboInfo).map(([label, value]) => {
                      return value || showVide ? (
                        <div key={label}>
                          <span className={classes.label}>{label.replace('#', '')}&nbsp;:</span>
                          &nbsp;&nbsp;
                          <span className={classes.value}>{value as any}</span>
                        </div>
                      ) : (
                        ''
                      );
                    })}
                  <br />
                  <div>
                    <span className={classes.label}>Prochain rendez-vous&nbsp;:</span>
                    &nbsp;&nbsp;
                    <span className={classes.value}>{moment(dateProchainRendezVous).format('LL')}</span>
                  </div>
                  {compteRenduValue?.dateRendezVous && (
                    <div>
                      <span className={classes.label}>RDV en présentiel&nbsp;:</span>
                      &nbsp;&nbsp;
                      <span className={classes.value}>{moment(compteRenduValue.dateRendezVous).format('LL')}</span>
                    </div>
                  )}
                </div>
              </div>
              <div className={classes.bodyContainer}>
                <div className={classes.containerInformation}>
                  <h3>Rapport de visite :</h3>
                  <span dangerouslySetInnerHTML={{ __html: compteRendu?.rapportVisite || '' }} />
                </div>

                <h3 className={classes.titre3}>1 - Litiges</h3>
                <div className={classes.ActionContainer}>
                  <Table
                    search={false}
                    columns={litigeColumn}
                    data={(litiges || []).map((item) => ({
                      type: <span className={classes.textInTable}>{item.type?.libelle}</span>,
                      date: <span className={classes.textInTable}>{moment(item.dateHeure).format('DD/MM/YYYY')}</span>,
                      titre: (
                        <span className={classes.textInTable}>
                          {item.description ? strippedString(item.description) : ''}
                        </span>
                      ),
                      montant: <span className={classes.textInTable}>{item.montant.toFixed(2)} €</span>,
                      statut: <span className={classes.textInTable}>{item.statut.libelle}</span>,
                    }))}
                    notablePagination
                  />
                </div>

                <h3 className={classes.titre3}>2 - Actions</h3>
                <div className={classes.ActionContainer}>
                  <Table
                    search={false}
                    columns={suiviColumn}
                    data={(actions || []).map((item) => ({
                      type: <span className={classes.textInTable}>{item.type?.libelle}</span>,
                      titre: (
                        <span className={classes.textInTable}>
                          {item.description ? strippedString(item.description) : ''}
                        </span>
                      ),
                      participants:
                        item.participants && item.participants.length > 0 ? (
                          <AvatarGroup max={4}>
                            {item.participants.map((user: any) => (
                              <Avatar
                                key={user.id}
                                src={user.photo.publicUrl || user.photo.avatar?.fichier?.publicUrl}
                                alt={user.id}
                              />
                            ))}
                          </AvatarGroup>
                        ) : (
                          ''
                        ),
                      responsables:
                        item.contacts && item.contacts.length > 0 ? (
                          <Box display="flex" alignItems="center">
                            {/* <Avatar src={item.contacts[0].photo?.publicUrl} /> */}
                            &nbsp;&nbsp;
                            <span>
                              {item.contacts[0].nom}&nbsp;{item.contacts[0].prenom}
                            </span>
                          </Box>
                        ) : (
                          ''
                        ),
                      date: <span className={classes.textInTable}>{moment(item.dateHeure).format('DD/MM/YYYY')}</span>,
                    }))}
                    notablePagination
                  />
                </div>
                <h3 className={classes.titre3}>3 - Accords</h3>
                <div className={classes.AccordContainer}>
                  <h4 className={classes.titre3}>Conditions commerciales</h4>
                  <Table
                    search={false}
                    columns={conditionAndPlanColumn}
                    data={(compteRendu?.conditionsCommerciales || []).map((item) => ({
                      type: <span className={classes.textInTable}>{item.type?.libelle || ''}</span>,
                      titre: <span className={classes.textInTable}>{item.titre || ''}</span>,
                      periode: (
                        <span className={classes.textInTable}>{`Du ${moment(item.dateDebut).format(
                          'DD/MM/YYYY'
                        )} au ${moment(item.dateFin).format('DD/MM/YYYY')}`}</span>
                      ),
                      description: <span dangerouslySetInnerHTML={{ __html: item?.description || '' }} />,
                    }))}
                    notablePagination
                    style={{ marginBottom: 32 }}
                  />
                  {[false, true].map((estRemiseArriere) => {
                    return (
                      <>
                        <h4 className={classes.titre3}>{estRemiseArriere ? 'Remise Arrière' : 'Trade Marketing'}</h4>
                        <Table
                          search={false}
                          columns={conditionAndPlanColumn}
                          data={(compteRendu?.planMarketings || [])
                            .filter((pm) => pm.estRemiseArriere === estRemiseArriere)
                            .map((item) => ({
                              type: <span className={classes.textInTable}>{item.type?.libelle || ''}</span>,
                              titre: <span className={classes.textInTable}>{item.titre || ''}</span>,
                              periode: (
                                <span className={classes.textInTable}>{`Du ${moment(item.dateDebut).format(
                                  'DD/MM/YYYY'
                                )} au ${moment(item.dateFin).format('DD/MM/YYYY')}`}</span>
                              ),
                              description: <span dangerouslySetInnerHTML={{ __html: item?.description || '' }} />,
                            }))}
                          notablePagination
                          style={!estRemiseArriere ? { marginBottom: 32 } : undefined}
                        />
                      </>
                    );
                  })}
                </div>
                <div className={classes.containerInformation}>
                  <h3>Nouveauté :</h3>
                  <span>Remise d'échantillon : {compteRendu?.remiseEchantillon ? 'Oui' : 'Non'}</span>
                </div>
                <div className={classes.containerInformation}>
                  <h3>Gestion des périmés :</h3>
                  <span dangerouslySetInnerHTML={{ __html: compteRendu?.gestionPerime || '' }} />
                </div>
                <div className={classes.containerInformation}>
                  <h3>Commentaire :</h3>
                  <span dangerouslySetInnerHTML={{ __html: compteRendu?.conclusion || '' }} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export const CompteRenduDetail = memo(CompteRenduDetailComponnent);
