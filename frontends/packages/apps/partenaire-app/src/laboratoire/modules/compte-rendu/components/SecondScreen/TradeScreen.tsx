import { useApplicationContext } from '@lib/common';
import { PrtPlanMarketingInfoFragment } from '@lib/common/src/federation';
import { ConfirmDeleteDialog, CustomModal, NewCustomButton, Table } from '@lib/ui-kit';
import { useWindowDimensions } from '@lib/ui-kit/src/components/atoms/MobileTopBar/MobileTopBar';
import { Box, IconButton, Typography } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import moment from 'moment';
import React, { FC, useCallback, useEffect, useState } from 'react';
import { useLaboratoireParams } from './../../../../hooks/useLaboratoireParams';
import PlanForm from './../../../trade-marketing/components/plan-form/PlanForm';
import { PlanFormRequestSavingProps } from './../../../trade-marketing/components/plan-form/PlanFormProps';
import { useStyles } from './styles';
import { SecondFormulaireCompteRenduProps } from './types';
import { useColumnPlanMarketingAccord } from './useColumnPlanMarketing';
interface Form {
  open: boolean;
  mode: 'ajout' | 'modification';
  saved: boolean;
}

export const TradeCompteRendu: FC<SecondFormulaireCompteRenduProps> = ({
  valueToSave,
  setValueToSave,
  planMarketingTypes,
  produits,
  planMarketings,
  onRequestSelectedPlan,
  planSelected,
  setPlanSelected,
  partenaireAssocie,
}) => {
  const styles = useStyles();
  const dimension = useWindowDimensions();
  const {
    params: { id },
  } = useLaboratoireParams();
  const { isMobile } = useApplicationContext();
  const [planMarketing, setPlanMarketing] = useState<PrtPlanMarketingInfoFragment | undefined>();
  const [anchor, setAnchor] = useState<any>();
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const [planMarketingForm, setPlanMarketingForm] = useState<Form>({
    open: false,
    mode: 'ajout',
    saved: false,
  });

  useEffect(() => {
    if (!planMarketingForm.open) {
      setPlanMarketing(undefined);
    }
  }, [planMarketingForm.open]);

  const handleClickMenuPlanMarketing = (e: any, row: PrtPlanMarketingInfoFragment | undefined) => {
    setAnchor(e.currentTarget);
    setPlanMarketing(row);
  };

  const handleSelectChangePlan = (dataSelected: any) => {
    setPlanSelected && setPlanSelected(dataSelected);
    onRequestSelectedPlan && onRequestSelectedPlan(dataSelected);
  };

  const handleSelectAllPlan = () => {
    setPlanSelected && setPlanSelected(valueToSave?.planMarketings as any);
  };

  const handleClearSelectionPlan = () => {
    setPlanSelected && setPlanSelected([]);
  };

  const handleEdit = (row: PrtPlanMarketingInfoFragment) => {
    setPlanMarketingForm({
      open: true,
      mode: 'modification',
      saved: false,
    });
    setAnchor(null);
  };

  const handleDelete = (row: PrtPlanMarketingInfoFragment) => {
    setOpenDeleteDialog(true);
    setAnchor(null);
  };

  const handleCloseMenu = () => {
    setAnchor(null);
  };

  const handleConfirmDelete = () => {
    planMarketings?.onRequestDeleteTrade(planMarketing as any);
    setOpenDeleteDialog(false);
  };

  const columnsPlanMarketing = useColumnPlanMarketingAccord({
    onMenuClick: handleClickMenuPlanMarketing,
    onEditClick: handleEdit,
    onDeleteClick: handleDelete,
    anchor: anchor,
    planMarketing,
    onCloseMenu: handleCloseMenu,
  });

  const handleOpenFormPlanMarketing = () => {
    setPlanMarketingForm((prev) => ({ ...prev, open: !prev.open }));
  };
  const handleSavePlanMarketing = useCallback(
    (data: PlanFormRequestSavingProps, mode?: 'ajout' | 'modification') => {
      const _planMarketing: PrtPlanMarketingInfoFragment = {
        id: data.id as any,
        titre: data.titre,
        fichiers: data.selectedFiles,
        dateDebut: data.dateDebut,
        dateFin: data.dateFin,
        description: data.description,
        type: {
          id: data.idType,
        },
        idType: data.idType,
        montant: data.montant ? parseFloat(data.montant) : null,
        remise: data.remise ? parseFloat(data.remise) : null,
        montantStatutRealise: null,
        idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
        typeRemuneration: data.typeRemuneration,
        estRemiseArriere: false,
        produits: (data.produits || []).filter((canalProduit: any) => canalProduit.produit?.remisePrevue || 0 > 0),
      } as any;
      planMarketings?.onSavePlan(_planMarketing);

      if (mode === 'ajout') {
        setValueToSave({
          ...valueToSave,
          planMarketings: [...(valueToSave?.planMarketings || []), _planMarketing],
        });
      } else {
        if (planMarketing && valueToSave && valueToSave.planMarketings) {
          const keyFinding = (valueToSave.planMarketings || []).indexOf(planMarketing);
          if (keyFinding >= 0) {
            setValueToSave({
              ...valueToSave,
              planMarketings: valueToSave.planMarketings.map((item, index) => {
                if (index === keyFinding) {
                  return _planMarketing;
                }
                return item;
              }),
            });
            setAnchor(null);
          }
        }
      }

      setPlanMarketingForm((prev) => ({ ...prev, open: false, mode: 'ajout', saved: false }));
    },
    [valueToSave, planMarketing, planMarketingTypes]
  );

  // const handleSelectChangePlan = (dataSelected: any) => {
  //   setPlanSelected && setPlanSelected(dataSelected);
  //   onRequestSelectedPlan && onRequestSelectedPlan(dataSelected);
  // };

  return (
    <Box className={styles.root}>
      <Box className={styles.container}>
        <Box className={styles.tableConditionCommercialeContainer}>
          <Box className={styles.headerTable}>
            {isMobile ? (
              <IconButton style={{ float: 'right', padding: 0 }} onClick={handleOpenFormPlanMarketing}>
                <Add />
              </IconButton>
            ) : (
              ''
            )}
            <h2 className={styles.title}>Trade Marketing</h2>
            {dimension !== 'isSm' && (
              <NewCustomButton
                theme="flatPrimary"
                variant="text"
                onClick={handleOpenFormPlanMarketing}
                startIcon={<Add />}
              >
                AJOUTER UNE OPÉRATION MARKETING
              </NewCustomButton>
            )}
          </Box>

          {isMobile ? (
            <>
              {planMarketings?.data.map((item, index) => {
                return (
                  <div key={index}>
                    <Typography style={{ fontWeight: 600 }}>{item.titre} </Typography>
                    <Typography variant="caption">
                      {`Du ${moment(item.dateDebut).format('DD/MM/YYYY')} - Au ${moment(item.dateFin).format(
                        'DD/MM/YYYY'
                      )}`}{' '}
                      <FiberManualRecordIcon style={{ width: 7, height: 7 }} /> <span>{item.type.libelle} </span>
                    </Typography>
                  </div>
                );
              })}
            </>
          ) : (
            <>
              <Table
                search={false}
                noTableToolbarSearch
                columns={columnsPlanMarketing}
                data={planMarketings?.data || []}
                error={planMarketings?.error as any}
                loading={planMarketings?.loading}
                selectable={id ? true : false}
                onClearSelection={handleClearSelectionPlan}
                onSelectAll={handleSelectAllPlan}
                rowsSelected={planSelected}
                onRowsSelectionChange={handleSelectChangePlan}
                dataIdName="id"
              />
              {dimension === 'isSm' && (
                <Box width={1} display="flex" justifyContent="flex-end" marginTop={2}>
                  <NewCustomButton
                    theme="flatPrimary"
                    variant="text"
                    onClick={handleOpenFormPlanMarketing}
                    startIcon={<Add />}
                  >
                    AJOUTER UNE OPÉRATION MARKETING
                  </NewCustomButton>
                </Box>
              )}
            </>
          )}
        </Box>
      </Box>
      <CustomModal
        open={planMarketingForm.open}
        setOpen={handleOpenFormPlanMarketing}
        title={
          planMarketingForm.mode === 'modification' ? 'Modifier opération marketing' : 'Nouvelle opération marketing'
        }
        closeIcon
        headerWithBgColor
        maxWidth="md"
        fullScreen={!!isMobile}
        withBtnsActions={false}
      >
        <PlanForm
          withTitle={false}
          mode={planMarketingForm.mode}
          open={planMarketingForm.open}
          setOpen={handleOpenFormPlanMarketing}
          inModalForm
          formData={planMarketing ? planMarketing : undefined}
          onRequestSave={handleSavePlanMarketing}
          listTypePlan={{
            error: undefined,
            loading: false,
            data: planMarketingTypes as any,
          }}
          produits={produits}
          saved={planMarketingForm.saved}
          saving={false}
          partenaireAssocie={partenaireAssocie}
        />
      </CustomModal>
      <ConfirmDeleteDialog
        title="Suppression"
        content="Etes-vous sûr de vouloir supprimer cette trade marketing ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDelete}
      />
    </Box>
  );
};
