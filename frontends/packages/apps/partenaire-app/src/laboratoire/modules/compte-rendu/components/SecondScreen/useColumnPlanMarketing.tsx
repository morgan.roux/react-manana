import { Box, IconButton, Menu, MenuItem, Typography } from '@material-ui/core';
import MoreHoriz from '@material-ui/icons/MoreHoriz';
import moment from 'moment';
import { Column } from '@lib/ui-kit';
import React from 'react';
import { PrtPlanMarketingInfoFragment } from '@lib/common/src/federation';
import { Delete, Edit } from '@material-ui/icons';

export interface ColumnPlanMarketingAccordProps {
  onMenuClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>, row: PrtPlanMarketingInfoFragment) => void;
  onCloseMenu: () => void;
  onEditClick: (row: PrtPlanMarketingInfoFragment) => void;
  onDeleteClick: (row: PrtPlanMarketingInfoFragment) => void;
  planMarketing?: PrtPlanMarketingInfoFragment;
  anchor: any;
}

export const useColumnPlanMarketingAccord = ({
  onMenuClick,
  onCloseMenu,
  onEditClick,
  onDeleteClick,
  planMarketing,
  anchor,
}: ColumnPlanMarketingAccordProps): Column[] => {
  return [
    {
      name: 'type',
      label: 'Type',
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        return row.type.libelle;
      },
    },
    {
      name: 'titre',
      label: 'Titre',
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        return row.titre;
      },
    },
    {
      name: 'periode',
      label: 'Période',
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        return `Du ${moment.utc(row.dateDebut).format('DD/MM/YYYY')} - Au ${moment
          .utc(row.dateFin)
          .format('DD/MM/YYYY')}`;
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        return (
          <Box key={`row-${row.id}`}>
            <IconButton
              onClick={(e) => {
                e.stopPropagation();
                onMenuClick(e, row);
              }}
            >
              <MoreHoriz />
            </IconButton>
            <Menu
              id="simple-menu"
              anchorEl={anchor}
              keepMounted
              open={Boolean(anchor) && row.id === planMarketing?.id}
              onClose={onCloseMenu}
            >
              <MenuItem
                onClick={(e) => {
                  e.stopPropagation();
                  onEditClick(row);
                }}
              >
                <Edit />
                &nbsp;
                <Typography variant="inherit">Modifier</Typography>
              </MenuItem>
              <MenuItem
                onClick={(e) => {
                  e.stopPropagation();
                  onDeleteClick(row);
                }}
              >
                <Delete />
                &nbsp;
                <Typography variant="inherit">Suprimer</Typography>
              </MenuItem>
            </Menu>
          </Box>
        );
      },
    },
  ];
};
