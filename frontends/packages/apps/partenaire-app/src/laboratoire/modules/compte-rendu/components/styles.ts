import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: 'calc(100vh - 155px)',
      overflowY: 'auto',
    },
    add: {
      position: 'absolute',
      right: 24,
      bottom: 24,
      zIndex: 999,
    },
    caption: {
      fontFamily: 'Roboto',
      color: '#9E9E9E',
      fontWeight: 'normal',
      fontSize: 12,
    },
    iconButton: {
      padding: 0,
    },
    formWidth: {
      [theme.breakpoints.down('md')]: {
        maxWidth: '100vw',
      },
      marginTop: 32,
    },
    formHeight: {
      [theme.breakpoints.down('md')]: {
        maxHeight: '100vh',
      },
    },
    scroll: {
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
    tableBox: {
      marginLeft: 24,
      marginRight: 24,
    },
    textTable: {
      fontFamily: 'Roboto',
      color: '#212121',
      fontWeight: 'normal',
      textAlign: 'left',
      fontSize: 14,
    },
    textTitle: {
      fontFamily: 'Roboto',
      color: '#424242',
      fontWeight: 'bold',
      textAlign: 'left',
      fontSize: 14,
    },
    textHead: {
      fontFamily: 'Roboto',
      fontSize: 16,
      marginTop: 16,
      marginBottom: 16,
    },
    titreComponent: {
      fontFamily: 'Roboto',
      color: '#424242',
      fontWeight: 'bold',
      textAlign: 'left',
      fontSize: 22,
    },
    topBarBox: {
      paddingBottom: 16,
    },
    btnEditPartenariatContainer: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      marginTop: 32,
      [theme.breakpoints.up('md')]: {
        justifyContent: 'flex-end',
      },
    },
    btnWhite: {
      marginBottom: 8,
      padding: 12,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      background: theme.palette.common.white,
      color: theme.palette.secondary.light,
      textDecoration: 'none',
      borderRadius: 4,
      cursor: 'pointer',
      border: `1px solid ${theme.palette.secondary.light}`,
      fontSize: 14,
      '&:hover': {
        background: theme.palette.grey[200],
      },
    },
    noteBox: {
      display: 'flex',
      alignItems: 'center',
      backgroundColor: '#fffaed',
      color: '#fbb104',
    },
    subtitle: {
      fontFamily: 'Roboto',
      fontSize: 18,
      fontWeight: '500' as any,
      color: '#212121',
      textAlign: 'left',
    },
    datePeriode: {
      display: 'flex',
      alignItems: 'center',
    },
    field: {
      minWidth: 100,
    },
    nouveauCR: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    titreCR: {
      marginRight: '16px',
      fontSize: '14px',
      fontWeight: 600,
      textTransform: 'capitalize',
    },
    titrePartenaire: {
      display: 'flex',
      alignItems: 'center',
      background: '#f2f2f2',
      borderRadius: '4px',
    },
    small: {
      width: '24px',
      height: '24px',
      marginLeft: '8px',
    },
    containerForm: {
      width: '75%',
      marginBottom: 24,
      [theme.breakpoints.up('sm')]: {
        minWidth: 700,
      },
      [theme.breakpoints.down('sm')]: {
        width: '100%',
      },
    },
    editorText: {
      minHeight: 92,
    },
  })
);
