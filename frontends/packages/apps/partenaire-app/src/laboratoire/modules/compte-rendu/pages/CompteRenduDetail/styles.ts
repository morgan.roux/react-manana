import { makeStyles, createStyles, Theme } from '@material-ui/core';
// import './../../../../../../../services/embeded-fonts/style.css';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      padding: '24px',
      [theme.breakpoints.up('sm')]: {
        padding: '24px 10%',
      },
      background: theme.palette.grey[100],
      fontFamily: 'Roboto',
    },
    btnContainer: {
      width: '100%',
      marginBottom: 24,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    pdfContainer: {
      width: '100%',
      padding: 32,
      fontFamily: 'DejaVu Sans',
      background: theme.palette.common.white,
      '& h1': {
        textAlign: 'center',
        margin: '0 0 32px 0',
        fontSize: 16,
      },
    },
    page: {
      overflowY: 'scroll',
      overflowX: 'auto',
      height: 'calc(100vh - 200px)',
      width: 1000,
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
      fontFamily: 'DejaVu Sans',
    },
    headerPage: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
      marginBottom: 32,
    },
    pharmacieInformation: {
      width: 350,
      '& h2': {
        fontSize: 16,
        margin: 0,
      },
    },
    bordered: {
      border: `2px solid ${theme.palette.grey[400]}`,
      padding: 16,
      width: '100%',
      '& div': {
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%',
        marginTop: 16,
      },
    },
    textInTable: {
      fontFamily: 'DejaVu Sans',
      fontSize: 13,
      fontWeight: 'normal',
    },
    laboratoireInformation: {
      width: 350,
      '& div': {
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%',
        marginBottom: 16,
      },
    },
    label: {
      fontSize: 16,
      color: theme.palette.grey[400],
    },
    value: {
      fontSize: 16,
      fontWeight: 500 as any,
    },
    dateContainer: {
      fontSize: 16,
      fontWeight: 500 as any,
      marginTop: 24,
    },
    bodyContainer: {
      width: '100%',
    },
    containerInformation: {
      marginBottom: 32,
      padding: 16,
      background: theme.palette.grey[100],
      borderRadius: 4,
      '& h3': {
        fontSize: 16,
        fontWeight: 500 as any,
        margin: '0 0 16px 0',
      },
      '& span': {
        fontSize: 14,
      },
    },
    titre3: {
      fontSize: 16,
      fontWeight: 'bold',
      margin: '0 0 24px 0',
    },
    ActionContainer: {
      width: '100%',
      marginBottom: 32,
    },
    AccordContainer: {
      width: '100%',
      marginBottom: 32,
      paddingLeft: 32,
      '& h4': {
        fontSize: 14,
        fontWeight: 500 as any,
        margin: '0 0 24px 0',
      },
    },
    top: {
      display: 'flex',
      alignContent: 'center',
      justifyContent: 'space-between',
      width: '100%',
      padding: 32,
      paddingBottom: 8,
    },
  })
);
