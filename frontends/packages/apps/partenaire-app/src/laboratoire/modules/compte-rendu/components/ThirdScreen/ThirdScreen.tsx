import { Box, Checkbox, FormControl, FormControlLabel, Grid, IconButton } from '@material-ui/core';
import React, { ChangeEvent, Dispatch, FC, ReactNode, useCallback, useEffect, useState } from 'react';
import { useStyles } from './styles';
import { CustomEditorText, CustomButton, NewCustomButton } from '@lib/ui-kit';
import { Edit, Update } from '@material-ui/icons';
import classNames from 'classnames';
import moment from 'moment';
import { CompteRenduFormValue } from '../interface';
import { LaboratoireInfoFragment, PrtRendezVousInfoFragment } from '@lib/common/src/federation';
import { RendezVousFormOutput } from 'src/laboratoire/modules/rendezVous/pages/rendez-vous-form/interface';
import RendezVousForm from './../../../rendezVous/pages/rendez-vous-form';
import UserInput from '@lib/common/src/components/UserInput';
import RdvDateHeureForm from '../../../../components/RdvDateHeureForm';
export interface ThirdFormulaireCompteRenduProps {
  laboratoire?: LaboratoireInfoFragment;
  setValueToSave?: React.Dispatch<React.SetStateAction<CompteRenduFormValue | undefined>>;
  valueToSave?: CompteRenduFormValue;
  rendezVous?: {
    onRequestSave: (rdv: RendezVousFormOutput) => void;
    rendezVous?: PrtRendezVousInfoFragment;
    saving: {
      loading?: boolean;
      error?: Error;
      success?: boolean;
    };
    selectedInvites: any[];
    selectedCollaborateurs: any[];
    collaborateurPiker: ReactNode;
    changeSuccess: () => void;
    setSelectedParticipants?: (selecteds: any[]) => void;
    setSelectedInvites?: (selecteds: any[]) => void;
  };

  setUserParticipants?: Dispatch<any>;
  userParticipants?: any;
  responsables?: any;
  setResponsables?: Dispatch<any>;
  imprimable: boolean;
  setIsImprimable: (imprimable: boolean) => void;
}

interface FormRdv {
  open: boolean;
  mode: 'creation' | 'modification' | 'report';
}

export const ThirdFormulaireCompteRendu: FC<ThirdFormulaireCompteRenduProps> = ({
  laboratoire,
  setValueToSave,
  valueToSave,
  rendezVous,
  userParticipants,
  setUserParticipants,
  responsables,
  setResponsables,
  imprimable,
  setIsImprimable,
}) => {
  const classes = useStyles();
  const [formRdv, setFormRdv] = useState<FormRdv>({
    open: false,
    mode: 'creation',
  });
  const [rdvToEdit, setRdvToEdit] = useState<PrtRendezVousInfoFragment | undefined>();
  const [success, setSuccess] = useState<boolean>(false);
  const [openModalCollaborateur, setOpenModalCollaborateur] = useState<boolean>(false);
  const [openModalResponsables, setOpenModalResponsables] = useState<boolean>(false);

  useEffect(() => {
    setRdvToEdit(rendezVous?.rendezVous);
  }, [rendezVous?.rendezVous]);

  useEffect(() => {
    if (rendezVous?.saving.success) {
      setSuccess(true);
      rendezVous?.changeSuccess && rendezVous.changeSuccess();
    }
  }, [rendezVous?.saving.success]);

  const handleOpenFormRdv = () => {
    setFormRdv((prev) => ({ ...prev, open: !prev.open }));
    setSuccess(false);
  };

  const handlePlanifierRdv = () => {
    setFormRdv((prev) => ({ ...prev, open: true, mode: 'creation' }));
  };

  const handleReporterRdv = () => {
    if (rdvToEdit) {
      setFormRdv((prev) => ({ ...prev, open: true, mode: 'report' }));
    }
  };

  const handleChangeConclusion = useCallback(
    (value: string) => {
      setValueToSave && setValueToSave({ ...valueToSave, conclusion: value });
    },
    [valueToSave]
  );

  const handleChangeImprimable = (event: ChangeEvent<HTMLInputElement>) => {
    setIsImprimable(event.target.checked);
  };

  return (
    <Box className={classes.root}>
      <Box className={classes.containerForm}>
        <h2>Information générale</h2>
        <Grid container spacing={2} className={classes.container}>
          <Grid sm={12} xs={12} md={6} lg={6} item>
            <UserInput
              className={classes.userInputCompteRendu}
              openModal={openModalCollaborateur}
              withNotAssigned={false}
              setOpenModal={setOpenModalCollaborateur}
              selected={userParticipants}
              setSelected={setUserParticipants}
              label="Collaborateur*"
              key="collaborateur"
              defaultValueMe={true}
            />
          </Grid>
          <Grid sm={12} xs={12} md={6} lg={6} item>
            <UserInput
              className={classes.userInputCompteRendu}
              openModal={openModalResponsables}
              // idPartenaireTypeAssocie={idLabo}
              withAssignTeam={false}
              withNotAssigned={false}
              partenaireType="LABORATOIRE"
              category="CONTACT"
              setOpenModal={setOpenModalResponsables}
              selected={responsables}
              setSelected={setResponsables}
              label="Représentant labo*"
              title="Responsables"
              idPartenaireTypeAssocie={laboratoire?.id}
              key="responsables"
            />
          </Grid>
        </Grid>
        <RdvDateHeureForm
          values={
            valueToSave
              ? {
                  dateRendezVous: valueToSave?.dateRendezVous || new Date(),
                  heureDebut: valueToSave?.heureDebut,
                  heureFin: valueToSave?.heureFin,
                  typeReunion: valueToSave?.typeReunion || 'VISIO',
                }
              : undefined
          }
          setValues={(values) => setValueToSave && setValueToSave((prev) => ({ ...prev, ...values }))}
        />
        <Box className={classes.container}>
          {rendezVous && (
            <>
              <Box className={classes.rendezVousContainer}>
                <span>Prochain Rendez-Vous :</span>&nbsp;&nbsp;
                {rendezVous?.rendezVous?.dateRendezVous ? (
                  <>
                    <span>{`Rendez-Vous en ${rendezVous?.rendezVous?.typeReunion} le ${moment(
                      rendezVous?.rendezVous.dateRendezVous
                    ).format('LL')} à partir de ${rendezVous?.rendezVous?.heureDebut.slice(0, -3)} ${
                      rendezVous?.rendezVous?.heureFin ? `jusqu'à ${rendezVous?.rendezVous?.heureFin.slice(0, -3)}` : ''
                    }`}</span>
                    &nbsp;
                    <IconButton onClick={handleReporterRdv}>
                      <Edit />
                    </IconButton>
                  </>
                ) : (
                  <span>N/A</span>
                )}
              </Box>
              <NewCustomButton
                onClick={handlePlanifierRdv}
                variant="outlined"
                theme="flatPrimary"
                startIcon={<Update />}
                style={{ padding: 12 }}
              >
                NOUVEAU RENDEZ-VOUS
              </NewCustomButton>
            </>
          )}
        </Box>
        <Box className={classNames(classes.container, classes.editorTextContainer)}>
          <CustomEditorText
            className={classes.editorText}
            placeholder="Conclusion"
            value={valueToSave?.conclusion || ''}
            onChange={handleChangeConclusion}
          />
        </Box>
        <Box className={classes.container}>
          <FormControl component="fieldset">
            <FormControlLabel
              control={<Checkbox checked={imprimable} onChange={handleChangeImprimable} name="imprimer" />}
              label="Imprimer"
            />
          </FormControl>
        </Box>
      </Box>
      {formRdv.open && (
        <RendezVousForm
          open={formRdv.open}
          setOpen={handleOpenFormRdv}
          onRequestSave={rendezVous?.onRequestSave}
          saving={rendezVous?.saving.loading}
          mode={formRdv.mode}
          saved={success}
          selectedInvites={rendezVous?.selectedInvites || []}
          selectedParticipants={rendezVous?.selectedCollaborateurs || []}
          rendezVousEdit={
            formRdv.mode === 'report' ? ({ data: rdvToEdit, loading: false, error: undefined } as any) : undefined
          }
          setSelectedInvites={rendezVous?.setSelectedInvites}
          setSelectedParticipants={rendezVous?.setSelectedParticipants}
          laboratoire={laboratoire as any}
          isRedirect={false}
        />
      )}
    </Box>
  );
};
