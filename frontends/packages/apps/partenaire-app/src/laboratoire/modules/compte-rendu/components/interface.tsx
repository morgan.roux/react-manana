import { Dispatch, ReactNode } from 'react';
import { TableChange } from '@lib/ui-kit';
import {
  LaboratoireInfoFragment,
  PrtPlanMarketingInfoFragment,
  PrtRendezVousInfoFragment,
  PrtConditionInfoFragment,
  ConditionTypeInfoFragment,
  PrtSuiviOperationnelInfoFragment,
  PrtCompteRenduInfoFragment,
} from '@lib/common/src/federation';
import { RendezVousFormOutput } from '../../rendezVous/pages/rendez-vous-form/interface';
import { LaboratoireInformation, PharmacieInformation } from '../pages/CompteRenduDetail/CompteRenduDetail';

export interface PlanMarketingType {
  id: string;
  libelle: string;
  code: string;
}
export interface CompteRenduFormProps {
  laboratoire?: LaboratoireInfoFragment;
  onChangeStep?: (step: number) => void;
  suiviComponent?: ReactNode;
  onSideNavListClick?: (item: string) => void;
  onRequestSave?: (data: CompteRenduFormOuptput) => void;
  selectedSuiviOperationnelIds?: string[];
  saving?: {
    error?: Error;
    success?: boolean;
    loading?: boolean;
  };
  value?: CompteRenduFormValue;
  setValueToSave: (value: any | undefined) => void;
  onReturn?: (selectedResponsablesIds: string[]) => void;
  conditionCommercialeTypes: ConditionTypeInfoFragment[];
  planMarketingTypes: PlanMarketingType[];
  rendezVous?: {
    onRequestSave: (rdv: RendezVousFormOutput) => void;
    rendezVous: PrtRendezVousInfoFragment;
    saving: {
      loading?: boolean;
      error?: Error;
      success?: boolean;
    };
    collaborateurPiker: ReactNode;
    selectedInvites: any[];
    selectedCollaborateurs: any[];
    changeSuccess: () => void;
    setSelectedParticipants?: (selecteds: any[]) => void;
    setSelectedInvites?: (selecteds: any[]) => void;
  };
  produits?: {
    error?: Error;
    loading?: boolean;
    data?: any;
    rowsTotal?: number;
    onRequestSearch?: (change?: TableChange | null, ids?: string[]) => void;
    planMarketingProduits?: any;
  };
  planMarketings?: {
    onSavePlan: (data: PrtPlanMarketingInfoFragment) => void;
    saving: {
      loading?: boolean;
      error?: Error;
      success?: boolean;
    };
    data: PrtPlanMarketingInfoFragment[];
    loading?: boolean;
    error?: boolean;
    onRequestDeleteTrade: (trade: PrtPlanMarketingInfoFragment) => void;
  };
  remiseArrieres?: {
    onSavePlan: (data: PrtPlanMarketingInfoFragment) => void;
    saving: {
      loading?: boolean;
      error?: Error;
      success?: boolean;
    };
    data: PrtPlanMarketingInfoFragment[];
    loading?: boolean;
    error?: boolean;
    onRequestDeleteRemiseArriere: (trade: PrtPlanMarketingInfoFragment) => void;
  };
  conditionCommerciales?: {
    onSaveCondition: (data: PrtConditionInfoFragment) => void;
    saving: {
      loading?: boolean;
      error?: Error;
      success?: boolean;
    };
    data: PrtConditionInfoFragment[];
    loading?: boolean;
    error?: boolean;
    onRequestDeleteCondition: (condition: PrtConditionInfoFragment) => void;
  };
  setPlanSelected?: (plan: PrtPlanMarketingInfoFragment[]) => void;
  planSelected?: PrtPlanMarketingInfoFragment[];
  setConditionSelected?: (condition: PrtConditionInfoFragment[]) => void;
  conditionSelected?: PrtConditionInfoFragment[];
  partenaireAssocie?: any;

  setUserParticipants?: Dispatch<any>;
  userParticipants?: any;
  responsables?: any;
  setResponsables?: Dispatch<any>;
  suiviOperationnels?: any;
  setRemiseSelected?: (plan: PrtPlanMarketingInfoFragment[]) => void;
  remiseSelected?: PrtPlanMarketingInfoFragment[];
}

export interface CompteRenduFormValue {
  id?: string;
  titre?: string;
  remiseEchantillon?: boolean;
  gestionPerime?: string;
  conclusion?: string;
  rapportVisite?: string;
  conditionsCommerciales?: PrtConditionInfoFragment[];
  planMarketings?: PrtPlanMarketingInfoFragment[];
  responsablesIds?: string[];
  collaborateurIds?: string[];
  suiviOperationIds?: string[];
  dateRendezVous?: any;
  heureDebut?: string;
  heureFin?: string;
  typeReunion?: string;
  remiseArrieres?: PrtPlanMarketingInfoFragment[];
}

export interface ConditionCommercialeInput {
  idType: string;
  idStatut?: string;
  titre: string;
  dateDebut: Date;
  dateFin: Date;
  description?: string;
  fichiers: any[];
}

export interface PlanMarketingInput {
  idType: string;
  titre: string;
  description: string;
  dateDebut: Date;
  dateFin: Date;
  idStatut?: string;
  fichiers: any[];
  idMiseAvants: string[];
}
export interface CompteRenduFormOuptput {
  id?: string;
  idUserParticipants: string[];
  titre: string;
  remiseEchantillon: boolean;
  idResponsables: string[];
  idSuiviOperationnels: string[];
  conditionCommerciales: ConditionCommercialeInput[];
  planMarketings: PlanMarketingInput[];
  conclusion?: string;
  rapportVisite?: string;
  gestionPerime?: string;
  idPartenaireTypeAssocie: string;
  dateRendezVous?: any;
  heureDebut?: string;
  heureFin?: string;
  typeReunion?: string;
  isImprimable?: boolean;
}
