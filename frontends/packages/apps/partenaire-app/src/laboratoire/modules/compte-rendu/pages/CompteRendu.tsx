import React, { useState, MouseEvent, FC, useEffect } from 'react';
import {
  Table,
  TableChange,
  TopBar,
  ConfirmDeleteDialog,
  ErrorPage,
  CustomModal,
  NewCustomButton,
  CustomAvatar,
} from '@lib/ui-kit';
import { useColumns } from './useColumns';
import { CompteRendu } from './interface';

import { Box, Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography, Fab, Grid } from '@material-ui/core';
import { useStyles } from './styles';
import { useStyles as useCommonStyles } from './../../condition-commerciale/pages/condition-commerciale/styles';
import { Add, Edit, Delete, MoreHoriz, Visibility, ArrowForwardIos } from '@material-ui/icons';
import { BottomScrollListener } from 'react-bottom-scroll-listener';
import moment from 'moment';
import { useApplicationContext } from '@lib/common';
import { useCompteRenduClient } from './utils/useCompteRenduClient';
import { useHistory } from 'react-router';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import { ActionOperationnel } from '../../action-operationnel/pages';
import EventIcon from '@material-ui/icons/Event';
import { SortDirection } from '@lib/common/src/federation';
const CompteRenduComp: FC<{}> = ({}): JSX.Element => {
  const history = useHistory();

  const [searchText, setSearchText] = useState<string>('');
  const [rowSelected, setRowSelected] = useState<CompteRendu>();
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(12);
  const [sortTable, setSortTable] = useState<any>({
    column: null,
    direction: 'DESC',
  });
  const [openAction, setOpenAction] = useState<boolean>(false);
  const toggleOpenAction = () => {
    setOpenAction((prev) => !prev);
  };

  const classes = useStyles();
  const commonStyles = useCommonStyles();
  const { isMobile } = useApplicationContext();

  const { params, redirectTo } = useLaboratoireParams('compte-rendu');
  const { id: idCompteRendu, view, idLabo, sorting } = params;
  const {
    compteRendus,
    rowsTotal,
    onRequestDelete,
    onRequestSearch,
    deletingSuccess,
    refetchCompteRendu,
    onRequestFetchMore,
  } = useCompteRenduClient(idCompteRendu || null);

  useEffect(() => {
    if (deletingSuccess && deletingSuccess === true) {
      setOpenDeleteDialog(false);
    }
  }, [deletingSuccess]);

  useEffect(() => {
    if (onRequestSearch) onRequestSearch({ skip, take, searchText, sorting });
  }, [
    skip,
    take,
    searchText,
    sortTable,
    idLabo?.length && idLabo[0],
    sorting && sorting[0] && sorting[0].field,
    sorting && sorting[0] && sorting[0].direction,
  ]);

  useEffect(() => {
    if (!compteRendus?.loading && rowSelected) {
      const newSelected = compteRendus.data.find((compteRendu: any) => compteRendu?.id === rowSelected.id);
      if (newSelected) setRowSelected(newSelected);
    }
  }, [compteRendus?.loading]);

  const open = Boolean(anchorEl);

  const handleSearchTable = ({ skip, take, searchText, sortDirection, sortBy }: TableChange) => {
    // setSkip(skip);
    // setTake(take);
    // setSearchText(searchText);
    redirectTo({
      ...params,
      skip: skip ? skip.toString() : undefined,
      take: take ? take.toString() : undefined,
      searchText,
      sorting: sortDirection && sortBy ? [{ field: sortBy, direction: sortDirection }] : undefined,
    });
  };

  const handleOpenAdd = () => {
    redirectTo({ view: 'creation', backParams: params }, 'compte-rendu-form');
  };
  const handleShowMenuClick = (row: CompteRendu, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    setRowSelected(row);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleEdit = (row: CompteRendu) => {
    redirectTo(
      { view: 'creation', id: row.id, idLabo: [row.partenaireTypeAssocie.id], backParams: params },
      'compte-rendu-form'
    );
  };

  const handleConfirmDeleteOne = (): void => {
    if (rowSelected?.id) {
      onRequestDelete && onRequestDelete(rowSelected.id);
    }
  };

  const handleDetail = (compteRendu: CompteRendu) => {
    if (rowSelected) {
      history.push(
        `/laboratoires/detail-compte-rendu?id=${rowSelected.id}&idLabo=${compteRendu.partenaireTypeAssocie.id}`
      );
    }
  };
  const handleDetailMobile = (compteRendu: CompteRendu) => {
    history.push(
      `/laboratoires/detail-compte-rendu?id=${compteRendu.id}&idLabo=${compteRendu.partenaireTypeAssocie.id}`
    );
  };
  const handleDetailAction = (compteRendu: CompteRendu) => {
    setOpenAction(true);
    setRowSelected(compteRendu);
  };

  const columns = useColumns({
    handleShowMenuClick,
    anchorEl,
    open,
    handleClose,
    handleEdit,
    handleDetail,
    setOpenDeleteDialog,
    compteRenduToEdit: rowSelected,
    handleDetailAction,
  });
  const handleContainerOnBottom = () => {
    if (onRequestFetchMore) onRequestFetchMore();
  };

  const handleChangeFilter = (_data: any) => {
    // setFilter(data);
  };

  const handleSortTable = (column: any, direction: SortDirection) => {
    redirectTo({ ...params, sorting: [{ field: column, direction }] });
  };

  const action = (row: CompteRendu) => {
    return (
      <div key={`row-${row.id}`}>
        <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleShowMenuClick.bind(null, row)}>
          <MoreHoriz />
        </IconButton>
        <Menu
          id="fade-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open && row.id === rowSelected?.id}
          onClose={handleClose}
          TransitionComponent={Fade}
        >
          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              handleClose();
              handleDetail(row);
            }}
          >
            <ListItemIcon>
              <Visibility />
            </ListItemIcon>
            <Typography variant="inherit">Voir détail compte rendu</Typography>
          </MenuItem>
          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              handleClose();
              handleEdit(row);
            }}
          >
            <ListItemIcon>
              <Edit />
            </ListItemIcon>
            <Typography variant="inherit">Modifier</Typography>
          </MenuItem>
          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              handleClose();
              setOpenDeleteDialog(true);
            }}
          >
            <ListItemIcon>
              <Delete />
            </ListItemIcon>
            <Typography variant="inherit">Supprimer</Typography>
          </MenuItem>
        </Menu>
      </div>
    );
  };

  return (
    <>
      <CustomModal
        open={openAction}
        setOpen={toggleOpenAction}
        title="Détail Action/Litige"
        closeIcon
        headerWithBgColor
        withBtnsActions={false}
        maxWidth="lg"
        fullScreen={!!isMobile}
      >
        <Box minWidth={isMobile ? undefined : 960}>
          <Box className={classes.topText}>Liste des Actions/Litiges</Box>
          <ActionOperationnel
            dataSuivi={rowSelected?.suiviOperationnels || []}
            refetchData={() => {
              refetchCompteRendu && refetchCompteRendu();
            }}
          />
        </Box>
      </CustomModal>
      {isMobile ? (
        <Box>
          <Box textAlign="center">
            <NewCustomButton onClick={handleOpenAdd} theme="flatPrimary" shadow={false} startIcon={<Add />}>
              Ajouter compte rendu
            </NewCustomButton>
          </Box>
          <div className={commonStyles.rootMobile}>
            <BottomScrollListener onBottom={handleContainerOnBottom}>
              {(scrollRef) => (
                <Grid id="infinity-suivi" item ref={scrollRef as any} className={commonStyles.infiniteScrollContainer}>
                  <Box>
                    {compteRendus?.data?.map((item: any, index: number) => {
                      console.log('--------compte rendu----------------', item);
                      const date = item.createdAt ? moment(item.createdAt).format('DD/MM/YYYY') : '-';
                      const suiviCloture = item.suiviOperationnels.filter(
                        (suivi: any) => suivi.statut?.code === 'CLOTURE'
                      ).length;
                      return (
                        <Box className={classes.listPlanMobile} py={2} px={1} key={item.id || index}>
                          <Box>
                            <Typography className={classes.textTitle} component="div">
                              {item.rapportVisite ? (
                                <span dangerouslySetInnerHTML={{ __html: item.rapportVisite }} />
                              ) : (
                                ' '
                              )}
                            </Typography>
                            <Typography className={classes.caption}>
                              <EventIcon style={{ marginRight: 10 }} />
                              <span style={{ color: '#424242' }}>{date}</span>
                            </Typography>
                            <Typography className={classes.typeAssocie}>
                              <CustomAvatar
                                name={item.partenaireTypeAssocie?.nom || ''}
                                url={item.partenaireTypeAssocie?.photo?.publicUrl || ''}
                                className={classes.logoTypeAssocie}
                              />{' '}
                              {item.partenaireTypeAssocie?.nom}
                            </Typography>
                          </Box>
                          <Box
                            textAlign="right"
                            width="50%"
                            display="flex"
                            flexDirection="column"
                            justifyContent="space-between"
                          >
                            <Typography>{action(item as any)}</Typography>
                            <Box px={1}>
                              <Typography color="primary">
                                {suiviCloture}/{item.suiviOperationnels?.length}
                              </Typography>
                            </Box>
                            <Typography className={classes.btnDetails}>
                              <IconButton
                                onClick={(event) => {
                                  event.stopPropagation();
                                  handleDetailMobile(item);
                                }}
                                style={{ padding: 0 }}
                              >
                                <ArrowForwardIos fontSize="small" />
                              </IconButton>
                            </Typography>
                          </Box>
                        </Box>
                      );
                    })}
                  </Box>
                </Grid>
              )}
            </BottomScrollListener>
            {compteRendus?.error && <ErrorPage />}
          </div>
        </Box>
      ) : (
        <>
          {!view && (
            <>
              <Box className={classes.root}>
                <Table
                  error={compteRendus?.error as any}
                  loading={compteRendus?.loading}
                  search={false}
                  data={compteRendus?.data || []}
                  selectable={false}
                  columns={columns}
                  rowsTotal={rowsTotal}
                  topBarComponent={(searchComponent) => (
                    <TopBar
                      handleOpenAdd={handleOpenAdd}
                      searchComponent={<></>}
                      titleButton="AJOUTER UN COMPTE RENDU"
                      // titleTopBar="Liste des comptes rendus"
                      withFilter
                    />
                  )}
                  onRunSearch={handleSearchTable}
                  onSortColumn={handleSortTable}
                />
                <ConfirmDeleteDialog
                  title="Suppression du compte rendu"
                  content="Etes-vous sûr de vouloir supprimer ce compte rendu ?"
                  open={openDeleteDialog}
                  setOpen={setOpenDeleteDialog}
                  onClickConfirm={handleConfirmDeleteOne}
                />
              </Box>
            </>
          )}
        </>
      )}
    </>
  );
};

export default CompteRenduComp;
