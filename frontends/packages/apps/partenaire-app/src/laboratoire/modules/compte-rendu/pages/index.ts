import CompteRendu from './CompteRendu';
import { CompteRenduComponentProps } from './interface';

export { CompteRendu, CompteRenduComponentProps };
