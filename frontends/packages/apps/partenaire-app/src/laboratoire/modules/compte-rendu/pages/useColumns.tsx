// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable react/jsx-no-bind */
import React, { MouseEvent } from 'react';
import { Avatar, Box, Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Edit, Delete, MoreHoriz, Visibility, Person } from '@material-ui/icons';
import moment from 'moment';
import { Column } from '@lib/ui-kit';
import { useStyles } from './styles';
import { CompteRendu } from './interface';
import { strippedColumnStyle } from '@lib/common';
import { useGet_List_Suivi_Operationnel_StatutQuery } from '@lib/common/src/federation';

export interface UseColumnCompteRendu {
  handleShowMenuClick: (compteRendu: CompteRendu, event: MouseEvent<HTMLElement>) => void;
  anchorEl: HTMLElement | null;
  open: boolean;
  handleClose: () => void;
  handleEdit: (compteRendu: CompteRendu) => void;
  handleDetail: (compteRendu: CompteRendu) => void;
  setOpenDeleteDialog: (open: boolean) => void;
  compteRenduToEdit?: CompteRendu;
  handleDetailAction: (compteRendu: CompteRendu) => void;
}

export const useColumns = ({
  handleShowMenuClick,
  anchorEl,
  open,
  handleClose,
  handleEdit,
  handleDetail,
  setOpenDeleteDialog,
  compteRenduToEdit,
  handleDetailAction,
}: UseColumnCompteRendu) => {
  const classes = useStyles();

  const columns: Column[] = [
    {
      name: 'titre',
      label: 'Titre',
      sortable: true,
      className: classes.wrappedColumn,
      renderer: (row: CompteRendu) => {
        return row.titre ? <Typography>{row.titre}</Typography> : '-';
      },
      style: strippedColumnStyle,
    },
    {
      name: 'partenaireTypeAssocie.nom',
      label: 'Labo',
      renderer: (row: CompteRendu) => {
        return row?.createdAt ? (
          <Box
            onClick={(event) => {
              event.stopPropagation();
              handleDetail(row);
            }}
          >
            <Typography className={classes.tableText}>{row.partenaireTypeAssocie?.nom || '-'}</Typography>
          </Box>
        ) : (
          '-'
        );
      },
    },
    {
      name: 'collaborateur',
      label: 'Pharmacie',
      renderer: (row: CompteRendu) => {
        return row.collaborateurs && row.collaborateurs.length > 0 ? (
          <Box
            display="flex"
            flexDirection="row"
            alignItems="center"
            onClick={(event) => {
              event.stopPropagation();
              handleDetail(row);
            }}
          >
            <Avatar
              alt="collaborateur"
              src={row.collaborateurs[0].photo ? row.collaborateurs[0].photo.publicUrl || '#' : undefined}
            >
              <Person />
            </Avatar>
            &nbsp; &nbsp;
            <Typography className={classes.tableText}>
              {((row.collaborateurs[0] as any).fullName as string).split(' ')[0]}
            </Typography>
          </Box>
        ) : (
          ''
        );
      },
    },
    {
      name: 'responsable',
      label: 'Laboratoire',
      renderer: (row: CompteRendu) => {
        return row.responsables && row.responsables.length > 0 ? (
          <Box
            display="flex"
            flexDirection="row"
            alignItems="center"
            onClick={(event) => {
              event.stopPropagation();
              handleDetail(row);
            }}
          >
            <Avatar
              alt="responsableLabo"
              src={row.responsables[0].photo ? row.responsables[0].photo?.publicUrl || '#' : undefined}
            >
              <Person />
            </Avatar>
            &nbsp; &nbsp;
            <Typography className={classes.tableText}>{row.responsables[0].prenom}</Typography>
          </Box>
        ) : (
          ''
        );
      },
    },
    {
      name: 'action',
      label: 'Action',
      renderer: (row: CompteRendu) => {
        const nombreAction = row?.suiviOperationnels?.length;
        const nombreActionCloture = (row?.suiviOperationnels || []).filter(
          (action) => action.statut.code === 'CLOTURE'
        )?.length;
        return row?.createdAt ? (
          <Box
            onClick={(event) => {
              event.stopPropagation();
              handleDetailAction(row);
            }}
          >
            <Typography className={classes.textAction}>
              {nombreAction ? `${nombreActionCloture}/${nombreAction}` : 0}
            </Typography>
          </Box>
        ) : (
          '-'
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: CompteRendu) => {
        return (
          <div key={`row-${row.id}`}>
            <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleShowMenuClick.bind(null, row)}>
              <MoreHoriz />
            </IconButton>
            <Menu
              id="fade-menu"
              anchorEl={anchorEl}
              keepMounted
              open={open && row.id === compteRenduToEdit?.id}
              onClose={handleClose}
              TransitionComponent={Fade}
            >
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  handleDetail(row);
                }}
              >
                <ListItemIcon>
                  <Visibility />
                </ListItemIcon>
                <Typography variant="inherit">Voir détail compte rendu</Typography>
              </MenuItem>
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  handleEdit(row);
                }}
              >
                <ListItemIcon>
                  <Edit />
                </ListItemIcon>
                <Typography variant="inherit">Modifier</Typography>
              </MenuItem>
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  setOpenDeleteDialog(true);
                }}
              >
                <ListItemIcon>
                  <Delete />
                </ListItemIcon>
                <Typography variant="inherit">Supprimer</Typography>
              </MenuItem>
            </Menu>
          </div>
        );
      },
    },
  ];

  return columns;
};
