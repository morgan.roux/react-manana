import { TableChange } from '@lib/ui-kit';
import { CompteRenduFormValue } from '../interface';
import {
  PrtPlanMarketingInfoFragment,
  PrtConditionInfoFragment,
  ConditionTypeInfoFragment,
} from '@lib/common/src/federation';
import { ReactNode } from 'react';

export interface MiseEnAvant {
  id: string;
  libelle: string;
  code: string;
  couleur?: string;
}

export interface SecondFormulaireCompteRenduProps {
  setValueToSave: React.Dispatch<React.SetStateAction<CompteRenduFormValue | undefined>>;
  valueToSave?: CompteRenduFormValue;
  conditionCommercialeTypes: ConditionTypeInfoFragment[];
  planMarketingTypes: PrtPlanMarketingInfoFragment[];
  onRequestSelectedCondition?: (condition: PrtConditionInfoFragment[]) => void;
  onRequestSelectedPlan?: (plan: PrtPlanMarketingInfoFragment[]) => void;
  produits?: {
    error?: Error;
    loading?: boolean;
    data?: any;
    rowsTotal?: number;
    onRequestSearch?: (change?: TableChange | null, ids?: string[]) => void;
    planMarketingProduits?: any;
  };
  planMarketings?: {
    onSavePlan: (data: PrtPlanMarketingInfoFragment) => void;
    saving: {
      loading?: boolean;
      error?: Error;
      success?: boolean;
    };
    data: PrtPlanMarketingInfoFragment[];
    loading?: boolean;
    error?: boolean;
    onRequestDeleteTrade: (trade: PrtPlanMarketingInfoFragment) => void;
  };

  remiseArrieres?: {
    onSavePlan: (data: PrtPlanMarketingInfoFragment) => void;
    saving: {
      loading?: boolean;
      error?: Error;
      success?: boolean;
    };
    data: PrtPlanMarketingInfoFragment[];
    loading?: boolean;
    error?: boolean;
    onRequestDeleteRemiseArriere: (trade: PrtPlanMarketingInfoFragment) => void;
  };
  conditionCommerciales?: {
    onSaveCondition: (data: PrtConditionInfoFragment) => void;
    saving: {
      loading?: boolean;
      error?: Error;
      success?: boolean;
    };
    data: PrtConditionInfoFragment[];
    loading?: boolean;
    error?: boolean;
    onRequestDeleteCondition: (condition: PrtConditionInfoFragment) => void;
  };
  setConditionSelected?: (condition: PrtConditionInfoFragment | []) => void;
  conditionSelected?: PrtConditionInfoFragment[];
  setRemiseSelected?: (condition: PrtPlanMarketingInfoFragment | []) => void;
  remiseSelected?: PrtPlanMarketingInfoFragment[];
  setPlanSelected?: (plan: PrtPlanMarketingInfoFragment | []) => void;
  planSelected?: PrtPlanMarketingInfoFragment[];
  partenaireAssocie?: any;
}
