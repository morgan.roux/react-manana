import { IconButton, Menu, MenuItem, Typography, Box } from '@material-ui/core';
import { Delete, Edit, MoreHoriz } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, memo } from 'react';
import { Column } from '@lib/ui-kit';
import { PrtConditionInfoFragment } from '@lib/common/src/federation';

export interface ColumnsConditionCommercialeAccordProps {
  onMenuClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>, row: PrtConditionInfoFragment) => void;
  onDeleteClick: (row: PrtConditionInfoFragment) => void;
  onEditClick: (row: PrtConditionInfoFragment) => void;
  onCloseMenu: (row: PrtConditionInfoFragment) => void;
  anchor: any;
  conditionCommerciale?: PrtConditionInfoFragment;
}

export const useColumnConditionCommerciale = ({
  onMenuClick,
  onDeleteClick,
  onEditClick,
  onCloseMenu,
  anchor,
  conditionCommerciale,
}: ColumnsConditionCommercialeAccordProps): Column[] => {
  return [
    {
      name: 'type',
      label: 'Type',
      renderer: (row: PrtConditionInfoFragment) => {
        return row.type.libelle;
      },
    },
    {
      name: 'titre',
      label: 'Titre',
      renderer: (row: PrtConditionInfoFragment) => {
        return row.titre;
      },
    },
    {
      name: 'labo',
      label: 'Labo',
      renderer: (row: PrtConditionInfoFragment) => {
        return row.typeAssocie?.nom;
      },
    },
    {
      name: 'periode',
      label: 'Période',
      renderer: (row: PrtConditionInfoFragment) => {
        return `Du ${moment.utc(row.dateDebut).format('DD/MM/YYYY')} - Au ${moment
          .utc(row.dateFin)
          .format('DD/MM/YYYY')}`;
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: PrtConditionInfoFragment) => {
        return (
          <Box key={`row-${row.id}`}>
            <IconButton
              onClick={(e) => {
                e.stopPropagation();
                onMenuClick(e, row);
              }}
            >
              <MoreHoriz />
            </IconButton>
            <Menu
              id="simple-menu"
              anchorEl={anchor}
              keepMounted
              open={Boolean(anchor) && row.id === conditionCommerciale?.id}
              onClose={onCloseMenu}
            >
              <MenuItem
                onClick={(e) => {
                  e.stopPropagation();
                  onEditClick(row);
                }}
              >
                <Edit />
                &nbsp;
                <Typography variant="inherit">Modifier</Typography>
              </MenuItem>
              <MenuItem
                onClick={(e) => {
                  e.stopPropagation();
                  onDeleteClick(row);
                }}
              >
                <Delete />
                &nbsp;
                <Typography variant="inherit">Suprimer</Typography>
              </MenuItem>
            </Menu>
          </Box>
        );
      },
    },
  ];
};
