import { PRTPlanMarketing } from '@lib/types';
import { ReactNode } from 'react';

export interface OperationProps {
  mode: 'FACTURE' | 'CLOTURE' | 'REALISE';
  onRequestDataOperation?: ({ operation, fichiers, type, produit }: OperationStatus) => void;
  openRealise?: boolean;
  openCloture?: boolean;
  openFacture?: boolean;
  planMarketing: PRTPlanMarketing;
  factureOperationComponent?: ReactNode;
  listModeReglement?: OperationModeReglement[];
  listProduits?: any[];
}

export interface OperationRealise {
  description: string;
  montantStatutRealise: number;
  idModeReglement?: string;
}

export interface OperationCloture {
  date: Date;
  montant: number;
  idModeReglement: string;
}

export interface OperationStatus {
  operation: OperationRealise | OperationCloture;
  fichiers: any[];
  type: string;
  produit?: any[];
}

export interface OperationModeReglement {
  id: string;
  libelle: string;
  code: string;
}
