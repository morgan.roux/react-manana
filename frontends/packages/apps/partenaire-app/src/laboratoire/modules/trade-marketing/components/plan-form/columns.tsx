import { Box, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import { Column, CustomFormTextField } from '@lib/ui-kit';
import { computeMontantRemuneration } from './util';

export const useColumn = (
  dataTable: any[],
  rowsSelected: any[],
  setRowsSelected: (newRowsSelected: any[]) => void,
  remise: number,
  typeRemuneration: string,
  isSelectable: boolean,
) => {
  const [inputReduction, setInputReduction] = useState<{ mode: string; index: number }>({
    mode: 'label',
    index: 0,
  });
  const [inputPrevue, setInputPrevue] = useState<{ mode: string; index: number }>({
    mode: 'label',
    index: 0,
  });
  const [nombre, setNombre] = useState<number>(0);

  const handleInputReduction = (value: number, index?: number) => {
    setInputPrevue({ mode: 'label', index: -1 });
    setInputReduction({ mode: 'input', index: index || 0 });
    setNombre(value);

    updateSelectedRow(dataTable[index || 0].id, 'prixUnitaire', value);
  };
  const handleInputPrevue = (value: number, index?: number) => {
    setInputReduction({ mode: 'label', index: -1 });
    setInputPrevue({ mode: 'input', index: index || 0 });
    setNombre(value);

    updateSelectedRow(dataTable[index || 0].id, 'quantitePrevue', value);
  };
  const handleChangeOnBlur = (type: string) => {
    type === 'PU' && setInputReduction({ mode: 'label', index: -1 });
    type === 'PREVUE' && setInputPrevue({ mode: 'label', index: -1 });

    setNombre(0);
  };

  const updateSelectedRow = (newCanalProduitId: string, name: string, value: any) => {
    const alreadySelected = rowsSelected.some(
      (canalProduit) => canalProduit.id === newCanalProduitId,
    );

    if (alreadySelected) {
      setRowsSelected(
        rowsSelected.map((element) => {
          if (element.id === newCanalProduitId) {
            if (element.produit) {
              element.produit[name] = value;
              element.produit['remisePrevue'] = computeMontantRemuneration(
                typeRemuneration,
                remise,
                element,
              );
            }
          }
          return element;
        }),
        //.filter((element) => element.produit.remisePrevue > 0),
      );
    } else {
      const produitCanal = dataTable.find((element) => element.id === newCanalProduitId);
      setRowsSelected(
        [
          ...rowsSelected,
          {
            ...produitCanal,
            produit: {
              ...produitCanal.produit,
              remisePrevue: computeMontantRemuneration(typeRemuneration, remise, produitCanal),
            },
          },
        ], //.filter((element) => element.produit.remisePrevue > 0),
      );
    }
  };

  const handleChangeNombre = ({ event, id }: any) => {
    event.preventDefault();
    event.stopPropagation();
    const type = event.target.name;
    setNombre(parseFloat(event.target.value));

    updateSelectedRow(id, type, parseFloat(event.target.value));

    /*setDataTable(
      (dataTable || []).map((element: any) => {
        if (element.id === id) {
          if (element.produit) {
            element.produit[type] = parseFloat(event.target.value);
            element.produit['remisePrevue'] = computeMontantRemuneration(typePlan, remise, element);
          }
        }
        return element;
      }),
    );*/
  };

  /* useEffect(() => {
    console.log('--------------------rowsSelectedProduits column-----: ', rowsSelectedProduits);
    const totalPrevue =
      (rowsSelectedProduits || [])
        .map((el: any) => {
          return parseFloat(el.produit?.remisePrevue) || 0;
        })
        .reduce((accumulator: number, current: number) => {
          return accumulator + current;
        }, 0) || 0;
    console.log('---------------------totalPrevue:', totalPrevue);
    setMontantPrevue(totalPrevue);
    const dataChanged = (data || []).map((element: any) => {
      if (element.produit) {
        element.produit['remisePrevue'] =
          typePlan === 'BRI'
            ? element.produit['prixUnitaire'] * element.produit['quantitePrevue'] || 0
            : ((element.produit['prixUnitaire'] * remise) / 100) *
              element.produit['quantitePrevue'];
      }
      return element;
    });
    setRowChangedProduit(dataChanged);
  }, [remise, typePlan, rowsSelectedProduits, nombre]);*/

  const produitColumns: Column[] = [
    {
      name: 'produit.produitCode.code',
      label: 'Code',
      renderer: (value: any) => {
        return value?.produit?.produitCode?.code || '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return value?.produit?.produitPhoto?.fichier?.publicUrl ? (
          <img
            style={{ height: 48, width: 50, objectFit: 'cover' }}
            src={value?.produit?.produitPhoto?.fichier?.publicUrl}
          />
        ) : (
          '-'
        );
      },
    },
    {
      name: 'produit.libelle',
      label: 'Nom',
      renderer: (value: any) => {
        return <div style={{ minWidth: 150 }}>{value?.produit?.libelle || '-'}</div>;
      },
    },
    {
      name: 'produit.famille.libelleFamille',
      label: 'Famille',
      renderer: (value: any) => {
        console.log('---value: ', value);
        return value?.produit?.famille?.libelleFamille || '-';
      },
    },
    {
      name: `${typeRemuneration === 'REDUCTION_EURO' ? 'produit.reduction' : 'produit.pu'}`,
      label: `${isSelectable ? ' ' : typeRemuneration === 'REDUCTION_EURO' ? 'Reduction' : 'PU'}`,
      style: { width: 100, textAlign: 'right' },
      renderer: (value: any, index?: number) => {
        return (
          <>
            {isSelectable ? ' ' : inputReduction.mode === 'input' && inputReduction.index === index ? (
              <CustomFormTextField
                type="number"
                name="prixUnitaire"
                onChange={(event) =>
                  handleChangeNombre({
                    event,
                    id: value.id,
                  })
                }
                onBlur={() => {
                  handleChangeOnBlur('PU');
                }}
                onKeyPress={(event) => {
                  if (event.key === 'Enter') {
                    event.preventDefault();
                    event.stopPropagation();
                    handleChangeOnBlur('PU');
                  }
                }}
                value={nombre}
                inputProps={{ min: 0 }}
              />
            ) : (
              <Box onClick={() => handleInputReduction(value?.produit?.prixUnitaire, index)}>
                <Typography>{(value?.produit?.prixUnitaire || 0).toFixed(2)} ‎€</Typography>
              </Box>
            )}
          </>
        );
      },
    },
    {
      name: `produit.quantite_prevue`,
      label: 'Quantité(s) Prévue(s)',
      style: { width: 100, textAlign: 'center' },
      renderer: (value: any, index: any) => {
        return (
          <>
            {inputPrevue.mode === 'input' && inputPrevue.index === index ? (
              <CustomFormTextField
                type="number"
                name="quantitePrevue"
                onChange={(event) => handleChangeNombre({ event, id: value.id })}
                value={nombre}
                onBlur={() => handleChangeOnBlur('PREVUE')}
                inputProps={{ min: 0 }}
                onKeyPress={(event) => {
                  if (event.key === 'Enter') {
                    event.preventDefault();
                    event.stopPropagation();
                    handleChangeOnBlur('PREVUE');
                  }
                }}
              />
            ) : (
              <Box onClick={() => handleInputPrevue(value?.produit?.quantitePrevue, index)}>
                <Typography>{value?.produit?.quantitePrevue || 0} </Typography>
              </Box>
            )}
          </>
        );
      },
    },
    {
      name: 'produit.remisePrevue',
      label: `${isSelectable ? ' ' : 'Remise prévue'}`,
      style: { textAlign: 'right' },
      renderer: (value: any) => {
        return isSelectable ? ' ' :  `${(value?.produit?.remisePrevue || 0).toFixed(2)} ‎€`;
      },
    },
  ];

  return produitColumns;
};
