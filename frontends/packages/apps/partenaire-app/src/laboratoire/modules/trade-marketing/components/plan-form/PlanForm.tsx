import { Dropzone, PartenaireInput, useApplicationContext } from '@lib/common';
import { useGet_LaboratoireLazyQuery, usePlan_Marketing_CategoriesQuery } from '@lib/common/src/federation';
import {
  CustomDatePicker,
  CustomEditorText,
  CustomFormTextField,
  CustomSelect,
  FormButtons,
  MontantInputField,
  Table,
  TopBar,
} from '@lib/ui-kit';
import { Box, FormControlLabel, InputAdornment, Switch, TableCell, TableRow, Typography } from '@material-ui/core';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import classNames from 'classnames';
import moment from 'moment';
import React, { ChangeEvent, FC, useEffect, useMemo, useState } from 'react';
import { LieuDeclarationInput } from '../../../../components/LieuDeclarationInput/LieuDeclarationInput';
import { ModeDeclarationInput } from '../../../../components/ModeDeclarationInput/ModeDeclarationInput';
import { ReglementTypeInput } from '../../../../components/ReglementTypeInput/ReglementTypeInput';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import { useCataloguePharmacie as useCataloguePharmacieHook } from './../../../../hooks/useCataloguePharmacie';
import { useColumn } from './columns';
import { MiseAvantSelection, PlanFormProps, PlanFormRequestSavingProps } from './PlanFormProps';
import { useStyles } from './styles';
import { computeMontantRemuneration, computeTotalRemunerationPrevue } from './util';
import { useCatalogueOperationMarketing } from './../../../../hooks/useCatalogueOperationMarketing';

const remunerationLists = [
  { id: 'forfaitaire', value: 'Forfaitaire', code: 'FORFAITAIRE' },
  { id: 'produit', value: 'Par produit', code: 'PRODUIT' },
];

const PlanForm: FC<PlanFormProps> = ({
  mode,
  open,
  setOpen,
  listTypePlan,
  onRequestSave,
  saving,
  saved,
  setSaved,
  // setOpenEditDialog,
  formData,
  produits,
  withTitle = true,
  inModalForm,
  partenaireAssocie,
  estRemiseArriereProp = false,
}) => {
  const classes = useStyles();

  const { isMobile, currentPharmacie } = useApplicationContext();
  const activateCatalogueProduits = useCatalogueOperationMarketing();
  const { params, goBack } = useLaboratoireParams();
  const { idLabo, estRemiseArriere: estRemiseArriereParam } = params;
  const estRemiseArriere = estRemiseArriereProp || estRemiseArriereParam;

  const [loadCataloguePharmacie, shouldUseCataloguePharmacie] = useCataloguePharmacieHook();
  const planCategories = usePlan_Marketing_CategoriesQuery().data?.pRTPlanMarketingCategories?.nodes;
  const categorieBriequipe = planCategories?.find((categorie) => categorie.code === 'CHALLENGE');

  const initialValues = {
    idType: '',
    dateDebut: moment(new Date()).clone().startOf('month').toDate(),
    dateFin: moment(new Date()).clone().endOf('month').toDate(),
    titre: '',
    description: '',
    selectedFiles: [] as any,
    state: {} as any,
    voirTout: false,
    montant: 0,
    remise: 0,
    typeRemuneration: 'REDUCTION_EURO',
    remuneration: 'FORFAITAIRE',
  };

  const [values, setValues] = useState<any>(initialValues);
  const [partenaire, setPartenaire] = useState<any>(partenaireAssocie);

  const [loadLabo] = useGet_LaboratoireLazyQuery({
    onCompleted: (result) => {
      setPartenaire(result.laboratoire);

      setRenderLabo(true);
    },
    onError: () => {
      setRenderLabo(true);
    },
  });

  const [showSelectedProduits, setShowSelectedProduits] = useState<boolean>(false);

  const [montantPrevue, setMontantPrevue] = useState<any>(0);
  const [dataTable, setDataTable] = useState<any>([]);
  const [selectedRows, setSelectedRows] = useState<any[]>([]);
  const [renderLabo, setRenderLabo] = useState<boolean>(true);

  const {
    idType,
    dateDebut,
    dateFin,
    titre,
    description,
    selectedFiles,
    state,
    remise,
    typeRemuneration,
    remuneration,
    idReglement,
    idLieuDeclaration,
    idModeDeclaration,
    montantStatutRealise,
    montantStatutCloture,
    precedentMontantRealise,
  } = values;

  const isSelectable =
    values?.typeRemuneration === 'SANS_REMUNERATION' ||
    (values?.typeRemuneration === 'REDUCTION_EURO' && values?.remuneration === 'FORFAITAIRE');

  const initDataTable = (displayCanalProduits: any[]) => {
    const newDataTable = displayCanalProduits.map((canalProduit) => {
      const selectedRow = selectedRows.find(
        (selectedCanalProduit) => selectedCanalProduit?.produit?.id === canalProduit?.produit?.id
      );

      return (
        selectedRow || {
          ...canalProduit,
          produit: {
            ...(canalProduit.produit || {}),
            prixUnitaire: 0,
            quantitePrevue: 0,
            remisePrevue: 0,
          },
        }
      );
    });

    setDataTable(newDataTable);
  };

  // const reglements = useGet_Prt_Reglement_TypesQuery().data?.pRTReglementTypes?.nodes || [];

  useEffect(() => {
    if (saved) {
      if (!inModalForm) {
        goBack(estRemiseArriere ? 'remise' : 'plan');
      }
      setOpen && setOpen(false);

      setSelectedRows([]);
      if (setSaved) setSaved(false);
    }
  }, [saved]);

  useEffect(() => {
    if (mode === 'ajout' && idType) {
      const type = (listTypePlan?.data || []).find(({ id }) => id === idType);
      if (type) {
        setValues((prev: any) => ({
          ...prev,
          idReglement: type.reglementType?.id,
          idLieuDeclaration: type.reglementLieuDeclaration?.id,
          idModeDeclaration: type.reglementModeDeclaration?.id,
        }));
      }
    }
  }, [idType, listTypePlan?.data, mode]);

  useEffect(() => {
    if (idLabo?.length) {
      if (!partenaire) setRenderLabo(false);
      loadLabo({
        variables: {
          id: idLabo[0],
          idPharmacie: currentPharmacie.id,
        },
      });

      if (!partenaire) {
        setPartenaire(idLabo[0]);
      }
    }
  }, [idLabo?.length && idLabo[0]]);

  useEffect(() => {
    if (partenaire?.id) {
      loadCataloguePharmacie(partenaire.id);
    }
  }, [partenaire?.id]);

  useEffect(() => {
    if (partenaire?.id) {
      produits?.onRequestSearch &&
        produits.onRequestSearch(undefined, undefined, shouldUseCataloguePharmacie, partenaire.id);
    }
  }, [shouldUseCataloguePharmacie]);

  useEffect(() => {
    setSelectedRows([]);
    setMontantPrevue(formData?.montant || 0);
    setShowSelectedProduits(false);
  }, [remuneration, typeRemuneration]);

  useEffect(() => {
    if (
      (values.typeRemuneration === 'REDUCTION_EURO' && values.remuneration !== 'FORFAITAIRE') ||
      values.typeRemuneration === 'REDUCTION_POURCENTAGE'
    ) {
      const newMontantPrevue = computeTotalRemunerationPrevue(values?.typeRemuneration, remise, selectedRows);
      setMontantPrevue(newMontantPrevue);
    }
  }, [selectedRows]);

  useEffect(() => {
    if (formData && mode === 'modification') {
      setValues((prev: any) => ({
        idType: formData.type.id,
        dateDebut: formData.dateDebut,
        dateFin: formData.dateFin,
        titre: formData.titre,
        description: formData.description,
        selectedFiles: formData.fichiers || [],
        state:
          formData.miseAvants?.reduce((previousMiseAvants: MiseAvantSelection, miseAvant: any) => {
            return { ...previousMiseAvants, [miseAvant.id]: true };
          }, {}) || {},
        voirTout: false,
        montant: formData.montant,
        remise: formData.remise as any,
        typeRemuneration: formData.typeRemuneration || 'REDUCTION_EURO',
        remuneration: formData.remuneration ?? estRemiseArriere ? 'FORFAITAIRE' : 'PRODUIT',
        idLieuDeclaration: formData.reglementLieuDeclaration?.id || prev?.idLieuDeclaration,
        idModeDeclaration: formData.reglementModeDeclaration?.id || prev?.idModeDeclaration,
        idReglement: formData.reglementType?.id,
        montantStatutRealise: formData.montantStatutRealise,
        montantStatutCloture: formData.montantStatutCloture,
        precedentMontantRealise: formData.precedentMontantRealise,
      }));

      if (formData.idPartenaireTypeAssocie) {
        if (!partenaire) setRenderLabo(false);
        loadLabo({
          variables: {
            id: formData.idPartenaireTypeAssocie,
            idPharmacie: currentPharmacie.id,
          },
        });
      }

      setMontantPrevue(formData.montant);
      setShowSelectedProduits(true);
    } else {
      setValues((prev: any) => ({
        ...initialValues,
        idReglement: prev?.idReglement,
        idLieuDeclaration: prev?.idLieuDeclaration,
        idModeDeclaration: prev?.idModeDeclaration,
      }));
    }
  }, [formData, mode]);

  useEffect(() => {
    if (!open) {
      setSelectedRows([]);
      setMontantPrevue(0);
      setPartenaire(partenaireAssocie);
      return;
    }
    if (formData && mode === 'modification') {
      setSelectedRows(
        (formData?.produits || []).map((produitCanal: any) => ({
          ...produitCanal,
          produit: {
            ...produitCanal.produit,
            remisePrevue: computeMontantRemuneration(typeRemuneration, remise, produitCanal),
          },
        }))
      );
    } else {
      setSelectedRows(
        selectedRows.map((produitCanal: any) => ({
          ...produitCanal,
          produit: {
            ...produitCanal.produit,
            remisePrevue: computeMontantRemuneration(typeRemuneration, remise, produitCanal),
          },
        }))
      );
    }
  }, [typeRemuneration, remise, open, formData]);

  /*  useEffect(() => {
    if (formData?.produits && produits?.onRequestSearch) {
      // produits.onRequestSearch(null, formData?.produits, produits?.pharmacieCatalogue);
    }
  }, [open]);
*/
  useEffect(() => {
    initDataTable(produits?.data || []);
  }, [produits, selectedRows]);

  const categorie = useMemo(() => {
    return idType ? listTypePlan?.data.find(({ id }) => id === idType)?.categorie : undefined;
  }, [idType, listTypePlan]);

  const miseEnAvant = categorie && categorie.code === 'MISE_EN_AVANT';
  const isBriEquipe = categorie && categorieBriequipe?.id === categorie.id;

  const menuLists = useMemo(() => {
    if (miseEnAvant) {
      return [
        { id: 'remuneration', value: 'Sans rémunération', code: 'SANS_REMUNERATION' },
        { id: 'euro', value: 'Montant', code: 'REDUCTION_EURO' },
      ];
    }

    let options = [{ id: 'remuneration', value: 'Sans rémunération', code: 'SANS_REMUNERATION' }];

    if (activateCatalogueProduits) {
      options = [
        ...options,
        { id: 'euro', value: 'Réduction Euro', code: 'REDUCTION_EURO' },
        { id: 'pourcentage', value: 'Réduction pourcentage', code: 'REDUCTION_POURCENTAGE' },
      ];
    } else {
      options = [...options, { id: 'euro', value: 'Montant', code: 'REDUCTION_EURO' }];
    }

    return options;
  }, [miseEnAvant]);

  const handleChanges = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setValues((prevState: any) => ({ ...prevState, [name]: value }));
    // setRemunerationType(value as string);
    // console.log('Brrrr', remunerationType, value);
  };

  const handleMyPharmacieChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target;
    produits?.onRequestSearch && produits.onRequestSearch(undefined, undefined, checked, partenaire?.id || undefined);
  };

  const handleChangeMontantPrevu = (prevu?: number) => {
    setMontantPrevue(prevu);
  };

  const handleChange = (name: string, value?: any) => {
    if (name) {
      setValues((prevState: any) => ({ ...prevState, [name]: value }));
    }
  };

  const handleSubmit = () => {
    const data: PlanFormRequestSavingProps = {
      id: mode === 'modification' ? formData?.id : undefined,
      idType,
      dateDebut,
      dateFin,
      titre,
      description,
      selectedFiles,
      idMiseAvants: Object.keys(state)
        .map((key) => [key, state[key]])
        .filter((result) => result[1])
        .map((result) => result[0] as string),
      produits: isSelectable
        ? selectedRows
        : selectedRows.filter((canalProduit: any) => canalProduit.produit?.remisePrevue || 0 > 0),
      montant: montantPrevue,
      remise: remise,
      montantRealise: null,
      typeRemuneration: typeRemuneration || 'REDUCTION_EURO',
      remuneration,
      idPartenaireTypeAssocie: partenaire?.id || partenaire,
      estRemiseArriere: !!estRemiseArriere,
      idReglementModeDeclaration: !idModeDeclaration || idModeDeclaration === 'AUCUN' ? undefined : idModeDeclaration,
      idReglementType: !idReglement || idReglement === 'AUCUN' ? undefined : idReglement,
      idReglementLieuDeclaration: !idLieuDeclaration || idLieuDeclaration === 'AUCUN' ? undefined : idLieuDeclaration,
      montantStatutRealise,
      montantStatutCloture,
      precedentMontantRealise,
    };

    onRequestSave && onRequestSave(data, mode);
    // setOpenEditDialog && setOpenEditDialog(false);
  };

  const handleClose = () => {
    setOpen && setOpen(false);
    if (!inModalForm) goBack(estRemiseArriere ? 'remise' : 'plan');
  };

  const isValid = () => {
    return !(
      (idType === '' || !dateDebut || !dateFin || titre === '' || dateDebut > dateFin || !partenaire?.id) /* ||
      (values?.typeRemuneration !== 'SANS_REMUNERATION'  && !montantPrevue)*/
    );
  };

  const onClearSelectionProduits = () => {
    setSelectedRows([]);
    //initDataTable(produits?.data || []); // Reset all values
  };

  const onShowSelectedProduits = () => {
    setShowSelectedProduits(!showSelectedProduits);
  };

  const handleSelectChange = (dataSelected: any) => {
    setSelectedRows(dataSelected);
  };

  const handleChangeLaboratoire = (laboratoire: any) => {
    setPartenaire(laboratoire);
    loadLabo({
      variables: {
        id: laboratoire.id,
        idPharmacie: currentPharmacie.id,
      },
    });
    produits?.onRequestSearch && produits.onRequestSearch(undefined, undefined, undefined, laboratoire.id);
  };

  const produitColumns = useColumn(
    dataTable,
    selectedRows,
    setSelectedRows,
    remise,
    values.typeRemuneration,
    isSelectable
  );

  const additionalRows = (
    <TableRow style={{ borderBottom: 'none' }}>
      <TableCell colSpan={4} />
      <TableCell colSpan={2} className={classes.info}>
        Total remise prévue
      </TableCell>
      <TableCell style={{ textAlign: 'right' }}>{parseFloat(montantPrevue || '0').toFixed(2)} ‎€</TableCell>
    </TableRow>
  );

  const topBarComponent = (searchComponent: any) => (
    <>
      <TopBar
        // handleOpenAdd={handleOpenAdd}
        searchComponent={
          <Box display="flex" justifyContent="space-between">
            {searchComponent}
            <FormControlLabel
              //disabled={listResult.loading}
              control={<Switch checked={produits?.pharmacieCatalogue || false} onChange={handleMyPharmacieChange} />}
              label="Mon Catalogue Produit"
            />
          </Box>
        }
        withFilter
        filterClassName={classNames(classes.filterClassName, classes.filterClassNameForm)}
      />
    </>
  );

  const aideMessage = React.useMemo(() => {
    if (values?.remuneration === 'PRODUIT' && !selectedRows?.length) {
      if (values?.typeRemuneration === 'REDUCTION_EURO') {
        return 'Vous devez saisir les réductions et les quantités prévues sur le tableau de liste de produits.';
      }

      if (values?.typeRemuneration === 'REDUCTION_POURCENTAGE') {
        return 'Vous devez saisir les prix unitaire et les quantités prévues sur le tableau de liste de produits.';
      }
    }

    return null;
  }, [values?.typeRemuneration, values?.remuneration, selectedRows?.length]);

  return (
    <>
      <Box pt={3}>
        {withTitle && (
          <Box display="flex" alignItems="center" justifyContent="center" width="100%">
            <Typography style={{ fontWeight: 'bold', fontSize: '18px' }}>
              {mode === 'ajout' && (estRemiseArriere ? 'Nouvelle Remise Arrière' : 'Nouvelle Opération Marketing')}
              {mode === 'modification' &&
                (estRemiseArriere ? 'Modification de Remise Arrière' : "Modification de l'Opération Marketing")}
            </Typography>
          </Box>
        )}
      </Box>
      <Box className={classes.form} mt={2} mb={isMobile ? '70px' : undefined}>
        <Box display="flex" justifyContent="center" alignItems="center" paddingX={0.5}>
          <form className={inModalForm ? classes.formFullWidth : classes.formWidth}>
            <Box className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}>
              <CustomSelect
                list={listTypePlan?.data}
                index="libelle"
                listId="id"
                label="Type"
                value={idType}
                name="idType"
                onChange={handleChanges}
                required
                placeholder="Type Plan Marketing"
                defaultValue={idType}
                className={classes.selectPadding10}
              />
            </Box>
            {idType && !estRemiseArriere && (listTypePlan?.data.length || 0) > 0 && (
              <Box className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}>
                <Box className={classes.categorie}>
                  {listTypePlan?.data.find(({ id }) => id === idType)?.categorie?.libelle}
                </Box>
              </Box>
            )}
            <Box
              pt={1}
              pb={1}
              mb={'15px !important'}
              className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}
            >
              {renderLabo && (
                <PartenaireInput
                  key="laboratoire"
                  index="laboratoire"
                  label="Labo"
                  value={partenaire}
                  onChange={handleChangeLaboratoire}
                  disabled={partenaireAssocie ? true : false}
                />
              )}
            </Box>
            <Box py={1} className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}>
              <Typography className={classes.subtitle}>Période</Typography>
            </Box>
            <Box
              py={1}
              display="flex"
              flexDirection="row"
              justifyContent="space-between"
              className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}
            >
              <Box pr={1} width="50%">
                <CustomDatePicker
                  value={dateDebut}
                  onChange={(date) => handleChanges({ target: { name: 'dateDebut', value: date } } as any)}
                  required
                  label="Date de début"
                  placeholder="00/00/0000"
                />
              </Box>
              <Box pl={1} width="50%">
                <CustomDatePicker
                  value={dateFin}
                  onChange={(date) => handleChanges({ target: { name: 'dateFin', value: date } } as any)}
                  required
                  label="Date de fin"
                  placeholder="00/00/0000"
                />
              </Box>
            </Box>
            <Box pt={1} pb={1} className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}>
              <CustomFormTextField
                label="Titre"
                type="text"
                name="titre"
                onChange={handleChanges}
                value={titre}
                placeholder={estRemiseArriere ? 'Titre' : 'Titre plan marketing'}
                required
              />
            </Box>

            <Box
              py={1}
              mb={'15px !important'}
              className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}
            >
              <CustomEditorText
                value={description}
                placeholder="Description Détaillée"
                onChange={(content) => handleChanges({ target: { name: 'description', value: content } } as any)}
              />
            </Box>
            {!estRemiseArriere && !isBriEquipe && (
              <Box className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}>
                <CustomSelect
                  list={menuLists}
                  index="value"
                  listId="code"
                  label="Type de rémunération"
                  value={values.typeRemuneration}
                  onChange={handleChanges}
                  name="typeRemuneration"
                  className={classes.selectPadding10}
                />
              </Box>
            )}
            {values.typeRemuneration === 'REDUCTION_EURO' &&
              !estRemiseArriere &&
              !miseEnAvant &&
              !isBriEquipe &&
              activateCatalogueProduits && (
                <Box
                  style={{ marginTop: 10 }}
                  className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}
                >
                  <CustomSelect
                    list={remunerationLists}
                    index="value"
                    listId="code"
                    label="Rémunération"
                    value={values.remuneration}
                    name="remuneration"
                    onChange={handleChanges}
                    className={classes.selectPadding10}
                  />
                </Box>
              )}

            {isBriEquipe && (
              <Box pt={1} pb={1} className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}>
                <MontantInputField
                  label="Vente réalisée année n-1"
                  onChangeValue={(montant) => handleChange('precedentMontantRealise', montant)}
                  value={values?.precedentMontantRealise}
                />
              </Box>
            )}

            {values.typeRemuneration === 'REDUCTION_EURO' && values.remuneration === 'FORFAITAIRE' && (
              <Box pt={1} pb={1} className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}>
                <MontantInputField
                  label={isBriEquipe ? 'Objectif' : 'Rémunération prévue'}
                  value={montantPrevue}
                  onChangeValue={handleChangeMontantPrevu}
                  disabled={!!formData?.statut?.code && formData?.statut?.code !== 'EN_COURS'}
                />
              </Box>
            )}

            <Box pt={1} pb={1} className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}>
              <ReglementTypeInput onChange={handleChanges} idReglement={values.idReglement} />
              <ModeDeclarationInput idModeDeclaration={idModeDeclaration} onChangeValue={handleChange} />
              <LieuDeclarationInput idLieuDeclaration={idLieuDeclaration} onChangeValue={handleChange} />
            </Box>

            {formData && (
              <>
                <Box pt={1} pb={1} className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}>
                  <MontantInputField
                    label="Rémunération réalisée"
                    onChangeValue={(montant) => handleChange('montantStatutRealise', montant)}
                    value={values?.montantStatutRealise}
                    disabled={formData?.statut?.code !== 'REALISE'}
                  />
                </Box>
                <Box pt={1} pb={1} className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}>
                  <MontantInputField
                    label="Rémunération payée"
                    onChangeValue={(montant) => handleChange('montantStatutCloture', montant)}
                    value={values?.montantStatutCloture}
                    disabled={formData?.statut?.code !== 'CLOTURE'}
                  />
                </Box>
              </>
            )}

            {/* <Reglement laboratoire={partenaire} /> */}
            {activateCatalogueProduits && !estRemiseArriere && (
              <Box py={1}>
                <Typography className={classes.subtitle}>Choix produits</Typography>
              </Box>
            )}
            {!estRemiseArriere && (
              <Box py={1}>
                {isMobile ? (
                  <>
                    {/*<Box>
                    <Filter
                      filters={[
                        {
                          titre: 'Type',
                          itemFilters: (dataTable?.data || []).map((item: any) => ({
                            ...item,
                            keyword: 'idType',
                          })) as any,
                        },
                        {
                          titre: 'Statut',
                          itemFilters: (dataTable || []).map((item: any) => ({
                            ...item,
                            keyword: 'idStatut',
                          })) as any,
                        },
                      ]}
                      onChange={handleChangeFilter}
                      filterText="FILTRER PAR FAMILLE"
                      filterIcon={<FilterListIcon />}
                    />
                    </Box>*/}
                    <Box mt={3}>
                      {(dataTable || []).map((item: any) => {
                        return (
                          <div className={classes.newOperationMarketingMobile}>
                            <div style={{ marginRight: 15 }}>
                              {item?.produit?.produitPhoto?.fichier?.publicUrl ? (
                                <img
                                  style={{ height: 50, width: 50, objectFit: 'cover' }}
                                  src={item?.produit?.produitPhoto?.fichier?.publicUrl}
                                />
                              ) : (
                                '-'
                              )}
                            </div>
                            <div>
                              <Typography>{item?.produit?.libelle || '-'}</Typography>
                              <Typography variant="caption" style={{ color: '#888' }}>
                                <span style={{ color: '#424242' }}>{item?.produit?.produitCode?.code || '-'}</span>{' '}
                                <FiberManualRecordIcon style={{ width: 7, height: 7 }} />{' '}
                                {item?.produit?.famille?.libelleFamille || '-'}
                              </Typography>
                            </div>
                          </div>
                        );
                      })}
                    </Box>
                  </>
                ) : (
                  <>
                    {values.typeRemuneration === 'REDUCTION_POURCENTAGE' && (
                      <Box py={1}>
                        <CustomFormTextField
                          label="Remise"
                          type="number"
                          name="remise"
                          onChange={handleChanges}
                          value={remise}
                          placeholder="Remise"
                          required
                          style={{ maxWidth: 180 }}
                          InputProps={{
                            endAdornment: <InputAdornment position="end">‎%</InputAdornment>,
                          }}
                          inputProps={{ min: 0, step: 0.5, style: { textAlign: 'right' } }}
                        />
                      </Box>
                    )}
                    {activateCatalogueProduits && (
                      <Table
                        error={produits?.error as Error | undefined}
                        loading={produits?.loading}
                        search={false}
                        data={showSelectedProduits ? selectedRows : dataTable || []} //{rowsChangedProduit || []}
                        dataIdName="id"
                        selectable
                        showSelectionCheckBox={isSelectable}
                        topBarComponent={topBarComponent}
                        noTableToolbarSearch
                        columns={produitColumns}
                        rowsTotal={showSelectedProduits ? selectedRows.length : produits?.rowsTotal}
                        showSelectedRows={showSelectedProduits}
                        rowsSelected={selectedRows}
                        onRunSearch={(tableChange) =>
                          produits?.onRequestSearch &&
                          produits?.onRequestSearch(
                            tableChange,
                            undefined,
                            produits?.pharmacieCatalogue,
                            partenaire?.id
                          )
                        }
                        onClearSelection={onClearSelectionProduits}
                        onShowSelected={onShowSelectedProduits}
                        onSelectAll={/*onSelectAllProduits*/ undefined}
                        onRowsSelectionChange={isSelectable ? handleSelectChange : undefined}
                        additionalRows={isSelectable ? undefined : additionalRows}
                        tableBodyClassName={classes.strippedTR}
                        emptyLabel="Aucun produit trouvé"
                      />
                    )}
                  </>
                )}
              </Box>
            )}
            <Box py={2} className={inModalForm ? classes.fullWidthChildren : classes.formDirectChildren}>
              <Dropzone
                multiple
                setSelectedFiles={(files: any) =>
                  setValues((prevState: any) => ({ ...prevState, selectedFiles: files }))
                }
                selectedFiles={selectedFiles}
                contentText="Déposer vos documents"
              />
            </Box>
            <Box textAlign="right" mb={2}>
              {aideMessage && (
                <div
                  style={{
                    color: 'rgb(13, 60, 97)',
                    backgroundColor: 'rgb(232, 244, 253)',
                    padding: '16px',
                    borderRadius: '4px',
                    marginBottom: '5px',
                    textAlign: 'left',
                  }}
                >
                  {aideMessage}
                </div>
              )}
              <FormButtons
                onClickCancel={handleClose}
                onClickConfirm={handleSubmit}
                disableConfirm={!isValid() || saving}
              />
            </Box>
          </form>
        </Box>

        {/* // </CustomModal> */}
      </Box>
    </>
  );
};

export default PlanForm;
