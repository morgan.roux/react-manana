import { LazyQueryResult } from '@apollo/client';
import { useApplicationContext } from '@lib/common';
import { useChange_Status_ActionMutation, usePlanning_MarketingsLazyQuery } from '@lib/common/src/federation';
import {
  Search_Plan_Marketing_Produit_CanalsQueryVariables,
  Search_Plan_Marketing_Produit_CanalsQuery,
  useSearch_Plan_Marketing_Produit_CanalsLazyQuery,
} from '@lib/common/src/graphql';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import { FormattedPlanningMarketing } from './types';

export interface RequestPlanning {
  searchText: string;
  date?: any;
  limit?: number;
  offset?: number;
  dateDebut?: any;
  dateFin?: any;
}

export const usePlanningPlanMarketing = (): [
  (filtre: RequestPlanning) => void,
  FormattedPlanningMarketing[],
  boolean,
  (idPlanMarketing: string, produits: string[]) => void,
  LazyQueryResult<Search_Plan_Marketing_Produit_CanalsQuery, Search_Plan_Marketing_Produit_CanalsQueryVariables>,
  (id: string, statut: 'DONE' | 'ACTIVE') => void
] => {
  const { auth, currentPharmacie: pharmacie, graphql } = useApplicationContext();
  const { params } = useLaboratoireParams('planning');
  const { idLabo, idCategorie, idType, idStatut, idReglementType, idReglementModeDeclaration } = params;

  const [loadPlanningPlanMarketings, loadingPlanningPlanMarketings] = usePlanning_MarketingsLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [changeStatutAction] = useChange_Status_ActionMutation({
    onCompleted: (data) => {
      if (data.changeStatutAction) {
        loadingPlanningPlanMarketings.refetch && loadingPlanningPlanMarketings.refetch();
      }
    },
  });

  const [loadProduits, loadingProduits] = useSearch_Plan_Marketing_Produit_CanalsLazyQuery({ client: graphql });

  const pRTPlanningMarketings = loadingPlanningPlanMarketings.data?.pRTPlanningMarketing || [];

  const data = pRTPlanningMarketings.map((el) => ({
    prestataire: el.partenaireTypeAssocie as any,
    planMarketings: (el.planMarketings || []).map((el) => {
      const end = moment(el.dateFin).week();
      const start = moment(el.dateDebut).week();
      const week = end === start ? [start] : Array.from({ length: end - start }, (_x, i) => start + i);
      return { week, titre: el.titre, color: (el.type as any)?.couleur, planMarketing: el };
    }),
  }));

  const handleRequestPlanning = ({ searchText, offset, limit, date, dateDebut, dateFin }: RequestPlanning) => {
    const validPeriode = dateDebut && dateFin && moment(dateDebut).isBefore(dateFin);
    const input =
      !validPeriode || date
        ? {
            annee: moment(date).year(),
            partenaireType: 'LABORATOIRE',
            idPharmacie: pharmacie?.id,
            searchText,
            idPartenaireTypeAssocie: idLabo?.length ? idLabo[0] : undefined,
            offset,
            limit,
            idCategorie,
            idType: idType && idType.length > 0 ? idType[0] : undefined,
          }
        : {
            dateFin,
            dateDebut,
            partenaireType: 'LABORATOIRE',
            idPharmacie: pharmacie?.id,
            searchText,
            idPartenaireTypeAssocie: idLabo?.length ? idLabo[0] : undefined,
            offset,
            limit,
            idCategorie,
            idType: idType && idType.length > 0 ? idType[0] : undefined,
          };
    loadPlanningPlanMarketings({
      variables: {
        idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
        input,
      },
    });
  };

  useEffect(() => {
    const variables = loadingPlanningPlanMarketings.variables;
    if (idLabo && idLabo.length > 0) {
      if (variables) {
        loadPlanningPlanMarketings({
          variables: {
            ...variables,
            input: {
              ...variables.input,
              idPartenaireTypeAssocie: idLabo[0],
              idCategorie,
              idType: idType && idType.length > 0 ? idType[0] : undefined,
              idStatut: idStatut && idStatut.length > 0 ? idStatut[0] : undefined,
            },
          },
        });
      }
    } else {
      if (variables) {
        loadPlanningPlanMarketings({
          variables: {
            ...variables,
            input: {
              ...variables.input,
              idPartenaireTypeAssocie: '',
              idCategorie,
              idType: idType && idType.length > 0 ? idType[0] : undefined,
              idStatut: idStatut && idStatut.length > 0 ? idStatut[0] : undefined,
              idReglementType: idReglementType && idReglementType.length > 0 ? idReglementType[0] : undefined,
              idReglementModeDeclaration:
                idReglementModeDeclaration && idReglementModeDeclaration.length > 0
                  ? idReglementModeDeclaration[0]
                  : undefined,
            },
          },
        });
      }
    }
  }, [
    idLabo && idLabo[0],
    idCategorie,
    idType && idType[0],
    idStatut?.length && idStatut[0],
    idReglementType?.length && idReglementType[0],
    idReglementModeDeclaration?.length && idReglementModeDeclaration[0],
  ]);

  const handleRequestSearchProduits = (idPartenaire: string, ids: string[]) => {
    let must: any = [
      {
        term: {
          'commandeCanal.code': 'PFL',
        },
      },
      {
        term: {
          'produit.produitTechReg.laboExploitant.id': idPartenaire,
        },
      },
    ];

    if (ids && ids.length) {
      must.push({
        terms: {
          _id: ids,
        },
      });
    }

    const variables: Search_Plan_Marketing_Produit_CanalsQueryVariables = {
      type: ['produitcanal'],
      query: {
        query: {
          bool: {
            must,
          },
        },
      },
    };

    if (ids.length) {
      loadProduits({ variables });
    }
  };

  const handleOnRequestProduits = (idPartenaire: string, produits: string[]) => {
    handleRequestSearchProduits(idPartenaire, produits);
  };

  const handleOnRequestChangeStatutAction = (id: string, status: 'ACTIVE' | 'DONE') => {
    changeStatutAction({
      variables: {
        id,
        status,
      },
    });
  };

  return [
    handleRequestPlanning,
    data,
    loadingPlanningPlanMarketings.loading,
    handleOnRequestProduits,
    loadingProduits,
    handleOnRequestChangeStatutAction,
  ];
};
