import { PRTPlanMarketing } from '@lib/types';
import { TableChange } from '@lib/ui-kit';
import { PrtPlanMarketingInfoFragment, PrtPlanMarketingType } from '@lib/common/src/federation';

export interface PlanFormProps {
  mode: 'ajout' | 'modification';
  open: boolean;
  saving?: boolean;
  saved?: boolean;
  setSaved?: (newSaved: boolean) => void;
  setOpenEditDialog?: (OP: boolean) => void;
  setOpen?: (open: boolean) => void;
  onRequestSave?: (data: PlanFormRequestSavingProps, mode?: 'ajout' | 'modification') => void;
  listTypePlan?: {
    error?: boolean;
    loading?: boolean;
    data: PrtPlanMarketingType[];
  };
  formData?: PrtPlanMarketingInfoFragment;
  produits?: {
    error?: Error;
    loading?: boolean;
    data?: any;
    rowsTotal?: number;
    onRequestSearch?: (
      change?: TableChange | null,
      ids?: string[],
      pharmacieCatalogue?: boolean,
      idPartenaireTypeAssocie?: string
    ) => void;
    planMarketingProduits?: any;
    pharmacieCatalogue?: boolean;
  };
  withTitle?: boolean;
  filterIcon?: any;
  inModalForm?: boolean;
  partenaireAssocie?: any;
  estRemiseArriereProp?: boolean;
}

export interface PlanFormRequestSavingProps {
  id?: string;
  idType: string;
  dateDebut: Date;
  dateFin: Date;
  titre: string;
  description: string;
  selectedFiles: any[];
  idMiseAvants?: string[];
  produits?: any[];
  montant: string;
  remise: string | null;
  montantRealise: string | null;
  typeRemuneration: string;
  remuneration?: string;
  idPartenaireTypeAssocie?: string;
  estRemiseArriere?: boolean;
  idReglementLieuDeclaration?: string;
  idReglementModeDeclaration?: string;
  idReglementType?: string;
  montantStatutRealise?: string;
  montantStatutCloture?: string;
  precedentMontantRealise?: string;
}

export interface MiseAvantSelection {
  [key: string]: boolean;
}
