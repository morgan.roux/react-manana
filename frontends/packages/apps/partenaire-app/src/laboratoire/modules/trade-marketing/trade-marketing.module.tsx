import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';
import loadable from '@loadable/component';
import React from 'react';

const TradeMarketingPage = loadable(() => import('./pages/plan-trade'), {
  fallback: <SmallLoading />,
});

const RemiseArrierePage = loadable(() => import('./pages/remise-arriere'), {
  fallback: <SmallLoading />,
});

const PlanningTradeMarketingPage = loadable(() => import('./pages/planning-marketing/PlanningTradeMarketing'), {
  fallback: <SmallLoading />,
});

const activationParameters = {
  groupement: '0305',
  pharmacie: '0708',
};

const tradeMarketingModule: ModuleDefinition = {
  routes: [
    {
      attachTo: 'MAIN',
      path: '/laboratoires/plan',
      component: TradeMarketingPage,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: '/laboratoires/remise',
      component: RemiseArrierePage,
      activationParameters,
    },
    {
      attachTo: 'MAIN',
      path: ['/laboratoires/planning'],
      component: PlanningTradeMarketingPage,
      activationParameters,
    },
  ],
};

export default tradeMarketingModule;
