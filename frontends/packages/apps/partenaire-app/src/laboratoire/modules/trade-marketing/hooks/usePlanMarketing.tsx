import { useApplicationContext, useUploadFiles } from '@lib/common';
import {
  PrtPlanMarketingFilter,
  PrtPlanMarketingInfoFragment,
  PrtPlanMarketingSort,
  PrtPlanMarketingSortFields,
  SortDirection,
  useChange_Plan_Marketing_StatusMutation,
  useCreate_One_Prt_Plan_Marketing_FichierMutation,
  useCreate_Plan_MarketingMutation,
  useDelete_One_Plan_MarketingMutation,
  useGet_LaboratoireLazyQuery,
  useGet_List_Plan_Marketing_StatutQuery,
  useGet_List_Plan_Marketing_TypesQuery,
  useGet_Plan_MarketingsLazyQuery,
  useGet_Prt_ContactLazyQuery,
  useGet_Prt_Reglement_ModeQuery,
  useSend_Email_Trade_ManuelLazyQuery,
  useUpdate_Plan_MarketingMutation,
} from '@lib/common/src/federation';
import {
  Search_Plan_Marketing_Produit_CanalsQueryVariables,
  useSearch_Plan_Marketing_Produit_CanalsLazyQuery,
} from '@lib/common/src/graphql';
import { TableChange } from '@lib/ui-kit';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import { useHistory } from 'react-router';
import { refetchStatsVar } from '../../../reactive-vars';
import { useCatalogueOperationMarketing } from '../../../hooks/useCatalogueOperationMarketing';

export const usePlanMarketing = (estRemiseArriere?: boolean) => {
  const {
    auth,
    currentGroupement: groupement,
    currentPharmacie: pharmacie,
    graphql,
    notify,
    config,
    federation,
  } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const history = useHistory();
  const { params, redirectTo } = useLaboratoireParams(estRemiseArriere ? 'remise' : 'plan');
  const {
    idLabo,
    idStatut,
    idType,
    idCategorie,
    idReglementModeDeclaration,
    idReglementType,
    searchText,
    dateDebut,
    dateFin,
    sorting,
  } = params;

  const skip = params.skip ? parseInt(params.skip) : 0;
  const take = params.take ? parseInt(params.take) : 12;

  const searchTextPlan = searchText || '';
  // const [filterPlan, setFilterPlan] = useState<any[]>();
  const [planToEdit, setPlanToEdit] = useState<any>(undefined);
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [savingStatut, setSavingStatut] = useState<boolean>(false);
  const [savedStatut, setSavedStatut] = useState<boolean>(false);
  const [useCataloguePharmacie, setUseCataloguePharmacie] = useState<boolean>(true);
  const activateCatalogueProduits = useCatalogueOperationMarketing();

  const [recepteur, setRecepteur] = useState<any[]>([]);
  const [valueFacture, setValueFacture] = useState<{
    message: string | undefined;
    objet: string | undefined;
    recepteurs: any[];
    selectedFiles: any[];
  }>({
    message: undefined,
    objet: undefined,
    recepteurs: [],
    selectedFiles: [],
  });
  const [itemEmail, setItemEmail] = useState<any>();
  const [savingFichier, setSavingFichier] = useState<boolean>(false);
  const [savedFichier, setSavedFichier] = useState<boolean>(false);

  const typePlan = idType && idType.length > 0 ? idType[0] : undefined;

  useEffect(() => {
    onRequestPlanSearch();
  }, [
    idLabo?.length && idLabo[0],
    dateDebut,
    dateFin,
    searchTextPlan,
    typePlan,
    idReglementType?.length && idReglementType[0],
    idReglementModeDeclaration?.length && idReglementModeDeclaration[0],
    idStatut?.length && idStatut[0],
    take,
    skip,
    sorting && sorting[0] && sorting[0].field,
    sorting && sorting[0] && sorting[0].direction,
  ]);

  // Query PlanMarketing
  const planMarketingTypes = useGet_List_Plan_Marketing_TypesQuery({
    variables: {
      idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
      filter: { idGroupement: { eq: groupement.id } },
    },
  });

  // QUERY LISTE DES STATUTS
  const planMarketingStatut = useGet_List_Plan_Marketing_StatutQuery();

  const listTypePlan = {
    error: planMarketingTypes.error as any,
    loading: planMarketingTypes.loading,
    data: !auth.isSupAdminOrIsGpmAdmin
      ? (planMarketingTypes.data?.pRTPlanMarketingTypes.nodes || []).filter(
          (planMarketingType) => planMarketingType.active === true
        )
      : planMarketingTypes.data?.pRTPlanMarketingTypes.nodes || [],
  };

  const [loadLabo, loadingLabo] = useGet_LaboratoireLazyQuery();

  // QUERY
  const [loadPlanMarketing, loadingPlanMarketing] = useGet_Plan_MarketingsLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [loadProduits, loadingProduits] = useSearch_Plan_Marketing_Produit_CanalsLazyQuery({ client: graphql });

  const [loadPlanMarketingProduits, loadingPlanMarketingProduits] = useSearch_Plan_Marketing_Produit_CanalsLazyQuery({
    client: graphql,
  });

  /**create */
  const [createPlanMarketing] = useCreate_Plan_MarketingMutation({
    onCompleted: (data) => {
      notify({
        message: ' Le plan Marketing a été crée avec succés !',
        type: 'success',
      });

      loadingPlanMarketing.refetch && loadingPlanMarketing.refetch();
      refetchStatsVar(true);
      setSaving(false);
      setSaved(true);
    },
    onError: (error) => {
      console.log('error: ', error);
      notify({
        message: 'Erreurs lors de la création du plan Marketing ',
        type: 'error',
      });

      setSaving(false);
    },
  });

  /**update plan Marketing */
  const [updatePlanMarketing] = useUpdate_Plan_MarketingMutation({
    onCompleted: () => {
      notify({
        message: 'Le plan Marketing a été modifié avec succès !',
        type: 'success',
      });
      refetchStatsVar(true);

      setSaving(false);
      setSaved(true);
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du plan Marketing',
        type: 'error',
      });

      setSaving(false);
    },
  });

  /**delete Plan Marketing */
  const [deletePlanMarketing] = useDelete_One_Plan_MarketingMutation({
    onCompleted: () => {
      notify({
        message: 'Le plan Marketing a été supprimé avec succès !',
        type: 'success',
      });
      refetchStatsVar(true);
      loadingPlanMarketing.refetch && loadingPlanMarketing.refetch();
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression du plan Marketing',
        type: 'error',
      });
    },
  });

  const [createPlanMarketingFichier] = useCreate_One_Prt_Plan_Marketing_FichierMutation({
    client: federation,
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'Ajout du fichier a été réussi.',
      });
      loadingPlanMarketing.refetch && loadingPlanMarketing.refetch();
    },
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur est survenue lors de l'ajout du fichier.`,
      });
    },
  });

  //Change Statut
  const [changePlanStatut] = useChange_Plan_Marketing_StatusMutation({
    onCompleted: () => {
      notify({
        message: 'Le statut a été modifié avec succès !',
        type: 'success',
      });
      loadingPlanMarketing.refetch && loadingPlanMarketing.refetch();
      setSavingStatut(false);
      setSavedStatut(true);
      refetchStatsVar(true);
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant le changement du statut',
        type: 'error',
      });
    },
  });

  const [loadContact, loadingContact] = useGet_Prt_ContactLazyQuery();

  const loadingReglementMode = useGet_Prt_Reglement_ModeQuery();

  const [sendEmailTrade, sendingEmailTrade] = useSend_Email_Trade_ManuelLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
    onCompleted: (data) => {
      notify({
        type: sendingEmailTrade.data?.sendEmailTradeManuel.statut as any,
        message: sendingEmailTrade.data?.sendEmailTradeManuel.message,
      });
    },
    onError: (error) => {
      notify({
        type: 'error',
        message: sendingEmailTrade.data?.sendEmailTradeManuel.message || 'une erreur est survenue',
      });
    },
  });

  const idLaboExist = idLabo ? idLabo[0] : undefined;

  useEffect(() => {
    if (idLaboExist) {
      loadLabo({
        variables: {
          id: idLaboExist,
          idPharmacie: pharmacie.id,
        },
      });
    }
  }, [idLaboExist]);

  useEffect(() => {
    if (sendingEmailTrade.loading) {
      notify({
        type: 'success',
        message: `En cours d'envoi d'email`,
      });
    }
  }, [sendingEmailTrade]);

  const handleFetchMorePlans = () => {
    const fetchMore = loadingPlanMarketing.fetchMore;
    const variables = loadingPlanMarketing.variables;
    const paging = variables?.paging;
    if (loadingPlanMarketing.data?.pRTPlanMarketings.nodes) {
      fetchMore &&
        fetchMore({
          variables: {
            ...variables,
            paging: {
              ...paging,
              offset: loadingPlanMarketing.data?.pRTPlanMarketings.nodes,
            },
          },

          updateQuery: (prev: any, { fetchMoreResult }) => {
            if (prev?.pRTPlanMarketings && fetchMoreResult?.pRTPlanMarketings.nodes) {
              return {
                ...prev,
                pRTPlanMarketings: {
                  ...prev.pRTPlanMarketings,
                  nodes: [...prev.pRTPlanMarketings.nodes, ...fetchMoreResult.pRTPlanMarketings.nodes],
                },
              };
            }

            return prev;
          },
        });
    }
  };

  const handleRequestSearchProduits = (
    change?: TableChange | null,
    ids?: string[],
    cataloguePharmacie?: boolean,
    idPartenaireTypeAssocie?: string
  ) => {
    if (!activateCatalogueProduits) {
      return;
    }
    console.log('****************Request search');
    let must: any = [
      {
        term: {
          'commandeCanal.code': 'PFL',
        },
      },
      {
        term: {
          'produit.produitTechReg.laboExploitant.id': idPartenaireTypeAssocie || '',
        },
      },
    ];

    if (typeof cataloguePharmacie === 'undefined' || cataloguePharmacie) {
      must.push({ terms: { 'produit.pharmacieReferences': [pharmacie.id] } });
      setUseCataloguePharmacie(true);
    } else {
      setUseCataloguePharmacie(false);
    }

    if (ids && ids.length) {
      must.push({
        terms: {
          _id: ids,
        },
      });
    }

    if (change) {
      const searchText = change.searchText.replace(/\s+/g, '\\ ').replace('/', '\\/');
      if (searchText) {
        must.push({
          query_string: {
            query: searchText.startsWith('*') || searchText.endsWith('*') ? searchText : `*${searchText}*`,
            fields: [],
            analyze_wildcard: true,
          },
        });
      }
    }

    const variables: Search_Plan_Marketing_Produit_CanalsQueryVariables = {
      type: ['produitcanal'],
      query: {
        query: {
          bool: {
            must,
          },
        },
      },
      take: change?.take || 12,
      skip: change?.skip || 0,
    };

    if (ids && ids.length) {
      loadPlanMarketingProduits({ variables });
    }

    //if (change) {
    loadProduits({ variables });
    //}
  };

  const saveplanMarketing = (planMarketing: any, fichiers: any) => {
    setItemEmail(planMarketing);
    if (planMarketing.id) {
      updatePlanMarketing({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          id: planMarketing.id,
          input: {
            titre: planMarketing.titre,
            description: planMarketing.description,
            dateDebut: moment(planMarketing.dateDebut).startOf('day').utc(true).format(),
            dateFin: moment(planMarketing.dateFin).startOf('day').utc(true).format(),
            partenaireType: 'LABORATOIRE',
            idPartenaireTypeAssocie: planMarketing.idPartenaireTypeAssocie,
            idType: planMarketing.idType,
            idStatut: '',
            fichiers,
            idMiseAvants: planMarketing?.idMiseAvants || null,
            produits:
              (planMarketing?.produits || []).map((element: any) => {
                return {
                  idProduit: element?.produit?.id,
                  quantitePrevue: parseFloat(element?.produit?.quantitePrevue),
                  prixUnitaire: parseFloat(element?.produit?.prixUnitaire),
                  remisePrevue: parseFloat(element?.produit?.remisePrevue),
                  quantiteRealise: parseFloat(element?.produit?.quantiterealise),
                };
              }) || null,
            montant: parseFloat(planMarketing?.montant),
            remise: parseFloat(planMarketing?.remise),
            typeRemuneration: planMarketing.typeRemuneration,
            remuneration: planMarketing.remuneration,
            estRemiseArriere: planMarketing.estRemiseArriere || false,
            idReglementLieuDeclaration: planMarketing.idReglementLieuDeclaration || null,
            idReglementModeDeclaration: planMarketing.idReglementModeDeclaration || null,
            idReglementType: planMarketing.idReglementType || null,
            montantStatutRealise: planMarketing?.montantStatutRealise
              ? parseFloat(planMarketing?.montantStatutRealise)
              : undefined,
            montantStatutCloture: planMarketing?.montantStatutCloture
              ? parseFloat(planMarketing?.montantStatutCloture)
              : undefined,
            precedentMontantRealise: planMarketing?.precedentMontantRealise
              ? parseFloat(planMarketing?.precedentMontantRealise)
              : undefined,
          },
        },
      });
    } else {
      createPlanMarketing({
        variables: {
          idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
          input: {
            titre: planMarketing.titre,
            description: planMarketing.description,
            dateDebut: moment(planMarketing.dateDebut).startOf('day').utc(true).format(),
            dateFin: moment(planMarketing.dateFin).startOf('day').utc(true).format(),
            partenaireType: 'LABORATOIRE',
            idPartenaireTypeAssocie: planMarketing.idPartenaireTypeAssocie,
            idType: planMarketing.idType,
            fichiers,
            idStatut: '',
            idMiseAvants: planMarketing?.idMiseAvants,
            produits:
              (planMarketing?.produits || []).map((element: any) => {
                return {
                  idProduit: element.produit.id,
                  quantitePrevue: parseFloat(element.produit.quantitePrevue) || 0,
                  prixUnitaire: parseFloat(element.produit.prixUnitaire) || 0,
                  remisePrevue: parseFloat(element.produit.remisePrevue) || 0,
                  quantiteRealise: parseFloat(element.produit.quantiteRealise) || 0,
                };
              }) || null,
            montant: parseFloat(planMarketing?.montant) || 0,
            remise: parseFloat(planMarketing.remise),
            typeRemuneration: planMarketing.typeRemuneration,
            remuneration: planMarketing.remuneration,
            estRemiseArriere: planMarketing.estRemiseArriere || false,
            idReglementLieuDeclaration: planMarketing.idReglementLieuDeclaration || null,
            idReglementModeDeclaration: planMarketing.idReglementModeDeclaration || null,
            idReglementType: planMarketing.idReglementType || null,
            precedentMontantRealise: planMarketing?.precedentMontantRealise
              ? parseFloat(planMarketing?.precedentMontantRealise)
              : undefined,
          },
        },
      });
    }
  };

  // Plan marketing Props
  const handlePlanSave = (planMarketing: any) => {
    setSaving(true);
    setSaved(false);

    if ((planMarketing?.selectedFiles?.length || 0) > 0) {
      uploadFiles(planMarketing.selectedFiles, {
        directory: 'relation-fournisseurs/plan-trade-marketing',
      })
        .then((uploadedFiles) => {
          saveplanMarketing(planMarketing, uploadedFiles);
        })
        .catch((error) => {
          setSaving(false);
          notify({
            type: 'error',
            message: `Une erreur s'est produite lors de chargement des fichiers.`,
          });
        });
    } else {
      saveplanMarketing(planMarketing, undefined);
    }
  };

  // Plan to edit
  const handlePlanEdit = (planMarketing: any) => {
    setPlanToEdit(planMarketing);
  };

  // Delete
  const handlePlanDelete = (planMarketing: any) => {
    deletePlanMarketing({
      variables: {
        input: { id: planMarketing.id },
      },
    });
  };

  // Recherche
  const handlePlanSearch = ({ skip, take, searchText, sortTable, selectedDateDebut, selectedDateFin }: any) => {
    redirectTo({
      ...params,
      skip,
      take,
      searchText,
      dateDebut: moment.utc(selectedDateDebut).format('YYYY-MM-DD'),
      dateFin: moment.utc(selectedDateFin).format('YYYY-MM-DD'),
    });
    //setSortTable(sortTable);
  };

  // Changement de Statut
  const handlePlanChangeStatut = (id: string, idStatut: string) => {
    const statut = planMarketingStatut.data?.pRTPlanMarketingStatuts.nodes.find((el) => el.id === idStatut);
    console.log('idPlan: ', id);
    if (statut) {
      changePlanStatut({
        variables: {
          id,
          input: {
            statut: {
              libelle: statut.libelle,
              code: statut.code,
            },
          },
        },
      });
    }
  };

  const handleGoBack = () => {
    history.goBack();
  };

  // Plan Search
  const onRequestPlanSearch = () => {
    const validPeriode = dateDebut && dateFin && moment(dateDebut).isBefore(dateFin);
    console.log('validPeriode', validPeriode, dateDebut, dateFin);
    const currentYear = moment().year();
    console.log('validPeriode', validPeriode, dateDebut, dateFin);
    const filterAnd: PrtPlanMarketingFilter[] = [
      {
        partenaireType: {
          eq: 'LABORATOIRE',
        },
      },
      {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
      {
        dateDebut: {
          gte: validPeriode ? moment.utc(dateDebut).toISOString() : `${currentYear}-01-01`,
        },
      },
      {
        dateDebut: {
          lte: validPeriode ? moment.utc(dateFin).toISOString() : `${currentYear}-12-31`,
        },
      },
      {
        estRemiseArriere: {
          is: !!estRemiseArriere,
        },
      },
      {
        or: [
          {
            titre: {
              iLike: `%${searchTextPlan}%`,
            },
          },
          {
            description: {
              iLike: `%${searchTextPlan}%`,
            },
          },
        ],
      },
    ];

    if (idLabo?.length)
      filterAnd.push({
        idPartenaireTypeAssocie: { in: idLabo },
      });

    if (idCategorie) {
      filterAnd.push({
        idCategorie: { eq: idCategorie },
      });
    }

    if (idStatut?.length) filterAnd.push({ idStatut: { in: idStatut } });

    if (idType?.length) filterAnd.push({ idType: { in: idType } });

    if (idReglementModeDeclaration?.length)
      filterAnd.push({ idReglementModeDeclaration: { in: idReglementModeDeclaration } });

    if (idReglementType?.length) filterAnd.push({ idReglementType: { in: idReglementType } });

    const sortingFilter: PrtPlanMarketingSort[] = sorting || [
      {
        field: PrtPlanMarketingSortFields.DateDebut,
        direction: SortDirection.Asc,
      },
      {
        field: PrtPlanMarketingSortFields.DateFin,
        direction: SortDirection.Asc,
      },
    ];

    // sorting
    //   ? sorting
    //   : sortColumn === 'periode'
    //   ? [
    //       {
    //         field: 'dateDebut',
    //         direction: sortTable.direction,
    //       },
    //       {
    //         field: 'dateFin',
    //         direction: sortTable.direction,
    //       },
    //     ]
    //   : [
    //       {
    //         field: sortTable.column,
    //         direction: sortTable.direction,
    //       },
    //     ];

    // idPharmacie
    loadPlanMarketing({
      variables: {
        idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
        filter: {
          and: filterAnd,
        },
        paging: {
          offset: skip,
          limit: take,
        },
        sorting: sortingFilter,
      },
    });
  };

  const saveChangeStatut = ({ operation, idPlanMarketing, type, fichiers, produits }: any) => {
    const statut = planMarketingStatut.data?.pRTPlanMarketingStatuts.nodes.find((el) => el.code === type);
    const modeReglement = loadingReglementMode.data?.pRTReglementModes.nodes.find(
      (el) => el.code === operation?.idModeReglement
    );
    if (statut) {
      changePlanStatut({
        variables: {
          id: idPlanMarketing,
          input: {
            statut: {
              libelle: statut.libelle,
              code: statut.code,
            },
            descriptionStatutRealise: operation?.description,
            dateStatutCloture: moment(operation?.date).toISOString(),
            dateStatutRealise: moment(new Date()).toISOString(),
            montantStatutCloture: parseFloat(operation?.montant) || 0,
            montantStatutRealise: parseFloat(operation?.montantStatutRealise) || 0,
            objetStatutFacture: valueFacture?.objet || '',
            messageStatutFacture: valueFacture?.message || '',
            idModeReglement: modeReglement?.id,
            fichiers,
            produits:
              type === 'REALISE'
                ? (produits || []).map((element: any) => {
                    return {
                      id: element.id,
                      produit: {
                        id: element.produit.id,
                        prixUnitaire: element.produit.prixUnitaire,
                        quantitePrevue: element.produit.quantitePrevue,
                        quantiteRealise: element.produit.quantiteRealise,
                        remisePrevue: element.produit.remisePrevue,
                      },
                    };
                  })
                : [],
            idContact:
              (valueFacture.recepteurs || []).map((element) => {
                return element.id;
              }) || [],
          },
        },
      });
    }
  };

  const handleSubmitOperationFacture = (idPlanMarketing: any) => {
    setSaving(true);
    setSaved(false);
    if (valueFacture.selectedFiles.length > 0) {
      uploadFiles(valueFacture.selectedFiles, {
        directory: 'relation-fournisseurs/plan-trade-marketing',
      })
        .then((uploadedFiles) => {
          saveChangeStatut({
            operation: undefined,
            idPlanMarketing,
            type: 'FACTURE',
            fichiers: uploadedFiles,
            produits: undefined,
          });
        })
        .catch((error) => {
          setSaving(false);
          notify({
            type: 'error',
            message: `Une erreur s'est produite lors de chargement des fichiers`,
          });
        });
    }
  };

  const handleSubmitOperationStatut = ({ operation, fichiers, idPlanMarketing, type, produits }: any) => {
    if (type === 'FACTURE') {
      handleSubmitOperationFacture(idPlanMarketing);
      return;
    }
    if (fichiers?.length > 0) {
      uploadFiles(fichiers, {
        directory: 'relation-fournisseurs/operation-trade-marketing',
      })
        .then((uploadedFiles) => {
          saveChangeStatut({
            operation,
            idPlanMarketing,
            type,
            fichiers: uploadedFiles,
            produits,
          });
        })
        .catch((error) => {
          notify({
            type: 'error',
            message: `Une erreur s'est produite lors de chargement des fichiers`,
          });
        });
    } else {
      saveChangeStatut({ operation, idPlanMarketing, type, fichiers: undefined, produits });
    }
  };

  const handleSaveFile = (files: any[], id: string) => {
    uploadFiles(files)
      .then((uploadedFiles) => {
        createPlanMarketingFichier({
          variables: {
            input: {
              idPlanMarketing: id,
              fichiers: uploadedFiles,
            },
          },
        });
        setSavedFichier(true);
      })
      .catch(() => {
        notify({
          type: 'error',
          message: `Une erreur s'est produite lors du chargement du fichier.`,
        });
      });
  };

  const handleOpenListModal = () => {
    loadContact({
      variables: {
        filter: {
          and: [
            {
              partenaireType: {
                eq: 'LABORATOIRE',
              },
            },
            {
              or: idLabo?.length ? idLabo.map((id) => ({ idPartenaireTypeAssocie: { eq: id } })) : [],
            },
            {
              idPharmacie: {
                eq: pharmacie.id,
              },
            },
          ],
        },
      },
    });
  };

  const handleSaveFacture = (value: any) => {
    setValueFacture(value);
  };

  const isFactureOperationValid = () => {
    return (
      valueFacture.message !== '' &&
      valueFacture.objet !== '' &&
      valueFacture.recepteurs.length > 0 &&
      valueFacture.selectedFiles.length > 0
    );
  };

  const handleSendEmail = (item: PrtPlanMarketingInfoFragment) => {
    sendEmailTrade({
      variables: {
        input: {
          id: item.id,
          idCategorie: item.categorie?.id,
        },
      },
    });
  };

  const produits = {
    error: loadingProduits.error as any,
    loading: loadingProduits.loading as any,
    data: loadingProduits.data?.search?.data,
    rowsTotal: loadingProduits.data?.search?.total || 0,
    onRequestSearch: handleRequestSearchProduits,
    planMarketingProduits: loadingPlanMarketingProduits.data?.search?.data || [],
    pharmacieCatalogue: useCataloguePharmacie,
  };

  // Liste des statuts
  const listStatutPlan = planMarketingStatut.data?.pRTPlanMarketingStatuts.nodes || [];

  const laboratoirePlan = {
    plan: {
      error: loadingPlanMarketing.error as any,
      loading: loadingPlanMarketing.loading,
      totalCount: loadingPlanMarketing.data?.pRTPlanMarketings.totalCount || 0,
      data: loadingPlanMarketing.data?.pRTPlanMarketings.nodes || [] /*.map(pm => {
        // Changer le type de plan marketing
        const currentType = listTypePlan.data.find(tpm => tpm.code === pm.type.code);

        return currentType
          ? {
              ...pm,
              type: currentType,
            }
          : pm;
      })*/,
    },
    onRequest: {
      save: handlePlanSave,
      edit: handlePlanEdit,
      delete: handlePlanDelete,
      next: handlePlanSearch,
      changeStatut: handlePlanChangeStatut,
      fetchMore: handleFetchMorePlans,
      saveFile: handleSaveFile,
    },
    listType: listTypePlan,
    listStatutPlan,
    listModeReglement: loadingReglementMode.data?.pRTReglementModes.nodes,
    planToEdit: planToEdit,
    saving: saving,
    saved: saved,
    setSaved,
    produits,
    onRequestGoBack: handleGoBack,
    onRequestSubmitOperationStatut: handleSubmitOperationStatut,
    onSavedStatut: {
      saved: savedStatut,
      setSaved: (newSaved: boolean) => setSavedStatut(newSaved),
      saving: savingStatut,
      setSaving: setSavingStatut,
    },
    factureOperation: {
      isValid: isFactureOperationValid(),
    },
    handleSaveFacture,
    handleOpenListModal,
    recepteurs: {
      data: recepteur,
      error: loadingContact.error as any,
      loading: loadingContact.loading,
      type: 'LABORATOIRE',
      // id: idCurrentLabo,
      category: 'CONTACT',
      setRecepteur,
    },
    handleSendEmail,
    savingFichier,
    setSavingFichier,
    setSavedFichier,
    savedFichier,
  };

  return { onRequestPlanSearch, laboratoirePlan, partenaireAssocie: loadingLabo.data?.laboratoire };
};
