import { intersection } from 'lodash';
import { CSSProperties } from 'react';

/**
         *  {
              prestataire: 'AVENE 1',
              planMarketings: [
                { titre: 'STREPSILS', week: [1, 2, 3], color: '#00A745' },
                {
                  titre: 'NUROFEN',
                  week: [1, 2, 3],
                  color: '#63B8DD',
                },
                { titre: 'STREPSILS', week: [16, 17, 18, 19], color: '#00A745' },
              ],
            },

              devient

              [
                  {
                    prestataire: 'AVENE 1',
                    index:0,
                    planMarketings: [
                      { titre: 'STREPSILS', week: [1, 2, 3], color: '#00A745' },
                      { titre: 'STREPSILS', week: [16, 17, 18, 19], color: '#00A745' },
                    ],
                  },
                  {
                    prestataire: 'AVENE 1',
                    index:1,
                    planMarketings: [
                      {
                        titre: 'NUROFEN',
                        week: [1, 2, 3],
                        color: '#63B8DD',
                      },
                    ],
                  }


              ]

         */

export const formatPlanningData = (data: any[]) => {
  return data.reduce((formatted, item, groupIndex) => {
    const sortedPlanMarketings: any[] = [...item.planMarketings].sort((p1: any, p2: any) => p1.week[0] - p2.week[0]);

    let nonIntersectionItems: any[] = [
      {
        ...item,
        groupIndex,
        index: 0,
        planMarketings: [],
      },
    ];

    let index = 1;
    for (const planMarketing of sortedPlanMarketings) {
      /** if (intersection(prevWeek, currentWeek).length > 0) {
        rowsSpan++;
      } */

      // Rechercher l'item
      const nonIntersetionIndex = nonIntersectionItems.findIndex((noItersectionItem) => {
        for (const prevPlanMarketing of noItersectionItem.planMarketings) {
          if (intersection(prevPlanMarketing.week, planMarketing.week).length > 0) {
            return false;
          }
        }

        return true;
      });

      if (nonIntersetionIndex === -1) {
        nonIntersectionItems = [
          ...nonIntersectionItems,
          {
            ...item,
            prestataire: '',
            groupIndex,
            index,
            planMarketings: [planMarketing],
          },
        ];

        index++;
      } else {
        nonIntersectionItems[nonIntersetionIndex] = {
          ...nonIntersectionItems[nonIntersetionIndex],
          planMarketings: [...nonIntersectionItems[nonIntersetionIndex].planMarketings, planMarketing],
        };
      }
    }

    // Update rowspan first colonne

    nonIntersectionItems[0] = {
      ...nonIntersectionItems[0],
      rowSpan: index,
    };

    return [...formatted, ...nonIntersectionItems];
  }, []);
};

export const getCellStyle = (groupIndex: number): CSSProperties => {
  return {
    backgroundColor: groupIndex % 2 === 0 ? '#ebebeb' : '#f6f6f6',
    cursor: 'pointer',
  };
};

export const formatPlanningByType = (idType: string, data?: any[]): any[] => {
  return data
    ? data
        .map((item: any) => ({
          ...item,
          planMarketings: item.planMarketings?.filter((plan: any) => plan.planMarketing?.type?.id === idType),
        }))
        ?.filter((item: any) => item.planMarketings.length)
    : [];
};
