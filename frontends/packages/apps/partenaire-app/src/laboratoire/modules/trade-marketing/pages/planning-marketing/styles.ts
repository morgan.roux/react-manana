import { makeStyles, createStyles, Theme, lighten } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: 16,
      [theme.breakpoints.down('sm')]: {
        paddingTop: 0,
      },
      // backgroundColor: '#f8f8f8',
      // '@global': {
      //   '*::-webkit-scrollbar': {
      //     height: '0px !important',
      //   },
      // },
    },
    minSearch: {
      borderColor: '#ccc',
      height: 35,
      width: 150,
    },
    containerBox: {
      border: 'solid 16px white',
      borderRadius: 4,
    },
    title: {
      [theme.breakpoints.down('xs')]: {
        fontSize: 16,
      },
      fontSize: 19,
      fontWeight: 'bold',
      fontFamily: 'Roboto',
      color: '#27272F',
    },
    subToolbarContainer: {
      display: 'flex',
      '& > *:nth-child(2)': {
        marginLeft: 51,
      },
    },
    toolbarContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingBottom: 16,
      [theme.breakpoints.down('sm')]: {
        padding: 0,
      },
    },
    calendarContainer: {
      display: 'flex',
      marginLeft: 21,
      alignItems: 'center',
      '& .MuiTypography-root': {
        fontFamily: 'Roboto',
        fontSize: 14,
      },
    },
    calendar: {
      '& .MuiOutlinedInput-notchedOutline': {
        border: 'none',
      },
      '& .MuiInputBase-input.MuiOutlinedInput-input.MuiInputBase-inputAdornedEnd.MuiOutlinedInput-inputAdornedEnd': {
        display: 'none',
      },
    },
    grid: {
      border: 0,
      borderBottom: '1px solid rgba(0, 0, 0, 0.08) !important',
      borderRight: '1px solid rgba(0, 0, 0, 0.08) !important',
      '& .k-grid-header-wrap': {
        background: '#FFFFFF',
      },
      marginTop: 32,
    },
    columnCell: {
      padding: '0 8px !important',
      // height: 40,
      '& div': {
        // height: 26,
        borderRadius: 4,
        display: 'flex',
        alignItems: 'center',
        fontSize: 12,
        fontFamily: 'Roboto',
        padding: 8,
        marginTop: 4,
        color: '#FFFFFF',
      },
    },
    firstColumnHeader: {
      background: '#FFFFFF !important',
      '& > .k-link': {
        display: 'none !important',
      },
    },
    firstColumnCell: {
      fontFamily: 'Roboto',
      textAlign: 'left',
      textTransform: 'uppercase',
      padding: '8px 12px',
      fontSize: 14,
      border: '0 !important',
      borderLeft: '1px solid rgba(0, 0, 0, 0.08) !important',
    },
    columnHeaderCell: {
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 14,
      fontFamily: 'Roboto',
    },
    subColumnHeaderContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      fontFamily: 'Roboto',
      '& .MuiTypography-root:nth-child(1)': {
        fontWeight: 'bold',
        fontSize: 14,
      },
      '& .MuiTypography-root:nth-child(2)': {
        fontSize: 14,
        color: '#42424261',
      },
    },
    headerColumn: {
      borderTop: '1px solid rgba(0, 0, 0, 0.08) !important',
    },
    displayNone: {
      display: 'none',
    },
    toolBarChartMobile: {
      display: 'block !important',
      '&>div': {
        display: 'inline-flex',
      },
      '&>div:not(:last-child)': {
        marginLeft: '0 !important',
      },
      '&>div:first-child': {
        width: '100%',
      },
      '&>div:not(:first-child)': {
        marginTop: '15px !important',
      },
    },
    selectContainer: {
      '& .MuiFormControl-root': {
        margin: 10,
      },
      margin: 10,
    },
    selectPilotageMobile: {
      marginBottom: 0,
      padding: '0 16px',
      marginTop: 15,
      '& *': {
        margin: '0 !important',
        border: 'none !important',
      },
    },
    gridBox: {},
    btnAction: {
      padding: '10px !important',
      '&.MuiButton-containedSecondary': {
        background: theme.palette.primary.main,
      },
      [theme.breakpoints.up('sm')]: {
        marginTop: 0,
      },
    },
    vue: {
      paddingRight: 16,
      cursor: 'pointer',
    },
    active: {
      color: theme.palette.primary.main,
      background: lighten(theme.palette.secondary.light, 0.9),
    },
    fabPlanningMobile: {
      position: 'fixed',
      bottom: 80,
      right: 10,
      zIndex: 999999999,
    },
    innerTitle: {
      fontSize: 18,
      fontWeight: 500,
    },
    boxMobilePlan: {
      background: '#f8f8f8',
      padding: '10px 20px',
      borderRadius: 10,
    },
  })
);
