import { useApplicationContext } from '@lib/common';
import {
  Laboratoire,
  PrtPlanMarketingInfoFragment,
  useGet_LaboratoireLazyQuery,
  usePlan_Marketing_CategoriesQuery,
} from '@lib/common/src/federation';
import { CustomGrid, CustomSelect, NewCustomButton, SmallLoading } from '@lib/ui-kit';
import TableChartSubToolbar from '@lib/ui-kit/src/components/molecules/TableChartSubToolbar';
import { getDaysInAMonth, getMonthNumber, getMonths } from '@lib/ui-kit/src/services/Helpers';
import { Box, Button, Divider, IconButton, Typography } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import { GridColumn } from '@progress/kendo-react-grid';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import { PlanningPlanMarketingDetails } from '../../components/details-trade-marketing';
import { usePlanningPlanMarketing } from '../../hooks/usePlanningPlanMarketing';
import { formatPlanningData, getCellStyle } from '../../hooks/util';
import { useStyles } from './styles';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import PilotagePlanMarketing from './../../../pilotage/component/PilotagePlanMarketing';
import { useQueryPilotage } from './../../../pilotage/utils/utils';
import moment from 'moment';
import Fullscreen from '@material-ui/icons/Fullscreen';
import FullscreenExit from '@material-ui/icons/FullscreenExit';

import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import { SwiperSlide } from 'swiper/react';
import { ReactNode } from 'react';

const PlanningPlanMarketing: FC<{
  topBarComponent?: ReactNode;
  containerClasses?: any;
}> = ({ topBarComponent, containerClasses }) => {
  const classes = useStyles();
  // const searchText = useReactiveVar<string>(searchTextVar);

  const { isMobile, currentPharmacie: pharmacie } = useApplicationContext();

  const { push } = useHistory();
  const { pathname } = useLocation();
  const isInPilotage = pathname.includes('/pilotages');
  const { params, redirectTo } = useLaboratoireParams('planning');
  const idLabo = params.idLabo?.length ? params.idLabo[0] : '';
  const { idCategorie } = params;

  const [date, setDate] = useState<any>(moment().year());
  const [openDetail, setOpenDetail] = useState<boolean>(false);
  const [planMarketing, setPlanMarketing] = useState<PrtPlanMarketingInfoFragment>();
  const [formattedData, setFormattedData] = useState<any[]>();

  const [activeListItem, setActiveListeItem] = useState('1');
  const [laboratoire, setLaboratoire] = useState<Laboratoire>();
  const [selectedDateDebut, setSelectedDateDebut] = React.useState<Date>(moment().startOf('year').toDate());
  const [selectedDateFin, setSelectedDateFin] = React.useState<Date>(moment().endOf('year').toDate());

  const [showPilotageMarketing, setshowPilotageMarketing] = useState(false);
  const [onRequestPlanning, data, loading, onRequestProduits, produitData, onRequestChangeStatutAction] =
    usePlanningPlanMarketing();

  const produits = {
    loading: produitData.loading,
    error: produitData.error,
    data: (produitData.data?.search?.data || []).map((produit: any) => ({
      code: produit.produit?.produitCode?.code || '',
      famille: produit.produit?.famille?.libelleFamille || '',
      nom: produit.produit?.libelle || '',
    })),
  };

  const loadPlanCategories = usePlan_Marketing_CategoriesQuery();

  const [loadLaboratoire, loadingLaboratoire] = useGet_LaboratoireLazyQuery({
    onCompleted: (data) => {
      if (data.laboratoire) {
        setLaboratoire(data.laboratoire as Laboratoire);
      }
    },
  });

  const listViewPilotages = [
    {
      code: 'PILOTAGES',
      libelle: `${laboratoire ? `Pilotage Labo / ${laboratoire.nom}` : 'Pilotage des Labos'}`,
    },
    {
      code: 'PLAN_MARKETING_GLOBAL',
      libelle: `Plan marketing ${laboratoire ? `/ ${laboratoire.nom}` : 'global'}`,
    },
    {
      code: 'PILOTAGE_REMUNERATIONS',
      libelle: `Pilotage des rémunérations ${laboratoire ? `/ ${laboratoire.nom}` : 'global'}`,
    },
    {
      code: 'SUIVI_REMUNERATIONS',
      libelle: `${laboratoire ? `Historique des Rémunérations / ${laboratoire.nom}` : 'Historique des Rémunérations'}`,
    },
  ];

  useEffect(() => {
    if (idLabo) {
      loadLaboratoire({
        variables: { id: idLabo, idPharmacie: pharmacie.id },
      });
    }
  }, [idLabo]);

  useEffect(() => {
    setActiveListeItem(idCategorie || 'TOUS');
  }, [idCategorie]);

  const handleListeClick = (id: string) => {
    redirectTo({ idCategorie: id === 'TOUS' ? undefined : id });
  };

  const handleOpenDetail = () => {
    setOpenDetail((prev) => !prev);
  };

  useEffect(() => {
    if (onRequestPlanning) {
      onRequestPlanning({ searchText: '', date: new Date().setFullYear(date) });
    }
  }, [idLabo]);

  useEffect(() => {
    if (!loading && data) {
      setFormattedData(formatPlanningData(data));
    }
    if (planMarketing && data && !loading) {
      const active = data
        .map((el: any) => el.planMarketings)
        .find((el: any) => el.planMarketing?.id === planMarketing.id);

      if (active) setPlanMarketing(active);
    }
  }, [loading]);

  const headersData = getMonths().map((month) => ({
    month,
    weeks: getDaysInAMonth(date, getMonthNumber(month) - 1),
  }));
  const headers = headersData.map((item, index) => {
    return {
      ...item,
      weeks: item.weeks.filter(
        (week) =>
          index === headersData.length - 1 || !headersData[index + 1].weeks.some((weekIt) => week.week === weekIt.week)
      ),
    };
  });

  const handleOpenModalDetail = (plan: any, prestataire: any) => {
    setOpenDetail(true);
    setPlanMarketing(plan);
    setLaboratoire(prestataire);
    onRequestProduits &&
      onRequestProduits(
        plan?.idPartenaireTypeAssocie,
        (plan.produits || []).map(({ id }: any) => id)
      );
  };

  const handleOpenPlan = () => {
    redirectTo({ ...params }, 'plan');
  };

  const handleOpenAdd = () => {
    push(`/laboratoires/plan-form?id=new${idLabo ? `&idLabo=${idLabo}` : ``}`);
  };

  const columnWithChildren = (title: string, subTitles: { range: string; week: number }[]) => {
    return (
      <GridColumn
        headerCell={(props) => <ColumnHeaderCell {...props} {...{ title }} />}
        children={subTitles.map((subTitle) => (
          <GridColumn
            width="90px"
            resizable
            // resizableany
            minResizableWidth={90}
            headerCell={(props) => <SubColumnHeaderCell {...props} {...subTitle} />}
            cell={(props) => (
              <ColumnCell
                {...props}
                {...{
                  week: subTitle.week,
                  onClick: handleOpenModalDetail,
                }}
              />
            )}
          />
        ))}
        headerClassName={classes.headerColumn}
      />
    );
  };

  const ColumnCell = (props: any) => {
    const { dataItem, week, onClick } = props;
    const weeks = dataItem?.planMarketings || [];
    const element = weeks.find((el: any) => el.week.includes(week));
    const activeElement = element?.week.includes(week) ? element : undefined;
    const plan = element?.planMarketing;
    return element?.week[0] === week ? (
      <td
        style={getCellStyle(dataItem.groupIndex)}
        className={classes.columnCell}
        onClick={() => onClick(plan, dataItem?.prestataire)}
        colSpan={activeElement ? element?.week.length : undefined}
      >
        <div
          style={{ background: element.color || 'gray' }}
        >{`${activeElement?.planMarketing?.type?.libelle} ${activeElement?.titre}`}</div>
      </td>
    ) : element?.week.includes(week) ? null : (
      <td style={getCellStyle(dataItem.groupIndex)} />
    );
  };

  const ColumnHeaderCell = (props: any) => {
    const { title } = props;
    return <Typography className={classes.columnHeaderCell}>{title}</Typography>;
  };

  const SubColumnHeaderCell = (props: any) => {
    const { week, range } = props;
    return (
      <Box className={classes.subColumnHeaderContainer}>
        <Typography>{`S${week}`}</Typography>
        <Typography>{range}</Typography>
      </Box>
    );
  };

  const firstColumn = (
    <GridColumn
      locked
      headerClassName={`${classes.firstColumnHeader} ${isMobile ? classes.displayNone : ''}`}
      className={classes.firstColumnCell}
      width="180px"
      resizable={true}
      minResizableWidth={180}
      cell={(props: any) => {
        return props.dataItem.index === 0 ? (
          <td {...props} rowSpan={props.dataItem.rowSpan}>
            <div>{props.dataItem.prestataire?.nom}</div>
          </td>
        ) : null;
      }}
    />
  );

  const columns = headers.map((el) =>
    columnWithChildren(
      el.month,
      el.weeks.map((week) => ({ range: `${week.start}-${week.end}`, week: week.week }))
    )
  );

  const handleTableChartSubToolbarChange = (searchText: string, date: any) => {
    if (onRequestPlanning) {
      onRequestPlanning({ searchText: '', date });
    }
    setDate(moment(date).year());
    setSelectedDateDebut(moment(date).startOf('year').toDate());
    setSelectedDateFin(moment(date).endOf('year').toDate());
  };

  const handleChangeViewPilotage = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.value === listViewPilotages[0].code)
      push(`/laboratoires/${idLabo}/pilotage${idLabo && idLabo !== 'list' ? '' : 's'}/pilotage`);
    if (event.target.value === listViewPilotages[2].code)
      push(`/laboratoires/${idLabo}/pilotage${idLabo && idLabo !== 'list' ? '' : 's'}/suivi`);
  };

  const bannerRightComponent = (
    <Box className={classes.selectContainer}>
      <CustomSelect
        list={listViewPilotages}
        index="libelle"
        listId="code"
        label=""
        value={listViewPilotages[1].code}
        onChange={handleChangeViewPilotage}
        required
        style={{ height: 50, background: 'white', margin: 10 }}
      />
    </Box>
  );

  console.log('formatted data', formattedData);
  const listButton = [
    { id: 'TOUS', code: 'TOUS', libelle: 'Tous' },
    ...(loadPlanCategories.data?.pRTPlanMarketingCategories.nodes || []),
  ];

  const { dataPilotagePlanMarketing, onRequestPilotagePlanMarketing, planningDetail } = useQueryPilotage({
    filterAnneeComparer: new Date().getFullYear(),
    startDate: moment(new Date()).add(-4, 'month'),
    endDate: moment(new Date()),
    skipSuiviRemuneration: 0,
    takeSuiviRemuneration: 12,
  });
  const handleChangeToPilotages = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    setshowPilotageMarketing(!showPilotageMarketing);
  };

  const handleDateDebutChange = (dateDebut: Date) => {
    setSelectedDateDebut(dateDebut);
    if (dateDebut && selectedDateFin && selectedDateFin > dateDebut) {
      setDateTrade(false);
      if (onRequestPlanning) {
        onRequestPlanning({ searchText: '', dateDebut: dateDebut, dateFin: selectedDateFin });
      }
    }
  };

  const handleDateFinChange = (dateFin: Date) => {
    setSelectedDateFin(dateFin);
    if (dateFin && selectedDateDebut && dateFin > selectedDateDebut) {
      setDateTrade(false);
      if (onRequestPlanning) {
        onRequestPlanning({ searchText: '', dateDebut: selectedDateDebut, dateFin: dateFin });
      }
    }
  };

  const [dateTrade, setDateTrade] = React.useState<boolean>();
  const handleTradeChange = () => {
    setDateTrade(!dateTrade);
  };

  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('lg'));

  return (
    <>
      {/* {isMobile ? <div className={classes.selectPilotageMobile}>{bannerRightComponent}</div> : null} */}
      <Box className={containerClasses?.root || classes.root}>
        <Box className={containerClasses?.containerBox || classes.containerBox}>
          {loading && <SmallLoading />}
          {topBarComponent}
          <Box className={classes.toolbarContainer}>
            {/* <Typography className={classes.title}>{title || `Trade Marketing`}</Typography> */}
            <TableChartSubToolbar
              withSearch={false}
              searchClassName={classes.minSearch}
              onRequestChange={handleTableChartSubToolbarChange}
              ToolBarBoxClassName={isMobile ? classes.toolBarChartMobile : ''}
              intervalDate={true}
              dateTrade={dateTrade}
              handleDateDebutChange={handleDateDebutChange}
              handleDateFinChange={handleDateFinChange}
              handleTradeChange={handleTradeChange}
              selectedDateDebut={selectedDateDebut}
              selectedDateFin={selectedDateFin}
            />

            {!isInPilotage && (
              <Box display="flex">
                <Box display="flex" alignItems="center">
                  <SwiperSlide>
                    {listButton.map((item) => (
                      <Button
                        onClick={() => {
                          handleListeClick(item.id);
                        }}
                        className={activeListItem && activeListItem === item.id ? classes.active : ''}
                      >
                        <Typography
                          style={{
                            textTransform: 'initial',
                            fontSize: '14px',
                            fontFamily: 'Roboto',
                          }}
                        >
                          {item.libelle}
                          {/* Du {`${moment(selectedDateDebut).format('DD-MM-YYYY')}`} au{' '}
                          {`${moment(selectedDateFin).format('DD-MM-YYYY')}`} */}
                        </Typography>
                      </Button>
                    ))}
                  </SwiperSlide>
                </Box>
                {!isMobile && (
                  <>
                    <NewCustomButton
                      className={classes.vue}
                      onClick={handleOpenPlan}
                      startIcon={<FormatListBulletedIcon />}
                      theme="transparent"
                      shadow={false}
                      style={{
                        textTransform: 'none',
                        fontWeight: 400,
                        marginRight: 24,
                        paddingInline: '10px !important',
                      }}
                    >
                      {/* <Event /> */}
                      {isMobile ? null : 'Vue liste'}
                    </NewCustomButton>

                    <NewCustomButton
                      variant="contained"
                      className={classes.btnAction}
                      startIcon={<Add />}
                      onClick={handleOpenAdd}
                    >
                      AJOUTER UNE OPÉRATION MARKETING
                    </NewCustomButton>
                  </>
                )}
              </Box>
            )}
          </Box>
          {isMobile && (
            <NewCustomButton
              variant="contained"
              className={classes.btnAction}
              startIcon={<Add />}
              theme="flatPrimary"
              onClick={handleOpenAdd}
              shadow={false}
              style={{ fontSize: 12 }}
            >
              AJOUTER UNE OPÉRATION MARKETING
            </NewCustomButton>
          )}
          <Box>
            <Box className={classes.gridBox}>
              {isMobile ? (
                <Box className={classes.boxMobilePlan}>
                  <Box display="flex" alignItems="center" justifyContent="space-between">
                    <Typography className={classes.innerTitle}>Mise en avant</Typography>
                    <IconButton onClick={handleChangeToPilotages} className={classes.innerTitle}>
                      {showPilotageMarketing ? <FullscreenExit /> : <Fullscreen />}
                    </IconButton>
                  </Box>
                  <Divider />
                  {showPilotageMarketing ? (
                    <PilotagePlanMarketing
                      data={dataPilotagePlanMarketing}
                      onRequest={() => {}}
                      hideFirstColumn
                      planningDetail={planningDetail}
                    />
                  ) : (
                    <CustomGrid
                      pageable={false}
                      groupable={false}
                      columns={!isMobile ? [firstColumn, ...columns] : columns}
                      filterable={false}
                      data={formattedData}
                      className={classes.grid}
                      resizable={true}
                    />
                  )}
                </Box>
              ) : (
                <>
                  <Box>
                    <Typography className={classes.innerTitle}>Mise en avant</Typography>
                  </Box>
                  <CustomGrid
                    pageable={false}
                    groupable={false}
                    columns={!isMobile ? [firstColumn, ...columns] : columns}
                    filterable={false}
                    data={formattedData}
                    className={classes.grid}
                    resizable={true}
                  />
                </>
              )}
            </Box>
          </Box>
          <PlanningPlanMarketingDetails
            planMarketing={planMarketing}
            open={openDetail}
            setOpen={handleOpenDetail}
            onRequestChangeStatutAction={onRequestChangeStatutAction}
            produits={produits}
            laboratoire={laboratoire as Laboratoire}
          />
        </Box>
      </Box>
      {/* </CustomContainer> */}
    </>
  );
};

export default PlanningPlanMarketing;
