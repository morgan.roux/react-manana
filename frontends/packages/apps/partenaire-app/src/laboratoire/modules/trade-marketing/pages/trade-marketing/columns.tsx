import { PrtPlanMarketingInfoFragment } from '@lib/common/src/federation';
import { Column } from '@lib/ui-kit';
import { Typography } from '@material-ui/core';
import { formatMoney } from '../../../../util';
import moment from 'moment';
import React from 'react';

export const useColumn = ({
  isToDownloadPdf,
  classes,
  listStatutPlan,
  handleChangeStatut,
  action,
  SelectComp,
}: any) => {
  const columns: Column[] = [
    {
      name: 'idType',
      label: 'Type',
      sortable: !isToDownloadPdf,
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        return <Typography className={classes.textTable}>{row?.type?.libelle ? row.type.libelle : '-'}</Typography>;
      },
    },
    {
      name: 'titre',
      label: 'Titre',
      sortable: !isToDownloadPdf,
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        return <Typography className={classes.textTable}>{row?.titre ? row?.titre : '-'}</Typography>;
      },
    },
    {
      name: 'montant',
      label: 'Rém. prévue',
      align: 'right',
      sortable: !isToDownloadPdf,
      style: { minWidth: 120 },
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        if (row?.typeRemuneration === 'SANS_REMUNERATION') {
          return <Typography className={classes.textTable}>-</Typography>;
        }
        return (
          <Typography style={{ textAlign: 'right' }} className={classes.textTable}>
            {formatMoney(row?.montant)} €
          </Typography>
        );
      },
    },
    {
      name: 'montantStatutRealise',
      label: 'Rém. réalisée',
      sortable: !isToDownloadPdf,
      align: 'right',
      style: { minWidth: 120 },
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        if (row?.typeRemuneration === 'SANS_REMUNERATION') {
          return <Typography className={classes.textTable}>-</Typography>;
        }
        return (
          <Typography style={{ textAlign: 'right' }} className={classes.textTable}>
            {formatMoney(row?.montantStatutRealise)} €
          </Typography>
        );
      },
    },
    {
      name: 'montantStatutCloture',
      label: 'Rém. payée',
      sortable: !isToDownloadPdf,
      align: 'right',
      style: { minWidth: 120 },
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        if (row?.typeRemuneration === 'SANS_REMUNERATION') {
          return <Typography className={classes.textTable}>-</Typography>;
        }
        return (
          <Typography style={{ textAlign: 'right' }} className={classes.textTable}>
            {formatMoney(row?.montantStatutCloture as any)} €
          </Typography>
        );
      },
    },
    {
      name: 'typeAssocie',
      label: 'Labo',
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        return (
          <Typography className={classes.textTable}>{row?.typeAssocie?.nom ? row?.typeAssocie?.nom : '-'}</Typography>
        );
      },
    },
    {
      name: 'dateDebut',
      label: 'Période',
      sortable: !isToDownloadPdf,
      style: { minWidth: 220 },
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        return (
          <Typography className={classes.textTable}>
            {row?.dateDebut && row?.dateFin
              ? `Du ${moment.utc(row?.dateDebut).format('DD/MM/YYYY')} - Au ${moment
                  .utc(row?.dateFin)
                  .format('DD/MM/YYYY')}`
              : '-'}
          </Typography>
        );
      },
    },
    {
      name: 'idReglementType',
      label: 'Règlement',
      sortable: !isToDownloadPdf,

      renderer: (row: PrtPlanMarketingInfoFragment) => {
        return (
          <Typography className={classes.textTable}>
            {row?.reglementType?.libelle ? row?.reglementType.libelle : '-'}
          </Typography>
        );
      },
    },
    {
      name: 'idReglementModeDeclaration',
      label: 'Déclaration',
      sortable: !isToDownloadPdf,
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        return (
          <Typography className={classes.textTable}>
            {row?.reglementModeDeclaration?.libelle ? row?.reglementModeDeclaration.libelle : '-'}
          </Typography>
        );
      },
    },
    {
      name: 'modeReglement.libelle',
      label: 'Paiement',
      sortable: false,
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        return (
          <Typography className={classes.textTable}>{row?.modeReglement ? row?.modeReglement.libelle : '-'}</Typography>
        );
      },
    },
    {
      name: 'statut',
      label: 'Statut',
      renderer: (row: PrtPlanMarketingInfoFragment) => {
        if (isToDownloadPdf) {
          return row.statut.libelle;
        }
        return (
          <SelectComp
            planMarketing={row || ''}
            statut={row.statut}
            statutList={listStatutPlan || []}
            onChange={handleChangeStatut}
          />
        );
      },
    },
  ];

  if (!isToDownloadPdf) {
    return [
      ...columns,
      {
        name: '',
        label: '',
        renderer: (row: PrtPlanMarketingInfoFragment) => {
          return action(row);
        },
      },
    ];
  }

  return columns;
};
