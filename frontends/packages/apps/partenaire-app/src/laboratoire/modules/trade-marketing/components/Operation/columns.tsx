// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable no-use-before-define */
// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable dot-notation */
import { PRTPlanMarketing } from '@lib/types';
import { Box, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import { computeMontantRemunerationRealisee } from '../plan-form/util';
import { Column, CustomFormTextField } from '@lib/ui-kit';

export const useColumn = (
  planMarketing: PRTPlanMarketing,
  rowsSelected: any[],
  setRowsSelected: (newRowsSelected: any[]) => void
) => {
  const [inputRealise, setInputRealise] = useState<{
    mode: string;
    index: number;
  }>({ mode: 'label', index: 0 });
  const handleInputRealise = (index?: number) => {
    setInputRealise({ mode: 'input', index: index || 0 });
  };
  const handleChangeOnBlur = (type: string) => {
    type === 'QU' && setInputRealise({ mode: 'label', index: -1 });
  };

  const updateSelectedRow = (canalProduitId: string, value: any) => {
    setRowsSelected(
      rowsSelected.map((element) => {
        if (element.produit.id === canalProduitId) {
          element.produit['quantiteRealise'] = value;
          element.produit['remiseRealise'] = computeMontantRemunerationRealisee(
            planMarketing.typeRemuneration || 'SANS_REMUNERATION',
            planMarketing.remise || 0,
            element
          );
        }
        return element;
      })
    );
  };

  const handleChangeNombre = ({ event, id }: any) => {
    updateSelectedRow(id, parseFloat(event.target.value));
  };

  const produitColumns: Column[] = [
    {
      name: 'produit.produitCode.code',
      label: 'Code',
      renderer: (value: any) => {
        return value?.produit?.produitCode?.code || '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        console.log('--------------value------: ', value);
        return value?.produit?.produitPhoto?.fichier?.publicUrl ? (
          <img style={{ maxHeight: 48 }} src={value?.produitPhoto?.fichier?.publicUrl} />
        ) : (
          '-'
        );
      },
    },
    {
      name: 'produit.libelle',
      label: 'Nom',
      renderer: (value: any) => {
        return value?.produit?.libelle || '-';
      },
    },
    {
      name: 'produit.famille.libelleFamille',
      label: 'Famille',
      renderer: (value: any) => {
        return value?.produit?.famille?.libelleFamille || '-';
      },
    },
  ];

  if (
    planMarketing.typeRemuneration === 'SANS_REMUNERATION' ||
    (planMarketing.typeRemuneration === 'REDUCTION_EURO' && planMarketing.remuneration === 'FORFAITAIRE')
  ) {
    produitColumns.push({
      name: `produit.quantite_realise`,
      label: 'Quantité(s) vendue(s)',
      renderer: (value: any, index?: number) => {
        console.log('----value operation realise--------: ', value);
        return (
          <>
            {inputRealise.mode === 'input' && inputRealise.index === index ? (
              <CustomFormTextField
                type="number"
                name="quantiteRealise"
                // defaultValue={value?.produit?.quantiteRealise}
                onChange={(event) => {
                  handleChangeNombre({
                    event,
                    id: value.produit?.id || '',
                  });
                }}
                value={value?.produit?.quantiteRealise}
                inputProps={{ min: 0 }}
                onBlur={() => handleChangeOnBlur('QU')}
              />
            ) : (
              <Box onClick={() => handleInputRealise(index)}>
                <Typography>{value?.produit?.quantiteRealise || 0}</Typography>
              </Box>
            )}
          </>
        );
      },
    });
  } else {
    produitColumns.push(
      {
        name: 'produit.reduction',
        label: 'Réduction unitaire',
        renderer: (value: any) => {
          return (
            <Box>
              <Typography>{value?.produit?.prixUnitaire || 0}</Typography>
            </Box>
          );
        },
      },
      {
        name: `produit.quantite_realise`,
        label: 'Quantité(s) vendue(s)',
        renderer: (value: any, index?: number) => {
          console.log('----value operation realise--------: ', value);
          return (
            <>
              {inputRealise.mode === 'input' && inputRealise.index === index ? (
                <CustomFormTextField
                  type="number"
                  name="quantiteRealise"
                  // defaultValue={value?.produit?.quantiteRealise}
                  onChange={(event) => {
                    handleChangeNombre({
                      event,
                      id: value.produit?.id || '',
                    });
                  }}
                  value={value?.produit?.quantiteRealise}
                  inputProps={{ min: 0 }}
                  onBlur={() => handleChangeOnBlur('QU')}
                />
              ) : (
                <Box onClick={() => handleInputRealise(index)}>
                  <Typography>{value?.produit?.quantiteRealise || 0}</Typography>
                </Box>
              )}
            </>
          );
        },
      },
      {
        name: 'produit.total',
        label: 'Total',
        style: {
          minWidth: 100,
          textAlign: 'right',
        },
        renderer: (value: any) => {
          return (value?.produit?.remiseRealise || 0).toFixed(2);
        },
      }
    );
  }

  return produitColumns;
};
