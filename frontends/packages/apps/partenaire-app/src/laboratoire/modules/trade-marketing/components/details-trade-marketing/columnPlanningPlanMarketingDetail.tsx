import { PrtPlanMarketingInfoFragment } from '@lib/common/src/federation';
import { Column } from '@lib/ui-kit';
import React from 'react';

export const ColumnPlanningPlanMarketingDetail = (planMarketing: PrtPlanMarketingInfoFragment) => {
  console.log('---------------------------planMarketing collumn: ', planMarketing);
  let columnProduit: Column[] = [
    {
      name: 'code',
      label: 'Code',
      renderer: (value: any) => {
        console.log('valuProduit', value);
        return value?.produit?.produitCode?.code || '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return value?.produit?.produitPhoto?.fichier?.publicUrl ? (
          <img style={{ maxHeight: 48 }} src={value?.produit?.produitPhoto?.fichier?.publicUrl} />
        ) : (
          '-'
        );
      },
    },
    {
      name: 'nom',
      label: 'Nom',
      renderer: (value: any) => {
        return value?.produit?.libelle || '-';
      },
    },
    {
      name: 'famille',
      label: 'Famille',
      renderer: (value: any) => {
        return value?.produit?.famille?.libelleFamille || '-';
      },
    },
  ];

  if (planMarketing?.typeRemuneration === 'SANS_REMUNERATION' || planMarketing?.remuneration === 'FORFAITAIRE') {
    columnProduit = [
      ...columnProduit,
      {
        name: `produit.quantite_prevue`,
        label: 'Quantité prévue',
        renderer: (value: any) => {
          return value?.produit?.quantitePrevue ?? '-';
        },
      },
    ];
  } else {
    columnProduit = [
      ...columnProduit,
      {
        name: 'prixUnitaire',
        label: 'PU',
        renderer: (value: any) => {
          return value?.produit?.prixUnitaire || '-';
        },
      },
      {
        name: `produit.quantite_prevue`,
        label: 'Quantité prévue',
        renderer: (value: any) => {
          return value?.produit?.quantitePrevue ?? '-';
        },
      },
      {
        name: 'produit.total',
        label: 'Total réduction',
        renderer: (value: any) => {
          return `${(value?.produit?.remisePrevue || 0).toFixed(2)} €`;
        },
      },
    ];
  }
  return columnProduit;
};
