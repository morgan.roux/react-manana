import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((_theme: Theme) =>
  createStyles({
    root: {
      fontFamily: 'Roboto',
      '& h2': {
        margin: '0 0 24px 0',
        fontSize: 18,
        fontWeight: 'bold',
      },
      '& h3': {
        margin: '8px 0 24px 0',
        fontSize: 16,
        fontWeight: 500 as any,
      },
      minWidth: '100%',
      [_theme.breakpoints.up('sm')]: {
        minWidth: 786,
      },
    },
    textFieldContainer: {
      width: '100%',
      margin: '0 0 16px 0',
    },
    label: {
      fontWeight: 500 as any,
      fontSize: 16,
      fontFamily: 'Roboto',
      color: '#212121',
    },
    statutContainer: {
      margin: '16px 0 40px 0',
      width: '100%',
    },
    remunerationContainer: {
      paddingLeft: 16,
      paddingTop: 8,
      marginBottom: 24,
      width: '100%',
      fontSize: 16,
      fontFamily: 'Roboto',
      color: '#212121',
    },
    remunerationCell: {
      borderColor: _theme.palette.common.white,
      paddingTop: 0,
      paddingBottom: 8,
      fontSize: 16,
      fontFamily: 'Roboto',
      color: '#212121',
    },
    todoContainer: {},
  })
);
