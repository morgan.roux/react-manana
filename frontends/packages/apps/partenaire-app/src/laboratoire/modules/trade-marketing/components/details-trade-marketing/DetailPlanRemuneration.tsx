import classNames from 'classnames';
import React, { FC } from 'react';
import { formatMoney } from '../../../../util';
import { PrtPlanMarketingInfoFragment } from '../../../../../../../../libs/common/src/federation';
import { useStyles } from './styles';
import { Box, TableCell, TableRow } from '@material-ui/core';
export interface DetailPlanRemunerationProps {
  planMarketing?: PrtPlanMarketingInfoFragment;
}

export const DetailPlanRemuneration: FC<DetailPlanRemunerationProps> = ({ planMarketing }) => {
  const classes = useStyles();
  return planMarketing && planMarketing.typeRemuneration !== 'SANS_REMUNERATION' ? (
    <>
      <h3 style={{ margin: 0 }}>Rémunération:</h3>
      <Box className={classes.remunerationContainer}>
        {planMarketing?.categorie?.code === 'CHALLENGE' && (
          <TableRow>
            <TableCell align="left" className={classes.remunerationCell}>
              Réalisée précédemment:
            </TableCell>
            <TableCell align="right" className={classNames(classes.label, classes.remunerationCell)}>
              {formatMoney(planMarketing?.precedentMontantRealise)} €
            </TableCell>
          </TableRow>
        )}

        <TableRow>
          <TableCell align="left" className={classes.remunerationCell}>
            Prévue:
          </TableCell>
          <TableCell align="right" className={classNames(classes.label, classes.remunerationCell)}>
            {formatMoney(planMarketing?.montant)} €
          </TableCell>
        </TableRow>

        <TableRow>
          <TableCell align="left" className={classes.remunerationCell}>
            Réalisée:
          </TableCell>
          <TableCell align="right" className={classNames(classes.label, classes.remunerationCell)}>
            {formatMoney(planMarketing?.montantStatutRealise)} €
          </TableCell>
        </TableRow>

        <TableRow>
          <TableCell align="left" className={classes.remunerationCell}>
            Payée:
          </TableCell>
          <TableCell align="right" className={classNames(classes.label, classes.remunerationCell)}>
            {formatMoney(planMarketing?.montantStatutCloture)} €
          </TableCell>
        </TableRow>
      </Box>
    </>
  ) : null;
};
