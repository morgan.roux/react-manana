import { PrtPlanMarketingInfoFragment, PartenaireAssocieUnion } from '@lib/common/src/federation';

export interface FormattedPlanningMarketing {
  prestataire?: PartenaireAssocieUnion | null;
  planMarketings: {
    week: number[];
    titre: string;
    color: string;
    planMarketing: PrtPlanMarketingInfoFragment;
  }[];
}
