import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: 'calc(100vh - 155px)',
      overflowY: 'auto',
    },
    form: {
      // height: 'calc(100vh - 260px)',
      overflowY: 'auto',
    },
    add: {
      position: 'absolute',
      right: 24,
      bottom: 24,
      zIndex: 999,
    },
    caption: {
      fontFamily: 'Roboto',
      color: '#9E9E9E',
      fontWeight: 'normal',
      fontSize: 12,
    },
    iconButton: {
      padding: 0,
    },
    formWidth: {
      [theme.breakpoints.up('md')]: {
        width: '75%',
      },
      [theme.breakpoints.down('md')]: {
        maxWidth: '100vw',
        padding: 20,
      },
      marginTop: 8,
    },
    formFullWidth: {
      width: '100%',
      [theme.breakpoints.down('md')]: {
        maxWidth: '100vw',
        padding: 20,
      },
      marginTop: 8,
    },
    scroll: {
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
    tableBox: {
      width: '100%',
      paddingLeft: 15,
      paddingRight: 15,
    },
    textTable: {
      fontFamily: 'Roboto',
      color: '#212121',
      fontWeight: 'normal',
      textAlign: 'left',
      fontSize: 14,
    },
    textTitle: {
      fontFamily: 'Roboto',
      color: '#424242',
      fontWeight: 'bold',
      textAlign: 'left',
      fontSize: 14,
    },
    textHead: {
      fontFamily: 'Roboto',
      fontSize: 16,
      marginTop: 16,
      marginBottom: 16,
    },
    subtitle: {
      fontFamily: 'Roboto',
      color: '#212121',
      fontWeight: '500' as any,
      textAlign: 'left',
      fontSize: 16,
    },
    titreComponent: {
      fontFamily: 'Roboto',
      color: '#424242',
      fontWeight: 'bold',
      textAlign: 'left',
      fontSize: 22,
    },
    topBarBox: {
      paddingBottom: 16,
    },
    miseAvant: {
      paddingLeft: 24,
      paddingBottom: 8,
      paddingTop: 8,
    },
    textPlus: {
      fontFamily: 'Roboto',
      color: '#f34f9c',
      fontWeight: 500,
      textAlign: 'right',
      fontSize: 16,
    },
    miseAvantBox: {
      paddingTop: 16,
      paddingBottom: 24,
    },
    planningBtn: {
      marginLeft: 8,
      order: 1,
      background: '#fff',
    },
    buttonLast: {
      order: 2,
    },
    info: {
      color: 'rgba(0,0,0,0.5)',
    },
    selectPadding10: {
      '& .MuiSelect-root': {
        padding: 6,
      },
    },
    fabPlanningMobile: {
      background: '#7F2CCB',
      left: 24,
      bottom: 90,
      position: 'absolute',
    },
    [theme.breakpoints.up('md')]: {
      tableSiblingPlanMarketing: {
        width: '70%',
      },
      formDirectChildren: {
        width: '65%',
        margin: 'auto',
      },
    },
    filterClassName: {
      display: 'block',
      width: '100%',
      '&>span': {
        float: 'right',
        marginTop: 10,
      },
    },
    filterClassNameForm: {
      display: 'flex',
      flexDirection: 'column',
      '&>div': {
        marginLeft: 0,
      },
    },
    searchContainerClassName: {
      maxWidth: 250,
      margin: 0,
    },
    strippedTR: {
      '& .MuiTableRow-root:nth-child(even)': {
        background: '#efefef',
      },
      '& .MuiTableRow-root:last-child': {
        background: 'unset',
      },
      '& .MuiTableRow-root .MuiTableCell-root': {
        border: 'none',
      },
    },
    newOperationMarketingMobile: {
      '&:first-child': {
        borderTop: '1px solid #ccc',
      },
      display: 'flex',
      borderBottom: '1px solid #ccc',
      padding: '15px 0',
    },
    formDirectChildren: {
      [theme.breakpoints.up('sm')]: {
        width: '65%',
      },
      width: '100%',
      margin: 'auto',
    },
    fullWidthChildren: {
      width: '100%',
    },
    selectMontant: {
      '& .MuiInputBase-root': {
        width: '250px',
      },
    },

    categorie: {
      height: 40,
      padding: '10px 0 0 10px',
      fontWeight: 300,
      color: '#38b25b',
      fontSize: 15,
      background: '#f5fcf8',
      marginBottom: 6,
    },
    bold: {
      fontWeight: 'bold',
    },
    link: {
      color: '#85c7e3',
      textDecoration: 'underline',
    },
    reglement: {
      marginLeft: 16,
      marginBottom: 16,
    },
  })
);
