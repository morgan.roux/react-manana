import React, { FC, useEffect, useState } from 'react';
import PlanForm from './../components/plan-form/PlanForm';
import { usePlanMarketing } from '../hooks/usePlanMarketing';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import { useGet_Plan_MarketingLazyQuery, useRemise_Arriere_TypesQuery } from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';

const plan: FC<{}> = () => {
  const { federation, currentPharmacie } = useApplicationContext();

  const { params } = useLaboratoireParams();
  const { id: idTrade, estRemiseArriere } = params;

  const [loadOperationMarketing, loadingOperationMarketing] = useGet_Plan_MarketingLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  const loadRemiseArriereTypes = useRemise_Arriere_TypesQuery();

  const {
    laboratoirePlan: {
      onRequest: { save: onRequestSave },
      listType,
      saving,
      saved,
      setSaved,
      produits,
    },
  } = usePlanMarketing(!!estRemiseArriere);

  const listTypePlan = estRemiseArriere
    ? {
        loading: loadRemiseArriereTypes.loading,
        error: loadRemiseArriereTypes.error,
        data: loadRemiseArriereTypes.data?.pRTRemiseArriereTypes?.nodes || [],
      }
    : listType;

  useEffect(() => {
    if (idTrade && idTrade !== 'new') {
      loadOperationMarketing({
        variables: {
          id: idTrade,
          idPharmacie: currentPharmacie.id,
        },
      });
    }
  }, [idTrade]);

  if (idTrade !== 'new' && loadingOperationMarketing.loading) {
    return <SmallLoading />;
  }

  return (
    <PlanForm
      mode={idTrade !== 'new' ? 'modification' : 'ajout'}
      open={true}
      setOpen={() => {}}
      onRequestSave={onRequestSave}
      listTypePlan={listTypePlan as any}
      formData={loadingOperationMarketing.data?.pRTPlanMarketing || undefined}
      saving={saving}
      saved={saved}
      setSaved={setSaved}
      produits={produits}
      setOpenEditDialog={() => {}}
    />
  );
};

export default plan;
