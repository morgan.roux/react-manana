export const unique = (array: any[] = [], id: string = 'id') => {
  const uniqueIds = [...new Set(array.map((item) => item[id]))];

  return array.filter((item) => uniqueIds.includes(item[id]));
};

const isNaN = (value: any) => {
  try {
    return Number.isNaN(parseFloat(`${value}`));
  } catch {}

  return true;
};

export const computeMontantRemuneration = (
  typeRemuneration: string,
  remise: number,
  dataTableLigne: any,
): number => {
  if (
    typeRemuneration === 'SANS_REMUNERATION' ||
    isNaN(dataTableLigne.produit['prixUnitaire']) ||
    isNaN(dataTableLigne.produit['quantitePrevue'])
  ) {
    return 0;
  }

  return typeRemuneration === 'REDUCTION_EURO'
    ? dataTableLigne.produit['prixUnitaire'] * dataTableLigne.produit['quantitePrevue']
    : ((dataTableLigne.produit['prixUnitaire'] * remise) / 100) *
        dataTableLigne.produit['quantitePrevue'];
};

export const computeMontantRemunerationRealisee = (
  typeRemuneration: string,
  remise: number,
  dataTableLigne: any,
): number => {
  if (
    typeRemuneration == 'SANS_REMUNERATION' ||
    isNaN(dataTableLigne.produit['prixUnitaire']) ||
    isNaN(dataTableLigne.produit['quantiteRealise'])
  ) {
    return 0;
  }

  return typeRemuneration === 'REDUCTION_EURO'
    ? dataTableLigne.produit['prixUnitaire'] * dataTableLigne.produit['quantiteRealise']
    : ((dataTableLigne.produit['prixUnitaire'] * remise) / 100) *
        dataTableLigne.produit['quantiteRealise'];
};

export const computeTotalRemunerationPrevue = (
  typeRemuneration: string,
  remise: number,
  dataTable: any[],
): number => {
  return dataTable.reduce((total, dataTableLigne) => {
    return total + computeMontantRemuneration(typeRemuneration, remise, dataTableLigne);
  }, 0);
};

export const computeTotalRemunerationRealisee = (
  typeRemuneration: string,
  remise: number,
  dataTable: any[],
): number => {
  return dataTable.reduce((total, dataTableLigne) => {
    return total + computeMontantRemunerationRealisee(typeRemuneration, remise, dataTableLigne);
  }, 0);
};
