import {
  CustomFormFiles,
  CustomFormMessage,
  exportExcel,
  useApplicationContext,
  ConsultFichierModal,
} from '@lib/common';
import { PRTPlanMarketingStatut } from '@lib/types';
import {
  Column,
  ConfirmDeleteDialog,
  CustomAvatar,
  CustomExport,
  CustomModal,
  ErrorPage,
  NewCustomButton,
  Table,
  TableChange,
  TopBar,
} from '@lib/ui-kit';
import TableChartSubToolbar from '@lib/ui-kit/src/components/molecules/TableChartSubToolbar';
import {
  Box,
  Button,
  Fab,
  Fade,
  Grid,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Select,
  Tooltip,
  Typography,
} from '@material-ui/core';
import {
  Add,
  Delete,
  Description,
  Edit,
  Email,
  Event,
  MoreHoriz,
  PostAdd,
  SaveAlt,
  Visibility,
} from '@material-ui/icons';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import moment from 'moment';
import React, { FC, MouseEvent, useCallback, useEffect, useRef, useState } from 'react';
import { BottomScrollListener } from 'react-bottom-scroll-listener';
import { CSVLink } from 'react-csv';
import { useHistory } from 'react-router';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import { useStyles as useCommonStyles } from '../../../commonStyles';
import { PlanningPlanMarketingDetails } from '../../components/details-trade-marketing';
import { Operation } from '../../components/Operation';
import { OperationCloture, OperationRealise, OperationStatus } from '../../components/Operation/interface';
import PlanForm from '../../components/plan-form/PlanForm';
import { usePlanMarketing } from '../../hooks/usePlanMarketing';
import {
  PrtPlanMarketingInfoFragment,
  SortDirection,
  useChange_Status_ActionMutation,
  usePlan_Marketing_CategoriesQuery,
  useRemise_Arriere_TypesQuery,
} from '@lib/common/src/federation';

import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import ArrowForward from '@material-ui/icons/ArrowForwardIos';
import { formatMoney } from '../../../../util';
import { useStyles } from './styles';
import { makePdf } from './../../../compte-rendu/pages/CompteRenduDetail/CompteRenduDetail';
import { useColumn } from './columns';

const NEXT_STATUT_MAPPING: any = {
  EN_COURS: ['REALISE'],
  REALISE: ['CLOTURE'],
  FACTURE: ['PAYE', 'CLOTURE'],
  PAYE: ['CLOTURE'],
  CLOTURE: [],
};

const headersExport = [
  { label: 'Type', key: 'type.libelle' },
  { label: 'Titre', key: 'titre' },
  { label: 'Période', key: 'periode' },
  { label: 'Labo', key: 'typeAssocie.nom' },
  { label: 'Statut', key: 'statut.libelle' },
  { label: 'Rémunération prévue', key: 'montant' },
];

export interface PlanPageProps {
  estRemiseArriere?: boolean;
}

const plan: FC<PlanPageProps> = ({ estRemiseArriere }) => {
  const classes = useStyles();
  // const searchText = useReactiveVar<string>(searchTextVar);

  const history = useHistory();
  const { params, redirectTo, goBack } = useLaboratoireParams();
  const { id: idTrade, idCategorie, dateFin, dateDebut, idType } = params;
  const skip = params.skip ? parseInt(params.skip) : 0;
  const take = params.take ? parseInt(params.take) : 0;
  const searchText = params.searchText;

  const { isMobile } = useApplicationContext();
  const commonStyles = useCommonStyles();
  const [openEditDialog, setOpenEditDialog] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [filter, setFilter] = useState<any>();
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [planToEdit, setPlanToEdit] = React.useState<any>();
  const [sortTable, setSortTable] = useState<any>({
    column: 'periode',
    direction: 'ASC',
  });
  const [showFichier, setShowFichier] = useState<boolean>(false);
  const [openOperationRealise, setOpenOperationRealise] = useState<boolean>(false);
  const [openOperationCloture, setOpenOperationCloture] = useState<boolean>(false);
  const [openOperationFacture, setOpenOperationFacture] = useState<boolean>(false);
  const [openRowDetail, setOpenRowDetail] = useState<boolean>(false);
  const [operationStatut, setOperationStatut] = useState<{
    operation: OperationCloture | OperationRealise;
    fichiers: any[];
    type: string;
    produits: [];
  }>();
  const [openModalAjoutFichier, setOpenModalAjoutFichier] = useState<boolean>(false);
  const [isToDownloadPdf, setToDownloadPdf] = useState<boolean>(false);

  const selectedDateDebut = dateDebut
    ? moment.utc(new Date(dateDebut)).toDate()
    : moment.utc().startOf('year').toDate();

  const selectedDateFin = dateFin ? moment(new Date(dateFin)).toDate() : moment().endOf('year').toDate();

  const open = Boolean(anchorEl);

  const componentToExport = useRef(null);

  const [activeListItem, setActiveListeItem] = useState('TOUS');

  const [date, setDate] = useState<any>(moment.utc().year());

  const {
    laboratoirePlan: {
      plan: laboratoirePlan,
      onRequest: {
        fetchMore: onRequestFetchMore,
        save: onRequestSave,
        delete: onRequestDelete,
        next: onRequestSearch,
        changeStatut: onRequestChangeStatut,
        saveFile: onRequestSaveFile,
      },
      listType,
      listStatutPlan,
      saving,
      saved,
      setSaved,
      produits,
      onRequestSubmitOperationStatut,
      onSavedStatut,
      handleSaveFacture,
      handleOpenListModal,
      recepteurs,
      factureOperation,
      listModeReglement,
      handleSendEmail,
      savedFichier,
      setSavedFichier,
      savingFichier,
      setSavingFichier,
    },
    onRequestPlanSearch,
    partenaireAssocie,
  } = usePlanMarketing(estRemiseArriere);

  useEffect(() => {
    setActiveListeItem(idCategorie || 'TOUS');
    onRequestPlanSearch();
  }, [idCategorie]);

  useEffect(() => {
    if (idType?.length && estRemiseArriere) setActiveListeItem(idType[0] || 'TOUS');
  }, [idType?.length && idType[0]]);

  useEffect(() => {
    setOpenEditDialog(!!idTrade);
    if (idTrade === 'new') setPlanToEdit(undefined);
  }, [idTrade]);

  useEffect(() => {
    if (saved) {
      setOpenEditDialog(false);
      if (setSaved) setSaved(false);
    }
  }, [saved]);

  useEffect(() => {
    if (onSavedStatut?.saved) {
      setOpenOperationCloture(false);
      setOpenOperationRealise(false);
      setOpenOperationFacture(false);
    }
    if (onSavedStatut?.setSaved) onSavedStatut?.setSaved(false);
  }, [onSavedStatut?.saved]);

  useEffect(() => {
    if (componentToExport && isToDownloadPdf) {
      makePdf(componentToExport.current, `Liste des trades marketings`);
      setToDownloadPdf(false);
    }
  }, [componentToExport, isToDownloadPdf]);

  const toggleOpenEditDialog = () => {
    setOpenEditDialog((prev) => !prev);
  };

  const loadPlanCategories = usePlan_Marketing_CategoriesQuery();
  const loadRemiseArriereTypes = useRemise_Arriere_TypesQuery();

  const listTypePlan = estRemiseArriere
    ? {
        loading: loadRemiseArriereTypes.loading,
        error: loadRemiseArriereTypes.error,
        data: loadRemiseArriereTypes.data?.pRTRemiseArriereTypes?.nodes || [],
      }
    : listType;

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const handleOpenAdd = () => {
    redirectTo({ id: 'new', estRemiseArriere, backParams: params }, 'plan-form');
    // history.push(
    //   `/laboratoires/plan-marketing?id=new${params.idLabo?.length ? `&idLabo=${params.idLabo[0]}` : ``}${
    //     estRemiseArriere ? `&estRemiseArriere=${estRemiseArriere}` : ``
    //   }`
    // );
  };

  const handleOpenPlanning = () => {
    // if (onRequestPlanning) {
    //   onRequestPlanning('', Date.now());
    // }
    redirectTo({ ...params }, 'planning');
  };

  const handleEdit = (plan: PrtPlanMarketingInfoFragment) => {
    setPlanToEdit(plan);
    redirectTo({ id: plan.id, estRemiseArriere, backParams: params }, 'plan-form');
    // history.push(
    //   `/laboratoires/plan?id=${plan.id}${params.idLabo?.length ? `&idLabo=${params.idLabo[0]}` : ``}${
    //     estRemiseArriere ? `&estRemiseArriere=${estRemiseArriere}` : ``
    //   }`
    // );
  };

  const handleConfirmDeleteOne = (): void => {
    if (planToEdit) {
      onRequestDelete && onRequestDelete(planToEdit);
      setOpenDeleteDialog(false);
    }
  };

  const handleShowMenuClick = (plan: PrtPlanMarketingInfoFragment, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    setPlanToEdit(plan);
    setAnchorEl(event.currentTarget);
  };

  const handleContainerOnBottom = () => {
    if (onRequestFetchMore) onRequestFetchMore();
  };

  // eslint-disable-next-line no-empty-pattern
  const handleNextDataTable = ({ skip, take, searchText, sortDirection, sortBy }: TableChange) => {
    if (skip || take) {
      redirectTo({
        ...params,
        sorting: sortBy && sortDirection ? [{ field: sortBy, direction: sortDirection }] : undefined,
        skip: skip ? skip.toString() : undefined,
        take: take ? take.toString() : undefined,
        searchText,
      });
    }
  };

  const handleSortTable = (column: any, direction: SortDirection) => {
    redirectTo({
      ...params,
      sorting: column && direction ? [{ field: column, direction }] : undefined,
    });
  };

  const handleShowFichier = () => {
    setShowFichier(true);
  };

  const handleAjoutFichier = () => {
    setSavedFichier(false);
    setOpenModalAjoutFichier(true);
  };

  const handleRequestSaveFichier = (files: any[]) => {
    if (planToEdit) {
      onRequestSaveFile(files, planToEdit.id);
    }
  };

  const handleChangeOperationStatut = ({ operation, fichiers, type, produit }: OperationStatus) => {
    setOperationStatut({ operation, fichiers, type, produits: produit as any });
  };

  const handleSubmitOperationStatut = () => {
    onSavedStatut.setSaving(true);
    onRequestSubmitOperationStatut &&
      onRequestSubmitOperationStatut({
        operation: operationStatut?.operation,
        fichiers: operationStatut?.fichiers,
        type: operationStatut?.type,
        idPlanMarketing: planToEdit.id,
        produits: operationStatut?.produits || [],
      });
  };

  const handleSubmitFacture = () => {
    onSavedStatut.setSaving(true);
    onRequestSubmitOperationStatut &&
      onRequestSubmitOperationStatut({
        operation: operationStatut?.operation,
        fichiers: operationStatut?.fichiers,
        type: 'FACTURE',
        idPlanMarketing: planToEdit.id,
        produits: operationStatut?.produits || [],
      });
  };

  const [changeStatutAction] = useChange_Status_ActionMutation();

  const handleOnRequestChangeStatutAction = (id: string, status: 'ACTIVE' | 'DONE') => {
    changeStatutAction({
      variables: {
        id,
        status,
      },
    });
  };

  const isValidOperationStatut = () => {
    return operationStatut?.operation;
  };

  const isValidOperationClotureStatut = () => {
    return operationStatut?.type === 'CLOTURE' && operationStatut?.operation?.idModeReglement;
  };

  const action = (item: PrtPlanMarketingInfoFragment) => {
    return (
      <div key={`row-${item.id}`}>
        <IconButton
          aria-controls="simple-menu"
          aria-haspopup="true"
          // eslint-disable-next-line react/jsx-no-bind
          onClick={handleShowMenuClick.bind(null, item)}
          className={classes.iconButton}
        >
          <MoreHoriz />
        </IconButton>
        <Menu
          id="fade-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open && item.id === planToEdit?.id}
          onClose={(e: any) => {
            e.stopPropagation();
            handleClose();
          }}
          TransitionComponent={Fade}
        >
          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              handleAjoutFichier();
              handleClose();
            }}
          >
            <ListItemIcon>
              <PostAdd />
            </ListItemIcon>
            <Typography variant="inherit">Ajouter des fichiers-joints</Typography>
          </MenuItem>
          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              handleShowFichier();
              handleClose();
            }}
          >
            <ListItemIcon>
              <Description />
            </ListItemIcon>
            <Typography variant="inherit">Consulter le fichier ci-joint</Typography>
          </MenuItem>
          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              handleShowDetail();
              handleClose();
            }}
          >
            <ListItemIcon>
              <Visibility />
            </ListItemIcon>
            <Typography variant="inherit">Voir détail</Typography>
          </MenuItem>
          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              handleSendEmail(item as any);
              handleClose();
            }}
          >
            <ListItemIcon>
              <Email />
            </ListItemIcon>
            <Typography variant="inherit">Envoyer e-mail</Typography>
          </MenuItem>
          <MenuItem
            onClick={(event) => {
              handleEdit(item);
              event.stopPropagation();
              handleClose();
            }}
          >
            <ListItemIcon>
              <Edit />
            </ListItemIcon>
            <Typography variant="inherit">Modifier</Typography>
          </MenuItem>

          <MenuItem
            onClick={(event) => {
              event.stopPropagation();
              handleClose();
              setOpenDeleteDialog(true);
            }}
          >
            <ListItemIcon>
              <Delete />
            </ListItemIcon>
            <Typography variant="inherit">Supprimer</Typography>
          </MenuItem>
        </Menu>
      </div>
    );
  };

  interface SelectCompProps {
    planMarketing: PrtPlanMarketingInfoFragment;
    statut: PRTPlanMarketingStatut;
    statutList: PRTPlanMarketingStatut[];
    onChange: (id: string, value: string) => void;
  }

  const SelectComp: FC<SelectCompProps> = ({ planMarketing, statut, onChange, statutList }) => {
    const handleChange = (
      event: React.ChangeEvent<{
        name?: string | undefined;
        value: unknown;
      }>,
      __child: React.ReactNode
    ) => {
      event.stopPropagation();

      setPlanToEdit(planMarketing);

      const statutChoisi = statutList.find((element) => element.id === event.target.value)?.code;
      if (statutChoisi === 'REALISE') {
        setOpenOperationRealise(true);
      }
      if (statutChoisi === 'CLOTURE') {
        setOpenOperationCloture(true);
      }
      if (statutChoisi === 'FACTURE') {
        setOpenOperationFacture(true);
      }
      (statutChoisi === 'PAYE' || statutChoisi === 'EN_COURS') &&
        planMarketing?.id &&
        onChange(planMarketing?.id, event.target.value as string);
    };

    const statutActive = NEXT_STATUT_MAPPING[planMarketing.statut.code as any];

    return (
      <>
        <Select
          style={{ minWidth: 120 }}
          variant="standard"
          value={statut.id}
          onChange={handleChange}
          displayEmpty
          disabled={statutActive?.length === 0}
          onClick={(event) => {
            if (statutActive?.length) event.stopPropagation();
          }}
        >
          {statutList.map(({ id, libelle, code }) => {
            const disabled = !statutActive.includes(code);
            return (
              <MenuItem key={id} value={id} disabled={disabled}>
                {libelle}
              </MenuItem>
            );
          })}
        </Select>
      </>
    );
  };

  const handleChangeStatut = (id: string, value: string) => {
    if (onRequestChangeStatut) onRequestChangeStatut(id, value);
  };

  const handleRowClick = (row: any, e: any) => {
    e.stopPropagation();
    setOpenRowDetail(true);
    setPlanToEdit(row);
  };

  const handleOpenRowDetail = () => {
    setOpenRowDetail(!openRowDetail);
  };

  const handleShowDetail = () => {
    handleOpenRowDetail();
  };

  const handleListeClick = (id: string) => {
    redirectTo(
      estRemiseArriere ? { idType: id === 'TOUS' ? undefined : [id] } : { idCategorie: id === 'TOUS' ? undefined : id }
    );
  };

  const columns = useColumn({
    isToDownloadPdf,
    classes,
    listStatutPlan,
    handleChangeStatut,
    action,
    SelectComp,
  });

  const listButton = [
    { id: 'TOUS', code: 'TOUS', libelle: 'Tous' },
    ...(estRemiseArriere
      ? loadRemiseArriereTypes.data?.pRTRemiseArriereTypes?.nodes || []
      : loadPlanCategories.data?.pRTPlanMarketingCategories?.nodes || []),
  ];

  const handleTableChartSubToolbarChange = (searchText: string, date: any) => {
    setDate(moment(date).year());
    if (onRequestSearch)
      onRequestSearch({
        skip,
        take,
        searchText,
        filter,
        sortTable,
        selectedDateDebut: moment.utc(date).startOf('year').toISOString(),
        selectedDateFin: moment.utc(date).endOf('year').toISOString(),
      });
  };

  const dataExport = (laboratoirePlan?.data || []).map((plan: PrtPlanMarketingInfoFragment) => ({
    ...plan,
    periode: `Du ${moment.utc(plan.dateDebut).format('DD/MM/YYYY')} - Au ${moment
      .utc(plan.dateFin)
      .format('DD/MM/YYYY')}`,
  }));

  const handleDateDebutChange = (dateDebut: Date) => {
    if (dateDebut && selectedDateFin && selectedDateFin > dateDebut) {
      onRequestSearch &&
        onRequestSearch({
          skip,
          take,
          searchText,
          filter,
          sortTable,
          selectedDateDebut: moment.utc(dateDebut).toISOString(),
          selectedDateFin: moment.utc(selectedDateFin).toISOString(),
        });
    }
  };

  const handleDateFinChange = (dateFin: Date) => {
    if (dateFin && selectedDateDebut && dateFin > selectedDateDebut) {
      setDateTrade(false);
      onRequestSearch &&
        onRequestSearch({
          skip,
          take,
          searchText,
          filter,
          sortTable,
          selectedDateDebut: moment.utc(selectedDateDebut).toISOString(),
          selectedDateFin: moment.utc(dateFin).toISOString(),
        });
    }
  };

  const [dateTrade, setDateTrade] = React.useState<boolean>();
  const handleTradeChange = () => {
    setDateTrade(!dateTrade);
  };

  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('lg'));

  const [menuAnchorEl, setMenuAnchorEl] = React.useState<HTMLButtonElement | null>(null);

  const handleMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setMenuAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setMenuAnchorEl(null);
  };

  const handleDownloadPdf = () => {
    setToDownloadPdf(true);
  };

  const handleDownloadExcel = () => {
    const dataExport = (laboratoirePlan?.data || []).map((element) => {
      return [
        element.type.libelle,
        element.titre,
        element.montant,
        element.montantStatutRealise,
        element.montantStatutCloture,
        element.typeAssocie?.nom,
        moment.utc(element.dateDebut).format('DD/MM/YYYY'),
        moment.utc(element.dateFin).format('DD/MM/YYYY'),
        element.reglementType?.libelle,
        element.reglementModeDeclaration?.libelle,
        element.modeReglement?.libelle,
        element.statut.libelle,
      ];
    });
    exportExcel({
      data: [
        [
          'Type',
          'Titre',
          'Rém. prévue',
          'Rém. réalisée',
          'Rém. payée',
          'Labo',
          'Date début',
          'Date fin',
          'Règlement',
          'Déclaration',
          'Paiement',
          'Statut',
        ],
        ...dataExport,
      ],
      title: 'Liste des trades marketings',
      filename: 'Liste des trades marketings',
    });
  };

  const openMenu = Boolean(menuAnchorEl);
  const id = openMenu ? 'simple-popover' : undefined;

  return (
    <>
      {openEditDialog && (
        <PlanForm
          mode={planToEdit ? 'modification' : 'ajout'}
          open={openEditDialog}
          setOpen={toggleOpenEditDialog}
          onRequestSave={onRequestSave}
          listTypePlan={listTypePlan as any}
          RData={planToEdit}
          saving={saving}
          saved={saved}
          setSaved={setSaved}
          produits={produits}
          setOpenEditDialog={setOpenEditDialog}
          partenaireAssocie={partenaireAssocie}
        />
      )}
      <CustomFormFiles
        open={openModalAjoutFichier}
        setOpen={setOpenModalAjoutFichier}
        onRequestSave={handleRequestSaveFichier}
        saving={savingFichier}
        setSaving={setSavingFichier}
        saved={savedFichier}
      />
      <ConsultFichierModal
        open={showFichier}
        setOpen={setShowFichier}
        title={`Fichier ci-joint (${planToEdit?.titre || ''})`}
        files={planToEdit?.fichiers || []}
      />
      <ConfirmDeleteDialog
        title="Suppression du plan marketing"
        content="Etes-vous sûr de vouloir supprimer ce plan marketing?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDeleteOne}
      />
      {isMobile ? (
        <div className={commonStyles.rootMobile}>
          <Box textAlign="center">
            <NewCustomButton onClick={handleOpenAdd} theme="flatPrimary" startIcon={<Add />}>
              NOUVELLE OPERATION MARKETING
            </NewCustomButton>
          </Box>
          <BottomScrollListener onBottom={handleContainerOnBottom}>
            {(scrollRef) => (
              <Grid id="infinity-plan" item ref={scrollRef as any} className={commonStyles.infiniteScrollContainer}>
                <Box py={2}>
                  {laboratoirePlan?.data?.map((item, index) => {
                    const date =
                      item?.dateDebut && item?.dateFin
                        ? `Du ${moment.utc(item?.dateDebut).format('DD/MM/YYYY')} - Au ${moment
                            .utc(item?.dateFin)
                            .format('DD/MM/YYYY')}`
                        : '-';
                    return (
                      <Box className={classes.listPlanMobile} py={2} px={1} key={item.id || index}>
                        <Box>
                          <Typography className={classes.textTitle}>{item.titre}</Typography>
                          <Typography variant="caption" className={classes.caption}>
                            <span style={{ color: '#424242' }}>{date}</span>{' '}
                            <FiberManualRecordIcon style={{ width: 7, height: 7 }} /> {item.type.libelle}
                          </Typography>
                          <Typography className={classes.typeAssocie}>
                            <CustomAvatar
                              name={item.typeAssocie?.nom || ''}
                              url={item.typeAssocie?.photo?.publicUrl || ''}
                              className={classes.logoTypeAssocie}
                            />{' '}
                            {item.typeAssocie?.nom}
                          </Typography>
                        </Box>
                        <Box textAlign="right" width="50%">
                          <Typography>{action(item as any)}</Typography>
                          <Typography variant="caption" className={classes.caption}>
                            {item.statut.libelle}
                          </Typography>
                          <Box textAlign="right">
                            <IconButton onClick={handleShowFichier} style={{ paddingRight: 0 }}>
                              <ArrowForward />
                            </IconButton>
                          </Box>
                        </Box>
                      </Box>
                    );
                  })}
                </Box>
              </Grid>
            )}
          </BottomScrollListener>
          {laboratoirePlan?.error && <ErrorPage />}
        </div>
      ) : (
        <Box className={classes.tableBox}>
          {!openEditDialog && (
            <>
              <Table
                componentTable={componentToExport}
                searchIconFirst
                error={laboratoirePlan?.error as Error | undefined}
                loading={laboratoirePlan?.loading}
                search={false}
                data={laboratoirePlan?.data || []}
                selectable={false}
                columns={columns}
                rowsTotal={laboratoirePlan.totalCount}
                onRowClick={handleRowClick}
                topBarComponent={(searchComponent) => (
                  <TopBar
                    handleOpenAdd={handleOpenAdd}
                    searchComponent={searchComponent}
                    titleButton={estRemiseArriere ? 'AJOUTER UNE REMISE ARRIÈRE' : 'AJOUTER UNE OPÉRATION MARKETING'}
                    buttonClassName={classes.buttonLast}
                    titleTopBar=" "
                    withFilter
                    filterComponent={
                      <>
                        <TableChartSubToolbar
                          withSearch={false}
                          onRequestChange={handleTableChartSubToolbarChange}
                          ToolBarBoxClassName={isMobile ? classes.toolBarChartMobile : ''}
                          intervalDate={true}
                          dateTrade={dateTrade}
                          handleDateDebutChange={handleDateDebutChange}
                          handleDateFinChange={handleDateFinChange}
                          handleTradeChange={handleTradeChange}
                          selectedDateDebut={selectedDateDebut}
                          selectedDateFin={selectedDateFin}
                          searchClassName={classes.searchToolBar}
                        />
                        <Box
                          style={{ cursor: 'pointer', marginLeft: 'auto' }}
                          display="flex"
                          alignItems="center"
                          className={classes.vue}
                          onClick={handleOpenPlanning}
                        >
                          <Box
                            style={{
                              display: 'flex',
                            }}
                          >
                            {listButton.map((item) => (
                              <Button
                                onClick={(event: MouseEvent<HTMLElement>) => {
                                  event.preventDefault();
                                  event.stopPropagation();
                                  handleListeClick(item.id);
                                }}
                                className={activeListItem && activeListItem === item.id ? classes.active : ''}
                              >
                                <Typography
                                  style={{
                                    textTransform: 'initial',
                                    fontSize: '14px',
                                    fontFamily: 'Roboto',
                                  }}
                                >
                                  {item.libelle}
                                </Typography>
                              </Button>
                            ))}
                          </Box>
                          <Box>
                            <CustomExport
                              onRequestDownloadPdf={handleDownloadPdf}
                              onRequestDownloadExcel={handleDownloadExcel}
                            />
                          </Box>
                          {/* <Box onClick={(event) => event.stopPropagation()}>
                            <CSVLink data={dataExport || ''} headers={headersExport} className={classes.exportButton}>
                              <Tooltip title="Exporter au format CSV">
                                <IconButton>
                                  <SaveAlt />
                                </IconButton>
                              </Tooltip>
                            </CSVLink>
                          </Box> */}
                          {!estRemiseArriere && (
                            <NewCustomButton
                              className={classes.vue}
                              onClick={handleOpenPlanning}
                              startIcon={<Event />}
                              theme="transparent"
                              shadow={false}
                              style={{
                                textTransform: 'none',
                                fontWeight: 400,
                                padding: 0,
                                marginRight: '0 !important',
                                marginLeft: '0 !important',
                              }}
                            >
                              {isMobile ? null : 'Vue planning'}
                            </NewCustomButton>
                          )}
                        </Box>
                      </>
                    }
                  />
                )}
                onRunSearch={handleNextDataTable}
                onSortColumn={handleSortTable}
              />
              <CustomModal
                open={openOperationRealise}
                headerWithBgColor
                setOpen={setOpenOperationRealise}
                title={`${planToEdit?.type.code === 'BRI' ? 'Opération BRI réalisée' : 'Opération réalisée'}`}
                withBtnsActions
                closeIcon
                maxWidth="lg"
                onClickConfirm={handleSubmitOperationStatut}
                fullScreen={!!isMobile}
                withCancelButton={false}
                disabledButton={!isValidOperationStatut() || onSavedStatut?.saving}
              >
                <Operation
                  key={planToEdit?.id || 'realise-operation'}
                  mode="REALISE"
                  onRequestDataOperation={handleChangeOperationStatut}
                  openRealise={openOperationRealise}
                  planMarketing={planToEdit}
                  listProduits={produits?.data || []}
                />
              </CustomModal>
              <CustomModal
                open={openOperationCloture}
                headerWithBgColor
                setOpen={setOpenOperationCloture}
                title="Paiement"
                withBtnsActions
                closeIcon
                maxWidth="lg"
                onClickConfirm={handleSubmitOperationStatut}
                fullScreen={!!isMobile}
                withCancelButton={false}
                disabledButton={!isValidOperationClotureStatut() || onSavedStatut?.saving}
              >
                <Operation
                  mode="CLOTURE"
                  onRequestDataOperation={handleChangeOperationStatut}
                  openCloture={openOperationCloture}
                  planMarketing={planToEdit}
                  listModeReglement={listModeReglement}
                />
              </CustomModal>
              <CustomModal
                open={openOperationFacture}
                headerWithBgColor
                setOpen={setOpenOperationFacture}
                title="Facturer l'opération"
                withBtnsActions
                closeIcon
                maxWidth="lg"
                onClickConfirm={handleSubmitFacture}
                fullScreen={!!isMobile}
                withCancelButton={false}
                disabledButton={!factureOperation?.isValid || onSavedStatut?.saving}
              >
                <Operation
                  mode="FACTURE"
                  openFacture={openOperationFacture}
                  planMarketing={planToEdit}
                  factureOperationComponent={
                    <CustomFormMessage
                      onOpenUserModal={handleOpenListModal}
                      recepteurs={recepteurs}
                      onRequestSave={handleSaveFacture}
                      partenaireAssocie={planToEdit?.typeAssocie}
                      openForm={openOperationFacture}
                    />
                  }
                />
              </CustomModal>
              <PlanningPlanMarketingDetails
                planMarketing={planToEdit}
                open={openRowDetail}
                setOpen={handleOpenRowDetail}
                onRequestChangeStatutAction={handleOnRequestChangeStatutAction}
                produits={produits as any}
              />
            </>
          )}
        </Box>
      )}
      {/* </CustomContainer> */}
    </>
  );
};

export default plan;
