import { PRTPlanMarketing, PRTPlanMarketingStatut, PRTPlanMarketingType } from '@lib/types';
import { PRTMiseAvant } from '../../../../types';
import { ReactNode } from 'react';
import { PlanFormRequestSavingProps } from '../../components/plan-form/PlanFormProps';
import { TableChange } from '@lib/ui-kit';
import { OperationCloture, OperationModeReglement, OperationRealise } from '../../components/Operation/interface';

export interface PlanProps {}

export interface SubmitOperationStatut {
  operation?: OperationRealise | OperationCloture;
  fichiers?: any[];
  idPlanMarketing?: string | undefined;
  type?: string;
  produits: any[];
}
