import { useApplicationContext } from '@lib/common';
import { Laboratoire, PrtPlanMarketingInfoFragment } from '@lib/common/src/federation';
import { ActionValue, Column, CustomEditorText, CustomFormTextField, CustomModal, Table, TodoTask } from '@lib/ui-kit';
import { useWindowDimensions } from '@lib/ui-kit/src/components/atoms/MobileTopBar/MobileTopBar';
import { Box, Grid, TableCell, TableRow, Typography } from '@material-ui/core';
import { Storefront } from '@material-ui/icons';
import classNames from 'classnames';
import moment from 'moment';
import React, { FC, memo, useEffect, useState } from 'react';
import { Reglement } from '../../../../components/Reglement';
import { ColumnPlanningPlanMarketingDetail } from './columnPlanningPlanMarketingDetail';
import { useStyles } from './styles';
import { formatMoney } from '../../../../util';
import { DetailPlanRemuneration } from './DetailPlanRemuneration';

export interface ProduitPlanMarketing {
  id?: string;
  nom: string;
  code: string;
  famille: string;
}

export interface PlanningPlanMarketingDetailsProps {
  planMarketing?: PrtPlanMarketingInfoFragment;
  open: boolean;
  setOpen: () => void;
  produits?: {
    loading?: boolean;
    error?: Error;
    data?: ProduitPlanMarketing[];
  };
  onRequestChangeStatutAction?: (id: string, statut: 'ACTIVE' | 'DONE') => void;
  laboratoire?: Laboratoire;
}

export const PlanningPlanMarketingDetails: FC<PlanningPlanMarketingDetailsProps> = memo(
  ({ planMarketing, open, setOpen, produits, onRequestChangeStatutAction, laboratoire }) => {
    const dimensions = useWindowDimensions();
    const classes = useStyles();
    const [, /* doneActions, */ setDoneActions] = useState<string>('');
    const actions = planMarketing?.actions || [];
    useEffect(() => {
      let count = 0;
      actions.map((item: any) => {
        if (item?.status === 'DONE') {
          count = count + 1;
        }
      });
      setDoneActions(`${count}/${actions.length || 0}`);
    }, [actions]);

    const columnProduit = ColumnPlanningPlanMarketingDetail(planMarketing as any);

    console.log('---------------------------plan marketing produits: ', planMarketing?.produits, columnProduit);

    return (
      <CustomModal
        open={open}
        setOpen={setOpen}
        title={`Détail ${laboratoire?.nom || ''}`}
        withBtnsActions={false}
        closeIcon
        headerWithBgColor
        maxWidth="lg"
        fullWidth={dimensions === 'isSm'}
        fullScreen={dimensions === 'isSm' ? true : false}
      >
        <Box className={classes.root}>
          <h2>{planMarketing?.estRemiseArriere ? 'Remise arrière' : 'Opération Trade Marketing'}</h2>
          <Box className={classes.textFieldContainer}>
            <CustomFormTextField label="Type" value={planMarketing?.type?.libelle || ''} />
          </Box>
          {!planMarketing?.estRemiseArriere && (
            <Box className={classes.textFieldContainer}>
              <CustomFormTextField label="Catégorie" value={planMarketing?.categorie?.libelle || ''} />
            </Box>
          )}
          <Box className={classes.textFieldContainer}>
            <CustomFormTextField label="Titre" value={planMarketing?.titre || ''} />
          </Box>
          <Box className={classes.textFieldContainer}>
            <CustomEditorText
              disabled={true}
              value={planMarketing?.description || ''}
              placeholder="Description Détaillée"
            />
          </Box>

          <h3>Période</h3>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <CustomFormTextField
                label="Date de début"
                value={moment.utc(planMarketing?.dateDebut).format('DD/MM/YYYY')}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={6}>
              <CustomFormTextField
                label="Date de fin"
                value={moment.utc(planMarketing?.dateFin).format('DD/MM/YYYY')}
              />
            </Grid>
          </Grid>
          <Box className={classes.statutContainer}>
            <span className={classes.label}>Statut&nbsp;:&nbsp;&nbsp;</span>
            <span className={classes.label}>{planMarketing?.statut?.libelle}</span>
          </Box>

          <DetailPlanRemuneration planMarketing={planMarketing} />

          <Reglement
            laboratoire={planMarketing?.typeAssocie as any}
            reglementType={planMarketing?.reglementType as any}
            reglementLieuDeclaration={planMarketing?.reglementLieuDeclaration as any}
            reglementModeDeclaration={planMarketing?.reglementModeDeclaration as any}
          />

          {!planMarketing?.estRemiseArriere && (
            <>
              {(planMarketing?.produits?.length || 0) > 0 && (
                <>
                  {' '}
                  <h2>Liste des produits</h2>
                  <Box marginBottom={4} width={1}>
                    <Table
                      error={produits?.error}
                      loading={produits?.loading}
                      search={false}
                      columns={columnProduit}
                      notablePagination
                      data={planMarketing?.produits || []}
                    />
                  </Box>
                </>
              )}

              <Box className={classes.todoContainer}>
                {actions.map((action) => (
                  <TodoTask
                    key={action.id}
                    onChangeStatutClick={onRequestChangeStatutAction}
                    task={action as ActionValue}
                    iconIndication={<Storefront />}
                  />
                ))}
              </Box>
            </>
          )}
        </Box>
      </CustomModal>
    );
  }
);
