import React, { FC } from 'react';
import TradeMarketing from './trade-marketing/TradeMarketing'

const RemiseArriere: FC<{}> = () => {
    return <TradeMarketing estRemiseArriere/>
}

export default RemiseArriere;