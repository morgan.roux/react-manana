// eslint-disable-next-line no-use-before-define
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { Box, InputAdornment, TableCell, TableRow } from '@material-ui/core';
import { useColumn } from './columns';
import {
  CustomDatePicker,
  CustomEditorText,
  CustomFormTextField,
  CustomSelect,
  MontantInputField,
  Table,
} from '@lib/ui-kit';
import { OperationCloture, OperationProps, OperationRealise } from './interface';
import { useStyles } from '../plan-form/styles';
import { Dropzone } from '@lib/common';
import { computeMontantRemuneration, computeTotalRemunerationRealisee } from '../plan-form/util';

const operationRealiseInitial = {
  description: '',
  montantStatutRealise: 0,
};

const operationClotureInitial = {
  date: new Date(),
  montant: 0,
  idModeReglement: '',
};

const Operation: FC<OperationProps> = ({
  mode,
  onRequestDataOperation,
  openRealise,
  openCloture,
  openFacture,
  planMarketing,
  factureOperationComponent,
  listModeReglement,
}) => {
  const classes = useStyles();

  const [realise, setChangeRealise] = useState<OperationRealise>(operationRealiseInitial);
  const [fichiersJointsRealise, setFichiersJointsRealise] = useState<any[]>([]);

  const [fichiersJointsCloture, setFichiersJointsCloture] = useState<any[]>([]);
  const [cloture, setChangeCloture] = useState<OperationCloture>(operationClotureInitial);

  const [selectedRows, setSelectedRows] = useState<any>([]);

  const handleChangeRealise = (e: ChangeEvent<HTMLInputElement>) => {
    setChangeRealise({ ...realise, [e.target.name]: e.target.value });
  };
  const handleRealiseMontant = (value?: number) => {
    setChangeRealise((prevState: any) => ({ ...prevState, montantStatutRealise: value }));
  };

  const handleChangeCloture = (e: ChangeEvent<HTMLInputElement>) => {
    setChangeCloture({ ...cloture, [e.target.name]: e.target.value });
  };
  const handleClotureMontant = (value?: number) => {
    setChangeCloture((prevState: any) => ({ ...prevState, montant: value }));
  };

  useEffect(() => {
    if (planMarketing) {
      const newProduits = (planMarketing?.produits || []).map((produit) => ({
        ...produit,
        produit: {
          ...produit.produit,
          quantiteRealise: produit.produit.quantitePrevue,
          remiseRealise: computeMontantRemuneration(
            planMarketing.typeRemuneration || 'SANS_REMUNERATION',
            planMarketing.remise || 0,
            produit
          ),
        },
      }));

      setSelectedRows(newProduits);
    }
  }, [planMarketing]);

  useEffect(() => {
    if (planMarketing) {
      const newMontantRealisee =
        planMarketing.remuneration === 'FORFAITAIRE'
          ? planMarketing.montantStatutRealise || planMarketing.montant
          : computeTotalRemunerationRealisee(
              planMarketing.typeRemuneration || 'SANS_REMUNERATION',
              planMarketing.remise || 0,
              selectedRows
            );
      setChangeRealise((prev) => ({ ...prev, montantStatutRealise: newMontantRealisee }));
    }
  }, [selectedRows]);

  useEffect(() => {
    onRequestDataOperation &&
      onRequestDataOperation({
        operation: openRealise ? realise : cloture,
        fichiers: openRealise ? fichiersJointsRealise : fichiersJointsCloture,
        type: mode,
        produit: selectedRows,
      });
  }, [realise, fichiersJointsRealise, fichiersJointsCloture, cloture]);

  useEffect(() => {
    if (openRealise) {
      setChangeRealise(operationRealiseInitial);
      setFichiersJointsRealise([]);
    }
  }, [openRealise]);

  useEffect(() => {
    if (openCloture && planMarketing) {
      setChangeCloture({
        ...operationClotureInitial,
        montant: (planMarketing as any).montantStatutRealise,
      });
      setFichiersJointsCloture([]);
    }
  }, [openCloture, planMarketing]);

  useEffect(() => {
    if (openFacture) {
      onRequestDataOperation &&
        onRequestDataOperation({
          operation: cloture,
          fichiers: fichiersJointsCloture,
          type: 'FACTURE',
        });
    }
  }, [openFacture]);

  const produitColumns = useColumn(planMarketing, selectedRows, setSelectedRows);

  const withAdditionalRows = () => {
    if (
      (planMarketing.typeRemuneration === 'REDUCTION_EURO' && planMarketing.remuneration !== 'FORFAITAIRE') ||
      planMarketing.typeRemuneration === 'REDUCTION_POURCENTAGE'
    ) {
      return true;
    }
    return false;
  };

  const additionalRows = (
    <TableRow style={{ borderBottom: 'none' }}>
      <TableCell colSpan={4} />
      <TableCell colSpan={2} className={classes.info}>
        Total rémunération réalisée
      </TableCell>
      <TableCell style={{ textAlign: 'right' }}>
        {withAdditionalRows() ? (realise.montantStatutRealise || 0).toFixed(2) : 0} ‎€
      </TableCell>
    </TableRow>
  );

  return (
    <>
      {mode === 'REALISE' && (
        <Box display="flex" flexDirection="column">
          {selectedRows.length > 0 && (
            <Box py={1}>
              <Table
                error={false as any}
                loading={false}
                search={false}
                data={selectedRows || []}
                columns={produitColumns}
                rowsTotal={selectedRows.length || 0}
                additionalRows={withAdditionalRows() && additionalRows}
              />
            </Box>
          )}
          {planMarketing.remuneration === 'FORFAITAIRE' && planMarketing.typeRemuneration === 'REDUCTION_EURO' && (
            <Box py={1} px={0}>
              <MontantInputField
                onChangeValue={(montant) => handleRealiseMontant(montant)}
                value={realise.montantStatutRealise}
                label="Rémunération réalisée"
                inputProps={{ min: 0, step: 0.5, style: { textAlign: 'right' } }}
              />
            </Box>
          )}
          <Box py={1} px={0}>
            <CustomEditorText
              value={realise.description}
              placeholder="Commentaire"
              onChange={(content) => handleChangeRealise({ target: { name: 'description', value: content } } as any)}
            />
          </Box>
          <Box py={1} px={0}>
            <Dropzone
              contentText="Glissez et déposez vos documents"
              selectedFiles={fichiersJointsRealise}
              setSelectedFiles={setFichiersJointsRealise}
              multiple
            />
          </Box>
        </Box>
      )}
      {mode === 'FACTURE' && <>{factureOperationComponent}</>}
      {mode === 'CLOTURE' && (
        <Box display="flex" flexDirection="column">
          <Box py={1} px={0}>
            <CustomDatePicker
              onChange={(content) => handleChangeCloture({ target: { name: 'date', value: content } } as any)}
              value={cloture.date}
              label="Date"
            />
          </Box>
          {planMarketing.typeRemuneration !== 'SANS_REMUNERATION' && (
            <>
              <Box py={1} px={0}>
                <MontantInputField
                  onChangeValue={(montant) => handleClotureMontant(montant)}
                  value={cloture.montant}
                  label="Rémunération payée"
                  inputProps={{ min: 0, step: 0.5, style: { textAlign: 'right' } }}
                />
              </Box>
              <Box py={1} px={0}>
                <CustomSelect
                  list={listModeReglement || []}
                  index="libelle"
                  listId="code"
                  label="Mode règlement"
                  value={cloture.idModeReglement}
                  onChange={handleChangeCloture}
                  required
                  name="idModeReglement"
                />
              </Box>
            </>
          )}
          <Box py={1} px={0}>
            <Dropzone
              contentText="Glissez et déposez vos documents"
              selectedFiles={fichiersJointsCloture}
              setSelectedFiles={setFichiersJointsCloture}
              multiple
            />
          </Box>
        </Box>
      )}
    </>
  );
};

export default Operation;
