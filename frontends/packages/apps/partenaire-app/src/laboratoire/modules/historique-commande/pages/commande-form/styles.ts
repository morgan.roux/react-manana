import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    subtitle: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      color: '#424242',
      fontSize: 20,
      paddingBottom: 24,
    },
    lineBox: {
      display: 'flex',
      marginTop: '8px',
      '&>div:last-of-type': {
        marginLeft: theme.spacing(3),
      },
    },
    ttcBox: {},
    datePicker: {
      '& .main-MuiInputBase-root': {
        height: '48px',
      },
    },
  })
);
