import React, { FC, useState, useEffect } from 'react';

import { Box, Typography } from '@material-ui/core';
import { useColumns } from './columns';
import { useStyles } from './styles';
import { LigneCommande } from '../../../../types';
import { CustomModal, Table } from '@lib/ui-kit';

interface DetailCommandeProps {
  lignesCommande?: LigneCommande[];
  titreCommande?: number;
  open: boolean;
  setOpen: (open: boolean) => void;
}

export const DetailCommande: FC<DetailCommandeProps> = ({ lignesCommande, titreCommande, open, setOpen }) => {
  const classes = useStyles();

  // DATA
  const [data, setData] = useState<LigneCommande[]>([
    {
      id: '',
      produit: {
        id: '',
        libelle: '',
        prix: 0,
      },
      quantite: 0,
      remise: 0,
      total: 0,
    },
  ]);
  const [rowsTotal, setRowsTotal] = useState<number>(1);

  useEffect(() => {
    if (lignesCommande) {
      const totalRemise = lignesCommande.reduce((prevTotalRemise: number, currentProduit: LigneCommande) => {
        return prevTotalRemise + currentProduit.remise;
      }, 0);
      const total = lignesCommande.reduce((prev: number, currentProduit: LigneCommande) => {
        return prev + currentProduit.total;
      }, 0);
      setData([
        ...(lignesCommande || []),
        {
          id: '',
          produit: {
            id: '',
            libelle: '',
            prix: 0,
          },
          quantite: 0,
          remise: totalRemise,
          total,
        },
      ]);
    }
  }, [lignesCommande]);

  useEffect(() => {
    setRowsTotal(data.length);
  }, [data]);

  const columns = useColumns(rowsTotal);

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Détail Commande"
      withBtnsActions={false}
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      fullWidth
    >
      <Box mt={1} width={1}>
        <Typography className={classes.textTitle}>{titreCommande}</Typography>
        <Table
          search={false}
          data={data || []}
          selectable={false}
          columns={columns}
          rowsTotal={rowsTotal}
          notablePagination
          rowsPerPageOptions={[1000]}
        />
      </Box>
    </CustomModal>
  );
};
