import React from 'react';
import loadable from '@loadable/component';

import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';

const HistoriqueCommandePage = loadable(() => import('./pages/historique-commande/Historique'), {
  fallback: <SmallLoading />,
});

const historiqueCommandeModule: ModuleDefinition = {
  routes: [
    {
      attachTo: 'MAIN',
      path: '/laboratoires/commande',
      component: HistoriqueCommandePage,
      activationParameters: {
        groupement: '0305',
        pharmacie: '0708',
      },
    },
  ],
};

export default historiqueCommandeModule;
