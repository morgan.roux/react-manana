import React, { ChangeEvent, MouseEvent } from 'react';
import moment from 'moment';
import { Link, Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography, Box } from '@material-ui/core';
import { Delete, Edit, MoreHoriz } from '@material-ui/icons';
import { useStyles } from './styles';

import { Column, CustomSelect } from '@lib/ui-kit';
import { Historique } from '../../../../types';
import { CommandeStatut } from '../../pages/historique-commande/Historique';

// const historiqueCommandeStatus: { code: string }[] = [
//   { code: 'Transmise' },
//   { code: 'Attente' },
//   { code: 'Clôturée' },
//   { code: 'Annulée' },
//   { code: 'Acquittée' },
//   { code: 'Préparation' },
// ];

export const useColumns = (
  handleShowMenuClick: (reunion: Historique, event: MouseEvent<HTMLElement>) => void,
  anchorEl: HTMLElement | null,
  open: boolean,
  handleClose: () => void,
  handleEdit: (historique: Historique) => void,
  setOpenDeleteDialog: (open: boolean) => void,
  onRequestShowDetail: (data: Historique) => void,
  historiqueToEdit?: Historique,
  isFacture?: boolean,
  onRequestChangeStatut?: (id?: string, statut?: string) => void,
  commandeStatuts?: CommandeStatut[]
) => {
  const classes = useStyles();

  const columns: Column[] = [
    {
      name: 'referenceCommande',
      label: 'Numéro de commande',
      sortable: !isFacture,
      renderer: (row: Historique) => {
        return row?.referenceCommande ? (
          <Typography className={classes.textTitre}>
            <Link
              color="secondary"
              onClick={(event: MouseEvent<HTMLElement>) => {
                event.preventDefault();
                event.stopPropagation();
                onRequestShowDetail(row);
              }}
            >
              {row.referenceCommande}
            </Link>
          </Typography>
        ) : (
          ''
        );
      },
    },
    {
      name: 'labo',
      label: 'Labo',
      renderer: (row: Historique) => {
        return (
          row?.totalTTC && (
            <Typography className={classes.tableText}>{row?.typeAssocie?.nom}</Typography>
          )
        );
      },
    },
    {
      name: 'dateCommande',
      label: 'Date de la commande',
      sortable: !isFacture,
      renderer: (row: Historique) => {
        return !React.isValidElement(row.date) ? (
          <Typography className={classes.tableText}>{moment(row.date).format('DD/MM/YYYY')}</Typography>
        ) : (
          row.date
        );
      },
    },
    {
      name: 'totalTTC',
      label: 'Total TTC',
      sortable: !isFacture,
      renderer: (row: Historique) => {
        return (
          row?.totalTTC && (
            <Typography className={classes.tableMonetaire}>{(row.totalTTC || 0).toFixed(2)}&nbsp;€</Typography>
          )
        );
      },
    },
    {
      name: 'totalHT',
      label: 'Total HT',
      sortable: !isFacture,
      renderer: (row: Historique) => {
        return (
          row?.totalHT && <Typography className={classes.tableMonetaire}>{(row.totalHT || 0).toFixed(2)}&nbsp;€</Typography>
        );
      },
    },
    {
      name: 'totalTVA',
      label: 'Total TVA',
      sortable: !isFacture,
      renderer: (row: Historique) => {
        return (
          row?.totalTVA && (
            <Typography className={classes.tableMonetaire}>{(row.totalTVA || 0).toFixed(2)}&nbsp;€</Typography>
          )
        );
      },
    },
    {
      name: 'statut',
      label: !isFacture ? 'Statut' : '',
      renderer: (row: Historique) => {
        return !isFacture ? (
          <Box className={classes.selectBox}>
            <Box style={{ width: '50%' }}>
              <CustomSelect
                list={commandeStatuts}
                index="libelle"
                listId="code"
                label=""
                value={row?.status}
                onChange={(e: ChangeEvent<HTMLInputElement>) => {
                  onRequestChangeStatut && onRequestChangeStatut(row?.id, e.target.value);
                }}
                variant="standard"
                defaultValue={row?.status}
              />
            </Box>
          </Box>
        ) : (
          ''
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: Historique) => {
        return row.id && !isFacture ? (
          <div key={`row-${row.id}`}>
            <IconButton
              aria-controls="simple-menu"
              aria-haspopup="true"
              // eslint-disable-next-line react/jsx-no-bind
              onClick={handleShowMenuClick.bind(null, row)}
            >
              <MoreHoriz />
            </IconButton>
            <Menu
              id="fade-menu"
              anchorEl={anchorEl}
              keepMounted
              open={open && row.id === historiqueToEdit?.id}
              onClose={handleClose}
              TransitionComponent={Fade}
            >
              {/* <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  onRequestShowDetail(row);
                }}
              >
                <ListItemIcon>
                  <Visibility />
                </ListItemIcon>
                <Typography variant="inherit">Voir détail</Typography>
              </MenuItem> */}
              {row?.source === 'PLATEFORME' && (
                <MenuItem
                  onClick={(event) => {
                    event.stopPropagation();
                    handleClose();
                    handleEdit(row);
                  }}
                >
                  <ListItemIcon>
                    <Edit />
                  </ListItemIcon>
                  <Typography variant="inherit">Modifier</Typography>
                </MenuItem>
              )}
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  setOpenDeleteDialog(true);
                }}
              >
                <ListItemIcon>
                  <Delete />
                </ListItemIcon>
                <Typography variant="inherit">Supprimer</Typography>
              </MenuItem>
            </Menu>
          </div>
        ) : (
          ''
        );
      },
    },
  ];
  return columns;
};
