import { makeStyles, createStyles } from '@material-ui/core';
import GREY from '@material-ui/core/colors/grey';
export const useStyles = makeStyles(() =>
  createStyles({
    root: {},
    produitField: {
      maxWidth: 200,
      fontSize: 8,
    },
    field: {
      minWidth: 100,
    },
    textNormal: {
      textAlign: 'right',
      color: GREY[600],
      fontSize: 14,
    },
    textProduit: {
      fontSize: 12,
    },
    formField: {
      marginTop: 10,
    },
    textTitle: {
      fontSize: 16,
      fontWeight: 'bold',
      color: GREY[600],
      marginBottom: 16,
    },
  })
);
