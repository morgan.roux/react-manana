import React from 'react';
import { useStyles } from './styles';
import { Typography } from '@material-ui/core';

import { LigneCommande } from '../../../../types';
import { Column } from '@lib/ui-kit';

export const useColumns = (rowsTotal: number) => {
  const classes = useStyles();
  const columns: Column[] = [
    {
      name: 'produit',
      label: 'Produit',
      renderer: (row: LigneCommande) => {
        return row?.produit ? (
          <Typography className={classes.textProduit}>{row.produit.libelle}</Typography>
        ) : (
          '-'
        );
      },
    },
    {
      name: 'quantite',
      label: 'Quantité',
      renderer: (row: LigneCommande, index) => {
        if (index === rowsTotal - 1) {
          return <Typography className={classes.textNormal}>Total</Typography>;
        }
        return row?.quantite ? (
          <Typography className={classes.textNormal}>{row.quantite}</Typography>
        ) : (
          <Typography className={classes.textNormal}>0</Typography>
        );
      },
    },
    {
      name: 'remise',
      label: 'Remise',
      renderer: (row: LigneCommande) => {
        return row?.remise ? (
          <Typography className={classes.textNormal}>{`${row.remise}€`}</Typography>
        ) : (
          <Typography className={classes.textNormal}>0 €</Typography>
        );
      },
    },
    {
      name: 'total',
      label: 'Total',
      renderer: (row: LigneCommande) => {
        return row?.total ? (
          <Typography className={classes.textNormal}>{`${row.total}€`}</Typography>
        ) : (
          <Typography className={classes.textNormal}>0 €</Typography>
        );
      },
    },
  ];
  return columns;
};
