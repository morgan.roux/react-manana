import { Box, TableCell, TableRow, Typography, Divider, Fab } from '@material-ui/core';
import React, { FC, useState, MouseEvent, Dispatch, useEffect } from 'react';
import { useStyles } from './styles';
import { useTheme } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import moment from 'moment';
import { Add } from '@material-ui/icons';
import { useStyles as useCommonStyles } from '../../../commonStyles';
import { useColumns } from '../../components/historique-commande/columns';
import { ConfirmDeleteDialog, Table, TopBar } from '@lib/ui-kit';
import { Historique } from '../../../../types';
import HistoriqueForm, { OnRequestSaveProps } from '../commande-form/CommandeForm';
import { DetailCommande } from '../../components/DetailCommande';
import {
  PrtCommandeFilter,
  PrtCommandeSortFields,
  SortDirection,
  useChange_Prt_Commande_StatutMutation,
  useCommandes_For_DocumentLazyQuery,
  useCommande_For_Document_Total_RowsLazyQuery,
  useCreate_Prt_CommandeMutation,
  useDelete_Prt_CommandeMutation,
  useGet_Commandes_LignesLazyQuery,
  useGet_Historiques_CommadesLazyQuery,
  useGet_LaboratoireLazyQuery,
  useGet_Rows_TotalLazyQuery,
  useGet_Totaux_CommandesLazyQuery,
  usePrt_Commande_StatutsLazyQuery,
  useTotaux_Commandes_For_DocumentLazyQuery,
  useUpdate_Prt_CommandeMutation,
} from '@lib/common/src/federation';
import { endTime, startTime, useApplicationContext, useUploadFiles } from '@lib/common';

import { round2Decimals } from '../../../../util';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';

const COLUMN_FIELD_COMMANDE = {
  referenceCommande: PrtCommandeSortFields.NbrRef,
  dateCommande: PrtCommandeSortFields.DateCommande,
  totalHT: PrtCommandeSortFields.PrixNetTotalHt,
  totalTTC: PrtCommandeSortFields.PrixNetTotalTtc,
  totalTVA: PrtCommandeSortFields.TotalTva,
};

export interface TotauxFacture {
  totalHT?: number;
  totalTVA?: number;
  totalTTC?: number;
}

export interface FilterHistorique {
  take?: number;
  skip?: number;
  searchText?: string;
  filterFacture?: {
    active?: boolean;
    idDocument?: string;
  };
  sorting?: Array<{
    field: any;
    direction: SortDirection;
  }>;
}

export interface CommandeStatut {
  id: string;
  code: string;
  libelle: string;
}

export interface CommandeStatut {
  id: string;
  code: string;
  libelle: string;
}
export interface HistoriqueProps {
  isFacture?: boolean;
  selectedItemsIds?: string[];
  onRequestSelected?: (commandes: Historique[]) => void;
  idDocument?: string;
  savedAssociation?: boolean;
  setSavedAssociation?: Dispatch<boolean>;
}

const HistoriquePage: FC<HistoriqueProps> = ({
  isFacture = false,
  selectedItemsIds,
  onRequestSelected,
  idDocument,
  savedAssociation,
  setSavedAssociation,
}) => {
  /** STATE */
  const [openEditDialog, setOpenEditDialog] = useState<boolean>(false);
  const [openShowDialog, setOpenShowDialog] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [historiqueToEdit, setHistoriqueToEdit] = useState<Historique | undefined>();

  const [commandeSelected, setCommandeSelected] = useState<Historique[]>([]);
  const [ajoutCommande, setAjoutCommande] = useState<boolean>(false);

  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const open = Boolean(anchorEl);

  const classes = useStyles();
  const commonStyles = useCommonStyles();

  const { params, redirectTo } = useLaboratoireParams('commande');
  const { idLabo, skip, take, searchText, sorting } = params;
  const idLaboExist = idLabo ? idLabo[0] : undefined;

  const { notify, currentPharmacie: pharmacie } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  // React.useEffect(() => {
  //   if (activeItem === 'ajout-compte-rendu') {
  //     setFilter((prev) => {
  //       return { ...prev, filterFacture: { active: true, idDocument } };
  //     });
  //   }
  // }, [activeItem, idDocument]);

  React.useEffect(() => {
    handleOnRequestSearch({
      skip: skip ? parseInt(skip) : undefined,
      take: take ? parseInt(take) : undefined,
      searchText,
      sorting,
    });
  }, [
    skip,
    take,
    searchText,
    sorting && sorting[0] && sorting[0].direction,
    sorting && sorting[0] && sorting[0].field,
    JSON.stringify(idLabo),
  ]);

  React.useEffect(() => {
    if (saved) {
      setOpenEditDialog(false);
      if (setSaved) setSaved(false);
    }
  }, [saved]);

  /** HANDLERS */
  const toggleOpenEditDialog = () => {
    if (!openEditDialog) {
      // if closed, set historique to edit undefined
      setHistoriqueToEdit(undefined);
    }
    setOpenEditDialog((prev) => !prev);
  };

  const toggleOpenShowDialog = () => {
    if (!openShowDialog) {
      // if closed, set historique to show undefid
      setHistoriqueToEdit(undefined);
    }
    setOpenShowDialog((prev) => !prev);
  };

  const [loadLabo, loadingLabo] = useGet_LaboratoireLazyQuery();

  const [loadCommandesForDocument, loadingCommandesForDocument] = useCommandes_For_DocumentLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [loadCommandes, loadingCommandes] = useGet_Historiques_CommadesLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [loadTotalRowsForDocument, loadingTotalRowsForDocument] = useCommande_For_Document_Total_RowsLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [loadTotalRows, loadingTotalRows] = useGet_Rows_TotalLazyQuery({});

  const [loadLignesCommandes, loadingLignesCommandes] = useGet_Commandes_LignesLazyQuery();

  const [loadTotauxCommandesForDocument, loadingTotauxCommandesForDocument] = useTotaux_Commandes_For_DocumentLazyQuery(
    {
      fetchPolicy: 'cache-and-network',
    }
  );

  const [loadTotauxCommande, loadingTotauxCommande] = useGet_Totaux_CommandesLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const refetchCommandeForDocument = () => {
    loadingCommandesForDocument.refetch && loadingCommandesForDocument.refetch();
    loadingTotalRowsForDocument.refetch && loadingTotalRowsForDocument.refetch();
    loadingTotauxCommandesForDocument.refetch && loadingTotauxCommandesForDocument.refetch();
  };

  useEffect(() => {
    if (savedAssociation) {
      refetchCommandeForDocument();
      if (setSavedAssociation) setSavedAssociation(false);
    }
  }, [savedAssociation]);

  useEffect(() => {
    if (idLaboExist) {
      loadLabo({
        variables: {
          id: idLaboExist,
          idPharmacie: pharmacie.id,
        },
      });
    }
  }, [idLaboExist]);

  const [loadCommandeStatuts, loadingCommandeStatuts] = usePrt_Commande_StatutsLazyQuery();

  const [changeCommandeStatut] = useChange_Prt_Commande_StatutMutation({
    onCompleted: () => {
      notify({
        message: 'Le statut a été modifié avec succès !',
        type: 'success',
      });
      loadingCommandes.refetch && loadingCommandes.refetch();
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant le changement du statut',
        type: 'error',
      });
    },
  });

  const [createCommande] = useCreate_Prt_CommandeMutation({
    onCompleted: () => {
      notify({
        message: 'La commande a été ajoutée avec succès !',
        type: 'success',
      });

      loadingCommandes.refetch && loadingCommandes.refetch();

      loadingTotauxCommandesForDocument.refetch && loadingTotauxCommandesForDocument.refetch();
      loadingTotauxCommande.refetch && loadingTotauxCommande.refetch();
      setSaving(false);
      setSaved(true);
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la création de la nouvelle commande',
        type: 'error',
      });

      setSaving(false);
    },
  });

  const [updateCommande] = useUpdate_Prt_CommandeMutation({
    onCompleted: () => {
      notify({
        message: 'La commande a été modifiée avec succès !',
        type: 'success',
      });

      setSaving(false);
      setSaved(true);
      loadingTotauxCommandesForDocument.refetch && loadingTotauxCommandesForDocument.refetch();
      loadingTotauxCommande.refetch && loadingTotauxCommande.refetch();
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification de la commande',
        type: 'error',
      });

      setSaving(false);
    },
  });

  const [deleteCommande] = useDelete_Prt_CommandeMutation({
    onCompleted: () => {
      notify({
        message: 'La commande a été supprimée avec succès !',
        type: 'success',
      });
      loadingCommandes.refetch && loadingCommandes.refetch();
      loadingTotauxCommandesForDocument.refetch && loadingTotauxCommandesForDocument.refetch();
      loadingTotauxCommande.refetch && loadingTotauxCommande.refetch();
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression de la commande',
        type: 'error',
      });
    },
  });

  const handleOpenAdd = () => {
    toggleOpenEditDialog();
    setHistoriqueToEdit(undefined);
  };

  const handleEdit = (historique: Historique) => {
    toggleOpenEditDialog();
    setHistoriqueToEdit(historique);
  };

  const handleConfirmDeleteOne = (): void => {
    if (historiqueToEdit) {
      handleRequestDelete(historiqueToEdit);
      setOpenDeleteDialog(false);
    }
  };

  const handleShowMenuClick = (reunion: Historique, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    setHistoriqueToEdit(reunion);
    setAnchorEl(event.currentTarget);
  };

  const handleShowDetail = (data: Historique) => {
    setHistoriqueToEdit(data);
    data.id && handleOnRequestLignesCommandes(data.id);
    // setOpenShowDialog(true);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const handleClearSelection = () => {
    setCommandeSelected([]);
    onRequestSelected && onRequestSelected([]);
  };

  const handleSelectAll = () => {
    setCommandeSelected(data || []);
    onRequestSelected && onRequestSelected(data || []);
  };

  const handleSelectChange = (dataSelected: any) => {
    setCommandeSelected(dataSelected);
    onRequestSelected && onRequestSelected(dataSelected);
  };

  const saveCommande = (commande?: OnRequestSaveProps, fichiers?: any[]) => {
    if (commande?.id) {
      updateCommande({
        variables: {
          input: {
            id: commande.id,
            update: {
              idLaboratoire: commande?.idTypeAssocie as any,
              nbrRef: commande.numeroCommande,
              dateCommande: commande.date,
              prixNetTotalHT: commande.totalHT,
              totalTVA: commande.totalTVA,
              prixNetTotalTTC: commande.totalTTC,
              source: 'PLATEFORME',
              fichiers,
            },
          },
        },
      });
    } else {
      console.log('commande to save', commande);
      createCommande({
        variables: {
          input: {
            pRTCommande: {
              idLaboratoire: commande?.idTypeAssocie as any,
              nbrRef: commande?.numeroCommande,
              dateCommande: commande?.date,
              prixNetTotalHT: commande?.totalHT,
              totalTVA: commande?.totalTVA,
              prixNetTotalTTC: commande?.totalTTC,
              source: 'PLATEFORME',
              fichiers,
            },
          },
        },
      });
    }
  };

  const handleRequestSave = (commande?: OnRequestSaveProps) => {
    setSaving(true);
    setSaved(false);
    if (commande?.fichiers && commande.fichiers.length > 0) {
      uploadFiles(commande.fichiers, {
        directory: 'relation-fournisseurs/historique-commandes',
      })
        .then((uploadedFiles) => {
          saveCommande(commande, uploadedFiles);
        })
        .catch((error) => {
          setSaving(false);
          notify({
            type: 'error',
            message: `Une erreur s'est produite lors de chargement des fichiers`,
          });
        });
    } else {
      saveCommande(commande, undefined);
    }
  };

  const handleRequestDelete = (historique: Historique) => {
    if (historique.id) {
      deleteCommande({
        variables: {
          input: {
            id: historique.id,
          },
        },
      });
    }
  };

  const handleOnRequestLignesCommandes = (idCommande: string) => {
    loadLignesCommandes({
      variables: {
        idCommande,
      },
    });
  };

  const handleOnRequestSearch = ({ skip, take, searchText, filterFacture, sorting }: FilterHistorique) => {
    const filterAnd: PrtCommandeFilter[] = [
      {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
    ];

    if (idLabo?.length)
      filterAnd.push({
        idLaboratoire: {
          in: idLabo,
        },
      });

    if (searchText != '' && moment(searchText, 'DD/MM/YYYY').isValid()) {
      filterAnd.push({
        dateCommande: {
          between: {
            lower: startTime(moment(searchText, 'DD/MM/YYYY').add(1, 'day')),
            upper: endTime(moment(searchText, 'DD/MM/YYYY').add(1, 'day')),
          },
        },
      });
    }

    if (
      searchText &&
      searchText !== '' &&
      !moment(searchText, 'DD/MM/YYYY').isValid() &&
      !isNaN(parseInt(searchText))
    ) {
      filterAnd.push({
        nbrRef: {
          eq: parseInt(searchText),
        },
      });
    }

    loadTotauxCommandesForDocument({
      variables: {
        idTypeAssocie: idLabo?.length ? idLabo[0] : undefined,
        //idDocument: filterFacture?.idDocument,
        //searchText,
      },
    });
    if (isFacture) {
      if (filterFacture?.idDocument) {
        loadCommandesForDocument({
          variables: {
            idTypeAssocie: idLabo?.length ? idLabo[0] : undefined,
            idDocument: filterFacture.idDocument,
            searchText,
            skip,
            take,
          },
        });
        loadTotalRowsForDocument({
          variables: {
            idTypeAssocie: idLabo?.length ? idLabo[0] : undefined,
            idDocument: filterFacture.idDocument,
            searchText,
          },
        });
      }
      // loadTotauxCommandesForDocument({
      //   variables: {
      //     idTypeAssocie: idlabo,
      //     idDocument: filterFacture?.idDocument,
      //     searchText,
      //   },
      // });
    } else {
      if (sorting) {
        loadCommandes({
          variables: {
            paging: { offset: skip, limit: take },
            filter: {
              and: filterAnd,
            },
            sorting,
          },
        });
      } else {
        loadCommandes({
          variables: {
            paging: { offset: skip, limit: take },
            filter: {
              and: filterAnd,
            },
          },
        });
        loadTotauxCommande({
          variables: {
            idLaboratoire: idLabo?.length ? idLabo[0] : undefined,
            idPharmacie: pharmacie.id,
          },
        });
      }

      loadTotalRows({
        variables: {
          filter: {
            and: filterAnd,
          },
        },
      });
      loadCommandeStatuts();
    }
  };

  const handleChangeStatut = (id?: string, statut?: string) => {
    if (id && statut) {
      changeCommandeStatut({
        variables: {
          id,
          statut,
        },
      });
    }
  };

  const columns = useColumns(
    handleShowMenuClick,
    anchorEl,
    open,
    handleClose,
    handleEdit,
    setOpenDeleteDialog,
    handleShowDetail,
    historiqueToEdit,
    isFacture,
    handleChangeStatut,
    loadingCommandeStatuts.data?.pRTCommandeStatuts.nodes
  );

  const theme = useTheme();
  const match = useMediaQuery(theme.breakpoints.down('sm'));

  const handleChangeFilter = (_data: any) => {};

  const loading = isFacture ? loadingCommandesForDocument?.loading : loadingCommandes?.loading;
  const error = isFacture ? loadingCommandesForDocument?.error : loadingCommandes?.error;
  const data = (
    (isFacture
      ? loadingCommandesForDocument.data?.getCommandeForDocument
      : loadingCommandes.data?.pRTCommandes.nodes) || []
  ).map((commande) => ({
    id: commande.id,
    referenceCommande: commande.nbrRef || 0,
    date: commande.dateCommande,
    totalHT: parseFloat(commande.prixNetTotalHT?.toFixed(4) || '0'),
    totalTTC: parseFloat((commande.prixNetTotalTTC || 0).toFixed(4)),
    totalTVA: parseFloat((commande.totalTVA || 0).toFixed(4)),
    status: commande.statut || '',
    source: commande.source || '',
    idLaboratoire: commande.idLaboratoire as string,
    typeAssocie: commande.typeAssocie || undefined,
  }));

  const totaux = isFacture
    ? {
        totalHT: loadingTotauxCommandesForDocument.data?.getCommandesForDocumentTotaux?.totalHT || 0,
        totalTVA: loadingTotauxCommandesForDocument.data?.getCommandesForDocumentTotaux?.totalTVA || 0,
        totalTTC: loadingTotauxCommandesForDocument.data?.getCommandesForDocumentTotaux?.totalTTC || 0,
      }
    : {
        totalHT: parseFloat(parseFloat(loadingTotauxCommande.data?.getTotauxCommandes?.totalHT || '0').toFixed(4)),
        totalTVA: parseFloat(parseFloat(loadingTotauxCommande.data?.getTotauxCommandes?.totalTVA || '0').toFixed(4)),
        totalTTC: parseFloat(parseFloat(loadingTotauxCommande.data?.getTotauxCommandes?.totalTTC || '0').toFixed(4)),
      };

  React.useEffect(() => {
    if (selectedItemsIds?.length && (data || []).length > 0) {
      const selectedRows = (data || []).filter((item) => selectedItemsIds.includes(item.id || ''));
      setCommandeSelected(selectedRows);
    }
  }, [selectedItemsIds, data]);

  return (
    <>
      <Box className={classes.root}>
        {match ? (
          <>
            <Box mt={3} className={classes.historiqueRoot}>
              <div className={classes.historiqueTitre}>
                <div>
                  <Box className={classes.titre}>Total TTC</Box>
                  <Typography className={classes.sousTitre}>{totaux?.totalTTC}</Typography>
                </div>
                <div>
                  <Box className={classes.titre}>Total HT</Box>
                  <Typography className={classes.sousTitre}>{totaux?.totalHT}</Typography>
                </div>
                <div>
                  <Box className={classes.titre}>Total TVA</Box>
                  <Typography className={classes.sousTitre}>{totaux?.totalTVA}</Typography>
                </div>
              </div>
            </Box>
            <div>
              {data?.map((item) => (
                <>
                  <div className={classes.historiqueItems}>
                    <div>
                      <div className={classes.historiqueListeItems}>
                        <div className={classes.commande}>
                          <Typography className={classes.numeroCommande}>N° commande: </Typography>
                          <Typography className={classes.reference}>{item.referenceCommande}</Typography>
                        </div>
                        <Typography className={classes.status}>{item.status}</Typography>
                      </div>
                      <div className={classes.date}>{moment(item.date).format('DD/MM/YYYY')}</div>
                    </div>
                    <div className={classes.historique}>
                      <div className={classes.historiqueTitreItems}>
                        <div>
                          <Box className={classes.historiqueDetails}>Total TTC</Box>
                          <Typography className={classes.totalHistorique}>{item.totalTTC}</Typography>
                        </div>
                        <div>
                          <Box className={classes.historiqueDetails}>Total HT</Box>
                          <Typography className={classes.totalHistorique}>{item.totalHT}</Typography>
                        </div>
                        <div>
                          <Box className={classes.historiqueDetails}>Total TVA</Box>
                          <Typography className={classes.totalHistorique}>{item.totalTVA}</Typography>
                        </div>
                      </div>
                    </div>
                  </div>
                  <Divider />
                </>
              ))}
            </div>
            {/* <div onClick={() => setAjoutCommande(!ajoutCommande)} className={classes.bouttonAdd}>
              <AddIcon />
            </div> */}
            <Fab
              className={commonStyles.fab}
              onClick={() => setAjoutCommande(!ajoutCommande)}
              variant="round"
              color="secondary"
            >
              <Add />
            </Fab>
            <HistoriqueForm
              open={ajoutCommande}
              setOpen={toggleOpenEditDialog}
              mode={historiqueToEdit ? 'modification' : 'creation'}
              onRequestSave={handleRequestSave}
              saving={saving}
              commandeToEdit={historiqueToEdit}
            />
          </>
        ) : (
          <Table
            topBarComponent={
              !isFacture
                ? (searchComponent) => (
                    <TopBar
                      handleOpenAdd={handleOpenAdd}
                      titleButton="Ajouter une commande"
                      withFilter={false}
                      searchComponent={<></>}
                      // titleTopBar="Historique des commandes"
                    />
                  )
                : undefined
            }
            error={error}
            loading={loading}
            search={false}
            data={data || []}
            additionalRows={
              <TableRow>
                {isFacture && <TableCell />}
                <TableCell style={{ borderBottomColor: '#fff' }} />
                <TableCell style={{ borderBottomColor: '#fff' }}>
                  <span
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-end',
                      borderBottomColor: '#fff',
                    }}
                  >
                    Total
                  </span>
                </TableCell>
                <TableCell style={{ borderBottomColor: '#fff' }}>
                  <span className={classes.tableMonetaire}>
                    {round2Decimals(totaux?.totalTTC || 0).toFixed(2)}&nbsp;€
                  </span>
                </TableCell>
                <TableCell style={{ borderBottomColor: '#fff' }}>
                  <span className={classes.tableMonetaire}>
                    {round2Decimals(totaux?.totalHT || 0).toFixed(2)}&nbsp;€
                  </span>
                </TableCell>
                <TableCell style={{ borderBottomColor: '#fff' }}>
                  <span className={classes.tableMonetaire}>
                    {round2Decimals(totaux?.totalTVA || 0).toFixed(2)}&nbsp;€
                  </span>
                </TableCell>
                <TableCell style={{ borderBottomColor: '#fff' }} />
                <TableCell style={{ borderBottomColor: '#fff' }} />
              </TableRow>
            }
            selectable={isFacture}
            onClearSelection={isFacture ? handleClearSelection : undefined}
            onSelectAll={isFacture ? handleSelectAll : undefined}
            rowsSelected={isFacture ? commandeSelected : undefined}
            onRowsSelectionChange={isFacture ? handleSelectChange : undefined}
            columns={columns}
            rowsTotal={
              isFacture
                ? loadingTotalRowsForDocument.data?.getCommandeForDocumentRowsTotal || 0
                : loadingTotalRows.data?.pRTCommandeAggregate[0].count?.id || 0
            }
            onRunSearch={({ skip, take, searchText, sortDirection, sortBy }) => {
              redirectTo({
                ...params,
                skip: skip ? skip.toString() : undefined,
                take: take ? take.toString() : undefined,
                searchText,
                sorting: sortDirection && sortBy ? [{ field: sortBy, direction: sortDirection }] : undefined,
              });
            }}
            onSortColumn={(column: any, direction: SortDirection) => {
              redirectTo({ ...params, sorting: [{ field: column, direction }] });
            }}
          />
        )}
      </Box>
      <HistoriqueForm
        open={openEditDialog}
        setOpen={toggleOpenEditDialog}
        mode={historiqueToEdit ? 'modification' : 'creation'}
        onRequestSave={handleRequestSave}
        saving={saving}
        commandeToEdit={historiqueToEdit}
        partenaireAssocie={loadingLabo.data?.laboratoire as any}
      />
      <DetailCommande
        open={openShowDialog}
        setOpen={toggleOpenShowDialog}
        lignesCommande={(loadingLignesCommandes.data?.getCommandeLignesByIdCommande || []).map((line) => ({
          id: line.id,
          produit: line.produit as any,
          quantite: line.quantiteLivree || 0,
          remise: line.remiseLigne || ('' as any),
          total: line.prixTotalHT || 0,
        }))}
        titreCommande={historiqueToEdit?.referenceCommande}
      />
      <ConfirmDeleteDialog
        title="Suppression de Commande"
        content="Etes-vous sûr de vouloir supprimer cette Commande ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDeleteOne}
      />
    </>
  );
};

export default HistoriquePage;
