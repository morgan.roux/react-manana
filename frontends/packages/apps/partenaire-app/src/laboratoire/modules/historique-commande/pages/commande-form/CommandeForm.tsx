import React, { FC, useState, ChangeEvent, FormEvent, useEffect } from 'react';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { Box, Typography } from '@material-ui/core/';
import { useStyles } from './styles';
import { CustomDatePicker, CustomFormTextField, CustomModal } from '@lib/ui-kit';
import { Dropzone, PartenaireInput, useApplicationContext } from '@lib/common';
import { Historique } from 'src/laboratoire/types';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import { LaboratoireInfoFragment } from '@lib/common/src/federation';

export interface OnRequestSaveProps {
  idTypeAssocie?: string;
  id?: string;
  numeroCommande?: any;
  date?: Date;
  totalTTC?: any;
  totalTVA?: any;
  totalHT?: any;
  fichiers?: any[];
}

interface HistoriqueFormProps {
  mode?: 'creation' | 'modification';
  open: boolean;
  saving?: boolean;
  setOpen: (open: boolean) => void;
  onRequestSave?: (commandeToSave: OnRequestSaveProps | undefined) => void;
  commandeToEdit?: Historique;
  partenaireAssocie?: LaboratoireInfoFragment;
}

const CommandeForm: FC<HistoriqueFormProps> = ({
  mode,
  open,
  saving,
  setOpen,
  onRequestSave,
  commandeToEdit,
  partenaireAssocie,
}) => {
  const classes = useStyles();

  const { isMobile } = useApplicationContext();

  const { params } = useLaboratoireParams('commande');
  const { idLabo } = params;

  const initialFormValue: OnRequestSaveProps = {
    numeroCommande: '',
    date: new Date(),
    idTypeAssocie: idLabo?.length ? idLabo[0] : undefined,
  };
  // STATE
  const [commandeToSave, setCommandeToSave] = useState<OnRequestSaveProps>(initialFormValue);
  const [fichiersJoints, setFichiersJoints] = useState<any[]>([]);
  const [partenaire, setPartenaire] = useState<any>();

  useEffect(() => {
    setCommandeToSave({
      ...commandeToSave,
      totalTTC: parseFloat(commandeToSave?.totalHT || '0') + parseFloat(commandeToSave?.totalTVA || '0'),
    });
  }, [commandeToSave?.totalHT, commandeToSave?.totalTVA]);

  const handleChange = (event: any) => {
    if (event.name) {
      setCommandeToSave({ ...commandeToSave, [event.name]: event.value });
    }
  };

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.stopPropagation();
    e.preventDefault();
    const data: OnRequestSaveProps = {
      id: mode === 'modification' ? commandeToEdit?.id : undefined,
      numeroCommande: parseInt(commandeToSave?.numeroCommande || '0'),
      date: commandeToSave?.date,
      totalHT: parseFloat(commandeToSave?.totalHT || '0'),
      totalTVA: parseFloat(commandeToSave?.totalTVA || '0'),
      totalTTC: parseFloat(commandeToSave?.totalTTC || '0'),
      fichiers: fichiersJoints.length > 0 ? fichiersJoints : [],
      idTypeAssocie: partenaire?.id,
    };
    onRequestSave && onRequestSave(data);
  };

  useEffect(() => {
    if (commandeToEdit && mode === 'modification') {
      setPartenaire(commandeToEdit.typeAssocie);
      setCommandeToSave({
        ...commandeToEdit,
        numeroCommande: commandeToEdit.referenceCommande,
        idTypeAssocie: commandeToEdit.idLaboratoire,
      });
      setFichiersJoints(commandeToEdit.fichiers?.length ? commandeToEdit.fichiers : []);
    }
  }, [commandeToEdit]);

  useEffect(() => {
    if (open && mode === 'creation') {
      setCommandeToSave(initialFormValue);
      partenaireAssocie && setPartenaire(partenaireAssocie);
    }
  }, [open]);

  const handleChangeLaboratoire = (laboratoire: any) => {
    setPartenaire(laboratoire);
    setCommandeToSave((prev) => ({ ...prev, idTypeAssocie: laboratoire.id }));
  };

  const isFormValid = () => commandeToSave?.numeroCommande !== '' && commandeToSave?.date;

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={mode === 'creation' ? 'Nouvelle Commande' : 'Modification de la commande'}
      withBtnsActions
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      disabledButton={saving || !isFormValid()}
      fullWidth
      onClickConfirm={handleSubmit}
      fullScreen={!!isMobile}
    >
      <form style={{ width: '100%' }}>
        <Typography className={classes.subtitle}>Détail commande</Typography>

        <Box pb={1}>
          <PartenaireInput
            key="laboratoire"
            index="laboratoire"
            label="Laboratoire"
            value={partenaire}
            onChange={handleChangeLaboratoire}
          />
        </Box>

        <Box className={classes.lineBox}>
          <CustomFormTextField
            type="number"
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              handleChange({ name: 'numeroCommande', value: e.target.value })
            }
            label="Numero de commande"
            value={commandeToSave?.numeroCommande}
            required
          />
          <CustomDatePicker
            style={{ width: '100%' }}
            value={commandeToSave?.date}
            onChange={(date: MaterialUiPickersDate) => handleChange({ name: 'date', value: date })}
            label="Date de commande"
            required
            className={classes.datePicker}
          />
        </Box>

        <Box className={classes.lineBox}>
          <CustomFormTextField
            type="number"
            onChange={(e: ChangeEvent<HTMLInputElement>) => handleChange({ name: 'totalHT', value: e.target.value })}
            label="Total HT"
            value={commandeToSave?.totalHT}
            inputProps={{
              step: 0.5,
            }}
          />
          <CustomFormTextField
            type="number"
            onChange={(e: ChangeEvent<HTMLInputElement>) => handleChange({ name: 'totalTVA', value: e.target.value })}
            label="Total TVA"
            value={commandeToSave?.totalTVA}
            inputProps={{
              step: 0.5,
            }}
          />
        </Box>

        <Box className={classes.ttcBox}>
          <CustomFormTextField
            type="number"
            label="Total TTC"
            value={commandeToSave?.totalTTC}
            inputProps={{
              readOnly: true,
            }}
          />
        </Box>
        <Dropzone
          contentText="Glissez et déposez vos documents"
          selectedFiles={fichiersJoints.map((item) => {
            if (item.nomOriginal) {
              // fake object
              return {
                lastModified: 1609604208734,
                lastModifiedDate: new Date(),
                name: item.nomOriginal,
                path: item.nomOriginal,
                size: 18293,
                type: '*',
                webkitRelativePath: '',
              };
            }
            return item;
          })}
          setSelectedFiles={setFichiersJoints}
          multiple
        />
      </form>
    </CustomModal>
  );
};

export default CommandeForm;
