import React, { FC, useEffect } from 'react';
import { Column, CustomModal, Table } from '@lib/ui-kit';
import { DetailHistoriquePaiementProps } from './interfaces';
import {
  PrtSuiviOperationnelPaiementInfoFragment,
  useGet_Prt_Suivi_Operationnel_PaiementsLazyQuery,
} from '@lib/common/src/federation';
import { Typography } from '@material-ui/core';
import { useStyles } from './styles';
import moment from 'moment';
import { useApplicationContext } from '@lib/common';

export const DetailHistoriquePaiement: FC<DetailHistoriquePaiementProps> = ({
  open,
  setOpen,
  isMobile,
  suiviToEdit,
}) => {
  const { federation } = useApplicationContext();
  const [loadHistoriquePaiement, loadingHistoriquePaiement] = useGet_Prt_Suivi_Operationnel_PaiementsLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });
  const classes = useStyles();

  useEffect(() => {
    if (open && suiviToEdit) {
      loadHistoriquePaiement({
        variables: {
          filter: {
            and: [
              {
                idSuiviOperationnel: {
                  eq: suiviToEdit.id,
                },
              },
            ],
          },
        },
      });
    }
  }, [open, suiviToEdit]);

  const columns: Column[] = [
    {
      name: 'date',
      label: 'Date',
      renderer: (row: PrtSuiviOperationnelPaiementInfoFragment) => {
        return <Typography className={classes.textTable}>{moment.utc(row.date).format('DD/MM/YYYY')}</Typography>;
      },
    },
    {
      name: 'montant',
      label: 'Montant',
      renderer: (row: PrtSuiviOperationnelPaiementInfoFragment) => {
        return <Typography className={classes.textTable}>{(row.montant || 0).toFixed(2)} €</Typography>;
      },
    },
    {
      name: 'idModeReglement',
      label: 'Paiement',
      renderer: (row: PrtSuiviOperationnelPaiementInfoFragment) => {
        return <Typography className={classes.textTable}>{row?.modePaiement.libelle}</Typography>;
      },
    },
  ];

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Paiements"
      closeIcon
      headerWithBgColor
      withBtnsActions={true}
      withConfirmButton
      withCancelButton={false}
      actionButtonTitle="Fermer"
      maxWidth="lg"
      fullScreen={!!isMobile}
      onClickConfirm={() => setOpen(false)}
    >
      <Table
        error={loadingHistoriquePaiement.error as any}
        loading={loadingHistoriquePaiement.loading}
        search={false}
        data={loadingHistoriquePaiement.data?.pRTSuiviOperationnelPaiements.nodes || []}
        rowsTotal={loadingHistoriquePaiement.data?.pRTSuiviOperationnelPaiements.totalCount}
        columns={columns}
        notablePagination
        style={{
          width: '450px',
        }}
      />
    </CustomModal>
  );
};
