import { Box, Fade, Grid, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import {
  Add,
  Delete,
  Edit,
  MoreHoriz,
  Visibility,
  Description,
  LocalBar,
  LocalHospital,
  SyncAlt,
} from '@material-ui/icons';
import React, { FC, MouseEvent, useEffect, useMemo, useState } from 'react';
import { BottomScrollListener } from 'react-bottom-scroll-listener';
import { ErrorPage, ConfirmDeleteDialog, TopBar, NewCustomButton } from '@lib/ui-kit';
import { CustomAvatar, Table, TableChange } from '@lib/ui-kit';
import { DetailActionOperationnel } from './DetailActionOperationnel';
import { ExternalFormDataSuiviOperationnel, FilterAjoutCompteRendu, SuiviFormRequestSavingProps } from './interfaces';
import { useStyles } from './styles/styles';
import ActionOperationnelForm from './ActionOperationnelForm';
import { useColumn } from './../components/Column';
import classNames from 'classnames';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import { useApplicationContext, useURLSearchParams, useUploadFiles, ConsultFichierModal } from '@lib/common';
// import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import moment from 'moment';
import EventIcon from '@material-ui/icons/Event';
import PaymentIcon from '@material-ui/icons/Payment';
import { ColumnSuiviProps, PaiementFormRequestSaving } from '../components/interfaces';
import {
  PrtSuiviOperationnelFilter,
  SortDirection,
  useChange_Suivi_Opertionnel_StatutMutation,
  useCreate_Prt_Suivi_Operationnel_PaiementMutation,
  useCreate_Suivi_OperationnelMutation,
  useDelete_One_Suivi_OperationnelMutation,
  useGet_LaboratoireLazyQuery,
  useGet_List_Suivi_Operationnel_StatutLazyQuery,
  useGet_List_Suivi_Operationnel_TypesLazyQuery,
  useGet_Suivis_Operationnels_For_Compte_RenduLazyQuery,
  useGet_Suivis_Operationnels_For_Compte_Rendu_Total_RowsLazyQuery,
  useGet_Suivi_OperationnelsLazyQuery,
  useSend_Email_Action_LitigeLazyQuery,
  useUpdate_Suivi_OperationnelMutation,
} from '@lib/common/src/federation';
import { PrtSuiviOperationnelInfoFragment } from '@lib/common/src/federation';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import ArrowForward from '@material-ui/icons/ArrowForwardIos';
import { refetchStatsVar } from './../../../reactive-vars';
import { PaiementActionOperationnel } from '../components/PaiementActionOperationnel';
import { DetailHistoriquePaiement } from '../components/DetailHistoriquePaiement';

const headersExport = [
  { label: 'Type litige', key: 'type.libelle' },
  { label: 'Titre', key: 'description' },
  { label: 'Labo', key: 'typeAssocie.nom' },
  { label: 'Date', key: 'dateHeure' },
  { label: 'Montant', key: 'montant' },
  { label: 'Statut', key: 'statut.libelle' },
  { label: 'Nombre commentaires', key: 'nombreCommentaires' },
];

const ActionOperationnel: FC<{
  onRequestSelectedIds?: (ids: string[]) => void;
  suiviOperationnelSelected?: any[];
  partenaireAssocie?: any;
  categorie?: string;
  dataSuivi?: any[];
  refetchData?: () => void;
}> = ({ onRequestSelectedIds, suiviOperationnelSelected, partenaireAssocie, categorie, dataSuivi, refetchData }) => {
  const classes = useStyles();
  const { isMobile, federation, graphql, notify, currentPharmacie } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();
  const query = useURLSearchParams();
  const { params, redirectTo } = useLaboratoireParams();
  const { idLabo, view, idStatut, id: idCompteRendu, searchText, sorting } = params;

  const [openEditDialogSuivi, setOpenEditDialogSuivi] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [filter, setFilter] = useState<any>();
  const [suiviToEdit, setSuiviToEdit] = React.useState<PrtSuiviOperationnelInfoFragment>();
  const [suiviSelected, setSuiviSelected] = useState<PrtSuiviOperationnelInfoFragment[]>([]);
  const [activeFiltreAddCompteRendu, setActiveFiltreAddCompteRendu] = useState<FilterAjoutCompteRendu>({
    active: false,
    mode: undefined,
    idCompteRendu: undefined,
  });
  const [showDetail, setShowDetail] = useState<boolean>(false);
  const [modeForm, setModeForm] = useState<'creation' | 'modification' | 'cloturer'>('creation');
  const [showFichier, setShowFichier] = useState<boolean>(false);
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [externalFormData, setExternalFormData] = useState<ExternalFormDataSuiviOperationnel | undefined>();
  const [responsables, setResponsables] = useState<any[]>([]);
  const [openToLitige, setOpenToLitige] = useState<boolean>(false);
  const [typeAction, setTypeAction] = useState<string | undefined>();
  const [openModalPaiement, setOpenModalPaiement] = useState<boolean>(false);
  const [openModalHistorique, setOpenModalHistorique] = useState<boolean>(false);

  useEffect(() => {
    if (idLabo && idLabo[0]) {
      loadLabo({
        variables: {
          id: idLabo[0],
          idPharmacie: currentPharmacie.id,
        },
      });
    }
  }, [idLabo && idLabo[0]]);

  const [loadLabo, loadingLabo] = useGet_LaboratoireLazyQuery({ fetchPolicy: 'cache-and-network' });

  const [loadListTypeSuivi, loadingListTypeSuivi] = useGet_List_Suivi_Operationnel_TypesLazyQuery({
    client: federation,
  });
  const [loadListTypeLitige, loadingListTypeLitige] = useGet_List_Suivi_Operationnel_TypesLazyQuery({
    client: federation,
  });
  const [loadListStatutSuivi, loadingListStatutSuivi] = useGet_List_Suivi_Operationnel_StatutLazyQuery({
    client: federation,

    onCompleted: () => {
      handleSuiviSearch({
        skip: params.skip ? parseInt(params.skip, 10) : 0,
        take: params.take ? parseInt(params.take, 10) : 12,
        searchText,
        filter,
        activeFiltreAddCompteRendu,
        sorting,
      });
    },
  });
  const [createPaiementAction, creatingPaiementAction] = useCreate_Prt_Suivi_Operationnel_PaiementMutation({
    client: federation,
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'Paiement avec succès',
      });
      loadingSuiviOperationnel.refetch && loadingSuiviOperationnel.refetch();
    },
    onError: () => {
      notify({
        type: 'error',
        message: 'Une erreur est survenue lors du paiement',
      });
    },
  });

  const idTypes = params.idType || loadingListTypeSuivi.data?.pRTSuiviOperationnelTypes.nodes.map(({ id }) => id);
  const [changeSuiviStatut] = useChange_Suivi_Opertionnel_StatutMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Le statut a été modifié avec succès !',
        type: 'success',
      });
      loadingSuiviOperationnel.refetch && loadingSuiviOperationnel.refetch();
      if (dataSuivi && refetchData) {
        refetchData();
      }
      refetchStatsVar(true);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant le changement du statut',
        type: 'error',
      });
    },
  });
  const [loadSuiviOperationnel, loadingSuiviOperationnel] = useGet_Suivi_OperationnelsLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });
  const [createSuiviOperationnel, creatingSuiviOperationnel] = useCreate_Suivi_OperationnelMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Le Suivi Operationnel a été crée avec succés !',
        type: 'success',
      });
      loadingSuiviOperationnel?.refetch && loadingSuiviOperationnel.refetch();
      if (view === 'creation') {
        loadingActionFromCompteRendu?.refetch && loadingActionFromCompteRendu.refetch();
        loadingLitigeFromCompteRendu?.refetch && loadingLitigeFromCompteRendu.refetch();
      }

      refetchStatsVar(true);

      setSaving(false);
    },
    onError: () => {
      notify({
        message: 'Erreurs lors de la création du Suivi Operationnel',
        type: 'error',
      });
      setSaving(false);
    },
  });
  const [updateSuiviOperationnel] = useUpdate_Suivi_OperationnelMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message:
          dataSuivi && modeForm !== 'modification'
            ? "L'action a été tranformée en litige avec succés"
            : 'Le Suivi Operationnel a été modifié avec succés !',
        type: 'success',
      });
      if (dataSuivi && refetchData) {
        refetchData();
      } else {
        loadingSuiviOperationnel?.refetch && loadingSuiviOperationnel.refetch();
        //if (view !== 'creation') refetchLitige();
        refetchStatsVar(true);
        setSaving(false);
      }
    },
    onError: () => {
      notify({
        message: dataSuivi
          ? 'Erreurs lors de la transformation en litige'
          : 'Erreurs lors de la modification du Suivi Operationnel',
        type: 'error',
      });
      setSaving(false);
    },
  });
  const [deleteSuiviOperationnel] = useDelete_One_Suivi_OperationnelMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Le Suivi Operationnel a été supprimé avec succès !',
        type: 'success',
      });
      if (dataSuivi && refetchData) {
        refetchData();
      } else {
        loadingSuiviOperationnel?.refetch && loadingSuiviOperationnel.refetch();
        if (view === 'creation') {
          loadingActionFromCompteRendu?.refetch && loadingActionFromCompteRendu.refetch();
          loadingLitigeFromCompteRendu?.refetch && loadingLitigeFromCompteRendu.refetch();
        }

        refetchStatsVar(true);
      }
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression du Suivi Operationnel',
        type: 'error',
      });
    },
  });

  const laboratoireSuivi = dataSuivi
    ? { data: dataSuivi, loading: false, error: false }
    : {
        data: loadingSuiviOperationnel?.data?.pRTSuiviOperationnels?.nodes as any,
        loading: loadingSuiviOperationnel?.loading as any,
        error: loadingSuiviOperationnel?.error as any,
      };

  const [loadActionFromCompteRendu, loadingActionFromCompteRendu] =
    useGet_Suivis_Operationnels_For_Compte_RenduLazyQuery({
      client: federation,
      fetchPolicy: 'cache-and-network',
    });

  const [loadLitigeFromCompteRendu, loadingLitigeFromCompteRendu] =
    useGet_Suivis_Operationnels_For_Compte_RenduLazyQuery({
      client: federation,
      fetchPolicy: 'cache-and-network',
    });

  const [loadTotalForCompteRendu, loadingTotalForCompteRendu] =
    useGet_Suivis_Operationnels_For_Compte_Rendu_Total_RowsLazyQuery({
      client: federation,
      fetchPolicy: 'cache-and-network',
    });

  const [sendEmailLitige, sendingEmailLitige] = useSend_Email_Action_LitigeLazyQuery({
    fetchPolicy: 'network-only',
    client: federation,
    onCompleted: () => {
      notify({
        type: sendingEmailLitige.data?.sendEmailActionLitige.statut as any,
        message: sendingEmailLitige.data?.sendEmailActionLitige.message,
      });
    },
    onError: () => {
      notify({
        type: 'error',
        message: sendingEmailLitige.data?.sendEmailActionLitige.message || 'une erreur est survenue',
      });
    },
  });

  useEffect(() => {
    loadListTypeSuivi({
      variables: {
        filter: {
          categorie: {
            eq: categorie || 'LITIGE',
          },
        },
      },
    });
    loadListTypeLitige({
      variables: {
        filter: {
          categorie: {
            eq: 'LITIGE',
          },
        },
      },
    });
    loadListStatutSuivi();
  }, [categorie]);

  const loadingSuiviFromCompteRendu =
    categorie === 'ACTION' ? loadingActionFromCompteRendu : loadingLitigeFromCompteRendu;

  useEffect(() => {
    if (sendingEmailLitige.loading) {
      notify({
        type: 'success',
        message: `En cours d'envoi d'email`,
      });
    }
  }, [sendingEmailLitige]);

  useEffect(() => {
    if (suiviOperationnelSelected && suiviOperationnelSelected.length > 0 && !loadingSuiviFromCompteRendu.loading) {
      const list = suiviOperationnelSelected.map((selected) =>
        [
          ...(loadingActionFromCompteRendu.data?.getSuiviOperationnelForCompteRendu || []),
          ...(loadingLitigeFromCompteRendu.data?.getSuiviOperationnelForCompteRendu || []),
        ].find((el) => el.id === selected)
      );
      setSuiviSelected(list as any);
    }
  }, [suiviOperationnelSelected, loadingActionFromCompteRendu.loading, loadingLitigeFromCompteRendu.loading]);

  useEffect(() => {
    if (view === 'creation') {
      setActiveFiltreAddCompteRendu({
        active: true,
        mode: 'AJOUT',
      });
    }

    if (view === 'edit-compte-rendu' && query.get('idCompteRendu')) {
      setActiveFiltreAddCompteRendu({
        active: true,
        mode: 'MODIFIER',
        idCompteRendu: query.get('idCompteRendu') as any,
      });
    }
  }, [query.get('idCompteRendu'), view]);

  useEffect(() => {
    if ((loadingListStatutSuivi.data?.pRTSuiviOperationnelStatuts.nodes.length || 0) > 0) {
      handleSuiviSearch({
        skip: params.skip ? parseInt(params.skip, 10) : 0,
        take: params.take ? parseInt(params.take, 10) : 12,
        searchText,
        filter,
        activeFiltreAddCompteRendu,
        sorting,
      });
    }
  }, [
    params.skip,
    params.take,
    searchText,
    filter,
    activeFiltreAddCompteRendu,
    sorting && sorting[0] && sorting[0].field,
    sorting && sorting[0] && sorting[0].direction,
    loadingListStatutSuivi.data?.pRTSuiviOperationnelStatuts.nodes.length,
    JSON.stringify([idLabo]),
    JSON.stringify([idTypes]),
    JSON.stringify([idStatut]),
    partenaireAssocie,
    categorie,
  ]);

  useEffect(() => {
    if (
      loadingSuiviFromCompteRendu &&
      loadingSuiviFromCompteRendu.data?.getSuiviOperationnelForCompteRendu &&
      loadingSuiviFromCompteRendu.data?.getSuiviOperationnelForCompteRendu.length > 0 &&
      laboratoireSuivi &&
      (laboratoireSuivi.data || []).length > 0
    ) {
      const selectedRows = (laboratoireSuivi.data || []).filter((item: { id: any }) =>
        (loadingSuiviFromCompteRendu.data?.getSuiviOperationnelForCompteRendu || [])
          .map((el) => {
            return el.id;
          })
          .includes(item.id)
      );
      onRequestSelectedIds && onRequestSelectedIds(selectedRows);
      setSuiviSelected(selectedRows);
    }
  }, [loadingSuiviFromCompteRendu.data?.getSuiviOperationnelForCompteRendu, laboratoireSuivi?.data]);

  const open = useMemo(() => Boolean(anchorEl), [anchorEl]);

  const toggleOpenEditDialogSuivi = () => {
    setOpenEditDialogSuivi((prev) => !prev);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const handleOpenAddLabo = (type?: string) => {
    setModeForm('creation');
    setTypeAction(type);
    handleParticipant([]);
    setSuiviToEdit(undefined);
    toggleOpenEditDialogSuivi();
    handleChangeProjectWhenEdit(undefined, undefined);
    handleResponsableSelected && handleResponsableSelected([]);
  };

  const handleEdit = (suivi: PrtSuiviOperationnelInfoFragment) => {
    setModeForm('modification');
    suivi.participants.length > 0 ? setTypeAction('PHARMACIE') : setTypeAction('LABO'),
      handleChangeImportance(suivi.importance);
    toggleOpenEditDialogSuivi();
    handleChangeProjectWhenEdit(suivi.idTache, suivi.idFonction);
    handleParticipant(suivi.participants || []);
    handleResponsableSelected && handleResponsableSelected(suivi.contacts || []);
  };

  const handleShowTypeLitige = (suivi: PrtSuiviOperationnelInfoFragment) => {
    setSuiviToEdit(suivi);
    setOpenToLitige(true);
  };

  const handleChangeImportance = (importance: any) => {
    setExternalFormData((prev) => ({ ...prev, selectedImportance: importance }));
  };

  const handleChangeProjectWhenEdit = (idTache?: any, idFonction?: any) => {
    setExternalFormData((prev) => ({ ...prev, selectedTache: { idTache, idFonction } }));
  };

  const handleResponsableSelected = (value: any[]) => {
    if (responsables.length > 0) {
      setExternalFormData({ ...externalFormData, selectedParticipant: undefined });
    }
    setResponsables(value);
  };

  const handleParticipant = (participants: any[], _id?: string) => {
    if (participants.length > 0) {
      setResponsables([]);
    }
    setExternalFormData((prev) => ({ ...prev, selectedParticipant: participants }));
  };

  const handleConfirmDeleteOne = (): void => {
    if (suiviToEdit) {
      handleSuiviDelete && handleSuiviDelete(suiviToEdit);
      setOpenDeleteDialog(false);
    }
  };

  const handleShowMenuClick = (suivi: PrtSuiviOperationnelInfoFragment, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    setSuiviToEdit(suivi);
    setAnchorEl(event.currentTarget);
  };

  const handleContainerOnBottom = () => {
    handleFetchMoreSuivis();
  };

  const handleNextDataTable = ({ skip, take, searchText, sortBy, sortDirection }: TableChange) => {
    // setSkip(skip);
    // setTake(take);
    // setSearchText(searchText);
    redirectTo({
      ...params,
      skip: skip ? skip.toString() : undefined,
      take: take ? take.toString() : undefined,
      searchText,
      sorting: sortBy && sortDirection ? [{ field: sortBy, direction: sortDirection }] : undefined,
    });
  };

  const handleChangeFilter = (data: any) => {
    setFilter(data);
  };

  const handleShowDetail = (suivi: PrtSuiviOperationnelInfoFragment) => {
    redirectTo({ id: suivi.id, backParams: params }, 'detail-action-operationnelle');
  };

  const handleRowDetail = (row: PrtSuiviOperationnelInfoFragment) => {
    handleShowDetail(row);
  };

  const handleShowFichier = (suivi: PrtSuiviOperationnelInfoFragment) => {
    setShowFichier(true);
    setSuiviToEdit(suivi);
  };

  const handleToLitige = (idType: string) => {
    if (suiviToEdit) {
      const toSave = {
        montant: parseFloat(suiviToEdit.montant as any),
        idImportance: suiviToEdit.importance?.id || undefined,
        idTache: (suiviToEdit.idTache as any) || undefined,
        idFonction: (suiviToEdit.idFonction as any) || undefined,
        idContacts: (suiviToEdit.contacts || []).map(({ id }) => id),
        description: suiviToEdit.description,
        dateHeure: suiviToEdit.dateHeure,
        partenaireType: 'LABORATOIRE',
        idTypeAssocie: suiviToEdit.idTypeAssocie,
        idType: idType,
        fichiers: suiviToEdit.fichiers as any,
        idParticipants: (suiviToEdit.participants || []).map(({ id }) => id),
      };
      updateSuiviOperationnel({
        variables: {
          id: suiviToEdit.id,
          input: toSave,
        },
      });
    }
  };

  const handleSendEmail = (item: PrtSuiviOperationnelInfoFragment) => {
    sendEmailLitige({
      variables: {
        input: {
          idType: item.type.id,
          id: item.id,
        },
      },
    });
  };

  const action = (item: PrtSuiviOperationnelInfoFragment) => {
    return (
      <div key={`row-${item.id}`}>
        <IconButton
          aria-controls="simple-menu"
          aria-haspopup="true"
          // eslint-disable-next-line react/jsx-no-bind
          onClick={handleShowMenuClick.bind(null, item)}
          className={`${classes.iconButton} ${classes.avatarSuiviMobile}`}
        >
          {isMobile ? (
            <>
              {!item.participants ? <LocalBar /> : <LocalHospital />}
              {item.participants.length > 0 ? (
                <CustomAvatar
                  name={item.participants?.length ? (item.participants[0].fullName as any) : ''}
                  url={item.participants?.length ? (item?.participants[0].photo?.publicUrl as any) : ''}
                />
              ) : (
                <CustomAvatar
                  name={item.contacts?.length ? (item.contacts[0].nom as any) : ''}
                  url={item.contacts?.length ? (item?.contacts[0].photo?.publicUrl as any) : ''}
                />
              )}
            </>
          ) : (
            <MoreHoriz />
          )}
        </IconButton>
        {openToLitige ? (
          <Menu
            id="fade-menu"
            anchorEl={anchorEl}
            keepMounted
            open={open && item.id === suiviToEdit?.id}
            onClose={handleClose}
            TransitionComponent={Fade}
          >
            <Typography variant="inherit" className={classes.typeText}>
              Choisir le type de litige:
            </Typography>
            {(loadingListTypeLitige.data?.pRTSuiviOperationnelTypes?.nodes || []).map((type) => (
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  setOpenToLitige(false);
                  handleToLitige(type.id);
                }}
                key={`type-action${type.id}`}
              >
                <Typography variant="inherit">{type.libelle}</Typography>
              </MenuItem>
            ))}
          </Menu>
        ) : (
          <Menu
            id="fade-menu"
            anchorEl={anchorEl}
            keepMounted
            open={open && item.id === suiviToEdit?.id}
            onClose={handleClose}
            TransitionComponent={Fade}
          >
            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                setOpenModalPaiement(true);
                handleClose();
              }}
              disabled={item.statut.code === 'CLOTURE'}
            >
              <ListItemIcon>
                <PaymentIcon />
              </ListItemIcon>
              <Typography variant="inherit">Paiement</Typography>
            </MenuItem>
            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                handleShowFichier(item);
                handleClose();
              }}
            >
              <ListItemIcon>
                <Description />
              </ListItemIcon>
              <Typography variant="inherit">Consulter le fichier ci-joint</Typography>
            </MenuItem>
            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                handleShowDetail(item);
                handleClose();
              }}
            >
              <ListItemIcon>
                <Visibility />
              </ListItemIcon>
              <Typography variant="inherit">Voir détail</Typography>
            </MenuItem>
            {item.type.categorie !== 'LITIGE' && (
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleShowTypeLitige(item);
                }}
              >
                <ListItemIcon>
                  <SyncAlt />
                </ListItemIcon>
                <Typography variant="inherit">Transformer en litige</Typography>
              </MenuItem>
            )}
            {/*item.type.categorie === 'LITIGE' && (
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleSendEmail(item);
                  handleClose();
                }}
              >
                <ListItemIcon>
                  <SyncAlt />
                </ListItemIcon>
                <Typography variant="inherit">Envoyer l'email</Typography>
              </MenuItem>
            )*/}
            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                handleEdit(item);
                handleClose();
              }}
            >
              <ListItemIcon>
                <Edit />
              </ListItemIcon>
              <Typography variant="inherit">Modifier</Typography>
            </MenuItem>
            <MenuItem
              onClick={(event) => {
                event.stopPropagation();
                handleClose();
                setOpenDeleteDialog(true);
              }}
            >
              <ListItemIcon>
                <Delete />
              </ListItemIcon>
              <Typography variant="inherit">Supprimer</Typography>
            </MenuItem>
          </Menu>
        )}
      </div>
    );
  };

  const handleClearSelection = () => {
    setSuiviSelected([]);
  };

  const handleSelectAll = () => {
    setSuiviSelected(laboratoireSuivi?.data || []);
  };

  const handleSelectChange = (dataSelected: any) => {
    onRequestSelectedIds &&
      onRequestSelectedIds(
        (dataSelected || []).map((el: any) => {
          return el.id;
        })
      );
    setSuiviSelected(dataSelected);
  };

  const handleSortTable = (column: any, direction: SortDirection) => {
    //setSortTable({ column, direction });
    redirectTo({ ...params, sorting: [{ field: column, direction }] });
  };

  const handleChangeStatut = (id: string, value: string) => {
    changeSuiviStatut({
      variables: {
        id,
        idStatut: value,
      },
    });
  };

  const handleFetchMoreSuivis = () => {
    const fetchMore = loadingSuiviOperationnel.fetchMore;
    const variables = loadingSuiviOperationnel.variables;
    const paging = variables?.paging;
    if (loadingSuiviOperationnel.data?.pRTSuiviOperationnels.nodes) {
      fetchMore &&
        fetchMore({
          variables: {
            ...variables,
            paging: {
              ...paging,
              offset: loadingSuiviOperationnel.data?.pRTSuiviOperationnels.nodes,
            },
          },

          updateQuery: (prev: any, { fetchMoreResult }) => {
            if (prev?.PrtSuiviOperationnelInfoFragments && fetchMoreResult?.PrtSuiviOperationnelInfoFragments.nodes) {
              return {
                ...prev,
                PrtSuiviOperationnelInfoFragments: {
                  ...prev.PrtSuiviOperationnelInfoFragments,
                  nodes: [
                    ...prev.PrtSuiviOperationnelInfoFragments.nodes,
                    ...fetchMoreResult.PrtSuiviOperationnelInfoFragments.nodes,
                  ],
                },
              };
            }

            return prev;
          },
        });
    }
  };

  const handleSuiviSearch = ({ skip, take, searchText, filter: filters, sorting }: any) => {
    if (view === 'creation') {
      loadActionFromCompteRendu({
        variables: {
          idTypeAssocie: partenaireAssocie?.id || (idLabo && idLabo[0]),
          idCompteRendu,
          searchText,
          skip,
          take,
          categorie: 'ACTION',
        },
      });
      loadLitigeFromCompteRendu({
        variables: {
          idTypeAssocie: partenaireAssocie?.id || (idLabo && idLabo[0]),
          idCompteRendu,
          searchText,
          skip,
          take,
          categorie: 'LITIGE',
        },
      });
      loadTotalForCompteRendu({
        variables: {
          idTypeAssocie: partenaireAssocie?.id || (idLabo && idLabo[0]),
          idCompteRendu,
          searchText,
        },
      });
    } else {
      const filterAnd: PrtSuiviOperationnelFilter[] = [
        {
          partenaireType: {
            eq: 'LABORATOIRE',
          },
        },
        {
          idPharmacie: {
            eq: currentPharmacie.id,
          },
        },
      ];

      if (searchText) {
        filterAnd.push({
          description: {
            iLike: `%${searchText}%`,
          },
        });
      }

      if (idLabo?.length) {
        filterAnd.push({
          or: idLabo.map((id) => ({ idTypeAssocie: { eq: id } })),
        });
      }

      const sortTableFilter = sorting || [];

      if (idStatut?.length) filterAnd.push({ or: idStatut.map((id) => ({ idStatut: { eq: id } })) });

      if (idTypes?.length) filterAnd.push({ or: idTypes.map((id) => ({ idType: { eq: id } })) });

      loadSuiviOperationnel({
        variables: {
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: skip,
            limit: take,
          },
          sorting: sortTableFilter,
        },
      });
    }
  };

  const saveSuiviOperationnel = (suiviOperationnel: SuiviFormRequestSavingProps, fichiers: any) => {
    if (suiviOperationnel.id) {
      const toSave = {
        montant: parseFloat(suiviOperationnel.montant as any),
        idImportance: suiviOperationnel.importance?.id || undefined,
        idTache: (suiviOperationnel.tache?.id as any) || undefined,
        idFonction: (suiviOperationnel.fontion?.id as any) || undefined,
        idContacts: suiviOperationnel.idContacts,
        description: suiviOperationnel.description,
        dateHeure: suiviOperationnel.dateSuivi,
        partenaireType: 'LABORATOIRE',
        idTypeAssocie: suiviOperationnel.idTypeAssocie || '',
        idType: suiviOperationnel.idTypeSuivi,
        fichiers,
        idParticipants: (suiviOperationnel.idParticipants || [])
          .map((el) => {
            return el.id || undefined;
          })
          .filter((response) => {
            return response !== undefined;
          }),
      };
      updateSuiviOperationnel({
        variables: {
          id: suiviOperationnel.id,
          input: (suiviOperationnel as any).idStatut
            ? { ...toSave, idStatut: (suiviOperationnel as any).idStatut }
            : toSave,
        },
      });
    } else {
      createSuiviOperationnel({
        variables: {
          input: {
            montant: parseFloat(suiviOperationnel.montant as any),
            idImportance: suiviOperationnel.importance?.id || '',
            idTache: suiviOperationnel.tache?.id as any,
            idFonction: suiviOperationnel.fontion?.id as any,
            idContacts: suiviOperationnel.idContacts,
            description: suiviOperationnel.description,
            dateHeure: suiviOperationnel.dateSuivi,
            partenaireType: 'LABORATOIRE',
            idTypeAssocie: suiviOperationnel.idTypeAssocie || '',
            idType: suiviOperationnel.idTypeSuivi,
            fichiers,
            idParticipants: (suiviOperationnel.idParticipants || [])
              .map((el) => {
                return el.id || undefined;
              })
              .filter((response) => {
                return response !== undefined;
              }),
          },
        },
      });
    }
  };

  const handleSuiviSave = (suiviOperationnel: SuiviFormRequestSavingProps) => {
    setSaving(true);
    setSaved(false);
    if (suiviOperationnel.selectedFiles && suiviOperationnel.selectedFiles.length > 0) {
      uploadFiles(suiviOperationnel.selectedFiles, {
        directory: 'relation-fournisseurs/actions-operationnelles',
      })
        .then((uploadedFiles) => {
          saveSuiviOperationnel(suiviOperationnel, uploadedFiles);
        })
        .catch((error) => {
          notify({
            type: 'error',
            message: `Une erreur s'est produite lors de chargement des fichiers`,
          });
          setSaving(false);
          setSaved(false);
        });
    } else {
      saveSuiviOperationnel(suiviOperationnel, undefined);
    }
  };

  const handleSuiviDelete = (suiviOperationnel: any) => {
    deleteSuiviOperationnel({
      variables: {
        input: { id: suiviOperationnel.id },
      },
    });
  };

  const handleRowHistoriquePaiement = (suiviOperationnel: PrtSuiviOperationnelInfoFragment) => {
    setSuiviToEdit(suiviOperationnel);
    setOpenModalHistorique(true);
  };

  const columnsSuiviProps: ColumnSuiviProps = {
    action,
    handleChangeStatut,
    listStatutSuivi: loadingListStatutSuivi?.data?.pRTSuiviOperationnelStatuts.nodes,
    activeItem: view || dataSuivi ? 'detail-action' : '',
    handleRowDetail,
    handleRowHistoriquePaiement,
  };

  const handleCloturerClick = (suivi: PrtSuiviOperationnelInfoFragment) => {
    toggleOpenEditDialogSuivi();
    setSuiviToEdit(suivi);
  };

  const handleSavePaiement = (data: PaiementFormRequestSaving) => {
    createPaiementAction({
      variables: {
        input: {
          montant: data.montant,
          date: moment.utc(data.date),
          idModeReglement: data.idModePaiement,
          idSuiviOperationnel: data.idSuiviOperationnel,
        },
      },
    });
  };

  const columns = useColumn(columnsSuiviProps);

  const dataExport = (laboratoireSuivi?.data || []).map((suivi: PrtSuiviOperationnelInfoFragment) => ({
    ...suivi,
    dateHeure: moment.utc(suivi.dateHeure).format('DD/MM/YYYY HH:mm'),
  }));

  const topBarComponent =
    !view && !dataSuivi
      ? (searchComponent: any) => (
          <TopBar
            // handleOpenAdd={handleOpenAdd}
            searchComponent={searchComponent}
            titleButton=" "
            titleTopBar=" "
            withMoreButton
            buttons={[
              {
                component: (
                  <Box ml={2}>
                    {/*<CSVLink data={dataExport || ''} headers={headersExport} className={classes.exportButton}>
                      Exporter
                </CSVLink> FIXME : Export HTML */}
                    <NewCustomButton onClick={() => handleOpenAddLabo()}>
                      <Add />
                      NOUVEAU LITIGE
                    </NewCustomButton>
                  </Box>
                ),
              },
              // {
              //   component: (
              //     <Box ml={2}>
              //       <NewCustomButton onClick={handleOpenAddPharma}>
              //         <Add />
              //         NOUVELLE ACTION PHARMACIE
              //       </NewCustomButton>
              //     </Box>
              //   ),
              // },
            ]}
            withFilter={false}
          />
        )
      : undefined;
  const BottomScrollListenerPage = (
    <BottomScrollListener onBottom={handleContainerOnBottom}>
      {(scrollRef) => (
        <Grid
          id="infinity-suivi"
          item
          ref={scrollRef as any}
          className={classes.infiniteScrollContainer}
          style={view === 'ajout-compte-rendu' || view === 'edit-compte-rendu' ? { height: 'auto' } : {}}
        >
          <Box py={2} px={0}>
            {laboratoireSuivi?.data?.map((item: PrtSuiviOperationnelInfoFragment, index: any) => {
              const statusUtils = (statusCode: string) => {
                switch (statusCode) {
                  case 'EN_COURS':
                    return {
                      styles: { background: '#e3eff4', color: '#67bade' },
                      libelle: 'EN COURS',
                    };
                  case 'CLOTURE':
                    return {
                      styles: { background: '#d5eddf', color: '#34a745' },
                      libelle: 'TERMINEE',
                    };

                  case 'PLANIFIER':
                    return {
                      styles: { background: '#dbdbdc', color: '#27272f' },
                      libelle: 'A FAIRE',
                    };
                  default:
                    break;
                }
              };
              return (
                <>
                  <Box className={classes.cardActionOp}>
                    <div className={classes.leftBannerActionOp} style={{ background: `${item.importance?.couleur}` }} />
                    <Box display="flex" justifyContent="space-between">
                      <Typography className={classes.textTitle}>
                        <span dangerouslySetInnerHTML={{ __html: item.description }} />
                      </Typography>
                      <Typography style={{ fontSize: 12 }}>{item.type?.libelle || ''}</Typography>
                    </Box>
                    <Box display="flex" justifyContent="space-between" alignItems="center">
                      <Box display="flex" alignItems="center">
                        <Typography className={classes.dateIcons}>
                          <EventIcon /> {item.dateHeure && moment(item.dateHeure).format('DD/MM/YYYY')}
                        </Typography>
                        <FiberManualRecordIcon style={{ width: 7, height: 7, marginRight: 5 }} />
                        <Typography>{item.montant || 0} €</Typography>
                      </Box>
                      <Typography className={classes.btnDetails}>
                        <IconButton onClick={() => handleShowDetail(item)} style={{ padding: 0 }}>
                          <ArrowForward fontSize="small" />
                        </IconButton>
                      </Typography>
                    </Box>
                    <Box display="flex" justifyContent="space-between" alignItems="center">
                      <Box display="flex" alignItems="center">
                        <CustomAvatar
                          name={item.typeAssocie?.nom || '-'}
                          url={item.typeAssocie?.photo?.publicUrl as any}
                          style={{ width: 30, height: 30, marginRight: 5 }}
                        />
                        <Typography style={{ fontSize: 13 }}>{item.typeAssocie?.nom}</Typography>
                      </Box>
                      <Typography>
                        <span className={classes.spanStatusActionOp} style={statusUtils(item.statut.code)?.styles}>
                          {statusUtils(item.statut.code)?.libelle}
                        </span>
                      </Typography>
                    </Box>
                  </Box>
                </>
              );
            })}
          </Box>
        </Grid>
      )}
    </BottomScrollListener>
  );

  const page = (
    <>
      {openEditDialogSuivi && (
        <ActionOperationnelForm
          mode={modeForm}
          open={openEditDialogSuivi}
          suiviStatuts={loadingListStatutSuivi?.data?.pRTSuiviOperationnelStatuts?.nodes || []}
          setOpen={toggleOpenEditDialogSuivi}
          onRequestSave={handleSuiviSave}
          listTypeSuivi={loadingListTypeSuivi?.data?.pRTSuiviOperationnelTypes?.nodes || []}
          value={
            suiviToEdit
              ? {
                  id: suiviToEdit?.id,
                  idType: suiviToEdit?.type.id,
                  description: suiviToEdit?.description || '',
                  montant: suiviToEdit?.montant,
                  dateSuivi: suiviToEdit?.dateHeure,
                  fichier: suiviToEdit?.fichiers as any,
                  participants: suiviToEdit?.participants,
                  idTypeAssocie: suiviToEdit?.idTypeAssocie,
                  typeAssocie: suiviToEdit?.typeAssocie,
                }
              : undefined
          }
          saving={saving}
          saved={saved}
          setSaved={setSaved}
          externalFormData={externalFormData}
          setExternalFormData={setExternalFormData}
          onRequestImportance={handleChangeImportance}
          onRequestParticipant={handleParticipant}
          onRequestProjet={handleChangeProjectWhenEdit}
          responsables={responsables}
          setResponsables={setResponsables}
          partenaireAssocie={idLabo?.length && !loadingLabo.loading ? loadingLabo.data?.laboratoire : undefined}
          categorie={categorie}
          typeAction={typeAction}
        />
      )}
      <>
        <ConfirmDeleteDialog
          title="Suppression du suivi"
          content="Etes-vous sûr de vouloir supprimer ce suivi ?"
          open={openDeleteDialog}
          setOpen={setOpenDeleteDialog}
          onClickConfirm={handleConfirmDeleteOne}
        />
        <ConsultFichierModal
          open={showFichier}
          setOpen={setShowFichier}
          title={`Fichier-joint`}
          files={(suiviToEdit?.fichiers as any) || []}
        />
        <PaiementActionOperationnel
          open={openModalPaiement}
          setOpen={setOpenModalPaiement}
          isMobile={isMobile}
          onRequestSave={handleSavePaiement}
          suiviToEdit={suiviToEdit}
        />
        <DetailHistoriquePaiement
          open={openModalHistorique}
          setOpen={setOpenModalHistorique}
          isMobile={isMobile}
          suiviToEdit={suiviToEdit}
        />
        {showDetail ? (
          <DetailActionOperationnel
            suiviOperationnel={suiviToEdit}
            onCloturerClick={handleCloturerClick}
            onClose={() => setShowDetail(false)}
          />
        ) : (
          <>
            {isMobile && !(view === 'ajout-compte-rendu' || view === 'edit-compte-rendu') ? (
              <div className={classes.rootMobile}>
                <Box textAlign="center">
                  <NewCustomButton
                    variant="text"
                    theme="flatPrimary"
                    onClick={() => handleOpenAddLabo()}
                    startIcon={<Add />}
                    shadow={false}
                  >
                    NOUVEAU LITIGE
                  </NewCustomButton>
                </Box>
                {BottomScrollListenerPage}

                {/* <Fab className={classes.fab} onClick={handleOpenAdd} variant="round" color="secondary">
                  <Add />
                </Fab> */}
                {laboratoireSuivi?.error && <ErrorPage />}
              </div>
            ) : isMobile && view === 'creation' ? (
              <>
                <Box className={classes.buttonActionLaboPharmaMobile}>
                  {categorie === 'ACTION' ? (
                    <Box>
                      <NewCustomButton
                        variant="text"
                        theme="flatPrimary"
                        onClick={() => handleOpenAddLabo('LABO')}
                        startIcon={<Add />}
                        style={{ marginRight: 16 }}
                      >
                        NOUVELLE ACTION LABO
                      </NewCustomButton>
                      <NewCustomButton
                        variant="text"
                        theme="flatPrimary"
                        onClick={() => handleOpenAddLabo('PHARMACIE')}
                        startIcon={<Add />}
                        style={{ marginRight: 16 }}
                      >
                        NOUVELLE ACTION PHARMACIE
                      </NewCustomButton>
                    </Box>
                  ) : (
                    <NewCustomButton
                      variant="text"
                      theme="flatPrimary"
                      onClick={() => handleOpenAddLabo()}
                      startIcon={<Add />}
                      style={{ marginRight: 16 }}
                    >
                      NOUVEAU LITIGE
                    </NewCustomButton>
                  )}
                </Box>
                {BottomScrollListenerPage}
              </>
            ) : (
              // condition mobile not tab === ajout-compte-rendu || edit-compte-rendu
              <Box
                className={classNames({
                  [classes.padding]: !(view === 'creation'),
                })}
                width="100%"
              >
                <Table
                  error={laboratoireSuivi?.error || (loadingSuiviFromCompteRendu.error as any)}
                  loading={laboratoireSuivi?.loading || loadingSuiviFromCompteRendu.loading}
                  search={false}
                  data={
                    view === 'creation'
                      ? loadingSuiviFromCompteRendu.data?.getSuiviOperationnelForCompteRendu || []
                      : laboratoireSuivi?.data || []
                  }
                  selectable={params?.id && view === 'creation' ? true : false}
                  onClearSelection={params?.id && view === 'creation' ? handleClearSelection : undefined}
                  onSelectAll={params?.id && view === 'creation' ? handleSelectAll : undefined}
                  rowsSelected={params?.id && view === 'creation' ? (suiviSelected as any) : undefined}
                  onRowsSelectionChange={params?.id && view === 'creation' ? handleSelectChange : undefined}
                  columns={columns}
                  topBarComponent={topBarComponent}
                  rowsTotal={loadingSuiviOperationnel.data?.pRTSuiviOperationnels.totalCount || 0}
                  onRunSearch={handleNextDataTable}
                  onSortColumn={handleSortTable}
                  tableBodyClassName={classes.tableNoBoldSelect}
                  notablePagination={!!dataSuivi}
                />
                {view === 'creation' ? (
                  <Box display="flex" marginTop={3} marginBottom={3} justifyContent="flex-end" width={1}>
                    {categorie === 'ACTION' ? (
                      <Box>
                        <NewCustomButton
                          variant="text"
                          theme="flatPrimary"
                          onClick={() => handleOpenAddLabo('LABO')}
                          startIcon={<Add />}
                          style={{ marginRight: 16 }}
                        >
                          NOUVELLE ACTION LABO
                        </NewCustomButton>
                        <NewCustomButton
                          variant="text"
                          theme="flatPrimary"
                          onClick={() => handleOpenAddLabo('PHARMACIE')}
                          startIcon={<Add />}
                          style={{ marginRight: 16 }}
                        >
                          NOUVELLE ACTION PHARMACIE
                        </NewCustomButton>
                      </Box>
                    ) : (
                      <NewCustomButton
                        variant="text"
                        theme="flatPrimary"
                        onClick={() => handleOpenAddLabo()}
                        startIcon={<Add />}
                        style={{ marginRight: 16 }}
                      >
                        NOUVEAU LITIGE
                      </NewCustomButton>
                    )}
                  </Box>
                ) : (
                  ''
                )}
              </Box>
            )}
          </>
        )}
      </>
    </>
  );
  return page;
};

export default ActionOperationnel;
