import { useApplicationContext } from '@lib/common';
import { useGet_Prt_Reglement_ModeQuery } from '@lib/common/src/federation';
import { CustomDatePicker, CustomFormTextField, CustomModal, CustomSelect, MontantInputField } from '@lib/ui-kit';
import { Box, InputAdornment } from '@material-ui/core';
import React, { ChangeEvent, FC, useEffect, useState } from 'react';
import { InitialValuePaiementForm, PaiementActionOperationnelProps } from './interfaces';

const initialValue = {
  date: new Date(),
  montant: 0,
  idModePaiement: '',
};

export const PaiementActionOperationnel: FC<PaiementActionOperationnelProps> = ({
  open,
  setOpen,
  isMobile,
  onRequestSave,
  suiviToEdit,
}) => {
  const { federation } = useApplicationContext();

  const loadingModeReglement = useGet_Prt_Reglement_ModeQuery({ client: federation });

  const [value, setValue] = useState<InitialValuePaiementForm>(initialValue);

  const [saving, setSaving] = useState<boolean>(false);

  useEffect(() => {
    if (!open) {
      setValue(initialValue);
    }
    if (open && suiviToEdit) {
      setValue((prevState) => ({ ...prevState, montant: suiviToEdit.restePaiement }));
    }
  }, [open]);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;

    setValue((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSave = () => {
    setSaving(true);
    if (suiviToEdit) {
      console.log('---------------------SAVE PAIEMENT-------------------: ', {
        ...value,
        idSuiviOperationnel: suiviToEdit.id,
      });
      onRequestSave({
        ...value,
        montant: parseFloat(`${value.montant}`),
        idSuiviOperationnel: suiviToEdit.id,
      });
      setSaving(false);
      setOpen(false);
    }
  };

  const isValid = () => {
    return value.date && value.montant && value.idModePaiement && suiviToEdit;
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Paiement"
      withBtnsActions
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      disabledButton={!isValid() || saving}
      onClickConfirm={handleSave}
      fullScreen={!!isMobile}
    >
      <form className={''}>
        <Box display="flex" py={2}>
          <CustomDatePicker
            value={value.date}
            onChange={(date) => handleChange({ target: { name: 'date', value: date } } as any)}
            required
            label="Date de paiement"
            placeholder="00/00/0000"
          />
        </Box>
        <Box display="flex" py={2}>
          <MontantInputField
            label="Montant"
            onChangeValue={(montant) => handleChange({ target: { name: 'montant', value: montant } } as any)}
            value={value.montant}
          />
        </Box>
        <Box display="flex" py={2}>
          <CustomSelect
            list={loadingModeReglement.data?.pRTReglementModes.nodes || []}
            index="libelle"
            listId="id"
            label="Type"
            name="idModePaiement"
            value={value.idModePaiement}
            onChange={handleChange}
            required
            defaultValue={undefined}
          />
        </Box>
      </form>
    </CustomModal>
  );
};
