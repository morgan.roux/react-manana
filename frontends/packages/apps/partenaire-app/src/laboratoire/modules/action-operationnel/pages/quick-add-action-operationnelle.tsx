import React, { FC, useEffect, useState } from 'react';
import { ExternalFormDataSuiviOperationnel, SuiviFormRequestSavingProps } from './interfaces';
import ActionOperationnelForm from './ActionOperationnelForm';
import { useApplicationContext, useUploadFiles } from '@lib/common';
import {
  useCreate_Suivi_OperationnelMutation,
  useGet_LaboratoireLazyQuery,
  useGet_List_Suivi_Operationnel_StatutLazyQuery,
  useGet_List_Suivi_Operationnel_TypesLazyQuery,
  useSend_Email_Action_LitigeLazyQuery,
} from '@lib/common/src/federation';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import { useReactiveVar } from '@apollo/client';
import { openAddActionOperationnelle } from './reactive-vars';

const QuickAddActionOperationnelle: FC<{}> = () => {
  const { federation, graphql, notify, currentPharmacie } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();
  const { params } = useLaboratoireParams('litige');
  const { idLabo } = params;
  const openVar = useReactiveVar<boolean>(openAddActionOperationnelle);

  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [externalFormData, setExternalFormData] = useState<ExternalFormDataSuiviOperationnel | undefined>();
  const [responsables, setResponsables] = useState<any[]>([]);

  useEffect(() => {
    if (idLabo && idLabo[0]) {
      loadLabo({
        variables: {
          id: idLabo[0],
          idPharmacie: currentPharmacie.id,
        },
      });
    }
  }, [idLabo && idLabo[0]]);

  const [loadLabo, loadingLabo] = useGet_LaboratoireLazyQuery();

  const [loadListTypeSuivi, loadingListTypeSuivi] = useGet_List_Suivi_Operationnel_TypesLazyQuery({
    client: federation,
  });
  const [loadListStatutSuivi, loadingListStatutSuivi] = useGet_List_Suivi_Operationnel_StatutLazyQuery({
    client: federation,

    onCompleted: () => {},
  });

  const [createSuiviOperationnel, creatingSuiviOperationnel] = useCreate_Suivi_OperationnelMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Le Suivi Operationnel a été crée avec succés !',
        type: 'success',
      });
      setSaving(false);
    },
    onError: () => {
      notify({
        message: 'Erreurs lors de la création du Suivi Operationnel',
        type: 'error',
      });
      setSaving(false);
    },
  });

  const [sendEmailLitige, sendingEmailLitige] = useSend_Email_Action_LitigeLazyQuery({
    client: federation,
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'Email envoyé',
      });
    },
    onError: () => {
      notify({
        type: 'error',
        message: "Une erreur est survenue lors d'envoi d'email",
      });
    },
  });

  useEffect(() => {
    loadListTypeSuivi({
      variables: {
        filter: {
          categorie: {
            eq: 'LITIGE',
          },
        },
      },
    });
    loadListStatutSuivi();
  }, []);

  const toggleOpenEditDialogSuivi = () => {
    openAddActionOperationnelle(!openVar);
  };

  const handleChangeImportance = (importance: any) => {
    setExternalFormData((prev) => ({ ...prev, selectedImportance: importance }));
  };

  const handleChangeProjectWhenEdit = (idTache?: any, idFonction?: any) => {
    setExternalFormData((prev) => ({ ...prev, selectedTache: { idTache, idFonction } }));
  };

  const handleParticipant = (participants: any[], _id?: string) => {
    if (participants.length > 0) {
      setResponsables([]);
    }
    setExternalFormData((prev) => ({ ...prev, selectedParticipant: participants }));
  };

  const saveSuiviOperationnel = (suiviOperationnel: SuiviFormRequestSavingProps, fichiers: any) => {
    createSuiviOperationnel({
      variables: {
        input: {
          montant: parseFloat(suiviOperationnel.montant as any),
          idImportance: suiviOperationnel.importance?.id || '',
          idTache: suiviOperationnel.tache?.id as any,
          idFonction: suiviOperationnel.fontion?.id as any,
          idContacts: suiviOperationnel.idContacts,
          description: suiviOperationnel.description,
          dateHeure: suiviOperationnel.dateSuivi,
          partenaireType: 'LABORATOIRE',
          idTypeAssocie: suiviOperationnel.idTypeAssocie || '',
          idType: suiviOperationnel.idTypeSuivi,
          fichiers,
          idParticipants: (suiviOperationnel.idParticipants || [])
            .map((el) => {
              return el.id || undefined;
            })
            .filter((response) => {
              return response !== undefined;
            }),
        },
      },
    }).then((result) => {
      if (result.data?.createOnePRTSuiviOperationnel) {
        /*
       TODO : Condition
       sendEmailLitige({
          variables: {
            input: {
              id: result.data?.createOnePRTSuiviOperationnel.id,
              idType: result.data?.createOnePRTSuiviOperationnel.type.id,
            },
          },
        });*/
      }
    });
  };

  const handleSuiviSave = (suiviOperationnel: SuiviFormRequestSavingProps) => {
    setSaving(true);
    setSaved(false);
    if (suiviOperationnel.selectedFiles && suiviOperationnel.selectedFiles.length > 0) {
      uploadFiles(suiviOperationnel.selectedFiles, {
        directory: 'relation-fournisseurs/actions-operationnelles',
      })
        .then((uploadedFiles) => {
          saveSuiviOperationnel(suiviOperationnel, uploadedFiles);
        })
        .catch((error) => {
          setSaving(false);
          setSaved(false);

          notify({
            type: 'error',
            message: `Une erreur s'est produite lors de chargement des fichiers`,
          });
        });
    } else {
      saveSuiviOperationnel(suiviOperationnel, undefined);
    }
  };

  const page = (
    <ActionOperationnelForm
      mode="creation"
      open={openVar}
      suiviStatuts={loadingListStatutSuivi?.data?.pRTSuiviOperationnelStatuts?.nodes || []}
      setOpen={toggleOpenEditDialogSuivi}
      onRequestSave={handleSuiviSave}
      listTypeSuivi={loadingListTypeSuivi?.data?.pRTSuiviOperationnelTypes?.nodes || []}
      saving={saving}
      saved={saved}
      setSaved={setSaved}
      externalFormData={externalFormData}
      setExternalFormData={setExternalFormData}
      onRequestImportance={handleChangeImportance}
      onRequestParticipant={handleParticipant}
      onRequestProjet={handleChangeProjectWhenEdit}
      responsables={responsables}
      setResponsables={setResponsables}
      partenaireAssocie={loadingLabo.data?.laboratoire}
      categorie="LITIGE"
    />
  );
  return page;
};

export default QuickAddActionOperationnelle;
