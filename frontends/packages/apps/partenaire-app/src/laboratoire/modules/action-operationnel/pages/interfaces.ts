import { ReactNode } from 'react';
import { PRTSuiviOperationnelStatut, PRTSuiviOperationnelType, User } from '@lib/types';
import {
  PrtSuiviOperationnelInfoFragment,
  ImportanceInfoFragment,
  TacheInfoFragment,
  FonctionInfoFragment,
} from '@lib/common/src/federation';

export interface FilterAjoutCompteRendu {
  active: boolean;
  mode: 'MODIFIER' | 'AJOUT' | undefined;
  idCompteRendu?: string;
}

export interface FilterAjoutCompteRendu {
  active: boolean;
  mode: 'MODIFIER' | 'AJOUT' | undefined;
  idCompteRendu?: string;
}

export interface SuiviProps {
  setSelectParticipants?: (participants: User[], idSuiviOpperationnel?: string) => void;
  commonFiledFormComponnent?: ReactNode;
  idCompteRendu?: string;
  selectedItemsIds?: string[];
  onRequestSelected?: (suivis: PrtSuiviOperationnelInfoFragment[]) => void;
  activeItem: string;
  laboratoireSuivi?: {
    loading?: boolean;
    error?: boolean;
    data?: PrtSuiviOperationnelInfoFragment[];
  };
  rowsTotal: number;
  onRequestSave?: (suivi: SuiviFormRequestSavingProps) => void;
  onRequestDelete?: (suivi: PrtSuiviOperationnelInfoFragment) => void;
  onRequestSuiviToEdit?: (suivi?: PrtSuiviOperationnelInfoFragment) => void;
  onRequestSearch?: ({ skip, take, searchText, filter, filterAjoutCompteRendu, sortTable }: any) => void;
  onRequestChangeStatut?: (id: string, value: string) => void;
  onRequestFetchMore?: () => void;
  listTypeSuivi?: {
    error?: boolean;
    loading?: boolean;
    data: PRTSuiviOperationnelType[];
  };
  listStatutSuivi?: PRTSuiviOperationnelStatut[];
  saving?: boolean;
  saved?: boolean;
  setSaved?: (newSaved: boolean) => void;
  filterIcon: any;
  setSelectImportanceToClient?: (importance: any) => void;
  allSelectedFromcommonFieldsComponent?: {
    selectedParticipant?: any[];
    selectedTache?: any;
    selectedFonction?: any;
    selectedImportance?: any;
  };
  setTacheAndFonctionToClient?: (idTache?: string, idFontion?: string) => void;
  responsableComponnent?: ReactNode;
  responsableSelected?: any[];
  setResponsableSelected?: (responsable: any[]) => void;
  setSuiviOperationnelChoosen?: (suivi: PrtSuiviOperationnelInfoFragment) => void;
  suiviOperationnelSaved?: PrtSuiviOperationnelInfoFragment;
  commentaireComponnent?: ReactNode;
  onSideNavListClick?: (item: string) => void;
  onRequestGoBack?: () => void;
  actionMode?: 'labo' | 'pharma';
  setActionMode?: (action?: 'labo' | 'pharma') => void;
}

export interface ExternalFormDataSuiviOperationnel {
  selectedParticipant?: any[];
  selectedResponsable?: any[];
  selectedTache?: any;
  selectedFonction?: any;
  selectedImportance?: any;
}

export interface SuiviFormValue {
  id?: string;
  idType?: string;
  dateSuivi?: Date;
  description: string;
  fichier?: any[];
  montant?: number;
  titre?: string;
  idStatut?: string;
  actionMode?: string;
  participants?: any[];
  idTypeAssocie?: string;
  typeAssocie?: any;
}

export interface SuiviFormProps {
  mode: 'creation' | 'modification' | 'cloturer';
  actionMode?: 'labo' | 'pharma';
  setActionMode?: (action?: string) => void;
  open: boolean;
  saving?: boolean;
  saved?: boolean;
  setSaved?: (newSaved: boolean) => void;
  setOpen: (open: boolean) => void;
  onRequestSave?: (data: SuiviFormRequestSavingProps) => void;
  listTypeSuivi?: {
    id?: string;
    code?: string;
    libelle?: string;
  }[];
  value?: SuiviFormValue;
  suiviStatuts?: any[];
  externalFormData?: ExternalFormDataSuiviOperationnel;
  setExternalFormData?: (data: ExternalFormDataSuiviOperationnel | undefined) => void;
  onRequestImportance: (importance: any) => void;
  onRequestProjet: (projet: any) => void;
  onRequestParticipant: (participant: any) => void;
  responsables: any[];
  setResponsables: (responsables: any) => void;
  partenaireAssocie?: any;
  categorie?: string;
  typeAction?: string;
}

export interface SuiviFormRequestSavingProps {
  id?: string;
  idTypeSuivi: string;
  dateSuivi: Date;
  montant: number;
  importance: ImportanceInfoFragment;
  description: string;
  selectedFiles: any;
  idParticipants: any[];
  tache?: TacheInfoFragment;
  fontion?: FonctionInfoFragment;
  idContacts?: string[];
  actionMode?: 'labo' | 'pharma';
  idTypeAssocie?: string;
}

export interface DetailSuiviComponnentProps {
  suiviOperationnel?: PrtSuiviOperationnelInfoFragment;
  onCloturerClick?: (suivi: PrtSuiviOperationnelInfoFragment) => void;
  onClose?: () => void;
}

export interface DetailActionOperationnelValues {
  Type: string;
  Date: string;
  Montant: string;
  Statut: string;
}
