import React from 'react';
import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';
import loadable from '@loadable/component';

const ActionOperationnelPage = loadable(() => import('./pages/ActionOperationnel'), {
  fallback: <SmallLoading />,
});

const activationParameters = {
  groupement: '0305',
  pharmacie: '0708',
};

const actionOperationnelModule: ModuleDefinition = {
  routes: [
    {
      attachTo: 'MAIN',
      path: '/laboratoires/litige',
      component: ActionOperationnelPage,
      activationParameters,
    },
  ],
};

export default actionOperationnelModule;
