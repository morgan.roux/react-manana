import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      padding: 24,
    },
    informationContainer: {
      margin: '24px 0',
      padding: 8,
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'center',
      justifyContent: 'flex-start',
      background: theme.palette.grey[200],
      '& div': {
        marginRight: 40,
      },
    },
    value: {
      fontFamily: 'Roboto',
      fontSize: 15,
      fontWeight: 500 as any,
    },
    label: {
      fontFamily: 'Roboto',
      fontSize: 15,
    },
    descriptionContainer: {
      width: '100%',
      '& h3': {
        margin: '0 0 16px 0',
        fontSize: 16,
        fontFamily: 'Roboto',
        fontWeight: 500 as any,
      },
      paddingBottom: 24,
      marginBottom: 24,
      borderBottom: `2px solid ${theme.palette.grey[300]}`,
    },
    filesContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      marginTop: 32,
      '& div': {
        background: theme.palette.grey[200],
        padding: 8,
        marginRight: 32,
        '& a': {
          textDecoration: 'none',
          color: theme.palette.secondary.dark,
          cursor: 'pointer',
        },
      },
    },
  })
);
