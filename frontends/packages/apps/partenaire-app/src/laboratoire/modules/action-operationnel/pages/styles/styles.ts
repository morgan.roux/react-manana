import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: 'calc(100vh - 155px)',
      overflowY: 'auto',
      minWidth: 400,
    },
    rootMobile: {
      paddingTop: 0,
      padding: 16,
      width: '100%',
      fontFamily: 'Roboto',
      '& h3': {
        fontFamily: 'Roboto',
        fontWeight: 'normal',
        fontSize: 16,
        margin: '16px 0 24px 0',
      },
    },
    padding: {
      padding: '0px 24px 24px 24px',
    },
    minHeight: {
      height: 30,
    },
    reverse: {
      display: 'flex',
      flexDirection: 'column-reverse',
    },
    titre: {
      marginTop: 24,
      fontSize: 16,
      fontWeight: 500 as any,
      fontFamily: 'Roboto',
      marginBottom: -8,
    },
    add: {
      position: 'absolute',
      right: 24,
      bottom: 24,
      zIndex: 999,
    },
    caption: {
      fontFamily: 'Roboto',
      color: '#9E9E9E',
      fontWeight: 'normal',
      fontSize: 12,
    },
    iconButton: {
      padding: 0,
    },
    formWidth: {
      width: '100%',
      minWidth: '100%',
      [theme.breakpoints.up('sm')]: {
        minWidth: 500,
      },
    },
    formHeight: {
      [theme.breakpoints.down('md')]: {
        maxHeight: '100vh',
      },
    },
    scroll: {
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
    textTable: {
      fontFamily: 'Roboto',
      color: '#212121',
      fontWeight: 'normal',
      textAlign: 'left',
      fontSize: 14,
    },
    textTitle: {
      fontFamily: 'Roboto',
      color: '#424242',
      textAlign: 'left',
      fontSize: 14,
      marginBottom: 10,
    },
    textHead: {
      fontFamily: 'Roboto',
      fontSize: 16,
      marginTop: 16,
      marginBottom: 16,
    },
    titreComponent: {
      fontFamily: 'Roboto',
      color: '#424242',
      fontWeight: 'bold',
      textAlign: 'left',
      fontSize: 22,
    },
    topBarBox: {
      paddingBottom: 16,
    },
    selectPadding10: {
      '& .MuiSelect-root': {
        padding: 10,
      },
    },
    tableNoBoldSelect: {
      '& .MuiTableCell-root .MuiSelect-root': {
        fontWeight: 400,
      },
    },
    avatarSuiviMobile: {
      '& .MuiAvatar-root': {
        width: 25,
        height: 25,
        marginLeft: 15,
      },
    },
    gridSuiviMobile: {
      borderTop: '1px solid #ccc',
      '&:last-child': {
        borderBottom: '1px solid #ccc',
      },
    },
    buttonActionLaboPharmaMobile: {
      display: 'flex',
      marginTop: 24,
      marginBottom: 24,
      justifyContent: 'flex-end',
      width: '100%',
      [theme.breakpoints.down('xs')]: {
        display: 'block',
      },
    },
    commomFormMobile: {
      width: '100%',
      '&>div': {
        padding: 0,
      },
    },
    noMrginFieldset: {
      marginTop: 20,
      paddingBottom: 0,
      '& fieldset': {
        marginBottom: 0,
      },
    },
    infiniteScrollContainer: {
      height: 'calc(100vh - 370px)',
      width: '100%',
      overflowY: 'scroll',
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
    fab: {
      position: 'absolute',
      bottom: 90,
      right: 24,
      zIndex: 1000,
    },
    userInput: {
      marginTop: 15,
    },
    type: {
      paddingRight: 8,
      width: '100%',
    },
    typeText: {
      padding: 16,
      fontFamily: 'Roboto',
      fontSize: 16,
      fontWeight: 500,
    },
    dateIcons: {
      display: 'flex',
      color: '#afafaf',
      fontSize: 14,
      alignItems: 'center',
      marginRight: 5,
      '& svg': {
        fontSize: 18,
        marginRight: 5,
      },
    },
    cardActionOp: {
      background: '#f8f8f8',
      marginBottom: 10,
      padding: '5px 15px',
      position: 'relative',
    },
    leftBannerActionOp: {
      position: 'absolute',
      width: 4,
      height: '100%',
      left: 0,
      top: 0,
      borderRadius: 5,
    },
    spanStatusActionOp: {
      borderRadius: 5,
      fontSize: 12,
      padding: '2px 10px',
    },
    exportButton: {
      textDecoration: 'none',
      fontFamily: 'Roboto',
      fontSize: 16,
      color: '#212121',
      paddingLeft: 16,
      cursor: 'pointer',
    },
    btnDetails: {
      position: 'absolute',
      top: '50%',
      transform: 'translateY(-50%)',
      right: 0,
    },
  })
);
