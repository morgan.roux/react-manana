import { PrtSuiviOperationnelInfoFragment } from '@lib/common/src/federation';
import { Box, MenuItem, Select, Typography } from '@material-ui/core';
import React, { useState, FC, useEffect } from 'react';
import moment from 'moment';
import { Column } from '@lib/ui-kit';
import { useStyles } from './styles';
import { ColumnSuiviProps, SelectCompProps } from './interfaces';
import { strippedColumnStyle, strippedString } from '@lib/common';
import { InsertComment } from '@material-ui/icons';

const SelectComp: FC<SelectCompProps> = ({ value, id, onChange, statutList }) => {
  const [idCurrent, setIdCurent] = useState<string | undefined>();

  useEffect(() => {
    setIdCurent(id);
  }, [id]);

  const handleChange = (
    event: React.ChangeEvent<{
      name?: string | undefined;
      value: unknown;
    }>,
    __child: React.ReactNode
  ) => {
    event.stopPropagation();
    if (idCurrent) onChange(idCurrent, event.target.value as string);
  };

  return (
    <>
      <Select style={{ minWidth: 120 }} variant="standard" value={value} onChange={handleChange} displayEmpty>
        {statutList.map(({ id, libelle }) => (
          <MenuItem key={id} value={id}>
            {libelle}
          </MenuItem>
        ))}
      </Select>
    </>
  );
};

export const useColumn = (props: ColumnSuiviProps) => {
  const classes = useStyles();

  const columns: Column[] = [
    {
      name: 'idType',
      label: props.activeItem === 'creation' || props.activeItem === 'detail-action' ? 'Type' : 'Type litige',
      sortable: props.activeItem !== 'detail-action',
      renderer: (row: PrtSuiviOperationnelInfoFragment) => {
        return (
          <Box onClick={() => props?.handleRowDetail(row)}>
            <Typography className={classes.textTable}>{row?.type?.libelle || '-'}</Typography>
          </Box>
        );
      },
    },
    {
      name: 'description',
      label: 'Titre',
      sortable: true,
      renderer: (row: PrtSuiviOperationnelInfoFragment) => {
        return row.description ? strippedString(row.description) : '';
      },
      style: strippedColumnStyle,
    },

    {
      name: 'representant',
      label: 'Laboratoire',
      renderer: (row: PrtSuiviOperationnelInfoFragment) => {
        return (
          <Box display="flex" alignItems="center" onClick={() => props.handleRowDetail(row)}>
            {row.typeAssocie?.nom}
          </Box>
        );
      },
    },
    {
      name: 'updatedAt',
      label: 'Date',
      sortable: props.activeItem !== 'detail-action',
      renderer: (row: PrtSuiviOperationnelInfoFragment) => {
        return (
          <Box onClick={() => props.handleRowDetail(row)}>
            <Typography className={classes.textTable}>
              {row?.statut.code === 'CLOTURE'
                ? moment.utc(row?.updatedAt).format('DD/MM/YYYY HH:mm')
                : moment.utc(row?.createdAt).format('DD/MM/YYYY HH:mm')}
            </Typography>
          </Box>
        );
      },
    },

    // {
    //   name: 'participants',
    //   label: 'Pharmacie',
    //   renderer: (row: PrtSuiviOperationnelInfoFragment) => {
    //     return row.participants && row.participants.length > 0 ? (
    //       <Box display="flex" alignItems="center" onClick={() => props.handleRowDetail(row)}>
    //         <CustomAvatar
    //           name={((row.participants[0] as any).fullName as string).split(' ')[0]}
    //           url={row?.participants[0].photo?.publicUrl || '#'}
    //         />
    //         &nbsp;&nbsp;
    //         <span>{((row.participants[0] as any).fullName as string).split(' ')[0]}</span>
    //       </Box>
    //     ) : (
    //       // <CustomAvatarGroup max={4} sizes="30px" users={(row.participants || []) as any} />
    //       ''
    //     );
    //   },
    // },
    {
      name: 'montant',
      label: 'Montant',
      sortable: props.activeItem !== 'detail-action',
      renderer: (row: PrtSuiviOperationnelInfoFragment) => {
        return (
          <Box onClick={() => props.handleRowDetail(row)}>
            <Typography className={classes.textTable}>{(row.montant || 0).toFixed(2)} €</Typography>
          </Box>
        );
      },
    },
    {
      name: 'restePaiement',
      label: 'Reste à payer',
      sortable: props.activeItem !== 'detail-action',
      renderer: (row: PrtSuiviOperationnelInfoFragment) => {
        return (
          <Box onClick={() => props.handleRowHistoriquePaiement(row)}>
            <Typography className={classes.textTable}>{(row.restePaiement || 0).toFixed(2)} €</Typography>
          </Box>
        );
      },
    },
    {
      name: 'statut',
      label: 'Statut',
      renderer: (row: PrtSuiviOperationnelInfoFragment) => {
        return (
          <SelectComp
            id={row.id || ''}
            value={row.statut.id}
            statutList={props.listStatutSuivi || []}
            onChange={props.handleChangeStatut}
          />
        );
      },
    },
    {
      name: 'nombreCommentaires',
      label: 'Nombre commentaires',
      renderer: (row: PrtSuiviOperationnelInfoFragment) => {
        return (
          <Box onClick={() => props.handleRowDetail(row)} className={classes.commentBox}>
            <InsertComment />
            <Typography className={classes.textTable} style={{ paddingLeft: 16 }}>
              {row.nombreCommentaires}
            </Typography>
          </Box>
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: PrtSuiviOperationnelInfoFragment) => {
        return props.activeItem === 'ajout-compte-rendu' || props.activeItem === 'edit-compte-rendu'
          ? ''
          : props.action(row);
      },
    },
  ];

  return columns;
};
