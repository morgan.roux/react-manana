import React, { ChangeEvent, useState, FC, useEffect, useCallback, useMemo } from 'react';
import { CustomModal, CustomSelect, CustomDatePicker, CustomFormTextField, CustomEditorText } from '@lib/ui-kit';
import { Box, InputAdornment } from '@material-ui/core';
import { SuiviFormProps, SuiviFormRequestSavingProps, SuiviFormValue } from './interfaces';
import { useStyles } from './styles/styles';
import classNames from 'classnames';
import { CommonFieldsForm, Dropzone, PartenaireInput, useApplicationContext } from '@lib/common';
import UserInput from '@lib/common/src/components/UserInput';
import { useLaboratoireCorrespondants } from '../../../hooks/useLaboratoireCorrespondants';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';

const ActionOperationnelForm: FC<SuiviFormProps> = ({
  mode,
  open,
  setOpen,
  listTypeSuivi,
  onRequestSave,
  saved,
  setSaved,
  value,
  suiviStatuts,
  saving,
  actionMode,
  setActionMode,
  externalFormData,
  setExternalFormData,
  onRequestProjet,
  responsables,
  setResponsables,
  partenaireAssocie,
  categorie,
  typeAction,
}) => {
  const classes = useStyles();
  const { isMobile } = useApplicationContext();
  const {
    params: { view },
  } = useLaboratoireParams();

  const [selectedFiles, setSelectedFiles] = useState<any[]>([]);
  const [valueForm, setValueForm] = useState<SuiviFormValue | undefined>();
  const [openModalResponsable, setOpenModalResponsable] = useState<boolean>(false);
  const [openModalCollaborateur, setOpenModalCollaborateur] = useState<boolean>(false);
  const [participants, setParticipants] = useState<any[]>([]);
  const [importance, setImportance] = useState<any>();
  const [partenaire, setPartenaire] = useState<any>();

  const [loadCorrespondants, responsablesCorrespondants] = useLaboratoireCorrespondants('LITIGE_CORRESPONDANT');

  useEffect(() => {
    if (open) {
      setExternalFormData &&
        setExternalFormData({
          selectedParticipant: (externalFormData?.selectedParticipant || []).map((item) => item.id),
          selectedTache: externalFormData?.selectedTache?.idTache,
          selectedFonction: externalFormData?.selectedTache?.idFonction,
          selectedImportance: externalFormData?.selectedImportance?.id,
        });
    }
  }, [open]);

  useEffect(() => {
    if (value && (mode === 'modification' || mode === 'cloturer')) {
      console.log('-----------value modification---: ', value);
      setValueForm(value);
      setSelectedFiles(value.fichier || []);
      setActionMode && setActionMode(value?.actionMode);
      setParticipants(value.participants || []);
      setPartenaire(value.typeAssocie);
      // setResponsables((value.co || []).map((item) => item.id));
    } else if (mode === 'creation') {
      setValueForm(undefined);
      setSelectedFiles([]);
      if (partenaireAssocie) {
        setPartenaire(partenaireAssocie);
      }
    }
  }, [value, mode, partenaireAssocie]);

  useEffect(() => {
    if (partenaire?.id && mode === 'creation') {
      loadCorrespondants(partenaire.id);
    }
  }, [mode, partenaire?.id]);

  useEffect(() => {
    if (mode === 'creation') {
      setResponsables(responsablesCorrespondants);
    }
  }, [JSON.stringify(responsablesCorrespondants), mode]);

  useEffect(() => {
    if ((open && mode === 'creation') || (!open && mode === 'modification')) {
      setValueForm(undefined);
      setSelectedFiles([]);
      setParticipants([]);
    }
  }, [open]);

  useEffect(() => {
    if (saved) {
      setOpen && setOpen(false);
      if (setSaved) setSaved(false);
    }
  }, [saved]);

  const handleSave = useCallback(() => {
    const data: SuiviFormRequestSavingProps = {
      id: valueForm?.id,
      montant: valueForm?.montant || 0,
      importance: externalFormData?.selectedImportance || '',
      tache: externalFormData?.selectedTache,
      fontion: externalFormData?.selectedFonction,
      idTypeSuivi: valueForm?.idType || '',
      dateSuivi: valueForm?.dateSuivi || new Date(),
      description: valueForm?.description || '',
      selectedFiles,
      idParticipants: participants || [],
      idContacts: (responsables || []).map((item) => item.id),
      actionMode,
      idTypeAssocie: partenaire.id,
    };
    console.log('--------------------data action---------: ', responsables);
    if (mode === 'cloturer') {
      onRequestSave &&
        onRequestSave({
          ...data,
          idStatut: (suiviStatuts || []).find((item) => item.code === 'CLOTURE')?.id,
        } as any);
    } else {
      onRequestSave && onRequestSave(data);
    }

    setSaved && setSaved(true);
  }, [selectedFiles, valueForm, externalFormData, participants, responsables, suiviStatuts]);

  const handleChangeForm = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      setValueForm({ ...valueForm, [e.target.name]: e.target.value } as any);
    },
    [valueForm]
  );

  const handleChangeType = useCallback(
    (e: any) => {
      setValueForm({ ...valueForm, idType: e.target.value } as any);
    },
    [valueForm]
  );

  const handleChangeDate = useCallback(
    (date: Date | null, __value: any) => {
      setValueForm({ ...valueForm, dateSuivi: date || new Date() } as any);
    },
    [valueForm]
  );

  const handleChangeDescription = useCallback(
    (value: string) => {
      setValueForm({ ...valueForm, description: value });
    },
    [valueForm]
  );

  const handleChangeImportance = (importance: any) => {
    setImportance(importance);
    setExternalFormData && setExternalFormData({ ...externalFormData, selectedImportance: importance });
  };

  const handleChangeParticipant = (value: any[]) => {
    setParticipants(value);
  };

  const handleChangeLaboratoire = (laboratoire: any) => {
    setPartenaire(laboratoire);
  };

  const isValid = useMemo(() => {
    const validation = valueForm && valueForm.description && valueForm.idType;
    if (mode === 'cloturer') {
      return validation && valueForm?.description && valueForm.description !== '' && partenaire.id;
    }
    return validation;
  }, [valueForm, externalFormData, responsables, mode]);

  const titleForm = () => {
    const title =
      // actionMode === 'labo'
      //   ? mode === 'modification'
      //     ? "Modification d'une action Labo"
      //     : 'Nouvelle action Labo'
      // : actionMode === 'pharma'
      // ? mode === 'modification'
      //   ? "Modification d'une action pharma"
      //   : 'Nouvelle action Pharmacie'
      mode === 'modification'
        ? `Modification d'${categorie === 'ACTION' ? 'une action' : 'un litige'}`
        : mode === 'cloturer'
        ? 'Clôturer'
        : categorie === 'ACTION'
        ? 'Nouvelle Action'
        : 'Nouveau litige';
    return title;
  };

  console.log('------------------participants: ', participants, 'responsables:', responsables);

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={titleForm()}
      withBtnsActions
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      disabledButton={!isValid || saving}
      onClickConfirm={handleSave}
      fullScreen={!!isMobile}
    >
      <form className={classes.formWidth}>
        {mode !== 'cloturer' && (
          <>
            <Box display="flex">
              <Box className={classes.type}>
                <CustomSelect
                  list={listTypeSuivi}
                  index="libelle"
                  listId="id"
                  label="Type"
                  value={valueForm?.idType}
                  onChange={handleChangeType}
                  required
                  defaultValue={undefined}
                  className={classes.selectPadding10}
                />
              </Box>
              <CustomFormTextField
                label="Montant"
                type="number"
                onChange={handleChangeForm}
                value={valueForm?.montant}
                required
                name="montant"
                InputProps={{
                  endAdornment: <InputAdornment position="start">€</InputAdornment>,
                }}
              />
            </Box>
            <Box display="flex" alignItems="center">
              <Box className={classes.commomFormMobile}>
                <CommonFieldsForm
                  selectedUsers={null}
                  urgence={null}
                  urgenceProps={{
                    useCode: false,
                  }}
                  noMarginTop
                  usersModalProps={{
                    withNotAssigned: false,
                    searchPlaceholder: 'Rechercher...',
                    withAssignTeam: false,
                    singleSelect: false,
                  }}
                  projet={externalFormData?.selectedTache?.idFonction || externalFormData?.selectedTache?.idTache}
                  importance={importance}
                  onChangeUsersSelection={handleChangeParticipant}
                  onChangeProjet={actionMode === 'pharma' ? onRequestProjet : undefined}
                  onChangeImportance={handleChangeImportance}
                  radioImportance={false}
                  labelImportance="Importance"
                  hideUserInput
                />
              </Box>
              <Box width="100%">
                <CustomDatePicker
                  value={valueForm?.dateSuivi}
                  onChange={handleChangeDate}
                  required
                  label="Date échéance"
                  placeholder="00/00/0000"
                />
              </Box>
            </Box>

            <Box>
              <PartenaireInput
                key="laboratoire"
                index="laboratoire"
                label="Laboratoire"
                value={partenaire}
                onChange={handleChangeLaboratoire}
                disabled={partenaire && view === 'creation'}
              />
            </Box>
            {typeAction === 'PHARMACIE' ? (
              <Box py={1} className={classes.noMrginFieldset}>
                <h3 className={''} style={{ marginTop: 0 }}>
                  Fonction ou collègue(s) concerné(s)
                </h3>
                <UserInput
                  className={classes.userInput}
                  openModal={openModalCollaborateur}
                  withNotAssigned={false}
                  setOpenModal={setOpenModalCollaborateur}
                  selected={participants}
                  setSelected={(value) => {
                    setParticipants(value);
                    setExternalFormData && setExternalFormData({ ...externalFormData, selectedResponsable: undefined });
                  }}
                  label="Collaborateurs"
                  defaultValueMe={true}
                  disabled={!partenaire?.id}
                />
              </Box>
            ) : (
              <Box py={1} className={classes.noMrginFieldset}>
                <UserInput
                  className={classes.userInput}
                  openModal={openModalResponsable}
                  idPartenaireTypeAssocie={partenaire?.id}
                  withAssignTeam={false}
                  withNotAssigned={false}
                  partenaireType="LABORATOIRE"
                  category="CONTACT"
                  setOpenModal={setOpenModalResponsable}
                  selected={responsables}
                  setSelected={(value) => {
                    setResponsables(value);
                    setExternalFormData && setExternalFormData({ ...externalFormData, selectedParticipant: undefined });
                  }}
                  label="Responsables"
                  title="Responsables"
                  disabled={!partenaire?.id}
                />
              </Box>
            )}
          </>
        )}
        <Box className={classNames({ [classes.reverse]: mode === 'cloturer' })} py={1}>
          <Box py={1}>
            <CustomEditorText
              value={valueForm?.description || ''}
              placeholder="Description Détaillée (*)"
              onChange={handleChangeDescription}
            />
          </Box>
          <Box py={2}>
            <Dropzone
              multiple
              setSelectedFiles={setSelectedFiles}
              selectedFiles={selectedFiles}
              contentText="Déposer vos documents"
            />
          </Box>
        </Box>
      </form>
    </CustomModal>
  );
};

export default ActionOperationnelForm;
