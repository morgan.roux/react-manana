import { ReactNode } from 'react';
import { PrtSuiviOperationnelInfoFragment, PrtSuiviOperationnelStatutInfoFragment } from '@lib/common/src/federation';

export interface ItemFilter {
  id: string;
  libelle: string;
  code: string;
  keyword: string;
}

export interface IFilter {
  titre: string;
  itemFilters: ItemFilter[];
}

export interface FilterProps {
  onChange: (data: ItemFilter[]) => void;
  filters: IFilter[];
  filterIcon: any;
  withNotText?: boolean;
  btns?: ReactNode[];
  hideText?: boolean;
  filterText?: string;
}

export interface ColumnSuiviProps {
  action: (suivi: PrtSuiviOperationnelInfoFragment) => void;
  handleChangeStatut: (id: string, value: string) => void;
  listStatutSuivi?: PrtSuiviOperationnelStatutInfoFragment[];
  activeItem: string;
  handleRowDetail: (row: PrtSuiviOperationnelInfoFragment) => void;
  handleRowHistoriquePaiement: (row: PrtSuiviOperationnelInfoFragment) => void;
}

export interface SelectCompProps {
  id: string;
  value: string;
  statutList: PrtSuiviOperationnelStatutInfoFragment[];
  onChange: (id: string, value: string) => void;
}

export interface InitialValuePaiementForm {
  date: Date;
  montant: number;
  idModePaiement: string;
}

export interface PaiementFormRequestSaving {
  date: Date;
  montant: number;
  idModePaiement: string;
  idSuiviOperationnel: string;
}

export interface PaiementActionOperationnelProps {
  open: boolean;
  setOpen: (open: boolean) => void;
  isMobile: boolean;
  onRequestSave: (data: PaiementFormRequestSaving) => void;
  suiviToEdit?: PrtSuiviOperationnelInfoFragment;
}

export interface DetailHistoriquePaiementProps {
  open: boolean;
  setOpen: (open: boolean) => void;
  isMobile: boolean;
  suiviToEdit?: PrtSuiviOperationnelInfoFragment;
}
