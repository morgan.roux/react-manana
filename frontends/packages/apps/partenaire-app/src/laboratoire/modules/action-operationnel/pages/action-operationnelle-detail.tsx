import React, { FC } from 'react';
import { SmallLoading } from '@lib/ui-kit';
import { DetailActionOperationnel } from './DetailActionOperationnel';
import { useApplicationContext } from '@lib/common';
// import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import { useGet_Suivi_OperationnelQuery } from '@lib/common/src/federation';
import { PrtSuiviOperationnelInfoFragment } from '@lib/common/src/federation';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';

const ActionOperationnel: FC<{}> = () => {
  const { federation } = useApplicationContext();
  const { params } = useLaboratoireParams('litige');

  const { loading, data } = useGet_Suivi_OperationnelQuery({
    client: federation,
    variables: {
      id: params.id || '',
    },
  });

  const handleCloturerClick = (suivi: PrtSuiviOperationnelInfoFragment) => {
    //setSuiviToEdit(suivi);
  };

  if (loading || !data?.pRTSuiviOperationnel) {
    return <SmallLoading />;
  }

  return (
    <DetailActionOperationnel
      suiviOperationnel={data?.pRTSuiviOperationnel as any}
      onCloturerClick={handleCloturerClick}
      onClose={() => {}}
    />
  );
};

export default ActionOperationnel;
