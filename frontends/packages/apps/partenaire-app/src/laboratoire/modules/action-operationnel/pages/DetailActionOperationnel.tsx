import {
  PrtSuiviOperationnelInfoFragment,
  useChange_Suivi_Opertionnel_StatutMutation,
  useGet_List_Suivi_Operationnel_StatutQuery,
} from '@lib/common/src/federation';
import React, { FC, memo, useCallback, useEffect, useState } from 'react';
import { useStyles } from './styles/detailStyles';
import { ErrorPage, Loader, NewCustomButton } from '@lib/ui-kit';
import { Close } from '@material-ui/icons';
import moment from 'moment';
import { DetailActionOperationnelValues, DetailSuiviComponnentProps } from './interfaces';
import { CommentList, UserAction, Comment, useApplicationContext } from '@lib/common';
import { useCommentsLazyQuery, useSmyleysLazyQuery, useUser_SmyleysLazyQuery } from '@lib/common/src/graphql';
import { ClosableTitle } from '../../../components/ClosableTitle/ClosableTitle';
import { useHistory } from 'react-router';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';

const DetailActionOperationnelComponent: FC<DetailSuiviComponnentProps> = ({ suiviOperationnel, onClose }) => {
  const {
    params: { idLabo },
    redirectTo,
    goBack,
  } = useLaboratoireParams();

  const classes = useStyles();
  const { graphql, currentGroupement, notify } = useApplicationContext();
  const [values, setValues] = useState<DetailActionOperationnelValues | undefined>();
  const [suivi, setSuivi] = useState<PrtSuiviOperationnelInfoFragment | undefined>();

  const [loadCommentaires, loadingCommentaire] = useCommentsLazyQuery({ client: graphql });
  const [loadUserSmyleys, loadingUserSmyleys] = useUser_SmyleysLazyQuery({ client: graphql });
  const [loadSmyleys, loadingSmyleys] = useSmyleysLazyQuery({
    client: graphql,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  const statutCloturer = useGet_List_Suivi_Operationnel_StatutQuery().data?.pRTSuiviOperationnelStatuts?.nodes?.filter(
    (statut) => statut.code === 'CLOTURE'
  );

  useEffect(() => {
    if (suiviOperationnel) {
      loadCommentaires({
        variables: { codeItem: 'SUIVI_OPERATIONNEL', idItemAssocie: suiviOperationnel.id || '' },
      });
      loadUserSmyleys({
        variables: {
          codeItem: 'SUIVI_OPERATIONNEL',
          idSource: suiviOperationnel.id || '',
        },
      });
      loadSmyleys({
        variables: {
          idGroupement: currentGroupement.id,
        },
      });
      setSuivi(suiviOperationnel);
      setValues({
        Type: suiviOperationnel.type.libelle,
        Date: moment(suiviOperationnel.dateHeure).format('DD/MM/YYYY'),
        Montant: `${suiviOperationnel.montant}`,
        Statut: suiviOperationnel.statut.libelle,
      });
    }
  }, [suiviOperationnel]);

  const [changeSuiviStatut] = useChange_Suivi_Opertionnel_StatutMutation({
    onCompleted: () => {
      notify({
        message: 'Le litige a été cloturer avec succès !',
        type: 'success',
      });
      redirectTo(undefined, 'litige');
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant le changement du statut',
        type: 'error',
      });
    },
  });

  const handleClotureClick = useCallback(() => {
    if (suivi && statutCloturer?.length) {
      changeSuiviStatut({
        variables: {
          id: suivi.id,
          idStatut: statutCloturer[0].id,
        },
      });
    }
  }, [suivi]);

  const handleRefecthComments = useCallback(() => {
    loadingCommentaire?.refetch && loadingCommentaire.refetch();
  }, [loadingCommentaire]);

  const handleRefecthSmyles = () => () => {
    loadingUserSmyleys?.refetch && loadingUserSmyleys.refetch();
  };

  const handleFecthMoreComments = () => {};

  const handleGoBack = () => {
    //history.push(`/laboratoires/litige${idLabo?.length ? `?idLabo=${idLabo[0]}` : ``}`);
    goBack('litige');
  };

  return (
    <div className={classes.root}>
      <ClosableTitle title="Détail action opérationnelle" onClose={handleGoBack} />
      <NewCustomButton
        disabled={suivi && suivi.statut && suivi.statut.code === 'CLOTURE'}
        onClick={handleClotureClick}
        startIcon={<Close />}
      >
        CLOTURER
      </NewCustomButton>
      <div className={classes.informationContainer}>
        {values &&
          Object.entries(values).map(([label, value]) => (
            <div key={label}>
              <span className={classes.label}>{label}</span>:&nbsp;&nbsp;
              <span className={classes.value}>{value}</span>
            </div>
          ))}
      </div>
      <div className={classes.descriptionContainer}>
        <h3>Description</h3>
        <div dangerouslySetInnerHTML={{ __html: suiviOperationnel?.description || '' }} />
        <div className={classes.filesContainer}>
          {suiviOperationnel &&
            (suiviOperationnel.fichiers || []).map((file) => (
              <div key={file.id}>
                <a href={file.publicUrl || '#'}>{file.nomOriginal}</a>
              </div>
            ))}
        </div>
      </div>
      <div>
        <>
          <UserAction
            refetchSmyleys={handleRefecthSmyles}
            codeItem="SUIVI_OPERATIONNEL"
            idSource={suiviOperationnel?.id || ''}
            nbComment={suiviOperationnel?.nombreCommentaires || 0}
            nbSmyley={suiviOperationnel?.nombreReaction || 0}
            userSmyleys={loadingUserSmyleys.data?.userSmyleys as any}
            smyleys={loadingSmyleys && loadingSmyleys.data && loadingSmyleys.data.smyleys}
            hidePartage
            hideRecommandation
          />
          {loadingCommentaire.loading ? (
            <Loader />
          ) : loadingCommentaire.error ? (
            <ErrorPage />
          ) : (
            <CommentList
              key={`comment_list`}
              comments={loadingCommentaire.data?.comments?.data || []}
              fetchMoreComments={handleFecthMoreComments}
            />
          )}
          <Comment
            refetch={handleRefecthComments}
            codeItem="SUIVI_OPERATIONNEL"
            idSource={suiviOperationnel?.id || ''}
          />
        </>
      </div>
    </div>
  );
};

export const DetailActionOperationnel = memo(DetailActionOperationnelComponent);
