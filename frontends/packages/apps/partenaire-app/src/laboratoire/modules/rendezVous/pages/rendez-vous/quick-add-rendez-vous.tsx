import React, { FC } from 'react';
import { useRendezVous } from '../../hooks/useRendezVous';
import RendezVousForm from '../rendez-vous-form';

import { useReactiveVar } from '@apollo/client';
import { openAddRendezVous } from './../reactive-vars';

const QuickAddRendezVous: FC<{}> = ({}) => {
  const openVar = useReactiveVar<boolean>(openAddRendezVous);

  const { handleRequestSaveRdv, setSavedCreateRdv, savedCreateRdv, savingCreateRdv, rendezVousEdit, laboratoire } =
    useRendezVous();

  return (
    <RendezVousForm
      open={openVar}
      setOpen={openAddRendezVous}
      onRequestSave={handleRequestSaveRdv}
      saved={savedCreateRdv}
      saving={savingCreateRdv}
      setSaved={setSavedCreateRdv}
      rendezVousEdit={rendezVousEdit}
      mode="creation"
      laboratoire={laboratoire}
      isRedirect={false}
    />
  );
};

export default QuickAddRendezVous;
