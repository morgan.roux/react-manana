import React, { Fragment } from 'react';
import { Menu, MenuItem, Typography, IconButton, Avatar, Box } from '@material-ui/core';
import { Delete, Edit, MoreHoriz, Schedule, Cancel, Person } from '@material-ui/icons';
import { PRTRendezVous } from '@lib/types';
import { formatDateRdv } from './../pages/rendez-vous/RendezVous';
import { useStyles } from './../pages/rendez-vous/styles';
import { Column } from '@lib/ui-kit';

export const useColumn = (
  anchorEl: HTMLElement | null,
  onClose: () => void,
  onDeleteClick: () => void,
  onEditClick: () => void,
  onCancelClick: () => void,
  onReportClick: () => void,
  onOpenAction: (event: React.MouseEvent<HTMLElement, MouseEvent>, itemChoosen: PRTRendezVous) => void,
  statutRdv?: string | undefined
) => {
  const styles = useStyles();
  const handleOpenAction = (itemChoosen: PRTRendezVous, e: React.MouseEvent<HTMLElement, MouseEvent>) => {
    onOpenAction(e, itemChoosen);
  };

  const tableColumn: Column[] = [
    {
      name: 'dateHeure',
      label: 'Date Rendez-Vous',
      renderer: (row: PRTRendezVous) => {
        return <Typography className={styles.tableText}>{formatDateRdv(row)}</Typography>;
      },
    },
    {
      name: 'invites',
      label: 'Représentant Labo',
      renderer: (row: PRTRendezVous) => {
        return row.invites && row.invites.length > 0 ? (
          <Box display="flex" flexDirection="row" alignItems="center">
            <Avatar
              alt={row.invites[0].nom}
              src={row.invites[0].photo?.avatar?.fichier?.publicUrl || row.invites[0].photo?.publicUrl}
            >
              <Person />
            </Avatar>
            &nbsp;&nbsp;
            <Typography className={styles.tableText}>
              {row.invites[0].nom}&nbsp;{row.invites[0].prenom}
            </Typography>
          </Box>
        ) : (
          '-'
        );
      },
    },
    {
      name: 'fonction',
      label: 'Fonctions',
      renderer: (row: PRTRendezVous) => {
        return row.invites && row.invites.length > 0 ? (
          <Typography className={styles.tableText}>{row.invites[0].fonction?.libelle}</Typography>
        ) : (
          '-'
        );
      },
    },
    {
      name: 'note',
      label: 'Note',
      renderer: (row: PRTRendezVous) => {
        return row.note ? (
          <Typography className={styles.tableText}>
            <div dangerouslySetInnerHTML={{ __html: row.note }} />
          </Typography>
        ) : (
          '-'
        );
      },
    },
    // {
    //   name: 'ordreJour',
    //   label: 'Sujet de la visite',
    //   renderer: (row: PRTRendezVous) => {
    //     return row.ordreJour ? (
    //       <Typography className={styles.tableText}>{row.ordreJour}</Typography>
    //     ) : (
    //       '-'
    //     );
    //   },
    // },
    {
      name: 'collaborateur',
      label: 'Collaborateur',
      renderer: (row: PRTRendezVous) => {
        return row.participants && row.participants.length > 0 ? (
          <Box display="flex" flexDirection="row" alignItems="center">
            <Avatar
              alt={(row.participants[0] as any).fullName}
              src={row.participants[0]?.photo?.avatar?.fichier?.publicUrl || row.participants[0]?.photo?.publicUrl}
            >
              <Person />
            </Avatar>
            &nbsp;&nbsp;
            <Typography className={styles.tableText}>{(row.participants[0] as any).fullName}</Typography>
          </Box>
        ) : (
          '-'
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: PRTRendezVous) => {
        return (
          <div key={`row-${row.id}`}>
            <IconButton
              aria-controls="simple-menu"
              aria-haspopup="true"
              // eslint-disable-next-line react/jsx-no-bind
              onClick={handleOpenAction.bind(null, row)}
            >
              <MoreHoriz />
            </IconButton>
            <Menu id="simple-menu-rdv" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={onClose}>
              {statutRdv === 'planifie' && (
                <Fragment>
                  <MenuItem
                    onClick={(e) => {
                      e.stopPropagation();
                      onReportClick();
                    }}
                  >
                    <Schedule />
                    &nbsp;
                    <Typography variant="inherit">Reporter</Typography>
                  </MenuItem>

                  <MenuItem
                    onClick={(e) => {
                      e.stopPropagation();
                      onCancelClick();
                    }}
                  >
                    <Cancel />
                    &nbsp;
                    <Typography variant="inherit">Annuler</Typography>
                  </MenuItem>

                  <MenuItem
                    onClick={(e) => {
                      e.stopPropagation();
                      onEditClick();
                    }}
                  >
                    <Edit />
                    &nbsp;
                    <Typography variant="inherit">Modifier</Typography>
                  </MenuItem>
                </Fragment>
              )}

              <MenuItem
                onClick={(e) => {
                  e.stopPropagation();
                  onDeleteClick();
                }}
              >
                <Delete />
                &nbsp;
                <Typography variant="inherit">Supprimer</Typography>
              </MenuItem>
            </Menu>
          </div>
        );
      },
    },
  ];

  return tableColumn;
};
