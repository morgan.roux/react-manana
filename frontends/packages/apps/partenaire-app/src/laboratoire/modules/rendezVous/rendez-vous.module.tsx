import React from 'react';
// @ts-ignore
import loadable from '@loadable/component';
import { SmallLoading } from '@lib/ui-kit';
import { ModuleDefinition } from '@lib/common';

const RendezVousPage = loadable(() => import('./pages/rendez-vous/RendezVous'), {
  fallback: <SmallLoading />,
});

const activationParameters = {
  groupement: '0305',
  pharmacie: '0708',
};

const rendezVousModule: ModuleDefinition = {
  routes: [
    {
      attachTo: 'MAIN',
      path: ['/laboratoires/rendez-vous/:view?'],
      component: RendezVousPage,
      activationParameters,
    },
  ],
};

export default rendezVousModule;
