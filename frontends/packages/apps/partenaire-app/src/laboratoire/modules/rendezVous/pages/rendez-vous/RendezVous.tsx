import React, { FC, useEffect, useState, Fragment } from 'react';
import { useStyles } from './styles';
import { Delete, Edit, MoreHoriz, Schedule, Cancel } from '@material-ui/icons';
import moment from 'moment';
import { Menu, MenuItem, Typography, Box, List, ListItem, ListItemText } from '@material-ui/core';
import { PRTRendezVous, PRTContact } from '@lib/types';
import { ConfirmDeleteDialog, CustomAvatar, NewCustomButton, SchedulerParameter, TableChange } from '@lib/ui-kit';
import { useRendezVous } from '../../hooks/useRendezVous';
import RendezVousForm from '../rendez-vous-form';
import { useLaboratoireParams } from './../../../../hooks/useLaboratoireParams';
import { useApplicationContext } from '@lib/common';
import HistoryIcon from '@material-ui/icons/History';
export interface MenuRdvProps {
  onEditClick: () => void;
  onDeleteClick: () => void;
  onReportClick: () => void;
  onCancelClick: () => void;
  anchorEl: HTMLElement | null;
  onClose: () => void;
  statutRdv?: string;
}

export interface RendezVousProps {
  data?: PRTRendezVous[];
  error?: Error;
  loading?: boolean;
  saving?: boolean;
  saved?: boolean;
  statutRdv?: string;
  setSaved?: (newSaved: boolean) => void;
  setRdvEdit?: (data: PRTRendezVous) => void;
  rowsTotal?: number;
  onOpenFormRendezVous?: (open: boolean) => void;
  onModeFormRendezVous?: (mode: 'creation' | 'modification' | 'report') => void;
  onRequestSearch?: (change: TableChange) => void;
  onRequestDelete?: (data: PRTRendezVous) => void;
  onRequestCancel?: (data: PRTRendezVous) => void;
  onReportClick?: (data: PRTRendezVous) => void;
  onCancelClick?: (data: PRTRendezVous) => void;
  onRequestParticipantInviteEdit?: (participants: any, invites: any) => void;
}

interface ListCompProps {
  item: PRTRendezVous;
  onClick: (event: React.MouseEvent<HTMLSpanElement, MouseEvent>, item: PRTRendezVous) => void;
}

const MenuRdv: FC<MenuRdvProps> = ({
  anchorEl,
  onClose,
  onDeleteClick,
  onEditClick,
  onReportClick,
  onCancelClick,
  statutRdv,
}) => {
  return (
    <Menu id="simple-menu-rdv" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={onClose}>
      {statutRdv === 'planifie' && (
        <Fragment>
          <MenuItem
            onClick={(e) => {
              e.stopPropagation();
              onReportClick();
            }}
          >
            <Schedule />
            &nbsp;
            <Typography variant="inherit">Reporter</Typography>
          </MenuItem>

          <MenuItem
            onClick={(e) => {
              e.stopPropagation();
              onCancelClick();
            }}
          >
            <Cancel />
            &nbsp;
            <Typography variant="inherit">Annuler</Typography>
          </MenuItem>

          <MenuItem
            onClick={(e) => {
              e.stopPropagation();
              onEditClick();
            }}
          >
            <Edit />
            &nbsp;
            <Typography variant="inherit">Modifier</Typography>
          </MenuItem>
        </Fragment>
      )}
      <MenuItem
        onClick={(e) => {
          e.stopPropagation();
          onDeleteClick();
        }}
      >
        <Delete />
        &nbsp;
        <Typography variant="inherit">Supprimer</Typography>
      </MenuItem>
    </Menu>
  );
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const formatDateRdv = (item: PRTRendezVous, withHeure = true) => {
  const heure = withHeure ? ` à ${item.heureDebut}${item.heureFin ? ` jusqu'à ${item.heureFin}` : ''}` : '';
  return `${moment.utc(item.dateRendezVous).format('DD/MM/YYYY')} ${heure}`;
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const formatContact = (contact: PRTContact, withFonction = false) => {
  const fullName = `${contact.prenom} ${contact.nom}`;
  return withFonction && contact.fonction?.libelle ? `${fullName}(${contact.fonction.libelle})` : fullName;
};

const ListComp: FC<ListCompProps> = ({ item, onClick }) => {
  const styles = useStyles();

  const handleClick = (event: React.MouseEvent<HTMLSpanElement, MouseEvent>) => {
    onClick(event, item);
  };

  return (
    <>
      <Box display="flex" flexDirection="row" justifyContent="space-between" py={2}>
        <Box display="flex" flexDirection="column">
          <Typography className={styles.mobileOrdreJour}>{item.ordreJour}</Typography>
          {/* <List className={styles.listItem}>
            {item.invites.map((contact, index) => (
              <ListItem key={contact.id || `${index}`} className={styles.listItem}>
                <ListItemText className={styles.listItemText}>
                   {formatContact(contact, true)}
                </ListItemText>
              </ListItem>
            ))}
          </List>
          <CustomAvatarGroup max={3} users={(item.participants || []) as any} /> */}
          <List className={styles.listItem}>
            {item.invites.map((contact, index) =>
              index === 0 ? (
                <ListItem key={contact.id || `${index}`} className={styles.listItem}>
                  <ListItemText className={styles.listItemText}>
                    <div className={styles.listRendezVousMobile}>
                      <CustomAvatar url={contact.photo?.publicUrl} name={contact.nom} />
                      <span>{formatContact(contact, false)}</span>
                    </div>
                  </ListItemText>
                </ListItem>
              ) : (
                ''
              )
            )}
            {item.participants.map((participant, index) =>
              index === 0 ? (
                <ListItem key={participant.id || `${index}`} className={styles.listItem}>
                  <ListItemText className={styles.listItemText}>
                    <div className={styles.listRendezVousMobile}>
                      <CustomAvatar url={participant.photo?.publicUrl} name={participant.userName} />
                      <span>{(participant as any).fullName}</span>
                    </div>
                  </ListItemText>
                </ListItem>
              ) : (
                ''
              )
            )}
          </List>
          {/* modified can be restored */}
        </Box>
        <Box display="flex" flexDirection="column" textAlign="right">
          <Box onClick={handleClick}>
            <MoreHoriz />
          </Box>
          <Typography className={styles.mobileDateRendezVous}>{formatDateRdv(item, false)}</Typography>
        </Box>
      </Box>
    </>
  );
};

const RendezVous: FC<{}> = () => {
  const {
    redirectTo,
    params: { idLabo, view },
  } = useLaboratoireParams('plan');
  const { isMobile } = useApplicationContext();

  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(12);
  const [searchText, setSearchText] = useState<string>('');
  const [anchorEL, setAnchorEl] = useState<HTMLElement | null>(null);
  const [item, setItem] = useState<string>();
  const [typeConfirmDialog, setTypeConfirmDialog] = useState<string>('');
  const [openConfirmDialog, setOpenConfirmDialog] = useState<boolean>(false);
  const [modeRdv, setModeRdv] = useState<'creation' | 'modification' | 'report'>('creation');
  const [openFormRdv, setOpenFormRdv] = useState<boolean>(false);
  const [openDrawer, setOpenDrawer] = useState<boolean>(false);
  const {
    rendezVous,
    handleRdvSearch,
    handleRequestDeleteRdv,
    handleRequestCancelRdv,
    handleRequestSaveRdv,
    setSavedCreateRdv,
    savedCreateRdv,
    savingCreateRdv,
    rendezVousEdit,
    laboratoire,
    handleChangeFilter,
  } = useRendezVous();

  useEffect(() => {
    if (handleRdvSearch) handleRdvSearch({ skip, take, searchText });
  }, [skip, take, searchText]);

  useEffect(() => {
    if (view && view === 'ajout') {
      setOpenFormRdv(true);
    } else {
      setOpenFormRdv(false);
    }
  }, [view]);

  // // ///////////////////// Mobile ///////////////////////////
  // const handleNextInfiniteScroll = () => {
  //   setTake(skip + take);
  // };
  // // ///////////////////////////////////////////////////////

  // const onCloseMenu = () => {
  //   setAnchorEl(null);
  // };

  const handleConfirmDeleteRdv = () => {
    item && handleRequestDeleteRdv && handleRequestDeleteRdv(item);
    setOpenConfirmDialog(false);
  };

  const handleConfirmCancelRdv = () => {
    setOpenConfirmDialog(false);
    item && handleRequestCancelRdv && handleRequestCancelRdv(item);
  };

  // const handleClickOpenAction = (event: React.MouseEvent<HTMLElement, MouseEvent>, itemChoosen: PRTRendezVous) => {
  //   event.stopPropagation();
  //   setAnchorEl(event.currentTarget); // open Menu
  //   setItem(itemChoosen);
  // };

  const handleDeleteClick = (id: string) => {
    setTypeConfirmDialog('delete');
    setOpenConfirmDialog(true);
    setItem(id);
  };

  const handleEditClick = (id: string) => {
    setAnchorEl(null);
    setModeRdv && setModeRdv('modification');
    setOpenFormRdv && setOpenFormRdv(true);
    redirectTo({ idLabo, view: 'edit', id }, 'rendez-vous');
  };

  const handleOpenAdd = () => {
    setModeRdv('creation');
    redirectTo({ idLabo, view: 'ajout' }, 'rendez-vous');
    setOpenFormRdv(true);
  };

  const handleReportClick = () => {
    setAnchorEl(null); // close Menu
    // item && setRdvEdit && setRdvEdit(item);
    setModeRdv && setModeRdv('report');
    setOpenFormRdv && setOpenFormRdv(true);
    // onRequestParticipantInviteEdit && onRequestParticipantInviteEdit(item?.participants, item?.invites);
  };

  const handleCancelClick = () => {
    setTypeConfirmDialog('cancel');
    setOpenConfirmDialog(true);
    setAnchorEl(null); // close Menu
  };

  const handleSearchTable = (searchText: string) => {
    setSearchText(searchText);
  };

  // const tableColumn = useColumn(
  //   anchorEL,
  //   onCloseMenu,
  //   handleDeleteClick,
  //   handleEditClick,
  //   handleCancelClick,
  //   handleReportClick,
  //   handleClickOpenAction,
  //   undefined
  // );

  const data = (rendezVous.data || []).map((rdv: any) => {
    const invite = (rdv.invites || [])
      .map((invite: any) => {
        return `${invite.civilite} ${invite.nom}`;
      })
      .join('/ ');

    const heureDebut = rdv.heureDebut.split(':');
    const heureFin = rdv.heureFin ? rdv.heureFin.split(':') : undefined;
    const start = moment.utc(rdv.dateRendezVous).set({
      hour: parseInt(heureDebut[0], 10) || 0,
      minute: parseInt(heureDebut[1], 10),
    });
    const fin = heureFin
      ? moment
          .utc(rdv.dateRendezVous)
          .set({
            hour: parseInt(heureFin[0], 10) || 0,
            minute: parseInt(heureFin[1], 10),
          })
          .toDate()
      : undefined;
    return {
      TaskID: rdv.id,
      Title: `${heureDebut[0]}:${heureDebut[1]} ${rdv.partenaireTypeAssocie?.nom} / ${invite}`,
      Description: rdv.note,
      Start: start.toDate(),
      End: fin ? fin : start.add(30, 'minutes').toDate(),
      isAllDay: false,
      Couleur: '#63B8DD',
    };
  });

  // return <>Test</>;

  return (
    <>
      {isMobile && (
        <Box p={2} textAlign="center">
          <NewCustomButton startIcon={<HistoryIcon />} onClick={handleOpenAdd}>
            Nouveau Rendez-Vous
          </NewCustomButton>
        </Box>
      )}
      <SchedulerParameter
        setOpenDrawer={setOpenDrawer}
        data={data}
        onRequestReunion={handleOpenAdd}
        onRequestChangeFilter={handleChangeFilter}
        onRequestUpdate={handleEditClick}
        onRequestDelete={handleDeleteClick}
        withSearch
        onSearch={handleSearchTable}
      />
      {openFormRdv && (
        <RendezVousForm
          open={openFormRdv}
          setOpen={setOpenFormRdv}
          onRequestSave={handleRequestSaveRdv}
          saved={savedCreateRdv}
          saving={savingCreateRdv}
          setSaved={setSavedCreateRdv}
          rendezVousEdit={rendezVousEdit}
          mode={modeRdv}
          laboratoire={laboratoire}
          isRedirect={true}
        />
      )}
      <ConfirmDeleteDialog
        title={typeConfirmDialog === 'delete' ? 'Suppression du rendez-vous' : 'Annulation du rendez-vous'}
        content={
          typeConfirmDialog === 'delete' ? 'Voulez-vous supprimez ce rendez-vous' : 'Voulez-vous annuler ce rendez-vous'
        }
        open={openConfirmDialog}
        setOpen={setOpenConfirmDialog}
        confirmBtnLabel={typeConfirmDialog === 'delete' ? 'Supprimer' : 'Confirmer'}
        onClickConfirm={typeConfirmDialog === 'delete' ? handleConfirmDeleteRdv : handleConfirmCancelRdv}
      />
    </>
  );

  // return (
  //   <>
  //     {isMobile ? (
  //       <div className={styles.rootMobile}>
  //         <div id="infinity-rdv" className={styles.infiniteScrollContainer}>
  //           <InfiniteScroll
  //             dataLength={rendezVous.rowsTotal || 0}
  //             next={handleNextInfiniteScroll}
  //             hasMore
  //             loader={<br />}
  //             scrollableTarget="infinity-rdv"
  //           >
  //             {(rendezVous.data || []).map((item: any) => (
  //               <ListComp key={item.id} item={item} onClick={handleClickOpenAction} />
  //             ))}
  //           </InfiniteScroll>
  //         </div>

  //         {rendezVous.error && <ErrorPage />}
  //       </div>
  //     ) : (
  //       <div className={styles.rootWeb}>
  //         <div className={styles.tableContainer}>
  //           <Table
  //             loading={rendezVous.loading}
  //             columns={tableColumn}
  //             error={rendezVous.error}
  //             data={rendezVous.data || []}
  //             search={false}
  //             rowsTotal={rendezVous.rowsTotal}
  //             onRunSearch={handleSearchTable}
  //           />
  //         </div>
  //       </div>
  //     )}

  //     <MenuRdv
  //       anchorEl={anchorEL}
  //       onClose={onCloseMenu}
  //       onDeleteClick={handleDeleteClick}
  //       onEditClick={handleEditClick}
  //       onCancelClick={handleCancelClick}
  //       onReportClick={handleReportClick}
  //       //statutRdv={statutRdv}
  //     />
  //     <ConfirmDeleteDialog
  //       title={typeConfirmDialog === 'delete' ? 'Suppression du rendez-vous' : 'Annulation du rendez-vous'}
  //       content={
  //         typeConfirmDialog === 'delete' ? 'Voulez-vous supprimez ce rendez-vous' : 'Voulez-vous annuler ce rendez-vous'
  //       }
  //       open={openConfirmDialog}
  //       setOpen={setOpenConfirmDialog}
  //       confirmBtnLabel={typeConfirmDialog === 'delete' ? 'Supprimer' : 'Confirmer'}
  //       onClickConfirm={typeConfirmDialog === 'delete' ? handleConfirmDeleteRdv : handleConfirmCancelRdv}
  //     />
  //   </>
  // );
};

export default RendezVous;
