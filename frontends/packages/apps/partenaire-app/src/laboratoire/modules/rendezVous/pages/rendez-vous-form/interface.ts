import { Laboratoire, PrtRendezVousInfoFragment } from '@lib/common/src/federation';
import { PRTRendezVous } from '@lib/types';

export interface RendezVousFormProps {
  setOpen: (open: boolean) => void;
  open: boolean;
  onRequestSave?: (data: RendezVousFormOutput) => void;
  saving?: boolean;
  saved?: boolean;
  setMode?: (mode: 'creation' | 'modification' | 'report') => void;
  setSaved?: (newSaved: boolean) => void;
  selectedParticipants?: any[];
  selectedInvites?: any[];
  mode?: 'creation' | 'modification' | 'report';
  rendezVousEdit?: {
    data: PrtRendezVousInfoFragment;
    loading: any;
    error: any;
  };
  setSelectedParticipants?: (selecteds: any[]) => void;
  setSelectedInvites?: (selecteds: any[]) => void;
  laboratoire?: Laboratoire;
  isRedirect: boolean;
}

export interface RendezVousProps {
  date: any;
  debut: any;
  fin: any;
  note?: string;
  typeReunion: string;
  partenaireTypeAssocie: any;
}

export interface RendezVousRequestSavingProps {
  id?: string;
  dateRendezVous?: Date;
  heureDebut: string;
  heureFin?: string;
  subject: string;
  idInvites: string[];
  idUserParticipants: string[];
  statut: string;
  note?: string;
}

export interface RendezVousFormOutput {
  id?: string;
  // idSubject: string;
  dateRendezVous: Date;
  heureDebut: string;
  heureFin?: string;
  note?: string;
  idInvites: string[];
  idUserParticipants: string[];
  statut: string;
  typeReunion: string;
  idPartenaireTypeAssocie: string;
}
