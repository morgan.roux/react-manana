import { useReactiveVar } from '@apollo/client';
import { endTime, startTime, useApplicationContext } from '@lib/common';
import {
  PrtRendezVousFilter,
  useCreate_Rendez_VousMutation,
  useDelete_Rendez_VousMutation,
  useGet_LaboratoireLazyQuery,
  useGet_Rendez_VousLazyQuery,
  useUpdate_Rendez_VousMutation,
} from '@lib/common/src/federation';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import { RendezVousFormOutput } from '../pages/rendez-vous-form/interface';
import { searchTextVar } from '../../../reactive-vars';

export const useRendezVous = () => {
  const {
    params: { idLabo, id: idRdv },
  } = useLaboratoireParams('plan');
  // const searchText = useReactiveVar<string>(searchTextVar);

  const [savingCreateRdv, setSavingCreateRdv] = useState<boolean>(false);
  const [savedCreateRdv, setSavedCreateRdv] = useState<boolean>(false);
  const [skipRdv, setSkipRdv] = useState<number>(0);
  const [takeRdv, setTakeRdv] = useState<number>(2);
  const [filterDate, setFilterDate] = useState<any>(new Date());
  const [searchText, setSearchText] = useState<string>('');

  const { federation, notify, currentPharmacie } = useApplicationContext();

  const [loadLabo, loadingLabo] = useGet_LaboratoireLazyQuery();

  const [loadRdv, loadingRdv] = useGet_Rendez_VousLazyQuery({ client: federation });
  const [loadRdvEdit, loadingRdvEdit] = useGet_Rendez_VousLazyQuery({ client: federation });

  const [deleteRdv, deletingRdv] = useDelete_Rendez_VousMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Le rendez-vous a été supprimé avec succès !',
        type: 'success',
      });
      loadingRdv?.refetch && loadingRdv.refetch();
      loadingRdv?.refetch && loadingRdv.refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression du rendez-vous',
        type: 'error',
      });
    },
  });
  const [updateRdv, updatingRdv] = useUpdate_Rendez_VousMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Le rendez-vous a été modifié avec succès !',
        type: 'success',
      });
      loadingRdv?.refetch && loadingRdv.refetch();
      setSavingCreateRdv(false);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du rendez-vous',
        type: 'error',
      });
      setSavingCreateRdv(false);
    },
  });
  const [createRdv, creatingRdv] = useCreate_Rendez_VousMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Rendez-vous planifié.',
        type: 'success',
      });
      loadingRdv?.refetch && loadingRdv.refetch();
      setSavingCreateRdv(false);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la planification du rendez-vous',
        type: 'error',
      });
      setSavingCreateRdv(false);
    },
  });
  const idLaboExist = idLabo ? idLabo[0] : undefined;

  useEffect(() => {
    searchRdv();
  }, [idLaboExist, searchText, filterDate]);

  useEffect(() => {
    if (idRdv) {
      loadRdvEdit({
        variables: {
          filter: {
            and: [
              {
                id: {
                  eq: idRdv as any,
                },
              },
            ],
          },
          paging: {
            limit: 1,
          },
        },
      });
    }
  }, [idRdv]);

  const handleRdvSearch = ({ skip, take, searchText: search }: any) => {
    setSkipRdv(skip);
    setTakeRdv(take);
    setSearchText(search);
  };

  const searchRdv = () => {
    const filterLoadRdv: PrtRendezVousFilter = {
      and: [
        {
          idPharmacie: {
            eq: currentPharmacie.id,
          },
        },
        // {
        //   dateRendezVous: {
        //     between: {
        //       lower: startTime(moment.utc(filterDate).startOf('month')),
        //       upper: endTime(moment.utc(filterDate).endOf('month')),
        //     },
        //   },
        // },
        // {
        //   or: [
        //     {
        //       dateRendezVous: {
        //         lt: endTime(moment.utc(filterDate).format('YYYY-MM-DD')),
        //       },
        //     },
        //     {
        //       and: [
        //         {
        // dateRendezVous: {
        //   between: {
        //     lower: startTime(moment.utc(filterDate)),
        //     upper: endTime(moment.utc(filterDate)),
        //   },
        // },
        //           heureFin: {
        //             lt: moment.utc(filterDate).format('HH:mm'),
        //           },
        //         },
        //       ],
        //     },
        //   ],
        // },
      ],
    };

    if (idLabo && idLabo.length > 0) {
      (filterLoadRdv?.and || []).push({
        idPartenaireTypeAssocie: {
          eq: idLabo[0],
        },
      });
      loadLabo({
        variables: {
          id: idLabo[0],
          idPharmacie: currentPharmacie.id,
        },
      });
    }

    if (searchText) {
      (filterLoadRdv?.and || []).push({
        note: {
          iLike: `%${searchText}%`,
        },
      });
    }

    loadRdv({
      variables: {
        filter: filterLoadRdv,
      },
    });
  };

  const handleRequestSaveRdv = (data: RendezVousFormOutput) => {
    setSavingCreateRdv(true);
    setSavedCreateRdv(false);
    if (!data.id) {
      createRdv({
        variables: {
          input: {
            ...data,
            partenaireType: 'LABORATOIRE',
            idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
          },
        },
      });
    } else {
      const id = data.id;
      delete data['id'];
      updateRdv({
        variables: {
          id,
          input: {
            ...data,
            partenaireType: 'LABORATOIRE',
            idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
          },
        },
      });
    }
  };

  const handleRequestDeleteRdv = (id: any) => {
    deleteRdv({
      variables: {
        id: id,
      },
    });
  };

  const handleRequestCancelRdv = (data: any) => {
    updateRdv({
      variables: {
        id: data.id,
        input: {
          statut: 'ANNULE',
          partenaireType: 'LABORATOIRE',
          idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
          idInvites: data.invites.map(({ id }: any) => id),
          idUserParticipants: data.participants.map(({ id }: any) => id),
          idSubject: data.idSubject,
          dateRendezVous: data.dateRendezVous,
          heureDebut: data.heureDebut,
          heureFin: data.heureFin,
          typeReunion: data.typeReunion,
        },
      },
    });
  };

  const handleChangeFilter = (_searchText: string, date: any) => {
    setFilterDate(date);
  };

  const rendezVous = {
    data: loadingRdv.data?.pRTRendezVous.nodes as any,
    loading: loadingRdv.loading as any,
    error: loadingRdv.error as any,
    rowsTotal: loadingRdv.data?.pRTRendezVous.nodes.length || 0,
  };

  const rendezVousEdit = {
    data: loadingRdvEdit.data?.pRTRendezVous.nodes && (loadingRdvEdit.data?.pRTRendezVous.nodes[0] as any),
    loading: loadingRdvEdit.loading as any,
    error: loadingRdvEdit.error as any,
  };

  const laboratoire = loadingLabo.data?.laboratoire as any;

  return {
    rendezVous,
    handleRequestSaveRdv,
    handleRdvSearch,
    handleRequestDeleteRdv,
    handleRequestCancelRdv,
    savingCreateRdv,
    savedCreateRdv,
    setSavedCreateRdv,
    rendezVousEdit,
    laboratoire,
    handleChangeFilter,
  };
};
