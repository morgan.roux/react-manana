import React, { FC, useState, useEffect, Fragment, useCallback, ChangeEvent } from 'react';
import { RendezVousFormProps, RendezVousProps, RendezVousFormOutput } from './interface';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { Box, Grid } from '@material-ui/core';
import moment from 'moment';
import {
  CustomDatePicker,
  CustomEditorText,
  CustomFormTextField,
  CustomModal,
  CustomSelect,
  NewCustomButton,
} from '@lib/ui-kit';
import { PartenaireInput, useApplicationContext } from '@lib/common';
import UserInput from '@lib/common/src/components/UserInput';
import { useLaboratoireParams } from './../../../../hooks/useLaboratoireParams';
import { useLaboratoireCorrespondants } from '../../../../hooks/useLaboratoireCorrespondants';
import { computeHourPlus, typeReunions } from '../../../../util';

const initialValues = {
  date: new Date(),
  debut: null,
  fin: null,
  typeReunion: 'VISIO',
  partenaireTypeAssocie: {},
};

const RendezVousForm: FC<RendezVousFormProps> = ({
  open,
  setOpen,
  setMode,
  onRequestSave,
  saving,
  saved = false,
  setSaved,
  mode,
  rendezVousEdit,
  laboratoire,
  isRedirect = true,
}) => {
  const { isMobile } = useApplicationContext();
  const {
    redirectTo,
    params: { idLabo },
  } = useLaboratoireParams('plan');

  const [rdv, setRdv] = useState<RendezVousProps>({ ...initialValues, partenaireTypeAssocie: laboratoire });
  const [openModalCollaborateur, setOpenModalCollaborateur] = useState<boolean>(false);
  const [openModalInvites, setOpenModalInvites] = useState<boolean>(false);
  const [invites, setInvites] = useState<any>([]);
  const [participants, setParticipants] = useState<any>([]);

  const [loadCorrespondants, responsablesCorrespondants] = useLaboratoireCorrespondants('RENDEZVOUS_CORRESPONDANT');

  useEffect(() => {
    if (saved) {
      setRdv(initialValues);
      setOpen(false);
      if (setSaved) setSaved(false);
    }
  }, [saved]);

  useEffect(() => {
    if (rendezVousEdit?.data && !rendezVousEdit?.loading && (mode === 'modification' || mode === 'report')) {
      if (!open) {
        setOpen(true);
      }
      setRdv({
        date: rendezVousEdit.data.dateRendezVous,
        debut: rendezVousEdit.data.heureDebut, // moment(rendezVousEdit.heureDebut, 'HH:mm').toDate(),
        fin: rendezVousEdit.data.heureFin, // ? moment(rendezVousEdit.heureFin, 'HH:mm').toDate() : null,
        note: rendezVousEdit.data.note || '',
        typeReunion: rendezVousEdit.data.typeReunion,
        partenaireTypeAssocie: {
          id: rendezVousEdit.data.partenaireTypeAssocie?.id,
          nom: rendezVousEdit.data.partenaireTypeAssocie?.nom,
        },
      });
      setInvites && setInvites(rendezVousEdit.data.invites);
      setParticipants && setParticipants(rendezVousEdit.data.participants);
      // setIdSubject(rendezVousEdit.subject?.id);
    }
  }, [rendezVousEdit]);

  useEffect(() => {
    setSaved && setSaved(false);
    if (open && mode === 'creation') {
      setRdv({
        ...initialValues,
        partenaireTypeAssocie: laboratoire,
        typeReunion: laboratoire ? laboratoire.partenariat?.typeReunion || 'VISIO' : 'VISIO',
      });
      setParticipants([]);
      setInvites([]);
      // setIdSubject('none');
    }
  }, [open]);

  useEffect(() => {
    if (mode === 'creation' && rdv?.partenaireTypeAssocie?.id) {
      loadCorrespondants(rdv.partenaireTypeAssocie.id);
    }
  }, [mode, rdv?.partenaireTypeAssocie?.id]);

  useEffect(() => {
    if (mode === 'creation') {
      setInvites(responsablesCorrespondants);
    }
  }, [JSON.stringify(responsablesCorrespondants), mode]);

  const handleClose = () => {
    setOpen(false);
    setMode && setMode('creation');
    isRedirect && redirectTo({ idLabo, view: undefined, id: undefined }, 'rendez-vous');
  };

  const handleChangeDate = (date: MaterialUiPickersDate) => {
    setRdv({ ...rdv, date: date });
  };

  const handleChangeDebut = (e: ChangeEvent<HTMLInputElement>) => {
    const fin = computeHourPlus(e.target.value);
    setRdv({ ...rdv, debut: e.target.value, fin });
  };

  const handleChangeFin = (e: ChangeEvent<HTMLInputElement>) => {
    setRdv({ ...rdv, fin: e.target.value });
  };

  const handleChangeTypeReunion = (e: ChangeEvent<HTMLInputElement>) => {
    setRdv({ ...rdv, typeReunion: e.target.value });
  };

  const handleChangePartenaireTypeAssocie = (value: any) => {
    setRdv({ ...rdv, partenaireTypeAssocie: value });
  };

  const handleSubmit = () => {
    if (onRequestSave) {
      onRequestSave({
        id: mode === 'modification' || mode === 'report' ? rendezVousEdit?.data.id : undefined,
        dateRendezVous: rdv.date || new Date(),
        heureDebut: moment(rdv.debut, 'HH:mm').format('HH:mm'),
        heureFin: rdv.fin ? moment(rdv.fin, 'HH:mm').format('HH:mm') : undefined,
        statut: mode === 'report' ? 'REPORTE' : 'PLANIFIE',
        idInvites: (invites || []).map((item: any) => item.id),
        idUserParticipants: (participants || []).map((item: any) => item.id),
        note: rdv.note,
        typeReunion: rdv.typeReunion,
        idPartenaireTypeAssocie: rdv.partenaireTypeAssocie?.id,
      });
      setSaved && setSaved(true);
      setMode && setMode('creation');
      isRedirect && redirectTo({ idLabo, view: undefined, id: undefined }, 'rendez-vous');
    }
  };

  const isValid = () => {
    const dateBetweenValid = rdv.fin ? moment(rdv.debut, 'HH:mm').isBefore(moment(rdv.fin, 'HH:mm')) : true;
    const dateRdvValid = rdv.date && moment(rdv.date).isSameOrAfter(moment(new Date()), 'day');
    return (
      dateRdvValid && rdv.debut && dateBetweenValid && (invites || []).length > 0 && (participants || []).length > 0
    );
  };

  const handleChangeNote = useCallback(
    (value: string) => {
      setRdv({ ...rdv, note: value });
    },
    [rdv]
  );

  return (
    <CustomModal
      title={
        mode === 'creation'
          ? 'Planifier un rendez-vous'
          : mode === 'modification'
          ? 'Modifier ce rendez-vous'
          : mode === 'report'
          ? 'Reporter ce rendez-vous'
          : 'Planifier un rendez-vous'
      }
      open={open}
      setOpen={setOpen}
      withBtnsActions={false}
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      fullScreen={!!isMobile}
      customHandleClose={handleClose}
    >
      <Box width={1}>
        <Grid style={{ marginTop: 8 }} container spacing={2}>
          <Grid item lg={12} sm={12} md={12} xs={12}>
            <PartenaireInput
              onChange={handleChangePartenaireTypeAssocie}
              label="Labo"
              index="laboratoire"
              value={rdv.partenaireTypeAssocie}
              defaultValue={laboratoire}
              disabled={laboratoire ? true : false}
            />
          </Grid>
        </Grid>
        {mode !== 'report' && (
          <Fragment>
            <Grid container spacing={2}>
              <Grid item lg={6} sm={12} xs={12} md={6}>
                <UserInput
                  openModal={openModalInvites}
                  idPartenaireTypeAssocie={rdv.partenaireTypeAssocie?.id}
                  withAssignTeam={false}
                  withNotAssigned={false}
                  partenaireType="LABORATOIRE"
                  category="CONTACT"
                  setOpenModal={setOpenModalInvites}
                  selected={invites}
                  setSelected={setInvites}
                  label="Représentant labo"
                  title="Invités"
                  disabled={!rdv.partenaireTypeAssocie?.id}
                />
              </Grid>
              <Grid item lg={6} sm={12} xs={12} md={6}>
                <UserInput
                  openModal={openModalCollaborateur}
                  withNotAssigned={false}
                  setOpenModal={setOpenModalCollaborateur}
                  selected={participants}
                  setSelected={setParticipants}
                  label="Collaborateurs"
                  defaultValueMe={true}
                />
              </Grid>
            </Grid>
          </Fragment>
        )}
        <Grid style={{ marginTop: 8 }} container spacing={2}>
          <Grid item lg={6} sm={12} md={6} xs={12}>
            <CustomDatePicker
              style={{ width: '100%' }}
              name="date"
              value={rdv.date}
              onChange={handleChangeDate}
              required
              label="Date"
            />
          </Grid>
          <Grid container item lg={6} sm={12} md={6} xs={12}>
            <Grid style={{ paddingRight: 8 }} item sm={6} xs={6}>
              <CustomFormTextField type="time" label="A" onChange={handleChangeDebut} value={rdv.debut} />
            </Grid>
            <Grid style={{ paddingLeft: 8 }} item sm={6} xs={6}>
              <CustomFormTextField type="time" label="Jusqu'à" onChange={handleChangeFin} value={rdv.fin} />
            </Grid>
          </Grid>
        </Grid>
        <Grid style={{ marginTop: 8 }} container spacing={2}>
          <Grid item lg={12} sm={12} md={12} xs={12}>
            <CustomSelect
              list={typeReunions}
              index="libelle"
              listId="code"
              label="Type Rendez-Vous"
              value={rdv?.typeReunion || 'VISIO'}
              onChange={handleChangeTypeReunion}
              required
            />
          </Grid>
        </Grid>
        <Grid style={{ marginTop: 8 }} container spacing={2}>
          <Grid item lg={12} sm={12} md={12} xs={12}>
            <CustomEditorText value={rdv.note || ''} placeholder="Note" onChange={handleChangeNote} />
          </Grid>
        </Grid>
        <Box display="flex" marginTop={3} justifyContent="flex-end" width={1}>
          <NewCustomButton disabled={!isValid() || saving} onClick={handleSubmit}>
            {mode === 'creation' ? 'PLANIFIER' : mode === 'report' ? 'REPORTER' : 'SAUVEGARDER'}
          </NewCustomButton>
        </Box>
      </Box>
    </CustomModal>
  );
};

export default RendezVousForm;
