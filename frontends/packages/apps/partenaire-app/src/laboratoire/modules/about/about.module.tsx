import React from 'react';
import loadable from '@loadable/component';
import { SmallLoading } from '@lib/ui-kit';
import { ModuleDefinition } from '@lib/common';

const AboutPage = loadable(() => import('./pages/About'), {
  fallback: <SmallLoading />,
});

const activationParameters = {
  groupement: '0305',
  pharmacie: '0708',
};

const aboutModule: ModuleDefinition = {
  routes: [
    {
      attachTo: 'MAIN',
      path: ['/laboratoires/:idLabo/about/:view?', '/laboratoires/:idLabo/selected/:view?'],
      component: AboutPage,
      activationParameters,
    },
  ],
};

export default aboutModule;
