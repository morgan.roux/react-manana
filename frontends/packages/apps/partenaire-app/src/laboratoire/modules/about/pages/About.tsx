import { endTime, startTime, useApplicationContext } from '@lib/common';
import { Backdrop, CustomAvatarGroup, ErrorPage, TabPanel } from '@lib/ui-kit';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import { Avatar, Box, Grid, Paper, Tab, Tabs, Typography } from '@material-ui/core';
import { Equalizer, Language, Phone, Place, Visibility, History } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import RendezVous from '../../rendezVous/pages/rendez-vous';
import RendezVousForm from '../../rendezVous/pages/rendez-vous-form';
import { useStyles } from './style';
import { SvgGroupement, SvgPartenaire } from './../../../components/svg';
import { noImageSrc } from './../../../assets/img/pas_image.png';
import { RendezVousFormOutput } from '../../rendezVous/pages/rendez-vous-form/interface';
import {
  useCreate_Rendez_VousMutation,
  useDelete_Rendez_VousMutation,
  useGet_LaboratoireLazyQuery,
  useGet_Rendez_VousLazyQuery,
  useUpdate_Rendez_VousMutation,
  useCreate_PartenariatMutation,
  useUpdate_PartenariatMutation,
  useGet_List_Partenariat_TypesLazyQuery,
  useGet_List_Partenariat_StatutsLazyQuery,
} from '@lib/common/src/federation';
import { PRTRendezVous } from '@lib/types';
import { PartenariatForm } from '../../partenariat';

const About: FC<{}> = () => {
  const [modeRdv, setModeRdv] = useState<'creation' | 'modification' | 'report'>('creation');
  const [openEditPartenariat, setOpenEditPartenariat] = useState<boolean>(false);
  const [activeRdvTab, setActiveRdvTab] = useState<number>(0);
  const [openFormRdv, setOpenFormRdv] = useState<boolean>(false);
  const [savingCreateRdv, setSavingCreateRdv] = useState<boolean>(false);
  const [savedCreateRdv, setSavedCreateRdv] = useState<boolean>(false);
  const [savingCreatePartenariat, setSavingCreatePartenariat] = useState<boolean>(false);
  const [savedCreatePartenariat, setSavedCreatePartenariat] = useState<boolean>(false);
  const [skipRdv, setSkipRdv] = useState<number>(0);
  const [takeRdv, setTakeRdv] = useState<number>(2);
  const [typeUpdateRdv, setTypeUpdateRdv] = useState<string>('update');
  const [rdvEdit, setRdvEdit] = useState<PRTRendezVous | undefined>();

  const styles = useStyles();
  const { push } = useHistory();
  const { tab, idLabo } = useParams<{ tab: string; idLabo: string }>();
  const { isMobile, federation, currentPharmacie, notify } = useApplicationContext();

  const [loadLaboratoire, loadingLaboratoire] = useGet_LaboratoireLazyQuery({ client: federation });

  const [loadRealisesRdv, loadingRealisesRdv] = useGet_Rendez_VousLazyQuery({ client: federation });

  const [loadPlanifiesRdv, loadingPlanifiesRdv] = useGet_Rendez_VousLazyQuery({ client: federation });

  const [deleteRdv, deletingRdv] = useDelete_Rendez_VousMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Le rendez-vous a été supprimé avec succès !',
        type: 'success',
      });
      loadingRealisesRdv?.refetch && loadingRealisesRdv.refetch();
      loadingPlanifiesRdv?.refetch && loadingPlanifiesRdv.refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression du rendez-vous',
        type: 'error',
      });
    },
  });
  const [updateRdv, updatingRdv] = useUpdate_Rendez_VousMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Le rendez-vous a été modifié avec succès !',
        type: 'success',
      });
      loadingRealisesRdv?.refetch && loadingRealisesRdv.refetch();
      loadingPlanifiesRdv?.refetch && loadingPlanifiesRdv.refetch();
      setSavingCreateRdv(false);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du rendez-vous',
        type: 'error',
      });
      setSavingCreateRdv(false);
    },
  });
  const [createRdv, creatingRdv] = useCreate_Rendez_VousMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Rendez-vous planifié.',
        type: 'success',
      });
      loadingRealisesRdv?.refetch && loadingRealisesRdv.refetch();
      loadingPlanifiesRdv?.refetch && loadingPlanifiesRdv.refetch();
      setSavingCreateRdv(false);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la planification du rendez-vous',
        type: 'error',
      });
      setSavingCreateRdv(false);
    },
  });
  const [createPartenariat, creatingPartenariat] = useCreate_PartenariatMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Le partenariat a été modifié avec succès!',
        type: 'success',
      });
      loadingLaboratoire?.refetch && loadingLaboratoire.refetch();
      setSavingCreatePartenariat(false);
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du rendez-vous',
        type: 'error',
      });
    },
  });
  const [updatePartenariat, updatingPartenariat] = useUpdate_PartenariatMutation({
    client: federation,
    onCompleted: () => {
      notify({
        message: 'Le partenariat a été modifié avec succès!',
        type: 'success',
      });
      loadingLaboratoire?.refetch && loadingLaboratoire.refetch();
      setSavingCreatePartenariat(false);
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'enregistrement de partenariat",
        type: 'error',
      });
    },
  });
  const [loadTypePartenariats, loadingTypePartenariats] = useGet_List_Partenariat_TypesLazyQuery({
    client: federation,
  });
  const [loadStatutPartenariats, loadingStatutPartenariats] = useGet_List_Partenariat_StatutsLazyQuery({
    client: federation,
  });

  useEffect(() => {
    searchRdvRealise();
    searchRdvPlanifie();
    loadTypePartenariats();
    loadStatutPartenariats();
  }, []);

  useEffect(() => {
    if (idLabo) {
      loadLaboratoire({
        variables: {
          id: idLabo,
          idPharmacie: currentPharmacie.id,
        },
      });
    }
  }, [idLabo]);

  const searchRdvRealise = () => {
    loadRealisesRdv({
      variables: {
        filter: {
          and: [
            {
              idPharmacie: {
                eq: currentPharmacie.id,
              },
            },
            {
              idPartenaireTypeAssocie: {
                eq: idLabo,
              },
            },
            {
              statut: {
                in: ['REALISE', 'ANNULE', 'PLANIFIE', 'REPORTER'],
              },
            },
            {
              or: [
                {
                  dateRendezVous: {
                    lt: endTime(moment(new Date()).format('YYYY-MM-DD')),
                  },
                },
                {
                  and: [
                    {
                      dateRendezVous: {
                        between: {
                          lower: startTime(moment(new Date())),
                          upper: endTime(moment(new Date())),
                        },
                      },
                      heureFin: {
                        lt: moment(new Date()).format('HH:mm'),
                      },
                    },
                  ],
                },
              ],
            },
          ],
        },
        paging: {
          offset: skipRdv,
          limit: takeRdv,
        },
      },
    });
  };

  const searchRdvPlanifie = () => {
    loadPlanifiesRdv({
      variables: {
        filter: {
          and: [
            {
              idPharmacie: {
                eq: currentPharmacie.id,
              },
            },
            {
              idPartenaireTypeAssocie: {
                eq: idLabo,
              },
            },
            {
              statut: {
                in: ['PLANIFIE', 'REPORTE'],
              },
            },
            {
              or: [
                {
                  dateRendezVous: {
                    gt: endTime(moment(new Date())),
                  },
                },
                {
                  and: [
                    {
                      dateRendezVous: {
                        between: {
                          lower: startTime(moment(new Date())),
                          upper: endTime(moment(new Date())),
                        },
                      },
                      heureFin: {
                        gt: moment(new Date()).format('HH:mm'),
                      },
                    },
                  ],
                },
              ],
            },
          ],
        },
        paging: {
          offset: skipRdv,
          limit: takeRdv,
        },
      },
    });
  };

  const handleGoBack = () => {
    alert(tab);
    if (tab === 'selected' || tab === 'list' || tab === 'about') {
      if (idLabo) {
        push('/laboratoires');
      } else {
        push('/');
      }
    } else {
      push(`/laboratoires/${idLabo}/selected`);
    }
  };

  const handleOpenFormRdv = () => {
    setOpenFormRdv((prev) => !prev);
  };

  const setOpenModalEditPartenariat = () => {
    setOpenEditPartenariat((prev) => !prev);
  };

  const handleOpenAddRdv = () => {
    setModeRdv('creation');
    handleOpenFormRdv();
  };

  const handleClickPilotage = () => {
    push(`/laboratoires/${idLabo}/pilotage`);
  };

  const handleClickVoirPlus = () => {
    push(`/laboratoires/${idLabo}/contact`);
  };

  const handleRequestDeleteRdv = (data: any) => {
    if (laboratoire?.id) {
      deleteRdv({
        variables: {
          id: data.id,
        },
      });
    }
  };

  const handleRequestCancelRdv = (data: any) => {
    if (laboratoire?.id) {
      setTypeUpdateRdv('cancel');
      updateRdv({
        variables: {
          id: data.id,
          input: {
            statut: 'ANNULE',
            partenaireType: 'LABORATOIRE',
            idPartenaireTypeAssocie: laboratoire.id,
            idInvites: data.invites.map(({ id }: any) => id),
            idUserParticipants: data.participants.map(({ id }: any) => id),
            idSubject: data.idSubject,
            dateRendezVous: data.dateRendezVous,
            heureDebut: data.heureDebut,
            heureFin: data.heureFin,
            typeReunion: data.typeReunion,
          },
        },
      });
    }
  };

  const handleRdvSearch = ({ skip, take }: any) => {
    setSkipRdv(skip);
    setTakeRdv(take);
  };

  const handleRequestSaveRdv = (data: RendezVousFormOutput) => {
    setSavingCreateRdv(true);
    setSavedCreateRdv(false);
    if (laboratoire?.id) {
      if (!data.id) {
        createRdv({
          variables: {
            input: {
              ...data,
              partenaireType: 'LABORATOIRE',
              idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
            },
          },
        });
      } else {
        setTypeUpdateRdv('update');
        const id = data.id;
        delete data['id'];
        updateRdv({
          variables: {
            id,
            input: {
              ...data,
              partenaireType: 'LABORATOIRE',
              idPartenaireTypeAssocie: data.idPartenaireTypeAssocie,
            },
          },
        });
      }
    }
  };

  const handleRequestSavePartenariat = (data: any) => {
    if (laboratoire?.id) {
      setSavingCreatePartenariat(true);
      setSavedCreatePartenariat(false);
      if (data.id) {
        updatePartenariat({
          variables: {
            id: data.id,
            update: {
              idPartenaireTypeAssocie: laboratoire.id,
              partenaireType: 'LABORATOIRE',
              ...data,
              dateDebut: moment(data.dateDebut).startOf('day').utc(true).format(),
              dateFin: data.dateFin ? moment(data.dateFin).endOf('day').utc(true).format() : undefined,
              id: undefined,
              idLabo: undefined,
              isEnvoiEmail: data.isEnvoiEmail || false,
              typeReunion: data.typeReunion,
            },
          },
        });
      } else {
        createPartenariat({
          variables: {
            input: {
              idPartenaireTypeAssocie: laboratoire.id,
              partenaireType: 'LABORATOIRE',
              ...data,
              dateDebut: moment(data.dateDebut).startOf('day').utc(true).format(),
              dateFin: data.dateFin ? moment(data.dateFin).endOf('day').utc(true).format() : undefined,
              idLabo: undefined,
              isEnvoiEmail: data.isEnvoiEmail || false,
              typeReunion: data.typeReunion,
            },
          },
        });
      }
    }
  };

  const rendezVousRealises = {
    loading: loadingRealisesRdv.loading,
    error: loadingRealisesRdv.error,
    data: loadingRealisesRdv.data?.pRTRendezVous.nodes || ([] as any),
    rowsTotal: (loadingRealisesRdv.data?.pRTRendezVous.nodes || []).length, // TODO : use aggregate
  };

  const rendezVousPlanifies = {
    loading: loadingPlanifiesRdv.loading,
    error: loadingPlanifiesRdv.error,
    data: loadingPlanifiesRdv.data?.pRTRendezVous.nodes || ([] as any),
    rowsTotal: (loadingPlanifiesRdv.data?.pRTRendezVous.nodes || []).length,
  };

  const laboratoire = loadingLaboratoire?.data?.laboratoire;

  return (
    <CustomContainer
      filled
      bannerContentStyle={{
        justifyContent: 'center',
      }}
      bannerBack
      onBackClick={handleGoBack}
      bannerTitle="À propos"
      contentStyle={{
        width: '100%',
      }}
      onlyBackAndTitle={true}
      divider={false}
    >
      <div className={styles.root}>
        {loadingLaboratoire?.loading && <Backdrop />}
        <div className={styles.bandRed}>
          <Box display="flex" flexDirection="row" justifyContent={isMobile ? 'flex-start' : 'center'}>
            <Box>
              <Avatar
                className={styles.img}
                src={laboratoire?.photo?.publicUrl || noImageSrc}
                alt="photo laboratoire"
              />
            </Box>
            <Box ml={3}>
              <Box className={styles.titleLaboratoire}>
                <Typography>{laboratoire?.nom || 'n/a'}</Typography>
              </Box>
              <Box pb={2}>
                <Box pb={1}>
                  <Typography>
                    <Place />
                    &nbsp;{laboratoire?.laboratoireSuite?.adresse || 'N/A'}
                  </Typography>
                </Box>
                <Box pb={1}>
                  <Typography>
                    <Phone />
                    Tél:&nbsp;
                    {laboratoire?.laboratoireSuite?.telephone || 'N/A'}
                  </Typography>
                </Box>
              </Box>
              <Box
                display="flex"
                height="50px"
                flexDirection={isMobile ? 'column' : 'row'}
                className={isMobile ? styles.mobileBandTopAbout : ''}
              >
                <a
                  href={laboratoire?.laboratoireSuite?.webSiteUrl || '#'}
                  target="_blank"
                  className={`${styles.btnActionBandRed} ${isMobile ? styles.mobileBandTopAboutChild : ''}`}
                >
                  <Language />
                  &nbsp;SITE INTERNET
                </a>
                {
                  /* !isMobile() && */ <div
                    className={`${styles.btnActionBandRed} ${isMobile ? styles.mobileBandTopAboutChild : ''}`}
                    onClick={handleClickVoirPlus}
                  >
                    <Visibility />
                    &nbsp;RELATION LABO
                  </div>
                }

                <div
                  className={`${styles.btnActionBandRed} ${isMobile ? styles.mobileBandTopAboutChild : ''}`}
                  style={{ borderRadius: '50%', width: 43 }}
                  onClick={handleClickPilotage}
                >
                  <Equalizer />
                </div>
              </Box>
            </Box>
          </Box>
        </div>
        {!loadingLaboratoire.error ? (
          <Grid container spacing={0} className={styles.borderBottomCCC}>
            <Grid className={styles.partenariatContainer} item lg={8} md={8} sm={12}>
              <Box
                display="flex"
                flexDirection="row"
                justifyContent="space-between"
                className={styles.boxTitlePartenariat}
              >
                <Box className={styles.titlePartenariat}>
                  <Typography className={styles.partenaire}>Partenariat</Typography>
                  {laboratoire?.partenariat?.type?.code && (
                    <Box>
                      {laboratoire?.partenariat?.type?.code === 'GROUPEMENT' ? <SvgGroupement /> : <SvgPartenaire />}
                    </Box>
                  )}
                </Box>
                <Box onClick={setOpenModalEditPartenariat} className={styles.link}>
                  MODIFIER LE PARTENARIAT
                </Box>
              </Box>
              <div className={styles.containerInfo}>
                <div>
                  <span className={styles.attribut}>Date Début :</span>
                  <span>
                    {laboratoire?.partenariat?.dateDebut
                      ? moment(laboratoire?.partenariat?.dateDebut).utc(true).startOf('day').format('DD/MM/YYYY')
                      : '-'}
                  </span>
                </div>
                <div>
                  <span className={styles.attribut}>Date Fin :</span>
                  <span>
                    {laboratoire?.partenariat?.dateFin
                      ? moment.utc(laboratoire?.partenariat?.dateFin).format('DD/MM/YYYY')
                      : '-'}
                  </span>
                </div>
                <div>
                  <span className={styles.attribut}>Status partenariat :</span>
                  <span>{laboratoire?.partenariat?.statut?.libelle || '-'}</span>
                </div>
                <div>
                  <span className={styles.attribut}>Type Partenariat :</span>
                  <span>{laboratoire?.partenariat?.type?.libelle || '-'}</span>
                </div>
                <div style={{ width: 'unset !important' }}>
                  <span className={styles.attribut}>Responsables :</span>
                  <span>
                    <CustomAvatarGroup users={(laboratoire?.partenariat?.responsables || []) as any} max={3} />
                  </span>
                </div>
              </div>
              <div className={styles.btnEditPartenariatContainer} onClick={handleOpenAddRdv}>
                <button className={styles.btnWhite}>
                  <History />
                  &nbsp;PROCHAIN RENDEZ-VOUS
                </button>
              </div>
            </Grid>
          </Grid>
        ) : (
          <ErrorPage />
        )}
        <Grid container>
          <Paper className={styles.labInformationContainer}>
            <Box className={styles.titlePartenariat}>Rendez-Vous</Box>

            <Tabs
              value={activeRdvTab}
              indicatorColor="secondary"
              textColor="secondary"
              onChange={(_event, newValue) => setActiveRdvTab(newValue)}
              aria-label="Reunions"
            >
              <Tab label="Planifiés" className={styles.titleTab} />
              <Tab label="Passées" className={styles.titleTab} />
            </Tabs>
            <TabPanel value={activeRdvTab} index={0}>
              <RendezVous
                {...rendezVousPlanifies}
                onOpenFormRendezVous={handleOpenFormRdv}
                onModeFormRendezVous={setModeRdv}
                // onRequestParticipantInviteEdit={onRequestParticipantInviteEdit}
                onRequestDelete={handleRequestDeleteRdv}
                onRequestCancel={handleRequestCancelRdv}
                onRequestSearch={handleRdvSearch}
                setRdvEdit={setRdvEdit}
                statutRdv="planifie"
              />
            </TabPanel>
            <TabPanel value={activeRdvTab} index={1}>
              <RendezVous
                {...rendezVousRealises}
                statutRdv="realise"
                onRequestDelete={handleRequestDeleteRdv}
                onRequestCancel={handleRequestCancelRdv}
                onRequestSearch={handleRdvSearch}
              />
            </TabPanel>
          </Paper>
        </Grid>
        <PartenariatForm
          open={openEditPartenariat}
          setOpen={setOpenModalEditPartenariat}
          onSave={handleRequestSavePartenariat}
          saved={savedCreatePartenariat}
          saving={savingCreatePartenariat}
          partenariat={laboratoire?.partenariat as any}
          idLabo={laboratoire?.id || ''}
          typesPartenariats={loadingTypePartenariats?.data?.pRTPartenariatTypes.nodes as any}
          statutsPartenariats={loadingStatutPartenariats?.data?.pRTPartenariatStatuts.nodes as any}
          setSaved={setSavedCreatePartenariat}
        />
        {openFormRdv && (
          <RendezVousForm
            mode={modeRdv}
            setOpen={handleOpenFormRdv}
            open={openFormRdv}
            onRequestSave={handleRequestSaveRdv}
            saving={savingCreateRdv}
            saved={savedCreateRdv}
            setSaved={setSavedCreateRdv}
            rendezVousEdit={rdvEdit}
            laboratoire={laboratoire as any}
          />
        )}
      </div>
    </CustomContainer>
  );
};

export default About;
