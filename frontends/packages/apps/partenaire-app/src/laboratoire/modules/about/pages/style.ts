import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      fontFamily: 'Roboto',
    },
    bandRed: {
      width: '100%',
      background: theme.palette.primary.main,
      padding: 24,
      color: theme.palette.common.white,
    },
    informationLaboContainer: {
      maxWidth: 400,
      display: 'flex',
      alignItems: 'center',
    },
    photoLaboContainer: {
      width: 80,
      height: 80,
      overflow: 'hidden',
      borderRadius: 100,
      background: theme.palette.common.white,
      marginRight: 32,
    },
    img: {
      objectFit: 'cover',
      objectPosition: 'center',
      width: 80,
      height: 80,
      [theme.breakpoints.down('sm')]: {
        width: 52,
        height: 52,
      },
    },
    informationLabo: {
      '& h2': {
        marginBottom: 16,
        marginTop: 0,
        fontWeight: 'normal',
        fontSize: 'x-large',
        [theme.breakpoints.down('sm')]: {
          fontSize: 16,
        },
      },
      '& div': {
        display: 'flex',
        alignItems: 'center',
        marginBottom: 8,
        fontSize: 'medium',
        marginLeft: -5,
      },
    },
    actionBandRedContainer: {
      marginTop: 32,
      display: 'flex',
      width: '100%',
      justifyContent: 'center',
      flexWrap: 'wrap',
      [theme.breakpoints.up('sm')]: {
        justifyContent: 'flex-start',
      },
    },
    boxTitlePartenariat: {
      paddingBottom: 10,
      marginBottom: 10,
      borderBottom: '1px solid rgba(0,0,0,0.2)',
      alignItems: 'center',
    },
    titlePartenariat: {
      display: 'flex',
      fontWeight: 'bold',
      fontSize: 20,
      [theme.breakpoints.down('sm')]: {
        fontSize: 16,
      },
    },
    partenaire: {
      marginRight: '16px',
      color: '#424242',
      opacity: '1',
      fontSize: '20px',
      fontFamily: 'Roboto,Medium',
      fontWeight: 400,
    },
    svg: {
      fill: theme.palette.primary.main,
    },
    btnActionBandRed: {
      marginRight: 24,
      marginBottom: 8,
      padding: 12,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      background: theme.palette.common.white,
      color: theme.palette.common.black,
      textDecoration: 'none',
      borderRadius: 4,
      cursor: 'pointer',
      '&:hover': {
        background: theme.palette.grey[200],
      },
    },
    detailContainer: {
      width: '100%',
      display: 'flex',
      flexWrap: 'wrap',
    },
    partenariatContainer: {
      width: '100%',
      padding: 24,
      height: 'auto',
      borderRight: `none`,
      [theme.breakpoints.up('md')]: {
        // borderBottom: `2px solid ${theme.palette.grey[400]}`,
        marginLeft: 'auto',
        marginRight: 'auto',
      },
      '& h2': {
        fontSize: 20,
        fontWeight: 'bolder',
        margin: '0 0 32px 0',
      },
    },
    labInformationContainer: {
      width: '100%',
      padding: 24,
      '& h2': {
        fontSize: 20,
        fontWeight: 'bolder',
        margin: '0 0 32px 0',
      },
    },
    containerInfo: {
      width: '100%',
      '& > div': {
        marginBottom: 16,
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        [theme.breakpoints.up('md')]: {
          marginBottom: 32,
        },
      },
    },
    btnEditPartenariatContainer: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
      marginTop: 32,
      [theme.breakpoints.up('md')]: {
        justifyContent: 'flex-end',
      },
    },
    btnWhite: {
      marginBottom: 8,
      padding: 12,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      background: theme.palette.common.white,
      color: theme.palette.primary.main,
      textDecoration: 'none',
      borderRadius: 4,
      cursor: 'pointer',
      border: `1px solid ${theme.palette.primary.main}`,
      fontSize: 14,
      '&:hover': {
        background: theme.palette.grey[200],
      },
    },
    attribut: {
      color: theme.palette.grey[500],
      fontSize: 16,
      [theme.breakpoints.down('sm')]: {
        fontSize: 14,
      },
    },
    articleContainer: {
      display: 'flex',
      justifyContent: 'flex-end',
      flexDirection: 'column',
      textAlign: 'right',
    },
    link: {
      color: theme.palette.primary.main,
      cursor: 'pointer',
      fontSize: 14,
      textAlign: 'right',
      [theme.breakpoints.down('sm')]: {
        alignItems: 'center',
      },
    },
    titleLaboratoire: {
      fontFamily: 'Roboto',
      fontSize: 20,
      marginBottom: 14,
      [theme.breakpoints.down('sm')]: {
        fontSize: 16,
      },
    },
    infoLaboratoire: {
      fontSize: 14,
    },
    titleTab: {
      fontSize: 16,
      textTransform: 'none',
      [theme.breakpoints.down('sm')]: {
        fontSize: 14,
      },
    },
    btnGoToPilotage: {
      color: theme.palette.secondary.main,
      background: '#FFFFFF',
    },
    borderBottomCCC: {
      borderBottom: '1px solid #ccc',
    },
    mobileBandTopAbout: {
      display: 'block',
      marginLeft: -75,
      height: '100px !important',
    },
    mobileBandTopAboutChild: {
      display: 'inline-flex !important',
      width: 170,
    },
  })
);
