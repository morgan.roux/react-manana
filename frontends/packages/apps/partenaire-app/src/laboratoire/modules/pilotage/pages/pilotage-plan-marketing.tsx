import { Box, createStyles, makeStyles, Theme, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import PlanningPlanMarketing from '../../trade-marketing/pages/planning-marketing/PlanningTradeMarketing';
import { useQueryPilotage } from '../utils';
import PilotageBanner from './PilotageBanner';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    planninRoot: {
      background: '#F8F8F8',
      padding: 24,
    },
    planningContainerBox: {
      background: '#FFF',
      padding: 20,
    },
  })
);

const Pilotage: FC<{}> = ({}) => {
  const classes = useStyles();

  const { laboratoire, planningPageProps } = useQueryPilotage({});

  return (
    <>
      <PilotageBanner value="PLAN_MARKETING_GLOBAL" />
      <PlanningPlanMarketing
        topBarComponent={
          <Box style={{ marginBottom: 10 }}>
            <Typography style={{ fontSize: 20, fontWeight: 'bold' }}>Plan Marketing global</Typography>
          </Box>
        }
        containerClasses={{
          root: classes.planninRoot,
          containerBox: classes.planningContainerBox,
        }}
        key="global"
        // title={`Plan marketing ${laboratoire?.nom ? `/ ${laboratoire.nom}` : 'global'}`}
        //hideFirstColumn={false}
        // hideCloseBtn
        {...planningPageProps}
      />
    </>
  );
};

export default Pilotage;
