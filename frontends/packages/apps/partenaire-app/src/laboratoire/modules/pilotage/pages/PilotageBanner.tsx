import { useApplicationContext } from '@lib/common';
import { CustomSelect } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import { CSSProperties } from '@material-ui/styles';
import React, { ChangeEvent, FC } from 'react';
import { useHistory, useParams } from 'react-router';
import { LaboTopSection } from '../../../components/LaboTopSection/LaboTopSection';
import { useQueryPilotage } from '../utils';
import { useLaboratoireParams } from './../../../hooks/useLaboratoireParams';

interface PilotageBannerProps {
  value: string;
}

const PilotageBanner: FC<PilotageBannerProps> = ({ value }) => {
  const history = useHistory();
  const {
    params: { idLabo },
  } = useLaboratoireParams();
  const { laboratoire } = useQueryPilotage({});
  const { isMobile } = useApplicationContext();
  const handleChangeViewPilotage = (e: ChangeEvent<HTMLInputElement>) => {
    history.push(`/laboratoires/pilotages/${e.target.value}${idLabo?.length ? `?idLabo=${idLabo[0]}` : ``}`);
  };

  const handleClose = () => {
    history.push(`/laboratoires/plan${idLabo?.length ? `?idLabo=${idLabo[0]}` : ``}`);
  };

  const viewSelectionProps = {
    list: [
      {
        id: '1',
        label: laboratoire?.nom ? `Pilotage du labo` : 'Pilotage des Labos',
        value: 'PILOTAGES',
      },
      {
        id: '2',
        label: 'Plan Marketing Global',
        value: 'PLAN_MARKETING_GLOBAL',
      },
      {
        id: '3',
        label: 'Pilotage des rémunérations',
        value: 'PILOTAGE_REMUNERATIONS',
      },
      {
        id: '3',
        label: 'Historique des rémunérations',
        value: 'SUIVI_REMUNERATIONS',
      },
    ],
    index: 'label',
    value: value,
    listId: 'value',
    onChange: (e: any) => {
      handleChangeViewPilotage(e);
    },
  };

  return (
    <>
      <LaboTopSection
        titleStyle={{
          padding: '20px 20px 0 20px',
        }}
        withCloseBtn={true}
        withStats={false}
        handleCloseButton={handleClose}
        title="Pilotage"
        selectProps={viewSelectionProps}
      />
      {isMobile && (
        <Box py={2} px={3}>
          <CustomSelect {...viewSelectionProps} />
        </Box>
      )}
    </>
  );
};

export default PilotageBanner;
