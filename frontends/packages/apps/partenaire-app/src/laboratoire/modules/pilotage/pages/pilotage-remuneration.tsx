import { createStyles, makeStyles, Theme } from '@material-ui/core';
import React, { FC } from 'react';
import PilotageBanner from './PilotageBanner';
import PilotageRemuneration from './../../remuneration/pages/Remuneration';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    planninRoot: {
      background: '#F8F8F8',
      padding: 24,
    },
    planningContainerBox: {
      background: '#FFF',
      padding: 20,
    },
  })
);

const Pilotage: FC<{}> = ({}) => {
  const classes = useStyles();

  return (
    <>
      <PilotageBanner value="PILOTAGE_REMUNERATIONS" />
      <PilotageRemuneration
        classes={{
          root: classes.planninRoot,
          containerBox: classes.planningContainerBox,
        }}
      />
    </>
  );
};

export default Pilotage;
