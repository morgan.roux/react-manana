import { Box, Typography } from '@material-ui/core';
import React from 'react';
import { Column } from '@lib/ui-kit';
import { SuiviRemunerationEntreDate } from './../interfaces';
import { formatMoney, round2Decimals } from '../../../util';
import { useStyles } from './style';

export interface ColumnSuiviRemunerationProps {
  data: SuiviRemunerationEntreDate[];
}

export const useColumnSuiviRemuneration = ({ data }: ColumnSuiviRemunerationProps) => {
  const classes = useStyles();

  const columns: Column[] = [
    {
      name: 'labo',
      label: 'Nom du labo',
      colSpan: 1,
      sortable: false,
      renderer: (row: any, index: any) => {
        if (index + 1 === data?.length && data?.length > 1) {
          return (
            <Box textAlign="right">
              <Typography>{row?.nomPartenaire || ''}</Typography>
            </Box>
          );
        }
        return <Typography>{row?.nomPartenaire || ''}</Typography>;
      },
    },
    {
      name: 'coop_prevue',
      label: 'Autres Rém. prévue',
      colSpan: 1,
      sortable: false,
      renderer: (row: any) => {
        return <Typography>{formatMoney(row?.CoopTotal.prevu)} €</Typography>;
      },
    },
    {
      name: 'bri_prevue',
      label: 'BRI prévue',
      colSpan: 1,
      sortable: false,
      renderer: (row: any) => {
        return <Typography>{formatMoney(row.BRITotal.prevu) || 0} €</Typography>;
      },
    },
    {
      name: 'total_prevue',
      label: 'Total prévue',
      colSpan: 1,
      sortable: false,
      renderer: (row: any) => {
        return <Typography>{formatMoney(row?.totalPrevu)} €</Typography>;
      },
    },
    {
      name: 'coop_realise',
      label: 'Autres Rém. payée',
      colSpan: 1,
      sortable: false,
      renderer: (row: any) => {
        return <Typography>{formatMoney(row?.CoopTotal.realise)} €</Typography>;
      },
    },
    {
      name: 'bri_realise',
      label: 'BRI payée',
      colSpan: 1,
      sortable: false,
      renderer: (row: any) => {
        return <Typography>{formatMoney(row?.BRITotal.realise)} €</Typography>;
      },
    },
    {
      name: 'total_realise',
      label: 'Total payée',
      colSpan: 1,
      sortable: false,
      renderer: (row: any) => {
        return <Typography>{formatMoney(row?.totalRealise)} €</Typography>;
      },
    },
    {
      name: 'coop_%',
      label: 'Autres Rém. %',
      colSpan: 1,
      sortable: false,
      renderer: (row: any) => {
        return <Typography>{round2Decimals(row?.CoopTotal?.pourcentage) || 0}</Typography>;
      },
    },
    {
      name: 'bri_%',
      label: 'BRI %',
      colSpan: 1,
      sortable: false,
      renderer: (row: any) => {
        return <Typography>{round2Decimals(row?.BRITotal?.pourcentage) || 0}</Typography>;
      },
    },
    {
      name: 'total_%',
      label: 'Total %',
      colSpan: 1,
      sortable: false,
      renderer: (row: any) => {
        return <Typography>{round2Decimals(row?.pourcentageTotal) || 0}</Typography>;
      },
    },
  ];

  return columns;
};
