import { Box, Typography } from '@material-ui/core';
import { GridColumn } from '@progress/kendo-react-grid';
import React, { FC, useEffect, useState } from 'react';
import { useStyles } from './style';
import moment from 'moment';
import NavigationButtons from '@lib/ui-kit/src/components/atoms/NavigationButtons';
import { CustomGrid } from '@lib/ui-kit';
import { getMonthName } from '@lib/common';
import {
  PlanningPlanMarketingDetails,
  ProduitPlanMarketing,
} from '../../trade-marketing/components/details-trade-marketing';
import { useLaboratoireParams } from '../../../../laboratoire/hooks/useLaboratoireParams';
import { Laboratoire } from '@lib/common/src/federation';

export interface PlanningDetailsProps {
  onRequestChangeStatutAction?: (id: string, statut: 'DONE' | 'ACTIVE') => void;
  onRequestProduitsAndActions?: (idPlanMarketing: string, produits: string[]) => void;
  produits?: {
    loading?: boolean;
    data?: ProduitPlanMarketing[];
    error?: Error;
  };
  actions?: {
    loading?: boolean;
    data?: any[];
    error?: Error;
  };
}

interface PilotagePlanMarketingProps {
  data?: any;
  onRequest?: (date: Date) => void;
  hideFirstColumn?: boolean;
  planningDetail?: PlanningDetailsProps;
}

const getDatesSemaine = (date: Date): Date[] => {
  const nombres = [0, 1, 2, 3, 4, 5, 6];
  return nombres.map((nombre) => moment(date).add(nombre, 'days').toDate());
};
const initialiseTemps = (date?: Date): Date => {
  return moment(date).hour(0).minute(0).second(0).millisecond(0).toDate();
};
const joursSemaine = ['Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam', 'Dim'];

const PilotagePlanMarketing: FC<PilotagePlanMarketingProps> = ({
  data,
  onRequest,
  hideFirstColumn = false,
  planningDetail,
}) => {
  const classes = useStyles();

  const {
    params: { idLabo },
  } = useLaboratoireParams('pilotages');

  const [dateMois, setDateMois] = useState<moment.Moment>(moment().startOf('month'));
  const [dateSemaine, setDateSemaine] = useState<moment.Moment>(moment().day(1));

  const [openDetail, setOpenDetail] = useState<boolean>(false);
  const [planMarketing, setPlanMarketing] = useState<any>();
  const [laboratoire, setLaboratoire] = useState<Laboratoire>();

  const handleOpenDetail = () => {
    setOpenDetail((prev) => !prev);
  };

  useEffect(() => {
    onRequest && onRequest(dateSemaine.toDate());
  }, [dateSemaine, idLabo && idLabo[0]]);

  const handleMoisPrecedent = () => {
    const moisPrecedent = dateMois.clone().subtract(1, 'months');
    setDateMois(moisPrecedent.clone());
    setDateSemaine(moisPrecedent.clone().day(1));
  };

  const handleMoisProchain = () => {
    const moisProchain = dateMois.clone().add(1, 'months');
    setDateMois(moisProchain.clone());
    setDateSemaine(moisProchain.clone().day(1));
  };

  const handleSemainePrecedente = () => {
    const semainePrecedente = dateSemaine.clone().subtract(1, 'weeks');
    if (dateSemaine.month() === dateMois.month()) {
      setDateSemaine(semainePrecedente.clone());
      if (dateMois.date() === dateSemaine.date()) {
        setDateMois(dateMois.clone().subtract(1, 'months'));
      }
    } else {
      setDateMois(dateMois.clone().subtract(1, 'months'));
    }
  };

  const handleSemaineSuivante = () => {
    const semaineSuivante = dateSemaine.clone().add(1, 'weeks');
    if (semaineSuivante.month() === dateMois.month()) {
      setDateSemaine(semaineSuivante.clone());
    } else {
      if (semaineSuivante.date() === dateMois.date()) {
        setDateSemaine(semaineSuivante.clone());
      }
      setDateMois(dateMois.clone().add(1, 'months'));
    }
  };

  const handleOpenModalDetail = (plan: any, prestataire: any) => {
    setOpenDetail(true);
    setPlanMarketing(plan);
    setLaboratoire(prestataire);
    planningDetail?.onRequestProduitsAndActions &&
      planningDetail.onRequestProduitsAndActions(
        plan?.idPartenaireTypeAssocie,
        (plan.produits || []).map(({ id }: any) => id)
      );
  };

  const ColumnCell = (props: any) => {
    const currentDate = initialiseTemps(props?.currentDate);
    let dateSemaine = initialiseTemps(props?.dateSemaine);
    const dateMois = initialiseTemps(props?.dateMois);
    let dateFinSemaine = moment(dateSemaine).add(6, 'days').toDate();

    if (moment(dateSemaine).month() !== moment(dateFinSemaine).month()) {
      if (moment(dateSemaine).month() === moment(dateMois).month()) {
        dateFinSemaine = moment(dateMois).endOf('month').toDate();
      } else {
        dateSemaine = moment(dateMois).startOf('month').toDate();
      }
    }

    const planMarketings = (props?.dataItem?.planMarketings || []).map((plan: any) => {
      return {
        ...plan,
        dateDebut: initialiseTemps(plan?.dateDebut),
        dateFin: initialiseTemps(plan?.dateFin),
      };
    });
    const element = planMarketings.find(
      (plan: any) =>
        (moment(plan?.dateDebut).isBetween(dateSemaine, dateFinSemaine, undefined, '[]') ||
          moment(dateSemaine).isBetween(plan?.dateDebut, plan?.dateFin, undefined, '[]')) &&
        moment(currentDate).isBetween(plan?.dateDebut, plan?.dateFin, undefined, '[]')
    );
    const activeElement = element ? element : undefined;
    const colspan = element
      ? Math.min(
          moment(element.dateFin).diff(moment(element.dateDebut), 'days'),
          moment(dateFinSemaine).diff(moment(element.dateDebut), 'days'),
          moment(element.dateFin).diff(moment(dateSemaine), 'days'),
          moment(dateFinSemaine).diff(moment(dateSemaine), 'days')
        ) + 1
      : undefined;
    return element &&
      ((dateSemaine < element.dateDebut && moment(currentDate).diff(moment(element.dateDebut), 'days') === 0) ||
        moment(currentDate).diff(moment(dateSemaine), 'days') === 0) ? (
      <td
        className={classes.columnCell}
        colSpan={activeElement ? colspan : undefined}
        onClick={() => props.onClick(element, props?.dataItem?.prestataire)}
      >
        <div
          style={{ background: element.type.couleur || 'gray' }}
        >{`${activeElement?.type?.libelle} ${activeElement?.titre}`}</div>
      </td>
    ) : element &&
      ((dateSemaine < element.dateDebut && moment(currentDate).diff(moment(element.dateDebut), 'days') > 0) ||
        moment(currentDate).diff(moment(dateSemaine), 'days') > 0) ? null : (
      <td />
    );
  };

  const firstColumn = (
    <GridColumn
      locked
      headerClassName={classes.firstColumnHeader}
      className={classes.firstColumnCell}
      width="180px"
      resizable
      minResizableWidth={180}
    />
  );

  const columns = getDatesSemaine(dateSemaine.toDate()).map((currentDate, index) => (
    <GridColumn
      key={`date${moment(currentDate).format('DD:MM:YYYY')}`}
      // width="70px"
      resizable
      // minResizableWidth={70}
      headerCell={() => (
        <Box className={classes.headerContainer}>
          <Typography>{joursSemaine[index]}</Typography>
          <Typography>{moment(currentDate).month() === dateMois.month() ? moment(currentDate).date() : '-'}</Typography>
        </Box>
      )}
      cell={(props) => (
        <ColumnCell
          {...props}
          {...{
            dateSemaine: dateSemaine.toDate(),
            dateMois: dateMois.toDate(),
            currentDate,
            onClick: handleOpenModalDetail,
          }}
        />
      )}
    />
  ));

  return (
    <Box pt={2}>
      {/* <TableChartSubToolbar onRequestChange={handleTableChartSubToolbarChange} /> */}
      <Box className={classes.navigationBox}>
        <NavigationButtons
          // variantButton="text"
          onLeftBtnClick={handleMoisPrecedent}
          onRightBtnClick={handleMoisProchain}
        >
          <Typography className={classes.textMois}>
            {`${getMonthName(dateMois.month() + 1)} ${dateMois.year()}`}
          </Typography>
        </NavigationButtons>
      </Box>
      <Box className={classes.navigationBox}>
        <NavigationButtons
          // variantButton="text"
          onLeftBtnClick={handleSemainePrecedente}
          onRightBtnClick={handleSemaineSuivante}
        >
          <Typography className={classes.textSemaine}>{`Semaine ${dateSemaine.week()}`}</Typography>
        </NavigationButtons>
      </Box>
      <Box display="flex" justifyContent="center" overflow="auto">
        <CustomGrid
          pageable={false}
          groupable={false}
          columns={!hideFirstColumn ? [firstColumn, ...columns] : columns}
          filterable={false}
          data={data}
          className={classes.grid}
          resizable
        />
      </Box>
      <PlanningPlanMarketingDetails
        planMarketing={planMarketing}
        open={openDetail}
        setOpen={handleOpenDetail}
        onRequestChangeStatutAction={planningDetail?.onRequestChangeStatutAction}
        produits={planningDetail?.produits}
        laboratoire={laboratoire}
      />
    </Box>
  );
};

export default PilotagePlanMarketing;
