import { Laboratoire } from '@lib/common/src/federation';
// import { PlanningPageProps } from '../Plan';
import { CustomBarChartProps, CustomTimeChartProps, CustomGaugeChartProps } from '@lib/ui-kit';
// import { PlanningDetailsProps } from './PilotagePlanMarketing';

export interface PilotageProps {
  totalChiffreParMoisProps: CustomBarChartProps;
  totalOperationCommercialProps: EvolutionRemunerationProps;
  suiviRemunerationProps: SuiviRemunerationProps;
  suiviOperationnelProps: CustomTimeChartProps;
  onRequestFilter: (filter: any) => void;
  loading?: boolean;
  error?: boolean;
  activeItem?: string;
  laboratoire?: Laboratoire;
  onSideNavListClick?: (item: string) => void;
  dataPilotagePlanMarketing?: any;
  onRequestPilotagePlanMarketing?: (date: Date) => void;
  // planningPageProps?: PlanningPageProps;
  hideFirstColumn?: boolean;
  // planningDetail?: PlanningDetailsProps;
  onRequestGoBack?: () => void;
}

export interface SuiviRemunerationProps {
  title?: string;
  total: CustomGaugeChartProps;
  data: CustomGaugeChartProps[];
  suiviRemunerationEntreDate?: {
    data?: SuiviRemunerationEntreDate[];
    loading?: boolean;
    error?: boolean;
    rowsTotal?: number;
  };
}

export interface EvolutionRemunerationProps {
  totalOperationCommercialCurrentProps: CustomBarChartProps;
}

export interface SuiviRemunerationEntreDate {
  nomPartenaire: string;
  BRITotal: {
    prevu: number;
    realise: number;
    pourcentage: number;
  };
  CoopTotal: {
    prevu: number;
    realise: number;
    pourcentage: number;
  };
  totalPrevu?: number;
  totalRealise?: number;
  pourcentageTotal?: number;
}
