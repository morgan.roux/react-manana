import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginTop: -24,
      fontFamily: 'Roboto',
      width: '100%',
      padding: 16,
      [theme.breakpoints.up('sm')]: {
        padding: 12,
      },
      [theme.breakpoints.down('sm')]: {
        paddingTop: 0,
      },
    },
    boxRow: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
    },
    boxRowBarChard: {
      display: 'flex',
      flexDirection: 'row',
      background: 'white',
      flexWrap: 'wrap',
      borderRadius: 5,
      backgroundColor: '#fff',
      margin: 0,
      padding: 16,
      [theme.breakpoints.up('sm')]: {
        margin: 12,
        padding: 24,
      },
      [theme.breakpoints.down('xs')]: {
        '& h2': {
          fontSize: '16px !important',
        },
      },
    },
    paddingLeft: {
      paddingLeft: 0,
      [theme.breakpoints.up('sm')]: {
        paddingLeft: 24,
      },
    },
    paddingRight: {
      paddingRight: 0,
      [theme.breakpoints.up('sm')]: {
        paddingRight: 24,
      },
    },
    bgWhite: {
      background: '#ffffff',
    },
    borderRight: {
      borderRight: 'none',
      [theme.breakpoints.up('sm')]: {
        borderRight: `1px solid ${theme.palette.grey[300]}`,
      },
    },
    boxPilotage: {
      width: '100%',
      borderRadius: 5,
      marginBottom: 16,
      [theme.breakpoints.up('sm')]: {
        width: '50%',
        marginBottom: 0,
      },
    },
    boxInRow: {
      margin: 0,
      padding: 16,
      width: '100%',
      background: '#ffffff',
      [theme.breakpoints.up('md')]: {
        width: 'calc(50% - 24px)',
        margin: 12,
        padding: 24,
      },
    },
    titleSuiviRenumeration: {
      display: 'flex',
      width: '100%',
      alignItems: 'first baseline',
      justifyContent: 'space-between',
    },
    boxPilotageTitle: {
      color: '#27272F !important',
      fontSize: '20px !important',
      fontWeight: 'bold',
      alignItems: 'center',
      margin: '0 0 32px 0',
    },
    information: {
      color: 'rgba(0,0,0,0.6)',
    },
    link: {
      color: theme.palette.secondary.main,
      cursor: 'pointer',
    },
    titleFullScreen: {
      paddingBottom: 16,
    },
    bigTitleFullScreen: {
      fontWeight: 'bold',
      fontSize: 20,
      [theme.breakpoints.down('md')]: {
        fontSize: 16,
      },
    },
    captionTitleFullScreen: {
      color: 'rgba(0,0,0,0.6)',
    },
    grid: {
      border: 0,
      borderBottom: '1px solid rgba(0, 0, 0, 0.08) !important',
      '& .k-grid-header-wrap': {
        background: '#FFFFFF',
      },
      '& .k-grid-header .k-header': {
        borderColor: '#FFFFFF',
      },
      '& div.k-grid-header': {
        padding: '0px 0px 0px 0px !important',
      },
    },
    navigationBox: {
      display: 'flex',
      justifyContent: 'center',
      width: '100%',
    },
    textMois: {
      fontFamily: 'Roboto',
      fontSize: 18,
      fontWeight: 'bold',
      color: '#424242',
      minWidth: 150,
      textAlign: 'center',
    },
    textSemaine: {
      fontFamily: 'Roboto',
      fontSize: 16,
      fontWeight: 'normal',
      color: '#212121',
      minWidth: 150,
      textAlign: 'center',
    },
    headerContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      fontFamily: 'Roboto',
      '& .MuiTypography-root:nth-child(1)': {
        color: '#42424261',
        fontSize: 16,
        paddingBottom: 8,
      },
      '& .MuiTypography-root:nth-child(2)': {
        fontSize: 16,
        color: '#424242',
        paddingBottom: 8,
      },
    },
    columnCell: {
      padding: '0px 8px !important',
      // height: 40,
      cursor: 'pointer',
      '& div': {
        borderRadius: 4,
        display: 'flex',
        alignItems: 'center',
        fontSize: 12,
        fontFamily: 'Roboto',
        padding: 8,
        color: '#FFFFFF',
      },
    },
    selectContainer: {
      '& .MuiFormControl-root': {
        margin: 10,
      },
      margin: 10,
    },
    boxToTalSaugeSuiviRenumeration: {
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
    },
    saugeContainer: {
      width: 200,
      marginBottom: 0,
      [theme.breakpoints.up('sm')]: {
        width: 300,
        marginBottom: -40,
      },
      [theme.breakpoints.up('md')]: {
        marginBottom: 0,
      },
      [theme.breakpoints.up('lg')]: {
        marginBottom: -100,
      },
    },
    boxOthersSaugeSuiviRenumeration: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    saugeOtherContainer: {
      width: 130,
      margin: 'auto',
      [theme.breakpoints.up('sm')]: {
        width: 300,
      },
      [theme.breakpoints.up('md')]: {
        width: 200,
      },
      [theme.breakpoints.up('lg')]: {
        width: 300,
      },
    },
    firstColumnHeader: {
      background: '#FFFFFF !important',
      '& > .k-link': {
        display: 'none !important',
      },
    },
    firstColumnCell: {
      fontFamily: 'Roboto',
      textAlign: 'left',
      textTransform: 'uppercase',
      padding: '8px 12px',
      fontSize: 14,
      border: '0 !important',
      borderLeft: '1px solid rgba(0, 0, 0, 0.08) !important',
    },
    marginTop: {
      marginTop: '-12px',
    },
    selectPilotageMobile: {
      marginBottom: 0,
      padding: '0 16px',
      marginTop: 15,
      '& *': {
        margin: '0 !important',
        border: 'none !important',
      },
    },
    saugeFlexNoneMobile: {
      [theme.breakpoints.down('xs')]: {
        display: 'block !important',
        '& .MuiBox-root': {
          width: 'unset !important',
        },
      },
    },
    fontSizeTitleMobile: {
      fontSize: '16px !important',
    },
    suiviRenumerationDataMobile: {
      marginBottom: 20,
      '& .with-caption-col': {
        background: '#f8f8f8',
        marginBottom: 2,
        padding: 10,
        display: 'flex',
      },
      '& .with-caption-col>.MuiTypography-root': {
        marginRight: 15,
        width: '33.33%',
        fontSize: 13,
      },
      '& .with-caption-col .MuiTypography-caption': {
        display: 'block',
        fontWeight: 700,
        color: '#212121',
        fontSize: '14px !important',
      },
    },
    captionMobile: {
      display: 'block',
      fontWeight: 700,
      color: '#212121',
      fontSize: '14px !important',
    },
    bold: {
      fontWeight: 'bold',
    },
  })
);
