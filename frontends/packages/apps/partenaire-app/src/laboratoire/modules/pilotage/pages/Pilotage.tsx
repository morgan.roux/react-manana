import { useApplicationContext } from '@lib/common';
import { CustomBarChart, CustomBarChartProps, CustomGaugeChart, SmallLoading, Table, TableChange } from '@lib/ui-kit';
import { Box, Menu, MenuItem, Typography } from '@material-ui/core';
import { ArrowDropDown, Close } from '@material-ui/icons';
import classNames from 'classnames';
import moment from 'moment';
import React, { FC, MouseEvent, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import PilotagePlanMarketing from '../component/PilotagePlanMarketing';
import { useStylePilotage } from '../styles';
import { useQueryPilotage } from '../utils';
import { useLaboratoireParams } from './../../../hooks/useLaboratoireParams';
import { useColumnSuiviRemuneration } from './../component';
import { SuiviRemunerationProps } from './../interfaces';
import PilotageBanner from './PilotageBanner';
import { formatMoney, round2Decimals } from '../../../util';

const Pilotage: FC<{}> = ({}) => {
  const classes = useStylePilotage();
  const { isMobile } = useApplicationContext();
  const { view } = useParams<{ view: string }>();
  const history = useHistory();

  const [fullScreenTotalOperation, setFullScreenTotalOperation] = useState<boolean>(false);
  const [fullScreenTotalChiffre, setFullScreenTotalChiffre] = useState<boolean>(false);
  const [startDate, setFilterStartDate] = useState<any>(moment(new Date()).add(-4, 'month'));
  const [endDate, setFilterEndDate] = useState<any>(moment(new Date()));
  const [filterAnneeComparer, setFilterAnneeComparer] = useState<number>(new Date().getFullYear());
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [skipSuiviRemuneration, setSkipSuiviRemuneration] = useState<number>(0);
  const [takeSuiviRemuneration, setTakeSuiviRemuneration] = useState<number>(12);
  const [filterOrderTable, setFilterOrderTable] = useState<any>();
  const {
    params: { idLabo },
  } = useLaboratoireParams();

  const {
    suiviRemunerationProps,
    totalOperationCommercialProps,
    totalChiffreParMoisProps,
    dataPilotagePlanMarketing,
    onRequestPilotagePlanMarketing,
    hideFirstColumn,
    planningDetail,
  } = useQueryPilotage({
    filterAnneeComparer,
    startDate,
    endDate,
    skipSuiviRemuneration,
    takeSuiviRemuneration,
  });

  const columns = useColumnSuiviRemuneration({
    data: suiviRemunerationProps?.suiviRemunerationEntreDate?.data || [],
  });

  const handleFullScreenTotalOperation = (screen: boolean) => {
    setFullScreenTotalOperation(screen);
  };

  const handleFullScreenTotalChiffre = (screen: boolean) => {
    setFullScreenTotalChiffre(screen);
  };

  const handleCompare = (e: MouseEvent<HTMLElement>) => {
    e.preventDefault();
    const year = filterAnneeComparer === new Date().getFullYear() ? filterAnneeComparer - 1 : filterAnneeComparer + 1;
    setFilterAnneeComparer(year);
    setAnchorEl(null);
  };

  const handleOpenCompare = (e: MouseEvent<HTMLInputElement>) => {
    e.preventDefault();
    setAnchorEl(e.currentTarget);
  };

  const handleCloseCompare = () => {
    setAnchorEl(null);
  };

  const handleChangeFullScrrenTotalOperation = () => {
    handleFullScreenTotalOperation(false);
  };

  const handleSearchTable = ({ skip, take }: TableChange) => {
    setSkipSuiviRemuneration(skip);
    setTakeSuiviRemuneration(take);
    // setSearchText(searchText);
  };

  const handleSortTable = (column: string, direction: 'DESC' | 'ASC') => {
    setFilterOrderTable({ column, direction });
  };

  const handleGoBack = () => {
    history.push(`/laboratoires/plan${idLabo?.length ? `?idLabo=${idLabo[0]}` : ``}`);
  };

  const filterComparaisonComponent = (
    <Box display="flex" flexDirection="row" alignItems="center" justifyContent="space-between" px={2} py={1}>
      <Box display="flex" flexDirection="row" alignItems="center">
        <Typography className={classes.information}>Année de référence: &nbsp;</Typography>
        <Typography
          onClick={handleOpenCompare}
          aria-controls="simple-menu"
          aria-haspopup="true"
          style={{ cursor: 'pointer' }}
        >
          {filterAnneeComparer}
        </Typography>
        <Box>
          <ArrowDropDown />
        </Box>
        <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleCloseCompare}>
          <MenuItem onClick={handleCompare}>
            {filterAnneeComparer === new Date().getFullYear() ? filterAnneeComparer - 1 : filterAnneeComparer + 1}
          </MenuItem>
        </Menu>
      </Box>
    </Box>
  );

  const totalOperationCurrentComponentProps: CustomBarChartProps = {
    series: totalOperationCommercialProps?.totalOperationCommercialCurrentProps?.series,
    options: {
      ...totalOperationCommercialProps?.totalOperationCommercialCurrentProps?.options,
      fullScreen: fullScreenTotalOperation,
      onRequestFullScreen: handleFullScreenTotalOperation,
    },
    otherComponent: !fullScreenTotalOperation ? filterComparaisonComponent : <></>,
    withSubtitle: true,
  };

  const totalChiffreComponentProps: CustomBarChartProps = {
    series: totalChiffreParMoisProps.series,
    options: {
      ...totalChiffreParMoisProps.options,
      fullScreen: fullScreenTotalChiffre,
      onRequestFullScreen: handleFullScreenTotalChiffre,
    },
  };

  const suiviRemunerationComponentProps: SuiviRemunerationProps = {
    total: suiviRemunerationProps.total,
    data: suiviRemunerationProps.data,
  };

  if (fullScreenTotalOperation) {
    return (
      <>
        <Box display="flex" flexDirection="row" justifyContent="space-between" className={classes.titleFullScreen}>
          <Box>
            <Typography className={classes.bigTitleFullScreen}>Comparaison</Typography>
            <Typography className={classes.captionTitleFullScreen}>
              {/* Année {new Date().getFullYear()} vs année {filterAnneeComparer} */}
            </Typography>
          </Box>
          <Box textAlign="right">
            <Close onClick={handleChangeFullScrrenTotalOperation} className={classes.link} />
          </Box>
        </Box>
        <CustomBarChart {...totalOperationCurrentComponentProps} />
      </>
    );
  }
  const suiviRemunerationTable = (
    <Box style={isMobile ? { padding: 15 } : { padding: 24, background: '#F8F8F8' }}>
      <Box
        display="flex"
        flexDirection="row"
        justifyContent="space-between"
        alignItems="center"
        className={classes.titleFullScreen}
        style={{ background: '#FFF', padding: 24 }}
      >
        <Box>
          <Typography className={classes.bigTitleFullScreen}>Historique des Rémunérations</Typography>
          <Typography className={classes.captionTitleFullScreen}>
            Du 01/01/{new Date().getFullYear()} au {moment().format('DD/MM/YYYY')}
          </Typography>
        </Box>
      </Box>
      {isMobile ? (
        <Box>
          {(suiviRemunerationProps?.suiviRemunerationEntreDate?.data || []).map((item: any, index: any) => {
            return (
              <div key={index} className={classes.suiviRenumerationDataMobile}>
                <Typography>{item.nomPartenaire}</Typography>
                <div className="with-caption-col">
                  <Typography component="span" className={classes.rootTypo}>
                    Autres Rém. prévue{' '}
                    <Typography variant="caption" className={classes.captionMobile}>
                      {formatMoney(item.CoopTotal.prevu)} €
                    </Typography>
                  </Typography>
                  <Typography component="span" className={classes.rootTypo}>
                    BRI prévue{' '}
                    <Typography variant="caption" className={classes.captionMobile}>
                      {formatMoney(item.BRITotal.prevu)} €
                    </Typography>
                  </Typography>
                  <Typography component="span" className={classes.rootTypo}>
                    Total prévue{' '}
                    <Typography variant="caption" className={classes.captionMobile}>
                      {formatMoney(item.totalPrevu ? item.totalPrevu : 0)} €
                    </Typography>
                  </Typography>
                </div>
                <div className="with-caption-col">
                  <Typography component="span" className={classes.rootTypo}>
                    Autre rémun payée{' '}
                    <Typography variant="caption" className={classes.captionMobile}>
                      {formatMoney(item.CoopTotal.realise)} €
                    </Typography>
                  </Typography>
                  <Typography component="span" className={classes.rootTypo}>
                    BRI payée{' '}
                    <Typography variant="caption" className={classes.captionMobile}>
                      {formatMoney(item.BRITotal.realise)} €
                    </Typography>
                  </Typography>
                  <Typography component="span" className={classes.rootTypo}>
                    Total payée{' '}
                    <Typography variant="caption" className={classes.captionMobile}>
                      {formatMoney(item.totalRealise ? item.totalRealise : 0)} €
                    </Typography>
                  </Typography>
                </div>
                <div className="with-caption-col">
                  <Typography component="span" className={classes.rootTypo}>
                    Autres Rém. %{' '}
                    <Typography variant="caption" className={classes.captionMobile}>
                      {round2Decimals(item.CoopTotal?.pourcentage) || 0} %
                    </Typography>
                  </Typography>
                  <Typography component="span" className={classes.rootTypo}>
                    BRI %{' '}
                    <Typography variant="caption" className={classes.captionMobile}>
                      {round2Decimals(item.BRITotal?.pourcentage) || 0} %
                    </Typography>
                  </Typography>
                  <Typography component="span" className={classes.rootTypo}>
                    Total %{' '}
                    <Typography variant="caption" className={classes.captionMobile}>
                      {round2Decimals(item.pourcentageTotal ? item.pourcentageTotal : 0) || 0} %
                    </Typography>
                  </Typography>
                </div>
              </div>
            );
          })}
        </Box>
      ) : (
        <Table
          columns={columns}
          data={suiviRemunerationProps.suiviRemunerationEntreDate?.data || []}
          search={false}
          selectable={false}
          rowsTotal={suiviRemunerationProps.suiviRemunerationEntreDate?.rowsTotal || 0}
          error={suiviRemunerationProps.suiviRemunerationEntreDate?.error as any}
          loading={suiviRemunerationProps.suiviRemunerationEntreDate?.loading}
          onRunSearch={handleSearchTable}
          onSortColumn={handleSortTable}
        />
      )}
    </Box>
  );

  const loading = suiviRemunerationProps.loading;
  const error = suiviRemunerationProps.error;

  const renderContent = () => {
    if (view === 'PILOTAGES') {
      return (
        <Box className={classes.root}>
          <Box className={classes.boxRowBarChard} alignItems="center">
            <Box className={classNames(classes.boxPilotage, classes.borderRight, classes.paddingRight)}>
              <CustomBarChart {...totalChiffreComponentProps} />
            </Box>
            <Box className={classNames(classes.boxPilotage, classes.paddingLeft, classes.marginTop)}>
              <CustomBarChart {...totalOperationCurrentComponentProps} />
            </Box>
          </Box>
          <Box className={classes.boxRow}>
            <Box className={classNames(classes.boxInRow)}>
              <Typography className={`${classes.boxPilotageTitle} ${isMobile ? classes.fontSizeTitleMobile : ''}`}>
                Plan Marketing Global
              </Typography>
              <PilotagePlanMarketing
                data={dataPilotagePlanMarketing}
                onRequest={onRequestPilotagePlanMarketing}
                hideFirstColumn={isMobile}
                planningDetail={planningDetail}
              />
            </Box>
            <Box className={classNames(classes.boxInRow)}>
              <Box className={classes.titleSuiviRenumeration}>
                <h2 className={`${classes.boxPilotageTitle} ${isMobile ? classes.fontSizeTitleMobile : ''}`}>
                  Historique des Rémunérations
                </h2>
                <Box display="flex" flexDirection="row" alignItems="center">
                  <Typography className={classes.information}>{new Date().getFullYear()}</Typography>
                </Box>
              </Box>
              <Box className={classes.boxToTalSaugeSuiviRenumeration}>
                <Box className={classes.saugeContainer}>
                  <CustomGaugeChart {...suiviRemunerationProps.total} style={{ width: 250, height: 120 }} />
                </Box>
              </Box>
              <Box
                className={`${classes.boxOthersSaugeSuiviRenumeration} ${isMobile ? classes.saugeFlexNoneMobile : ''}`}
              >
                {suiviRemunerationComponentProps?.data?.map((props: any, index: any) => {
                  return index < 2 ? (
                    <Box className={classes.saugeOtherContainer} key={index}>
                      <CustomGaugeChart {...props} style={{ width: 250, height: 120 }} />
                    </Box>
                  ) : null;
                })}
              </Box>
            </Box>
          </Box>
        </Box>
      );
    }

    return suiviRemunerationTable;
  };

  if (error) {
    return <Box textAlign="center">Une erreur est survenue</Box>;
  }

  return (
    <>
      <PilotageBanner value={view} />
      {/* {isMobile ? <div className={classes.selectPilotageMobile}>{bannerRightComponent}</div> : null} */}
      {loading ? <SmallLoading /> : renderContent()}
    </>
  );
};

export default Pilotage;
