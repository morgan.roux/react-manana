import { useApplicationContext } from '@lib/common';
import {
  useGet_LaboratoireLazyQuery,
  useGet_Remuneration_Suivi_OperationnelLazyQuery,
  useGet_Remuneration_Suivi_OperationnellesLazyQuery,
  useGet_Prestation_TypeLazyQuery,
  useGet_Remuneration_MensuelleLazyQuery,
  usePlanning_MarketingsLazyQuery,
  PrestationTypeSortFields,
  SortDirection,
} from '@lib/common/src/federation';
import { Box, Typography } from '@material-ui/core';
import moment from 'moment';
import React, { useEffect } from 'react';
import { useParams } from 'react-router';
import { useLaboratoireParams } from './../../../hooks/useLaboratoireParams';
import { usePlanningPlanMarketing } from '../../trade-marketing/hooks/usePlanningPlanMarketing';
import { useStyles } from './../styles/styleUtil';

const options = ({ title, percent, level, totalPaye, totalPrevu, type, classes }: any) => {
  return {
    title,
    options: {
      percent,
      level,
      colors: ['#F46036', '#FBB104', '#FFE43A', '#C2C63F', '#00A745', '#008000'],
    },
    descriptionComponent: (
      <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
        <Box>
          <Typography /* className={classes.montant} */>{totalPaye?.toFixed(2) || 0} €</Typography>
        </Box>
        <Box display="flex" flexDirection="column" py={2}>
          <Box display="flex" flexDirection="row">
            <Typography /* className={classes.libelle} */>{type} prévu(es): &nbsp;</Typography>
            <Typography /* className={classes.montant} */>{totalPrevu?.toFixed(2) || 0} €</Typography>
          </Box>
          <Box display="flex" flexDirection="row">
            <Typography /* className={classes.libelle} */>{type} payé(es): &nbsp;</Typography>
            <Typography /* className={classes.montant} */>{totalPaye?.toFixed(2) || 0} €</Typography>
          </Box>
        </Box>
      </Box>
    ),
  };
};

export const useQueryPilotage = ({
  filterAnneeComparer,
  startDate,
  endDate,
  skipSuiviRemuneration,
  takeSuiviRemuneration,
}: any) => {
  const { federation, currentPharmacie } = useApplicationContext();
  const { view, tab } = useParams<{ view: string; tab: string }>();
  const classes = useStyles();
  const {
    params: { idLabo },
  } = useLaboratoireParams('pilotages');

  const [loadLaboratoire, loadingLaboratoire] = useGet_LaboratoireLazyQuery({ client: federation });
  const [loadPrestationType, loadingPrestationType] = useGet_Prestation_TypeLazyQuery({
    client: federation,
    variables: { sorting: { field: PrestationTypeSortFields.Code, direction: SortDirection.Desc } },
  });
  /** -------------------------------------------SUIVI REMUNERATION------------------------------------------ */
  const [loadRemunerationSuiviOperationnelle, loadingRemunerationSuiviOperationnelle] =
    useGet_Remuneration_Suivi_OperationnelLazyQuery({ client: federation });
  const [loadRemunerationSuiviOperationnelleTable, loadingRemunerationSuiviOperationnelleTable] =
    useGet_Remuneration_Suivi_OperationnelLazyQuery({ client: federation });
  const [loadRemunerationSuiviOperationnelleTables, loadingRemunerationSuiviOperationnelleTables] =
    useGet_Remuneration_Suivi_OperationnellesLazyQuery({ client: federation });

  const [loadPlanningPlanMarketings, loadingPlanningPlanMarketings] = usePlanning_MarketingsLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [
    handleRequestPlanning,
    planningPlanMarketingsData,
    planningPlanMarketingsLoading,
    handleRequestProduits,
    produitData,
    onRequestChangeStatutAction,
  ] = usePlanningPlanMarketing();

  const currentLaboPlanningProps = {
    onRequestPlanning: handleRequestPlanning,
    data: planningPlanMarketingsData,
    loading: planningPlanMarketingsLoading,
    handleRequestProduits,
    produitData,
    onRequestChangeStatutAction,
  };

  useEffect(() => {
    if (idLabo && idLabo?.length === 1) {
      loadLaboratoire({
        variables: {
          id: idLabo[0],
          idPharmacie: currentPharmacie.id,
        },
      });
    }
  }, [tab, view, idLabo && idLabo[0]]);

  useEffect(() => {
    if (view === 'PILOTAGES') {
      loadRemunerationSuiviOperationnelle({
        variables: {
          input: {
            idPartenaireTypeAssocie: idLabo && idLabo.length > 0 ? idLabo[0] : '',
            partenaireType: 'LABORATOIRE',
            dateDebut: new Date(),
          },
        },
      });
    }
    if (view === 'SUIVI_REMUNERATIONS' && /* tab === 'pilotage' */ idLabo && idLabo.length > 0) {
      loadRemunerationSuiviOperationnelleTable({
        variables: {
          input: {
            idPartenaireTypeAssocie: idLabo && idLabo.length > 0 ? idLabo[0] : '',
            partenaireType: 'LABORATOIRE',
            dateDebut: `01-11-${new Date().getFullYear()}`,
            dateFin: new Date(),
          },
        },
      });
    }
    if (view === 'SUIVI_REMUNERATIONS' && /* tab === 'pilotages' */ (!idLabo || idLabo.length > 0)) {
      loadRemunerationSuiviOperationnelleTables({
        variables: {
          input: {
            partenaireType: 'LABORATOIRE',
            dateDebut: `01-11-${new Date().getFullYear()}`,
            dateFin: moment(new Date()).endOf('month'),
            skip: skipSuiviRemuneration,
            take: takeSuiviRemuneration,
          },
        },
      });
    }
  }, [tab, view, skipSuiviRemuneration, takeSuiviRemuneration, idLabo && idLabo[0]]);

  const dataRemunerationSuiviOperationnel = loadingRemunerationSuiviOperationnelle.data?.remunerationSuiviOperationnel;
  const dataRemunerationSuiviOperationnelTable =
    loadingRemunerationSuiviOperationnelleTable.data?.remunerationSuiviOperationnel;

  const listDataSuiviRemunerations = (dataRemunerationSuiviOperationnel?.types || []).map((element) => {
    const percent =
      (element.pourcentagePrestation || 0 / 100) / 100 > 100 ? 100 : (element.pourcentagePrestation || 0 / 100) / 100;
    return options({
      title: element.prestation.code === 'COOP' ? 'Autres Rém.' : element.prestation.libelle,
      percent,
      level: 6,
      totalPaye: element.totalMontantReglementPrestation,
      totalPrevu: element.totalMontantPrevuPrestation,
      type: element.prestation.code === 'COOP' ? 'Autres Rém.' : element.prestation.libelle,
      classes,
    });
  });

  const suiviRemunerationOperationnelTable = [
    {
      nomPartenaire: dataRemunerationSuiviOperationnelTable?.types[0].nomPartenaire || '',
      BRITotal: {
        prevu:
          (dataRemunerationSuiviOperationnelTable?.types || []).find((element) => element.prestation.code === 'BRI')
            ?.totalMontantPrevuPrestation || 0,
        realise:
          (dataRemunerationSuiviOperationnelTable?.types || []).find((element) => element.prestation.code === 'BRI')
            ?.totalMontantReglementPrestation || 0,
        pourcentage:
          (dataRemunerationSuiviOperationnelTable?.types || []).find((element) => element.prestation.code === 'BRI')
            ?.pourcentagePrestation || 0,
      },
      CoopTotal: {
        prevu:
          (dataRemunerationSuiviOperationnelTable?.types || []).find((element) => element.prestation.code === 'COOP')
            ?.totalMontantPrevuPrestation || 0,
        realise:
          (dataRemunerationSuiviOperationnelTable?.types || []).find((element) => element.prestation.code === 'COOP')
            ?.totalMontantReglementPrestation || 0,
        pourcentage:
          (dataRemunerationSuiviOperationnelTable?.types || []).find((element) => element.prestation.code === 'COOP')
            ?.pourcentagePrestation || 0,
      },
      totalPrevu: dataRemunerationSuiviOperationnelTable?.totalMontantPrevues || 0,
      totalRealise: dataRemunerationSuiviOperationnelTable?.totalMontantReglements || 0,
      pourcentageTotal: dataRemunerationSuiviOperationnelTable?.pourcentageReelReglement,
    },
  ];

  const suiviRemunerationOperationnelTables =
    loadingRemunerationSuiviOperationnelleTables.data?.remunerationsSuiviOperationnelles.map((suiviRemuneration) => {
      return {
        nomPartenaire: suiviRemuneration.types[0].nomPartenaire,
        BRITotal: {
          prevu:
            (suiviRemuneration?.types || []).find((element) => element.prestation.code === 'BRI')
              ?.totalMontantPrevuPrestation || 0,
          realise:
            (suiviRemuneration?.types || []).find((element) => element.prestation.code === 'BRI')
              ?.totalMontantReglementPrestation || 0,
          pourcentage:
            (suiviRemuneration?.types || []).find((element) => element.prestation.code === 'BRI')
              ?.pourcentagePrestation || 0,
        },
        CoopTotal: {
          prevu:
            (suiviRemuneration?.types || []).find((element) => element.prestation.code === 'COOP')
              ?.totalMontantPrevuPrestation || 0,
          realise:
            (suiviRemuneration?.types || []).find((element) => element.prestation.code === 'COOP')
              ?.totalMontantReglementPrestation || 0,
          pourcentage:
            (suiviRemuneration?.types || []).find((element) => element.prestation.code === 'COOP')
              ?.pourcentagePrestation || 0,
        },
        totalPrevu: suiviRemuneration?.totalMontantPrevues || 0,
        totalRealise: suiviRemuneration?.totalMontantReglements || 0,
        pourcentageTotal: suiviRemuneration?.pourcentageReelReglement,
      };
    });

  const suiviRemunerationWithTotal = {
    nomPartenaire: 'Total',
    BRITotal: {
      prevu:
        parseFloat(
          (suiviRemunerationOperationnelTables || [])
            .map((element) => {
              return element.BRITotal.prevu;
            })
            .reduce((accumulator, current) => accumulator + current, 0)
            .toFixed(2)
        ) || (0 as any),
      realise:
        parseFloat(
          (suiviRemunerationOperationnelTables || [])
            .map((element) => {
              return element.BRITotal.realise;
            })
            .reduce((accumulator, current) => accumulator + current, 0)
            .toFixed(2)
        ) || (0 as any),
      pourcentage:
        parseFloat(
          (suiviRemunerationOperationnelTables || [])
            .map((element) => {
              return element.BRITotal.pourcentage;
            })
            .reduce((accumulator, current) => accumulator + current, 0)
            .toFixed(2)
        ) || (0 as any),
    },
    CoopTotal: {
      prevu:
        parseFloat(
          (suiviRemunerationOperationnelTables || [])
            .map((element) => {
              return element.CoopTotal.prevu;
            })
            .reduce((accumulator, current) => accumulator + current, 0)
            .toFixed(2)
        ) || 0,
      realise:
        parseFloat(
          (suiviRemunerationOperationnelTables || [])
            .map((element) => {
              return element.CoopTotal.realise;
            })
            .reduce((accumulator, current) => accumulator + current, 0)
            .toFixed(2)
        ) || 0,
      pourcentage:
        parseFloat(
          (suiviRemunerationOperationnelTables || [])
            .map((element) => {
              return element.CoopTotal.pourcentage;
            })
            .reduce((accumulator, current) => accumulator + current, 0)
            .toFixed(2)
        ) || 0,
    },
    totalPrevu:
      parseFloat(
        (suiviRemunerationOperationnelTables || [])
          .map((element) => {
            return element.totalPrevu;
          })
          .reduce((accumulator, current) => accumulator + current, 0)
          .toFixed(2)
      ) || 0,
    totalRealise:
      parseFloat(
        (suiviRemunerationOperationnelTables || [])
          .map((element) => {
            return element.totalRealise;
          })
          .reduce((accumulator, current) => accumulator + current, 0)
          .toFixed(2)
      ) || 0,
    pourcentageTotal:
      parseFloat(
        (suiviRemunerationOperationnelTables || [])
          .map((element) => {
            return element.pourcentageTotal;
          })
          .reduce((accumulator, current) => accumulator + current, 0)
          .toFixed(2)
      ) || 0,
  };

  /** ----------------------------------------- EVOLUTION REMUNERATION ----------------------------------------- */

  const array_vide = new Array(6);

  const [loadRemunerationCompareCurrentDate, loadingRemunerationCompareCurrentDate] =
    useGet_Remuneration_Suivi_OperationnelLazyQuery({ client: federation, fetchPolicy: 'cache-and-network' });

  const [loadRemunerationCompareDate, loadingRemunerationCompareDate] = useGet_Remuneration_Suivi_OperationnelLazyQuery(
    { client: federation, fetchPolicy: 'cache-and-network' }
  );

  useEffect(() => {
    if (view === 'PILOTAGES') {
      loadRemunerationCompareCurrentDate({
        variables: {
          input: {
            idPartenaireTypeAssocie: idLabo && idLabo.length > 0 ? idLabo[0] : '',
            partenaireType: 'LABORATOIRE',
            dateDebut: `${filterAnneeComparer}-01-01`,
          },
        },
      });
      loadRemunerationCompareDate({
        variables: {
          input: {
            idPartenaireTypeAssocie: idLabo && idLabo.length > 0 ? idLabo[0] : '',
            partenaireType: 'LABORATOIRE',
            dateDebut: `${filterAnneeComparer - 1}-01-01`,
          },
        },
      });
      loadPrestationType();
    }
  }, [tab, view, filterAnneeComparer, idLabo && idLabo[0]]);

  const dataRemunerationEvolutionCurrent = loadingRemunerationCompareCurrentDate.data?.remunerationSuiviOperationnel;
  const dataRemunerationEvolutionPrecedent = loadingRemunerationCompareDate.data?.remunerationSuiviOperationnel;

  const prevuRealise = (loadingPrestationType.data?.prestationTypes.nodes || []).map((type) => {
    const prevuPrecedent = (dataRemunerationEvolutionPrecedent?.types || []).map((element) => {
      if (element.prestation.id === type.id) {
        return element.totalMontantPrevuPrestation;
      }
    });
    const prevuCurrent = (dataRemunerationEvolutionCurrent?.types || []).map((element) => {
      if (element.prestation.id === type.id) {
        return element.totalMontantPrevuPrestation;
      }
    });
    return [prevuPrecedent, prevuCurrent];
  });

  const totalPrevu = {
    precedentPrevu: dataRemunerationEvolutionPrecedent?.totalMontantPrevues || 0,
    currentPrevu: dataRemunerationEvolutionCurrent?.totalMontantPrevues || 0,
  };

  const serieRealise = (loadingPrestationType.data?.prestationTypes.nodes || []).map((type) => {
    const realisePrecedent = (dataRemunerationEvolutionPrecedent?.types || []).map((element) => {
      if (element.prestation.id === type.id) {
        return element.totalMontantReglementPrestation;
      }
    });
    const realiseCurrent = (dataRemunerationEvolutionCurrent?.types || []).map((element) => {
      if (element.prestation.id === type.id) {
        return element.totalMontantReglementPrestation;
      }
    });
    return [realisePrecedent, realiseCurrent];
  });

  const totalRealise = {
    precedentRealise: dataRemunerationEvolutionPrecedent?.totalMontantReglements || 0,
    currentRealise: dataRemunerationEvolutionCurrent?.totalMontantReglements || 0,
  };

  const xaxis = Array.from(array_vide, (_, i) => (i % 2 === 0 ? filterAnneeComparer - 1 : filterAnneeComparer));

  const series = [
    {
      name: 'Prévue',
      type: 'column',
      data: prevuRealise
        .flat(4)
        .filter((el) => {
          return el != null;
        })
        .concat([totalPrevu.precedentPrevu, totalPrevu.currentPrevu]) as any,
    },
    {
      name: 'Payée',
      type: 'column',
      data: serieRealise
        .flat(4)
        .filter((el) => {
          return el != null;
        })
        .concat([totalRealise.precedentRealise, totalRealise.currentRealise]) as any,
    },
  ];

  console.log('prevuRealise', prevuRealise, serieRealise, 'series', series);

  const optionsOperationCommercialCurrent = {
    title: 'Évolution de la rémunération',
    xaxis,
    colorBar: ['#FFE43A', '#63B8DD'],
    withRefPrecedent: true,
  };

  const totalOperationCommercialCurrentProps = {
    series,
    options: optionsOperationCommercialCurrent,
  };

  const totalOperationCommercialProps = {
    totalOperationCommercialCurrentProps,
  };

  /******************************************************REMUNERATION  MENSUELLE ************************ */

  const [loadRemunerationMensuelle, loadingRemunerationMensuelle] = useGet_Remuneration_MensuelleLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    if (view === 'PILOTAGES') {
      loadRemunerationMensuelle({
        variables: {
          input: {
            idPartenaireTypeAssocie: idLabo && idLabo.length > 0 ? idLabo[0] : '',
            partenaireType: 'LABORATOIRE',
            dateDebut: moment(startDate).startOf('day'),
            dateFin: moment(endDate).endOf('day'),
          },
        },
      });
    }
  }, [tab, view, startDate, endDate, idLabo && idLabo[0]]);

  const dataRemunerationMensuelle = loadingRemunerationMensuelle.data?.remunerationMensuelle;

  const MoisAnnee = (dataRemunerationMensuelle?.data || []).map((element) => {
    return moment(element.month).format('MMMM');
  }) as any;

  const seriesChiffreParMois = [
    {
      name: 'Prévue',
      type: 'column',
      data: dataRemunerationMensuelle?.data.map((element) => {
        return element.totalRemunerationPrevu;
      }) as any,
    },
    {
      name: 'Payée',
      type: 'column',
      data: dataRemunerationMensuelle?.data.map((element) => {
        return element.totalRemunerationReglement;
      }) as any,
    },
  ];

  const optionsChiffreParMois = {
    title: 'Rémunération mensuelle',
    xaxis: MoisAnnee,
    colorBar: ['#FFE43A', '#63B8DD'],
  };

  const totalChiffreParMoisProps = {
    series: seriesChiffreParMois,
    options: optionsChiffreParMois,
  };

  const suiviRemunerationProps = {
    title: 'Historique des rémunérations',
    total: options({
      title: 'Total',
      percent:
        (dataRemunerationSuiviOperationnel?.pourcentageReelReglement || 0 / 100) / 100 > 100
          ? 100
          : (dataRemunerationSuiviOperationnel?.pourcentageReelReglement || 0 / 100) / 100,
      level: 6,
      totalPaye: dataRemunerationSuiviOperationnel?.totalMontantReglements,
      totalPrevu: dataRemunerationSuiviOperationnel?.totalMontantPrevues,
      type: 'Total',
    }),
    data: listDataSuiviRemunerations,
    loading:
      loadingRemunerationMensuelle.loading &&
      (loadingRemunerationSuiviOperationnelle.loading as any) &&
      loadingRemunerationCompareCurrentDate.loading &&
      loadingRemunerationCompareDate.loading,
    error:
      loadingRemunerationMensuelle.error &&
      (loadingRemunerationSuiviOperationnelle.error as any) &&
      loadingRemunerationCompareCurrentDate.loading &&
      loadingRemunerationCompareDate.loading,
    suiviRemunerationEntreDate:
      /* tab === 'pilotage' */ idLabo && idLabo.length > 0
        ? {
            data: suiviRemunerationOperationnelTable,
            loading: loadingRemunerationSuiviOperationnelleTable.loading,
            error: loadingRemunerationSuiviOperationnelleTable.error,
            rowsTotal: suiviRemunerationOperationnelTable?.length,
          }
        : // : tab === 'pilotages'
          {
            data: suiviRemunerationOperationnelTables?.concat([suiviRemunerationWithTotal]),
            loading: loadingRemunerationSuiviOperationnelleTables.loading,
            error: loadingRemunerationSuiviOperationnelleTables.error,
            rowsTotal: suiviRemunerationOperationnelTables?.length,
          },
    // : {
    //     data: suiviRemunerationOperationnelTables?.concat([suiviRemunerationWithTotal]),
    //     loading: loadingRemunerationSuiviOperationnelleTables.loading,
    //     error: loadingRemunerationSuiviOperationnelleTables.error,
    //     rowsTotal: suiviRemunerationOperationnelTables?.length,
    //   },
  };

  const handlePilotagePlanMarketing = (date: any) => {
    loadPlanningPlanMarketings({
      variables: {
        input: {
          dateDebut: date,
          dateFin: moment(date).add(6, 'days').toDate(),
          partenaireType: 'LABORATOIRE',
          idPharmacie: currentPharmacie?.id,
          idPartenaireTypeAssocie: idLabo && idLabo.length > 0 ? idLabo[0] : '',
        },
      },
    });
  };

  return {
    laboratoire: loadingLaboratoire.data?.laboratoire,
    suiviRemunerationProps,
    totalOperationCommercialProps,
    totalChiffreParMoisProps,

    planningPageProps:
      'pilotage' === tab ? { ...currentLaboPlanningProps, hideFirstColumn: true } : currentLaboPlanningProps,
    dataPilotagePlanMarketing: loadingPlanningPlanMarketings.data?.pRTPlanningMarketing.map((planning) => ({
      ...planning,
      prestataire: planning.partenaireTypeAssocie?.nom,
    })),
    onRequestPilotagePlanMarketing: handlePilotagePlanMarketing,
    hideFirstColumn: 'pilotage' === tab,
    planningDetail: {
      onRequestChangeStatutAction,
      onRequestProduitsAndActions: handleRequestProduits,
      produits: {
        loading: produitData.loading,
        error: produitData.error,
        data: (produitData.data?.search?.data || []).map((produit: any) => ({
          code: produit.produit?.produitCode?.code || '',
          famille: produit.produit?.famille?.libelleFamille || '',
          nom: produit.produit?.libelle || '',
        })),
      },
    },
  };
};
