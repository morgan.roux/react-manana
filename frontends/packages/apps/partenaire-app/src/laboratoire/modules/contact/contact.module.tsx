import React from 'react';
import loadable from '@loadable/component';

import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';

const ContactPage = loadable(() => import('./pages/contact/Contact'), {
  fallback: <SmallLoading />,
});
const FicheLabo = loadable(() => import('./../FicheLabo'), {
  fallback: <SmallLoading />,
});

const activationParameters = {
  groupement: '0305',
  pharmacie: '0708',
};

const contactModule: ModuleDefinition = {
  routes: [
    {
      attachTo: 'MAIN',
      path: '/laboratoires/contact',
      component: FicheLabo,
      activationParameters,
    },
  ],
};

export default contactModule;
