import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    avatar: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
    tableBox: {
      width: '100%',
      paddingLeft: 24,
      paddingRight: 24,
    },
    tableText: {
      fontWeight: 'normal',
      fontSize: 14,
    },
    contactRoot: {
      display: 'flex',
      alignItems: 'center',
      [theme.breakpoints.down('sm')]: {
        marginTop: '8px',
        marginBottom: '8px',
      },
    },
    contactItems: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginTop: '8px',
      marginBottom: '8px',
      marginLeft: '16px',
    },
    topbar: {
      '& .MuiBox-root': {
        [theme.breakpoints.down('md')]: {
          fontSize: '16px',
          fontWeight: '400',
        },
      },
    },
    bouttonAdd: {
      marginTop: '50px',
      marginBottom: '20px',
      display: 'flex',
      justifyContent: 'flex-end',
      '& .MuiSvgIcon-root': {
        fontSize: '56px',
        background: theme.palette.primary.main,
        borderRadius: '50%',
        color: '#ffffff',
        padding: '10px',
        position: 'absolute',
        zIndex: '1000',
        bottom: '90 !important',
        right: '24 !important',
      },
    },
    contactHeader: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      marginBottom: '20px',
      '& .MuiTypography-root': {
        color: '#27272F',
        opacity: '1',
        fontSize: '16px',
        fontFamily: 'Roboto,Regular',
        fontWeight: '400',
      },
    },
    nom: {
      '& .MuiTypography-root': {
        fontSize: '14px',
        color: '#424242',
        opacity: '1',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
      },
    },
    fonction: {
      '& .MuiTypography-root': {
        fontSize: '14px',
        fontFamily: 'Roboto,Regular',
        color: '#9E9E9E',
        opacity: '1',
      },
    },
  })
);
