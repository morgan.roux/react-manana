import { useApplicationContext } from '@lib/common';
import {
  DepartementSortFields,
  PrtContactFullInfoFragment,
  SortDirection,
  useCreate_Prt_ContactMutation,
  useDelete_Prt_ContactMutation,
  useDepartementsLazyQuery,
  useFonctionsLazyQuery,
  useSearch_Prt_ContactLazyQuery,
  useSend_Email_ContactLazyQuery,
  useUpdate_Prt_ContactMutation,
} from '@lib/common/src/federation';
import { Contact, Fichier, PRTContact } from '@lib/types';
import { ConfirmDeleteDialog, Table, TableChange, TopBar } from '@lib/ui-kit';
import { Avatar, Box, Divider, Fab, Typography, useTheme } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { Add } from '@material-ui/icons';
import PhoneIcon from '@material-ui/icons/Phone';
import SortIcon from '@material-ui/icons/Sort';
import React, { FC, MouseEvent, useEffect, useState } from 'react';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import { useColumns } from '../../components/contact/column';
import { useStyles as useCommonStyles } from './commonStyles';
import { useStyles } from './styles';
import ContactForm from '../../../FicheLabo/components/contact-form/ContactForm';

export interface Civilite {
  id: string;
  code: string;
  libelle: string;
}

export interface ContactRequestSaving {
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  fonction: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: Fichier;
  contact: Contact;
}

const ContactPage: FC<{}> = ({}) => {
  const classes = useStyles();
  // const searchText = useReactiveVar<string>(searchTextVar);

  const commonStyles = useCommonStyles();

  const { notify, currentPharmacie: pharmacie, federation, user } = useApplicationContext();

  const {
    params: { idLabo, idSecteur, idPrestataireService },
    redirectTo,
  } = useLaboratoireParams('contact');

  /** STATE */
  const [openEditDialog, setOpenEditDialog] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(10);
  const [contactToEdit, setContactToEdit] = useState<PrtContactFullInfoFragment>();
  const [sortTable, setSortTable] = useState<any>({
    column: '',
    direction: 'DESC',
  });

  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [photo, setPhoto] = useState<any>(null);
  const [filterPolicy, setFilterPolicy] = useState<'PHARMACIE' | 'TOUT'>('PHARMACIE');
  const open = Boolean(anchorEl);
  const [searchText, setSearchText] = useState<string>('');

  useEffect(() => {
    handleRequestSearch();
    loadFonctions();
    loadDepartements();
  }, [skip, take, searchText, sortTable, idLabo && idLabo[0], idPrestataireService]);

  useEffect(() => {
    handleRequestSearch();
  }, [filterPolicy]);

  /** HANDLERS */
  const toggleOpenEditDialog = () => {
    if (!openEditDialog) {
      // if closed, set analyse to edit undefid
      setContactToEdit(undefined);
    }
    setOpenEditDialog((prev) => !prev);
  };

  const [loadContacts, loadingContacts] = useSearch_Prt_ContactLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [loadFonctions, loadingFonctions] = useFonctionsLazyQuery();

  const [loadDepartements, loadingDepartements] = useDepartementsLazyQuery({
    variables: {
      paging: { offset: 0, limit: 1000 },
      sorting: [
        {
          field: DepartementSortFields.Code,
          direction: SortDirection.Asc,
        },
      ],
    },
  });

  const [createContact, creatingContact] = useCreate_Prt_ContactMutation({
    onCompleted: () => {
      setPhoto(null);
      notify({
        message: ' Le contact a été crée avec succés !',
        type: 'success',
      });

      loadingContacts.refetch && loadingContacts.refetch();
      setSaving(false);
      setSaved(true);
    },
    onError: () => {
      notify({
        message: 'Erreurs lors de la création du Contact.',
        type: 'error',
      });
    },
  });

  /**update plan Marketing */
  const [updateContact, updatingContact] = useUpdate_Prt_ContactMutation({
    onCompleted: () => {
      setPhoto(null);
      notify({
        message: 'Le Contact a été modifié avec succès !',
        type: 'success',
      });

      loadingContacts.refetch && loadingContacts.refetch();
      setSaving(false);
      setSaved(true);
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du contact de laboratoire',
        type: 'error',
      });
    },
  });

  const [deleteContact, deletingContact] = useDelete_Prt_ContactMutation({
    onCompleted: () => {
      notify({
        message: 'Le Contact a été supprimé avec succès !',
        type: 'success',
      });

      let timer: number; // WHY : Attendre la reindexation
      timer = setTimeout(() => {
        if (timer) {
          clearTimeout(timer);
        }

        loadingContacts.refetch && loadingContacts.refetch();
      }, 1000);
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression du contact',
        type: 'error',
      });
    },
  });

  const [sendEmailContact, sendingEmailContact] = useSend_Email_ContactLazyQuery({
    client: federation,
    fetchPolicy: 'network-only',
    onCompleted: () => {
      notify({
        type: sendingEmailContact.data?.sendEmailContact.statut as any,
        message: sendingEmailContact.data?.sendEmailContact.message,
      });
    },
    onError: (error) => {
      notify({
        type: 'error',
        message: error.graphQLErrors[0].message,
      });
    },
  });

  useEffect(() => {
    if (sendingEmailContact.loading) {
      notify({
        type: 'success',
        message: `En cours d'envoi d'email`,
      });
    }
  }, [sendingEmailContact]);

  const handleOpenAdd = () => {
    toggleOpenEditDialog();
    setContactToEdit(undefined);
  };

  const handleEdit = (contact: PrtContactFullInfoFragment | undefined) => {
    toggleOpenEditDialog();
    setContactToEdit(contact);
  };

  const handleShowMenuClick = (reunion: PrtContactFullInfoFragment, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    setContactToEdit(reunion);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const handleSearch = ({ skip, take, searchText: search }: TableChange) => {
    setSkip(skip);
    setTake(take);
    setSearchText(search);
  };

  const handleSortTable = (column: string, direction: 'DESC' | 'ASC') => {
    setSortTable({ column, direction });
  };

  const saveContact = (contactInput: any, userPhoto: any) => {
    // console.log('detection', userPhoto);
    const contact = contactInput.contact;
    setSaving(true);
    setSaved(false);
    if (contactInput.id) {
      updateContact({
        variables: {
          id: contactInput.id,
          input: {
            civilite: contactInput.civilite,
            idFonction: contactInput.idFonction,
            nom: contactInput.nom,
            prenom: contactInput.prenom,
            idDepartements: contactInput.idDepartements,
            contact: {
              adresse1: contact.adresse1,
              mailPerso: contact.adresseEmail,
              mailProf: contact.adresseEmailProf,
              telProf: contact.bureauTelephone,
              cp: contact.cp,
              telPerso: contact.domicileTelephone,
              telMobPerso: contact.mobileTelephone,
              urlFacebookPerso: contact.facebook,
              urlLinkedInPerso: contact.linkedIn,
              urlMessenger: contact.messenger,
              sitePerso: contact.pageWeb,
              ville: contact.ville,
              whatsappMobPerso: contact.whatsApp,
              urlYoutube: contact.youTube,
            },
            photo: userPhoto ? userPhoto[0] : undefined,
            idPartenaireTypeAssocie: contactInput.idPartenaireTypeAssocie,
            partenaireType: 'LABORATOIRE',
          },
        },
      });
    } else {
      createContact({
        variables: {
          input: {
            civilite: contactInput.civilite,
            idFonction: contactInput.idFonction,
            idDepartements: contactInput.idDepartements,
            nom: contactInput.nom,
            prenom: contactInput.prenom,
            contact: {
              adresse1: contact.adresse1,
              mailPerso: contact.adresseEmail,
              mailProf: contact.adresseEmailProf,
              telProf: contact.bureauTelephone,
              cp: contact.cp,
              telPerso: contact.domicileTelephone,
              telMobPerso: contact.mobileTelephone,
              urlFacebookPerso: contact.facebook,
              urlLinkedInPerso: contact.linkedIn,
              urlMessenger: contact.messenger,
              sitePerso: contact.pageWeb,
              ville: contact.ville,
              whatsappMobPerso: contact.whatsApp,
              urlYoutube: contact.youTube,
            },
            photo: userPhoto ? userPhoto[0] : undefined,
            idPartenaireTypeAssocie: contactInput.idPartenaireTypeAssocie,
            partenaireType: 'LABORATOIRE',
          },
        },
      });
    }
  };
  const handleDeleteContact = (contact: any) => {
    deleteContact({
      variables: { id: contact.id },
    });
  };

  const handleConfirmDeleteOne = (): void => {
    if (contactToEdit) {
      handleDeleteContact(contactToEdit);
      setOpenDeleteDialog(false);
    }
  };

  const handleSendEmail = (contact: PrtContactFullInfoFragment) => {
    sendEmailContact({
      variables: {
        input: {
          idContact: contact.id,
          idTypeAssocie: contact.partenaireTypeAssocie?.id as any,
          typeAssocie: contact.partenaireType,
          withNotificationLogs: false,
          withRendezVous: false,
          idUser: user.id,
        },
      },
    });
  };

  const handleRequestSearch = () => {
    const must: any[] = [
      {
        term: {
          partenaireType: idPrestataireService ? 'PRESTATAIRE_SERVICE' : 'LABORATOIRE',
        },
      },
    ];
    let should: any = undefined;
    let sort: any = undefined;

    // if (idLabo && idLabo[0]) {
    //   must.push({ term: { idPartenaireTypeAssocie: idLabo[0] } });
    // }

    if (searchText) {
      must.push({
        query_string: {
          query: `*${searchText}*`,
          fields: [],
          analyze_wildcard: true,
        },
      });
    }

    if ((idLabo && idLabo[0]) || idPrestataireService) {
      must.push({
        term: {
          idPartenaireTypeAssocie: idPrestataireService ?? ((idLabo && idLabo[0]) || ''),
        },
      });
    }

    if (!idPrestataireService && idSecteur) {
      must.push({
        term: {
          idSecteur: idSecteur,
        },
      });
    }

    if (!idPrestataireService && filterPolicy === 'PHARMACIE') {
      should = pharmacie.departement?.id
        ? [
            {
              term: {
                idPharmacie: pharmacie.id,
              },
            },
            {
              term: {
                'departements.id': pharmacie.departement?.id,
              },
            },
          ]
        : [
            {
              term: {
                idPharmacie: pharmacie.id,
              },
            },
          ];
    }

    if (sortTable.column) {
      sort = [
        {
          [sortTable.column]: {
            order: sortTable.direction.toLowerCase(),
          },
        },
      ];
    }

    loadContacts({
      variables: {
        index: ['prtcontact'],
        query: {
          query: {
            bool: {
              must,
              minimum_should_match: should ? 1 : undefined,
              // sort: [{ post_date: { order: 'asc' } }],
              should,
            },
          },
        },
      },
    });
  };

  const columns = useColumns(
    handleSendEmail,
    handleShowMenuClick,
    anchorEl,
    open,
    handleClose,
    handleEdit,
    setOpenDeleteDialog,
    contactToEdit
  );

  const error = loadingContacts.error;
  const loading = loadingContacts.loading;
  const data = (loadingContacts.data?.search?.data || []) as PRTContact[];

  const theme = useTheme();
  const match = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <>
      <Box className={classes.tableBox}>
        {match ? (
          <>
            <div>
              <div>
                <div className={classes.contactHeader}>
                  <Typography>
                    {idPrestataireService ? 'Liste des Contacts Prestataire service' : 'Liste des Contacts Labo'}
                  </Typography>
                  <SortIcon />
                </div>
                <Divider />
              </div>
              {data?.map((item) => (
                <div>
                  <div className={classes.contactRoot}>
                    <Avatar />
                    <div className={classes.contactItems}>
                      <div>
                        <div className={classes.nom}>
                          <Typography>{item.nom}</Typography>
                        </div>
                        <div className={classes.fonction}>
                          <Typography>{item.fonction?.libelle}</Typography>
                        </div>
                      </div>
                      <div>
                        <PhoneIcon />
                      </div>
                    </div>
                  </div>
                  <Divider />
                </div>
              ))}
            </div>
            {/* <div onClick={() => handleOpenAdd()} className={classes.bouttonAdd}>
              <AddIcon />
            </div> */}
            <Fab className={commonStyles.fab} onClick={handleOpenAdd} variant="round" color="secondary">
              <Add />
            </Fab>
          </>
        ) : (
          <Table
            error={error}
            loading={loading}
            search={false}
            data={data || []}
            selectable={false}
            columns={columns}
            rowsTotal={2}
            topBarComponent={(searchComponent) => (
              <TopBar
                handleOpenAdd={handleOpenAdd}
                searchComponent={
                  searchComponent
                  // <Box display="flex" justifyContent="space-between">
                  //   <FormControlLabel
                  //     //disabled={listResult.loading}
                  //     control={
                  //       <Switch
                  //         checked={filterPolicy === 'TOUT'}
                  //         onChange={(_event, checked) => setFilterPolicy(checked ? 'TOUT' : 'PHARMACIE')}
                  //       />
                  //     }
                  //     label="Tous les contacts"
                  //   />
                  //   {searchComponent}
                  // </Box>
                }
                titleButton="AJOUTER UN CONTACT"
                titleTopBar={
                  idPrestataireService ? 'Liste des Contacts Prestataire Service' : 'Liste des Contacts Labo'
                }
                withFilter={false}
              />
            )}
            onRunSearch={handleSearch}
            onSortColumn={handleSortTable}
          />
        )}
      </Box>
      {openEditDialog && ((idSecteur && idLabo && (idLabo || []).length > 0) || idPrestataireService) && (
        <ContactForm
          open={openEditDialog}
          setOpen={setOpenEditDialog}
          contactToEdit={contactToEdit}
          partenaireType={idPrestataireService ? 'PRESTATAIRE_SERVICE' : 'LABORATOIRE'}
          idPartenaireTypeAssocie={(idPrestataireService ?? (idLabo && idLabo[0])) || ''}
          mode={contactToEdit ? 'modification' : 'creation'}
          idSecteur={idSecteur}
          refetch={loadingContacts.refetch}
        />
      )}
      <ConfirmDeleteDialog
        title="Suppression de Contact"
        content="Etes-vous sûr de vouloir supprimer ce contact ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDeleteOne}
      />
    </>
  );
};

export default ContactPage;
