import React, { MouseEvent } from 'react';
import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography, Avatar } from '@material-ui/core';
import { Edit, Delete, MoreHoriz, Email } from '@material-ui/icons';
import { useStyles } from './styles';

import { Column } from '@lib/ui-kit';
import { PrtContactFullInfoFragment } from '@lib/common/src/federation';
import { useLaboratoireParams } from './../../../../hooks/useLaboratoireParams';

export const useColumns = (
  handleSendEmail: (contact: PrtContactFullInfoFragment) => void,
  handleShowMenuClick: (reunion: PrtContactFullInfoFragment, event: MouseEvent<HTMLElement>) => void,
  anchorEl: any,
  open: boolean,
  handleClose: () => void,
  handleEdit: (contact: PrtContactFullInfoFragment) => void,
  setOpenDeleteDialog: (open: boolean) => void,
  analyseToEdit?: PrtContactFullInfoFragment
) => {
  const classes = useStyles();
  const {
    params: { idPrestataireService },
  } = useLaboratoireParams();

  const columns: Column[] = [
    {
      name: 'profil',
      label: 'Profil',
      renderer: (row: PrtContactFullInfoFragment) => {
        return row?.photo?.publicUrl ? <Avatar className={classes.avatar} src={row.photo.publicUrl} /> : '-';
      },
    },
    {
      name: 'nom',
      label: 'Nom et Prénom',
      sortable: true,
      renderer: (row: PrtContactFullInfoFragment) => {
        return row ? (
          <Typography className={classes.tableText}>
            {row.nom} {row.prenom}
          </Typography>
        ) : (
          '-'
        );
      },
    },
    {
      name: 'partenaireTypeAssocie.nom',
      label: idPrestataireService ? 'Prestataire service' : 'Labo',
      sortable: true,
      renderer: (row: PrtContactFullInfoFragment) => {
        return row ? (
          <Typography className={classes.tableText}>{row.partenaireTypeAssocie?.nom || '-'}</Typography>
        ) : (
          '-'
        );
      },
    },
    {
      name: 'fonction',
      label: 'Fonctions',
      sortable: true,
      renderer: (row: PrtContactFullInfoFragment) => {
        return row?.fonction?.libelle ? (
          <Typography className={classes.tableText}>{row.fonction.libelle}</Typography>
        ) : (
          '-'
        );
      },
    },
    {
      name: 'telephone',
      label: 'Téléphone',
      renderer: (row: PrtContactFullInfoFragment) => {
        const phone = row?.contact?.telMobProf
          ? row?.contact.telMobProf
          : row?.contact?.telMobPerso
          ? row?.contact.telMobPerso
          : '-';
        return <Typography className={classes.tableText}>{phone}</Typography>;
      },
    },
    {
      name: 'email',
      label: 'Email',
      renderer: (row: PrtContactFullInfoFragment) => {
        const email = row?.contact?.mailProf
          ? row.contact.mailProf
          : row?.contact?.mailPerso
          ? row.contact.mailPerso
          : '-';
        return <Typography className={classes.tableText}>{email}</Typography>;
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: PrtContactFullInfoFragment) => {
        return (
          <div key={`row-${row.id}`}>
            <IconButton
              aria-controls="simple-menu"
              aria-haspopup="true"
              // eslint-disable-next-line react/jsx-no-bind
              onClick={handleShowMenuClick.bind(null, row)}
            >
              <MoreHoriz />
            </IconButton>
            <Menu
              id="fade-menu"
              anchorEl={anchorEl}
              keepMounted
              open={open && row.id === analyseToEdit?.id}
              onClose={handleClose}
              TransitionComponent={Fade}
            >
              {/*<MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  handleSendEmail(row);
                }}
              >
                <ListItemIcon>
                  <Email />
                </ListItemIcon>
                <Typography variant="inherit">Envoyer l'email</Typography>
              </MenuItem>*/}
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  handleEdit(row);
                }}
              >
                <ListItemIcon>
                  <Edit />
                </ListItemIcon>
                <Typography variant="inherit">Modifier</Typography>
              </MenuItem>

              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  setOpenDeleteDialog(true);
                }}
              >
                <ListItemIcon>
                  <Delete />
                </ListItemIcon>
                <Typography variant="inherit">Supprimer</Typography>
              </MenuItem>
            </Menu>
          </div>
        );
      },
    },
  ];
  return columns;
};
