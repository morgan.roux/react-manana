import React, { FC, useState, ChangeEvent, useEffect, Dispatch } from 'react';
import { CustomFormTextField, CustomModal, CustomSelect, Stepper } from '@lib/ui-kit';

import { PRTContact, Fonction } from '@lib/types';
import { Box, FormControl, Typography } from '@material-ui/core';
import { useStyles } from './styles';
import { Civilite } from '../contact/Contact';
import { Autocomplete } from '@material-ui/lab';
import { PartenaireInput, useApplicationContext } from '@lib/common';
import { PrtContactFullInfoFragment } from '@lib/common/src/federation';
import { SelectAvatar } from '../../components/select-avatar/SelectAvatar';
import CustomCheckbox from '@lib/ui-kit/src/components/atoms/CustomCheckBox/CustomCheckbox';

interface ContactFormProps {
  mode?: 'creation' | 'modification';
  open: boolean;
  errorSaving?: boolean;
  saving?: boolean;
  saved?: boolean;
  setSaved?: (newSaved: boolean) => void;
  listCivilites?: Civilite[];
  setOpen: (open: boolean) => void;
  onRequestSave?: (data: any) => void;
  contactToEdit?: PrtContactFullInfoFragment;
  photo: any;
  setPhoto: Dispatch<any>;
  fonctions: {
    loading?: boolean;
    error?: Error;
    data?: Fonction[];
  };

  departements: {
    loading?: boolean;
    error?: Error;
    data?: any[];
    defaultValue?: any[];
  };
}

interface ContactValueProps {
  adresseEmail: string;
  pageWeb?: string;
  adresseEmailProf?: string;
  bureauTelephone?: string;
  domicileTelephone?: string;
  mobileTelephone?: string;
  linkedIn?: string;
  facebook?: string;
  messenger?: string;
  whatsApp?: string;
  youTube?: string;
}

interface PrtContactValueProps {
  civilite: string;
  nom: string;
  prenom?: string;
  idFonction: string;
  adresse?: string;
  ville?: string;
  cp?: string;
  idDepartements: string[];
  partenaireTypeAssocie: any;
  prive?: boolean;
}

const ContactValue = {
  adresseEmail: '',
  pageWeb: '',
  adresseEmailProf: '',
  bureauTelephone: '',
  domicileTelephone: '',
  mobileTelephone: '',
  linkedIn: '',
  facebook: '',
  messenger: '',
  whatsApp: '',
  youTube: '',
};

const PrtContactValue = {
  civilite: '',
  idFonction: '',
  nom: '',
  prenom: '',
  adresse: '',
  ville: '',
  cp: '',
  idDepartements: [] as string[],
  partenaireTypeAssocie: {},
  prive: false,
};

const ContactForm: FC<ContactFormProps> = ({
  mode,
  open,
  saving,
  saved,
  setSaved,
  listCivilites,
  setOpen,
  onRequestSave,
  contactToEdit,
  fonctions,
  departements,
  photo,
  setPhoto,
}) => {
  const { isMobile } = useApplicationContext();
  const classes = useStyles();

  useEffect(() => {
    if (saved) {
      setOpen(false);
      if (setSaved) setSaved(false);
    }
  }, [saved]);

  const validateEmail = (mail: string): boolean => {
    if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.([a-zA-Z0-9-]+)*$/.test(mail)) {
      return true;
    }
    return false;
  };

  // STATE

  const [prtContact, setPrtContact] = useState<PrtContactValueProps>(PrtContactValue);
  const [contact, setContact] = useState<ContactValueProps>(ContactValue);
  const [emailError, setEmailError] = useState<string>('');
  const [messagerieError, setMessagerieError] = useState<string>('');
  const [activeStep, setActiveStep] = useState<number>(0);

  /** ****************************Empty field if open modal creation ou fermer la modification ****************************************** */
  useEffect(() => {
    if ((open && mode === 'creation') || (!open && mode === 'modification')) {
      setPrtContact(PrtContactValue);
      setContact(ContactValue);
      setActiveStep(0);
    }
  }, [open]);

  /** ****************************END Open modal if error saving ****************************************** */
  useEffect(() => {
    if (contactToEdit && mode === 'modification') {
      setPrtContact({
        civilite: contactToEdit.civilite,
        idFonction: contactToEdit.idFonction || '',
        idDepartements: ((contactToEdit as any).departements || []).map(({ id }: any) => id),
        nom: contactToEdit.nom,
        prenom: contactToEdit.prenom ? contactToEdit.prenom : '',
        adresse: contactToEdit.contact?.adresse1 ? contactToEdit.contact.adresse1 : '',
        ville: contactToEdit.contact?.ville ? contactToEdit.contact.ville : '',
        cp: contactToEdit.contact?.cp ? contactToEdit.contact?.cp : '',
        partenaireTypeAssocie: contactToEdit.partenaireTypeAssocie,
        prive: contactToEdit.prive || false,
      });
      setContact({
        adresseEmail: contactToEdit.contact?.mailPerso ? contactToEdit.contact.mailPerso : '',
        pageWeb: contactToEdit.contact?.sitePerso ? contactToEdit.contact.sitePerso : '',
        adresseEmailProf: contactToEdit.contact?.mailProf ? contactToEdit.contact.mailProf : '',
        bureauTelephone: contactToEdit.contact?.telProf ? contactToEdit.contact.telProf : '',
        domicileTelephone: contactToEdit.contact?.telPerso ? contactToEdit.contact.telPerso : '',
        mobileTelephone: contactToEdit.contact?.telMobPerso ? contactToEdit.contact.telMobPerso : '',
        linkedIn: contactToEdit.contact?.urlLinkedInPerso ? contactToEdit.contact.urlLinkedInPerso : '',
        facebook: contactToEdit.contact?.urlFacebookPerso ? contactToEdit.contact.urlFacebookPerso : '',
        messenger: contactToEdit.contact?.urlMessenger ? contactToEdit.contact?.urlMessenger : '',
        whatsApp: contactToEdit.contact?.whatsappMobPerso ? contactToEdit.contact.whatsappMobPerso : '',
        youTube: contactToEdit.contact?.urlYoutube ? contactToEdit.contact?.urlYoutube : '',
      });
    } else if (mode === 'creation' && departements?.defaultValue) {
      setPrtContact((prev) => ({
        ...(prev || {}),
        idDepartements: (departements.defaultValue || []).map((departement) => departement?.id),
      }));
    }
  }, [contactToEdit, open, departements?.defaultValue]);

  const handleChangePrtContact = (e: ChangeEvent<HTMLInputElement>) => {
    setPrtContact({ ...prtContact, [e.target.name]: e.target.value });
  };
  const handleChangeContact = (e: ChangeEvent<HTMLInputElement>) => {
    setContact({ ...contact, [e.target.name]: e.target.value });
  };
  const handleChangePartenaireTypeAssocie = (value: any) => {
    setPrtContact({ ...prtContact, partenaireTypeAssocie: value });
  };

  /** **************************SUBMIT DATA ****************************************************** */
  const handleSubmit = () => {
    if (!validateEmail(contact.adresseEmail)) {
      setEmailError('Adresse Email invalide');
    } else if (contact.adresseEmailProf && !validateEmail(contact.adresseEmailProf)) {
      setMessagerieError('Adresse Email invalide');
    } else {
      const data: PRTContact = {
        id: mode === 'modification' ? contactToEdit?.id : undefined,
        civilite: prtContact.civilite,
        nom: prtContact.nom,
        prenom: prtContact.prenom,
        idFonction: prtContact.idFonction,
        idDepartements: prtContact.idDepartements?.length ? prtContact.idDepartements : undefined,
        prive: prtContact.prive,
        contact: {
          ...contact,
          cp: prtContact.cp,
          ville: prtContact.ville,
          adresse1: prtContact.adresse,
        },
        idPartenaireTypeAssocie: prtContact.partenaireTypeAssocie?.id,
      };
      console.log('-----------------data---------------: ', data);
      onRequestSave && onRequestSave(data);
      setSaved && setSaved(true);
      setActiveStep(0);
    }
  };
  /** **************************END SUBMIT DATA ****************************************************** */

  /** **************************rADIO GROUP ****************************************************** */

  /** ****************************Empty field if open modal creation ou fermer la modification ****************************************** */

  const isFormValid = () => {
    return (
      prtContact.civilite &&
      prtContact.nom &&
      prtContact.idFonction &&
      prtContact.partenaireTypeAssocie &&
      contact.adresseEmail &&
      validateEmail(contact.adresseEmail) &&
      (!contact.adresseEmailProf || validateEmail(contact.adresseEmailProf))
    );
  };

  const isNextButtonDisabledStepper = () => {
    if (activeStep === 0) {
      return !(prtContact.civilite === '' || prtContact.nom === '' || prtContact.idFonction === '');
    } else {
      return isFormValid();
    }
  };

  const showDepartement = prtContact?.idFonction
    ? (fonctions.data || []).find((fonction) => fonction.id === prtContact.idFonction)?.competenceTerritoriale
    : false;
  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={mode === 'modification' ? 'Modification de Contact' : 'Ajout de Contact'}
      withBtnsActions
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      fullWidth
      fullScreen={!!isMobile}
      disabledButton={!isFormValid()}
    >
      <SelectAvatar photo={photo} setPhoto={setPhoto} />
      <Box my={2} className={classes.infoPersonnelle}>
        <Typography className={classes.textTitle}>Informations Personnelles</Typography>
        <CustomSelect
          list={listCivilites}
          index="libelle"
          listId="id"
          label="Civilité"
          name="civilite"
          value={prtContact.civilite}
          onChange={handleChangePrtContact}
          required
          className={classes.selectPadding10}
        />
        <CustomFormTextField
          type="text"
          required
          placeholder="Nom"
          label="Nom"
          name="nom"
          value={prtContact.nom}
          onChange={handleChangePrtContact}
        />
        <CustomFormTextField
          type="text"
          placeholder="Prénom"
          label="Prénom"
          name="prenom"
          value={prtContact.prenom}
          onChange={handleChangePrtContact}
        />
        <PartenaireInput
          onChange={handleChangePartenaireTypeAssocie}
          value={prtContact.partenaireTypeAssocie}
          index="laboratoire"
          label="Labo"
        />
        <CustomSelect
          list={fonctions.data || []}
          loading={fonctions.loading}
          error={fonctions.error}
          index="libelle"
          listId="id"
          label="Fonction"
          name="idFonction"
          value={prtContact.idFonction}
          onChange={handleChangePrtContact}
          required
        />
        {showDepartement && (
          <Autocomplete
            loading={departements.loading}
            multiple
            options={departements.data || []}
            getOptionLabel={(option: any) => (option?.code ? `${option.code} - ${option.nom}` : option?.nom)}
            onChange={(_event, newValue) => {
              setPrtContact((prev) => ({
                ...prev,
                idDepartements: newValue.map(({ id }) => id),
              }));
            }}
            value={(departements.data || []).filter(({ id }: any) => prtContact.idDepartements.includes(id))}
            renderInput={(params) => (
              <CustomFormTextField
                {...params}
                variant="standard"
                // label="Départements"
                placeholder="Les départements aux quels est rattaché ce contact"
              />
            )}
          />
        )}
        <FormControl className={classes.checkbox} style={{ marginBottom: '16px', marginTop: '10px', width: '100%' }}>
          <CustomCheckbox
            value={prtContact.prive}
            name="prive"
            checked={prtContact.prive}
            onClick={(e: any) => e.stopPropagation()}
            onChange={handleChangePrtContact}
            label="Contact propre à la pharmacie"
          />
        </FormControl>
      </Box>

      <Box my={2} className={classes.infoPersonnelle}>
        <Typography className={classes.textTitle}>Coordonnées</Typography>
        <CustomFormTextField
          error={emailError !== ''}
          type="text"
          required
          placeholder="Adresse de Messagerie de la Personne"
          label="Adresse de messagerie"
          name="adresseEmail"
          value={contact.adresseEmail}
          onChange={handleChangeContact}
          helperText={emailError}
        />
        <CustomFormTextField
          type="text"
          placeholder="Adresse de la Personne"
          label="Adresse"
          name="adresse"
          value={prtContact.adresse}
          onChange={handleChangePrtContact}
        />
        <Box display="flex" flexDirection="row">
          <Box mr={1} width={1} style={{ width: '60%' }}>
            <CustomFormTextField
              type="text"
              placeholder="Ville"
              label="Ville"
              name="ville"
              value={prtContact.ville}
              onChange={handleChangePrtContact}
            />
          </Box>
          <Box ml={1} width={1} style={{ width: '40%' }}>
            <CustomFormTextField
              type="text"
              placeholder="Code Postal"
              label="Code Postal"
              name="cp"
              value={prtContact.cp}
              onChange={handleChangePrtContact}
            />
          </Box>
        </Box>
      </Box>
    </CustomModal>
  );
};
export default ContactForm;
