import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    rootMobile: {
      padding: 16,
      width: '100%',
      fontFamily: 'Roboto',
      '& h3': {
        fontFamily: 'Roboto',
        fontWeight: 'normal',
        fontSize: 16,
        margin: '16px 0 24px 0',
      },
    },
    searchContainerMb: {
      width: '100%',
      marginBottom: 16,
    },
    infiniteScrollContainer: {
      height: 'calc(100vh - 370px)',
      width: '100%',
      overflowY: 'scroll',
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
    conditionItemContainer: {
      width: '100%',
      padding: '8px 0',
    },
    conditionContent: {
      display: 'flex',
      width: '100%',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    point: {
      fontSize: 20,
    },
    searchContainer: {
      display: 'flex',
      justifyContent: 'flex-end',
      flexDirection: 'row',
      width: '100%',
      margin: '8px 0 16px 0',
      alignItems: 'center',
    },
    searchContent: {
      width: 340,
    },
    conditionItemBottom: {
      fontSize: 12,
      color: theme.palette.grey[500],
      marginTop: -8,
    },
    conditionItemTop: {
      '& h3': {
        margin: 0,
        fontSize: 14,
        fontWeight: 'bold',
        fontFamily: 'Roboto',
      },
    },
    rootWeb: {
      width: '100%',
      padding: '0px 16px 0 16px',
      fontFamily: 'Roboto',
    },
    headerTable: {
      display: 'flex',
      width: '100%',
      justifyContent: 'space-between',
      flexWrap: 'wrap',
      alignItems: 'center',
      marginBottom: 16,
      '& h3': {
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 22,
      },
    },
    tableContainer: {
      width: '100%',
      marginTop: 8,
      ' & td': {
        fontSize: 14,
      },
    },
    fab: {
      position: 'absolute',
      bottom: 90,
      right: 24,
      zIndex: 1000,
    },
    filterParContainer: {
      marginRight: 16,
    },
    tableText: {
      fontWeight: 'normal',
      fontSize: 14,
    },
    joindreButton: {
      paddingTop: 8,
      paddingBottom: 8,
      paddingLeft: 24,
      paddingRight: 24,
    },
  })
);
