import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    root: {},
    tableText: {
      fontWeight: 'normal',
      fontSize: 14,
    },
    joindreButton: {
      paddingTop: 8,
      paddingBottom: 8,
      paddingLeft: 24,
      paddingRight: 24,
    },
  }),
);
