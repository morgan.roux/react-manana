import { endTime, startTime, useApplicationContext } from '@lib/common';
import {
  PrtPlanMarketingFilter,
  useGet_LaboratoireLazyQuery,
  useGet_List_Plan_Marketing_TypesQuery,
  useGet_Plan_MarketingsLazyQuery,
  useGet_Prestation_TypeLazyQuery,
  useGet_Prt_Reglement_ModeLazyQuery,
  useGet_Remuneration_AnneeLazyQuery,
} from '@lib/common/src/federation';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { useLaboratoireParams } from './../../../hooks/useLaboratoireParams';

export const useRemuneration = ({ tab }: { tab: string }) => {
  const [dateFilter, setDateFilter] = useState<Date>(new Date());
  const [searchText, setSearchText] = useState<string>('');
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(0);

  const { auth, notify, currentPharmacie: pharmacie, currentGroupement: groupement } = useApplicationContext();

  const {
    params: { idLabo, idReglementType, idReglementModeDeclaration },
    redirectTo,
  } = useLaboratoireParams('remuneration');

  const [valeurRemunerationPrevuToSave, setValeurRemunerationPrevuToSave] = useState<any>(undefined);
  const [sortTable, setSortTable] = useState<any>({
    column: null,
    direction: 'DESC',
  });
  const [openDetail, setOpenDetail] = useState<boolean>(false);
  const [remunerationDetailParams, setRemunerationParams] = useState<any>();

  //'#F46036', '#FBB104', '#FFE43A', '#C2C63F', '#00A745', '#008000'
  const backgroundRemuneration = (pourcentage: any) => {
    const tier = 100 / 6;
    const pourcentageTier = pourcentage;
    if (pourcentageTier >= 0 && pourcentage < tier) {
      return '#F46036';
    }
    if (pourcentageTier >= tier && pourcentage < tier * 2) {
      return '#FBB104';
    }
    if (pourcentageTier >= tier * 2 && pourcentage < tier * 3) {
      return '#FFE43A';
    }
    if (pourcentageTier >= tier * 3 && pourcentage < tier * 4) {
      return '#C2C63F';
    }
    if (pourcentageTier >= tier * 4 && pourcentage < tier * 5) {
      return '#00A745';
    }
    if (pourcentageTier >= tier * 5) {
      return '#008000';
    }
  };

  const variablesFilterFinal: PrtPlanMarketingFilter[] = [
    {
      partenaireType: {
        eq: 'LABORATOIRE',
      },
    },
    {
      idPharmacie: {
        eq: pharmacie.id,
      },
    },
    {
      dateStatutCloture: {
        between: {
          lower: `${dateFilter.getFullYear()}-01-01`,
          upper: `${dateFilter.getFullYear()}-12-31`,
        },
      },
    },
  ];

  const [loadLaboratoire, loadingLaboratoire] = useGet_LaboratoireLazyQuery();

  useEffect(() => {
    if (idLabo && idLabo.length > 0) {
      loadLaboratoire({
        variables: { id: idLabo[0], idPharmacie: pharmacie.id },
      });
    }
  }, [idLabo && idLabo[0]]);
  const laboratoire = loadingLaboratoire.data?.laboratoire;

  const [loadReglementMode, loadingReglementMode] = useGet_Prt_Reglement_ModeLazyQuery();

  const [loadPrestationType, loadingPrestationType] = useGet_Prestation_TypeLazyQuery();

  const [loadPlanMarketing, loadingPlanMarketing] = useGet_Plan_MarketingsLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [loadRemunerationAnnee, loadingRemunerationAnnee] = useGet_Remuneration_AnneeLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  //loaddetailRemuneration
  const [loadDetailsRemuneration, loadingDetailsRemuneration] = useGet_Plan_MarketingsLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  // Query PlanMarketing
  const loadPlanMarketingTypes = useGet_List_Plan_Marketing_TypesQuery({
    variables: {
      idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? pharmacie.id : undefined,
      filter: { idGroupement: { eq: groupement.id } },
    },
  });

  useEffect(() => {
    if (tab === 'remuneration') {
      const sortingTableFilter = sortTable?.column
        ? [
            {
              field: sortTable.column,
              direction: sortTable.direction,
            },
          ]
        : [];

      if (idLabo && idLabo.length > 0) {
        variablesFilterFinal.push({
          idPartenaireTypeAssocie: {
            eq: idLabo[0],
          },
        });
      }

      if (idReglementModeDeclaration && idReglementModeDeclaration.length > 0) {
        variablesFilterFinal.push({
          idReglementModeDeclaration: {
            in: idReglementModeDeclaration,
          },
        });
      }

      if (idReglementType && idReglementType.length > 0) {
        variablesFilterFinal.push({
          idReglementType: {
            in: idReglementType,
          },
        });
      }

      loadPlanMarketing({
        variables: {
          filter: {
            and: variablesFilterFinal,
          },
          sorting: sortingTableFilter,
        },
      });
      loadRemunerationAnnee({
        variables: {
          input: {
            idPartenaireTypeAssocie: idLabo && idLabo.length > 0 ? idLabo[0] : undefined,
            partenaireType: 'LABORATOIRE',
            date: dateFilter,
            idReglementType: idReglementType || [],
            idReglementModeDeclaration: idReglementModeDeclaration || [],
          },
        },
      });
    }
  }, [
    tab,
    dateFilter,
    skip,
    take,
    searchText,
    idLabo && idLabo[0],
    idReglementType && idReglementType[0],
    idReglementModeDeclaration && idReglementModeDeclaration[0],
  ]);

  useEffect(() => {
    if (tab === 'remuneration') {
      loadReglementMode();
      loadPrestationType();
    }
  }, [tab]);

  //Details Remuneration
  const handleShowDetail = (params: any) => {
    if (params) {
      setRemunerationParams(params);

      const dateDebut = moment
        .utc()
        .set('year', moment(dateFilter).get('year'))
        .set('month', params.month - 1)
        .set('date', 1);

      const dateFin = dateDebut.clone().endOf('month');

      if (!dateDebut.isValid() || !dateFin.isValid()) {
        return;
      }

      const dateDebutAsIsoString = startTime(dateDebut);
      const dateFinAsIsoString = endTime(dateFin);

      const idBriType = loadPlanMarketingTypes?.data?.pRTPlanMarketingTypes?.nodes.find(
        (type) => type.code === 'BRI'
      )?.id;

      console.log('params', params.column);

      const filterDetailsRemuneration: PrtPlanMarketingFilter = {
        and: [
          {
            partenaireType: {
              eq: 'LABORATOIRE',
            },
          },
          {
            // FIXME : Check backend
            idType:
              params.column === 'BRI'
                ? {
                    eq: idBriType,
                  }
                : {
                    neq: idBriType,
                  },
          },
          {
            idPharmacie: {
              eq: pharmacie.id,
            },
          },
          {
            dateDebut: {
              gte: dateDebutAsIsoString,
            },
          },
          {
            dateDebut: {
              // TODO: Date fin ???
              lte: dateFinAsIsoString,
            },
          },
        ],
      };

      if (idLabo && idLabo.length > 0) {
        filterDetailsRemuneration?.and?.push({
          idPartenaireTypeAssocie: {
            eq: idLabo[0],
          },
        });
      } else {
        filterDetailsRemuneration?.and?.push({
          idPartenaireTypeAssocie: {
            eq: params?.idPartenaire,
          },
        });
      }

      loadDetailsRemuneration({
        variables: {
          paging: {
            offset: 0,
            limit: 1000,
          },
          filter: filterDetailsRemuneration,
        },
      });
      // }
    }
  };

  const handleRequestSearch = ({ filter, searchText, skip, take, sortTable }: any) => {
    filter && setDateFilter(moment(filter[0].date).toDate());
    skip && setSkip(skip);
    take && setTake(take);
    searchText && setSearchText(searchText);
    sortTable && setSortTable(sortTable);
  };

  const handleGoBack = () => {
    redirectTo(undefined, 'plan');
  };

  const dataValueRemunerationAnnee = loadingRemunerationAnnee.data?.remunerationAnnee;

  const dataRemunerationAnnee = (dataValueRemunerationAnnee || []).map((dataLoadingRemunerationAnnee) => {
    return {
      laboratoire: dataLoadingRemunerationAnnee?.nomPartenaire || '',
      composition: dataLoadingRemunerationAnnee?.composition || '',
      total: {
        prevue: {
          value: dataLoadingRemunerationAnnee?.totalPrevues || 0,
        },
        reel: {
          value: dataLoadingRemunerationAnnee?.totalRealises || 0,
          percentage: dataLoadingRemunerationAnnee?.pourcentageRealises || 0,
          background: dataLoadingRemunerationAnnee?.totalPrevues
            ? backgroundRemuneration(dataLoadingRemunerationAnnee?.pourcentageRealises || 0)
            : '#FFF',
        },
      },
      totalVerse: {
        prevue: {
          value: '0.00',
        },
        reel: {
          value: '0.00',
        },
      },
      months: (dataLoadingRemunerationAnnee?.dataAnnee || []).map((element) => {
        const coop = (element.data.types || []).find((type) => type.prestation.code === 'COOP');
        const bri = (element.data.types || []).find((type) => type.prestation.code === 'BRI');

        return {
          idPartenaire: dataLoadingRemunerationAnnee?.idPartenaire,
          month: element.indexMois,
          prevue: {
            coop: {
              value: coop?.totalMontantPrevuPrestation || '',
              background: '',
              tooltip: '',
            },
            bri: {
              value: bri?.totalMontantPrevuPrestation || '',
              background: '',
              tooltip: '',
            },
            total: {
              value: element.data.totalMontantPrevues || '',
            },
          },
          reel: {
            coop: {
              value: coop?.totalMontantReglementPrestation || '',
              percentage: coop?.pourcentagePrestation || '',
              background: coop?.totalMontantPrevuPrestation
                ? backgroundRemuneration(coop?.pourcentagePrestation || 0)
                : '#FFF',
            },
            bri: {
              value: bri?.totalMontantReglementPrestation || '',
              percentage: bri?.pourcentagePrestation || '',
              background: bri?.totalMontantPrevuPrestation
                ? backgroundRemuneration(bri?.pourcentagePrestation || 0)
                : '#FFF',
            },
            total: {
              value: element.data.totalMontantReglements || '',
              percentage: element.data.pourcentageReelReglement || '',
              background: element.data.totalMontantReglements
                ? backgroundRemuneration(element.data.pourcentageReelReglement || 0)
                : '#FFF',
            },
          },
        };
      }),
    };
  });

  const dataRemuneration = loadingPlanMarketing.data?.pRTPlanMarketings.nodes.map((element) => {
    return {
      ...element,
    };
  });

  const detail = loadingDetailsRemuneration.data?.pRTPlanMarketings.nodes || [];

  const remunerationPageProps = {
    listPrestationType: {
      data: loadingPrestationType.data?.prestationTypes.nodes,
      error: loadingPrestationType.error,
      loading: loadingPrestationType.loading,
    },
    listTypeRemuneration: loadingPrestationType.data?.prestationTypes.nodes,
    listModePaiement: loadingReglementMode.data?.pRTReglementModes.nodes,
    listReglementMode: {
      data: loadingReglementMode.data?.pRTReglementModes.nodes,
      error: loadingReglementMode.error,
      loading: loadingReglementMode.loading,
    },
    remunerations: {
      data: dataRemuneration as any,
      error: loadingPlanMarketing.error as any,
      loading: loadingPlanMarketing.loading,
    },
    // onRequestSaving: handleRequestSave,
    // onRequestDelete: handleRequestDeleteRemunerationReglement,
    onRequestSearch: handleRequestSearch,
    // onRequestEdit: handleRequestEdit,
    // onRequestFilterRemuneration: handleRequestFilterRemuneration,
    // remunerationPrevue: remunerationPrevueExist,
    // remunerationToEdit: { ...remunerationToEdit, file: remunerationToEdit?.fichiers },
    // saving: savingRemuneration,
    // saved: savedRemuneration,
    // setSaved: setSavedRemuneration,
    laboratoire,
    // collone: loadingPrestationType.data?.prestationTypes.nodes,
    // savingInputRemuneration: {
    //   loading: valeurRemunerationPrevuToSave ? creatingRemuneration.loading : false,
    //   onRequestSaveInput: handleRequestSavingInputRemuneration,
    // },
    remunerationsTables: {
      data: dataRemunerationAnnee,
      error: loadingRemunerationAnnee?.error as any,
      loading: loadingRemunerationAnnee?.loading,
    },
    onRequestGoBack: handleGoBack,
    onRequestShowDetails: handleShowDetail,
    open: detail ? openDetail : false,
    setOpen: detail ? setOpenDetail : undefined,
    details: {
      ...(remunerationDetailParams || {}),
      error: loadingDetailsRemuneration.error,
      planMarketings: loadingDetailsRemuneration.data?.pRTPlanMarketings.nodes || [],
      loading: loadingDetailsRemuneration.loading,
    },
  };

  return remunerationPageProps;
};
