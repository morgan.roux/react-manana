import { getMonthNumber } from '@lib/ui-kit/src/services/Helpers';
import { Grid, List, ListItem, ListItemText } from '@material-ui/core';
import React, { Dispatch, FC } from 'react';
import { useStyles } from './style';
interface MonthProps {
  month?: any;
  coopPrevue?: any;
  coopRealise?: any;
  briPrevue?: any;
  briRealise?: any;
  totalPrevue?: any;
  totalRealise?: any;
  coopPrecent?: any;
  briPrecent?: any;
  totalPrecent?: any;
  onRequestShowDetails?: (params?: any) => void;
}
export const Month: FC<MonthProps & { setOpen?: Dispatch<boolean>; open?: boolean }> = ({
  month,
  coopPrevue,
  coopRealise,
  briPrevue,
  briRealise,
  totalPrevue,
  totalRealise,
  coopPrecent,
  briPrecent,
  totalPrecent,
  onRequestShowDetails,
  setOpen,
}) => {
  const classes = useStyles();
  const handleClick = (column: 'BRI' | 'COOP', type: 'PREVUE' | 'REALISEE') => {
    onRequestShowDetails && onRequestShowDetails({ month: getMonthNumber(month), column, type });
    setOpen && setOpen(true);

    console.log('******************* onRequestShowDetails *******************************', {
      month: getMonthNumber(month),
      column,
      type,
    });
  };
  return (
    <Grid container>
      <Grid item xs={3} className={classes.gridBody}>
        <div className={classes.month}>{month}</div>
      </Grid>
      <Grid item xs={3} className={classes.gridBody}>
        <List className={classes.coopBritotal}>
          <ListItem>
            <ListItemText>Coop</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>BRI</ListItemText>
          </ListItem>
          <ListItem>
            <ListItemText>Total</ListItemText>
          </ListItem>
        </List>
      </Grid>
      <Grid item xs={3} className={classes.gridBody}>
        <List className={classes.chiffre}>
          <ListItem
            style={{ background: coopPrevue?.background || '#FFF' }}
            onClick={() => handleClick('COOP', 'PREVUE')}
          >
            <ListItemText
              style={{ cursor: 'pointer' }}
              onClick={() => handleClick('COOP', 'PREVUE')}
            >
              {parseFloat(coopPrevue?.value || '0').toFixed(2)} €
            </ListItemText>
          </ListItem>
          <ListItem
            style={{ background: briPrevue?.background || '#FFF' }}
            onClick={() => handleClick('BRI', 'PREVUE')}
          >
            <ListItemText
              style={{ cursor: 'pointer' }}
              onClick={() => handleClick('BRI', 'PREVUE')}
            >
              {parseFloat(briPrevue?.value || '0').toFixed(2)} €
            </ListItemText>
          </ListItem>
          <ListItem style={{ background: totalPrevue?.background || '#FFF' }}>
            <ListItemText className={classes.totalPrevueBold}>
              {parseFloat(totalPrevue?.value || '0').toFixed(2)} €
            </ListItemText>
          </ListItem>
        </List>
      </Grid>
      <Grid item xs={3} className={classes.gridBody}>
        <List className={classes.chiffre}>
          <ListItem
            style={{ background: coopRealise?.background || '#FFF' }}
            onClick={() => handleClick('COOP', 'REALISEE')}
          >
            <ListItemText style={{ cursor: 'pointer' }}>
              {parseFloat(coopRealise?.value || '0').toFixed(2)} €{' '}
              <span className={classes.spanPercent}>{coopPrecent || '0'} %</span>
            </ListItemText>
          </ListItem>
          <ListItem
            style={{ background: briRealise?.background || '#FFF' }}
            onClick={() => handleClick('BRI', 'REALISEE')}
          >
            <ListItemText style={{ cursor: 'pointer' }}>
              {parseFloat(briRealise?.value || '0').toFixed(2)} €{' '}
              <span className={classes.spanPercent}>{briPrecent || '0'} %</span>
            </ListItemText>
          </ListItem>
          <ListItem style={{ background: totalRealise?.background || '#FFF' }}>
            <ListItemText>
              {parseFloat(totalRealise?.value || '0').toFixed(2)} €{' '}
              <span className={classes.spanPercent}>{totalPrecent || '0'} %</span>
            </ListItemText>
          </ListItem>
        </List>
      </Grid>
    </Grid>
  );
};
