import { Column } from '@lib/ui-kit';
import moment from 'moment';

export const useReglementColumn = () => {
  const reglementColumns: Column[] = [
    {
      name: 'produit.cloture.mode',
      label: 'Mode',
      renderer: (details: any) => {
        console.log('DETAILS REGLEMENT', details);
        return details?.modeReglement?.libelle || '-';
      },
    },
    {
      name: 'produit.cloture.date',
      label: 'Date',
      renderer: (details: any) => {
        return details.dateStatutCloture
          ? moment.utc(details?.dateStatutCloture).format('DD-MM-YYYY')
          : '-';
      },
    },
    {
      name: 'produit.cloture.montant',
      label: 'Montant',
      renderer: (details: any) => {
        return `${details?.montantStatutCloture || 0} €`;
      },
    },
  ];

  return reglementColumns;
};
