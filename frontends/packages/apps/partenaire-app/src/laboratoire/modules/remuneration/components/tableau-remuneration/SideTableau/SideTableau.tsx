import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Box,
} from '@material-ui/core';
import React, { FC } from 'react';
import {
  ModePaiementRemunerationProps,
  RemunerationRequestSavingProps,
  TypeRemunerationFinalProps,
} from '../../form-remuneration/interface';
import { CollonneProps } from '../TableChart/HeadTableChart/HeaderTableChart';
import { TableChart } from '../TableChart/TableChart';

import { PRTRemuneration, PRTRemunerationReglement } from '../TableChart/interface';

import { useStyles } from './styles';

import { Laboratoire } from '../../../../../types';

export interface SideTableauProps {
  remunerations?: PRTRemunerationReglement[];
  collone?: CollonneProps[];
  listModePaiement?: ModePaiementRemunerationProps[];
  listTypeRemuneration?: TypeRemunerationFinalProps[];
  onRequestSaving?: (data: RemunerationRequestSavingProps) => void;
  laboratoire?: Laboratoire;
  setMode?: (mode: 'creation' | 'modification') => void;
  setOpen?: (open: boolean) => void;
  setTypeMode?: (type: 'prevu' | 'reel') => void;
  onRequestEdit?: (edit: PRTRemunerationReglement) => void;
  onRequestDelete?: (data: PRTRemunerationReglement | PRTRemuneration) => void;
  setOpenDialog?: (open: boolean) => void;
}

// interface totalProps {
//   totalVerse: number;
//   rfa: number;
//   totalglobal: number;
// }

// eslint-disable-next-line @typescript-eslint/no-empty-interface

export const SideTableau: FC<SideTableauProps> = ({
  remunerations,
  collone,
  listTypeRemuneration,
  onRequestSaving,
  laboratoire,
  setMode,
  setOpen,
  setTypeMode,
  onRequestDelete,
  onRequestEdit,
  setOpenDialog,
}) => {
  const classes = useStyles();
  console.log(onRequestSaving);
  // const handleSave = (data: RemunerationRequestSavingProps) => {
  //   onRequestSaving(data);
  // };
  return (
    <>
      <TableContainer className={classes.root}>
        <Box display="flex" flexDirection="row" justifyContent="flex-start">
          <Box width="25%" className={classes.left}>
            <Table style={{ border: 2 }}>
              <TableHead>
                <TableRow>
                  <TableCell>Laboratoires</TableCell>
                  <TableCell>Compositions de la prestation</TableCell>
                  <TableCell rowSpan={2}>Rémunération prévu/réelle</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {/* // eslint-disable-next-line react/jsx-key */}
                <TableRow>
                  <TableCell>{laboratoire?.nom}</TableCell>
                  <TableCell>condition commerciale</TableCell>

                  <TableRow>
                    <TableCell>Rémunération prévue</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Rémunération réelle</TableCell>
                  </TableRow>
                </TableRow>
              </TableBody>
            </Table>
          </Box>
          <Box bgcolor="" width="55%" position="relative" className={classes.chart} ml={0.5}>
            <TableChart
              collone={collone}
              remunerations={remunerations}
              listTypeRemuneration={listTypeRemuneration}
              setMode={setMode}
              setOpen={setOpen}
              setTypeMode={setTypeMode}
              onRequestDelete={onRequestDelete}
              onRequestEdit={onRequestEdit}
              setOpenDialog={setOpenDialog}
            />
          </Box>
          <Box bgcolor="" width="20%" className={classes.right}>
            <Table style={{ border: 2 }}>
              <TableHead>
                <TableRow>
                  <TableCell style={{ fontWeight: 'bold' }}>Total versé</TableCell>
                  <TableCell style={{ fontWeight: 'bold' }}>Total</TableCell>
                </TableRow>
              </TableHead>
              {/* {(remunerations || []).map(_item => ( */}
              {/* // eslint-disable-next-line react/jsx-key */}
              <TableBody>
                <TableRow>
                  <TableCell>0.00€</TableCell>
                  <TableCell>total montant prévu</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell style={{ fontWeight: 'bold' }}>total versé</TableCell>
                  <TableCell style={{ fontWeight: 'bold' }}>total</TableCell>
                </TableRow>
              </TableBody>
              {/* ))} */}
            </Table>
          </Box>
        </Box>
      </TableContainer>
    </>
  );
};
