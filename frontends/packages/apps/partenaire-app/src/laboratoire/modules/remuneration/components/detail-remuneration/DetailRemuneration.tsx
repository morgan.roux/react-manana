import React, { FC } from 'react';
import { Box, Typography, Grid } from '@material-ui/core';
import { CustomModal, Table } from '@lib/ui-kit';
import { useProduitColumn } from './ProduitColumn';
import { useReglementColumn } from './ReglementColumn';
//import moment from 'moment';
import { ProduitAssocieMobile, ReglementMobile } from './utilsMobile';
import { makeStyles, createStyles } from '@material-ui/styles';
import { PRTPlanMarketing } from '@lib/types';
import { useApplicationContext } from '@lib/common';
import { computeMontantRemunerationRealisee } from '../../../trade-marketing/components/plan-form/util';
import { useCatalogueOperationMarketing } from '../../../../hooks/useCatalogueOperationMarketing';

const useStyles = makeStyles(() =>
  createStyles({
    noBold: {
      '& *': {
        fontWeight: 'normal !important',
      },
    },
  })
);
interface DetailRemunerationProps {
  open?: boolean;
  setOpen?: (open: boolean) => void;
  details: {
    column: 'BRI' | 'AUTRES';
    type: 'PREVUE' | 'REALISEE';
    error?: Error;
    loading?: boolean;
    planMarketings?: PRTPlanMarketing[];
  };
}

const DetailRemuneration: FC<DetailRemunerationProps> = ({ open, setOpen, details }) => {
  const produitColumns = useProduitColumn();
  const reglementColumns = useReglementColumn();
  const classes = useStyles();

  const { isMobile } = useApplicationContext();
  const activateCatalogueProduits = useCatalogueOperationMarketing();

  const typeAsText = details.type === 'PREVUE' ? 'prévue' : 'payée';
  const title = `Détail ${details.column} ${typeAsText}`;

  const planMarketings = details?.planMarketings || [];

  const cumulProduits: any[] = planMarketings.reduce((listProduits, planMarketing) => {
    return [
      ...listProduits,
      ...(planMarketing?.produits || []).map((produitCanal: any) => ({
        produit: {
          ...produitCanal.produit,
          quantiteRealise: produitCanal.produit.quantiteRealise || produitCanal.produit.quantitePrevue,
          totalRealise: produitCanal.produit.quantiteRealise
            ? computeMontantRemunerationRealisee(
                planMarketing.typeRemuneration || 'SANS_REMUNERATION',
                planMarketing.remise || 0,
                produitCanal
              )
            : produitCanal.produit.remisePrevue,
        },
      })),
    ];
  }, [] as any[]);

  console.log('CUMULPRODUIT', cumulProduits);

  const cumulPlanMarketingRegles = planMarketings.filter((pm: any) => pm?.modeReglement);

  const montantTotal = planMarketings.reduce((total, planMarketing) => {
    return (
      total +
      (details.type === 'PREVUE' ? planMarketing?.montant ?? 0 : (planMarketing as any)?.montantStatutCloture ?? 0)
    );
  }, 0);
  /*
  const date =
    planMarketings.length > 0
      ? details.type === 'PREVUE'
        ? planMarketings[0].dateDebut
        : (planMarketings[0] as any).dateStatutRealise
      : undefined;

  console.log('****************************cumulProduits', cumulProduits);*/

  const showProduits = activateCatalogueProduits && cumulProduits.length > 0;
  return (
    <CustomModal
      open={open || false}
      setOpen={setOpen || ((_open: boolean) => {})}
      title={title}
      withBtnsActions={false}
      closeIcon
      headerWithBgColor
      fullScreen={!!isMobile}
    >
      <Box display="flex" flexDirection="column" minWidth={isMobile ? '100%' : '860px'}>
        {/*<Box mt={1} display="flex" justifyContent="row">
          <Typography>Date {typeAsText} : </Typography>
          <Typography style={{ fontWeight: 'bold', marginLeft: '57px' }}>
            &nbsp; {date ? moment(date).format('DD-MM-YYYY') : '-'}
          </Typography>
        </Box>*/}
        <Box mt={1} display="flex" justifyContent="row">
          <Typography>Rémunération {details.type === 'PREVUE' ? 'prévue' : 'payée'} : </Typography>
          <Typography style={{ fontWeight: 'bold' }}>&nbsp; {(montantTotal || 0).toFixed(2)} €</Typography>
        </Box>
        {showProduits && (
          <Box mt={2} style={{ fontWeight: 'bold' }}>
            Produits associés
          </Box>
        )}
        {isMobile ? (
          <Grid container spacing={3} style={{ marginTop: 10 }}>
            {cumulProduits.map((item: any, index: number) => {
              return <ProduitAssocieMobile key={index} data={item} />;
            })}
          </Grid>
        ) : showProduits ? (
          <Box mt={1}>
            <Table
              error={details.error as Error | undefined}
              loading={details.loading}
              search={false}
              data={cumulProduits}
              tableBodyClassName={classes.noBold}
              columns={produitColumns}
              notablePagination={true}
            />
          </Box>
        ) : null}

        {details.type === 'REALISEE' && (
          <>
            <Box mt={2} style={{ fontWeight: 'bold' }}>
              Reglement
            </Box>
            {isMobile ? (
              <Grid container spacing={3} style={{ marginTop: 10 }}>
                {cumulPlanMarketingRegles.map((item: any, index: number) => {
                  return <ReglementMobile key={index} data={item} />;
                })}
              </Grid>
            ) : (
              <Box mt={1}>
                <Table
                  error={details?.error as Error | undefined}
                  loading={details?.loading}
                  search={false}
                  data={cumulPlanMarketingRegles}
                  columns={reglementColumns}
                  notablePagination={true}
                  tableBodyClassName={classes.noBold}
                />
              </Box>
            )}
          </>
        )}
      </Box>
    </CustomModal>
  );
};
export default DetailRemuneration;
