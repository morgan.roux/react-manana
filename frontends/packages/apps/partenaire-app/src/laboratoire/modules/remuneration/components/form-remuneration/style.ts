import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((_theme: Theme) =>
  createStyles({
    root: {
      fontFamily: 'Roboto',
      display: 'flex',
      flexDirection: 'column',
      width: 800,
      margin: 'auto',
    },
    rangeHoriz: {
      display: 'flex',
      flexDirection: 'row',
    },
    tableAddRemuneration: {
      ' & .MuiTablePagination-root': {
        display: 'none !important',
      },
    },
    contentTotal: {
      justifyContent: 'flex-end',
      alignItems: 'flex-end',
      background: '#EFEFEF',
      padding: '8px 16px',
      paddingRight: '18%',
    },
    contentButton: {
      marginLeft: 'auto',
      marginRight: 0,
    },
    contentButtonAction: {
      minHeight: 56,
    },
    label: {
      padding: '16px 0px',
    },
    heightSelect: {
      height: 56,
    },
    fullWidth: {
      width: '100%',
    },
  }),
);
