import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      borderTopWidth: 1,
      borderColor: 'black',
      tableLayout: 'fixed',
      '& .MuiTableCell-body': {
        borderRight: '1px solid #7777',
        borderLeft: '1px solid #7777',
        borderBottom: '1px solid #7777',
      },
      '& .MuiTableCell-root.MuiTableCell-head': {
        borderRight: '1px solid #7777',
        borderLeft: '1px solid #7777',
        borderTop: '1px solid #7777',
        borderBottom: '1px solid #7777',
      },
      '& .MuiTableCell-head': {
        frontWeight: 'bold',
      },
      '& .k-widget k-toolbar.k-scheduler-toolbar': {
        display: 'none !important',
      },
      '& .MuiBox-root.MuiBox-root-23': {
        height: '20px',
      },
      '& .MuiTableHead-root': {
        width: '50px !important',
      },

      '& .MuiBox-root MuiBox-root-33.makeStyles-chart-29': {
        marginLeft: '0px !important',
      },
    },
    right: {
      '& .MuiTableRow-root.MuiTableRow-head': {
        height: '114px',
        border: '3px solid rgba(0,0,0,0.08)',
      },
      '& .MuiTableCell-root.MuiTableCell-body': {
        height: '50px',
        border: '3px solid rgba(0,0,0,0.08)',
      },
      marginRight: '0px',
    },
    chart: {
      '& .MuiTableRow-root.MuiTableRow-head': {
        border: '3px solid rgba(0,0,0,0.08)',
      },
      '& .MuiTableCell-root.MuiTableCell-body': {
        border: '3px solid rgba(0,0,0,0.08)',
      },
      ' & tbody .MuiTableRow-root': {
        height: 75,
      },
      // margineft: '10%',
    },
    left: {
      '& .MuiTableRow-root.MuiTableRow-head': {
        height: '114px',
        // border: '3px solid rgba(0,0,0,0.08)',
      },
      '& .MuiTableCell-root.MuiTableCell-body': {
        height: '50px',
        border: '3px solid rgba(0,0,0,0.08)',
      },
    },
  }),
);
