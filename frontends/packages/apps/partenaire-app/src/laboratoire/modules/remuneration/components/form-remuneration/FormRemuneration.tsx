import React, { ChangeEvent, FC, useState, useEffect } from 'react';
import { Box, InputAdornment } from '@material-ui/core';
import { CustomDatePicker, CustomEditorText, CustomFormTextField, CustomModal, CustomSelect } from '@lib/ui-kit';
import { FormRemunerationProps, RemunerationRequestSavingProps } from './interface';
import { useStyles } from './style';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { Dropzone, useApplicationContext } from '@lib/common';
import { PRTRemuneration } from '../tableau-remuneration/TableChart/interface';

const FormRemuneration: FC<FormRemunerationProps> = ({
  listModePaiement,
  listTypeRemuneration,
  onRequestSaving,
  mode,
  remunerationToEdit,
  saving,
  open,
  setOpen,
  saved,
  setSaved,
  onRequestFilterRemuneration,
  remunerationPrevue,
}) => {
  const classes = useStyles();
  const { isMobile } = useApplicationContext();

  const [dateEcheance, setDateEcheance] = useState<any>(new Date());
  const [type, setType] = useState<string>('');
  const [modePaiement, setModePaiement] = useState<string>('');
  const [date, setDate] = useState<any>(new Date());
  const [montant, setMontant] = useState<number>(0);
  const [description, setDescription] = useState<string>('');
  const [selectedFiles, setSelectedFiles] = useState<File[]>();
  const [estimation, setEstimation] = useState<number>(0);
  const [remunerationCurrent, setRemunerationCurrent] = useState<PRTRemuneration>();
  const [filterRemuneration, setFilterRemuneration] = useState<any>({
    type: '',
    date: new Date(),
  });
  console.log(date);

  useEffect(() => {
    onRequestFilterRemuneration && onRequestFilterRemuneration(filterRemuneration);
  }, [filterRemuneration]);

  useEffect(() => {
    if (remunerationToEdit && mode === 'modification') {
      setDateEcheance(remunerationToEdit.remuneration.dateEcheance);
      setType(remunerationToEdit.prestationType.code);
      setModePaiement(remunerationToEdit.reglementMode.code);
      setDate(remunerationToEdit.dateReglement);
      setMontant(remunerationToEdit.montant);
      setDescription(remunerationToEdit.description || '');
      setEstimation(remunerationToEdit.remuneration.montantPrevu);
      setSelectedFiles(remunerationToEdit.file);
      setRemunerationCurrent(remunerationToEdit.remuneration);
    }
  }, [remunerationToEdit, open]);

  useEffect(() => {
    if ((open && mode === 'creation') || (!open && mode === 'modification')) {
      setDateEcheance(new Date());
      setType('');
      setModePaiement('');
      setDate(new Date());
      setMontant(0);
      setDescription('');
      setSelectedFiles([]);
      setEstimation(0);
    }
  }, [open]);

  useEffect(() => {
    if (remunerationPrevue) {
      setEstimation(remunerationPrevue.montantPrevu);
    } else {
      setEstimation(0);
    }
    setRemunerationCurrent(remunerationPrevue);
  }, [remunerationPrevue]);

  useEffect(() => {
    if (saved) {
      setOpen && setOpen(false);
      if (setSaved) setSaved(false);
    }
  }, [saved]);

  const handleChangeDateEcheance = (value: MaterialUiPickersDate) => {
    setFilterRemuneration({ ...filterRemuneration, date: value });
    setDateEcheance(value);
  };

  const handleChangeDescription = (text: string) => {
    setDescription(text);
  };

  const handleChangeTypeRemuneration = (e: ChangeEvent<HTMLInputElement>) => {
    const typeSelectionne = (listTypeRemuneration || []).find((type) => type.code === e.target.value);
    setFilterRemuneration({ ...filterRemuneration, type: typeSelectionne });
    setType(typeSelectionne?.code || '');
  };

  const handleChangeMontantRemuneration = (e: ChangeEvent<HTMLInputElement>) => {
    setMontant(parseFloat(e.target.value));
  };

  const handleChangeValeurEstime = (e: ChangeEvent<HTMLInputElement>) => {
    setEstimation(parseFloat(e.target.value));
  };

  const handleSubmit = () => {
    const data: RemunerationRequestSavingProps = {
      id: remunerationCurrent?.id,
      dateEcheance,
      estimation,
      prestationType: remunerationCurrent ? remunerationCurrent?.prestationType : undefined,
      remunerationFinal: {
        id: remunerationToEdit?.id,
        type: (listTypeRemuneration || []).find((element) => element.code === type),
        date: dateEcheance,
        modePaiement: (listModePaiement || []).find((element) => element.code === modePaiement),
        montant,
        description,
        fichiers: selectedFiles,
      },
      mode,
    };
    onRequestSaving && onRequestSaving(data);
    setSaved && setSaved(true);
  };

  const isFormValid = () => {
    return montant >= 0 && dateEcheance && estimation >= 0 && type;
  };

  return (
    <CustomModal
      open={open || false}
      setOpen={setOpen || ((_open: boolean) => {})}
      title={mode === 'creation' ? 'Nouvelle rémunération' : 'Modification de la rémunération réalisée'}
      withBtnsActions
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      disabledButton={!isFormValid() || saving}
      onClickConfirm={handleSubmit}
      fullScreen={!!isMobile}
    >
      <Box className={classes.root}>
        <Box py={1}>
          {/* <Typography className={classes.label}>Montant éstimé</Typography> */}
          <Box className={classes.rangeHoriz}>
            {/* <Box pr={1} className={classes.fullWidth}>
                <CustomFormTextField
                  label="Estimation de la rémunération"
                  value={estimation}
                  type="number"
                  onChange={handleChangeEstimation}
                  InputProps={{
                    endAdornment: <InputAdornment position="end">‎€</InputAdornment>,
                  }}
                  inputProps={{ min: 0, step: 1, style: { textAlign: 'right' } }}
                  name="estimation"
                />
              </Box> */}
            <Box pl={1} className={classes.fullWidth}>
              <CustomDatePicker
                label="Date de règlement"
                value={dateEcheance}
                required
                onChange={handleChangeDateEcheance}
                name="echeance"
                // disabled={mode === 'modification'}
              />
            </Box>
          </Box>
        </Box>

        <Box py={1} pl={1} className={classes.tableAddRemuneration}>
          {/* <Typography className={classes.label}>Rémunération finale</Typography> */}
          <Box className={classes.rangeHoriz}>
            <Box pr={1} className={classes.fullWidth}>
              <CustomSelect
                list={listTypeRemuneration}
                index="libelle"
                listId="code"
                label="Type de rémunération"
                value={type}
                onChange={handleChangeTypeRemuneration}
                // className={classes.heightSelect}
                name="type"
                style={{ height: 56 }}
              />
            </Box>
            <Box className={classes.fullWidth}>
              <CustomFormTextField
                label="Rém. prévue"
                onChange={handleChangeValeurEstime}
                value={estimation}
                type="number"
                InputProps={{
                  endAdornment: <InputAdornment position="end">‎€</InputAdornment>,
                }}
                inputProps={{ min: 0, step: 1, style: { textAlign: 'right' } }}
                name="valeurEstime"
                // disabled={mode === 'modification'}
              />
            </Box>
          </Box>
          <Box className={classes.rangeHoriz}>
            <Box className={classes.fullWidth}>
              <CustomFormTextField
                label="Rém. réalisée"
                onChange={handleChangeMontantRemuneration}
                value={montant}
                type="number"
                InputProps={{
                  endAdornment: <InputAdornment position="end">‎€</InputAdornment>,
                }}
                inputProps={{ min: 0, step: 1, style: { textAlign: 'right' } }}
                name="montant"
              />
            </Box>
          </Box>
          <Box py={1}>
            <CustomEditorText value={description} placeholder="Description" onChange={handleChangeDescription} />
          </Box>
          <Box py={1}>
            <Dropzone selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles} multiple />
          </Box>
        </Box>
      </Box>
    </CustomModal>
  );
};

export default FormRemuneration;
