import React, { FC } from 'react';
import { Laboratoire } from '../../../../types';
import {
  ModePaiementRemunerationProps,
  RemunerationRequestSavingProps,
  TypeRemunerationFinalProps,
} from '../form-remuneration/interface';
import { SideTableau } from './SideTableau/SideTableau';
import { CollonneProps } from './TableChart/HeadTableChart/HeaderTableChart';
import { PRTRemuneration, PRTRemunerationReglement } from './TableChart/interface';

export interface TableauRemunerationProps {
  remunerations?: PRTRemunerationReglement[];
  collone?: CollonneProps[];
  listModePaiement?: ModePaiementRemunerationProps[];
  listTypeRemuneration?: TypeRemunerationFinalProps[];
  remunerationToEdit?: PRTRemunerationReglement;
  onRequestSaving?: (data: RemunerationRequestSavingProps) => void;
  // saving: boolean;
  laboratoire?: Laboratoire;
  setMode?: (mode: 'creation' | 'modification') => void;
  setOpen?: (open: boolean) => void;
  setTypeMode?: (type: 'prevu' | 'reel') => void;
  onRequestEdit?: (edit: PRTRemunerationReglement) => void;
  onRequestDelete?: (edit: PRTRemunerationReglement | PRTRemuneration) => void;
  setOpenDialog?: (open: boolean) => void;
}

export const TableauRemuneration: FC<TableauRemunerationProps> = ({
  remunerations,
  collone,
  listModePaiement,
  listTypeRemuneration,
  onRequestSaving,
  laboratoire,
  setMode,
  setOpen,
  setTypeMode,
  onRequestEdit,
  onRequestDelete,
  setOpenDialog,
}) => {
  return (
    <>
      <SideTableau
        remunerations={remunerations}
        collone={collone}
        listModePaiement={listModePaiement}
        listTypeRemuneration={listTypeRemuneration}
        onRequestSaving={onRequestSaving}
        laboratoire={laboratoire}
        setMode={setMode}
        setOpen={setOpen}
        setTypeMode={setTypeMode}
        onRequestEdit={onRequestEdit}
        onRequestDelete={onRequestDelete}
        setOpenDialog={setOpenDialog}
      />
    </>
  );
};
