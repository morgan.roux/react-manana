import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      '& .k-widget.k-toolbar.k-scheduler-toolbar': {
        display: 'none !important',
      },
      '& .k-scheduler-cell.k-side-cell': {
        display: 'none',
      },
      '& .k-widget.k-toolbar.k-scheduler-footer': {
        display: 'none',
      },
      '& .k-scheduler-layout.k-scheduler-layout-flex.k-scheduler-timeline-view': {
        height: '30px',
      },
      '& .k-scheduler-cell.k-heading-cell': {
        colspan: 2,
        borderRight: '1px solid #7777',
        borderTop: '1px solid #7777',
        width: '2px !important',
      },
    },
  }),
);
