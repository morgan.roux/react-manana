import { Typography } from '@material-ui/core';
import React, { FC } from 'react';
import moment from 'moment';
import { createStyles, makeStyles } from '@material-ui/styles';
import { Grid, Paper } from '@material-ui/core';
interface AssociePropsMobile {
  data?: any;
}
const useStyles = makeStyles(() =>
  createStyles({
    paper: {
      background: '#f1f1f1',
      padding: 15,
      height: '100%',
    },
    spanBold: {
      fontWeight: 500,
    },
  }),
);
export const ProduitAssocieMobile: FC<AssociePropsMobile> = ({ data }) => {
  const classes = useStyles();
  return (
    <Grid item xs={12} sm={6}>
      <Paper className={classes.paper}>
        <Grid container>
          <Grid item style={{ width: 50, marginRight: 5 }}>
            {data?.produitPhoto?.fichier?.publicUrl ? (
              <img
                style={{ height: 50, width: 50, objectFit: 'cover' }}
                src={data?.produit.produitPhoto?.fichier?.publicUrl}
              />
            ) : (
              '-'
            )}
          </Grid>
          <Grid item style={{ width: 'calc(100% - 50px)' }}>
            <Typography>
              <span className={classes.spanBold}>{data?.produit?.libelle || '-'}</span>
            </Typography>
            <Typography>---------</Typography>
          </Grid>
        </Grid>
        <Typography>
          <span className={classes.spanBold}>Code :</span> {data?.produit?.produitCode?.code || '-'}
        </Typography>
        <Typography>
          <span className={classes.spanBold}>Famille : </span>
          {data?.produit?.famille?.libelleFamille || '-'}
        </Typography>
        <Typography>
          <span className={classes.spanBold}>Reduction unitaire : </span>
          {data?.produit?.prixUnitaire || '-'}
        </Typography>
        <Typography>
          <span className={classes.spanBold}>Quantité vendue : </span>
          {data?.produit?.quantiteRealise || '-'}
        </Typography>
        <Typography>
          <span className={classes.spanBold}>Total réalisée : </span>
          {(data?.remisePrevue || 0).toFixed(2)} €
        </Typography>
      </Paper>
    </Grid>
  );
};

interface ReglementPropsMobile {
  data?: any;
}
export const ReglementMobile: FC<ReglementPropsMobile> = ({ data }) => {
  const classes = useStyles();
  return (
    <Grid item xs={12} sm={6}>
      <Paper className={classes.paper}>
        <Typography>
          <span className={classes.spanBold}>Mode : </span>
          {data?.modeReglement?.libelle || '-'}
        </Typography>
        <Typography>
          <span className={classes.spanBold}>Date : </span>{' '}
          {data.dateStatutCloture ? moment.utc(data?.dateStatutCloture).format('DD-MM-YYYY') : '-'}
        </Typography>
        <Typography>
          <span className={classes.spanBold}>Montant : </span>{' '}
          {`${data?.montantStatutCloture || 0} €`}
        </Typography>
      </Paper>
    </Grid>
  );
};
