import { Column } from '@lib/ui-kit';
import React from 'react';

export const useProduitColumn = () => {
  const produitColumns: Column[] = [
    {
      name: 'produit.produitCode.code',
      label: 'Code',
      renderer: (value: any) => {
        console.log('VALUE', value);
        return value?.produit?.produitCode?.code || '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return value?.produitPhoto?.fichier?.publicUrl ? (
          <img style={{ maxHeight: 48 }} src={value?.produit.produitPhoto?.fichier?.publicUrl} />
        ) : (
          '-'
        );
      },
    },
    {
      name: 'produit.libelle',
      label: 'Nom',
      renderer: (value: any) => {
        return value?.produit?.libelle || '-';
      },
    },
    {
      name: 'produit.famille.libelleFamille',
      label: 'Famille',
      renderer: (value: any) => {
        return value?.produit?.famille?.libelleFamille || '-';
      },
    },
    {
      name: 'produit.pu',
      label: 'Reduction unitaire',
      renderer: (value: any) => {
        console.log('---------------------value remuneration---------: ', value);
        return `${(value?.produit?.prixUnitaire || 0).toFixed(2)} €`;
      },
    },
    {
      name: `produit.quantite_vendue`,
      label: 'Quantité vendue',
      renderer: (value: any) => {
        return value?.produit?.quantiteRealise || '-';
      },
      // ?? value?.produit?.quantitePrevue ??
    },
    {
      name: 'produit.total_realise',
      label: 'Total réalisée',
      renderer: (value: any) => {
        return `${(value?.produit?.totalRealise || 0).toFixed(2)} €`;
      },
    },
  ];

  return produitColumns;
};
