import React from 'react';
import { Box, IconButton, InputAdornment, Menu, MenuItem, Typography } from '@material-ui/core';
import {
  Column,
  CustomDatePicker,
  CustomFormTextField,
  CustomSelect,
} from '@lib/ui-kit';
import { useStyles } from './style';
import { RemunerationFinalComponentProps, RemunerationFinalProps } from './interface';
import moment from 'moment';
import { MoreHoriz, Delete, Edit, Visibility } from '@material-ui/icons';

export const useAddColumnRemuneration = (props: RemunerationFinalComponentProps) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const handleClickMenu = () => {};
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleEditReel = () => {};
  const handleShowDetail = () => {
    console.log();
  };
  const classes = useStyles();
  //   const isMoreRemuneration = () => {
  //     return (
  //       props.remunerationCurrent.date &&
  //       props.remunerationCurrent.modePaiement &&
  //       props.remunerationCurrent.montant &&
  //       props.remunerationCurrent.type
  //     );
  //   };
  const columns: Column[] = [
    {
      name: 'type',
      label: 'Type',
      renderer: (row: RemunerationFinalProps, index) => {
        if (index === 0) {
          return (
            <CustomSelect
              list={props.listTypeRemuneration}
              index="libelle"
              listId="id"
              label="Type"
              value={props.remunerationCurrent.type}
              onChange={props.onRequestChangeRemuneration.handleChangeType}
              className={classes.heightSelect}
              name="type"
            />
          );
        }
        return <Typography>{row?.type?.libelle}</Typography>;
      },
    },
    {
      name: 'mode',
      label: 'Mode',
      renderer: (row: RemunerationFinalProps, index) => {
        if (index === 0) {
          return (
            <CustomSelect
              list={props.listModePaiement}
              index="libelle"
              listId="id"
              label="Mode"
              value={props.remunerationCurrent.modePaiement}
              onChange={props.onRequestChangeRemuneration.handleChangeModePaiement}
              className={classes.heightSelect}
              name="mode"
            />
          );
        }
        return <Typography>{row?.modePaiement?.libelle}</Typography>;
      },
    },
    {
      name: 'date',
      label: 'Date',
      renderer: (row: RemunerationFinalProps, index) => {
        if (index === 0) {
          return (
            <CustomDatePicker
              value={props.remunerationCurrent.date}
              onChange={props.onRequestChangeRemuneration.handleChangeDate}
              required
              label="Date"
              name="date"
            />
          );
        }
        return <Typography>{moment(row?.date).format('DD/MM/YYYY')}</Typography>;
      },
    },
    {
      name: 'montant',
      label: 'Montant',
      renderer: (row: RemunerationFinalProps, index) => {
        if (index === 0) {
          return (
            <CustomFormTextField
              label="Montant"
              onChange={props.onRequestChangeRemuneration.handleChangeMontant}
              value={props.remunerationCurrent.montant}
              type="number"
              InputProps={{
                endAdornment: <InputAdornment position="end">‎€</InputAdornment>,
              }}
              inputProps={{ min: 0, step: 1, style: { textAlign: 'right' } }}
              name="montant"
            />
          );
        }
        return <Typography>{row?.montant}</Typography>;
      },
    },
    {
      name: '',
      label: '',
      renderer: (_row: RemunerationFinalProps, index) => {
        if (index === 0) {
          return (
            <Box style={{ position: 'relative' }}>
              <IconButton
                style={{ position: 'absolute', top: '-40px', left: '35px' }}
                onClick={handleClickMenu}
              >
                <MoreHoriz />
              </IconButton>

              <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
              >
                <MenuItem onClick={handleShowDetail}>
                  <Visibility /> Détail rémunération
                </MenuItem>
                <MenuItem onClick={() => handleEditReel}>
                  <Edit />
                  Modifier
                </MenuItem>
                <MenuItem onClick={handleClose}>
                  <Delete />
                  Supprimer
                </MenuItem>
              </Menu>
            </Box>
          );
        }
        return '-';
      },
    },
    // {
    //   name: 'action',
    //   label: 'Action',
    //   renderer: (row: RemunerationFinalProps, index) => {
    //     if (index === 0) {
    //       return (
    //         <CustomButton
    //           color="secondary"
    //           className={classes.contentButtonAction}
    //           onClick={props.onMoreRemuneration}
    //           // disabled={!isMoreRemuneration()}
    //         >
    //           <Add />
    //         </CustomButton>
    //       );
    //     }
    //     return (
    //       <IconButton onClick={() => props.onRemoveRemuneration(row)}>
    //         <Close />
    //       </IconButton>
    //     );
    //   },
    // },
  ];
  return columns;
};
