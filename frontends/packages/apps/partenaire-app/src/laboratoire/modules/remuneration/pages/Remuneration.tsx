import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { useStyles } from './styles';
import TableChartSubToolbar from '@lib/ui-kit/src/components/molecules/TableChartSubToolbar';
import { SmallLoading } from '@lib/ui-kit';
import HistoriqueRemuneration from '../components/historique-remuneration/HistoriqueRemuneration';
import { RemunerationGrid } from '../components/remuneration-grid/RemunerationGrid';
import { MobileTable } from '../components/mobile-table/MobileTable';
import { useApplicationContext } from '@lib/common';
import { useRemuneration } from '../hooks/useRemuneration';
import { useHistory } from 'react-router';

export interface RemunerationPageProps {
  classes?: {
    root?: string;
    containerBox?: string;
  };
}

export { HistoriqueRemuneration };

const RemunerationPage: FC<RemunerationPageProps> = ({ classes: providedClasses }) => {
  const classes = useStyles();
  const { isMobile } = useApplicationContext();
  const history = useHistory();

  // const [open, setOpen] = useState<boolean>(false);
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(12);

  const { onRequestSearch, remunerationsTables, onRequestShowDetails, open, setOpen, details, remunerations } =
    useRemuneration({ tab: 'remuneration' });

  useEffect(() => {
    if (onRequestSearch) onRequestSearch({ skip, take });
  }, [skip, take]);

  const handleSearchToolbar = (searchText: string, date: any, skip?: any, take?: any) => {
    onRequestSearch && onRequestSearch({ filter: [{ date }], searchText, skip, take });
  };

  return (
    <Box className={providedClasses?.root}>
      <Box
        p={2}
        className={providedClasses?.containerBox}
        style={{
          width: '100%',
          paddingLeft: 15,
          paddingRight: 15,
        }}
      >
        <TableChartSubToolbar
          onRequestChange={handleSearchToolbar}
          withSearch={false}
          ToolBarBoxClassName={isMobile ? classes.toolBarChartMobile : ''}
        />
        {isMobile ? (
          <div>
            <MobileTable
              data={remunerationsTables?.data}
              onRequestShowDetails={onRequestShowDetails}
              open={open}
              setOpen={(open: boolean) => {
                setOpen && setOpen(open);
              }}
              details={details}
            />
          </div>
        ) : (
          <Box mt={2}>
            {remunerations?.loading && <SmallLoading />}
            {remunerations?.error && <Box textAlign="center">Une erreur est survenue</Box>}
            {!remunerations?.error && (
              <>
                <RemunerationGrid
                  data={remunerationsTables?.data}
                  // savingInputRemuneration={savingInputRemuneration}
                  onRequestShowDetails={onRequestShowDetails}
                  open={open}
                  setOpen={setOpen}
                  details={details}
                />
              </>
            )}
          </Box>
        )}
        <Box mt={5}>
          <HistoriqueRemuneration
            data={remunerations?.data}
            error={remunerations?.error as any}
            loading={remunerations?.loading as any}
            setOpen={setOpen}
            onRequestSearch={onRequestSearch}
            setSkip={setSkip}
            setTake={setTake}
            onSortTable={(sortTable) => {
              onRequestSearch && onRequestSearch({ sortTable });
            }}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default RemunerationPage;
