import React, { FC } from 'react';
import { Month } from './Month';
import { HeadMobile } from './HeadMobile';
import { FooterMobile } from './FooterTable';
import { useStyles } from './style';
import { getMonthNumber, getMonths } from '@lib/ui-kit/src/services/Helpers';
import { RemunerationGridProps } from '../remuneration-grid/RemunerationGrid';
import DetailRemuneration from '../detail-remuneration/DetailRemuneration';

interface MobileTableProps {
  data: any;
}
export const MobileTable: FC<MobileTableProps & RemunerationGridProps> = ({
  data,
  open,
  setOpen,
  onRequestShowDetails,
  details,
}) => {
  const classes = useStyles();
  const handleShowDetails = () => {
    setOpen && setOpen(true);
  };
  return (
    <div style={{ marginTop: 20 }}>
      <HeadMobile composition={!!data.length && data[0].composition} />
      <div className={classes.hideScrollbar}>
        {getMonths().map((month, index) => {
          const currentMonth = ((!!data.length && data[0].months) || []).find(
            (el: any) => getMonthNumber(month) === el.month
          );
          return (
            <Month
              month={month}
              coopPrevue={currentMonth?.prevue?.coop}
              coopRealise={currentMonth?.reel?.coop}
              briPrevue={currentMonth?.prevue?.bri}
              briRealise={currentMonth?.reel?.bri}
              totalPrevue={currentMonth?.prevue?.total}
              totalRealise={currentMonth?.reel?.total}
              briPrecent={currentMonth?.reel?.bri?.percentage}
              coopPrecent={currentMonth?.reel?.coop?.percentage}
              totalPrecent={currentMonth?.reel?.total?.percentage}
              key={index}
              open={open}
              setOpen={setOpen}
              onRequestShowDetails={onRequestShowDetails && handleShowDetails}
            />
          );
        })}
      </div>
      <FooterMobile
        totalPrevue={!!data.length && data[0].total?.prevue}
        totalRealise={!!data.length && data[0].total?.reel}
        totalVersePrevue={!!data.length && data[0].totalVerse?.prevue}
        totalVerseRealise={!!data.length && data[0].totalVerse?.reel}
      />
      <DetailRemuneration open={open} setOpen={setOpen} details={details} />
    </div>
  );
};
