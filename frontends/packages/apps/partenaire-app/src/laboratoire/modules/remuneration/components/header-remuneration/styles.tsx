import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      '& .MuiInputBase-input.MuiOutlinedInput-input MuiInputBase-inputAdornedEnd.MuiOutlinedInput-inputAdornedEnd': {
        display: 'none',
      },
    },
    calendar: {
      '& .MuiOutlinedInput-notchedOutline': {
        border: 'none',
      },
      '& .MuiInputBase-input.MuiOutlinedInput-input.MuiInputBase-inputAdornedEnd.MuiOutlinedInput-inputAdornedEnd': {
        display: 'none',
      },
      '& .MuiInputAdornment-root.MuiInputAdornment-positionEnd': {
        marginTop: '25px',
        marginLeft: '30px',
      },
    },
  }),
);
