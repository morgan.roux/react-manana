import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    '& .k-grid-header.k-header': {
      width: '200px',
    },
    overflow: {
      maxWidth: 100,
      overflow: 'auto',
    },
    alignLeft: {
      '& > .k-link': {
        textAlign: 'left',
      },
    },
    alignCenter: {
      '& > .k-link': {
        textAlign: 'center',
      },
    },
    header: {
      verticalAlign: 'middle !important',
      fontFamily: 'Roboto',
      fontSize: 14,
      color: '#212121',
      background: '#FFFFFF !important',
    },
    monthHeader: {
      fontWeight: 'bold',
    },
    renumerationHeader: {
      '& > .k-link': {
        whiteSpace: 'normal',
      },
    },
    renumerationFont: {
      fontFamily: 'Roboto',
      fontSize: 14,
      textAlign: 'center',
    },
    innerCell: {
      padding: '0px !important',
      '& .k-grid td': {
        padding: '0px !important',
      },
    },
    grid: {
      '& td': {
        padding: '0px !important',
      },
      '& .k-grid-header': {
        padding: '0 !important',
      },
    },
    cellWithPadding: {
      padding: '12px 8px !important',
    },
  })
);
