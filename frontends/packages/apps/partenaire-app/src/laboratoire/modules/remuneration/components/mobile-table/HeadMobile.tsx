import { Grid } from '@material-ui/core';
import React, { FC } from 'react';
import { useStyles } from './style';
interface HeadMobileProps {
  composition?: any;
}
export const HeadMobile: FC<HeadMobileProps> = ({ composition }) => {
  const classes = useStyles();
  return (
    <>
      <Grid container className={classes.gridFix}>
        <Grid item xs={6} className={classes.gridItem}>
          Compositions de la prestation
        </Grid>
        <Grid item xs={6} className={classes.gridItem}>
          <div className={classes.innerComposition}>{composition}</div>
        </Grid>
      </Grid>
      <Grid container className={classes.gridFix}>
        <Grid item xs={6} className={classes.gridItem}>
          Rémunération prévue / réalisée
        </Grid>
        <Grid item xs={6} className={classes.gridItem}>
          <Grid container>
            <Grid item xs={6} className={classes.gridItem}>
              R. prévue
            </Grid>
            <Grid item xs={6} className={classes.gridItem}>
              R. réalisée
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
