// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable react/jsx-key */
import {
  Box,
  // IconButton,
  // Menu,
  // MenuItem,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  // Typography,
} from '@material-ui/core';
import React, { FC } from 'react';
import ScrollContainer from 'react-indiana-drag-scroll';
// import { Delete, Edit, MoreHoriz } from '@material-ui/icons';
import { CollonneProps } from './HeadTableChart/HeaderTableChart';
import moment from 'moment';
import { TypeRemunerationFinalProps } from '../../form-remuneration/interface';
import { PRTRemuneration, PRTRemunerationReglement } from './interface';

export interface TableChartProps {
  collone?: CollonneProps[];
  remunerations?: PRTRemunerationReglement[];
  listTypeRemuneration?: TypeRemunerationFinalProps[];
  setOpen?: (open: boolean) => void;
  setMode?: (mode: 'creation' | 'modification') => void;
  setTypeMode?: (type: 'prevu' | 'reel') => void;
  onRequestEdit?: (edit: PRTRemunerationReglement) => void;
  onRequestDelete?: (data: PRTRemunerationReglement | PRTRemuneration) => void;
  setOpenDialog?: (open: boolean) => void;
}

export const TableChart: FC<TableChartProps> = ({
  collone,
  // remunerations,
  listTypeRemuneration,
  // setMode,
  // setOpen,
  // setTypeMode,
  // onRequestEdit,
  // onRequestDelete,
  // setOpenDialog,
}) => {
  // const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  // const [anchorEl2, setAnchorEl2] = React.useState<null | HTMLElement>(null);

  // const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
  //   setAnchorEl(event.currentTarget);
  // };

  // const handleClick2 = (event: React.MouseEvent<HTMLButtonElement>) => {
  //   setAnchorEl2(event.currentTarget);
  // };

  // const handleEditPrevu = (edit: PRTRemunerationReglement) => {
  //   setAnchorEl(null);
  //   setMode && setMode('modification');
  //   setTypeMode && setTypeMode('prevu');
  //   setOpen && setOpen(true);
  //   onRequestEdit && onRequestEdit(edit);
  // };

  // const handleEditReel = (edit: PRTRemunerationReglement) => {
  //   setAnchorEl2(null);
  //   setMode && setMode('modification');
  //   setOpen && setOpen(true);
  //   setTypeMode && setTypeMode('reel');
  //   setOpen && setOpen(true);
  //   onRequestEdit && onRequestEdit(edit);
  // };
  // const handleClose = () => {
  //   setAnchorEl(null);
  // };

  // const handleDeletePrevu = (data: PRTRemuneration) => {
  //   setOpenDialog && setOpenDialog(true);
  //   onRequestDelete && onRequestDelete(data);
  //   setTypeMode && setTypeMode('prevu');
  //   setAnchorEl(null);
  // };

  // const handleDeleteReel = (data: PRTRemunerationReglement) => {
  //   setOpenDialog && setOpenDialog(true);
  //   onRequestDelete && onRequestDelete(data);
  //   setTypeMode && setTypeMode('reel');
  //   handleClose();
  //   setAnchorEl2(null);
  // };

  // const handleCreate = () => {
  //   setAnchorEl2(null);
  //   setMode && setMode('creation');
  // };

  const dateTableau = moment();
  return (
    <>
      <Box>
        <ScrollContainer className="scroll-container">
          <Table>
            <TableHead>
              <TableRow>
                {Array.apply(0, Array(12)).map((_, i) => {
                  return (
                    <TableCell
                      colSpan={collone?.length}
                      style={{ textAlign: 'center', fontWeight: 'bold' }}
                    >
                      {dateTableau
                        .month(i)
                        .locale('fr')
                        .format('MMMM')}
                    </TableCell>
                  );
                })}
              </TableRow>
              <TableRow>
                {Array.apply(0, Array(12)).map(() => {
                  return (
                    <>
                      {(listTypeRemuneration || []).map(item => (
                        <TableCell style={{ width: '250px', fontWeight: 'bold' }}>
                          {item.libelle}
                        </TableCell>
                      ))}
                    </>
                  );
                })}
              </TableRow>
            </TableHead>
            {/* {(remunerations || []).map(dataRemuneration => ( */}
            <TableBody>
              <TableRow>
                {/* {Array.apply(0, Array(12)).map(() => { */}
                {/* return ( */}
                <>
                  {/* <TableCell
                    style={{
                      backgroundColor: '',
                      height: '50px',
                      width: '100px',
                    }}
                  > */}
                  {/* <Box display="flex" flexDirection="column">
                        <Box style={{ position: 'relative' }}>
                          <IconButton
                            style={{ position: 'absolute', top: '-40px', left: '35px' }}
                            // onClick={handleClick}
                          >
                            <MoreHoriz />
                          </IconButton>

                          <Menu
                            id="simple-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                          >
                            <MenuItem onClick={() => handleEditPrevu(dataRemuneration)}>
                              <Edit />
                              Modifier
                            </MenuItem>
                            <MenuItem onClick={handleClose}>
                              <Delete />
                              Supprimer
                            </MenuItem>
                          </Menu>
                        </Box>
                        {(remunerations || []).map(_item => (
                          <Box>
                            {dataRemuneration.prestationType.code === 'COOP' &&
                              dateTableau.year() ===
                                dataRemuneration.remuneration.dateEcheance.year() &&
                              dateTableau.month() ===
                                dataRemuneration.remuneration.dateEcheance.month() && (
                                <Typography
                                  style={{
                                    color: '',
                                    opacity: 1,
                                    textAlign: 'right',
                                    fontWeight: 'bold',
                                  }}
                                >
                                  {dataRemuneration.remuneration.montantPrevu} €
                                </Typography>
                              )}
                          </Box>
                        ))}
                      </Box> */}
                  {/* </TableCell> */}
                  {/* <TableCell> */}
                  {/* <Box style={{ position: 'relative' }}>
                        <IconButton
                          style={{ position: 'absolute', top: '-40px', left: '35px' }}
                          // onClick={handleClick}
                        >
                          <MoreHoriz />
                        </IconButton>
                        <Menu
                          id="simple-menu"
                          anchorEl={anchorEl}
                          keepMounted
                          open={Boolean(anchorEl)}
                          onClose={handleClose}
                        >
                          <MenuItem onClick={() => handleEditPrevu(dataRemuneration)}>
                            <Edit />
                            Modifier
                          </MenuItem>
                          <MenuItem
                            onClick={() => handleDeletePrevu(dataRemuneration.remuneration)}
                          >
                            <Delete />
                            Supprimer
                          </MenuItem>
                        </Menu>
                      </Box> */}
                  {/* {(remunerations || []).map(item => (
                        <Box>
                          {item?.prestationType.code === 'BRI' && (
                            <Typography style={{ textAlign: 'right', fontWeight: 'bold' }}>
                              {dataRemuneration.remuneration.montantPrevu} €
                            </Typography>
                          )}
                        </Box>
                      ))} */}
                  {/* </TableCell> */}
                </>
                {/* ); */}
                {/* })}  */}
              </TableRow>
              {/** ETO */}

              <TableRow>
                {/* {Array.apply(0, Array(12)).map(() => { */}
                {/* return ( */}
                <>
                  {/* <TableCell
                    style={{
                      backgroundColor: '#21b02b',
                      height: '50px',
                      width: '100px',
                    }}
                  > */}
                  {/* <Box width={60}>
                      <Box display="flex" flexDirection="column">
                        <Box> */}
                  {/* <Box>
                                  <Typography
                                    style={{
                                      textAlign: 'right',
                                      color: 'white',
                                    }}
                                  >
                                  </Typography>
                                  <Typography
                                    style={{
                                      textAlign: 'right',
                                      color: 'white',
                                      fontWeight: 'bold',
                                    }}
                                  >
                                  </Typography>
                                </Box> */}
                  {/* </Box>
                      </Box>
                    </Box> */}
                  {/* </TableCell> */}
                  {/* <TableCell
                    style={{
                      backgroundColor: '',
                      height: '50px',
                      width: '100px',
                    }}
                  > */}
                  {/* <Box width={60}>
                        <Box display="flex" flexDirection="row">
                          {dataRemuneration.prestationType.code === 'BRI' &&
                            dateTableau.month() === dataRemuneration.dateReglement.month() && (
                              <Box>
                                <Typography style={{ textAlign: 'right' }}>
                                  {dataRemuneration.montant} €
                                </Typography>
                                <Typography style={{ textAlign: 'right' }}>10%</Typography>
                              </Box>
                            )}
                        </Box>
                      </Box> */}
                  {/* </TableCell> */}
                </>
                {/* ); */}
                {/* })} */}
              </TableRow>
            </TableBody>
            {/* ))} */}
          </Table>
        </ScrollContainer>
      </Box>
    </>
  );
};
