import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    btnAction: {},
    toolBarChartMobile: {
      display: 'block !important',
      '&>div': {
        display: 'inline-flex',
      },
      '&>div:not(:last-child)': {
        marginLeft: '0 !important',
      },
      '&>div:first-child': {
        width: '100%',
      },
      '&>div:not(:first-child)': {
        marginTop: '15px !important',
      },
    },
  }),
);
