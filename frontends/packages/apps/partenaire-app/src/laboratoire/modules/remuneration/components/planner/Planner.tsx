import React, { FC } from 'react';

export interface PlannerProps {
  titre: string;
}

const Planner: FC<PlannerProps> = ({ titre }) => {
  return <div>{titre}</div>;
};

export default Planner;
