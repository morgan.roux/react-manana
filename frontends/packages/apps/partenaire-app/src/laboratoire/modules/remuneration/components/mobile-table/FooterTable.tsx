import { Grid } from '@material-ui/core';
import React, { FC } from 'react';
import { useStyles } from './style';

interface FooterMobileProps {
  totalPrevue?: any;
  totalRealise?: any;
  totalVersePrevue?: any;
  totalVerseRealise?: any;
  totalRealisePercent?: any;
}

export const FooterMobile: FC<FooterMobileProps> = ({
  totalPrevue,
  totalRealise,
  totalVersePrevue,
  totalVerseRealise,
  totalRealisePercent,
}) => {
  const classes = useStyles();
  return (
    <>
      <Grid container className={classes.gridFix} style={{ borderTop: '1px solid #ccc' }}>
        <Grid item xs={6} className={classes.gridItem}>
          Total
        </Grid>
        <Grid item xs={6} className={classes.gridItem} style={{ padding: 0 }}>
          <Grid container>
            <Grid
              item
              xs={6}
              className={classes.gridItem}
              style={{ background: totalPrevue?.background || '#fff' }}
            >
              <div className={classes.rightchiffre}>
                {parseFloat(totalPrevue?.value || '0').toFixed(2)} €
              </div>
            </Grid>
            <Grid
              item
              xs={6}
              className={classes.gridItem}
              style={{ background: totalRealise?.background || '#fff' }}
            >
              <div className={classes.rightchiffre}>
                {parseFloat(totalRealise?.value || '0').toFixed(2)} €
                <span className={classes.spanPercent}>{totalRealisePercent || '0'} %</span>
              </div>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid container className={classes.gridFix}>
        <Grid item xs={6} className={classes.gridItem}>
          Total versé
        </Grid>
        <Grid item xs={6} className={classes.gridItem} style={{ padding: 0 }}>
          <Grid container>
            <Grid
              item
              xs={6}
              className={classes.gridItem}
              style={{ background: totalVersePrevue?.background || '#fff' }}
            >
              <div className={classes.rightchiffre}>
                {parseFloat(totalVersePrevue?.value || '0').toFixed(2)} €
              </div>
            </Grid>
            <Grid
              item
              xs={6}
              className={classes.gridItem}
              style={{ background: totalVerseRealise?.background || '#fff' }}
            >
              <div className={classes.rightchiffre}>
                {parseFloat(totalVerseRealise?.value || '0').toFixed(2)} €
              </div>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};
