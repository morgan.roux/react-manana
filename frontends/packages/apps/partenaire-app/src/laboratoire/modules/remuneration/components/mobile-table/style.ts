import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    spanPercent: {
      display: 'block',
      fontWeight: 'bold',
    },
    space: {
      margin: theme.spacing(2),
    },
    gridFix: {
      border: '1px solid #ccc',
      '&:not(:first-child)': {
        borderTop: 'none',
      },
    },
    gridItem: {
      height: 60,
      '&:not(:last-child)': {
        borderRight: '1px solid #ccc',
      },
      display: 'flex',
      alignItems: 'center',
      padding: '0 5px',
      '& *': {
        fontSize: 12,
      },
      fontSize: 12,
    },
    gridBody: {
      borderBottom: '1px solid #ccc',
      borderRight: '1px solid #ccc',
      '&:first-child': {
        borderLeft: '1px solid #ccc',
      },
      '& .MuiList-root': {
        padding: 0,
      },
      '& .MuiListItem-root': {
        height: 60,
        padding: 7,
        '&:not(:last-child)': {
          borderBottom: '1px solid #ccc',
        },
      },
      '& *': {
        fontSize: 12,
      },
    },
    coopBritotal: {
      '& .MuiListItem-root': {
        textAlign: 'center',
      },
    },
    chiffre: {
      '& .MuiListItem-root': {
        textAlign: 'right',
      },
    },
    rightchiffre: {
      textAlign: 'right',
      width: '100%',
      padding: '0 5px',
    },
    month: {
      height: '100%',
      display: 'flex',
      alignItems: 'center',
      padding: '0 16px',
    },
    hideScrollbar: {
      '&::-webkit-scrollbar': {
        display: 'none',
      },
      msOverflowStyle: 'none',
      scrollbarWidth: 'none',
      overflowY: 'scroll',
      height: 300,
    },
    totalPrevueBold: {
      '& span': {
        fontWeight: 'bold',
      },
    },
    innerComposition: {
      width: '100%',
      alignItems: 'center',
      overflow: 'auto',
      maxHeight: '100%',
    },
  }),
);
