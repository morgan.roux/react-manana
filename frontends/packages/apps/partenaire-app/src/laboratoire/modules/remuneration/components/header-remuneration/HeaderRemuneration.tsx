import { CustomDatePicker, DebouncedSearchInput } from '@lib/ui-kit';
import { Box, Button, ButtonGroup, Typography } from '@material-ui/core';
import { ChevronLeft, ChevronRight } from '@material-ui/icons';
import { addYears, subYears } from 'date-fns';
import moment from 'moment';
import React, { FC, useState } from 'react';
import { useStyles } from './styles';
export interface HeaderRemunerationProps {
  date: Date;
  onRequestYear: (date: Date) => void;
}

export const HeaderRemuneration: FC<HeaderRemunerationProps> = ({ date, onRequestYear }) => {
  const [value, setValue] = useState<string>('');
  const classes = useStyles();
  const handlePrev = () => {
    onRequestYear(subYears(date, 1));
  };

  const handleNext = () => {
    onRequestYear(addYears(date, 1));
  };

  const handleDateChange = (date: Date | null) => {
    onRequestYear(date || new Date());
  };

  return (
    <>
      <Box display="flex" flexDirection="row">
        <Box>
          <DebouncedSearchInput
            value={value}
            placeholder="Rechercher"
            fullWidth={false}
            onChange={setValue}
          />
        </Box>
        <Box display="flex" flexDirection="row" marginLeft={5} style={{ height: '30px' }} mt={1.5}>
          <ButtonGroup disableElevation color="primary" aria-label="small outlined button group">
            <Button onClick={handlePrev}>
              <ChevronLeft />
            </Button>
            <Button onClick={handleNext}>
              <ChevronRight />
            </Button>
          </ButtonGroup>
        </Box>
        <Box mr={5} display="flex" flex="row" className={classes.calendar}>
          <CustomDatePicker
            value={date}
            views={['year']}
            onChange={handleDateChange}
            format="yyyy"
          />
        </Box>
        <Box mt={2}>
          <Typography>{moment(date).format('YYYY')}</Typography>
        </Box>
      </Box>
    </>
  );
};
