import { ChangeEvent } from 'react';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';

import { PrestationType } from '../../pages/interface';
import { PRTRemuneration, PRTRemunerationReglement } from '../tableau-remuneration/TableChart/interface';

export interface FormRemunerationProps {
  listTypeRemuneration?: TypeRemunerationFinalProps[];
  listModePaiement?: ModePaiementRemunerationProps[];
  onRequestSaving?: (data: RemunerationRequestSavingProps) => void;
  mode: 'creation' | 'modification';
  saving?: boolean;
  saved?: boolean;
  setSaved?: (saved: boolean) => void;
  remunerationToEdit?: PRTRemunerationReglement;
  open?: boolean;
  setOpen?: (open: boolean) => void;
  typeMode?: 'prevu' | 'reel';
  onRequestFilterRemuneration?: (filter: any) => void;
  remunerationPrevue?: PRTRemuneration;
}

export interface RemunerationFinalProps {
  id?: string;
  type?: TypeRemunerationFinalProps;
  modePaiement?: ModePaiementRemunerationProps;
  date: Date;
  montant: number;
  description: string;
  fichiers?: any[];
}

export interface RemunerationFinalComponentProps {
  listTypeRemuneration: TypeRemunerationFinalProps[];
  listModePaiement: ModePaiementRemunerationProps[];
  onRequestChangeRemuneration: {
    handleChangeType: (e: ChangeEvent<HTMLInputElement>) => void;
    handleChangeModePaiement: (e: ChangeEvent<HTMLInputElement>) => void;
    handleChangeDate: (date: MaterialUiPickersDate) => void;
    handleChangeMontant: (e: ChangeEvent<HTMLInputElement>) => void;
  };
  onMoreRemuneration: () => void;
  onRemoveRemuneration: (data: RemunerationFinalProps) => void;
  remunerationCurrent: RemunerationFinalProps;
}

export interface TypeRemunerationFinalProps {
  id: string;
  libelle: string;
  code: string;
}

export interface ModePaiementRemunerationProps {
  id: string;
  libelle: string;
  code: string;
}

export interface RemunerationRequestSavingProps {
  id?: string;
  dateEcheance: Date;
  estimation: number;
  remunerationFinal: RemunerationFinalProps;
  prestationType?: PrestationType;
  mode: 'creation' | 'modification';
}

export interface Remuneration {
  id?: string;
  type: TypeRemunerationFinalProps;
  modePaiement: ModePaiementRemunerationProps;
  date: Date;
  montant: number;
  dateEcheance: Date;
  estimation: number;
  description: string;
  file?: File[];
}
