export interface PRTRemunerationReglement {
  id: string;
  idPrestationType: string;
  idModeReglement: string;
  idRemuneration: string;
  dateReglement: moment.Moment;
  reglementMode: PRTReglementMode;
  remuneration: PRTRemuneration;
  prestationType: PrestationType;
  montant: number;
  description?: string;
  file?: any[];
}

export interface PRTReglementMode {
  id: string;
  code: string;
  libelle: string;
}

export interface PRTRemuneration {
  id: string;
  dateEcheance: moment.Moment;
  montantPrevu: number;
  montantRealise: number;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  prestationType: PrestationType;
}

export interface PrestationType {
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}
