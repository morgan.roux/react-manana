/* eslint-disable eslint-comments/disable-enable-pair */
/* eslint-disable react/jsx-key */
import { Table, TableHead, TableRow, TableCell } from '@material-ui/core';
import moment from 'moment';
import React, { FC } from 'react';

export interface HeaderTableChartProps {
  // colNumber: number;
  collonne: CollonneProps[];
}

export interface CollonneProps {
  id: string;
  libelle: string;
  code: string;
}

export const HeaderTableChart: FC<HeaderTableChartProps> = ({ collonne }) => {
  return (
    <Table>
      <TableHead>
        <TableRow>
          {Array.apply(0, Array(12)).map((_, i) => {
            return (
              <TableCell
                colSpan={collonne.length}
                style={{ textAlign: 'center', fontWeight: 'bold' }}
              >
                {moment().month(i).locale('fr').format('MMMM')}
              </TableCell>
            );
          })}
        </TableRow>
        <TableRow>
          {Array.apply(0, Array(12)).map(() => {
            return (
              <>
                {collonne.map((item) => (
                  <TableCell style={{ width: '250px', fontWeight: 'bold' }}>
                    {item.libelle}
                  </TableCell>
                ))}
              </>
            );
          })}
        </TableRow>
      </TableHead>
    </Table>
  );
};
