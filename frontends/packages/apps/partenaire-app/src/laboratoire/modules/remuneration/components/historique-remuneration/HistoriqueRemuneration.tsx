import React, { FC, useState } from 'react';
import { Box, TableCell, TableRow, Typography } from '@material-ui/core';
import { columnHistoriqueRemuneration } from './column';
import { useStyles } from './style';
import { NewCustomButton, Table, TableChange } from '@lib/ui-kit';
import { PRTRemunerationReglement } from '../../pages/interface';

export interface PlanMarketingHistorique {
  id: string;
  modeReglement: {
    id: string;
    libelle: string;
    code: string;
  };
  type: {
    id: string;
    libelle: string;
    code: string;
  };
  montantStatutCloture: number | null;
  montantStatutRealise: number | null;
  dateStatutCloture: Date | null;
  dateStatutRealise: Date | null;
  fichiers: any[];
  typeAssocie?: { id: string; nom: string };
}
export interface HistoriqueRemunerationProps {
  loading?: boolean;
  error?: Error;
  data?: PlanMarketingHistorique[];
  remunerationToEdit?: PRTRemunerationReglement;
  errorSaving?: boolean;
  saved?: boolean;
  setSaved?: (newSaved: boolean) => void;
  saving?: boolean;
  onRequestEdit?: (remunerationToEdit: PlanMarketingHistorique | undefined) => void;
  onRequestSave?: (remuneration: PlanMarketingHistorique) => void;
  onRequestDelete?: (remuneration: PlanMarketingHistorique) => void;
  onRequestSearch?: ({ searchText, filter, skip, take }: any) => void;
  setOpenDialog?: (open: boolean) => void;
  rowsTotal?: number;
  setOpen?: (open: boolean) => void;
  setMode?: (mode: 'creation' | 'modification') => void;
  setSkip?: (skip: number) => void;
  setTake?: (take: number) => void;

  isSelectable?: boolean;
  onRequestSelected?: (factures?: PlanMarketingHistorique[]) => void;
  selectedItemsIds?: string[];
  onSortTable?: ({ column, direction }: any) => void;
}

const HistoriqueRemuneration: FC<HistoriqueRemunerationProps> = ({
  loading,
  error,
  data,
  setSkip,
  setTake,
  isSelectable,
  onRequestSelected,
  selectedItemsIds,
  onSortTable,
}) => {
  const [remunerationsSelected, setRemunerationsSelected] = useState<PlanMarketingHistorique[]>();

  const classes = useStyles();

  React.useEffect(() => {
    if (selectedItemsIds?.length && (data || []).length > 0) {
      const selectedRows = (data || []).filter((item) => selectedItemsIds.includes(item.id || ''));
      setRemunerationsSelected(selectedRows);
    }
  }, [selectedItemsIds, data]);

  const handleSearchTable = ({ skip, take }: TableChange) => {
    setSkip && setSkip(skip);
    setTake && setTake(take);
  };

  const handleSelectAll = () => {
    if (remunerationsSelected?.length === data?.length) {
      setRemunerationsSelected([]);
    } else {
      setRemunerationsSelected(data || []);
    }
  };

  const handleSelectChange = (dataSelected: any) => {
    setRemunerationsSelected(dataSelected);
  };

  const handleSortTable = (column: string, direction: 'DESC' | 'ASC') => {
    onSortTable && onSortTable({ column, direction });
  };

  const columns = columnHistoriqueRemuneration();

  const total = data
    ? data
        .map((element) => {
          return element.montantStatutCloture ?? 0;
        })
        .reduce((accumulator, currentValue) => {
          return accumulator + currentValue;
        }, 0)
    : 0;

  return (
    <Box className={classes.root}>
      <Box py={2} textAlign="center">
        <Typography variant="h6">Historique des rémunérations payées</Typography>
      </Box>
      <Table
        onSortColumn={handleSortTable}
        columns={columns}
        error={error}
        loading={loading}
        search={false}
        data={data || []}
        onRunSearch={handleSearchTable}
        rowsTotal={data?.length || 0}
        additionalRows={
          <TableRow>
            <TableCell>
              <TableCell />
            </TableCell>
            <TableCell>
              <TableCell />
            </TableCell>
            <TableCell>
              <TableCell />
            </TableCell>
            <TableCell style={{ borderBottomColor: 'white', display: 'flex', justifyContent: 'flex-end' }}>
              <span
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                  borderBottomColor: 'white',
                  color: '#777',
                  opacity: 1,
                }}
              >
                Total
              </span>
            </TableCell>
            <TableCell style={{ borderBottomColor: 'white' }}>{total} &nbsp;€</TableCell>
          </TableRow>
        }
        selectable={isSelectable}
        noTableToolbarSearch={isSelectable}
        onClearSelection={isSelectable ? () => setRemunerationsSelected([]) : undefined}
        onSelectAll={isSelectable ? handleSelectAll : undefined}
        rowsSelected={isSelectable ? remunerationsSelected : undefined}
        onRowsSelectionChange={isSelectable ? handleSelectChange : undefined}
      />
      {isSelectable && (
        <Box display="flex" justifyContent="flex-end">
          <NewCustomButton
            onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
              event.preventDefault();
              event.stopPropagation();
              onRequestSelected && onRequestSelected(remunerationsSelected);
            }}
            className={classes.joindreButton}
            disabled={!remunerationsSelected?.length}
          >
            Joindre
          </NewCustomButton>
        </Box>
      )}
    </Box>
  );
};

export default HistoriqueRemuneration;
