// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
// eslint-disable-next-line no-use-before-define
import moment from 'moment';
import { PlanMarketingHistorique } from './HistoriqueRemuneration';
import { Column } from '@lib/ui-kit';

export const columnHistoriqueRemuneration = () => {
  const columns: Column[] = [
    {
      name: 'idType',
      label: 'Type',
      renderer: (details: PlanMarketingHistorique) => {
        return details?.type?.libelle || '-';
      },
    },
    {
      name: 'idMode',
      label: 'Mode',
      renderer: (details: PlanMarketingHistorique) => {
        return details?.modeReglement?.libelle || '-';
      },
    },
    {
      name: 'labo',
      label: 'labo',
      renderer: (details: PlanMarketingHistorique) => {
        return details?.typeAssocie?.nom ?? '-';
      },
    },
    {
      name: 'dateStatutRealise',
      label: 'Date',
      renderer: (details: PlanMarketingHistorique) => {
        return details.dateStatutCloture
          ? moment.utc(details?.dateStatutCloture).format('DD-MM-YYYY')
          : details.dateStatutRealise
          ? moment.utc(details?.dateStatutRealise).format('DD-MM-YYYY')
          : '-';
      },
    },
    {
      name: 'montantStatutCloture',
      label: 'Montant',
      renderer: (details: PlanMarketingHistorique) => {
        return `${details?.montantStatutCloture || details?.montantStatutRealise || 0} €`;
      },
    },
  ];

  return columns;
};
