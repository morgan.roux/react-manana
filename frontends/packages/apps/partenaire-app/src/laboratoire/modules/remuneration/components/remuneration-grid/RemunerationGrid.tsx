import { PRTPlanMarketing } from '@lib/types';
import { CustomGrid, CustomGridCell } from '@lib/ui-kit';
import { getMonthNumber, getMonths } from '@lib/ui-kit/src/services/Helpers';
import { IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Delete, Edit, MoreHoriz } from '@material-ui/icons';
import { GridColumn } from '@progress/kendo-react-grid';
import classnames from 'classnames';
import * as React from 'react';
import { FC, useState } from 'react';
import { Laboratoire } from '../../../../types';
import { PRTRemuneration, PRTRemunerationReglement } from '../../pages/interface';
import DetailRemuneration from '../detail-remuneration/DetailRemuneration';
import {
  ModePaiementRemunerationProps,
  RemunerationRequestSavingProps,
  TypeRemunerationFinalProps,
} from '../form-remuneration/interface';
import { useStyles } from './styles';
import { round2Decimals } from '../../../../util';

export interface RemunerationGridProps {
  remunerations?: PRTRemunerationReglement[];
  listModePaiement?: ModePaiementRemunerationProps[];
  listTypeRemuneration?: TypeRemunerationFinalProps[];
  remunerationToEdit?: PRTRemunerationReglement;
  onRequestSaving?: (data: RemunerationRequestSavingProps) => void;
  // saving: boolean;
  laboratoire?: Laboratoire;
  setMode?: (mode: 'creation' | 'modification') => void;
  setOpen?: (open: boolean) => void;
  setTypeMode?: (type: 'prevu' | 'reel') => void;
  onRequestEdit?: (edit: PRTRemunerationReglement) => void;
  onRequestDelete?: (edit: PRTRemunerationReglement | PRTRemuneration) => void;
  setOpenDialog?: (open: boolean) => void;
  data: any;
  savingInputRemuneration?: {
    loading?: boolean;
    onRequestSaveInput?: (value: string, params: any) => void;
  };
  onRequestShowDetails?: (params?: any) => void;
  open?: boolean;
  details: {
    column: 'BRI' | 'AUTRES';
    type: 'PREVUE' | 'REALISEE';
    planMarketings?: PRTPlanMarketing[];
    error?: Error;
    loading?: boolean;
  };
}

export const RemunerationGrid: FC<RemunerationGridProps> = ({
  data,
  savingInputRemuneration,
  onRequestShowDetails,
  open,
  setOpen,
  details,
}) => {
  const months = getMonths();
  const classes = useStyles();

  // const optionBtn = (
  //   <>
  //     <IconButton
  //       // aria-controls={`option-btn-${props.id}`}
  //       aria-haspopup="true"
  //       onClick={handleClickOptionBtnMenu}
  //     >
  //       <MoreHoriz />
  //     </IconButton>
  //     <Menu
  //       // id={`option-btn-${props.id}`}
  //       anchorEl={anchorEl}
  //       keepMounted
  //       open={Boolean(anchorEl)}
  //       onClose={handleCloseOptionBtnMenu}
  //       getContentAnchorEl={null}
  //       anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
  //       transformOrigin={{ vertical: 'top', horizontal: 'right' }}
  //     >
  //       <MenuItem onClick={handleCloseOptionBtnMenu}>
  //         <ListItemIcon>
  //           <Edit />
  //         </ListItemIcon>
  //         Modifier
  //       </MenuItem>
  //       <MenuItem onClick={handleCloseOptionBtnMenu}>
  //         <ListItemIcon>
  //           <Delete />
  //         </ListItemIcon>
  //         Supprimer
  //       </MenuItem>
  //     </Menu>
  //   </>
  // );

  const MonthColumn = (props: any) => {
    const { prevue, reel, item, editionDisabled, fontWeight, params } = props;

    return (
      <>
        <td {...props}>
          <div style={{ width: '100%', minHeight: 110, display: 'flex', cursor: 'pointer' }}>
            <div
              style={{
                textAlign: 'right',
                background: prevue?.background || 'unset',
                flexBasis: '100%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <CustomGridCell
                onRequestSaveInput={(value: string) => {
                  if (savingInputRemuneration?.onRequestSaveInput) {
                    // eslint-disable-next-line no-unused-expressions
                    savingInputRemuneration?.onRequestSaveInput(value, params);
                  }
                }}
                onClick={() => {
                  if (params?.column) {
                    onRequestShowDetails && onRequestShowDetails({ ...params, type: 'PREVUE' });
                    setOpen && setOpen(true);
                  }
                }}
                bodyValue={prevue?.value}
                bodyElement={
                  <Typography style={{ fontFamily: 'Roboto', fontSize: 14, fontWeight }}>
                    {prevue?.value && `${parseFloat(prevue?.value || '0').toFixed(2)} €`}
                  </Typography>
                }
                editionDisabled={editionDisabled}
                labelOnly={false}
              />
            </div>
          </div>
          <div style={{ width: '100%', minHeight: 110, display: 'flex', cursor: 'pointer' }}>
            <div
              style={{
                textAlign: 'right',
                background: reel?.background || 'unset',
                color: reel?.background ? '#FFFFFF' : 'unset',
                flexBasis: '100%',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                borderTop: '1px solid #E3E3E3',
              }}
            >
              <CustomGridCell
                onRequestSaveInput={(value: string) => {
                  if (savingInputRemuneration?.onRequestSaveInput) {
                    // eslint-disable-next-line no-unused-expressions
                    savingInputRemuneration?.onRequestSaveInput(value, item);
                  }
                }}
                onClick={() => {
                  if (params?.column) {
                    onRequestShowDetails && onRequestShowDetails({ ...params, type: 'REALISEE' });
                    setOpen && setOpen(true);
                  }
                }}
                bodyValue={reel?.value}
                bodyElement={
                  <Typography style={{ fontFamily: 'Roboto', fontSize: 16, fontWeight }}>
                    {`${parseFloat(reel?.value || '0').toFixed(2)} €`}
                  </Typography>
                }
                footerValue={reel?.percentage}
                footerElement={
                  <Typography style={{ fontFamily: 'Roboto', fontWeight: 'bold', fontSize: 18 }}>
                    {reel?.percentage ? `${round2Decimals(reel?.percentage)}%` : `0%`}
                  </Typography>
                }
                editionDisabled={editionDisabled}
                loading={savingInputRemuneration?.loading}
                labelOnly
              />
            </div>
          </div>
        </td>
      </>
    );
  };

  const columnWithChildren = (title: string) => {
    return (
      <GridColumn
        title={title}
        headerClassName={classnames(classes.header, classes.alignCenter, classes.monthHeader)}
        children={[
          // eslint-disable-next-line react/jsx-key
          <GridColumn
            editable
            width="100px"
            title="Autres rém"
            resizable
            minResizableWidth={100}
            cell={(props: any) => {
              const item = props.dataItem;
              const month = (item?.months || []).find((el: any) => getMonthNumber(title) === el.month);
              return (
                <MonthColumn
                  {...props}
                  {...{
                    item,
                    prevue: month?.prevue?.coop,
                    reel: month?.reel?.coop,
                    params: {
                      month: getMonthNumber(title),
                      column: 'Autres Rém.',
                      type: 'PREVUE',
                      idPartenaire: month.idPartenaire,
                    },
                  }}
                />
              );
            }}
            headerClassName={classnames(classes.header, classes.alignCenter, classes.monthHeader)}
          />,
          // eslint-disable-next-line react/jsx-key
          <GridColumn
            editable
            width="100px"
            title="BRI"
            resizable
            minResizableWidth={100}
            cell={(props: any) => {
              const item = props.dataItem;
              const month = (item?.months || []).find((el: any) => getMonthNumber(title) === el.month);
              return (
                <>
                  <MonthColumn
                    {...props}
                    {...{
                      item,
                      prevue: month?.prevue?.bri,
                      reel: month?.reel?.bri,
                      params: {
                        month: getMonthNumber(title),
                        column: 'BRI',
                        type: 'PREVUE',
                        idPartenaire: month.idPartenaire,
                      },
                    }}
                  />
                </>
              );
            }}
            headerClassName={classnames(classes.header, classes.alignCenter, classes.monthHeader)}
          />,
          // eslint-disable-next-line react/jsx-key
          <GridColumn
            width="100px"
            title="Total"
            resizable
            minResizableWidth={100}
            cell={(props: any) => {
              const item = props.dataItem;
              const month = (item?.months || []).find((el: any) => getMonthNumber(title) === el.month);
              return (
                <MonthColumn
                  {...props}
                  {...{
                    item,
                    prevue: month?.prevue?.total,
                    reel: month?.reel?.total,
                    editionDisabled: true,
                    fontWeight: 'bold',
                  }}
                />
              );
            }}
            headerClassName={classnames(classes.header, classes.alignCenter, classes.monthHeader)}
          />,
        ]}
      />
    );
  };

  const LaboratoireColumn = (
    <GridColumn
      title="Laboratoire"
      width="180px"
      locked
      resizable
      minResizableWidth={180}
      headerClassName={classnames(classes.header)}
      cell={(props: any) => (
        <td {...props}>
          <div style={{ padding: '12px 8px' }}>
            <Typography style={{ fontFamily: 'Roboto', fontSize: 14, textTransform: 'uppercase' }}>
              {props.dataItem.laboratoire}
            </Typography>
          </div>
        </td>
      )}
    />
  );

  const TotalColumn = (
    <GridColumn
      title="Total"
      width="100px"
      locked
      resizable
      minResizableWidth={100}
      cell={(props: any) => {
        const item = props.dataItem;
        return (
          <MonthColumn
            {...props}
            {...{
              item,
              prevue: item?.total?.prevue,
              reel: item?.total?.reel,
              editionDisabled: true,
              fontWeight: 'bold',
            }}
          />
        );
      }}
      headerClassName={classnames(classes.header, classes.alignCenter, classes.monthHeader)}
    />
  );

  // const TotalVerseColumn = (
  //   <GridColumn
  //     locked
  //     width="100px"
  //     title="Total Versé"
  //     resizable
  //     minResizableWidth={100}
  //     cell={(props: any) => {
  //       const item = props.dataItem;
  //       return (
  //         <MonthColumn
  //           {...props}
  //           {...{
  //             item,
  //             prevue: item?.totalVerse?.prevue,
  //             reel: item?.totalVerse?.reel,
  //             editionDisabled: true,
  //             fontWeight: 'bold',
  //           }}
  //         />
  //       );
  //     }}
  //     headerClassName={classnames(classes.header, classes.alignCenter, classes.monthHeader)}
  //   />
  // );

  // const CompositionColumn = (
  //   <GridColumn
  //     locked
  //     width="235px"
  //     title="Composition de la préstation"
  //     resizable
  //     minResizableWidth={235}
  //     headerClassName={classnames(classes.header, classes.alignCenter)}
  //     cell={(props: any) => (
  //       <td {...props}>
  //         <div style={{ padding: '12px 8px' }}>
  //           <Typography style={{ fontFamily: 'Roboto', fontSize: 14 }}>{props.dataItem.composition}</Typography>
  //         </div>
  //       </td>
  //     )}
  //   />
  // );

  const RemunerationColumn = (
    <GridColumn
      locked
      width="120px"
      title="Rémunération prévue / payée"
      resizable
      minResizableWidth={120}
      cell={(props: any) => (
        <>
          <td {...props}>
            <div
              style={{
                width: '100%',
                minHeight: 110,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Typography className={classes.renumerationFont}>Rémunération prévue</Typography>
            </div>
            <div
              style={{
                width: '100%',
                minHeight: 110,
                display: 'flex',
                borderTop: '1px solid #E3E3E3',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Typography className={classes.renumerationFont}>Rémunération payée</Typography>
            </div>
          </td>
        </>
      )}
      className={classes.innerCell}
      headerClassName={classnames(classes.header, classes.renumerationHeader, classes.alignCenter)}
    />
  );

  const Columns = months.map((month) => columnWithChildren(month));
  return (
    <div>
      <CustomGrid
        columns={[
          LaboratoireColumn,
          //CompositionColumn,
          RemunerationColumn,
          Columns,
          TotalColumn,
          // TotalVerseColumn,
        ]}
        data={data}
        className={classes.grid}
        resizable
      />
      <DetailRemuneration open={open} setOpen={setOpen} details={details} />
    </div>
  );
};
