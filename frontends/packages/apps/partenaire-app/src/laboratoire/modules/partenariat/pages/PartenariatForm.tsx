import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  Switch,
  Typography,
  withStyles,
} from '@material-ui/core';
import React, { FC, ReactNode, useEffect, useState } from 'react';
import { useStyles } from './styles';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import moment from 'moment';
import { PartenariatValue, PartenatiatFormProps } from './interface';
import { CustomDatePicker, CustomFormTextField, CustomModal, CustomSelect } from '@lib/ui-kit';
import { useApplicationContext } from '@lib/common';
import UserInput from '@lib/common/src/components/UserInput';
import { ExpandMore } from '@material-ui/icons';
import { PrtContactInfoFragment } from '@lib/common/src/federation';
import { typeReunions } from '../../../util';

const dateDebut = moment(new Date()).startOf('year').toDate();
const dateFin = moment(new Date()).endOf('year').toDate();

const listOptionNotifications = [
  {
    id: '1',
    code: '1',
    libelle: 'Automatique',
  },
  {
    id: '2',
    code: '0',
    libelle: 'Manuelle',
  },
];

const emailOptionValue = {
  RENDEZVOUS: 'NONE',
  COMPTERENDU: 'NONE',
  LITIGES: 'NONE',
  OFFREPROMOTIONNELLE: 'NONE',
  MISEENAVANT: 'NONE',
  CHALLENGE: 'NONE',
};

const correspondantValue = {
  LITIGE_CORRESPONDANT: [],
  COMPTERENDU_CORRESPONDANT: [],
  RENDEZVOUS_CORRESPONDANT: [],
};

export const PartenariatForm: FC<PartenatiatFormProps> = ({
  open,
  onSave,
  typesPartenariats,
  statutsPartenariats,
  setOpen,
  idLabo,
  partenariat,
}) => {
  const styles = useStyles();
  const { isMobile } = useApplicationContext();
  const [userResponsables, setUserResponsables] = useState<any>([]);
  const [partenariatValuesInput, setPartenariatValuesInput] = useState<PartenariatValue>({
    idLabo: '',
    dateDebut,
    dateFin,
    idUserResponsables: [],
    typeReunion: 'VISIO',
  });
  const [emailOptionInput, setEmailOptionInput] =
    useState<
      Record<'RENDEZVOUS' | 'COMPTERENDU' | 'LITIGES' | 'OFFREPROMOTIONNELLE' | 'MISEENAVANT' | 'CHALLENGE', string>
    >(emailOptionValue);
  const [correspondantInput, setCorrespondantInput] =
    useState<
      Record<
        'LITIGE_CORRESPONDANT' | 'COMPTERENDU_CORRESPONDANT' | 'RENDEZVOUS_CORRESPONDANT',
        PrtContactInfoFragment[]
      >
    >(correspondantValue);

  console.log('correspondant', correspondantInput);

  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openModalResponsables, setOpenModalResponsables] = useState<boolean>(false);
  const [openCorrespondantLitige, setOpenCorrespondantLitige] = useState<boolean>(false);
  const [openCorrespondantCompteRendu, setOpenCorrespondantCompteRendu] = useState<boolean>(false);
  const [openCorrespondantRdv, setOpenCorrespondantRdv] = useState<boolean>(false);

  useEffect(() => {
    setOpenModal(open);
  }, [open]);

  useEffect(() => {
    if (!open) {
      setPartenariatValuesInput({
        idLabo,
        dateDebut,
        dateFin,
        idUserResponsables: [],
        typeReunion: 'VISIO',
      });
      setUserResponsables([]);
      setCorrespondantInput({
        LITIGE_CORRESPONDANT: [],
        COMPTERENDU_CORRESPONDANT: [],
        RENDEZVOUS_CORRESPONDANT: [],
      });
    }
  }, [open]);

  useEffect(() => {
    if (open && partenariat) {
      setUserResponsables(partenariat?.responsables || []);
      setPartenariatValuesInput({
        id: partenariat?.id,
        idLabo,
        idType: partenariat?.type?.id,
        idStatut: partenariat?.statut?.id,
        dateDebut: moment.utc(partenariat?.dateDebut).startOf('day').toDate(),
        dateFin: moment.utc(partenariat?.dateFin).startOf('day').toDate(),
        idUserResponsables: (userResponsables || []).map(({ id }: any) => id),
        typeReunion: partenariat?.typeReunion || 'VISIO',
      });

      const emailOption = (partenariat.optionNotifications || []).reduce((result, option) => {
        return {
          ...result,
          [option.code]: option.valeur,
        };
      }, {});

      const correspondantOptions = (partenariat.partenariatCorrespondants || []).reduce((result, option) => {
        return {
          ...result,
          [option.code]: [...((result as any)[option.code] || []), option.contact],
        };
      }, {});

      setCorrespondantInput({
        ...correspondantValue,
        ...correspondantOptions,
      });
      setEmailOptionInput({
        ...emailOptionValue,
        ...emailOption,
      });
    }
  }, [partenariat, open]);

  const handleDateDebutChange = (date: MaterialUiPickersDate) => {
    setPartenariatValuesInput((prev: any) => ({ ...prev, idLabo, dateDebut: date }));
  };

  const handleDateFinChange = (date: MaterialUiPickersDate) => {
    setPartenariatValuesInput((prev: any) => ({ ...prev, idLabo, dateFin: date }));
  };

  const handleChange = (event: React.ChangeEvent<{ name: string; value: unknown }>) => {
    const { value, name } = event.target;
    setPartenariatValuesInput((prev) => ({ ...prev, [name]: value }));
  };

  const handleChangetype = (
    event: React.ChangeEvent<{
      name?: string | undefined;
      value: unknown;
    }>,
    __child: React.ReactNode
  ) => {
    if (partenariatValuesInput) {
      setPartenariatValuesInput({
        ...partenariatValuesInput,
        idType: event.target.value as string,
      });
    }
  };

  const handleChangeStatut = (
    event: React.ChangeEvent<{
      name?: string | undefined;
      value: unknown;
    }>,
    __child: React.ReactNode
  ) => {
    if (partenariatValuesInput) {
      setPartenariatValuesInput({
        ...partenariatValuesInput,
        idStatut: event.target.value as string,
      });
    }
  };

  const handleChangeTypeReunion = (
    event: React.ChangeEvent<{
      name?: string | undefined;
      value: unknown;
    }>
  ) => {
    if (partenariatValuesInput) {
      setPartenariatValuesInput({
        ...partenariatValuesInput,
        typeReunion: event.target.value as string,
      });
    }
  };

  const handleChangeSwitch = (event: React.ChangeEvent<HTMLInputElement>, value: string) => {
    setEmailOptionInput({
      ...emailOptionInput,
      [event.target.name]: event.target.checked ? value : 'NONE',
    });
  };

  const handleChangeOption = (event: React.ChangeEvent<HTMLInputElement>, value: string) => {
    const key = event.target.name;
    setEmailOptionInput((prev) => ({
      ...prev,
      [key]: key === 'RENDEZVOUS' ? parseInt(value) : value,
    }));
  };

  const handleSubmit = () => {
    if (onSave) {
      onSave({
        ...partenariatValuesInput,
        id: partenariat?.id,
        idUserResponsables: (userResponsables || []).map(({ id }: any) => id),
        optionNotifications: Object.keys(emailOptionInput as any).map((key) => {
          return {
            code: key,
            value: key === 'RENDEZVOUS' ? emailOptionInput[key].toString() : (emailOptionInput as any)[key],
          };
        }),
        correspondants: Object.keys(correspondantInput as any).map((key) => {
          return {
            code: key,
            value: (correspondantInput as any)[key],
          };
        }),
      });

      setOpen();
      setOpenModal(false);
    }
  };

  const isValid = () => {
    return (
      partenariatValuesInput.dateDebut &&
      partenariatValuesInput.dateFin &&
      partenariatValuesInput.idStatut &&
      partenariatValuesInput.idType &&
      partenariatValuesInput.dateDebut < partenariatValuesInput.dateFin
    );
  };

  const CustomAccordeonSummary = withStyles({
    expandIcon: {
      order: -1,
    },
  })(AccordionSummary);
  console.log('------------------isValid-------------------------------------', isValid());

  return (
    <CustomModal
      title="Definir partenariat"
      open={openModal}
      setOpen={setOpen}
      withBtnsActions
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      disabledButton={!isValid()}
      onClickConfirm={handleSubmit}
      fullScreen={!!isMobile}
      className={styles.dialogPaperFullWidth}
    >
      <div className={styles.root}>
        <Box display={isMobile ? undefined : 'flex'} justifyContent={isMobile ? undefined : 'space-between'}>
          <Box py={0} className={styles.noMarginBottom} width={isMobile ? undefined : 'calc(50% - 5px)'}>
            <CustomDatePicker
              style={{ width: '100%' }}
              name="dateDebut"
              value={partenariatValuesInput?.dateDebut || moment(new Date()).startOf('year').toDate()}
              onChange={handleDateDebutChange}
              required
              label="Date début de partenariat"
            />
          </Box>
          <Box py={0} className={styles.noMarginBottom} width={isMobile ? undefined : 'calc(50% - 5px)'}>
            <CustomDatePicker
              style={{ width: '100%' }}
              name="dateFin"
              value={partenariatValuesInput?.dateFin || moment(new Date()).endOf('year').toDate()}
              onChange={handleDateFinChange}
              label="Date fin de partenariat"
            />
          </Box>
        </Box>

        <Box className={styles.noMarginBottom}>
          <CustomSelect
            list={statutsPartenariats}
            index="libelle"
            listId="id"
            label="Statut de partenariat"
            value={partenariatValuesInput?.idStatut || undefined}
            onChange={handleChangeStatut}
            required
          />
        </Box>

        <Box className={styles.noMarginBottom}>
          <CustomSelect
            list={typesPartenariats}
            index="libelle"
            listId="id"
            label="Type de partenariat"
            value={partenariatValuesInput?.idType || undefined}
            onChange={handleChangetype}
            required
          />
        </Box>
        <Box className={styles.noMarginBottom}>
          <UserInput
            openModal={openModalResponsables}
            withNotAssigned={false}
            setOpenModal={setOpenModalResponsables}
            selected={userResponsables}
            setSelected={setUserResponsables}
            label="Responsables"
          />
        </Box>
        <Box className={styles.noMarginBottom}>
          <CustomSelect
            list={typeReunions}
            index="libelle"
            listId="code"
            label="Type Rendez-Vous"
            value={partenariatValuesInput?.typeReunion || 'VISIO'}
            onChange={handleChangeTypeReunion}
            required
          />
        </Box>
        <Box>
          <Accordion elevation={0}>
            <CustomAccordeonSummary
              expandIcon={<ExpandMore />}
              IconButtonProps={{
                edge: 'start',
              }}
              style={{ paddingLeft: 0 }}
            >
              <Typography>Option correspondants</Typography>
            </CustomAccordeonSummary>
            <AccordionDetails style={{ padding: 0 }}>
              <FormControl component="fieldset" style={{ width: '100%' }}>
                <Box className={styles.noMarginCorrespondant}>
                  <UserInput
                    key="LITIGES_CORRESPONDANT"
                    openModal={openCorrespondantLitige}
                    idPartenaireTypeAssocie={idLabo}
                    withAssignTeam={false}
                    withNotAssigned={false}
                    partenaireType="LABORATOIRE"
                    category="CONTACT"
                    setOpenModal={setOpenCorrespondantLitige}
                    selected={correspondantInput.LITIGE_CORRESPONDANT}
                    setSelected={(selected) =>
                      setCorrespondantInput((prev) => ({ ...prev, LITIGE_CORRESPONDANT: selected } as any))
                    }
                    label="Litiges"
                    title="Représentant(s) Labo"
                    disabled={!idLabo}
                  />
                </Box>
                <Box className={styles.noMarginCorrespondant}>
                  <UserInput
                    key="COMPTE_RENDU_CORRESPONDANT"
                    openModal={openCorrespondantCompteRendu}
                    idPartenaireTypeAssocie={idLabo}
                    withAssignTeam={false}
                    withNotAssigned={false}
                    partenaireType="LABORATOIRE"
                    category="CONTACT"
                    setOpenModal={setOpenCorrespondantCompteRendu}
                    selected={correspondantInput.COMPTERENDU_CORRESPONDANT}
                    setSelected={(selected) => {
                      setCorrespondantInput((prev) => ({ ...prev, COMPTERENDU_CORRESPONDANT: selected } as any));
                    }}
                    label="Compte-rendu"
                    title="Représentant(s) Labo"
                    disabled={!idLabo}
                  />
                </Box>
                <Box className={styles.noMarginCorrespondant}>
                  <UserInput
                    key="RENDEZ_VOUS"
                    openModal={openCorrespondantRdv}
                    idPartenaireTypeAssocie={idLabo}
                    withAssignTeam={false}
                    withNotAssigned={false}
                    partenaireType="LABORATOIRE"
                    category="CONTACT"
                    setOpenModal={setOpenCorrespondantRdv}
                    selected={correspondantInput.RENDEZVOUS_CORRESPONDANT}
                    setSelected={(selected) =>
                      setCorrespondantInput((prev) => ({ ...prev, RENDEZVOUS_CORRESPONDANT: selected } as any))
                    }
                    label="Rendez-vous"
                    title="Représentant(s) Labo"
                    disabled={!idLabo}
                  />
                </Box>
              </FormControl>
            </AccordionDetails>
          </Accordion>
        </Box>
        <Box>
          <Accordion elevation={0}>
            <CustomAccordeonSummary
              expandIcon={<ExpandMore />}
              aria-controls="panel1a-content"
              id="panel1a-header"
              IconButtonProps={{
                edge: 'start',
              }}
              style={{ paddingLeft: 0 }}
            >
              <Typography>Option envoie d'email</Typography>
            </CustomAccordeonSummary>
            <AccordionDetails style={{ paddingLeft: 0 }}>
              <FormControl component="fieldset">
                <FormGroup style={{ minWidth: 300 }}>
                  <FormControlLabel
                    className={styles.spaceBetwen}
                    control={
                      <Switch
                        checked={emailOptionInput.RENDEZVOUS !== 'NONE'}
                        onChange={(e) => handleChangeSwitch(e, '15')}
                        name="RENDEZVOUS"
                      />
                    }
                    label="Rappel Rendez-vous"
                    labelPlacement="start"
                  />
                  {emailOptionInput.RENDEZVOUS !== 'NONE' && (
                    <Box display="flex" flexDirection="row" alignItems="center">
                      <Box pr={2}>
                        <Typography>J- </Typography>
                      </Box>
                      <CustomFormTextField
                        name="RENDEZVOUS"
                        type="number"
                        onChange={(e: any) => handleChangeOption(e, e.target.value)}
                        value={emailOptionInput.RENDEZVOUS}
                      />
                    </Box>
                  )}
                  <FormControlLabel
                    className={styles.spaceBetwen}
                    control={
                      <Switch
                        checked={emailOptionInput.COMPTERENDU !== 'NONE'}
                        onChange={(e) => handleChangeSwitch(e, '0')}
                        name="COMPTERENDU"
                      />
                    }
                    label="Compte Rendu"
                    labelPlacement="start"
                  />
                  {emailOptionInput.COMPTERENDU !== 'NONE' && (
                    <CustomSelect
                      list={listOptionNotifications}
                      index="libelle"
                      listId="code"
                      name="COMPTERENDU"
                      value={emailOptionInput.COMPTERENDU}
                      onChange={(e: any) => handleChangeOption(e, e.target.value)}
                    />
                  )}
                  <FormControlLabel
                    className={styles.spaceBetwen}
                    control={
                      <Switch
                        checked={emailOptionInput.LITIGES !== 'NONE'}
                        onChange={(e) => handleChangeSwitch(e, '0')}
                        name="LITIGES"
                      />
                    }
                    label="Litiges"
                    labelPlacement="start"
                  />
                  {emailOptionInput.LITIGES !== 'NONE' && (
                    <CustomSelect
                      list={listOptionNotifications}
                      index="libelle"
                      listId="code"
                      name="LITIGES"
                      value={emailOptionInput.LITIGES}
                      onChange={(e: any) => handleChangeOption(e, e.target.value)}
                    />
                  )}
                  <FormControlLabel
                    className={styles.spaceBetwen}
                    control={
                      <Switch
                        checked={emailOptionInput.OFFREPROMOTIONNELLE !== 'NONE'}
                        onChange={(e) => handleChangeSwitch(e, '0')}
                        name="OFFREPROMOTIONNELLE"
                      />
                    }
                    label="Offre Promotionnelle"
                    labelPlacement="start"
                  />
                  {emailOptionInput.OFFREPROMOTIONNELLE !== 'NONE' && (
                    <CustomSelect
                      list={listOptionNotifications}
                      index="libelle"
                      listId="code"
                      name="OFFREPROMOTIONNELLE"
                      value={emailOptionInput.OFFREPROMOTIONNELLE}
                      onChange={(e: any) => handleChangeOption(e, e.target.value)}
                    />
                  )}
                  <FormControlLabel
                    className={styles.spaceBetwen}
                    control={
                      <Switch
                        checked={emailOptionInput.MISEENAVANT !== 'NONE'}
                        onChange={(e) => handleChangeSwitch(e, '0')}
                        name="MISEENAVANT"
                      />
                    }
                    label="Mise en avant"
                    labelPlacement="start"
                  />
                  {emailOptionInput.MISEENAVANT !== 'NONE' && (
                    <CustomSelect
                      list={listOptionNotifications}
                      index="libelle"
                      listId="code"
                      name="MISEENAVANT"
                      value={emailOptionInput.MISEENAVANT}
                      onChange={(e: any) => handleChangeOption(e, e.target.value)}
                    />
                  )}
                  <FormControlLabel
                    className={styles.spaceBetwen}
                    control={
                      <Switch
                        checked={emailOptionInput.CHALLENGE !== 'NONE'}
                        onChange={(e) => handleChangeSwitch(e, '0')}
                        name="CHALLENGE"
                      />
                    }
                    label="BRI équipe"
                    labelPlacement="start"
                  />
                  {emailOptionInput.CHALLENGE !== 'NONE' && (
                    <CustomSelect
                      list={listOptionNotifications}
                      index="libelle"
                      listId="code"
                      name="CHALLENGE"
                      value={emailOptionInput.CHALLENGE}
                      onChange={(e: any) => handleChangeOption(e, e.target.value)}
                    />
                  )}
                </FormGroup>
              </FormControl>
            </AccordionDetails>
          </Accordion>
        </Box>
      </div>
    </CustomModal>
  );
};
