import { makeStyles, createStyles } from '@material-ui/core';
export const useStyles = makeStyles(() =>
  createStyles({
    root: {
      width: '100%',
      padding: '16px 0',
      '& .MuiFormControl-root': {
        marginBottom: 0,
      },
    },
    datePiker: {
      marginBottom: 16,
      width: '100%',
    },
    formControl: {
      width: '100%',
      margin: '16px 0 24px 0',
    },
    btnActionContainer: {
      width: '100%',
      display: 'flex',
      justifyContent: 'flex-end',
    },
    dialogPaperFullWidth: {
      '& .MuiDialog-paper, & .main-MuiPaper-root': {
        width: '100%',
      },
    },
    noMarginBottom: {
      marginBottom: 20,
      '& .main-MuiFormControl-root, & .MuiFormControl-root': {
        marginBottom: '0 !important',
      },
    },
    noMarginCorrespondant: {
      marginBottom: 0,
      '& .main-MuiFormControl-root, & .MuiFormControl-root': {
        marginBottom: '0 !important',
      },
    },
    spaceBetwen: {
      justifyContent: 'space-between',
    },
  })
);
