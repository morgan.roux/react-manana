import { User } from '@lib/types';
import { ReactNode } from 'react';
import {
  PartenariatTypeInfoFragment,
  PartenariatStatutInfoFragment,
  PartenariatInfoFragment,
} from '@lib/common/src/federation';

export interface PartenariatValue {
  idLabo: string;
  id?: string;
  dateDebut: any;
  dateFin: any;
  idType?: string;
  idStatut?: string;
  idUserResponsables: string[];
  typeReunion: string;
  optionNotifications?: any[];
  correspondants?: any[];
}

export interface PartenatiatFormProps {
  open: boolean;
  setOpen: () => void;
  onSave?: (data: PartenariatValue) => void;
  idLabo: string;
  partenariat?: PartenariatInfoFragment;
  typesPartenariats: PartenariatTypeInfoFragment[];
  statutsPartenariats: PartenariatStatutInfoFragment[];
}
