import { FullDocumentCategorieFragment } from '@lib/common/src/federation';
import { Box, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import React from 'react';
import { Column } from '@lib/ui-kit';
import { useStyles } from './styles';
import moment from 'moment';
import { Delete, Edit, MoreHoriz, Visibility } from '@material-ui/icons';

export interface ColumnContratProps {
  handleRowDetail: (document: FullDocumentCategorieFragment) => void;
  handleShowMenuClick: (document: FullDocumentCategorieFragment, event: React.MouseEvent<HTMLElement>) => void;

  handleClose: () => void;
  anchorEl: HTMLElement | null;
  open: boolean;
  isAuthorizedToAddDocument?: boolean;
  handleModifier?: () => void;
  handleSupprimer?: () => void;
  documentToShow?: FullDocumentCategorieFragment;
}

export const useColumn = ({
  handleRowDetail,
  handleShowMenuClick,
  anchorEl,
  open,
  handleClose,
  isAuthorizedToAddDocument,
  handleModifier,
  handleSupprimer,
  documentToShow,
}: ColumnContratProps) => {
  const classes = useStyles();

  const columns: Column[] = [
    {
      name: 'description',
      label: 'Titre',
      sortable: true,
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box onClick={() => handleRowDetail(row)}>
            <Typography dangerouslySetInnerHTML={{ __html: row?.description || '' }} className={classes.textTable} />
          </Box>
        );
      },
    },
    {
      name: 'idOrigineAssocie',
      label: 'Origine',
      sortable: true,
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box onClick={() => handleRowDetail(row)}>
            <Typography className={classes.textTable}>{(row?.origineAssocie as any)?.nom}</Typography>
          </Box>
        );
      },
    },
    {
      name: 'datedebut',
      label: 'Date début',
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box display="flex" className={classes.textTable} alignItems="center" onClick={() => handleRowDetail(row)}>
            {row?.dateHeureDebutValidite ? moment.utc(row?.dateHeureDebutValidite).format('DD/MM/YYYY HH:mm') : '-'}
          </Box>
        );
      },
    },
    {
      name: 'datefin',
      label: 'Date fin',
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box display="flex" className={classes.textTable} alignItems="center" onClick={() => handleRowDetail(row)}>
            {row?.dateHeureFinValidite ? moment.utc(row?.dateHeureFinValidite).format('DD/MM/YYYY HH:mm') : '-'}
          </Box>
        );
      },
    },
    {
      name: 'tacite',
      label: 'Renouvellement par tacite',
      sortable: true,
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <Box onClick={() => handleRowDetail(row)}>
            <Typography className={classes.textTable}>{row?.isRenouvellementTacite ? 'Oui' : 'Non'}</Typography>
          </Box>
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: FullDocumentCategorieFragment) => {
        return (
          <div key={`row-${row.id}`}>
            <IconButton
              aria-controls="simple-menu"
              aria-haspopup="true"
              // eslint-disable-next-line react/jsx-no-bind
              onClick={handleShowMenuClick.bind(null, row)}
            >
              <MoreHoriz />
            </IconButton>
            <Menu
              id="fade-menu"
              anchorEl={anchorEl}
              keepMounted
              open={open && row.id === documentToShow?.id}
              onClose={handleClose}
            >
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  handleRowDetail(row);
                }}
              >
                <ListItemIcon>
                  <Visibility />
                </ListItemIcon>
                <Typography variant="inherit">Voir détail</Typography>
              </MenuItem>
              {isAuthorizedToAddDocument && (
                <MenuItem
                  onClick={(event) => {
                    event.stopPropagation();
                    handleClose();
                    handleModifier && handleModifier();
                  }}
                >
                  <ListItemIcon>
                    <Edit />
                  </ListItemIcon>
                  <Typography variant="inherit">Modifier</Typography>
                </MenuItem>
              )}
              {isAuthorizedToAddDocument && (
                <MenuItem
                  onClick={(event) => {
                    event.stopPropagation();
                    handleClose();
                    handleSupprimer && handleSupprimer();
                  }}
                >
                  <ListItemIcon>
                    <Delete />
                  </ListItemIcon>
                  <Typography variant="inherit">Supprimer</Typography>
                </MenuItem>
              )}
            </Menu>
          </div>
        );
      },
    },
  ];

  return columns;
};
