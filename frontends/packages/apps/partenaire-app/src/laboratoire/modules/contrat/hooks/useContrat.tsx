import { useState } from 'react';
import {
  FullDocumentCategorieFragment,
  useCreate_Document_CategorieMutation,
  useDelete_Document_CategorieMutation,
  useUpdate_Document_CategorieMutation,
} from '@lib/common/src/federation';
import { useApplicationContext, useUploadFiles } from '@lib/common';
import { Dispatch } from 'react';

interface UseHistoriqueProps {
  handleDeleteDocument: (doc: FullDocumentCategorieFragment) => void;
  handleReplaceDocument: (doc: any) => void;
  saving: boolean;
  saved: boolean;
  setSaved: Dispatch<boolean>;
}

export const useContrat = (): UseHistoriqueProps => {
  const { notify, graphql } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);

  const [addDocumentCategorie] = useCreate_Document_CategorieMutation({
    onCompleted: (data) => {
      setSaving(false);
      setSaved(true);
      notify({
        message: 'Le contrat a été ajoutée avec succès !',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout du contrat!",
        type: 'error',
      });

      setSaving(false);
    },
  });

  const showError = () => {
    notify({
      message: `Une erreur est survenue pendant le chargement du document`,
      type: 'error',
    });
  };

  const [updateDocumentCategorie] = useUpdate_Document_CategorieMutation({
    onCompleted: () => {
      setSaving(false);
      setSaved(true);
      notify({
        message: 'Le contrat a été modifiée avec succès !',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du contrat!',
        type: 'error',
      });
      setSaving(false);
    },
  });

  const [deleteDocument] = useDelete_Document_CategorieMutation({
    onCompleted: () => {
      notify({
        message: 'Ce document a été suprimée avec succès.',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la supression du contrat.',
        type: 'error',
      });
    },
  });

  const upsertDocument = ({
    id,
    idSousCategorie,
    description,
    nomenclature,
    numeroVersion,
    idUserVerificateur,
    idUserRedacteur,
    dateHeureParution,
    dateHeureDebutValidite,
    dateHeureFinValidite,
    motCle1,
    motCle2,
    motCle3,
    fichier,
    idOrigine,
    idOrigineAssocie,
    dateFacture,
    hT,
    tva,
    ttc,
    type,

    numeroFacture,
    factureDateReglement,
    idReglementMode,
    numeroCommande,
    isGenererCommande,
    avoirType,
    avoirCorrespondants,
    nombreJoursPreavis,
    isRenouvellementTacite,
    isMultipleTva,
    factureTvas,
  }: any): void => {
    if (!id) {
      // setMode('creation');
      addDocumentCategorie({
        variables: {
          input: {
            idSousCategorie: idSousCategorie,
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: new Date(dateHeureParution),
            dateHeureDebutValidite: new Date(dateHeureDebutValidite),
            dateHeureFinValidite: new Date(dateHeureFinValidite),
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,
            statut: 'APPROUVE',

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
            nombreJoursPreavis,
            isRenouvellementTacite,
            isMultipleTva,
            facturesTva: factureTvas,
          },
        },
      });
    } else {
      // setMode('remplacement');
      updateDocumentCategorie({
        variables: {
          input: {
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: dateHeureParution,
            dateHeureDebutValidite: dateHeureDebutValidite,
            dateHeureFinValidite: dateHeureFinValidite,
            idSousCategorie: idSousCategorie,
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,
            statut: 'APPROUVE',

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
            nombreJoursPreavis,
            isRenouvellementTacite,
            isMultipleTva,
            facturesTva: factureTvas,
          },
          id: id,
        },
      });
    }
  };

  const handleReplaceDocument = (doc: any): void => {
    setSaving(true);
    setSaved(false);
    if (doc.launchUpload) {
      uploadFiles([doc.selectedFile], {
        directory: 'relation-fournisseurs/contrat',
      })
        .then((uploadedFiles) => {
          upsertDocument({
            id: doc.id,
            idSousCategorie: doc.idSousCategorie,
            description: doc.description,
            nomenclature: doc.nomenclature,
            numeroVersion: doc.numeroVersion,
            idUserVerificateur: doc.idUserVerificateur,
            idUserRedacteur: doc.idUserRedacteur,
            dateHeureParution: doc.dateHeureParution,
            dateHeureDebutValidite: doc.dateHeureDebutValidite,
            dateHeureFinValidite: doc.dateHeureFinValidite,
            motCle1: doc.motCle1,
            motCle2: doc.motCle2,
            motCle3: doc.motCle3,
            fichier: uploadedFiles[0],
            idOrigine: doc?.origine?.id || doc?.origine,
            idOrigineAssocie: doc.correspondant.id,
            dateFacture: doc.dateFacture,
            hT: doc.hT ? parseFloat(doc.hT) : undefined,
            tva: doc.tva ? parseFloat(doc.tva) : undefined,
            ttc: doc.ttc ? parseFloat(doc.ttc) : undefined,
            type: doc.type,

            numeroFacture: doc?.numeroFacture,
            factureDateReglement: doc?.factureDateReglement,
            idReglementMode: doc?.idReglementMode,
            numeroCommande: doc?.numeroCommande,
            isGenererCommande: doc?.isGenererCommande,
            avoirType: doc?.avoirType,
            avoirCorrespondants: doc?.avoirCorrespondants,
            nombreJoursPreavis: doc?.nombreJoursPreavis,
            isRenouvellementTacite: doc?.isRenouvellementTacite,
            isMultipleTva: doc?.isMultipleTva,
            factureTvas: doc?.factureTvas,
          });
        })
        .catch(() => {
          setSaving(false);
          showError();
        });
    } else {
      const { nomOriginal, chemin, type } = doc.previousFichier || {};

      upsertDocument({
        id: doc.id,
        idSousCategorie: doc.idSousCategorie,
        description: doc.description,
        nomenclature: doc.nomenclature,
        numeroVersion: doc.numeroVersion,
        idUserVerificateur: doc.idUserVerificateur,
        idUserRedacteur: doc.idUserRedacteur,
        dateHeureParution: doc.dateHeureParution,
        dateHeureDebutValidite: doc.dateHeureDebutValidite,
        dateHeureFinValidite: doc.dateHeureFinValidite,
        motCle1: doc.motCle1,
        motCle2: doc.motCle2,
        motCle3: doc.motCle3,
        fichier: doc.previousFichier ? { nomOriginal, chemin, type } : undefined,
        idOrigine: doc.origine?.id,
        idOrigineAssocie: doc.correspondant?.id,
        dateFacture: 'FACTURE' === doc.type ? doc.dateFacture : undefined,
        hT: 'FACTURE' === doc.type && doc.hT ? parseFloat(doc.hT) : undefined,
        tva: 'FACTURE' === doc.type && doc.tva ? parseFloat(doc.tva) : undefined,
        ttc: 'FACTURE' === doc.type && doc.ttc ? parseFloat(doc.ttc) : undefined,
        type: doc.type,

        numeroFacture: doc?.numeroFacture,
        factureDateReglement: doc?.factureDateReglement,
        idReglementMode: doc?.idReglementMode,
        numeroCommande: doc?.numeroCommande,
        isGenererCommande: doc?.isGenererCommande,
        avoirType: doc?.avoirType,
        avoirCorrespondants: doc?.avoirCorrespondants,
        nombreJoursPreavis: doc?.nombreJoursPreavis,
        isRenouvellementTacite: doc?.isRenouvellementTacite,
        isMultipleTva: doc?.isMultipleTva,
        factureTvas: doc?.factureTvas,
      });
    }
  };

  const handleDeleteDocument = (document: FullDocumentCategorieFragment) => {
    if (document?.id) {
      deleteDocument({
        variables: {
          input: {
            id: document.id,
          },
        },
      });
    }
  };

  return { handleDeleteDocument, handleReplaceDocument, saving, saved, setSaved };
};
