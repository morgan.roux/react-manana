import { DocumentCategorie, useApplicationContext } from '@lib/common';
import Content from '@lib/common/src/components/EspaceDocumentaire/Content/Content';
import {
  useCreate_ActiviteMutation,
  useGet_Document_CategorieLazyQuery,
  useToggle_Document_To_FavoriteMutation,
} from '@lib/common/src/federation';
import { useCommentsLazyQuery, useUser_SmyleysLazyQuery } from '@lib/common/src/graphql';
import { Box, IconButton, Typography } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import React, { FC, useEffect } from 'react';
import { ClosableTitle } from '../../../../components/ClosableTitle/ClosableTitle';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import { useContrat } from '../../hooks/useContrat';

import { useStyles } from './styles';

const DetailsContrat: FC<{}> = () => {
  const classes = useStyles();

  const {
    params: { id: idDocument },
    redirectTo,
    goBack,
  } = useLaboratoireParams('contrat');
  const { graphql, federation, notify } = useApplicationContext();

  const {
    handleDeleteDocument: onRequestDelete,
    handleReplaceDocument: onRequestReplace,
    saving,
    saved,
    setSaved,
  } = useContrat();

  const [getDocument, gettingDocument] = useGet_Document_CategorieLazyQuery({
    fetchPolicy: 'network-only',
    client: federation,
  });

  const activeDocument = gettingDocument.data?.gedDocument;

  useEffect(() => {
    if (idDocument) {
      getDocument({
        variables: {
          id: idDocument,
        },
      });
    }
  }, [idDocument]);

  const [
    getCommentaires,
    { loading: commentsLoading, error: commentsError, data: comments, refetch: refetchComments },
  ] = useCommentsLazyQuery({ client: graphql });

  const [loadUserSmyleys, loadingUserSmyleys] = useUser_SmyleysLazyQuery({
    client: graphql,
  });

  const [toggleDocumentFavorite, { loading: toggleLoading, error: toggleError, data: toggle }] =
    useToggle_Document_To_FavoriteMutation({
      client: federation,
      onError: () => {
        notify({
          message: "Des erreurs se sont survenues pendant l'ajout du document aux favoris!",
          type: 'error',
        });
      },
    });

  const [addActivite] = useCreate_ActiviteMutation({
    client: federation,
    // onCompleted: () => {
    //   refetchDocument();
    // },
  });

  useEffect(() => {
    if (toggle?.toggleToGedDocumentFavoris && !toggleLoading && !toggleError) {
      notify({
        message: `Le document a été ${
          toggle?.toggleToGedDocumentFavoris?.favoris ? 'ajouté aux' : 'retiré des'
        } favoris!`,
        type: 'success',
      });
    }
  }, [toggle, toggleLoading, toggleError]);

  const handleRefetchComments = () => {
    refetchComments && refetchComments();
  };

  useEffect(() => {
    if (activeDocument) {
      getCommentaires({
        variables: { codeItem: 'GED_DOCUMENT', idItemAssocie: activeDocument.id },
      });
      loadUserSmyleys({
        variables: {
          codeItem: 'GED_DOCUMENT',
          idSource: activeDocument.id,
        },
      });
    }
  }, [activeDocument]);

  const handleRefecthSmyleys = () => {
    loadingUserSmyleys.refetch && loadingUserSmyleys.refetch();
  };

  const handleToggleDocumentToOrFromFavorite = (document: DocumentCategorie): void => {
    toggleDocumentFavorite({
      variables: {
        idDocument: document?.id as any,
      },
    });
  };
  const handleClose = () => {
    goBack('contrat');
  };

  const handleFetchMoreComments = (document: DocumentCategorie): void => {};
  const handleDeplace = (document: DocumentCategorie): void => {};

  const handleCompleteDownload = (document: DocumentCategorie): void => {
    addActivite({
      variables: {
        input: {
          activiteTypeCode: 'DOWNLOAD',
          itemCode: 'GED_DOCUMENT',
          idItemAssocie: document.id as any,
        },
      },
    });
  };

  return (
    <Box className={classes.container}>
      <ClosableTitle title="Détail contrat" onClose={handleClose} />
      <Content
        saving={saving}
        saved={saved}
        setSaved={setSaved}
        activeSousCategorie={undefined}
        activeDocument={activeDocument}
        mobileView={window.innerWidth < 1024}
        tabletView={window.innerWidth < 1366}
        validation
        content={{ loading: false, error: false as any, data: activeDocument as any }}
        commentaires={{ loading: commentsLoading, error: commentsError as any, data: comments?.comments }}
        refetchComments={handleRefetchComments}
        handleSidebarMenu={() => {}}
        handleFilterMenu={() => {}}
        onRequestCreateDocument={(document: any) => {}}
        onRequestAddToFavorite={handleToggleDocumentToOrFromFavorite}
        onRequestDelete={onRequestDelete}
        onRequestRemoveFromFavorite={handleToggleDocumentToOrFromFavorite}
        onRequestReplace={onRequestReplace}
        onRequestFetchMoreComments={handleFetchMoreComments}
        onRequestLike={(document: DocumentCategorie, smyley: any | null) => {}}
        onRequestDeplace={handleDeplace}
        onCompleteDownload={handleCompleteDownload}
        refetchSmyleys={handleRefecthSmyleys}
        hidePartage
        hideRecommandation
        refetchDocument={() => {}}
        onRequestDocument={(id: string) => {}}
        onRequestFetchAll={(withSearch: boolean) => {}}
        noValidation
        tvas={{
          loading: false,
          error: false as any,
          data: [],
        }}
      />
    </Box>
  );
};

export default DetailsContrat;
