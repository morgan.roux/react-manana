import { DocumentCategorie, IDocumentSearch, useApplicationContext, useUploadFiles } from '@lib/common';
import CreateDocument from '@lib/common/src/components/EspaceDocumentaire/Content/CreateDocument';
import {
  FullDocumentCategorieFragment,
  GedDocumentFilter,
  Get_Documents_CategorieDocument,
  Get_Documents_CategorieQuery,
  Get_Documents_CategorieQueryVariables,
  SortDirection,
  useCreate_ActiviteMutation,
  useCreate_Document_CategorieMutation,
  useDelete_Document_CategorieMutation,
  useGet_DocumentsLazyQuery,
  useGet_Document_CategorieLazyQuery,
  useGet_LaboratoireLazyQuery,
  useGet_OriginesQuery,
  useToggle_Document_To_FavoriteMutation,
  useUpdate_Document_CategorieMutation,
} from '@lib/common/src/federation';
import { useCommentsLazyQuery, useUser_SmyleysLazyQuery } from '@lib/common/src/graphql';
import { ConfirmDeleteDialog, CustomAvatar, NewCustomButton, Table, TableChange, TopBar } from '@lib/ui-kit';
import { Box, Grid, Typography } from '@material-ui/core';
import { Add, Event } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, useEffect, useState } from 'react';
import { BottomScrollListener } from 'react-bottom-scroll-listener';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import { useColumn } from '../../components/Column';
import { useStyles } from './styles';

const ContratPage: FC<{}> = () => {
  const { params, redirectTo } = useLaboratoireParams('contrat');
  const { idLabo, id: idDocument, skip, take, searchText, sorting } = params;
  // const searchText = useReactiveVar<string>(searchTextVar);

  const classes = useStyles();

  const { currentPharmacie: pharmacie, notify, graphql, isMobile, auth } = useApplicationContext();

  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  // const [mode, setMode] = useState<'creation' | 'remplacement'>('creation');
  const [searchParameters, setSearchParameters] = useState<IDocumentSearch>({ type: 'CONTRAT' });

  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [documentToShow, setDocumentToShow] = useState<FullDocumentCategorieFragment | undefined>();
  const [openReplaceDocument, setOpenReplaceDocument] = useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const open = Boolean(anchorEl);

  const origines = useGet_OriginesQuery();
  const idOrigineLaboratoire = (origines.data?.origines.nodes || []).find(({ code }) => code === 'LABORATOIRE')?.id;

  const [loadLaboratoire, loadingLaboratoire] = useGet_LaboratoireLazyQuery();

  useEffect(() => {
    onRequestSearch({
      skip: skip ? parseInt(skip) : undefined,
      take: take ? parseInt(take) : undefined,
      searchText,
      sorting,
    });
  }, [
    skip,
    take,
    searchText,
    sorting && sorting[0] && sorting[0].field,
    sorting && sorting[0] && sorting[0].direction,
  ]);

  const firstLabo = params.idLabo?.length ? params.idLabo[0] : undefined;

  useEffect(() => {
    if (idLabo?.length) {
      loadLaboratoire({
        variables: { id: idLabo[0], idPharmacie: pharmacie.id },
      });
    }
  }, [firstLabo]);

  useEffect(() => {
    setSearchParameters((prev: any) => ({ ...prev, idOrigine: idOrigineLaboratoire }));
  }, [idOrigineLaboratoire]);

  useEffect(() => {
    if (idDocument) {
      getDoc(idDocument);
    }
  }, [idDocument]);

  useEffect(() => {
    if (idOrigineLaboratoire) {
      getDocuments({
        variables: {
          filter: {
            and: [
              {
                idOrigine: {
                  eq: idOrigineLaboratoire,
                },
              },

              ...(idLabo?.length
                ? [
                    {
                      idOrigineAssocie: {
                        in: idLabo,
                      },
                    },
                  ]
                : []),
              {
                idPharmacie: {
                  eq: pharmacie.id,
                },
              },
              {
                type: {
                  eq: 'CONTRAT',
                },
              },
            ],
          },
        },
      });
    }
  }, [firstLabo, idOrigineLaboratoire]);

  useEffect(() => {
    if (!isMobile && saved) {
      setOpenReplaceDocument(false);
      setSaved(false);
    }
  }, [saved]);

  const [getDocuments, gettingDocuments] = useGet_DocumentsLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [toggleDocumentFavorite, { loading: toggleLoading, error: toggleError, data: toggle }] =
    useToggle_Document_To_FavoriteMutation({
      onError: () => {
        notify({
          message: "Des erreurs se sont survenues pendant l'ajout du document aux favoris!",
          type: 'error',
        });
      },
    });

  const [getDocument, { loading: documentLoading, error: documentError, data: document, refetch: refetchDocument }] =
    useGet_Document_CategorieLazyQuery({
      fetchPolicy: 'network-only',
    });

  const [addActivite] = useCreate_ActiviteMutation({
    onCompleted: () => {
      refetchDocument && refetchDocument();
    },
  });

  const [
    getCommentaires,
    { loading: commentsLoading, error: commentsError, data: comments, refetch: refetchComments },
  ] = useCommentsLazyQuery({ client: graphql });

  const [loadUserSmyleys, loadingUserSmyleys] = useUser_SmyleysLazyQuery({ client: graphql });

  const [addDocumentCategorie] = useCreate_Document_CategorieMutation({
    onCompleted: () => {
      setSaving(false);
      setSaved(true);

      gettingDocuments.refetch && gettingDocuments.refetch();
      notify({
        message: 'Le contrat a été ajoutée avec succès !',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout du contrat!",
        type: 'error',
      });

      setSaving(false);
    },
  });

  const [updateDocumentCategorie] = useUpdate_Document_CategorieMutation({
    onCompleted: () => {
      setSaving(false);
      setSaved(true);

      gettingDocuments.refetch && gettingDocuments.refetch();
      notify({
        message: 'Le contrat a été modifiée avec succès !',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification du contrat!',
        type: 'error',
      });
      setSaving(false);
    },
  });

  const [deleteDocumentCategorie] = useDelete_Document_CategorieMutation({
    onCompleted: () => {
      notify({
        message: 'Le contrat a été supprimé avec succès !',
        type: 'success',
      });
      gettingDocuments.refetch && gettingDocuments.refetch();
      setOpenDeleteDialog(false);

      handleRefetchAll();
      //redirectTo();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression du document !',
        type: 'error',
      });
    },
    update: (cache, deletionResult) => {
      if (deletionResult?.data?.deleteOneGedDocument?.id) {
        const previousList = cache.readQuery<Get_Documents_CategorieQuery, Get_Documents_CategorieQueryVariables>({
          query: Get_Documents_CategorieDocument,
        })?.gedDocuments;

        const deletedId = deletionResult.data.deleteOneGedDocument.id;

        cache.writeQuery<Get_Documents_CategorieQuery, Get_Documents_CategorieQueryVariables>({
          query: Get_Documents_CategorieDocument,
          data: {
            gedDocuments: {
              ...(previousList || {}),
              nodes: (previousList?.nodes || []).filter((item) => item?.id !== deletedId),
            } as any,
          },
        });
      }
    },
  });

  const getDoc = (id: string) => {
    getDocument({
      variables: {
        id: id,
      },
    });

    addActivite({
      variables: {
        input: {
          activiteTypeCode: 'VISIT',
          itemCode: 'GED_DOCUMENT',
          idItemAssocie: id,
        },
      },
    });

    getCommentaires({
      variables: {
        codeItem: 'GED_DOCUMENT',
        idItemAssocie: id,
      },
    });
  };

  const showError = () => {
    notify({
      message: `Une erreur est survenue pendant le chargement du document`,
      type: 'error',
    });
  };

  useEffect(() => {
    if (toggle?.toggleToGedDocumentFavoris && !toggleLoading && !toggleError) {
      notify({
        message: `Le document a été ${
          toggle?.toggleToGedDocumentFavoris?.favoris ? 'ajouté aux' : 'retiré des'
        } favoris!`,
        type: 'success',
      });
    }
  }, [toggle, toggleLoading, toggleError]);

  const upsertDocument = ({
    id,
    idSousCategorie,
    description,
    nomenclature,
    numeroVersion,
    idUserVerificateur,
    idUserRedacteur,
    dateHeureParution,
    dateHeureDebutValidite,
    dateHeureFinValidite,
    motCle1,
    motCle2,
    motCle3,
    fichier,
    idOrigine,
    idOrigineAssocie,
    dateFacture,
    hT,
    tva,
    ttc,
    type,

    numeroFacture,
    factureDateReglement,
    idReglementMode,
    numeroCommande,
    isGenererCommande,
    avoirType,
    avoirCorrespondants,
    nombreJoursPreavis,
    isRenouvellementTacite,
    isMultipleTva,
    factureTvas,
  }: any): void => {
    if (!id) {
      // setMode('creation');
      addDocumentCategorie({
        variables: {
          input: {
            idSousCategorie: idSousCategorie,
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: new Date(dateHeureParution),
            dateHeureDebutValidite: new Date(dateHeureDebutValidite),
            dateHeureFinValidite: new Date(dateHeureFinValidite),
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,
            statut: 'APPROUVE',

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
            nombreJoursPreavis,
            isRenouvellementTacite,
            isMultipleTva,
            facturesTva: factureTvas,
          },
        },
      });
      // .then(() => {
      //   gettingDocuments.refetch && gettingDocuments.refetch();
      // });
    } else {
      // setMode('remplacement');
      updateDocumentCategorie({
        variables: {
          input: {
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: dateHeureParution,
            dateHeureDebutValidite: dateHeureDebutValidite,
            dateHeureFinValidite: dateHeureFinValidite,
            idSousCategorie: idSousCategorie,
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,
            statut: 'APPROUVE',

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
            nombreJoursPreavis,
            isRenouvellementTacite,
            isMultipleTva,
            facturesTva: factureTvas,
          },
          id: id,
        },
      });
      // .then(() => {
      //   gettingDocuments.refetch && gettingDocuments.refetch();
      // });
    }
  };

  const handleRefecthSmyleys = () => {
    loadingUserSmyleys.refetch && loadingUserSmyleys.refetch();
  };

  const handleDetailsDocument = (document: any): void => {
    // history.push(`/laboratoires/detail-contrat?id=${document.id}`);
    redirectTo({ id: document.id, backParams: params }, 'detail-contrat');
    //redirectTo({ view: 'details', id: document.id });
    // window.history.pushState(
    //   { urlPath: `#/laboratoires/${idLabo}/contrat/${document.id}` },
    //   '',
    //   `#/laboratoires/${idLabo}/contrat/${document.id}`
    // );
  };

  const handleDeplace = (document: DocumentCategorie): void => {};

  const handleFetchMoreComments = (document: DocumentCategorie): void => {};

  const handleRefetchAll = () => {
    gettingDocuments.refetch && gettingDocuments.refetch();
  };

  const handleToggleDocumentToOrFromFavorite = (document: DocumentCategorie): void => {
    toggleDocumentFavorite({
      variables: {
        idDocument: document?.id as any,
      },
    });
  };

  const handleDeleteDocument = (document: DocumentCategorie): void => {
    deleteDocumentCategorie({
      variables: {
        input: {
          id: document?.id as any,
        },
      },
    });
  };

  const handleScroll = (): void => {
    const fetchMore = gettingDocuments.fetchMore;
    const variables = gettingDocuments.variables;
    const paging = variables?.paging;
    if (gettingDocuments.data?.gedDocuments.nodes) {
      fetchMore &&
        fetchMore({
          variables: {
            ...variables,
            paging: {
              ...paging,
              offset: gettingDocuments.data?.gedDocuments.nodes,
            },
          },

          updateQuery: (prev: any, { fetchMoreResult }) => {
            if (prev?.gedDocuments && fetchMoreResult?.gedDocuments.nodes) {
              return {
                ...prev,
                gedDocuments: {
                  ...prev.gedDocuments,
                  nodes: [...prev.gedDocuments.nodes, ...fetchMoreResult.gedDocuments.nodes],
                },
              };
            }

            return prev;
          },
        });
    }
  };

  const handleRefresh = (): void => {
    gettingDocuments.refetch && gettingDocuments.refetch();
    redirectTo();
  };

  const handleOnRequestDocument = (id: string) => {
    getDoc(id);
  };

  const handleCompleteDownload = (document: DocumentCategorie): void => {
    addActivite({
      variables: {
        input: {
          activiteTypeCode: 'DOWNLOAD',
          itemCode: 'GED_DOCUMENT',
          idItemAssocie: document.id as any,
        },
      },
    });
  };

  const handleGoBack = () => {
    redirectTo(undefined, 'plan');
  };

  const handleShowMenuClick = (document: FullDocumentCategorieFragment, event: React.MouseEvent<HTMLElement>): void => {
    setAnchorEl(event.currentTarget);
    setDocumentToShow(document);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const handleModifier = () => {
    setOpenReplaceDocument(true);
  };

  const handleSupprimer = () => {
    setOpenDeleteDialog(true);
  };

  const handleSearchTable = ({ skip, take, searchText, sortBy, sortDirection }: TableChange) => {
    redirectTo({
      ...params,
      skip: skip ? skip.toString() : undefined,
      take: take ? take.toString() : undefined,
      searchText,
      sorting: sortBy && sortDirection ? [{ field: sortBy, direction: sortDirection }] : undefined,
    });
  };

  const handleSortTable = (column: any, direction: SortDirection) => {
    redirectTo({ ...params, sorting: [{ field: column, direction }] });
  };

  const onRequestSearch = (change: any) => {
    const filterAnd: GedDocumentFilter[] = [
      {
        idOrigine: {
          eq: idOrigineLaboratoire,
        },
      },
      {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
      {
        type: {
          eq: 'CONTRAT',
        },
      },
    ];

    if (idLabo?.length)
      filterAnd.push({
        idOrigineAssocie: {
          in: idLabo,
        },
      });

    if (change.searchText) {
      filterAnd.push({
        description: {
          iLike: `%${change.searchText}%`,
        },
      } as any);
    }

    if (idOrigineLaboratoire) {
      const sortTableFilter = change.sorting || [];
      getDocuments({
        variables: {
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: change.skip,
            limit: change.take,
          },
          sorting: sortTableFilter,
        },
      });
    }
  };

  const handleReplaceDocument = (doc: any): void => {
    setSaving(true);
    setSaved(false);
    if (doc.launchUpload) {
      uploadFiles([doc.selectedFile], {
        directory: 'relation-fournisseurs/contrat',
      })
        .then((uploadedFiles) => {
          upsertDocument({
            id: doc.id,
            idSousCategorie: doc.idSousCategorie,
            description: doc.description,
            nomenclature: doc.nomenclature,
            numeroVersion: doc.numeroVersion,
            idUserVerificateur: doc.idUserVerificateur,
            idUserRedacteur: doc.idUserRedacteur,
            dateHeureParution: doc.dateHeureParution,
            dateHeureDebutValidite: doc.dateHeureDebutValidite,
            dateHeureFinValidite: doc.dateHeureFinValidite,
            motCle1: doc.motCle1,
            motCle2: doc.motCle2,
            motCle3: doc.motCle3,
            fichier: uploadedFiles[0],
            idOrigine: doc?.origine?.id || doc?.origine,
            idOrigineAssocie: doc.correspondant.id,
            dateFacture: doc.dateFacture,
            hT: doc.hT ? parseFloat(doc.hT) : undefined,
            tva: doc.tva ? parseFloat(doc.tva) : undefined,
            ttc: doc.ttc ? parseFloat(doc.ttc) : undefined,
            type: doc.type,

            numeroFacture: doc?.numeroFacture,
            factureDateReglement: doc?.factureDateReglement,
            idReglementMode: doc?.idReglementMode,
            numeroCommande: doc?.numeroCommande,
            isGenererCommande: doc?.isGenererCommande,
            avoirType: doc?.avoirType,
            avoirCorrespondants: doc?.avoirCorrespondants,
            nombreJoursPreavis: doc?.nombreJoursPreavis,
            isRenouvellementTacite: doc?.isRenouvellementTacite,
            isMultipleTva: doc?.isMultipleTva,
            factureTvas: doc?.factureTvas,
          });
        })
        .catch(() => {
          setSaving(false);
          showError();
        });
    } else {
      const { nomOriginal, chemin, type } = doc.previousFichier || {};

      upsertDocument({
        id: doc.id,
        idSousCategorie: doc.idSousCategorie,
        description: doc.description,
        nomenclature: doc.nomenclature,
        numeroVersion: doc.numeroVersion,
        idUserVerificateur: doc.idUserVerificateur,
        idUserRedacteur: doc.idUserRedacteur,
        dateHeureParution: doc.dateHeureParution,
        dateHeureDebutValidite: doc.dateHeureDebutValidite,
        dateHeureFinValidite: doc.dateHeureFinValidite,
        motCle1: doc.motCle1,
        motCle2: doc.motCle2,
        motCle3: doc.motCle3,
        fichier: doc.previousFichier ? { nomOriginal, chemin, type } : undefined,
        idOrigine: doc.origine?.id,
        idOrigineAssocie: doc.correspondant?.id,
        dateFacture: 'FACTURE' === doc.type ? doc.dateFacture : undefined,
        hT: 'FACTURE' === doc.type && doc.hT ? parseFloat(doc.hT) : undefined,
        tva: 'FACTURE' === doc.type && doc.tva ? parseFloat(doc.tva) : undefined,
        ttc: 'FACTURE' === doc.type && doc.ttc ? parseFloat(doc.ttc) : undefined,
        type: doc.type,

        numeroFacture: doc?.numeroFacture,
        factureDateReglement: doc?.factureDateReglement,
        idReglementMode: doc?.idReglementMode,
        numeroCommande: doc?.numeroCommande,
        isGenererCommande: doc?.isGenererCommande,
        avoirType: doc?.avoirType,
        avoirCorrespondants: doc?.avoirCorrespondants,
        nombreJoursPreavis: doc?.nombreJoursPreavis,
        isRenouvellementTacite: doc?.isRenouvellementTacite,
        isMultipleTva: doc?.isMultipleTva,
        factureTvas: doc?.factureTvas,
      });
    }
  };

  const columns = useColumn({
    handleShowMenuClick,
    handleClose,
    anchorEl,
    open,
    handleRowDetail: handleDetailsDocument,
    documentToShow,
    isAuthorizedToAddDocument: auth.isAuthorizedToAddGedDocument(),
    handleModifier,
    handleSupprimer,
  });

  const topBarComponent = (searchComponent: any) => (
    <TopBar
      // handleOpenAdd={handleOpenAdd}
      searchComponent={searchComponent}
      titleButton=" "
      titleTopBar=" "
      withMoreButton
      buttons={[
        {
          component: (
            <Box mr={3}>
              <NewCustomButton
                onClick={() => {
                  setDocumentToShow(undefined);
                  handleModifier();
                }}
              >
                <Add />
                NOUVEAU CONTRAT
              </NewCustomButton>
            </Box>
          ),
        },
        // {
        //   component: (
        //     <Box ml={2}>
        //       <NewCustomButton onClick={handleOpenAddPharma}>
        //         <Add />
        //         NOUVELLE ACTION PHARMACIE
        //       </NewCustomButton>
        //     </Box>
        //   ),
        // },
      ]}
      withFilter={false}
    />
  );
  const handleContainerOnBottom = () => {};
  const mobilePage = (
    <BottomScrollListener onBottom={handleContainerOnBottom}>
      {(scrollRef) => (
        <Grid id="infinity-suivi" item ref={scrollRef as any} className={classes.infiniteScrollContainer}>
          <Box py={2} px={2}>
            {(gettingDocuments.data?.gedDocuments.nodes || []).map((item: any, index: number) => {
              console.log('................. gettingDocument Data.............', item);
              return (
                <>
                  <Box className={classes.cardActionOp}>
                    <div className={classes.leftBannerActionOp} style={{ background: `${item.importance?.couleur}` }} />

                    <Box>
                      <Box display="flex" justifyContent="space-between" alignItems="center">
                        <Typography className={classes.textTitle}>
                          <span dangerouslySetInnerHTML={{ __html: item.description }} />
                        </Typography>
                        <Typography>{item.type?.libelle || ''}</Typography>
                      </Box>
                      <Box display="flex" justifyContent="space-between" alignItems="center">
                        <Box display="flex" alignItems="center">
                          <Typography className={classes.dateIcons}>
                            <Event />{' '}
                            {item.dateHeureDebutValidite && moment(item.dateHeureDebutValidite).format('DD/MM/YYYY')} -{' '}
                            {item.dateHeureFinValidite && moment(item.dateHeureFinValidite).format('DD/MM/YYYY')}
                          </Typography>
                        </Box>
                      </Box>
                      <Box display="flex" justifyContent="space-between" alignItems="center">
                        <Box display="flex" alignItems="center">
                          <CustomAvatar
                            name={item.origineAssocie?.nom || '-'}
                            url={item.origineAssocie?.photo?.publicUrl as any}
                            style={{ width: 30, height: 30, marginRight: 5 }}
                          />
                          <Typography style={{ fontSize: 13 }}>{item.origineAssocie?.nom}</Typography>
                        </Box>
                        <Typography
                          className={
                            item.isRenouvellementTacite ? classes.renouvelementTaciteGreen : classes.renouvelementTacite
                          }
                        >
                          <span>RENOUVELEMENT PAR TACITE</span>
                        </Typography>
                      </Box>
                    </Box>
                  </Box>
                </>
              );
            })}
          </Box>
        </Grid>
      )}
    </BottomScrollListener>
  );
  return isMobile ? (
    <Box>{mobilePage}</Box>
  ) : (
    // <CustomContainer
    //   filled
    //   bannerContentStyle={{
    //     justifyContent: 'center',
    //   }}
    //   bannerBack
    //   onBackClick={handleGoBack}
    //   bannerTitle="Contrat"
    //   contentStyle={{
    //     marginTop: 0,
    //     width: '100%',
    //   }}
    //   onlyBackAndTitle
    // >
    //   <Box className={classes.container}>
    //     <MainContainer
    //       sideBarProps={{
    //         saving: saving,
    //         saved: saved,
    //         setSaved: setSaved,
    //         activeSousCategorie: undefined,
    //         documents: {
    //           loading: gettingDocuments.loading,
    //           error: gettingDocuments.error as any,
    //           data: gettingDocuments.data?.gedDocuments.nodes || ([] as any),
    //           total: gettingDocuments.data?.gedDocuments.totalCount || 0,
    //         },
    //         searchVariables: searchParameters,
    //         setActiveDocument: handleDetailsDocument,
    //         onRequestDeplace: handleDeplace,
    //         onRequestAddToFavorite: handleToggleDocumentToOrFromFavorite,
    //         onRequestDelete: handleDeleteDocument,
    //         onRequestDetails: handleDetailsDocument,
    //         onRequestRemoveFromFavorite: handleToggleDocumentToOrFromFavorite,
    //         onRequestReplace: handleReplaceDocument,
    //         onRequestScroll: handleScroll,
    //         onRequestGetComments: () => {},
    //         setSearchVariables: setSearchParameters,
    //         onRequestCreateDocument: handleReplaceDocument,
    //         onRequestRefresh: handleRefresh,
    //         document: document?.gedDocument as any,
    //         espaceType: 'espace-documentaire',
    //         tvas: {
    //           loading: false,
    //           error: false as any,
    //           data: [],
    //         },
    //         defaultCorrespondant: loadingLaboratoire.data?.laboratoire,
    //       }}
    //       contentProps={{
    //         saving: saving,
    //         saved: saved,
    //         setSaved: setSaved,
    //         activeSousCategorie: undefined,
    //         activeDocument: document?.gedDocument,
    //         mobileView: window.innerWidth < 1024,
    //         tabletView: window.innerWidth < 1366,
    //         validation: true,
    //         content: {
    //           loading: documentLoading,
    //           error: documentError as any,
    //           data: document?.gedDocument as any,
    //         },
    //         commentaires: {
    //           loading: commentsLoading,
    //           error: commentsError as any,
    //           data: comments?.comments as any,
    //         },
    //         onRequestDocument: handleOnRequestDocument,
    //         refetchComments: refetchComments || (() => {}),
    //         handleSidebarMenu: () => {},
    //         handleFilterMenu: () => {},
    //         onRequestCreateDocument: handleReplaceDocument,
    //         onRequestAddToFavorite: handleToggleDocumentToOrFromFavorite,
    //         onRequestDelete: handleDeleteDocument,
    //         onRequestRemoveFromFavorite: handleToggleDocumentToOrFromFavorite,
    //         onRequestReplace: handleReplaceDocument,
    //         onRequestFetchMoreComments: handleFetchMoreComments,
    //         onRequestLike: (document: DocumentCategorie, smyley: any | null) => {},
    //         onRequestDeplace: handleDeplace,
    //         onCompleteDownload: handleCompleteDownload,
    //         refetchSmyleys: handleRefecthSmyleys,
    //         refetchDocument: refetchDocument,
    //         onRequestFetchAll: handleRefetchAll,
    //         setSearchVariables: setSearchParameters,
    //         searchVariables: searchParameters,
    //         tvas: {
    //           loading: false,
    //           error: false as any,
    //           data: [],
    //         },
    //       }}
    //     />
    //   </Box>
    // </CustomContainer>
    <>
      <Table
        error={gettingDocuments.error as any}
        loading={gettingDocuments.loading}
        search={false}
        data={gettingDocuments.data?.gedDocuments.nodes || ([] as any)}
        columns={columns}
        topBarComponent={topBarComponent}
        rowsTotal={gettingDocuments.data?.gedDocuments.totalCount}
        onRunSearch={handleSearchTable}
        onSortColumn={handleSortTable}
        tableBodyClassName={classes.tableNoBoldSelect}
      />
      <ConfirmDeleteDialog
        title="Suppression"
        content="Etes-vous sûr de vouloir supprimer ce contrat ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={() => handleDeleteDocument(documentToShow as any)}
      />

      {openReplaceDocument && (
        <CreateDocument
          open={openReplaceDocument}
          setOpen={setOpenReplaceDocument}
          title={documentToShow ? 'Modifier contrat' : 'Nouveau contrat'}
          saving={saving}
          activeSousCategorie={documentToShow?.sousCategorie}
          document={documentToShow as any}
          mode={documentToShow ? 'remplacement' : 'creation'}
          onRequestCreateDocument={handleReplaceDocument}
          defaultType="CONTRAT"
          idDefaultOrigine={idOrigineLaboratoire}
          defaultCorrespondant={idLabo?.length ? loadingLaboratoire.data?.laboratoire : undefined}
          tvas={{
            loading: false,
            error: false as any,
            data: [],
          }}
        />
      )}
    </>
  );
};

export default ContratPage;
