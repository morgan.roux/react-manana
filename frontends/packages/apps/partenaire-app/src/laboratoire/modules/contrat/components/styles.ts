import { createStyles, makeStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    btnFilterPar: {
      display: 'flex',
      alignItems: 'center',
      fontFamily: 'Roboto',
      fontWeight: 'bolder',
      fontSize: 14,
      cursor: 'pointer',
    },
    rootModal: {
      width: '100%',
      padding: '24px 0',
      paddingBottom: 0,
      display: 'block',
      [theme.breakpoints.up('xs')]: {
        minWidth: 400,
      },
    },
    formControl: {
      marginBottom: 24,
      width: '50%',
      [theme.breakpoints.down('xs')]: {
        width: '100%',
      },
    },
    btnSubmit: {
      width: '100%',
      display: 'flex',
      justifyContent: 'flex-end',
    },
    Label: {
      fontFamily: 'Roboto',
      color: '#000000',
      fontSize: 18,
      fontWeight: 'bold',
      marginBottom: 16,
    },
    textTable: {
      fontFamily: 'Roboto',
      color: '#212121',
      fontWeight: 'normal',
      textAlign: 'left',
      fontSize: 14,
    },
  })
);
