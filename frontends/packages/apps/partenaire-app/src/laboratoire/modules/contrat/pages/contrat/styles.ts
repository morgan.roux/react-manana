import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      backgroundColor: theme.palette.common.white,
    },
    tableNoBoldSelect: {
      '& .MuiTableCell-root .MuiSelect-root': {
        fontWeight: 400,
      },
    },
    infiniteScrollContainer: {
      height: 'calc(100vh - 370px)',
      width: '100%',
      overflowY: 'scroll',
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
    dateIcons: {
      display: 'flex',
      color: '#afafaf',
      fontSize: 14,
      alignItems: 'center',
      marginRight: 5,
      '& svg': {
        fontSize: 18,
        marginRight: 5,
      },
    },
    cardActionOp: {
      background: '#f8f8f8',
      marginBottom: 10,
      padding: '5px 15px',
      position: 'relative',
    },
    leftBannerActionOp: {
      position: 'absolute',
      width: 4,
      height: '100%',
      left: 0,
      top: 0,
      borderRadius: 5,
    },
    renouvelementTacite: {
      width: 120,
      fontSize: 12,
      textAlign: 'center',
      background: '#dcdcdc',
      padding: 5,
      borderRadius: 7,
      color: '#909090',
    },
    renouvelementTaciteGreen: {
      width: 120,
      fontSize: 12,
      textAlign: 'center',
      background: '#c1eac5',
      padding: 5,
      borderRadius: 7,
      color: '#37b400',
    },
    textTitle: {
      fontFamily: 'Roboto',
      color: '#424242',
      textAlign: 'left',
      fontWeight: 'bold',
      fontSize: 14,
    },
    imgAction: {
      width: '22px !important',
      marginBottom: -3,
      marginLeft: 10,
    },
  })
);
