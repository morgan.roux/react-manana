// import { Box, Typography, useMediaQuery, useTheme } from '@material-ui/core';
// import React from 'react';
// import SortIcon from '@material-ui/icons/Sort';

// import { useStyles } from './styles';
// import { Filter } from '@lib/ui-kit';
// import { SearchInputBase } from '@lib/ui-kit/src/components/atoms/SearchInputBase';

// const ContratPage: React.FC<{}> = () => {
//   const classes = useStyles();
//   const theme = useTheme();
//   const match = useMediaQuery(theme.breakpoints.down('sm'));

//   return (
//     <Box className={classes.tableBox}>
//       <>
//         {match ? (
//           <>
//             <div className={classes.historiqueHeader}>
//               <div className={classes.historiqueHeaderItems}>
//                 <Typography>Historiques des factures</Typography>
//                 <div className={classes.icon}>
//                   <div className={classes.iconSort}>
//                     <SortIcon />
//                   </div>
//                   {/* <Filter
//                     filters={[
//                       {
//                         titre: 'Type',
//                         itemFilters: (data || []).map((item: any) => ({
//                           ...item,
//                           keyword: 'idType',
//                         })) as any,
//                       },
//                       {
//                         titre: 'Statut',
//                         itemFilters: (data || []).map((item: any) => ({
//                           ...item,
//                           keyword: 'idStatut',
//                         })) as any,
//                       },
//                     ]}
//                     onChange={handleChangeFilter}
//                     filterIcon={<img src={FilterAlt} />}
//                     hideText
//                   /> */}
//                 </div>
//               </div>
//               <div className={classes.search}>
//                 <SearchInputBase dark onChange={function noRefCheck() {}} value="" placeholder="Recherche" />
//               </div>
//             </div>

//             <div className={classes.historiqueRoot}>
//               <div className={classes.historiqueTitre}>
//                 <div>
//                   <Box className={classes.titre}>Total TTC</Box>
//                   <Typography className={classes.sousTitre}>{round2Decimals(totaux?.totalTTC).toFixed(2)}</Typography>
//                 </div>
//                 <div>
//                   <Box className={classes.titre}>Total HT</Box>
//                   <Typography className={classes.sousTitre}>{round2Decimals(totaux?.totalHT).toFixed(2)}</Typography>
//                 </div>
//                 <div>
//                   <Box className={classes.titre}>Total TVA</Box>
//                   <Typography className={classes.sousTitre}>{round2Decimals(totaux?.totalTVA).toFixed(2)}</Typography>
//                 </div>
//               </div>
//             </div>
//             <div>
//               {data?.map((item) => (
//                 <>
//                   <div className={classes.historiqueItems}>
//                     <div>
//                       <div className={classes.historiqueListeItems}>
//                         <div className={classes.commande}>
//                           <div className={classes.facture}>
//                             <Typography className={classes.numeroCommande}>N° facture: </Typography>
//                             <Typography>{item.numeroFacture}</Typography>
//                             <Typography className={classes.status}>{status}</Typography>
//                           </div>
//                           <div>
//                             <Typography className={classes.numeroCommande}>N° commande: </Typography>
//                             <Typography>{item.numeroCommande}</Typography>
//                           </div>
//                         </div>
//                         <Typography className={classes.numeroCommande}>{item.numeroCommande}</Typography>
//                       </div>
//                       {/* <div className={classes.date}>
//                       {moment(item.factureDateReglement).format('DD/MM/YYYY')}
//                     </div> */}
//                     </div>
//                     <div className={classes.historique}>
//                       <div className={classes.historiqueTitreItems}>
//                         <div>
//                           <Box className={classes.historiqueDetails}>Total TTC</Box>
//                           <Typography className={classes.totalHistorique}>
//                             {round2Decimals(item.factureTotalTtc).toFixed(2)}
//                           </Typography>
//                         </div>
//                         <div>
//                           <Box className={classes.historiqueDetails}>Total HT</Box>
//                           <Typography className={classes.totalHistorique}>
//                             {round2Decimals(item.factureTotalHt).toFixed(2)}
//                           </Typography>
//                         </div>
//                         <div>
//                           <Box className={classes.historiqueDetails}>Total TVA</Box>
//                           <Typography className={classes.totalHistorique}>
//                             {round2Decimals(item.factureTva).toFixed(2)}
//                           </Typography>
//                         </div>
//                       </div>
//                     </div>
//                   </div>
//                   <Divider />
//                 </>
//               ))}
//             </div>
//             {/* <div
//             onClick={() => onRequestEdit && onRequestEdit()}
//             className={classes.bouttonAdd}
//           >
//             <AddIcon />
//           </div> */}
//             <Fab
//               className={commonStyles.fab}
//               onClick={() => setOpenReplaceDocument(true)}
//               variant="round"
//               color="secondary"
//             >
//               <Add />
//             </Fab>
//             {/* <HistoriqueForm
//             open={ajoutCommande}
//             setOpen={toggleOpenEditDialog}
//             mode={historiqueToEdit ? 'modification' : 'creation'}
//             onRequestSave={onRequestSave}
//             saving={saving}
//             commandeToEdit={historiqueToEdit}
//           /> */}
//             {/* <CustomModal
//             open={ajout}
//             setOpen={toggleOpenJoindreDialog}
//             title="Joindre une commande"
//             withBtnsActions
//             closeIcon
//             headerWithBgColor
//             disabledButton={saving}
//             onClickConfirm={handleConfirmJoindreCommande}
//             withCancelButton={false}
//             actionButtonTitle="Joindre"
//             maxWidth="lg"
//           >
//             <Box minWidth={960}>{historiqueCommandeComponent}</Box>
//           </CustomModal> */}
//           </>
//         ) : (
//           <Table
//             onSortColumn={handleSortTable}
//             error={gettingDocuments.error as any}
//             loading={gettingDocuments.loading || originesLoading}
//             data={data || []}
//             additionalRows={
//               <TableRow>
//                 {isSelectable && <TableCell />}
//                 <TableCell style={{ borderBottomColor: '#fff' }} />
//                 <TableCell style={{ borderBottomColor: '#fff' }} />
//                 <TableCell style={{ borderBottomColor: '#fff' }} />
//                 <TableCell style={{ borderBottomColor: '#fff' }}>
//                   <span
//                     style={{
//                       display: 'flex',
//                       justifyContent: 'flex-end',
//                       borderBottomColor: '#fff',
//                     }}
//                   >
//                     Total
//                   </span>
//                 </TableCell>
//                 <TableCell style={{ borderBottomColor: '#fff' }}>
//                   {round2Decimals(totaux?.totalTTC || 0).toFixed(2)}&nbsp;€
//                 </TableCell>
//                 <TableCell style={{ borderBottomColor: '#fff' }}>
//                   {round2Decimals(totaux?.totalHT || 0).toFixed(2)}&nbsp;€
//                 </TableCell>
//                 <TableCell style={{ borderBottomColor: '#fff' }}>
//                   {round2Decimals(totaux?.totalTVA || 0).toFixed(2)}&nbsp;€
//                 </TableCell>
//                 <TableCell style={{ borderBottomColor: '#fff' }} />
//               </TableRow>
//             }
//             search={false}
//             columns={columns}
//             rowsTotal={gettingDocuments.data?.gedDocuments.nodes.length || 0}
//             selectable={isSelectable}
//             noTableToolbarSearch={isSelectable}
//             onClearSelection={isSelectable ? () => setFacturesSelected([]) : undefined}
//             onSelectAll={isSelectable ? handleSelectAll : undefined}
//             rowsSelected={isSelectable ? facturesSelected : undefined}
//             onRowsSelectionChange={isSelectable ? handleSelectChange : undefined}
//             onShowSelected={() => {
//               setShowSelected(!showSelected);
//             }}
//             topBarComponent={(searchComponent) => (
//               <>
//                 {match ? (
//                   <Box display="flex" alignItems="center">
//                     <TopBar
//                       searchComponent={searchComponent}
//                       titleTopBar="Liste des factures"
//                       withFilter
//                       titleButton=""
//                       fullwidth
//                       noAddBtn
//                       // filterComponent={
//                       //   <Filter
//                       //     filters={[
//                       //       {
//                       //         titre: 'Statut',
//                       //         itemFilters: (factureStatuts || []).map((item) => ({
//                       //           ...item,
//                       //           keyword: 'code',
//                       //         })) as any,
//                       //       },
//                       //     ]}
//                       //     onChange={handleChangeFilter}
//                       //     filterIcon={filterIcon}
//                       //   />
//                       // }
//                     />
//                   </Box>
//                 ) : (
//                   <Box display="flex" alignItems="center">
//                     <TopBar
//                       searchComponent={searchComponent}
//                       titleTopBar="Liste des factures"
//                       withFilter
//                       titleButton=""
//                       fullwidth
//                       noAddBtn
//                       // filterComponent={
//                       //   <Filter
//                       //     filters={[
//                       //       {
//                       //         titre: 'Statut',
//                       //         itemFilters: (factureStatuts || []).map((item) => ({
//                       //           ...item,
//                       //           keyword: 'code',
//                       //         })) as any,
//                       //       },
//                       //     ]}
//                       //     onChange={handleChangeFilter}
//                       //     filterIcon={filterIcon}
//                       //   />
//                       // }
//                     />

//                     {!isSelectable && (
//                       <Box pl={2}>
//                         <NewCustomButton
//                           style={{ minWidth: 300, minHeight: 40 }}
//                           variant="outlined"
//                           onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
//                             event.preventDefault();
//                             event.stopPropagation();
//                             onConsultGed && onConsultGed();
//                           }}
//                           className={classes.espaceFacturetion}
//                         >
//                           Consulter espace facturation
//                         </NewCustomButton>
//                       </Box>
//                     )}
//                     {isAuthorizedToAddDocument && !isSelectable && (
//                       <Box pl={2}>
//                         <NewCustomButton
//                           style={{ minWidth: 230, minHeight: 40 }}
//                           onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
//                             event.preventDefault();
//                             event.stopPropagation();
//                             setOpenReplaceDocument(true);
//                           }}
//                           startIcon={<Add />}
//                           className={classes.nouvelleFacture}
//                         >
//                           Nouvelle Facture
//                         </NewCustomButton>
//                       </Box>
//                     )}
//                   </Box>
//                 )}
//               </>
//             )}
//             onRunSearch={handleSearchTable}
//           />
//         )}
//       </>
//       {isSelectable && (
//         <Box display="flex" justifyContent="flex-end">
//           <NewCustomButton
//             onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
//               event.preventDefault();
//               event.stopPropagation();
//               onRequestSelected && onRequestSelected(facturesSelected);
//             }}
//             className={classes.joindreButton}
//             disabled={!facturesSelected?.length}
//           >
//             Joindre
//           </NewCustomButton>
//         </Box>
//       )}
//       {openJoindreDialog && (
//         <JoindreCommande
//           selectedIds={documentToShow?.idCommandes}
//           onRequestSelected={handleConfirmJoindreCommande}
//           idDocument={documentToShow?.id}
//           open={openJoindreDialog}
//           setOpen={toggleOpenJoindreDialog}
//           saving={savingAssociation}
//           saved={savedAssociation}
//           setSaved={setSavedAssociation}
//         />
//       )}
//       <ConfirmDeleteDialog
//         title="Suppression"
//         content="Etes-vous sûr de vouloir supprimer cette Facture ?"
//         open={openDeleteDialog}
//         setOpen={setOpenDeleteDialog}
//         onClickConfirm={handleConfirmDeleteOne}
//       />

//       {openReplaceDocument && (
//         <CreateDocument
//           open={openReplaceDocument}
//           setOpen={setOpenReplaceDocument}
//           title={documentToShow ? 'Modifier facture' : 'Nouvelle facture'}
//           saving={saving}
//           activeSousCategorie={documentToShow?.sousCategorie}
//           document={documentToShow}
//           mode={documentToShow ? 'remplacement' : 'creation'}
//           onRequestCreateDocument={handleReplaceDocument}
//           defaultType="FACTURE"
//           idDefaultOrigine={origine?.id}
//           defaultCorrespondant={currentLabo}
//           tvas={tvas}
//         />
//       )}
//     </Box>
//   );
// };

// export default ContratPage;
