import React from 'react';
import loadable from '@loadable/component';
import { SmallLoading } from '@lib/ui-kit';
import { ModuleDefinition } from '@lib/common';

const CatalogueProduitPage = loadable(() => import('./pages/CatalogueProduit'), {
  fallback: <SmallLoading />,
});

const activationParameters = {
  groupement: '0303',
  pharmacie: '0706',
};

const catalogueProduitModule: ModuleDefinition = {
  routes: [
    {
      attachTo: 'MAIN',
      path: ['/laboratoires/:idLabo/catalogue/:view'],
      component: CatalogueProduitPage,
      activationParameters,
    },
  ],
};

export default catalogueProduitModule;
