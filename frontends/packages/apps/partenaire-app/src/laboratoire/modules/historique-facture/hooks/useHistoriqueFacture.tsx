import { useState } from 'react';
import { QueryLazyOptions } from '@apollo/client';
import { useEffect } from 'react';
import { DocumentFacture, TotauxFacture } from '../pages/historique-facture/HistoriqueFacture';
import { QueryResult } from '@apollo/client';
import {
  GedDocumentFilter,
  Get_DocumentsQuery,
  Get_DocumentsQueryVariables,
  Get_OriginesQueryVariables,
  SortDirection,
  useCreate_Document_CategorieMutation,
  useDelete_Document_CategorieMutation,
  useGet_DocumentsLazyQuery,
  useGet_LaboratoireLazyQuery,
  useGet_OriginesLazyQuery,
  useJoindre_Commande_FactureMutation,
  useTotaux_FacturesLazyQuery,
  useUpdate_Document_CategorieMutation,
} from '@lib/common/src/federation';
import { useApplicationContext, useUploadFiles } from '@lib/common';
import { useTvasQuery } from '@lib/common/src/graphql';
import { Dispatch } from 'react';
import { useHistory } from 'react-router';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';

// const factureStatuts: FactureStatut[] = [
//   {
//     id: 'APPROUVE',
//     code: 'APPROUVE',
//     libelle: 'Validée',
//   },
//   {
//     id: 'REFUSE',
//     code: 'REFUSE',
//     libelle: 'Refusée',
//   },
// ];
interface UseHistoriqueProps {
  isSelectable?: boolean;
}

export const useHistoriqueFacture = ({
  isSelectable,
}: UseHistoriqueProps): [
  (change: any) => void,
  QueryResult<Get_DocumentsQuery, Get_DocumentsQueryVariables>,
  (facture: DocumentFacture) => void,
  (options?: QueryLazyOptions<Get_OriginesQueryVariables> | undefined) => void,
  boolean,
  (ids: string[], idDocument?: string) => void,
  () => void,
  boolean,
  (factureToDelete: DocumentFacture) => void,
  TotauxFacture,
  boolean,
  boolean,
  (newSaved: boolean) => void,
  (doc: any) => void,
  boolean,
  boolean,
  Dispatch<boolean>,
  any,
  any,
  any
] => {
  const { auth, currentPharmacie: pharmacie, notify, graphql, federation } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();
  const { push } = useHistory();

  const { params } = useLaboratoireParams('factures');

  const { idLabo } = params;

  // const [document, setDocument] = useState<DocumentFacture>();
  const [savingAssociation, setSavingAssociation] = useState<boolean>(false);
  const [savedAssociation, setSavedAssociation] = useState<boolean>(false);
  const [saving, setSaving] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  // const [openConsultDialog, setOpenConsultDialog] = useState<boolean>(false);

  const [loadLaboratoire, loadingLaboratoire] = useGet_LaboratoireLazyQuery();

  useEffect(() => {
    if (idLabo?.length) {
      loadLaboratoire({
        variables: { id: idLabo[0], idPharmacie: pharmacie.id },
      });
    }
  }, [JSON.stringify(idLabo)]);

  /** */
  const [getOrigines, gettingOrigines] = useGet_OriginesLazyQuery();

  const [getDocuments, gettingDocuments] = useGet_DocumentsLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  const { loading: loadingTva, error: errorTva, data: tvas } = useTvasQuery({ client: graphql });

  const [loadTotauxFacture, loadingTotauxFacture] = useTotaux_FacturesLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [joindreCommande] = useJoindre_Commande_FactureMutation({
    onCompleted: () => {
      notify({
        message: "L'association aux commandes a été effectuée avec succés !",
        type: 'success',
      });

      gettingDocuments.refetch && gettingDocuments.refetch();
      setSavingAssociation(false);
      setSavedAssociation(true);
    },
    onError: () => {
      notify({
        message: "Erreurs lors de l'association des commandes à la facture",
        type: 'error',
      });

      setSavingAssociation(false);
    },
  });

  const [addDocumentCategorie] = useCreate_Document_CategorieMutation({
    onCompleted: (data) => {
      setSaving(false);
      setSaved(true);

      gettingDocuments.refetch && gettingDocuments.refetch();
      loadingTotauxFacture.refetch && loadingTotauxFacture.refetch();
      notify({
        message: 'La Facture a été ajoutée avec succès !',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: "Des erreurs se sont survenues pendant l'ajout de la facture!",
        type: 'error',
      });

      setSaving(false);
    },
  });

  const showError = () => {
    notify({
      message: `Une erreur est survenue pendant le chargement du document`,
      type: 'error',
    });
  };

  const [updateDocumentCategorie] = useUpdate_Document_CategorieMutation({
    onCompleted: () => {
      setSaving(false);
      setSaved(true);

      gettingDocuments.refetch && gettingDocuments.refetch();
      loadingTotauxFacture.refetch && loadingTotauxFacture.refetch();
      notify({
        message: 'La Facture a été modifiée avec succès !',
        type: 'success',
      });
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification de la Facture!',
        type: 'error',
      });
      setSaving(false);
    },
  });

  const [deleteDocument] = useDelete_Document_CategorieMutation({
    onCompleted: () => {
      notify({
        message: 'Cette facture a été suprimée avec succès.',
        type: 'success',
      });
      gettingDocuments.refetch && gettingDocuments.refetch();
      loadingTotauxFacture.refetch && loadingTotauxFacture.refetch();
    },
    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la supression de la facture.',
        type: 'error',
      });
    },
  });

  const upsertDocument = ({
    id,
    idSousCategorie,
    description,
    nomenclature,
    numeroVersion,
    idUserVerificateur,
    idUserRedacteur,
    dateHeureParution,
    dateHeureDebutValidite,
    dateHeureFinValidite,
    motCle1,
    motCle2,
    motCle3,
    fichier,
    idOrigine,
    idOrigineAssocie,
    dateFacture,
    hT,
    tva,
    ttc,
    type,

    numeroFacture,
    factureDateReglement,
    idReglementMode,
    numeroCommande,
    isGenererCommande,
    avoirType,
    avoirCorrespondants,
    nombreJoursPreavis,
    isRenouvellementTacite,
    isMultipleTva,
    factureTvas,
  }: any): void => {
    if (!id) {
      // setMode('creation');
      addDocumentCategorie({
        variables: {
          input: {
            idSousCategorie: idSousCategorie,
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: new Date(dateHeureParution),
            dateHeureDebutValidite: new Date(dateHeureDebutValidite),
            dateHeureFinValidite: new Date(dateHeureFinValidite),
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,
            statut: 'APPROUVE',

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
            nombreJoursPreavis,
            isRenouvellementTacite,
            isMultipleTva,
            facturesTva: factureTvas,
          },
        },
      }).then(() => {
        gettingDocuments.refetch && gettingDocuments.refetch();
      });
    } else {
      // setMode('remplacement');
      updateDocumentCategorie({
        variables: {
          input: {
            description: description,
            nomenclature: nomenclature,
            numeroVersion: numeroVersion,
            dateHeureParution: dateHeureParution,
            dateHeureDebutValidite: dateHeureDebutValidite,
            dateHeureFinValidite: dateHeureFinValidite,
            idSousCategorie: idSousCategorie,
            idUserVerificateur: idUserVerificateur,
            idUserRedacteur: idUserRedacteur,
            motCle1: motCle1,
            motCle2: motCle2,
            motCle3: motCle3,
            fichier: fichier,
            idOrigine: idOrigine,
            idOrigineAssocie: idOrigineAssocie,
            factureDate: dateFacture,
            factureTotalHt: hT,
            factureTva: tva,
            factureTotalTtc: ttc,
            type: type,
            statut: 'APPROUVE',

            numeroFacture,
            factureDateReglement,
            idReglementMode,
            numeroCommande,
            isGenererCommande,
            avoirType,
            avoirCorrespondants,
            nombreJoursPreavis,
            isRenouvellementTacite,
            isMultipleTva,
            facturesTva: factureTvas,
          },
          id: id,
        },
      }).then(() => {
        gettingDocuments.refetch && gettingDocuments.refetch();
      });
    }
  };

  const handleConsult = (facture: DocumentFacture) => {
    // setDocument(facture);
    // setOpenConsultDialog(true);
  };

  const handleConsultGed = () => {
    push && push(`/espace-facturation`);
  };

  const handleJoindreCommande = (ids: string[], idDocument?: string) => {
    setSavedAssociation(false);
    setSavingAssociation(true);
    if (idDocument) {
      joindreCommande({
        variables: {
          input: {
            idDocument,
            idCommandes: ids,
          },
        },
      });
    }
  };

  const handleReplaceDocument = (doc: any): void => {
    setSaving(true);
    setSaved(false);
    if (doc.launchUpload) {
      uploadFiles([doc.selectedFile], {
        directory: 'relation-fournisseurs/historique-factures',
      })
        .then((uploadedFiles) => {
          upsertDocument({
            id: doc.id,
            idSousCategorie: doc.idSousCategorie,
            description: doc.description,
            nomenclature: doc.nomenclature,
            numeroVersion: doc.numeroVersion,
            idUserVerificateur: doc.idUserVerificateur,
            idUserRedacteur: doc.idUserRedacteur,
            dateHeureParution: doc.dateHeureParution,
            dateHeureDebutValidite: doc.dateHeureDebutValidite,
            dateHeureFinValidite: doc.dateHeureFinValidite,
            motCle1: doc.motCle1,
            motCle2: doc.motCle2,
            motCle3: doc.motCle3,
            fichier: uploadedFiles[0],
            idOrigine: doc?.origine?.id || doc?.origine,
            idOrigineAssocie: doc.correspondant.id,
            dateFacture: doc.dateFacture,
            hT: doc.hT ? parseFloat(doc.hT) : undefined,
            tva: doc.tva ? parseFloat(doc.tva) : undefined,
            ttc: doc.ttc ? parseFloat(doc.ttc) : undefined,
            type: doc.type,
            numeroFacture: doc?.numeroFacture,
            factureDateReglement: doc?.factureDateReglement,
            idReglementMode: doc?.idReglementMode,
            numeroCommande: doc?.numeroCommande,
            isGenererCommande: doc?.isGenererCommande,
            avoirType: doc?.avoirType,
            avoirCorrespondants: doc?.avoirCorrespondants,
            nombreJoursPreavis: doc?.nombreJoursPreavis,
            isRenouvellementTacite: doc?.isRenouvellementTacite,
            isMultipleTva: doc?.isMultipleTva,
            factureTvas: doc?.factureTvas,
          });
        })
        .catch((error) => {
          showError();
          setSaving(false);
        });
    } else {
      const { nomOriginal, chemin, type } = doc.previousFichier || {};

      upsertDocument({
        id: doc.id,
        idSousCategorie: doc.idSousCategorie,
        description: doc.description,
        nomenclature: doc.nomenclature,
        numeroVersion: doc.numeroVersion,
        idUserVerificateur: doc.idUserVerificateur,
        idUserRedacteur: doc.idUserRedacteur,
        dateHeureParution: doc.dateHeureParution,
        dateHeureDebutValidite: doc.dateHeureDebutValidite,
        dateHeureFinValidite: doc.dateHeureFinValidite,
        motCle1: doc.motCle1,
        motCle2: doc.motCle2,
        motCle3: doc.motCle3,
        fichier: doc.previousFichier ? { nomOriginal, chemin, type } : undefined,
        idOrigine: doc.origine?.id,
        idOrigineAssocie: doc.correspondant?.id,
        dateFacture: 'FACTURE' === doc.type ? doc.dateFacture : undefined,
        hT: 'FACTURE' === doc.type && doc.hT ? parseFloat(doc.hT) : undefined,
        tva: 'FACTURE' === doc.type && doc.tva ? parseFloat(doc.tva) : undefined,
        ttc: 'FACTURE' === doc.type && doc.ttc ? parseFloat(doc.ttc) : undefined,
        type: doc.type,

        numeroFacture: doc?.numeroFacture,
        factureDateReglement: doc?.factureDateReglement,
        idReglementMode: doc?.idReglementMode,
        numeroCommande: doc?.numeroCommande,
        isGenererCommande: doc?.isGenererCommande,
        avoirType: doc?.avoirType,
        avoirCorrespondants: doc?.avoirCorrespondants,
        nombreJoursPreavis: doc?.nombreJoursPreavis,
        isRenouvellementTacite: doc?.isRenouvellementTacite,
        isMultipleTva: doc?.isMultipleTva,
        factureTvas: doc?.factureTvas,
      });
    }
  };

  const handleDeleteFacture = (factureToDelete: DocumentFacture) => {
    if (factureToDelete?.id) {
      deleteDocument({
        variables: {
          input: {
            id: factureToDelete.id,
          },
        },
      });
    }
  };

  useEffect(() => {
    getOrigines();
  }, []);

  const origine = (gettingOrigines.data?.origines.nodes || []).find((origine) => origine.code === 'LABORATOIRE');

  const searchDocuments = (change: any) => {
    const filterAnd: GedDocumentFilter[] = [
      {
        idOrigine: {
          eq: origine?.id,
        },
      },
      {
        idPharmacie: {
          eq: pharmacie.id,
        },
      },
      {
        type: {
          eq: 'FACTURE',
        },
      },
    ];

    if (idLabo?.length)
      filterAnd.push({
        idOrigineAssocie: {
          in: idLabo,
        },
      });

    if (change.searchText) {
      filterAnd.push({
        numeroFacture: {
          iLike: `%${change.searchText}%`,
        },
      } as any);
    }

    if (isSelectable) {
      filterAnd.push({
        factureTotalHt: {
          gte: 0,
        },
      } as any);
      filterAnd.push({
        factureTotalHt: {
          gte: 0,
        },
      } as any);
    }

    if (origine?.id) {
      const sortTableFilter = change?.sorting || [
        {
          field: 'createdAt',
          direction: SortDirection.Desc,
        },
      ];
      getDocuments({
        variables: {
          filter: {
            and: filterAnd,
          },
          paging: {
            offset: change.skip ? parseInt(change.skip) : undefined,
            limit: change.take ? parseInt(change.take) : undefined,
          },
          sorting: sortTableFilter,
        },
      });
      loadTotauxFacture({
        variables: {
          idPharmacie: pharmacie.id,
          idOrigine: origine.id,
          idOrigineAssocie: idLabo?.length ? idLabo[0] : undefined,
          noAvoir: isSelectable || false,
          searchText: change.searchText ? `%${change.searchText}%` : undefined,
        },
      });
    }
  };
  useEffect(() => {
    searchDocuments({ searchText: undefined, skip: 0, take: 12 });
  }, [origine]);

  // const ConsultModal = (
  //   <CustomModal
  //     open={openConsultDialog}
  //     setOpen={setOpenConsultDialog}
  //     title="Facture"
  //     closeIcon
  //     withBtnsActions={false}
  //     headerWithBgColor
  //     disableBackdropClick={true}
  //     fullWidth
  //     maxWidth="md"
  //     fullScreen={!!isMobile()}
  //   >
  //     <iframe
  //       id={document?.id}
  //       style={{
  //         height: 600,
  //         border: 'none',
  //       }}
  //       src={`https://docs.google.com/viewerng/viewer?url=${document?.fichier?.publicUrl}&embedded=true`}
  //       frameBorder="0"
  //       width="100%"
  //     />
  //   </CustomModal>
  // );

  // const detailComponent = (
  //   <DetailFacture
  //     saving={saving}
  //     saved={saved}
  //     setSaved={setSaved}
  //     activeDocument={document}
  //     onRequestReplace={handleReplaceDocument}
  //     onRequestDelete={handleDeleteFacture}
  //     tvas={{
  //       loading: loadingTva,
  //       error: errorTva as any,
  //       data: tvas?.tvas || [],
  //     }}
  //   />
  // );

  return [
    searchDocuments,
    gettingDocuments as any,
    handleConsult,
    getOrigines,
    gettingOrigines.loading,
    handleJoindreCommande,
    handleConsultGed,
    auth.isAuthorizedToAddGedDocument(),
    handleDeleteFacture,
    {
      totalHT: loadingTotauxFacture?.data?.getTotauxFactures?.totalHT || 0,
      totalTVA: loadingTotauxFacture?.data?.getTotauxFactures?.totalTVA || 0,
      totalTTC: loadingTotauxFacture?.data?.getTotauxFactures?.totalTTC || 0,
    },
    savingAssociation,
    savedAssociation,
    setSavedAssociation,
    handleReplaceDocument,
    saving,
    saved,
    setSaved,
    loadingLaboratoire.data?.laboratoire,
    origine,
    {
      loading: loadingTva,
      error: errorTva as any,
      data: tvas?.tvas || [],
    },
  ];
};
