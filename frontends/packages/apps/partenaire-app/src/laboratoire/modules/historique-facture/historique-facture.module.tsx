import React from 'react';
import loadable from '@loadable/component';

import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';

const HistoriqueFacturePage = loadable(() => import('./pages/historique-facture/HistoriqueFacture'), {
  fallback: <SmallLoading />,
});

const historiqueFactureModule: ModuleDefinition = {
  routes: [
    {
      attachTo: 'MAIN',
      path: '/laboratoires/factures/',
      component: HistoriqueFacturePage,
      activationParameters: {
        groupement: '0305',
        pharmacie: '0708',
      },
    },
  ],
};

export default historiqueFactureModule;
