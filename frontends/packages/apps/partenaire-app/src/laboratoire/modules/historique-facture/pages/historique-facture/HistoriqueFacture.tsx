import CreateDocument from '@lib/common/src/components/EspaceDocumentaire/Content/CreateDocument';
import { ConfirmDeleteDialog, NewCustomButton, Table, TableChange, TopBar } from '@lib/ui-kit';
import { SearchInputBase } from '@lib/ui-kit/src/components/atoms/SearchInputBase';
import { Box, Divider, Fab, TableCell, TableRow, Typography, useTheme } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { Add } from '@material-ui/icons';
import React, { FC, MouseEvent, useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { SortDirection } from '@lib/common/src/federation';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import { round2Decimals } from '../../../../util';
import { useStyles as useCommonStyles } from '../../../commonStyles';
import { JoindreCommande } from '../../components/joindre-commande.tsx/JoindreCommande';
import { useColumns } from '../../hooks/columns';
import { useHistoriqueFacture } from '../../hooks/useHistoriqueFacture';
import { useStyles } from './styles';

interface User {
  id: string;
  fullName: string;
}

interface Fichier {
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string;
}

export interface TotauxFacture {
  totalHT?: number;
  totalTVA?: number;
  totalTTC?: number;
}

export interface FactureStatut {
  id?: string;
  code: string;
  libelle: string;
}

export interface DocumentFacture {
  id?: string;
  description?: string;
  nomenclature?: string;
  numeroVersion?: string;
  motCle1?: string;
  motCle2?: string;
  motCle3?: string;
  dateHeureParution?: Date;
  dateHeureDebutValidite?: Date;
  dateHeureFinValidite?: Date;
  idDocumentARemplacer?: string;
  idUserRedacteur?: string;
  idUserVerificateur?: string;
  idFichier?: string;
  idSousCategorie?: string;
  idPharmacie?: string;
  idGroupement?: string;
  createdAt?: Date;
  updatedAt?: Date;
  createdBy?: {
    id: string;
  };
  favoris?: boolean;
  nombreConsultations?: number;
  nombreTelechargements?: number;
  nombreCommentaires?: number;
  nombreReactions?: number;
  sousCategorie?: {
    id?: string;
    libelle?: string;
  };
  fichier?: Fichier;
  verificateur?: User;
  redacteur?: User;
  categorie?: {
    id?: string;
    libelle?: string;
  };
  dernierChangementStatut?: {
    id?: string;
    idDocument?: string;
    status?: string;
    commentaire?: string;
    idGroupement?: string;
    createdBy?: string;
    updatedBy?: string;
    createdAt?: Date;
    updatedAt?: Date;
  };
  documentARemplacer?: DocumentFacture;
  idOrigine?: string;
  idOrigineAssocie?: string;
  factureDate?: Date;
  factureTotalHt?: number;
  factureTva?: number;
  factureTotalTtc?: number;
  type: 'FACTURE' | 'CATEGORIE';
  factureDateReglement?: Date;
  idCommandes?: string[];
  idReglementMode?: string;
  numeroFacture?: string;
  typeAvoirAssociations?: {
    idDocument: string;
    type: string;
    correspondant: string;
  }[];
  commande?: {
    id?: string;
    nbrRef?: number;
  };

  numeroCommande?: number;
  isGenererCommande?: boolean;
  avoirType?: string;
  avoirCorrespondants?: string[];
  origineAssocie?: { id: string; nom: string };
}

export interface HistoriqueFactureProps {
  isSelectable?: boolean;
  onRequestSelected?: (factures: DocumentFacture[]) => void;
  selectedItemsIds?: string[];
}

const HistoriqueFacturePage: FC<HistoriqueFactureProps> = ({ isSelectable, onRequestSelected, selectedItemsIds }) => {
  const classes = useStyles();

  const { push } = useHistory();

  const { params, redirectTo } = useLaboratoireParams('factures');
  const { idLabo, take, skip, searchText, sorting } = params;

  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openJoindreDialog, setOpenJoindreDialog] = useState<boolean>(false);
  const [documentToShow, setDocumentToShow] = useState<DocumentFacture | undefined>();
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const [facturesSelected, setFacturesSelected] = useState<DocumentFacture[]>([]);
  const [showSelected, setShowSelected] = useState<boolean>(false);
  const [openReplaceDocument, setOpenReplaceDocument] = useState<boolean>(false);

  // const [filter, setFilter] = useState<any>();
  const open = Boolean(anchorEl);

  const toggleOpenJoindreDialog = () => {
    if (!openJoindreDialog) {
      // if closed, set document to show undefined
      setDocumentToShow(undefined);
    }
    setOpenJoindreDialog((prev) => !prev);
  };

  const [
    onRequestSearch,
    gettingDocuments,
    onRequestConsultFacture,
    getOrigines,
    originesLoading,
    handleJoindreCommande,
    onConsultGed,
    isAuthorizedToAddDocument,
    onRequestDelete,
    totaux,
    savingAssociation,
    savedAssociation,
    setSavedAssociation,
    handleReplaceDocument,
    saving,
    saved,
    setSaved,
    currentLabo,
    origine,
    tvas,
  ] = useHistoriqueFacture({});

  useEffect(() => {
    getOrigines();
  }, []);

  useEffect(() => {
    if (saved) {
      setOpenReplaceDocument(false);
      setDocumentToShow(undefined);
      setSaved(false);
    }
  }, [saved]);

  useEffect(() => {
    if (savedAssociation) {
      setOpenJoindreDialog(false);
      // setSavedAssociation(false);
    }
  }, [savedAssociation]);

  const data: DocumentFacture[] = gettingDocuments.data?.gedDocuments.nodes || ([] as any);

  useEffect(() => {
    onRequestSearch({ skip, take, searchText, sorting });
  }, [
    skip,
    take,
    searchText,
    JSON.stringify(idLabo),
    sorting && sorting[0] && sorting[0].direction,
    sorting && sorting[0] && sorting[0].field,
  ]);

  useEffect(() => {
    if (selectedItemsIds?.length && (data || []).length > 0) {
      const selectedRows = (data || []).filter((item) => selectedItemsIds.includes(item.id || ''));
      setFacturesSelected(selectedRows);
    }
  }, [selectedItemsIds, data]);

  useEffect(() => {
    if (saved) {
      setOpenJoindreDialog(false);
      if (setSaved) setSaved(false);
    }
  }, [saved]);

  const handleSearchTable = ({ skip, take, searchText, sortBy, sortDirection }: TableChange) => {
    redirectTo({
      skip: skip ? skip.toString() : undefined,
      take: take ? take.toString() : undefined,
      searchText,
      sorting: sortBy && sortDirection ? [{ field: sortBy, direction: sortDirection }] : undefined,
    });
  };

  const handleShowMenuClick = (__facture: DocumentFacture, event: MouseEvent<HTMLElement>): void => {
    setAnchorEl(event.currentTarget);
    setDocumentToShow(__facture);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const handleConsultDocument = (facture: DocumentFacture): void => {
    onRequestConsultFacture && onRequestConsultFacture(facture);
    // push(`/laboratoires/factures/${documentToShow?.id}`);
    redirectTo({ id: facture.id, backParams: params }, 'facture-detail');
  };

  const handleOpenJoindreCommande = (facture: DocumentFacture): void => {
    setOpenJoindreDialog(true);
  };

  const handleConfirmJoindreCommande = (idsCommandes: string[]) => {
    handleJoindreCommande(idsCommandes || [], documentToShow?.id);
  };

  const handleModifier = (factureToEdit?: DocumentFacture) => {
    setOpenReplaceDocument(true);
  };

  const handleSupprimer = () => {
    setOpenDeleteDialog(true);
  };

  const handleConfirmDeleteOne = (): void => {
    if (onRequestDelete && documentToShow) {
      onRequestDelete(documentToShow);
      setOpenDeleteDialog(false);
    }
  };

  const handleSelectAll = () => {
    if (facturesSelected.length === data?.length) {
      setFacturesSelected([]);
    } else {
      setFacturesSelected(data || []);
    }
  };

  const handleSelectChange = (dataSelected: any) => {
    setFacturesSelected(dataSelected);
  };

  const handleSortTable = (column: any, direction: SortDirection) => {
    redirectTo({ ...params, sorting: [{ field: column, direction }] });
  };

  // const handleChangeFilter = (data: any) => {
  //   setFilter(data);
  // };

  const columns = useColumns({
    handleShowMenuClick,
    handleClose,
    anchorEl,
    open,
    handleConsultDocument,
    handleJoindreCommande: handleOpenJoindreCommande,
    documentToShow,
    isAuthorizedToAddDocument,
    handleModifier,
    handleSupprimer,
    isSelectable,
  });

  const theme = useTheme();
  const match = useMediaQuery(theme.breakpoints.down('sm'));
  const commonStyles = useCommonStyles();

  const handleChangeFilter = (_data: any) => {};

  return (
    <Box className={classes.tableBox}>
      <>
        {match ? (
          <>
            <Box className={classes.historiqueHeader} mt={3}>
              <div className={classes.search}>
                <SearchInputBase dark onChange={function noRefCheck() {}} value="" placeholder="Recherche" />
              </div>
            </Box>

            <div className={classes.historiqueRoot}>
              <div className={classes.historiqueTitre}>
                <div>
                  <Box className={classes.titre}>Total TTC</Box>
                  <Typography className={classes.sousTitre}>{round2Decimals(totaux?.totalTTC).toFixed(2)}</Typography>
                </div>
                <div>
                  <Box className={classes.titre}>Total HT</Box>
                  <Typography className={classes.sousTitre}>{round2Decimals(totaux?.totalHT).toFixed(2)}</Typography>
                </div>
                <div>
                  <Box className={classes.titre}>Total TVA</Box>
                  <Typography className={classes.sousTitre}>{round2Decimals(totaux?.totalTVA).toFixed(2)}</Typography>
                </div>
              </div>
            </div>
            <div>
              {data?.map((item) => (
                <>
                  <div className={classes.historiqueItems}>
                    <div>
                      <div className={classes.historiqueListeItems}>
                        <div className={classes.commande}>
                          <div className={classes.facture}>
                            <Typography className={classes.numeroCommande}>N° facture: </Typography>
                            <Typography>{item.numeroFacture}</Typography>
                            <Typography className={classes.status}>{status}</Typography>
                          </div>
                          <div>
                            <Typography className={classes.numeroCommande}>N° commande: </Typography>
                            <Typography>{item.numeroCommande}</Typography>
                          </div>
                        </div>
                        <Typography className={classes.numeroCommande}>{item.numeroCommande}</Typography>
                      </div>
                      {/* <div className={classes.date}>
                            {moment(item.factureDateReglement).format('DD/MM/YYYY')}
                          </div> */}
                    </div>
                    <div className={classes.historique}>
                      <div className={classes.historiqueTitreItems}>
                        <div>
                          <Box className={classes.historiqueDetails}>Total TTC</Box>
                          <Typography className={classes.totalHistorique}>
                            {round2Decimals(item.factureTotalTtc).toFixed(2)}
                          </Typography>
                        </div>
                        <div>
                          <Box className={classes.historiqueDetails}>Total HT</Box>
                          <Typography className={classes.totalHistorique}>
                            {round2Decimals(item.factureTotalHt).toFixed(2)}
                          </Typography>
                        </div>
                        <div>
                          <Box className={classes.historiqueDetails}>Total TVA</Box>
                          <Typography className={classes.totalHistorique}>
                            {round2Decimals(item.factureTva).toFixed(2)}
                          </Typography>
                        </div>
                      </div>
                    </div>
                  </div>
                  <Divider />
                </>
              ))}
            </div>
            {/* <div
                  onClick={() => onRequestEdit && onRequestEdit()}
                  className={classes.bouttonAdd}
                >
                  <AddIcon />
                </div> */}
            <Fab
              className={commonStyles.fab}
              onClick={() => setOpenReplaceDocument(true)}
              variant="round"
              color="secondary"
            >
              <Add />
            </Fab>
            {/* <HistoriqueForm
                  open={ajoutCommande}
                  setOpen={toggleOpenEditDialog}
                  mode={historiqueToEdit ? 'modification' : 'creation'}
                  onRequestSave={onRequestSave}
                  saving={saving}
                  commandeToEdit={historiqueToEdit}
                /> */}
            {/* <CustomModal
                  open={ajout}
                  setOpen={toggleOpenJoindreDialog}
                  title="Joindre une commande"
                  withBtnsActions
                  closeIcon
                  headerWithBgColor
                  disabledButton={saving}
                  onClickConfirm={handleConfirmJoindreCommande}
                  withCancelButton={false}
                  actionButtonTitle="Joindre"
                  maxWidth="lg"
                >
                  <Box minWidth={960}>{historiqueCommandeComponent}</Box>
                </CustomModal> */}
          </>
        ) : (
          <Table
            onSortColumn={handleSortTable}
            error={gettingDocuments.error as any}
            loading={gettingDocuments.loading || originesLoading}
            data={data || []}
            additionalRows={
              <TableRow>
                {isSelectable && <TableCell />}
                <TableCell style={{ borderBottomColor: '#fff' }} />
                <TableCell style={{ borderBottomColor: '#fff' }} />
                <TableCell style={{ borderBottomColor: '#fff' }} />
                <TableCell style={{ borderBottomColor: '#fff' }}>
                  <span
                    style={{
                      display: 'flex',
                      justifyContent: 'flex-end',
                      borderBottomColor: '#fff',
                    }}
                  >
                    Total
                  </span>
                </TableCell>
                <TableCell style={{ borderBottomColor: '#fff' }} className={classes.tableMonetaire}>
                  {round2Decimals(totaux?.totalTTC || 0).toFixed(2)}&nbsp;€
                </TableCell>
                <TableCell style={{ borderBottomColor: '#fff' }} className={classes.tableMonetaire}>
                  {round2Decimals(totaux?.totalHT || 0).toFixed(2)}&nbsp;€
                </TableCell>
                <TableCell style={{ borderBottomColor: '#fff' }} className={classes.tableMonetaire}>
                  {round2Decimals(totaux?.totalTVA || 0).toFixed(2)}&nbsp;€
                </TableCell>
                <TableCell style={{ borderBottomColor: '#fff' }} />
              </TableRow>
            }
            search={false}
            columns={columns}
            rowsTotal={gettingDocuments.data?.gedDocuments.nodes.length || 0}
            selectable={isSelectable}
            noTableToolbarSearch
            onClearSelection={isSelectable ? () => setFacturesSelected([]) : undefined}
            onSelectAll={isSelectable ? handleSelectAll : undefined}
            rowsSelected={isSelectable ? facturesSelected : undefined}
            onRowsSelectionChange={isSelectable ? handleSelectChange : undefined}
            onShowSelected={() => {
              setShowSelected(!showSelected);
            }}
            topBarComponent={(searchComponent) => (
              <>
                {match ? (
                  <Box display="flex" alignItems="center" padding="10px !important">
                    <TopBar
                      searchComponent={<></>}
                      titleTopBar="Liste des factures"
                      withFilter
                      titleButton=""
                      fullwidth
                      noAddBtn
                      // filterComponent={
                      //   <Filter
                      //     filters={[
                      //       {
                      //         titre: 'Statut',
                      //         itemFilters: (factureStatuts || []).map((item) => ({
                      //           ...item,
                      //           keyword: 'code',
                      //         })) as any,
                      //       },
                      //     ]}
                      //     onChange={handleChangeFilter}
                      //     filterIcon={filterIcon}
                      //   />
                      // }
                    />
                  </Box>
                ) : (
                  <Box display="flex" alignItems="center" padding="10px">
                    <TopBar
                      searchComponent={<></>}
                      // titleTopBar="Liste des factures"
                      withFilter
                      titleButton=""
                      fullwidth
                      noAddBtn
                      // filterComponent={
                      //   <Filter
                      //     filters={[
                      //       {
                      //         titre: 'Statut',
                      //         itemFilters: (factureStatuts || []).map((item) => ({
                      //           ...item,
                      //           keyword: 'code',
                      //         })) as any,
                      //       },
                      //     ]}
                      //     onChange={handleChangeFilter}
                      //     filterIcon={filterIcon}
                      //   />
                      // }
                    />

                    {!isSelectable && (
                      <Box pl={2}>
                        <NewCustomButton
                          style={{ minWidth: 300, minHeight: 40 }}
                          variant="outlined"
                          onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
                            event.preventDefault();
                            event.stopPropagation();
                            onConsultGed && onConsultGed();
                          }}
                          className={classes.espaceFacturetion}
                        >
                          Consulter espace facturation
                        </NewCustomButton>
                      </Box>
                    )}
                    {isAuthorizedToAddDocument && !isSelectable && (
                      <Box pl={2}>
                        <NewCustomButton
                          style={{ minWidth: 230, minHeight: 40 }}
                          onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
                            event.preventDefault();
                            event.stopPropagation();
                            setOpenReplaceDocument(true);
                          }}
                          startIcon={<Add />}
                          className={classes.nouvelleFacture}
                        >
                          Nouvelle Facture
                        </NewCustomButton>
                      </Box>
                    )}
                  </Box>
                )}
              </>
            )}
            onRunSearch={handleSearchTable}
          />
        )}
      </>
      {isSelectable && (
        <Box display="flex" justifyContent="flex-end">
          <NewCustomButton
            onClick={(event: React.MouseEvent<HTMLButtonElement>) => {
              event.preventDefault();
              event.stopPropagation();
              onRequestSelected && onRequestSelected(facturesSelected);
            }}
            className={classes.joindreButton}
            disabled={!facturesSelected?.length}
          >
            Joindre
          </NewCustomButton>
        </Box>
      )}
      {openJoindreDialog && (
        <JoindreCommande
          selectedIds={documentToShow?.idCommandes}
          onRequestSelected={handleConfirmJoindreCommande}
          idDocument={documentToShow?.id}
          open={openJoindreDialog}
          setOpen={toggleOpenJoindreDialog}
          saving={savingAssociation}
          saved={savedAssociation}
          setSaved={setSavedAssociation}
        />
      )}
      <ConfirmDeleteDialog
        title="Suppression"
        content="Etes-vous sûr de vouloir supprimer cette Facture ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDeleteOne}
      />

      {openReplaceDocument && (
        <CreateDocument
          open={openReplaceDocument}
          setOpen={setOpenReplaceDocument}
          title={documentToShow ? 'Modifier facture' : 'Nouvelle facture'}
          saving={saving}
          activeSousCategorie={documentToShow?.sousCategorie}
          document={documentToShow}
          mode={documentToShow ? 'remplacement' : 'creation'}
          onRequestCreateDocument={handleReplaceDocument}
          defaultType="FACTURE"
          idDefaultOrigine={origine?.id}
          defaultCorrespondant={currentLabo}
          tvas={tvas}
        />
      )}
    </Box>
  );
};

export default HistoriqueFacturePage;
