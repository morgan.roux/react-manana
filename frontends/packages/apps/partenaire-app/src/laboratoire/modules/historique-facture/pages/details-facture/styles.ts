import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      paddingLeft: 24,
      backgroundColor: theme.palette.common.white,
    },
    top: {
      display: 'flex',
      alignContent: 'center',
      justifyContent: 'space-between',
      width: '100%',
      padding: 32,
      paddingBottom: 8,
    },
    titre3: {
      fontSize: 16,
      fontWeight: 'bold',
      margin: '0 0 24px 0',
    },
  })
);
