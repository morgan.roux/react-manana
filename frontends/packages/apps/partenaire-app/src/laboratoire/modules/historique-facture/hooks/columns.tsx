import React, { MouseEvent } from 'react';
import { Box, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { useStyles } from './styles';
import { AttachFile, Delete, Edit, MoreHoriz, Visibility } from '@material-ui/icons';
import moment from 'moment';
import { Column } from '@lib/ui-kit';
import { DocumentFacture } from '../pages/historique-facture/HistoriqueFacture';
import { round2Decimals } from '../../../util';
// import { Fichier } from '@lib/types';

interface UseColumnProps {
  handleShowMenuClick: (facture: DocumentFacture, event: MouseEvent<HTMLElement>) => void;
  handleClose: () => void;
  anchorEl: HTMLElement | null;
  open: boolean;

  handleConsultDocument: (row: DocumentFacture) => void;
  handleJoindreCommande?: (row: DocumentFacture) => void;
  documentToShow?: DocumentFacture;
  isAuthorizedToAddDocument?: boolean;
  handleModifier?: () => void;
  handleSupprimer?: () => void;
  isSelectable?: boolean;
}

export const useColumns = ({
  handleShowMenuClick,
  handleClose,
  anchorEl,
  open,
  handleConsultDocument,
  handleJoindreCommande,
  documentToShow,
  isAuthorizedToAddDocument,
  handleModifier,
  handleSupprimer,
  isSelectable,
}: UseColumnProps) => {
  const classes = useStyles();
  const columns: Column[] = [
    {
      name: 'numeroFacture',
      label: 'Numéro de facture',
      sortable: true,
      renderer: (row: DocumentFacture) => {
        return row?.numeroFacture ? (
          <Box
            onClick={(event) => {
              event.stopPropagation();
              handleConsultDocument(row);
            }}
          >
            <Typography className={classes.tableText}>{row.numeroFacture}</Typography>
          </Box>
        ) : (
          '-'
        );
      },
    },
    {
      name: 'numeroCommande',
      label: 'Numéro de commande',
      sortable: true,
      renderer: (row: DocumentFacture) => {
        const numeroCommande = row?.numeroCommande || row?.commande?.nbrRef;
        return numeroCommande ? (
          <Box
            onClick={(event) => {
              event.stopPropagation();
              handleConsultDocument(row);
            }}
          >
            <Typography className={classes.tableText}>{numeroCommande}</Typography>
          </Box>
        ) : (
          '-'
        );
      },
    },
    {
      name: 'labo',
      label: 'Labo',
      renderer: (row: DocumentFacture) => {
        return (
          <Box
            onClick={(event) => {
              event.stopPropagation();
              handleConsultDocument(row);
            }}
          >
            <Typography className={classes.tableText}>{row.origineAssocie?.nom}</Typography>
          </Box>
        );
      },
    },
    {
      name: 'factureDate',
      label: 'Date facture',
      sortable: true,
      renderer: (row: DocumentFacture) => {
        return row?.factureDate ? (
          <Box
            onClick={(event) => {
              event.stopPropagation();
              handleConsultDocument(row);
            }}
          >
            <Typography className={classes.tableText}>{moment(row.factureDate).format('DD-MM-YYYY')}</Typography>
          </Box>
        ) : (
          '-'
        );
      },
    },
    {
      name: 'factureDateReglement',
      label: 'Date de règlement',
      sortable: true,
      renderer: (row: DocumentFacture) => {
        return row?.factureDateReglement ? (
          <Box
            onClick={(event) => {
              event.stopPropagation();
              handleConsultDocument(row);
            }}
          >
            <Typography className={classes.tableText}>
              {moment.utc(row.factureDateReglement).format('DD-MM-YYYY')}
            </Typography>
          </Box>
        ) : (
          '-'
        );
      },
    },
    {
      name: 'factureTotalTtc',
      label: 'Total TTC',
      sortable: true,
      renderer: (row: DocumentFacture) => {
        return (
          <Box
            onClick={(event) => {
              event.stopPropagation();
              handleConsultDocument(row);
            }}
          >
            <Typography className={classes.tableMonetaire}>
              {round2Decimals(row?.factureTotalTtc || 0).toFixed(2)}&nbsp;€
            </Typography>
          </Box>
        );
      },
    },
    {
      name: 'factureTotalHt',
      label: 'Total HT',
      sortable: true,
      renderer: (row: DocumentFacture) => {
        return (
          <Box
            onClick={(event) => {
              event.stopPropagation();
              handleConsultDocument(row);
            }}
          >
            <Typography className={classes.tableMonetaire}>
              {round2Decimals(row?.factureTotalHt || 0).toFixed(2)}&nbsp;€
            </Typography>
          </Box>
        );
      },
    },
    {
      name: 'factureTva',
      label: 'TVA',
      sortable: true,
      renderer: (row: DocumentFacture) => {
        return (
          <Box
            onClick={(event) => {
              event.stopPropagation();
              handleConsultDocument(row);
            }}
          >
            <Typography className={classes.tableMonetaire}>
              {round2Decimals(row?.factureTva || 0).toFixed(2)}&nbsp;€
            </Typography>
          </Box>
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: DocumentFacture) => {
        return isSelectable ? (
          ' '
        ) : (
          <div key={`row-${row.id}`}>
            <IconButton
              aria-controls="simple-menu"
              aria-haspopup="true"
              // eslint-disable-next-line react/jsx-no-bind
              onClick={handleShowMenuClick.bind(null, row)}
            >
              <MoreHoriz />
            </IconButton>
            <Menu
              id="fade-menu"
              anchorEl={anchorEl}
              keepMounted
              open={open && row.id === documentToShow?.id}
              onClose={handleClose}
            >
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  handleConsultDocument(row);
                }}
              >
                <ListItemIcon>
                  <Visibility />
                </ListItemIcon>
                <Typography variant="inherit">Voir détail</Typography>
              </MenuItem>
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  handleJoindreCommande && handleJoindreCommande(row);
                }}
              >
                <ListItemIcon>
                  <AttachFile />
                </ListItemIcon>
                <Typography variant="inherit">Associer à une commande</Typography>
              </MenuItem>
              {isAuthorizedToAddDocument && (
                <MenuItem
                  onClick={(event) => {
                    event.stopPropagation();
                    handleClose();
                    handleModifier && handleModifier();
                  }}
                >
                  <ListItemIcon>
                    <Edit />
                  </ListItemIcon>
                  <Typography variant="inherit">Modifier</Typography>
                </MenuItem>
              )}
              {isAuthorizedToAddDocument && (
                <MenuItem
                  onClick={(event) => {
                    event.stopPropagation();
                    handleClose();
                    handleSupprimer && handleSupprimer();
                  }}
                >
                  <ListItemIcon>
                    <Delete />
                  </ListItemIcon>
                  <Typography variant="inherit">Supprimer</Typography>
                </MenuItem>
              )}
            </Menu>
          </div>
        );
      },
    },
  ];
  return columns;
};
