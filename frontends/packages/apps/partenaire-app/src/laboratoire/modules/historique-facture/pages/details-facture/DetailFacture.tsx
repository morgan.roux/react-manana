import { DocumentCategorie, useApplicationContext } from '@lib/common';
import Content from '@lib/common/src/components/EspaceDocumentaire/Content/Content';
import {
  useCreate_ActiviteMutation,
  useGet_Document_CategorieLazyQuery,
  useToggle_Document_To_FavoriteMutation,
} from '@lib/common/src/federation';
import { useCommentsLazyQuery, useUser_SmyleysLazyQuery } from '@lib/common/src/graphql';
import { Box } from '@material-ui/core';
import { Clear } from '@material-ui/icons';
import React, { FC, useEffect } from 'react';
import { useParams } from 'react-router';
import { useLaboratoireParams } from '../../../../hooks/useLaboratoireParams';
import { useHistoriqueFacture } from '../../hooks/useHistoriqueFacture';

import { useStyles } from './styles';

const DetailFacture: FC<{}> = () => {
  const classes = useStyles();

  const { graphql, notify } = useApplicationContext();

  const { redirectTo, params, goBack } = useLaboratoireParams('compte-rendu');

  const [
    onRequestSearch,
    gettingDocuments,
    onRequestConsultFacture,
    getOrigines,
    originesLoading,
    handleJoindreCommande,
    onConsultGed,
    isAuthorizedToAddDocument,
    onRequestDelete,
    totaux,
    savingAssociation,
    savedAssociation,
    setSavedAssociation,
    onRequestReplace,
    saving,
    saved,
    setSaved,
    currentLabo,
    origine,
    tvas,
  ] = useHistoriqueFacture({});

  const [getDocument, gettingDocument] = useGet_Document_CategorieLazyQuery({
    fetchPolicy: 'network-only',
  });

  const activeDocument = gettingDocument.data?.gedDocument;

  useEffect(() => {
    if (params.id) {
      getDocument({
        variables: {
          id: params.id,
        },
      });
    }
  }, [params.id]);

  const [
    getCommentaires,
    { loading: commentsLoading, error: commentsError, data: comments, refetch: refetchComments },
  ] = useCommentsLazyQuery({ client: graphql });

  const [loadUserSmyleys, loadingUserSmyleys] = useUser_SmyleysLazyQuery();

  const [toggleDocumentFavorite, { loading: toggleLoading, error: toggleError, data: toggle }] =
    useToggle_Document_To_FavoriteMutation({
      onError: () => {
        notify({
          message: "Des erreurs se sont survenues pendant l'ajout du document aux favoris!",
          type: 'error',
        });
      },
    });

  const [addActivite] = useCreate_ActiviteMutation({
    // onCompleted: () => {
    //   refetchDocument();
    // },
  });

  useEffect(() => {
    if (toggle?.toggleToGedDocumentFavoris && !toggleLoading && !toggleError) {
      notify({
        message: `Le document a été ${
          toggle?.toggleToGedDocumentFavoris?.favoris ? 'ajouté aux' : 'retiré des'
        } favoris!`,
        type: 'success',
      });
    }
  }, [toggle, toggleLoading, toggleError]);

  const handleRefetchComments = () => {
    refetchComments && refetchComments();
  };

  useEffect(() => {
    if (activeDocument) {
      getCommentaires({
        variables: { codeItem: 'GED_DOCUMENT', idItemAssocie: activeDocument.id },
      });
      loadUserSmyleys({
        variables: {
          codeItem: 'GED_DOCUMENT',
          idSource: activeDocument.id,
        },
      });
    }
  }, [activeDocument]);

  const handleRefecthSmyleys = () => {
    loadingUserSmyleys.refetch && loadingUserSmyleys.refetch();
  };

  const handleToggleDocumentToOrFromFavorite = (document: DocumentCategorie): void => {
    toggleDocumentFavorite({
      variables: {
        idDocument: document?.id as any,
      },
    });
  };

  const handleFetchMoreComments = (document: DocumentCategorie): void => {};
  const handleDeplace = (document: DocumentCategorie): void => {};

  const handleCompleteDownload = (document: DocumentCategorie): void => {
    addActivite({
      variables: {
        input: {
          activiteTypeCode: 'DOWNLOAD',
          itemCode: 'GED_DOCUMENT',
          idItemAssocie: document.id as any,
        },
      },
    });
  };

  console.log('comments facture', comments);

  return (
    <Box className={classes.container}>
      <div className={classes.top}>
        <div>
          <h2 className={classes.titre3}>Détail Facture</h2>
        </div>
        <div onClick={() => goBack('factures')} style={{ cursor: 'pointer' }}>
          <Clear />
        </div>
      </div>
      <Content
        saving={saving}
        saved={saved}
        setSaved={setSaved}
        activeSousCategorie={undefined}
        activeDocument={activeDocument}
        mobileView={window.innerWidth < 1024}
        tabletView={window.innerWidth < 1366}
        validation
        content={{ loading: false, error: false as any, data: activeDocument as any }}
        commentaires={{ loading: commentsLoading, error: commentsError as any, data: comments?.comments?.data }}
        refetchComments={handleRefetchComments}
        handleSidebarMenu={() => {}}
        handleFilterMenu={() => {}}
        onRequestCreateDocument={(document: any) => {}}
        onRequestAddToFavorite={handleToggleDocumentToOrFromFavorite}
        onRequestDelete={onRequestDelete}
        onRequestRemoveFromFavorite={handleToggleDocumentToOrFromFavorite}
        onRequestReplace={onRequestReplace}
        onRequestFetchMoreComments={handleFetchMoreComments}
        onRequestLike={(document: DocumentCategorie, smyley: any | null) => {}}
        onRequestDeplace={handleDeplace}
        onCompleteDownload={handleCompleteDownload}
        refetchSmyleys={handleRefecthSmyleys}
        hidePartage
        hideRecommandation
        refetchDocument={() => {}}
        onRequestDocument={(id: string) => {}}
        onRequestFetchAll={(withSearch: boolean) => {}}
        noValidation
        tvas={tvas}
      />
    </Box>
  );
};

export default DetailFacture;
