import { CustomModal } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { Dispatch } from 'react';
import { Historique } from '../../../../types';

import HistoriqueCommande from '../../../historique-commande/pages/historique-commande/Historique';

export interface JoindreCommandeProps {
  selectedIds?: string[];
  onRequestSelected?: (commandes: string[]) => void;
  idDocument?: string;
  open: boolean;
  setOpen: (open: boolean) => void;
  saving?: boolean;
  saved?: boolean;
  setSaved?: Dispatch<boolean>;
}

export const JoindreCommande: FC<JoindreCommandeProps> = ({
  selectedIds,
  onRequestSelected,
  idDocument,
  open,
  setOpen,
  saving,
  saved,
  setSaved,
}) => {
  const [selectedItemsIds, setSelectedItemsIds] = useState<string[]>(selectedIds || []);
  useEffect(() => {
    setSelectedItemsIds(selectedIds || []);
  }, [selectedIds]);

  const handleChangeSelected = (historiques: Historique[]) => {
    const ids = historiques.filter((historique) => historique.id).map((historique) => historique.id || '');
    setSelectedItemsIds(ids || []);
  };

  const handleJoindreCommande = (event: React.FormEvent<HTMLInputElement>) => {
    event.preventDefault();
    event.stopPropagation();
    onRequestSelected && onRequestSelected(selectedItemsIds);
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title="Joindre une commande"
      withBtnsActions
      closeIcon
      headerWithBgColor
      disabledButton={saving}
      onClickConfirm={handleJoindreCommande}
      withCancelButton={false}
      actionButtonTitle="Joindre"
      maxWidth="lg"
    >
      <Box minWidth={960}>
        <HistoriqueCommande
          isFacture
          selectedItemsIds={selectedItemsIds}
          idDocument={idDocument}
          onRequestSelected={handleChangeSelected}
          savedAssociation={saved}
          setSavedAssociation={setSaved}
        />
      </Box>
    </CustomModal>
  );
};
