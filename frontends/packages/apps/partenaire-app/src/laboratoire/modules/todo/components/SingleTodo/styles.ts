import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      '&:not(:last-child)': {
        marginBottom: 24,
      },
      border: '1px solid #ccc',
      padding: 10,
      borderRadius: 15,
      paddingLeft: 0,
      overflow: 'hidden',
      cursor: 'pointer',
      '&:hover': {
        transform: 'scale(1.01)',
        transition: '.5s',
        borderColor: theme.palette.primary.main,
      },
    },
    icon: {
      fontSize: 20,
      marginBottom: -5,
    },
    title: {
      fontSize: 14,
      textTransform: 'uppercase',
    },
    checkedBox: {
      color: `${theme.palette.primary.main} !important`,
    },
    laboName: {
      maxWidth: 100,
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      fontSize: 14,
      margin: '0 10px',
    },
    mobileCard: {
      justifyContent: 'flex-end',
      [theme.breakpoints.down('xs')]: {
        display: 'block',
      },
    },
  })
);
