import {
  SearchTodoActionInfoFragment,
  useSearch_Partenaire_Todo_ActionLazyQuery,
} from '../../../../../../../libs/common/src/federation';
import { useApplicationContext } from '@lib/common';
import { useEffect, useState } from 'react';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import { TableChange } from '../../../../../../../libs/ui-kit/src';

interface TableFiltering {
  skip: number;
  take: number;
  orderColumn?: string;
  orderDirection?: string;
  searchText: string;
  selectedDateDebut?: Date;
  selectedDateFin?: Date;
}

export const useTodoLabo = () => {
  const { currentPharmacie } = useApplicationContext();
  const { params } = useLaboratoireParams();
  const firstLabo = params.idLabo?.length ? params.idLabo[0] : undefined;

  const [filter, setFilter] = useState<TableFiltering>({
    skip: 0,
    take: 6,
    orderColumn: 'dateDebut',
    orderDirection: 'ASC',
    searchText: '',
  });

  const [search, searching] = useSearch_Partenaire_Todo_ActionLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  useEffect(() => {
    runSearch();
  }, [filter, firstLabo, params.todoActionStatus, params.assignee]);

  const runSearch = () => {
    const must: any[] = [
      {
        term: {
          idPharmacie: currentPharmacie.id,
        },
      },
      {
        term: {
          partenaireType: 'LABORATOIRE',
        },
      },
    ];

    const { searchText, selectedDateDebut, selectedDateFin } = filter;

    if (searchText) {
      must.push({
        query_string: {
          query: searchText.startsWith('*') || searchText.endsWith('*') ? searchText : `*${searchText}*`,
          analyze_wildcard: true,
        },
      });
    }

    if (params.idLabo?.length) {
      must.push({
        terms: {
          idPartenaireTypeAssocie: params.idLabo,
        },
      });
    }

    if (params.todoActionStatus) {
      must.push({
        term: {
          status: params.todoActionStatus,
        },
      });
    }

    if (params.assignee) {
      must.push({
        term: {
          todoIdUsers: params.assignee,
        },
      });
    }

    if (selectedDateDebut && selectedDateFin) {
      must.push({
        range: {
          dateDebut: {
            gte: selectedDateDebut,
            lte: selectedDateFin,
          },
        },
      });
    }

    const sort: any = [];

    if (filter.orderColumn !== 'status') {
      sort.push({ status: 'asc' });
    }

    if (filter.orderColumn && filter.orderDirection) {
      sort.push({ [filter.orderColumn]: filter.orderDirection.toLowerCase() });
    }

    search({
      variables: {
        index: ['todoaction'],
        skip: filter.skip,
        take: filter.take,
        query: {
          sort: sort.length > 0 ? sort : undefined,
          query: {
            bool: {
              must,
            },
          },
        },
      },
    });
  };

  const handleSearch = ({ skip, take, searchText, selectedDateDebut, selectedDateFin }: TableFiltering) => {
    setFilter((prev) => ({ ...prev, skip, take, searchText, selectedDateDebut, selectedDateFin }));
  };

  return {
    data: searching?.data?.search?.data as SearchTodoActionInfoFragment[],
    onRequestSearch: handleSearch,
    refetch: searching.refetch,
    total: searching.data?.search?.total,
    fetchMore: searching.fetchMore,
    variables: searching.variables,
    loading: searching.loading,
  };
};
