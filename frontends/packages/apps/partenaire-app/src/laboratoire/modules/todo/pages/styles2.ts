import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    btnEquipe: {
      textTransform: 'none',
      '& svg': {
        color: theme.palette.primary.main,
        fontSize: 30,
      },
    },
    spanDay: {
      color: '#fff',
      background: theme.palette.primary.main,
      padding: 10,
      borderRadius: 20,
      width: 35,
      height: 35,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      marginRight: 10,
      border: '1px solid #fff',
    },
    paraDateGroup: {
      textTransform: 'capitalize',
      display: 'flex',
      alignItems: 'center',
    },
    cardGroup: {
      display: 'flex',
      width: '100%',
      justifyContent: 'space-between',
      marginBottom: 24,
      background: '#f8f8f8',
      padding: 15,
      borderRadius: 20,
      marginRottom: 20,
    },
    downloadTitle: {
      width: '100%',
      fontSize: 20,
      textAlign: 'center',
      color: '#120',
      fontWeight: 500,
      marginBottom: 24,
    },
    cardMobileByGroup: {
      padding: 20,
      borderRadius: 20,
      marginBottom: 20,
    },
    buttonImprimer: {
      [theme.breakpoints.down('md')]: {
        padding: '0 10px !important',
      },
    },
  })
);
