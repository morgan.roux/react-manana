import React from 'react';
import loadable from '@loadable/component';

import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';

// const TodoPage = loadable(() => import('./pages/TodoLabo'), {
//   fallback: <SmallLoading />,
// });
const TodoPage = loadable(() => import('./pages/NewTodoLabo'), {
  fallback: <SmallLoading />,
});

const activationParameters = {
  groupement: '0305',
  pharmacie: '0708',
};

const tradeMarketingModule: ModuleDefinition = {
  routes: [
    {
      attachTo: 'MAIN',
      path: '/laboratoires/to-do',
      component: TodoPage,
      activationParameters,
    },
  ],
};

export default tradeMarketingModule;
