//import { usePlan_Marketing_CategoriesQuery } from '@lib/common/src/federation';
import { CustomModal, CustomSelectMenu, NewCustomButton, SmallLoading } from '@lib/ui-kit';
import TableChartSubToolbar from '@lib/ui-kit/src/components/molecules/TableChartSubToolbar';
import { Box, Button, IconButton, Typography } from '@material-ui/core';
import moment from 'moment';
import React, { CSSProperties, useRef, useState } from 'react';
import PrintIcon from '@material-ui/icons/Print';
//import UserCircle from '@material-ui/icons/SupervisedUserCircle';
import { useStyles } from './styles2';
import filterIcon from '../../../assets/img/filter_alt.svg';
import { SingleTodo } from '../components/SingleTodo/SingleTodo';
import { useTodoLabo } from '../hooks/useTodoLabo';
import { useEffect } from 'react';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
//import InfiniteScroll from 'react-infinite-scroll-component';
//
//import SkipPrevious from '@material-ui/icons/SkipPrevious';
//import SkipNext from '@material-ui/icons/SkipNext';
import { useApplicationContext, UserSelect } from '@lib/common';
import { makePdf } from '../../compte-rendu/pages/CompteRenduDetail/CompteRenduDetail';
import { StickyContainer, Sticky } from 'react-sticky';
import { useTheme } from '@material-ui/core';

const statuses = [
  {
    id: 'ACTIVE',
    name: 'A FAIRE',
  },
  {
    id: 'DONE',
    name: 'TERMINÉE',
  },
];

const NewTodoLabo = () => {
  const { redirectTo, params } = useLaboratoireParams();
  const { isMobile } = useApplicationContext();
  const todoActionStatus = params.todoActionStatus;
  const classes = useStyles();
  const componentToPrint = useRef(null);
  const [openMobileFilter, setOpenMobileFilter] = useState<boolean>(false);
  const [dateTrade, setDateTrade] = React.useState<boolean>();
  const [selectedDateDebut, setSelectedDateDebut] = React.useState<Date>(moment().startOf('month').toDate());
  const [selectedDateFin, setSelectedDateFin] = React.useState<Date>(moment().endOf('month').toDate());
  const [searchText, setSearchText] = useState<string>('');
  const skip = 0;
  const take = 10000;
  const [isDownload, setIsDownload] = useState<boolean>(false);

  const theme = useTheme();

  useEffect(() => {
    onRequestSearch({ skip, take, searchText, selectedDateDebut, selectedDateFin });
  }, []);

  useEffect(() => {
    if (isDownload) {
      makePdf(componentToPrint.current, `todoAction${moment(selectedDateDebut).format('DD-MM-YYYY')}`, {
        margin: 24,
      });
      setTimeout(() => {
        setIsDownload(false);
      }, 100);
    }
  }, [isDownload]);

  const { data, onRequestSearch, refetch, loading } = useTodoLabo();

  // const loadPlanCategories = usePlan_Marketing_CategoriesQuery();

  // const listButton = [
  //   { id: 'TOUS', code: 'TOUS', libelle: 'Tous' },
  //   ...(loadPlanCategories.data?.pRTPlanMarketingCategories.nodes || []),
  // ];

  const handleTradeChange = () => {
    setDateTrade(!dateTrade);
  };

  const handleTableChartSubToolbarChange = (search: string, date: any) => {
    setSelectedDateDebut(moment(date).startOf('month').toDate());
    setSelectedDateFin(moment(date).endOf('month').toDate());
    setSearchText(search);

    if (onRequestSearch)
      onRequestSearch({
        skip,
        take,
        searchText: search,
        selectedDateDebut: moment(date).startOf('month').toDate(),
        selectedDateFin: moment(date).endOf('month').toDate(),
      });
  };

  const handleDateDebutChange = (dateDebut: Date) => {
    setSelectedDateDebut(dateDebut);
    if (dateDebut && selectedDateFin && selectedDateFin > dateDebut) {
      setDateTrade(false);
      onRequestSearch && onRequestSearch({ skip, take, searchText, selectedDateDebut: dateDebut, selectedDateFin });
    }
  };

  const handleDateFinChange = (dateFin: Date) => {
    setSelectedDateFin(dateFin);
    if (dateFin && selectedDateDebut && dateFin > selectedDateDebut) {
      setDateTrade(false);
      onRequestSearch && onRequestSearch({ skip, take, searchText, selectedDateDebut, selectedDateFin: dateFin });
    }
  };

  const iconFilter = <img src={filterIcon} alt="filter" />;

  const handleFilterStatus = (id?: string) => {
    setOpenMobileFilter(!openMobileFilter);
    redirectTo({ todoActionStatus: id === 'DONE' ? 'DONE' : 'ACTIVE' });
  };

  const groupedByDate = data
    ?.filter((action) => action?.status === todoActionStatus)
    ?.reduce((acc, next: any) => {
      const group: any = moment(next.dateDebut).format('MM-DD-YYYY');
      return {
        ...acc,
        [group]: [...(acc[group] || []), next],
      };
    }, {} as any);

  const handleSelected = (user: any) => {
    redirectTo({ ...params, assignee: user?.id && user.id !== 'TOUT' ? user.id : undefined });
  };
  const handleDownload = () => {
    setIsDownload(true);
  };
  const handleOpenMobileFilter = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    setOpenMobileFilter(!openMobileFilter);
  };
  console.log('-------------- group by-----------', data, groupedByDate);
  return (
    <Box width={isMobile ? '100%' : '80%'} m="auto" py={3}>
      <Box display="flex" justifyContent={isMobile ? 'flex-end' : 'space-between'}>
        <TableChartSubToolbar
          withSearch={false}
          onRequestChange={handleTableChartSubToolbarChange}
          intervalDate={true}
          dateTrade={dateTrade}
          handleDateDebutChange={handleDateDebutChange}
          handleDateFinChange={handleDateFinChange}
          handleTradeChange={handleTradeChange}
          selectedDateDebut={selectedDateDebut}
          selectedDateFin={selectedDateFin}
          showMonth
        />
        {/* <Box display="none" alignItems="center">
          <Box
            style={{
              display: 'flex',
            }}
          >
            {listButton.map((item) => (
              <Button
                onClick={(event: React.MouseEvent<HTMLElement>) => {
                  event.preventDefault();
                  event.stopPropagation();
                  // handleListeClick(item.id);
                }}
              >
                <Typography
                  style={{
                    textTransform: 'initial',
                    fontSize: '14px',
                    fontFamily: 'Roboto',
                  }}
                >
                  {item.libelle}
                </Typography>
              </Button>
            ))}
          </Box>
        </Box> */}
        <Box display="flex">
          {isMobile ? (
            <>
              <IconButton onClick={handleOpenMobileFilter} style={{ padding: '0 15px' }}>
                <img src={filterIcon} alt="filter" />
              </IconButton>
              <CustomModal
                open={openMobileFilter}
                setOpen={setOpenMobileFilter}
                headerWithBgColor
                title="Filtre statut"
              >
                <CustomSelectMenu
                  whiteBtn
                  buttonLabel="Statut"
                  listFilters={statuses}
                  handleClickItem={handleFilterStatus}
                  iconFilter={iconFilter}
                />
              </CustomModal>
            </>
          ) : (
            <CustomSelectMenu
              whiteBtn
              buttonLabel="Statut"
              listFilters={statuses}
              handleClickItem={handleFilterStatus}
              iconFilter={iconFilter}
            />
          )}
          <UserSelect onChange={handleSelected} />

          <NewCustomButton startIcon={<PrintIcon />} onClick={handleDownload} className={classes.buttonImprimer}>
            Imprimer
          </NewCustomButton>
        </Box>
      </Box>
      <Box my={3}>
        {loading ? (
          <SmallLoading />
        ) : (
          <div ref={componentToPrint}>
            {isDownload && (
              <Box className={classes.downloadTitle}>
                Todo Labo du {moment(selectedDateDebut).format('DD/MMMM/YYYY')} au{' '}
                {moment(selectedDateFin).format('DD/MMMM/YYYY')}
              </Box>
            )}
            {!!groupedByDate && isMobile && (
              <>
                {Object.keys(groupedByDate).map((group, index) => {
                  const weekDay = moment(group).day();
                  const styless: CSSProperties = {
                    top: 85,
                    width: '100%',
                    left: 0,
                    background: '#fff',
                    zIndex: 999,
                  };
                  return (
                    <StickyContainer>
                      <Sticky>
                        {({ style, isSticky }) => (
                          <div style={isSticky ? { ...style, ...styless } : style}>
                            <Box
                              style={
                                isSticky ? { background: theme.palette.primary.main, color: '#fff', opacity: 0.8 } : {}
                              }
                            >
                              <Typography
                                className={classes.paraDateGroup}
                                style={isSticky ? { padding: '20px' } : { padding: '0 20px' }}
                              >
                                <span className={classes.spanDay}>{moment(group).date()}</span>{' '}
                                {moment(group).format('MMMM')}
                                {', '}
                                {moment().add(weekDay, 'days').format('ddd')}
                              </Typography>
                            </Box>
                          </div>
                        )}
                      </Sticky>
                      <Box key={`group-${index}`} className={classes.cardMobileByGroup}>
                        <Box>
                          {(groupedByDate[group] || []).map((item: any, index: any) => {
                            return <SingleTodo data={item} key={`list-todo-labo-${index}`} refetch={refetch} />;
                          })}
                        </Box>
                      </Box>
                    </StickyContainer>
                  );
                })}
              </>
            )}
            {!!groupedByDate &&
              !isMobile &&
              Object.keys(groupedByDate).map((group, index) => {
                const weekDay = moment(group).day();
                return (
                  <>
                    <Box key={`group-${index}`} className={classes.cardGroup}>
                      <Box>
                        <Typography className={classes.paraDateGroup}>
                          <span className={classes.spanDay}>{moment(group).date()}</span> {moment(group).format('MMMM')}
                          {', '}
                          {moment().add(weekDay, 'days').format('ddd')}
                        </Typography>
                      </Box>
                      <Box width="75%">
                        {(groupedByDate[group] || []).map((item: any, index: any) => {
                          return <SingleTodo data={item} key={`list-todo-labo-${index}`} refetch={refetch} />;
                        })}
                      </Box>
                    </Box>
                  </>
                );
              })}
          </div>
        )}
        {/*!!groupedByDate && Object.keys(groupedByDate).length > 0 && (
          <Box display="flex" justifyContent="flex-end">
            <NewCustomButton startIcon={<SkipPrevious />} style={{ marginRight: 15 }}>
              Précédent
            </NewCustomButton>
            <NewCustomButton endIcon={<SkipNext />}>Suivant</NewCustomButton>
          </Box>
        )*/}
      </Box>
    </Box>
  );
};
export default NewTodoLabo;
