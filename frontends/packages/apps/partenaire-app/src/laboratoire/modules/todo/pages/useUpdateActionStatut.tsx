import { useApplicationContext } from '@lib/common';
import { useUpdate_Action_StatusMutation } from '@lib/common/src/graphql';

export const useUpdateTodoActionStatut = (props: any) => {
  const { graphql, notify } = useApplicationContext();
  const { refetch } = props;
  const [updateActionStatus, updatingActionStatus] = useUpdate_Action_StatusMutation({
    client: graphql,
    onCompleted: (data) => {
      refetch && refetch();
      console.log('statut changed')
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });
  return { updateActionStatus, updatingActionStatus };
};
