import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},

    cursor: {
      cursor: 'pointer',
    },
    checkedBox: {
      color: `${theme.palette.primary.main} !important`,
    },
    infiniteScrollContainer: {
      height: 'calc(100vh - 370px)',
      width: '100%',
      overflowY: 'scroll',
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
    dateIcons: {
      display: 'flex',
      color: '#afafaf',
      fontSize: 14,
      alignItems: 'center',
      marginRight: 5,
      '& svg': {
        fontSize: 18,
        marginRight: 5,
      },
    },
    cardActionOp: {
      background: '#f8f8f8',
      marginBottom: 10,
      padding: '5px 15px',
      paddingLeft: 0,
      position: 'relative',
    },
    leftBannerActionOp: {
      position: 'absolute',
      width: 4,
      height: '100%',
      left: 0,
      top: 0,
      borderRadius: 5,
    },
    spanStatusActionOp: {
      borderRadius: 5,
      fontSize: 12,
      padding: '2px 10px',
      background: theme.palette.primary.main,
      color: '#fff',
    },
    textTitle: {
      fontFamily: 'Roboto',
      color: '#424242',
      textAlign: 'left',
      fontSize: 14,
      marginBottom: 10,
    },
    imgAction: {
      width: '22px !important',
      marginBottom: -3,
      marginLeft: 10,
    },
  })
);
