import { CustomAvatar, CustomAvatarGroup, OptionSelect } from '@lib/ui-kit';
import { Box, Checkbox, IconButton, Tooltip, Typography } from '@material-ui/core';
import { Event, MoreHoriz, PersonAdd } from '@material-ui/icons';
import React, { FC } from 'react';
import { useStyles } from './styles';
import {
  CustomSelectUser,
  TodoActionStatus,
  TodoParticipantSelect,
  ToggleTodoAction,
  useApplicationContext,
} from '@lib/common';
import moment from 'moment';
type SingleTodoDataType = {
  id?: string;
  dateDebut?: string;
  dateFin?: string;
  description?: string;
  status?: any;
  participants?: any[];
  partenaireTypeAssocie?: any;
};
interface SingleTodoProps {
  data: any;
  refetch?: () => void;
  disabled?: boolean;
}
export const SingleTodo: FC<SingleTodoProps> = ({ data, refetch, disabled = true }) => {
  const { participants, dateDebut, dateFin, description, partenaireTypeAssocie } = data;
  const classes = useStyles();
  const parsedDateDebut = moment(dateDebut).format('DD MMMM');
  const parsedDateFin = !!dateFin && moment(dateFin).format('DD MMMM');
  const { isMobile } = useApplicationContext();

  return (
    <>
      {isMobile ? (
        <Box className={classes.root}>
          <Box display="flex" mb={2} alignItems="flex-start">
            <Box>
              <ToggleTodoAction action={data} refetch={refetch} />
            </Box>
            <Box>
              <Typography>
                <span dangerouslySetInnerHTML={{ __html: description }} />
              </Typography>
            </Box>
            <TodoParticipantSelect todoAction={data} refetch={refetch} maxAvatar={2} />
          </Box>
          <Box display="flex" alignItems="center" className={classes.mobileCard}>
            <Box display="flex" alignItems="center" justifyContent="space-between">
              <Box display="flex" alignItems="center" ml={2}>
                <Tooltip title={partenaireTypeAssocie.nom}>
                  <Box display="flex" alignItems="center">
                    <CustomAvatar
                      name={partenaireTypeAssocie.nom}
                      url={partenaireTypeAssocie.photo?.publicUrl}
                      style={{ maxWidth: 30, maxHeight: 30 }}
                    />
                    <Typography className={classes.laboName}>{partenaireTypeAssocie.nom}</Typography>
                  </Box>
                </Tooltip>
              </Box>
              <TodoActionStatus action={data} refetch={refetch} />
            </Box>
          </Box>
        </Box>
      ) : (
        <Box className={classes.root} display="flex" justifyContent="space-between" alignItems="center">
          <Box display="flex" width="55%">
            <Box>
              <ToggleTodoAction action={data} refetch={refetch} />
            </Box>
            <Box>
              <Typography>
                <span dangerouslySetInnerHTML={{ __html: description }} />
              </Typography>
            </Box>
            {!disabled && (
              <Box>
                <Typography>
                  <Event className={classes.icon} /> {parsedDateDebut} {parsedDateFin && ` - ${parsedDateFin}`}
                </Typography>
              </Box>
            )}
          </Box>
          <Box display="flex" alignItems="center" justifyContent="inherit" width="45%">
            <Box display="flex" alignItems="center" justifyContent="flex-end">
              <TodoActionStatus action={data} refetch={refetch} />
              <Box display="flex" alignItems="center" ml={2}>
                <Tooltip title={partenaireTypeAssocie.nom}>
                  <Box display="flex" alignItems="center">
                    <CustomAvatar
                      name={partenaireTypeAssocie.nom}
                      url={partenaireTypeAssocie.photo?.publicUrl}
                      style={{ maxWidth: 30, maxHeight: 30 }}
                    />
                    <Typography className={classes.laboName}>{partenaireTypeAssocie.nom}</Typography>
                  </Box>
                </Tooltip>
              </Box>
            </Box>
            <Box textAlign="right">
              {!disabled && (
                <div>
                  <IconButton style={{ paddingTop: 0 }}>
                    <MoreHoriz />
                  </IconButton>
                </div>
              )}
              <TodoParticipantSelect todoAction={data} refetch={refetch} />
            </Box>
          </Box>
        </Box>
      )}
    </>
  );
};
