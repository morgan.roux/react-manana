import { strippedColumnStyle, strippedString, TodoActionIcon, useApplicationContext } from '@lib/common';
import { Box, Checkbox, Grid, Typography } from '@material-ui/core';
import moment from 'moment';
import NotificationImportantIcon from '@material-ui/icons/NotificationImportant';
import React, { CSSProperties, FC, useState } from 'react';
import { Column, CustomAvatar, CustomAvatarGroup, ImportanceLabel, Table, TableChange, TopBar } from '@lib/ui-kit';
import { useLaboratoireParams } from '../../../hooks/useLaboratoireParams';
import { useStyles } from './style';
import { useSearch_Partenaire_Todo_ActionLazyQuery, SearchTodoActionInfoFragment } from '@lib/common/src/federation';
import { useEffect } from 'react';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import { useUpdateTodoActionStatut } from './useUpdateActionStatut';
import { ActionStatus } from '@lib/common/src/graphql';
import { searchTextVar } from '../../../reactive-vars';
import { useReactiveVar } from '@apollo/client';
import { BottomScrollListener } from 'react-bottom-scroll-listener';
import { Event } from '@material-ui/icons';

const headerColumnStyle: CSSProperties = {
  textAlign: 'center',
};

interface TableFiltering {
  skip: number;
  take: number;
  orderColumn?: string;
  orderDirection?: string;
  searchText: string;
}

const plan /*: FC<{}>*/ = () => {
  const classes = useStyles();
  const searchText = useReactiveVar<string>(searchTextVar);

  const { params } = useLaboratoireParams('plan');
  const { federation, currentPharmacie, isMobile } = useApplicationContext();
  const [filter, setFilter] = useState<TableFiltering>({
    skip: 0,
    take: 12,
    orderColumn: 'dateDebut',
    orderDirection: 'ASC',
    searchText: '',
  });

  const [search, searching] = useSearch_Partenaire_Todo_ActionLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  const { updateActionStatus } = useUpdateTodoActionStatut({
    refetch: searching.refetch,
  });

  const firstLabo = params.idLabo?.length ? params.idLabo[0] : undefined;

  useEffect(() => {
    runSearch();
  }, [filter, firstLabo, params.todoActionStatus]);

  const runSearch = () => {
    const must: any[] = [
      {
        term: {
          idPharmacie: currentPharmacie.id,
        },
      },
      {
        term: {
          partenaireType: 'LABORATOIRE',
        },
      },
    ];

    const { searchText } = filter;

    if (searchText) {
      must.push({
        query_string: {
          query: searchText.startsWith('*') || searchText.endsWith('*') ? searchText : `*${searchText}*`,
          analyze_wildcard: true,
        },
      });
    }

    if (params.idLabo?.length) {
      must.push({
        terms: {
          idPartenaireTypeAssocie: params.idLabo,
        },
      });
    }

    if (params.todoActionStatus) {
      must.push({
        term: {
          status: params.todoActionStatus,
        },
      });
    }

    const sort: any = [];

    if (filter.orderColumn !== 'status') {
      sort.push({ status: 'asc' });
    }

    if (filter.orderColumn && filter.orderDirection) {
      sort.push({ [filter.orderColumn]: filter.orderDirection.toLowerCase() });
    }

    search({
      variables: {
        index: ['todoaction'],
        skip: filter.skip,
        take: filter.take,
        query: {
          sort: sort.length > 0 ? sort : undefined,
          query: {
            bool: {
              must,
            },
          },
        },
      },
    });
  };

  // eslint-disable-next-line no-empty-pattern
  const handleSearch = ({ skip, take, searchText }: TableChange) => {
    setFilter((prev) => ({ ...prev, skip, take, searchText }));
  };

  const handleSort = (orderColumn: string, orderDirection: 'DESC' | 'ASC') => {
    setFilter((prev) => ({ ...prev, orderColumn, orderDirection }));
  };

  const columns: Column[] = [
    {
      name: 'partenaireTypeAssocie.nom',
      label: 'Labo',
      sortable: true,
      renderer: (row: any) => {
        return (
          <Box display="flex" flexDirection="row" alignItems="center">
            <Checkbox
              icon={<CircleUnchecked fontSize="large" />}
              checkedIcon={<CircleChecked fontSize="large" />}
              onClick={(event) => {
                event.preventDefault();
                event.stopPropagation();

                updateActionStatus({
                  variables: {
                    input: {
                      idAction: row.id,
                      status: row.status === 'DONE' ? ActionStatus.Active : ActionStatus.Done,
                    },
                  },
                });
              }}
              checked={row?.status === 'DONE'}
              color="primary"
              style={{ color: '#757575' }}
              classes={{ checked: classes.checkedBox }}
            />
            <Typography>{row.partenaireTypeAssocie?.nom}</Typography>
          </Box>
        );
      },
    },
    {
      name: 'type',
      label: '',
      sortable: false,
      renderer: (row: SearchTodoActionInfoFragment) => {
        return <TodoActionIcon action={row} />;
      },
    },
    {
      name: 'dateDebut',
      label: 'Date début',
      sortable: true,
      renderer: (row: SearchTodoActionInfoFragment) => {
        return <Typography>{row?.dateDebut ? `${moment.utc(row?.dateDebut).format('DD/MM/YYYY')}` : '-'}</Typography>;
      },
    } /*,

    {
      name: 'dateFin',
      label: 'Date fin',
      sortable: true,
      renderer: (row: SearchTodoActionInfoFragment) => {
        return <Typography>{row?.dateFin ? `${moment.utc(row?.dateFin).format('DD/MM/YYYY')}` : '-'}</Typography>;
      },
    }*/,

    {
      name: 'importance.libelle',
      label: 'Priorité',
      sortable: true,
      renderer: (row: SearchTodoActionInfoFragment, index?: number) => {
        return (
          <Box className={classes.cursor}>
            <ImportanceLabel importance={row?.importance as any} />
          </Box>
        );
      },
      headerStyle: headerColumnStyle,
    },
    {
      name: 'detail',
      label: 'Détail',
      renderer: (row: SearchTodoActionInfoFragment, index?: number) => {
        return <Box className={classes.cursor}>{row.description ? strippedString(row.description) : '-'}</Box>;
      },
      style: strippedColumnStyle,
      headerStyle: headerColumnStyle,
    },

    {
      name: 'equipe',
      label: 'Equipe',
      renderer: (row: SearchTodoActionInfoFragment, index?: number) => {
        return (
          <Box display="flex" flexDirection="row" justifyContent="flex-start" className={classes.cursor}>
            <CustomAvatarGroup max={4} sizes="30px" users={(row.participants || []) as any} />
          </Box>
        );
      },
      headerStyle: headerColumnStyle,
    },
    {
      name: 'status',
      label: 'Statut',
      sortable: true,
      renderer: (row: SearchTodoActionInfoFragment) => {
        return row.status === 'DONE' ? 'Terminée' : 'A faire';
      },
      headerStyle: headerColumnStyle,
    },
    {
      name: 'dateDebut',
      sortable: true,
      label: 'En retard',
      renderer: (row: SearchTodoActionInfoFragment, index?: number) => {
        const nombreJoursRetard = moment().diff(row.dateDebut, 'days');
        if (row.status === 'DONE') {
          return null;
        }

        return (
          <Box className={classes.cursor} style={{ textAlign: 'center' }}>
            {nombreJoursRetard > 0 ? (
              <>
                <NotificationImportantIcon color="secondary" />({nombreJoursRetard})
              </>
            ) : (
              ''
            )}
          </Box>
        );
      },
      headerStyle: headerColumnStyle,
    },
  ];
  const handleContainerOnBottom = () => {
    // handleFetchMoreSuivis();
  };

  const mobilePage = (
    <BottomScrollListener onBottom={handleContainerOnBottom}>
      {(scrollRef) => (
        <Grid id="infinity-suivi" item ref={scrollRef as any} className={classes.infiniteScrollContainer}>
          <Box py={2} px={2}>
            {(searching?.data?.search?.data || []).map((item: any, index: number) => {
              return (
                <>
                  <Box className={classes.cardActionOp}>
                    <div className={classes.leftBannerActionOp} style={{ background: `${item.importance?.couleur}` }} />
                    <Box display="flex">
                      <Box>
                        <Checkbox
                          icon={<CircleUnchecked fontSize="inherit" />}
                          checkedIcon={<CircleChecked fontSize="inherit" />}
                          onClick={(event) => {
                            event.preventDefault();
                            event.stopPropagation();

                            updateActionStatus({
                              variables: {
                                input: {
                                  idAction: item.id,
                                  status: item.status === 'DONE' ? ActionStatus.Active : ActionStatus.Done,
                                },
                              },
                            });
                          }}
                          checked={item?.status === 'DONE'}
                          color="primary"
                          style={{ color: '#757575' }}
                          classes={{ checked: classes.checkedBox }}
                        />
                      </Box>
                      <Box width="100%">
                        <Box display="flex" justifyContent="space-between" alignItems="center">
                          <Typography className={classes.textTitle}>
                            <span dangerouslySetInnerHTML={{ __html: item.description }} />
                          </Typography>
                          <Typography>{item.type?.libelle || ''}</Typography>
                        </Box>
                        <Box display="flex" justifyContent="space-between" alignItems="center">
                          <Box display="flex" alignItems="center">
                            <Typography className={classes.dateIcons}>
                              <Event /> {item.dateDebut && moment(item.dateDebut).format('DD/MM/YYYY')} -{' '}
                              {item.dateFin && moment(item.dateFin).format('DD/MM/YYYY')}
                            </Typography>
                          </Box>
                          <Typography></Typography>
                        </Box>
                        <Box display="flex" justifyContent="space-between" alignItems="center">
                          <Box display="flex" alignItems="center">
                            <CustomAvatar
                              name={item.partenaireTypeAssocie?.nom || '-'}
                              url={item.partenaireTypeAssocie?.photo?.publicUrl as any}
                              style={{ width: 30, height: 30, marginRight: 5 }}
                            />
                            <Typography style={{ fontSize: 13 }}>
                              {item.partenaireTypeAssocie?.nom}{' '}
                              <TodoActionIcon action={item} className={classes.imgAction} />
                            </Typography>
                          </Box>
                          <Typography>
                            <span className={classes.spanStatusActionOp}>{item.status}</span>
                          </Typography>
                        </Box>
                      </Box>
                    </Box>
                  </Box>
                </>
              );
            })}
          </Box>
        </Grid>
      )}
    </BottomScrollListener>
  );
  // return (
  //   <>
  //     {/* {isMobile ? (
  //       <Box>{mobilePage}</Box>
  //     ) : (
  //       <Table
  //         style={{ marginTop: 25 }}
  //         error={searching?.error as Error | undefined}
  //         loading={searching?.loading}
  //         search={false}
  //         data={(searching?.data?.search?.data || []) as any}
  //         selectable={false}
  //         columns={columns}
  //         rowsTotal={searching.data?.search?.total || 0}
  //         // onRowClick={handleRowClick}
  //         onRunSearch={handleSearch}
  //         onSortColumn={handleSort}
  //         topBarComponent={(searchComponent: any) => (
  //           <TopBar
  //             searchComponent={searchComponent}
  //             titleButton=" "
  //             titleTopBar=" "
  //             withFilter={false}
  //             withMoreButton
  //           />
  //         )}
  //       />
  //     )} */}
  //     <Box>Boom</Box>
  //   </>
  // );
  return {
    data: searching?.data?.search?.data,
  };
};

export default plan;
