import { useGet_Prt_Reglement_Lieu_DeclarationsQuery } from '@lib/common/src/federation';
import { CustomSelect } from '@lib/ui-kit';
import { Box } from 'mdi-material-ui';
import React, { FC, useEffect } from 'react';
export interface LieuDeclarationInputProps {
  label?: string;
  idLieuDeclaration?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  name?: string;
  onChangeValue?: (name: string, value?: string) => void;
}

export const LieuDeclarationInput: FC<LieuDeclarationInputProps> = ({
  label,
  idLieuDeclaration,
  onChange,
  name,
  onChangeValue,
}) => {
  const lieuDeclarations =
    useGet_Prt_Reglement_Lieu_DeclarationsQuery().data?.pRTReglementLieuDeclarations?.nodes || [];

  const idAutre = lieuDeclarations.find((lieu) => lieu.code === 'AUTRE')?.id;
  useEffect(() => {
    if (idAutre && !idLieuDeclaration && onChangeValue) onChangeValue('idLieuDeclaration', idAutre);
  }, [idAutre, idLieuDeclaration]);

  return lieuDeclarations?.length ? (
    <CustomSelect
      list={lieuDeclarations}
      index="libelle"
      listId="id"
      name={name || 'idLieuDeclaration'}
      value={idLieuDeclaration ?? (onChangeValue ? idAutre : undefined)}
      onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
        onChange && onChange(event);
        onChangeValue && onChangeValue(event.target.name, event.target.value);
      }}
      label={label || 'Lieu déclaration'}
      placeholder="Lieu déclaration"
      withPlaceholder={true}
    />
  ) : (
    <Box>Aucun résultat</Box>
  );
};
