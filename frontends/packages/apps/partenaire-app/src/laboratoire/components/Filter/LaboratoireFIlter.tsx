import React, { FC, FormEvent, useState } from 'react';
import { FormGroup, FormControlLabel, Checkbox, Box, Typography } from '@material-ui/core';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { CustomButton, NewCustomButton } from '@lib/ui-kit';
import { useStyles } from './styles';

interface LaboratoireFilterProps {
  filterTypePartenariat: any[];
  filterStatutPartenariat: any[];
  onRequestFilter: (statut: any, type: any, date: any) => void;
  onCloseFilter: (open: boolean) => void;
}

const dateFilterValue = {
  dateDebut: null,
  dateFin: null,
};

export const LaboratoireFilter: FC<LaboratoireFilterProps> = ({
  filterTypePartenariat,
  filterStatutPartenariat,
  onRequestFilter,
  onCloseFilter,
}) => {
  const classes = useStyles();
  const [statutChecked, setStatutChecked] = useState<any[]>([]);
  const [typeChecked, setTypeChecked] = useState<any[]>([]);
  const [dateFilter, setDateFilter] = useState<any>(dateFilterValue);

  //   const handleChangeDateDebut = (date: MaterialUiPickersDate) => {
  //     setDateFilter({ ...dateFilter, dateDebut: date });
  //   };
  //   const handleChangeDateFin = (date: MaterialUiPickersDate) => {
  //     setDateFilter({ ...dateFilter, dateFin: date });
  //   };

  const handleChangeType = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!event.target.checked) {
      const type = typeChecked.reduce((p, c) => (c.id !== event.target.name && p.push(c), p), []);
      setTypeChecked(type);
    } else {
      const type = typeChecked ? [...typeChecked, { keyword: 'idType', id: event.target.name }] : [];
      setTypeChecked(type);
    }
  };

  const handleChangeStatut = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (!event.target.checked) {
      const statut = statutChecked.reduce((p, c) => (c.id !== event.target.name && p.push(c), p), []);
      setStatutChecked(statut);
    } else {
      const statut = statutChecked ? [...statutChecked, { keyword: 'idStatut', id: event.target.name }] : [];
      setStatutChecked(statut);
    }
  };

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    onRequestFilter(statutChecked, typeChecked, dateFilter);
    onCloseFilter(false);
  };

  return (
    <form onSubmit={handleSubmit} style={{ width: '100%' }}>
      <div className={classes.containerFilter}>
        <div className={classes.itemFilter}>
          <Typography style={{ fontWeight: 'bold' }}>Type partenariat</Typography>
          <FormGroup>
            {filterTypePartenariat &&
              filterTypePartenariat.map(({ id, libelle }) => (
                <FormControlLabel
                  key={id}
                  control={<Checkbox checked={typeChecked && typeChecked[id]} onChange={handleChangeType} name={id} />}
                  label={libelle}
                />
              ))}
          </FormGroup>
        </div>
        <div className={classes.itemFilter}>
          {filterStatutPartenariat && <Typography style={{ fontWeight: 'bold' }}>Statut partenariat</Typography>}
          <FormGroup>
            {filterStatutPartenariat &&
              filterStatutPartenariat.map(({ id, libelle }) => (
                <FormControlLabel
                  key={id}
                  control={
                    <Checkbox checked={statutChecked && statutChecked[id]} onChange={handleChangeStatut} name={id} />
                  }
                  label={libelle}
                />
              ))}
          </FormGroup>
        </div>
      </div>
      <Box textAlign="right">
        <NewCustomButton type="submit" className={classes.btnFilter}>
          Filtrer
        </NewCustomButton>
      </Box>
    </form>
  );
};
