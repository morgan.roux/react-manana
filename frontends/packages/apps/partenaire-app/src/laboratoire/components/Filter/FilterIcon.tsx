import React from 'react';
import FilterAlt from './../../assets/img/filter_alt.svg';

export const FilterIcon = () => {
  return <img src={FilterAlt} />;
};
