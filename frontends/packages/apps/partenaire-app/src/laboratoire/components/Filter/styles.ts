import { createStyles, makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    containerFilter: {
      [theme.breakpoints.up('xs')]: {
        display: 'block',
        padding: '35px 0',
        minWidth: 400,
      },
    },
    itemFilter: {
      [theme.breakpoints.up('xs')]: {
        display: 'inline-block',
        width: '50%',
      },
    },
    btnFilter: {
      '&.MuiButton-containedSecondary': {
        backgroundColor: theme.palette.primary.main,
      },
    },
  })
);
