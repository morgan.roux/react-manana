import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((_theme: Theme) =>
  createStyles({
    bold: {
      fontWeight: 'bold',
    },
    link: {
      color: '#85c7e3',
      textDecoration: 'underline',
      cursor: 'pointer',
    },
    reglement: {
      marginLeft: 16,
      marginBottom: 16,
    },
  })
);
