import { capitalizeFirstLetter } from '@lib/common';
import { Laboratoire } from '@lib/common/src/federation';
import { Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { useStyles } from './styles';
import { PrtReglementLieuDeclaration, PrtReglementModeDeclaration, PrtReglementType } from '@lib/common/src/federation';
// import classNames from 'classnames';

interface ReglementProps {
  laboratoire?: Laboratoire;
  reglementType?: PrtReglementType | null;
  reglementModeDeclaration?: PrtReglementModeDeclaration | null;
  reglementLieuDeclaration?: PrtReglementLieuDeclaration | null;
}

export const Reglement: FC<ReglementProps> = ({
  laboratoire,
  reglementType,
  reglementLieuDeclaration,
  reglementModeDeclaration,
}) => {
  const classes = useStyles();
  const lieuDeclaration = reglementLieuDeclaration?.code === 'SITE' ? ' sur le site ' : ' chez ';

  const goToUrl = (url?: string | null) => {
    if (url) window.open(url, '');
  };

  if (!reglementType) {
    return <Typography className={classes.reglement}>Règlement : Non renseigné</Typography>;
  }

  return (
    <Typography className={classes.reglement}>
      Règlement sera effectué par{' '}
      <span className={classes.bold}>
        {reglementType.code === 'LABORATOIRE' ? laboratoire?.nom || 'le labo' : 'le groupement'}
      </span>
      ,{' '}
      {reglementModeDeclaration && reglementModeDeclaration?.code !== 'AUTRE' && (
        <>
          déclaration par{' '}
          <span className={classes.bold}>{capitalizeFirstLetter(reglementModeDeclaration.libelle)}</span>
          {lieuDeclaration}
          {reglementLieuDeclaration?.code === 'SITE' ? (
            laboratoire?.laboratoireSuite?.webSiteUrl ? (
              <span className={classes.link} onClick={() => goToUrl(laboratoire?.laboratoireSuite?.webSiteUrl)}>
                {laboratoire?.laboratoireSuite?.webSiteUrl}
              </span>
            ) : (
              'internet'
            )
          ) : (
            <span className={classes.bold}>{capitalizeFirstLetter(reglementLieuDeclaration?.libelle)}</span>
          )}
        </>
      )}
    </Typography>
  );
};
