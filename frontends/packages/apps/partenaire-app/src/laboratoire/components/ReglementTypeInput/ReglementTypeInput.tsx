import { useGet_Prt_Reglement_TypesQuery } from '@lib/common/src/federation';
import { CustomSelect } from '@lib/ui-kit';
import { Box } from 'mdi-material-ui';
import React, { FC } from 'react';
export interface ReglementTypeInputProps {
  label?: string;
  idReglement?: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  name?: string;
}

export const ReglementTypeInput: FC<ReglementTypeInputProps> = ({ label, idReglement, onChange, name }) => {
  const reglements = useGet_Prt_Reglement_TypesQuery().data?.pRTReglementTypes?.nodes || [];

  return reglements?.length ? (
    <CustomSelect
      list={reglements}
      index="libelle"
      listId="id"
      name={name || 'idReglement'}
      value={idReglement || ''}
      onChange={onChange}
      label={label || 'Règlement par'}
      withPlaceholder={true}
    />
  ) : (
    <Box>Aucun réglement</Box>
  );
};
