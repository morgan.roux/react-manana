import React, { FC, ReactNode } from 'react';
import { Box, AppBar, Tabs, Tab, createStyles, makeStyles, Theme } from '@material-ui/core';
import { Route, Switch, useHistory, useParams } from 'react-router';
import { mergeModules, ModuleDefinition, useApplicationContext } from '@lib/common';

import contactModule from '../../modules/contact/contact.module';
import contratModule from '../../modules/contrat/contrat.module';
import commandeModule from '../../modules/historique-commande/historique-commande.module';
import aboutModule from '../../modules/about/about.module';
import actionOperationnelModule from '../../modules/action-operationnel/action-operationnel.module';
import historiqueFactureModule from '../../modules/historique-facture/historique-facture.module';
import tradeMarketingModule from '../../modules/trade-marketing/trade-marketing.module';
import catalogueProduitModule from '../../modules/catalogue-produit/catalogue-produit.module';
import compteRenduModule from '../../modules/compte-rendu/compte-rendu.module';
import conditionCommercialeModule from '../../modules/condition-commerciale/condition-commerciale.module';
import { useLaboratoireParams, Tab as TabType, LaboratoireURLParams } from '../../hooks/useLaboratoireParams';
import todoModule from '../../modules/todo/todo.module';
import rendezVousModule from './../../modules/rendezVous/rendez-vous.module';
import { useEffect } from 'react';

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
  paddingx: number;
  paddingy: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, paddingx, paddingy, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`custom-tabpanel-${index}`}
      aria-labelledby={`custom-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box px={paddingx} py={paddingy}>
          {children}
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: any) {
  return {
    id: `custom-tab-${index}`,
    'aria-controls': `custom-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      [theme.breakpoints.down('md')]: {
        marginBottom: 70,
      },
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,

      '& .MuiTabs-indicator': {
        background: theme.palette.primary.main,
      },
    },
    headerTab: {
      background: '#fff !important',
      boxShadow: 'none !important',
      [theme.breakpoints.up('md')]: {
        borderBottom: '1px solid #ccc',
      },
    },
    labelTab: {
      textTransform: 'none !important' as any,
      color: '#000 !important',
      minWidth: 'auto',
      padding: '0 20px',
      [theme.breakpoints.down('md')]: {
        minHeight: 35,
        height: 35,
        padding: '0 10px',
      },
    },
    tabSelected: {
      color: `${theme.palette.primary.main} !important`,
    },
    controllScroll: {
      [theme.breakpoints.down('md')]: {
        display: 'none',
      },
      color: theme.palette.primary.main,
      '& svg': {
        fontSize: '2.5em',
      },
    },
    tabIndicator: {
      [theme.breakpoints.down('md')]: {
        height: '100%',
        opacity: 0.1,
        borderRadius: 10,
      },
    },
    tabContainer: {
      [theme.breakpoints.down('md')]: {
        padding: '0 20px',
        minHeight: 35,
        height: 35,
      },
    },
  })
);

export interface TabsProps {
  tabTitle?: string;
  topBar?: ReactNode;
  content?: ReactNode;
  path?: TabType;
  id?: string;
  params?: LaboratoireURLParams;
  cardMobile?: any;
  active?: boolean;
}
interface CustomTabProps {
  tab?: TabsProps[];
  pageError?: any;
}

const partenaireModule: ModuleDefinition = mergeModules(
  tradeMarketingModule,
  actionOperationnelModule,
  contactModule,
  contratModule,
  commandeModule,
  aboutModule,
  historiqueFactureModule,
  catalogueProduitModule,
  compteRenduModule,
  conditionCommercialeModule,
  todoModule,
  rendezVousModule
);

export const CustomTabs: FC<CustomTabProps> = ({ tab, pageError }) => {
  const classes = useStyles();

  const { redirectTo } = useLaboratoireParams();
  const { isMobile } = useApplicationContext();

  const { tab: tabUrl } = useParams<{ tab: string }>();
  const tabIndex = (tab || []).findIndex((currentTab) => currentTab.path == tabUrl);
  const value = tabIndex >= 0 ? tabIndex : 0;

  const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    event.stopPropagation();
    event.preventDefault();

    // if (tab?.length) {
    //   history.push(`/laboratoires/${tab[newValue].path}`);
    // }
  };

  const handleClick = (event: React.ChangeEvent<{}>, path?: TabType, params?: LaboratoireURLParams) => {
    event.stopPropagation();
    event.preventDefault();
    redirectTo(params, path || 'plan');
  };

  return (
    <div className={classes.root}>
      {tab && tab.length > 0 ? (
        <>
          {tab.map((item, index) => (
            <TabPanel value={value} index={index} paddingx={3} paddingy={3}>
              {item.topBar}
            </TabPanel>
          ))}

          <AppBar position="static" className={classes.headerTab} elevation={0}>
            <Tabs
              TabScrollButtonProps={{
                classes: {
                  root: classes.controllScroll,
                },
              }}
              TabIndicatorProps={{
                className: classes.tabIndicator,
              }}
              className={classes.tabContainer}
              variant="scrollable"
              scrollButtons={isMobile ? 'on' : 'auto'}
              value={value}
              onChange={handleChange}
              aria-label="labo-tab-custom"
            >
              {tab.map((item, index) => (
                <Tab
                  label={item.tabTitle}
                  {...a11yProps(index)}
                  className={classes.labelTab}
                  classes={{ selected: classes.tabSelected }}
                  onClick={(event) => handleClick(event, item.path, item.params)}
                />
              ))}
            </Tabs>
          </AppBar>
          {isMobile &&
            tab.map((item, index) => (
              <TabPanel value={value} index={index} paddingx={3} paddingy={isMobile ? 0 : 3}>
                {item.cardMobile}
              </TabPanel>
            ))}
          <Switch>
            {(partenaireModule.routes || []).map((route) => {
              return <Route path={route.path} component={route.component} exact={route.exact} />;
            })}
          </Switch>
        </>
      ) : (
        <>{pageError}</>
      )}
    </div>
  );
};
