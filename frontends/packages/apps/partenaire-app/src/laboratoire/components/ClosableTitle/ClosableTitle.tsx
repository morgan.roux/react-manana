import { Box, IconButton, Typography } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import React, { FC, useEffect } from 'react';

import { useStyles } from './styles';

interface ClosableTitleProps {
  title: string;
  onClose: () => void;
}

export const ClosableTitle: FC<ClosableTitleProps> = ({ title, onClose }) => {
  const classes = useStyles();

  return (
    <Box className={classes.top}>
      <Typography className={classes.title}>{title}</Typography>
      <IconButton onClick={onClose}>
        <Close />
      </IconButton>
    </Box>
  );
};
