import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      paddingLeft: 24,
      backgroundColor: theme.palette.common.white,
    },
    top: {
      display: 'flex',
      justifyContent: 'space-between',
      width: '100%',
      alignItems: 'center',
      paddingBottom: 0,
    },
    title: {
      fontWeight: 'bold',
      fontSize: 18,
    },
  })
);
