import { SchedulerParameter } from '@lib/ui-kit';
import { DashboardRepartition } from '@lib/ui-kit/src/components/pages/DashboardPage/DashboardRepartition';
import { Box, Grid } from '@material-ui/core';
import React, { FC, Dispatch } from 'react';
import useStyles from './styles';
// import classNames from 'classnames';

interface HeaderPageProps {
  setOpenDrawer: Dispatch<boolean>;
}

export const HeaderPage: FC<HeaderPageProps> = ({ setOpenDrawer }) => {
  const [fullScreen, setFullScreen] = React.useState<boolean>(false);
  const classes = useStyles();

  const series = [
    { categorie: 'Retours labo', valeur: 0.4, couleur: 'blue' },
    { categorie: 'Invendus', valeur: 0.1, couleur: 'yellow' },
    { categorie: 'Problèm logistique', valeur: 0.2, couleur: 'violet' },
    { categorie: 'Rérimés', valeur: 0.2, couleur: 'red' },
  ];

  return (
    <>
      <Box className={classes.chart}>
        <DashboardRepartition series={series} graphType="donut" showProportion={false} />;
      </Box>
      <SchedulerParameter setOpenDrawer={setOpenDrawer} />
    </>
  );
};
