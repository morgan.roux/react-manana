import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    repartitionBox: {
      width: '100%',
      minWidth: 408,
    },
    chart: {
      padding: '16px',
    },
    icon: {
      '& .MuiSvgIcon-root': {
        fontSize: '16px',
        marginRight: '6px',
      },
    },
    containerCard: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginTop: theme.spacing(0.3),
      minWidth: '340px',
      height: theme.spacing(15),
      [theme.breakpoints.down('lg')]: {
        marginTop: '20px',
      },
      [theme.breakpoints.down('md')]: {
        marginTop: '20px',
      },
      [theme.breakpoints.down('sm')]: {
        marginTop: '20px',
      },
    },
    cardLitige: {
      width: '100%',
      backgroundColor: '#63B8DD',
      color: '#fff',
    },
    suiviIcon: {
      '&.MuiSvgIcon-root': {
        color: '#fff',
        opacity: '0.1',
        fontSize: '200px',
        marginTop: '36px',
        marginLeft: '100px',
      },
    },
    containerRoot: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-evenly',
      flexWrap: 'wrap',
      '& .MuiPaper-root': {
        maxWidth: '950px',
      },
      [theme.breakpoints.down('md')]: {
        display: 'flex',
        justifyContent: 'space-evenly',
        flexWrap: 'wrap',
      },
    },
    cardRemuneration: {
      backgroundColor: '#cb48b7',
      color: '#fff',
    },
    // cardVacance: {
    //   backgroundColor: '#63B8DD',
    //   color: '#fff',
    // },
    vacanceIcon: {
      '&.MuiSvgIcon-root': {
        fontSize: '177px',
        color: '#fff',
        opacity: '0.4',
        marginLeft: '90px',
        marginTop: '33px',
      },
    },
    cardFeries: {
      backgroundColor: '#FBB104',
      color: '#fff',
    },
    feriesIcon: {
      '&.MuiSvgIcon-root': {
        fontSize: '177px',
        color: '#fff',
        opacity: '0.4',
        marginLeft: '90px',
        marginTop: '33px',
      },
    },
    cardPartenariat: {
      backgroundColor: '#00A745',
      color: '#fff',
    },
    gardeIcon: {
      '&.MuiSvgIcon-root': {
        fontSize: '177px',
        color: '#fff',
        opacity: '0.1',
        marginLeft: '90px',
        marginTop: '33px',
      },
    },
  })
);

export default useStyles;
