import { useApplicationContext } from '@lib/common';
import { CustomSelectMenu } from '@lib/ui-kit';
import filterIcon from './../../assets/img/filter_alt.svg';
import React, { FC } from 'react';
import {
  useGet_Prt_Reglement_Mode_DeclarationsQuery,
  useGet_Prt_Reglement_TypesQuery,
} from '@lib/common/src/federation';
import { Box } from '@material-ui/core';
import { useStyles } from './style';
import { ModeDeclarationInput } from '../ModeDeclarationInput/ModeDeclarationInput';

interface ReglementFilterProps {
  onRequestModeDeclarationFilter: (id?: string) => void;
  onRequestTypeDeclarationFilter: (id?: string) => void;
}

export const ReglementFilter: FC<ReglementFilterProps> = ({
  onRequestModeDeclarationFilter,
  onRequestTypeDeclarationFilter,
}) => {
  const { isMobile } = useApplicationContext();
  const iconFilter = <img src={filterIcon} alt="filter" />;
  const classes = useStyles();
  const loadPRTModeDeclarations = useGet_Prt_Reglement_Mode_DeclarationsQuery();
  const loadPRTTypeDeclarations = useGet_Prt_Reglement_TypesQuery();

  const typeDeclarations = [
    { id: 'Tous', name: 'Tous' },
    ...(loadPRTTypeDeclarations.data?.pRTReglementTypes.nodes || [])?.map((element) => {
      return {
        id: element.id,
        name: element.libelle,
      };
    }),
  ];
  const modeDclarations = [
    { id: 'Tous', name: 'Tous' },
    ...(loadPRTModeDeclarations.data?.pRTReglementModeDeclarations.nodes || []).map((element) => {
      return {
        id: element.id,
        name: element.libelle,
      };
    }),
  ];

  const handleModeDeclarationFilter = (id?: string) => {
    onRequestModeDeclarationFilter(id);
  };

  const handleTypeDeclarationFilter = (id?: string) => {
    onRequestTypeDeclarationFilter(id);
  };

  return !isMobile ? (
    <Box display="flex" flexDirection="row">
      <Box px={1}>
        <CustomSelectMenu
          buttonLabel="Mode déclaration"
          listFilters={modeDclarations || []}
          handleClickItem={handleModeDeclarationFilter}
          iconFilter={iconFilter}
        />
      </Box>
      <Box px={1}>
        <CustomSelectMenu
          buttonLabel="Règlement par: "
          listFilters={typeDeclarations || []}
          handleClickItem={handleTypeDeclarationFilter}
          iconFilter={iconFilter}
        />
      </Box>
    </Box>
  ) : (
    <Box>
      <Box className={classes.displayInline}>
        <CustomSelectMenu
          buttonLabel="Mode déclaration"
          listFilters={modeDclarations || []}
          handleClickItem={handleModeDeclarationFilter}
          iconFilter={iconFilter}
        />
      </Box>
      <Box className={classes.displayInline}>
        <CustomSelectMenu
          buttonLabel="Règlement par: "
          listFilters={typeDeclarations || []}
          handleClickItem={handleTypeDeclarationFilter}
          iconFilter={iconFilter}
        />
      </Box>
    </Box>
  );
};
