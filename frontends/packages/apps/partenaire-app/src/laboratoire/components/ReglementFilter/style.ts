import { Theme, createStyles, makeStyles } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    displayInline: {
      verticalAlign: 'top',
      display: 'inline-block',
      width: '50%',
    },
  })
);
