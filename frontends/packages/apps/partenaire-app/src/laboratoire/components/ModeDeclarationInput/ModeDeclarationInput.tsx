import { useGet_Prt_Reglement_Mode_DeclarationsQuery } from '@lib/common/src/federation';
import { CustomSelect } from '@lib/ui-kit';
import { Box } from 'mdi-material-ui';
import React, { FC } from 'react';
import { useEffect } from 'react';
export interface ModeDeclarationInputProps {
  label?: string;
  idModeDeclaration?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  name?: string;
  onChangeValue?: (name: string, value?: string) => void;
}

export const ModeDeclarationInput: FC<ModeDeclarationInputProps> = ({
  label,
  idModeDeclaration,
  onChange,
  name,
  onChangeValue,
}) => {
  const modeDeclarations =
    useGet_Prt_Reglement_Mode_DeclarationsQuery().data?.pRTReglementModeDeclarations?.nodes || [];

  const idAutre = modeDeclarations.find((mode) => mode.code === 'AUTRE')?.id;
  useEffect(() => {
    if (idAutre && !idModeDeclaration && onChangeValue) onChangeValue('idModeDeclaration', idAutre);
  }, [idAutre, idModeDeclaration]);

  return modeDeclarations?.length ? (
    <CustomSelect
      list={modeDeclarations}
      index="libelle"
      listId="id"
      name={name || 'idModeDeclaration'}
      value={idModeDeclaration ?? (onChangeValue ? idAutre : undefined)}
      onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
        onChange && onChange(event);
        onChangeValue && onChangeValue(event.target.name, event.target.value);
      }}
      placeholder="Mode déclaration"
      label={label || 'Mode déclaration'}
      withPlaceholder={true}
    />
  ) : (
    <Box>Aucun résultat</Box>
  );
};
