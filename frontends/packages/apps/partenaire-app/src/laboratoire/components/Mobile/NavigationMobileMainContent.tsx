import { useApplicationContext } from '@lib/common';
import { Box, Typography } from '@material-ui/core';
import React, { FC } from 'react';

export const TitleMobile: FC<{}> = () => {
  const { isMobile } = useApplicationContext();
  return isMobile ? (
    <div style={{ padding: '0 16px', paddingTop: 16 }}>
      <Typography style={{ fontWeight: 'bold' }}>Liste des laboratoires</Typography>
    </div>
  ) : (
    <></>
  );
};
