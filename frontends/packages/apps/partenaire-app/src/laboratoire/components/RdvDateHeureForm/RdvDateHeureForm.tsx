import { CustomDatePicker, CustomFormTextField, CustomSelect } from '@lib/ui-kit';
import { Grid } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { computeHourPlus, typeReunions } from '../../../laboratoire/util';

interface RdvDateHeureFormProps {
  values?: {
    dateRendezVous: any;
    heureDebut: any;
    heureFin: any;
    typeReunion: string;
  };
  setValues: (values: any) => void;
}

const RdvDateHeureForm: FC<RdvDateHeureFormProps> = ({ values, setValues }) => {
  const [state, setState] = useState(
    values || {
      dateRendezVous: null,
      heureDebut: null,
      heureFin: null,
      typeReunion: 'VISIO',
    }
  );

  useEffect(() => {
    setValues({ ...values, dateRendezVous: new Date() });
  }, []);

  const { dateRendezVous, heureDebut, heureFin, typeReunion } = state;

  const handleChange = (e: any) => {
    const { value, name } = e.target;

    if (name === 'heureDebut') {
      const heureFin = computeHourPlus(value);

      setState((prev) => {
        const values = { ...prev, [name]: value, heureFin };
        setValues(values);
        return values;
      });

      return;
    }

    setState((prev) => {
      const values = { ...prev, [name]: value };
      setValues(values);
      return values;
    });
  };

  return (
    <>
      <Grid style={{ marginTop: 8 }} container spacing={2}>
        <Grid item lg={6} sm={12} md={6} xs={12}>
          <CustomDatePicker
            style={{ width: '100%' }}
            name="dateRendezVous"
            value={dateRendezVous || new Date()}
            onChange={(date) => handleChange({ target: { value: date, name: 'dateRendezVous' } })}
            required
            label="Date Rendez-Vous"
          />
        </Grid>
        <Grid container item lg={6} sm={12} md={6} xs={12}>
          <Grid style={{ paddingRight: 8 }} item sm={6} xs={6}>
            <CustomFormTextField type="time" label="A" name="heureDebut" onChange={handleChange} value={heureDebut} />
          </Grid>
          <Grid style={{ paddingLeft: 8 }} item sm={6} xs={6}>
            <CustomFormTextField type="time" label="Jusqu'à" name="heureFin" onChange={handleChange} value={heureFin} />
          </Grid>
        </Grid>
      </Grid>
      <Grid style={{ marginTop: 8 }} container spacing={2}>
        <Grid item lg={12} sm={12} md={12} xs={12}>
          <CustomSelect
            list={typeReunions}
            index="libelle"
            listId="code"
            label="Type Rendez-Vous"
            name="typeReunion"
            value={typeReunion}
            onChange={handleChange}
            required
          />
        </Grid>
      </Grid>
    </>
  );
};

export default RdvDateHeureForm;
