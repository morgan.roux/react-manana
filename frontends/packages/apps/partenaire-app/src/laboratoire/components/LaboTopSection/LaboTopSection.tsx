import { CustomModal, DebouncedSearchInput } from '@lib/ui-kit';
import { SearchInput, CustomSetting, CustomSelectMenu, CustomSelect } from '@lib/ui-kit';
import { Box, IconButton, Popover, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { useStyles } from './styles';
import filterIcon from '../../assets/img/filter_alt.svg';
import laboFilterIcon from '../../assets/img/science_black_24dp.svg';
import { CardSection } from './CardSection';
import { PartenaireSelect, useApplicationContext } from '@lib/common';
import { useLaboratoireParams } from '../../hooks/useLaboratoireParams';
import { useReactiveVar } from '@apollo/client';
import { searchTextVar } from '../../reactive-vars';
import CloseIcon from '@material-ui/icons/Close';
import { CSSProperties } from '@material-ui/styles';
import { Search } from '@material-ui/icons';
import { ReglementFilter } from '../ReglementFilter/ReglementFilter';

interface Parametre {
  libelle?: any;
  code?: any;
  active?: boolean;
}
interface LaboTopSectionProps {
  title?: string;
  titleStyle?: CSSProperties;
  operationFilter?: any[];
  categoryFilter?: any[];
  litigeFilter?: any[];
  withSearch?: boolean;
  selectProps?: any;
  onChangeParametre?: (id: any) => void;
  parametres?: Parametre[];
  onSearchLaboChange?: any;
  listLabo?: any[];
  handleCategorieFilter?: (id?: any) => void;
  handleOperationFilter?: (id?: any) => void;
  handleLitigeFilter?: (id?: any) => void;
  litigeCardData?: any;
  partenariatsCardData?: any;
  remunerationCardData?: any;
  remiseArriereData?: any;
  chartCardData?: any;
  withCloseBtn?: boolean;
  withStats?: boolean;
  handleCloseButton?: (event: React.MouseEvent<{}>) => void;
  operationFilterName?: string;
  reglementFilter?: {
    with: boolean;
    handleModeReglementFilter: (id?: string) => void;
    handleTypeDeclarationFilter: (id?: string) => void;
  };
  categorieFilterName?: string;
}
export const LaboTopSection: FC<LaboTopSectionProps> = ({
  title,
  operationFilter,
  categoryFilter,
  litigeFilter,
  parametres,
  onChangeParametre,
  handleCategorieFilter,
  handleLitigeFilter,
  handleOperationFilter,
  litigeCardData,
  partenariatsCardData,
  remunerationCardData,
  chartCardData,
  selectProps,
  withSearch,
  titleStyle,
  handleCloseButton,
  withCloseBtn,
  operationFilterName,
  withStats = true,
  remiseArriereData,
  reglementFilter,
  categorieFilterName,
}) => {
  const classes = useStyles();
  const { params, redirectTo } = useLaboratoireParams();
  const [getPath, setGetPath] = React.useState<boolean>();
  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
  const [openFilterModal, setOpenFilterModal] = React.useState<boolean>(false);
  const searchTextValue = useReactiveVar<string>(searchTextVar);
  const { isMobile } = useApplicationContext();

  React.useEffect(() => {
    if (window.location.href.includes('pilotages')) {
      setGetPath(true);
    }
  }, []);
  const iconFilter = <img src={filterIcon} alt="filter" />;
  const handleOpenMobileFilter = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    setOpenFilterModal(!openFilterModal);
  };
  const handleLitigeFiltere = (id?: any) => {
    setOpenFilterModal(!openFilterModal);
    handleLitigeFilter && handleLitigeFilter(id);
  };
  const handleCategorieFiltere = (id?: any) => {
    handleCategorieFilter && handleCategorieFilter(id);
    setOpenFilterModal(!openFilterModal);
  };
  const handleOperationFiltere = (id?: any) => {
    handleOperationFilter && handleOperationFilter(id);
    setOpenFilterModal(!openFilterModal);
  };
  const handleRequestModeDeclarationFilter = (id?: any) => {
    reglementFilter?.handleModeReglementFilter(id);
    setOpenFilterModal(!openFilterModal);
  };
  const handleRequestTypeDeclarationFilter = (id?: any) => {
    reglementFilter?.handleTypeDeclarationFilter(id);
    setOpenFilterModal(!openFilterModal);
  };
  const handleMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;
  return (
    <>
      {isMobile && (
        <Box className={classes.mobileAppBar} style={getPath ? { top: 0 } : undefined}>
          <Typography className={classes.title}>{title}</Typography>
        </Box>
      )}
      <div className={classes.root}>
        <Box
          display="flex"
          style={titleStyle}
          alignItems="center"
          justifyContent={isMobile ? undefined : 'space-between'}
        >
          {!isMobile && (
            <Box>
              <Typography className={classes.title}>{title}</Typography>
            </Box>
          )}
          <Box display="flex" className={classes.containerMobileTop}>
            {!isMobile && withSearch && (
              <Box mr={1}>
                <DebouncedSearchInput
                  value={searchTextValue}
                  outlined
                  placeholder="Rechercher"
                  onChange={searchTextVar}
                />
              </Box>
            )}
            <Box mr={1} className={classes.laboFilterMobile}>
              <PartenaireSelect
                // noTitle={isMobile}
                filterIcon={<img src={laboFilterIcon} alt="filter" style={{ filter: 'brightness(10) invert(1)' }} />}
                value={params.idLabo?.length ? params.idLabo[0] : undefined}
                index="laboratoire"
                onCreated={(newLaboratoire) => {
                  redirectTo({
                    ...params,
                    idLabo: newLaboratoire?.id ? [newLaboratoire.id] : undefined,
                  });
                }}
                onChange={(value) => {
                  redirectTo({
                    ...params,
                    idLabo: value?.id ? [value.id] : undefined,
                  });
                }}
              />
            </Box>
            {/* hide mobile */}
            {isMobile && withSearch && (
              <>
                <IconButton onClick={handleMenuClick} style={{ padding: 5, marginRight: 10, color: '#000' }}>
                  <Search />
                </IconButton>

                <Popover
                  id={id}
                  open={open}
                  anchorEl={anchorEl}
                  onClose={handleClose}
                  anchorOrigin={{
                    vertical: 'center',
                    horizontal: 'center',
                  }}
                  transformOrigin={{
                    vertical: 'center',
                    horizontal: 'center',
                  }}
                >
                  <Box p={2}>
                    <DebouncedSearchInput
                      value={searchTextValue}
                      outlined
                      placeholder="Rechercher"
                      onChange={searchTextVar}
                    />
                  </Box>
                </Popover>
              </>
            )}
            {isMobile &&
            ((categoryFilter && categoryFilter?.length > 0) ||
              (litigeFilter && litigeFilter.length) ||
              reglementFilter ||
              (operationFilter && operationFilter.length > 0)) ? (
              <>
                <IconButton onClick={handleOpenMobileFilter} style={{ padding: 5, marginRight: 10 }}>
                  <img src={filterIcon} alt="filter" />
                </IconButton>
                <CustomModal
                  open={openFilterModal}
                  setOpen={setOpenFilterModal}
                  closeIcon
                  title="Filtres"
                  headerWithBgColor
                  withBtnsActions={false}
                >
                  <Box>
                    {categoryFilter && categoryFilter?.length > 0 && (
                      <Box className={classes.displayInline}>
                        <CustomSelectMenu
                          buttonLabel={categorieFilterName || 'Catégorie'}
                          listFilters={categoryFilter}
                          handleClickItem={handleCategorieFiltere}
                          iconFilter={iconFilter}
                        />
                      </Box>
                    )}
                    {litigeFilter && litigeFilter.length && (
                      <Box className={classes.displayInline}>
                        <CustomSelectMenu
                          buttonLabel="Litiges"
                          listFilters={litigeFilter}
                          handleClickItem={handleLitigeFiltere}
                          iconFilter={iconFilter}
                        />
                      </Box>
                    )}
                    {operationFilter && operationFilter.length > 0 && (
                      <Box className={classes.displayInline}>
                        <CustomSelectMenu
                          buttonLabel={operationFilterName || 'Opération'}
                          listFilters={operationFilter}
                          handleClickItem={handleOperationFiltere}
                          iconFilter={iconFilter}
                        />
                      </Box>
                    )}
                    {reglementFilter?.with && (
                      <ReglementFilter
                        onRequestModeDeclarationFilter={handleRequestModeDeclarationFilter}
                        onRequestTypeDeclarationFilter={handleRequestTypeDeclarationFilter}
                      />
                    )}
                  </Box>
                </CustomModal>
              </>
            ) : (
              <>
                {categoryFilter && categoryFilter?.length > 0 && (
                  <Box mr={1}>
                    <CustomSelectMenu
                      buttonLabel={categorieFilterName || 'Catégorie'}
                      listFilters={categoryFilter}
                      handleClickItem={handleCategorieFilter}
                      iconFilter={iconFilter}
                    />
                  </Box>
                )}
                {litigeFilter && litigeFilter.length && (
                  <Box mr={1}>
                    <CustomSelectMenu
                      buttonLabel="Litiges"
                      listFilters={litigeFilter}
                      handleClickItem={handleLitigeFilter}
                      iconFilter={iconFilter}
                    />
                  </Box>
                )}
                {operationFilter && operationFilter.length > 0 && (
                  <Box mr={1}>
                    <CustomSelectMenu
                      buttonLabel={operationFilterName || 'Opération'}
                      listFilters={operationFilter}
                      handleClickItem={handleOperationFilter}
                      iconFilter={iconFilter}
                    />
                  </Box>
                )}
                {reglementFilter?.with && (
                  <ReglementFilter
                    onRequestModeDeclarationFilter={handleRequestModeDeclarationFilter}
                    onRequestTypeDeclarationFilter={handleRequestTypeDeclarationFilter}
                  />
                )}
              </>
            )}
            {parametres && (
              <Box mr={1} display="flex" alignItems="center">
                <CustomSetting parametres={parametres} onChange={onChangeParametre} />
              </Box>
            )}

            {/* hide mobile */}
            {!isMobile && selectProps && (
              <CustomSelect {...selectProps} classes={{ formControl: classes.customSelect }} noMarginBottom />
            )}
            {withCloseBtn && (
              <Box mr={1} display="flex" alignItems="center">
                <IconButton onClick={handleCloseButton} className={classes.closeBtn}>
                  <CloseIcon />
                </IconButton>
              </Box>
            )}
          </Box>
        </Box>
        {!isMobile && withStats && (
          <Box>
            <CardSection
              litiges={litigeCardData}
              partenariats={partenariatsCardData}
              remuneration={remunerationCardData}
              chartCard={chartCardData}
              remiseArriere={remiseArriereData}
            />
          </Box>
        )}
      </div>
    </>
  );
};
