import { useApplicationContext } from '@lib/common';
import { CardPartenaire, CustomCard } from '@lib/ui-kit';
import { DashboardRepartition } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import swapIcon from '../../assets/img/swap_icon.svg';
import { useStyles } from './styles';
import swapHorizontalCircle from '../../assets/img/swapHorizontalCircle.svg';
import euro from '../../assets/img/euro.svg';
import partenariatbg from '../../assets/img/partenariat.png';
import { SwiperSlide } from 'swiper/react';

interface DataProps {
  lastYear?: any; // date passe
  currentYear?: any; // date current
  currentYearMontantPrevu?: any; // Montant mise en avant
  lastYearNombres1?: any; // Premiers nombres (colone 1) last year
  currentYearNombres1?: any; // Deuxieme Nombres en colone 1 current year
  lastYearNombres2?: any; // Column 2 last year
  currentYearNombres2?: any; // Column 2 current year
}
interface CardSectionProps {
  litiges?: any;
  partenariats?: any;
  remuneration?: any;
  chartCard?: any;
  seriesRepartition?: any;
  remiseArriere?: any;
}

const withLitige = (litiges?: { valeur: number }[]) => {
  const nblitige = (litiges || []).reduce((accumulator, litige) => accumulator + litige.valeur, 0);

  console.log('nblitige', nblitige, litiges);
  return !!nblitige;
};

export const CardSection: FC<CardSectionProps> = ({
  litiges,
  partenariats,
  remuneration,
  chartCard,
  remiseArriere,
}) => {
  const { isMobile } = useApplicationContext();
  const classes = useStyles();
  return (
    <SwiperSlide className={classes.swiperSection}>
      <Box display={isMobile ? 'inline-flex' : 'flex'} justifyContent="center" mt={isMobile ? 2 : 4}>
        {litiges && (
          <Box mr={3} className={isMobile ? classes.boxMobileCard : ''}>
            <CustomCard
              data={litiges}
              title="Litiges"
              bgColor="#63b8dd"
              bgImages={swapHorizontalCircle}
              label1="En cours"
              label2="Clôturés"
              backgroundFilter="rgb(99 184 221 / 79%)"
              style={{ background: `url(${swapHorizontalCircle}) 170px -5px / 210px no-repeat` }}
            />
          </Box>
        )}
        {remuneration && (
          <Box mr={3} className={isMobile ? classes.boxMobileCard : ''}>
            <CustomCard
              data={remuneration}
              title="Rémunérations"
              bgColor="#cb48b7"
              bgImages={euro}
              label1="Réalisées"
              label2="Payées"
              style={{ background: `url(${euro}) 170px -5px / 230px no-repeat` }}
              backgroundFilter="rgb(204 108 184 / 0.79)"
            />
          </Box>
        )}
        {partenariats?.data?.length > 0 && (
          <Box mr={3} className={isMobile ? classes.boxMobileCard : ''}>
            <CardPartenaire
              currentYear={partenariats?.currentYear}
              bgColor="#34a745"
              bgImages={partenariatbg}
              backgroundPosition="53px"
              backgroundPositionY="8px"
              data={partenariats?.data}
              titre="Plan Trade"
            />
          </Box>
        )}
        {remiseArriere?.data?.length > 0 && (
          <Box mr={3} className={isMobile ? classes.boxMobileCard : ''}>
            <CardPartenaire
              currentYear={remiseArriere?.currentYear}
              bgColor="#34a745"
              bgImages={partenariatbg}
              backgroundPosition="53px"
              backgroundPositionY="8px"
              data={remiseArriere?.data}
              titre="Remise Arrière"
            />
          </Box>
        )}
        {chartCard && withLitige(chartCard) && (
          <Box
            boxShadow="0 0 5px 0 #ccc"
            borderRadius={5}
            overflow="hidden"
            className={isMobile ? classes.boxMobileCard : ''}
          >
            <DashboardRepartition series={chartCard} graphType="donut" showProportion={true} withLabel />
          </Box>
        )}
      </Box>
    </SwiperSlide>
  );
};
