import { Theme, createStyles, makeStyles } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {},
    title: {
      [theme.breakpoints.down('md')]: {
        fontSize: 16,
      },
      fontSize: 20,
      fontWeight: 700,
      marginRight: 10,
    },
    customSelect: {
      width: 'auto !important',
      '&>div': {
        padding: '0 20px',
      },
      '& fieldset': {
        border: 'none !important',
      },
    },
    mobileAppBar: {
      position: 'relative',
      width: 'calc(100% + 48px)',
      background: theme.palette.primary.main,
      left: '-24px',
      top: '-24px',
      padding: 15,
      textAlign: 'center',
      color: '#fff',
      opacity: 0.8,
    },
    boxMobileCard: {
      display: 'inline-block',
      verticalAlign: 'top',
    },
    swiperSection: {
      display: 'content',
      [theme.breakpoints.down('sm')]: {
        overflow: 'auto',
        display: 'flex',
        alignItems: 'center',
        marginRight: '0 !important',
        '&::-webkit-scrollbar': {
          height: '0 !important',
          width: '0 !important',
        },
      },
    },
    laboFilterMobile: {
      [theme.breakpoints.down('md')]: {
        position: 'absolute',
        left: 0,
        top: 0,
        display: 'flex',
        whiteSpace: 'nowrap',
        maxWidth: '60%',
        overflow: 'hidden',
        borderRadius: 5,
      },
    },
    containerMobileTop: {
      [theme.breakpoints.down('md')]: {
        position: 'relative',
        justifyContent: 'flex-end',
        width: '100%',
        minHeight: 35,
      },
    },
    closeBtn: {
      padding: 5,
    },
    displayInline: {
      verticalAlign: 'top',
      display: 'inline-block',
      width: '50%',
    },
  })
);
