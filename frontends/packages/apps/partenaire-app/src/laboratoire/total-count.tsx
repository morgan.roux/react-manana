import {
  MeUserInfoFragment,
  Get_Row_Rendez_VousDocument,
  Get_Row_Rendez_VousQuery,
  Get_Row_Rendez_VousQueryVariables,
  Pharmacie,
} from '@lib/common/src/federation';
import { ApolloClient } from '@apollo/client';
import moment from 'moment';

export const fetchRendezVousTotalCount = async (context: {
  user: MeUserInfoFragment;
  graphql: ApolloClient<any>;
  federation: ApolloClient<any>;
  currentPharmacie: Pharmacie;
}): Promise<number> => {
  const today = moment.utc().format('YYYY-MM-DD'); // 2021-06-27T12:00:00.000Z

  const response = await context.federation.query<Get_Row_Rendez_VousQuery, Get_Row_Rendez_VousQueryVariables>({
    query: Get_Row_Rendez_VousDocument,
    variables: {
      filter: {
        and: [
          {
            idPharmacie: {
              eq: context.currentPharmacie.id,
            },
          },
          {
            dateRendezVous: {
              between: {
                lower: `${today}T00:00:00.000Z`,
                upper: `${today}T23:59:59.999Z`,
              },
            },
          },
        ],
      },
    },
    fetchPolicy: 'network-only',
  });
  return response?.data?.pRTRendezVousAggregate?.length ? response.data.pRTRendezVousAggregate[0].count?.id || 0 : 0;
};
