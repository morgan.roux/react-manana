import { Fichier, User } from '@lib/types';

export interface StatutPartenariat {
  id: string;
  libelle: string;
  code: string;
}

export interface TypePartenariat {
  id: string;
  libelle: string;
  code: string;
}

export interface Produit {
  id: string;
  libelle: string;
  prix?: number;
}

export interface HistoriqueStatut {
  id: string;
  libelle: string;
}

export interface LigneCommande {
  id?: string;
  produit: Produit;
  quantite: number;
  remise: number;
  total: number;
}

export interface Historique {
  id?: string;
  referenceCommande: number;
  date: Date;
  totalHT: number;
  totalTTC: number;
  totalTVA: number;
  status: string;
  fichiers?: any[];
  source?: string;
  idLaboratoire?: string;
  typeAssocie?: { id: string; nom: string };
}

export interface Laboratoire {
  id: string;
  nom: string;
  partenariat?: Partenariat;
  laboratoireSuite?: LaboratoireSuite;
  photo?: Fichier;
  partenaireType?: {
    id: string;
    libelle: string;
    code: string;
  };
  rdv?: {
    id: string;
    heureDebut: string;
    heureFin: string;
    ordreJour: string;
    dateRendezVous: string;
  };
}

export interface LaboratoireSuite {
  adresse?: string;
  codePostal?: string;
  id?: string;
  telecopie?: string;
  telephone?: string;
  ville?: string;
  webSiteUrl?: string;
  email?: string;
}

export interface Partenariat {
  id?: string;
  dateDebut?: Date;
  dateFin?: Date;
  statut?: StatutPartenariat;
  type?: TypePartenariat;
  responsables?: User[];
  isEnvoiEmail?: boolean;
  typeReunion?: string;
}

export interface LaboratoireSuivi {
  id?: string;
  dateSuivi: Date;
  titre: string;
  description: string;
  type: LaboratoireSuiviType;
  selectedFiles: File[];
  statut: LaboratoireSuiviStatus;
}

export interface LaboratoireSuiviType {
  id?: string;
  libelle: string;
  value: string;
}
export interface LaboratoireSuiviFiles {
  id?: string;
}

export interface LaboratoireSuiviStatus {
  id?: string;
  libelle: string;
  value: string;
}

export interface LaboratoirePlan {
  id?: string;
  dateDebut: Date;
  dateFin: Date;
  titre: string;
  description: string;
  type: LaboratoirePlanType;
  selectedFiles: File[];
  statut: LaboratoirePlanStatus;
}

export interface LaboratoirePlanStatus {
  id?: string;
  libelle: string;
  value: string;
}

export interface LaboratoirePlanFiles {
  id?: string;
}

export interface LaboratoirePlanType {
  id: string;
  libelle: string;
  value: string;
}

export interface PRTMiseAvant {
  id: string;
  libelle: string;
  code: string;
  couleur?: string;
}
