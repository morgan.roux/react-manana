import { Theme, createStyles, makeStyles } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: 96,
    },

    photo: {
      height: 54,
      width: 54,
    },
    iconAction: {
      padding: theme.spacing(0.5),
      margin: theme.spacing(0, 0.65, 0, 0),
      '& img': {
        filter: 'brightness(10)',
      },
    },
    createButton: {
      position: 'absolute',
      bottom: 30,
      right: 16,
      background: theme.palette.secondary.main,
      borderRadius: '50%',
      '& .MuiIconButton-root': {
        color: theme.palette.common.white,
      },
    },
    activeLaboTypography: {
      paddingLeft: '15px',
    },
    tabs: {
      background: 'white',
      height: '100%',
      width: '281px',
      borderRight: '1px solid rgba(0,0,0,0.2)',
    },
    tab: {
      width: '100%',
      background: 'none',
      paddingTop: '15px',
      paddingLeft: '15px',
      fontWeight: 'bolder',
      cursor: 'pointer',
    },
    gridListLabo: {
      width: 281,
      borderRight: '1px solid rgba(0,0,0,0.1)',
      height: '100%',
    },
    gridScroll: {
      height: 'calc(100vh - 250px)',
      overflow: 'auto',
    },
    active: {
      background: theme.palette.primary.main,
      color: '#FFFFFF',
    },
    tabListLabo: {
      fontFamily: 'Roboto',
      fontSize: 14,
      fontWeight: 'bold',
      cursor: 'pointer',
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    textList: {
      height: 65,
    },
    bold: {
      fontWeight: 'bold',
    },

    tousLabosIcon: {
      border: '2px #bdbdbd solid',
      color: '#bdbdbd',
      background: 'unset',
    },
    tousLabosLabel: {
      paddingLeft: 18,
      paddingTop: 10,
      fontWeight: 'bold',
    },
    laboItemInfos: {
      fontFamily: 'Roboto',
      fontSize: 14,
      '& span': {
        color: '#42424260',
      },
    },
    typoFontSideNav: {
      '& .MuiListItem-root .MuiTypography-root': {
        fontSize: 14,
        fontWeight: 500,
      },
    },
    scrollableClassNameMobile: {
      background: '#fff',
      '& button': {
        color: '#27272F !important',
        textTransform: 'none',
      },
    },
    filterLaboModal: {
      '& .MuiPaper-root': {
        [theme.breakpoints.up('xs')]: {
          minWidth: 400,
        },
      },
    },
    buttonImgFilter: {
      cursor: 'pointer',
    },
  })
);
