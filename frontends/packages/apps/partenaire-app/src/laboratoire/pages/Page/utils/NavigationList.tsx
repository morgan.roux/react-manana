import { Avatar, Box, Tab, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { INavigationList, TabActiveLaboComponentProps } from './interface';
import { useStyles } from '../style';
import noImageSrc from './../../../assets/img/pas_image.png';
import { SideNavListItem } from '@lib/ui-kit/src/components/templates/ParameterContainer';

const checkActiveItem = (item: string, activeItem: string) => {
  return item === activeItem;
};

const TabActiveLaboComponent: FC<TabActiveLaboComponentProps> = ({ activeLabo, noImageSrc, active }) => {
  const classes = useStyles();

  return (
    <Box display="flex" flexDirection="row" alignItems="center" mb={3} style={{ marginBottom: 0 }}>
      <Box alignItems="center" display="flex" flexDirection="row" width="80%">
        {!active &&
          (activeLabo?.photo ? (
            <Avatar className={classes.photo} src={activeLabo?.photo?.publicUrl || ''} />
          ) : (
            <Avatar className={classes.photo} src={noImageSrc} />
          ))}
        <Typography className={classes.activeLaboTypography}>
          {!active && activeLabo ? activeLabo?.nom : 'Liste des laboratoires'}
        </Typography>
      </Box>
    </Box>
  );
};

export const NavigationList = ({
  tab,
  laboratoire,
  onClickLaboActive,
  onSideNavListClick,
}: INavigationList): [SideNavListItem[], (item: string) => void] => {
  const list = [
    {
      label:
        tab !== 'list' && tab !== 'selected' && tab ? (
          <TabActiveLaboComponent active={tab === 'list'} activeLabo={laboratoire} noImageSrc={noImageSrc} />
        ) : (
          ''
        ),
      item: tab === 'list' ? 'list' : tab === 'pilotages' ? 'pilotages' : 'list',
      active: checkActiveItem('list', tab) || checkActiveItem('pilotages', tab),
      // mobile: true,
    },
    {
      label: 'A propos',
      item: 'about',
      active: checkActiveItem('about', tab),
      mobile: true,
    },
    {
      label: 'Contacts',
      item: 'contact',
      active: checkActiveItem('contact', tab),
      mobile: true,
    },
    {
      label: 'Pilotage',
      item: 'pilotage',
      active: checkActiveItem('pilotage', tab),
      mobile: true,
    },
    {
      label: 'Relation avec le laboratoire',
      item: 'suivi',
      active: checkActiveItem('suivi', tab),
      mobile: true,
    },
    {
      label: 'Comptes rendus',
      item:
        tab === 'compte-rendu'
          ? 'compte-rendu'
          : tab === 'ajout-compte-rendu'
          ? 'ajout-compte-rendu'
          : tab === 'edit-compte-rendu'
          ? 'edit-compte-rendu'
          : tab === 'detail-compte-rendu'
          ? 'detail-compte-rendu'
          : 'compte-rendu',
      active:
        checkActiveItem('compte-rendu', tab) ||
        checkActiveItem('edit-compte-rendu', tab) ||
        checkActiveItem('detail-compte-rendu', tab),
      mobile: true,
    },
    {
      label: 'Historique des commandes',
      item: 'historique',
      active: checkActiveItem('historique', tab),
      mobile: true,
    },
    {
      label: 'Historique des factures',
      item: tab === 'detail-facture' ? 'detail-facture' : 'factures',
      active: checkActiveItem('factures', tab) || checkActiveItem('detail-facture', tab),
      mobile: true,
    },
    {
      label: 'Conditions commerciales',
      item: tab === 'detail-condition' ? 'detail-condition' : 'condition',
      active: checkActiveItem('condition', tab) || checkActiveItem('detail-condition', tab),
      mobile: true,
    },
    {
      label: 'Trade marketing',
      item: 'plan',
      active: checkActiveItem('plan', tab),
      mobile: true,
    },

    {
      label: 'Rémunération',
      item: 'remuneration',
      active: checkActiveItem('remuneration', tab),
      mobile: true,
    },
    {
      label: 'Actualités',
      item: 'actualite',
      active: checkActiveItem('actualite', tab),
      mobile: true,
    },
    {
      label: 'Catalogue Produit',
      item: 'catalogue',
      active: checkActiveItem('catalogue', tab),
      mobile: true,
    },
    {
      label: 'Contrat',
      item: 'contrat',
      active: checkActiveItem('contrat', tab),
      mobile: true,
    },
  ];

  const handleSideNavListClick = (item: string) => {
    if (item === 'list') {
      onClickLaboActive('');
    }
    onSideNavListClick(item);
  };

  return [list.map((el) => ({ ...el, active: el.item === tab })), handleSideNavListClick];
};
