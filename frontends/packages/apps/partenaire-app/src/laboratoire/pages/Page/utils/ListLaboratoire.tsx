import React, { FC, useState, useEffect } from 'react';
import moment from 'moment';
import { ListItem, Box, Typography, Grid, Avatar, List, IconButton } from '@material-ui/core';
import { Domain, Equalizer } from '@material-ui/icons';
import { BottomScrollListener } from 'react-bottom-scroll-listener';
import { useStyles } from './../style';
import classnames from 'classnames';
import { DebouncedSearchInput, Backdrop, CustomModal } from '@lib/ui-kit';
import { ItemLaboratoireProps, LaboratoireProps, SideNavListLaboratoireProps } from './interface';
import { useApplicationContext } from '@lib/common';
import { FilterIcon } from '../../../components/Filter';

const ItemLaboratoire: FC<ItemLaboratoireProps> = ({ laboratoire, onClick, active, noImage, onSideNavListClick }) => {
  const handleClick = () => {
    onClick(laboratoire.id);
    onSideNavListClick('selected');
  };
  const classes = useStyles();
  return (
    <ListItem
      alignItems="flex-start"
      onClick={handleClick}
      className={classnames(active ? classes.active : '', classes.tabListLabo)}
    >
      <Box>
        <Avatar src={(laboratoire && laboratoire.photo && laboratoire.photo.publicUrl) || noImage} />
      </Box>
      <Box
        display="flex"
        alignItems="flex-start"
        justifyContent="center"
        flexDirection="column"
        style={{ paddingLeft: '20px', minHeight: '69px' }}
      >
        <Typography style={{ fontFamily: 'Roboto', fontSize: 16, fontWeight: 500 }}>{laboratoire.nom}</Typography>
        <Typography className={classes.laboItemInfos}>
          Type :{' '}
          <span>
            {laboratoire.partenaireType ? `Partenaire ${laboratoire.partenaireType?.libelle}` : `Non Partenaire`}
          </span>
        </Typography>
        {laboratoire.rdv && (
          <Typography className={classes.laboItemInfos}>
            Date RDV :{' '}
            <span>
              {laboratoire.rdv?.dateRendezVous && moment(laboratoire.rdv?.dateRendezVous).format('DD/MM/YYYY')}
            </span>
          </Typography>
        )}
      </Box>
    </ListItem>
  );
};

const ListLaboratoiresResult: FC<LaboratoireProps> = ({
  onScroll,
  laboratoires,
  currentLabo,
  updateView,
  onSideNavListClick,
  noImageSrc,
  activeItem,
}) => {
  const classes = useStyles();
  const menuClick = (id: string) => {
    updateView(id);
  };

  const handleContainerOnBottom = () => {
    onScroll();
  };

  const handleGoPilotage = () => {
    updateView('');
    onSideNavListClick('pilotages');
    console.log("NAvigation", onSideNavListClick)
  };

  return (
    <BottomScrollListener onBottom={handleContainerOnBottom}>
      {(scrollRef: any) => (
        <Grid item id="listLaboGrid" ref={scrollRef as any} className={classes.gridScroll}>
          {laboratoires.loading && <Backdrop />}
          {laboratoires.error && 'Une erreur est survenue'}
          {!laboratoires.error && (
            <List className={classes.textList}>
              <ListItem>
                <Box display="flex" flexDirection="row" justifyContent="space-between" width={1}>
                  <Box display="flex" alignItems="start" flexDirection="row" justifyContent="flex-start">
                    <Box>
                      <Avatar className={classes.tousLabosIcon}>
                        <Domain />
                      </Avatar>
                    </Box>
                    <Typography className={classes.tousLabosLabel} style={{ paddingLeft: '35px' }}>
                      Tous les labos
                    </Typography>
                  </Box>
                  <Box alignItems="flex-end" onClick={handleGoPilotage}>
                    <IconButton className={activeItem === 'pilotages' ? classes.active : undefined}>
                      <Equalizer />
                    </IconButton>
                  </Box>
                </Box>
              </ListItem>
            </List>
          )}
          <List>
            {(laboratoires.data || []).map((labo: any) => (
              <ItemLaboratoire
                onSideNavListClick={onSideNavListClick}
                laboratoire={labo}
                key={JSON.stringify(labo)}
                active={currentLabo ? currentLabo.id === labo.id : false}
                onClick={menuClick}
                noImage={noImageSrc}
              />
            ))}
          </List>
          {!laboratoires.data?.length && <Box textAlign="center">Liste vide</Box>}
        </Grid>
      )}
    </BottomScrollListener>
  );
};

export const SideNavListLaboratoire: FC<SideNavListLaboratoireProps> = ({
  openFilterLaboratoire,
  laboratoires,
  currentLabo,
  fetchMore,
  updateLabo,
  onSideNavListClick,
  onRequestChangeFilter,
  onRequestSearchTextChange,
  searchText,
  laboratoireFilter,
  noImageSrc,
  activeItem,
}) => {
  const { isMobile } = useApplicationContext();
  const classes = useStyles();
  const [open, setOpen] = useState<boolean>(false);

  useEffect(() => {
    if (!openFilterLaboratoire) {
      setOpen(false);
    }
  }, [openFilterLaboratoire]);

  const handleSearchTextChange = (value: string) => {
    onRequestSearchTextChange(value);
  };

  const handleOpenFilter = () => {
    setOpen((prev) => !prev);
  };

  return (
    <Box py={2} className={classes.gridListLabo}>
      <Box textAlign="center" py={1} pb={2}>
        <Typography className={classes.bold}>Liste des laboratoires</Typography>
      </Box>
      <Box display="flex" flexDirection="row" justifyContent="space-between" alignItems="center" px={2}>
        <DebouncedSearchInput onChange={handleSearchTextChange} wait={1000} value={searchText} />
        <Box alignItems="flex-end" px={2}>
          <Typography onClick={handleOpenFilter} style={{ cursor: 'pointer' }} className={classes.buttonImgFilter}>
            <FilterIcon />
          </Typography>
          <CustomModal
            open={open}
            setOpen={setOpen}
            title="Filtre laboratoire"
            closeIcon
            withBtnsActions={false}
            headerWithBgColor
            maxWidth="md"
            fullScreen={!!isMobile}
            className={classes.filterLaboModal}
          >
            {laboratoireFilter}
          </CustomModal>
        </Box>
      </Box>
      <Box py={2}>
        <ListLaboratoiresResult
          onScroll={() => fetchMore && fetchMore(10)}
          updateView={updateLabo}
          currentLabo={currentLabo}
          laboratoires={laboratoires}
          onSideNavListClick={onSideNavListClick}
          noImageSrc={noImageSrc}
          activeItem={activeItem}
        />
      </Box>
    </Box>
  );
};
