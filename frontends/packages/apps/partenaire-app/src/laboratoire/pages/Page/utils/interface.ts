import { Laboratoire } from '@lib/common/src/federation';
import { ReactNode } from 'react';

export interface TabActiveLaboComponentProps {
  activeLabo?: Laboratoire;
  noImageSrc: string;
  active: boolean;
  onSideNavListClick?: (item: string) => void;
}

export interface INavigationList {
  tab: string;
  laboratoire?: Laboratoire;
  onClickLaboActive: (id: string) => void;
  onSideNavListClick: (item: string) => void;
}

export interface ItemLaboratoireProps {
  laboratoire: any;
  onClick: (id: string) => void;
  active: boolean;
  noImage: string;
  onSideNavListClick: (item: string) => void;
}

export interface LaboratoireProps {
  onScroll: any;
  laboratoires: {
    data?: any[];
    error?: any;
    loading?: boolean;
  };
  currentLabo?: Laboratoire;
  updateView: (id: string) => void;
  onSideNavListClick: (item: string) => void;
  noImageSrc: string;
  activeItem: string;
}

export interface SideNavListLaboratoireProps {
  openFilterLaboratoire?: boolean;
  laboratoires: {
    data?: any[];
    error?: any;
    loading?: boolean;
  };
  currentLabo?: Laboratoire;
  fetchMore?: (number: number) => void;
  updateLabo: (id: string) => void;
  onSideNavListClick: (item: string) => void;
  onRequestChangeFilter: (data: any) => void;
  onRequestSearchTextChange: (value: string) => void;
  searchText: string;
  filterIcon: any;
  laboratoireFilter: ReactNode;
  noImageSrc: string;
  activeItem: string;
}
