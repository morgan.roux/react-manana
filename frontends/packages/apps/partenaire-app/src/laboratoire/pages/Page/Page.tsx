import { useApplicationContext } from '@lib/common';
import { ParameterContainer } from '@lib/ui-kit/src/components/templates';
import { IconButton } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { useHistory, useLocation, useParams } from 'react-router';
import { FilterIcon, LaboratoireFilter } from './../../components/Filter';
import { useStyles } from './style';
import { SideNavListLaboratoire } from './utils/ListLaboratoire';
import { NavigationList } from './utils/NavigationList';
import {
  useGet_List_Partenariat_TypesLazyQuery,
  useGet_List_Partenariat_StatutsLazyQuery,
  useGet_LaboratoireLazyQuery,
  useSearch_Parametre_LaboratoiresLazyQuery,
} from '@lib/common/src/federation';
import noImageSrc from './../../assets/img/pas_image.png';
import Content from '../Content';
import { TitleMobile } from '../../components/Mobile';
import classnames from 'classnames';
const Page: FC<{}> = ({}) => {
  const classes = useStyles();

  const { isMobile, federation, currentPharmacie } = useApplicationContext();
  const { tab, idLabo } = useParams<{ tab: string; idLabo: string }>();
  const { push } = useHistory();
  const { pathname } = useLocation();

  const [openFilterLaboratoire, setOpenFilterLaboratoire] = useState<boolean>(false);
  const [searchFilter, setSearchFilter] = useState<any>();
  const [filter, setFilter] = useState<any>();
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(10);
  const [searchTxt, setSearchText] = useState<string>('');
  const isCatalogueProduit = pathname.includes('/catalogue');

  const [loadTypePartenariat, loadingTypePartenariat] = useGet_List_Partenariat_TypesLazyQuery({ client: federation });
  const [loadStatutPartenariat, loadingStatutPartenariat] = useGet_List_Partenariat_StatutsLazyQuery({
    client: federation,
  });
  const [loadLaboratoire, loadingLaboratoire] = useGet_LaboratoireLazyQuery({ client: federation });
  const [
    searchLaboratoires,
    { data: listResult, loading, error, fetchMore },
  ] = useSearch_Parametre_LaboratoiresLazyQuery({ fetchPolicy: 'cache-and-network' });

  const must: any[] = [];
  if (searchTxt) {
    must.push({
      query_string: {
        query: `*${searchTxt.replaceAll(' ', '\\ ')}*`,
        analyze_wildcard: true,
        fields: [
          'nom',
          'laboratoireSuite.email',
          'laboratoireSuite.adresse',
          'laboratoireSuite.codePostal',
          'laboratoireSuite.ville',
        ],
      },
    });
  }

  if (searchFilter) {
    const selectedPartenaireFilter: string[] = searchFilter
      .filter(({ keyword }: any) => keyword === 'partenaire')
      .map(({ code }: any) => code);
    const selectedIdTypesFilter: string[] = searchFilter
      ?.filter(({ keyword }: any) => keyword === 'idType')
      .map(({ id }: any) => id);
    const selectedIdStatusFilter: string[] = searchFilter
      ?.filter(({ keyword }: any) => keyword === 'idStatut')
      .map(({ id }: any) => id);

    if (selectedIdTypesFilter.length > 0) {
      must.push({
        terms: {
          couplePharmaciePartenariatTypes: selectedIdTypesFilter.map((idType) => `${currentPharmacie.id}-${idType}`),
        },
      });
    }

    if (selectedIdStatusFilter.length > 0) {
      must.push({
        terms: {
          couplePharmaciePartenariatStatuts: selectedIdStatusFilter.map(
            (idStatus) => `${currentPharmacie.id}-${idStatus}`
          ),
        },
      });
    }
  }

  // if (searchDatePartenariatFilter) {
  //   const dateDebutFilter = searchDatePartenariatFilter.dateDebut || '';
  //   const dateFinFilter = searchDatePartenariatFilter.dateFin || '';
  // }

  const variables = {
    index: ['laboratoires'],
    query: {
      query: {
        bool: {
          must,
        },
      },
      sort: [
        //   { updatedAt: { order: 'desc' } },
        { couplePharmaciePartenariatTypes: { order: 'desc' } },
      ],
    },
    take,
    skip,
  };

  useEffect(() => {
    searchLaboratoires({
      variables,
    });
  }, [skip, take, searchTxt, searchFilter]);

  useEffect(() => {
    if (tab === 'list' || tab === 'selected' || !tab) {
      loadTypePartenariat();
      loadStatutPartenariat();
    }
  }, [tab]);

  useEffect(() => {
    if (idLabo) {
      loadLaboratoire({
        variables: {
          id: idLabo,
          idPharmacie: currentPharmacie.id,
        },
      });
    }
  }, [idLabo]);

  const handleRequestGoBack = () => {
    if (idLabo) {
      push('/laboratoires');
    } else {
      push('/');
    }
  };

  const handleShowFiltersClick = () => {
    setOpenFilterLaboratoire(!openFilterLaboratoire);
  };

  const handleDrawerToggle = () => {
    setOpenFilterLaboratoire(!openFilterLaboratoire);
  };

  const handleCloseFilterLaboratoire = (bool: boolean) => {
    setOpenFilterLaboratoire(bool);
  };

  const handleLaboratoireFilter = (statut: any, type: any, _date: any) => {
    if (!statut) {
      statut = [{ keyword: 'idStatut', id: '' }];
    }
    if (!type) {
      type = [{ keyword: 'idType', id: '' }];
    }
    const filter = statut.concat(type);
    setSearchFilter(filter);
    // setSearchDatePartenariatFilter(date);
  };

  const handleChangeFilter = (data: any) => {
    setFilter(data);
  };

  const handleSearchTextChange = (value: string) => {
    setSearchText(value);
  };

  const handleFetchMore = (count: number) => {
    if (listResult?.search?.data?.length) {
      fetchMore &&
        fetchMore({
          variables: { ...variables, skip: listResult.search.data.length },

          updateQuery: (prev, { fetchMoreResult }) => {
            if (
              prev &&
              prev.search &&
              prev.search.data &&
              fetchMoreResult &&
              fetchMoreResult.search &&
              fetchMoreResult.search.data
            ) {
              const { data: currentData } = prev.search;
              return {
                ...prev,
                search: {
                  ...prev.search,
                  data: [...currentData, ...fetchMoreResult.search.data],
                  total: fetchMoreResult.search.total,
                },
              };
            }

            return prev;
          },
        });
    }
  };

  const handleSearch = (change: any) => {
    const { skip, take, searchText, filter } = change;
    setSkip(skip);
    setTake(take);
    setSearchText(searchText);
    setSearchFilter(filter);
  };

  const handleSideNavListClick = (item: string) => {
    if (item !== 'selected' && item !== 'pilotages') {
      if (item === 'catalogue') {
        push(`/laboratoires/${idLabo}/${item}/card`);
      } else {
        push(`/laboratoires/${idLabo}/${item}`);
      }
    } else if (item === 'pilotages') {
      push('/laboratoires/list/pilotages');
    } /*else if (item === 'selected') {
      push(`/laboratoires/${idLabo}/about`);
    }*/
  };

  const handleClickLaboActive = (laboOrId: any) => {
    if (!laboOrId) {
      // Return to list
      window.history.pushState(null, 'relation labo', `/#/laboratoires/`);
    } else {
      const id = typeof laboOrId === 'string' ? laboOrId : laboOrId.id;

      push(isMobile ? `/laboratoires/${id}/pilotages` : `/laboratoires/${id}/selected`);
    }
  };

  const filterByPharmacieInfos = (data: any[]) => {
    return (data || []).map((laboratoire: any) => {
      const partenaireTypes = laboratoire.pharmacieInfos?.partenariats[currentPharmacie.id];
      const rdvs = laboratoire.pharmacieInfos?.rdvs[currentPharmacie.id];
      const partenaireType = partenaireTypes && partenaireTypes.length && partenaireTypes[0];
      const rdv = rdvs && rdvs.length && rdvs[0];
      return { ...laboratoire, partenaireType, rdv };
    });
  };

  const optionBtn = [
    // eslint-disable-next-line react/jsx-key
    <IconButton
      className={classes.iconAction}
      color="inherit"
      aria-label="settings"
      edge="start"
      onClick={handleShowFiltersClick}
    >
      <FilterIcon />
    </IconButton>,
  ];

  const laboratoireFilter = (
    <LaboratoireFilter
      filterTypePartenariat={loadingTypePartenariat.data?.pRTPartenariatTypes.nodes as any}
      filterStatutPartenariat={loadingStatutPartenariat.data?.pRTPartenariatStatuts.nodes as any}
      onRequestFilter={handleLaboratoireFilter}
      onCloseFilter={handleCloseFilterLaboratoire}
    />
  );

  const sideNavListProps = {
    sideNavList: NavigationList({
      tab,
      laboratoire: loadingLaboratoire?.data?.laboratoire as any,
      onClickLaboActive: handleClickLaboActive,
      onSideNavListClick: handleSideNavListClick,
    })[0],
    onSideNavListClick: handleSideNavListClick,
  };
  const sideNavListLaboProps = {
    sideNavList: NavigationList({
      tab,
      laboratoire: loadingLaboratoire?.data?.laboratoire as any,
      onClickLaboActive: handleClickLaboActive,
      onSideNavListClick: handleSideNavListClick,
    })[0],
    onSideNavListClick: handleSideNavListClick,
    component: (
      <SideNavListLaboratoire
        openFilterLaboratoire={openFilterLaboratoire}
        laboratoires={{
          data: filterByPharmacieInfos(listResult?.search?.data || []),
          error,
          loading,
        }}
        currentLabo={loadingLaboratoire?.data?.laboratoire as any}
        fetchMore={handleFetchMore}
        updateLabo={handleClickLaboActive}
        onSideNavListClick={handleSideNavListClick}
        onRequestChangeFilter={handleChangeFilter}
        onRequestSearchTextChange={handleSearchTextChange}
        searchText={searchTxt}
        filterIcon={FilterIcon}
        laboratoireFilter={laboratoireFilter}
        noImageSrc={noImageSrc}
        activeItem={tab}
      />
    ),
    width: 281,
  };

  const content = (
    <Content
      laboratoires={{
        data: listResult?.search?.data as any,
        error: error as any,
        loading: loading,
        rowsTotal: listResult?.search?.total,
      }}
      onLaboClick={handleClickLaboActive}
      onRequestSearchTextChange={handleSearchTextChange}
      searchText={searchTxt}
      fetchMore={handleFetchMore}
      activeItem={tab}
      onSideNavListClick={handleSideNavListClick}
      onRequestSearch={handleSearch}
      filter={filter}
    />
  );
  return (
    <ParameterContainer
      isMobile={isMobile}
      tab={tab !== 'list' && tab !== 'selected' && tab !== 'pilotages' && tab !== 'plannings' && !!tab}
      sideNavListProps={
        tab === 'list' || tab === 'selected' || tab === 'pilotages' || tab === 'plannings' || !tab
          ? sideNavListLaboProps
          : sideNavListProps
      }
      sideNavProps={{
        title: 'Laboratoires',
        searchPlaceholder: 'Rechercher ...',
        onRequestGoBack: () => handleRequestGoBack(),
        dropdownFilters: [
          {
            title: '',
            children: laboratoireFilter,
            withExpandMore: false,
          },
        ],
      }}
      mainContent={content}
      optionBtn={[optionBtn]}
      handleDrawerToggle={handleDrawerToggle}
      openDrawer={openFilterLaboratoire}
      drawerTitle={
        tab === 'list' || tab === 'pilotages'
          ? 'Les laboratoires'
          : loadingLaboratoire?.data?.laboratoire?.nom || 'Les laboratoires'
      }
      handleSideNavListClick={handleSideNavListClick}
      onGoBack={() => handleRequestGoBack()}
      drawerMdDownClassName={classes.typoFontSideNav}
      tabScrollableClassNameMobile={classes.scrollableClassNameMobile}
      additionnalSubtitle={tab === 'list' || tab === undefined ? <TitleMobile /> : null}
    />
  );
};

export default Page;
