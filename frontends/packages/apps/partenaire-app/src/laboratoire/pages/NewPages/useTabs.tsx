import React, { useEffect, useState } from 'react';
import { LaboTopSection } from '../../../laboratoire/components/LaboTopSection/LaboTopSection';
import { CustomTabs, TabsProps } from '../../../laboratoire/components/LaboCustomContainer/LaboCustomContainer';
import { useScoreLaboratoire } from './../../hooks/useScoreLaboratoire';
import { useLaboratoireParams } from '../../hooks/useLaboratoireParams';
import {
  useGet_List_Plan_Marketing_StatutQuery,
  useGet_List_Plan_Marketing_TypesQuery,
  useGet_List_Suivi_Operationnel_StatutQuery,
  useGet_List_Suivi_Operationnel_TypesQuery,
  useGet_Status_ConditionQuery,
  useGet_Types_ConditionQuery,
} from '@lib/common/src/federation';
import { useApplicationContext, parseParameterValueAsBoolean, findParameter } from '@lib/common';
import { useUpdate_Parameter_ValuesMutation } from '@lib/common/src/graphql';
import { CardSection } from './../../components/LaboTopSection/CardSection';
import { refetchStatsVar } from './../../reactive-vars';
import { useReactiveVar } from '@apollo/client';
const PATH_CODE = {
  contrat: '0880',
  condition: '0881',
  factures: '0882',
  commande: '0883',
  'catalogue/card': '0884',
};

interface ParametreChange {
  code: string;
  value: boolean;
}

interface TabsContextProps {
  refetchPlan: () => void;
  refetchLitige: () => void;
}

const Norefetch = {
  refetchPlan: () => {},
  refetchLitige: () => {},
};

const TabsContext = React.createContext<TabsContextProps>(Norefetch);

// const series = [
//   { categorie: 'Relation Laboratoire', valeur: 0.25, couleur: 'red' },
//   { categorie: 'Information de Liaison', valeur: 0.15, couleur: 'green' },
//   { categorie: 'Ma To-do', valeur: 0.4, couleur: 'blue' },
//   { categorie: 'Autre', valeur: 0.2, couleur: '#888' },
// ];

export const UseTabs = () => {
  const refetchStats = useReactiveVar<boolean>(refetchStatsVar);

  const { planData, litigeData, remunerationData, actionOperationelTypeData, remiseArriereData, refetch } =
    useScoreLaboratoire();
  const { redirectTo, activeTab } = useLaboratoireParams();

  const { parameters, graphql, auth, currentGroupement, currentPharmacie, useParameterValueAsBoolean } =
    useApplicationContext();

  const parametres = [
    { code: '0880', libelle: 'contrat', active: parseParameterValueAsBoolean(parameters, '0880') },
    { code: '0881', libelle: 'Conditions Commerciales', active: parseParameterValueAsBoolean(parameters, '0881') },
    { code: '0882', libelle: 'Historique des Factures', active: parseParameterValueAsBoolean(parameters, '0882') },
    { code: '0883', libelle: 'Historique des Commandes', active: parseParameterValueAsBoolean(parameters, '0883') },
    { code: '0884', libelle: 'Catalogue des Produits', active: parseParameterValueAsBoolean(parameters, '0883') },
  ];

  const initialState = (parametres || []).reduce((accumulator, parametre) => {
    return { ...accumulator, [parametre.code]: parametre.active };
  }, {});

  const [state, setState] = useState<{ [code: string]: boolean }>(initialState);

  console.log('parametres', parametres, state);

  useEffect(() => {
    if (refetchStats) {
      refetchStatsVar(false);
      refetch.refetchPlan();
      refetch.refetchLitige();
    }
  }, [refetchStats]);

  const loadLitigeTypes = useGet_List_Suivi_Operationnel_TypesQuery({
    variables: {
      filter: {
        categorie: {
          eq: 'LITIGE',
        },
      },
    },
  });

  const [updateParameter] = useUpdate_Parameter_ValuesMutation({
    client: graphql,
  });

  const loadLitigeStatuts = useGet_List_Suivi_Operationnel_StatutQuery();

  const loadConditionCommercialeTypes = useGet_Types_ConditionQuery();
  const loadConditionCommercialeStatuts = useGet_Status_ConditionQuery();

  const loadPlanMarketingTypes = useGet_List_Plan_Marketing_TypesQuery({
    variables: {
      idPharmacie: !auth.isSupAdminOrIsGpmAdmin ? currentPharmacie.id : undefined,
      filter: { idGroupement: { eq: currentGroupement.id } },
    },
  });

  const isActiveGenerateTodo = useParameterValueAsBoolean('0887');

  console.log('-----------------IS DESACTIVATION GENERATE TODO: ', isActiveGenerateTodo);

  const planStatuts = useGet_List_Plan_Marketing_StatutQuery().data?.pRTPlanMarketingStatuts?.nodes;

  const listTypePlan = {
    error: loadPlanMarketingTypes.error as any,
    loading: loadPlanMarketingTypes.loading,
    data: !auth.isSupAdminOrIsGpmAdmin
      ? (loadPlanMarketingTypes.data?.pRTPlanMarketingTypes.nodes || []).filter(
          (planMarketingType) => planMarketingType.active === true
        )
      : loadPlanMarketingTypes.data?.pRTPlanMarketingTypes.nodes || [],
  };

  const handleFilter = (name: string, id?: string) => {
    redirectTo({ [name]: id && id !== 'Tous' ? [id] : undefined });
  };

  const handleModeReglementFilter = (id?: string) => {
    redirectTo({ idReglementModeDeclaration: id && id !== 'Tous' ? [id] : undefined });
  };

  const handleTypeDeclarationFilter = (id?: string) => {
    redirectTo({ idReglementType: id && id !== 'Tous' ? [id] : undefined });
  };

  const handleChangeParametre = ({ code, value }: ParametreChange) => {
    if (!value && (PATH_CODE as any)[activeTab] === code) redirectTo(undefined, 'plan');
    setState((prev) => ({ ...prev, [code]: value }));
    updateParameter({
      variables: { parameters: { id: findParameter(parameters, code)?.id as string, value: value ? 'true' : 'false' } },
    });
  };

  const tabParametres: TabsProps[] = [
    {
      tabTitle: 'Contrat',
      active: state[(PATH_CODE as any)['contrat']],
      topBar: (
        <LaboTopSection
          title="Contrat"
          parametres={parametres}
          litigeCardData={litigeData}
          partenariatsCardData={planData}
          remunerationCardData={remunerationData}
          onChangeParametre={handleChangeParametre}
        />
      ),
      // cardMobile: <CardSection litiges={litigeData} partenariats={planData} remuneration={remunerationData} />,
      path: 'contrat',
      params: { take: '12' },
    },

    {
      tabTitle: 'Conditions Commerciales',
      active: state[(PATH_CODE as any)['condition']],
      // cardMobile: <CardSection litiges={litigeData} partenariats={planData} remuneration={remunerationData} />,
      topBar: (
        <LaboTopSection
          title="Conditions Commerciales"
          litigeCardData={litigeData}
          partenariatsCardData={planData}
          remunerationCardData={remunerationData}
          categoryFilter={[
            { id: 'Tous', name: 'Tous' },
            ...(loadConditionCommercialeTypes.data?.pRTConditionCommercialeTypes.nodes || []).map((type) => ({
              ...type,
              name: type.libelle,
            })),
          ]}
          handleCategorieFilter={(id: string) => handleFilter('idType', id)}
          operationFilterName="Statut"
          operationFilter={[
            { id: 'Tous', name: 'Tous' },
            ...(loadConditionCommercialeStatuts.data?.pRTConditionCommercialeStatuts.nodes || []).map((statut) => ({
              ...statut,
              name: statut.libelle,
            })),
          ]}
          handleOperationFilter={(id: string) => handleFilter('idStatut', id)}
          parametres={parametres}
          onChangeParametre={handleChangeParametre}
        />
      ),
      path: 'condition',
      params: { take: '12' },
    },
    {
      tabTitle: 'Historique des Factures',
      active: state[(PATH_CODE as any)['factures']],
      // cardMobile: <CardSection litiges={litigeData} partenariats={planData} remuneration={remunerationData} />,
      topBar: (
        <LaboTopSection
          title="Historique des Factures"
          litigeCardData={litigeData}
          partenariatsCardData={planData}
          remunerationCardData={remunerationData}
          parametres={parametres}
          onChangeParametre={handleChangeParametre}
        />
      ),
      path: 'factures',
      params: { take: '12' },
    },
    {
      tabTitle: 'Historique des Commandes',
      active: state[(PATH_CODE as any)['commande']],
      // cardMobile: <CardSection litiges={litigeData} partenariats={planData} remuneration={remunerationData} />,
      topBar: (
        <LaboTopSection
          title="Historique des Commandes"
          litigeCardData={litigeData}
          partenariatsCardData={planData}
          remunerationCardData={remunerationData}
          parametres={parametres}
          onChangeParametre={handleChangeParametre}
        />
      ),
      path: 'commande',
      params: { take: '12' },
    },
    {
      tabTitle: 'Catalogue des produits',
      active: state[(PATH_CODE as any)['catalogue/card']],
      path: 'catalogue/card',
    },
  ];

  return (
    <CustomTabs
      tab={
        [
          {
            tabTitle: 'Plan Trade',
            active: true,
            cardMobile: (
              <CardSection remiseArriere={remiseArriereData} partenariats={planData} remuneration={remunerationData} />
            ),
            topBar: (
              <LaboTopSection
                title="Plan Trade"
                remiseArriereData={remiseArriereData}
                partenariatsCardData={planData}
                remunerationCardData={remunerationData}
                parametres={parametres}
                onChangeParametre={handleChangeParametre}
                operationFilter={[
                  { id: 'Tous', name: 'Tous' },
                  ...(listTypePlan.data || []).map((type) => {
                    return {
                      id: type.id,
                      name: type.libelle,
                    };
                  }),
                ]}
                operationFilterName={'Opération'}
                handleOperationFilter={(id: string) => handleFilter('idType', id)}
                reglementFilter={{
                  with: true,
                  handleModeReglementFilter,
                  handleTypeDeclarationFilter,
                }}
                categorieFilterName="Statut"
                categoryFilter={[
                  { id: 'Tous', name: 'Tous' },
                  ...(planStatuts || []).map((type) => {
                    return {
                      id: type.id,
                      name: type.libelle,
                    };
                  }),
                ]}
                handleCategorieFilter={(id: string) => handleFilter('idStatut', id)}
              />
            ),
            path: 'plan',
            params: { take: 12 },
          },

          {
            tabTitle: 'Remise Arrière',
            active: true,
            cardMobile: (
              <CardSection remiseArriere={remiseArriereData} partenariats={planData} remuneration={remunerationData} />
            ),
            topBar: (
              <LaboTopSection
                title="Remise Arrière"
                remiseArriereData={remiseArriereData}
                partenariatsCardData={planData}
                remunerationCardData={remunerationData}
                parametres={parametres}
                onChangeParametre={handleChangeParametre}
                operationFilter={[
                  { id: 'Tous', name: 'Tous' },
                  ...(listTypePlan.data || []).map((type) => {
                    return {
                      id: type.id,
                      name: type.libelle,
                    };
                  }),
                ]}
                operationFilterName={'Opération'}
                handleOperationFilter={(id: string) => handleFilter('idType', id)}
                reglementFilter={{
                  with: true,
                  handleModeReglementFilter,
                  handleTypeDeclarationFilter,
                }}
                categorieFilterName="Statut"
                categoryFilter={[
                  { id: 'Tous', name: 'Tous' },
                  ...(planStatuts || []).map((type) => {
                    return {
                      id: type.id,
                      name: type.libelle,
                    };
                  }),
                ]}
                handleCategorieFilter={(id: string) => handleFilter('idStatut', id)}
              />
            ),
            params: { take: 12 },
            path: 'remise',
          },
          {
            tabTitle: 'Gestion Litiges',
            active: true,
            // cardMobile: (
            //   <CardSection
            //     litiges={litigeData}
            //     chartCard={actionOperationelTypeData.length === 0 ? undefined : actionOperationelTypeData}
            //   />
            // ),
            topBar: (
              <LaboTopSection
                title="Gestion Litiges"
                parametres={parametres}
                onChangeParametre={handleChangeParametre}
                litigeCardData={litigeData}
                litigeFilter={[
                  { id: 'Tous', name: 'Tous' },
                  ...(loadLitigeTypes.data?.pRTSuiviOperationnelTypes.nodes || []).map((type) => ({
                    ...type,
                    name: type.libelle,
                  })),
                ]}
                handleLitigeFilter={(id: string) => handleFilter('idType', id)}
                operationFilterName={'Statut'}
                chartCardData={actionOperationelTypeData.length === 0 ? undefined : actionOperationelTypeData}
                operationFilter={[
                  { id: 'Tous', name: 'Tous' },
                  ...(loadLitigeStatuts.data?.pRTSuiviOperationnelStatuts.nodes || []).map((statut) => ({
                    ...statut,
                    name: statut.libelle,
                  })),
                ]}
                handleOperationFilter={(id: string) => handleFilter('idStatut', id)}
              />
            ),
            path: 'litige',
            params: { take: 12 },
          },

          {
            tabTitle: 'Compte Rendu',
            active: true,
            // cardMobile: <CardSection litiges={litigeData} partenariats={planData} remuneration={remunerationData} />,
            topBar: (
              <LaboTopSection
                title="Compte Rendu"
                litigeCardData={litigeData}
                partenariatsCardData={planData}
                remunerationCardData={remunerationData}
                parametres={parametres}
                onChangeParametre={handleChangeParametre}
              />
            ),
            path: 'compte-rendu',
            params: { take: 12 },
          },
          {
            tabTitle: 'Rendez-Vous',
            active: true,
            // cardMobile: <CardSection litiges={litigeData} partenariats={planData} remuneration={remunerationData} />,
            topBar: (
              <LaboTopSection
                title="Rendez-Vous"
                litigeCardData={litigeData}
                partenariatsCardData={planData}
                remunerationCardData={remunerationData}
                parametres={parametres}
                onChangeParametre={handleChangeParametre}
              />
            ),
            path: 'rendez-vous',
          },
          {
            tabTitle: 'To-do Labo',
            active: isActiveGenerateTodo,
            // cardMobile: <CardSection litiges={litigeData} partenariats={planData} remuneration={remunerationData} />,
            topBar: (
              <LaboTopSection
                title="To-do Labo"
                litigeCardData={litigeData}
                partenariatsCardData={planData}
                remunerationCardData={remunerationData}
                parametres={parametres}
                onChangeParametre={handleChangeParametre}
                operationFilterName="Statut"
                operationFilter={[
                  { id: 'Tous', name: 'Tous' },
                  { id: 'ACTIVE', name: 'A faire' },
                  { id: 'DONE', name: 'Teminée' },
                ]}
                handleOperationFilter={(id: string) => handleFilter('todoActionStatus', id)}
              />
            ),
            path: 'to-do',
            params: { todoActionStatus: 'ACTIVE' },
          },
          {
            tabTitle: 'Pilotage',
            active: true,
            path: 'pilotages',
          },

          {
            tabTitle: 'Fiche Labo',
            active: true,
            topBar: (
              <LaboTopSection
                title="Contact"
                litigeCardData={litigeData}
                partenariatsCardData={planData}
                remunerationCardData={remunerationData}
                parametres={parametres}
                onChangeParametre={handleChangeParametre}
              />
            ),
            // cardMobile: <CardSection litiges={litigeData} partenariats={planData} remuneration={remunerationData} />,
            path: 'contact',
          },
          ...tabParametres,
        ].filter((element) => {
          return element.active;
        }) as any
      }
    />
  );
};
