import { Box } from '@material-ui/core';
import React from 'react';
import { UseTabs } from './useTabs';

const NewPagesLabo = () => {
  return (
    <Box>
      <UseTabs />
    </Box>
  );
};

export default NewPagesLabo;
