import { makeStyles, createStyles, Theme } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      height: '100%',
      overflow: 'auto',
      [theme.breakpoints.only('md')]: {
        paddingTop: 50,
      },
      [theme.breakpoints.down('md')]: {
        overflow: 'hidden',
      },
    },

    topBarBox: {
      paddingBottom: 16,
    },
    tableText: {
      fontWeight: 'normal',
      fontSize: 14,
    },
    infinityScroll: {
      height: 'calc(100vh - 100px)',
      width: '100%',
      overflowY: 'scroll',
      scrollbarWidth: 'none',
      '&::-webkit-scrollbar': {
        height: 0,
        width: 0,
      },
    },
    centerBlock: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      height: 'calc(100vh - 45px)',
      textAlign: 'center',
      fontFamily: 'Roboto',
      color: '#4D4D4D',
    },
    titleInformation: {
      fontWeight: 'bold',
      fontSize: 22,
    },
    paragraphInfo: {
      fontSize: 14,
    },
    textList: {
      height: 65,
    },
    tousLabosIcon: {
      border: '2px #bdbdbd solid',
      color: '#bdbdbd',
      background: 'unset',
    },
    tousLabosLabel: {
      paddingLeft: 18,
      paddingTop: 10,
      fontWeight: 'bold',
    },
    listLaboItemMobile: {
      cursor: 'pointer',
    },
  })
);
