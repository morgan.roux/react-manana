import { mergeModules, ModuleDefinition, useApplicationContext } from '@lib/common';
import { Backdrop, DebouncedSearchInput, NoItemContentImage, SmallLoading } from '@lib/ui-kit';
import {
  Avatar,
  Box,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from '@material-ui/core';
import { Domain, Equalizer, MoreHoriz } from '@material-ui/icons';
import React, { FC } from 'react';
import { BottomScrollListener } from 'react-bottom-scroll-listener';
import { useStyles } from './style';
import { MainProps } from './interface';
import { Route, Switch, useHistory, useParams } from 'react-router';
import contactModule from './../../modules/contact/contact.module';
import contratModule from '../../modules/contrat/contrat.module';
import commandeModule from '../../modules/historique-commande/historique-commande.module';
import aboutModule from '../../modules/about/about.module';
import actionOperationnelModule from '../../modules/action-operationnel/action-operationnel.module';
import historiqueFactureModule from '../../modules/historique-facture/historique-facture.module';
import pilotageModule from './../../modules/pilotage/pilotage.module';
import tradeMarketingModule from '../../modules/trade-marketing/trade-marketing.module';
import catalogueProduitModule from '../../modules/catalogue-produit/catalogue-produit.module';
import compteRenduModule from './../../modules/compte-rendu/compte-rendu.module';
import remunerationModule from './../../modules/remuneration/remuneration.module';
import conditionCommercialeModule from '../../modules/condition-commerciale/condition-commerciale.module';
import rendezVousModule from './../../modules/rendezVous/rendez-vous.module';
import TodoModule from './../../modules/todo/todo.module';

const partenaireModule: ModuleDefinition = mergeModules(
  tradeMarketingModule,
  actionOperationnelModule,
  contactModule,
  contratModule,
  commandeModule,
  aboutModule,
  historiqueFactureModule,
  pilotageModule,
  catalogueProduitModule,
  compteRenduModule,
  remunerationModule,
  conditionCommercialeModule,
  rendezVousModule,
  TodoModule
);

const Information: FC<{}> = () => {
  return (
    <NoItemContentImage
      title="Aperçu des laboratoires"
      subtitle="Veuillez cliquez un laboratoire pour voir des informations"
    />
  );
};

const Content: FC<MainProps> = ({
  laboratoires,
  onLaboClick,
  onRequestSearchTextChange,
  searchText,
  fetchMore,
  activeItem,
  //pilotageProps,
  onSideNavListClick,
}) => {
  const classes = useStyles();
  const { push } = useHistory();
  const { isMobile } = useApplicationContext();
  const { tab } = useParams<{ tab: string }>();
  const { data, error, loading } = laboratoires;

  const handleSearchTextChange = (value: string) => {
    onRequestSearchTextChange(value);
  };

  const handleContainerOnBottom = () => {
    fetchMore(10);
  };

  const handleClick = (element: any) => {
    push(`/laboratoires/${element.id}/contact`);
  };

  const handleClickToPilotage = () => {
    onSideNavListClick && onSideNavListClick('pilotages');
  };

  if (isMobile && !tab) {
    return (
      <Box>
        <Box px={2} py={2}>
          <DebouncedSearchInput onChange={handleSearchTextChange} wait={1000} value={searchText} />
        </Box>
        <List className={classes.textList}>
          <ListItem>
            <Box display="flex" flexDirection="row" justifyContent="space-between" width={1}>
              <Box display="flex" alignItems="start" flexDirection="row" justifyContent="flex-start">
                <Box>
                  <Avatar className={classes.tousLabosIcon}>
                    <Domain />
                  </Avatar>
                </Box>
                <Typography className={classes.tousLabosLabel} style={{ paddingLeft: '35px' }}>
                  Tous les labos
                </Typography>
              </Box>
              <Box alignItems="flex-end" onClick={handleClickToPilotage}>
                <IconButton>
                  <Equalizer />
                </IconButton>
              </Box>
            </Box>
          </ListItem>
        </List>
        <BottomScrollListener onBottom={handleContainerOnBottom}>
          {(scrollRef) => (
            <Grid item id="listLaboGrid" className={classes.infinityScroll} ref={scrollRef as any}>
              <List>
                {loading && <Backdrop />}
                {error && 'Une erreur est survenue'}
                {data?.map((el: any) => (
                  <ListItem
                    key={`labo-list-item-${el.id}`}
                    onClick={() => el && handleClick(el)}
                    className={classes.listLaboItemMobile}
                  >
                    <ListItemAvatar>
                      <Avatar alt="Remy Sharp" src={el?.photo?.publicUrl || ''} />
                    </ListItemAvatar>
                    <ListItemText primary={el?.nom} />
                    <MoreHoriz />
                  </ListItem>
                ))}
              </List>
            </Grid>
          )}
        </BottomScrollListener>
      </Box>
    );
  }

  return (
    <Box className={classes.root}>
      <Switch>
        {(partenaireModule.routes || []).map((route) => {
          return <Route path={route.path} component={route.component} exact={route.exact} />;
        })}
        <Route path={['/laboratoires/', '/laboratoires']} component={Information} />
      </Switch>
    </Box>
  );
};

export default Content;
