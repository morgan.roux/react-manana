import { TableChange } from '@lib/ui-kit';
import { ReactNode } from 'react';

export interface MainProps {
  laboratoires: {
    error?: boolean;
    loading?: boolean;
    data?: any[];
    rowsTotal?: number;
  };
  onRequestSearch: (change: TableChange) => void;
  onLaboClick: (labo: any) => void;
  onRequestSearchTextChange: (value: string) => void;
  searchText: string;
  filter: Filter[];
  fetchMore: (count: number) => void;
  activeItem: string;
  // pilotageProps: any; // PilotageProps;
  onSideNavListClick?: (item: string) => void;
}

export interface ItemFilter {
  id: string;
  libelle: string;
  code: string;
  keyword: string;
}

export interface Filter {
  titre: string;
  itemFilters: ItemFilter[];
}
