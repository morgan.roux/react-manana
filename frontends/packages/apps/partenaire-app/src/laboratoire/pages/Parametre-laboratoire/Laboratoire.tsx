import { LABORATOIRE_URL, useApplicationContext, useSubToolbar } from '@lib/common';
import {
  useSearch_Parametre_LaboratoiresQuery,
  useRattacher_LaboratoireMutation,
  LaboratoireInfoFragment,
  useDelete_Laboratoire_RattachementMutation,
} from '@lib/common/src/federation';
import CustomContent, { SearchInput } from '@lib/common/src/components/newCustomContent';
import { BuildQuery } from '@lib/common/src/components/withSearch/queryBuilder';
import React, { FC, useState } from 'react';
import { Helmet } from 'react-helmet';
import { CustomModal } from '@lib/ui-kit';
import useColumns from './useColumns';
import useStyles from './styles';

const Laboratoires: FC = () => {
  const type = 'laboratoires';

  const classes = useStyles({});

  const [selected, setSelected] = useState<any>([]);

  const [openConfirmRattachement, setOpenConfirmRattachement] = useState<boolean>(false);
  const [laboratoire, setLaboratoire] = useState<LaboratoireInfoFragment>();
  const [laboratoireRattachement, setLaboratoireRattachement] = useState<LaboratoireInfoFragment>();

  const [openConfirmDeleteRattachement, setOpenDeleteConfirmRattachement] = useState<boolean>(false);
  const [laboratoireToDeleteRattachement, setLaboratoireToDeleteRattachement] = useState<LaboratoireInfoFragment>();
  const [laboratoireRattachementToDeleteRattachement, setLaboratoireRattachementToDeleteRattachement] =
    useState<LaboratoireInfoFragment>();

  const { federation, notify } = useApplicationContext();

  const must = [{ term: { sortie: 0 } }];

  const { query, skip, take } = BuildQuery({
    type,
    optionalMust: must,
    optionalMustNot: { exists: { field: 'laboratoireRattachement.id' } },
  });

  const variables = { take, skip, index: [type], query };

  const listResult = useSearch_Parametre_LaboratoiresQuery({ client: federation, variables });
  const [rattacherLaboratoire, rattachementLaboratoire] = useRattacher_LaboratoireMutation({
    client: federation,
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'Modification effectuée',
      });

      setLaboratoire(undefined);
      setLaboratoireRattachement(undefined);
      setOpenConfirmRattachement(false);
      listResult.refetch && listResult.refetch();
    },
    onError: () => {
      notify({
        type: 'error',
        message: "Une erreurs s'est produite lors de la modification.",
      });
    },
  });

  const [deleteLaboratoireRattachement, deletingLaboratoireRattachement] = useDelete_Laboratoire_RattachementMutation({
    client: federation,
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'Modification effectuée',
      });

      setLaboratoireToDeleteRattachement(undefined);
      setLaboratoireRattachementToDeleteRattachement(undefined);
      setOpenDeleteConfirmRattachement(false);
      listResult.refetch && listResult.refetch();
    },
    onError: () => {
      notify({
        type: 'error',
        message: "Une erreurs s'est produite lors de la modification.",
      });
    },
  });

  const handleRattachementLaboratoire = (
    laboratoire: LaboratoireInfoFragment,
    laboratoireRattachement: LaboratoireInfoFragment
  ) => {
    setLaboratoire(laboratoire);
    setLaboratoireRattachement(laboratoireRattachement);
    setOpenConfirmRattachement(true);
  };

  const handleDeleteLaboratoireRattachement = (
    laboratoire: LaboratoireInfoFragment,
    laboratoireRattachement: LaboratoireInfoFragment
  ) => {
    setLaboratoireToDeleteRattachement(laboratoire);
    setLaboratoireRattachementToDeleteRattachement(laboratoireRattachement);
    setOpenDeleteConfirmRattachement(true);
  };

  const columns = useColumns({
    onRequestRattachementLaboratoire: handleRattachementLaboratoire,
    onRequestDeleteRattachement: handleDeleteLaboratoireRattachement,
  });

  const listPharmacie = (
    <div className={classes.listTitulaireRoot}>
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher un laboratoire" />
      </div>
      <CustomContent selected={selected} setSelected={setSelected} {...{ listResult, columns }} isSelectable={true} />
    </div>
  );

  const subToolbar = useSubToolbar({
    url: LABORATOIRE_URL,
    title: 'Liste de Laboratoires',
    selected,
    addBtnLabel: 'Ajouter un laboratoire',
  });

  return (
    <div>
      <Helmet>
        <title>Laboratoires</title>
      </Helmet>
      {subToolbar}
      {listPharmacie}

      <CustomModal
        open={!!(laboratoire && laboratoireRattachement && openConfirmRattachement)}
        setOpen={setOpenConfirmRattachement}
        onClickConfirm={() => {
          if (laboratoire && laboratoireRattachement) {
            rattacherLaboratoire({
              variables: {
                idLaboratoire: laboratoire?.id,
                idLaboratoireRattachement: laboratoireRattachement.id,
              },
            });
          }
        }}
        disabledButton={rattachementLaboratoire.loading}
      >
        Êtes-vous sûr de rattacher le laboratoire {laboratoire?.nom} au laboratoire {laboratoireRattachement?.nom} ?
      </CustomModal>

      <CustomModal
        open={!!(laboratoireToDeleteRattachement && openConfirmDeleteRattachement)}
        setOpen={setOpenDeleteConfirmRattachement}
        onClickConfirm={() => {
          if (laboratoireToDeleteRattachement) {
            deleteLaboratoireRattachement({
              variables: {
                id: laboratoireToDeleteRattachement?.id,
              },
            });
          }
        }}
        disabledButton={deletingLaboratoireRattachement.loading}
      >
        Êtes-vous sûr de supprimer le rattachement du laboratoire {laboratoireToDeleteRattachement?.nom} au laboratoire{' '}
        {laboratoireRattachementToDeleteRattachement?.nom} ?
      </CustomModal>
    </div>
  );
};

export default Laboratoires;
