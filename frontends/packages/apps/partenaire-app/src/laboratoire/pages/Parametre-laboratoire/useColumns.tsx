import React from 'react';
import { LABORATOIRE_URL, PartenaireInput } from '@lib/common';
import { LaboratoireInfoFragment } from '@lib/common/src/federation';
import { Chip } from '@material-ui/core';

interface UseColumnsInterface {
  onRequestRattachementLaboratoire: (
    laboratoire: LaboratoireInfoFragment,
    laboratoireRattachement: LaboratoireInfoFragment
  ) => void;

  onRequestDeleteRattachement: (
    laboratoire: LaboratoireInfoFragment,

    laboratoireRattachement: LaboratoireInfoFragment
  ) => void;
}

const useColumns = (params: UseColumnsInterface) => {
  const { onRequestRattachementLaboratoire, onRequestDeleteRattachement } = params;

  return [
    { name: 'nom', label: 'Nom' },
    {
      name: 'laboratoireSuite.email',
      label: 'Email',
      renderer: (value: any) => {
        return value?.laboratoireSuite?.email || '-';
      },
    },
    {
      name: 'laboratoireSuite.codePostal',
      label: 'Code Postal',
      renderer: (value: any) => {
        return value?.laboratoireSuite?.codePostal || '-';
      },
    },
    {
      name: 'laboratoireSuite.ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value?.laboratoireSuite?.ville || '-';
      },
    },
    {
      name: 'laboratoireSuite.adresse',
      label: 'Adresse',
      renderer: (value: any) => {
        return value?.laboratoireSuite?.adresse || '-';
      },
    },
    {
      style: { width: 250 },
      name: 'laboratoireRattachement.nom',
      label: 'Rattacher au laboratoire',
      sortable: false,
      renderer: (value: LaboratoireInfoFragment) => {
        return (
          <PartenaireInput
            index="laboratoire"
            onChange={(rattachementLaboratoire) => {
              if (value.id !== rattachementLaboratoire.id) {
                onRequestRattachementLaboratoire(value, rattachementLaboratoire);
              }
            }}
            value={value.laboratoireRattachement}
            take={20}
            includeRattaches
          />
        );

        //value?.laboratoireRattachement ? value.laboratoireRattachement.nom : '-';
      },
    },

    {
      name: 'laboratoireRattaches.nom',
      label: 'Laboratoires rattachés',
      sortable: false,
      renderer: (value: LaboratoireInfoFragment) => {
        return (value?.laboratoireRattaches?.length || 0) > 0
          ? (value.laboratoireRattaches || []).map((laboratoire) => (
              <Chip
                label={laboratoire.nom}
                onDelete={() => onRequestDeleteRattachement(laboratoire as any, value)}
                variant="outlined"
                style={{
                  margin: 5,
                }}
              />
            ))
          : '-';
      },
    },
  ];
};

export default useColumns;
