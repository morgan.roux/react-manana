import { useApplicationContext } from '@lib/common';

export const useCatalogueOperationMarketing = (): boolean => {
  const { useParameterValueAsBoolean } = useApplicationContext();
  return useParameterValueAsBoolean('0889');
};
