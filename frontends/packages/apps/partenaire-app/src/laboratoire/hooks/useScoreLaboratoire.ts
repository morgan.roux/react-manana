import { useApplicationContext } from '@lib/common';
import {
  useCompute_Plan_By_CategorieLazyQuery,
  useGet_Compute_Montants_RemunerationLazyQuery,
  useGet_Compute_Prt_Action_OperationnelsLazyQuery,
  useGet_Compute_Prt_Action_OperationnElS_By_TypeLazyQuery,
  useGet_Compute_RemunerationsLazyQuery,
  useGet_List_Plan_Marketing_StatutQuery,
} from '@lib/common/src/federation';
import { useEffect } from 'react';
import { useLaboratoireParams } from './useLaboratoireParams';
import moment from 'moment';
import { LazyQueryResult, QueryResult } from '@apollo/client';
import { formatMoney } from '../util';

export const useScoreLaboratoire = () => {
  const { federation, currentPharmacie } = useApplicationContext();
  const {
    params: { idLabo, load },
    redirectTo,
  } = useLaboratoireParams();
  const [computeRemuneration, computingRemuneration] = useGet_Compute_RemunerationsLazyQuery({ client: federation });
  const [computeLitige, computingLitige] = useGet_Compute_Prt_Action_OperationnelsLazyQuery({ client: federation });
  const [computeActionOperationnelByType, computingActionOperationnelByType] =
    useGet_Compute_Prt_Action_OperationnElS_By_TypeLazyQuery({ client: federation });
  const [countPlan, countingPlan] = useCompute_Plan_By_CategorieLazyQuery({ fetchPolicy: 'cache-and-network' });
  const [countPlanCloture, countingPlanCloture] = useCompute_Plan_By_CategorieLazyQuery({
    fetchPolicy: 'cache-and-network',
  });

  const [computeRemiseArriere, computingRemiseArriere] = useGet_Compute_Montants_RemunerationLazyQuery({
    client: federation,
  });

  const [computeOperationTradeMarketing, computingOperationTradeMarketing] =
    useGet_Compute_Montants_RemunerationLazyQuery({
      client: federation,
    });

  const statutPlanCloture = (useGet_List_Plan_Marketing_StatutQuery().data?.pRTPlanMarketingStatuts.nodes || []).find(
    ({ code }) => code === 'CLOTURE'
  );

  const scoreLitige = computingLitige.data?.computeActionOperationnels;
  const scoreRemuneration = computingRemuneration.data?.computeRemunerations;
  const scoreTypeActionOperation = computingActionOperationnelByType.data?.computeActionOperationnelsByType;

  useEffect(() => {
    computeRemuneration({
      variables: {
        input: {
          partenaireType: 'LABORATOIRE',
          date: new Date(),
          idTypeAssocies: idLabo?.length ? idLabo : [],
        },
      },
    });
    computeLitige({
      variables: {
        input: {
          partenaireType: 'LABORATOIRE',
          idPartenaireTypeAssocies: idLabo?.length ? idLabo : [],
          categorie: 'LITIGE',
        },
      },
    });

    computeActionOperationnelByType({
      variables: {
        input: {
          partenaireType: 'LABORATOIRE',
          idPartenaireTypeAssocies: idLabo?.length ? idLabo : [],
          categorie: 'LITIGE',
        },
      },
    });

    computeRemiseArriere({
      variables: {
        input: {
          partenaireType: 'LABORATOIRE',
          idPartenaireTypeAssocies: idLabo?.length ? idLabo : undefined,
          estRemiseArriere: true,
          date: new Date(),
        },
      },
    });

    computeOperationTradeMarketing({
      variables: {
        input: {
          partenaireType: 'LABORATOIRE',
          idPartenaireTypeAssocies: idLabo?.length ? idLabo : undefined,
          estRemiseArriere: false,
          date: new Date(),
        },
      },
    });

    countPlan({
      variables: {
        filter: {
          partenaireType: { eq: 'LABORATOIRE' },
          idPartenaireTypeAssocie: idLabo?.length ? { eq: idLabo[0] } : undefined,
          idPharmacie: {
            eq: currentPharmacie.id,
          },
          dateDebut: {
            between: {
              lower: moment.utc().startOf('year').toISOString(),
              upper: moment.utc().endOf('year').toISOString(),
            },
          },
        },
      },
    });
  }, [JSON.stringify(idLabo)]);

  useEffect(() => {
    if (statutPlanCloture?.id) {
      countPlanCloture({
        variables: {
          filter: {
            partenaireType: { eq: 'LABORATOIRE' },
            idPartenaireTypeAssocie: idLabo?.length ? { eq: idLabo[0] } : undefined,
            idStatut: { eq: statutPlanCloture?.id },
            idPharmacie: {
              eq: currentPharmacie.id,
            },
            dateDebut: {
              between: {
                lower: moment.utc().startOf('year').toISOString(),
                upper: moment.utc().endOf('year').toISOString(),
              },
            },
          },
        },
      });
    }
  }, [JSON.stringify(idLabo), statutPlanCloture]);

  useEffect(() => {
    console.log('---------------------LOAD------------------', load);
    if (load === 'plan') {
      computingRemuneration.refetch && computingRemuneration.refetch();
      countingPlan.refetch && countingPlan.refetch();
      countingPlanCloture.refetch && countingPlanCloture.refetch();
    }
    if (load === 'litige') {
      computingLitige.refetch && computingLitige.refetch();
      computingActionOperationnelByType.refetch && computingActionOperationnelByType.refetch();
    }
    if (load === 'remise') {
      computingRemuneration.refetch && computingRemuneration.refetch();
      countingPlan.refetch && countingPlan.refetch();
      countingPlanCloture.refetch && countingPlanCloture.refetch();
      computingRemiseArriere.refetch && computingRemiseArriere.refetch();
      computingOperationTradeMarketing.refetch && computingOperationTradeMarketing.refetch();
    }
    if (load) {
      redirectTo();
    }
  }, [load]);

  const litigeData = {
    lastYear: scoreLitige?.lastYear.annee,
    currentYear: scoreLitige?.currentYear.annee,
    currentYearMontantPrevu: scoreLitige?.total,
    lastYearNombres1: scoreLitige?.lastYear.enCours,
    currentYearNombres1: scoreLitige?.currentYear.enCours,
    lastYearNombres2: scoreLitige?.lastYear.cloture,
    currentYearNombres2: scoreLitige?.currentYear.cloture,
  };
  const remunerationData = {
    lastYear: scoreRemuneration?.lastYear.annee,
    currentYear: scoreRemuneration?.currentYear.annee,
    currentYearMontantPrevu: `${formatMoney(scoreRemuneration?.currentYear.montantPrevu)} €`,
    lastYearNombres1: `${formatMoney(scoreRemuneration?.lastYear.montantRealise)} €`,
    currentYearNombres1: `${formatMoney(scoreRemuneration?.currentYear.montantRealise)} €`,
    lastYearNombres2: `${formatMoney(scoreRemuneration?.lastYear.montantConfirme)} €`,
    currentYearNombres2: `${formatMoney(scoreRemuneration?.currentYear.montantConfirme)} €`,
  };

  const planData = {
    data: (computingOperationTradeMarketing.data?.computeMontantsRemuneration || []).map((operationTrade) => {
      return {
        titre: operationTrade.prevue.type,
        prevue: `${formatMoney(operationTrade.prevue.montant || 0)} € `,
        realise: `${formatMoney(operationTrade.realise.montant || 0)} € `,
        paye: `${formatMoney(operationTrade.paye.montant || 0)} € `,
      };
    }),
    currentYear: moment.utc().format('YYYY'),
  };

  const actionOperationelTypeData = (scoreTypeActionOperation || []).map((score) => {
    return {
      categorie: score.type.libelle,
      couleur: score.type.couleur || 'blue',
      valeur: score.total === 0 ? 0 : 1 * score.count,
    };
  });

  const remiseArriereData = {
    data: (computingRemiseArriere.data?.computeMontantsRemuneration || []).map((remiseArriere) => {
      return {
        titre: remiseArriere.prevue.type,
        prevue: `${formatMoney(remiseArriere.prevue.montant || 0)} € `,
        realise: `${formatMoney(remiseArriere.realise.montant || 0)} € `,
        paye: `${formatMoney(remiseArriere.paye.montant || 0)} € `,
      };
    }),
    currentYear: moment.utc().format('YYYY'),
  };

  const refetchPlan = () => {
    computingRemuneration.refetch && computingRemuneration.refetch();
    countingPlan.refetch && countingPlan.refetch();
    countingPlanCloture.refetch && countingPlanCloture.refetch();
    computingRemiseArriere.refetch && computingRemiseArriere.refetch();
    computingOperationTradeMarketing.refetch && computingOperationTradeMarketing.refetch();
  };

  const refetchLitige = () => {
    computingLitige.refetch && computingLitige.refetch();
    computingActionOperationnelByType.refetch && computingActionOperationnelByType.refetch();
  };

  return {
    litigeData,
    remunerationData,
    actionOperationelTypeData,
    planData,
    remiseArriereData,
    refetch: {
      refetchPlan,
      refetchLitige,
    },
  };
};
