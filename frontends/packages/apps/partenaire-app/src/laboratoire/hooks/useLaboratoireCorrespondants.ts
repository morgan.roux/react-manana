import { useApplicationContext } from '@lib/common';
import { PrtContactFullInfoFragment, useGet_Correspondants_Prt_PartenariatLazyQuery } from '@lib/common/src/federation';

export const useLaboratoireCorrespondants = (
  code: 'LITIGE_CORRESPONDANT' | 'COMPTERENDU_CORRESPONDANT' | 'RENDEZVOUS_CORRESPONDANT'
): [(idLaboratoire?: string) => void, PrtContactFullInfoFragment[]] => {
  const { federation, currentPharmacie } = useApplicationContext();

  const [loadCorrespondantsPartenariats, loadingCorrespondantsPartenariats] =
    useGet_Correspondants_Prt_PartenariatLazyQuery({
      client: federation,
      fetchPolicy: 'cache-and-network',
    });

  const load = (idLaboratoire?: string) => {
    if (idLaboratoire) {
      loadCorrespondantsPartenariats({
        variables: {
          filter: {
            and: [
              {
                idPartenaireTypeAssocie: {
                  eq: idLaboratoire,
                },
              },
              {
                code: {
                  eq: code,
                },
              },
              {
                partenaireType: {
                  eq: 'LABORATOIRE',
                },
              },
              {
                idPharmacie: {
                  eq: currentPharmacie.id,
                },
              },
            ],
          },
        },
      });
    }
  };

  return [
    load,
    (loadingCorrespondantsPartenariats.data?.pRTPartenariatCorrespondants.nodes || []).map(({ contact }) => contact),
  ];
};
