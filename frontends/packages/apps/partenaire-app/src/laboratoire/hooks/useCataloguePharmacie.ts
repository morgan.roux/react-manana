import { useApplicationContext } from '@lib/common';
import { useProduit_Stock_AggregateLazyQuery } from '@lib/common/src/federation';

export const useCataloguePharmacie = (): [(idLaboratoire?: string) => void, boolean] => {
  const { federation, currentPharmacie } = useApplicationContext();

  const [loadAggregate, loadingAggregate] = useProduit_Stock_AggregateLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  const load = (idLaboratoire?: string) => {
    console.log('***********************************************load aggregate idLaboratoire', idLaboratoire);
    if (idLaboratoire) {
      loadAggregate({
        variables: {
          filter: {
            and: [
              {
                idLaboratoire: {
                  eq: idLaboratoire,
                },
              },
              {
                idPharmacie: {
                  eq: currentPharmacie.id,
                },
              },
            ],
          },
        },
      });
    }
  };

  return [
    load,
    (loadingAggregate.data?.produitStockAggregate.length || 0) > 0
      ? (loadingAggregate.data?.produitStockAggregate[0].count?.id || 0) < 0
      : true,
  ];
};
