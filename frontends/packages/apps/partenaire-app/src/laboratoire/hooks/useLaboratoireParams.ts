import { useURLSearchParams } from '@lib/common';
import { useCallback, useMemo } from 'react';
import { useHistory, useLocation, useParams } from 'react-router';
import { SortDirection, SortNulls } from '@lib/common/src/federation';

const isPrimitive = (value: any): boolean => {
  return value !== Object(value);
};

export interface LaboratoireURLParams {
  id?: string;
  idLabo?: string[];
  idType?: string[];
  idStatut?: string[];
  searchText?: string;
  idFonction?: string[];
  view?: string;
  load?: string;
  idCategorie?: string;
  idSecteur?: string;
  idPrestataireService?: string;
  idReglementModeDeclaration?: string[];
  idReglementType?: string[];
  take?: string;
  skip?: string;
  dateDebut?: string;
  dateFin?: string;
  // TODO
  todoActionStatus?: 'DONE' | 'ACTIVE';
  assignee?: string;
  estRemiseArriere?: boolean;
  sorting?: Array<{ field: any; direction: SortDirection; nulls?: SortNulls }>;
  isAutoDownload?: boolean;
  backParams?: LaboratoireURLParams;
}

export interface UseLaboratoireURLParamsResult {
  params: LaboratoireURLParams;
  redirectTo: (params?: LaboratoireURLParams, tab?: Tab) => void;
  goBack: (tab: Tab, backParams?: LaboratoireURLParams) => void;
  isParamsEmpty: () => boolean;
  activeTab: string;
}
const encodeParam = (param: any, isObjectArray?: boolean): string | undefined => {
  if (Array.isArray(param) && param.length > 0) {
    return isObjectArray
      ? encodeURIComponent(param.map((el) => JSON.stringify(el)).join(','))
      : encodeURIComponent(param.join(','));
  } else if (typeof param === 'string') {
    return encodeURIComponent(param);
  } else if (isPrimitive(param)) {
    return param;
  } else if (param) {
    return encodeURIComponent(JSON.stringify(param));
  }

  return undefined;
};

const decodeParam = (
  params: URLSearchParams,
  name: keyof LaboratoireURLParams,
  isArray: boolean,
  isObjectArray?: boolean
): any | undefined => {
  const value = params.get(name);
  if (value) {
    const decodedValue = decodeURIComponent(value);
    if (isObjectArray) {
      return JSON.parse(`[${decodedValue}]`);
    }
    if (isArray) {
      return decodedValue.split(',');
    }
    return decodedValue;
  }

  return undefined;
};

export type Tab =
  | 'plan'
  | 'plan-form'
  | 'pilotages'
  | 'planning'
  | 'litige'
  | 'rendez-vous'
  | 'ajout-rendez-vous'
  | 'compte-rendu'
  | 'compte-rendu-form'
  | 'compte-rendu-detail'
  | 'edit-compte-rendu'
  | 'to-do'
  | 'remuneration'
  | 'contrat'
  | 'fiche'
  | 'condition'
  | 'contact'
  | 'factures'
  | 'commande'
  | 'catalogue/card'
  | 'detail-action-operationnelle'
  | 'detail-contrat'
  | 'condition-detail'
  | 'facture-detail'
  | 'remise';

const basePath = '/laboratoires';

const extractTabFromLocationPathname = (pathname: string) => {
  if (pathname && pathname.startsWith(basePath)) {
    const omitBasePath = pathname.substr(basePath.length + 1);
    return omitBasePath;
  }

  return undefined;
};

export const useLaboratoireParams = (initTab?: Tab): UseLaboratoireURLParamsResult => {
  const query = useURLSearchParams();
  const history = useHistory();
  const location = useLocation();
  const { tab: tabFromParam } = useParams<{ tab: string }>();

  const params: LaboratoireURLParams = useMemo(
    () => ({
      id: decodeParam(query, 'id', false),
      idLabo: decodeParam(query, 'idLabo', true),
      idType: decodeParam(query, 'idType', true),
      idStatut: decodeParam(query, 'idStatut', true),
      searchText: decodeParam(query, 'searchText', false),
      idFonction: decodeParam(query, 'idFonction', true),
      view: decodeParam(query, 'view', false),
      load: decodeParam(query, 'load', false),
      idCategorie: decodeParam(query, 'idCategorie', false),
      idSecteur: decodeParam(query, 'idSecteur', false),
      idPrestataireService: decodeParam(query, 'idPrestataireService', false),
      todoActionStatus: decodeParam(query, 'todoActionStatus', false),
      estRemiseArriere: decodeParam(query, 'estRemiseArriere', false) === 'true',
      idReglementModeDeclaration: decodeParam(query, 'idReglementModeDeclaration', true),
      idReglementType: decodeParam(query, 'idReglementType', true),
      take: decodeParam(query, 'take', false),
      skip: decodeParam(query, 'skip', false),
      dateDebut: decodeParam(query, 'dateDebut', false),
      dateFin: decodeParam(query, 'dateFin', false),
      sorting: decodeParam(query, 'sorting', true, true),
      backParams: decodeParam(query, 'backParams', false),
      isAutoDownload: decodeParam(query, 'isAutoDownload', false),
      assignee: decodeParam(query, 'assignee', false),
    }),
    [query]
  );

  const redirectTo = useCallback(
    (changedParams?: LaboratoireURLParams, tab?: Tab) => {
      const targetTab = tab || initTab || tabFromParam || extractTabFromLocationPathname(location.pathname);

      const newParams: any = {
        // ...(tab ? params : []),
        idLabo: params.idLabo,
        load: undefined,
        ...(changedParams || {}),
      };

      const objectArrayParams = ['sorting'];

      const newUrl = Object.keys(newParams).reduce((url, paramName: any) => {
        const newValue = newParams[paramName];
        if (!newValue) {
          return url;
        }
        const encodedValue = encodeParam(newValue, objectArrayParams.includes(paramName) ? true : false);
        if (!encodedValue) {
          return url;
        }

        const keyValue = `${paramName}=${encodedValue}`;

        const toAppend = !url.includes('?') ? `?${keyValue}` : `&${keyValue}`;

        return `${url}${toAppend}`;
      }, `${basePath}${targetTab ? (targetTab === 'pilotages' ? `/${targetTab}/PILOTAGES` : `/${targetTab}`) : ``}`);

      history.push(newUrl);
    },
    [params]
  );

  const goBack = useCallback(
    (tab: Tab, changedBackParams?: LaboratoireURLParams) => {
      const targetTab = tab;

      if (params.backParams) {
        const newParams: any = {
          ...JSON.parse(params.backParams as string),
          ...(changedBackParams || {}),
        };

        const objectArrayParams = ['sorting'];

        const newUrl = Object.keys(newParams).reduce((url, paramName: any) => {
          const newValue = newParams[paramName];
          if (!newValue) {
            return url;
          }
          const encodedValue = encodeParam(newValue, objectArrayParams.includes(paramName) ? true : false);
          if (!encodedValue) {
            return url;
          }

          const keyValue = `${paramName}=${encodedValue}`;

          const toAppend = !url.includes('?') ? `?${keyValue}` : `&${keyValue}`;

          return `${url}${toAppend}`;
        }, `${basePath}${targetTab ? (targetTab === 'pilotages' ? `/${targetTab}/PILOTAGES` : `/${targetTab}`) : ``}`);

        history.push(newUrl);
      } else if (history.goBack) {
        history.goBack();
      } else {
        redirectTo(undefined, tab);
      }
    },
    [params]
  );

  const isParamsEmpty = useCallback(() => {
    return Object.keys(params).reduce(
      (previousResult, paramName) => previousResult && !(params as any)[paramName],
      true
    );
  }, [params]);

  return {
    params,
    redirectTo,
    isParamsEmpty,
    goBack,
    activeTab: tabFromParam || extractTabFromLocationPathname(location.pathname) || '',
  };
};
