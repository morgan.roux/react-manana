import { ModuleDefinition, mergeModules } from '@lib/common';
import moment from 'moment';
import laboratoireModule from './laboratoire/laboratoire.module';
import modeleOperationMarketingModule from './modele-operation-marketing/modele-operation-marketing.module';
import prestataireServiceModule from './prestataire-service/prestataire-service.module';
import produitModule from './produit/produit.module';
moment.locale('fr');

const partenaireModule: ModuleDefinition = mergeModules(
  laboratoireModule,
  modeleOperationMarketingModule,
  prestataireServiceModule,
  produitModule
);

export default partenaireModule;
