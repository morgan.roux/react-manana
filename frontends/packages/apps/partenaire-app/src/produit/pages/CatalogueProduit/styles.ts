import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    imgSlider: {
      width: '100%',
      height: 420,
      '&::before': {
        backgroundColor: 'rgba(0, 0, 0, 0.02)',
        content: `''`,
        position: 'absolute',
        height: 'inherit',
        width: '100%',
        zIndex: 1,
      },
    },
    rightContainer: {
      width: '100%',
      height: 'calc(100vh - 86px)',
      overflowY: 'auto',
    },
    fullWidth: {
      width: '100%',
      objectFit: 'cover',
      height: '100%',
    },
    panierListe: {
      flex: 1,
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      padding: '20px',
      '@media (max-width: 599px)': {
        justifyContent: 'center',
      },
      height: 'calc(100vh - 257px)',
      overflowY: 'auto',
    },
    operationListe: {
      flex: 1,
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      padding: '20px',
      '@media (max-width: 599px)': {
        justifyContent: 'center',
      },
      height: 'calc(100vh - 435px)',
      overflowY: 'auto',
    },
    /* FLEX */
    flexColumn: {
      display: 'flex',
      flexDirection: 'column',
    },
    flexRow: {
      display: 'flex',
      flexDirection: 'row',
    },
    inline: {
      display: 'inline',
    },
    rowTypo: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    justifyCenter: {
      justifyContent: 'center',
    },
    alignCenter: {
      alignItems: 'center',
    },
    /* FONT STYLE */
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },
    MontserratBold: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    RobotoRegular: {
      fontFamily: 'Roboto',
      fontWeight: 'normal',
    },
    RobotoBold: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
    },

    underlined: {
      textDecoration: 'underline',
    },
    /*FONT SIZE */
    douze: {
      fontSize: '12px',
    },
    quatorze: {
      fontSize: '14px',
    },
    vingt: {
      fontSize: '20px',
    },
    image: {
      cursor: 'zoom-in',
      maxHeight: 60,
    },
  })
);

export default useStyles;
