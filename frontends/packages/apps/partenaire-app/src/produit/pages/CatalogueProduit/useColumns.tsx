import { Box } from '@material-ui/core';
import { BlockTwoTone, CheckCircle } from '@material-ui/icons';
import React from 'react';
import RemiseCell from '../../components/RemiseCell';
import ArticleButton from '../../components/Article/Button';
import ArticleHistorique from '../../components/Article/Historique/Historique';
import Quantite from '../../components/Article/Quantite/Quantite';

export const catalogueColumns = (operationArticles?: any) => {
  const getQteMin = (produit: any) => {
    const operationArticle =
      operationArticles &&
      operationArticles.find(
        (operationArticle: any) =>
          produit &&
          operationArticle &&
          operationArticle.produitCanal &&
          operationArticle.produitCanal.id === produit.id
      );
    return (operationArticle && operationArticle.quantite) || 0;
  };
  return [
    {
      name: 'produit.produitCode.code',
      label: 'Code',
      renderer: (value: any) => {
        return (value && value.produit && value.produit.produitCode && value.produit.produitCode.code) || '-';
      },
    },
    {
      name: 'produit.libelle',
      label: 'Libellé',
      renderer: (value: any) => {
        return (value && value.produit && value.produit.libelle) || '-';
      },
    },
    {
      name: 'produit.produitTechReg.laboExploitant.nomLabo',
      label: 'Labo',
      renderer: (value: any) => {
        return (
          (value &&
            value.produit &&
            value.produit.produitTechReg &&
            value.produit.produitTechReg.laboExploitant &&
            value.produit.produitTechReg.laboExploitant.nomLabo) ||
          '-'
        );
      },
    },
    {
      name: 'stv',
      label: 'STV',
      editable: true,
      sortable: true,
    },
    {
      name: 'unitePetitCond',
      label: 'Nbr/Carton',
      editable: true,
      renderer: (value: any) => {
        return value?.unitePetitCond || '-';
      },
    },
    {
      name: '',
      label: 'Stock plateforme',
      renderer: (value: any) => {
        if (value.qteStock > 0) {
          return <CheckCircle />;
        } else {
          return <BlockTwoTone color="secondary" />;
        }
      },
      centered: true,
    },
    {
      name: '',
      label: 'Quantité commandée',
      renderer: (value: any) =>
        operationArticles ? (
          <Quantite currentCanalArticle={value ? value : null} qteMin={getQteMin(value)} acceptZero={true} />
        ) : (
          <Quantite currentCanalArticle={value ? value : null} acceptZero={true} />
        ),
      centered: true,
    },
    {
      name: '',
      label: 'Remises',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="remise" />;
      },
      centered: true,
    },
    {
      name: 'prixPhv',
      label: 'Prix Achat',
      editable: true,
      renderer: (value: any) => {
        return value.prixPhv ? value.prixPhv + '€' : '-';
      },
      centered: true,
    },
    {
      name: '',
      label: 'Prix net',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="prixNet" />;
      },
      centered: true,
    },
    {
      name: '',
      label: 'Total net',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="totalNet" />;
      },
      centered: true,
    },
    {
      name: '',
      label: 'Unités gratuites',
      renderer: (value: any) => {
        return <RemiseCell currentCanalArticle={value ? value : null} type="uG" />;
      },
      centered: true,
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <Box display="flex" alignItems="center">
            <ArticleHistorique view="table" produit={value && value.produit} />
            <ArticleButton view="table" currentCanalArticle={value} />
          </Box>
        );
      },
    },
  ];
};

export const gestionColumns = () => {
  return [
    {
      name: 'produitCode.code',
      label: 'Code',
      renderer: (value: any) => {
        return value?.produit?.produitCode?.code || '-';
      },
    },
    {
      name: 'libelle',
      label: 'Libellé',
      renderer: (value: any) => {
        return value?.produit?.libelle || '-';
      },
    },
    {
      name: 'produitTechReg.laboExploitant.nomLabo',
      label: 'Labo',
      renderer: (value: any) => {
        return value?.produit?.produitTechReg?.laboExploitant?.nomLabo || '-';
      },
    },
    {
      name: 'stv',
      label: 'STV',
      editable: true,
      renderer: (value: any) => {
        return value?.stv ? `${value.stv}` : '-';
      },
    },
    {
      name: 'prixPhv',
      label: 'Prix Achat',
      editable: true,
      renderer: (value: any) => {
        return value?.prixPhv ? `${value.prixPhv} €` : '-';
      },
    },
    {
      name: 'unitePetitCond',
      label: 'Nbr/Carton',
      editable: true,
      renderer: (value: any) => {
        return value?.unitePetitCond || '-';
      },
    },
  ];
};

export const laboratoireColums = () => [
  {
    name: 'produit.produitCode.code',
    label: 'Code',
    renderer: (value: any) => {
      return (value && value.produit && value.produit.produitCode && value.produit.produitCode.code) || '-';
    },
  },
  {
    name: 'produit.libelle',
    label: 'Libellé',
    renderer: (value: any) => {
      return (value && value.produit && value.produit.libelle) || '-';
    },
  },
  {
    name: 'produit.produitTechReg.laboExploitant.nomLabo',
    label: 'Labo',
    renderer: (value: any) => {
      return (
        (value &&
          value.produit &&
          value.produit.produitTechReg &&
          value.produit.produitTechReg.laboExploitant &&
          value.produit.produitTechReg.laboExploitant.nomLabo) ||
        '-'
      );
    },
  },
  {
    name: 'stv',
    label: 'STV',
    editable: true,
    sortable: true,
  },
  {
    name: 'unitePetitCond',
    label: 'Nbr/Carton',
    editable: true,
    renderer: (value: any) => {
      return value?.unitePetitCond || '-';
    },
  },
  {
    name: '',
    label: 'Stock plateforme',
    renderer: (value: any) => {
      if (value.qteStock > 0) {
        return <CheckCircle />;
      } else {
        return <BlockTwoTone color="secondary" />;
      }
    },
    centered: true,
  },
  {
    name: 'prixPhv',
    label: 'Prix Achat',
    editable: true,
    renderer: (value: any) => {
      return value?.prixPhv ? `${value.prixPhv} €` : '-';
    },
  },
];
