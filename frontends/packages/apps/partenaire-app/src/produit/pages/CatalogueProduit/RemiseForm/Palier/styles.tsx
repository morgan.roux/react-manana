import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    palierRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      padding: '25px 150px',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '@media (max-width: 1060px)': {
        padding: '25px 30px',
      },
    },
    palierForm: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'space-between',
      margin: '25px 0px',
      '& > div': {
        marginRight: 20,
        marginBottom: 0,
        width: 'auto',
        minWidth: 246,
      },
      '& > button': {
        minWidth: 246,
        height: 50,
      },
      '@media (max-width: 1615px)': {
        justifyContent: 'flex-start',
        '& > div': {
          marginBottom: '20px !important',
        },
      },
      '@media (max-width: 595px)': {
        flexDirection: 'column',
        justifyContent: 'center',
        '& > div': {
          marginRight: 0,
        },
      },
    },
    palierTable: {
      width: '100%',
      minWidth: 1060,
    },
  })
);

export default useStyles;
