import { Comment, CommentList, useApplicationContext, UserAction } from '@lib/common';
import { Panier, useMyPanierQuery, useProduit_CanalQuery, useSmyleysQuery } from '@lib/common/src/graphql';
import { Loader, NewCustomButton } from '@lib/ui-kit';
import { Box, IconButton } from '@material-ui/core';
import { ChevronLeft, ChevronRight } from '@material-ui/icons';
import ChatBubbleIcon from '@material-ui/icons/ChatBubble';
import React, { FC, useEffect, useState } from 'react';
import { useParams } from 'react-router';
import { TarifHTComponent, TooltipContent } from '../../../components/Article/Article';
import ArticleButton from '../../../components/Article/Button';
import ArticleHistorique from '../../../components/Article/Historique/Historique';
import InfoContainer from '../../../components/Article/InfoContainer';
import ArticleListBox from '../../../components/Article/ListBox';
import TooltipComponent from '../../../components/Article/Tooltip/Tooltip';
import CreationProduit from '../../../components/CreationProduit';
import { useStyles } from './styles';

interface ProduitDetailsProps {
  idProduct?: string;
}

const ProduitDetails: FC<ProduitDetailsProps> = ({ idProduct }) => {
  const classes = useStyles({});
  const { id }: any = useParams();
  const { currentPharmacie, graphql, currentGroupement } = useApplicationContext();
  const [openFormModal, setOpenFormModal] = useState(false);
  const [produitComments, setProduitComments] = useState<any>();

  const { data, loading, fetchMore, refetch } = useProduit_CanalQuery({
    client: graphql,
    variables: {
      id: id || idProduct,
      idPharmacie: currentPharmacie ? currentPharmacie.id : '',
      take: 10,
      skip: 0,
    },
    skip: !currentPharmacie ? true : false,
    fetchPolicy: 'cache-and-network',
  });
  const produitCanal = data ? data.produitCanal : null;
  const produitPhoto =
    data &&
    data.produitCanal &&
    data.produitCanal.produit &&
    data.produitCanal.produit.produitPhoto &&
    data.produitCanal.produit.produitPhoto.fichier;

  // take currentPanier
  const [currentPanier, setCurrentPanier] = useState<Panier | null>(null);

  const myPanier = useMyPanierQuery({
    client: graphql,
    variables: {
      idPharmacie: (currentPharmacie && currentPharmacie.id) || '',
      codeCanal: produitCanal && produitCanal.commandeCanal && produitCanal.commandeCanal.code,
      take: 10,
      skip: 0,
    },
    skip: !produitCanal || !currentPharmacie || (currentPharmacie && !currentPharmacie.id) ? true : false,
  });

  useEffect(() => {
    if (myPanier?.data?.myPanier) {
      setCurrentPanier(myPanier?.data?.myPanier as Panier);
    }
  }, [myPanier]);

  const fetchMoreComments = () => {
    if (produitCanal && produitCanal.comments && produitCanal.comments.data) {
      fetchMore({
        variables: { id: id || idProduct, skip: +produitCanal.comments.data.length, take: 10 },
        updateQuery: (prev: any, { fetchMoreResult }: any) => {
          if (!fetchMoreResult) return prev;
          const next = {
            ...prev,
            canalArticle: {
              ...prev.canalArticle,
              comments: {
                ...((prev && prev.canalArticle && prev.canalArticle.comments) || {}),
                total:
                  (prev && prev.canalArticle && prev.canalArticle.comments && prev.canalArticle.comments.total) || 0,
                data: [
                  ...((fetchMoreResult &&
                    fetchMoreResult.canalArticle &&
                    fetchMoreResult.canalArticle.comments &&
                    fetchMoreResult.canalArticle.comments.data) ||
                    []),
                  ...((prev && prev.canalArticle && prev.canalArticle.comments && prev.canalArticle.comments.data) ||
                    []),
                ],
              },
            },
          };

          return next as any;
        },
      });
    }
  };

  const nbSmyley = (produitCanal && produitCanal.userSmyleys && produitCanal.userSmyleys.total) || 0;

  // Get smyleys by groupement
  const getSmyleys = useSmyleysQuery({
    client: graphql,
    variables: {
      idGroupement: currentGroupement.id,
    },
  });

  const smyleys = getSmyleys && getSmyleys.data && getSmyleys.data.smyleys;

  useEffect(() => {
    if (produitCanal && produitCanal.comments) {
      setProduitComments(produitCanal.comments);
    }
  }, [produitCanal]);

  if (loading && !data) return <Loader />;
  // if (!produit) return <div>Aucun produit corespondant à l'id</div>;

  return produitCanal && produitCanal.produit ? (
    <Box className={classes.root}>
      {/* Product Title */}
      {/* Btn Preview, Image, description and Btn Next */}
      <Box className={classes.row}>
        <IconButton className={classes.btnPrevNext} disabled={true}>
          <ChevronLeft />
        </IconButton>
        <Box className={classes.contentDetailsProduits}>
          <Box display="flex" flexDirection="column" marginBottom="24px">
            <TarifHTComponent currentCanalArticle={produitCanal} />
            <TooltipComponent
              titleLabel={produitCanal.produit.libelle || ''}
              famille={
                (produitCanal.produit && produitCanal.produit.famille && produitCanal.produit.famille.libelleFamille) ||
                ''
              }
              component={<TooltipContent data={produitCanal || null} />}
            />
            <InfoContainer canalArticle={produitCanal} column={true} />
            <ArticleHistorique produit={produitCanal && produitCanal.produit} pharmacie={currentPharmacie} />
            <NewCustomButton
              //onClick={() => setOpenUpdateProduit(true)}
              style={{ margin: '15px 0 0 0' }}
            >
              Voir Fiche produit
            </NewCustomButton>
          </Box>
          <Box className={classes.imageAndBtnAddToCartContainer}>
            {produitPhoto && produitPhoto.urlPresigned && <img src={produitPhoto.urlPresigned} alt="" />}
          </Box>
          <Box className={classes.detailsPrix}>
            <ArticleListBox
              currentPanier={currentPanier}
              currentCanalArticle={produitCanal as any}
              currentPharmacie={currentPharmacie as any}
            />
            <Box className={classes.fullWidthButton} marginTop="16px" display="flex" justifyContent="center">
              <ArticleButton currentCanalArticle={produitCanal as any} />
            </Box>
          </Box>
        </Box>

        <IconButton className={classes.btnPrevNext} disabled={true}>
          <ChevronRight />
        </IconButton>
      </Box>
      <Box display="flex" width="100%" paddingLeft="180px" paddingRight="180px" marginTop="64px">
        <hr className={classes.hr} />
      </Box>
      <Box className={classes.userActionSection}>
        {/* Emoji, comment and share btn */}
        <UserAction
          codeItem="PROD"
          idSource={produitCanal.id}
          nbComment={produitCanal.comments ? produitCanal.comments.total : 0}
          nbSmyley={nbSmyley}
          idArticle={produitCanal.id}
          userSmyleys={(produitCanal && produitCanal.userSmyleys) || undefined}
          smyleys={smyleys}
        />

        {/* Comment section*/}
        <Box className={classes.commentSection}>
          {produitComments && (
            <CommentList comments={produitComments} fetchMoreComments={fetchMoreComments} loading={loading} />
          )}
          <Comment codeItem="PROD" idSource={produitCanal.id} refetch={refetch} />
          <Box display="flex" justifyContent="center" alignItems="center" marginTop="16px">
            <ChatBubbleIcon className={classes.panierListeSocialIcon} />
            <Box marginLeft="8px">
              {(produitCanal && produitCanal.comments && produitCanal.comments.total) || 0}{' '}
              {produitCanal && produitCanal.comments && produitCanal.comments.total > 1
                ? 'Commentaires'
                : 'Commentaire'}
            </Box>
          </Box>
        </Box>
      </Box>
      <CreationProduit openFormModal={openFormModal} setOpenFormModal={setOpenFormModal} refetch={null} />
    </Box>
  ) : (
    <div className={classes.root}></div>
  );
};

export default ProduitDetails;
