import {
  Footer,
  PhotoModal,
  produitCanalFilterParam,
  produitCanalSortParam,
  useApplicationContext,
  useDisplayNotification,
  useFilters,
} from '@lib/common';
import CustomContent from '@lib/common/src/components/newCustomContent';
import Filter from '@lib/common/src/components/withSearch/Filter';
import { BuildQuery } from '@lib/common/src/components/withSearch/queryBuilder';
import { SearchQueryVariables, useUpdate_One_Produit_CanalMutation } from '@lib/common/src/federation';
import {
  FullProduitCanalInfoFragment,
  Panier,
  SearchDocument,
  SearchQuery,
  useGestion_Produit_CanalLazyQuery,
  useMyPaniersQuery,
  useOperation_ArticlesQuery,
  useOperation_CommercialeQuery,
  useSearchQuery,
  useSmyleysQuery,
  useUpdateProduitSupprimerMutation,
} from '@lib/common/src/graphql';
import {
  useLocalArticleOCArray,
  useLocalOperationPanier,
  useLocalSelectedOC,
} from '@lib/common/src/shared/operations/operation-commerciale/oc';
import { useLocalCheckedsProduitCanal } from '@lib/common/src/shared/operations/produit-canal';
import { CustomDatePicker, CustomModal, Loader, NewCustomButton, NoItemContentImage } from '@lib/ui-kit';
import { IconButton, Tooltip, Box } from '@material-ui/core';
import { Delete, Edit, StoreMallDirectory } from '@material-ui/icons';
import classNames from 'classnames';
import React, { FC, useEffect, useState } from 'react';
import { useHistory, useLocation, useParams } from 'react-router';
import { useLaboratoireParams } from '../../../laboratoire/hooks/useLaboratoireParams';
import { AppBarFilter } from '../../components/AppBarFilter';
import Article from '../../components/Article/Article';
import CatalogueProduitOcInfo from '../../components/CatalogueProduitOcInfo';
import CreationProduit from '../../components/CreationProduit';
import Pagination from '../../components/Pagination/Pagination';
import useStyles from './styles';
import { catalogueColumns, gestionColumns, laboratoireColums } from './useColumns';
interface ProduitProps {}

const Produit: FC<ProduitProps> = ({}) => {
  const classes = useStyles({});
  const location = useLocation();
  const { pathname } = location;
  console.log('location', location);
  const { idOperation, idRemise, id: idProduit, idLabo: idLaboParams }: any = useParams();
  const { params } = useLaboratoireParams();
  const { idLabo: idLaboratoire } = params;

  const isOnCreate = pathname.includes('/create');
  const currentView = pathname.includes('/card') ? 'card' : 'list';
  const displayNotification = useDisplayNotification();
  const { currentGroupement: groupement, auth, graphql, currentPharmacie, federation } = useApplicationContext();
  const { push } = useHistory();
  const [parentPanier, setParentPanier] = useState<any>(null);
  const isInGestionProduits = pathname.includes('/gestion-produits');
  const isInLaboratoireProduits = pathname.includes('/laboratoires');
  const idLabo = isInLaboratoireProduits ? idLaboratoire : idLaboParams ? [idLaboParams] : undefined;
  const [openPhoto, setOpenPhoto] = useState<boolean>(false);
  const [currentPhoto, setCurrentPhoto] = useState<any>();
  const [openDialog, setOpenDialog] = useState(false);
  const [dateSupprimer, setDateSupprimer] = useState(null);
  const [openFormModal, setOpenFormModal] = useState((isInGestionProduits && idProduit) || isOnCreate ? true : false);
  const [selectedProduits, setSelectedProduits] = useState<any>([]);
  const { pagination, setPagination } = useFilters();
  const take = pagination.data?.pagination.take || 12;
  const skip = pagination.data?.pagination.skip || 0;

  const type = 'produitcanal',
    must = idLabo?.length
      ? [
          {
            terms: {
              'produit.produitTechReg.laboExploitant.id': idLabo,
            },
          },
          { term: { isRemoved: false } },
        ]
      : [{ term: { isRemoved: false } }];

  const { query } = BuildQuery({ type, optionalMust: must });

  const listResult = useSearchQuery({ client: graphql, variables: { type: [type], query, take, skip } });

  // Get smyleys by groupement
  const getSmyleys = useSmyleysQuery({
    client: graphql,
    variables: {
      idGroupement: groupement.id,
    },
  });

  const operationArticlesResult = useOperation_ArticlesQuery({
    client: graphql,
    variables: {
      id: idOperation || '',
      idPharmacie: currentPharmacie.id,
      idRemiseOperation: idRemise,
    },
    skip: !idOperation || !currentPharmacie,
  });

  const operationResult = useOperation_CommercialeQuery({
    client: graphql,
    variables: {
      id: idOperation || '',
      idPharmacie: currentPharmacie.id,
      idRemiseOperation: idRemise,
    },
    skip: !idOperation || !currentPharmacie,
  });

  const operationArticles =
    (operationArticlesResult &&
      operationArticlesResult.data &&
      operationArticlesResult.data.operation &&
      operationArticlesResult.data.operation.operationArticlesWithQte) ||
    [];

  const [loadProduitCanal, loadingProduitCanal] = useGestion_Produit_CanalLazyQuery({
    client: graphql,
    fetchPolicy: 'network-only',
  });

  const [updateProduitCanal, updatingProduitCanal] = useUpdate_One_Produit_CanalMutation({
    client: federation,
    onCompleted: (data) => {
      if (data && data.updateOneProduitCanal) {
        loadProduitCanal({ variables: { id: data.updateOneProduitCanal.id } });
      }
    },
    onError: (error) => {
      displayNotification({
        type: 'error',
        message: 'Erreur lors de la modification du produit',
      });
    },
  });

  const smyleys = getSmyleys && getSmyleys.data && getSmyleys.data.smyleys;

  const zoomImage = (photo: string) => {
    setCurrentPhoto(photo);
    setOpenPhoto(true);
  };

  const [articleToDelete, setArticleToDelete] = useState<any>();

  const handleOpenConfirmDialog = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, article: any) => {
    e.stopPropagation();
    setArticleToDelete(article);
    setOpenDialog(true);
  };

  const [checkedsProduit, setCheckedsProduit] = useLocalCheckedsProduitCanal();

  useEffect(() => {
    setCheckedsProduit(null);
  }, [currentView]);

  const myPanierResult = useMyPaniersQuery({
    client: graphql,
    variables: {
      idPharmacie: currentPharmacie.id,
      take: 10,
      skip: 0,
    },
    fetchPolicy: 'cache-and-network',
    skip: true, //listResult && listResult.data && !listResult.data.search,
  });

  const familleResult = useSearchQuery({
    client: graphql,
    variables: {
      type: ['famille'],
      skip: 0,
      take: 2000,
      idPharmacie: currentPharmacie && currentPharmacie.id,
    },
    fetchPolicy: 'cache-first',
  });

  const handleEdit = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    produitCanal: FullProduitCanalInfoFragment
  ) => {
    e.stopPropagation();
    if (produitCanal.produit?.id) {
      push(`/gestion-produits/edit/${produitCanal.produit?.id}`);
      setOpenFormModal(true);
    }
  };

  const imageColumn = [
    {
      name: '',
      label: 'Photo',
      renderer: (value: any) => {
        return value && value.produitPhoto && value.produitPhoto.fichier && value.produitPhoto.fichier.publicUrl ? (
          <img
            className={classes.image}
            onClick={() => zoomImage(value.produitPhoto.fichier)}
            src={value.produitPhoto.fichier.publicUrl}
          />
        ) : (
          <img className={classes.image} src={''} />
        );
      },
    },
  ];

  const controlColumns = [
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <>
            <Tooltip title="Modifier">
              <IconButton
                onClick={(e) => handleEdit(e, value)}
                color="secondary"
                disabled={!auth.isAuthorizedToEditProduct()}
              >
                <Edit />
              </IconButton>
            </Tooltip>
            <Tooltip title="Supprimer">
              <IconButton
                onClick={(e) => handleOpenConfirmDialog(e, value)}
                disabled={!auth.isAuthorizedToDeleteProduct()}
              >
                <Delete />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  // currentPanier
  const currentPanier: Panier | null =
    ({
      panierLignes:
        (myPanierResult &&
          myPanierResult.data &&
          myPanierResult.data.myPaniers &&
          myPanierResult.data.myPaniers
            .map((panier) => panier && panier.panierLignes)
            .reduce((total, value) => {
              return total && total.concat(value as any);
            }, [])) ||
        [],
    } as Panier) || null;
  // change view mode : card or list

  const changeView = () => {
    if (isInGestionProduits) {
      if (currentView === 'card') {
        push(`/gestion-produits/list`);
      } else {
        push(`/gestion-produits/card`);
      }
    } else if (idOperation) {
      const url = idRemise
        ? `/operation-produits/list/${idOperation}/${idRemise}`
        : `/operation-produits/card/${idOperation}`;
      if (currentView === 'card') {
        push(url);
      } else {
        push(url);
      }
    } else if (isInLaboratoireProduits) {
      if (currentView === 'card') {
        push(`${pathname.replace('card', 'list')}${location.search}`);
      } else {
        push(`${pathname.replace('list', 'card')}${location.search}`);
      }
    } else {
      if (currentView === 'card') {
        push('/catalogue-produits/list');
      } else {
        push('/catalogue-produits/card');
      }
    }
  };

  const onTakeChanged = (value: number) => {
    setPagination({ skip: 0, take: value });
  };

  const onSkipChanged = (value: number) => {
    setPagination({ take, skip: value });
  };

  const getQteMin = (produit: any) => {
    const operationArticle = operationArticles.find(
      (operationArticle) =>
        produit && operationArticle && operationArticle.produitCanal && operationArticle.produitCanal.id === produit.id
    );
    return (operationArticle && operationArticle.quantite) || 0;
  };

  const [articleOCArray, setArticleOCArray] = useLocalArticleOCArray();
  const [selectedOC, setSelectedOC] = useLocalSelectedOC();
  const [operationPanier, setOperationPanier] = useLocalOperationPanier();
  useEffect(() => {
    const list: any = [];
    if (idOperation) {
      setSelectedOC({ id: idOperation, __typename: 'operationId' });
    }
    operationArticles.map((ligne) => {
      if (
        ligne &&
        ligne.quantite &&
        ligne.produitCanal &&
        ligne.quantite > 0 &&
        ligne.produitCanal.qteStock &&
        ligne.produitCanal.qteStock > 0
      ) {
        const article = {
          id: ligne.produitCanal.id,
          quantite: ligne.quantite,
          produitCanal: {
            id: ligne.produitCanal.id,
            prixPhv: ligne.produitCanal.prixPhv,
            remises: ligne.produitCanal.remises,
            __typename: 'remises',
          },
          __typename: 'ArticleOCInterface',
        };

        list.push(article);
      }
    });

    if (!articleOCArray || articleOCArray.length === 0) {
      setArticleOCArray(list);
    }
  }, [operationArticles]);

  useEffect(() => {
    return () => {
      //TODO
      //resetSearchFilters(client);
      setCheckedsProduit(null);
    };
  }, []);

  useEffect(() => {
    if (articleOCArray && articleOCArray.length > 0) {
      const operationPanier = {
        panierLignes: articleOCArray,
        __typename: 'OperationPanierLignes',
      };
      setOperationPanier(operationPanier);
      setParentPanier(operationPanier);
    }
  }, [articleOCArray]);

  useEffect(() => {
    if (articleToDelete && articleToDelete.supprimer) setDateSupprimer(articleToDelete.supprimer);
  }, [articleToDelete]);

  const onChange = (date: any) => {
    setDateSupprimer(date);
  };

  const [doUpdateDateSupprimer, resultProduitDateSupprimer] = useUpdateProduitSupprimerMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (listResult && listResult.variables && data && data.updateProduitSupprimer && data.updateProduitSupprimer.id) {
        const req = cache.readQuery<SearchQuery, SearchQueryVariables>({
          query: SearchDocument,
          variables: listResult.variables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SearchDocument,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.map((item: any) => {
                    if (item && item.id && data.updateProduitSupprimer && data.updateProduitSupprimer.id === item.id) {
                      return {
                        ...item,
                        supprimer: data.updateProduitSupprimer.supprimer,
                      };
                    }

                    return item;
                  }),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onCompleted: () => {
      displayNotification({
        type: 'success',
        message: 'Article supprimé avec succès',
      });
    },
  });

  const handleUpdateDateSupprimer = () => {
    // do modif
    const date: any = dateSupprimer && dateSupprimer !== null ? dateSupprimer : '';
    doUpdateDateSupprimer({
      variables: { id: articleToDelete.id, date },
    });

    setOpenDialog(false);
  };

  const handleCloseDialog = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.stopPropagation();
    setDateSupprimer(null);
    setOpenDialog(false);
  };

  // if (listResult && listResult.data && !listResult.data.search) {
  //   return (
  //     <Box>
  //       <Box>
  //         {
  //           <Box className={classes.panierListe}>
  //             {listResult &&
  //               listResult.data &&
  //               listResult.data.map((item: any | null) => {
  //                 const produit = item && item.article;
  //                 return (
  //                   <Article
  //                     key={produit.id}
  //                     currentPanier={parentPanier}
  //                     currentPharmacie={currentPharmacie}
  //                     currentCanalArticle={produit}
  //                     smyleys={smyleys}
  //                     qteMin={(item && item.quantite) || 0}
  //                   />
  //                 );
  //               })}
  //           </Box>
  //         }
  //       </Box>
  //       <Box className={classNames(classes.flexRow, classes.justifyCenter)}>
  //         <Pagination
  //           take={currentTake || 12}
  //           skip={currentSkip || 0}
  //           onSkipChanged={onSkipChanged}
  //           onTakeChanged={onTakeChanged}
  //           total={(listResult && listResult.total) || 0}
  //         />
  //       </Box>
  //     </Box>
  //   );
  // }

  const columns = isInLaboratoireProduits
    ? laboratoireColums()
    : isInGestionProduits
    ? gestionColumns()
    : catalogueColumns(operationArticles);

  const handleRequestEditProduit = (produit: any) => {
    const stv = produit.stv ? parseInt(produit.stv) : undefined;
    const prixPhv = produit.prixPhv ? parseFloat(parseFloat(produit.prixPhv).toFixed(2)) : undefined;
    const unitePetitCond = produit.unitePetitCond ? parseInt(produit.unitePetitCond) : undefined;
    updateProduitCanal({
      variables: { input: { id: produit.id, update: { stv, prixPhv, unitePetitCond } } },
    });
  };

  const ProduitCanalFilterProps = {
    key: 'produitcanal',
    title: { icon: <StoreMallDirectory />, label: 'Catalogue des produits' },
    sortParams: produitCanalSortParam,
    filterParams: produitCanalFilterParam,
    placeholder: 'Rechercher un produit',
    sortCanal: true,
  };

  return (
    <Box className={classes.root}>
      {!isInLaboratoireProduits && <Filter filterProps={ProduitCanalFilterProps} />}
      <Box width="100%">
        <Box>
          {idOperation && (
            <CatalogueProduitOcInfo
              operation={operationResult && operationResult.data && operationResult.data.operation}
            />
          )}
          <AppBarFilter
            changeView={changeView}
            view={currentView}
            listResult={listResult}
            showGestionProduitsBtns={selectedProduits && selectedProduits.length > 0 ? true : false}
            selected={selectedProduits}
            setSelected={setSelectedProduits}
          />
          {(listResult && !listResult.data) ||
          (listResult && listResult.loading) ||
          (myPanierResult && myPanierResult.loading) ||
          (operationArticlesResult && operationArticlesResult.loading) ? (
            <Loader />
          ) : listResult?.data?.search?.data?.length ? (
            currentView === 'card' ? (
              <Box className={idOperation ? classes.operationListe : classes.panierListe}>
                {(listResult?.data?.search?.data || []).map((produit: any | null) => (
                  <Article
                    key={produit.id}
                    currentPanier={idOperation ? parentPanier : currentPanier}
                    currentPharmacie={currentPharmacie}
                    currentCanalArticle={isInGestionProduits ? produit.produit : produit}
                    smyleys={smyleys}
                    qteMin={idOperation ? getQteMin(produit) : undefined}
                    familleResult={familleResult}
                    disablePanier={isInLaboratoireProduits}
                  />
                ))}
              </Box>
            ) : (
              <CustomContent
                selected={isInGestionProduits ? selectedProduits : undefined}
                setSelected={isInGestionProduits ? setSelectedProduits : undefined}
                listResult={listResult}
                isSelectable={isInGestionProduits}
                columns={isInGestionProduits ? [...imageColumn, ...columns, ...controlColumns] : columns}
                hidePagination={true}
                unResetFilter={true}
                onRequestEdit={isInGestionProduits ? handleRequestEditProduit : undefined}
                mutationLoading={loadingProduitCanal.loading || updatingProduitCanal.loading}
              />
            )
          ) : (
            <NoItemContentImage
              title={'Aucun produit trouvé selon vos critères de recherche'}
              subtitle={"Merci d'essayer d'autres filtres."}
            />
          )}
        </Box>

        <Box className={classNames(classes.flexRow, classes.justifyCenter)}>
          <Pagination
            take={take}
            skip={skip}
            onSkipChanged={onSkipChanged}
            onTakeChanged={onTakeChanged}
            total={listResult && listResult.data && listResult.data.search ? listResult.data.search.total : 0}
          />
        </Box>

        <CustomModal
          open={openDialog}
          setOpen={setOpenDialog}
          title={`Modifier la date de suppression du produit`}
          withBtnsActions={false}
          closeIcon={false}
          headerWithBgColor={true}
          maxWidth={`xl`}
        >
          <Box>
            <Box style={{ margin: '15px 0px 22px 0px' }}>
              <CustomDatePicker
                label="Date suppression"
                placeholder="Date suppression"
                onChange={onChange}
                name="dateSupprimer"
                value={dateSupprimer}
                required={true}
              />
            </Box>
            <Box>
              <NewCustomButton theme="transparent" onClick={handleCloseDialog} style={{ marginRight: '25px' }}>
                Annuler
              </NewCustomButton>
              <NewCustomButton onClick={handleUpdateDateSupprimer} disabled={!dateSupprimer}>
                Modifier
              </NewCustomButton>
            </Box>
          </Box>
        </CustomModal>
        <Footer />
        <PhotoModal
          open={openPhoto}
          setOpen={setOpenPhoto}
          url={(currentPhoto && currentPhoto.publicUrl) || ''}
          libelle={(currentPhoto && currentPhoto.nomOriginal) || 'Image par défaut'}
        />

        <CreationProduit
          openFormModal={openFormModal}
          setOpenFormModal={setOpenFormModal}
          refetch={listResult?.refetch}
        />
      </Box>
    </Box>
  );
};

export default Produit;
