import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      margin: '30px 10%',
      alignItems: 'center',
    },
    productTitle: {
      margin: '0px 0px 20px 0px',
      color: '#004354',
      fontSize: 24,
      fontWeight: 500,
      letterSpacing: 0,
    },
    contentDetailsProduits: {
      display: 'flex',
      width: '100%',
      paddingLeft: '70px',
      paddingRight: '70px',
      justifyContent: 'space-between',
      '@media (max-width: 1200px)': {
        flexWrap: 'wrap',
        justifyContent: 'space-around',
      },
      '@media (max-width: 756px)': {
        paddingLeft: '8px',
        paddingRight: '8px',
      },
    },
    detailsPrix: {
      width: 342,
      '@media (max-width: 1200px)': {
        marginTop: 48,
      },
    },
    row: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      width: '100%',
    },
    productContainer: {
      display: 'flex',
      alignItems: 'center',
      margin: '0px 100px 20px',
      width: '100%',
    },
    imageAndBtnAddToCartContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      maxWidth: 400,
      maxHeight: 320,
      width: '100%',
      margin: '0 16px',
      '& img': {
        width: '100%',
        objectFit: 'contain',
      },
      '@media (max-width: 1024px)': {
        maxWidth: 299,
      },
      '@media (max-width: 600px)': {
        maxWidth: '98%',
      },
    },
    btnAddToCart: {
      textTransform: 'none',
      color: '#fff',
      backgroundColor: '#8CC63F',
      marginTop: 40,
      '&:hover': {
        backgroundColor: '#79aa37',
        borderColor: '#79aa37',
      },
      '&:active': {
        boxShadow: 'none',
        backgroundColor: '#79aa37',
        borderColor: '#79aa37',
      },
    },
    descriptionContainer: {
      width: '100%',
    },
    descriptionTitle: {
      marginBottom: '10px',
      color: '#1D1D1D',
      fontSize: 20,
      letterSpacing: 0,
      fontWeight: 500,
    },
    descriptionText: {
      marginBottom: '10px',
      color: '#5D5D5D',
      letterSpacing: 0,
      fontSize: 20,
    },
    descriptionInfo: {
      marginBottom: '10px',
      color: '#878787',
      letterSpacing: 0,
      fontSize: 16,
    },
    btnPrevNext: {
      height: 52,
      fontSize: 16,
      color: theme.palette.secondary.main,
    },
    fullWidthButton: {
      height: 64,
      '& > *': {
        minWidth: '100% !important',
      },
    },
    userActionSection: {
      width: '100%',
      '& > div': {
        padding: '0px 180px',
      },
    },

    commentSection: {
      width: '100%',
      marginTop: 50,
    },
    hr: {
      width: '100%',
      border: '1px solid #E5E5E5',
    },
    panierListeSocialIcon: {
      height: '20px',
      width: '20px',
      color: '#B1B1B1',
    },
  })
);
