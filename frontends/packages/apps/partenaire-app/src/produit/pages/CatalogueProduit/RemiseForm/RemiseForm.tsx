import { CustomDatePicker, CustomFormTextField, CustomSelect } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Palier, { RemiseDetailInterface } from './Palier/Palier';
import useStyles from './styles';

const TYPE_REMISES = [
  { code: 'LIGNE', libelle: 'Par Palier Quantitatif' },
  { code: 'PANACHEE', libelle: 'Panachée' },
];

export interface RemiseFormValues {
  dateDebut: Date | null;
  dateFin: Date | null;
  nom: string;
  model: 'LIGNE' | 'PANACHEE';
  remiseDetails: RemiseDetailInterface[];
}
interface RemiseFormProps {
  values: RemiseFormValues;
  onChange: (newValues: RemiseFormValues) => void;
}
const RemiseForm: FC<RemiseFormProps & RouteComponentProps<any, any, any>> = ({ values, onChange }) => {
  const classes = useStyles({});

  const { dateDebut, dateFin, nom, model, remiseDetails } = values;

  const handleChange = (e: any) => {
    const { name, value } = e.target;
    onChange({ ...values, [name]: value });
  };

  return (
    <Box>
      <Box className={classes.columnContainer}>
        <CustomFormTextField
          label="Nom"
          name="nom"
          value={nom}
          onChange={handleChange}
          required={true}
          style={{ width: 300 }}
        />

        <CustomSelect
          label="Type de remise"
          list={TYPE_REMISES}
          listId="code"
          index="libelle"
          name="model"
          value={model}
          onChange={handleChange}
          required={false}
          style={{ width: 300 }}
        />
        <Box className={classes.rowContainer}>
          <CustomDatePicker
            label="Date Début"
            placeholder="Date début"
            onChange={(date, value) => handleChange({ target: { name: 'dateDebut', value: date } })}
            value={dateDebut}
            fullWidth={false}
          />
          <CustomDatePicker
            label="Date Fin"
            placeholder="Date Fin"
            onChange={(date, value) => handleChange({ target: { name: 'dateFin', value: date } })}
            value={dateFin}
            fullWidth={false}
          />
        </Box>
      </Box>
      <Box padding="0px 150px">
        <hr />
      </Box>
      <Palier
        state={remiseDetails}
        setState={(newRemiseDetails) => handleChange({ target: { name: 'remiseDetails', value: newRemiseDetails } })}
      />
    </Box>
  );
};

export default withRouter(RemiseForm);
