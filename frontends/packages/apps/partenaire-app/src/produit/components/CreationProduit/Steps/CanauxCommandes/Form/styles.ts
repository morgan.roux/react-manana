import { Theme, createStyles, makeStyles, lighten } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      marginTop: 35,
      '& .MuiFormControl-root': {
        marginBottom: 33,
      },
    },
    container: {
      width: 815,
      maxWidth: 815,
    },
    inlineThreeContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      '& .MuiFormControl-root:not(:nth-child(1))': {
        marginLeft: 27,
      },
    },
    inlineContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      '& > .MuiFormControl-root:nth-child(1)': {
        marginRight: 38,
      },
      '& > .MuiAutocomplete-root:nth-child(1)': {
        marginRight: 38,
      },
    },
    subTitle: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 20,
    },
    articleInfo: {
      background: '#F5F6FA',
      height: 93,
      width: '100%',
      marginTop: '-7px',
      marginBottom: 33,
      padding: 15,
    },
    articleInfoLabel: {
      fontFamily: 'Roboto',
      fontSize: 12,
      width: 48,
    },
    articleInfoValue: {
      fontFamily: 'Montserrat',
      fontSize: 16,
      fontWeight: 600,
    },
  })
);

export default useStyles;
