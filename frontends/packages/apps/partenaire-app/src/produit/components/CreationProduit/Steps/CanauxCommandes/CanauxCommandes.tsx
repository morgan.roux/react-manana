import { useApplicationContext } from '@lib/common';
import { ProduitCanalInput, useSearchQuery } from '@lib/common/src/graphql';
import { Backdrop } from '@lib/ui-kit';
import { Tab, Tabs } from '@material-ui/core';
import React, { FC, useEffect } from 'react';
import { useParams } from 'react-router';
import { CanalArticleInterface } from '../../Interface/CanalArticleInterface';
import Form from './Form';
import useStyles from './styles';

interface CanauxCommandesProps {
  setNextBtnDisabled: (disables: boolean) => void;
  allValues: any;
  setAllValues: (values: any) => void;
  createUpdateProduit: () => void;
}

interface CustomTabPanelInterface {
  index: number;
  currentTab: number;
}

const CanauxCommandes: FC<CanauxCommandesProps> = ({
  setNextBtnDisabled,
  allValues,
  setAllValues,
  createUpdateProduit,
}) => {
  const classes = useStyles({});
  const [tab, setTab] = React.useState(0);
  const { id }: any = useParams();
  const { graphql } = useApplicationContext();

  const commandeCanalResult = useSearchQuery({ client: graphql, variables: { type: ['commandecanal'] } });
  console.log('allValues', allValues);

  useEffect(() => {
    if (
      commandeCanalResult &&
      commandeCanalResult.data &&
      commandeCanalResult.data.search &&
      commandeCanalResult.data.search.data
    ) {
      const produitCanaux: ProduitCanalInput[] = (allValues && allValues.produitCanaux) || [];

      commandeCanalResult.data.search.data.map((commandeCanal: any) => {
        if (
          !produitCanaux.some((canalArticle) => canalArticle && canalArticle.codeCommandeCanal === commandeCanal.code)
        ) {
          console.log('Canal empty ===> ', commandeCanal.code);

          produitCanaux.push({
            codeCommandeCanal: commandeCanal.code,
            isRemoved: true,
            qteMin: 0,
          });
        }
      });

      if (!allValues || (allValues && !allValues.activeCommandeCanal))
        setAllValues((prevState: any) => ({
          ...prevState,
          produitCanaux,
          activeCommandeCanal: commandeCanalResult?.data?.search?.data && commandeCanalResult.data.search.data[0],
        }));

      const actives =
        allValues && allValues.activeCommandeCanal
          ? produitCanaux
          : produitCanaux.filter((produitCanal) => produitCanal.isRemoved === false);

      if (actives.length) {
        const canals =
          (allValues &&
            allValues.activeCommandeCanal &&
            commandeCanalResult.data.search.data.filter(
              (commandeCanal: any) => commandeCanal.code === allValues.activeCommandeCanal.code
            )) ||
          (commandeCanalResult &&
            commandeCanalResult.data &&
            commandeCanalResult.data.search &&
            commandeCanalResult.data.search.data &&
            commandeCanalResult.data.search.data.filter(
              (commandeCanal: any) => commandeCanal.code === actives[0].codeCommandeCanal
            ));

        if (canals && canals.length) {
          const index =
            commandeCanalResult &&
            commandeCanalResult.data &&
            commandeCanalResult.data.search &&
            commandeCanalResult.data.search.data &&
            commandeCanalResult.data.search.data.indexOf(canals[0]);
          setAllValues((prevState: any) => ({
            ...prevState,
            activeCommandeCanal: canals[0],
          }));

          setTab(index);
        }
      } else {
        setNextBtnDisabled(true);
      }
    }
  }, [commandeCanalResult]);

  useEffect(() => {
    if (
      commandeCanalResult &&
      commandeCanalResult.data &&
      commandeCanalResult.data.search &&
      commandeCanalResult.data.search.data &&
      allValues &&
      allValues.produitCanaux &&
      !allValues.produitCanaux.length
    ) {
      const produitCanaux: ProduitCanalInput[] = (allValues && allValues.produitCanaux) || [];

      commandeCanalResult.data.search.data.map((commandeCanal: any) => {
        if (
          !produitCanaux.some((canalArticle) => canalArticle && canalArticle.codeCommandeCanal === commandeCanal.code)
        ) {
          console.log('Canal empty ===> ', commandeCanal.code);

          produitCanaux.push({
            codeCommandeCanal: commandeCanal.code,
            isRemoved: true,
            qteMin: 0,
          });
        }
      });
    }
  }, [commandeCanalResult, allValues]);

  useEffect(() => {
    const produitCanaux: ProduitCanalInput[] = (allValues && allValues.produitCanaux) || [];
    const actives = produitCanaux.filter((produitCanal) => produitCanal.isRemoved === false);
    if (actives.length) {
      setNextBtnDisabled(false);
    }
  }, [allValues]);

  const handleTabsChange = (event: React.ChangeEvent<{}>, newValue: number) => {
    setTab(newValue);
  };

  const handleCanalCommandeChange = (commandeCanal: any) => {
    setAllValues((prevState: any) => ({
      ...prevState,
      activeCommandeCanal: commandeCanal,
    }));
  };

  const currentCanalArticle: CanalArticleInterface & ProduitCanalInput =
    allValues &&
    allValues.produitCanaux &&
    allValues.produitCanaux.find(
      (canalArticle: any) =>
        allValues &&
        allValues.activeCommandeCanal &&
        allValues.activeCommandeCanal.code === canalArticle.codeCommandeCanal
    );

  return (
    <div className={classes.root}>
      {commandeCanalResult.loading && <Backdrop />}
      <div className={classes.container}>
        <Tabs value={tab} onChange={handleTabsChange} centered={true} classes={{ root: classes.tabs }}>
          {commandeCanalResult &&
            commandeCanalResult.data &&
            commandeCanalResult.data.search &&
            commandeCanalResult.data.search.data &&
            commandeCanalResult.data.search.data.map((commandeCanal: any) => (
              <Tab
                key={`commandeCanal-${commandeCanal.id}`}
                classes={{ selected: classes.selectedTab, root: classes.tab }}
                label={commandeCanal.libelle}
                onClick={() => handleCanalCommandeChange(commandeCanal)}
              />
            ))}
        </Tabs>

        <Form
          tab={tab}
          setNextBtnDisabled={setNextBtnDisabled}
          allValues={allValues}
          setAllValues={setAllValues}
          canalArticle={currentCanalArticle}
          disabled={!id}
          createUpdateProduit={createUpdateProduit}
        />
      </div>
    </div>
  );
};

export default CanauxCommandes;
