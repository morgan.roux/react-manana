import { InfoGeneraleInterface } from '../../Interface/InfoGeneraleInterface';

const Validator = (values: InfoGeneraleInterface) => {
  // ! Comment because image is not required
  // switch (values.fichierImage) {
  //   case null: {
  //     return true;
  //   }
  // }
  // switch (values.selectedFiles.length) {
  //   case 0: {
  //     return true;
  //   }
  // }
  switch (values.libelle) {
    case '': {
      return true;
    }
    case null: {
      return true;
    }
    case undefined: {
      return true;
    }
  }
  switch (values.typeCodeReferent) {
    case 0: {
      return true;
    }
    case null: {
      return true;
    }
    case undefined: {
      return true;
    }
  }
  switch (values.codeFamille) {
    case '': {
      return true;
    }
    case null: {
      return true;
    }
    case undefined: {
      return true;
    }
  }
  switch (values.codeReference) {
    case '': {
      return true;
    }
    case null: {
      return true;
    }
    case undefined: {
      return true;
    }
  }
  return false;
};
export default Validator;
