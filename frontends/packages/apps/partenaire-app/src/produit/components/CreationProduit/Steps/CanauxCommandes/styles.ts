import { Theme, createStyles, makeStyles, lighten } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
    },
    container: {
      width: 815,
      maxWidth: 815,
    },
    inlineContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    subTitle: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
    },
    tabs: {
      '& .MuiButtonBase-root:not(:nth-child(1))': {
        marginLeft: 25,
      },
      '& .MuiTabs-indicator': {
        display: 'none',
      },
    },
    tab: {
      borderRadius: 6,
      textTransform: 'none',
      border: '1px solid #9E9E9E',
      color: theme.palette.common.black,
      fontFamily: 'Roboto',
      fontSize: 16,
      fontWeight: 'bolder',
    },
    selectedTab: {
      background: theme.palette.primary.main,
      boxShadow: '0px 3px 6px #00000029',
      color: `${theme.palette.common.white} !important`,
    },
  })
);

export default useStyles;
