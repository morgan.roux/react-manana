import { Theme, lighten } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      height: '100%',
      minHeight: 'calc(100vh - 86px)',
      width: '100%',
      '& .MuiPaper-root': {
        boxShadow: 'none !important',
      },
      '& .MuiStepIcon-root': {
        color: '#EFEFEF',
        '& .MuiStepIcon-text': {
          fill: '#878787',
        },
      },
      '& .MuiStepIcon-root.MuiStepIcon-active, & .MuiStepIcon-root.MuiStepIcon-completed': {
        color: theme.palette.secondary.main,
        '& .MuiStepIcon-text': {
          fill: '#ffffff',
        },
      },
      '& .MuiStepLabel-label, & .MuiStepLabel-label.MuiStepLabel-active': {
        color: '#878787',
        fontSize: 12,
      },
      '& .MuiStepper-root': {
        width: '40%',
        maxWidth: '40%',
      },
      '& button': {
        fontWeight: 'bold',
      },
    },
    fullWidth: {
      '& .MuiStepper-root': {
        width: '100%',
        maxWidth: '100%',
      },
    },
    title: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 20,
    },
    content: {
      margin: '20px 0px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      width: '100%',
    },
    instructions: {
      margin: '10px 0px',
      width: '100%',
      display: 'flex',
      justifyContent: 'center',
    },
    btnActionsContainer: {
      display: 'flex',
      flexDirection: 'row',
      maxWidth: 650,
      justifyContent: 'center',
      '& button:nth-child(1)': {
        marginRight: theme.spacing(3),
      },
    },
    btnCancel: {
      marginTop: 30,
    },
    stepperHeader: {
      position: 'sticky',
      top: 0,
      zIndex: 999,
      width: '100%',
      height: 70,
      display: 'flex',
      padding: '0 24px',
      justifyContent: 'space-between',
      alignItems: 'center',
      color: theme.palette.common.white,
      background: lighten(theme.palette.primary.main, 0.1),
    },
    stepperContainer: {
      height: 'calc(100vh - 295px)',
      overflow: 'auto',
    },
    stepperRoot: {
      // marginTop: 75
      width: '100%',
      '@media (max-width: 825px)': {
        minWidth: '80%',
      },
      '@media (max-width: 450px)': {
        minWidth: '100%',
      },
    },
  })
);

export default useStyles;
