import { useApplicationContext, Stepper, useUploadFiles } from '@lib/common';
import {
  CreateUpdateArticleMutationVariables,
  ProduitCanalInput,
  TypeFichier,
  useCreateUpdateArticleMutation,
  useProduitQuery,
} from '@lib/common/src/graphql';
import { Backdrop, CustomFullScreenModal, Loader } from '@lib/ui-kit';
import React, { FC, Fragment, useState } from 'react';
import { useParams } from 'react-router';
import { CanalArticleInterface } from './Interface/CanalArticleInterface';
import CanauxCommandes from './Steps/CanauxCommandes';
//import Stepper from './stepProduitt';
import InfoGenerale from './Steps/InfoGenerale';
import useStyles from './styles';

interface CreationProduitProps {
  setOpenFormModal: (open: boolean) => void;
  openFormModal: boolean;
  refetch: any;
}

const CreationProduit: FC<CreationProduitProps> = ({ setOpenFormModal, openFormModal, refetch }) => {
  const [nextBtnDisabled, setNextBtnDisabled] = useState<boolean>(true);
  const { id }: any = useParams();
  const { graphql, notify } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();
  const title = id ? 'Modification de produit' : 'Ajout de produit';
  const initialValues = {
    isGenerique: 0,
    unitePetitCond: 0,
    pxVente: 0,
    pxAchat: 0,
    codeFamille: '',
    codeTauxss: 0,
    dateSuppression: null,
    fichierImage: null,
    selectedFiles: [],
  };
  const [values, setValues] = useState<any>(initialValues);

  const initValues = (article: any) => {
    return {
      idProduit: article.id,
      dateSuppression: article.supprimer,
      pxVente: article.produitTechReg?.prixVenteTTC,
      pxAchat: article.produitTechReg?.prixAchatHT,
      libelle: article.libelle,
      codeReference: article.produitCode?.code,
      typeCodeReferent: article.produitCode?.typeCode,
      codeTva: article.produitTechReg?.tva?.codeTva,
      codeLibelleStockage: article.produitTechReg?.libelleStockage?.codeInfo,
      acte: article.produitTechReg?.acte,
      codeActe: article.produitTechReg?.acte?.codeActe,
      codeListe: article.produitTechReg?.liste?.codeListe,
      codeTauxss: article.produitTechReg?.tauxss?.codeTaux,
      idCategorie: article.categorie?.resipIdCategorie,
      laboTitulaire: article.produitTechReg?.laboTitulaire,
      laboExploitant: article.produitTechReg?.laboExploitant,
      laboDistributaire: article.produitTechReg?.laboDistirbuteur,
      idLaboTitulaire: article.produitTechReg?.laboTitulaire?.id,
      idLaboExploitant: article.produitTechReg?.laboExploitant?.id,
      idLaboDistributaire: article.produitTechReg?.laboDistirbuteur?.id,
      famille: article.famille,
      codeFamille: article.famille?.codeFamille,
      fichierImage:
        (article.produitPhoto?.fichier && {
          type: TypeFichier.Photo,
          nomOriginal: article.produitPhoto.fichier.nomOriginal,
          chemin: article.produitPhoto.fichier.chemin,
        }) ||
        null,
      selectedFiles:
        (article.produitPhoto?.fichier && [
          {
            type: 'image/png',
            name: article.produitPhoto.fichier.nomOriginal,
            publicUrl: article.produitPhoto.fichier.publicUrl,
          },
        ]) ||
        [],
      produitCanaux:
        article.canauxArticle &&
        article.canauxArticle.map((canalArticle: any) => ({
          ...canalArticle,
          codeCommandeCanal: canalArticle && canalArticle.commandeCanal && canalArticle.commandeCanal.code,
          gammeCommercial:
            canalArticle && canalArticle.sousGammeCommercial && canalArticle.sousGammeCommercial.gammeCommercial,
          idCanalSousGamme: canalArticle && canalArticle.sousGammeCommercial && canalArticle.sousGammeCommercial.id,
          gammeCatalogue:
            canalArticle && canalArticle.sousGammeCatalogue && canalArticle.sousGammeCatalogue.gammeCatalogue,
          codeSousGammeCatalogue:
            canalArticle && canalArticle.sousGammeCatalogue && canalArticle.sousGammeCatalogue.codeSousGamme,
        })),
    };
  };

  const loadingProduit = useProduitQuery({
    client: graphql,
    variables: {
      id,
    },
    onCompleted: (data) => {
      if (data && data.produit) {
        const values = initValues(data.produit);
        setValues(values);
      }
    },
    skip: !id ? true : false,
    fetchPolicy: 'cache-and-network',
  });

  const getVarialbesValues = (allValues: any) => {
    let variables = allValues;
    delete variables.resetChange;
    delete variables.acte;
    delete variables.famille;
    delete variables.gammeCatalogue;
    delete variables.gammeCommercial;
    delete variables.laboratoire;
    delete variables.selectedFiles;
    delete variables.sousGammeCatalogue;
    delete variables.sousGammeCommercial;
    delete variables.activeCommandeCanal;
    delete variables.laboDistributaire;
    delete variables.laboExploitant;
    delete variables.laboTitulaire;
    const produitCanaux =
      variables &&
      variables.produitCanaux &&
      variables.produitCanaux.map((canalArticle: ProduitCanalInput & CanalArticleInterface) => {
        const variables: any = canalArticle;
        delete variables.laboratoire;
        delete variables.gammeCommercial;
        delete variables.sousGammeCommercial;
        delete variables.gammeCatalogue;
        delete variables.sousGammeCatalogue;
        delete variables.commandeCanal;
        delete variables.__typename;
        return variables;
      });

    variables = { ...variables, produitCanaux };
    return variables as CreateUpdateArticleMutationVariables;
  };

  const [upsertProduit, upsertingProduit] = useCreateUpdateArticleMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createUpdateProduit) {
        notify({
          type: 'success',
          message: `Produit ${id ? 'modifié' : 'créée'} avec succès`,
        });
        refetch && refetch();
        setOpenFormModal(false);

        // // articleResult.refetch();
        // if (id) {
        //   openCloseModal(false);
        // }

        // setIsFinale(true);
        // if (isEdit) setIsEdit(false);
        // if (setIsAfterCreateUpdate) setIsAfterCreateUpdate(true);
      }
    },
    onError: (errors) => {
      notify({
        type: 'error',
        message: errors.message,
      });
    },
  });

  const createUpdateProduit = () => {
    const fichierImage = values.selectedFiles && values.selectedFiles[0];
    if (fichierImage && !fichierImage.publicUrl) {
      uploadFiles([fichierImage], {
        directory: 'produits',
      })
        .then((uploadedFiles) => {
          const variables = getVarialbesValues({ ...values, fichierImage: uploadedFiles[0] });
          return upsertProduit({ variables });
        })
        .catch((error) => {
          notify({
            type: 'error',
            message: error.message,
          });
        });
    } else {
      const variables = getVarialbesValues({ ...values });
      upsertProduit({ variables });
    }
  };

  const steps = [
    {
      title: 'Données économiques',
      content: <InfoGenerale setNextBtnDisabled={setNextBtnDisabled} allValues={values} setAllValues={setValues} />,
    },
    {
      title: 'Canaux de commandes',
      content: (
        <CanauxCommandes
          setNextBtnDisabled={setNextBtnDisabled}
          allValues={values}
          setAllValues={setValues}
          createUpdateProduit={createUpdateProduit}
        />
      ),
    },
  ];

  const finalStep = {
    buttonLabel: id ? 'MODIFIER PRODUIT' : 'AJOUTER PRODUIT',
    action: createUpdateProduit,
  };

  const handleBackBtnClick = () => {
    setOpenFormModal(false);
  };

  const isLoading = (): boolean => {
    return upsertingProduit.loading || !!uploadingFiles.loading;
  };

  if (loadingProduit.loading) return <Loader />;

  return (
    <Fragment>
      {isLoading() && <Backdrop value={`${id ? 'Modification' : 'Ajout'} de l'article en cours ...`} />}
      <CustomFullScreenModal open={openFormModal} setOpen={setOpenFormModal} title={`Produit`} withBtnsActions={false}>
        {/* <Stepper
        title={title}
        steps={steps}
        backToHome={handleBackBtnClick}
        disableNextBtn={nextBtnDisabled}
        fullWidth={false}
        finalStep={finalStep}
        currentStep={currentStep}
        setCurrentStep={setCurrentStep}
        setResetChange={setResetChange}
        isFinale={isFinale}
        setIsFinale={setIsFinale}
        setAllValues={setAllValues}
      /> */}
        <Stepper
          title={title}
          steps={steps}
          backToHome={handleBackBtnClick}
          disableNextBtn={nextBtnDisabled}
          finalStep={finalStep}
        />
      </CustomFullScreenModal>
    </Fragment>
  );
};

export default CreationProduit;
