import { Theme, createStyles, makeStyles, lighten } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => createStyles({}));

export default useStyles;
