import React, { FC, useEffect } from 'react';
import useStyles from './styles';
import Validator from './Validator';
interface ChoixFamilleProps {
  setNextBtnDisabled: (disables: boolean) => void;
  allValues: any;
  setAllValues: (values: any) => void;
  disabled?: boolean;
}

const ChoixFamille: FC<ChoixFamilleProps> = ({ setNextBtnDisabled, allValues, setAllValues, disabled }) => {
  const classes = useStyles({});

  useEffect(() => {
    if (allValues) {
      setNextBtnDisabled(Validator(allValues));
    }
  }, [allValues]);

  return (
    <ItemSelection
      itemLabel="Choix de famille"
      setNextBtnDisabled={setNextBtnDisabled}
      allValues={allValues}
      setAllValues={setAllValues}
      type="famille"
      nodeKey="codeFamille"
      nodeLabel="libelleFamille"
      nodeParent="parent"
      multiple={false}
      allValuesKey="codeFamille"
      itemKey="famille"
      disabled={disabled}
    />
  );
};

export default ChoixFamille;
