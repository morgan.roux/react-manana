import { Theme, createStyles, makeStyles, lighten } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
    },
    container: {
      width: 815,
      maxWidth: 815,
      '& .MuiFormControl-root': {
        marginBottom: 33,
      },
    },
    inlineContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      '& > .MuiFormControl-root:nth-child(1), & > .MuiAutocomplete-root:nth-child(1)': {
        marginRight: 38,
      },
    },
    inlineCheckboxContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginTop: '-18px',
      marginBottom: '18px',
    },
    typeCodeReferentContainer: {
      display: 'flex',
      '& > div': {
        margin: 0,
      },
      minWidth: 210,
      height: 51,
      marginRight: 25,
    },
    codeCanalCommandeContainer: {
      display: 'flex',
      alignItems: 'center',
      '& > div': {
        margin: 0,
      },
      minWidth: 150,
    },
    codeReferenceContainer: {
      display: 'flex',
      '& > div': {
        margin: 0,
      },
      height: 51,
      marginRight: 25,
      minWidth: 210,
    },
    remboursementContainer: {
      display: 'flex',
      alignItems: 'center',
      minWidth: 140,
      '& > div': {
        margin: 0,
      },
      marginRight: theme.spacing(3),
    },
    formPhotoDropzone: {
      '& > div > div': {
        margin: 0,
      },
      marginBottom: 25,
    },
    subTitle: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 25,
    },
    checkboxContainer: {
      columnCount: 3,
      width: '100%',
    },
    familleExpander: {
      display: 'flex',
      width: '100%',
      alignItems: 'center',
      color: '#616161',
      fontFamily: 'Roboto',
      fontSize: 16,
      marginTop: '-12px',
    },
    familleDivider: {
      width: '100%',
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
    content: {
      display: 'flex',
      justifyContent: 'center',
    },
    contained: {
      display: 'flex',
      flexDirection: 'column',
    },
  })
);

export default useStyles;
