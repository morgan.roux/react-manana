import React, { FC, useEffect, useState, Dispatch, SetStateAction, ReactNode, Fragment } from 'react';
import useStyles from './styles_steper';
import { Stepper as MUIStepper, StepLabel, Step as MUIStep, Typography, Box, IconButton } from '@material-ui/core';
import classnames from 'classnames';
import { useQuery, useApolloClient } from '@apollo/client';
import { GET_SHOW_SUB_SIDEBAR } from '../../../../graphql/Actualite/local';
import { ArrowBack } from '@material-ui/icons';
import { NewCustomButton } from '@lib/ui-kit';

export interface Step {
  title: string;
  content: JSX.Element;
  pageTitle?: string;
}

export interface StepperProps {
  title: string;
  steps: Step[];
  disableNextBtn: boolean;
  isEdit?: boolean;
  finalStep?: any;
  children?: ReactNode;
  backToHome?: () => void;
  setEdit: (edit: boolean) => any;
  isCreate?: boolean;
  fullWidth?: boolean;
  currentStep: number;
  setCurrentStep: (step: number) => any;
  setResetChange: (edit: boolean) => any;
  isFinale: boolean;
  setIsFinale: (finale: boolean) => any;
  setAllValues: (values: any) => void;
}

const Stepper: FC<StepperProps> = ({
  steps,
  backToHome,
  disableNextBtn,
  isEdit,
  setEdit,
  isCreate,
  title,
  finalStep,
  children,
  fullWidth,
  currentStep,
  setCurrentStep,
  setResetChange,
  isFinale,
  setIsFinale,
  setAllValues,
}) => {
  const classes = useStyles({});
  const [activeStep, setActiveStep] = useState(0);
  const getShowSubSidebar = useQuery<{ showSubSidebar: boolean }>(GET_SHOW_SUB_SIDEBAR);

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    if (activeStep > 0) {
      if (isEdit)
        setAllValues((prevState) => ({
          ...prevState,
          activeCommandeCanal: null,
        }));
      setActiveStep((prevActiveStep) => prevActiveStep - 1);
    }
  };

  useEffect(() => {
    setActiveStep(0);
  }, []);

  useEffect(() => {
    setCurrentStep(activeStep);
  }, [activeStep]);

  useEffect(() => {
    if (isFinale) {
      setActiveStep(currentStep);
    }
  }, [isFinale]);

  return (
    <div className={fullWidth ? classnames(classes.root, classes.fullWidth) : classes.root}>
      <Box className={classes.stepperHeader}>
        <Box display="flex" alignItems="center">
          <IconButton color="inherit" onClick={backToHome}>
            <ArrowBack />
          </IconButton>
        </Box>
        <Typography className={classes.title}>{title}</Typography>
        {isCreate ? (
          <div>
            <NewCustomButton
              style={{ marginRight: '15px' }}
              onClick={finalStep && activeStep === steps.length - 1 ? handleBack : handleNext}
              disabled={
                finalStep && activeStep === steps.length - 1
                  ? false
                  : (isEdit && disableNextBtn) || (isCreate && disableNextBtn)
              }
            >
              {!isEdit && finalStep && activeStep === steps.length - 1 ? 'Retour' : 'Suivant'}
            </NewCustomButton>{' '}
            <NewCustomButton
              onClick={finalStep && finalStep.action}
              disabled={(finalStep && finalStep.loading) || disableNextBtn}
            >
              {finalStep && finalStep.buttonLabel}
            </NewCustomButton>
          </div>
        ) : isEdit ? (
          <div>
            <NewCustomButton
              style={{ marginRight: '15px' }}
              onClick={() => {
                setEdit(false)();
                setResetChange(true);
              }}
            >
              {'Annuler'}
            </NewCustomButton>{' '}
            <NewCustomButton
              onClick={finalStep && finalStep.action}
              disabled={(finalStep && finalStep.loading) || disableNextBtn}
            >
              {finalStep && finalStep.buttonLabel}
            </NewCustomButton>
          </div>
        ) : (
          <div>
            <NewCustomButton
              style={{ marginRight: '15px' }}
              onClick={finalStep && activeStep === steps.length - 1 ? handleBack : handleNext}
              disabled={finalStep && activeStep === steps.length - 1 ? false : isEdit && disableNextBtn}
            >
              {!isEdit && finalStep && activeStep === steps.length - 1 ? 'Retour' : 'Suivant'}
            </NewCustomButton>{' '}
            <NewCustomButton onClick={setEdit(true)}>{'Modifier'}</NewCustomButton>
          </div>
        )}
      </Box>
      <MUIStepper activeStep={activeStep} alternativeLabel={true} className={classes.stepperRoot}>
        {steps.map((step) => (
          <MUIStep key={step.title}>
            <StepLabel>{step.title}</StepLabel>
          </MUIStep>
        ))}
      </MUIStepper>
      {children}
      <Box width="100%" className={classes.stepperContainer}>
        <div className={classes.content}>
          <div className={classes.instructions}>{steps && steps.length > 0 && steps[activeStep].content}</div>
        </div>
      </Box>
    </div>
  );
};

// activeStep === steps.length - 1

export default Stepper;
