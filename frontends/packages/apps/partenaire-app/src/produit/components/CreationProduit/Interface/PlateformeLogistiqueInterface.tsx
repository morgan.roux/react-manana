export interface PlateformeLogistiqueInterface {
  unitePetitCond: number;
  qteStock: number;
  stv: number;
  pxVenteArticle: number;
  pxAchatArticle: number;
  datePeremption: any;
  dateCreation: any;
  dateModification: any;
  gammeCommercial: any;
  sousGammeCommercial: any;
  idCanalSousGamme: string;
  gammeCatalogue: any;
  sousGammeCatalogue: any;
  codeSousGammeCatalogue: string;
}
