import React, { FC, useState, useEffect } from 'react';
import { useStyles } from '../styles';
import { IconButton, CircularProgress, InputBase, Box, Typography } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import RemoveIcon from '@material-ui/icons/Remove';
import {
  MyPaniersDocument,
  MyPaniersQuery,
  MyPaniersQueryVariables,
  Panier,
  PanierLigne,
  ProduitCanal,
  useAddToPanierMutation,
  useMyPaniersQuery,
  useUpdate_QuantiteMutation,
} from '@lib/common/src/graphql';
import { gql } from '@apollo/client';
import { useLocation, useParams } from 'react-router';
import {
  ADMINISTRATEUR_GROUPEMENT,
  COLLABORATEUR_COMMERCIAL,
  COLLABORATEUR_NON_COMMERCIAL,
  GROUPEMENT_AUTRE,
  useApplicationContext,
} from '@lib/common';
import {
  useLocalArticleOCArray,
  useLocalOperationPanier,
} from '@lib/common/src/shared/operations/operation-commerciale/oc';
import classnames from 'classnames';
import { Save } from '@material-ui/icons';

interface QuantiteProps {
  currentCanalArticle: ProduitCanal | null;
  // currentPharmacie: Pharmacie | null;
  // currentPanier: Panier | null;
  acceptZero?: boolean;
  handleParentQuantite?: (article: any, qte: number) => void;
  parentQuantite?: number;
  qteMin?: number;
}

export interface ArticleOC {
  id: string;
  quantite: number;
  article: {
    id: string;
    prixPhv: number;
    remises: {
      id: string;
      quantiteMin: number;
      quantiteMax: number;
      pourcentageRemise: number;
      nombreUg: number;
      codeCipUg: number;
      codeCip13Ug: string;
    };
  };
}

export interface ArticleOCArray {
  articleOcArray: ArticleOC[];
}
export const GET_ARTICLE_OC_ARRAY = gql`
  {
    articleOcArray @client {
      id
      quantite
      produitCanal {
        id
        prixPhv
        remises {
          id
          nombreUg
          pourcentageRemise
          quantiteMax
          quantiteMin
        }
      }
    }
  }
`;

const Quantite: FC<QuantiteProps> = ({
  currentCanalArticle,
  acceptZero,
  handleParentQuantite,
  parentQuantite,
  qteMin,
}) => {
  const classes = useStyles({});
  const { idOperation }: any = useParams();
  const { state }: any = useLocation();
  const { user, currentPharmacie, graphql } = useApplicationContext();

  const myPanierResult = useMyPaniersQuery({
    client: graphql,
    variables: {
      idPharmacie: currentPharmacie.id,
      take: 10,
      skip: 0,
    },
    fetchPolicy: 'cache-only',
    // skip: listResult && listResult.data && !listResult.data.search,
  });

  const [operationPanier] = useLocalOperationPanier();

  // currentPanier
  const currentPanier: Panier | null = idOperation
    ? operationPanier
    : ({
        panierLignes:
          (myPanierResult &&
            myPanierResult.data &&
            myPanierResult.data.myPaniers &&
            myPanierResult.data.myPaniers
              .map((panier) => panier && panier.panierLignes)
              .reduce((total, value) => {
                return total && total.concat(value as any);
              }, [])) ||
          [],
      } as Panier);

  // disable quantite change if user is groupement collaborateur
  const isChangeDisabled =
    user &&
    user.role &&
    user.role.code &&
    user.role.code !== COLLABORATEUR_COMMERCIAL &&
    user.role.code !== ADMINISTRATEUR_GROUPEMENT &&
    user.role.code !== COLLABORATEUR_NON_COMMERCIAL &&
    user.role.code !== GROUPEMENT_AUTRE
      ? false
      : user.role?.code === ADMINISTRATEUR_GROUPEMENT && location.pathname.includes('operations-commerciales')
      ? false
      : // : currentCanalArticle && currentCanalArticle.qteStock && currentCanalArticle.qteStock > 0
        // ? false
        true;

  const stv = currentCanalArticle && currentCanalArticle.stv ? currentCanalArticle.stv : 0;
  const qteStock = currentCanalArticle && currentCanalArticle.qteStock ? currentCanalArticle.qteStock : 0;

  const [showQteSave, setShowQteSave] = useState<boolean>(false);
  const [isQuerying, setIsQuerying] = useState<boolean>(false);

  const [currentQuantite, setCurrentQuantite] = useState<number>(parentQuantite || 0);
  const [currentLigne, setCurrentLigne] = useState<PanierLigne | null>(null);
  const lastPath = state?.from ? state.from : '';

  const quantiteStv = (stv: number, qte: number) => {
    if (qte === 0) return 0;
    return stv === 1 || stv === 0 ? qte : qte < stv ? stv : qte - (qte % stv);
  };

  const getPanierLigneItem = () => {
    if (currentCanalArticle && currentPanier && currentPanier.panierLignes) {
      const currentLigne = currentPanier.panierLignes.find(
        (ligne: PanierLigne | null) => ligne && ligne.produitCanal && ligne.produitCanal.id === currentCanalArticle.id
      );
      return currentLigne ? currentLigne : null;
    } else return null;
  };

  useEffect(() => {
    const ligne = getPanierLigneItem();
    const quantite =
      ligne && ligne.quantite
        ? ligne.quantite
        : acceptZero
        ? 0
        : currentCanalArticle && currentCanalArticle.stv
        ? currentCanalArticle.stv
        : 0;
    setCurrentLigne(ligne);
    if (acceptZero && handleParentQuantite) {
      setCurrentQuantite(parentQuantite || 0);
    } else {
      setCurrentQuantite(quantite);
    }
  }, [currentPanier]);

  const incQuantite = (e: any) => {
    e.stopPropagation();
    if (handleParentQuantite) {
      const newValue = quantiteStv(stv, currentQuantite + stv);
      setCurrentQuantite(newValue);
      handleParentQuantite(currentCanalArticle, newValue);
    } else {
      const newValue =
        qteMin && qteMin > 0 && currentQuantite < qteMin
          ? qteMin
          : currentQuantite === qteStock
          ? currentQuantite
          : stv
          ? currentQuantite + stv
          : currentQuantite + 1;

      if (qteStock >= stv && currentQuantite <= newValue) {
        if (!location.pathname.includes('/operation-produits')) {
          if (currentLigne && currentLigne.id && currentPharmacie && currentPharmacie.id) {
            setIsQuerying(true);
            doUpdateQte({
              variables: {
                id: currentLigne.id,
                quantite: newValue,
                idPharmacie: currentPharmacie.id,
                take: 10,
                skip: 0,
              },
            });
          } else {
            if (currentCanalArticle && currentCanalArticle.id && currentPharmacie && currentPharmacie.id) {
              setIsQuerying(true);
              doAddToPanier({
                variables: {
                  idCanalArticle: currentCanalArticle.id,
                  quantite: newValue,
                  idPharmacie: currentPharmacie.id,
                  codeCanal:
                    (currentCanalArticle &&
                      currentCanalArticle.commandeCanal &&
                      currentCanalArticle.commandeCanal.code) ||
                    '',
                  take: 10,
                  skip: 0,
                },
              });
            }
          }
        } else {
          // checkEditQuantite(newValue);
          if (currentCanalArticle && currentCanalArticle.id) {
            // if (qteStock < newValue) newValue = qteStock;
            saveQuantiteToApolloCache(
              currentCanalArticle,
              newValue === currentQuantite ? quantiteStv(stv, newValue + stv) : quantiteStv(stv, newValue)
            );
          }
        }

        setShowQteSave(false);
      }
    }
  };

  const decQuantite = (e: any) => {
    e.stopPropagation();
    if (handleParentQuantite) {
      if (acceptZero && currentQuantite > 0) {
        const newValue = quantiteStv(stv, currentQuantite - stv);
        setCurrentQuantite(newValue);
        handleParentQuantite(currentCanalArticle, newValue);
      }
    } else {
      if (
        (currentQuantite > stv &&
          currentCanalArticle &&
          currentCanalArticle.qteStock &&
          currentCanalArticle.qteStock > stv) ||
        (acceptZero && currentQuantite > 0)
      ) {
        // const newValue = qteMin && currentQuantite > qteMin ? currentQuantite - stv : 0;
        const newValue = currentQuantite > stv ? currentQuantite - stv : 0;
        if (!location.pathname.includes('/operation-produits')) {
          if (currentLigne && currentLigne.id && currentPharmacie && currentPharmacie.id) {
            setIsQuerying(true);
            doUpdateQte({
              variables: {
                id: currentLigne.id,
                quantite: newValue,
                idPharmacie: currentPharmacie.id,
                take: 10,
                skip: 0,
              },
            });
          } else {
            if (currentPharmacie && currentPharmacie.id && currentCanalArticle && currentCanalArticle.id) {
              setIsQuerying(true);
              doAddToPanier({
                variables: {
                  idCanalArticle: currentCanalArticle.id,
                  quantite: newValue,
                  idPharmacie: currentPharmacie.id,
                  codeCanal:
                    (currentCanalArticle &&
                      currentCanalArticle.commandeCanal &&
                      currentCanalArticle.commandeCanal.code) ||
                    '',
                },
              });
            }
          }
        } else {
          checkEditQuantite(newValue);
          if (currentCanalArticle && currentCanalArticle.id) {
            saveQuantiteToApolloCache(currentCanalArticle, quantiteStv(stv, newValue));
          }
        }

        setShowQteSave(false);
      }
    }
  };

  const handleChangeQuantite = (event: React.ChangeEvent<{ value: string }>) => {
    const quantite = parseInt(event.target.value, 10);

    checkEditQuantite(quantite);
    setShowQteSave(true);
  };

  const checkEditQuantite = (quantite: number) => {
    const qteStock = currentCanalArticle && currentCanalArticle.qteStock ? currentCanalArticle.qteStock : 0;
    if (quantite >= qteStock && !handleParentQuantite && !qteMin) {
      if (qteMin !== 0) setCurrentQuantite(qteStock);
      else setCurrentQuantite(quantite);
      return;
    }
    if (isNaN(quantite)) {
      setCurrentQuantite(0);
      return;
    }

    setCurrentQuantite(quantite);
  };

  const saveQte = (e: any) => {
    e.stopPropagation();
    if (handleParentQuantite) {
      handleParentQuantite(currentCanalArticle, quantiteStv(stv, currentQuantite));
      setCurrentQuantite(quantiteStv(stv, currentQuantite));
      setShowQteSave(false);
    } else {
      if (currentCanalArticle && currentCanalArticle.qteStock && currentCanalArticle.qteStock >= stv) {
        if (!location.pathname.includes('/operation-produits')) {
          setIsQuerying(true);
          if (currentLigne && currentLigne.id && currentPharmacie && currentPharmacie.id) {
            setIsQuerying(true);
            doUpdateQte({
              variables: {
                id: currentLigne.id,
                quantite: currentQuantite,
                idPharmacie: currentPharmacie.id,
                take: 10,
                skip: 0,
              },
            });
          } else {
            if (currentPharmacie && currentPharmacie.id && currentCanalArticle && currentCanalArticle.id) {
              setIsQuerying(true);
              doAddToPanier({
                variables: {
                  idCanalArticle: currentCanalArticle.id,
                  quantite: currentQuantite,
                  codeCanal:
                    (currentCanalArticle &&
                      currentCanalArticle.commandeCanal &&
                      currentCanalArticle.commandeCanal.code) ||
                    '',
                  idPharmacie: currentPharmacie.id,
                },
              });
            }
          }
        } else {
          if (currentCanalArticle && currentCanalArticle.id) {
            saveQuantiteToApolloCache(currentCanalArticle, quantiteStv(stv, currentQuantite));
          }

          setShowQteSave(false);
        }
      }
    }
  };

  const [doAddToPanier, AddToPanierResult] = useAddToPanierMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.addToPanier && currentCanalArticle && currentPharmacie) {
        const req = cache.readQuery<MyPaniersQuery, MyPaniersQueryVariables>({
          query: MyPaniersDocument,
          variables: {
            idPharmacie: currentPharmacie.id || '',
            take: 10,
            skip: 0,
          },
        });
        if (req && req.myPaniers) {
          if (
            req.myPaniers.length === 0 ||
            !req.myPaniers.map((panier) => panier && panier.id).includes(data.addToPanier.id)
          ) {
            cache.writeQuery({
              query: MyPaniersDocument,
              data: { myPaniers: [data.addToPanier] },
              variables: {
                idPharmacie: currentPharmacie.id || '',
                take: 10,
                skip: 0,
              },
            });
          } else {
            cache.writeQuery({
              query: MyPaniersDocument,
              data: req.myPaniers.map((panier) => {
                if (panier && data && data.addToPanier && panier.id === data.addToPanier.id) {
                  return data.addToPanier;
                } else return panier;
              }),
              variables: {
                idPharmacie: currentPharmacie.id || '',
                take: 10,
                skip: 0,
              },
            });
          }
        }
      }
    },
    onCompleted: (data) => {
      setIsQuerying(false);
      setShowQteSave(false);
    },
  });

  const [doUpdateQte, doUpdateQteResult] = useUpdate_QuantiteMutation({
    client: graphql,
    onCompleted: (data) => {
      setIsQuerying(false);
      setShowQteSave(false);
    },
  });

  const [articleOCArray, setArticleOCArray] = useLocalArticleOCArray();

  /**
   * Function to save the articles chosen by the user in commercial operation to apollo local state
   * @param id - article id
   * @param quantite - article quantity
   */
  const saveQuantiteToApolloCache = (article: ProduitCanal | any, quantite: number) => {
    setCurrentQuantite(quantite);

    if (location.pathname.includes('/operation-produits')) {
      if (quantite >= 0) {
        const ligne = {
          id: article.id,
          quantite,
          produitCanal: {
            id: article.id,
            prixPhv: article.prixPhv,
            remises: article.remises,
            __typename: 'remises',
          },
        };

        if (articleOCArray) {
          const currentArticle = articleOCArray.find((item: any) => item && item.id === article.id);

          if (currentArticle) {
            let newArticleArray = articleOCArray.map((item: any) => {
              if (item && item.id && item.id === article.id) {
                // Get new article with new quantity
                return {
                  ...ligne,
                  quantite,
                  __typename: 'ArticleOCInterface',
                };
              } else {
                return item;
              }
            });

            if (newArticleArray) {
              // Filter argticle, we only want articles greater than 0
              newArticleArray = newArticleArray.filter((item: any) => item && item.quantite > 0);
              setArticleOCArray({ articleOCArray: newArticleArray });
            }
          } else {
            const newArticle = {
              ...ligne,
              quantite,
              __typename: 'ArticleOCInterface',
            };

            // We only want article greater than 0
            if (articleOCArray.length && newArticle && newArticle.quantite > 0) {
              const newArticleArray = [...articleOCArray, newArticle];
              setArticleOCArray({ articleOCArray: newArticleArray });
            } else if (newArticle && newArticle.quantite > 0) {
              setArticleOCArray({ articleOCArray: [newArticle] });
            }
          }
        }
      }
    }
  };

  if (lastPath === '/suivi-commandes' || lastPath === '/create/operations-commerciales') {
    return (
      <Box className={classnames(classes.MontserratRegular, classes.medium, classes.colorDefault)}>
        {currentQuantite}
      </Box>
    );
  } else {
    return qteStock <= 0 ? (
      <Typography className={classes.ruptureText}>Rupture</Typography>
    ) : (
      <Box className={classes.panierListeRemiseQuantite} width={showQteSave && qteStock > 0 ? '152px' : '112px'}>
        <Box display="flex" justifyContent="center" alignItems="center">
          <IconButton
            size="small"
            onClick={decQuantite}
            disabled={
              qteStock <= 0
                ? true
                : isQuerying || location.pathname === '/autrepanier' || isChangeDisabled
                ? true
                : false
            }
          >
            <RemoveIcon className={classes.quantiteButton} />
          </IconButton>
        </Box>

        <Box className={classes.quantiteTextField}>
          {isQuerying ? (
            <CircularProgress size={14} color="secondary" />
          ) : (
            <InputBase
              onChange={handleChangeQuantite}
              onClick={(e) => e.stopPropagation()}
              value={currentQuantite}
              inputProps={{ classes: classes.underline, input: classes.inputFont }}
              disabled={qteStock <= 0 ? true : location.pathname === '/autrepanier' || isChangeDisabled ? true : false}
            />
          )}
        </Box>

        <Box display="flex" justifyContent="center" alignItems="center">
          <IconButton
            size="small"
            onClick={incQuantite}
            disabled={
              qteStock <= 0
                ? true
                : isQuerying || location.pathname === '/autrepanier' || isChangeDisabled
                ? true
                : false
            }
          >
            <AddIcon className={classes.quantiteButton} />
          </IconButton>
        </Box>
        {showQteSave && qteStock > 0 && (
          <Box display="flex" justifyContent="center" alignItems="center" className={classes.quantiteSaveButton}>
            <IconButton size="small" onClick={saveQte} disabled={isQuerying ? true : false}>
              <Save className={classes.blueButton} />
            </IconButton>
          </Box>
        )}
      </Box>
    );
  }
};

export default Quantite;
