import React, { FC } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { Box, Typography } from '@material-ui/core';
import Quantite from '../../Quantite/Quantite';
import { Panier, Pharmacie, ProduitCanal } from '@lib/common/src/graphql';
interface ArticleListQuantiteProps {
  label: string;
  currentCanalArticle: ProduitCanal | null;
  currentPanier: Panier | null;
  currentPharmacie: Pharmacie | null;
  qteMin?: number;
}
const ArticleListQuantite: FC<ArticleListQuantiteProps & RouteComponentProps<any, any, any>> = ({
  label,
  // currentPanier,
  currentCanalArticle,
  // currentPharmacie,
  qteMin,
}) => {
  const classes = useStyles({});
  return (
    <Box className={classes.rowTypo}>
      <Typography className={classnames(classes.RobotoMedium, classes.small, classes.gray)}>{label}</Typography>
      <Quantite currentCanalArticle={currentCanalArticle} acceptZero={true} qteMin={qteMin} />
    </Box>
  );
};

export default withRouter(ArticleListQuantite);
