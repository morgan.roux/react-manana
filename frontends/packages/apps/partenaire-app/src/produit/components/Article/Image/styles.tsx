import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    contentImgProduct: {
      width: '100%',
      height: 197,
      marginBottom: 12,
      '& img': {
        width: '100%',
        height: '100%',
        objectFit: 'contain',
      },
      '@media (max-width: 1366px)': {
        height: 'auto',
      },
      '@media (max-width: 768px)': {
        width: 258,
        height: 197,
      },
    },
    imgProduct: {
      height: 200,
      maxWidth: '100%',
    },
  })
);
