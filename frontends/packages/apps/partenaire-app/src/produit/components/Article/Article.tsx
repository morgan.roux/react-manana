import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { SearchDocument, SearchQuery, SearchQueryVariables } from '@lib/common/src/federation';
import { Panier, ProduitCanal, useSoftDeleteCanalArticleMutation } from '@lib/common/src/graphql';
import { ConfirmDeleteDialog, CustomButton, CustomFullScreenModal, NewCustomButton } from '@lib/ui-kit';
import { Box, IconButton, Paper, Typography } from '@material-ui/core';
import { ChatBubble, ThumbUp } from '@material-ui/icons';
import classnames from 'classnames';
import React, { FC, Fragment, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import NoImage from '../../assets/img/product_no_image.jpg';
import { ProduitDetails } from '../../pages/CatalogueProduit/ProduitDetails';
import ArticleButton from './Button/Button';
import ArticleHistorique from './Historique/Historique';
import ArticleImage from './Image/Image';
import InfoContainer from './InfoContainer';
import ArticleListBox from './ListBox/ListBox';
import { useStyles } from './styles';
import TooltipComponent from './Tooltip/Tooltip';
interface ArticleProps {
  currentPharmacie: any | null;
  currentPanier: Panier | null;
  currentCanalArticle: ProduitCanal | null | any;
  presignedUrls?: any;
  smyleys?: any;
  qteMin?: number;
  listResult?: any;
  familleResult?: any;
  disablePanier?: boolean;
}

interface TooltipContentProps {
  data: any;
  familleResult?: any;
}

export const TarifHTComponent = (props: any) => {
  const { currentCanalArticle } = props;
  const classes = useStyles({});
  return (
    <Box display="flex" flexDirection="row">
      <Typography className={classnames(classes.RobotoBold, classes.prixHt)}>
        {(currentCanalArticle && currentCanalArticle.prixPhv) || 0}
      </Typography>
      <Typography className={classnames(classes.RobotoRegular, classes.prixHt)}>€ HT</Typography>
    </Box>
  );
};

export const TooltipContent: FC<TooltipContentProps> = ({ data, familleResult }) => {
  const classes = useStyles({});
  const [familles, setFamilles] = useState<any>(null);

  const arborescenceCodeFamille = (codeFamille: string[]): string[] => {
    let letter: string = '';
    const famille: string[] = [];
    codeFamille.map((fam, index) => {
      letter += fam;
      famille[index] = letter;
    });
    return famille;
  };

  const arborescence = (codeFamille: string): string | null => {
    if (codeFamille && familles) {
      const objet = familles.find((data: any) => data.codeFamille === codeFamille);
      return (objet && objet.libelleFamille) || 'Famille non définie';
    }

    return null;
  };

  useEffect(() => {
    if (familleResult && familleResult.data && familleResult.data.search && familleResult.data.search) {
      setFamilles(familleResult.data.search.data);
    }
  }, [familleResult]);

  return (
    <div className={classes.paper}>
      {data ? (
        <>
          <h2>Famille</h2>
          <div className={classes.content}>
            <div>
              <div className={classes.contained}>
                {data && data.produit && data.produit.famille
                  ? arborescenceCodeFamille(Array.from(data.produit.famille.codeFamille)).map((letter, index) => {
                      return (
                        <div key={`${letter}-${index}`}>
                          <div>{arborescence(letter)}</div>
                          {index !== data.produit.famille.codeFamille.length - 1 ? <div>&dArr;</div> : null}
                        </div>
                      );
                    })
                  : null}
              </div>
            </div>
          </div>
        </>
      ) : null}
    </div>
  );
};

const Article: FC<ArticleProps> = ({
  currentPanier,
  currentPharmacie,
  currentCanalArticle,
  qteMin,
  listResult,
  familleResult,
  disablePanier,
}) => {
  const classes = useStyles({});
  const article = currentCanalArticle;
  const { pathname } = useLocation();
  const { auth, graphql } = useApplicationContext();
  const displayNotificaton = useDisplayNotification();
  const nomCanalArticle = article && article.produit && article.produit.libelle ? article.produit.libelle : '';
  const isInGestionProduits = pathname.includes('/gestion-produits');
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const [openModalDetail, setOpenModalDetail] = useState(false);
  const { push } = useHistory();
  const goToDetails = () => {
    if (article) {
      setOpenModalDetail(true);
      push(`/catalogue-produits/card/${article.id}`, { from: location.pathname });
    }
  };

  const handleEdit = (canalArticle: ProduitCanal) => (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.stopPropagation();
    push(`/gestion-produits/create/${canalArticle && canalArticle.produit && canalArticle.produit.id}`);
  };

  const handleOpenConfirmDialog = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    e.stopPropagation();
    setOpenConfirmDialog(true);
  };

  const [doDeleteArticle, doDeleteArticleResult] = useSoftDeleteCanalArticleMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (listResult && listResult.variables && data && data.softDeleteCanalArticle && data.softDeleteCanalArticle.id) {
        const req = cache.readQuery<SearchQuery, SearchQueryVariables>({
          query: SearchDocument,
          variables: listResult.variables,
        });
        if (req && req.search && req.search.data) {
          cache.writeQuery({
            query: SearchDocument,
            data: {
              search: {
                ...req.search,
                ...{
                  data: req.search.data.filter(
                    (item: any) =>
                      item && item.id && data.softDeleteCanalArticle && item.id !== data.softDeleteCanalArticle.id
                  ),
                },
              },
            },
            variables: listResult && listResult.variables,
          });
        }
      }
    },
    onCompleted: () => {
      displayNotificaton({
        type: 'success',
        message: 'Article supprimé avec succès',
      });
    },
  });

  const handleDelete = (canalArticle: ProduitCanal) => (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    if (canalArticle && canalArticle.id) {
      doDeleteArticle({ variables: { id: canalArticle.id } });
    }

    e.stopPropagation();
    setOpenConfirmDialog(false);
  };

  return (
    <Fragment>
      <Paper className={classes.panierListeItem}>
        <ArticleImage
          src={
            currentCanalArticle && currentCanalArticle.produit && currentCanalArticle.produit.produitPhoto
              ? currentCanalArticle.produit.produitPhoto.fichier.publicUrl
              : NoImage
          }
        />
        <TarifHTComponent currentCanalArticle={currentCanalArticle} />
        <TooltipComponent
          titleLabel={nomCanalArticle}
          famille={
            currentCanalArticle &&
            currentCanalArticle.produit &&
            currentCanalArticle.produit.famille &&
            currentCanalArticle.produit.famille.libelleFamille
          }
          component={<TooltipContent data={currentCanalArticle} familleResult={familleResult} />}
        />

        <div onClick={goToDetails} style={{ cursor: 'pointer' }}>
          <InfoContainer canalArticle={currentCanalArticle} />
        </div>
        {!isInGestionProduits && !disablePanier && (
          <ArticleListBox
            currentPanier={currentPanier}
            currentCanalArticle={currentCanalArticle}
            currentPharmacie={currentPharmacie}
            qteMin={qteMin}
            goToDetails={goToDetails}
          />
        )}
        <Box display="flex" justifyContent="space-between" onClick={goToDetails}>
          <div className={classes.iconButtonContainer}>
            <IconButton>
              <ThumbUp />
            </IconButton>
            <div>
              {(currentCanalArticle && currentCanalArticle.userSmyleys && currentCanalArticle.userSmyleys.total) || 0}
            </div>
          </div>
          <div className={classes.iconButtonContainer}>
            <IconButton>
              <ChatBubble />
            </IconButton>
            <div>
              {(currentCanalArticle && currentCanalArticle.comments && currentCanalArticle.comments.total) || 0}
            </div>
          </div>
        </Box>
        <Box>
          {!disablePanier && (
            <Box display="flex" flexDirection="row" marginTop="16px" justifyContent="space-between">
              <ArticleHistorique
                produit={currentCanalArticle && currentCanalArticle.produit}
                pharmacie={currentPharmacie}
              />

              {isInGestionProduits ? (
                <Box width="100%" marginLeft="8px" marginBottom="16px">
                  <NewCustomButton
                    id={currentCanalArticle.id}
                    name={currentCanalArticle.nomCanalArticle}
                    fullWidth={true}
                    onClick={handleOpenConfirmDialog}
                    disabled={!auth.isAuthorizedToDeleteProduct()}
                  >
                    Supprimer
                  </NewCustomButton>
                </Box>
              ) : (
                <ArticleButton currentCanalArticle={currentCanalArticle} />
              )}
            </Box>
          )}
          {isInGestionProduits && (
            <NewCustomButton
              fullWidth={true}
              onClick={handleEdit(currentCanalArticle)}
              disabled={!auth.isAuthorizedToEditProduct()}
            >
              Modifier
            </NewCustomButton>
          )}

          <ConfirmDeleteDialog
            title={'Suppression'}
            content="Êtes vous sur de vouloir supprimer ce produit"
            open={openConfirmDialog}
            setOpen={setOpenConfirmDialog}
            onClickConfirm={handleDelete(currentCanalArticle)}
          />
        </Box>
      </Paper>
      {openModalDetail && (
        <CustomFullScreenModal
          open={openModalDetail}
          setOpen={setOpenModalDetail}
          title={`Produit`}
          withBtnsActions={false}
          fullScreen={true}
          style={{ margin: '30px !important' }}
        >
          <ProduitDetails idProduct={article.id} />
        </CustomFullScreenModal>
      )}
    </Fragment>
  );
};

export default Article;
