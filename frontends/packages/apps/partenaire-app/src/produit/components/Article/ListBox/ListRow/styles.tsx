import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    rowTypo: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },
    pink: {
      // color: theme.palette.text.primary,
    },
    small: {
      // fontSize: theme.typography.small.fontSize,
    },
    gray: {
      // color: theme.palette.text.secondary,
    },
    medium: {
      // fontSize: theme.typography.medium.fontSize,
    },
    colorDefault: {
      // color: theme.palette.text.primary,
    },
    green: {
      // color: theme.palette.text.secondary,
    },
    stockIcon: {
      width: '20px',
      height: '20px',
    },
    textAlignRight: {
      textAlign: 'right',
    },
  })
);
