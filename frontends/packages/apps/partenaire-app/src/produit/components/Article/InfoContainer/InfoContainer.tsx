import React, { FC } from 'react';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Box, Typography } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import BlockTwoToneIcon from '@material-ui/icons/BlockTwoTone';
interface InfoContainerProps {
  canalArticle: any;
  column?: boolean;
}
const InfoContainer: FC<InfoContainerProps & RouteComponentProps<any, any, any>> = ({
  canalArticle,
  column,
}) => {
  const classes = useStyles({});
  if (column) {
    return (
      <div className={classes.infoContainerRoot}>
        <Box display="flex" marginTop="24px" marginBottom="8px">
          <Typography className={classes.label}>Code : </Typography>
          <Typography className={classes.value}>
            {canalArticle &&
              canalArticle.produit &&
              canalArticle.produit.produitCode &&
              canalArticle.produit.produitCode.code}
          </Typography>
        </Box>
        <Box display="flex" marginBottom="8px">
          <Typography className={classes.label}>STV : </Typography>
          <Typography className={classes.value}>{canalArticle.stv}</Typography>
        </Box>
        <Box display="flex" marginBottom="8px">
          <Typography className={classes.label}>Labo : </Typography>
          <Typography className={classes.value}>
            {canalArticle &&
              canalArticle.produit &&
              canalArticle.produit.produitTechReg &&
              canalArticle.produit.produitTechReg.laboExploitant &&
              canalArticle.produit.produitTechReg.laboExploitant.nomLabo}
          </Typography>
        </Box>
        <Box display="flex" marginBottom="8px">
          <Typography className={classes.label}>Carton : </Typography>
          <Typography className={classes.value}>{'-'}</Typography>
        </Box>
        <Box display="flex" alignItems="center" marginBottom="8px">
          <Typography className={classes.label}>Stock plateforme : </Typography>
          {canalArticle && canalArticle.qteStock && canalArticle.qteStock > 0 ? (
            <CheckCircleIcon />
          ) : (
            <BlockTwoToneIcon className={classes.secondary} />
          )}
        </Box>
        <Box display="flex" alignItems="center" marginBottom="24px">
          <Typography className={classes.label}>Stock pharmacie : </Typography>
          <Typography className={classes.value}>
            {canalArticle?.produit?.qteStockPharmacie || 0}
          </Typography>
        </Box>
      </div>
    );
  }

  return (
    <Box
      paddingBottom="16px"
      marginBottom="16px"
      borderBottom="1px solid #E0E0E0"
      className={classes.infoContainerRoot}
    >
      <Box display="flex" justifyContent="space-between" marginBottom="4px">
        <Box display="flex">
          <Typography className={classes.label}>Code : </Typography>
          <Typography className={classes.value}>
            {canalArticle &&
              canalArticle.produit &&
              canalArticle.produit.produitCode &&
              canalArticle.produit.produitCode.code}
          </Typography>
        </Box>
        <Box display="flex">
          <Typography className={classes.label}>STV : </Typography>
          <Typography className={classes.value}>{canalArticle.stv}</Typography>
        </Box>
      </Box>
      <Box display="flex" justifyContent="space-between" marginBottom="4px">
        <Box display="flex">
          <Typography className={classes.label}>Labo : </Typography>
          <Typography className={classes.value}>
            {canalArticle &&
              canalArticle.produit &&
              canalArticle.produit.produitTechReg &&
              canalArticle.produit.produitTechReg.laboExploitant &&
              canalArticle.produit.produitTechReg.laboExploitant.nomLabo}
          </Typography>
        </Box>
        <Box display="flex">
          <Typography className={classes.label}>Carton : </Typography>
          <Typography className={classes.value}>
            {(canalArticle && canalArticle.unitePetitCond) || 0}
          </Typography>
        </Box>
      </Box>
      <Box display="flex" alignItems="center" justifyContent="space-between" marginBottom="4px">
        <Box display="flex" alignItems="center">
          <Typography className={classes.label}>Stock plateforme : </Typography>
          {canalArticle && canalArticle.qteStock && canalArticle.qteStock > 0 ? (
            <CheckCircleIcon />
          ) : (
            <BlockTwoToneIcon className={classes.secondary} />
          )}
        </Box>
        <Box display="flex" alignItems="center">
          <Typography className={classes.label}>Stock pharmacie : </Typography>
          <Typography className={classes.value}>
            {canalArticle?.produit?.qteStockPharmacie || 0}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default withRouter(InfoContainer);
