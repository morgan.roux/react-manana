import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    panierListeItemDescription: {
      textAlign: 'center',
      fontFamily: 'Montserrat',
      fontSize: '12px',
      fontWeight: 600,
      letterSpacing: 0,
      color: '#004354',
      opacity: 1,
      marginTop: '10px',
      marginBottom: '14px',
    },
  })
);
