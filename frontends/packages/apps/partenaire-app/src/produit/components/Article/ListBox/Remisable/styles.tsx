import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    rowTypo: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      marginBottom: 8,
    },
    small: {
      // fontSize: theme.typography.small.fontSize,
    },
    MontserratBold: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    RobotoMedium: {
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      fontSize: 14,
      color: '#616161',
    },
    RobotoBold: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: 16,
    },
    colorDefault: {
      color: theme.palette.text.primary,
    },
    medium: {
      // fontSize: theme.typography.medium.fontSize,
    },
    pink: {
      color: theme.palette.text.primary,
      cursor: 'pointer',
    },
    underlined: {
      textDecoration: 'underline',
    },
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },
    gray: {
      // color: theme.palette.text.secondary,
    },
    secondary: {
      color: theme.palette.secondary.main,
    },
    pointer: {
      cursor: 'pointer',
    },
  })
);
