import React, { FC, useState, useEffect } from 'react';
import { useStyles } from './styles';
import { IconButton, CircularProgress } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  MyPaniersDocument,
  MyPaniersQuery,
  MyPaniersQueryVariables,
  Panier,
  PanierLigne,
  Pharmacie,
  ProduitCanal,
  useDelete_Panier_LigneMutation,
} from '@lib/common/src/graphql';
import { useLocation } from 'react-router';
import { useApplicationContext } from '@lib/common';
import { CustomButton, NewCustomButton } from '@lib/ui-kit';

interface ArticleDeleteProps {
  currentCanalArticle: ProduitCanal | null;
  currentPanier: Panier | null;
  currentPharmacie: Pharmacie | null;
  view: string;
}

const ArticleDelete: FC<ArticleDeleteProps> = ({ currentPanier, currentPharmacie, currentCanalArticle, view }) => {
  const classes = useStyles({});
  const { pathname } = useLocation();
  const { user, auth, graphql } = useApplicationContext();

  const getPanierLigneItem = () => {
    if (currentCanalArticle && currentPanier && currentPanier.panierLignes) {
      const currentLigne = currentPanier.panierLignes.find(
        (ligne: PanierLigne | null) => ligne && ligne.produitCanal && ligne.produitCanal.id === currentCanalArticle.id
      );
      return currentLigne ? currentLigne : null;
    } else return null;
  };

  const [currentLigne, setCurrentLigne] = useState<PanierLigne | null>(getPanierLigneItem());

  useEffect(() => {
    setCurrentLigne(getPanierLigneItem());
  }, [currentPanier]);

  /* DELETING PANIER LIGNE */
  const [isDeleting, setIsDeleting] = useState<boolean>(false);

  const [doDeletePanierLigne, DeletePanierLigneResult] = useDelete_Panier_LigneMutation({
    client: graphql,
    update: (cache, { data }) => {
      const req = cache.readQuery<MyPaniersQuery, MyPaniersQueryVariables>({
        query: MyPaniersDocument,
        variables: {
          idPharmacie: (currentPharmacie && currentPharmacie.id) || '',
          take: 10,
          skip: 0,
        },
      });
      if (req && req.myPaniers) {
        cache.writeQuery({
          query: MyPaniersDocument,
          data: req.myPaniers.map((panier) => {
            if (
              panier &&
              data &&
              data.deletePanierLigne &&
              currentPanier &&
              data.deletePanierLigne.id === currentPanier.id
            ) {
              return data.deletePanierLigne;
            } else return panier;
          }),
          variables: {
            idPharmacie: (currentPharmacie && currentPharmacie.id) || '',
            take: 10,
            skip: 0,
          },
        });
      }
    },
    onCompleted: (data) => {
      // const panier = data && data.deletePanierLigne ? data.deletePanierLigne : currentPanier;
      // client.writeData({ data: { panier } });
      setIsDeleting(false);
    },
  });

  const handleDeleteArticle = (e: any) => {
    e.stopPropagation();
    setIsDeleting(true);
    doDeletePanierLigne({
      variables: {
        id: currentLigne && currentLigne.id ? currentLigne.id : '',
        idPharmacie: currentPharmacie ? currentPharmacie.id : '',
        take: 10,
        skip: 0,
      },
    });
  };

  const disabledBtn = (): boolean => {
    if (!auth.isAuthorizedToRemoveProductInCart()) {
      return true;
    }
    return false;
  };

  return !pathname.includes('/operations-commerciales') ? (
    <div>
      {view === 'list' ? (
        <IconButton color="primary" onClick={handleDeleteArticle} disabled={disabledBtn()}>
          {isDeleting ? <CircularProgress size={20} color="secondary" /> : <DeleteIcon />}
        </IconButton>
      ) : (
        <NewCustomButton
          startIcon={isDeleting ? <CircularProgress size={20} color="secondary" /> : <DeleteIcon />}
          className={classes.btnDelete}
          onClick={handleDeleteArticle}
          disabled={disabledBtn()}
        >
          ANNULER
        </NewCustomButton>
      )}
    </div>
  ) : null;
};

export default ArticleDelete;
