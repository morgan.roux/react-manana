import { Panier, Pharmacie, ProduitCanal } from '@lib/common/src/graphql';
import { Box, IconButton } from '@material-ui/core';
import { ChatBubble, ThumbUp } from '@material-ui/icons';
import classnames from 'classnames';
import React, { FC } from 'react';
import { useParams } from 'react-router-dom';
import ArticleListQuantite from './Quantite/Quantite';
import ArticleRemisable from './Remisable/Remisable';
import { useStyles } from './styles';

interface ArticleListBoxProps {
  currentCanalArticle: ProduitCanal | null;
  currentPanier: Panier | null;
  currentPharmacie: Pharmacie | null;
  qteMin?: number;
  goToDetails?: () => void;
}
const ArticleListBox: FC<ArticleListBoxProps> = ({
  currentPanier,
  currentCanalArticle,
  currentPharmacie,
  qteMin,
  goToDetails,
}) => {
  const classes = useStyles({});
  const { idCanalArticle }: any = useParams();

  return (
    <Box className={classnames(classes.flex, classes.gray)}>
      <ArticleListQuantite
        currentPharmacie={currentPharmacie}
        currentCanalArticle={currentCanalArticle}
        currentPanier={currentPanier}
        label={'Quantités'}
        qteMin={qteMin}
      />
      <ArticleRemisable
        currentPanier={currentPanier}
        currentCanalArticle={currentCanalArticle}
        currentPharmacie={currentPharmacie}
      />
    </Box>
  );
};

export default ArticleListBox;
