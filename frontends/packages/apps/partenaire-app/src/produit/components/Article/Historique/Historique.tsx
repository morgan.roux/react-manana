import { CustomButton, NewCustomButton } from '@lib/ui-kit';
import { Box, Fade, IconButton, Tooltip } from '@material-ui/core';
import { History } from '@material-ui/icons';
import React, { FC, useState } from 'react';
import SalesHistory from '../../Graphs';

interface ArticleHistoriqueProps {
  view?: string;
  produit: any;
  pharmacie?: any;
}

const ArticleHistorique: FC<ArticleHistoriqueProps> = ({ view, produit, pharmacie }) => {
  // const classes = useStyles({});
  const [{ openSalesHistory }, setOpenSalesHistory] = React.useState({ openSalesHistory: false });
  const [historyColor, setHistoryColor] = useState<string>('');
  const isInGestionProduits = location.pathname.includes('/gestion-produits');

  const toggleSalesHistory = (e: any) => {
    e.stopPropagation();
    setOpenSalesHistory((prevState) => ({
      ...prevState,
      openSalesHistory: !prevState.openSalesHistory,
    }));
  };

  const handleSendColor = (color: string) => {
    setHistoryColor(color);
  };
  return (
    <Box width={isInGestionProduits ? '100%' : 'auto'}>
      {view === 'table' ? (
        <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Historique">
          <IconButton onClick={toggleSalesHistory}>
            <History />
          </IconButton>
        </Tooltip>
      ) : (
        <NewCustomButton
          theme={isInGestionProduits ? 'primary' : 'transparent'}
          onClick={toggleSalesHistory}
          fullWidth={isInGestionProduits ? true : false}
        >
          HISTORIQUE
        </NewCustomButton>
      )}
      <SalesHistory
        open={openSalesHistory}
        onClose={toggleSalesHistory}
        sendColor={handleSendColor}
        produit={produit}
        pharmacie={pharmacie}
      />
    </Box>
  );
};

export default ArticleHistorique;
