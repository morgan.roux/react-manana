import { Panier, PanierLigne, Pharmacie, ProduitCanal } from '@lib/common/src/graphql';
import { useLocalOperationPanier } from '@lib/common/src/shared/operations/operation-commerciale/oc';
import { Box, Typography } from '@material-ui/core';
import classnames from 'classnames';
import React, { FC, Fragment, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import RemisePalier from '../../../RemisePalier/RemisePalier';
import { useStyles } from './styles';

interface ArticleRemisableProps {
  currentCanalArticle: ProduitCanal | null;
  currentPanier: Panier | null;
  currentPharmacie: Pharmacie | null;
}

const ArticleRemisable: FC<ArticleRemisableProps> = ({ currentCanalArticle, currentPanier }) => {
  const classes = useStyles({});
  const { idOperation }: any = useParams();
  const [prixNet, setPrixNet] = useState<number>(0);
  const [totalNet, setTotalNet] = useState<number>(0);
  const [uG, setUG] = useState<number>(0);

  const [operationPanier] = useLocalOperationPanier();

  const getPanierLigneItem = (): PanierLigne | null => {
    const panier = idOperation ? operationPanier : currentPanier;
    if (currentCanalArticle && panier && panier.panierLignes) {
      const currentLigne = panier.panierLignes.find(
        (ligne: PanierLigne | null) => ligne && ligne.produitCanal && ligne.produitCanal.id === currentCanalArticle.id
      );
      return currentLigne ? currentLigne : null;
    } else return null;
  };

  const handleCloseModal = () => {
    if (isRemiseOpen) setIsRemiseOpen(false);
  };

  const ligne = getPanierLigneItem();
  const [currentRemise, setCurrentRemise] = useState<string>('');
  const article: any = ligne && ligne.produitCanal ? ligne.produitCanal : currentCanalArticle;
  const [isRemiseOpen, setIsRemiseOpen] = useState<boolean>(false);

  const checkRemise = (quantite: number) => {
    const panier = idOperation ? operationPanier : currentPanier;
    const remises = (article && article.remises) || [];
    const prixPhv = article && article.prixPhv ? article.prixPhv : 0;
    let pourcentage = 0;
    let qteMin = 0;
    let qteRemisePanachee = 0;

    if (article && article.articleSamePanachees) {
      article.articleSamePanachees.map((article: any) => {
        if (panier && panier.panierLignes) {
          panier.panierLignes.map((ligne: any) => {
            if (
              article &&
              ligne &&
              ligne.quantite &&
              ligne.produitCanal &&
              ligne.produitCanal.id &&
              ligne.produitCanal.id === article.id
            ) {
              qteRemisePanachee += ligne.quantite;
            }
          });
        }
      });
    }
    if (qteRemisePanachee === 0) {
      qteRemisePanachee = quantite;
    }
    const minimalRemise = remises && remises[0];
    remises.map((remise: any | null) => {
      const pourcentageRemise = remise && remise.pourcentageRemise ? remise.pourcentageRemise : 0;
      const quantiteMin = remise && remise.quantiteMin ? remise.quantiteMin : 0;
      const uG = remise && remise.nombreUg ? remise.nombreUg : 0;
      if (
        remise &&
        remise.quantiteMin &&
        remise.quantiteMax &&
        quantite > 0 &&
        qteRemisePanachee >= remise.quantiteMin &&
        qteRemisePanachee <= remise.quantiteMax
      ) {
        pourcentage = pourcentageRemise;
        qteMin = quantiteMin;
        setUG(uG);
      }
    });
    const prixNet = quantite > 0 ? parseFloat((prixPhv - (prixPhv * pourcentage) / 100).toFixed(2)) : 0;

    const totalNet = parseFloat((prixNet * quantite).toFixed(2));
    setPrixNet(prixNet);
    setTotalNet(totalNet);
    const result =
      pourcentage > 0
        ? `${pourcentage}%`
        : minimalRemise
        ? `${minimalRemise && minimalRemise.pourcentageRemise}% à partir de ${
            minimalRemise && minimalRemise.quantiteMin
          } achetés`
        : '0%';
    return setCurrentRemise(result);
  };

  useEffect(() => {
    const ligne = getPanierLigneItem();
    if (ligne && ligne.quantite) {
      checkRemise(ligne.quantite);
    } else {
      checkRemise(0);
    }
  }, [currentPanier, operationPanier]);

  const handleRemiseButton = (e: any) => {
    e.stopPropagation();
    setIsRemiseOpen(true);
  };
  return (
    <Fragment>
      <Box className={classes.rowTypo}>
        <Typography className={classnames(classes.RobotoMedium)}>Remise</Typography>
        <Typography
          className={classnames(classes.RobotoBold, classes.secondary, classes.underlined, classes.pointer)}
          onClick={handleRemiseButton}
        >
          {currentRemise}
        </Typography>
      </Box>
      <Box className={classes.rowTypo}>
        <Typography className={classnames(classes.RobotoMedium)}>Prix Net</Typography>
        <Typography className={classnames(classes.RobotoBold, classes.colorDefault)} onClick={handleRemiseButton}>
          {prixNet}€
        </Typography>
      </Box>
      <Box className={classes.rowTypo}>
        <Typography className={classnames(classes.RobotoMedium)}>Total Net</Typography>
        <Typography className={classnames(classes.RobotoBold, classes.secondary)}>{totalNet}€</Typography>
      </Box>
      <Box className={classes.rowTypo}>
        <Typography className={classnames(classes.RobotoMedium)}>Unités gratuites</Typography>
        <Typography className={classnames(classes.RobotoBold)} onClick={handleRemiseButton}>
          {uG}
        </Typography>
      </Box>
      {isRemiseOpen && <RemisePalier handleCloseModal={handleCloseModal} article={currentCanalArticle} />}
    </Fragment>
  );
};

export default ArticleRemisable;
