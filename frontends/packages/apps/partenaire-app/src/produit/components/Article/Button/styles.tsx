import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    btnAddPanier: {
      textTransform: 'uppercase',
      fontFamily: 'Montserrat',
      maxWidth: 154,
      minWidth: 124,
      fontSize: '0.75rem',
      maxHeight: 36,
      fontWeight: 'bold',
      boxShadow: '0px 3px 2px #14141429',
    },
    disabledButton: {
      fontFamily: 'Montserrat',
      maxWidth: 154,
      minWidth: 124,
      fontSize: '0.75rem',
      textTransform: 'uppercase',
      boxShadow: '0px 3px 2px #14141429',
    },
    whiteCircular: {
      color: theme.palette.common.white,
    },
  })
);
