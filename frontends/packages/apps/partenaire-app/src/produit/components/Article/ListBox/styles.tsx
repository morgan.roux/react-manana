import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    flex: {
      flex: 1,
    },
    gray: {
      // color: theme.palette.text.secondary,
    },
    root: {
      display: 'flex',
      alignItems: 'center',
      margin: '10px 0px',
      '& > img': {
        marginLeft: -5,
      },
      '& > img:nth-child(1)': {
        marginLeft: 0,
      },
    },
  })
);
