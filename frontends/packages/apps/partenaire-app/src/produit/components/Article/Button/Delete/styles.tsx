import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    btnDelete: {
      textTransform: 'uppercase',
      fontFamily: 'Roboto',
      maxWidth: 154,
      minWidth: 124,
      fontSize: 14,
      fontWeight: 'bold',
    },
  })
);
