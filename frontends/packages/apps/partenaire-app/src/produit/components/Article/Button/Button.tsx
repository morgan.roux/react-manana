import React, { FC, useState, useEffect } from 'react';
import { useStyles } from './styles';
import classnames from 'classnames';
import { CircularProgress, Tooltip, Fade, IconButton } from '@material-ui/core';
import { ShoppingCart } from '@material-ui/icons';
import ArticleDelete from './Delete/Delete';
import {
  MyPaniersDocument,
  MyPaniersQuery,
  MyPaniersQueryVariables,
  Panier,
  PanierLigne,
  ProduitCanal,
  useAddToPanierMutation,
  useMyPaniersQuery,
  useTakeProductToMyPanierMutation,
} from '@lib/common/src/graphql';
import { useLocation } from 'react-router';
import {
  ADMINISTRATEUR_GROUPEMENT,
  COLLABORATEUR_COMMERCIAL,
  COLLABORATEUR_NON_COMMERCIAL,
  GROUPEMENT_AUTRE,
  useApplicationContext,
} from '@lib/common';
import { CustomButton, NewCustomButton } from '@lib/ui-kit';

interface ArticleButtonProps {
  currentCanalArticle: ProduitCanal | null;
  view?: string;
}

const ArticleButton: FC<ArticleButtonProps> = ({ currentCanalArticle, view }) => {
  const classes = useStyles({});
  const { pathname, state } = useLocation();
  const { currentPharmacie, graphql, user, auth } = useApplicationContext();

  const qteStock = currentCanalArticle && currentCanalArticle.qteStock ? currentCanalArticle.qteStock : 0;
  const viewCard: boolean = pathname.includes('card');

  const myPanierResult = useMyPaniersQuery({
    client: graphql,
    variables: {
      idPharmacie: currentPharmacie.id,
      take: 10,
      skip: 0,
    },
    // skip: listResult && listResult.data && !listResult.data.search,
  });

  // currentPanier
  const currentPanier: Panier | null =
    ({
      panierLignes:
        (myPanierResult &&
          myPanierResult.data &&
          myPanierResult.data.myPaniers &&
          myPanierResult.data.myPaniers
            .map((panier) => panier && panier.panierLignes)
            .reduce((total, value) => {
              return total && total.concat(value as any);
            }, [])) ||
        [],
    } as Panier) || null;

  // get last path
  const lastPath = (state as any) && (state as any).from ? (state as any).from : '';

  // disable quantite change if user is groupement collaborateur
  const isChangeDisabled =
    user &&
    user.role &&
    user.role.code &&
    user.role.code !== COLLABORATEUR_COMMERCIAL &&
    user.role.code !== ADMINISTRATEUR_GROUPEMENT &&
    user.role.code !== COLLABORATEUR_NON_COMMERCIAL &&
    user.role.code !== GROUPEMENT_AUTRE
      ? false
      : true;

  const getPanierLigneItem = () => {
    if (currentCanalArticle && currentPanier && currentPanier.panierLignes) {
      const currentLigne = currentPanier.panierLignes.find(
        (ligne: PanierLigne | null) => ligne && ligne.produitCanal && ligne.produitCanal.id === currentCanalArticle.id
      );
      return currentLigne ? currentLigne : null;
    } else return null;
  };

  const [currentLigne, setCurrentLigne] = useState<PanierLigne | null>(getPanierLigneItem());

  useEffect(() => {
    setCurrentLigne(getPanierLigneItem());
  }, [currentPanier]);

  /* ADDING PANIER LIGNE */
  const [isAdding, setIsAdding] = useState<boolean>(false);

  const [doAddToPanier, AddToPanierResult] = useAddToPanierMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.addToPanier && currentCanalArticle && currentPharmacie) {
        const req = cache.readQuery<MyPaniersQuery, MyPaniersQueryVariables>({
          query: MyPaniersDocument,
          variables: {
            idPharmacie: currentPharmacie.id,
            take: 10,
            skip: 0,
          },
        });

        cache.writeQuery({
          query: MyPaniersDocument,
          data: {
            myPaniers:
              req && req.myPaniers && req.myPaniers.length
                ? req.myPaniers.map((panier) => {
                    if (panier && data && data.addToPanier && panier.id === data.addToPanier.id) {
                      return data.addToPanier;
                    }
                    return panier;
                  })
                : [data.addToPanier],
          },
          variables: {
            idPharmacie: currentPharmacie.id || '',
            take: 10,
            skip: 0,
          },
        });
      }
    },
    onCompleted: (data) => {
      // const panier = data && data.addToPanier ? data.addToPanier : currentPanier;
      // client.writeData({ data: { panier } });
      setIsAdding(false);
    },
  });
  const [doTakeProductToMyPanier, TakeProductToMyPanierResult] = useTakeProductToMyPanierMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.takeProductToMyPanier && currentCanalArticle && currentPharmacie) {
        const req = cache.readQuery<MyPaniersQuery, MyPaniersQueryVariables>({
          query: MyPaniersDocument,
          variables: {
            idPharmacie: currentPharmacie.id,
            take: 10,
            skip: 0,
          },
        });

        cache.writeQuery({
          query: MyPaniersDocument,
          data: {
            myPaniers:
              req && req.myPaniers && req.myPaniers.length
                ? req.myPaniers.map((panier) => {
                    if (panier && data && data.takeProductToMyPanier && panier.id === data.takeProductToMyPanier.id) {
                      return data.takeProductToMyPanier;
                    }
                    return panier;
                  })
                : [data.takeProductToMyPanier],
          },
          variables: {
            idPharmacie: currentPharmacie.id || '',
            take: 10,
            skip: 0,
          },
        });
      }
    },
    onCompleted: (data) => {
      // const panier =
      //   data && data.takeProductToMyPanier ? data.takeProductToMyPanier : currentPanier;

      // client.writeData({ data: { panier } });
      setIsAdding(false);
    },
  });

  const handleAjouter = (e: any) => {
    e.stopPropagation();
    if (
      currentCanalArticle &&
      currentCanalArticle.qteStock &&
      currentCanalArticle.stv &&
      currentCanalArticle.qteStock >= currentCanalArticle.stv
    ) {
      setIsAdding(true);
      if (currentCanalArticle.inOtherPanier && currentCanalArticle.inOtherPanier.id) {
        doTakeProductToMyPanier({
          variables: {
            id: currentCanalArticle.inOtherPanier.id,
            idPharmacie: currentPharmacie ? currentPharmacie.id : '',
            codeCanal:
              (currentCanalArticle && currentCanalArticle.commandeCanal && currentCanalArticle.commandeCanal.code) ||
              '',
            take: 10,
            skip: 0,
          },
        });
      } else {
        doAddToPanier({
          variables: {
            idCanalArticle: currentCanalArticle ? currentCanalArticle.id : '',
            idPharmacie: currentPharmacie ? currentPharmacie.id : '',
            codeCanal:
              (currentCanalArticle && currentCanalArticle.commandeCanal && currentCanalArticle.commandeCanal.code) ||
              '',
            take: 10,
            skip: 0,
          },
        });
      }
    }
  };

  const disabledAddToCartBtn = (): boolean => {
    if (!auth.isAuthorizedToAddProductToCart() || qteStock === 0 || isChangeDisabled) {
      return true;
    }
    return false;
  };

  if (
    !pathname.includes('/operation-produits') &&
    lastPath !== '/suivi-commandes' &&
    lastPath !== '/create/operations-commerciales'
  ) {
    if (currentLigne) {
      return (
        <ArticleDelete
          view={viewCard ? 'card' : 'list'}
          currentPanier={currentPanier}
          currentCanalArticle={currentCanalArticle}
          currentPharmacie={currentPharmacie}
        />
      );
    } else {
      if (qteStock === 0) {
        return (
          <>
            {view === 'table' ? (
              <IconButton disabled={true} color="secondary">
                <ShoppingCart />
              </IconButton>
            ) : (
              <NewCustomButton
                startIcon={<ShoppingCart />}
                className={classnames(classes.disabledButton)}
                disabled={true}
              >
                AJOUTER
              </NewCustomButton>
            )}
          </>
        );
      } else {
        return isAdding ? (
          <>
            {view === 'table' ? (
              <IconButton>
                <CircularProgress color="secondary" size={20} />
              </IconButton>
            ) : (
              <NewCustomButton
                onClick={handleAjouter}
                className={classnames(classes.btnAddPanier)}
                disabled={isChangeDisabled ? true : false}
              >
                <CircularProgress color="inherit" size={20} />
              </NewCustomButton>
            )}
          </>
        ) : currentCanalArticle && currentCanalArticle.inOtherPanier ? (
          <NewCustomButton
            onClick={handleAjouter}
            className={classnames(classes.btnAddPanier)}
            disabled={disabledAddToCartBtn()}
          >
            Envoyer à mon panier
          </NewCustomButton>
        ) : (
          <>
            {view === 'table' ? (
              <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Ajouter à mon panier">
                <IconButton onClick={handleAjouter} color="secondary" disabled={disabledAddToCartBtn()}>
                  <ShoppingCart />
                </IconButton>
              </Tooltip>
            ) : (
              <NewCustomButton
                onClick={handleAjouter}
                className={classnames(classes.btnAddPanier)}
                disabled={disabledAddToCartBtn()}
                startIcon={<ShoppingCart />}
              >
                AJOUTER
              </NewCustomButton>
            )}
          </>
        );
      }
    }
  } else return <></>;
};

export default ArticleButton;
