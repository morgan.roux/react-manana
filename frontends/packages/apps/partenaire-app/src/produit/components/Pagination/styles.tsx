import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    /* FLEX */
    flexColumn: {
      display: 'flex',
      flexDirection: 'column',
    },
    flexRow: {
      display: 'flex',
      flexDirection: 'row',
      margin: '20px 0px',
      justifyContent: 'center',
    },
    inline: {
      display: 'inline',
    },
    rowTypo: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    justifyCenter: {
      justifyContent: 'center',
    },
    alignCenter: {
      alignItems: 'center',
    },
    /* FONT STYLE */
    MontserratRegular: {
      fontFamily: 'Montserrat',
      fontWeight: 400,
    },
    MontserratBold: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    RobotoRegular: {
      fontFamily: 'Roboto',
      fontWeight: 'normal',
    },
    RobotoBold: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
    },

    underlined: {
      textDecoration: 'underline',
    },

    /*FONT SIZE */
    douze: {
      fontSize: '12px',
    },
    quatorze: {
      fontSize: '14px',
    },
    vingt: {
      fontSize: '20px',
    },
    selectInput: {
      fontSize: '12px',
      paddingTop: 0,
    },
    formControl: {
      marginLeft: '16px',
      marginRight: '16px',
    },
  })
);

export default useStyles;
