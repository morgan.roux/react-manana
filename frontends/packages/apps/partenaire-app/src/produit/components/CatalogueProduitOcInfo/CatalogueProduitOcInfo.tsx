import React, { FC } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { OPERATION_COMMERCIALE_operation } from '../../../../../graphql/OperationCommerciale/types/OPERATION_COMMERCIALE';
import moment from 'moment';
import { Box, Typography } from '@material-ui/core';
interface CatalogueProduitOcInfoProps {
  operation: OPERATION_COMMERCIALE_operation | undefined | null;
}

const PRIORITES = { 1: 'NORMAL', 2: 'IMPORTANT', 3: 'BLOQUANT' };

const CatalogueProduitOcInfo: FC<CatalogueProduitOcInfoProps & RouteComponentProps> = ({
  history,
  operation,
}) => {
  const classes = useStyles({});

  return operation ? (
    <Box
      className={classes.root}
      display="flex"
      alignItems="center"
      justifyContent="center"
      flexDirection="column"
    >
      <Typography className={classes.typeOC}>{operation.item && operation.item.name}</Typography>
      <Typography className={classes.libelle}>{operation.libelle}</Typography>
      <Box width="100%" display="flex" justifyContent="space-between">
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          du
          <Typography>{operation.dateDebut && moment(operation.dateDebut).format('L')} </Typography>
          au <Typography> {operation.dateFin && moment(operation.dateFin).format('L')} </Typography>
        </Box>
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          Créée le :
          <Typography>{operation.dateCreation && moment(operation.dateFin).format('L')}</Typography>
        </Box>
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          Modifiée le :
          <Typography>
            {operation.dateModification && moment(operation.dateFin).format('L')}
          </Typography>
        </Box>
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          Type :<Typography>{(operation.item && operation.item.name) || '-'}</Typography>
        </Box>
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          Labo :
          <Typography>{(operation.laboratoire && operation.laboratoire.nomLabo) || '-'}</Typography>
        </Box>
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          Priorité :
          <Typography>
            {(operation.niveauPriorite && PRIORITES[operation.niveauPriorite]) || 'NORMAL'}
          </Typography>
        </Box>
      </Box>
      <Box width="100%" display="flex" justifyContent="space-around ">
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          Canal de commande :
          <Typography>
            {(operation.commandeCanal && operation.commandeCanal.libelle) || '-'}
          </Typography>
        </Box>
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          CA Global :<Typography>{`${operation.CATotal} €`}</Typography>
        </Box>
        <Box display="flex" alignItems="baseline" className={classes.labelName}>
          CA Moyenne :<Typography>{`${operation.caMoyenPharmacie} €`}</Typography>
        </Box>
      </Box>
    </Box>
  ) : null;
  // <div>{`dateDebut : ${operation &&
  //   operation.dateDebut &&
  //   moment(operation.dateDebut).format('L')}`}</div>
};

export default withRouter(CatalogueProduitOcInfo);
