import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      '& *': {
        letterSpacing: 0,
      },
    },
    appbar: {
      color: theme.palette.primary.main,
      background: theme.palette.common.white,
      '&.MuiAppBar-colorPrimary': {
        color: '#fff',
        backgroundColor: theme.palette.primary.main,
      },
      [theme.breakpoints.down('sm')]: {
        '&.MuiAppBar-colorPrimary': {
          backgroundColor: 'none',
        },
      },
    },
    menuButton: {
      '&.MuiIconButton-colorPrimary': {
        color: 'rgba(0, 0, 0, 0.54)',
      },
    },
    activeAffichage: {
      color: theme.palette.secondary.main,
    },
    contentSearch: {
      flexGrow: 1,
      display: 'flex',
      justifyContent: 'start',
      alignItems: 'center',
    },
    title: {
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      fontSize: '1rem',
    },
    affichage: {
      fontFamily: 'Roboto',
      fontSize: '0.75rem',
      marginRight: '0.25rem',
    },
    btnPasserCmd: {
      fontWeight: 'bold',
      fontSize: 14,
      fontFamily: 'Roboto',
    },
    toolbar: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    deleteButton: {
      background: '#E0E0E0',
      color: theme.palette.common.black,
      marginRight: theme.spacing(3),
    },
    viewButton: {
      marginRight: theme.spacing(3),
    },
    headerMobile: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: '10px',
      '& .MuiTypography-root': {
        fontSize: '16px',
        fontFamily: 'Roboto',
      },
    },
    icons: {
      display: 'flex',
      alignItems: 'center',
    },
    divider: {
      '& .MuiDivider-root': {
        marginTop: '16px',
      },
    },
  })
);

export default useStyles;
