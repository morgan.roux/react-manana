import { px2rem } from '@lib/common';
import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      '& .MuiAccordionSummary-root': {
        flexDirection: 'row',
        '&.Mui-expanded': {
          minHeight: 45,
        },
        '& .MuiAccordionSummary-expandIcon': {
          padding: 0,
        },
      },
      '& .MuiAccordionSummary-content': {
        justifyContent: 'space-between',
        margin: 0,
        '&.Mui-expanded': {
          margin: '11px 0',
        },
      },
      '& .MuiAccordion-root': {
        boxShadow: 'none!important',
        '&.Mui-expanded': {
          margin: '0',
        },
      },
      '& .MuiAccordionDetails-root': {
        padding: '0px!important',
        '& .MuiList-root': {
          width: '100%',
          paddingTop: 0,
          paddingBottom: 0,
          '& .MuiListItem-button': {
            paddingRight: 0,
            paddingTop: 0,
            paddingBottom: 0,
            '& .MuiIconButton-root': {
              padding: 10,
            },
          },
        },
      },
    },
    heading: {
      fontSize: px2rem(15, theme),
      fontWeight: 'normal',
      display: 'flex',
      alignItems: 'center',
      paddingLeft: 8,
    },
  })
);

export default useStyles;
