import { isMobile } from '@lib/common';
import { CustomButton, CustomModal, NewCustomButton } from '@lib/ui-kit';
import { Checkbox, FormControl, FormControlLabel, FormGroup, FormLabel } from '@material-ui/core';
import React, { FC, ReactNode, useEffect, useState } from 'react';
import { useStyles } from './styles';

export interface ItemFilter {
  id: string;
  libelle: string;
  code: string;
  keyword: string;
}

export interface Filter {
  titre: string;
  itemFilters: ItemFilter[];
}

export interface FilterProps {
  onChange: (data: ItemFilter[]) => void;
  filters: Filter[];
  filterIcon: any;
  withNotText?: boolean;
  btns?: ReactNode[];
  hideText?: boolean;
  filterText?: string;
}

export const Filter: FC<FilterProps> = ({
  onChange,
  filters,
  children,
  filterIcon,
  withNotText,
  btns,
  hideText,
  filterText,
}) => {
  const [allfilters, setAllFilters] = useState<Filter[] | undefined>();
  const [open, setOpen] = useState<boolean>(false);
  const styles = useStyles();
  const [itemsChecked, setItemsChecked] = useState<any>();
  const [filterResuts, setFilterResults] = useState<any[]>([]);

  useEffect(() => {
    setAllFilters(filters);
    (filters || []).map(({ itemFilters }) =>
      itemFilters.map(({ id }) => {
        setItemsChecked({ ...itemsChecked, [id]: false });
      })
    );
  }, [filters]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setItemsChecked({ ...itemsChecked, [event.target.name]: event.target.checked });
    filters.map(({ itemFilters }) =>
      itemFilters.map((item) => {
        if (event.target.name === item.id && event.target.checked === true) {
          setFilterResults((prev) => [...prev, item]);
        }
      })
    );
  };

  const handleSubmit = () => {
    const results = filterResuts.filter((item) => item.id in itemsChecked && itemsChecked[item.id] === true);
    onChange(results);
    setFilterResults(results);
    setOpen(false);
  };

  const handleClick = () => {
    setOpen(true);
  };

  return (
    <>
      <span onClick={handleClick} className={styles.btnFilterPar}>
        {filterIcon}
        {hideText ? '' : <>&nbsp; {!withNotText ? filterText || 'FILTRER PAR' : ''}</>}
      </span>
      {(btns || []).map((btn) => btn)}
      <CustomModal
        title="Filter par"
        open={open}
        setOpen={setOpen}
        withBtnsActions={false}
        closeIcon
        headerWithBgColor
        maxWidth="sm"
        fullScreen={!!isMobile()}
      >
        <div className={styles.rootModal}>
          {(allfilters || []).map((filter) => (
            <FormControl key={filter.titre} component="fieldset" className={styles.formControl}>
              <FormLabel className={styles.Label} component="legend">
                {filter.titre}
              </FormLabel>
              <FormGroup>
                {filter.itemFilters.map(({ id, libelle }) => (
                  <FormControlLabel
                    key={id}
                    control={<Checkbox checked={itemsChecked[id]} onChange={handleChange} name={id} />}
                    label={libelle}
                  />
                ))}
              </FormGroup>
            </FormControl>
          ))}
          {children}
          <div className={styles.btnSubmit}>
            <NewCustomButton onClick={handleSubmit}>Filtrer</NewCustomButton>
          </div>
        </div>
      </CustomModal>
    </>
  );
};
