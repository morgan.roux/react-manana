import { useApplicationContext, useDisplayNotification, useFilters } from '@lib/common';
import ButtonGoToCommand from '@lib/common/src/components/ButtonGoToCommand';
import SearchContent from '@lib/common/src/components/Filter/SearchContent';
import { useCreate_One_RemiseMutation } from '@lib/common/src/federation';
import { Search_Produit_CanalDocument, useSoftDeleteCanalArticlesMutation } from '@lib/common/src/graphql';
import { useLocalCheckedsProduitCanal } from '@lib/common/src/shared/operations/produit-canal';
import { ConfirmDeleteDialog, CustomFullScreenModal, NewCustomButton } from '@lib/ui-kit';
import { AppBar, Box, IconButton, Toolbar, Typography, FormControlLabel, Switch, Divider } from '@material-ui/core';
import { Add, ArrowBack, Delete, FilterList, Replay, ViewList, ViewModule } from '@material-ui/icons';
import ViewListIcon from '@material-ui/icons/ViewList';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import classNames from 'classnames';
import _ from 'lodash';
import React, { ChangeEvent, FC, useState } from 'react';
import { useHistory, useLocation, useParams } from 'react-router';
import RemiseForm, { RemiseFormValues } from '../../pages/CatalogueProduit/RemiseForm';
import ProduitFilter from './ProduitFilter';
import { canals, tris } from './ProduitFilter/ProduitFilter';
import useStyles from './styles';
import { Filter } from './Filter';
import FilterAlt from '../../assets/icon/filter_alt.svg';

import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

interface AppBarFilterProps {
  changeView: () => void;
  view: string;
  listResult?: any;
  showGestionProduitsBtns?: boolean;
  selected?: any;
  setSelected?: (values: any) => void;
  refetch?: any;
}

const AppBarFilter: FC<AppBarFilterProps> = ({
  changeView,
  view,
  listResult,
  showGestionProduitsBtns,
  selected,
  setSelected,
  refetch,
}) => {
  const classes = useStyles({});

  const {
    push,
    location: { state },
  } = useHistory();
  const { pathname } = useLocation();
  const { idOperation }: any = useParams();
  const { graphql, federation, user, auth } = useApplicationContext();
  const displayNotification = useDisplayNotification();
  const isInLaboratoireProduits = pathname.includes('/laboratoires');
  const isInGestionProduits = pathname.includes('/gestion-produits');
  const isInOperationCommande = window.location.hash.includes('#/operation-produits');
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const [openFormModal, setOpenFormModal] = useState(false);
  const [sort, setSort] = useState<'asc' | 'desc'>('asc');
  const [checkedItems, setCheckedsProduits] = useLocalCheckedsProduitCanal();
  const { setSort: setLocalSort, setFilters: setLocalFilters } = useFilters();

  const [remiseSource, setRemiseSource] = useState<'PERMANENT' | 'OPERATION'>('PERMANENT');
  const [openRemiseModal, setOpenRemiseModal] = useState<boolean>(false);
  const defaultRemiseFormValues: RemiseFormValues = {
    dateDebut: null,
    dateFin: null,
    nom: '',
    model: 'LIGNE',
    remiseDetails: [],
  };
  const [remiseFormValues, setRemiseFormValues] = useState<RemiseFormValues>(defaultRemiseFormValues);
  const { myPharmacieFilter, setMyPharmacieFilter } = useFilters();

  const [createRemise, creationRemise] = useCreate_One_RemiseMutation({
    client: federation,
    onCompleted: () => {
      displayNotification({
        type: 'success',
        message: 'La remise a été créée avec succès',
      });
      setRemiseFormValues(defaultRemiseFormValues);
      setSelected && setSelected([]);
      refetch && refetch();
      setOpenRemiseModal(false);
    },
    onError: () => {
      displayNotification({
        type: 'error',
        message: "Une erreur s'est produite lors de création de la remise",
      });
    },
  });

  const [filters, setFilters] = useState({
    canal: [canals[2]],
    reactions: [],
    remises: [],
    tri: [tris[1]],
  });

  // change view mode : card or list
  const handleChangeView = () => {
    changeView();
  };

  const handleCreateProduit = () => {
    push(`/gestion-produits/create`);
    setOpenFormModal(true);
  };

  const handleOpenRemiseModal = (source: 'PERMANENT' | 'OPERATION') => {
    setRemiseSource(source);
    setOpenRemiseModal(true);
  };

  const [doDeleteArticles, doDeleteArticlesResult] = useSoftDeleteCanalArticlesMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.softDeleteCanalArticles) {
        if (listResult && listResult.variables) {
          const query = cache.readQuery<any, any>({
            query: Search_Produit_CanalDocument,
            variables: listResult.variables,
          });
          if (query && query.search && query.search.data) {
            cache.writeQuery({
              query: Search_Produit_CanalDocument,
              data: {
                search: {
                  ...query.search,
                  ...{
                    data: _.differenceWith(query.search.data, checkedItems, _.isEqual),
                  },
                },
              },
              variables: listResult.variables,
            });
          }
        }
        setCheckedsProduits(null);
      }
    },
    onCompleted: (data) => {
      displayNotification({
        type: 'success',
        message: 'Article supprimé avec succès',
      });
      setOpenConfirmDialog(false);
    },
  });

  const handleOpenConfirmDialog = () => {
    setOpenConfirmDialog(true);
  };

  const handleDelete = () => {
    doDeleteArticles({
      variables: { ids: checkedItems && checkedItems.map((item: any) => item.id) },
    });
  };

  const clickBack = () => {
    if ((state as any).idLaboratoire)
      push(`/laboratoires/${(state as any).idLaboratoire}/${'partenaire'}`, {
        goBack: true,
      });
  };

  const handleFilterChange = (data: any) => {
    setLocalSort({
      label: 'tri',
      name: filters.tri[0].name,
      direction: sort,
      active: true,
    });
    setLocalFilters({
      idCheckeds: [],
      idLaboratoires: [],
      idFamilles: [],
      idGammesCommercials: [],
      idCommandeCanal: filters.canal[0].code,
      idCommandeCanals: [],
      idCommandeTypes: [],
      idActualiteOrigine: null,
      idOcCommande: null,
      idSeen: null,
      idTodoType: null,
      sortie: null,
      laboType: null,
      reaction: filters.reactions.length > 0 ? filters.reactions.map((el: any) => el.name) : [],
      idRemise: filters.remises.length > 0 ? filters.remises.map((el: any) => el.name) : [],
    });
  };

  const handleSortChange = () => {
    setSort((prevState) => (prevState === 'asc' ? 'desc' : 'asc'));
    setLocalSort({
      label: 'tri',
      name: filters.tri[0].name,
      direction: sort,
      active: true,
    });
  };

  const handleMyPharmacieChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target;

    setMyPharmacieFilter(checked);
  };

  const handleCreateRemise = () => {
    createRemise({
      variables: {
        input: {
          //TODO : idProduitCanals
          idProduitCanals: (selected || []).map((produitCanal: any) => produitCanal.id),
          source: remiseSource,
          ...remiseFormValues,
          remiseDetails: remiseFormValues.remiseDetails.map((remiseDetail) => ({
            ...remiseDetail,
            id: undefined,
          })),
        },
      },
    });
  };

  const btnWithSelection = (
    <>
      <Box>
        <NewCustomButton
          theme={view === 'card' ? 'primary' : 'transparent'}
          variant={view === 'card' ? 'contained' : 'outlined'}
          onClick={handleChangeView}
          startIcon={<ViewModule />}
          className={classes.viewButton}
        >
          Vue colonne
        </NewCustomButton>
        <NewCustomButton
          theme={view === 'list' ? 'primary' : 'transparent'}
          variant={view === 'list' ? 'contained' : 'outlined'}
          onClick={handleChangeView}
          startIcon={<ViewList />}
        >
          Vue tableau
        </NewCustomButton>
      </Box>
      <Box>
        {checkedItems && checkedItems.length > 0 && (
          <NewCustomButton
            className={classes.deleteButton}
            startIcon={<Delete />}
            onClick={handleOpenConfirmDialog}
            disabled={!auth.isAuthorizedToDeleteProduct()}
          >
            Supprimer la selection
          </NewCustomButton>
        )}

        {isInGestionProduits && (
          <Box display="flex">
            {auth.isAuthorizedToAddProduct() && (
              <NewCustomButton
                style={{ marginLeft: 5 }}
                startIcon={<Add />}
                onClick={handleCreateProduit}
                disabled={!auth.isAuthorizedToAddProduct()}
              >
                Ajouter un produit
              </NewCustomButton>
            )}
            {showGestionProduitsBtns && (
              <>
                <NewCustomButton
                  startIcon={<Add />}
                  style={{ marginLeft: 16 }}
                  name="remise"
                  onClick={() => handleOpenRemiseModal('PERMANENT')}
                >
                  Ajouter une remise
                </NewCustomButton>
                <NewCustomButton
                  style={{ marginLeft: 16 }}
                  startIcon={<Add />}
                  name="promotion"
                  onClick={() => handleOpenRemiseModal('OPERATION')}
                >
                  Ajouter une promotion
                </NewCustomButton>
              </>
            )}
          </Box>
        )}
        {isInOperationCommande && <ButtonGoToCommand />}

        <ConfirmDeleteDialog
          title={`Suppression de la sélection (${(checkedItems && checkedItems.length) || 0})`}
          content="Êtes-vous sûr de vouloir supprimer ces produits"
          open={openConfirmDialog}
          setOpen={setOpenConfirmDialog}
          onClickConfirm={handleDelete}
          isLoading={doDeleteArticlesResult.loading}
        />
      </Box>
    </>
  );

  const theme = useTheme();
  const match = useMediaQuery(theme.breakpoints.down('sm'));

  const btnWithoutSelection = (
    <Box display="flex">
      {!isInLaboratoireProduits && (
        <Box display="flex" alignItems="center">
          {idOperation && <ButtonGoToCommand />}
          <Typography className={classes.affichage}>Type d'affichage :</Typography>
        </Box>
      )}

      <Box marginLeft="10px" alignItems="center" display="flex">
        {match ? (
          <>
            {isInLaboratoireProduits && (
              <>
                <IconButton onClick={handleSortChange}>
                  {sort === 'asc' ? <FilterList /> : <FilterList style={{ transform: 'rotate(180deg)' }} />}
                </IconButton>
                <Box margin="0px 16px">
                  <Filter
                    filters={[]}
                    children={<ProduitFilter setFilters={setFilters} filters={filters} />}
                    onChange={handleFilterChange}
                    filterIcon={<img src={FilterAlt} />}
                  />
                </Box>
                <SearchContent placeholder="Rechercher un produit" />
              </>
            )}
          </>
        ) : (
          <>
            <IconButton
              color={view === 'card' ? 'primary' : 'default'}
              edge="start"
              className={classNames(classes.menuButton)}
              onClick={handleChangeView}
            >
              <ViewModuleIcon />
            </IconButton>
            <IconButton
              color={view === 'list' ? 'primary' : 'default'}
              edge="start"
              className={classNames(classes.menuButton)}
              onClick={handleChangeView}
            >
              <ViewListIcon />
            </IconButton>
            {isInLaboratoireProduits && (
              <>
                <IconButton onClick={handleSortChange}>
                  {sort === 'asc' ? <FilterList /> : <FilterList style={{ transform: 'rotate(180deg)' }} />}
                </IconButton>
                <Box margin="0px 16px">
                  <Filter
                    filters={[]}
                    children={<ProduitFilter setFilters={setFilters} filters={filters} />}
                    onChange={handleFilterChange}
                    filterIcon={<img src={FilterAlt} />}
                  />
                </Box>

                <IconButton onClick={() => refetch()} disabled={!refetch}>
                  <Replay />
                </IconButton>
                <SearchContent placeholder="Rechercher un produit" />
              </>
            )}
          </>
        )}
      </Box>
    </Box>
  );

  return (
    <Box className={classes.root}>
      {match ? (
        <Box>
          <Box className={classes.headerMobile}>
            <Typography>
              {isInGestionProduits || idOperation ? 'Liste des produits' : 'Liste de tous les produits'}
            </Typography>
            <Box className={classes.icons}>
              {/* <Box>
                <SortIcon />
              </Box> */}
              <IconButton onClick={handleSortChange}>
                {sort === 'asc' ? <FilterList /> : <FilterList style={{ transform: 'rotate(180deg)' }} />}
              </IconButton>
              <Box>
                <Filter
                  filters={[]}
                  children={<ProduitFilter setFilters={setFilters} filters={filters} />}
                  onChange={handleFilterChange}
                  filterIcon={<img src={FilterAlt} />}
                />
              </Box>
            </Box>
          </Box>
          <SearchContent placeholder="Rechercher un produit" />
          <Box className={classes.divider}>
            <Divider />
          </Box>
        </Box>
      ) : (
        <AppBar position="static" className={classes.appbar}>
          <Toolbar className={classes.toolbar}>
            {state && (state as any).idLaboratoire && <ArrowBack style={{ cursor: 'pointer' }} onClick={clickBack} />}
            <Typography className={classes.title}>
              {isInGestionProduits || idOperation ? 'Liste des produits' : 'Liste de tous les produits'}
            </Typography>
            <Box display="flex" marginBottom="8px">
              <FormControlLabel
                //disabled={listResult.loading}
                control={
                  <Switch
                    checked={myPharmacieFilter.data?.myPharmacieFilter ? true : false}
                    onChange={handleMyPharmacieChange}
                  />
                }
                label="Mon Catalogue Produit"
              />
              {listResult?.data?.search?.data && (isInGestionProduits || idOperation) ? (
                btnWithSelection
              ) : listResult?.data?.search?.data ? (
                btnWithoutSelection
              ) : (
                <></>
              )}
            </Box>
          </Toolbar>
        </AppBar>
      )}
      {/* <CreationProduit openFormModal={openFormModal} setOpenFormModal={setOpenFormModal} refetch={listResult.refetch} /> */}

      <CustomFullScreenModal
        open={openRemiseModal}
        setOpen={setOpenRemiseModal}
        title={remiseSource === 'PERMANENT' ? 'Remise' : 'Promotion'}
        withBtnsActions={true}
        disabledButton={creationRemise.loading}
        actionButton="Ajouter"
        onClickConfirm={handleCreateRemise}
      >
        <RemiseForm values={remiseFormValues} onChange={setRemiseFormValues} />
      </CustomFullScreenModal>
    </Box>
  );
};

export default AppBarFilter;
