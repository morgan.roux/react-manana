import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';
import loadable from '@loadable/component';
import React from 'react';
import CatalogueProduit from './assets/img/mon_catalogue_produit.png';

const CatalogueProduitPage = loadable(() => import('./pages/CatalogueProduit'), {
  fallback: <SmallLoading />,
});

const produitModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PRODUIT',
      location: 'ACCUEIL',
      name: 'Catalogues des produits',
      to: '/catalogue-produits/card',

      icon: CatalogueProduit,
      activationParameters: {
        groupement: '0303',
        pharmacie: '0706',
      },
      preferredOrder: 30,
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
  routes: [
    {
      path: ['/catalogue-produits/:view', '/catalogue-produits/:view/:id'],
      component: CatalogueProduitPage,
      exact: true,
      attachTo: 'MAIN',
    },
  ],
};

export default produitModule;
