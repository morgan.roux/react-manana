import React from 'react';
import PartenaireIcon from '@material-ui/icons/BusinessCenter';
import loadable from '@loadable/component';
import PrestataireService from './assets/img/prestataires_partenaires.png';

import { ModuleDefinition } from '@lib/common';

const prestataireServiceModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARTENAIRE_SERVICE',
      location: 'ACCUEIL',
      name: 'Prestataires partenaires',
      to: '/prestataires-partenaires',
      icon: PrestataireService,
      activationParameters: {
        groupement: '0306',
        pharmacie: '0709',
      },
      preferredOrder: 170,
    } /*,

    {
      id: 'FEATURE_PARAMETRE_PARTENAIRE_SERVICE',
      location: 'PARAMETRE',
      name: 'Partenaires de Services',
      to: '/partenaires-services',
      icon: <PartenaireIcon />,
      preferredOrder: 270,
      authorize: {
        roles: ['SUPADM', 'GRPADM', 'PRMTIT'],
      },
    },*/,
  ],
};

export default prestataireServiceModule;
