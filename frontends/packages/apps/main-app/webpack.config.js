const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const ModuleFederationPlugin = require('webpack').container.ModuleFederationPlugin;
const WebpackPwaManifest = require('webpack-pwa-manifest');
const WorkboxPlugin = require('workbox-webpack-plugin');
const path = require('path');
const deps = require('./package.json').dependencies;

const singletonDeps = Object.keys(deps).reduce((singletonResult, name) => {
  return {
    ...singletonResult,
    [name]: {
      singleton: true,
      requiredVersion: deps[name],
    },
  };
}, {});

module.exports = {
  entry: './src/index',
  mode: 'development',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    port: 3050,
  },
  watchOptions: {
    ignored: /node_modules/,
  },
  output: {
    publicPath: '/',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js'],
  },
  module: {
    rules: [
      {
        test: /bootstrap\.tsx$/,
        loader: 'bundle-loader',
        options: {
          lazy: true,
        },
      },
      {
        test: /\.tsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: [
            '@babel/preset-react',
            '@babel/preset-typescript',
            ['@babel/preset-env', { useBuiltIns: 'usage', corejs: 3, targets: '> 0.25%, not dead', modules: false }],
          ],
          plugins: [
            '@babel/proposal-class-properties',
            '@babel/transform-runtime' /*, {
            "regenerator": true,
            "corejs": 3
          }]*/,
          ],
        },
      },

      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [
    new CopyPlugin({
      patterns: [{ from: 'public/env-config.json', to: 'env-config.json' }],
    }),
    new ModuleFederationPlugin({
      name: 'main',
      library: { type: 'var', name: 'main' },
      shared: {
        ...singletonDeps,
        react: {
          eager: true,
          singleton: true,
          requiredVersion: deps.react,
        },
        'react-dom': {
          eager: true,
          singleton: true,
          requiredVersion: deps['react-dom'],
        },
        '@material-ui/core': {
          eager: true,
          singleton: true,
          requiredVersion: deps['@material-ui/core'],
        },
        '@material-ui/styles': {
          eager: true,
          singleton: true,
        },
        '@lib/ui-kit': {
          singleton: true,
        },
        '@lib/common': {
          singleton: true,
        },
      },
    }),
    new HtmlWebpackPlugin({
      favicon: './public/digital4win.png',
      template: './public/index.ejs',
    }),
    new WorkboxPlugin.GenerateSW({
      // these options encourage the ServiceWorkers to get in there fast
      // and not allow any straggling "old" SWs to hang around
      clientsClaim: true,
      skipWaiting: true,
    }),
    new WebpackPwaManifest({
      name: 'Digital4win',
      short_name: 'Digital4win',
      description: 'Digital4win - Bootsteur de performance',
      background_color: '#ffffff',
      crossorigin: 'use-credentials', // can be null, use-credentials or anonymous
      icons: [
        {
          src: path.resolve('public/icon.png'),
          sizes: [96, 128, 192, 256, 384, 512], // multiple sizes
        },
        /* {
          src: path.resolve('src/assets/large-icon.png'),
          size: '1024x1024' // you can also use the specifications pattern
        },
        {
          src: path.resolve('src/assets/maskable-icon.png'),
          size: '1024x1024',
          purpose: 'maskable'
        } */
      ],
    }),
  ],
};
