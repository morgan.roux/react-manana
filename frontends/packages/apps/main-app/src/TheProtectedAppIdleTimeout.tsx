import React, { FC } from 'react';
import { useTrackingTimer, useApplicationContext } from '@lib/common';
import { useLogoutMutation } from '@lib/common/src/federation';
import { IdleTimerAPI, useIdleTimer } from 'react-idle-timer';
import { useState } from 'react';
import { useHistory } from 'react-router';
import IdleTimeoutModal from './IdleTimeout';

interface TheProtectedAppIdleTimeoutState {
  timeout: number;
  openModal: boolean;
  isTimedOut: boolean;
}

const TIMEOUT_IN_MILLS = 45 * 60 * 1000;
const DELAY_IN_MILLS = 5 * 60 * 1000;

const TheProtectedAppIdleTimeout: FC<{}> = ({}) => {
  const { federation, ip, accessToken } = useApplicationContext();
  const history = useHistory();
  const create = useTrackingTimer();
  const [values, setValues] = useState<TheProtectedAppIdleTimeoutState>({
    timeout: TIMEOUT_IN_MILLS,
    openModal: false,
    isTimedOut: false,
  });

  const doLogout = () => {
    localStorage.clear();
    history.push('/signin');
    window.location.reload();
  };

  const [logout] = useLogoutMutation({
    client: federation,
    onCompleted: doLogout,
    onError: doLogout,
  });

  const register = (
    {
      getElapsedTime,
      getLastActiveTime,
      getLastIdleTime,
      getRemainingTime,
      getTotalActiveTime,
      getTotalIdleTime,
    }: IdleTimerAPI,
    type: 'IDLE' | 'ACTIVE' | 'ACTION'
  ) => {
    if (type !== 'ACTION') {
      create({
        type,
        remainingTime: getRemainingTime(),
        elaspedTime: getElapsedTime(),
        lastIdleTime: getLastIdleTime(),
        totalIdleTime: getTotalIdleTime(),
        lastActiveTime: getLastActiveTime(),
        totalActiveTime: getTotalActiveTime(),
      });
    }
  };

  const handleLogout = (autoClosed: boolean) => {
    logout({
      variables: {
        input: {
          ip,
          accessToken: accessToken as any,
          autoClosed,
        },
      },
    });
  };

  const handleAction = () => {
    register(idleTimer, 'ACTION');
  };

  const handleActive = () => {
    register(idleTimer, 'ACTIVE');
  };

  const handleIdle = () => {
    register(idleTimer, 'IDLE');
    if (values.isTimedOut) {
      handleLogout(true);
    } else {
      setValues((prev) => ({ ...prev, openModal: true, isTimedOut: true, timeout: DELAY_IN_MILLS }));
      idleTimer.reset();
    }
  };

  const handleClose = () => {
    setValues((prev) => ({ ...prev, openModal: false, isTimedOut: false, timeout: TIMEOUT_IN_MILLS }));
    idleTimer.reset();
  };

  const idleTimer = useIdleTimer({
    startOnMount: true,
    timeout: values.timeout,
    debounce: 250,
    onActive: handleActive,
    onIdle: handleIdle,
    onAction: handleAction,
  });

  return (
    <IdleTimeoutModal
      open={values.openModal}
      onRequestClose={handleClose}
      onRequestLogout={() => handleLogout(false)}
    />
  );
};

export default TheProtectedAppIdleTimeout;
