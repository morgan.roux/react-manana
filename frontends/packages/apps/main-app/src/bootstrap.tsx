import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';

ReactDOM.render(<App />, document.getElementById('root'));

if ('serviceWorker' in navigator) {
  caches.keys().then(function (cacheNames) {
    console.log('************************Cache names ', cacheNames);

    cacheNames.forEach(function (cacheName) {
      caches.delete(cacheName);
    });
  });
  /*window.addEventListener('load', () => {
    navigator.serviceWorker
      .register('/service-worker.js')
      .then((registration) => {
        console.log('SW registered: ', registration);
      })
      .catch((registrationError) => {
        console.log('SW registration failed: ', registrationError);
      });
  });*/
}
