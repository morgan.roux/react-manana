import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/page-navigation/lib/styles/index.css';
import moment from 'moment';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import HttpApi from 'i18next-http-backend';
import { i18nextPlugin } from 'translation-check';

import { getBasePath } from './util';

moment.locale('fr');

i18n
  .use(i18nextPlugin)
  .use(initReactI18next) // passes i18n down to react-i18next
  .use(HttpApi)
  .init({
    lng: 'fr',
    fallbackLng: 'fr',
    preload: ['fr', 'en'],
    ns: ['translation'],
    defaultNS: 'translation',
    saveMissing: true,
    backend: {
      loadPath: '/locales/{{ns}}/{{lng}}.json',
      addPath: '/locales/add/{{ns}}/{{lng}}',
      customHeaders: {
        'Content-Type': 'application/json',
      },
      request: async (options, url, payload, callback) => {
        try {
          const basePath = await getBasePath();
          return fetch(`${basePath}${url}`, {
            method: payload ? 'POST' : 'GET',
            body: payload ? JSON.stringify(payload) : undefined,
            headers: payload
              ? typeof options.customHeaders === 'function'
                ? options.customHeaders()
                : options.customHeaders
              : undefined,
            cache: 'no-store',
          }).then((response) => {
            if (!response.ok)
              return callback(response.statusText || 'Error', { status: response.status, data: 'Error fetching data' });
            response
              .text()
              .then((data) => {
                callback(null, { status: response.status, data });
              })
              .catch((error) => {
                callback(error, { status: 400, data: 'Error fetching data' });
              });
          });
        } catch (error) {
          return callback(error, { status: 400, data: 'Error fetching config' });
        }
      },
    },
    react: {
      useSuspense: true,
    },
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
