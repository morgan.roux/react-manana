import { useState, ChangeEvent } from 'react';
import { Create_GroupementMutationVariables } from '@lib/common/src/graphql';

const useForm = (
  callback: (data: Create_GroupementMutationVariables) => void,
  defaultState?: Create_GroupementMutationVariables
) => {
  const initialState: Create_GroupementMutationVariables = defaultState || {
    nom: '',
    adresse1: '',
    adresse2: '',
    cp: '',
    ville: '',
    pays: '',
    telBureau: '',
    telMobile: '',
    mail: '',
    site: '',
    commentaire: '',
    nomResponsable: '',
    prenomResponsable: '',
    dateSortie: null,
    sortie: null,
    isReference: false,
  };

  const [values, setValues] = useState<Create_GroupementMutationVariables>(initialState);

  const handleSubmit = (e: any) => {
    if (e) e.preventDefault();
    callback(values);
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const checkedFields = ['isReference'];
    const { name, value, checked } = e.target;
    e.persist();
    if (checkedFields.includes(name)) {
      setValues((prevState) => ({ ...prevState, [name]: checked }));
    } else {
      setValues((prevState) => ({ ...prevState, [name]: value }));
    }
  };

  return {
    handleChange,
    handleSubmit,
    values,
  };
};

export default useForm;
