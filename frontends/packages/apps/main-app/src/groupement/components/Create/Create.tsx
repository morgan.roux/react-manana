import React, { FC } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core';
import { SinglePage, SmallLoading } from '@lib/ui-kit';
import { useCreate_GroupementMutation, Create_GroupementMutationVariables } from '@lib/common/src/graphql';
import { useGroupementLazyQuery } from '@lib/common/src/federation';
import Form from '../Form';
import { ApolloClient } from '@apollo/client';
import { useCurrentGroupementWithProvidedClient } from '@lib/common';
import { setGroupement, getGroupement } from '@lib/common/src/auth/auth-util';
import { useHistory } from 'react-router';

const styles = () =>
  createStyles({
    padding: {
      paddingTop: 30,
    },
  });

interface CreateGroupement {
  graphql: ApolloClient<any>;
  federation: ApolloClient<any>;
}

const Create: FC<CreateGroupement & WithStyles> = ({ classes, graphql, federation }) => {
  const history = useHistory();
  const [currentGroupement, setCurrentGroupement] = useCurrentGroupementWithProvidedClient(graphql);

  const [loadGroupement, loadingGroupement] = useGroupementLazyQuery({
    client: federation,
    onCompleted: (data) => {
      if (data.groupement) {
        setCurrentGroupement(data.groupement);
        setGroupement(data.groupement);

        if (getGroupement()) {
          history.push('/');
          window.location.reload();
        }
      }
    },
  });

  const [createGroupement, { loading }] = useCreate_GroupementMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createGroupement) {
        loadGroupement({
          variables: {
            id: data.createGroupement.id,
          },
        });
      }
    },
  });

  const submitCreate = (data: Create_GroupementMutationVariables) => {
    createGroupement({ variables: { ...data } });
  };

  if (loading || loadingGroupement.loading) return <SmallLoading />;

  return (
    <div className={classes.padding}>
      <SinglePage title="Créer un nouveau groupement" maxWidth="600px">
        <Form action={submitCreate} />
      </SinglePage>
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(Create);
