import React, { FC } from 'react';
import { withStyles, createStyles } from '@material-ui/core';
import { SinglePage } from '@lib/ui-kit';
import Choice from './ChoiceGroupement';
import { ChoiceGroupementProps } from './ChoiceGroupement/ChoiceGroupement';

const styles = () => createStyles({});

const Groupement: FC<ChoiceGroupementProps> = (props) => {
  return (
    <SinglePage title="Choix du groupement">
      <Choice {...props} />
    </SinglePage>
  );
};

export default withStyles(styles, { withTheme: true })(Groupement);
