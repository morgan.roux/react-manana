import React, { FC } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core';
import { SinglePage, SmallLoading } from '@lib/ui-kit';
import { useUpdate_GroupementMutation, Update_GroupementMutationVariables } from '@lib/common/src/graphql';
import { useGroupementLazyQuery } from '@lib/common/src/graphql';

import Form from '../Form';
import { useHistory, useLocation } from 'react-router-dom';

import { useApplicationContext, useCurrentGroupementWithProvidedClient } from '@lib/common';

const styles = () => createStyles({});

const Update: FC<WithStyles> = ({}) => {
  const location = useLocation() as any;
  const history = useHistory();
  const { graphql, federation } = useApplicationContext();
  const [currentGroupement, setCurrentGroupement] = useCurrentGroupementWithProvidedClient(graphql);

  const [loadGroupement, loadingGroupement] = useGroupementLazyQuery({
    client: federation,
    onCompleted: (data) => {
      if (data.groupement) {
        setCurrentGroupement(data.groupement as any);
        history.push('/');
      }
    },
  });
  const [updateGroupement, { loading }] = useUpdate_GroupementMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.updateGroupement) {
        loadGroupement({
          variables: {
            id: data.updateGroupement.id,
          },
        });
      }
    },
  });

  const submitUpdate = (data: Update_GroupementMutationVariables) => {
    if (location && location.state && location.state.id) {
      updateGroupement({ variables: { ...data, id: location.state.id } });
    }
  };

  if (loading || loadingGroupement.loading) return <SmallLoading />;

  return (
    <SinglePage
      title={`Modification du groupement ${(location && location.state && location.state.nom) || ''}`}
      maxWidth="unset"
    >
      <Form defaultData={(location && location.state) || undefined} action={submitUpdate} />
    </SinglePage>
  );
};

export default withStyles(styles, { withTheme: true })(Update);
