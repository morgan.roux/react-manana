import React, { useState, ChangeEvent, FC } from 'react';
import { Fab, Theme, makeStyles, createStyles, InputAdornment } from '@material-ui/core';
import GroupIcon from '@material-ui/icons/Group';
import AddIcon from '@material-ui/icons/Add';
import { CustomButton, NewCustomButton } from '@lib/ui-kit';
import { useGroupementsQuery } from '@lib/common/src/federation';
import { useCurrentGroupementWithProvidedClient, useDisplayNotification } from '@lib/common';
import { setGroupement, getGroupement } from '@lib/common/src/auth/auth-util';

import CustomSelect from './CustomSelectChoixGroupement';
import { useHistory } from 'react-router';
import { ApolloClient } from '@apollo/client';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      marginBottom: 65,
      justifyContent: 'space-between',
      '& .MuiInputBase-adornedStart': {
        marginTop: '0!important',
        paddingLeft: 14,
      },
      '& .MuiSelect-select': {
        padding: '16px 14px',
        minHeight: 30,
      },
    },
    form: {
      width: '100%',
      '& button': {
        marginTop: 80,
      },
    },
    addGroupementBtn: {
      background: 'linear-gradient(to right, rgba(166,207,53,1) 0%, rgba(139,198,63,1) 100%)',
      height: 24,
      minHeight: 24,
      maxHeight: 24,
      width: 24,
      minWidth: 24,
      maxWidth: 24,
      marginLeft: 20,
      marginTop: 16,
      color: '#ffffff',
      '&:hover': {
        opacity: 0.8,
      },
    },
  })
);

export interface ChoiceGroupementProps {
  graphql: ApolloClient<any>;
  federation: ApolloClient<any>;
}

const ChoiceGroupement: FC<ChoiceGroupementProps> = ({ graphql, federation }) => {
  const classes = useStyles({});
  const history = useHistory();
  const [id, setId] = useState<string>('');
  const notify = useDisplayNotification(graphql);
  const [currentGroupement, setCurrentGroupement] = useCurrentGroupementWithProvidedClient(graphql);

  const { data, loading } = useGroupementsQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
    onError: () => {
      notify({
        type: 'error',
        message: `Une erreur s'est produite`,
      });
    },
  });

  const groupements = data?.groupements;

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    if (value) {
      setId(value);
    }
  };

  const isDisabled = () => {
    if (!id) return true;
    return false;
  };

  const handleNext = () => {
    const selectedGroupement = (groupements || []).find((grp) => grp && id === grp.id);
    if (selectedGroupement) {
      setCurrentGroupement(selectedGroupement);
      setGroupement(selectedGroupement);

      if (getGroupement()) {
        window.location.reload();
      }
    }
  };

  const goToCreateGroupement = () => history.push('/groupement/new');

  return (
    <div className={classes.content}>
      <div className={classes.form}>
        <CustomSelect
          list={groupements}
          listId="id"
          index="nom"
          variant="outlined"
          disabled={loading}
          value={id}
          onClick={handleChange}
          startAdornment={
            <InputAdornment position="start">
              <GroupIcon />
            </InputAdornment>
          }
        />
        <NewCustomButton size="large" onClick={handleNext} fullWidth={true} disabled={isDisabled()}>
          SUIVANT
        </NewCustomButton>
      </div>
      <Fab size="small" aria-label="add" className={classes.addGroupementBtn} onClick={goToCreateGroupement}>
        <AddIcon />
      </Fab>
    </div>
  );
};

export default ChoiceGroupement;
