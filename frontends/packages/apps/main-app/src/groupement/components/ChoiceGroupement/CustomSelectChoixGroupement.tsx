import React, { FC } from 'react';
import { MenuItem, FormControl, Select, withStyles, createStyles, InputLabel } from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';

const styles = () =>
  createStyles({
    formControl: {
      width: '100%',
      backgroundColor: '#EBEBEB',
      '& fieldset': {
        border: 0,
      },
      '& .MuiSelect-select': {
        paddingLeft: 10,
      },
      '& .MuiInputAdornment-positionStart': {
        marginRight: 13,
      },
      '& .MuiPaper': {
        marginTop: 50,
        backgroundColor: 'red',
      },
      '& .MuiSelect-root': {
        display: 'flex',
        alignItems: 'center',
      },
    },
    dropdownStyle: {
      width: 355,
      marginLeft: -51,
      maxHeight: 300,
      '@media (max-height: 661px)': {
        maxHeight: 200,
      },
      '@media (max-width: 442px)': {
        width: '78%',
        maxHeight: 250,
        marginLeft: 0,
        left: '25px!important',
      },
      '@media (max-width: 398px)': {
        width: '75%',
      },
      '@media (max-width: 334px)': {
        width: '71%',
      },
    },
  });

const CustomSelect: FC<any> = ({ ...props }) => {
  const { classes, name, index, listId, list, error, variant } = props;

  return (
    <FormControl className={classes.formControl} variant={variant} error={error}>
      <InputLabel htmlFor="name">{name}</InputLabel>
      <Select
        {...props}
        className={classes.select}
        MenuProps={{
          getContentAnchorEl: null,
          anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'left',
          },
          classes: { paper: classes.dropdownStyle },
        }}
      >
        {list &&
          list.map((item: any, key: number) => {
            const val = typeof item === 'object' ? item[listId] : item;
            const show = typeof item === 'object' ? item[index] : item;
            return (
              <MenuItem key={key} value={val} className={classes.menuItem}>
                {show}
              </MenuItem>
            );
          })}
      </Select>
    </FormControl>
  );
};

export default withStyles(styles, { withTheme: true })(CustomSelect);

CustomSelect.defaultProps = {
  list: [],
  name: '',
  variant: 'outlined',
  error: false,
};
