import React from 'react';
import GroupementIcon from '@material-ui/icons/SupervisedUserCircle';
import GroupIcon from '@material-ui/icons/Group';
import loadable from '@loadable/component';
import { ModuleDefinition } from '@lib/common';
import { SmallLoading } from '@lib/ui-kit';

const GroupementPage = loadable(() => import('./pages/Groupement/Groupement'), {
  fallback: <SmallLoading />,
});

const groupementModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_GROUPEMENT',
      location: 'PARAMETRE',
      name: 'Groupement',
      to: '/groupement',
      icon: <GroupementIcon />,
      preferredOrder: 160,
      authorize: {
        roles: ['SUPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_PARAMETRE_PERSONNEL_GROUPEMENT',
      location: 'PARAMETRE',
      name: 'Liste du Personnel du groupement',
      to: '/personnel-groupement',
      icon: <GroupIcon />,
      preferredOrder: 170,
      authorize: {
        roles: ['SUPADM', 'GRPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],

  routes: [
    {
      path: '/groupement',
      component: GroupementPage,
      authorize: {
        roles: ['SUPADM'],
        rolesDenied: ['PRTSERVICE']
      },
      attachTo: 'PARAMETRE',
    },
  ],
};

export default groupementModule;
