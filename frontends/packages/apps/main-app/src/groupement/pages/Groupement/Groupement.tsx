import React, { FC, useEffect, useState } from 'react';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import style from './style';
import {
  Update_GroupementMutationVariables,
  useUpdate_GroupementMutation,
  TypeFichier,
  useGroupementQuery as useGraphqlGroupementQuery,
} from '@lib/common/src/graphql';
import { useGroupementLazyQuery } from '@lib/common/src/federation';
import { SmallLoading } from '@lib/ui-kit';
import Form from '../../components/Form';
import { AxiosResponse } from 'axios';
import { useApplicationContext } from '@lib/common';
import { useCurrentGroupementWithProvidedClient } from '@lib/common';

export const DIRECTORY = 'avatars';

const Groupement: FC<WithStyles> = ({ classes }) => {
  const [myGroupement, setMyGroupement] = useState<any>();
  const [datas, setDatas] = useState<Update_GroupementMutationVariables | null>(null);
  const [startUpload, setStartUpload] = useState<boolean>(false);
  const [fileUploadResult, setFileUploadResult] = useState<AxiosResponse<any> | null>(null);
  const [files, setFiles] = useState<File[]>([]);
  const [fileAlreadySet, setFileAlreadySet] = useState<string | undefined>();
  const { currentGroupement, notify, graphql, federation, computeUploadedFilename, config } = useApplicationContext();

  const [_, setCurrentGroupement] = useCurrentGroupementWithProvidedClient(graphql);

  const { data, loading } = useGraphqlGroupementQuery({
    client: graphql,
    variables: {
      id: currentGroupement.id,
    },
  });

  const [loadGroupement, loadingGroupement] = useGroupementLazyQuery({
    client: federation,
    onCompleted: (data) => {
      notify({
        type: 'success',
        message: `Modification Groupement avec succès`,
      });

      data.groupement && setCurrentGroupement(data.groupement);
      window.location.reload();
    },
  });

  const [updateGroupement] = useUpdate_GroupementMutation({
    client: graphql,
    onCompleted: async (data) => {
      if (data && data.updateGroupement) {
        loadGroupement({
          variables: {
            id: data.updateGroupement.id,
          },
        });
      }
    },
    onError: (errors) => {
      notify({
        type: 'error',
        message: `Erreur lors de la modification du groupement`,
      });
    },
  });

  const submitUpdate = (incommingDatas: Update_GroupementMutationVariables) => {
    // Start the file upload
    setStartUpload(true);
    setDatas(incommingDatas);
  };

  useEffect(() => {
    if (
      fileAlreadySet === undefined &&
      files.length > 0 &&
      fileUploadResult &&
      fileUploadResult.status === 200 &&
      fileUploadResult.statusText === 'OK' &&
      datas
    ) {
      updateGroupement({
        variables: {
          ...datas,
          avatar: {
            chemin: computeUploadedFilename(files[0], { directory: DIRECTORY }),
            nomOriginal: files[0].name,
            type: TypeFichier.Photo,
          },
          id: currentGroupement.id,
        },
      });
    } else if (files.length === 0 && fileAlreadySet && datas) {
      updateGroupement({
        variables: {
          ...datas,
          id: currentGroupement.id,
        },
      });
    } else if (files.length === 0 && fileAlreadySet === undefined && datas) {
      updateGroupement({
        variables: {
          ...datas,
          avatar: null,
          id: currentGroupement.id,
        },
      });
    }
  }, [files, fileUploadResult, datas]);

  useEffect(() => {
    if (data && data.groupement) {
      if (
        data.groupement.groupementLogo &&
        data.groupement.groupementLogo.fichier &&
        data.groupement.groupementLogo.fichier.chemin
      ) {
        setFileAlreadySet(`${config.STORAGE_PUBLIC_BASE_PATH}/${data.groupement.groupementLogo.fichier.chemin}`);
      }

      const myGroupe = { ...data.groupement, pharmacies: [] };
      setMyGroupement(myGroupe);
    }
  }, [data]);

  if (loading) return <SmallLoading />;

  return myGroupement ? (
    <>
      <div className={classes.content}>
        <Form
          defaultData={myGroupement}
          action={submitUpdate}
          setFilesOut={setFiles}
          setFileUploadResult={setFileUploadResult}
          startUpload={startUpload}
          setStartUpload={setStartUpload}
          withImagePreview={true}
          uploadDirectory={DIRECTORY}
          fileAleadySetUrl={fileAlreadySet}
          setFileAlreadySet={setFileAlreadySet}
        />
      </div>
    </>
  ) : (
    <></>
  );
};

export default withStyles(style)(Groupement);
