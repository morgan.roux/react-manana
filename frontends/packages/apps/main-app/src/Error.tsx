import { Box, Button } from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import React, { FC } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import Logo from './core/assets/images/logo.png';

const ErrorPage: FC<{ error?: Error }> = ({ error }) => {
  const [show, setShow] = useState<boolean>(false);

  useEffect(() => {
    setTimeout(() => {
      setShow(true);
    }, 10000);
  }, []);

  return show ? (
    <Box
      style={{
        textAlign: 'center',
        height: '100vh',
        flexDirection: 'column',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Box>
        <img style={{ width: 100 }} src={Logo} alt="Digital4win" />
      </Box>
      <Alert style={{ margin: 10 }} variant="standard" severity="error">
        Une erreur s’est produite.
        <br />
        Ceci peut se produire en raison d’une erreur technique que nous sommes en train de résoudre. Essayez de
        recharger cette page.
      </Alert>
      <Box>
        <Button color="secondary" variant="contained" onClick={() => window.location.reload()}>
          Actualiser la page
        </Button>
      </Box>
    </Box>
  ) : null;
};

export default ErrorPage;
