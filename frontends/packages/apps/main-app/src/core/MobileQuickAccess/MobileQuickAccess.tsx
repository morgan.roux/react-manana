import { Feature, filterFeatures, getOptionValue, useApplicationContext } from '@lib/common';
import { AppBar, Typography } from '@material-ui/core';

import React, { FC, ReactNode, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router';
import { useStyles } from './styles';

interface MobileQuickAccessProps {}

const MobileQuickAccess: FC<MobileQuickAccessProps> = ({}) => {
  const classes = useStyles({});
  const appContext = useApplicationContext();
  const history = useHistory();
  const location = useLocation();

  const { features } = appContext;
  const [enabledQuickAccessFeatures, setEnabledQuickAccessFeatures] = useState<Feature[]>([]);

  useEffect(() => {
    setEnabledQuickAccessFeatures(filterFeatures(features, { location: 'MOBILE_QUICK_ACCESS' }));
  }, [features]);

  return (
    <AppBar position="fixed" className={classes.root}>
      {enabledQuickAccessFeatures.map((feature) => (
        <div
          key={`menuItem-${feature.id}`}
          className={
            feature.options?.active && feature.options?.active(location.pathname, appContext)
              ? classes.active
              : classes.menuItemRoot
          }
          onClick={() =>
            feature.options?.onClick
              ? feature.options.onClick()
              : feature.to
              ? history.push(getOptionValue(feature.to, appContext))
              : undefined
          }
        >
          {feature.icon}
          <Typography>{feature.name || ''}</Typography>
          {feature.component && feature.component}
        </div>
      ))}
    </AppBar>
  );
};

export default MobileQuickAccess;
