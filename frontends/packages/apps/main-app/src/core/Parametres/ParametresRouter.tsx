import React, { FC } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Feature, filterRoutes, ProtectedRouteProps } from '@lib/common';
import { PARAMETRES_ROUTES_PREFIX } from '@lib/common';

interface ParametresRouterProps {
  features: Feature[];
  routes: ProtectedRouteProps[];
}

const pathWithPrefix = (path: string | readonly string[] | undefined): string | string[] | undefined => {
  if (!path) {
    return path;
  }

  if (Array.isArray(path)) {
    return path.map((p) => `${PARAMETRES_ROUTES_PREFIX}${p}`);
  }

  return `${PARAMETRES_ROUTES_PREFIX}${path}`;
};

const ParametresRouter: FC<ParametresRouterProps> = ({ routes }) => {
  const filteredRoutes = filterRoutes(routes, { attachTo: 'PARAMETRE' });

  return (
    <Switch>
      {filteredRoutes.map(({ path, component }) => (
        <Route
          path={pathWithPrefix(path)}
          exact={true}
          key={`parametre-route-${path}`}
          strict={true}
          component={component as any}
        />
      ))}
    </Switch>
  );
};

export default ParametresRouter;
