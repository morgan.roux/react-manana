import React, { FC, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { useLoginMutation } from '@lib/common/src/federation';
import { getDevice } from '@lib/common/src/device-detector';

import { useTrackingEvent, useDisplayNotification } from '@lib/common';
import { getIp } from '@lib/common/src/ip-detector';
import {
  getAccessToken,
  setAccessToken,
  setRequestChooseGroupement,
  setRequestUpdatePassword,
  setRequestUpdatePasswordReason,
} from '@lib/common/src/auth/auth-util';
import moment from 'moment';
import { SigninPage } from '../components';

const Page: FC<{}> = () => {
  const history = useHistory();
  const trackEvent = useTrackingEvent();
  const displayNotification = useDisplayNotification();
  const [loading, setLoading] = useState<boolean>(false);

  const [executeLogin] = useLoginMutation({
    onCompleted: (response) => {
      setRequestChooseGroupement(!!(response.login?.user?.role?.code === 'SUPADM'));

      if (!!(response.login?.user?.status === 'RESETED')) {
        setRequestUpdatePassword(true);
        setRequestUpdatePasswordReason('RESETED');
      } else if (
        !response.login?.user?.lastPasswordChangedDate ||
        moment(Date.now()).diff(moment(response.login.user.lastPasswordChangedDate), 'day') > 180
      ) {
        setRequestUpdatePassword(true);
        setRequestUpdatePasswordReason('EXPIRED');
      }
      setAccessToken(response.login?.accessToken || '');

      if (getAccessToken()) {
        history.push('/');
        window.location.reload();
      }
    },
    onError: (error) => {
      console.log('eror', error);
      let errorMessage: string = 'Erreur du serveur';
      error.graphQLErrors.forEach((error) => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'ACCOUNT_NOT_EXIST':
              errorMessage = `Veuillez contacter l’administrateur du site : compte ou mot de passe incorrect`;
              break;
            case 'PASSWORD_INVALID':
              errorMessage = 'Mot de passe incorrect';
              break;
            case 'GROUPMENT_EXITED':
              errorMessage = 'Votre groupement est sortie, veuillez contacter votre administrateur';
              break;
            case 'PERSONNEL_EXITED':
              errorMessage =
                'Il semble que vous ne fait plus partie du personnel, veuillez contacter votre administrateur';
              break;
            case 'PPERSONNEL_EXITED':
              errorMessage =
                'Il semble que vous ne fait plus partie du personnel du pharmacie, veuillez contacter votre administrateur';
              break;
            case 'USER_BLOCKED':
              errorMessage = 'Veuillez contacter l’administrateur du site : compte a été bloqué';
              break;
            case 'USER_BLOCKED_OLD_LAST_LOGIN':
              errorMessage =
                'Veuillez contacter l’administrateur du site : votre compte a été bloqué pour cause d’inactivité';
              break;
            case 'ACTIVATION_REQUIRED':
              errorMessage = "Votre compte n'est pas encore activé, veuillez l'activer";
              break;
            case 'USE_SEND_GRID':
              errorMessage = "Utiliser sendGrid pour l'envoi du mail";
              break;
            case 'MAIL_IP_NOT_SENT':
              errorMessage = "Erreur lors de l'envoi de mail de confirmation d'adresse IP";
              break;
            case 'MAIL_CONFIRM_IP_SENT':
              errorMessage =
                "Un mail de confirmation d'adresse IP vous a été envoyé pour pouvoir s'assurer qu'il s'agit bien de vous";
              break;
            case 'IP_BLOCKED':
              errorMessage = 'Cet appareil a été bloqué par le groupement';
              break;
            case 'IP_TO_CONFIRM':
              errorMessage = 'Cet appareil est en attente de confirmation du groupement';
              break;
            case 'EXPIRED_TOKEN':
              errorMessage = 'Votre token est expiré';
              break;

            default:
              break;
          }
        }
      });

      displayNotification({
        type: 'error',
        message: errorMessage,
      });
      setLoading(false);
    },
  });

  const handleRequestSignin = ({ login, password }: any) => {
    const device = getDevice();
    setLoading(true);
    trackEvent({ module: 'AUTH', page: 'SIGNIN', action: 'OPEN' });

    getIp().then((ip) => {
      executeLogin({
        variables: {
          input: {
            ...device,
            ip,
            login,
            password,
          },
        },
      });
    });
  };

  return (
    <>
      <Helmet>
        <title>Connexion</title>
      </Helmet>
      <SigninPage
        loading={loading}
        onRequestSignin={handleRequestSignin}
        onRequestGoToForgotPassword={() => history.push('/forgot-password')}
      />
    </>
  );
};

export default Page;
