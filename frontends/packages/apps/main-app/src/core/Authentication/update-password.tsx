import React, { FC } from 'react';
import { Helmet } from 'react-helmet';
import { useUpdate_PasswordMutation } from '@lib/common/src/graphql';
import { ApolloClient } from '@apollo/client';
import { useDisplayNotification } from '@lib/common';
import { setRequestUpdatePassword, getRequestUpdatePasswordReason } from '@lib/common/src/auth/auth-util';
import { UpdatePasswordPage } from '../components';

const Page: FC<{ graphql: ApolloClient<any>; federation: ApolloClient<any> }> = ({ graphql, federation }) => {
  const notify = useDisplayNotification(federation);

  const reason = getRequestUpdatePasswordReason();

  const [update, updating] = useUpdate_PasswordMutation({
    client: graphql,
    onError: () => {
      notify({
        type: 'error',
        message: "Une erreur s'est produite lors de changement de mot de passe",
      });
    },
    onCompleted: () => {
      setRequestUpdatePassword(false);
      window.location.reload();
    },
  });

  const handleUpdatePassword = (newPassword: string) => {
    update({
      variables: {
        newPassword,
      },
    });
  };

  const handleCancel = () => {
    localStorage.clear();
    window.history.pushState(null, '', '/#/');
    window.location.reload();
  };

  const message =
    reason === 'RESETED'
      ? 'Votre mot de passe est encore un mot de passe par défaut. Pour assurer la securité de votre compte, vous devez mettre à jour votre mot de passe.'
      : "Votre mot de passe n'a pas été mis à jour depuis 180 jours. Pour assurer la securité de votre compte, vous devez mettre à jour votre mot de passe.";

  return (
    <>
      <Helmet>
        <title>Renouvellement de mot de passe</title>
      </Helmet>
      <UpdatePasswordPage
        message={message}
        loading={updating.loading}
        onRequestUpdatePassword={handleUpdatePassword}
        onCancel={handleCancel}
      />
    </>
  );
};

export default Page;
