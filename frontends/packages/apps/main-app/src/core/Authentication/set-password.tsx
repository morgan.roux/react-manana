import React, { FC, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory, useParams } from 'react-router-dom';
import { useReset_PasswordMutation } from '@lib/common/src/graphql';
import { ApolloClient } from '@apollo/client';
import { useDisplayNotification } from '@lib/common';
import { getAccessToken } from '@lib/common/src/auth/auth-util';
import { SetPasswordPage } from '../components';

const Page: FC<{ graphql: ApolloClient<any>; federation: ApolloClient<any> }> = ({ graphql, federation }) => {
  const history = useHistory();
  const { token } = useParams<{ token: string }>();
  const notify = useDisplayNotification(federation);

  const [resetPassword, resettingPassword] = useReset_PasswordMutation({
    client: graphql,
    onError: () => {
      notify({
        type: 'error',
        message: 'Le token est invalide ou expiré.',
      });
    },
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'Mot de passe reinitialisé.',
      });
      history.push('/signin');
      window.location.reload();
    },
  });

  useEffect(() => {
    if (getAccessToken()) {
      localStorage.clear();
      window.location.reload();
    }
  }, []);

  const handleRequestForgotPassword = (token: string, password: string) => {
    resetPassword({
      variables: {
        token,
        password,
      },
    });
  };

  const handleCancel = () => {
    localStorage.clear();
    window.history.pushState(null, '', '/#/');
    window.location.reload();
  };

  return (
    <>
      <Helmet>
        <title>Nouveau mot de passe</title>
      </Helmet>
      <SetPasswordPage
        token={token}
        loading={resettingPassword.loading}
        onRequestResetPassword={handleRequestForgotPassword}
        onCancel={handleCancel}
      />
    </>
  );
};

export default Page;
