import React, { FC } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router-dom';
import { useForgot_PasswordMutation } from '@lib/common/src/graphql';
import { ApolloClient } from '@apollo/client';
import { useDisplayNotification } from '@lib/common';
import { ForgotPasswordPage } from '../components';

const Page: FC<{ graphql: ApolloClient<any>; federation: ApolloClient<any> }> = ({ graphql, federation }) => {
  const history = useHistory();
  const notify = useDisplayNotification(federation);

  const [forgotPassword, sending] = useForgot_PasswordMutation({
    client: graphql,
    onError: () => {
      notify({
        type: 'error',
        message: 'Le login entré ne correspond à aucun compte',
      });
    },
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'Mail de réinitialisation de mot de passe envoyé',
      });
      history.push('/signin');
    },
  });

  const handleRequestForgotPassword = (login: string) => {
    forgotPassword({
      variables: {
        login,
      },
    });
  };

  const handleCancel = () => {
    localStorage.clear();
    history.push('/signin');
    window.location.reload();
  };

  return (
    <>
      <Helmet>
        <title>Mot de passe oublié</title>
      </Helmet>
      <ForgotPasswordPage
        loading={sending.loading}
        onRequestForgotPassword={handleRequestForgotPassword}
        onCancel={handleCancel}
      />
    </>
  );
};

export default Page;
