import React, { FC, useState } from 'react';
import { ThemeProvider as MuiThemeProvider, Theme, createMuiTheme } from '@material-ui/core';
import ThemeContext from './ThemeContext';
import { availableThemes, ThemeName } from './config';

const ThemeProvider: FC<any> = (props: any) => {
  const { children } = props;
  const [themeName, changeTheme] = useState<ThemeName>('angel');

  const theme: Theme = React.useMemo(() => {
    const nextTheme = createMuiTheme(availableThemes[themeName]);
    return nextTheme;
  }, [themeName]);

  return (
    <MuiThemeProvider theme={theme}>
      <ThemeContext.Provider value={[themeName, changeTheme]}> {children} </ThemeContext.Provider>
    </MuiThemeProvider>
  );
};

export default ThemeProvider;
