import { makeStyles, Theme } from '@material-ui/core';
import { red } from '@material-ui/core/colors';

const drawerWidth = 275;
const appBarHeight = 86;

const useStyles = makeStyles<any>((theme: Theme) => ({
  '@global': {
    '*::-webkit-scrollbar': {
      width: '10px',
      height: '10px',
    },
    '.k-grid-content::-webkit-scrollbar': {
      width: '10px',
      height: '10px',
    },
    [theme.breakpoints.down('md')]: {
      '*::-webkit-scrollbar': {
        width: '5px',
        height: '5px',
      },
      '.k-grid-content::-webkit-scrollbar': {
        width: '5px',
        height: '5px',
      },
    },
    '*::-webkit-scrollbar-track': {
      '-webkit-box-shadow': 'inset 0 0 6px rgba(0,0,0,0.00)',
    },
    '*::-webkit-scrollbar-thumb': {
      backgroundColor: theme.palette.secondary.main,
      outline: '1px solid slategrey',
    },
  },
  root: {
    display: 'flex',
    position: 'relative',
    //paddingTop: appBarHeight,
    height: `100vh`,
    overflow: 'hidden',
    '& .MuiPaper-root': {
      boxShadow: '0px 0px 4px 0px rgba(0,0,0,0.25);',
    },
    '& .MuiBadge-colorSecondary': {
      backgroundColor: red[600],
    },
    [theme.breakpoints.down('md')]: {
      //  height: `92vh`,
    },
  },
  drawer: {
    width: drawerWidth,
    height: '100vh',
    paddingTop: 100,
    flexShrink: 0,
    overflowY: 'auto',
    borderRight: '1px solid #E5E5E5',
    transform: `translateY(-${appBarHeight}px)`,
  },
  appBar: {
    marginLeft: drawerWidth,
    width: '100%',
    // background: 'linear-gradient(90deg, rgba(227,65,104,1) 14%, rgba(241,25,87,1) 100%)',
    background: theme.palette.primary.main,
    zIndex: theme.zIndex.drawer + 1,
    [theme.breakpoints.down('sm')]: {
      background: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
  },

  menuButton: {
    marginRight: theme.spacing(2),
  },
  drawerPaper: {
    width: drawerWidth,
    borderRight: 'none',
    '@media (max-width: 959.95px)': {
      width: 300,
    },
  },
  content: {
    flex: 1,
    overflow: 'auto',
    width: '100%',
    [theme.breakpoints.down('md')]: {
      // overflow: 'hidden',
    },
  },
  title: {
    textAlign: 'left',
    padding: 25,
    fontSize: 20,
    fontWeight: 'bold',
    backgroundColor: 'rgba(0, 0, 0, 0.13)',
  },
  flexAppBar: {
    display: 'flex',
    alignItems: 'center',
  },
  flexToolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    minHeight: 86,
    paddingLeft: 0,
  },
  brand: {
    paddingLeft: 20,
  },
  contentLogo: {
    position: 'fixed',
    background: '#fff',
    zIndex: 2,
    padding: '14px 24px 30px 16px',
    '@media (max-width: 959.95px)': {
      display: 'none',
    },
    top: 0,
  },
  contentLeft: {
    borderRight: '1px solid #E5E5E5',
    opacity: 1,
    minHeight: 'calc( 100vh - 86px )',
    '@media (max-width: 959.95px)': {
      minHeight: 'calc( 100vh - 124px )',
    },
  },
  contentLeftNotSuperAdmin: {
    paddingTop: '20px !important',
    '@media (max-width: 959.95px)': {
      paddingTop: '0px !important',
    },
  },
  logoFideMobile: {
    '@media (max-width: 600px)': {
      display: 'none',
    },
  },
  paddingNav: {
    padding: '0px 25px',
  },
  listItem: {
    minHeight: 37,
    padding: 5,
    margin: '2px 0px',
    borderRadius: 5,
    '& span': {
      fontWeight: 500,
    },
    '&:hover': {
      backgroundColor: '#F5F6FA',
      '& > *': {
        fontWeight: 900,
      },
    },
    '& .MuiListItemIcon-root': {
      minWidth: '35px !important',
      '& .MuiSvgIcon-root': {
        width: 22,
      },
    },
  },
  listItemActive: {
    backgroundColor: theme.palette.secondary.main,
    '& > *': {
      color: theme.palette.common.white,
    },
  },
  listItemDivider: {
    margin: '15px 0px',
  },

  appBarContainer: {
    height: appBarHeight,
    color: '#222',
    backgroundColor: '#FFF',
    top: 0,
    left: 'auto',
    right: 0,
    width: '100%',
    boxSizing: 'border-box',
    flexShrink: 0,
    flexDirection: 'row',
    boxShadow: '0px 2px 4px 1px rgba(0,0,0,0.2), 0px 4px 5px 0px rgba(0,0,0,0.14), 0px 1px 10px 0px rgba(0,0,0,0.12)',
  },

  reversedDirection: {
    flexDirection: 'row-reverse',
  },

  rightAppBar: {
    display: 'flex',
  },

  searchBar: {
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  groupementAndCloseButtonContainer: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    color: theme.palette.common.white,
    '&>svg': {
      fontSize: '',
    },
    '&>svg, &>img': {
      marginRight: 12,
    },
    '&>h2': {
      fontWeight: 'bold',
      fontSize: 28,
    },
    '& button': {
      marginLeft: 'auto',
    },
  },
  groupement: {
    flexGrow: 1,
    textAlign: 'center',
    fontSize: 19,
  },

  groupementLogo: {
    flex: '0 0 200px',
    height: 48,
    marginRight: 16,
    objectFit: 'cover',
    objectPosition: 'center',
    borderRadius: 5,
  },

  groupementName: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  flexMobLogo: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '14px 0 0 16px',
    fontFamily: 'Roboto',
    fontSize: '30px',
    fontWeight: 'bold',
  },
  whiteBackground: {
    background: '#FFFFFF',
  },
  white: {
    color: '#FFFFFF',
  },
  fullWidth: {
    [theme.breakpoints.up('md')]: {
      width: '100%',
    },
  },
  groupementAvatar: {
    maxWidth: 30,
    maxHeight: 30,
    borderRadius: '50%',
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: '#ffffff',
  },
  dashboardHeaderTitle: {
    minWidth: 'fit-content',
    fontWeight: 'bold',
    fontSize: 30,
    display: 'flex',
    alignItems: 'center',
    '& svg': {
      width: 50,
      height: 50,
      marginRight: 15,
    },
  },
  headerTitleIcon: {},
  fullScreenTitle: {
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 8,
  },
  navigationAvatar: {
    cursor: 'pointer',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
    marginRight: 40,
  },
  mobileSearch: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  fabsContainer: {
    display: 'flex',
    position: 'absolute',
    zIndex: 10000,
    bottom: 70 + 24,
    left: 24,
    [theme.breakpoints.up('md')]: {
      bottom: 32,
    },
    [theme.breakpoints.up('lg')]: {
      bottom: 32 + 27,
    },
    justifyContent: 'space-between',
    alignContent: 'center',
    flexDirection: 'column',
    '& button': {
      marginTop: 16,
    },
  },
  appBarWithDrawer: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: 73,
    width: theme.breakpoints.up('lg') ? `calc(100% - 73px)` : '100%',
  },
  appBarShift: {
    marginLeft: 300,
    width: `calc(100% - 300px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
}));

export default useStyles;
