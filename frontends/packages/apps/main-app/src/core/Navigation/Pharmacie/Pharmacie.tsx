import React, { FC, useState, Fragment } from 'react';
import useStyles from './styles';
import { Button, Tooltip } from '@material-ui/core';
import LocalPharmacyIcon from '../../assets/images/localPharmacy.svg';
import ArrowRight from '@material-ui/icons/ArrowRight';
import { useApplicationContext } from '@lib/common';
import { ChangePharmacieModal } from '@lib/common/src/components';
import { CustomFullScreenModal } from '@lib/ui-kit';

const Pharmacie: FC<{}> = ({}) => {
  const classes = useStyles({});
  const { user, pharmacies, currentPharmacie } = useApplicationContext();

  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [openPharmaDetail, setOpenPharmaDetail] = useState<boolean>(false);

  const handleOpenDialog = (value: boolean) => () => {
    if (pharmacies.length > 1) setOpenDialog(value);
  };

  const handleOpenDetailDialog = (value: boolean) => {
    //FIXME : Si le détail marche
    //setOpenPharmaDetail(true)
    if (pharmacies.length > 1) setOpenDialog(value);
  };

  return (
    <Fragment>
      <Tooltip title={currentPharmacie ? currentPharmacie.nom || '' : '...'}>
        <Button className={classes.root} disabled={currentPharmacie ? false : true}>
          <img src={LocalPharmacyIcon} alt="logo pharmacie" />
          <p className={classes.threeDots} onClick={() => handleOpenDetailDialog(true)}>
            {currentPharmacie ? currentPharmacie.nom : '...'}
          </p>
          <ArrowRight onClick={handleOpenDialog(true)} />
        </Button>
      </Tooltip>
      {<ChangePharmacieModal open={openDialog} setOpen={setOpenDialog} />}
      {openPharmaDetail && (
        <CustomFullScreenModal
          open={openPharmaDetail}
          setOpen={setOpenPharmaDetail}
          title={`Pharmacie`}
          withBtnsActions={false}
        >
          {/* <FichePharmacie
            id={myPharmacie && myPharmacie.data && myPharmacie.data.pharmacie && myPharmacie.data.pharmacie.id}
            inModalView={true}
            handleClickBack={() => setOpenPharmaDetail(false)}
          /> */}
        </CustomFullScreenModal>
      )}
    </Fragment>
  );
};

export default Pharmacie;
