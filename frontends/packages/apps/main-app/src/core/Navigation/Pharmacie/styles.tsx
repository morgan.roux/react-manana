import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  threeDots: {
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    maxWidth: 200,
    minWidth: 100,
    whiteSpace: 'nowrap',
    // color: '#FFFFFF',
    display: 'inline-block',
    fontFamily: 'Montserrat',
    fontSize: '14px',
    fontWeight: 'bold',
    '@media (max-width: 600px)': {
      display: 'none',
    },
  },
  root: {
    marginRight: 16,
    border: '2px solid #FFFFFF',
    borderRadius: '3px',
    color: '#FFFFFF',
    padding: '12px',
    textTransform: 'none',
  },
}));

export default useStyles;
