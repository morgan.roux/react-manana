import React, { FC } from 'react';
import logo from '../../assets/images/digital4win.svg';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core';
import { Link } from 'react-router-dom';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  logo: {
    [theme.breakpoints.up('md')]: {
      paddingRight: 30,
      marginRight: 30,
      height: 66,
      transition: theme.transitions.create('height', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    [theme.breakpoints.down('md')]: {
      height: 36,
    },
  },
  shrink: {
    height: 45,
    transition: theme.transitions.create('height', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
});
interface BrandProps {
  willShrink?: boolean;
}
const Brand: FC<WithStyles & BrandProps> = ({ classes, willShrink }) => {
  return (
    <Link to="/">
      <img className={`${classes.logo} ${willShrink ? classes.shrink : ''}`} src={logo} />
    </Link>
  );
};

export default withStyles(styles)(Brand);
