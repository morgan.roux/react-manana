import React, { FC } from 'react';
import useStyles from './styles';
import { Avatar, Box, Button, ButtonProps, Typography, Hidden } from '@material-ui/core';
import { AccountCircle, ArrowDropDown } from '@material-ui/icons';
import { upperFirst } from 'lodash';
import { stringToAvatar, useApplicationContext } from '@lib/common';

interface UserInfoProps {
  arrowDropDown?: boolean;
  withUserName?: boolean;
}

const UserInfo: FC<UserInfoProps & ButtonProps> = ({ arrowDropDown, withUserName, ...props }) => {
  const classes = useStyles({});
  const { user } = useApplicationContext();
  const userName = user.fullName;
  const role = user?.role?.nom;
  const userPhoto = user.photo?.publicUrl;
  const userRoleName = upperFirst(user.role?.nom || '');

  return (
    <Button {...props} color="inherit" style={{ textTransform: 'initial' }}>
      {userPhoto ? (
        <img src={userPhoto} alt={`${userName || ''}`} className={classes.avatar} />
      ) : userName ? (
        <Avatar className={classes.avatar}>{stringToAvatar(userName)}</Avatar>
      ) : (
        <AccountCircle />
      )}
      <Hidden mdDown={true}>
        <Box mr={2}>
          {userName && <Typography align="center">{userName}</Typography>}
          {userRoleName && (
            <Typography className={classes.role} align="center">
              {userRoleName}
            </Typography>
          )}
        </Box>
      </Hidden>
      {withUserName && (
        <Box className={classes.mobileInfoContainer}>
          <Typography className={classes.mobileUsername} align="center">
            {userName}
          </Typography>
          <Typography className={classes.mobileRole} align="center">
            {role}
          </Typography>
        </Box>
      )}
      {arrowDropDown && <ArrowDropDown />}
    </Button>
  );
};

export default UserInfo;
