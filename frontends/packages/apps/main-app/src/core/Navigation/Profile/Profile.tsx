import React, { FC, useEffect, useState } from 'react';
import useStyles from './styles';
import { Menu, MenuItem, ListItemIcon, Divider, IconButton, Box } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import UserInfo from './UserInfo';
import { Close, Menu as MenuIcon } from '@material-ui/icons';
import Pharmacie from '../Pharmacie';
import { Feature, filterFeatures, filterFeaturesByDevice, getOptionValue, useApplicationContext } from '@lib/common';

// import { INTELLIGENCE_COLLECTIVE_URL, MESSAGE_URL } from '../../../Constant/url';
export const INTELLIGENCE_COLLECTIVE_URL = '/intelligence-collective';
export const MESSAGE_URL = '/messagerie/inbox';

interface ProfileInterface {
  inDrawer?: boolean;
  onDrawerClose?: () => void;
}

const Profile: FC<ProfileInterface> = ({ inDrawer, onDrawerClose }) => {
  const classes = useStyles({});
  const history = useHistory();
  const appContext = useApplicationContext();
  const { features, auth } = appContext;

  const [enabledFeatures, setEnabledFeatures] = useState<Feature[]>([]);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const isMenuOpen = Boolean(anchorEl);
  const menuId = 'primary-search-account-menu';

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const [mobile, setMobile] = React.useState<boolean>(window.innerWidth < 960);
  const [xmobile, setXMobile] = React.useState<boolean>(window.innerWidth < 768);

  React.useEffect(() => {
    window.addEventListener('resize', () => {
      if (window.innerWidth < 1280) setMobile(true);
      else setMobile(false);

      if (window.innerWidth < 1050) setXMobile(true);
      else setXMobile(false);
    });
  }, []);

  const handleDrawerClose = () => {
    if (onDrawerClose) {
      onDrawerClose();
    }
  };

  useEffect(() => {
    let newEnabledFeatures = filterFeatures(features, { location: 'MENU_PRINCIPAL' });
    if (mobile) {
      newEnabledFeatures = filterFeaturesByDevice(newEnabledFeatures, 'MOBILE');
    }

    setEnabledFeatures(newEnabledFeatures);
  }, [features, mobile]);

  const menuItems = React.useMemo(
    () =>
      enabledFeatures.map((feature) => (
        <MenuItem
          className={classes.listParamsProfile}
          key={`profile_menu_drop_down_part_one_${feature.id}`}
          onClick={() => {
            handleMenuClose();
            feature.to && history.push(getOptionValue(feature.to, appContext));
          }}
        >
          <ListItemIcon style={{ minWidth: 35 }}>
            {typeof feature.icon === 'string' ? <img src={feature.icon} /> : feature.icon}
          </ListItemIcon>
          {feature.name}
        </MenuItem>
      )),
    [enabledFeatures]
  );

  if (inDrawer) {
    return (
      <Box>
        <Box display="flex" justifyContent="flex-end">
          <IconButton onClick={handleDrawerClose}>
            <Close />
          </IconButton>
        </Box>
        <Box>
          <UserInfo arrowDropDown={false} withUserName={true} aria-controls={menuId} />
        </Box>
        <Box>{menuItems}</Box>
      </Box>
    );
  }

  // menu list
  return (
    <>
      {mobile ? (
        <>
          {!xmobile && (
            <Box>
              <Pharmacie />
            </Box>
          )}

          <IconButton onClick={handleProfileMenuOpen} className={classes.menuIconButton}>
            <MenuIcon htmlColor="#FFF" />
          </IconButton>
        </>
      ) : (
        <>
          <Pharmacie />
          <UserInfo onClick={handleProfileMenuOpen} arrowDropDown={true} aria-controls={menuId} />
        </>
      )}
      <Menu
        anchorEl={anchorEl}
        id={menuId}
        keepMounted={true}
        open={isMenuOpen}
        onClose={handleMenuClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        className={classes.menu}
      >
        {mobile && (
          <>
            {xmobile && (
              <Box>
                <Pharmacie />
              </Box>
            )}
            <UserInfo fullWidth={true} />
            <Divider variant="fullWidth" className={classes.divider} />
          </>
        )}
        {menuItems}
      </Menu>
    </>
  );
};

export default Profile;
