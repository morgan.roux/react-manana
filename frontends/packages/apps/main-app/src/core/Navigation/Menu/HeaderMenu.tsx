import { Badge, Box, ClickAwayListener, IconButton } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { Feature, filterFeatures, useApplicationContext } from '@lib/common';
/*import { useHistory } from 'react-router';
import { Notifications } from '@material-ui/icons';
import {
  useNotificationsQuery,
  Notifications_SubscriptionDocument,
  Notifications_Updated_SubscriptionDocument,
  Notifications_Deleted_SubscriptionDocument,
} from '@lib/common/src/graphql';
import { NotificationMenu } from '../../../notification/components/NotificationMenu';
*/
const HeaderMenu: FC<{}> = () => {
  const { /* isMobile, graphql,*/ features } = useApplicationContext();
  // const history = useHistory();
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
  const [enabledFeatures, setEnabledFeatures] = useState<Feature[]>([]);

  useEffect(() => {
    const newEnabledFeatures = filterFeatures(features, { location: 'BANNIERE' });
    setEnabledFeatures(newEnabledFeatures);
  }, [features]);

  /* const result = useNotificationsQuery({
    client: graphql,
  });
*/
  // const { loading, data, fetchMore, subscribeToMore } = result;

  const clickAway = () => {
    setAnchorEl(null);
  };

  /*  const handleToggle = (e: React.MouseEvent) => {
    if (!isMobile) {
      setAnchorEl(anchorEl ? null : (e.currentTarget as HTMLElement));
    } else {
      history.push('/notifications');
    }
  };
*/
  /*
  const handleClose = (event: React.MouseEvent<EventTarget>) => {
    if (anchorEl && anchorEl.contains(event.target as HTMLElement)) {
      return;
    }
    setAnchorEl(null);
  };

  const fetchMoreNotifications = () => {
    fetchMore({
      variables: {
        first: 10,
        skip: data && data.notifications && data.notifications.data ? data.notifications.data.length : 0,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;
        return {
          notifications: {
            __typename: 'NotificationResult',
            data:
              prev &&
              prev.notifications &&
              prev.notifications.data &&
              fetchMoreResult.notifications &&
              fetchMoreResult.notifications.data
                ? [...prev.notifications.data, ...fetchMoreResult.notifications.data]
                : [],
            total: data && data.notifications ? data.notifications.total : 0,
            notSeen: data && data.notifications ? data.notifications.notSeen : 0,
          },
        };
      },
    });
  };
  */
  /*
  const subscribeToMoreNotification = () => {
    subscribeToMore({
      document: Notifications_SubscriptionDocument,
      onError: (error) => {
        console.log('erroraa ', error);
      },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData) return prev;

        const newData = subscriptionData && (subscriptionData.data as any);

        if (newData && newData.notificationAdded) {
          console.log('addedData', JSON.stringify(newData.notificationAdded));
        }

        return {
          notifications: {
            __typename: 'NotificationResult',
            data:
              prev && prev.notifications && prev.notifications.data && newData && newData.notificationAdded
                ? [newData.notificationAdded, ...prev.notifications.data]
                : prev && prev.notifications && prev.notifications.data
                ? [...prev.notifications.data]
                : [],
            total: (prev && prev.notifications && prev.notifications.total ? prev.notifications.total : 0) + 1,
            notSeen: (prev && prev.notifications && prev.notifications.total ? prev.notifications.notSeen : 0) + 1,
          } as any,
        };
      },
    });

    subscribeToMore({
      document: Notifications_Updated_SubscriptionDocument,
      onError: (error) => {
        console.log('erroraa ', error);
      },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData) return prev;
        const updatedData = subscriptionData && (subscriptionData.data as any);
        if (updatedData && updatedData.notificationUpdated) {
          console.log('updatedData', JSON.stringify(updatedData.notificationUpdated));
        }

        const total = prev && prev.notifications && prev.notifications.total ? prev.notifications.total : 0;
        const notSeen = prev && prev.notifications && prev.notifications.notSeen ? prev.notifications.notSeen : 0;
        console.log('update total : ', total, 'notSeen : ', notSeen);

        return {
          notifications: {
            __typename: 'NotificationResult',
            data:
              prev &&
              prev.notifications &&
              prev.notifications.data &&
              prev.notifications.data.length &&
              updatedData &&
              updatedData.notificationUpdated &&
              updatedData.notificationUpdated.id
                ? [
                    updatedData.notificationUpdated,
                    ...prev.notifications.data.filter(
                      (notif) => notif && notif.id && notif.id !== updatedData.notificationUpdated.id
                    ),
                  ]
                : prev && prev.notifications && prev.notifications.data && prev.notifications.data.length
                ? [...prev.notifications.data]
                : [],
            total:
              updatedData && updatedData.notificationUpdated && updatedData.notificationUpdated.seen
                ? total - 1
                : updatedData && updatedData.notificationUpdated && !updatedData.notificationUpdated.seen
                ? total + 1
                : total,
            notSeen:
              updatedData && updatedData.notificationUpdated && updatedData.notificationUpdated.seen
                ? notSeen - 1
                : updatedData && updatedData.notificationUpdated && !updatedData.notificationUpdated.seen
                ? notSeen + 1
                : notSeen,
          } as any,
        };
      },
    });

    subscribeToMore({
      document: Notifications_Deleted_SubscriptionDocument,
      onError: (error) => {
        console.log('erroraa ', error);
      },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData) return prev;

        console.clear();
        const deletedData = subscriptionData && (subscriptionData.data as any);
        if (deletedData && deletedData.notificationDeleted) {
          console.log('deletedData', JSON.stringify(deletedData.notificationDeleted));
        }

        const total = prev && prev.notifications && prev.notifications.total ? prev.notifications.total : 0;
        const notSeen = prev && prev.notifications && prev.notifications.notSeen ? prev.notifications.notSeen : 0;
        console.log(' delete total : ', total, 'notSeen : ', notSeen);

        return {
          notifications: {
            __typename: 'NotificationResult',
            data:
              prev &&
              prev.notifications &&
              prev.notifications.data &&
              prev.notifications.data.length &&
              deletedData &&
              deletedData.notificationDeleted &&
              deletedData.notificationDeleted.id
                ? [
                    ...prev.notifications.data.filter(
                      (notif) => notif && notif.id && notif.id !== deletedData.notificationDeleted.id
                    ),
                  ]
                : prev && prev.notifications && prev.notifications.data && prev.notifications.data.length
                ? [...prev.notifications.data]
                : [],
            total:
              deletedData && deletedData.notificationDeleted && !deletedData.notificationDeleted.seen
                ? total - 1
                : total,
            notSeen:
              deletedData && deletedData.notificationDeleted && !deletedData.notificationDeleted.seen
                ? notSeen - 1
                : notSeen,
          } as any,
        };
      },
    });
  };
*/
  /* useEffect(() => {
    subscribeToMoreNotification();
  }, []);*/

  //const notSeenNotification = data?.notifications?.notSeen ?? 0;
  const open = anchorEl ? true : false;

  return (
    <ClickAwayListener onClickAway={clickAway}>
      <div style={{ display: 'flex' }}>
        {enabledFeatures.map((feature) => feature.component)}
        {/* <IconButton onClick={handleToggle} aria-label={`show ${notSeenNotification} new notifications`}>
          <Badge badgeContent={notSeenNotification} max={999} color="error">
            <Notifications htmlColor="#FFF" />
          </Badge>
  </IconButton>*/}
        {/* <NotificationMenu
          popperProps={{
            open,
            anchorEl,
            children: null,
          }}
          handleClose={handleClose}
          notifications={result}
          loading={loading}
          fetchMoreNotifications={fetchMoreNotifications}
        />*/}
      </div>
    </ClickAwayListener>
  );
};

export default HeaderMenu;
