import React, { FC, useEffect, useState } from 'react';
import {
  List,
  Drawer,
  Divider,
  CssBaseline,
  createStyles,
  makeStyles,
  useTheme,
  Theme,
  Box,
  Typography,
} from '@material-ui/core';
import classnames from 'classnames';
import { Brand } from './Brand';
import { ListSideNav } from './ListSideNav/ListSideNav';
import { Feature, filterFeatures, isMobile, useApplicationContext } from '@lib/common';
import { useHistory } from 'react-router';

const drawerWidth = 300;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      width: '100%',
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    hide: {
      display: 'none',
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: 'nowrap',
      background: theme.palette.primary.main,
      '&>div': {
        border: 'none !important',
        background: theme.palette.primary.main,
      },
    },
    drawerOpen: {
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: 'hidden',
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
    boxLogo: {
      position: 'relative',
      '&:before': {
        content: '""',
        position: 'absolute',
        width: 2,
        height: '70%',
        background: '#fff',
        right: 0,
        top: '50%',
        transform: 'translateY(-50%)',
        opacity: 0.4,
      },
    },
    divider1: {
      backgroundColor: '#fff',
      opacity: 0.5,
    },
    dividerCenter: {
      backgroundColor: '#fff',
      opacity: 0.5,
      width: 'calc(100% - 32px)',
      margin: '0 auto',
    },
    groupTitle: {
      color: '#fff',
      padding: 16,
      fontWeight: 700,
      pointerEvents: 'none',
      transition: theme.transitions.create(['padding', 'height'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    hideTitle: {
      visibility: 'hidden',
      height: 0,
      paddingTop: 0,
      paddingBottom: 0,
      transition: theme.transitions.create(['padding', 'height'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
  })
);
interface MainContainerProps {
  appBar: any;
  main: any;
  openSidebar?: boolean;
  onMouseOver?: (event: React.MouseEvent<{}>) => void;
  onMouseOut?: (event: React.MouseEvent<{}>) => void;
  isSetting?: boolean;
}
export const MainContainer: FC<MainContainerProps> = ({
  appBar,
  main,
  openSidebar,
  onMouseOut,
  onMouseOver,
  isSetting,
}) => {
  const classes = useStyles();
  const theme = useTheme();

  const appContext = useApplicationContext();
  const { features, isMobile } = appContext;

  const [enabledFeatures, setEnabledFeatures] = useState<Feature[]>([]);
  const history = useHistory();

  const isDrawerOpen = (isopen?: boolean) => {
    switch (isopen) {
      case true:
        return classes.drawerOpen;

      default:
        return classes.drawerClose;
    }
  };
  useEffect(() => {
    const newEnabledFeatures = filterFeatures(features, { location: 'FAVORITE' });
    setEnabledFeatures(newEnabledFeatures);
  }, [features]);
  console.log('EnableFeatures----------------------------------------------:::::', enabledFeatures);

  const defaultGroup = enabledFeatures.filter((features) => !features.options?.group);

  const byGroup = enabledFeatures
    .filter((feature) => feature.options?.group)
    .reduce((acc, next) => {
      const group: any = next.options?.group;
      return {
        ...acc,
        [group]: [...(acc[group] || []), next],
      };
    }, {} as any);
  const handleClickItem = (to: any) => {
    history.push(to);
  };
  return (
    <div className={classes.root}>
      <CssBaseline />
      {appBar}
      <Drawer
        variant="permanent"
        className={classnames(classes.drawer, isDrawerOpen(openSidebar))}
        classes={{
          paper: classnames(isDrawerOpen(openSidebar)),
        }}
        onMouseOver={onMouseOver}
        onMouseOut={onMouseOut}
        style={isSetting || isMobile ? { display: 'none' } : {}}
      >
        <Box
          display="flex"
          alignItems="center"
          style={{ background: theme.palette.primary.main }}
          py={1}
          height={85.5}
          pl={2}
          className={classes.boxLogo}
        >
          <Brand willShrink={!openSidebar} />
        </Box>
        <Divider className={classes.divider1} />
        <List>
          {(defaultGroup || []).map((feature, index) => (
            <ListSideNav
              to={feature.to}
              label={(feature.name as string) || ''}
              startIcon={feature.icon}
              component={feature.component}
              onClickItem={handleClickItem}
            />
          ))}
        </List>
        <List>
          {Object.keys(byGroup).map((group) => {
            return (
              <>
                <Divider className={openSidebar ? classes.dividerCenter : classes.divider1} />
                {
                  <Typography className={`${classes.groupTitle} ${!openSidebar ? classes.hideTitle : ''}`}>
                    {group}
                  </Typography>
                }
                {byGroup[group].map((feature: any, index: number) => (
                  <ListSideNav
                    key={`sidenav-${index}`}
                    label={feature.name || ''}
                    startIcon={feature.icon}
                    component={feature.component}
                    to={feature.to}
                    onClickItem={handleClickItem}
                  />
                ))}
              </>
            );
          })}
        </List>
      </Drawer>
      {main}
    </div>
  );
};
