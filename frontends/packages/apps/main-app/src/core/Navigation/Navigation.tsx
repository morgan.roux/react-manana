import {
  List,
  ListItem,
  ListItemText,
  Tooltip,
  IconButton,
  Fade,
  Drawer,
  AppBar,
  Avatar,
  Box,
  Fab,
  Hidden,
  ListItemIcon,
  Toolbar,
  Typography,
  useTheme,
  CssBaseline,
} from '@material-ui/core';
import { Home, Settings } from '@material-ui/icons';
import CloseIcon from '@material-ui/icons/Close';
import React, { FC, Fragment, useEffect, useState } from 'react';
import { matchPath, Redirect, useHistory, useLocation } from 'react-router-dom';
import { Profile } from './Profile';
import useStyles from './styles';
import {
  Feature,
  filterFeatures,
  getOptionValue,
  ProtectedRouteProps,
  stringToAvatar,
  useApplicationContext,
  MobileTopBar,
} from '@lib/common';
import classnames from 'classnames';
import { PARAMETRES_ROUTES_PREFIX } from '@lib/common';
import ParametresRouter from '../Parametres/ParametresRouter';
import MainRouter from '../Main/MainRouter';
import { HeaderMenu } from './Menu';
import MobileQuickAccess from '../MobileQuickAccess';
import { MainContainer } from './MainContainer';
import ArrowBack from '@material-ui/icons/ArrowBackIos';
import ArrowForward from '@material-ui/icons/ArrowForwardIos';
const handleDrawerToggle = (setMobileOpen: (isOpen: boolean) => void, isOpen: boolean) => () => {
  setMobileOpen(!isOpen);
};

const Menus: FC<{}> = () => {
  const appContext = useApplicationContext();
  const { isSuperAdmin, user, isMobile, currentGroupement, features, routes, showAppBar, setShowAppBar } = appContext;
  const [currentRoute, setCurrentRoute] = useState<ProtectedRouteProps>();
  const [enabledFeatures, setEnabledFeatures] = useState<Feature[]>([]);
  const [enabledQuickAccessFeatures, setEnabledQuickAccessFeatures] = useState<Feature[]>([]);
  const [openAppDrawer, setOpenAppDrawer] = useState<boolean>(false);
  const [inSetting, setInSetting] = useState(false);
  const classes = useStyles();
  const location = useLocation();
  const history = useHistory();
  const pathname = location.pathname;

  const [mobileOpen, setMobileOpen] = React.useState(false);
  const mobileUrls = [
    '/todo/recu',
    '/todo/recu-equipe',
    '/todo/aujourdhui',
    '/todo/aujourdhui-equipe',
    '/todo/sept-jours',
    '/todo/sept-jours-equipe',
    '/todo/sans-date',
    '/todo/tout-voir',
  ];

  useEffect(() => {
    setEnabledFeatures(filterFeatures(features, { location: 'PARAMETRE' }));
    setEnabledQuickAccessFeatures(filterFeatures(features, { location: 'QUICK_ACCESS' }));
  }, [features]);

  useEffect(() => {
    if (pathname === '/') {
      setShowAppBar(true);
    }
  }, [pathname]);
  useEffect(() => {
    setCurrentRoute(routes.find((route) => route.path && matchPath(pathname, route.path as any)));
  }, [pathname, routes]);

  const onParametres = pathname.startsWith(PARAMETRES_ROUTES_PREFIX);

  const menuItems: any[] = React.useMemo(() => {
    return enabledFeatures.map((feature) => ({
      label: feature.name ? (typeof feature.name === 'string' ? feature.name : feature.name(appContext)) : '',
      path: `${PARAMETRES_ROUTES_PREFIX}${feature.to}`,
      disabled: false,
      authorizedParameter: true,
      icon: feature.icon,
    }));
  }, [enabledFeatures]);

  const DashboardDrawer = (
    <Box>
      <Box
        className={isSuperAdmin ? classes.contentLeft : `${classes.contentLeft} ${classes.contentLeftNotSuperAdmin}`}
      >
        <List className={classes.paddingNav} dense={true} disablePadding={true}>
          {menuItems.map((item, index) => {
            const pathName = pathname.split('/')[2];
            const itemPath = item.path.split('/')[2];
            return (
              <ListItem
                key={`menu-${index}`}
                className={classnames(
                  classes.listItem,
                  (pathname === item.path || (pathName && itemPath && pathName === itemPath)) && classes.listItemActive
                )}
                button={true}
                onClick={() => {
                  setMobileOpen(false);
                  history.push(item.path);
                }}
                disabled={item.disabled}
              >
                <ListItemText>{item.label}</ListItemText>
              </ListItem>
            );
          })}
        </List>
      </Box>
    </Box>
  );

  const handleClickCloseBtn = () => {
    history.push('/');
  };

  const memorized = React.useMemo(
    () => (
      <main style={{ width: '100%', overflowY: 'auto' }} className={classes.content}>
        <ParametresRouter routes={routes} features={features} key="parametresRouter" />
        <MainRouter routes={routes} features={features} key="mainRouter" />
      </main>
    ),
    [features, routes]
  );

  const GroupementAndCloseButton = () => {
    return (
      <Box className={classes.groupementAndCloseButtonContainer}>
        <Fragment>
          <Settings fontSize="large" />
          <Typography variant="h2">Paramètres</Typography>
        </Fragment>

        <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Quitter">
          <IconButton color="inherit" onClick={handleClickCloseBtn}>
            <CloseIcon />
          </IconButton>
        </Tooltip>
      </Box>
    );
  };

  const [openMobileProfileDrawer, setOpenMobileProfileDrawer] = useState<boolean>(false);

  const handleShowMobileProfile = () => {
    setOpenMobileProfileDrawer(true);
  };

  const container = window !== undefined ? () => window.document.body : undefined;
  const theme = useTheme();
  if (pathname === PARAMETRES_ROUTES_PREFIX) {
    const path = menuItems.length > 0 ? menuItems[0].path : '/';
    return <Redirect to={{ pathname: path }} />;
  }
  const handleOpenAppDrawer = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    setOpenAppDrawer(!openAppDrawer);
  };
  const generateClassIfDrawerOpen = (isOpen?: boolean) => {
    return isOpen ? classes.appBarShift : classes.appBarWithDrawer;
  };
  useEffect(() => {
    let cleanup = false;
    mobileOpen ? setInSetting(true) : setInSetting(false);
    return () => {
      cleanup = true;
    };
  }, [mobileOpen]);
  const TopHeaderBar = (
    <>
      <CssBaseline />
      <Hidden
        smDown={
          mobileUrls.includes(pathname) ||
          pathname.startsWith('/todo/projet') ||
          pathname.startsWith('/todo/etiquette') ||
          pathname.startsWith('/messagerie')
            ? true
            : false
        }
        implementation="css"
      >
        {showAppBar && (
          <AppBar
            position="fixed"
            className={classnames(classes.appBar, generateClassIfDrawerOpen(openAppDrawer))}
            elevation={0}
            style={pathname.startsWith('/db/') || isMobile ? { width: '100%', paddingLeft: 30 } : {}}
          >
            <Toolbar className={classes.flexToolbar}>
              {onParametres ? (
                <GroupementAndCloseButton />
              ) : (
                <Fragment>
                  <Avatar className={classes.navigationAvatar} onClick={handleShowMobileProfile}>
                    {stringToAvatar(user.fullName)}
                  </Avatar>
                  {!isMobile && (
                    <IconButton style={{ color: '#FFF' }} onClick={handleOpenAppDrawer}>
                      {openAppDrawer ? <ArrowBack /> : <ArrowForward />}
                    </IconButton>
                  )}
                  {!isMobile && (
                    <IconButton style={{ color: '#FFF' }} onClick={() => history.push('/')}>
                      <Home />
                    </IconButton>
                  )}
                  <Hidden smDown={true}>
                    <Fragment>
                      {currentGroupement.logo?.publicUrl ? (
                        <img
                          className={classes.groupementLogo}
                          src={currentGroupement.logo.publicUrl}
                          alt={currentGroupement.nom}
                        />
                      ) : null}
                    </Fragment>
                  </Hidden>
                  <div className={classes.searchBar}></div>
                  <Fragment>
                    <Box display="flex">
                      <Box className={classes.flexAppBar}>
                        <HeaderMenu />
                        <Profile />
                      </Box>
                    </Box>
                  </Fragment>
                </Fragment>
              )}
            </Toolbar>
          </AppBar>
        )}
      </Hidden>
      <CssBaseline />
      {onParametres && (
        <nav className={classes.drawer} aria-label="Menus">
          <List className={classes.paddingNav} dense={true} disablePadding={true}>
            {menuItems.map((item, index) => {
              const pathName = pathname.split('/')[2];
              const itemPath = item.path.split('/')[2];
              return (
                <ListItem
                  key={`menu-${index}`}
                  className={classnames(
                    classes.listItem,
                    (pathname === item.path || (pathName && itemPath && pathName === itemPath)) &&
                      classes.listItemActive
                  )}
                  button={true}
                  // tslint:disable-next-line: jsx-no-lambda
                  onClick={() => history.push(item.path)}
                  disabled={item.disabled}
                >
                  <ListItemIcon>{item.icon}</ListItemIcon>
                  <ListItemText>{item.label}</ListItemText>
                </ListItem>
              );
            })}
          </List>
        </nav>
      )}
    </>
  );
  const handleMouseOver = (event: React.MouseEvent<{}>) => {
    setOpenAppDrawer(true);
  };
  const handleMouseOut = (event: React.MouseEvent<{}>) => {
    setOpenAppDrawer(false);
  };
  return (
    <Fragment>
      <Drawer
        variant="temporary"
        anchor="left"
        open={mobileOpen}
        onClose={handleDrawerToggle(setMobileOpen, mobileOpen)}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <Box className={classes.flexMobLogo}>
          Menu
          <IconButton
            color="inherit"
            aria-label="Close drawer"
            edge="start"
            onClick={handleDrawerToggle(setMobileOpen, mobileOpen)}
          >
            <CloseIcon />
          </IconButton>
        </Box>
        {DashboardDrawer}
      </Drawer>
      <Box
        className={classes.root}
        style={{
          paddingTop: isMobile && pathname.startsWith('/messagerie') ? '70px' : '86px',
        }}
      >
        <MainContainer
          onMouseOut={handleMouseOut}
          onMouseOver={handleMouseOver}
          openSidebar={openAppDrawer}
          appBar={TopHeaderBar}
          isSetting={pathname.startsWith('/db/')}
          main={
            <>
              {isMobile && currentRoute?.mobileOptions?.topBar && (
                <MobileTopBar {...currentRoute.mobileOptions.topBar} />
              )}
              <MobileQuickAccess />
              {memorized}
            </>
          }
        />
        <Drawer
          container={container}
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={openMobileProfileDrawer}
          onClose={() => setOpenMobileProfileDrawer(false)}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <Profile inDrawer={true} onDrawerClose={() => setOpenMobileProfileDrawer(false)} />
        </Drawer>
      </Box>
      {enabledQuickAccessFeatures.length > 0 && (
        <Box className={classes.fabsContainer}>
          {enabledQuickAccessFeatures.map((feature) => (
            <Fab
              color="primary"
              style={(feature.options as any)?.style}
              onClick={() => feature.to && history.push(getOptionValue(feature.to, appContext), { from: pathname })}
            >
              {feature.icon as any}
            </Fab>
          ))}
        </Box>
      )}
    </Fragment>
  );
};

export default Menus;
