import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import React from 'react';
import { FC } from 'react';
import AddIcon from '@material-ui/icons/Add';
import { createStyles, makeStyles } from '@material-ui/core';
import MailIcon from '@material-ui/icons/Mail';
const useStyles = makeStyles(() =>
  createStyles({
    root: {
      color: '#fff',
      paddingTop: 5,
      paddingBottom: 5,
      paddingLeft: 24,
    },
    btnAction: {
      justifyContent: 'flex-end',
      color: '#fff',
    },
    active: {
      background: 'rgb(255 255 255 / .2)',
    },
  })
);
interface ListSideNavProps {
  startIcon?: any;
  label?: string;
  onClickItem?: (listId?: string) => void;
  handleQuickAction?: (listId?: string) => void;
  listId?: string;
  component?: any;
  to: any;

  isActive?: boolean;
}
export const ListSideNav: FC<ListSideNavProps> = ({
  startIcon,
  label,
  onClickItem,
  handleQuickAction,
  listId,
  component,
  to,
  isActive,
}) => {
  const classes = useStyles();
  const handleClickItem = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    onClickItem && onClickItem(to);
  };
  const handleClickItemAction = (event: React.MouseEvent<{}>) => {
    event.preventDefault();
    handleQuickAction && handleQuickAction(listId);
  };
  return (
    <>
      <ListItem button onClick={handleClickItem} className={`${classes.root} ${isActive ? classes.active : ''}`}>
        <ListItemIcon style={{ color: '#fff' }}>{startIcon || <MailIcon />}</ListItemIcon>
        <ListItemText primary={label} />
        {component && (
          <ListItemIcon onClick={handleClickItemAction} className={classes.btnAction}>
            {component}
          </ListItemIcon>
        )}
      </ListItem>
    </>
  );
};
