import React, { FC } from 'react';
import CopyrightIcon from './icons/copyright.svg';
import useStyles from './styles';

const Copyright: FC = () => {
  const classes = useStyles({});
  return (
    <div className={classes.copyright}>
      <img src={CopyrightIcon} alt="Copyright Icon" width={20} height={20} />
      <p>
        Copyright 2019 Digital4win <br />
        Tous droits réservés
      </p>
    </div>
  );
};

export default Copyright;
