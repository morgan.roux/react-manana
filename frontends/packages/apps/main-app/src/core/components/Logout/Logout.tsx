import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';

const Logout = () => {
  const [disconnected, setDisconnected] = useState<boolean>(false);
  useEffect(() => {
    localStorage.clear();
    setDisconnected(true);
  }, []);

  if (disconnected) {
    return <Redirect to="/" />;
  }

  return <div>Déconnexion...</div>;
};
export default Logout;
