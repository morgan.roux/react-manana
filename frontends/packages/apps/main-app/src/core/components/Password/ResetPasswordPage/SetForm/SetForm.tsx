import React, { useState, ChangeEvent, FC } from 'react';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { NewCustomButton } from '@lib/ui-kit';
import { CustomFormTextField } from '@lib/ui-kit';
import { FormHelperText, IconButton, InputAdornment } from '@material-ui/core';
import BackLink from '../../BackLink';
import { isValidPassword } from '@lib/common';

interface SetFormProps {
  token: string;
  saveBtn?: string;
  loading?: boolean;
  onCancel: () => void;
  onRequestResetPassword: (token: string, password: string) => void;
}

const SetForm: FC<SetFormProps> = ({ saveBtn, token, loading, onCancel, onRequestResetPassword }) => {
  const [password, setPassword] = useState<string>('');
  const [confirmPassword, setConfirmPassword] = useState<string>('');
  const [showPassword, setShowPassword] = useState<boolean>(false);

  const [showConfirmPassword, setShowConfirmPassword] = useState<boolean>(false);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target as any;
    name === 'password' ? setPassword(value) : setConfirmPassword(value);
  };

  // function reset password
  const submitSetPassword = async () => {
    onRequestResetPassword(token, password);
  };

  const tooglePasswordVisibility = () => {
    showPassword ? setShowPassword(false) : setShowPassword(true);
  };

  const toogleConfirmPasswordVisibility = () => {
    showConfirmPassword ? setShowConfirmPassword(false) : setShowConfirmPassword(true);
  };

  const isDisabled = () => {
    if (!password || !confirmPassword) return true;
    if (password && !isValidPassword(password)) return true;
    const status = password !== confirmPassword;
    return status;
  };

  const error = !!(password && password !== confirmPassword);

  return (
    <>
      <div style={{ marginBottom: 35 }}>
        <CustomFormTextField
          name="password"
          type={showPassword ? 'text' : 'password'}
          label="Mot de passe"
          value={password}
          onChange={handleChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={tooglePasswordVisibility}>
                  {showPassword ? <VisibilityIcon /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        {password && !isValidPassword(password) && (
          <FormHelperText error={true}>
            Le mot de passe doit contenir au moins 8 caractères de types différents (majuscules, minuscules, chiffres,
            caractères spéciaux)
          </FormHelperText>
        )}
      </div>
      <div style={{ marginBottom: 45 }}>
        <CustomFormTextField
          name="confirmPassword"
          type={showConfirmPassword ? 'text' : 'password'}
          label=" Confirmer le mot de passe"
          value={confirmPassword}
          onChange={handleChange}
          error={error}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={toogleConfirmPasswordVisibility}>
                  {showConfirmPassword ? <VisibilityIcon /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        {password && confirmPassword && password !== confirmPassword && (
          <FormHelperText error={true}>Les deux mots de passe ne sont pas identiques</FormHelperText>
        )}
      </div>
      <BackLink onClick={onCancel} />
      <NewCustomButton size="large" onClick={submitSetPassword} fullWidth={true} disabled={isDisabled() || loading}>
        {saveBtn}
      </NewCustomButton>
    </>
  );
};

export default SetForm;

/*
SetForm.defaultProps = {
  saveBtn: 'REINITIALISER',
  type: 'reset',
  message: 'Votre mot de passe a été réinitialisé avec succès',
};
*/
