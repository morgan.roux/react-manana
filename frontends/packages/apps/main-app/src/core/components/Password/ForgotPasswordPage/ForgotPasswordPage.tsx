import React, { FC } from 'react';
import { SinglePage } from '@lib/ui-kit';
import ForgotForm, { ForgotPasswordFormProps } from './ForgotForm';

const ForgotPasswordPage: FC<ForgotPasswordFormProps> = (props) => {
  return (
    <SinglePage title="Pas de souci, cela peut arriver à tout le monde !">
      <ForgotForm {...props} />
    </SinglePage>
  );
};

export default ForgotPasswordPage;
