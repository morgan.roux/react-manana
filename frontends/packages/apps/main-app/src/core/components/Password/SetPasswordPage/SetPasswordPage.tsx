import React, { FC } from 'react';
import { SinglePage } from '@lib/ui-kit';
import { SetForm } from '../ResetPasswordPage/SetForm';

interface SetPasswordPageProps {
  token: string;
  loading?: boolean;
  onCancel: () => void;
  onRequestResetPassword: (token: string, password: string) => void;
}

const SetPasswordPage: FC<SetPasswordPageProps> = ({ token, loading, onCancel, onRequestResetPassword }) => {
  return (
    <SinglePage title="Nouveau mot de passe">
      <SetForm
        token={token}
        loading={loading}
        saveBtn="Enregistrer"
        onCancel={onCancel}
        onRequestResetPassword={onRequestResetPassword}
      />
    </SinglePage>
  );
};

//  message="Mot de passe enregistré avec succès"

export default SetPasswordPage;
