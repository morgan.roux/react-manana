import React, { FC } from 'react';
import { SinglePage } from '@lib/ui-kit';
import UpdatePasswordForm, { UpdatePasswordFormProps } from './UpdatePasswordForm';

const UpdatePasswordPage: FC<UpdatePasswordFormProps & { message: string }> = (props) => {
  return (
    <SinglePage title="Renouvellement de mot de passe">
      <div style={{ paddingBottom: 30 }}>{props.message}</div>
      <UpdatePasswordForm {...props} />
    </SinglePage>
  );
};

export default UpdatePasswordPage;
