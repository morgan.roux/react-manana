import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      textAlign: 'right',
      cursor: 'pointer',
      margin: 10,
    },
  })
);

export default useStyles;
