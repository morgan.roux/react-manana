import React, { useState, ChangeEvent, FC } from 'react';
import { FormHelperText, IconButton, InputAdornment } from '@material-ui/core';
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { CustomFormTextField, NewCustomButton } from '@lib/ui-kit';
import BackLink from '../BackLink';
import { isValidPassword } from '@lib/common';

interface FormValues {
  password: string;
  confirmPassword: string;
  showPassword: boolean;
}

export interface UpdatePasswordFormProps {
  loading?: boolean;
  onCancel: () => void;
  onRequestUpdatePassword: (password: string) => void;
}

const UpdatePasswordForm: FC<UpdatePasswordFormProps> = ({ loading, onCancel, onRequestUpdatePassword }) => {
  const [{ password, confirmPassword, showPassword }, setValues] = useState<FormValues>({
    password: '',
    confirmPassword: '',
    showPassword: false,
  });

  const handleChange = (e: ChangeEvent<HTMLInputElement>): void => {
    const { name, value } = e.target as any;
    setValues((prevState) => ({ ...prevState, [name]: value }));
  };

  const tooglePasswordVisibility = (): void => {
    setValues((prevState) => ({ ...prevState, showPassword: !prevState.showPassword }));
  };

  const isDisabled = (): boolean => {
    if (!isValidPassword(password) || !(password === confirmPassword)) return true;
    return false;
  };

  return (
    <>
      <div style={{ marginBottom: 35 }}>
        <CustomFormTextField
          name="password"
          type={showPassword ? 'text' : 'password'}
          label="Nouveau mot de passe"
          value={password}
          onChange={handleChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={tooglePasswordVisibility}>
                  {showPassword ? <VisibilityIcon /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        {password && !isValidPassword(password) && (
          <FormHelperText error={true} style={{ lineHeight: '1.5em', letterSpacing: '0.095em' }}>
            Le mot de passe doit contenir au moins 8 caractères de types différents (majuscules, minuscules, chiffres,
            caractères spéciaux)
          </FormHelperText>
        )}
      </div>

      <div style={{ marginBottom: 45 }}>
        <CustomFormTextField
          name="confirmPassword"
          type={showPassword ? 'text' : 'password'}
          label="Confirmer le nouveau mot de passe"
          value={confirmPassword}
          onChange={handleChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={tooglePasswordVisibility}>
                  {showPassword ? <VisibilityIcon /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        {password && confirmPassword && password !== confirmPassword && (
          <FormHelperText error={true} style={{ lineHeight: '1.5em', letterSpacing: '0.095em' }}>
            Les deux mots de passe ne sont pas identiques
          </FormHelperText>
        )}
      </div>

      <BackLink onClick={onCancel} />
      <NewCustomButton
        size="large"
        onClick={() => onRequestUpdatePassword(password)}
        fullWidth={true}
        disabled={isDisabled() || loading}
      >
        Mettre à jour
      </NewCustomButton>
    </>
  );
};

export default UpdatePasswordForm;
