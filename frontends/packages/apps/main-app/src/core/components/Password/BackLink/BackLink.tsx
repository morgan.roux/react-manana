import React, { FC } from 'react';
import { Link } from '@material-ui/core';
import useStyles from './styles';

interface BackLinkProps {
  text?: string;
  onClick: () => void;
}

const BackLink: FC<BackLinkProps> = ({ text = 'Annuler', onClick }) => {
  const classes = useStyles();
  return (
    <Link className={classes.root} onClick={onClick}>
      {text}
    </Link>
  );
};

export default BackLink;
