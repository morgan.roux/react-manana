import React, { FC } from 'react';
import { withStyles, createStyles } from '@material-ui/core';
import { SinglePage } from '@lib/ui-kit';
import { SetForm } from './SetForm';

const styles = () => createStyles({});

interface ResetPasswordProps {
  token: string;
  loading?: boolean;
  onCancel: () => void;
  onRequestResetPassword: (token: string, password: string) => void;
}

const ResetPassword: FC<ResetPasswordProps> = ({ token, loading, onCancel, onRequestResetPassword }) => {
  return (
    <SinglePage title="Nouveau mot de passe">
      <SetForm
        saveBtn="Enregistrer"
        token={token}
        loading={loading}
        onRequestResetPassword={onRequestResetPassword}
        onCancel={onCancel}
      />
    </SinglePage>
  );
};

export default withStyles(styles, { withTheme: true })(ResetPassword);
