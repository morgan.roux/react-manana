import React, { FC, KeyboardEvent, useState } from 'react';
import { InputAdornment, IconButton, FormControl, OutlinedInput } from '@material-ui/core/';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';
import { withStyles } from '@material-ui/core';
import BackLink from '../BackLink';
import { NewCustomButton } from '@lib/ui-kit';
import useStyles from './styles';

const CustomOutlinedInput = withStyles({
  root: {
    background: '#F3F3F3 0% 0% no-repeat padding-box',
    borderRadius: 3,
    border: 0,
    color: '#878787',
    height: 50,
    '&.Mui-focused': {
      borderColor: 'red',
      border: '5px',
    },
  },
})(OutlinedInput);

interface FormValues {
  login: string;
}

export interface ForgotPasswordFormProps {
  loading: boolean;
  onCancel: () => void;
  onRequestForgotPassword: (login: string) => void;
}

const ForgotPasswordForm: FC<ForgotPasswordFormProps> = ({ loading, onCancel, onRequestForgotPassword }) => {
  const classes = useStyles();

  const [values, setValues] = useState<FormValues>({ login: '' });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setValues((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleKeyPress = (e: KeyboardEvent<HTMLFormElement>) => {
    if (e.key === 'Enter') {
      onRequestForgotPassword(values.login);
    }
  };

  return (
    <div className={classes.contentForm}>
      <form onKeyPress={handleKeyPress} className={classes.form}>
        <p className={classes.message}>Entrez votre login, on vous enverra un lien pour changer votre mot de passe.</p>
        <FormControl variant="outlined" className={classes.formControl}>
          <CustomOutlinedInput
            placeholder="Login..."
            id="login"
            name="login"
            value={values.login}
            fullWidth={true}
            margin="dense"
            startAdornment={
              <InputAdornment position="start">
                <IconButton>
                  <PermIdentityIcon />
                </IconButton>
              </InputAdornment>
            }
            labelWidth={0}
            onChange={handleChange}
          />
        </FormControl>
        <BackLink onClick={onCancel} />
        <NewCustomButton
          variant="contained"
          className={classes.btnSend}
          size="large"
          onClick={() => onRequestForgotPassword(values.login)}
          fullWidth={true}
          disabled={loading}
        >
          {loading ? 'Chargement...' : 'Envoyer'}
        </NewCustomButton>
      </form>
    </div>
  );
};

export default ForgotPasswordForm;
