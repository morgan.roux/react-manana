import React, { FC } from 'react';
import { Grid, Typography, Card, CardContent, Link } from '@material-ui/core';
import { useStyles } from './styles';
import SigninForm, { RequestSignin } from './SigninForm';
import { CustomCheckBox } from '@lib/ui-kit';
import { useDomaineActivite } from '@lib/common';

import SigninWithSocial from './SigninWithSocial';
import Copyright from '../Copyright';
import { useLocalStorage } from '@lib/hooks';

interface SignInPageProps {
  loading?: boolean;
  onRequestSignin: (values: RequestSignin) => void;
  onRequestGoToForgotPassword: () => void;
}

const SignIn: FC<SignInPageProps> = ({ loading, onRequestSignin, onRequestGoToForgotPassword }) => {
  const classes = useStyles({});
  const domaineActivite = useDomaineActivite();
  const [rememberMe, setRememberMe] = useLocalStorage<string>('REMEMBER_ME', 'false');

  return (
    <Grid
      container={true}
      className={classes.root}
      style={{
        backgroundImage: `url(${domaineActivite.connexionLandingPage.publicUrl})`,
      }}
    >
      <Grid item={true} className={classes.cardContent}>
        <Card className={classes.card}>
          <CardContent className={classes.fullWidth}>
            <Typography variant="h3" gutterBottom={true} className={`${classes.centerText} ${classes.title}`}>
              Bienvenue
            </Typography>
            <p className={`${classes.centerText} ${classes.subtitle}`}>Sur votre portail Digital4win</p>
            <SigninForm loading={loading} onRequestSignin={onRequestSignin} />
            <div className={classes.stayLoggedInAndForgotPassword}>
              <CustomCheckBox
                label="Rester connecté"
                location="inLogin"
                checked={rememberMe === 'true'}
                onChange={(event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
                  setRememberMe(checked ? 'true' : 'false');
                }}
              />

              <Link className={classes.link} onClick={onRequestGoToForgotPassword}>
                Mot de passe oublié ?
              </Link>
            </div>
            <SigninWithSocial />
            <Copyright />
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
};

export default SignIn;
