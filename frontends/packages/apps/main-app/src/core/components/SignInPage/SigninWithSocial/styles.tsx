import { createStyles, makeStyles } from '@material-ui/core';
const useStyles = makeStyles(() =>
  createStyles({
    socialIconsTopText: {
      display: 'flex',
      margin: 'auto -80px',
      '&::before, &::after': {
        content: '""',
        flex: 1,
        borderBottom: '1px solid #fff',
        margin: 'auto 10px',
        width: '100%',
      },
    },
    socialIcons: {
      margin: '10px 0',
      display: 'flex',
      justifyContent: 'center',
      '& img': {
        width: 50,
        margin: '0 5px',
        cursor: 'pointer',
      },
      '& img:hover': {
        opacity: 0.9,
      },
    },
  })
);

export default useStyles;
