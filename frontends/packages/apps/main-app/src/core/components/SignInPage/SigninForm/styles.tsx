import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() =>
  createStyles({
    form: {
      display: 'flex',
      flexDirection: 'column',
    },
    formField: {
      '& .MuiOutlinedInput-input': {
        minHeight: 30,
        padding: '9px 14px !important',
      },
      '& label': {
        color: '#fff',
      },
    },
    btnLogin: {
      marginTop: 30,
      fontSize: '13px',
      textTransform: 'uppercase',
      minHeight: 45,
      minWidth: 175,
      width: '100%',
      fontWeight: 500,
      background: 'transparent linear-gradient(239deg, #F6D945 0%, #B3D00A 100%) 0% 0% no-repeat padding-box',
      boxShadow: '0px 3px 6px #00000029',
      borderRadius: 3,
      opacity: 1,
      color: '#fff',
      '&:hover': {
        opacity: 0.9,
      },
      '@media (max-width: 992px)': {
        marginTop: 20,
        minHeight: 40,
      },
    },
  })
);

export default useStyles;
