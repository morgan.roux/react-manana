import { Button, IconButton, InputAdornment } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import React, { FC, KeyboardEvent } from 'react';
import { CustomTextField } from '@lib/ui-kit';
import * as yup from 'yup';
import { useFormik } from 'formik';
import useStyles from './styles';

export interface RequestSignin {
  login: string;
  password: string;
}

interface SigninFormProps {
  loading?: boolean;
  onRequestSignin: (values: RequestSignin) => void;
}

const validationSchema = yup.object({
  login: yup.string().required(),
  password: yup.string().min(2).required(),
  showPassword: yup.bool(),
});

const SigninForm: FC<SigninFormProps> = ({ loading, onRequestSignin }) => {
  const classes = useStyles();

  const { values, handleSubmit, handleChange, setFieldValue, touched, errors } = useFormik({
    initialValues: {
      login: '',
      password: '',
      showPassword: false,
    },
    validationSchema,
    onSubmit: (values) => {
      onRequestSignin(values);
    },
  });

  const handleKeyPress = (e: KeyboardEvent<HTMLFormElement>) => {
    if (e.key === 'Enter') {
      handleSubmit();
    }
  };

  return (
    <form className={classes.form} onSubmit={handleSubmit} onKeyPress={handleKeyPress}>
      <CustomTextField
        label="Login"
        type="text"
        name="login"
        value={values.login}
        onChange={handleChange}
        margin="dense"
        variant="outlined"
        fullWidth={true}
        className={classes.formField}
        error={touched.login && !!errors.login}
        helperText={touched.login && errors.login}
      />

      <CustomTextField
        className={classes.formField}
        label="Mot de passe"
        type={values.showPassword ? 'text' : 'password'}
        name="password"
        value={values.password}
        onChange={handleChange}
        error={touched.password && !!errors.password}
        helperText={touched.password && errors.password}
        margin="dense"
        variant="outlined"
        fullWidth={true}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton
                style={{ color: '#fff' }}
                size="small"
                aria-label="toggle password visibility"
                onClick={() => setFieldValue('showPassword', !values.showPassword)}
                onMouseDown={(event) => event.preventDefault()}
              >
                {values.showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
      <Button type="submit" variant="contained" className={classes.btnLogin} size="small" disabled={loading}>
        {loading ? 'Chargement...' : 'Se connecter'}
      </Button>
    </form>
  );
};

export default SigninForm;
