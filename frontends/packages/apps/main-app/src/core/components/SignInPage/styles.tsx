import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'start',
      alignItems: 'center',
      width: '100%',
      height: '100vh',
      backgroundAttachment: 'fixed',
      backgroundSize: 'contain',
      backgroundPosition: 'right',
      backgroundRepeat: 'no-repeat',
      '@media (max-height: 604px)': {
        height: '100%',
      },
      '@media (max-width: 992px)': {
        backgroundImage: 'initial',
        height: '100%',
        justifyContent: 'center',
      },
    },
    fullWidth: {
      width: '100%',
    },
    cardContent: {
      position: 'relative',
      margin: '0px 0px 0px 12%',
      '& input:-webkit-autofill': {
        '-webkit-text-fill-color': '#ffffff',
      },
      '@media (max-width: 1440px)': {
        margin: '0 0 0 5%',
      },
      '@media (max-width: 992px)': {
        backgroundImage: 'initial',
        margin: '12px 0 12px 0',
      },
    },
    card: {
      width: 460,
      borderRadius: 0,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      padding: '20px 30px',
      background: 'transparent linear-gradient(32deg, #E34741 0%, #E34173 100%) 0% 0% no-repeat padding-box',
      boxShadow: '0px 10px 12px #00000040',
      color: '#fff',
      '@media (max-width: 1366px)': {
        padding: '8px 20px',
      },

      '@media (max-width: 442px)': {
        padding: '8px 10px',
        width: 320,
      },
    },
    centerText: {
      textAlign: 'center',
    },
    title: {
      '@media (max-width: 992px)': {
        fontSize: '2rem',
      },
    },
    subtitle: {
      fontWeight: 100,
      letterSpacing: 3,
      marginTop: -10,
      marginBottom: 25,
      '@media (max-width: 992px)': {
        marginBottom: 20,
        fontSize: '0.875rem',
      },
    },

    stayLoggedInAndForgotPassword: {
      fontSize: 14,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginTop: 20,
      marginBottom: 30,
      '& a': {
        color: '#fff',
      },
    },

    link: {
      fontSize: 14,
      color: theme.palette.common.white,
      cursor: 'pointer',
      fontFamily: 'Roboto',
      textDecoration: 'none',
    },
  })
);
