import React, { FC } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { useStyles } from './styles';
import Version from './Version';

const Footer: FC<RouteComponentProps> = ({ history: { push } }) => {
  const classes = useStyles();

  const handleGoToPolicy = () => push('/rgpd-politique-de-confident');

  return (
    <footer className={classes.footerRoot}>
      <Version style={{marginLeft:5,marginRight:5}} /> © {new Date().getFullYear()}. All Rights Reserved.
      <span className={classes.policyLink} onClick={handleGoToPolicy}>
        Politique de confidentialité
      </span>
    </footer>
  );
};

export default withRouter(Footer);
