import React, { FC } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Feature, filterRoutes, ProtectedRouteProps } from '@lib/common';

interface MainRouterProps {
  features: Feature[];
  routes: ProtectedRouteProps[];
}

const MainRouter: FC<MainRouterProps> = ({ routes }) => {
  const filteredRoutes = filterRoutes(routes, { attachTo: 'MAIN' });
  return (
    <Switch>
      {filteredRoutes.map(({ path, component }, index) => (
        <Route path={path} exact={true} key={`main-route-${index}`} strict={true} component={component as any} />
      ))}
    </Switch>
  );
};

export default MainRouter;
