import React, { FC, useState, useEffect } from 'react';
import clsx from 'clsx';
import {
  List,
  ListItem,
  ListItemText,
  Avatar,
  IconButton,
  CardActions,
  Collapse,
  Card,
  CardHeader,
  CardContent,
  Typography,
} from '@material-ui/core';
import useStyles from './styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Draggable from 'react-draggable';
import { useHistory, useLocation } from 'react-router';
import { Feature, filterFeatures, getOptionValue, useApplicationContext } from '@lib/common';

interface DraggableData {
  node: HTMLElement;
  // lastX + deltaX === x
  x: number;
  y: number;
  deltaX: number;
  deltaY: number;
  lastX: number;
  lastY: number;
}

export interface ResponseMessage {
  type: string;
  message: string;
}

const RecipeReviewCard: FC<{}> = () => {
  const classes = useStyles({});
  const location = useLocation();
  const history = useHistory();
  const [expanded, setExpanded] = React.useState(false);

  const appContext = useApplicationContext();
  const { features } = appContext;
  const [enabledFeatures, setEnabledFeatures] = useState<Feature[]>([]);

  useEffect(() => {
    const newEnabledFeatures = features; // filterFeatures(features, { location: 'OUTIL_DIGITAL' });
    setEnabledFeatures(newEnabledFeatures);
  }, [features]);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const handleDrag = (e: Event, data: DraggableData) => {
    if (data.x >= 20) {
      console.log('stop');
    }
  };

  if (
    enabledFeatures.length <= 0 ||
    enabledFeatures.some((feature) => feature.to && getOptionValue(feature.to, appContext) === location.pathname)
  ) {
    return null;
  }

  return (
    <div>
      <Draggable
        axis="x"
        handle="#draggable-content"
        onDrag={(e: any, data: any) => {
          return handleDrag(e, data);
        }}
      >
        <Card className={classes.card}>
          <CardHeader
            id="draggable-content"
            title={<div className={classes.title}>Autres Outils</div>}
            className={classes.header}
            action={
              <CardActions disableSpacing={true}>
                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expanded,
                  })}
                  onClick={handleExpandClick}
                  aria-expanded={expanded}
                  size="small"
                >
                  <ExpandMoreIcon className={classes.expandIcon} />
                </IconButton>
              </CardActions>
            }
          />
          <Collapse in={expanded} timeout="auto">
            <CardContent className={classes.body}>
              <Typography variant="body2" color="textSecondary" component="div">
                <List component="nav" className={classes.root}>
                  {enabledFeatures.map((feature) => {
                    return (
                      <ListItem
                        button
                        key={`list_${feature.id}`}
                        className={classes.cursor}
                        onClick={() => feature.to && history.push(getOptionValue(feature.to, appContext))}
                      >
                        <Avatar>{feature.icon}</Avatar>
                        <ListItemText
                          className={classes.appname}
                          primary={feature.name && getOptionValue(feature.name, appContext)}
                        />
                      </ListItem>
                    );
                  })}
                </List>
              </Typography>
            </CardContent>
          </Collapse>
        </Card>
      </Draggable>
    </div>
  );
};

export default RecipeReviewCard;
