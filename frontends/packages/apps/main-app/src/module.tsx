import { ModuleDefinition, mergeModules } from '@lib/common';
import authenticationModule from './authentication/authentication.module';
import baseModule from './base/base.module';
import notificationModule from './notification/notification.module';
import optionsModule from './options/options.module';
import groupementModule from './groupement/groupement.module';

const mainModule: ModuleDefinition = mergeModules(
  authenticationModule,
  baseModule,
  notificationModule,
  optionsModule,
  groupementModule
);
export default mainModule;
