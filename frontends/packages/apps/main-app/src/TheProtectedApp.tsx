import React, { FC, useEffect } from 'react';
import { StylesProvider, createGenerateClassName } from '@material-ui/core';
import {
  ModuleDefinition,
  initApp,
  useDisplayNotification,
  useDevice,
  extractEnabledFeatures,
  extractEnabledRoutes,
  ComputeUploadedFilenameOptions,
  uploadFilename,
  ApplicationContext,
  ApolloClientsProvider,
  ModulesProvider,
  ApplicationConfig,
  filterUserEnabledModules,
  uploadFilenameWithoutDate,
  ApplicationContextOptions,
  AppAuthorization,
  UploadedFile,
  uploadToS3,
  uploadToStorage,
  getTypeFichier,
} from '@lib/common';
import { ApolloClient } from '@apollo/client';
import { ErrorPage, MainLoading } from '@lib/ui-kit';
import { useTheme } from './core/Theme';
import Navigation from './core/Navigation';
import {
  findParameter,
  findParameterValue,
  isParameterValueEnabled,
  parseParameterValueAsBoolean,
  parseParameterValueAsDate,
  parseParameterValueAsMoney,
  parseParameterValueAsNumber,
  parseParameterValueAsString,
} from '@lib/common/src/shared/operations/parametres/util';
import { HashRouter } from 'react-router-dom';
import { useState } from 'react';
import { useCreate_Put_Pesigned_UrlMutation } from '@lib/common/src/graphql';
import TheProtectedAppIdleTimeout from './TheProtectedAppIdleTimeout';
import { DomaineActiviteInfoFragment } from '@lib/common/src/federation';
import { useLocalStorage } from '@lib/hooks';

const generateClassName = createGenerateClassName({
  //  disableGlobal: true,
  productionPrefix: 'main',
  seed: 'main',
});

interface TheProtectedAppProps {
  modules: ModuleDefinition[];
  federation: ApolloClient<any>;
  graphql: ApolloClient<any>;
  config: ApplicationConfig;
  domaineActivite: DomaineActiviteInfoFragment;
}

const TheProtectedApp: FC<TheProtectedAppProps> = ({ modules, federation, graphql, config, domaineActivite }) => {
  const { loading, error, user, parameters, currentGroupement, currentPharmacie, pharmacies, ip, accessToken } =
    initApp(federation, graphql);
  const [rememberMe] = useLocalStorage<string>('REMEMBER_ME', 'false');
  const [theme, changeTheme] = useTheme();

  const [doCreatePutPresignedUrl] = useCreate_Put_Pesigned_UrlMutation({
    client: federation,
  });

  const uploadFiles = async (
    files: (File | Blob | UploadedFile)[],
    options?: Partial<ComputeUploadedFilenameOptions>
  ): Promise<UploadedFile[]> => {
    const alreadyUploadedFiles = files
      .filter((file) => !(file instanceof File) && !(file instanceof Blob))
      .map(({ chemin, nomOriginal, type }: any) => ({ chemin, nomOriginal, type }));

    const toUploadFiles = files
      .filter((file) => file instanceof File || file instanceof Blob)
      .map((fileOrBlob) => {
        if (fileOrBlob instanceof File) {
          return fileOrBlob;
        }

        return new File([fileOrBlob as any], `blob.${fileOrBlob.type.split('/')[1]}`);
      });

    if (toUploadFiles.length > 0) {
      const computeFileOptions = { ...(options || {}), groupement: currentGroupement, pharmacie: currentPharmacie };
      if (config.STORAGE_PROVIDER === 'OVH_OBJECT_STORAGE') {
        return uploadToStorage(toUploadFiles as File[], computeFileOptions, config, user).then(
          (haveBeenUploadedFiles) => {
            return [...(haveBeenUploadedFiles || []), ...(alreadyUploadedFiles || [])];
          }
        );
      }

      const filePaths = toUploadFiles.map((file) => uploadFilename(file as File, computeFileOptions));

      return doCreatePutPresignedUrl({
        variables: {
          filePaths,
        },
      }).then(async (response) => {
        if (response.data?.createPutPresignedUrls) {
          return Promise.all(
            response.data.createPutPresignedUrls.map(async (presignedUrl, index: number) => {
              const toUploadFile: File = toUploadFiles[index] as File;

              const uploadResult = await uploadToS3(toUploadFile, presignedUrl?.presignedUrl as any);
              if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                return {
                  chemin: presignedUrl?.filePath,
                  nomOriginal: toUploadFile.name,
                  type: getTypeFichier(toUploadFile),
                };
              } else {
                console.log('**********************Result', uploadResult);
                throw new Error(`Failed to upload file ${toUploadFile.name}`);
              }
            })
          ).then((haveBeenUploadedFiles) => {
            return [...(haveBeenUploadedFiles || []), ...(alreadyUploadedFiles || [])];
          });
        }

        return alreadyUploadedFiles as any;
      });
    }

    return alreadyUploadedFiles as any;
  };

  const notify = useDisplayNotification(federation);
  const { isMobile, isLg, isMd, isSm, width } = useDevice();
  const [showAppBar, setShowAppBar] = useState<boolean>(true);

  useEffect(() => {
    if (user?.theme && theme !== user.theme) {
      changeTheme(user.theme as any);
    }
  }, [user]);

  if (loading) {
    return <MainLoading />;
  }

  if (error) {
    return <ErrorPage />;
  }

  const newEnableModules = filterUserEnabledModules({
    modules,
    user: user as any,
    // device: isMobile ? 'MOBILE' : 'WEB',
    parameters,
  });
  const auth = new AppAuthorization(user as any);

  const isSuperAdmin = user?.role?.code === 'SUPADM';
  const isAdminGroupement = user?.role?.code === 'GRPADM';
  const isTitulaire = user?.role?.code === 'PRMTIT';
  const isGroupePharmacie = user?.role?.groupe === 'PHARMACIE';

  const appContextValue: ApplicationContextOptions = {
    currentGroupement,
    currentPharmacie,
    pharmacies,
    parameters,
    federation,
    graphql,
    isSuperAdmin,
    isAdminGroupement,
    isTitulaire,
    isGroupePharmacie,
    user: user as any,
    isMobile,
    isLg,
    isMd,
    isSm,
    width,
    ip: ip || '',
    accessToken: accessToken || '',
    notify,
    computeUploadedFilename: (file: File, options?: Omit<ComputeUploadedFilenameOptions, 'groupement' | 'pharmacie'>) =>
      uploadFilename(file, { ...(options || {}), groupement: currentGroupement, pharmacie: currentPharmacie }),
    computeUploadedFilenameWithoutDate: (
      file: File,
      options?: Omit<ComputeUploadedFilenameOptions, 'groupement' | 'pharmacie'>
    ) =>
      uploadFilenameWithoutDate(file, {
        ...(options || {}),
        groupement: currentGroupement,
        pharmacie: currentPharmacie,
      }),
    useParameter: (code: string) => findParameter(parameters, code),
    useParameterValue: (code: string) => findParameterValue(parameters, code),
    useParameterValueAsBoolean: (code: string) => parseParameterValueAsBoolean(parameters, code),
    useParameterValueEnabled: (codeGroupement: string, codePharmacie: string) =>
      isParameterValueEnabled(parameters, codeGroupement, codePharmacie, isMobile),
    useParameterValueAsMoney: (code: string) => parseParameterValueAsMoney(parameters, code),
    useParameterValueAsString: (code: string) => parseParameterValueAsString(parameters, code),
    useParameterValueAsNumber: (code: string) => parseParameterValueAsNumber(parameters, code),
    useParameterValueAsDate: (code: string) => parseParameterValueAsDate(parameters, code),

    features: extractEnabledFeatures(newEnableModules),
    routes: extractEnabledRoutes(newEnableModules),
    modules: newEnableModules,
    config,
    auth,
    showAppBar,
    setShowAppBar,
    uploadFiles,
    domaineActivite,
  };

  console.log('--modules', newEnableModules, modules);
  return (
    <ApolloClientsProvider federation={federation} graphql={graphql}>
      <ModulesProvider modules={newEnableModules}>
        <ApplicationContext.Provider value={appContextValue as any}>
          <HashRouter>
            <StylesProvider generateClassName={generateClassName}>
              <Navigation />
              {rememberMe === 'false' && <TheProtectedAppIdleTimeout />}
            </StylesProvider>
          </HashRouter>
        </ApplicationContext.Provider>
      </ModulesProvider>
    </ApolloClientsProvider>
  );
};

export default TheProtectedApp;
