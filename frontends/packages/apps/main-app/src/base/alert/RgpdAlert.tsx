import React, { FC } from 'react';
import { useApplicationContext } from '@lib/common';
import { useCheck_Accept_RgpdQuery } from '@lib/common/src/graphql';
import RgpdPopup from '../components/Rgpd/RgpdPopup/RgpdPopup';

export interface RgpdAlertProps {
  open: boolean;
  setOpen: (open: boolean) => void;
}

const RgpdAlert: FC<RgpdAlertProps> = ({ open, setOpen }) => {
  const { user, graphql } = useApplicationContext();

  const { data: checkRgpdData, loading: checkRgpdLoading } = useCheck_Accept_RgpdQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
    variables: { idUser: (user && user.id) || '' },
    onError: (error) => {
      setOpen(false);
    },
  });

  React.useEffect(() => {
    if (checkRgpdData && !checkRgpdData.checkAcceptRgpd) {
      setOpen(!checkRgpdData.checkAcceptRgpd);
    } else {
      setOpen(false);
    }
  }, [checkRgpdData]);

  if (checkRgpdLoading) {
    return null;
  }

  return <RgpdPopup open={open} setOpen={setOpen} />;
};

export default RgpdAlert;
