import React, { FC, useEffect, useState } from 'react';
import useStyles, { SERVICE_CARD_WIDTH } from './styles';
import Footer from '../../../core/Main/Footer/Footer';
import { Badge, Box, Card, CardActions, CardContent, CircularProgress, Hidden, Typography } from '@material-ui/core';
import { Feature, filterFeatures, getOptionValue, isMobile, RichTextEditor, useApplicationContext } from '@lib/common';
import { useHistory } from 'react-router';

const computeContainerWidth = (allFeatures: Feature[], width: number) => {
  const computedWidth = (SERVICE_CARD_WIDTH + 30) * Math.floor(allFeatures.length / 2);
  return computedWidth > width || computedWidth === 0 ? undefined : computedWidth;
};

export const Page: FC<{}> = () => {
  const classes = useStyles({});
  const history = useHistory();
  const appContext = useApplicationContext();
  const { features, graphql, federation, user, width, currentGroupement, currentPharmacie, isMobile } = appContext;
  const [enabledFeatures, setEnabledFeatures] = useState<Feature[]>([]);
  // key: feature id
  const [totalCounts, setTotalCounts] = useState<Record<string, { loading: boolean; total: number }>>({});

  // alert
  const [enabledAlertFeatures, setEnabledAlertFeatures] = useState<Feature[]>([]);
  const [openedAlerts, setOpenedAlerts] = useState<boolean[]>([]);

  useEffect(() => {
    const newEnabledFeatures = filterFeatures(features, { location: 'ACCUEIL' });
    const newEnabledAlertFeatures = filterFeatures(features, { location: 'ALERT' });
    setEnabledFeatures(newEnabledFeatures);
    setEnabledAlertFeatures(newEnabledAlertFeatures);
    setTotalCounts({});
    newEnabledFeatures.forEach((f) => {
      if (!f.options?.totalCount) {
        return;
      }

      setTotalCounts((prev) => ({
        ...prev,
        [f.id]: {
          loading: true,
          total: 0,
        },
      }));
      f.options
        ?.totalCount({ graphql, user, federation, currentGroupement, currentPharmacie })
        .then((count: number) => {
          setTotalCounts((prev) => ({
            ...prev,
            [f.id]: {
              loading: false,
              total: count,
            },
          }));
        });
    });

    setOpenedAlerts(newEnabledAlertFeatures.map((f, index) => index === 0));
  }, [features]);

  const enabledFeaturesContainerWidth = React.useMemo(
    () => computeContainerWidth(enabledFeatures, width),
    [width, enabledFeatures]
  );

  const handleCloseAlert = (indexOfClosed: number) => {
    const nextOpenIndex = openedAlerts.findIndex((open) => open) + 1;

    console.log('***********************handleCloseAlert**', indexOfClosed);

    if (nextOpenIndex < openedAlerts.length) {
      setOpenedAlerts((prev) =>
        prev.map((open, index) => {
          return index === nextOpenIndex;
        })
      );
    } else {
      setOpenedAlerts((prev) =>
        prev.map(() => {
          return false;
        })
      );
    }
  };

  const Alert = React.useMemo(() => {
    const indexOfToOpenAlert = openedAlerts.findIndex((open) => open);
    if (indexOfToOpenAlert >= 0) {
      const feature = enabledAlertFeatures[indexOfToOpenAlert];

      if (feature.options?.openAlert) {
        return feature.options.openAlert(
          {
            federation,
            graphql,
            user,
            currentGroupement,
          },
          () => handleCloseAlert(indexOfToOpenAlert)
        );
      }
    }

    return null;
  }, [JSON.stringify(openedAlerts)]);

  console.log('***************************openedAlerts', openedAlerts, enabledAlertFeatures);
  // React Quill Text Editor Preview
  const [texts, setTexts] = useState<string>('');
  const handleChangeTextEditor = (value: string) => {
    setTexts(value);
  };
  return (
    <>
      <Box p={4}>
        <RichTextEditor />
      </Box>
      <Box className={classes.homepageRoot}>
        <Box className={classes.homepageService}>
          <Hidden mdDown={true}>
            <Box className={classes.titleSectionHome}>
              <Typography className={classes.homepageSectionTitle}>Nos services pour vous accompagner </Typography>
            </Box>
          </Hidden>

          {/*<Typography className={classes.homepageSectionSubTitle}>
          Parcourez une multitude de services spécialement créés pour vous.
        </Typography>*/}
          <Box
            className={classes.homepageServiceList}
            style={{ width: isMobile ? undefined : enabledFeaturesContainerWidth }}
          >
            {enabledFeatures.map((feature) => (
              <Card
                key={`homepage_service_item_${feature.id}`}
                className={classes.homepageServiceCard}
                // tslint:disable-next-line: jsx-no-lambda
                onClick={() => feature.to && history.push(getOptionValue(feature.to, appContext))}
              >
                <CardContent>
                  <Typography align="center" variant="subtitle2" color="textSecondary" gutterBottom={true}>
                    {feature.name || ''}
                  </Typography>
                  {feature.icon ? (
                    typeof feature.icon === 'string' ? (
                      <img
                        className={classes.imgService}
                        src={feature.icon}
                        alt={feature.name && getOptionValue(feature.name, appContext)}
                      />
                    ) : (
                      feature.icon
                    )
                  ) : null}
                </CardContent>
                {totalCounts[feature.id] ? (
                  <CardActions className={classes.badgeContainer}>
                    {totalCounts[feature.id].loading ? (
                      <CircularProgress size="1em" />
                    ) : totalCounts[feature.id].total > 0 ? (
                      <Badge
                        color="secondary"
                        max={999}
                        badgeContent={totalCounts[feature.id].total}
                        className={classes.badge}
                      />
                    ) : null}
                  </CardActions>
                ) : null}
              </Card>
            ))}
          </Box>
        </Box>
        <Hidden mdDown={true}>
          <Footer />
        </Hidden>
      </Box>
      {Alert}
    </>
  );
};

export default Page;
