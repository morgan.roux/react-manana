import { createStyles, makeStyles } from '@material-ui/core';
import { Theme } from '@material-ui/core';

export const SERVICE_CARD_WIDTH = 198;
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    homepageRoot: {
      // width: '100%',
      fontFamily: 'Roboto',
      letterSpacing: 0,
      minHeight: 'calc(100vh - 86px)',
      display: 'flex',
      flexDirection: 'column',
      overflow: 'auto',
      [theme.breakpoints.up('lg')]: {
        //  height: '100%',
      },
      [theme.breakpoints.down('sm')]: {
        paddingBottom: 50,
      },
    },
    noPubContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      width: '100%',
      height: 310,
      border: '2px dashed #9E9E9E',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    noPubTitle: {
      fontSize: 25,
      fontWeight: 'bolder',
      color: theme.palette.secondary.main,
    },
    noPubTexte: {
      maxWidth: 575,
      fontSize: 18,
      fontWeight: 'normal',
      color: '#616161',
      textAlign: 'center',
      marginTop: 20,
    },
    homepageSectionTitle: {
      fontSize: 25,
      fontWeight: 'bolder',
      textAlign: 'center',
      backgroundColor: '#ffffff',
      color: theme.palette.secondary.main,
      maxWidth: 480,
      margin: '0 auto',
    },
    homepageSectionSubTitle: {
      color: theme.palette.text.primary,
      fontSize: 18,
      fontWeight: 'normal',
      marginTop: 5,
      marginBottom: 25,
    },
    titleSectionHome: {
      position: 'relative',
      marginBottom: '2.5rem',
      '&:before ': {
        content: '""',
        position: 'absolute',
        width: '100%',
        height: 1,
        border: '1px solid #9E9E9E',
        top: '50%',
        zIndex: -1,
        left: 0,
      },
    },
    homepagePublicity: {
      '& .slick-arrow': {
        width: '50px !important',
        height: '50px !important',
        color: `${theme.palette.secondary.main} !important`,
        zIndex: 1,
      },
      '& .slick-arrow.slick-prev': {
        left: 50,
      },
      '& .slick-arrow.slick-next': {
        right: 50,
      },
    },
    homepageService: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'column',
      height: '100%',
      paddingTop: 24,
      [theme.breakpoints.down('sm')]: {
        paddingTop: 0,
        // maxHeight: 'calc(100vh - 150px)',
        overflow: 'auto',
      },
    },
    homepageServiceList: {
      display: 'grid',
      gridGap: 10,
      [theme.breakpoints.down('sm')]: {
        // height: 'calc(100vh - 180px)',
        overflow: 'auto',
        padding: '30px 15px',
        gridTemplateColumns: 'auto auto',
        gridAutoRows: 'max-content',
      },
      [theme.breakpoints.only('md')]: {
        padding: '30px 15px',
        gridTemplateColumns: 'auto auto auto',
        gridAutoRows: 'auto',
      },
      [theme.breakpoints.only('lg')]: {
        padding: '30px 15px',
        gridTemplateColumns: 'auto auto auto auto',
        gridAutoRows: 'auto',
      },
      [theme.breakpoints.up('lg')]: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        maxWidth: 1300,
        margin: '0 auto',
      },
    },
    homepageServiceCard: {
      background: '#F5F6FA',
      margin: '4px 5px 12px',
      boxShadow: 'none !important',
      cursor: 'pointer',
      '&:hover': {
        boxShadow: '0px 2px 12px 0px rgba(20,20,20,0.16)!important',
      },
      '& .MuiCardContent-root:last-child': {
        paddingBottom: '0!important',
      },
      '& .MuiTypography-colorTextSecondary': {
        color: '#212121',
      },
      [theme.breakpoints.up('md')]: {
        width: SERVICE_CARD_WIDTH,
      },
      display: 'flex',
      flexDirection: 'column',
    },
    imgService: { width: '100%' },
    homepagePromotion: {
      minHeight: 583,
      maxWidth: 1665,
      margin: '0 auto',
      padding: '50px 48px',
      [theme.breakpoints.down('lg')]: {
        maxWidth: 900,
        minHeight: 583,
      },
    },
    homepagePromotionSlider: {},
    menuAppel: {
      position: 'absolute',
      zIndex: 1000,
      bottom: 30,
      right: 30,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-end',
    },
    menuAppelItemContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-end',
      marginRight: 8,
    },
    menuAppelItem: {
      marginBottom: 16,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    menuAppelLabel: {
      color: theme.palette.common.black,
      background: theme.palette.common.white,
      borderRadius: 8,
      marginRight: 16,
      padding: 8,
      boxShadow: '1px 1px 5px black',
    },
    menuAppelFab: {},

    badgeContainer: {
      display: 'flex',
      flexGrow: 1,
      justifyContent: 'flex-end',
      paddingRight: 20,
      alignItems: 'flex-end',
    },
    badge: {
      marginBottom: 10,
    },
    dashboardIcon: {
      position: 'fixed',
      bottom: 56,
      left: 32,
    },
    dashboardIconMobile: {
      position: 'fixed',
      bottom: 120,
      left: 32,
    },
    fabsContainer: {
      display: 'flex',
      position: 'absolute',
      zIndex: 10000,
      bottom: 70 + 24,
      left: 24,
      [theme.breakpoints.up('md')]: {
        bottom: 32,
      },
      [theme.breakpoints.up('lg')]: {
        bottom: 32 + 27,
      },
      justifyContent: 'space-between',
      alignContent: 'center',
      flexDirection: 'column',
      '& button': {
        marginTop: 16,
      },
    },
  })
);

export default useStyles;
