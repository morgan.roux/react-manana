import React, { FC } from 'react';

interface ThemePreview {
  color: {
    primary: string;
    secondary?: string;
  };
  secondaryColor?: string;
  name: string;
}

const ThemePreview: FC<ThemePreview> = ({ color, name }) => {
  return (
    <div>
      <svg
        className="theme_preview_icon"
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        width="166"
        height="77"
        viewBox="0 0 166 77"
      >
        <defs>
          <rect id="a" width="164" height="75" rx="3" fill="#FFF" />
        </defs>
        <g fill="none" fillRule="evenodd">
          <g transform="translate(1 1)">
            <mask id="b" fill="#fff">
              <use xlinkHref="#a" />
            </mask>
            <rect className="theme_bg" stroke="#E6E6E6" x="-.5" y="-.5" width="165" height="76" rx="4" fill="#FFF" />
            <path className="theme_header" fill={color.primary} mask="url(#b)" d="M0 0h164v30H0z" />
            <text className="theme_name" mask="url(#b)" fontSize="12" fill="#FFF">
              <tspan x="7" y="20">
                {name}
              </tspan>
            </text>
            <path
              className="theme_content"
              d="M11 43a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm0 1a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm11.3-7h47.4l.8.1.4.4.1.8v3.4l-.1.8-.4.4-.8.1H22.3l-.8-.1a1 1 0 0 1-.4-.4l-.1-.8v-3.4l.1-.8.4-.4.8-.1zm58 0h74.4l.8.1.4.4.1.8v3.4l-.1.8-.4.4-.8.1H80.3l-.8-.1a1 1 0 0 1-.4-.4l-.1-.8v-3.4l.1-.8.4-.4.8-.1zm-58 12h23.4l.8.1.4.4.1.8v3.4l-.1.8-.4.4-.8.1H22.3l-.8-.1a1 1 0 0 1-.4-.4l-.1-.8v-3.4l.1-.8.4-.4.8-.1zm34 0h62.4l.8.1.4.4.1.8v3.4l-.1.8-.4.4-.8.1H56.3l-.8-.1a1 1 0 0 1-.4-.4l-.1-.8v-3.4l.1-.8.4-.4.8-.1zM11 56a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm0-1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"
              fill="#E6E6E6"
              fillRule="nonzero"
              mask="url(#b)"
            />
            <rect
              className="theme_btn"
              fill={color.secondary || color.primary}
              mask="url(#b)"
              x="7"
              y="62"
              width="26"
              height="6"
              rx="1"
            />
          </g>
        </g>
      </svg>
    </div>
  );
};

export default ThemePreview;
