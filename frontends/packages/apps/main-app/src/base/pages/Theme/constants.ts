import { availableThemes } from '../../../core/Theme/config';

const { angel, caramel, leaf, magic, onyx, raspberry, sky } = availableThemes as any;

export const THEMES_LIST = [
  {
    value: 'onyx',
    libelle: 'Onyx (Par défaut)',
    color: {
      primary: onyx.palette && onyx.palette.primary && onyx.palette.primary.main,
      secondary: onyx.palette && onyx.palette.secondary && onyx.palette.secondary.main,
    },
  },
  {
    value: 'raspberry',
    libelle: 'Raspberry',
    color: {
      primary: raspberry.palette && raspberry.palette.primary && raspberry.palette.primary.main,
      secondary: raspberry.palette && raspberry.palette.secondary && raspberry.palette.secondary.main,
    },
  },
  {
    value: 'caramel',
    libelle: 'Caramel',
    color: {
      primary: caramel.palette && caramel.palette.primary && caramel.palette.primary.main,
      secondary: caramel.palette && caramel.palette.secondary && caramel.palette.secondary.main,
    },
  },
  {
    value: 'sky',
    libelle: 'Sky',
    color: {
      primary: sky.palette && sky.palette.primary && sky.palette.primary.main,
      secondary: sky.palette && sky.palette.secondary && sky.palette.secondary.main,
    },
  },
  {
    value: 'leaf',
    libelle: 'Leaf',
    color: {
      primary: leaf.palette && leaf.palette.primary && leaf.palette.primary.main,
      secondary: leaf.palette && leaf.palette.secondary && leaf.palette.secondary.main,
    },
  },
  {
    value: 'magic',
    libelle: 'Magic',
    color: {
      primary: magic.palette && magic.palette.primary && magic.palette.primary.main,
      secondary: magic.palette && magic.palette.secondary && magic.palette.secondary.main,
    },
  },
  {
    value: 'angel',
    libelle: 'Angel',
    color: {
      primary: angel.palette && angel.palette.primary && angel.palette.primary.main,
      secondary: angel.palette && angel.palette.secondary && angel.palette.secondary.main,
    },
  },
];
