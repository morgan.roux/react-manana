import React from 'react';
import { THEMES_LIST } from './constants';
import { Radio, Typography } from '@material-ui/core';
import CircleCheckedFilled from '@material-ui/icons/CheckCircle';
import ThemePreview from './ThemePreview';
import useStyles from './styles';
import classnames from 'classnames';
import { useUpdate_User_ThemeMutation } from '@lib/common/src/graphql';
import { useApplicationContext } from '@lib/common';
import { useTheme } from '../../../core/Theme';
import { Backdrop } from '@lib/ui-kit';

const ThemePage = () => {
  const classes = useStyles({});
  const { notify, graphql } = useApplicationContext();
  const [theme, changeTheme] = useTheme();

  const [updateTheme, updatingTheme] = useUpdate_User_ThemeMutation({
    client: graphql,
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'Thème mis à jour avec succès',
      });

      window.location.reload();
    },
    onError: () => {
      notify({
        type: 'error',
        message: "Une erreur s'est produite lors de modification de thème",
      });
    },
  });

  const handleChange = (newTheme: string) => {
    if (theme !== newTheme) {
      updateTheme({
        variables: {
          theme: newTheme,
        },
      }).then(() => {
        changeTheme(newTheme as any);
      });
    }
  };

  return (
    <>
      {updatingTheme.loading && <Backdrop />}
      <div className={classes.themePageRootContainer}>
        <div>
          <Typography variant="h6">Thème</Typography>
          <Typography variant="caption">Personnalisez Digital4win...</Typography>
        </div>
        <div className={classes.themeListContainer}>
          {THEMES_LIST.map((item) => (
            <div
              key={item.value}
              className={
                theme === item.value ? classnames(classes.themeItem, classes.themeItemChecked) : classes.themeItem
              }
              onClick={() => handleChange(item.value)}
            >
              <Radio
                className={classes.themeItemRadio}
                checked={theme === item.value}
                onChange={() => handleChange(item.value)}
                value={item.value}
                name={item.value}
                inputProps={{ 'aria-label': `${item.value.toUpperCase()}` }}
                checkedIcon={<CircleCheckedFilled />}
              />
              <ThemePreview
                color={{
                  primary: item.color.primary,
                  secondary: item.color.secondary,
                }}
                name={item.libelle}
              />
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default ThemePage;
