import { createStyles, makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    themePageRootContainer: {
      display: 'flex',
      flexDirection: 'column',
      padding: 24,
    },
    themeListContainer: {
      display: 'flex',
      flexWrap: 'wrap',
      flexDirection: 'row',
      margin: '20px 0px',
    },
    themeItem: {
      position: 'relative',
      display: 'flex',
      alignItems: 'flex-start',
      padding: 5,
      cursor: 'pointer',
      margin: 0,
      // borderRadius: 5,
      // border: '1px solid transparent',
      '&:hover': {
        // border: `1px solid ${theme.palette.primary.main}`,
        opacity: 0.8,
      },
    },
    themeItemChecked: {
      // border: `1px solid ${theme.palette.primary.main}`,
    },
    themeItemRadio: {
      position: 'absolute',
      top: 50,
      left: 135,
      padding: 5,
    },
  })
);

export default useStyles;
