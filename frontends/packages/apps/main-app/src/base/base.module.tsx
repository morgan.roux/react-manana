import React from 'react';
import { ModuleDefinition } from '@lib/common';
import loadable from '@loadable/component';
import { PsychologyWhite, SmallLoading } from '@lib/ui-kit';
import { PARAMETRES_ROUTES_PREFIX } from '@lib/common';
import ThemeMenuIcon from './assets/icons/menu/theme.svg';
import ThemeIcon from '@material-ui/icons/Palette';
import { Home } from '@material-ui/icons';
import RGPDAlert from './alert/RgpdAlert';

const HomePage = loadable(() => import('./pages/Home/home'), {
  fallback: <SmallLoading />,
});

const ThemePage = loadable(() => import('./pages/Theme/ThemePage'), {
  fallback: <SmallLoading />,
});

const RgpdPoliConfidentialite = loadable(
  () => import('./components/Rgpd/PolitiqueDeConfidentialite/PolitiqueDeConfidentialite'),
  {
    fallback: <SmallLoading />,
  }
);

const baseModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_THEME',
      location: 'PARAMETRE',
      name: 'Thème',
      to: '/theme',
      icon: <ThemeIcon />,
      preferredOrder: 20,
    },
    {
      id: 'FEATURE_MENU_PRINCIPAL_THEME',
      location: 'MENU_PRINCIPAL',
      name: 'Thème',
      to: `${PARAMETRES_ROUTES_PREFIX}/theme`,
      icon: ThemeMenuIcon,
      preferredOrder: 20,
    },

    {
      id: 'FEATURE_GO_HOME',
      location: 'MOBILE_QUICK_ACCESS',
      name: 'Accueil',
      to: '/',
      icon: <Home />,
      options: {
        active: (path) => path === '/',
      },
      preferredOrder: 10,
    },

    {
      id: 'FEATURE_PARAMETRE_RGPD',
      location: 'PARAMETRE',
      name: 'RGPD',
      to: '/rgpd',
      icon: <PsychologyWhite color="inherit" />,
      preferredOrder: 120,
      authorize: {
        roles: ['SUPADM', 'GRPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_ALERT_RGPD',
      location: 'ALERT',
      preferredOrder: 10,
      options: {
        openAlert: (options, onClose) => {
          return <RGPDAlert open={true} setOpen={() => onClose()} />;
        },
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
  routes: [
    {
      path: '/',
      component: HomePage,
      exact: false,
      attachTo: 'MAIN',
    },
    {
      path: '/theme',
      component: ThemePage,
      exact: false,
      attachTo: 'PARAMETRE',
    },
    {
      attachTo: 'MAIN',
      path: '/rgpd-politique-de-confident/:tab?',
      component: RgpdPoliConfidentialite,
      exact: false,
    },
  ],
};

export default baseModule;
