import { Typography } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import React, { FC, useState } from 'react';
import useStyles from './styles';
import Add from '@material-ui/icons/Add';
import { withRouter } from 'react-router';
import AutorisationModal from './AutorisationModal/AutorisationModal';
import PaperItem from '../Common/PaperItem/PaperItem';

import { useRgpd_AutorisationsQuery } from '@lib/common/src/graphql';
import { useApplicationContext } from '@lib/common';
import { Backdrop } from '@lib/ui-kit';

interface AutorisationProps {}
const Autorisation: FC<AutorisationProps> = () => {
  const classes = useStyles({});
  const { graphql, user, notify } = useApplicationContext();
  const [openModal, setOpenModal] = useState(false);

  //authorizationList query
  const { data: authorizationData, loading: autorizationLoading } = useRgpd_AutorisationsQuery({
    client: graphql,
    onError: (error) => {
      notify({
        type: 'error',
        message: error.message,
      });
    },
  });
  const autorisations = authorizationData && authorizationData.rgpdAutorisations;

  const handleClick = (event: any) => {
    // openModal
    event.stopPropagation();
    setOpenModal(true);
  };

  return (
    <>
      <Box className={classes.root}>
        <Box display="flex" flexDirection="column" width="100%">
          <Box display="flex" alignItems="flex-end">
            <Typography className={classes.title}>Autorisation</Typography>
          </Box>
          <Typography className={classes.icon} onClick={handleClick}>
            <Add />
            Ajouter une autorisation
          </Typography>
        </Box>
        {autorizationLoading && <Backdrop value="Chargement en cours, veuillez patienter ..." />}
        <Box className={classes.contentPaperItem}>
          {autorisations &&
            autorisations.length > 0 &&
            autorisations.map((autorisation) => (
              <PaperItem
                key={autorisation.id}
                title={autorisation.title}
                content={autorisation.description}
                item={autorisation}
                isOnAutorisation={true}
              />
            ))}
        </Box>
        <Box>
          <AutorisationModal
            setOpen={setOpenModal}
            open={openModal}
            title="Ajouter une autorisation"
            closeIcon={true}
            withHeaderColor={true}
            isOnCreate={true}
          />
        </Box>
      </Box>
    </>
  );
};
export default withRouter(Autorisation);
