import { Typography, Box } from '@material-ui/core';
import React, { Fragment, FC, useState } from 'react';
import ReactQuill from 'react-quill';
import {
  useCreate_Update_Rgp_AcceuilMutation,
  useCreate_Update_Rgp_Acceuil_PlusesMutation,
  Rgpd_AccueilsDocument,
  Rgpd_AccueilsQuery,
  Rgpd_AccueilsQueryVariables,
  Rgpd_Accueils_PlusesDocument,
  Rgpd_Accueils_PlusesQuery,
  Rgpd_Accueils_PlusesQueryVariables,
} from '@lib/common/src/graphql';

import { useGetRgpdAcceuil, useGetRgpdAcceuilPlus } from './hooks';
import useStyles from './styles';
import { useApplicationContext } from '@lib/common';
import { Backdrop, NewCustomButton as CustomButton, CustomFormTextField } from '@lib/ui-kit';

interface Accueilprops {}
interface acceuilInitialStateProps {
  titre: string;
  description: string;
}
interface acceuilPlusInitialstateProps {
  titrePlus: string;
  descriptionPlus: string;
}
const Accueil: FC<Accueilprops> = ({}) => {
  const { graphql, notify, user } = useApplicationContext();

  // getting acceuil value from back
  const { RgpdAccueilData: acceuilData, RgpdAccueilLoading: acceuilLoading, id: idGroupement } = useGetRgpdAcceuil();
  const resultID =
    acceuilData && acceuilData.rgpdAccueils && acceuilData.rgpdAccueils[0] && acceuilData.rgpdAccueils[0].id;
  const result = acceuilData && acceuilData.rgpdAccueils && acceuilData.rgpdAccueils[0];
  console.log('idddd group====', idGroupement);

  // acceuil
  const [onCreate, setOnCreate] = useState<boolean>(true);
  React.useEffect(() => {
    if (acceuilData && acceuilData.rgpdAccueils && acceuilData.rgpdAccueils.length > 0) {
      setOnCreate(false);
    } else setOnCreate(true);
  }, [acceuilData]);

  // acceuil plus
  const [onCreatePlus, setOnCreatePlus] = useState<boolean>(true);
  const { RgpdAccueilPlusData: resultPlusData, RgpdAccueilPlusLoading: acceuilPlusLoading } = useGetRgpdAcceuilPlus();

  const resultDataAcceuilPlus =
    resultPlusData && resultPlusData.rgpdAccueilPluses && resultPlusData.rgpdAccueilPluses[0];
  const resultPlusID =
    resultPlusData &&
    resultPlusData.rgpdAccueilPluses &&
    resultPlusData.rgpdAccueilPluses[0] &&
    resultPlusData.rgpdAccueilPluses[0].id;
  React.useEffect(() => {
    if (resultPlusData && resultPlusData.rgpdAccueilPluses && resultPlusData.rgpdAccueilPluses.length > 0) {
      setOnCreatePlus(false);
    } else setOnCreatePlus(true);
  }, [resultPlusData]);

  // initial state for form change

  const acceuilInitialState = {
    titre: (result && result.title) || '',
    description: (result && result.description) || '',
  };

  // getting value from formsetAccueilValuePlus
  const [accueilValue, setAccueilValue] = useState<acceuilInitialStateProps>(acceuilInitialState);
  const [titrePlus, setTitrePlus] = useState<string>((resultDataAcceuilPlus && resultDataAcceuilPlus.title) || '');
  const [descriptionPlus, setDescriptionPlus] = useState<string>(
    (resultDataAcceuilPlus && resultDataAcceuilPlus.description) || ''
  );

  React.useEffect(() => {
    setAccueilValue(acceuilInitialState);
  }, [acceuilData, resultPlusData]);

  React.useEffect(() => {
    setTitrePlus((resultDataAcceuilPlus && resultDataAcceuilPlus.title) || '');
    setDescriptionPlus((resultDataAcceuilPlus && resultDataAcceuilPlus.description) || '');
  }, [resultDataAcceuilPlus, resultPlusData]);

  const descriptionAcceuilChange = (content: string) => {
    setAccueilValue({ ...accueilValue, description: content });
  };
  const descriptionAcceuilPlusChange = (content: string) => {
    setDescriptionPlus(content);
  };
  const formChange = (event: any) => {
    const { value, name } = event.target;
    setAccueilValue({ ...accueilValue, [name]: value });
  };
  const formChangePlus = (event: any) => {
    const { value, name } = event.target;
    setTitrePlus(value);
  };
  // mutation createUpdate texte accueil
  const [createUpdateRgpdAccueil, { loading: isLoading }] = useCreate_Update_Rgp_AcceuilMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createUpdateRgpdAccueil) {
        notify({
          type: 'error',
          message: onCreate ? 'Texte Accueil ajouté avec succès' : 'Texte Accueil modifié avec succès',
        });
      }
      setOnCreate(false);
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
    update: (cache, { data }) => {
      if (cache && data) {
        const req = cache.readQuery<Rgpd_AccueilsQuery, Rgpd_AccueilsQueryVariables>({
          query: Rgpd_AccueilsDocument,
          variables: { idGroupement },
        });
        if (req && onCreate) {
          const updated = data.createUpdateRgpdAccueil;
          cache.writeQuery<Rgpd_AccueilsQuery, Rgpd_AccueilsQueryVariables>({
            query: Rgpd_AccueilsDocument,
            variables: { idGroupement },
            data: { rgpdAccueils: [updated] },
          });
        } else if (req) {
          const result = req.rgpdAccueils && req.rgpdAccueils[0];
          if (result) {
            cache.writeQuery<Rgpd_AccueilsQuery, Rgpd_AccueilsQueryVariables>({
              query: Rgpd_AccueilsDocument,
              variables: { idGroupement },
              data: {
                rgpdAccueils: result as any,
              },
            });
          }
        }
      }
    },
  });

  // mutation createUpdate texte accueil plus
  const [createUpdateRgpdAccueilPlus, { loading: dataPlusInloading }] = useCreate_Update_Rgp_Acceuil_PlusesMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createUpdateRgpdAccueilPlus) {
        notify({
          type: 'success',
          message: onCreatePlus
            ? 'Texte en savoir plus ajouté avec succès'
            : 'Texte en savoir plus modifié avec succès',
        });
      }
      setOnCreatePlus(false);
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },

    update: (cache, { data }) => {
      if (cache && data) {
        const req = cache.readQuery<Rgpd_Accueils_PlusesQuery, Rgpd_Accueils_PlusesQueryVariables>({
          query: Rgpd_Accueils_PlusesDocument,
          variables: { idGroupement },
        });
        if (req && onCreatePlus) {
          const updated = data.createUpdateRgpdAccueilPlus;
          cache.writeQuery<Rgpd_Accueils_PlusesQuery, Rgpd_Accueils_PlusesQueryVariables>({
            query: Rgpd_Accueils_PlusesDocument,
            variables: { idGroupement },
            data: { rgpdAccueilPluses: [updated] },
          });
        } else if (req) {
          const result = req.rgpdAccueilPluses && req.rgpdAccueilPluses[0];

          cache.writeQuery<Rgpd_Accueils_PlusesQuery, Rgpd_Accueils_PlusesQueryVariables>({
            query: Rgpd_Accueils_PlusesDocument,
            variables: { idGroupement },
            data: {
              rgpdAccueilPluses: result as any,
            },
          });
        }
      }
    },
  });
  // handle click texte acceuil
  const handleClickAcceuil = () => {
    if (onCreate) {
      createUpdateRgpdAccueil({
        variables: { input: { title: accueilValue.titre, description: accueilValue.description } },
      });
    } else {
      createUpdateRgpdAccueil({
        variables: {
          input: {
            id: resultID,
            title: accueilValue.titre,
            description: accueilValue.description,
          },
        },
      });
    }
  };

  // handle create texte acceuil plus
  const handleClickAcceuilPlus = () => {
    if (onCreatePlus) {
      createUpdateRgpdAccueilPlus({
        variables: {
          input: { title: titrePlus, description: descriptionPlus },
        },
      });
    } else {
      createUpdateRgpdAccueilPlus({
        variables: {
          input: {
            id: resultPlusID,
            title: titrePlus,
            description: descriptionPlus,
          },
        },
      });
    }
  };
  const classes = useStyles({});

  // test button disable
  const [clickDisable, setClickDisable] = useState<boolean>(true);
  React.useEffect(() => {
    if (accueilValue.titre !== '' && accueilValue.description !== '' && accueilValue.description !== '<p></p>') {
      setClickDisable(false);
    } else setClickDisable(true);
  }, [accueilValue]);
  const [clickDisablePlus, setClickDisablePlus] = useState<boolean>(true);
  React.useEffect(() => {
    if (titrePlus !== '' && descriptionPlus !== '') {
      setClickDisablePlus(false);
    } else setClickDisablePlus(true);
  }, [descriptionPlus, titrePlus]);

  return (
    <Box className={classes.formRgpdAccueilRoot}>
      <Box className={classes.formContainer}>
        {acceuilPlusLoading && <Backdrop value="Chargement en cours, veuillez patienter ..." />}
        {isLoading && dataPlusInloading && <Backdrop value="Modification en cours, veuillez patienter ..." />}
        <Fragment key={`rgpd_accueil_form_item_1`}>
          <Typography className={classes.inputTitle}>Texte accueil</Typography>
          <CustomFormTextField
            name="titre"
            label="Titre"
            required={true}
            value={accueilValue.titre}
            onChange={formChange}
          />
          <ReactQuill
            theme="snow"
            className={classes.customReactQuill}
            value={accueilValue.description}
            onChange={descriptionAcceuilChange}
          />
          <CustomButton onClick={handleClickAcceuil} disabled={clickDisable} color="secondary">
            {onCreate ? 'CREER UN TEXTE ACCUEIL' : 'MODIFIER UN TEXTE ACCUEIL'}
          </CustomButton>
        </Fragment>
      </Box>

      <Box className={classes.formContainer}>
        <Fragment key={`rgpd_accueil_form_item_2`}>
          <Typography className={classes.inputTitle}>Texte en savoir plus</Typography>
          <CustomFormTextField
            name="titrePlus"
            label="Titre"
            required={true}
            value={titrePlus}
            onChange={formChangePlus}
          />
          <ReactQuill
            theme="snow"
            className={classes.customReactQuill}
            value={descriptionPlus}
            onChange={descriptionAcceuilPlusChange}
          />
          <CustomButton onClick={handleClickAcceuilPlus} disabled={clickDisablePlus} color="secondary">
            {onCreatePlus ? 'CREER UN TEXTE EN SAVOIR PLUS' : 'MODIFIER UN TEXTE EN SAVOIR PLUS'}
          </CustomButton>
        </Fragment>
      </Box>
    </Box>
  );
};
export default Accueil;
