import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formRgpdAccueilRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      padding: '36px 50px',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    title: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 20,
    },
    btnSave: {
      marginTop: 25,
      width: 200,
    },
    formContainer: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
      marginBottom: 25,
      '& .MuiFormControl-root': {
        marginBottom: 24,
      },
    },
    customReactQuill: {
      width: '100%',
      maxWidth: 700,
      outline: 'none',
      marginBottom: 18,
      marginTop: 25,
      border: '1px solid rgba(0, 0, 0, 0.23)',
      borderRadius: 4,
      '& .ql-toolbar.ql-snow': {
        border: '0px',
        borderBottom: '1px solid rgba(0, 0, 0, 0.23)',
      },
      '& .ql-container': {
        minHeight: 200,
        border: '0px',
      },
      '&:hover': {
        borderColor: theme.palette.primary.main,
      },
      '&:focus': {
        outline: `auto ${theme.palette.primary.main}`,
      },
    },
  }),
);

export default useStyles;
