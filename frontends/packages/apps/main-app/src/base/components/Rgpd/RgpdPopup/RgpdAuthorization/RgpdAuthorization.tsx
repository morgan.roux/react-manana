import { IconButton, Typography, Box } from '@material-ui/core';
import React, { FC } from 'react';
import useStyles from './styles';
import AddIcon from '@material-ui/icons/Add';
interface RgpdAuthorizationProps {}
export const RgpdAuthorization: FC<RgpdAuthorizationProps> = () => {
  const classes = useStyles({});
  const handleClick = () => {};
  return (
    <>
      <Box className={classes.root}>
        <Box>
          <Typography className={classes.title}> Partenaires </Typography>
        </Box>
        <Box>
          <IconButton onClick={(event) => handleClick()}>
            <AddIcon />
          </IconButton>
        </Box>
      </Box>
    </>
  );
};
