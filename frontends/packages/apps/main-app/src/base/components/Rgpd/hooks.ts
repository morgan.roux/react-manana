import { useApplicationContext } from '@lib/common';
import {
  useCreate_Update_RgpdMutation,
  useRgpdsQuery,
  Create_Update_RgpdMutationVariables,
} from '@lib/common/src/graphql';

export const useCreateUpdateRgpd = (values: Create_Update_RgpdMutationVariables) => {
  const { graphql, notify } = useApplicationContext();

  const [createUpdateRgpd, { loading, data }] = useCreate_Update_RgpdMutation({
    client: graphql,
    variables: { ...values },
    onCompleted: (data) => {
      if (data && data.createUpdateRgpd) {
        notify({ type: 'success', message: 'Enregistrement effectué avec succès' });
      }
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  return { createUpdateRgpd, loading, data };
};

export const useGetRgpds = () => {
  const { graphql, currentGroupement, notify } = useApplicationContext();

  const { loading, data } = useRgpdsQuery({
    client: graphql,
    variables: { idGroupement: currentGroupement.id },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  return { loading, data };
};
