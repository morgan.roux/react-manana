export interface ActionButtonMenu {
  label: any;
  icon: ReactNode;
  disabled: boolean;
  onClick: (event: any) => any;
  setState?: (row: any) => void;
  setOperationName?: (operationName: string) => void;
  hide?: boolean;
}
