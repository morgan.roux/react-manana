import React, { FC } from 'react';
import { useTheme } from '@material-ui/core/styles';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import useStyles from './styles';
import classnames from 'classnames';
import { ListItem, Box, CssBaseline, Drawer, Hidden, IconButton } from '@material-ui/core';
import ArrowIcon from './Common/Icon/arrowDroprightCircle';
import RgpdRouter from './RgpdRouter';

export interface IMenuItem {
  path: string;
  label: string;
  show: boolean;
  icon?: JSX.Element;
}

const Rgpd: FC<RouteComponentProps> = ({ history, location: { pathname } }) => {
  const classes = useStyles({});
  const handlePush = (link: string) => {
    history.push(link);
  };
  const container = window !== undefined ? () => window.document.body : undefined;

  const items: IMenuItem[] = [
    {
      path: '/db/rgpd/accueil',
      label: 'Accueil',
      show: true,
    },
    {
      path: '/db/rgpd/autorisation',
      label: 'Autorisation',
      show: true,
    },
    {
      path: '/db/rgpd/partenaires',
      label: 'Partenaires',
      show: true,
    },
    {
      path: '/db/rgpd/politique-de-confidentialite',
      label: 'Politique de confidentialité',
      show: true,
    },
    {
      path: '/db/rgpd/conditions-d-utilisations',
      label: "Conditions d'utilisation",
      show: true,
    },
    {
      path: '/db/rgpd/informations-sur-les-cookies',
      label: 'Informations sur les Cookies',
      show: true,
    },
  ];

  const menuItems = items.filter((item) => item.show);

  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <Box>
      {menuItems &&
        menuItems.map((item, index) => {
          const pathName = pathname.split('/')[3];
          const itemPath = item.path.split('/')[3];
          const result: boolean =
            (pathName && itemPath && pathName === itemPath) === true || (index === 0 && !pathName);
          return (
            <ListItem
              className={classnames(
                classes.listItem,
                (pathname === item.path || (pathName && itemPath && pathName === itemPath)) && classes.listItemActive
              )}
              button={true}
              onClick={(e) => handlePush(item.path)}
              key={`item-${index}`}
            >
              {item.label}
            </ListItem>
          );
        })}
    </Box>
  );

  return (
    <Box className={classes.root}>
      <CssBaseline />
      <IconButton
        color="inherit"
        aria-label="open drawer"
        edge="start"
        onClick={handleDrawerToggle}
        className={classes.menuButton}
      >
        <ArrowIcon />
      </IconButton>
      <nav className={classes.drawer} aria-label="mailbox folders">
        <Hidden mdUp={true} implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor="left"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true,
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden smDown={true} implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open={true}
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <RgpdRouter />
      </main>
    </Box>
  );
};

export default withRouter(Rgpd);
