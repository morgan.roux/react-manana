import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    onPaper: {
      fontSize: '14px',
      marginTop: 25,
      padding: 20,
      margin: 20,
    }, 
  }),
);

export default useStyles;
