import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Delete, Edit, MoreHoriz } from '@material-ui/icons';
import React, { useState } from 'react';

import {
  useRemove_Rgpd_PartenaireMutation,
  Rgpd_PartenairesDocument,
  Rgpd_PartenairesQuery,
  Rgpd_PartenairesQueryVariables,
} from '@lib/common/src/graphql';

import PartenaireModal from '../PartenairesModal/PartenaireModal';
import { useApplicationContext } from '@lib/common';
import { ActionButtonMenu } from '../../types';
import { ConfirmDeleteDialog } from '@lib/ui-kit';

interface PartenaireActionButtonProps {
  rgpd?: any;
  refetch?: any;
  item: any;
}
const PartenaireActionButton: React.FC<PartenaireActionButtonProps> = ({ item }) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = anchorEl ? true : false;
  const [openEditModal, setOpenEditModal] = useState<boolean>(false);
  const [openDeleteDialogRgpd, setOpenDeleteDialogRgpd] = useState<boolean>(false);
  const { graphql, notify } = useApplicationContext();

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    }
  };

  // delete autorisation
  const [deletePartenaire] = useRemove_Rgpd_PartenaireMutation({
    client: graphql,
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'Autorisation supprimée avec succès',
      });
      setOpenDeleteDialogRgpd(false);
    },
    onError: (error) => {
      notify({
        type: 'error',
        message: error.message,
      });
    },
    update: (cache, { data }) => {
      if (cache && data) {
        const req = cache.readQuery<Rgpd_PartenairesQuery, Rgpd_PartenairesQueryVariables>({
          query: Rgpd_PartenairesDocument,
        });
        if (req) {
          const idTofilter = data && data.deleteRgpdPartenaire && data.deleteRgpdPartenaire.id;
          const result = [...req.rgpdPartenaires];
          const resultFiltered = result.filter((item) => idTofilter !== item.id);
          console.log('resultFiltered==>', resultFiltered);
          cache.writeQuery<Rgpd_PartenairesQuery, Rgpd_PartenairesQueryVariables>({
            query: Rgpd_PartenairesDocument,
            data: {
              rgpdPartenaires: resultFiltered,
            },
          });
        }
      }
    },
  });
  const onConfirmDelete = () => {
    console.log('itemmm id to delete====>>>', item.id);
    deletePartenaire({ variables: { id: item.id } });
  };
  const handleDeleteAutorisation = () => {
    setOpenDeleteDialogRgpd(true);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  // modification autorisation

  const handleOpenModalRgpdAction = () => {
    setOpenEditModal(true);
  };

  const rgpdlistMenuItems: ActionButtonMenu[] = [
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: handleOpenModalRgpdAction,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handleDeleteAutorisation,
      disabled: false,
    },
  ];

  return (
    <>
      <IconButton size="small" aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        <MoreHoriz />
      </IconButton>
      <Menu
        // tslint:disable-next-line: jsx-no-lambda
        onClick={(event) => {
          event.preventDefault();
          event.stopPropagation();
        }}
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {rgpdlistMenuItems &&
          rgpdlistMenuItems.length > 0 &&
          rgpdlistMenuItems.map((i: ActionButtonMenu, index: number) => (
            <MenuItem
              // tslint:disable-next-line: jsx-no-lambda
              onClick={(event) => {
                event.preventDefault();
                event.stopPropagation();
                handleClose();
                i.onClick(event);
              }}
              key={`table_menu_item_${index}`}
              disabled={i.disabled}
            >
              <ListItemIcon>{i.icon}</ListItemIcon>
              <Typography variant="inherit">{i.label}</Typography>
            </MenuItem>
          ))}
      </Menu>
      <ConfirmDeleteDialog
        open={openDeleteDialogRgpd}
        onClickConfirm={onConfirmDelete}
        setOpen={setOpenDeleteDialogRgpd}
      />
      <PartenaireModal
        setOpen={setOpenEditModal}
        open={openEditModal}
        title="Modifier un partenaire"
        closeIcon={true}
        withHeaderColor={true}
        item={item}
        isOnCreate={false}
      />
    </>
  );
};

export default PartenaireActionButton;
