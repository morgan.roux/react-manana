import React, { FC, useState } from 'react';
import { useStyles } from './styles';
import Box from '@material-ui/core/Box';
import ReactQuill from 'react-quill';

import {
  useCreate_Update_Rgpd_PartenaireMutation,
  Rgpd_PartenairesDocument,
  Rgpd_PartenairesQuery,
  Rgpd_PartenairesQueryVariables,
} from '@lib/common/src/graphql';
import { useApplicationContext } from '@lib/common';
import { CustomFormTextField, CustomModal } from '@lib/ui-kit';

interface PartenaireModalModalProps {
  open: boolean;
  setOpen: (open: boolean) => void;
  title: string;
  closeIcon?: boolean;
  withHeaderColor?: boolean;
  onClickConfirm?: any;
  item?: any | undefined;
  isOnCreate: boolean;
}

const PartenaireModal: FC<PartenaireModalModalProps> = ({
  open,
  setOpen,
  title,
  closeIcon,
  withHeaderColor,
  item,
  isOnCreate,
}) => {
  const classes = useStyles({});
  const { graphql, notify } = useApplicationContext();

  //valeur initiale popup

  const [titre, setTitre] = useState<string>(isOnCreate ? '' : item.title);
  const [description, setDescription] = useState<string>(isOnCreate ? '' : item.description);

  //form change popup

  const descriptionChange = (content: string) => {
    setDescription(content);
  };
  const formChange = (event: any) => {
    const { value, name } = event.target;
    setTitre(value);
  };

  //mutation create and update partenaires
  const [CreateUpdatePartenaire] = useCreate_Update_Rgpd_PartenaireMutation({
    client: graphql,
    onCompleted: () => {
      notify({
        type: 'success',
        message: isOnCreate ? 'Partenaire ajouté avec succès' : 'Partenaire modifié avec succès',
      });
      setOpen(false);
    },
    onError: (error) => {
      notify({
        type: 'error',
        message: error.message,
      });
    },
    update: (cache, { data }) => {
      if (cache && data) {
        const req = cache.readQuery<Rgpd_PartenairesQuery, Rgpd_PartenairesQueryVariables>({
          query: Rgpd_PartenairesDocument,
        });
        const IDdel = data.createUpdateRgpdPartenaire.id;
        if (req && isOnCreate) {
          const result = [...req.rgpdPartenaires, ...[data.createUpdateRgpdPartenaire]];
          cache.writeQuery<Rgpd_PartenairesQuery, Rgpd_PartenairesQueryVariables>({
            query: Rgpd_PartenairesDocument,
            data: {
              rgpdPartenaires: result,
            },
          });
          setDescription('');
          setTitre('');
        } else if (req) {
          const result = [...req.rgpdPartenaires];
          const resultFiltered = result.map((item) => {
            if (item.id === IDdel) return data.createUpdateRgpdPartenaire;
            else return item;
          });

          cache.writeQuery<Rgpd_PartenairesQuery, Rgpd_PartenairesQueryVariables>({
            query: Rgpd_PartenairesDocument,
            data: {
              rgpdPartenaires: resultFiltered,
            },
          });
        }
      }
    },
  });

  const handleCreateUpdatePartenaire = (title: string, content: string) => {
    if (title && content) {
      const RgpdCreatePartenaireInput = isOnCreate
        ? { title: title, description: content }
        : { id: item.id, title: title, description: content };
      CreateUpdatePartenaire({ variables: { input: { ...RgpdCreatePartenaireInput } } });
    }
  };

  const handleClickConfirm = (event: any) => {
    event.stopPropagation();
    if (titre !== '' && description !== '') {
      handleCreateUpdatePartenaire(titre, description);
    }
  };

  //activebutton
  const [activeButton, setActiveButton] = useState<boolean>(false);
  React.useEffect(() => {
    if (titre !== '' && description !== '') {
      setActiveButton(true);
    } else setActiveButton(false);
  }, [titre, description]);
  return (
    <>
      <CustomModal
        open={open}
        setOpen={setOpen}
        title={title}
        withBtnsActions={activeButton}
        closeIcon={closeIcon}
        headerWithBgColor={withHeaderColor}
        maxWidth="sm"
        fullWidth={true}
        className={classes.usersModalRoot}
        onClickConfirm={handleClickConfirm}
        withCancelButton={false}
        actionButtonTitle="ENREGISTRER"
      >
        <Box className={classes.tableContainer}>
          <Box className={classes.section}>
            <CustomFormTextField name="titre" label="titre" value={titre} required={true} onChange={formChange} />
          </Box>
          <Box className={classes.section}>
            <ReactQuill
              className={classes.customReactQuill}
              theme="snow"
              value={description}
              onChange={descriptionChange}
            />
          </Box>
        </Box>
      </CustomModal>
    </>
  );
};
export default PartenaireModal;
