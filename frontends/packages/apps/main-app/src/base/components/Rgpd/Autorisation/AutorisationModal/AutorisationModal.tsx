import React, { FC, useState } from 'react';
import { useStyles } from './styles';
import Box from '@material-ui/core/Box';
import ReactQuill from 'react-quill';
import {
  useCreate_AuthorizationMutation,
  Rgpd_AutorisationsDocument,
  Rgpd_AutorisationsQueryVariables,
  Rgpd_AutorisationsQuery,
} from '@lib/common/src/graphql';
import { useApplicationContext } from '@lib/common';
import { CustomFormTextField, CustomModal } from '@lib/ui-kit';

interface AutorisationModalProps {
  open: boolean;
  setOpen: (value: boolean) => void;
  title: string;
  withButtonAction?: boolean;
  closeIcon?: boolean;
  withHeaderColor?: boolean;
  onClickConfirm?: any;
  item?: any | undefined;
  isOnCreate: boolean;
}
const AutorisationModal: FC<AutorisationModalProps> = ({
  open,
  setOpen,
  title,
  closeIcon,
  withHeaderColor,
  item,
  isOnCreate,
}) => {
  const classes = useStyles({});
  const { graphql, user, notify } = useApplicationContext();

  //valeur initiale popup

  const [titre, setTitre] = useState<string>(isOnCreate ? '' : item.title);
  const [description, setDescription] = useState<string>(isOnCreate ? '' : item.description);

  //form change popup
  const descriptionChange = (content: string) => {
    setDescription(content);
  };
  const formChange = (event: any) => {
    const { value, name } = event.target;

    setTitre(value);
  };

  //mutation create and update autorisation
  const [CreateUpdateAuthorization] = useCreate_AuthorizationMutation({
    onCompleted: () => {
      notify({
        type: 'success',
        message: isOnCreate ? 'Autorisation ajoutée avec succès' : 'Autorisation modifiée avec succès',
      });
      setOpen(false);
    },
    onError: (error) => {
      notify({
        type: 'error',
        message: error.message,
      });
    },
    update: (cache, { data }) => {
      if (cache && data) {
        const req = cache.readQuery<Rgpd_AutorisationsQuery, Rgpd_AutorisationsQueryVariables>({
          query: Rgpd_AutorisationsDocument,
        });
        const IDdel = data.createUpdateRgpdAutorisation.id;
        if (req && isOnCreate) {
          const result = [...req.rgpdAutorisations, ...[data.createUpdateRgpdAutorisation]];
          cache.writeQuery<Rgpd_AutorisationsQuery, Rgpd_AutorisationsQueryVariables>({
            query: Rgpd_AutorisationsDocument,
            data: {
              rgpdAutorisations: result,
            },
          });
          setTitre('');
          setDescription('');
        } else if (req) {
          const result = [...req.rgpdAutorisations];
          const resultFiltered = result.map((item) => {
            if (item.id === IDdel) return data.createUpdateRgpdAutorisation;
            else return item;
          });

          cache.writeQuery<Rgpd_AutorisationsQuery, Rgpd_AutorisationsQueryVariables>({
            query: Rgpd_AutorisationsDocument,
            data: {
              rgpdAutorisations: resultFiltered,
            },
          });
        }
      }
    },
  });

  const handleCreateUpdateAuthorization = (title: string, content: string) => {
    if (title && content) {
      const RgpdCreateAutorisationInput = isOnCreate
        ? { title: title, description: content }
        : { id: item.id, title: title, description: content };
      CreateUpdateAuthorization({ variables: { input: { ...RgpdCreateAutorisationInput } } });
    }
  };

  const handleClickConfirm = (event: any) => {
    event.stopPropagation();
    if (titre !== '' && description !== '') {
      handleCreateUpdateAuthorization(titre, description);
    }
  };

  //activebutton
  const [activeButton, setActiveButton] = useState<boolean>(false);
  React.useEffect(() => {
    if (titre !== '' && description !== '') {
      setActiveButton(true);
    } else setActiveButton(false);
  }, [titre, description]);
  return (
    <>
      <CustomModal
        open={open}
        setOpen={setOpen}
        title={title}
        withBtnsActions={activeButton}
        closeIcon={closeIcon}
        headerWithBgColor={withHeaderColor}
        maxWidth="sm"
        fullWidth={true}
        className={classes.usersModalRoot}
        onClickConfirm={handleClickConfirm}
        withCancelButton={false}
        actionButtonTitle="ENREGISTRER"
      >
        <Box className={classes.tableContainer}>
          <Box className={classes.section}>
            <CustomFormTextField name="titre" label="titre" value={titre} required={true} onChange={formChange} />
          </Box>
          <Box className={classes.section}>
            <ReactQuill
              className={classes.customReactQuill}
              theme="snow"
              value={description}
              onChange={descriptionChange}
            />
          </Box>
        </Box>
      </CustomModal>
    </>
  );
};
export default AutorisationModal;
