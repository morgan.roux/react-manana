import React, { FC, useEffect, useState } from 'react';
import useStyles from './styles';
import { Box } from '@material-ui/core';
import Politique from './Politique/Politique';
import Information from './Information/Information';
import Condition from './Condition/Condition';
import { useParams } from 'react-router';
import { CustomTabs, TabInterface } from '@lib/ui-kit';
import SubToolbar from '../../SubToolbar';

const RgpdPoliConfidentialite: FC<{}> = ({}) => {
  const { tab } = useParams<{ tab?: string }>();

  const classes = useStyles({});
  const [currentTab, setCurrentTab] = useState(tab || '');

  const tabs: TabInterface[] = [
    {
      id: 0,
      label: 'POLITIQUE DE CONFIDENTIALITÉ',
      content: <Politique />,
      clickHandlerParams: 'politique',
    },
    {
      id: 1,
      label: `CONDITIONS D'UTILISATION`,
      content: <Condition />,
      clickHandlerParams: 'condition',
    },
    {
      id: 2,
      label: 'INFORMATIONS SUR LES COOKIES',
      content: <Information />,
      clickHandlerParams: 'information',
    },
  ];

  const handleTabClick = (param: string) => {
    window.history.pushState(null, '', `/#/rgpd-politique-de-confident/${param}`);
    setCurrentTab(param);
  };
  const tabStep = [
    { index: 0, tab: 'politique' },
    { index: 1, tab: 'information' },
    { index: 2, tab: 'condition' },
  ];

  const activeStepItem = tabStep.find((tabItem) => tabItem.tab === tab);
  const activeStep = activeStepItem && activeStepItem.index;
  const [title, setTitle] = useState<string>();

  useEffect(() => {
    if (currentTab === 'politique') setTitle('Politique de confidentialité');
    else if (currentTab === 'condition') setTitle(`Conditions d'utilisation`);
    else setTitle('Informations sur les cookies');
  }, [currentTab]);

  return (
    <Box className={classes.container}>
      <SubToolbar dark={true} withBackBtn={true} title={title}></SubToolbar>
      <Box className={classes.fichePharmacieMainContent}>
        <CustomTabs tabs={tabs} hideArrow={true} clickHandler={handleTabClick} activeStep={activeStep} />
      </Box>
    </Box>
  );
};
export default RgpdPoliConfidentialite;
