import { Typography } from '@material-ui/core';
import { uniqBy } from 'lodash';
import React, { Dispatch, FC, Fragment, SetStateAction } from 'react';
import image from '../../../assets/rgpd/rgpd_image.svg';
import {
  useRgpd_AccueilsQuery,
  useCreate_Update_Rgpd_HistoriqueMutation,
  RgpdHistoriqueInfoPlusInput,
  RgpdHistoriqueInfoPlusType,
  Create_Update_Rgpd_HistoriqueMutationVariables,
} from '@lib/common/src/graphql';
import RgpdPopupPartenaire, { RgpdPartenaireInterface } from './RgpdPopupPartenaire';
import RgpdPopupPlus, { AuthorizationInterface } from './RgpdPopupPlus';
import useStyles from './styles';
import { nl2br, useApplicationContext } from '@lib/common';
import { NewCustomButton as CustomButton, CustomModal } from '@lib/ui-kit';

export interface RgpdPopupProps {
  open: boolean;
  setOpen: (open: boolean) => void;
}

export interface RgpdPopupChildProps {
  historiquesInfosPlus: RgpdHistoriqueInfoPlusInput[];
  setHistoriquesInfosPlus: React.Dispatch<React.SetStateAction<RgpdHistoriqueInfoPlusInput[]>>;
  setIsOnAccueiPlus: Dispatch<SetStateAction<boolean>>;
  setIsOnPartenaire: Dispatch<SetStateAction<boolean>>;
  display: 'none' | 'flex';
  mutationVariables: Create_Update_Rgpd_HistoriqueMutationVariables;
  handleClickSaveAndClose: (variables?: Create_Update_Rgpd_HistoriqueMutationVariables) => void;
}

const RgpdPopup: FC<RgpdPopupProps> = ({ open, setOpen }) => {
  const classes = useStyles({});
  const { currentGroupement, user, graphql, notify } = useApplicationContext();

  const idGroupement = currentGroupement.id;
  const idUser = user.id;

  const [isOnAccueiPlus, setIsOnAccueiPlus] = React.useState<boolean>(false);
  const [isOnPartenaire, setIsOnPartenaire] = React.useState<boolean>(false);

  const [authorizationList, setAuthorizationList] = React.useState<AuthorizationInterface[]>([]);
  const [partenaireList, setPartenaireList] = React.useState<RgpdPartenaireInterface[]>([]);

  const [historiquesInfosPlus, setHistoriquesInfosPlus] = React.useState<RgpdHistoriqueInfoPlusInput[]>([]);

  const histoAccepteds = historiquesInfosPlus.map((i) => i.accepted);
  const isAcceptAll = !histoAccepteds.includes(false);
  const isRefuseAll = !histoAccepteds.includes(true);

  const title = 'La sécurité de vos données est pour nous une priorité absolue';
  const description =
    "Digital4win et ses partenaires utilisent des cookies ou des technologies similaires. En cliquant sur « Accepter », vous consentez à l’utilisation de ces technologies afin d'optimiser l’expérience utilisateur, d’améliorer le site, de réaliser des mesures d’audience et des études statistiques et mesurer la performance de nos campagnes publicitaires. Vous pouvez vous informer et modifier votre choix à tout moment en accédant à notre politique en matière de cookies.";

  const { data } = useRgpd_AccueilsQuery({
    client: graphql,
    variables: { idGroupement },
    onError: (error) => {
      console.log('error :>> ', error);
      error.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  const rgpdAccueil = data && data.rgpdAccueils && data.rgpdAccueils[0];

  const variables: Create_Update_Rgpd_HistoriqueMutationVariables = {
    input: {
      idUser,
      accept_all: isAcceptAll,
      refuse_all: isRefuseAll,
      historiquesInfosPlus: !isRefuseAll && !isAcceptAll ? historiquesInfosPlus : [],
    },
  };

  const [doCreateRgpdHistorique] = useCreate_Update_Rgpd_HistoriqueMutation({
    client: graphql,
    onError: (error) => {
      console.log('error :>> ', error);
      error.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
    },
  });

  const handleClickSaveAndClose = (variables?: Create_Update_Rgpd_HistoriqueMutationVariables) => {
    doCreateRgpdHistorique({ variables });
    setTimeout(() => {
      setOpen(false);
    }, 500);
  };

  const handleClickMore = () => {
    setIsOnAccueiPlus(true);
  };

  // Set historiquesInfosPlus
  React.useEffect(() => {
    const newsAuthHistoriques: RgpdHistoriqueInfoPlusInput[] = authorizationList.map((i) => {
      const histo: RgpdHistoriqueInfoPlusInput = {
        accepted: i.accepted !== undefined ? i.accepted : true,
        idItemAssocie: i.id,
        type: RgpdHistoriqueInfoPlusType.Autorisation,
      };
      return histo;
    });

    const newsParteHistoriques: RgpdHistoriqueInfoPlusInput[] = partenaireList.map((i) => {
      const histo: RgpdHistoriqueInfoPlusInput = {
        accepted: i.accepted !== undefined ? i.accepted : true,
        idItemAssocie: i.id,
        type: RgpdHistoriqueInfoPlusType.Partenaire,
      };
      return histo;
    });

    const array = [...newsAuthHistoriques, ...newsParteHistoriques];
    setHistoriquesInfosPlus(uniqBy(array, 'idItemAssocie'));
  }, [authorizationList, partenaireList]);

  return (
    <CustomModal
      title=""
      open={open}
      setOpen={setOpen}
      closeIcon={false}
      withBtnsActions={false}
      headerWithBgColor={false}
      maxWidth="sm"
      fullWidth={true}
    >
      <div className={classes.root}>
        <RgpdPopupPlus
          setIsOnAccueiPlus={setIsOnAccueiPlus}
          setIsOnPartenaire={setIsOnPartenaire}
          setHistoriquesInfosPlus={setHistoriquesInfosPlus}
          historiquesInfosPlus={historiquesInfosPlus}
          authorizationList={authorizationList}
          setAuthorizationList={setAuthorizationList}
          display={isOnAccueiPlus && !isOnPartenaire ? 'flex' : 'none'}
          handleClickSaveAndClose={handleClickSaveAndClose}
          mutationVariables={variables}
        />
        <RgpdPopupPartenaire
          setIsOnPartenaire={setIsOnPartenaire}
          setIsOnAccueiPlus={setIsOnAccueiPlus}
          setHistoriquesInfosPlus={setHistoriquesInfosPlus}
          historiquesInfosPlus={historiquesInfosPlus}
          display={isOnPartenaire && !isOnAccueiPlus ? 'flex' : 'none'}
          partenaireList={partenaireList}
          setPartenaireList={setPartenaireList}
          handleClickSaveAndClose={handleClickSaveAndClose}
          mutationVariables={variables}
        />
        {!isOnPartenaire && !isOnAccueiPlus && (
          <Fragment>
            <img className={classes.image} src={image} alt="Rgpd" />
            <Typography className={classes.title}>{(rgpdAccueil && rgpdAccueil.title) || title}</Typography>
            <Typography
              className={classes.description}
              dangerouslySetInnerHTML={
                { __html: nl2br((rgpdAccueil && rgpdAccueil.description) || description) } as any
              }
            />
            <div className={classes.btnsContainer}>
              <CustomButton
                color="secondary"
                variant="contained"
                // tslint:disable-next-line: jsx-no-lambda
                onClick={() => handleClickSaveAndClose(variables)}
              >
                Accepter & Fermer
              </CustomButton>
              <CustomButton
                color="default"
                variant="text"
                onClick={handleClickMore}
                className={classes.btnWithUnderline}
              >
                En savoir plus
              </CustomButton>
            </div>
          </Fragment>
        )}
      </div>
    </CustomModal>
  );
};

export default RgpdPopup;
