import { Theme, lighten } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';
const drawerWidth = 320;

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { display: 'flex', position: 'relative' },
    drawer: {
      height: 'calc(100vh - 86px)',
      overflowY: 'auto',
      '& .MuiListItem-root': {
        padding: '14px 20px',
      },
      [theme.breakpoints.up('md')]: {
        width: drawerWidth,
        flexShrink: 0,
        padding: '14px 40px',
        borderRight: '1px solid #E0E0E0',
      },
    },

    menuButton: {
      position: 'absolute',
      top: 0,
      left: 12,
      background: '#373740',
      borderTopLeftRadius: 0,
      borderBottomLeftRadius: 0,
      borderTopRightRadius: 50,
      borderBottomRightRadius: 50,
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
      '&:hover': {
        backgroundColor: '#373740',
        opacity: 0.98,
      },
      zIndex: 2,
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
      position: 'relative',
      boxShadow: 'none!important',
      borderRight: '1px solid rgba(0, 0, 0, 0)',
    },
    content: {
      flexGrow: 1,
      height: 'calc(100vh - 86px)',
      overflowY: 'auto',
      position: 'relative',
    },
    leftOperation: {
      minWidth: 280,
      padding: 24,
      borderRight: '2px solid #E5E5E5',
      height: 'calc(100vh - 86px)',
      overflowY: 'auto',
    },
    nested: {
      paddingLeft: theme.spacing(4),
    },
    icon: {
      fontSize: 20,
    },
    listItem: {
      borderRadius: 5,
      '&:hover': {
        backgroundColor: '#F5F6FA',

        '& > *': {
          color: theme.palette.common.white,
        },
      },
    },
    listItemActive: {
      backgroundColor: lighten(theme.palette.secondary.main, 0.2),
      color: theme.palette.common.white,
      '& > *': {
        color: theme.palette.common.white,
      },
      '&:hover': {
        color: '#373740',
      },
    },
  }),
);

export default useStyles;
