import { NewCustomButton as CustomButton } from '@lib/ui-kit';
import { Box, Typography } from '@material-ui/core';
import React, { Fragment, useEffect, useState } from 'react';
import ReactQuill from 'react-quill';
import { Create_Update_RgpdMutationVariables } from '@lib/common/src/graphql';
import { useCreateUpdateRgpd, useGetRgpds } from '../hooks';
import useStyles from './styles';

const ConditionsDutilisations = () => {
  const { data: getData, loading: getLoading } = useGetRgpds();
  const exist = getData && getData.rgpds && getData.rgpds[0];

  //const [isExit, setIsExist] = useState ("boolean");

  const [description, setDescription] = useState(exist ? exist.conditionUtilisation : '');
  const descriptionChange = (content: string) => {
    content && setDescription(content);
  };
  useEffect(() => {
    exist && setDescription(exist.conditionUtilisation);
  }, [getData]);
  const variables: Create_Update_RgpdMutationVariables = {
    input: {
      id: (exist && exist.id) || '',
      politiqueConfidentialite: (exist && exist.politiqueConfidentialite) || '',
      informationsCookies: (exist && exist.informationsCookies) || '',
      conditionUtilisation: description,
    },
  };
  const { createUpdateRgpd, loading, data } = useCreateUpdateRgpd(variables);
  const onClickSave = () => {
    createUpdateRgpd();
  };
  const classes = useStyles({});
  return (
    <Box className={classes.formRgpdAccueilRoot}>
      <Box className={classes.formContainer}>
        <Box display="flex" alignItems="flex-end">
          <Typography className={classes.title}>Conditions d'utilisation</Typography>
        </Box>

        <ReactQuill
          theme="snow"
          className={classes.customReactQuill}
          onChange={descriptionChange}
          value={description || ''}
        />

        <CustomButton color="secondary" className={classes.btnSave} onClick={onClickSave}>
          ENREGISTRER
        </CustomButton>
      </Box>
    </Box>
  );
};
export default ConditionsDutilisations;
