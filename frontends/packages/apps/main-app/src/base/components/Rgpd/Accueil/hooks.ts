import { useApplicationContext } from '@lib/common';
import { useRgpd_AccueilsQuery, useRgpd_Accueils_PlusesQuery } from '@lib/common/src/graphql';

// query to get accueil data
export const useGetRgpdAcceuil = () => {
  const { graphql, currentGroupement, notify } = useApplicationContext();
  const id = currentGroupement.id;

  const { loading: RgpdAccueilLoading, data: RgpdAccueilData } = useRgpd_AccueilsQuery({
    client: graphql,
    variables: { idGroupement: id },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  return { RgpdAccueilLoading, RgpdAccueilData, groupement: currentGroupement, id };
};

// query to get accueil data plus
export const useGetRgpdAcceuilPlus = () => {
  const { graphql, currentGroupement: groupement, notify } = useApplicationContext();

  const { loading: RgpdAccueilPlusLoading, data: RgpdAccueilPlusData } = useRgpd_Accueils_PlusesQuery({
    client: graphql,
    variables: { idGroupement: groupement.id },

    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  return { RgpdAccueilPlusLoading, RgpdAccueilPlusData };
};
