import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Delete, Edit, MoreHoriz } from '@material-ui/icons';
import React, { useState } from 'react';
import {
  useRemove_AuthorizationMutation,
  Rgpd_AutorisationsDocument,
  Rgpd_AutorisationsQueryVariables,
  Rgpd_AutorisationsQuery,
} from '@lib/common/src/graphql';

import AutorisationModal from '../AutorisationModal/AutorisationModal';
import { useApplicationContext } from '@lib/common';
import { ActionButtonMenu } from '../../types';
import { ConfirmDeleteDialog } from '@lib/ui-kit';

interface RgpdActionButtonProps {
  rgpd?: any;
  refetch?: any;
  item: any;
}
const AutorisationActionButton: React.FC<RgpdActionButtonProps> = ({ item }) => {
  const { graphql, notify } = useApplicationContext();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = anchorEl ? true : false;
  const [openEditModal, setOpenEditModal] = useState<boolean>(false);
  const [openDeleteDialogRgpd, setOpenDeleteDialogRgpd] = useState<boolean>(false);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    }
  };

  // delete autorisation
  const [deleteAutorisation] = useRemove_AuthorizationMutation({
    client: graphql,
    onCompleted: () => {
      notify({
        type: 'success',
        message: 'Autorisation supprimée avec succès',
      });
      setOpenDeleteDialogRgpd(false);
    },
    onError: (error) => {
      notify({
        type: 'error',
        message: error.message,
      });
    },
    update: (cache, { data }) => {
      if (cache && data) {
        const req = cache.readQuery<Rgpd_AutorisationsQuery, Rgpd_AutorisationsQueryVariables>({
          query: Rgpd_AutorisationsDocument,
        });
        if (req) {
          const idTofilter = data && data.deleteRgpdAutorisation && data.deleteRgpdAutorisation.id;
          const result = [...req.rgpdAutorisations];
          const resultFiltered = result.filter((item) => idTofilter !== item.id);
          console.log('result deleted===>>', data);
          cache.writeQuery<Rgpd_AutorisationsQuery, Rgpd_AutorisationsQueryVariables>({
            query: Rgpd_AutorisationsDocument,
            data: {
              rgpdAutorisations: [...resultFiltered],
            },
          });
        }
      }
    },
  });
  const onConfirmDelete = () => {
    deleteAutorisation({ variables: { id: item.id } });
  };
  const handleDeleteAutorisation = () => {
    setOpenDeleteDialogRgpd(true);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  // modification autorisation

  const handleOpenModalRgpdAction = () => {
    setOpenEditModal(true);
  };

  const rgpdlistMenuItems: ActionButtonMenu[] = [
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: handleOpenModalRgpdAction,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handleDeleteAutorisation,
      disabled: false,
    },
  ];

  return (
    <>
      <IconButton aria-controls="simple-menu" size="small" aria-haspopup="true" onClick={handleClick}>
        <MoreHoriz />
      </IconButton>
      <Menu
        // tslint:disable-next-line: jsx-no-lambda
        onClick={(event) => {
          event.preventDefault();
          event.stopPropagation();
        }}
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {rgpdlistMenuItems &&
          rgpdlistMenuItems.length > 0 &&
          rgpdlistMenuItems.map((i: ActionButtonMenu, index: number) => (
            <MenuItem
              // tslint:disable-next-line: jsx-no-lambda
              onClick={(event) => {
                event.preventDefault();
                event.stopPropagation();
                handleClose();
                i.onClick(event);
              }}
              key={`table_menu_item_${index}`}
              disabled={i.disabled}
            >
              <ListItemIcon>{i.icon}</ListItemIcon>
              <Typography variant="inherit">{i.label}</Typography>
            </MenuItem>
          ))}
      </Menu>
      <ConfirmDeleteDialog
        open={openDeleteDialogRgpd}
        onClickConfirm={onConfirmDelete}
        setOpen={setOpenDeleteDialogRgpd}
      />
      <AutorisationModal
        setOpen={setOpenEditModal}
        open={openEditModal}
        title="Modifier une autorisation"
        closeIcon={true}
        withHeaderColor={true}
        withButtonAction={true}
        item={item}
        isOnCreate={false}
      />
    </>
  );
};

export default AutorisationActionButton;
