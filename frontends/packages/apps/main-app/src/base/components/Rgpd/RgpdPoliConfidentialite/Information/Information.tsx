import React, { FC } from 'react';
import useStyles from './styles';
import { Box, Paper } from '@material-ui/core';
import { useGetRgpds } from '../../hooks';

interface InformationProps {
  Content?: any;
}

const Information: FC<InformationProps> = ({}) => {
  const { data: getData, loading: getLoading } = useGetRgpds();
  const exist = (getData && getData.rgpds && getData.rgpds[0] && getData.rgpds[0].informationsCookies) || '';
  const classes = useStyles({});
  return (
    <>
      <Paper elevation={20} className={classes.onPaper}>
        <Box dangerouslySetInnerHTML={{ __html: exist }}></Box>
      </Paper>
    </>
  );
};
export default Information;
