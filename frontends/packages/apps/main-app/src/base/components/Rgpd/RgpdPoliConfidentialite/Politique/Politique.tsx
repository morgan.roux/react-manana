import React, { FC } from 'react';
import useStyles from './styles';
import { Box, Paper } from '@material-ui/core';
import { useGetRgpds } from '../../hooks';

interface PolitiqueProps {
  Content?: any;
}

const Politique: FC<PolitiqueProps> = ({}) => {
  const { data: getData, loading: getLoading } = useGetRgpds();
  const exist = (getData && getData.rgpds && getData.rgpds[0] && getData.rgpds[0].politiqueConfidentialite) || '';

  const classes = useStyles({});
  return (
    <>
      <Paper elevation={20} className={classes.onPaper}>
        <Box dangerouslySetInnerHTML={{ __html: exist }}></Box>
      </Paper>
    </>
  );
};
export default Politique;
