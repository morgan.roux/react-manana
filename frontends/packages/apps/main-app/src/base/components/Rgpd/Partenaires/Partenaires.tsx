import { Box, Typography } from '@material-ui/core';
import { Add } from '@material-ui/icons';
import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';

import { useRgpd_PartenairesQuery } from '@lib/common/src/graphql';

import PaperItem from '../Common/PaperItem';
import PartenaireModal from './PartenairesModal/PartenaireModal';
//import ActionButton from '../Section/RgpdActionButton11';
import useStyles from './styles';
import { useApplicationContext } from '@lib/common';
import { Backdrop } from '@lib/ui-kit';

interface PartenairesProps {
  listResult?: any;
}

const Partenaires: FC<PartenairesProps & RouteComponentProps> = ({}) => {
  const classes = useStyles({});
  const { graphql, notify } = useApplicationContext();
  // query to get partenaires
  const { data: partenaireData, loading: partenaireDataLoading } = useRgpd_PartenairesQuery({
    client: graphql,
    onError: (error) => {
      notify({
        type: 'error',
        message: error.message,
      });
    },
  });
  const [openModal, setOpenModal] = useState(false);
  const handleClick = (event: any) => {
    // openModal
    event.stopPropagation();
    setOpenModal(true);
  };

  const partenaires = partenaireData && partenaireData.rgpdPartenaires;
  return (
    <Box className={classes.root}>
      <Box display="flex" flexDirection="column" width="100%">
        <Box display="flex" alignItems="flex-end">
          <Typography className={classes.title}>Partenaires</Typography>
        </Box>
        <Typography className={classes.icon} onClick={handleClick}>
          <Add />
          Ajouter un partenaire
        </Typography>
      </Box>
      {partenaireDataLoading && <Backdrop value="Chargement en cours, veuillez patienter ..." />}
      <Box className={classes.contentPaperItem}>
        {partenaires &&
          partenaires.length > 0 &&
          partenaires.map((partenaire) => (
            <PaperItem
              key={partenaire.id}
              title={partenaire.title}
              content={partenaire.description}
              item={partenaire}
              isOnAutorisation={false}
            />
          ))}
      </Box>
      <Box>
        <PartenaireModal
          setOpen={setOpenModal}
          open={openModal}
          title="Ajouter un partenaire"
          closeIcon={true}
          withHeaderColor={true}
          isOnCreate={true}
        />
      </Box>
    </Box>
  );
};

export default withRouter(Partenaires);
