import React, { FC } from 'react';
import { CssBaseline, makeStyles } from '@material-ui/core';

const useGlobalStyles = makeStyles(
  () => ({
    '@global': {
      '*:-webkit-full-screen': {
        height: '100%',
        width: '100%',
      },
      html: {
        position: 'fixed',
      },
      'html, body': {
        height: '100%',
        width: '100%',
      },
      '#root': {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
      },
    },
  }),
  { name: 'ViewportGlobals' }
);

const Viewport: FC<{}> = ({ children }) => {
  useGlobalStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      {children}
    </React.Fragment>
  );
};

export default Viewport;
