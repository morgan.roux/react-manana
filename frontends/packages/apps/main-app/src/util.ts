import { computePublicBasePath } from '@lib/common';

let cachedBasePath: string | undefined = undefined;

export const getBasePath = async (): Promise<string> => {
  if (!cachedBasePath) {
    //@ts-ignore
    return fetch(`${__webpack_public_path__}env-config.json`, {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'Cache-Control': 'no-cache',
      },
      cache: 'no-store',
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.I18N_TRANSLATION_FILES_STORAGE === 'WEBHOOK') {
          cachedBasePath = `${data.WEBHOOK_URL}/public`;
        } else {
          //STORAGE_PROVIDER
          cachedBasePath = computePublicBasePath(data);
        }
        return cachedBasePath;
      });
  }

  return cachedBasePath;
};
