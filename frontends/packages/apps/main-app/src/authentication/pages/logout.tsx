import React, { FC, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { MainLoading } from '@lib/ui-kit';
import { useHistory } from 'react-router-dom';
import { useLogoutMutation } from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';

const Page: FC<{}> = () => {
  const history = useHistory();
  const { federation, ip, accessToken } = useApplicationContext();

  const doLogout = () => {
    localStorage.clear();
    history.push('/signin');
    window.location.reload();
  };

  const [logout] = useLogoutMutation({
    client: federation,
    onCompleted: doLogout,
    onError: doLogout,
  });

  useEffect(() => {
    logout({
      variables: {
        input: {
          ip,
          accessToken: accessToken as any,
          autoClosed: false,
        },
      },
    });
  }, []);

  return (
    <>
      <Helmet>
        <title>Déconnexion</title>
      </Helmet>
      <MainLoading />
    </>
  );
};

export default Page;
