import React from 'react';
import { ModuleDefinition } from '@lib/common';
import VerifiedUserIcon from '@material-ui/icons/VerifiedUser';
import loadable from '@loadable/component';
import { MainLoading } from '@lib/ui-kit';
import LogoutIcon from './assets/icons/menu/deconnexion.svg';

const LogoutPage = loadable(() => import('./pages/logout'), {
  fallback: <MainLoading />,
});

const authenticationModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_MENU_PRINCIPAL_DECONNEXION',
      location: 'MENU_PRINCIPAL',
      name: 'Déconnexion',
      to: '/logout',
      icon: LogoutIcon,
      preferredOrder: Number.POSITIVE_INFINITY,
    },

    {
      id: 'FEATURE_PARAMETRE_ROLES',
      location: 'PARAMETRE',
      name: 'Rôles',
      to: '/roles"',
      icon: <VerifiedUserIcon />,
      preferredOrder: 270,
      authorize: {
        roles: ['SUPADM', 'GRPADM'],
        rolesDenied: ['PRTSERVICE'],
      },
    },
  ],
  routes: [
    {
      path: '/logout',
      component: LogoutPage,
      exact: false,
      attachTo: 'MAIN',
    },
  ],
};

export default authenticationModule;
