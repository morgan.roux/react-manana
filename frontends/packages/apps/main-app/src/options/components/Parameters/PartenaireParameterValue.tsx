import { useApplicationContext } from '@lib/common';
import { useSearch_Parametre_LaboratoiresLazyQuery } from '@lib/common/src/federation';
import { useSearch_Custom_Content_PartenaireLazyQuery } from '@lib/common/src/graphql';
import React, { FC, useEffect } from 'react';

interface PartenaireParameterValueProps {
  partenaireType: 'PRESTATAIRE_SERVICE' | 'LABORATOIRE';
  idPartenaireTypeAssocie?: string;
}

const PartenaireParameterValue: FC<PartenaireParameterValueProps> = ({ partenaireType, idPartenaireTypeAssocie }) => {
  const { federation, graphql, notify } = useApplicationContext();

  const [searchSelectedLaboratoire, searchSelectedLaboratoiresResult] = useSearch_Parametre_LaboratoiresLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: 'Une erreur est survenue lors du chargement des laboratoires',
        });
      });
    },
  });

  const [searchSelectedPartenaire, searchSelectedPartenaireResult] = useSearch_Custom_Content_PartenaireLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: graphql,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: 'Une erreur est survenue lors du chargement des prestataires de service',
        });
      });
    },
  });

  useEffect(() => {
    if (idPartenaireTypeAssocie) {
      /*if (typeof value === 'object' && value.nom !== searchText) {
        setSearchText(value.nom);
      }*/

      if (partenaireType === 'LABORATOIRE') {
        searchSelectedLaboratoire({
          variables: {
            index: ['laboratoires'],
            query: {
              query: {
                bool: {
                  must: [
                    {
                      term: {
                        _id: idPartenaireTypeAssocie,
                      },
                    },
                  ],
                },
              },
            },
          },
        });
      } else {
        searchSelectedPartenaire({
          variables: {
            type: ['partenaire'],
            query: {
              query: {
                bool: {
                  must: [
                    {
                      term: {
                        _id: idPartenaireTypeAssocie,
                      },
                    },
                  ],
                },
              },
            },
          },
        });
      }
    }
  }, [idPartenaireTypeAssocie]);

  const selectedValue: any =
    partenaireType === 'LABORATOIRE' &&
    idPartenaireTypeAssocie &&
    searchSelectedLaboratoiresResult.data?.search?.data?.length
      ? searchSelectedLaboratoiresResult.data.search.data[0]
      : partenaireType === 'PRESTATAIRE_SERVICE' &&
        idPartenaireTypeAssocie &&
        searchSelectedPartenaireResult.data?.search?.data?.length
      ? searchSelectedPartenaireResult.data.search.data[0]
      : undefined;

  if (!idPartenaireTypeAssocie || !selectedValue) {
    return null;
  }

  return selectedValue.nom;
};

export default PartenaireParameterValue;
