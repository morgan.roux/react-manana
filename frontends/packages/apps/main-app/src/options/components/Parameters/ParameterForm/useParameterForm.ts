import { useState, ChangeEvent, FormEvent, ReactNode } from 'react';
import { ParameterStateInterface } from '../Parameters';

const useParameterForm = (callback: () => void, defaultState?: any) => {
  const [values, setValues] = useState<ParameterStateInterface>(defaultState);

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    callback();
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { name, value } = e.target;
    if (e.preventDefault && e.stopPropagation) {
      e.preventDefault();
      e.stopPropagation();
    }
    setValues((prevState: any) => {
      prevState[name] = value;
      return prevState;
    });
  };

  const handleSwitch = (e: React.ChangeEvent<HTMLInputElement>, checked: boolean) => {
    const { name, value } = e.target;
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    setValues((prevState: any) => {
      prevState[name] = checked;
      return prevState;
    });
  };

  const handleChangeSelect = (e: ChangeEvent<any>, child: ReactNode) => {
    const { name, value } = e.target;
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    if (name) {
      setValues((prevState: any) => ({ ...prevState, [name]: value }));
    }
  };

  return {
    handleChange,
    handleSwitch,
    handleChangeSelect,
    handleSubmit,
    values,
    setValues,
  };
};

export default useParameterForm;
