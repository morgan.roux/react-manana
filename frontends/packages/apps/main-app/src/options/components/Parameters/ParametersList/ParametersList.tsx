import React, { ReactNode, FC } from 'react';
import { List } from '@material-ui/core';
import useStyles from '../style';

interface ParametersListProps {
  children: ReactNode;
}

const ParametersList: FC<ParametersListProps> = ({ children }) => {
  const classes = useStyles({});
  return <List className={classes.parameterList}>{children}</List>;
};

export default ParametersList;
