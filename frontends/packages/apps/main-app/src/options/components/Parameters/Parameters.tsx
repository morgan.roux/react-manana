import React, { useState, ReactNode, FC, useEffect, useCallback } from 'react';
import useStyles from './style';
import EditIcon from '@material-ui/icons/Edit';
import {
  Button,
  InputAdornment,
  IconButton,
  TextField,
  Switch,
  FormControl,
  Select,
  MenuItem,
  Collapse,
} from '@material-ui/core';
import classnames from 'classnames';
import SearchIcon from '@material-ui/icons/Search';
import { ParametersList } from './ParametersList';
import ParameterListItem from './ParametersList/ParameterListItem';
import useParameterForm from './ParameterForm/useParameterForm';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';
import { capitalize, UploadedFile, useApplicationContext, useUploadFiles } from '@lib/common';
import {
  useUpdate_Parameter_ValuesMutation,
  useUpdate_User_PhotoMutation,
  useDelete_User_PhotoMutation,
  ParameterInfoFragment,
  ParamCategory,
  TypeFichier,
} from '@lib/common/src/graphql';
import { MeDocument, MeQuery, MeQueryVariables } from '@lib/common/src/federation';
import { DO_GET_PARAMETER_LOCAL } from '@lib/common/src/shared/operations/parametres/_parametre';

import UserSettingsForm from '../../pages/UserSettings/UserSettingsForm/UserSettingsForm';
import { IDENTIFIER_PREFIX, DIRECTORY, ImageTypes } from '../../pages/UserSettings/UserSettings';
import {
  CODE_LIGNE_TABLEAU,
  CODE_THEME,
  THEMES_LIST,
  CODE_NOTIF_TIMERS,
  CODE_NOTIF_TAGS,
  NOTIF_TAGS,
  NOTIF_TIMERS,
} from './constants';
import { result, find, last } from 'lodash';
import { PartenaireInput } from '@lib/common';
import { Backdrop, ConfirmDeleteDialog, CustomModal, CustomReactEasyCrop, NewCustomButton } from '@lib/ui-kit';
import PartenaireParameterValue from './PartenaireParameterValue';

interface ParameterGroupInterface {
  children: ReactNode;
  title: string;
}

interface ParameterValueInterface {
  id: string;
  value: string;
}

interface ParametersProps {
  withSearchInput?: boolean;
  baseUrl: string;
  where: string;
  isOnOptionsGroupement: boolean;
}

export interface ParameterStateInterface {
  [key: string]: any;
}

export const formatString = (str: string) => {
  return str
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/[^a-zA-Z]/g, '');
};

const sortByCodes = (a: any, b: any) => {
  if (a.code < b.code) return -1;
  if (a.code > b.code) return 1;

  return 0;
};

const Parameters: FC<RouteComponentProps & ParametersProps> = ({
  history,
  location: { pathname },
  withSearchInput,
  baseUrl,
  where,
  isOnOptionsGroupement,
}) => {
  const classes = useStyles({});
  const {
    graphql,
    notify,
    user,
    parameters,
    isSuperAdmin,
    isAdminGroupement,
    isGroupePharmacie,
    config,
    computeUploadedFilename,
  } = useApplicationContext();

  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const [showAvatarform, setShowAvatarform] = useState<boolean>(true);
  const [isEditable, setIsEditable] = useState<boolean>(false);
  const [showOthersParameters, setShowOthersParameters] = useState<boolean>(false);
  const [defaultState, setDefaultState] = useState<ParameterStateInterface>({});
  const [loading, setLoading] = useState<boolean>(false);
  const [confirmDelete, setConfirmDelete] = useState<boolean>(false);
  const [openResize, setOpenResize] = useState<boolean>(false);

  const [avatarChemin, setAvatarChemin] = useState<string>('');

  const [files, setFiles] = useState<(File | UploadedFile)[]>([]);
  const [croppedFiles, setCroppedFiles] = useState<File[]>([]);
  const [imageType, setImageType] = useState<ImageTypes>(ImageTypes.SELECT);
  const [savedCroppedImage, setSavedCroppedImage] = useState<boolean>(false);

  const [selectedAvatar, setSelectedAvatar] = useState<any | undefined>(undefined);

  const [fileAlreadySet, setFileAlreadySet] = useState<string | undefined>(undefined);

  useEffect(() => {
    setIsEditable(pathname.endsWith('/edit'));
  }, [pathname]);

  const disabledEditBtn = (): boolean => {
    if (isOnOptionsGroupement) {
      return !isSuperAdmin;
    }
    return false;
  };

  useEffect(() => {
    if (avatarChemin && avatarChemin !== fileAlreadySet && files.length === 0) {
      setFileAlreadySet(`${config.STORAGE_PUBLIC_BASE_PATH}/${avatarChemin}`);
    }

    if (avatarChemin && imageType !== ImageTypes.IMPORT) {
      setImageType(ImageTypes.IMPORT);
    }
  }, [avatarChemin, fileAlreadySet, files]);

  // const avatarsQuery = useQuery<GROUPEMENT_AVATARS>(GET_GROUPEMENT_AVATARS);
  // const avatars = avatarsQuery.data;
  // const avatarsLoading = avatarsQuery.loading;

  const userImage = files && files.length > 0 && last(files);
  // const userImageSrc = fileAlreadySet || (userImage && URL.createObjectURL(userImage));

  const [userPhotoSrc, setUserPhotoSrc] = useState<any>(null);

  const [previewUrl, setPreviewUrl] = useState<any>('');

  const userImageToS3 = croppedFiles && croppedFiles.length > 0 ? last(croppedFiles) : userImage;

  // Get parameters from Cache
  let parametersList = parameters;

  if (where === 'inUserSettings') {
    parametersList =
      parametersList &&
      parametersList.filter(
        (parameter) => parameter && parameter.category && parameter.category === ParamCategory.User
      );
  }

  if (where === 'inDashboardPharmacie') {
    parametersList =
      parametersList &&
      parametersList.filter(
        (parameter) => parameter && parameter.category && parameter.category === ParamCategory.Pharmacie
      );
  }

  if (where === 'inDashboard') {
    parametersList =
      parametersList &&
      parametersList.filter(
        (parameter) =>
          parameter &&
          parameter.category &&
          (parameter.category === ParamCategory.Groupement || parameter.category === ParamCategory.GroupementSuperadmin)
      );
  }

  let parametersWithGroup =
    isGroupePharmacie || (where !== 'inUserSettings' && (isSuperAdmin || isAdminGroupement))
      ? parametersList && parametersList.filter((parameter) => parameter?.parameterGroupe?.id !== null)
      : parametersList &&
        parametersList.filter(
          (parameter: any) =>
            parameter &&
            parameter.parameterGroupe.id !== null &&
            parameter.parameterGroupe.code &&
            parameter.parameterGroupe.code !== 'MESSAGERIE'
        );

  if (!isSuperAdmin) {
    parametersWithGroup =
      parametersWithGroup &&
      parametersWithGroup.filter(
        (parameter: any) => parameter && parameter.parameterGroupe && parameter.parameterGroupe.code !== 'SSO'
      );
  }

  const parametersWithoutGroup =
    parametersList && parametersList.filter((parameter: any) => parameter && parameter.parameterGroupe.id === null);

  const filteredParameterGroup =
    parametersWithGroup &&
    parametersWithGroup.reduce((prev: Array<any> | null | undefined, current: any) => {
      if (prev && current) {
        const x = prev.find(
          (item) =>
            item &&
            item.parameterGroupe &&
            current.parameterGroupe &&
            item.parameterGroupe.id === current.parameterGroupe.id
        );
        if (!x) {
          return prev.concat([current]);
        } else {
          return prev;
        }
      }
    }, []);

  const [doUpdateParameterValues, doUpdateParameterValuesResult] = useUpdate_Parameter_ValuesMutation({
    client: graphql,
    onCompleted: ({ updateParameterValues }) => {
      if (updateParameterValues && updateParameterValues.length > 0) {
        updateDefaultState(updateParameterValues);
        graphql.writeQuery({
          query: DO_GET_PARAMETER_LOCAL,
          data: { parameters: updateParameterValues },
        });
        setLoading(false);
        redirectAfterSave();
        window.location.reload();
      }
    },
    onError: (errors) => {
      setLoading(false);
      notify({
        type: 'error',
        message: errors.message,
      });
    },
  });

  const [updateUserPhoto, updateUserPhotoResult] = useUpdate_User_PhotoMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.updateUserPhoto) {
        const req = cache.readQuery<MeQuery, MeQueryVariables>({ query: MeDocument });
        if (req && req.me) {
          cache.writeQuery<MeQuery, MeQueryVariables>({
            query: MeDocument,
            data: {
              me: { ...req.me, photo: data.updateUserPhoto.fichier as any },
            },
          });
        }
      }
    },
    onCompleted: (data) => {
      if (data && data.updateUserPhoto && data.updateUserPhoto.fichier && data.updateUserPhoto.fichier.chemin) {
        //getMe();
        setLoading(false);
        if (files && files.length > 0) {
          saveNewParams();
        } else {
          redirectAfterSave();
        }
      }
    },
    onError: (errors) => {
      notify({
        type: 'error',
        message: errors.message,
      });
    },
  });

  useEffect(() => {
    if (user.photo?.chemin) {
      setAvatarChemin(user.photo.chemin);
    }
  }, [user]);

  const redirectAfterSave = () => {
    toggleIsEditable();
    history.push(pathname.replace('/edit', ''));
  };

  const saveNewParams = () => {
    // Save parameters
    const newParameters: ParameterValueInterface[] = [];

    if (parametersList && parametersList.length > 0) {
      parametersList.map((parameter: ParameterInfoFragment) => {
        if (parameter && parameter.name) {
          const pName = formatString(parameter.name);
          let val = values[pName];

          if (val === true || val === false) val = val.toString();
          const newValue = {
            id: parameter.id,
            value: val,
          };

          newParameters.push(newValue);
        }
      });
    }
    // Do mutation
    doUpdateParameterValues({
      variables: { parameters: newParameters },
    });
  };

  const saveUserPhoto = () => {
    // Update userPhoto

    if (userImageToS3 && imageType === ImageTypes.IMPORT) {
      return uploadFiles([userImageToS3], {
        directory: DIRECTORY,
        prefix: IDENTIFIER_PREFIX,
      })
        .then((uploadedFiles) => {
          if (uploadedFiles.length > 0) {
            updateUserPhoto({
              variables: {
                userPhoto: {
                  chemin: uploadedFiles[0].chemin,
                  nomOriginal: uploadedFiles[0].nomOriginal,
                  type: TypeFichier.Photo,
                },
              },
            });
          }
        })
        .catch(() => {
          setLoading(false);
          notify({
            type: 'error',
            message:
              "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
          });
        });
    }

    if (imageType === ImageTypes.SELECT && selectedAvatar && selectedAvatar.fichier && selectedAvatar.fichier.chemin) {
      updateUserPhoto({
        variables: {
          userPhoto: {
            chemin: selectedAvatar.fichier.chemin,
            nomOriginal: 'Avatar Prédéfini',
            type: TypeFichier.Photo,
          },
        },
      });
    }
  };

  const submit = () => {
    setLoading(true);
    if (files && files.length === 0) {
      saveNewParams();
    }
    saveUserPhoto();
  };

  const { values, handleChangeSelect, handleChange, handleSwitch } = useParameterForm(submit, defaultState);

  const toggleIsEditable = () => {
    setIsEditable((prev) => !prev);
  };

  /* const toggleIsEditable = useCallback(() => {
    setIsEditable((prev) => !prev);
  }, []);*/

  const toggleOthersParameters = () => {
    setShowOthersParameters(!showOthersParameters);
  };

  const edit = () => {
    toggleIsEditable();
    setShowAvatarform(!showAvatarform);
    history.push({ pathname: `${baseUrl}/edit` });
  };

  const cancel = () => {
    toggleIsEditable();
    setShowAvatarform(!showAvatarform);
    history.push({ pathname: baseUrl });
  };

  const updateDefaultState = (parameters: Array<ParameterInfoFragment | null> | null | undefined) => {
    if (parameters && parameters.length > 0) {
      parameters.map((param: ParameterInfoFragment | null) => {
        if (param && param.name) {
          const name = formatString(param.name);
          const value = (param.value && param.value.value) || param.defaultValue;
          setDefaultState((prevState: any) => {
            prevState[name] = value;
            return prevState;
          });
        }
      });
    }
  };

  const ParameterGroup: FC<ParameterGroupInterface> = ({ children, title }) => {
    return (
      <div className={classes.parameterGroup}>
        <div className={classes.parameterGroupTitle}>{title}</div>
        <div className={classes.parameterGroupContent}>{children}</div>
      </div>
    );
  };

  const renderParameterValue = (
    parameterValue: any,
    parameterType: string | null,
    parameterCode?: string,
    options?: any
  ) => {
    let value = parameterValue;
    if ('BOOLEAN' === parameterType || typeof value === 'boolean') {
      value = value.toString();
      if (value === 'true') return 'Activé';
      if (value === 'false') return 'Désactivé';
    }

    if (parameterType && parameterType === 'MONEY') value = `${value} £`;

    if ('SELECT' === parameterType && options) {
      const selectedOption = options.find((option: any) => option.value === parameterValue);
      return selectedOption?.label;
    }

    if ('PRESTATAIRE_SERVICE' === parameterType || 'LABORATOIRE' === parameterType) {
      return <PartenaireParameterValue partenaireType={parameterType} idPartenaireTypeAssocie={parameterValue} />;
    }

    if ('TXT' === parameterType || 'INT' === parameterType) {
      return parameterValue;
    }

    switch (parameterCode) {
      case CODE_THEME:
        value = result(find(THEMES_LIST, ['value', value]), 'libelle');
        break;
      case CODE_NOTIF_TAGS:
        value = result(find(NOTIF_TAGS, ['value', value]), 'libelle');
        break;
      case CODE_NOTIF_TIMERS:
        value = result(find(NOTIF_TIMERS, ['value', value]), 'libelle');
        break;
      default:
        break;
    }
    if (options) {
      return getSelectLabel(parameterValue, options);
    }
    return value || '-';
  };

  const nom: string = user?.fullName || 'Non renseigné';
  const anneeNaissance = user?.anneeNaissance || 'Non renseigné';

  const userInfos: any[] = [
    { label: 'Nom', value: nom },
    //  { label: 'Prénom', value: prenom },
    { label: 'Année de naissance', value: anneeNaissance },
    { label: 'Sexe', value: 'Non renseigné' },
    { label: 'Téléphone', value: user.phoneNumber || 'Non renseigné' },
    { label: 'Email', value: user.email || 'Non renseigné' },
  ];

  const [doDeleteUserPhoto] = useDelete_User_PhotoMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.deleteUserPhoto) {
        const req = cache.readQuery<MeQuery, MeQueryVariables>({ query: MeDocument });
        if (req && req.me) {
          cache.writeQuery<MeQuery, MeQueryVariables>({
            query: MeDocument,
            data: {
              me: { ...req.me, photo: null },
            },
          });
        }
      }
    },
    onCompleted: (data) => {
      if (data && data.deleteUserPhoto) {
        // getMe();
        setFileAlreadySet(undefined);
        notify({
          type: 'success',
          message: 'Votre avatar a été supprimeé avec succès',
        });
      }
    },
    onError: (errors) => {
      errors.graphQLErrors.map((error) => {
        if (error.message) {
          notify({
            type: 'success',
            message: error.message,
          });
        }
      });
    },
  });

  const openConfirmDelete = () => {
    setConfirmDelete(true);
  };

  const handleDeleteUserPhoto = () => {
    setConfirmDelete(false);
    if (avatarChemin) {
      doDeleteUserPhoto({ variables: { chemin: avatarChemin } });
    }
  };

  const handleClickResize = useCallback(() => {
    setOpenResize(true);
    // Reset crop
    setSavedCroppedImage(false);
    setPreviewUrl(null);
  }, []);

  useEffect(() => {
    if (files && files.length > 0) {
      setPreviewUrl(null);
      // Open Resize
      handleClickResize();
    }
  }, [files]);

  const saveResizedImage = () => {
    setOpenResize(false);
    setSavedCroppedImage(true);
  };

  useEffect(() => {
    const image = files && files.length > 0 && last(files);
    const photo = previewUrl || (image && URL.createObjectURL(image));
    if (photo) {
      setUserPhotoSrc(photo);
    }
  }, [previewUrl, files]);

  const getSelectLabel = (value: string, options: any) => {
    const option = options.find((optionItem: any) => optionItem.value === value);
    return option && option.label;
  };

  return (
    <>
      <div className={classes.parameterRoot}>
        {/*meLoading && <Backdrop />*/}
        {(loading ||
          doUpdateParameterValuesResult.loading ||
          updateUserPhotoResult.loading ||
          uploadingFiles.loading) && <Backdrop value="Enregistrement en cours..." />}
        <div className={classes.head}>
          <div className={classes.headText}>Configuration de votre compte</div>
          <div className={classes.headerActions}>
            {isEditable ? (
              <div className={classes.headerActionsBtnEdit}>
                <NewCustomButton
                  theme="flatTransparent"
                  onClick={cancel}
                  className={classnames(classes.btn, classes.headerActionsBtnCancel)}
                >
                  ANNULER
                </NewCustomButton>
                <NewCustomButton onClick={submit} className={classnames(classes.btn)}>
                  SAUVEGARDER
                </NewCustomButton>
              </div>
            ) : (
              <div className={classes.headerActionsBtnEdit}>
                <NewCustomButton disabled={disabledEditBtn()} onClick={edit}>
                  <EditIcon /> Modifier
                </NewCustomButton>
              </div>
            )}
          </div>
        </div>
        <div className={classes.containerParameter}>
          <div className={classes.parameterMainContent}>
            {withSearchInput && (
              <div className={classes.searchContainer}>
                <TextField
                  className={classes.textField}
                  variant="outlined"
                  placeholder="Rechercher"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton>
                          <SearchIcon />
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </div>
            )}

            {where === 'inUserSettings' && (
              <div className={classes.parameterContainer}>
                <UserSettingsForm
                  disabledAvatarForm={showAvatarform}
                  setFilesOut={setFiles}
                  withImagePreview={true}
                  withImagePreviewCustomized={true}
                  fileAlreadySetUrl={fileAlreadySet || userPhotoSrc}
                  setFileAlreadySet={setFileAlreadySet}
                  submit={submit}
                  imageType={imageType}
                  setImageType={setImageType}
                  // avatars={avatars && avatars.groupementAvatars ? avatars.groupementAvatars : []}
                  // avatarsLoading={avatarsLoading}
                  avatarSelected={selectedAvatar}
                  setAvatarSelected={setSelectedAvatar}
                  selectedFiles={files}
                  setSelectedFiles={setFiles}
                  onClickDelete={openConfirmDelete}
                  onClickResize={handleClickResize}
                  previewUrl={previewUrl}
                />

                <ParameterGroup title="Mes informations">
                  <ParametersList>
                    {userInfos.map((info) => {
                      const labelId = `checkbox-list-label-${info.label}`;
                      return (
                        <ParameterListItem key={info.label} text={capitalize(info.label)}>
                          <div className={classes.parameterValue}>{info.value}</div>
                        </ParameterListItem>
                      );
                    })}
                  </ParametersList>
                </ParameterGroup>
              </div>
            )}

            <div className={classes.parameterContainer}>
              {filteredParameterGroup &&
                filteredParameterGroup.map((group: any) => {
                  if (group && group.parameterGroupe && group.parameterGroupe.nom) {
                    return (
                      <ParameterGroup key={group.parameterGroupe.id} title={group.parameterGroupe.nom}>
                        <ParametersList>
                          {parametersWithGroup &&
                            [...parametersWithGroup].sort(sortByCodes).map((parameter: any) => {
                              if (
                                parameter &&
                                parameter.parameterGroupe &&
                                group.parameterGroupe &&
                                parameter.parameterGroupe.code === group.parameterGroupe.code
                              ) {
                                const paramName = formatString(parameter.name || '');
                                const valueFromState = values && values[`${formatString(paramName)}`];
                                let paramValue: string | boolean | null | undefined =
                                  valueFromState ||
                                  (parameter.value && parameter.value.value) ||
                                  parameter.defaultValue;
                                if (paramValue === 'true') paramValue = true;
                                if (paramValue === 'false') paramValue = false;
                                return (
                                  <ParameterListItem key={parameter.id} text={parameter.name || ''}>
                                    {isEditable ? (
                                      parameter.code === CODE_NOTIF_TAGS ? (
                                        <FormControl variant="outlined" className={classes.textField}>
                                          <Select
                                            name={paramName}
                                            value={paramValue}
                                            onChange={handleChangeSelect}
                                            classes={{ root: classes.selectParams }}
                                            MenuProps={{
                                              getContentAnchorEl: null,
                                              anchorOrigin: {
                                                vertical: 'bottom',
                                                horizontal: 'left',
                                              },
                                            }}
                                          >
                                            {NOTIF_TAGS.map((item) => (
                                              <MenuItem key={`notif_tags_item_${item.value}`} value={item.value}>
                                                {item.libelle}
                                              </MenuItem>
                                            ))}
                                          </Select>
                                        </FormControl>
                                      ) : parameter.code === CODE_NOTIF_TIMERS ? (
                                        <FormControl variant="outlined" className={classes.textField}>
                                          <Select
                                            name={paramName}
                                            value={paramValue}
                                            onChange={handleChangeSelect}
                                            MenuProps={{
                                              getContentAnchorEl: null,
                                              anchorOrigin: {
                                                vertical: 'bottom',
                                                horizontal: 'left',
                                              },
                                            }}
                                          >
                                            {NOTIF_TIMERS.map((item) => (
                                              <MenuItem key={`notif_timers_item_${item.value}`} value={item.value}>
                                                {item.libelle}
                                              </MenuItem>
                                            ))}
                                          </Select>
                                        </FormControl>
                                      ) : parameter.code === CODE_LIGNE_TABLEAU ? (
                                        <FormControl variant="outlined" className={classes.textField}>
                                          <Select
                                            name={paramName}
                                            value={paramValue}
                                            onChange={handleChangeSelect}
                                            classes={{ root: classes.selectParams }}
                                            MenuProps={{
                                              getContentAnchorEl: null,
                                              anchorOrigin: {
                                                vertical: 'bottom',
                                                horizontal: 'left',
                                              },
                                            }}
                                          >
                                            <MenuItem value="12">12</MenuItem>
                                            <MenuItem value="25">25</MenuItem>
                                            <MenuItem value="50">50</MenuItem>
                                            <MenuItem value="100">100</MenuItem>
                                            <MenuItem value="250">250</MenuItem>
                                            <MenuItem value="500">500</MenuItem>
                                            <MenuItem value="1000">1000</MenuItem>
                                            <MenuItem value="2000">2000</MenuItem>
                                          </Select>
                                        </FormControl>
                                      ) : parameter.code === CODE_THEME ? (
                                        <FormControl variant="outlined" className={classes.textField}>
                                          <Select
                                            name={paramName}
                                            value={paramValue}
                                            onChange={handleChangeSelect}
                                            classes={{ root: classes.selectParams }}
                                            MenuProps={{
                                              getContentAnchorEl: null,
                                              anchorOrigin: {
                                                vertical: 'bottom',
                                                horizontal: 'left',
                                              },
                                            }}
                                          >
                                            {THEMES_LIST.map((item) => (
                                              <MenuItem key={`theme_item_${item.value}`} value={item.value}>
                                                {item.libelle}
                                              </MenuItem>
                                            ))}
                                          </Select>
                                        </FormControl>
                                      ) : parameter.parameterType && parameter.parameterType.code === 'BOOLEAN' ? (
                                        <Switch
                                          className={classes.switch}
                                          onChange={handleSwitch}
                                          name={paramName}
                                          defaultChecked={typeof paramValue === 'boolean' ? paramValue : undefined}
                                          // checked={typeof paramValue === 'boolean' ? paramValue : undefined}
                                        />
                                      ) : parameter.parameterType.code === 'TIME' ? (
                                        <TextField
                                          variant="outlined"
                                          placeholder=""
                                          type="time"
                                          name={paramName}
                                          defaultValue={paramValue}
                                          onChange={handleChange}
                                          className={classes.textField}
                                          InputLabelProps={{
                                            shrink: true,
                                          }}
                                          inputProps={{
                                            step: 300, // 5 min
                                          }}
                                        />
                                      ) : parameter.parameterType.code === 'SELECT' ? (
                                        <FormControl variant="outlined" className={classes.textField}>
                                          <Select
                                            name={paramName}
                                            value={paramValue}
                                            onChange={handleChangeSelect}
                                            classes={{ root: classes.selectParams }}
                                            MenuProps={{
                                              getContentAnchorEl: null,
                                              anchorOrigin: {
                                                vertical: 'bottom',
                                                horizontal: 'left',
                                              },
                                            }}
                                          >
                                            {parameter &&
                                              parameter.options &&
                                              parameter.options.map((option: any) => (
                                                <MenuItem key={`option-select-${option.value}`} value={option.value}>
                                                  {option.label}
                                                </MenuItem>
                                              ))}
                                          </Select>
                                        </FormControl>
                                      ) : parameter.parameterType.code === 'PRESTATAIRE_SERVICE' ||
                                        parameter.parameterType.code === 'LABORATOIRE' ? (
                                        <FormControl variant="outlined" className={classes.textField}>
                                          <PartenaireInput
                                            index={
                                              parameter.parameterType.code === 'PRESTATAIRE_SERVICE'
                                                ? 'partenaire'
                                                : 'laboratoire'
                                            }
                                            defaultValue={paramValue}
                                            onChange={(newPartenaire) =>
                                              handleChange({
                                                target: {
                                                  name: paramName,
                                                  value: newPartenaire?.id,
                                                },
                                              } as any)
                                            }
                                          />
                                        </FormControl>
                                      ) : (
                                        <TextField
                                          variant="outlined"
                                          placeholder=""
                                          name={paramName}
                                          defaultValue={paramValue}
                                          onChange={handleChange}
                                          className={classes.textField}
                                        />
                                      )
                                    ) : (
                                      <div className={classes.parameterValue}>
                                        {renderParameterValue(
                                          paramValue,
                                          parameter?.parameterType?.code,
                                          parameter.code,
                                          parameter.options
                                        )}
                                      </div>
                                    )}
                                  </ParameterListItem>
                                );
                              }
                            })}
                        </ParametersList>
                      </ParameterGroup>
                    );
                  }
                })}

              {parametersWithoutGroup && parametersWithoutGroup.length > 0 && (
                <div className={classes.othersParametersContainer}>
                  <Button className={classes.othersParametersBtn} onClick={toggleOthersParameters}>
                    Autres paramètres
                    {showOthersParameters || !(filteredParameterGroup && filteredParameterGroup.length) ? (
                      <ArrowDropUpIcon />
                    ) : (
                      <ArrowDropDownIcon />
                    )}
                  </Button>
                  <Collapse
                    in={showOthersParameters || !(filteredParameterGroup && filteredParameterGroup.length)}
                    timeout="auto"
                    unmountOnExit={true}
                  >
                    <div>
                      <ParametersList>
                        {parametersWithoutGroup &&
                          parametersWithoutGroup.map((parameter: any) => {
                            if (parameter) {
                              const paramName = formatString(parameter.name || '');
                              const valueFromState = values && values[`${formatString(paramName)}`];
                              let paramValue: string | boolean | null | undefined =
                                valueFromState || (parameter.value && parameter.value.value) || parameter.defaultValue;
                              if (paramValue === 'true') paramValue = true;
                              if (paramValue === 'false') paramValue = false;
                              return (
                                <ParameterListItem key={parameter.id} text={capitalize(parameter.name || '')}>
                                  {isEditable ? (
                                    parameter.parameterType && parameter.parameterType.code === 'BOOLEAN' ? (
                                      <Switch
                                        className={classes.switch}
                                        onChange={handleSwitch}
                                        name={paramName}
                                        defaultChecked={typeof paramValue === 'boolean' ? paramValue : undefined}
                                        // checked={typeof paramValue === 'boolean' ? paramValue : undefined}
                                      />
                                    ) : parameter.parameterType.code === 'TIME' ? (
                                      <TextField
                                        variant="outlined"
                                        placeholder=""
                                        type="time"
                                        name={paramName}
                                        defaultValue={paramValue}
                                        onChange={handleChange}
                                        className={classes.textField}
                                        InputLabelProps={{
                                          shrink: true,
                                        }}
                                        inputProps={{
                                          step: 300, // 5 min
                                        }}
                                      />
                                    ) : (
                                      <TextField
                                        variant="outlined"
                                        placeholder=""
                                        name={paramName}
                                        defaultValue={paramValue}
                                        onChange={handleChange}
                                        className={classes.textField}
                                      />
                                    )
                                  ) : (
                                    <div className={classes.parameterValue}>
                                      {renderParameterValue(
                                        paramValue,
                                        parameter && parameter.parameterType && parameter.parameterType.code
                                      )}
                                    </div>
                                  )}
                                </ParameterListItem>
                              );
                            }
                          })}
                      </ParametersList>
                    </div>
                  </Collapse>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>

      <ConfirmDeleteDialog
        open={confirmDelete}
        setOpen={setConfirmDelete}
        title="Suppression avatar"
        content="Vous êtes sûr de vouloir supprimer votre avatar ?"
        onClickConfirm={handleDeleteUserPhoto}
      />
      <CustomModal
        open={openResize}
        setOpen={setOpenResize}
        title="Redimensionnement"
        onClickConfirm={saveResizedImage}
        actionButton="Enregistrer"
        closeIcon={true}
        centerBtns={true}
      >
        {userPhotoSrc ? (
          <CustomReactEasyCrop
            src={userPhotoSrc}
            withZoom={true}
            withRotate={true}
            croppedImage={previewUrl}
            setCroppedImage={setPreviewUrl}
            setCropedFiles={setCroppedFiles}
            savedCroppedImage={savedCroppedImage}
          />
        ) : (
          <div>Aucune Image</div>
        )}
      </CustomModal>
    </>
  );

  return null;
};

Parameters.defaultProps = {
  withSearchInput: true,
  baseUrl: '/db/options-groupement',
  where: 'inDashboard',
};

export default withRouter(Parameters);
