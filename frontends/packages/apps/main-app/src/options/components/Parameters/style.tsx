import { Theme, createStyles, makeStyles, lighten } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    parameterRoot: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      /*marginBottom: 50,*/
    },
    head: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: 70,
      background: lighten(theme.palette.primary.main, 0.1),
      opacity: 1,
      color: '#FFFFFF',
      padding: '0px 30px',
      position: 'sticky',
      top: 0,
      zIndex: 1,
    },
    headText: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 20,
    },
    headerActions: {},
    headerActionsBtnEdit: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      cursor: 'pointer',
      '& svg': { marginRight: 5 },
      '& .MuiButton-containedSecondary': {
        color: '#fff',
        backgroundColor: theme.palette.primary.main,
      },
    },
    headerActionsBtnCancel: {
      marginRight: 15,
    },
    btn: {
      opacity: 1,
      letterSpacing: 0,
      textTransform: 'uppercase',
      fontWeight: 'bold',
      borderRadius: 3,
    },
    containerParameter: {
      width: '100%',
      height: 'calc(100vh - 156px)',
      overflow: 'auto',
      display: 'flex',
      justifyContent: 'center',
    },
    parameterMainContent: {
      padding: 50,
      maxWidth: 950,
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    searchContainer: {
      maxWidth: 415,
      marginBottom: 50,
    },
    parameterContainer: {
      width: '100%',
    },
    parameterGroup: {
      marginBottom: 25,
      display: 'flex',
      flexDirection: 'column',
    },
    parameterGroupTitle: {
      textAlign: 'left',
      fontWeight: 'bold',
      fontSize: 18,
      fontFamily: 'Montserrat',
      letterSpacing: 0,
      color: theme.palette.secondary.main,
      opacity: 1,
      marginBottom: 15,
    },
    parameterGroupContent: {
      border: '1px solid #cfcdcd',
      borderRadius: 10,
    },
    parameterList: {
      paddingTop: 0,
      paddingBottom: 0,
      '& li': {
        borderBottom: '1px solid #cfcdcd',
        padding: 10,
      },
      '& li:nth-last-child(1)': {
        borderBottom: 0,
      },
    },
    parameterListItem: {
      '& .MuiListItemText-root .MuiTypography-body1': {
        color: '#878787',
        letterSpacing: 0,
        fontSize: 14,
        fontWeight: 500,
        opacity: 1,
        fontFamily: 'Montserrat',
      },
      '& main-MuiSelect-root, & .MuiSelect-root': {
        padding: 14.5,
      },
    },
    textField: {
      minWidth: 246,
      '& .MuiOutlinedInput-root': {
        '& fieldset': {
          borderColor: '#000000',
        },
        '&:hover fieldset': {
          borderColor: '#000000',
        },
        '&.Mui-focused fieldset': {
          borderColor: '#000000',
        },
      },
      '& main-MuiSelect-root, & .MuiSelect-root': {
        padding: 14.5,
      },
      '& input': {
        padding: '14.5px',
      },
    },
    switch: {
      '& .MuiSwitch-colorSecondary.Mui-checked': {
        // color: '#004354',
      },
      '& .MuiSwitch-colorSecondary.Mui-checked + .MuiSwitch-track': {
        // backgroundColor: '#004354',
      },
    },
    parameterValue: {
      fontWeight: 600,
      opacity: 1,
      fontFamily: 'Montserrat',
      letterSpacing: 0,
      fontSize: 16,
      color: '#1D1D1D',
      maxWidth: '60%',
      textAlign: 'justify',
    },
    othersParametersContainer: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      '& > div': {
        width: '100%',
      },
    },
    othersParametersBtn: {
      textTransform: 'none',
      width: 'fit-content',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      cursor: 'pointer',
      color: '#1D1D1D',
      fontFamily: 'Montserrat',
      fontSize: 14,
      letterSpacing: 0,
      '& div:nth-child(1)': {
        marginRight: 20,
      },
    },
    userInfo: {
      '& > ul > li': {
        padding: '4px 0px',
        '& > div:nth-child(2)': {
          textAlign: 'right',
        },
      },
    },
    userInfoTitle: {
      '& > span': {
        fontWeight: 'bold',
      },
    },
    reactCrop: {
      '& .ReactCrop': {
        width: '590px !important',
        height: '660px !important ',
        minWidth: '590px !important',
        minhheight: '660px !important ',
      },
      '& .ReactCrop__crop-selection': {
        border: '0px !important',
      },
    },
    selectParams: {
      padding: 14.5,
    },
  })
);

export default useStyles;
