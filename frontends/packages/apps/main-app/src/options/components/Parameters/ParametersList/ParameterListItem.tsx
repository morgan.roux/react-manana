import React, { ReactNode, FC } from 'react';
import { ListItem, ListItemText } from '@material-ui/core';
import useStyles from '../style';

interface ParameterListItemProps {
  text: string;
  children: ReactNode;
}

const ParameterListItem: FC<ParameterListItemProps> = ({ text, children }) => {
  const classes = useStyles({});
  return (
    <ListItem className={classes.parameterListItem}>
      <ListItemText primary={text} />
      {children}
    </ListItem>
  );
};

export default ParameterListItem;
