import { makeStyles, createStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    userSettingsRoot: {
      marginBottom: 60,
      display: 'flex',
      flexDirection: 'column',
    },
    centerContent: {
      alignItems: 'center',
    },
    sectionTitle: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    dropzone: {
      width: '100%',
      '& .MuiListItemSecondaryAction-root, & .MuiListItemText-root': {
        display: 'none',
      },
    },
    imgFormContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
      '& > div > div': {
        height: 300,
        border: '1px dashed #DCDCDC',
        alignItems: 'flex-start',
        '& img': {
          width: 225,
          height: 225,
          borderRadius: '50%',
        },
      },
    },
    imageTypeChoice: {
      marginBottom: 16,
    },
    imageTypeOptionSize: {
      minHeight: 100,
      minWidth: 100,
    },
    avatarTitle: {
      fontWeight: 'bold',
      fontSize: 18,
      fontFamily: 'Montserrat',
      letterSpacing: 0,
      color: theme.palette.secondary.main,
      opacity: 1,
      marginBottom: 15,
    },
    avatarTitleCentered: {
      textAlign: 'center',
    },
    avatarsContainer: {
      border: '1px solid #DCDCDC',
      borderRadius: 12,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'column',
      // padding: '0px 30px',
    },
    avatar: {
      border: '1px solid #DCDCDC',
      borderRadius: '50%',
      width: 212,
      height: 212,
    },
    avatarItem: {
      width: '50px !important',
      height: '50px !important',
      margin: '0px !important',
      '& img': {
        width: '50px !important',
        height: '50px !important',
        margin: '0px !important',
      },
    },
    signleAvatarContainer: {
      padding: '10px 20px',
    },
    selectAvatarContainer: {
      width: 'auto !important',
      padding: '0px !important',
      display: 'flex !important',
      flexWrap: 'wrap',
      maxWidth: '85%',
    },
    btnSave: {},
    avatarListSubTitle: {
      textAlign: 'center',
      fontSize: 16,
      letterSpacing: 0,
      fontFamily: 'Montserrat',
      fontWeight: 'normal',
      marginTop: 15,
      marginBottom: 25,
    },
  })
);
