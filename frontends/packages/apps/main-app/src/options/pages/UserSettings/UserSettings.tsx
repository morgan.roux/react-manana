import React, { FC } from 'react';
import moment from 'moment';
import Parameters from '../../components/Parameters/Parameters';

// Image identifier prefix (for preventing filename conflicts)
export const IDENTIFIER_PREFIX = moment().format('YYYYMMDDhmmss');
export const DIRECTORY = 'avatars';

export enum ImageTypes {
  SELECT,
  IMPORT,
}

const UserSettings: FC = () => {
  // const classes = useStyles({});

  return (
    <Parameters
      isOnOptionsGroupement={false}
      withSearchInput={false}
      baseUrl="/db/user-settings"
      where="inUserSettings"
    />
  );
};

export default UserSettings;
