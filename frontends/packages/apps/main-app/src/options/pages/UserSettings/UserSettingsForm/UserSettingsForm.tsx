import { Avatar as MUIAvatar, FormControl, FormControlLabel, Radio, RadioGroup, Typography } from '@material-ui/core';
import CameraIcon from '@material-ui/icons/CameraAlt';
import classnames from 'classnames';
import { last } from 'lodash';
import React, { FC } from 'react';
import ChoiceAvatarForm from '../../../components/ChoiceAvatarForm';
import { DropzoneProps, Dropzone } from '@lib/common';
import { ImageTypes } from '../UserSettings';
import { useStyles } from './styles';

interface UserSettingsFormProps {
  disabledAvatarForm: boolean;
  loading?: boolean;
  submit: () => void;
}

interface FormAvatarProps {
  imageType: ImageTypes;
  setImageType: React.Dispatch<React.SetStateAction<ImageTypes>>;
  avatarSelected: any | undefined;
  setAvatarSelected: React.Dispatch<React.SetStateAction<any | undefined>>;
  previewUrl?: string;
}

const UserSettingsForm: FC<DropzoneProps & UserSettingsFormProps & FormAvatarProps> = ({
  setFilesOut,
  setFileAlreadySet,
  fileAlreadySetUrl,
  withImagePreview,
  withImagePreviewCustomized,
  imageType,
  setImageType,
  avatarSelected,
  setAvatarSelected,
  selectedFiles,
  setSelectedFiles,
  onClickDelete,
  onClickResize,
  previewUrl,
  disabledAvatarForm,
}) => {
  const classes = useStyles({});
  // const disabledAvatarForm: boolean = pathname === '/db/user-settings' ? true : false;

  const file = selectedFiles && selectedFiles.length > 0 && last(selectedFiles);

  const userAvatarUrl = fileAlreadySetUrl || (file && URL.createObjectURL(file));

  const onClickAvatar = (avatar: any) => {
    console.log('avatar :>> ', avatar);
  };

  return (
    <div
      className={
        disabledAvatarForm ? classnames(classes.userSettingsRoot, classes.centerContent) : classes.userSettingsRoot
      }
    >
      <Typography
        className={
          disabledAvatarForm ? classnames(classes.avatarTitle, classes.avatarTitleCentered) : classes.avatarTitle
        }
      >
        Votre avatar
      </Typography>
      {disabledAvatarForm && userAvatarUrl && <img className={classes.avatar} src={userAvatarUrl} alt="User Avatar" />}

      {disabledAvatarForm && !userAvatarUrl && (
        <MUIAvatar className={classes.avatar}>
          <CameraIcon />
        </MUIAvatar>
      )}

      {!disabledAvatarForm && (
        <div className={classes.dropzone}>
          <FormControl component="fieldset" className={classes.imageTypeChoice}>
            <RadioGroup value={imageType} onChange={(e) => setImageType(parseInt(e.target.value))} row={true}>
              <FormControlLabel
                disabled={disabledAvatarForm}
                value={ImageTypes.SELECT}
                control={<Radio />}
                label="Sélection par défaut"
              />
              <FormControlLabel
                disabled={disabledAvatarForm}
                value={ImageTypes.IMPORT}
                control={<Radio />}
                label="Télécharger la photo"
              />
            </RadioGroup>
          </FormControl>

          <div className={classes.imgFormContainer}>
            {imageType === ImageTypes.IMPORT ? (
              <Dropzone
                where="inUserSettings"
                customLabel="Glissez et déposez ici votre photo"
                withImagePreview={withImagePreview}
                withImagePreviewCustomized={withImagePreviewCustomized}
                setFilesOut={setFilesOut}
                fileAlreadySetUrl={fileAlreadySetUrl}
                setFileAlreadySet={setFileAlreadySet}
                disabled={disabledAvatarForm}
                multiple={false}
                acceptFiles="image/*"
                selectedFiles={selectedFiles}
                setSelectedFiles={setSelectedFiles}
                onClickDelete={onClickDelete}
                onClickResize={onClickResize}
                previewUrl={previewUrl}
                withFileIcon={false}
              />
            ) : (
              <ChoiceAvatarForm
                selectedAvatar={avatarSelected}
                setSelectedAvatar={setAvatarSelected}
                onClickAvatar={onClickAvatar}
              />
            )}
          </div>
        </div>
      )}
    </div>
  );
};

export default UserSettingsForm;
