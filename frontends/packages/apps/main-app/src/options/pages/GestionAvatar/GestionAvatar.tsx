import { useApplicationContext, useUploadFiles } from '@lib/common';
import { TypeFichier, useAvatarLazyQuery, useCreate_Update_AvatarMutation } from '@lib/common/src/graphql';
import { Backdrop } from '@lib/ui-kit';
import { last } from 'lodash';
import React, { FC, useEffect, useMemo, useState } from 'react';
import { useHistory, useLocation, useParams } from 'react-router-dom';
import AvatarForm from './AvatarForm';
import useAvatarForm from './AvatarForm/useAvatarForm';
import AvatarHead from './AvatarHead';
import AvatarList from './AvatarList';
import useStyles from './styles';

const GestionAvatar: FC = () => {
  const { graphql, notify } = useApplicationContext();
  const [uploadFiles, uploadingFiles] = useUploadFiles();

  const { pathname } = useLocation();
  const { push } = useHistory();
  const classes = useStyles({});

  const { avatarId }: any = useParams();
  const EDIT_URL = `/db/avatar/edit/${avatarId}`;

  const [selected, setSelected] = useState<any[]>([]);
  const [selectedFiles, setselectedFiles] = useState<File[]>([]);
  const [croppedFiles, setCroppedFiles] = useState<File[]>([]);
  const [imageUrl, setImageUrl] = useState<string | null | undefined>(undefined);
  const [deleteRow, setDeleteRow] = useState<any>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const [loading, setLoading] = useState<boolean>(false);

  const { handleChange, values, setValues } = useAvatarForm();

  const { id, nom, sexe, image } = values;

  const isOnList: boolean = pathname === '/db/avatar';
  const isOnCreate: boolean = pathname === '/db/avatar/create';
  const isOnEdit: boolean = pathname.startsWith('/db/avatar/edit');

  const avatarImage = selectedFiles && selectedFiles.length > 0 && last(selectedFiles);
  const avatarImageSrc = imageUrl || (avatarImage && URL.createObjectURL(avatarImage));

  // Img send to S3
  const avatarImageToS3 = croppedFiles && croppedFiles.length > 0 ? last(croppedFiles) : avatarImage;

  let loadingMsg = 'en cours...';
  loadingMsg = isOnCreate
    ? `Création ${loadingMsg}`
    : isOnEdit
    ? `Modification ${loadingMsg}`
    : `Chargement ${loadingMsg}`;

  const IMAGE_EXIST: boolean = image && image.chemin && image.nomOriginal ? true : false;

  const formIsValid = (): boolean => {
    if (nom && sexe && (selectedFiles.length > 0 || IMAGE_EXIST)) {
      return true;
    }
    return false;
  };

  // console.log('values ::::>> ', values);

  const onClickSaveAvatar = () => {
    if (formIsValid()) {
      setLoading(true);
      if (selectedFiles.length > 0) {
        return uploadFiles(selectedFiles, {
          directory: 'avatars',
        })
          .then((uploadedFiles) => {
            if (uploadedFiles.length > 0) {
              createUpdateAvatar({
                variables: {
                  id,
                  description: nom,
                  codeSexe: sexe as any,
                  fichier: {
                    chemin: uploadedFiles[0].chemin,
                    nomOriginal: uploadedFiles[0].nomOriginal,
                    type: TypeFichier.Avatar,
                    idAvatar: image && image.idAvatar,
                  },
                },
              });
            }
          })
          .catch(() => {
            setLoading(false);
            notify({
              type: 'error',
              message:
                "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
            });
          });
      }

      // EDIT WITHOUT NEW IMAGE
      if (isOnEdit && formIsValid() && IMAGE_EXIST && selectedFiles.length === 0) {
        // Execute createUpdateAvatar
        createUpdateAvatar({
          variables: {
            id,
            description: nom,
            codeSexe: sexe as any,
            fichier: {
              chemin: image && (image.chemin as any),
              nomOriginal: image && (image.nomOriginal as any),
              type: TypeFichier.Avatar,
              idAvatar: image && image.idAvatar,
            },
          },
        });
      }
    }
  };

  /**
   * Mutation createUpdateAvatar
   */
  const [createUpdateAvatar, { loading: mutationLoading }] = useCreate_Update_AvatarMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.createUpdateAvatar) {
        notify({
          type: 'success',
          message: `Avatar ${isOnCreate ? 'créé' : 'modifié'} avec succès`,
        });
        push('/db/avatar');
      }
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
      notify({ type: 'error', message: errors.message });
      setLoading(false);
    },
  });

  // EDIT
  /**
   * Get avatar for edit
   */
  const [getAvatar, { loading: avatarLoading }] = useAvatarLazyQuery({
    client: graphql,
    // fetchPolicy: 'network-only',
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        notify({
          type: 'error',
          message: err.message,
        });
      });
      setLoading(false);
    },
    onCompleted: (data) => {
      if (data && data.avatar) {
        const avatar = data.avatar;
        // Set values for edit
        setValues({
          id: avatar.id,
          nom: avatar.description as any,
          sexe: avatar.codeSexe as any,
          image: {
            chemin: avatar.fichier && avatar.fichier && (avatar.fichier.chemin as any),
            nomOriginal: avatar.fichier && avatar.fichier && (avatar.fichier.nomOriginal as any),
            type: TypeFichier.Avatar,
            idAvatar: image && image.idAvatar,
          },
        });
      }
    },
  });

  /**
   * Get publicite for edit
   */
  useEffect(() => {
    if (pathname === EDIT_URL && avatarId) {
      // Get avatar
      getAvatar({ variables: { id: avatarId } });
    }
  }, [pathname, avatarId]);

  /**
   * Set deleteRow to null
   */
  useMemo(() => {
    if (deleteRow) {
      setDeleteRow(null);
    }
  }, [selected]);

  const isLoading = (): boolean => {
    return loading || uploadingFiles.loading || mutationLoading || avatarLoading;
  };

  return (
    <div className={classes.avatarRoot}>
      {isLoading() && <Backdrop value={avatarLoading ? 'Chargement en cours...' : loadingMsg} />}
      <AvatarHead
        isOnList={isOnList}
        isOnCreate={isOnCreate}
        isOnEdit={isOnEdit}
        onClickSaveAvatar={onClickSaveAvatar}
        selected={selected}
        setSelected={setSelected}
        deleteRow={deleteRow}
        setDeleteRow={setDeleteRow}
        openDeleteDialog={openDeleteDialog}
        setOpenDeleteDialog={setOpenDeleteDialog}
      />
      {isOnList && (
        <AvatarList
          selected={selected}
          setSelected={setSelected}
          deleteRow={deleteRow}
          setDeleteRow={setDeleteRow}
          openDeleteDialog={openDeleteDialog}
          setOpenDeleteDialog={setOpenDeleteDialog}
        />
      )}
      {!isOnList && (
        <AvatarForm
          selectedFiles={selectedFiles}
          setSelectedFiles={setselectedFiles}
          handleChangeInput={handleChange}
          values={values}
          imageUrl={imageUrl}
          setImageUrl={setImageUrl}
          croppedFiles={croppedFiles}
          setCroppedFiles={setCroppedFiles}
          avatarImageSrc={avatarImageSrc as any}
        />
      )}
    </div>
  );
};

export default GestionAvatar;
