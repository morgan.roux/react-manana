import React, { FC, useCallback, Dispatch, SetStateAction } from 'react';
import useStyles from './styles';
import { Typography, IconButton } from '@material-ui/core';
import { Add, ArrowBack } from '@material-ui/icons';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { CustomButton } from '@lib/ui-kit';

interface AvatarHead {
  isOnList: boolean;
  isOnCreate: boolean;
  isOnEdit: boolean;
  selected: any[];
  deleteRow: any;
  openDeleteDialog: boolean;
  setSelected: Dispatch<SetStateAction<any[]>>;
  setDeleteRow: Dispatch<SetStateAction<any>>;
  setOpenDeleteDialog: Dispatch<SetStateAction<boolean>>;
  onClickSaveAvatar: () => void;
}

const AvatarHead: FC<AvatarHead & RouteComponentProps> = ({
  onClickSaveAvatar,
  isOnList,
  isOnCreate,
  // isOnEdit,
  selected,
  // setSelected,
  // deleteRow,
  // setDeleteRow,
  // openDeleteDialog,
  setOpenDeleteDialog,
  history: { push },
}) => {
  const classes = useStyles({});

  const title = isOnList ? 'Liste des avatars' : isOnCreate ? "Ajout d'un avatar" : "Modification d'un avatar";
  const saveBtnText = isOnCreate ? 'Ajouter' : 'Modifier';

  const goToBack = () => push('/db/avatar');

  const goToAddAvatar = () => push('/db/avatar/create');

  const onClickDeleteSelected = useCallback(() => setOpenDeleteDialog(true), []);

  const onClickCancel = () => {
    push('/db/avatar');
  };

  return (
    <div
      className={isOnList ? classes.avatarHeadRoot : classnames(classes.avatarHeadRoot, classes.avatarHeadRootWithBg)}
    >
      {!isOnList && (
        <IconButton onClick={goToBack} color="inherit">
          <ArrowBack />
        </IconButton>
      )}
      <Typography className={classes.avatarHeadTitle}>{title}</Typography>

      <div className={classes.avatarHeadBtnsContainer}>
        {isOnList && (
          <>
            {selected.length > 0 && (
              <CustomButton color="default" onClick={onClickDeleteSelected}>
                Supprimer la sélection
              </CustomButton>
            )}
            <CustomButton color="secondary" startIcon={<Add />} onClick={goToAddAvatar}>
              Ajouter un avatar
            </CustomButton>
          </>
        )}
        {!isOnList && (
          <>
            <CustomButton color="default" onClick={onClickCancel}>
              Annuler
            </CustomButton>
            <CustomButton color="secondary" onClick={onClickSaveAvatar}>
              {saveBtnText}
            </CustomButton>
          </>
        )}
      </div>
    </div>
  );
};

export default withRouter(AvatarHead);
