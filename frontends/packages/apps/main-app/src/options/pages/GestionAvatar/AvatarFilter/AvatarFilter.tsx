import React, { Dispatch, SetStateAction, ChangeEvent, FC } from 'react';
import { Typography } from '@material-ui/core';
import useStyles from './styles';
import { AvataFilterInterface } from './useAvatarFilterForm';
import { CustomButton, CustomFormTextField, CustomSelect } from '@lib/ui-kit';

interface AvatarFilterProps {
  state: AvataFilterInterface;
  setState: Dispatch<SetStateAction<AvataFilterInterface>>;
  handleChangeInput: (e: ChangeEvent<any>) => void;
  onClickApplyBtn: () => void;
  onClickInitBtn: () => void;
}

const AvatarFilter: FC<AvatarFilterProps> = ({ state, handleChangeInput, onClickApplyBtn, onClickInitBtn }) => {
  const classes = useStyles({});
  const { description, creator, sexe } = state;

  const SEXE_LIST = [
    { id: 'ALL', value: 'Tous les sexes' },
    { id: 'M', value: 'Homme' },
    { id: 'F', value: 'Femme' },
  ];

  return (
    <div className={classes.avatarFilterRoot}>
      <Typography className={classes.avatarFilterTitle}>Saisissez les filtres qui vous conviennent</Typography>
      <div className={classes.avatarFilterFormContainer}>
        <CustomFormTextField
          label="Désignation"
          name="description"
          value={description}
          onChange={handleChangeInput}
          placeholder="Tapez un mot"
        />
        <CustomFormTextField
          label="Créateur"
          name="creator"
          value={creator}
          onChange={handleChangeInput}
          placeholder="Tapez un mot"
        />
        <CustomSelect
          label="Sexe"
          list={SEXE_LIST}
          listId="id"
          index="value"
          name="sexe"
          value={sexe}
          onChange={handleChangeInput}
          placeholder="Sexe de l'avatar"
          withPlaceholder={true}
        />
        <div className={classes.avatarFilterBtnsContainer}>
          <CustomButton color="default" onClick={onClickInitBtn}>
            Réinitialiser
          </CustomButton>
          <CustomButton color="secondary" onClick={onClickApplyBtn}>
            Appliquer
          </CustomButton>
        </div>
      </div>
    </div>
  );
};

export default AvatarFilter;
