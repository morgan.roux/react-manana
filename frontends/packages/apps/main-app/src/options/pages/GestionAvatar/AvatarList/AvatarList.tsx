import { isInvalidArray, useApplicationContext } from '@lib/common';
import CustomContent from '@lib/common/src/components/newCustomContent';
import { Column } from '@lib/common/src/components/newCustomContent/interfaces';
import { BuildQuery } from '@lib/common/src/components/withSearch/queryBuilder';
import { Sexe, useDelete_AvatarsMutation, useSearch_AvatarsQuery } from '@lib/common/src/graphql';
import { Backdrop, ConfirmDeleteDialog, CustomModal } from '@lib/ui-kit';
import { Fade, IconButton, Tooltip } from '@material-ui/core';
import { Delete, Edit } from '@material-ui/icons';
import { startCase } from 'lodash';
import moment from 'moment';
import React, { Dispatch, FC, MouseEvent, SetStateAction, useCallback, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import AvatarFilter from '../AvatarFilter';
import useAvatarFilterForm from '../AvatarFilter/useAvatarFilterForm';
import useStyles from './styles';

interface AvatarListProps {
  selected: any[];
  deleteRow: any;
  openDeleteDialog: boolean;
  setSelected: Dispatch<SetStateAction<any[]>>;
  setDeleteRow: Dispatch<SetStateAction<any>>;
  setOpenDeleteDialog: Dispatch<SetStateAction<boolean>>;
}

const AvatarList: FC<AvatarListProps> = ({
  selected,
  setSelected,
  setDeleteRow,
  deleteRow,
  openDeleteDialog,
  setOpenDeleteDialog,
}) => {
  const classes = useStyles({});
  const { push } = useHistory();
  const { currentGroupement: groupement, graphql, notify } = useApplicationContext();
  const type = 'avatar';

  const [filterBy, setFilterBy] = useState<any[]>([]);
  const [openFilterModal, setOpenFilterModal] = useState<boolean>(false);

  const { values, setValues, handleChange } = useAvatarFilterForm();
  const { description, creator, sexe } = values;

  const must = [{ term: { idGroupement: groupement.id } }, { term: { sortie: 0 } }];

  const { query, skip, take } = BuildQuery({ type, optionalMust: must });

  const variables = { take, skip, type: [type], query };

  const listResult = useSearch_AvatarsQuery({ client: graphql, variables });

  const ACTIONS_COLUMNS: Column[] = [
    {
      name: '',
      label: '',
      renderer: (row: any) => {
        return (
          <>
            <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Supprimer">
              <IconButton color="inherit" onClick={onClickDelete(row)}>
                <Delete />
              </IconButton>
            </Tooltip>
            <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Modifier">
              <IconButton color="secondary" onClick={goToEdit(row.id)}>
                <Edit />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  const DATES_COLUMNS: Column[] = [
    {
      name: 'dateCreation',
      label: 'Date de création',
      renderer: (row: any) => {
        return moment(row.dateDebut).format('DD/MM/YYYY');
      },
    },
    {
      name: 'dateModification',
      label: 'Mise à jour',
      renderer: (row: any) => {
        return moment(row.dateFin).format('DD/MM/YYYY');
      },
    },
  ];

  const AVATAR_COLUMNS: Column[] = [
    {
      name: '',
      label: 'Image',
      renderer: (row: any) => {
        if (row && row.fichier && row.fichier.publicUrl) {
          return <img src={row.fichier.publicUrl} style={{ width: 50, height: 50, borderRadius: '50%' }} />;
        }
        return '-';
      },
    },
    {
      name: 'description',
      label: 'Désignation',
    },
    {
      name: 'codeSexe',
      label: 'Sexe',
      renderer: (row: any) => {
        return (row.codeSexe && row.codeSexe === Sexe.M ? 'Homme' : 'Femme') || '-';
      },
    },
    {
      name: 'userCreation.userName',
      label: 'Créateur',
      renderer: (row: any) => {
        return (row.userCreation && row.userCreation.userName) || '-';
      },
    },
    {
      name: 'userCreation.role.nom',
      label: 'Rôle',
      renderer: (row: any) => {
        return (
          (row.userCreation &&
            row.userCreation.role &&
            row.userCreation.role.nom &&
            startCase(row.userCreation.role.nom.toLowerCase())) ||
          '-'
        );
      },
    },
  ];

  const COLUMNS: Column[] = [...AVATAR_COLUMNS, ...DATES_COLUMNS, ...ACTIONS_COLUMNS];

  /**
   * Mutation delete avatars
   */
  const [deleteAvatars, { loading, data: deleteData }] = useDelete_AvatarsMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.deleteAvatars && data.deleteAvatars.length > 0) {
        setDeleteRow(null);
        const isInvalid: boolean = isInvalidArray(data.deleteAvatars);
        if (isInvalid) {
          notify({
            type: 'error',
            message: 'Erreur lors de la suppression',
          });
        } else {
          setSelected([]);
          notify({
            type: 'success',
            message: 'Avatar(s) supprimé(s) avec succès',
          });
        }
      }
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        const { extensions: ext } = err;
        let msg = err.message;
        if (ext && ext.code === 'NO_AVATAR_TO_DELETE') {
          msg = 'Aucune avatar a supprimé';
        }
        if (ext && ext.code === 'AVATAR_ALREADY_USED') {
          msg = 'Ces avatars sont déjà utilisés par des utilisateurs, impossible de les supprimés';
        }
        notify({ type: 'error', message: msg });
      });
      notify({ type: 'error', message: errors.message });
    },
  });

  const onClickDelete = (row: any) => (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    setOpenDeleteDialog(true);
    setDeleteRow(row);
    if (selected.length > 0) {
      setSelected([]);
    }
  };

  const goToEdit = (id: string) => () => {
    push(`/db/avatar/edit/${id}`);
  };

  const onClickBtnFilter = () => {
    setOpenFilterModal(true);
  };

  const onClickApplyBtn = () => {
    setOpenFilterModal(false);
    // Set filterBy
    const newFilterBy: any[] = [];
    if (description) {
      const newTerm = { wildcard: { description: `*${description}*` } };
      newFilterBy.push(newTerm);
    }
    if (creator) {
      const newTerm = { wildcard: { 'userCreation.userName': `*${creator}*` } };
      newFilterBy.push(newTerm);
    }

    if (sexe && sexe !== 'ALL') {
      const newTerm = { term: { codeSexe: sexe } };
      newFilterBy.push(newTerm);
    }
    setFilterBy(newFilterBy);
  };

  const onClickInitBtn = useCallback(() => {
    setOpenFilterModal(false);
    setValues((prevState) => ({ ...prevState, description: '', creator: '', sexe: 'ALL' }));
    setFilterBy([]);
  }, []);

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (selected.length > 0) {
      const ids: string[] = selected.map((i) => i.id);
      deleteAvatars({ variables: { ids } });
    }

    if (deleteRow && deleteRow.id) {
      deleteAvatars({ variables: { ids: [deleteRow.id] } });
    }
  };

  /**
   * Re-render component after delete
   */
  useEffect(() => {
    if (deleteData && deleteData.deleteAvatars) {
      onClickInitBtn();
      // setFilterBy([]);
    }
  }, [deleteData]);

  const DeleteDialogContent = () => {
    if (deleteRow && deleteRow.description) {
      return (
        <span>
          Êtes-vous sur de vouloir supprimer l'avatar
          <span style={{ fontWeight: 'bold' }}> {deleteRow.description}</span> ?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces avatars ?</span>;
  };

  return (
    <div className={classes.avatarListRoot}>
      {loading && <Backdrop value="Suppression en cours..." />}

      <CustomContent
        isSelectable={true}
        selected={selected}
        setSelected={setSelected}
        columns={COLUMNS}
        listResult={listResult}
      />
      <CustomModal
        title="Filtres de recherche"
        open={openFilterModal}
        setOpen={setOpenFilterModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        maxWidth="xl"
      >
        <AvatarFilter
          state={values}
          setState={setValues}
          handleChangeInput={handleChange}
          onClickApplyBtn={onClickApplyBtn}
          onClickInitBtn={onClickInitBtn}
        />
      </CustomModal>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </div>
  );
};

export default AvatarList;
