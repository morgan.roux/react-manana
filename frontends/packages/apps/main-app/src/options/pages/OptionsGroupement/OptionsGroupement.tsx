import { PARAMETRES_ROUTES_PREFIX } from '@lib/common';
import React, { FC } from 'react';
import Parameters from '../../components/Parameters/Parameters';

const OptionsGroupement: FC = () => {
  return (
    <Parameters
      isOnOptionsGroupement={true}
      withSearchInput={true}
      baseUrl={`${PARAMETRES_ROUTES_PREFIX}/options-groupement`}
      where="inDashboard"
    />
  );
};

export default OptionsGroupement;
