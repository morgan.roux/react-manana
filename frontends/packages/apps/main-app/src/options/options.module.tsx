import React from 'react';
import { ModuleDefinition } from '@lib/common';
import MyAccountIcon from '@material-ui/icons/Person';
import loadable from '@loadable/component';
import { SmallLoading } from '@lib/ui-kit';
import SettingsIcon from './assets/icons/menu/parametres.svg';
import OptionGroupementIcon from '@material-ui/icons/Tune';
import { PARAMETRES_ROUTES_PREFIX } from '@lib/common';
import { Face } from '@material-ui/icons';

const UserSettingsPage = loadable(() => import('./pages/UserSettings/UserSettings'), {
  fallback: <SmallLoading />,
});
const OptionsPharmaciePage = loadable(() => import('./pages/OptionsPharmacie/OptionsPharmacie'), {
  fallback: <SmallLoading />,
});

const OptionsGroupementPage = loadable(() => import('./pages/OptionsGroupement/OptionsGroupement'), {
  fallback: <SmallLoading />,
});

const GestionAvatarPage = loadable(() => import('./pages/GestionAvatar/GestionAvatar'), { fallback: <SmallLoading /> });

const optionsModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_MENU_PRINCIPAL_PARAMETRE',
      location: 'MENU_PRINCIPAL',
      name: 'Paramètres',
      to: `${PARAMETRES_ROUTES_PREFIX}/user-settings`,
      icon: SettingsIcon,
      preferredOrder: 10,
    },

    {
      id: 'FEATURE_PARAMETRE_MON_COMPTE',
      location: 'PARAMETRE',
      name: 'Mon compte',
      to: '/user-settings',
      icon: <MyAccountIcon />,
      preferredOrder: 10,
    },

    {
      id: 'FEATURE_PARAMETRE_OPTIONS_GROUPEMENT',
      location: 'PARAMETRE',
      name: 'Options Groupement',
      to: '/options-groupement',
      icon: <OptionGroupementIcon />,
      preferredOrder: 180,
      authorize: {
        roles: ['SUPADM', 'GRPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_PARAMETRE_OPTIONS_PHARMACIE',
      location: 'PARAMETRE',
      name: 'Options Pharmacie',
      to: '/options-pharmacie',
      icon: <OptionGroupementIcon />,
      preferredOrder: 250,
      authorize: {
        roles: ['SUPADM', 'GRPADM', 'PRMTIT'],
        rolesDenied: ['PRTSERVICE']
      },
    },
    {
      id: 'FEATURE_PARAMETRE_GESTION_AVATAR',
      location: 'PARAMETRE',
      name: "Gestion d'avatar",
      to: `/avatar`,
      icon: <Face />,
      preferredOrder: 20,
      authorize: {
        roles: ['SUPADM', 'GRPADM'],
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
  routes: [
    {
      path: ['/user-settings', '/user-settings/edit'],
      component: UserSettingsPage,
      exact: false,
      attachTo: 'PARAMETRE',
    },
    {
      path: ['/options-pharmacie', '/options-pharmacie/edit'],
      component: OptionsPharmaciePage,
      exact: false,
      attachTo: 'PARAMETRE',
    },
    {
      path: ['/options-groupement', '/options-groupement/edit'],
      component: OptionsGroupementPage,
      exact: false,
      attachTo: 'PARAMETRE',
    },
    {
      path: ['/avatar', '/avatar/create', '/avatar/edit/:avatarId'],
      component: GestionAvatarPage,
      exact: false,
      attachTo: 'PARAMETRE',
    },
  ],
};

export default optionsModule;
