import { ModuleDefinition } from '@lib/common';
import React from 'react';
import { useFetchJson } from '@lib/hooks';

interface ModuleOption {
  name: string; // module name :  "iam"
  url: string; // Url of remote module : "http://localhost:3003/remoteEntry.js"
}

const loadModules = (options: ModuleOption[]) => {
  return Promise.all(
    options.map(async ({ name }) => {
      // Initializes the share scope. This fills it with known provided modules from this build and all remotes
      // @ts-ignore
      await __webpack_init_sharing__('default');

      // @ts-ignore
      const container = window[name]; // or get the container somewhere else
      // Initialize the container, it may provide shared modules
      // @ts-ignore
      await container.init(__webpack_share_scopes__.default);
      // @ts-ignore
      const factory = await window[name].get('./module');
      const Module = factory();
      return Module;
    })
  );
};

const loadScript = (url: string): Promise<HTMLScriptElement> => {
  return new Promise((resolve, reject) => {
    const element = document.createElement('script');
    element.src = url;
    element.type = 'text/javascript';
    element.async = true;
    element.onload = () => {
      console.log(`Dynamic Script Loaded: ${url}`);
      resolve(element);
    };

    element.onerror = (error) => {
      console.error(`Dynamic Script Error: ${url}`, error);
      reject(error);
    };

    document.head.appendChild(element);
  });
};

interface DynamicModulesResult {
  loading?: boolean;
  error?: Error;
  modules?: ModuleDefinition[];
}

export const useDynamicModules = (options: ModuleOption[]): DynamicModulesResult => {
  const { data: envConfigData, loading: envConfigLoading, error: envConfigError } = useFetchJson(
    //@ts-ignore
    `${__webpack_public_path__}env-config.json`
  );
  const [loading, setLoading] = React.useState<boolean>(false);
  const [error, setError] = React.useState<Error>();
  const [scriptElements, setScriptElements] = React.useState<HTMLScriptElement[]>([]);
  const [modules, setModules] = React.useState<ModuleDefinition[]>([]);

  React.useEffect(() => {
    if (!envConfigLoading && !envConfigError && envConfigData) {
      setLoading(true);
      if (!options) {
        setLoading(false);
        return;
      }

      Promise.all(options.map((option) => loadScript(`${envConfigData[option.url]}/remoteEntry.js`)))
        .then(async (elements) => {
          const loadedModules = await loadModules(options);
          setScriptElements(elements);
          setLoading(false);
          setModules(loadedModules.map((m) => m.default));
        })
        .catch((error) => {
          setLoading(false);
          console.log(`Dynamic Script Error`, error);
          setError(error);
        });

      return () => {
        scriptElements.forEach((scriptElement) => {
          console.log(`Dynamic Script Removed: ${scriptElement.src}`);
          document.head.removeChild(scriptElement);
        });
      };
    }
  }, [envConfigLoading]);

  return {
    modules,
    loading: envConfigError || error ? false : envConfigLoading || loading || modules.length < options.length,
    error: envConfigError || error,
  };
};
