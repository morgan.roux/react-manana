import React from 'react';
import { ModuleDefinition } from '@lib/common';
import loadable from '@loadable/component';
import { SmallLoading } from '@lib/ui-kit';

const NotificationPage = loadable(() => import('./components/NotificationPage/NotificationPage'), {
  fallback: <SmallLoading />,
});

const notificationModule: ModuleDefinition = {
  routes: [
    {
      path: '/notifications',
      component: NotificationPage,
      exact: false,
      attachTo: 'MAIN',
    },
  ],
};

export default notificationModule;
