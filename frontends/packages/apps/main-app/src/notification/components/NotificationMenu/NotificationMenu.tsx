import React, { FC } from 'react';
import { Popper, Paper, Box, MenuList, Link, PopperProps } from '@material-ui/core';
import NotificationItem from './NotificationItem';
import { useStyles } from '../styles';
import { useHistory } from 'react-router-dom';
import { LoaderSmall } from '@lib/ui-kit';
import { NotificationsQueryResult } from '@lib/common/src/graphql';
import { Waypoint } from 'react-waypoint';

interface NotificationMenuProps {
  handleClose: (event: React.MouseEvent<EventTarget>) => void;
  notifications: NotificationsQueryResult | null | undefined;
  loading: boolean;
  fetchMoreNotifications: () => void;
}

const NotificationMenu: FC<{ popperProps: PopperProps } & NotificationMenuProps> = ({
  handleClose,
  notifications,
  loading,
  fetchMoreNotifications,
  popperProps,
}) => {
  const history = useHistory();
  const { anchorEl } = popperProps;
  const position = anchorEl ? (anchorEl as HTMLElement).getBoundingClientRect() : null;
  const classes = useStyles({ position });

  const gotToNotificationPage = () => {
    history.push('/notifications');
  };

  const renderNotificationItem = () => {
    return (
      handleClose &&
      (notifications?.data?.notifications?.data || [])
        .filter((notificationItem) => notificationItem && notificationItem.seen === false)
        .map(
          (notification, index) =>
            notification && (
              <>
                <NotificationItem handleClose={handleClose} notification={notification} key={`notif_${index}`} />
                {notifications &&
                  notifications.data &&
                  index === (notifications?.data?.notifications?.data?.length || 0) - 1 &&
                  notifications.data.notifications?.data?.length !== (notifications.data.notifications?.total || 0) && (
                    <Waypoint onEnter={fetchMoreNotifications} />
                  )}
              </>
            )
        )
    );
  };

  return (
    <Popper {...popperProps} style={{ zIndex: 10000 }}>
      <Paper className={classes.paper}>
        <MenuList className={classes.contentListMenu} id="menu-list-grow">
          {renderNotificationItem()}
        </MenuList>

        {loading && !notifications && (
          <Box textAlign="center" padding="12px 24px">
            <LoaderSmall />
          </Box>
        )}

        <Box className={classes.voirTout} textAlign="center" padding="12px 24px">
          {(notifications?.data?.notifications?.data?.length || 0) > 0 ? (
            <Link onClick={gotToNotificationPage} className={classes.linkNotificationFullPAge}>
              Voir toutes les notifications
            </Link>
          ) : (
            <div>Aucune notification</div>
          )}
        </Box>
      </Paper>
    </Popper>
  );
};

export default NotificationMenu;
