import React, { FC, MouseEvent } from 'react';
import { Box, MenuItem, Avatar, Typography } from '@material-ui/core';
import classNames from 'classnames';
import { useStyles } from '../styles';
import {
  useMark_Notification_As_SeenMutation,
  NotificationInfoFragment,
  NotificationsQuery,
  NotificationsDocument,
  NotificationsQueryVariables,
} from '@lib/common/src/graphql';
import moment from 'moment';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useApplicationContext, nl2br, stringToAvatar, stopEvent } from '@lib/common';

interface NotificationItemProps {
  handleClose: (event: React.MouseEvent<EventTarget>) => void;
  notification: NotificationInfoFragment;
  key: any;
}

const NotificationItem: FC<NotificationItemProps & RouteComponentProps> = ({
  handleClose,
  notification,
  history,
  key,
}) => {
  const classes = useStyles({});
  const { user, graphql } = useApplicationContext();

  const userName = user.fullName;
  const userImage = user.photo?.publicUrl;

  let notificationMsg: string | null = null;

  if (notification.targetName) {
    switch (notification.type) {
      case 'PATENAIRE_ADDED':
        notificationMsg = `Un nouveau partenaire <b>${notification.targetName}</b> vient d'être ajoutée`;
        break;
      case 'ACTUALITE_ADDED':
        notificationMsg = `Une nouvelle actualité <b>${notification.targetName}</b> vient d'être ajoutée`;
        break;
      case 'TODO_ASSIGN':
        notificationMsg = `On vient de vous assigner une tâche dans le projet </b>${notification.targetName}</b>`;
        break;
      default:
        break;
    }
  }

  const [markNotificationAsSeen] = useMark_Notification_As_SeenMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.markNotificationAsSeen) {
        const query = cache.readQuery<NotificationsQuery, NotificationsQueryVariables>({
          query: NotificationsDocument,
          variables: { first: 10, skip: 0 },
        });
        if (query && query.notifications && query.notifications.data) {
          cache.writeQuery<NotificationsQuery, NotificationsQueryVariables>({
            query: NotificationsDocument,
            data: {
              notifications: {
                __typename: 'NotificationResult',
                total: query.notifications.total,
                notSeen: query.notifications.notSeen - 1,
                data: query.notifications.data,
              },
            },
            variables: { first: 10, skip: 0 },
          });
        }
      }
    },
  });

  const clickNotification = (event: MouseEvent<EventTarget>): void => {
    stopEvent(event);
    handleClose(event);
    if (notification.seen === false && notification.type != 'ACTUALITE_ADDED') {
      markNotificationAsSeen({ variables: { id: notification.id, seen: true } });
    }
    // Go to actualite
    if (notification.type === 'ACTUALITE_ADDED' && notification.targetId) {
      history.push(`/actualite/${notification.targetId}`);
    }
    // GO to Todo
    if (notification.type === 'TODO_ASSIGN' && notification.targetId) {
      history.push(`/project/${notification.targetId}/tasks`);
    }
  };

  return (
    <MenuItem
      className={notification && notification.seen === false ? classes.notSeen : ''}
      onClick={clickNotification}
      key={key}
    >
      <Box className={classNames(classes.row, classes.contentNotificationToogle)} justifyContent="start">
        <Box position="relative">
          {userImage && <Avatar alt="Remy Sharp" src={userImage} className={classes.bigAvatar} />}
          {!userImage && <Avatar className={classes.bigAvatar}>{stringToAvatar(userName || '')}</Avatar>}
          {/* <Box position="absolute" className={classes.iconAvatar}>
            <ChatBubbleIcon className={classes.iconColorChat} />
          </Box> */}
        </Box>

        <Box justifyContent="space-between" className={classes.col}>
          <Box
            className={classNames(classes.row, classes.textLimit)}
            flexWrap="wrap"
            alignItems="center"
            justifyContent="start"
          >
            <Typography className={classes.nameNotification}>
              {notification.from && notification.from ? notification.from.userName : ''}
            </Typography>
            <Typography
              className={classes.pNotification}
              dangerouslySetInnerHTML={{ __html: nl2br(notificationMsg || notification.message || '') } as any}
            />
          </Box>
          <Box>
            <Typography className={classes.momentNotification}>
              {moment(notification.dateCreation).fromNow()}
            </Typography>
          </Box>
        </Box>
      </Box>
    </MenuItem>
  );
};

export default withRouter(NotificationItem);
