import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    menuButton: {
      marginRight: theme.spacing(2),
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
        paddingRight: 20,
        alignItems: 'center',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
    iconBtn: {
      color: '#ffffff',
      opacity: 1,
      maxHeight: 50,
    },
    bigAvatar: {
      width: 60,
      height: 60,
      marginRight: 24,
    },
    smallAvatar: {
      width: 36,
      height: 36,
      marginRight: 16,
    },
    row: {
      boxSizing: 'border-box',
      display: 'flex',
      '-webkit-flex': '0 1 auto',
      '-ms-flex': '0 1 auto',
      flex: '0 1 auto',
      flexDirection: 'row',
    },
    col: {
      boxSizing: 'border-box',
      display: 'flex',
      '-webkit-flex': '0 1 auto',
      '-ms-flex': '0 1 auto',
      flex: '0 1 auto',
      flexDirection: 'column',
    },
    contentNotificationToogle: {
      width: 465,
    },
    nameNotification: {
      fontSize: '0.925rem',
      fontWeight: 'bold',
      color: theme.palette.text.primary,
      marginRight: 6,
    },
    contentListMenu: {
      padding: '0',
      marginTop: 10,
      overflowY: 'auto',
      maxHeight: 390,
      '& .MuiListItem-root': {
        padding: '12px 24px',
      },
      '&:focus': {
        outline: 'none',
      },
    },
    notSeen: {
      backgroundColor: '#F5F6FA',
    },
    pNotification: {
      fontSize: '0.75rem',
      color: theme.palette.text.primary,
      marginRight: 6,
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      '-webkit-line-clamp': 1,
      '-webkit-box-orient': 'vertical',
      [theme.breakpoints.up('lg')]: {
        whiteSpace: 'nowrap',
      },
    },
    linkNotification: {
      fontSize: '0.8125rem',
      color: '#0B5FBF',
    },
    linkNotificationFullPAge: {
      fontSize: '0.8125rem',
      cursor: 'pointer',
      color: '#ffffff',
    },
    textLimit: {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      '-webkit-box-orient': 'vertical',
      '-webkit-line-clamp': 2,
      lineHeight: 8,
      maxHeight: 42,
      maxWidth: 370,
    },
    momentNotification: {
      color: '#878787',
      fontSize: '0.75rem',
    },
    iconAvatar: {
      position: 'absolute',
      bottom: -13,
      right: 17,
    },
    iconColorChat: {
      color: '#8CC63F',
    },
    iconColorPink: {
      color: '#F11957',
    },
    imgArc: {
      position: 'absolute',
      top: '-15px',
      right: '41%',
    },
    voirTout: {
      textAlign: 'center',
      padding: '12px 24px',
      color: theme.palette.common.white,
      background: theme.palette.secondary.main,
      borderRadius: '0px 0px 4px 4px',
    },
    fullPageContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
    },
    notifLeftSidebar: {
      maxWidth: 275,
      minHeight: 'calc(100vh - 86px)',
      borderRight: '1px solid #dddfe2',
      padding: '8px 10px',
    },
    notifMainContent: {
      width: '100%',
      padding: '0px 20px',
      [theme.breakpoints.down('md')]: {
        padding: 0,
      },
    },
    contentNotificationToogleFullPage: {
      width: '100%',
    },
    textLimitFullPage: {
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      '-webkit-box-orient': 'vertical',
      '-webkit-line-clamp': 1,
      lineHeight: 8,

      maxWidth: '100%',
      [theme.breakpoints.up('lg')]: {
        maxHeight: 20,
      },
    },
    iconAvatarFullPage: {
      position: 'absolute',
      bottom: -9,
      right: 8,
    },
    iconAvatarSmall: {
      width: '0.75em',
      height: '0.75em',
    },
    listItemNotificationFullPage: {
      borderBottom: '1px solid #dddfe2',
    },
    iconActionNotif: {
      padding: 0,
      '&:hover': {
        backgroundColor: 'inherit',
      },
      '& .MuiIconButton-label:hover': {
        opacity: 0.75,
      },
    },
    TitlePage: {
      fontSize: 18,
      fontWeight: 'bold',
      textAlign: 'start',
      marginBottom: 10,
      padding: '6px 0 10px',
      display: 'flex',
      alignItems: 'center',
      color: theme.palette.secondary.main,
      '& > svg': {
        marginRight: 10,
      },
    },
    paper: ({ position }: any) => ({
      '&:before': {
        borderColor: 'transparent transparent #fff transparent',
        borderWidth: '0 1em 1em 1em',
        width: 0,
        height: 0,
        margin: 'auto',
        content: '""',
        display: 'block',
        borderStyle: 'solid',
        position: 'absolute',
        top: 0,
        left: window.innerWidth < 1050 && position ? `${window.innerWidth - position.left + 172}px` : 0,
        right: 0,
        fontSize: '10px',
        [theme.breakpoints.down('xs')]: {
          left: 100,
        },
        [theme.breakpoints.between('xs', 'md')]: {
          left: 86,
        },
      },
    }),
    filterSection: {
      display: 'flex',
      alignItems: 'flex-start',
      justifyContent: 'flex-start',
      flexDirection: 'column',
      '& > div': {
        width: '100%',
        maxWidth: 253,
        minWidth: 200,
        marginBottom: 20,
      },
    },
    filterByInput: {
      width: 320,
    },
    orderByInput: {
      width: 200,
      border: '1px solid #1D1D1D',
      borderRadius: 4,
      '&:hover label': {
        color: '#B3D00A',
      },
    },
    orderByLabel: {
      background: '#ffffff',
      padding: '0px 4px',
      color: '#1D1D1D',
    },
    emptyNotification: {
      textAlign: 'center',
      marginTop: '48px',
      fontSize: '1.25em',
    },
  })
);
