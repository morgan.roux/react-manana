import React, { FC, Fragment, useState, useEffect } from 'react';
import { useNotificationsQuery, NotificationOrderByInput } from '@lib/common/src/graphql';
import { Waypoint } from 'react-waypoint';
import { useStyles } from '../styles';
import ListItem from './ListItem';
import { List, Hidden, Typography } from '@material-ui/core';
import moment, { Moment } from 'moment';
import { Notifications } from '@material-ui/icons';
import { useApplicationContext } from '@lib/common';
import { Loader, LoaderSmall, CustomSelect, DateRangePicker } from '@lib/ui-kit';

interface DateFilterState {
  dateFilterStart: any;
  dateFilterEnd: any;
}

interface OrderByState {
  label: string;
  value: NotificationOrderByInput;
}

const DATETIME_FORMAT = 'YYYY-MM-DDTLTS';

const initalDateFilter: DateFilterState = {
  dateFilterStart: moment().subtract(7, 'days'),
  dateFilterEnd: moment(),
};

const orderByItems: OrderByState[] = [
  { label: 'Date', value: NotificationOrderByInput.DateCreationDesc },
  { label: 'Type', value: NotificationOrderByInput.TypeAsc },
  { label: 'Non Lu', value: NotificationOrderByInput.SeenAsc },
  { label: 'Lu', value: NotificationOrderByInput.SeenDesc },
];

const reformatedDateTime = (dateTime: Moment, initTime: string) => {
  if (dateTime) {
    return moment(moment(dateTime).toISOString().split('T').shift() + ' ' + initTime).format(DATETIME_FORMAT);
  }
};

const NotificationPage: FC = () => {
  const classes = useStyles({});
  const { graphql, isMobile } = useApplicationContext();

  const [orderBy, setOrderBy] = useState<OrderByState>(orderByItems[0]);
  const [dateFilter, setDateFilter] = useState<DateFilterState>(initalDateFilter);

  const { loading, data, fetchMore } = useNotificationsQuery({
    client: graphql,
    fetchPolicy: 'cache-and-network',
    variables: isMobile
      ? {
          first: 10,
          skip: 0,
          orderBy: orderBy.value as NotificationOrderByInput,
          dateFilterStart: reformatedDateTime(dateFilter.dateFilterStart, '00:00:00'),
          dateFilterEnd: reformatedDateTime(dateFilter.dateFilterEnd, '23:59:00'),
        }
      : {
          skip: 0,
          orderBy: orderBy.value as NotificationOrderByInput,
        },
  });

  useEffect(() => {
    if (dateFilter.dateFilterStart && dateFilter.dateFilterEnd) {
      fetchMore({
        variables:
          window.innerWidth > 600
            ? {
                first: 10,
                skip: 0,
                orderBy: orderBy.value as NotificationOrderByInput,
                dateFilterStart: reformatedDateTime(dateFilter.dateFilterStart, '00:00:00'),
                dateFilterEnd: reformatedDateTime(dateFilter.dateFilterEnd, '23:59:00'),
              }
            : {
                skip: 0,
                orderBy: orderBy.value as NotificationOrderByInput,
              },
        updateQuery: (prev) => {
          return prev;
        },
      });
    }
  }, [orderBy, dateFilter]);

  const getDates = (dateStart: Moment | null, dateEnd: Moment | null) => {
    setDateFilter({
      dateFilterStart: dateStart,
      dateFilterEnd: dateEnd,
    });
  };

  const onHandleSelect = (event: any) => {
    setOrderBy({
      label: orderByItems.filter((item) => item.value === event.target.value)[0].label,
      value: event.target.value,
    });
  };

  const isOutsideRange = (day: Moment) => moment().add(1, 'days').diff(day) < 0;

  const fetchMoreNotifications = () => {
    fetchMore({
      variables: {
        first: 10,
        skip: data && data.notifications && data.notifications.data ? data.notifications.data.length : 0,
        orderBy: orderBy.value as NotificationOrderByInput,
        dateFilterStart: reformatedDateTime(dateFilter.dateFilterStart, '00:00:00'),
        dateFilterEnd: reformatedDateTime(dateFilter.dateFilterEnd, '23:59:00'),
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;
        return {
          notifications: {
            __typename: 'NotificationResult',
            data:
              prev &&
              prev.notifications &&
              prev.notifications.data &&
              fetchMoreResult.notifications &&
              fetchMoreResult.notifications.data
                ? [...prev.notifications.data, ...fetchMoreResult.notifications.data]
                : [],
            total: data && data.notifications ? data.notifications.total : 0,
            notSeen: data && data.notifications ? data.notifications.notSeen : 0,
          },
        };
      },
    });
  };

  if (loading && !data) return <Loader />;

  return (
    <div className={classes.fullPageContainer}>
      <Hidden mdDown={true}>
        <div className={classes.notifLeftSidebar}>
          <Typography className={classes.TitlePage}>
            <Notifications />
            Notifications
          </Typography>
          <div className={classes.filterSection}>
            <div className={classes.filterByInput}>
              <DateRangePicker
                startDate={dateFilter.dateFilterStart}
                startDateId="startDateUniqueId"
                endDate={dateFilter.dateFilterEnd}
                endDateId="endDateUniqueId"
                getDates={getDates}
                label="Filtrer par période"
                variant="outlined"
                isOutsideRange={isOutsideRange}
                showDefaultInputIcon={false}
              />
            </div>
            <CustomSelect
              label="Trier par"
              list={orderByItems}
              listId="value"
              index="label"
              value={orderBy.value}
              onChange={onHandleSelect}
            />
          </div>
        </div>
      </Hidden>

      <div className={classes.notifMainContent}>
        {(data?.notifications?.data?.length || 0) > 0 ? (
          <>
            <List component="nav" aria-label="listes notifications">
              {(data?.notifications?.data || []).map((notification, index) => {
                return (
                  notification && (
                    <Fragment key={`notif_${index}`}>
                      <ListItem notification={notification} />
                      {data?.notifications?.data &&
                        index === (data?.notifications?.data?.length || 0) - 1 &&
                        data.notifications.data.length !== data?.notifications?.total && (
                          <Waypoint onEnter={fetchMoreNotifications} />
                        )}
                    </Fragment>
                  )
                );
              })}
            </List>
            <div style={{ display: 'flex', margin: '15px 0px' }}>{loading && !data && <LoaderSmall />}</div>
          </>
        ) : (
          <div className={classes.emptyNotification}>Aucune notification</div>
        )}
      </div>
    </div>
  );
};

export default NotificationPage;
