import React, { FC } from 'react';
import {
  useMark_Notification_As_SeenMutation,
  NotificationsQuery,
  NotificationsDocument,
  NotificationsQueryVariables,
  NotificationInfoFragment,
} from '@lib/common/src/graphql';

import classNames from 'classnames';
import { useStyles } from '../styles';
import { Box, Typography, Avatar, ListItem as MaterialUIListItem } from '@material-ui/core';
import moment from 'moment';
import 'moment/locale/fr';
import { useApplicationContext, nl2br, stringToAvatar } from '@lib/common';
import { RouteComponentProps, withRouter } from 'react-router-dom';
moment.locale('fr');

interface ListItemProps {
  notification: NotificationInfoFragment;
}

const ListItem: FC<ListItemProps & RouteComponentProps> = ({ notification, history }) => {
  const classes = useStyles({});
  const { user, graphql } = useApplicationContext();

  const userName = user.fullName;
  const userImage = user.photo?.publicUrl;

  let notificationMsg: string | null = null;

  if (notification.targetName) {
    switch (notification.type) {
      case 'PATENAIRE_ADDED':
        notificationMsg = `Un nouveau partenaire <b>${notification.targetName}</b> vient d'être ajoutée`;
        break;
      case 'ACTUALITE_ADDED':
        notificationMsg = `Une nouvelle actualité <b>${notification.targetName}</b> vient d'être ajoutée`;
        break;
      case 'TODO_ASSIGN':
        notificationMsg = `On vient de vous assigner une tâche dans le projet </b>${notification.targetName}</b>`;
        break;
      default:
        break;
    }
  }

  const [markNotificationAsSeen] = useMark_Notification_As_SeenMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (data && data.markNotificationAsSeen) {
        const query = cache.readQuery<NotificationsQuery, NotificationsQueryVariables>({
          query: NotificationsDocument,
          variables: { first: 10, skip: 0 },
        });
        if (query && query.notifications && query.notifications.data) {
          cache.writeQuery<NotificationsQuery, NotificationsQueryVariables>({
            query: NotificationsDocument,
            data: {
              notifications: {
                __typename: 'NotificationResult',
                total: query.notifications.total,
                notSeen: query.notifications.notSeen - 1,
                data: query.notifications.data,
              },
            },
            variables: { first: 10, skip: 0 },
          });
        }
      }
    },
  });

  const clickNotification = () => {
    if (notification.seen === false && notification.type !== 'ACTUALITE_ADDED') {
      markNotificationAsSeen({ variables: { id: notification.id, seen: true } });
    }

    // Go to actualite

    if (notification.type === 'ACTUALITE_ADDED' && notification.targetId) {
      history.push(`/actualite/${notification.targetId}`);
    }
    // GO to Todo
    if (notification.type === 'TODO_ASSIGN' && notification.targetId) {
      history.push(`/project/${notification.targetId}/tasks`);
    }
  };

  return (
    <MaterialUIListItem
      button={true}
      className={classNames(classes.listItemNotificationFullPage, notification.seen === false ? classes.notSeen : '')}
      onClick={clickNotification}
    >
      <Box
        justifyContent="space-betwwen"
        className={classNames(classes.row, classes.contentNotificationToogleFullPage)}
      >
        <Box className={classNames(classes.row, classes.contentNotificationToogleFullPage)} justifyContent="start">
          <Box position="relative">
            {userImage && <Avatar alt="Remy Sharp" src={userImage} className={classes.smallAvatar} />}
            {!userImage && <Avatar className={classes.smallAvatar}>{stringToAvatar(userName || '')}</Avatar>}
            {/* <Box position="absolute" className={classes.iconAvatarFullPage}>
              <ChatBubbleIcon
                className={classNames(classes.iconColorChat, classes.iconAvatarSmall)}
              />
            </Box> */}
          </Box>

          <Box justifyContent="space-between" className={classes.col}>
            <Box
              className={classNames(classes.row, classes.textLimitFullPage)}
              flexWrap="wrap"
              alignItems="center"
              justifyContent="start"
            >
              {/* TODO */}
              <Typography className={classes.nameNotification}>
                {notification.from && notification.from ? notification.from.userName : ''}
              </Typography>
              <Typography
                className={classes.pNotification}
                dangerouslySetInnerHTML={{ __html: nl2br(notificationMsg || notification.message || '') } as any}
              />
            </Box>
            <Box>
              <Typography className={classes.momentNotification}>
                {moment(notification.dateCreation).fromNow()} &bull; {notification.seen ? 'Lu' : 'Non lu'}
              </Typography>
            </Box>
          </Box>
        </Box>

        {/* <IconButton
          className={classNames(classes.iconActionNotif)}
          aria-label="Open drawer"
          edge="start"
        >
          <DeleteIcon />
        </IconButton> */}
      </Box>
    </MaterialUIListItem>
  );
};

export default withRouter(ListItem);
