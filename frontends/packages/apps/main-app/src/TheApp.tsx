import React, { FC, useEffect, useState } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import { ApolloProvider } from '@apollo/client';
import { fr } from 'yup-locales';
import { setLocale } from 'yup';
import { ModuleDefinition, useInitClient, NotificationSnackBar, DomaineActiviteProvider } from '@lib/common';
import TheProtectedApp from './TheProtectedApp';
import { MainLoading } from '@lib/ui-kit';
import loadable from '@loadable/component';
import {
  getGroupement,
  isAuthenticated,
  isChooseGroupementRequested,
  isUpdatePasswordRequested,
  setDomaineActivite,
} from '@lib/common/src/auth/auth-util';
import ChoiceGroupement from './groupement/components/Groupement';
import CreateGroupement from './groupement/components/Create/Create';
import UpdatePassword from './core/Authentication/update-password';
import ForgotPassword from './core/Authentication/forgot-password';
import ResetPassword from './core/Authentication/reset-password';
import SetPassword from './core/Authentication/set-password';

import ErrorPage from './Error';
import {
  DomaineActiviteFilter,
  DomaineActiviteInfoFragment,
  Get_Domaine_ActivitesDocument,
  Get_Domaine_ActivitesQuery,
  Get_Domaine_ActivitesQueryVariables,
} from '@lib/common/src/federation';

setLocale(fr);

const SigninPage = loadable(() => import('./core/Authentication/signin'), {
  fallback: <MainLoading />,
});

interface TheAppProps {
  modules: ModuleDefinition[];
  sousDomaine: string;
}

interface DomaineActiviteResult {
  data?: DomaineActiviteInfoFragment;
  loading?: boolean;
  error?: Error;
}

const TheApp: FC<TheAppProps> = ({ modules, sousDomaine }) => {
  const { federation, graphql, loading, error, config } = useInitClient(modules);
  const [domaineActiviteContext, setDomaineActiviteContext] = useState<DomaineActiviteResult>({
    loading: true,
  });

  useEffect(() => {
    if (federation) {
      setDomaineActiviteContext({
        loading: true,
        data: undefined,
        error: undefined,
      });

      const filter: DomaineActiviteFilter =
        sousDomaine.length > 0
          ? {
              or: [
                {
                  sousDomaine: {
                    eq: sousDomaine,
                  },
                },
                {
                  parDefaut: {
                    is: true,
                  },
                },
              ],
            }
          : {
              parDefaut: {
                is: true,
              },
            };

      federation
        .query<Get_Domaine_ActivitesQuery, Get_Domaine_ActivitesQueryVariables>({
          query: Get_Domaine_ActivitesDocument,
          variables: {
            filter,
          },
        })
        .then((response) => {
          if (response.data.domaineActivites.nodes.length > 0) {
            const data =
              response.data.domaineActivites.nodes.find((da) => !da.parDefaut) ||
              response.data.domaineActivites.nodes[0];
            setDomaineActivite(data);
            setDomaineActiviteContext({
              data,
              loading: false,
            });
          } else {
            setDomaineActiviteContext({
              loading: false,
              error: new Error(`Domaine activite ${sousDomaine} non trouvé`),
            });
          }
        })
        .catch((error) => {
          setDomaineActiviteContext({
            loading: false,
            error,
          });
        });
    }
  }, [federation, sousDomaine]);

  if (loading || domaineActiviteContext.loading) {
    return <MainLoading />;
  }
  if (error || domaineActiviteContext.error || !federation || !graphql || !domaineActiviteContext.data) {
    return <ErrorPage error={error as any} />;
  }

  return (
    <DomaineActiviteProvider value={domaineActiviteContext.data}>
      <ApolloProvider client={federation}>
        <NotificationSnackBar />
        {!isAuthenticated() ? (
          <HashRouter>
            <Switch>
              <Route path="/forgot-password">
                <ForgotPassword graphql={graphql} federation={federation} />
              </Route>
              <Route path="/password/reset/:token">
                <ResetPassword graphql={graphql} federation={federation} />
              </Route>
              <Route path="/password/set/:token">
                <SetPassword graphql={graphql} federation={federation} />
              </Route>
              <Route path="/*" component={SigninPage} exact={true} />
            </Switch>
          </HashRouter>
        ) : isChooseGroupementRequested() && !getGroupement() ? (
          <HashRouter>
            <Switch>
              <Route path="/groupement/new">
                <CreateGroupement graphql={graphql} federation={federation} />
              </Route>
              <Route path="/*">
                <ChoiceGroupement graphql={graphql} federation={federation} />
              </Route>
            </Switch>
          </HashRouter>
        ) : isUpdatePasswordRequested() ? (
          <UpdatePassword graphql={graphql} federation={federation} />
        ) : (
          <TheProtectedApp
            domaineActivite={domaineActiviteContext.data}
            config={config}
            modules={modules}
            graphql={graphql}
            federation={federation}
          />
        )}
      </ApolloProvider>
    </DomaineActiviteProvider>
  );
};

export default TheApp;
