import React, { Suspense } from 'react';
import { Worker } from '@react-pdf-viewer/core';
import '@react-pdf-viewer/core/lib/styles/index.css';
import '@react-pdf-viewer/page-navigation/lib/styles/index.css';
import { ThemeProvider } from './core/Theme';
import { MainLoading } from '@lib/ui-kit';
// @ts-ignore
import TheApp from './TheApp';
import { useDynamicModules } from './useDynamicModules';
// @ts-ignore
import remotesModules from './../remote-modules.json';
import './App.css';
import mainModules from './module';
import { useEffect } from 'react';
import ErrorPage from './Error';
import moment from 'moment';
import { I18nextProvider } from 'react-i18next';

import i18n from './i18n';

moment.locale('fr');

const getSubdomain = (): string => {
  const hostname = window.location.hostname;
  const regexParse = new RegExp('[a-z-0-9]{2,63}.[a-z.]{2,5}$');
  const urlParts = regexParse.exec(hostname);

  if (urlParts) {
    const subdomain = hostname.replace(urlParts[0], '').slice(0, -1);
    if (subdomain.length) {
      return subdomain.trim();
    }
  }

  return '';
};

const SOUS_DOMAINE = getSubdomain();

const App = () => {
  const { loading, error, modules } = useDynamicModules(remotesModules);

  useEffect(() => {
    const el = document.querySelector('.loading-indicator');
    if (el) {
      el.remove();
    }
  }, []);

  console.log('---------------------modules --- sous domaine-----------------', modules, SOUS_DOMAINE);

  return (
    <I18nextProvider i18n={i18n}>
      <ThemeProvider>
        {loading ? (
          <MainLoading />
        ) : error ? (
          <ErrorPage error={error} />
        ) : (
          <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.8.335/build/pdf.worker.min.js">
            <TheApp sousDomaine={SOUS_DOMAINE} modules={[mainModules, ...(modules || [])]} />
          </Worker>
        )}
      </ThemeProvider>
    </I18nextProvider>
  );
};

export default function WrappedApp() {
  return (
    <Suspense fallback={<MainLoading />}>
      <App />
    </Suspense>
  );
}
