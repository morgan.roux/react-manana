import React, { FC } from 'react';
import { CustomModal } from '@lib/ui-kit';

interface IdleTimeoutProps {
  open: boolean;
  onRequestClose: () => void;
  onRequestLogout: () => void;
}

const IdleTimeout: FC<IdleTimeoutProps> = ({ open, onRequestClose, onRequestLogout }) => {
  return (
    <CustomModal
      open={open}
      setOpen={() => onRequestClose()}
      title="Déconnexion Automatique"
      withBtnsActions={true}
      cancelButtonTitle="Rester connecté"
      actionButtonTitle="Se déconnecter"
      onClickConfirm={() => onRequestLogout()}
      customHandleClose={() => onRequestClose()}
    >
      Vous êtes resté longtemps inactif!
    </CustomModal>
  );
};

export default IdleTimeout;
