import { ModuleDefinition, mergeModules } from '@lib/common';
import moment from 'moment';
import theTodoModule from './todo/todo.module';
import traitementRecurrentModule from './traitement-recurrent/traitement-recurrent.module';

moment.locale('fr');

const todoModule: ModuleDefinition = mergeModules(theTodoModule, traitementRecurrentModule);
export default todoModule;
