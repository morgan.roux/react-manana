import {
  Box,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
  List,
  ListItem,
  Typography,
} from '@material-ui/core';
import { Close } from '@material-ui/icons';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import React, { FC } from 'react';
import AlertImage from '../assets/img/Todo/alerte.svg';
import useStyles from './styles';
import { useApplicationContext } from '@lib/common';
import { useHistory } from 'react-router';
import { useBulk_CountQuery } from '@lib/common/src/federation';
import { NewCustomButton as CustomButton } from '@lib/ui-kit';
import { useEffect } from 'react';
import moment from 'moment';
import { buildTodoActionsQuery } from '../pages/NewTodo/hooks/util/query';

interface TodoAlertProps {
  setOpen: (open: boolean) => void;
  open: boolean;
}

const TodoAlert: FC<TodoAlertProps> = ({ open, setOpen }) => {
  const dateNow = moment.utc().startOf('day');

  const classes = useStyles({});
  const history = useHistory();
  const { user, federation } = useApplicationContext();

  useEffect(() => {
    const lastDate = localStorage.getItem('viewedLastDate');
    const dateIsOver: boolean = !lastDate || dateNow.diff(lastDate, 'days') >= 1;

    if (!dateIsOver) {
      setOpen(false);
    }
  }, []);

  const todoActionsCount = useBulk_CountQuery({
    client: federation,
    variables: {
      queries: {
        pastMe: {
          index: 'todoaction',
          ...buildTodoActionsQuery({
            user,
            params: {
              date: 'past',
              assignee: ['me'],
              type: ['ACTIVE'],
              projet: undefined,
            },
            urgences: [],
            options: {
              includePast: false,
            },
          }),
        },
        todayMe: {
          index: 'todoaction',
          ...buildTodoActionsQuery({
            user,
            params: {
              date: 'today',
              assignee: ['me'],
              type: ['ACTIVE'],
              projet: undefined,
            },
            urgences: [],
            options: {
              includePast: false,
            },
          }),
        },
      },
    },

    onError: () => {
      setOpen(false);
    },
  });

  const retard = todoActionsCount.data?.bulkCount.pastMe || 0;
  const aujourdhui = todoActionsCount.data?.bulkCount.todayMe || 0;

  useEffect(() => {
    if (!todoActionsCount.loading && retard === 0 && aujourdhui === 0) {
      setOpen(false);
    }
  }, [todoActionsCount.loading]);

  const handleClose = () => {
    localStorage.setItem('viewedLastDate', dateNow.toISOString());
    setOpen(false);
  };

  const gotoTodo = () => {
    handleClose();
    history.push('/todo?assignee=me&type=ACTIVE&sorting=%7B"priorite"%3A%7B"order"%3A"desc"%7D%7D&date=today');
  };

  if (todoActionsCount.loading) {
    return null;
  }

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      className={classes.rootDialog}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      disableBackdropClick
    >
      <DialogContent>
        <Box display="flex" position="relative" flexDirection="column" alignItems="center">
          <Box className={classes.iconContentButton}>
            <IconButton onClick={handleClose}>
              <Close />
            </IconButton>
          </Box>
          <Box display="flex" alignItems="flex-start" justifyContent="center" className={classes.alertModalImages}>
            <img src={AlertImage} />
          </Box>
          <Box>
            <Typography className={classes.alertModalTitle}>Bonjour {user.userName || '!'}</Typography>
            <Typography color="textSecondary">
              TODO vous donne un aperçu des actions de votre journée. Suivez et gérez en toute simplicité le déroulement
              de vos projets.
            </Typography>
            <List component="nav" aria-label="main mailbox folders">
              <ListItem>
                <FiberManualRecordIcon fontSize="small" color="disabled" />
                <Typography color="textSecondary">
                  {aujourdhui === 1 ? 'Action de la journée' : 'Actions de la journée'} :
                </Typography>
                <Typography className={classes.label} color="secondary">
                  {aujourdhui}
                </Typography>
              </ListItem>
              <ListItem>
                <FiberManualRecordIcon fontSize="small" color="disabled" />
                <Typography color="textSecondary">
                  {retard === 1 ? 'Action en retard' : 'Actions en retard'} :
                </Typography>
                <Typography className={classes.label} color="secondary">
                  {retard}
                </Typography>
              </ListItem>
            </List>
          </Box>
        </Box>
      </DialogContent>

      <DialogActions className={classes.alertModalButton}>
        <CustomButton color="secondary" onClick={gotoTodo}>
          Acceder à to do
        </CustomButton>
      </DialogActions>
    </Dialog>
  );
};

export default TodoAlert;
