import { useReactiveVar } from '@apollo/client';
import { todoBulkCountVar, useApplicationContext } from '@lib/common';
import { Badge, IconButton } from '@material-ui/core';
import { Assignment } from '@material-ui/icons';
import React, { FC } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useHistory } from 'react-router';
import { fetchTodoTotalCount } from '../../util/total-count';
import { useStyles } from './styles';

const TodoBannerButton: FC<{}> = () => {
  const classes = useStyles({});
  const history = useHistory();
  const { user, graphql, federation } = useApplicationContext();
  const [totalCount, setTotalCount] = useState<number>(0);

  const todoBulkCount = useReactiveVar(todoBulkCountVar);

  useEffect(() => {
    setTotalCount((todoBulkCount.pastMe || 0) + (todoBulkCount.todayMe || 0));
  }, [todoBulkCount]);

  useEffect(() => {
    fetchTodoTotalCount({ user, graphql, federation }).then(setTotalCount);
  }, []);
  return (
    <IconButton
      className={classes.iconBtn}
      onClick={() =>
        history.push(`/todo?assignee=me&type=ACTIVE&sorting=%7B"priorite"%3A%7B"order"%3A"desc"%7D%7D&date=today`)
      }
    >
      <Badge badgeContent={totalCount} max={999} color="error">
        <Assignment />
      </Badge>
    </IconButton>
  );
};

export default TodoBannerButton;
