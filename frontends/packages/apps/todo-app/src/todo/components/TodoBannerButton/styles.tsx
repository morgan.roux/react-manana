import { makeStyles, Theme, createStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    iconBtn: {
      color: theme.palette.common.white,
      opacity: 1,
      maxHeight: 50,
    },
  })
);
