import React from 'react';
import { SmallLoading } from '@lib/ui-kit';
import loadable from '@loadable/component';
import Todo from './assets/img/todo.png';
import { ModuleDefinition } from '@lib/common';
import { fetchTodoTotalCount } from './util/total-count';
import TodoBannerButton from './components/TodoBannerButton/TodoBannerButton';
import { Add, AddCircleRounded, Assignment, ListAlt } from '@material-ui/icons';
import AddTodoActionModal, { openAddTodoActionMobile } from './pages/NewTodo/add-todo-action-mobile';
import TodoAlert from './alert/TodoAlert';

const TodoPage = loadable(() => import('./pages/NewTodo/NewTodo'), {
  fallback: <SmallLoading />,
});

const todoModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_TODO',
      location: 'ACCUEIL',
      name: 'To-Do',
      to: '/todo?assignee=me&type=ACTIVE&sorting=%7B"priorite"%3A%7B"order"%3A"desc"%7D%7D&date=today',
      icon: Todo,
      activationParameters: {
        groupement: '0207',
        pharmacie: '0702',
      },
      preferredOrder: 130,
      options: {
        totalCount: fetchTodoTotalCount,
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_FAVORITE_TODO',
      location: 'FAVORITE',
      to: '/todo?assignee=me&type=ACTIVE&sorting=%7B"priorite"%3A%7B"order"%3A"desc"%7D%7D&date=today',
      name: 'To-Do',
      icon: <Assignment />,
      component: (
        <Add
          onClick={(event) => {
            event.preventDefault();
            event.stopPropagation();
            openAddTodoActionMobile(true);
          }}
        />
      ),
      preferredOrder: 20,
      activationParameters: {
        groupement: '0207',
        pharmacie: '0702',
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_TODO_BANNIERE',
      location: 'BANNIERE',
      preferredOrder: 10,
      activationParameters: {
        groupement: '0207',
        pharmacie: '0702',
      },
      component: <TodoBannerButton />,
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
    {
      id: 'FEATURE_MOBILE_QUICK_ACCESS_TODO',
      location: 'MOBILE_QUICK_ACCESS',
      name: 'Ma To-Do',
      icon: <Assignment />,
      to: '/todo?assignee=me&type=ACTIVE&sorting=%7B"priorite"%3A%7B"order"%3A"desc"%7D%7D&date=today',
      options: {
        active: (path) => path.startsWith('/todo'),
      },
      preferredOrder: 20,
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
    {
      id: 'FEATURE_MOBILE_QUICK_ADD_TODO',
      location: 'MOBILE_QUICK_ACCESS',
      name: 'Tâche',
      icon: <AddCircleRounded />,
      preferredOrder: 30,
      options: {
        onClick: () => openAddTodoActionMobile(true),
      },
      component: <AddTodoActionModal />,
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
    {
      id: 'FEATURE_ALERT_TODO',
      location: 'ALERT',
      preferredOrder: 30,
      options: {
        openAlert: (options, onClose) => {
          return <TodoAlert open={true} setOpen={() => onClose()} />;
        },
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
  routes: [
    {
      path: ['/todo'], ///:filter?/:filterId?/:task?/:taskId?
      component: TodoPage,
      exact: true,
      attachTo: 'MAIN',
    },
  ],
};

export default todoModule;
