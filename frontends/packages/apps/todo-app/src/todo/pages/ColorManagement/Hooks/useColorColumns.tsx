import { Box } from '@material-ui/core';
import moment from 'moment';
import React, { Dispatch, SetStateAction, useState, useMemo, Fragment } from 'react';
import { COLORS_URL } from '../../../../Constant/url';
import TableActionColumn from '../../TableActionColumn';
import { useDeleteColor } from './useDeleteColor';

export const useColorColumns = (setOpenDeleteDialog: Dispatch<SetStateAction<boolean>>) => {
  const [selected, setSelected] = useState<any[]>([]);
  const [deleteRow, setDeleteRow] = useState<any>(null);

  const { deleteColors, loading } = useDeleteColor(
    deleteRow ? [deleteRow] : selected,
    setSelected,
    deleteRow,
    setDeleteRow,
  );

  /**
   * Set null deleteRow if selected length > 0
   */
  useMemo(() => {
    if (selected && selected.length > 0) {
      if (deleteRow !== null) setDeleteRow(null);
    }
  }, [selected]);

  const onClickDelete = (row: any) => {
    setDeleteRow(row);
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (selected.length > 0) {
      deleteColors();
    }

    if (deleteRow && deleteRow.id) {
      deleteColors();
    }
  };

  const columns = [
    {
      label: 'Code couleur',
      name: 'code',
      renderer: (row: any) => {
        return row.code ? (
          <Box display="flex" alignItems="center">
            <div
              style={{
                width: 44,
                height: 44,
                borderRadius: 4,
                background: row.code,
                marginRight: 10,
              }}
            />
            {row.code}
          </Box>
        ) : (
          '-'
        );
      },
    },
    {
      label: 'Nom de la couleur',
      name: 'libelle',
    },
    {
      label: 'Date de création',
      name: 'dateCreation',
      renderer: (row: any) => {
        return row.dateCreation ? moment(row.dateCreation).format('DD/MM/YYYY') : '-';
      },
    },
    {
      label: 'Date de modification',
      name: 'dateModification',
      renderer: (row: any) => {
        return row.dateModification ? moment(row.dateModification).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: any) => {
        return (
          <TableActionColumn
            row={row}
            baseUrl={COLORS_URL}
            setOpenDeleteDialog={setOpenDeleteDialog}
            handleClickDelete={onClickDelete}
          />
        );
      },
    },
  ];

  return { columns, deleteRow, onClickConfirmDelete, loading, selected, setSelected };
};
