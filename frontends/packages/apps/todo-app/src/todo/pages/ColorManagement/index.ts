import ColorManagement from './ColorManagement';
import ColorManagementDashboard from './ColorManagementDashboard';

export { ColorManagement, ColorManagementDashboard };
