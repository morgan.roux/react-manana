import { useState, ChangeEvent } from 'react';

export interface ColorFormInterface {
  id?: string;
  code: string;
  libelle: string;
  isRemoved?: boolean;
}

export const initialState: ColorFormInterface = {
  id: '',
  code: '',
  libelle: '',
  isRemoved: false,
};

const useColorForm = (defaultState?: ColorFormInterface) => {
  const initValues: ColorFormInterface = defaultState || initialState;
  const [values, setValues] = useState<ColorFormInterface>(initValues);

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  const handleChangeColor = (color: any) => {
    setValues(prevState => ({ ...prevState, code: (color && color.hex) || '' }));
  };

  return {
    handleChange,
    handleChangeColor,
    values,
    setValues,
  };
};

export default useColorForm;
