import React from 'react';
import { DO_SEARCH_COLOR } from '../../../graphql/Color';
import WithSearch from '../../Common/newWithSearch/withSearch';
import ColorManagement from './ColorManagement';

const ColorManagementDashboard = () => {
  // const groupement = getGroupement();
  const must = [{ term: { isRemoved: false } }];
  return (
    <WithSearch
      type="couleur"
      WrappedComponent={ColorManagement}
      searchQuery={DO_SEARCH_COLOR}
      optionalMust={must}
    />
  );
};

export default ColorManagementDashboard;
