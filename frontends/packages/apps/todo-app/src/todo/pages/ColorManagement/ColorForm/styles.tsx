import { Theme, createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    colorFormRoot: {
      width: '100%',
      marginTop: '40px !important',
    },
    colorPickerContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    colorPicker: {
      width: '288px !important',
      height: '310px !important',
      border: '1px solid #F1F1F1 !important',
      boxShadow: 'none !important',
      marginBottom: '19px !important',
      '& > div': {
        height: '25px !important',
      },
    },
  })
);

export default useStyles;
