import moment from 'moment';
import { TodoURLParams } from '../useTodoURLParams';
import { Filter } from '../util/types';

export const buildDateFilter = (params: TodoURLParams, includePast: boolean = false): Filter => {
  if (params.date === 'past') {
    return {
      must: [{ range: { dateDebut: { lt: 'now/d' } } }, { exists: { field: 'dateDebut' } }],
    };
  } else if (params.date === 'today') {
    if (includePast) {
      return {
        must: [{ range: { dateDebut: { lt: 'now+1d/d' } } }, { exists: { field: 'dateDebut' } }],
      };
    }

    return {
      must: [
        { range: { dateDebut: { gte: 'now/d' } } },
        { range: { dateDebut: { lt: 'now+1d/d' } } },
        { exists: { field: 'dateDebut' } },
      ],
    };
  } else if (params.date === 'next') {
    return {
      must: [{ range: { dateDebut: { gt: 'now/d' } } }, { exists: { field: 'dateDebut' } }],
    };
  } else if (params.date) {
    const startTime = `${params.date}T00:00:00.000Z`;
    const endTime = `${params.date}T23:59:59.059Z`;

    if (params.type?.includes('DONE')) {
      return {
        must: [
          {
            bool: {
              must: [
                {
                  range: {
                    updatedAt: {
                      gte: startTime,
                      lte: endTime,
                    },
                  },
                },
              ],
            },
          },
        ],
      };
    }

    return {
      must: [
        {
          range: {
            dateDebut: {
              gte: startTime,
              lte: endTime,
            },
          },
        },
      ],
    };

    /* const should = [
      {
        bool: {
          must: [
            {
              range: {
                dateDebut: {
                  gte: startTime,
                },
              },
            },
            {
              range: {
                dateFin: {
                  lte: endTime,
                },
              },
            },
          ],
        },
      },

      {
        bool: {
          must: [
            {
              range: {
                dateDebut: {
                  gte: startTime,
                  lte: endTime,
                },
              },
            },
          ],
        },
      },
    ];

    return {
      should,
    };*/
  }

  return {};
};
