import { nl2br, strippedString, useApplicationContext, useDisplayNotification } from '@lib/common';
import { SearchTodoActionInfoFragment, useUpdate_Item_Scoring_For_ItemLazyQuery } from '@lib/common/src/federation';
import { useSoft_Delete_ActionsMutation } from '@lib/common/src/graphql';
import { Autocomplete, CustomModal, LoaderSmall } from '@lib/ui-kit';
import { Box, Typography, List, ListItem, ListItemText, Paper, IconButton } from '@material-ui/core';
import { debounce } from 'lodash';
import React, { FC, ReactNode, useRef, useState } from 'react';
import { useEffect } from 'react';
import { useTodoURLParams } from '../../hooks/useTodoURLParams';
import { useQueries } from '../../MainContent/useQueries';
import AjoutTaches from '../../Modals/AjoutTache';
import TaskDetails from '../../TaskDetails';
//import TaskDetails from '../../Task/TaskDetails';
import { useStyles } from './style';

interface SearchContentProps {
  placeholder: string;
  refetchAll?: () => void;
  updateActionStatus: (todoAction: SearchTodoActionInfoFragment, newStatut: string) => void;
  popupIcon?: ReactNode;
}

const SearchContent: FC<SearchContentProps> = ({ placeholder, refetchAll, updateActionStatus, popupIcon }) => {
  const classes = useStyles({});
  const { graphql, federation, isMobile } = useApplicationContext();

  const displayNotification = useDisplayNotification();

  const { lazyLoadActions, lazyLoadingActions } = useQueries();

  const { getActionsQuery, params, redirectTo } = useTodoURLParams();

  const [openModalTache, setOpenModalTache] = useState<boolean>(false);

  const [open, setOpen] = React.useState<boolean>(false);
  const [clickedTask, setClickedTask] = React.useState<any | null>(null);
  const [searchText, setSearchText] = useState<string>('');

  const firstType = params.type?.length ? params.type[0] : undefined;
  const firstAssignee = params.assignee?.length ? params.assignee[0] : undefined;
  useEffect(() => {
    debouncedSearchUser.current(searchText, params);
  }, [searchText, params.type && params.type[0], params.assignee && params.assignee[0]]);

  useEffect(() => {
    if (params.searchText) setSearchText(params.searchText || '');
  }, [params.searchText]);

  const debouncedSearchUser = useRef(
    debounce((text: string, params: any) => {
      if (text && text.length > 0) {
        lazyLoadActions({
          variables: {
            index: 'todoaction',
            skip: 0,
            take: 10,
            query: getActionsQuery(
              {
                assignee: params.assignee,
                date: undefined,
                type: params.type,
                projet: undefined,
                urgence: undefined,
                importance: undefined,
                sorting: [{ description: { order: 'asc' } }],
                searchText: text && text.length ? text : undefined,
              },
              { enableSort: true, includePast: true }
            ),
          },
        });
      }
      //setSearchText(text);
    }, 1500)
  );

  const handleChangeAutocomplete = (name: string) => (e: React.ChangeEvent<any>, value: any) => {
    setSearchText(value);
  };

  const data = [...(lazyLoadingActions.data?.search?.data || []), { showAll: true }];

  const handleChange = (value: any) => {
    if (value) {
      setSearchText(strippedString(value.description));
      setClickedTask(value);
      if (isMobile) {
        setOpenModalTache(true);
      } else {
        setOpen(true);
      }
    }
  };

  const [updateItemScoring] = useUpdate_Item_Scoring_For_ItemLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });

  // Mutation delete tasks
  const [doSoftDeleteActions] = useSoft_Delete_ActionsMutation({
    client: graphql,
    onCompleted: (data) => {
      if (data && data.softDeleteActions) {
        updateItemScoring({
          variables: {
            idItemAssocie: data.softDeleteActions[0].id,
            codeItem: 'L',
          },
        });
        displayNotification({
          type: 'success',
          message: 'Suppression effectuée avec succès !!!',
        });
      }
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const handleRefetchAll = () => {
    if (refetchAll) refetchAll();
    setOpen(false);
  };

  const handleShowAll = () => {
    redirectTo({
      ...params,
      projet: undefined,
      urgence: undefined,
      importance: undefined,
      date: undefined,
      searchText,
    });
  };

  return (
    <>
      <Box className={classes.search}>
        <Autocomplete
          inputValue={searchText}
          loading={lazyLoadingActions.loading}
          name="todo"
          multiple={false}
          variant="outlined"
          handleChange={() => () => {}}
          handleInputChange={handleChangeAutocomplete}
          options={data}
          groupBy={(option: any) => option.showAll}
          handleEnterKeyPressed={handleShowAll}
          renderGroup={(params: any) => {
            return !params.group ? (
              <List>
                {(lazyLoadingActions.data?.search?.data || []).map((option: any, index: any) => (
                  <ListItem button={true} key={`option-${index}`} onClick={() => handleChange(option)}>
                    <ListItemText primary={strippedString(option.description || '')} />
                  </ListItem>
                ))}
                {searchText && (
                  <ListItem button={true} onClick={handleShowAll}>
                    <Typography
                      color="secondary"
                      style={{
                        width: '100%',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        height: 40,
                        textDecoration: 'underline',
                      }}
                    >
                      Afficher tout
                    </Typography>
                  </ListItem>
                )}
              </List>
            ) : (
              <></>
            );
          }}
          optionLabel="description"
          placeholder={placeholder}
          style={{ width: '100%' }}
          noOptionsText={'Aucune tâche correspondant'}
          popupIcon={popupIcon}
          popupIndicatorOpen={classes.popupIndicatorOpen}
          clearOnEscape={false}
          clearOnBlur={false}
        />
      </Box>
      <AjoutTaches
        openModalTache={openModalTache}
        setOpenTache={setOpenModalTache}
        task={clickedTask}
        operationName="EDIT"
        refetchAll={refetchAll}
      />
      <CustomModal
        open={open}
        setOpen={setOpen}
        title="Détail de la tâche"
        withBtnsActions={false}
        closeIcon={true}
        headerWithBgColor={true}
        maxWidth="md"
        fullWidth={true}
        className={classes.taskDetailsModal}
      >
        {clickedTask && clickedTask.id && (
          <TaskDetails
            updateActionStatus={updateActionStatus}
            task={clickedTask}
            doSoftDeleteActions={doSoftDeleteActions}
            refetchAll={handleRefetchAll}
          />
        )}
      </CustomModal>
    </>
  );
};

export default SearchContent;
