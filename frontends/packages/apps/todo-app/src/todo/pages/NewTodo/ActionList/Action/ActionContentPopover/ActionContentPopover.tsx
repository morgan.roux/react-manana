import { Box, Popover } from '@material-ui/core';
import React, { useState } from 'react';
import moment from 'moment';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { CustomDatePickerHiddenInput } from '@lib/ui-kit';
import { SearchTodoActionInfoFragment } from '@lib/common/src/federation';

interface TaskContentPopoverProps {
  open: boolean;
  id: string | undefined;
  anchorEl: any | null;
  section: string | undefined;
  action: SearchTodoActionInfoFragment;
  setOpen: (value: boolean) => void;
  refetchAll?: any;
}

const TaskContentPopover: React.FC<TaskContentPopoverProps> = ({ id, open, anchorEl, section, action }) => {
  const [values, setValues] = useState<SearchTodoActionInfoFragment>(action);

  const onDateChange = (date: MaterialUiPickersDate, value?: string | null | undefined) => {
    setValues((prevState: any) => ({ ...prevState, dateDebut: moment(date).format() }));
  };

  return (
    <Popover id={id} open={open} anchorEl={anchorEl}>
      {section === 'DATE' && (
        <Box>
          <CustomDatePickerHiddenInput
            //open={openDatePicker}
            name="dateDebut"
            variant="static"
            onChange={onDateChange}
            value={values.dateDebut}
            autoOk={true}
            TextFieldComponent={() => null}
            autoFocus={true}
          />
        </Box>
      )}
    </Popover>
  );
};

export default TaskContentPopover;
