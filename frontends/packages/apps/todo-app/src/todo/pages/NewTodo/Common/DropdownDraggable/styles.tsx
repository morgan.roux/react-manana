import { px2rem } from '@lib/common';
import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      '& .MuiAccordionSummary-root': {
        flexDirection: 'row-reverse',
        '&.Mui-expanded': {
          minHeight: 45,
        },
        '& .MuiAccordionSummary-expandIcon': {
          padding: 0,
          marginRight: 10,
        },
      },
      '& .MuiAccordionSummary-content': {
        justifyContent: 'space-between',
        margin: 0,
        '&.Mui-expanded': {
          margin: '11px 0',
        },
      },
      '& .MuiAccordion-root': {
        boxShadow: 'none!important',
        '&.Mui-expanded': {
          margin: '0',
        },
      },
      '& .MuiAccordionDetails-root': {
        padding: '0px!important',
        '& .MuiList-root': {
          width: '100%',
          paddingTop: 0,
          paddingBottom: 0,
          '& .MuiListItem-button': {
            paddingRight: 0,
            paddingTop: 0,
            paddingBottom: 0,
            '& .MuiIconButton-root': {
              padding: 10,
            },
          },
        },
      },
    },
    icon: {},
    count: {
      marginLeft: 10,
      textAlign: 'center',
      background: '#E0E0E0',
      height: 20,
      minWidth: 20,
      borderRadius: 2,
      fontSize: 10,
      color: '#212121',
      lineHeight: '20px',
    },
    type: {
      marginLeft: 10,
      textAlign: 'center',
      background: '#E0E0E0',
      height: 20,
      minWidth: 20,
      borderRadius: 2,
      fontSize: 10,
      color: '#212121',
      lineHeight: '20px',
      padding: '0 5px',
    },
    title: {
      color: '#212121',
      fontFamily: 'Roboto',
      marginLeft: 10,
      fontSize: '0.875rem',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      '-webkit-line-clamp': 1,
      '-webkit-box-orient': 'vertical',
      maxWidth: 280,
    },

    subProjectTitle: {
      color: '#212121',
      fontFamily: 'Roboto',
      marginLeft: 10,
      fontSize: '0.875rem',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      '-webkit-line-clamp': 1,
      '-webkit-box-orient': 'vertical',
      maxWidth: 280,
    },
    heading: {
      fontSize: px2rem(15, theme),
      fontWeight: 'normal',
      display: 'flex',
      alignItems: 'center',
    },
    placeholder: {
      fontSize: 'small',
      color: '#9E9E9E',
    },
    addButton: {
      fontSize: 'small',
      position: 'absolute',
      right: 0,
      top: 0,
    },
    subDropdown: {
      '& .MuiAccordionSummary-content': {
        margin: 0,
      },
    },
  })
);

export default useStyles;
