import { QueryResult } from '@apollo/client';
import { useApplicationContext } from '@lib/common';
import {
  SearchTodoProjetInfoFragment,
  Search_Todo_ProjetQuery,
  Search_Todo_ProjetQueryVariables,
  useGet_UrgencesQuery,
  useImportancesQuery,
} from '@lib/common/src/federation';
import { useNiveauMatriceFonctions } from '@lib/hooks';
import { ConfirmDialog } from '@lib/ui-kit';
import { DateRange, FiberManualRecord, SupervisedUserCircle, Today } from '@material-ui/icons';
import _, { filter } from 'lodash';
import React, { useState } from 'react';
import DropdownDraggable from '../../Common/DropdownDraggable/';
import ImportanceFilter from '../../Common/ImportanceFilter/ImportanceFilter';
import { ImportanceInterface } from '../../Common/ImportanceFilter/ImportanceFilterList';
import UrgenceFilter from '../../Common/UrgenceFilter';
import { UrgenceInterface } from '../../Common/UrgenceFilter/UrgenceFilter';
//import UserFilter from '../../Common/UserFilter/UserFilter';
import { useUpdateProject } from '../../Common/utils/useProject';
import AjoutParticipant from '../../Modals/AjoutParticipants';
import FavorisHeader from './FavorisHeader';
import { filterSubProjects } from './../../util';
import ActionButton from './SiderActionButton';
import { useProjectList } from './utils/useProject';
import { CountTodo } from '../../util/types';
import { useTodoURLParams } from '../../hooks/useTodoURLParams';

interface SidebarProps {
  counting: CountTodo;
  setShowMainContent: (value: boolean) => void;
  loadingProjets: QueryResult<Search_Todo_ProjetQuery, Search_Todo_ProjetQueryVariables>;
}

const Sider: React.FC<SidebarProps> = ({ counting, setShowMainContent, loadingProjets }) => {
  const { user, federation } = useApplicationContext();
  const { params, redirectTo } = useTodoURLParams();

  const loadingImportances = useImportancesQuery({ client: federation });
  const loadingUrgences = useGet_UrgencesQuery({ client: federation });

  const urgences = React.useMemo(() => {
    if (loadingUrgences.data?.urgences.nodes) {
      return loadingUrgences.data.urgences.nodes.map((urgence) => ({
        ...urgence,
        checked: !!params.urgence?.includes(urgence.id),
      }));
    }

    return [];
  }, [params.urgence, loadingUrgences.data?.urgences.nodes]);

  const importances = React.useMemo(() => {
    if (loadingImportances.data?.importances.nodes) {
      return loadingImportances.data.importances.nodes.map((importance) => ({
        ...importance,
        checked: !!params.importance?.includes(importance.id),
      }));
    }

    return [];
  }, [params.importance, loadingImportances.data?.importances.nodes]);

  const handleChangeImportances = (newImportances: ImportanceInterface[] | null | undefined) => {
    if (newImportances) {
      const checkedIdImportances = newImportances
        .filter((importance) => importance.checked && importance.id)
        .map(({ id }) => id);

      if (checkedIdImportances.length) {
        redirectTo({
          importance: checkedIdImportances as any,
          searchText: undefined,
        });

        return;
      }
    }

    redirectTo({ importance: undefined, searchText: undefined });
    setShowMainContent(true);
  };

  const handleChangeUrgences = (newUrgences: UrgenceInterface[] | null | undefined) => {
    if (newUrgences) {
      const checkedIdUrgences = newUrgences.filter((urgence) => urgence.checked && urgence.id).map(({ id }) => id);

      if (checkedIdUrgences.length) {
        redirectTo({
          urgence: checkedIdUrgences as any,
          searchText: undefined,
        });
        return;
      }
    }
    redirectTo({ urgence: undefined, searchText: undefined });
    setShowMainContent(true);
  };

  const handleProjectClick = (project: any) => {
    if (project.id) {
      redirectTo({
        date: undefined,
        // assignee: ['me'],
        assignee: undefined,
        projet: [project.id],
      });
      setShowMainContent(true);
    }
  };

  const handleSubProjectClick = (subProjects: any) => {
    if (subProjects.id) {
      redirectTo({
        date: undefined,
        // assignee: ['me'],
        assignee: undefined,
        projet: [subProjects.id],
        searchText: undefined,
      });
      setShowMainContent(true);
    }
  };

  // state of modals (open or not)
  const [openAddToFavorite, setOpenAddToFavorite] = useState<boolean>(false);
  const [openShare, setOpenShare] = useState<boolean>(false);

  const [selectedUsersIds, setselectedUsersIds] = useState<string[]>([]);
  // Modals state handler
  const addToFavoriteclick = () => {
    setOpenAddToFavorite(!openAddToFavorite);
  };

  const [clickedRow, setClickedRow] = useState<any>();

  const [switchProjectFavs, switchProjectFavsLoading] = useUpdateProject(setOpenAddToFavorite, clickedRow);

  const switchFavs = (clickedRow: any) => {
    if (clickedRow && clickedRow.type === 'project') {
      return switchProjectFavs(clickedRow);
    }
  };

  const [projectMenuItems] = useProjectList(addToFavoriteclick, clickedRow);

  const niveauMatriceFonctions = useNiveauMatriceFonctions();

  const assigneeList = [
    {
      icon: <Today />,
      title: 'Ma To-Do',
      count: counting.loading ? 0 : counting.totalMe || 0,
      loading: counting.loading,
      path: `ma-to-do`,
      isActive: !!params.assignee?.includes('me'),
      onClick: () => {
        redirectTo({ assignee: ['me'], projet: undefined, date: 'today', searchText: undefined });
        setShowMainContent(true);
      },
    },
    {
      icon: <DateRange />,
      title: 'Todo Equipe',
      count: counting.loading ? 0 : counting.totalTeam || 0,
      loading: counting.loading,
      path: `todo-equipe`,
      isActive: !!params.assignee?.includes('team'),
      onClick: () => {
        redirectTo({ assignee: ['team'], projet: undefined, date: 'today', searchText: undefined });
        setShowMainContent(true);
      },
    },
  ];

  const projectResult: SearchTodoProjetInfoFragment[] = (loadingProjets?.data?.search?.data ||
    []) as SearchTodoProjetInfoFragment[];
  const filteredProject = filter(projectResult, (project) => !project.archive);

  const projectParents = filteredProject; //_.filter(filteredProject || [], (project) => project.type === null);
  const projectParentIds = projectParents.map((parent) => parent.id);

  let listFilterProjects = (
    niveauMatriceFonctions === 1
      ? [...projectParents]
      : [
          ...projectParents,
          ...filteredProject.filter((project) => project.parent?.id && !projectParentIds.includes(project.parent.id)),
        ]
  ).sort((p1, p2) => (p1.ordre || 0) - (p2.ordre || 0));

  if (niveauMatriceFonctions === 1) {
    listFilterProjects = listFilterProjects.sort((p1, p2) => p2.type.localeCompare(p1.type));
  }

  //  const allFavoris = concat(filter(filteredProject, (project) => project.inFavorisIdUsers.includes(user.id)));

  /*
  const favorisDropDown = {
    title: 'Favoris',
    element:
      allFavoris &&
      allFavoris.map((favoris) => ({
        id: favoris && favoris.id,
        actionButton: <ActionButton row={favoris} menuItems={favorisMenuItems} setClickedRow={setClickedRow} />,
        title: favoris && favoris.nom,
        typeName: favoris && favoris.__typename,
        count: 0, //favoris && favoris.stats,
        type: '',
        icon:
          favoris && favoris.partage ? (
            <SupervisedUserCircle htmlColor={(favoris && favoris.couleur && favoris.couleur.code) || '#A3B1B0'} />
          ) : !favoris.type ? (
            <LocalOffer htmlColor={(favoris && favoris.couleur && favoris.couleur.code) || '#DF0A6E'} />
          ) : (
            <FiberManualRecord htmlColor={(favoris && favoris.couleur && favoris.couleur.code) || '#A3B1B0'} />
          ),
        subProjects: filterSubProjects(projectResult, favoris, user, true)
          .sort((p1, p2) => (p1.ordre || 0) - (p2.ordre || 0))
          .map((subProject) => ({
            id: subProject.id,
            typeName: 'TodoProjet',
            actionButton: <ActionButton row={subProject} menuItems={projectMenuItems} setClickedRow={setClickedRow} />,
            title: subProject.nom,
            count: 0, //subProject && subProject.nbAction,
            type: '',
            isActive: false, // filterId === subProject.id,
            icon: subProject.partage ? (
              <SupervisedUserCircle
                htmlColor={(subProject && subProject.couleur && subProject.couleur.code) || '#A3B1B0'}
              />
            ) : (
              <FiberManualRecord
                htmlColor={(subProject && subProject.couleur && subProject.couleur.code) || '#A3B1B0'}
              />
            ),
          })),
      })),
    withAddAction: false,
  };
*/
  const projectDropDown = React.useMemo(
    () => ({
      title: `Fonctions`,
      element: listFilterProjects.map((project) => {
        const userStats = project.stats.find((statUser) => statUser.user.id === user.id);
        const activeStatusStats = userStats?.countsByStatus
          ? params.type
            ? userStats?.countsByStatus.filter((s) => params.type?.includes(s.status as any))
            : userStats?.countsByStatus
          : [];

        const count = activeStatusStats.reduce((total, currentStatusStats) => {
          return total + currentStatusStats.todoCount + currentStatusStats.teamCount;
        }, 0);

        return {
          id: project.id,
          typeName: 'TodoProjet',
          //   actionButton: <ActionButton row={project} menuItems={projectMenuItems} setClickedRow={setClickedRow} />,
          title: project.nom,
          count,
          type: '',
          isActive: !!params.projet?.includes(project.id),
          icon: project.partage ? (
            <SupervisedUserCircle htmlColor={project.couleur?.code || '#A3B1B0'} />
          ) : (
            <FiberManualRecord htmlColor={project.couleur?.code || '#A3B1B0'} />
          ),
          subProjects:
            niveauMatriceFonctions === 1
              ? []
              : filterSubProjects(projectResult, project, user, true)
                  .sort((p1, p2) => (p1.ordre || 0) - (p2.ordre || 0))
                  .map((subProject) => ({
                    id: subProject.id,
                    typeName: 'TodoProjet',
                    actionButton: (
                      <ActionButton row={subProject} menuItems={projectMenuItems} setClickedRow={setClickedRow} />
                    ),
                    title: subProject.nom,
                    count: 0, //subProject.nbAction,
                    type: '',
                    isActive: false, // filterId === subProject.id,
                    icon: subProject.partage ? (
                      <SupervisedUserCircle htmlColor={subProject.couleur?.code || '#A3B1B0'} />
                    ) : (
                      <FiberManualRecord htmlColor={subProject.couleur?.code || '#A3B1B0'} />
                    ),
                  })),
        };
      }),
      addAction: () => {},
      withAddAction: false,
    }),
    [listFilterProjects, params.projet]
  );

  return (
    <>
      <FavorisHeader headerList={assigneeList} setShowMainContent={setShowMainContent} />
      {/*<DropdownDraggable
        list={favorisDropDown}
        clickable={false}
        niveauMatriceFonctions={niveauMatriceFonctions}
        defaultExpanded={true}
        handleProjectClick={handleProjectClick}
        handleSubProjectClick={handleSubProjectClick}
        setShowMainContent={setShowMainContent}
      />*/}

      <DropdownDraggable
        disabled={niveauMatriceFonctions === 2}
        niveauMatriceFonctions={niveauMatriceFonctions}
        list={projectDropDown}
        handleProjectClick={handleProjectClick}
        handleSubProjectClick={handleSubProjectClick}
        clickable={false}
        setShowMainContent={setShowMainContent}
      />

      {/* <UserFilter
        withDropdown={true}
        value={params.assignee || []}
        onChange={(newValue) => {
          redirectTo({
            assignee: newValue.length === 0 ? ['me'] : newValue.map((user) => user.id),
          });
        }}
      />*/}
      <UrgenceFilter withDropdown={true} urgence={urgences} setUrgence={handleChangeUrgences} />
      <ImportanceFilter importances={importances} setImportances={handleChangeImportances} />

      <ConfirmDialog
        open={openAddToFavorite}
        message={
          clickedRow && clickedRow.isInFavoris
            ? 'Êtes-vous sûr de vouloir retirer des favoris ?'
            : 'Êtes-vous sûr de vouloir ajouter au favoris ?'
        }
        handleValidate={() => switchFavs(clickedRow)}
        handleClose={addToFavoriteclick}
        loading={switchProjectFavsLoading}
      />
      <AjoutParticipant
        openModalParticipant={openShare}
        setOpenParticipant={setOpenShare}
        userIds={selectedUsersIds}
        setUserIds={setselectedUsersIds}
        idProjectTask={(clickedRow && clickedRow.id) || ''}
      />
    </>
  );
};
export default Sider;
