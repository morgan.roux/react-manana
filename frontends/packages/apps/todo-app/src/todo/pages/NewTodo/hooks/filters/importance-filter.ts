import { MeUserInfoFragment } from '@lib/common/src/federation';
import { Filter } from '../util/types';
import { TodoURLParams } from '../useTodoURLParams';

export const buildImportanceFilter = (user: MeUserInfoFragment, params: TodoURLParams): Filter => {
  if (params.importance) {
    return {
      must: [
        {
          terms: {
            'importance.id': params.importance,
          },
        },
      ],
    };
  }

  return {};
};
