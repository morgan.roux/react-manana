import React, { useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import {
  Toolbar,
  AppBar,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  Typography,
} from '@material-ui/core';
import { Theme, createStyles, makeStyles } from '@material-ui/core';
import { NewCustomButton } from '@lib/ui-kit';

interface Menuprops {
  title?: string;
  handleChange: (e: React.ChangeEvent<any>) => void;
}
const useStyles = makeStyles((theme: Theme) => createStyles({}));
const Modals: React.FC<Menuprops & RouteComponentProps> = (props) => {
  const { title, handleChange } = props;
  const [open, setOpen] = useState(true);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div>
      <Dialog open={open} onClose={handleClose} aria-labelledby="draggable-dialog-title">
        <AppBar position="static">
          <Toolbar>
            <Typography variant="h6">{title}</Typography>
            <Button color="inherit" onClick={handleClose}>
              x
            </Button>
          </Toolbar>
        </AppBar>
        <DialogContent>
          <DialogContentText>{props.children}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <NewCustomButton onClick={handleChange}>Ajouter</NewCustomButton>
          <NewCustomButton theme="transparent" onClick={handleClose}>
            Annuler
          </NewCustomButton>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default withRouter(Modals);
