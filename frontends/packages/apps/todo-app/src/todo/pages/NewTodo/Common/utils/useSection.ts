import { useApolloClient, useMutation } from '@apollo/client';
import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useCreate_Update_Todo_SectionMutation, useDelete_Todo_SectionMutation } from '@lib/common/src/graphql';
import { ChangeEvent, useState } from 'react';

export const useCreateUpdateSection = (
  values: any,
  refetch: any,
  filter: string | undefined,
  filterId: string | undefined,
  setValues: (value: string) => void,
  setOpenAddSection: (value: boolean) => void
): [any] => {
  const isOnProject = filterId;
  const { user, graphql } = useApplicationContext();
  const displayNotification = useDisplayNotification();
  const isInbox = filter === 'recu';
  const isInboxTeam = filter === 'recu-equipe';

  const defaultVariable = {
    libelle: values && values.libelle,
    idUser: user.id,
  };

  const variables = isOnProject
    ? {
        idProject: filterId,
        ...defaultVariable,
      }
    : isInbox
    ? {
        isInInbox: true,
        ...defaultVariable,
      }
    : isInboxTeam
    ? {
        isInInboxTeam: true,
        ...defaultVariable,
      }
    : defaultVariable;

  const [doCreateUpdateSection] = useCreate_Update_Todo_SectionMutation({
    client: graphql,
    variables: {
      input: { id: values.id, ...variables },
    },
    onCompleted: (data) => {
      if (refetch) refetch();
      displayNotification({
        type: 'success',
        message: 'Section ajoutée avec succès',
      });
      setValues('');
      setOpenAddSection(false);
    },
  });
  return [doCreateUpdateSection];
};

export const useSection = () => {
  const [values, setValues] = useState<any>({});
  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | any>) => {
    const { name, value } = e.target;
    setValues((prevState: any) => ({
      ...prevState,
      [name]: value,
    }));
  };
  return [values, setValues, handleChange];
};

export const useDeleteSection = (id: string, refetch: any, setOpenDeleteDialog: (open: boolean) => void) => {
  const displayNotification = useDisplayNotification();
  const { graphql } = useApplicationContext();
  const [doDeleteTodoSection] = useDelete_Todo_SectionMutation({
    client: graphql,
    variables: {
      ids: [id],
    },
    onCompleted: (data) => {
      if (refetch) refetch();
      displayNotification({
        type: 'success',
        message: 'Section supprimée avec succès',
      });
      setOpenDeleteDialog(false);
    },
  });
  return [doDeleteTodoSection];
};
