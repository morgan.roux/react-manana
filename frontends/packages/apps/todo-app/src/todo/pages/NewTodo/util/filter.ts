const commonTerms = [
  {
    term: {
      supprime: false,
    },
  },
];

export const computeOverdueQuery = (idUser: string, status?: string) => {
  const mustStatus = status
    ? [
        {
          term: {
            status,
          },
        },
      ]
    : [];

  return {
    bool: {
      must: [
        ...commonTerms,
        ...mustStatus,
        { range: { dateDebut: { gt: 'now/d' } } },
        {
          term: {
            todoIdUsers: idUser,
          },
        },
      ],
    },
  };
};

export const computeTodayQuery = (idUser: string, status?: string) => {
  const mustStatus = status
    ? [
        {
          term: {
            status,
          },
        },
      ]
    : [];

  return {
    bool: {
      must: [
        ...commonTerms,
        ...mustStatus,

        { range: { dateDebut: { gt: 'now-1d/d', lt: 'now+1d/d' } } },
        {
          term: {
            todoIdUsers: idUser,
          },
        },
      ],
    },
  };
};

export const computeNextQuery = (idUser: string, status?: string) => {
  const mustStatus = status
    ? [
        {
          term: {
            status,
          },
        },
      ]
    : [];

  return {
    bool: {
      must: [
        ...commonTerms,
        ...mustStatus,

        { range: { dateDebut: { gte: 'now+1d/d' } } },
        {
          term: {
            todoIdUsers: idUser,
          },
        },
      ],
    },
  };
};

export const computeTeamTodayQuery = (idUser: string, status?: string) => {
  const mustStatus = status
    ? [
        {
          term: {
            status,
          },
        },
      ]
    : [];

  return {
    bool: {
      must: [
        ...commonTerms,
        ...mustStatus,

        { range: { dateDebut: { lt: 'now+1d/d' } } },
        {
          term: {
            teamIdUsers: idUser,
          },
        },
      ],
    },
  };
};

export const computeTeamNextQuery = (idUser: string, status?: string) => {
  const mustStatus = status
    ? [
        {
          term: {
            status,
          },
        },
      ]
    : [];

  return {
    bool: {
      must: [
        ...commonTerms,
        ...mustStatus,

        { range: { dateDebut: { gte: 'now+1d/d' } } },
        {
          term: {
            teamIdUsers: idUser,
          },
        },
      ],
    },
  };
};

export const computeTodoActionsQuery = (
  idUser: string,
  status?: 'active' | 'done',
  category?: 'ma-todo' | 'todo-team'
) => {
  let should = undefined;
  let minimum_should_match = undefined;
  let must: any[] = [...commonTerms];

  if (!category) {
    should = [
      {
        term: {
          teamIdUsers: idUser,
        },
      },
      {
        term: {
          todoIdUsers: idUser,
        },
      },
    ];
    minimum_should_match = 1;
  } else if (category === 'ma-todo') {
    must = [
      ...must,
      {
        term: {
          todoIdUsers: idUser,
        },
      },
    ];
  } else {
    must = [
      ...must,
      {
        term: {
          teamIdUsers: idUser,
        },
      },
    ];
  }

  if (status) {
    must = [
      ...must,
      {
        term: {
          status: status.toUpperCase(),
        },
      },
    ];
  }

  return {
    bool: {
      should,
      minimum_should_match,
      must,
    },
  };
};
