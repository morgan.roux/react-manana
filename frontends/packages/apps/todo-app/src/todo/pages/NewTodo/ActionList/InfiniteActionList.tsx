import { QueryResult } from '@apollo/client';
import { useApplicationContext } from '@lib/common';
import {
  SearchTodoActionInfoFragment,
  Search_Todo_ActionQuery,
  Search_Todo_ActionQueryVariables,
  useSearch_Todo_ActionQuery,
} from '@lib/common/src/federation';
import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
// You can import any component you want as a named export from 'react-virtualized', eg
import { AutoSizer, CellMeasurer, CellMeasurerCache, Index, IndexRange, InfiniteLoader, List } from 'react-virtualized';
import 'react-virtualized/styles.css';
import { useTodoURLParams } from '../hooks/useTodoURLParams';
import useStyles from './ styles';
import { Action } from './Action/InfiniteAction';

const cache = new CellMeasurerCache({
  defaultWidth: 500,
  minWidth: 75,
  fixedHeight: false,
});

interface InfiniteActionListProps {
  loadingActions: QueryResult<Search_Todo_ActionQuery, Search_Todo_ActionQueryVariables>;
  onRequestEditTodoAction: (todoAction: SearchTodoActionInfoFragment) => void;
  onRequestOpenDeleteDialog: (data: SearchTodoActionInfoFragment, title?: string) => void;
  onRequestOpenParticipantModal: (todoAction: SearchTodoActionInfoFragment) => void;
  onRequestChangeStatutTodoAction: (todoAction: SearchTodoActionInfoFragment, newStatut: string) => void;
}

const InfiniteActionList: React.FC<InfiniteActionListProps> = ({
  loadingActions,
  onRequestEditTodoAction,
  onRequestOpenDeleteDialog,
  onRequestOpenParticipantModal,
  onRequestChangeStatutTodoAction,
}) => {
  const classes = useStyles();

  let listRef = React.useRef();

  useEffect(() => {
    if (loadingActions.data?.search?.data?.length) {
      resizeAll();
    }
  }, [loadingActions.data?.search?.data]);

  const resizeAll = () => {
    cache.clearAll();
    if (listRef.current) {
      (listRef.current as any).recomputeRowHeights();
    }
  };

  const loadMoreItems = ({ startIndex, stopIndex }: IndexRange) => {
    return loadingActions.fetchMore({
      variables: {
        ...loadingActions.variables,
        skip: startIndex,
        take: stopIndex - startIndex + 1,
      },

      updateQuery: (prev: any, { fetchMoreResult }) => {
        if (
          prev &&
          prev.search &&
          prev.search.data &&
          fetchMoreResult &&
          fetchMoreResult.search &&
          fetchMoreResult.search.data
        ) {
          const { data: currentData } = prev.search;
          return {
            ...prev,
            search: {
              ...prev.search,
              data: [...currentData, ...fetchMoreResult.search.data],
              total: fetchMoreResult.search.total,
            },
          };
        }
        return prev;
      },
    });
  };

  const isItemLoaded = ({ index }: Index): boolean => index < (loadingActions.data?.search?.data || []).length;
  const totalCount = loadingActions.data?.search?.total || 0;

  if (!cache) {
    return null;
  }

  return (
    <InfiniteLoader isRowLoaded={isItemLoaded} rowCount={totalCount} loadMoreRows={loadMoreItems as any}>
      {({ onRowsRendered, registerChild }) => (
        <AutoSizer className={classes.autoSizer}>
          {({ width, height }) => {
            return (
              <List
                deferredMeasurementCache={cache}
                ref={(ref) => {
                  listRef = ref as any;
                  registerChild(ref);
                }}
                className={classes.list}
                onRowsRendered={onRowsRendered}
                width={width}
                //  style={{ marginLeft: GUTTER_SIZE }}
                height={height}
                rowHeight={cache.rowHeight}
                rowCount={totalCount}
                rowRenderer={({ index, key, parent, style }) => {
                  return (
                    <CellMeasurer cache={cache} columnIndex={0} key={key} rowIndex={index} parent={parent}>
                      {({ registerChild: registerChildCell }) => {
                        const data = (loadingActions.data?.search?.data || []) as SearchTodoActionInfoFragment[];
                        const loaded = index < data.length;

                        return (
                          <Action
                            ref={registerChildCell as any}
                            style={{
                              ...(style || {}),
                              //  top: parseInt(style.top?.toString() as any, 10) + index * GUTTER_SIZE,
                            }}
                            data={loaded ? data[index] : undefined}
                            loading={!loaded}
                            onRequestEditTodoAction={onRequestEditTodoAction}
                            onRequestOpenDeleteDialog={onRequestOpenDeleteDialog}
                            onRequestOpenParticipantModal={onRequestOpenParticipantModal}
                            onRequestChangeStatutTodoAction={onRequestChangeStatutTodoAction}
                          />
                        );
                      }}
                    </CellMeasurer>
                  );
                }}
              />
            );
          }}
        </AutoSizer>
      )}
    </InfiniteLoader>
  );
};

export default InfiniteActionList;
