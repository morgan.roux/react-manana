import { QueryResult } from '@apollo/client';
import { CustomSelectUser, MobileTopBar, useApplicationContext, InfiniteItemList } from '@lib/common';
import {
  Aggregate_Date_Histogram_By_DayQueryVariables,
  SearchTodoActionInfoFragment,
  Search_Todo_ActionQuery,
  Search_Todo_ActionQueryVariables,
  Search_Todo_ProjetQuery,
  Search_Todo_ProjetQueryVariables,
  Aggregate_Date_Histogram_By_DayQuery,
} from '@lib/common/src/federation';
import { ActionStatus, useProjectQuery } from '@lib/common/src/graphql';
import { ConfirmDeleteDialog } from '@lib/ui-kit';
import { Box } from '@material-ui/core';
import React, { useState } from 'react';
import { useEffect } from 'react';
import ActionButton from '../Common/ActionButton';
import { useTodoURLParams } from '../hooks/useTodoURLParams';
import MainHeader from '../MainHeader/MainHeader';
import AjoutTaches from '../Modals/AjoutTache';
import { CountTodo } from '../util/types';
import useStyles from './styles';
import { useMutations } from './useMutations';
import { Action } from './../ActionList/Action/InfiniteAction';
import InfiniteActionList from '../ActionList/InfiniteActionList';

interface MainContentProps {
  counting: CountTodo;
  window?: () => Window;
  parameters: any[];
  filters: any;
  refetchCounts: () => void;
  handleDrawerToggle: () => void;
  loadingActions: QueryResult<Search_Todo_ActionQuery, Search_Todo_ActionQueryVariables>;
  loadingProjets: QueryResult<Search_Todo_ProjetQuery, Search_Todo_ProjetQueryVariables>;
  setShowMainContent: (open: boolean) => void;

  refetchAll: () => void;
  loadAggregateDataHistogramByDay: (variables: Aggregate_Date_Histogram_By_DayQueryVariables) => void;
  loadingAggregateDataHistogramByDay: QueryResult<
    Aggregate_Date_Histogram_By_DayQuery,
    Aggregate_Date_Histogram_By_DayQueryVariables
  >;
}

export const defaultDateTerm = {
  range: { dateDebut: { gt: 'now/d' } },
};

export const operationVariable = {
  edit: 'EDIT',
  addSubs: 'ADD_SUBS',
  addUpper: 'ADD_UPPER',
  addDown: 'ADD_DOWN',
};

const MainContent: React.FC<MainContentProps> = (props) => {
  const classes = useStyles('auto')();

  const {
    handleDrawerToggle,
    refetchCounts,
    loadingActions,
    loadingProjets,
    counting,
    setShowMainContent,
    refetchAll,
    loadAggregateDataHistogramByDay,
    loadingAggregateDataHistogramByDay,
  } = props;
  const { params } = useTodoURLParams();
  const { graphql, isMobile } = useApplicationContext();
  const [todoAction, setTodoAction] = useState<SearchTodoActionInfoFragment>();

  const [openParticipantModal, setOpenParticipantModal] = useState<boolean>(false);

  const [openModalTache, setOpenModalTache] = useState<boolean>(false);
  const defaultDeleteDialogStates = { open: false, title: '', onRequestDelete: () => {} };
  const [deleteDialogStates, setDeleteDialogStates] = useState(defaultDeleteDialogStates);

  const resetDeleteDialogStates = () => {
    setDeleteDialogStates(defaultDeleteDialogStates);
  };

  const { deleteTodoAction, deletingTodoAction, updateTodoActionParticipants, updateActionStatus } = useMutations({
    refetch: refetchAll,
    todoAction,
    resetDeleteDialogStates,
    setTodoAction,
  });

  const loadAggregateDataHistogramByDayFunc = (variables: Aggregate_Date_Histogram_By_DayQueryVariables): void => {
    loadAggregateDataHistogramByDay(variables);
  };

  const handleSetOpenModalTache = (dateDebut: Date) => {
    setTodoAction((prev) => ({ dateDebut } as any));
    setOpenModalTache(true);
  };

  const getProject = useProjectQuery({
    client: graphql,
    variables: { id: (params.projet && params.projet[0]) || '' },
    skip: !params.projet || !params.projet?.length,
  });

  const projectInfo = getProject && getProject.data && getProject.data.project;

  const title = params.searchText
    ? 'Recherche'
    : params.projet
    ? projectInfo?._name || ''
    : params.assignee?.includes('me')
    ? 'Mes Tâches'
    : params.assignee?.includes('team')
    ? "Tâches de l'Equipe"
    : '';

  const optionBtn = <ActionButton actionMenu="FILTER_MENU" />;

  const handleClickBack = () => {
    setShowMainContent(false);
  };

  const handleRequestEditTodoAction = (todoAction: SearchTodoActionInfoFragment) => {
    setTodoAction(todoAction as any);
    setOpenModalTache(true);
  };

  const handleRequestParticipantModal = (todoAction: SearchTodoActionInfoFragment) => {
    setTodoAction(todoAction);
    setOpenParticipantModal(true);
  };

  const handleRequestOpenDeleteDialog = (data: SearchTodoActionInfoFragment, title?: string) => {
    setDeleteDialogStates({
      title: title || '',
      open: true,
      onRequestDelete: () =>
        deleteTodoAction({
          variables: {
            ids: [data.id],
          },
        }),
    });
  };
  const handleUpdateTodoActionParticipants = (newParticipants: any) => {
    const newAssignUsers = newParticipants.filter(
      ({ id }: any) => !(todoAction?.participants || []).some((assignment: any) => assignment.id === id)
    );
    if (newAssignUsers.length > 0 && todoAction) {
      updateTodoActionParticipants({
        variables: {
          input: {
            idAction: todoAction.id,
            idUserDestinations: newAssignUsers.map(({ id }: any) => id),
          },
        },
      });
    }
  };

  useEffect(() => {
    if (loadingActions.data?.search?.data && todoAction) {
      const newTodoAction = loadingActions.data?.search?.data.find((el: any) => el.id === todoAction.id);
      newTodoAction && setTodoAction(newTodoAction as SearchTodoActionInfoFragment);
    }
  }, [loadingActions.data?.search?.data]);

  return (
    <Box className={classes.root}>
      <MobileTopBar
        title={title}
        withBackBtn={true}
        handleDrawerToggle={handleDrawerToggle}
        optionBtn={[optionBtn]}
        onClickBack={handleClickBack}
        breakpoint="md"
      />
      <main
        className={classes.content}
        style={{ marginTop: isMobile ? -25 : undefined /**FIXME : Remove top blank space on mobile */ }}
      >
        <MainHeader
          loadingActions={loadingActions}
          counting={counting}
          title={title}
          todoProjet={projectInfo}
          handleSetOpenModalTache={handleSetOpenModalTache}
          participants={projectInfo && projectInfo.participants ? projectInfo.participants.length : 0}
          loadAggregateDataHistogramByDay={loadAggregateDataHistogramByDayFunc}
          loadingAggregateDataHistogramByDay={loadingAggregateDataHistogramByDay}
        />
        <Box flexGrow={1}>
          {/*<InfiniteItemList<SearchTodoActionInfoFragment, Search_Todo_ActionQuery, Search_Todo_ActionQueryVariables>
            queryResult={loadingActions}
            itemRenderer={({ registerChild, style, data, loading }) => {
              return (
                <Action
                  ref={registerChild}
                  style={style}
                  data={data}
                  loading={loading}
                  onRequestEditTodoAction={handleRequestEditTodoAction}
                  onRequestOpenDeleteDialog={handleRequestOpenDeleteDialog}
                  onRequestOpenParticipantModal={handleRequestParticipantModal}
                  onRequestChangeStatutTodoAction={(action, statut) => {
                    updateActionStatus({
                      variables: {
                        input: {
                          idAction: action.id,
                          status: statut === 'DONE' ? ActionStatus.Done : ActionStatus.Active,
                        },
                      },
                    });
                  }}
                />
              );
            }}
          />*/}

          <InfiniteActionList
            loadingActions={loadingActions}
            onRequestEditTodoAction={handleRequestEditTodoAction}
            onRequestOpenDeleteDialog={handleRequestOpenDeleteDialog}
            onRequestOpenParticipantModal={handleRequestParticipantModal}
            onRequestChangeStatutTodoAction={(action, statut) => {
              updateActionStatus({
                variables: {
                  input: {
                    idAction: action.id,
                    status: statut === 'DONE' ? ActionStatus.Done : ActionStatus.Active,
                  },
                },
              });
            }}
          />
        </Box>
      </main>
      <CustomSelectUser
        openModal={openParticipantModal}
        setOpenModal={() => {
          setOpenParticipantModal(false);
          setTodoAction(undefined);
        }}
        idParticipants={(todoAction?.projet?.participants || []).map(({ id }: any) => id)}
        selected={todoAction?.participants || []}
        setSelected={(participants: any) => handleUpdateTodoActionParticipants(participants)}
      />
      <AjoutTaches
        operationName={todoAction?.id ? 'EDIT' : ''}
        task={todoAction}
        setTodoAction={setTodoAction}
        openModalTache={openModalTache}
        setOpenTache={setOpenModalTache}
        refetchAll={refetchAll}
      />
      <ConfirmDeleteDialog
        open={deleteDialogStates.open}
        setOpen={resetDeleteDialogStates}
        title="Suppression d'une tâche"
        onClickConfirm={deleteDialogStates.onRequestDelete}
        isLoading={deletingTodoAction.loading}
      />
    </Box>
  );
};

export default MainContent;
