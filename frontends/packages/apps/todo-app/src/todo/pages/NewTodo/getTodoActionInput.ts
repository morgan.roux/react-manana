import { SearchTodoActionInfoFragment } from '@lib/common/src/federation';
import { ActionInput, ActionStatus } from '@lib/common/src/graphql';
import moment from 'moment';

export const getTodoActionInput = (task: SearchTodoActionInfoFragment): ActionInput => {
  const idsUsersDestinations = (task.participants && task.participants.map((i) => i.id)) || [];
  const dateDebut = task.dateDebut ? moment.utc(task.dateDebut).startOf('day') : null;
  const dateFin = task.dateFin ? moment.utc(task.dateFin).startOf('day') : null;
  const isPrivate = task.isPrivate ? true : false;

  const idProject = task.idProjet || '';

  return {
    id: task.id,
    description: task.description,
    idProject: idProject ? idProject : undefined,
    dateDebut,
    dateFin,
    isPrivate,
    //commentaire: {
    //   id: (task.firstComment && task.firstComment.id) || '',
    //   content: (task.firstComment && task.firstComment.content) || '',
    // },
    idActionParent: task.idParent || '',
    status: (task.status as ActionStatus) || ActionStatus.Active,
    idItemAssocie: task.idItemAssocie || task.id,
    idsUsersDestinations,
    ordre: task.ordre,
    isRemoved: task.supprime,
    idImportance: task.importance?.id,
  };
};
