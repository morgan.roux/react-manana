import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useCreate_ProjectMutation } from '@lib/common/src/graphql';

export const useUpdateProject = (setOpenAddToFavorite: (value: boolean) => void, row: any): [any, boolean] => {
  const { graphql, user: currentUser, currentPharmacie } = useApplicationContext();
  const displayNotification = useDisplayNotification();
  const [createUpdateProject, { loading }] = useCreate_ProjectMutation({
    client: graphql,
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
    onCompleted: (data) => {
      if (data && data.createUpdateProject) {
        displayNotification({
          type: 'success',
          message: 'Opération réussie',
        });
      }
      setOpenAddToFavorite(false);
    },
  });

  const projectInputVariables = {
    id: row && row.id,
    name: row && row._name,
    typeProject: row && row.typeProject,
    idUser: currentUser.id,
    idPharmacie: currentPharmacie.id,
  };

  const switchFavs = (row: any) => {
    createUpdateProject({
      variables: {
        input: {
          ...projectInputVariables,
          isFavoris: !(row && row.isInFavoris),
        },
      },
    });
  };
  return [switchFavs, loading];
};
