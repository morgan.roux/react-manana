import { useMutation } from '@apollo/client';
import { useApplicationContext, useDisplayNotification } from '@lib/common';
import { useUpdate_Item_Scoring_For_UserLazyQuery } from '@lib/common/src/federation';
import {
  Create_Update_ActionMutation,
  useAssign_Task_To_UsersMutation,
  useCreate_Update_ActionMutation,
  useDelete_ActionsMutation,
  useUpdate_Action_StatusMutation,
} from '@lib/common/src/graphql';

import { DO_UPDATE_TODO_ACTION_PARTICIPANTS } from '@lib/common/src/federation/basis/todo-action/mutation';
export const useMutations = (props: any) => {
  const { refetch, refetchCountTodos, todoAction, resetDeleteDialogStates, setTodoAction } = props;

  const { deleteTodoAction, deletingTodoAction } = useDeleteTodoAction({
    todoAction,
    refetch,
    refetchCountTodos,
    resetDeleteDialogStates,
  });

  const { updateTodoActionParticipants, updatingTodoActionParticipants } = useUpdateTodoActionParticipants({
    todoAction,
    refetch,
    refetchCountTodos,
    resetDeleteDialogStates,
  });

  const { updateActionStatus, updatingActionStatus } = useUpdateTodoActionStatut({
    refetch,
  });

  return {
    deleteTodoAction,
    deletingTodoAction,
    updateTodoActionParticipants,
    updatingTodoActionParticipants,
    updateActionStatus,
    updatingActionStatus,
  };
};

export const useDeleteTodoAction = (props: any) => {
  const displayNotification = useDisplayNotification();
  const { graphql } = useApplicationContext();
  const { todoAction, refetch, refetchCountTodos, resetDeleteDialogStates } = props;
  const [deleteTodoAction, deletingTodoAction] = useDelete_ActionsMutation({
    client: graphql,
    variables: { ids: [todoAction?.id] },
    onCompleted: (data) => {
      displayNotification({
        type: 'success',
        message: 'Suppression effectuée avec succès',
      });
      refetch && refetch();
      refetchCountTodos && refetchCountTodos();
      resetDeleteDialogStates && resetDeleteDialogStates();
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  return { deleteTodoAction, deletingTodoAction };
};

export const useUpdateTodoActionParticipants = (props: any) => {
  const displayNotification = useDisplayNotification();
  const { federation, user } = useApplicationContext();
  const { setTodoAction, refetch, refetchCountTodos } = props;
  const { updateItemScoringByUser } = useUpdateItemScoringByUser();
  const [updateTodoActionParticipants, updatingTodoActionParticipants] = useMutation(
    DO_UPDATE_TODO_ACTION_PARTICIPANTS,
    {
      client: federation,
      onCompleted: (data) => {
        updateItemScoringByUser({
          variables: {
            idUser: user.id,
            codeItem: 'L',
          },
        });
        displayNotification({
          type: 'success',
          message: 'Tâche assignée avec succès',
        });
        setTodoAction(undefined);
        refetch && refetch();
        refetchCountTodos && refetchCountTodos();
      },
      onError: (error) => {
        error.graphQLErrors.map((err) => {
          displayNotification({ type: 'error', message: err.message });
        });
      },
    }
  );
  // const [updateTodoActionParticipant, updatingTodoActionParticipant] = useAssign_Task_To_UsersMutation({
  //   client: graphql,
  //   onCompleted: (data) => {
  //     updateItemScoringByUser({
  //       variables: {
  //         idUser: user.id,
  //         codeItem: 'L',
  //       },
  //     });
  //     displayNotification({
  //       type: 'success',
  //       message: 'Tâche assignée avec succès',
  //     });
  //     setTodoAction(undefined);
  //     refetch && refetch();
  //     refetchCountTodos && refetchCountTodos();
  //   },
  //   onError: (error) => {
  //     error.graphQLErrors.map((err) => {
  //       displayNotification({ type: 'error', message: err.message });
  //     });
  //   },
  // });
  return { updateTodoActionParticipants, updatingTodoActionParticipants };
};

const useUpdateItemScoringByUser = () => {
  const { federation } = useApplicationContext();
  const [updateItemScoringByUser, updatingItemScoringByUser] = useUpdate_Item_Scoring_For_UserLazyQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
  });
  return { updateItemScoringByUser, updatingItemScoringByUser };
};

export const useUpdateTodoActionStatut = (props: any) => {
  const { graphql, notify } = useApplicationContext();
  const { refetch } = props;
  const [updateActionStatus, updatingActionStatus] = useUpdate_Action_StatusMutation({
    client: graphql,
    onCompleted: (data) => {
      refetch && refetch();
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });
  return { updateActionStatus, updatingActionStatus };
};

export const useUpsertTodoAction = (props: any) => {
  const { refetch, onCompleted } = props;
  const { graphql, notify } = useApplicationContext();

  const onCompletedUpsert = (data: Create_Update_ActionMutation) => {
    refetch && refetch();
  };

  const [upsertTodoAction, upsertingTodoAction] = useCreate_Update_ActionMutation({
    client: graphql,
    onCompleted: onCompleted || onCompletedUpsert,
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        notify({ type: 'error', message: err.message });
      });
    },
  });

  return { upsertTodoAction, upsertingTodoAction };
};
