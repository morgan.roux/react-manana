import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    priorityRoot: {
      zIndex: 2000,
      padding: 16,
      '& > .MuiFormControl-root': {
        marginBottom: 0,
        minWidth: 150,
      },
    },
    etiquetteRoot: {
      padding: 16,
      '& > .MuiFormControl-root': {
        marginBottom: 0,
        minWidth: 150,
      },
    },
  })
);

export default useStyles;
