import ImportanceInput from '@lib/common/src/components/ImportanceInput';
import { SearchTodoActionInfoFragment } from '@lib/common/src/federation';
import { Box, IconButton, Popover, Typography } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import React, { FC } from 'react';
import { isManuelAction } from '@lib/common';
import useStyles from './styles';

interface ReportProps {
  todoAction?: SearchTodoActionInfoFragment;
  open: boolean;
  setOpen: (open: boolean) => void;
  onRequestEdit: (importance: any) => void;
}

const ImportancePoppup: FC<ReportProps> = ({ open, setOpen, todoAction, onRequestEdit }) => {
  const classes = useStyles();
  const isGeneratedFromTA = !isManuelAction(todoAction);

  return (
    <Popover open={open}>
      <Box className={classes.root}>
        <Box display="flex" justifyContent="space-between" marginBottom="16px" alignItems="center">
          <Typography>Modifier l'importance d'une tâche</Typography>
          <IconButton onClick={() => setOpen(false)}>
            <Close />
          </IconButton>
        </Box>
        {isGeneratedFromTA && (
          <Box marginBottom="16px">
            <Typography>Vous ne pouvez pas modifier cette tâche qui a été générée</Typography>
          </Box>
        )}
        <Box className={classes.dateContainer}>
          <ImportanceInput
            disabled={isGeneratedFromTA}
            onChange={onRequestEdit}
            name="idImportance"
            value={todoAction?.importance?.id}
          />
        </Box>
      </Box>
    </Popover>
  );
};

export default ImportancePoppup;
