import React, { FC } from 'react';
import useStyles from './styles';
import { Popover, Typography, Box, IconButton } from '@material-ui/core';
import { CustomDatePicker } from '@lib/ui-kit';
import { Close } from '@material-ui/icons';
import { SearchTodoActionInfoFragment } from '@lib/common/src/federation';
import { isManuelAction } from '@lib/common';

interface ReportProps {
  todoAction?: SearchTodoActionInfoFragment;
  open: boolean;
  setOpen: (open: boolean) => void;
  onRequestReport: (values: any) => void;
}

const Report: FC<ReportProps> = ({ open, setOpen, todoAction, onRequestReport }) => {
  const classes = useStyles();
  const isGeneratedFromTA = !isManuelAction(todoAction);

  return (
    <Popover open={open}>
      <Box className={classes.root}>
        <Box display="flex" justifyContent="space-between" marginBottom="16px" alignItems="center">
          <Typography>Reporter une tâche</Typography>
          <IconButton onClick={() => setOpen(false)}>
            <Close />
          </IconButton>
        </Box>
        {isGeneratedFromTA && (
          <Box marginBottom="16px">
            <Typography>Vous ne pouvez pas modifier cette tâche qui a été générée</Typography>
          </Box>
        )}
        <Box className={classes.dateContainer}>
          <CustomDatePicker
            onChange={(date) => onRequestReport({ dateDebut: date })}
            value={todoAction?.dateDebut}
            disabled={isGeneratedFromTA}
            label="Date Début"
          />
          <CustomDatePicker
            onChange={(date) => onRequestReport({ dateFin: date })}
            value={todoAction?.dateFin}
            disabled={isGeneratedFromTA}
            label="Date Fin"
          />
        </Box>
      </Box>
    </Popover>
  );
};

export default Report;
