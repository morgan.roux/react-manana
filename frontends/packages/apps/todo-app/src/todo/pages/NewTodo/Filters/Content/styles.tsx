import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    checkBoxLeftName: {
      '& .MuiCheckbox-root': {
        padding: 0,
        color: '#000000',
        marginRight: 8,
      },
      '&$checked': {
        color: '#000000',
      },
    },

    nbrProduits: {
      padding: '2px 6px',
      fontSize: 10,
      background: '#E0E0E0',
      borderRadius: 2,
      height: 20,
      minWidth: 20,
      textAlign: 'center',
      [theme.breakpoints.down('md')]: {
        marginLeft: 8,
        marginRight: 16,
      },
    },
    name: {
      fontFamily: 'Roboto',
      fontSize: '14px',
      color: '#1D1D1D',
    },
    noStyle: {
      listStyleType: 'none',
      // '& .MuiListItem-root': {
      //   padding: '4px 0',
      //   display: 'flex',
      //   justifyContent: 'space-between',
      //   alignItems: 'center',
      // },
      '& .MuiRadio-root': {
        padding: '0 8px 0 0!important',
      },
      // [theme.breakpoints.down('md')]: {
      //   display: 'flex',
      //   flexDirection: 'row',
      //   overflowX: 'auto',
      //   paddingLeft: 16,
      //   flexWrap: 'wrap',
      //   '& .MuiListItem-root': {
      //     maxWidth: '50%',
      //   },
      // },
      display: 'flex',
      flexDirection: 'row',
      overflowX: 'auto',
      flexWrap: 'wrap',
      '& .MuiListItem-root': {
        maxWidth: '50%',
        justifyContent: 'space-between',
      },
      '& .MuiListItem-gutters': {
        paddingRight: 0,
      },
    },
    nom: {
      fontFamily: 'Roboto',
      fontSize: '14px',
      color: '#1D1D1D',
    },
    voirButton: {
      background: '#002444',
      textTransform: 'none',
      color: '#FFFFFF',
      height: '24px',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
    },
    searchBox: {
      maxHeight: '200px',
      overflowY: 'scroll',
      marginTop: '16px',
    },
    expandBtnContainer: {
      marginRight: 0,
    },
  })
);

export default useStyles;
