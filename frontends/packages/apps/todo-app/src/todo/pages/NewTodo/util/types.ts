export interface CountTodo {
  loading?: boolean;
  pastMe?: number;
  todayMe?: number;
  nextMe?: number;
  pastTeam?: number;
  todayTeam?: number;
  nextTeam?: number;
  active?: number;
  activeTeam?: number;
  done?: number;
  doneTeam?: number;
  totalMe?: number;
  totalTeam?: number;
}
export interface URLParams {
  status?: 'active' | 'done';
  category?: 'ma-todo' | 'team-todo';
}
