import { MeUserInfoFragment, SearchTodoProjetInfoFragment } from '@lib/common/src/federation';

export const filterSubProjects = (
  tousProjets: SearchTodoProjetInfoFragment[],
  projet: SearchTodoProjetInfoFragment,
  user: MeUserInfoFragment,
  applyFilterParticipant: boolean
): SearchTodoProjetInfoFragment[] => {
  return (projet.sousProjets || [])
    .map(({ id }) => {
      const sousProjet = findProject(tousProjets, id);
      const active =
        sousProjet &&
        !sousProjet.archive &&
        (!applyFilterParticipant ||
          (applyFilterParticipant ? sousProjet.participants.some((participant) => participant.id === user.id) : true));

      return (active ? sousProjet : null) as any;
    })
    .filter((sp) => !!sp);
};

export const findProject = (
  projects: SearchTodoProjetInfoFragment[],
  idProjet: string
): SearchTodoProjetInfoFragment | undefined => {
  for (const projet of projects) {
    if (projet.id === idProjet) {
      return projet;
    } else if (projet.sousProjets) {
      const sousProjetIds = projet.sousProjets.map(({ id }) => id);

      const subProject = findProject(
        projects.filter(({ id }) => sousProjetIds.includes(id)),
        idProjet
      );
      if (subProject) {
        return subProject;
      }
    }
  }

  return undefined;
};

export const extractProjectIds = (
  projects: SearchTodoProjetInfoFragment[],
  project: SearchTodoProjetInfoFragment,
  user: MeUserInfoFragment
): string[] => {
  if (!project) {
    return [];
  }
  const subProjects = filterSubProjects(projects, project, user, true);
  if (subProjects) {
    return [
      project.id,
      ...subProjects.reduce(
        (acc: string[], currentProject: SearchTodoProjetInfoFragment) => [
          ...acc,
          ...extractProjectIds(projects, currentProject, user),
        ],
        []
      ),
    ];
  }
  return [project.id];
};

export const extractPersoProject = (
  projects: SearchTodoProjetInfoFragment[],
  project: SearchTodoProjetInfoFragment,
  user: MeUserInfoFragment
) => {
  if (project?.type === 'PERSONNEL') {
    return project;
  } else if (project) {
    const subProjects = filterSubProjects(projects, project, user, true);
    if (subProjects.length === 1 && subProjects[0].type === 'PERSONNEL') {
      return subProjects[0];
    }
  }
  return undefined;
};

export const hiddenTypeProject = (codeFilter: string, parameters: any) => {
  let codePersonnel: string;

  switch (codeFilter) {
    case 'PERSONNEL':
      codePersonnel = '0118';
      break;
    case 'GROUPEMENT':
      codePersonnel = '0119';
      break;
  }

  const parameter = parameters.find((parameter: any) => parameter?.code === codePersonnel);

  return !('true' === parameter?.value?.value ? true : 'true' === parameter?.defaultValue);
};
