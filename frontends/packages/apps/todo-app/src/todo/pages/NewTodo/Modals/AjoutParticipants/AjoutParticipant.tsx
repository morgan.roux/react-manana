import {
  ADMINISTRATEUR_GROUPEMENT,
  SUPER_ADMINISTRATEUR,
  useApplicationContext,
  useDisplayNotification,
} from '@lib/common';
import { UserSelect } from '@lib/common/src/components/User/UserSelect';
import {
  Search_PartcipantDocument,
  Search_PartcipantQuery,
  useAdd_ParticipantMutation,
  useProjectLazyQuery,
} from '@lib/common/src/graphql';
import { CustomModal } from '@lib/ui-kit';
import React, { FC, useState } from 'react';
import { useStyles } from './styles';

interface UsersModalProps {
  openModalParticipant: boolean;
  setOpenParticipant: (opne: boolean) => any;
  userIds?: string[];
  setUserIds?: (value: any) => void;
  idProjectTask?: string;
  ajouteTache?: boolean;
  createUpdateActionEquipe?: (value: any) => void;
}

const AjoutParticipant: FC<UsersModalProps> = ({
  openModalParticipant,
  setOpenParticipant,
  userIds,
  setUserIds,
  idProjectTask,
  ajouteTache,
  createUpdateActionEquipe,
}) => {
  const title = `Ajout des personnes`;
  const classes = useStyles({});
  const displayNotification = useDisplayNotification();

  const [userIdsSelected, setUserIdsSelected] = useState<any[]>(userIds && userIds.length ? userIds : []);

  const { graphql, user: currentUser, isGroupePharmacie } = useApplicationContext();

  // mutation add participant
  // const {
  //   content: { variables },
  // } = useContext<ContentStateInterface>(ContentContext);

  const [getProject, resultProject] = useProjectLazyQuery({ client: graphql });

  React.useEffect(() => {
    if (idProjectTask && openModalParticipant) {
      getProject({
        variables: {
          id: idProjectTask,
        },
      });
    }
  }, [idProjectTask, openModalParticipant]);

  React.useEffect(() => {
    if (
      resultProject &&
      resultProject.data &&
      resultProject.data.project &&
      resultProject.data.project.participants &&
      resultProject.data.project.participants.length
    ) {
      setUserIdsSelected(resultProject.data.project.participants.map((participant) => participant.id) || []);
    } else {
      setUserIdsSelected(userIds || []);
    }
  }, [resultProject, userIds]);

  const [addParticipant] = useAdd_ParticipantMutation({
    client: graphql,
    update: (cache, { data }) => {
      if (
        data &&
        data.addParticipantsToProject &&
        data.addParticipantsToProject.participants &&
        data.addParticipantsToProject.participants.length > 0
      ) {
        // TODO
        // const req = cache.readQuery<Search_PartcipantQuery, any>({
        //   query: Search_PartcipantDocument,
        //   variables,
        // });
        // if (req && req.search && req.search.data) {
        //   const source = req.search.data || [];
        //   const result = data.addParticipantsToProject;
        //   const concat = [...source, result];
        //   cache.writeQuery({
        //     query: Search_PartcipantDocument,
        //     data: {
        //       search: {
        //         ...req.search,
        //         ...{
        //           data: concat,
        //         },
        //       },
        //     },
        //     variables,
        //   });
        // }
      }
    },
    onCompleted: (data) => {
      displayNotification({
        type: 'success',
        message: 'Participant ajouté avec succès',
      });
    },
    onError: (error) => {
      error.graphQLErrors.map((err) => {
        displayNotification({ type: 'error', message: err.message });
      });
    },
  });

  const handleConfirm = () => {
    //ajout tache + participant
    if (ajouteTache) {
      createUpdateActionEquipe && createUpdateActionEquipe(userIdsSelected);
      setOpenParticipant(!openModalParticipant);
    }

    // addparticipant selected
    if (idProjectTask) {
      addParticipant({
        variables: { input: { idProject: idProjectTask, idsParticipants: userIdsSelected || [] } },
      });
    }

    if (userIdsSelected && setUserIds) {
      setUserIds(userIdsSelected);
      setOpenParticipant(false);
    }
  };

  const handleClose = (open: boolean) => {
    if (userIds) setUserIdsSelected(userIds && userIds.length ? userIds : []);
    setOpenParticipant(open);
  };

  return (
    <CustomModal
      open={openModalParticipant}
      setOpen={handleClose}
      title={title}
      withBtnsActions={true}
      closeIcon={true}
      headerWithBgColor={true}
      fullWidth={true}
      className={classes.usersModalRoot}
      centerBtns={true}
      onClickConfirm={handleConfirm}
      actionButton="Ajouter"
      withCancelButton={false}
    >
      <UserSelect
        userIdsSelected={userIdsSelected}
        setUserIdsSelected={setUserIdsSelected}
        filtersSelected={isGroupePharmacie ? ['PHARMACIE', 'MY_PHARMACIE'] : ['ALL']}
        disableAllFilter={true}
        activeAllContact={
          currentUser.role &&
          currentUser.role.code &&
          [ADMINISTRATEUR_GROUPEMENT, SUPER_ADMINISTRATEUR].includes(currentUser.role.code)
            ? true
            : false
        }
      />
    </CustomModal>
  );
};
export default AjoutParticipant;
