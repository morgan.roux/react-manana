import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { flexWrap: 'nowrap' },
    contentFilter: {
      //padding: theme.spacing(3, 2.5, 10.75),
      '& .MuiRadio-root': {
        color: '#000000',
      },
      '& .MuiCheckbox-colorSecondary.Mui-checked': {
        color: `${theme.palette.secondary.main} !important`,
      },
      '& .MuiRadio-colorSecondary.Mui-checked': {
        color: `${theme.palette.secondary.main} !important`,
      },
      '& .MuiSvgIcon-root': {
        '& #Tracé_2726': {
          fill: theme.palette.secondary.main,
        },
        '& #Tracé_2725': {
          fill: theme.palette.secondary.main,
        },
      },
      // [theme.breakpoints.down('md')]: {
      //   padding: theme.spacing(3, 2.5, 0),
      // },
    },
    title: {
      marginLeft: 9,
      fontSize: 18,
      color: theme.palette.secondary.main,
      fontWeight: 'bold',
    },
    iconAction: {
      padding: theme.spacing(0.5),
      margin: theme.spacing(0, 0.65, 0, 0),
    },
    count: {
      padding: '2px 6px',
      marginLeft: 10,
      textAlign: 'center',
      background: '#E0E0E0',
      height: 20,
      minWidth: 20,
      borderRadius: 2,
      fontSize: 10,
      color: '#212121',
      lineHeight: '20px',
    },
    listItem: {
      marginBottom: 16,
      '& .MuiTypography-root': {
        marginLeft: 8,
      },
    },
    topBar: {
      height: 70,
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
      background: theme.palette.primary.main,
      color: theme.palette.common.white,
      paddingLeft: 8,
      '& .MuiTypography-root': {
        fontSize: 18,
        fontFamily: 'Roboto',
      },
    },
    row: {
      '& .MuiListItem-gutters': {
        padding: 0,
      },
    },
  })
);

export default useStyles;
