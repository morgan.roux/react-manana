import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

const skeletonStyles = makeStyles((theme: Theme) => 
  createStyles({
    root: {
      width: "100%",
      padding: "25px",
      height: "200px"
    },
    head: {
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
    },
    ordonnance: {
      display: "flex",
      justifyContent: "space-around",
      alignItems: "center",
      '& .MuiSkeleton-circle' : {
        marginRight: "25px"
      }
    },
    actions:{
      display: "flex",
      alignItems: "center",
      '& .MuiSkeleton-text' : {
        marginLeft: "25px"
      }
    },
    marginLeft:{
      marginLeft: "-15px",
    },
    body:{
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "start",
      height: "100%",
      alignItems: "center"
    },
  })
)

export default skeletonStyles;