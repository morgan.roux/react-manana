import React from 'react'
import Skeleton from '@material-ui/lab/Skeleton'
import skeletonStyles from './skeletonStyles'

const isMd = () => window.innerWidth <= 960
const isSm = () => window.innerWidth <= 760



const SkeletonList: React.FC = () => {

  const classes = skeletonStyles()

  return (
    <div className={classes.root}>
      <div className={classes.head}>
        <div className={classes.ordonnance}>
          <Skeleton
            variant='circle'
            width={35}
            height={35}
            animation='wave'
          />
          <Skeleton
            variant='text'
            width={117}
            height={24}
            animation='wave'
          />
        </div>
        <div className={classes.actions}>
          <Skeleton
            variant='circle'
            width={35}
            height={35}
            animation='wave'
          />
          <Skeleton
            variant='circle'
            width={35}
            height={35}
            className={classes.marginLeft}
            animation='wave'
          />
          <Skeleton
            variant='text'
            width={20}
            height={10}
            animation='wave'
          />
        </div>
      </div>
      <div className={classes.body}>
        <Skeleton
          variant='rect'
          width={500}
          height={20}
          animation='wave'
        />
      </div>
    </div>
  )
}

export default SkeletonList;

/**
 * text: 117*24 OK
 * DP: 40*40 circle OK
 * DM: 40*40 circle OK
 * 3points: text OK
 * calendar: 24*24 rect
 * date: 140*10 rect
 * comment: 20*20 rect
 * importance: 130*72 rect
 * MuiSvgIcon: 24*24 rect
 */
