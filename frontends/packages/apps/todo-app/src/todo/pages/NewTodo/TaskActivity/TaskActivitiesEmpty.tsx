import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import { Typography } from '@material-ui/core';
import EmptyPhoto from '../../../assets/img/Todo/autres.svg';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      textAlign: 'center',
      marginBottom: 50,
    },
    image: {
      width: 175,
      height: 220,
      marginBottom: 50,
      [theme.breakpoints.down('md')]: {
        width: '25%',
        height: 'auto',
        marginBottom: 25,
        marginTop: 25,
      },
    },
    title: {
      fontFamily: 'Montserrat',
      fontSize: 22,
      fontWeight: 'bold',
      color: '#4D4D4D',
      letterSpacing: 0.02,
      marginBottom: 15,
      [theme.breakpoints.down('md')]: {
        fontSize: 14,
      },
    },
    subTitle: {
      fontFamily: 'Roboto',
      fontSize: 14,
      color: '#4D4D4D',
      fontWeight: 'normal',
      letterSpacing: 0,
      [theme.breakpoints.down('md')]: {
        fontSize: 12,
      },
    },
  })
);

const TaskActivitiesEmpty = () => {
  const classes = useStyles({});
  return (
    <div className={classes.root}>
      <img className={classes.image} src={EmptyPhoto} alt="Todo Illustration" />
      <Typography className={classes.title}>Gardez vos tâches organisées par projet</Typography>
      <Typography className={classes.subTitle}>
        Regroupez vos tâches par objectif et domaine.
        <br />
        Glissez-dépossez les tâches pour les réorganiser ou créer des sous-tâches.
      </Typography>
    </div>
  );
};

export default TaskActivitiesEmpty;
