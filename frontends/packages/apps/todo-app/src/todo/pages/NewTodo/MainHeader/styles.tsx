import { makeStyles, Theme, createStyles, lighten } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      alignItems: 'start',
      justifyContent: 'space-between',
      width: '100%',
      padding: theme.spacing(3.125, 4),
      '& .MuiIconButton-root': {
        padding: 0,
        color: '#000000',
      },
      [theme.breakpoints.down('md')]: {
        padding: 24,
      },
    },
    title: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 20,
    },
    date: {
      font: 'normal normal normal 14px/22px Roboto',
      color: '#9E9E9E',
      marginLeft: 12,
    },
    childrenContainer: {
      display: 'flex',
      alignItems: 'flex-end',
      '& > button:not(:nth-child(1))': {
        marginLeft: 15,
      },
    },
    count: {
      marginLeft: 10,
      textAlign: 'center',
      background: '#E0E0E0',
      height: 20,
      minWidth: 20,
      borderRadius: 2,
      fontSize: 10,
      color: '#212121',
      lineHeight: '20px',
    },
    addTaskButton: {
      padding: '11px 34px',
      borderRadius: 5,
      display: 'flex',
      alignItems: 'center',
      fontFamily: 'Montserrat',
      fontSize: '14px',
      fontWeight: 'bold',
      // color: theme.palette.primary.main,
      marginTop: 12,
      cursor: 'pointer',
      width: 'fit-content',
    },
    nextWeekContainer: {
      display: 'flex',
    },
    appBarCalendar: {
      margin: '15px 0 12px',
      backgroundColor: 'transparent',
      boxShadow: 'none!important',
      color: '#212121',
      borderBottom: '1px solid #E0E0E0',
      '& .MuiTab-root, & .main-MuiTab-root': {
        minWidth: '60px !important',
        width: '14.28%!important',
        padding: 16,
      },
      '& .Mui-selected, & .main-Mui-selected': {
        backgroundColor: '#F5F6FA',
      },
    },
    jourName: {
      fontSize: 14,
      color: '#9E9E9E',
      textTransform: 'capitalize',
    },
    jourNombre: {
      fontSize: 19,
      fontWeight: 500,
    },
    iconCustom: {
      backgroundColor: 'white',
      color: 'black',
      width: theme.spacing(7),
      height: theme.spacing(7),
    },
    addParticipantIcon: {
      marginRight: 16,
    },
    labelParticipant: {
      fontSize: '0.875rem',
      color: '#424242',
      fontWeight: 500,
    },
    searchResult: {
      fontSize: 20,
      fontFamily: 'Roboto',
      fontWeight: 'bold',
      display: 'flex',
      alignItems: 'center',
      '& span': {
        color: theme.palette.secondary.main,
      },
    },
  })
);

export default useStyles;
