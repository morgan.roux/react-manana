import { Comment, CommentListNew, nl2br, useApplicationContext, useDisplayNotification } from '@lib/common';
import { SearchTodoActionInfoFragment, useUpdate_Item_Scoring_For_ItemLazyQuery } from '@lib/common/src/federation';
import { ActionStatus, useUpdate_Action_StatusMutation } from '@lib/common/src/graphql';
import { ConfirmDeleteDialog, CustomTabs, TabInterface } from '@lib/ui-kit';
import { Box, Icon, IconButton, ListItemIcon, Menu, MenuItem, Radio, Typography } from '@material-ui/core';
import { CheckCircleOutline, Delete, Edit, MoreHoriz, Today } from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import React, { useRef, useState } from 'react';
import { useEffect } from 'react';
import AjoutTaches from '../Modals/AjoutTache/AjoutTaches';
import { CountTodosProps } from '../NewTodo';
import TaskActivities from '../TaskActivity';
import useStyles from './styles';

interface TaskDetailsProps {
  task: SearchTodoActionInfoFragment;
  disabledActions?: boolean;
  doSoftDeleteActions?: Function;
  refetchAll?: () => void;
  updateActionStatus: (todoAction: SearchTodoActionInfoFragment, newStatut: string) => void;
  scrollToCommentArea?: boolean;
}

const TaskDetails: React.FC<TaskDetailsProps & CountTodosProps> = ({
  task,
  doSoftDeleteActions,
  disabledActions = false,
  refetchAll,
  scrollToCommentArea,
  updateActionStatus,
}) => {
  const classes = useStyles({});
  const { user, useParameterValueAsBoolean } = useApplicationContext();

  const [actionStatus, setActionsStatus] = useState<string>();

  const useMatriceResponsabilite = useParameterValueAsBoolean('0501');
  const commentAreaRef = useRef<HTMLDivElement>(null);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [openFormModal, setOpenFormModal] = React.useState<boolean>(false);
  const [openDeleteDialog, setOpenDeleteDialog] = React.useState(false);

  useEffect(() => {
    setActionsStatus(task.status);
  }, [task.status]);

  const tabs: TabInterface[] = [
    {
      id: 0,
      label: 'Commentaires',
      content: (
        <Box display="flex" flexDirection="column">
          <CommentListNew codeItem="TODO" idItemAssocie={task.id} />
          {!disabledActions && <Comment codeItem="TODO" idSource={task.id} withAttachement={true} />}
        </Box>
      ),
    },
    {
      id: 1,
      label: 'Activités',
      content: <TaskActivities idTask={task.id} />,
    },
  ];
  const assigns = (task?.participants || []).map((user) => user.id);
  const idCreation = task.createdBy?.id || '';

  const handleChange = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
    event.preventDefault();
    event.stopPropagation();
    if (task) {
      const newStatus = actionStatus === ActionStatus.Done ? ActionStatus.Active : ActionStatus.Done;
      updateActionStatus(task, newStatus);
      setActionsStatus(newStatus);
    }
  };

  const onClickMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseMenu = () => setAnchorEl(null);

  const onClickEdit = () => {
    handleCloseMenu();
    setOpenFormModal(true);
  };

  const onClickDelete = () => {
    handleCloseMenu();
    setOpenDeleteDialog(true);
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (doSoftDeleteActions) {
      doSoftDeleteActions({ variables: { ids: [task.id] } });
    }
  };

  if (scrollToCommentArea)
    commentAreaRef?.current?.scrollIntoView({
      behavior: 'smooth',
    });

  return (
    <div className={classes.taskDetailsRoot}>
      <div className={classes.taskContainer}>
        <div className={classes.taskTitleContainer}>
          <div className={classes.radioContainer}>
            <Radio
              className={classes.radio}
              checked={actionStatus === ActionStatus.Done}
              onClick={handleChange}
              checkedIcon={<CheckCircleOutline />}
              disabled={
                (!assigns.length && idCreation && idCreation === user.id) ||
                (assigns.length && assigns.includes(user.id))
                  ? false
                  : !useMatriceResponsabilite
              }
            />
            <Typography
              className={classes.taskTitle}
              dangerouslySetInnerHTML={{ __html: nl2br(task.description) } as any}
            />
          </div>
          <IconButton onClick={onClickMenu} disabled={disabledActions}>
            <MoreHoriz />
          </IconButton>
          <Menu
            id="task-details-menu"
            anchorEl={anchorEl}
            keepMounted={true}
            open={Boolean(anchorEl)}
            onClose={handleCloseMenu}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          >
            <MenuItem onClick={onClickEdit}>
              <ListItemIcon>
                <Edit fontSize="small" />
              </ListItemIcon>
              <Typography variant="inherit">Modifier</Typography>
            </MenuItem>
            <MenuItem
              onClick={onClickDelete}
              disabled={user && user.id && (assigns.includes(user.id) || idCreation === user.id) ? false : true}
            >
              <ListItemIcon>
                <Delete fontSize="small" />
              </ListItemIcon>
              <Typography variant="inherit">Supprimer</Typography>
            </MenuItem>
          </Menu>
        </div>
        <Box className={classnames(classes.contentLabels, classes.taskInfoContainer)}>
          {task.dateDebut && (
            <Box className={classes.labels}>
              <Icon color="secondary">
                <Today />
              </Icon>
              <Typography className={classnames(classes.labelText, classes.date)}>
                {moment(task.dateDebut).format('DD MMM YYYY')}
              </Typography>
            </Box>
          )}
          {task.dateFin && (
            <Box className={classes.labels}>
              <Icon color="secondary">
                <Today />
              </Icon>
              <Typography className={classnames(classes.labelText, classes.date)}>
                {moment(task.dateFin).format('DD MMM YYYY')}
              </Typography>
            </Box>
          )}
        </Box>
      </div>
      <div ref={commentAreaRef}>
        <CustomTabs tabs={tabs} hideArrow={true} />
      </div>

      <AjoutTaches
        openModalTache={openFormModal}
        setOpenTache={setOpenFormModal}
        task={task}
        operationName={'EDIT'}
        refetchAll={refetchAll}
      />
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content="Êtes-vous sûr de vouloir supprimer cette tâche?"
        onClickConfirm={onClickConfirmDelete}
      />
    </div>
  );
};

export default TaskDetails;
