import TaskActivities from './TaskActivities';
import TaskActivitiesEmpty from './TaskActivitiesEmpty';

export { TaskActivitiesEmpty };

export default TaskActivities;
