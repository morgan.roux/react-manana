import { Accordion, AccordionDetails, AccordionSummary, Box, Typography } from '@material-ui/core';
import {} from '@lib/common/src/federation';
import { ExpandMore } from '@material-ui/icons';
import React, { FC } from 'react';
import UserFilterList, { UserFilterListProps } from './UserFilterList';
import { useStyles } from './styles';

interface ImportanceFilterProps extends UserFilterListProps {
  withDropdown?: boolean;
}

const UserFilter: FC<ImportanceFilterProps> = ({ value, onChange, withDropdown = true }) => {
  const classes = useStyles({});

  return withDropdown ? (
    <Box className={classes.root}>
      <Accordion defaultExpanded={false}>
        <AccordionSummary expandIcon={<ExpandMore />}>
          <Typography className={classes.heading}>Collaborateurs</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <UserFilterList value={value} onChange={onChange} />
        </AccordionDetails>
      </Accordion>
    </Box>
  ) : (
    <UserFilterList value={value} onChange={onChange} />
  );
};

export default UserFilter;
