import { Favorite } from '@material-ui/icons';
import React from 'react';
import { ActionButtonMenu } from '../../../Common/ActionButton';

export const useProjectList = (addProjectToFavoriteclick: () => void, project: any) => {
  const projectMenuItems: ActionButtonMenu[] = [
    {
      label: 'Ajouter au favoris',
      icon: <Favorite />,
      onClick: addProjectToFavoriteclick,
      disabled: project && project.isInFavoris,
    },
  ];
  return [projectMenuItems];
};
