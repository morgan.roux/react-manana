import { useApplicationContext } from '@lib/common';
import { SearchTodoActionInfoFragment } from '@lib/common/src/federation';
import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Delete, Edit, MoreHoriz, Person } from '@material-ui/icons';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import EventIcon from '@material-ui/icons/Event';
import FlagIcon from '@material-ui/icons/Flag';
import SortByAlphaIcon from '@material-ui/icons/SortByAlpha';
import { createStyles, makeStyles } from '@material-ui/styles';
import React, { FC, ReactNode, useState } from 'react';
import { useTodoURLParams } from '../hooks/useTodoURLParams';

const useStyles = makeStyles(() =>
  createStyles({
    itemList: {
      fontSize: '0.875rem !important',
    },
  })
);
export interface ActionButtonMenu {
  label: any;
  icon: ReactNode;
  disabled: boolean;
  onClick: (event: any) => any;
  setState?: (row: any) => void;
  setOperationName?: (operationName: string) => void;
  hide?: boolean;
}

export interface ActionButtonProps {
  actionMenu: 'FILTER_MENU' | 'TODO_ACTION_MENU';
  onRequestEditTodoAction?: (todoAction: SearchTodoActionInfoFragment) => void;
  onRequestOpenDeleteDialog?: (data: any, title?: string) => void;
  todoAction?: SearchTodoActionInfoFragment;
}

const descNames = ['priorite'];
const Actionbutton: FC<ActionButtonProps> = ({
  actionMenu,
  onRequestEditTodoAction,
  onRequestOpenDeleteDialog,
  todoAction,
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const { redirectTo } = useTodoURLParams();
  const open = anchorEl ? true : false;
  const { user } = useApplicationContext();

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    }
  };

  const handleFiltre = (name: string) => {
    redirectTo({ sorting: [{ [name]: { order: descNames.includes(name) ? 'desc' : 'asc' } }] });
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleEditTodoActionClick = () => {
    onRequestEditTodoAction && todoAction && onRequestEditTodoAction(todoAction);
  };

  const handleDeleteTodoActionClick = () => {
    onRequestOpenDeleteDialog && onRequestOpenDeleteDialog(todoAction, todoAction?.description);
  };

  let menuList: ActionButtonMenu[] = [];

  const actionMenuItems: ActionButtonMenu[] = [
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: handleEditTodoActionClick,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handleDeleteTodoActionClick,
      disabled: todoAction?.createdBy?.id !== user.id,
    },
  ];

  const filterMenuItems: ActionButtonMenu[] = [
    {
      label: 'Trier par date de création',
      icon: <CalendarTodayIcon />,
      onClick: () => handleFiltre('createdAt'),
      disabled: false,
    },
    {
      label: "Trier par date d'echéance",
      icon: <EventIcon />,
      onClick: () => handleFiltre('dateDebut'),
      disabled: false,
    },
    {
      label: 'Trier par priorité',
      icon: <FlagIcon />,
      onClick: () => handleFiltre('priorite'),
      disabled: false,
    },
    {
      label: 'Trier par description',
      icon: <SortByAlphaIcon />,
      onClick: () => handleFiltre('description'),
      disabled: false,
    } /*,
    {
      label: 'Trier par responsable',
      icon: <Person />,
      onClick: () => handleFiltre('isAssigned'),
      disabled: false,
    },*/,
  ];

  switch (actionMenu) {
    case 'FILTER_MENU':
      menuList = filterMenuItems;
      break;
    case 'TODO_ACTION_MENU':
      menuList = actionMenuItems;
      break;
    default:
      menuList = [];
      break;
  }

  if (menuList.length > 0) {
    return (
      <>
        <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color="inherit">
          <MoreHoriz />
        </IconButton>
        <Menu
          // tslint:disable-next-line: jsx-no-lambda
          onClick={(event) => {
            event.preventDefault();
            event.stopPropagation();
          }}
          id="fade-menu"
          anchorEl={anchorEl}
          keepMounted={true}
          open={open}
          onClose={handleClose}
          TransitionComponent={Fade}
        >
          {menuList.map(
            (i: ActionButtonMenu, index: number) =>
              !i.hide && (
                <MenuItem
                  // tslint:disable-next-line: jsx-no-lambda
                  onClick={(event) => {
                    event.preventDefault();
                    event.stopPropagation();
                    handleClose();
                    i.onClick(event);
                  }}
                  key={`table_menu_item_${index}`}
                  disabled={i.disabled}
                  className={classes.itemList}
                >
                  <ListItemIcon>{i.icon}</ListItemIcon>
                  <Typography variant="inherit">{i.label} </Typography>
                </MenuItem>
              )
          )}
        </Menu>
      </>
    );
  }

  return <></>;
};

export default Actionbutton;
