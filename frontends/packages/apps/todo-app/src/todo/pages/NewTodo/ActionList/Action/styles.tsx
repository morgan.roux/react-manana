import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      flexWrap: 'nowrap',
      cursor: 'pointer',
      '& .MuiAccordion-root': {
        boxShadow: 'none!important',
        '&.Mui-expanded': {
          margin: '0',
        },
      },
      '& .MuiAccordionSummary-root': {
        flexDirection: 'row-reverse',
        padding: 0,
        '&.Mui-expanded': {
          minHeight: 45,
        },
        '& .MuiAccordionSummary-expandIcon': {
          padding: 0,
          marginRight: 10,
        },
      },
      '& .MuiAccordionSummary-content': {
        justifyContent: 'space-between',
        '&.Mui-expanded': {
          margin: '11px 0',
        },
      },

      '& .MuiAccordionDetails-root': {
        [theme.breakpoints.up('sm')]: {
          padding: '0px 0px 0px 40px!important',
        },
        padding: '0px',
        '& .MuiList-root': {
          width: '100%',
          paddingTop: 0,
          paddingBottom: 0,
          '& .MuiListItem-button': {
            paddingRight: 0,
            paddingTop: 0,
            paddingBottom: 0,
            paddingLeft: 0,
            marginBottom: 2,
            '& .MuiIconButton-root': {
              padding: 0,
            },
          },
        },
      },
      // ! IMPORTANT : Hide date picker input
      '& .MuiFormControl-marginNormal': {
        display: 'none',
      },
      [theme.breakpoints.down('md')]: {
        borderRadius: 0,
      },

      '&:active': {
        // color: 'red',
        textDecoration: 'none',
      },
      '&:focus': {
        outline: 'none',
        borderColor: 'blue',
        boxShadow: 'none',
      },
    },
    date: {
      fontSize: 12,
      width: 140,
      // color: theme.palette.primary.main,
      '& > input': {
        cursor: 'pointer',
      },
      [theme.breakpoints.down('sm')]: {
        width: 'auto',
      },
    },
    labelText: {
      marginLeft: 8,
      fontSize: 12,
      lineHeight: '10px',
      fontFamily: 'Montserrat',
      [theme.breakpoints.down('md')]: {
        whiteSpace: 'nowrap',
      },
    },
    bold: {
      fontWeight: 600,
    },
    contentLabels: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'start',
      marginLeft: 50,
      marginTop: 26,
      [theme.breakpoints.down('md')]: {
        width: '100%',
        flexWrap: 'nowrap',
        overflow: 'auto',
        marginLeft: 0,
      },
    },
    labelOrigine: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      textDecoration: 'underline',
      marginRight: 4,
      [theme.breakpoints.down('md')]: {
        whiteSpace: 'nowrap',
      },
    },
    rootButton: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      '& .MuiIconButton-root': {
        color: '#000000',
        padding: 0,
      },
      '& .MuiList-root': {
        width: '100%',
        paddingTop: 0,
        paddingBottom: 0,
        '& .MuiListItem-button': {
          paddingRight: 0,
          paddingTop: 0,
          paddingBottom: 0,
          paddingLeft: 0,
          marginBottom: 16,
        },
      },
    },
    labels: {
      display: 'flex',
      alignItems: 'center',
      marginRight: 28,
      marginBottom: 8,
      zIndex: 9999,
    },
    taskTitle: {
      textAlign: 'left',
      fontFamily: 'Montserrat',
      color: '#1D1D1D',
      opacity: 1,
      '& > p': {
        [theme.breakpoints.down('sm')]: {
          display: 'block',
          overflow: 'hidden',
          maxHeight: '3.6em',
          lineHeight: '1.8em',
          textOverflow: 'ellipsis',
        },
      },
      [theme.breakpoints.down('sm')]: {
        maxWidth: '60vw',
        display: 'inline-block',
        textOverflow: 'ellipsis',
        wordBreack: 'break-word',
        overflow: 'hidden',
        maxHeight: '3.6em',
        lineHeight: '1.8em',
      },
    },
    addPersonButton: {
      marginRight: 16,
    },
    tasks: {
      borderRadius: 5,
      boxShadow: 'none',
      width: '100%',
      padding: 15,
      border: '1px solid #E3E3E3',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'start',
      '& .MuiCheckbox-root': {
        marginRight: 15,
        padding: 0,
      },
      '&:hover': {
        background: '#F2F2F2',
      },
      [theme.breakpoints.down('md')]: {
        borderRadius: 0,
      },
    },
    list: {
      width: '100%',
      padding: '0px',
      marginBottom: 16,
    },
    reporteText: {
      fontSize: 14,
      fontFamily: 'Roboto',
      fontWeight: 'normal',
      letterSpacing: 0,
      color: theme.palette.secondary.main,
      cursor: 'pointer',
      '&:hover': {
        textDecoration: 'underline',
      },
    },
    taskDetailsModal: {
      '& .MuiDialog-paperWidthMd': {
        maxWidth: '762px !important',
        minHeight: '700px !important',
      },
      '& .MuiListItemIcon-root': {
        minWidth: '35px !important',
      },
    },
    emptyContainer: {
      width: '100%',
      minHeight: 'calc(100vh - 327px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    sectionLibelle: {
      fontFamily: 'Roboto',
      fontSize: 14,
      fontWeight: 500,
    },
    nbrProduits: {
      padding: '2px 6px',
      fontSize: 10,
      background: '#E0E0E0',
      borderRadius: 2,
      height: 20,
      minWidth: 20,
      textAlign: 'center',
      marginLeft: 10,
    },
    subTasks: {
      borderRadius: 5,
      boxShadow: 'none',
      width: '100%',
      padding: 15,
      border: '1px solid #E3E3E3',
      display: 'flex',
      alignItems: 'start',
      marginLeft: 30,
      '& .MuiCheckbox-root': {
        marginRight: 15,
        padding: 0,
      },
    },
    checkedBox: {
      color: `${theme.palette.primary.main} !important`,
    },
    boxnotchecked: {
      color: '#757575 !important',
    },
  })
);
/**
 * 
  
  border-color: ${(props) => getBorderColor(props.isDragging, props.colors)};
  background-color: ${(props) =>
    getBackgroundColor(props.isDragging, props.isGroupedOver, props.colors)};
  box-shadow: ${({ isDragging }) =>
    isDragging ? `2px 2px 1px ${colors.N70}` : 'none'};
  box-sizing: border-box;
  padding: ${grid}px;
  min-height: ${imageSize}px;
  margin-bottom: ${grid}px;
  user-select: none;
  color: ${colors.N900};

 */

export default useStyles;
