import { Accordion, AccordionDetails, AccordionSummary, Box, Typography } from '@material-ui/core';
import { ExpandMore } from '@material-ui/icons';
import React, { FC } from 'react';
import { useStyles } from './styles';
import UrgenceFilterList from './UrgenceFilterList';

export interface UrgenceInterface {
  checked: boolean;
  __typename?: 'Urgence';
  id?: string;
  code?: string | null;
  couleur?: string | null;
  libelle?: string | null;
}

interface UrgenceFilterProps {
  urgence: UrgenceInterface[] | null | undefined;
  setUrgence: (urgence: UrgenceInterface[] | null | undefined) => void;
  withDropdown?: boolean;
}

const UrgenceFilter: FC<UrgenceFilterProps> = ({ urgence, setUrgence, withDropdown = true }) => {
  const classes = useStyles({});

  return withDropdown ? (
    <Box className={classes.root}>
      <Accordion defaultExpanded={false}>
        <AccordionSummary expandIcon={<ExpandMore />}>
          <Typography className={classes.heading}>Urgence</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <UrgenceFilterList urgence={urgence} setUrgence={setUrgence} />
        </AccordionDetails>
      </Accordion>
    </Box>
  ) : (
    <UrgenceFilterList urgence={urgence} setUrgence={setUrgence} />
  );
};

export default UrgenceFilter;
