import { startTime, useApplicationContext } from '@lib/common';
import { AppBar, Box, CssBaseline, Hidden, IconButton, Tab, Tabs, Typography } from '@material-ui/core';
import { Add, Close, KeyboardArrowLeft, KeyboardArrowRight } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, useState } from 'react';
import ActionButton from '../Common/ActionButton';
import { useTodoURLParams } from '../hooks/useTodoURLParams';
import AjoutParticipant from '../Modals/AjoutParticipants';
import {
  Aggregate_Date_Histogram_By_DayQuery,
  Aggregate_Date_Histogram_By_DayQueryVariables,
  Search_Todo_ActionQuery,
  Search_Todo_ActionQueryVariables,
} from '@lib/common/src/federation';
import useStyles from './styles';
import { LazyQueryResult, QueryResult } from '@apollo/client';
import { CountTodo } from '../util/types';
import { useEffect } from 'react';

const getTotalTodoActions = (offsetFromNow: number, aggretionResult?: Aggregate_Date_Histogram_By_DayQuery) => {
  if ((aggretionResult?.aggregateDateHistogramByDay?.data || 0) === 0) {
    return 0;
  }
  const currentDate = moment().add(offsetFromNow, 'days').format('YYYY-MM-DD');
  const currentAggregation = (aggretionResult?.aggregateDateHistogramByDay?.data || []).find(
    (item) => item?.date === currentDate
  );

  return currentAggregation?.total || 0;
};

export const countActionsOffsetFromNow = (
  afterOrBefore: 'AFTER' | 'BEFORE',
  offsetFromNow: number,
  aggretionResult?: Aggregate_Date_Histogram_By_DayQuery
) => {
  if ((aggretionResult?.aggregateDateHistogramByDay?.data || 0) === 0) {
    return 0;
  }

  const startDay = startTime(moment().add(offsetFromNow, 'days'));
  const startDayAsMoment = moment(startDay);

  let count = 0;

  for (const histogram of aggretionResult?.aggregateDateHistogramByDay?.data || []) {
    if (histogram?.date) {
      const histogramDate = moment(histogram.date, 'YYYY-MM-DD');

      if (
        (afterOrBefore === 'AFTER' && histogramDate.isAfter(startDayAsMoment)) ||
        (afterOrBefore === 'BEFORE' && histogramDate.isBefore(startDayAsMoment))
      ) {
        count = count + histogram.total;
      }
    }
  }

  return count;
};

const daysRequired = 7;

interface MainHeaderProps {
  handleSetOpenModalTache: (dateDebut: Date) => void;
  participants?: number | null | undefined;
  todoProjet?: any;
  title: string;
  counting: CountTodo;
  loadAggregateDataHistogramByDay: (variables: Aggregate_Date_Histogram_By_DayQueryVariables) => void;
  loadingAggregateDataHistogramByDay: LazyQueryResult<
    Aggregate_Date_Histogram_By_DayQuery,
    Aggregate_Date_Histogram_By_DayQueryVariables
  >;
  loadingActions: QueryResult<Search_Todo_ActionQuery, Search_Todo_ActionQueryVariables>;
}

const MainHeader: FC<MainHeaderProps> = ({
  todoProjet,
  handleSetOpenModalTache,
  participants,
  title,
  loadAggregateDataHistogramByDay,
  loadingAggregateDataHistogramByDay,
  counting,
  loadingActions,
}) => {
  const date = moment().format('dddd Do MMMM YYYY');
  const classes = useStyles({});
  const [openModal, setOpenModal] = React.useState(false);
  const [selectedUsersIds, setselectedUsersIds] = useState<string[]>([]);
  const [daysOffset, setDaysOffset] = useState<number>(0);
  const [daysTabs, setDaysTabs] = useState<any>([]);
  const [totalTacheRestantes, setTotalTacheRestantes] = useState<number>(0);
  const { params, redirectTo, getActionsQuery } = useTodoURLParams();
  const { isMobile } = useApplicationContext();

  const isDoneType = params.type?.includes('DONE');

  const isOnProject = params.projet && params.projet.length > 0 ? true : false;

  const handleTabsChange = (event: React.ChangeEvent<{}>, value: any) => {
    const activeDate = daysTabs[value];
    redirectTo({
      date: moment().add(activeDate.offset, 'days').format('YYYY-MM-DD'),
    });
    /*setCurrentTab(value);
    
    if (onChangeOffsetFromNow) {
      onChangeOffsetFromNow(activeDate.offset);
    }*/
  };

  useEffect(() => {
    setDaysOffset(0);
    redirectTo({
      date: moment().format('YYYY-MM-DD'),
    });
  }, [params.type?.includes('DONE')]);

  React.useEffect(() => {
    if (params.date === 'next') {
      redirectTo({
        date: moment().add(1, 'days').format('YYYY-MM-DD'),
      });
    } else if (params.date === 'today') {
      redirectTo({
        date: moment().format('YYYY-MM-DD'),
      });
    }
  }, [params.date]);

  useEffect(() => {
    loadAggregateDataHistogramByDay({
      index: 'todoaction',
      startFieldName: isDoneType ? 'updatedAt' : 'dateDebut',
      endDate: isDoneType ? moment.utc().endOf('day').toISOString() : undefined,
      startDate: !isDoneType ? moment.utc().startOf('day').toISOString() : undefined,
      query: getActionsQuery(undefined, { enableSort: false, enableDate: false }),
    });
  }, [params.type?.includes('DONE'), params.assignee?.includes('me')]);

  React.useEffect(() => {
    if (params.date && params.date !== 'today' && params.date !== 'next' && params.date !== 'past') {
      const days: any[] = [];

      const monday = moment().startOf('isoWeek');
      const diffDays = moment().diff(monday, 'days');

      for (let i = daysOffset - diffDays; i < daysRequired + daysOffset - diffDays; i++) {
        days.push({
          id: `tab-${i}`,
          label: moment().add(i, 'days').format('DD'),
          date: moment().add(i, 'days').format('ddd'),

          value: moment().add(i, 'days').format('YYYY-MM-DD'),

          offset: i,
        });
      }

      setDaysTabs(days);
    }
  }, [daysOffset, params.date]);

  React.useEffect(() => {
    if (loadingAggregateDataHistogramByDay.loading) {
      setTotalTacheRestantes(0);
    } else if (daysTabs.length > 0 && loadingAggregateDataHistogramByDay.data?.aggregateDateHistogramByDay?.data) {
      const fromOffset = daysTabs[isDoneType ? 1 : daysTabs.length - 1]?.offset;

      if (!fromOffset) {
        setTotalTacheRestantes(0);
      } else {
        setTotalTacheRestantes(
          countActionsOffsetFromNow(
            isDoneType ? 'BEFORE' : 'AFTER',
            fromOffset,
            loadingAggregateDataHistogramByDay.data
          )
        );
      }
    }
  }, [
    daysTabs,
    loadingAggregateDataHistogramByDay.loading,
    params.type?.includes('DONE'),
    params.assignee?.includes('me'),
  ]);

  const handleScrollNext = () => {
    if (!isDoneType || daysOffset < 1) {
      setDaysOffset((prev) => prev + daysRequired);
    }
  };

  const handleScrollPrev = () => {
    if (isDoneType || daysOffset > 1) {
      setDaysOffset((prev) => prev - daysRequired);
    }
  };

  const onTaskCalendar = !isOnProject && params.date !== 'today' && params.date !== 'past' && !params.searchText;

  const nextTabIndex =
    onTaskCalendar &&
    (daysTabs || []).findIndex((dayTab: any) => {
      return moment().add(dayTab.offset, 'days').format('YYYY-MM-DD') === params.date;
    });

  const currentTab = onTaskCalendar && params.date !== 'next' ? nextTabIndex : 0;

  const getCountTodayActions = () => {
    if (params.assignee) {
      if (params.assignee.includes('me')) {
        return (counting.todayMe || 0) + (counting.pastMe || 0);
      }
      return (counting.todayTeam || 0) + (counting.pastTeam || 0);
    }

    return 0;
  };

  const handleClearSearch = () => {
    redirectTo({ assignee: ['me'], projet: undefined, date: 'today', searchText: undefined });
  };

  return (
    <Box className={classes.root}>
      <Box display="flex" flexDirection="column" width="100%">
        <Box display="flex" alignItems="flex-end">
          <CssBaseline />
          <Typography className={classes.title}>
            {onTaskCalendar ? `${moment().add(daysOffset, 'days').format('MMM YYYY')}` : title}
          </Typography>
        </Box>
        {params.searchText && (
          <Box className={classes.searchResult}>
            <Typography>
              {loadingActions.data?.search?.total && <span>{loadingActions.data?.search?.total}</span>} résultat(s)
              trouvé(s)
            </Typography>
            <IconButton onClick={handleClearSearch}>
              <Close />
            </IconButton>
          </Box>
        )}
        <Box className={classes.nextWeekContainer}>
          {onTaskCalendar && (
            <AppBar className={classes.appBarCalendar} position="static">
              <Tabs
                value={currentTab}
                onChange={handleTabsChange}
                variant="scrollable"
                scrollButtons="on"
                aria-label="simple tabs example"
                ScrollButtonComponent={(item) => {
                  const disableLeftArrow = !isDoneType && daysOffset <= 0;
                  const disableRightArrow = isDoneType && daysOffset >= 0;
                  if (
                    (item.direction === 'left' && disableLeftArrow) ||
                    (item.direction === 'right' && disableRightArrow)
                  ) {
                    return null;
                  }

                  const showRemaining = isDoneType ? item.direction === 'left' : item.direction === 'right';

                  return (
                    <IconButton
                      onClick={item.direction === 'right' ? handleScrollNext : handleScrollPrev}
                      style={{ borderRadius: 0 }}
                    >
                      {isDoneType && showRemaining && totalTacheRestantes > 0 ? (
                        <span style={{ fontSize: 12, marginRight: 2 }}>{totalTacheRestantes} </span>
                      ) : null}

                      {item.direction === 'right' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                      {!isDoneType && showRemaining && totalTacheRestantes > 0 ? (
                        <span style={{ fontSize: 12, marginRight: 2 }}>{totalTacheRestantes} </span>
                      ) : null}
                    </IconButton>
                  );
                }}
              >
                {onTaskCalendar &&
                  daysTabs.map((day: any) => {
                    let count = !isDoneType && day.offset === 0 ? getCountTodayActions() : 0;
                    if (!loadingAggregateDataHistogramByDay.loading) {
                      if (isDoneType) {
                        count =
                          day.offset > 0 ? 0 : getTotalTodoActions(day.offset, loadingAggregateDataHistogramByDay.data);
                      } else {
                        count =
                          day.offset < 0
                            ? 0
                            : day.offset === 0
                            ? getCountTodayActions()
                            : getTotalTodoActions(day.offset, loadingAggregateDataHistogramByDay.data);
                      }
                    }

                    return (
                      <Tab
                        disabled={isDoneType ? day.offset > 0 : day.offset < 0}
                        key={day.id}
                        label={
                          <Box height={75}>
                            <Typography color={day.offset === 0 ? 'secondary' : undefined} className={classes.jourName}>
                              {day.date}
                            </Typography>
                            <Typography
                              color={day.offset === 0 ? 'secondary' : undefined}
                              className={classes.jourNombre}
                            >
                              {day.label}
                            </Typography>
                            {count > 0 && <Typography>({count})</Typography>}
                          </Box>
                        }
                      />
                    );
                  })}
              </Tabs>
            </AppBar>
          )}
        </Box>
        {isOnProject && (
          <Box display="flex" alignItems="center">
            <Typography className={classes.labelParticipant}>Participant(s)</Typography>
            <Typography className={classes.count}>{participants}</Typography>
          </Box>
        )}

        {!isMobile ? (
          <Typography
            className={classes.addTaskButton}
            color="primary"
            onClick={() =>
              handleSetOpenModalTache(
                onTaskCalendar
                  ? moment.utc(params.date, 'YYYY-MM-DD').startOf('day').toDate()
                  : moment.utc().startOf('day').toDate()
              )
            }
          >
            <Add />
            Ajouter une tâche
          </Typography>
        ) : null}
      </Box>
      <Box display="flex" alignItems="center">
        <Box display="flex">
          <CssBaseline />
          <Hidden mdDown={true}>
            <ActionButton actionMenu="FILTER_MENU" />
          </Hidden>

          {!onTaskCalendar && (
            <AjoutParticipant
              openModalParticipant={openModal}
              setOpenParticipant={setOpenModal}
              userIds={selectedUsersIds}
              setUserIds={setselectedUsersIds}
              idProjectTask={(todoProjet && todoProjet.id) || ''}
            />
          )}
        </Box>
      </Box>
    </Box>
  );
};

export default MainHeader;
