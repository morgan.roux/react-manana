import { MeUserInfoFragment } from '@lib/common/src/federation';
import { Filter } from '../util/types';
import { TodoURLParams } from '../useTodoURLParams';

export const buildProjetFilter = (user: MeUserInfoFragment, params: TodoURLParams): Filter => {
  if (params.projet) {
    return {
      must: [
        {
          terms: {
            'projet.id': params.projet,
          },
        },
      ],
    };
  }

  return {};
};
