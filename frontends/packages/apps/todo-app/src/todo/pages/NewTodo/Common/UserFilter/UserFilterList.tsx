import { Box, Checkbox, List, ListItem, ListItemText, Typography } from '@material-ui/core';
import React, { FC } from 'react';
import { useStyles } from './styles';

import { User } from '@lib/common/src/graphql';
import { useApplicationContext, useParticipants } from '@lib/common';
import { CustomAvatar, SmallLoading } from '@lib/ui-kit';

export interface UserFilterListProps {
  value: string[];
  onChange: (value: User[]) => void;
}

const UserFilterList: FC<UserFilterListProps> = ({ value = [], onChange }) => {
  const classes = useStyles({});
  const { user } = useApplicationContext();
  const { result } = useParticipants({
    skip: 0,
    take: 2000,
    category: 'USER',
  });

  const users = ((result.data?.search?.data || []) as User[]).filter(({ id }) => id !== user.id);

  const handleChange = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, item: User) => {
    let newValueIds: string[];

    if (value.includes(item.id)) {
      newValueIds = value.filter((id) => id != item.id);
    } else {
      newValueIds = [...value, item.id];
    }

    onChange(users.filter((user) => newValueIds.includes(user.id)));
  };

  return result.loading ? (
    <SmallLoading />
  ) : (
    <List style={{ width: '100%' }}>
      {users.map((item, index) => (
        <ListItem dense={true} button={true} key={`${item.id}`} onClick={(event) => handleChange(event, item)}>
          <Box display="flex" alignItems="center">
            <Checkbox checked={value.includes(item.id)} tabIndex={-1} disableRipple={true} />
            <ListItemText
              primary={
                <Box display="flex" alignItems="center">
                  <CustomAvatar url={item.userPhoto?.fichier?.publicUrl} name={item.userName} />
                  <Typography className={classes.title}>{item.userName}</Typography>
                </Box>
              }
            />
          </Box>
        </ListItem>
      ))}
    </List>
  );
};

export default UserFilterList;
