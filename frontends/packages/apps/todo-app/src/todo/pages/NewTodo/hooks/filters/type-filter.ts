import { MeUserInfoFragment } from '@lib/common/src/federation';
import { Filter } from '../util/types';
import { TodoURLParams } from '../useTodoURLParams';

export const buildTypeFilter = (user: MeUserInfoFragment, params: TodoURLParams): Filter => {
  if (params.type?.length) {
    return {
      must: [
        {
          terms: {
            status: params.type,
          },
        },
      ],
    };
  }

  return {};
};
