import { useApplicationContext } from '@lib/common';
import {
  useAggregate_Date_Histogram_By_DayLazyQuery,
  useSearch_Todo_ActionLazyQuery,
  useSearch_Todo_ActionQuery,
} from '@lib/common/src/federation';
import moment from 'moment';

import { useTodoURLParams } from '../hooks/useTodoURLParams';

export const useQueries = () => {
  const { getActionsQuery, params } = useTodoURLParams();
  const { federation } = useApplicationContext();

  const isToday = !!(params.date && moment().format('YYYY-MM-DD') === params.date);
  const isDoneType = params.type?.includes('DONE');

  const loadingActions = useSearch_Todo_ActionQuery({
    fetchPolicy: 'cache-and-network',
    client: federation,
    variables: {
      index: 'todoaction',
      skip: 0,
      take: 10,
      query: getActionsQuery(isToday && !isDoneType ? { date: 'today' } : undefined, {
        enableSort: true,
        includePast: isToday && !isDoneType,
      }),
    },
  });

  const [loadAggregateDataHistogramByDay, loadingAggregateDataHistogramByDay] =
    useAggregate_Date_Histogram_By_DayLazyQuery({
      fetchPolicy: 'cache-and-network',
      client: federation,
    });

  const [lazyLoadActions, lazyLoadingActions] = useSearch_Todo_ActionLazyQuery({
    fetchPolicy: 'network-only',
    client: federation,
  });

  return {
    loadingActions,
    loadAggregateDataHistogramByDay,
    loadingAggregateDataHistogramByDay,
    lazyLoadActions,
    lazyLoadingActions,
  };
};
