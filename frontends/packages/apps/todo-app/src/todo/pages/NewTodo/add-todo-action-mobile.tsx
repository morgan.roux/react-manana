import { makeVar, useReactiveVar } from '@apollo/client';
import { SearchTodoActionInfoFragment } from '@lib/common/src/federation';
import React, { FC, useState } from 'react';
import AjoutTaches from './Modals/AjoutTache';
export const openAddTodoActionMobile = makeVar<boolean>(false);

const AddTask: FC<{}> = ({}) => {
  const open = useReactiveVar<boolean>(openAddTodoActionMobile);
  const [todoAction, setTodoAction] = useState<SearchTodoActionInfoFragment>();

  return (
    <AjoutTaches
      operationName={''}
      task={todoAction}
      setTodoAction={setTodoAction}
      openModalTache={open}
      setOpenTache={openAddTodoActionMobile}
    />
  );
};

export default AddTask;
