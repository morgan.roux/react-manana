import { TodoURLParams } from '../useTodoURLParams';
import { Filter } from '../util/types';

export const buildSearchTextFilter = (params: TodoURLParams): Filter => {
  if (params.searchText) {
    return {
      must: [
        {
          query_string: {
            query:
              params.searchText.startsWith('*') || params.searchText.endsWith('*')
                ? params.searchText
                : `*${params.searchText}*`,
            analyze_wildcard: true,
          },
        },
      ],
    };
  }

  return {};
};
