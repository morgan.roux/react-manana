import React, { FC, useState } from 'react';
import { SearchTodoActionInfoFragment } from '@lib/common/src/federation';
import { Box, Checkbox, Icon, IconButton, Typography } from '@material-ui/core';

import 'react-virtualized/styles.css';
import { CSSProperties } from 'react';
import useStyles from './styles';
import {
  nl2br,
  TodoActionIcon,
  UrgenceImportanceBadge,
  useApplicationContext,
  useDisplayNotification,
} from '@lib/common';
import { ChatBubble, Dashboard, Today } from '@material-ui/icons';
import classnames from 'classnames';
import moment from 'moment';
import { computeCodeFromdateDebut } from '../../Common/UrgenceLabel/UrgenceLabel';
import PrivateIcon from '../../../../assets/icons/todo/private.svg';
import { isManuelAction } from '@lib/common';
import ActionContentPopover from './ActionContentPopover';
import BlockIcon from '@material-ui/icons/Block';
import CircleChecked from '@material-ui/icons/CheckCircleOutline';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import CircleUnchecked from '@material-ui/icons/RadioButtonUnchecked';
import { Backdrop, CustomAvatarGroup } from '@lib/ui-kit';
import ActionButton from '../../Common/ActionButton';
import Skeleton from './Skeleton';
//import Report from './Poppup/Report/Report';
import { useUpsertTodoAction } from '../../MainContent/useMutations';
import { getTodoActionInput } from '../../getTodoActionInput';
import ImportancePoppup from './Poppup/Importance';

export interface ActionProps {
  loading?: boolean;
  data?: SearchTodoActionInfoFragment;
  style?: CSSProperties;
  onRequestChangeStatutTodoAction: (todoAction: SearchTodoActionInfoFragment, newStatut: string) => void;
  onRequestEditTodoAction: (todoAction: SearchTodoActionInfoFragment) => void;
  onRequestOpenDeleteDialog: (data: any, title?: string) => void;
  onRequestOpenParticipantModal: (todoAction: SearchTodoActionInfoFragment) => void;
}

// SearchTodoActionInfo
export const Action = React.forwardRef<HTMLDivElement, ActionProps>(
  (
    {
      loading,
      data,
      style,
      onRequestEditTodoAction,
      onRequestOpenDeleteDialog,
      onRequestOpenParticipantModal,
      onRequestChangeStatutTodoAction,
    },
    containerRef
  ) => {
    const classes = useStyles();
    const { user: me, useParameterValueAsBoolean } = useApplicationContext();
    const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
    const displayNotification = useDisplayNotification();
    const [notificationMessage, setNotificationMessage] = useState<string>('');

    const onUpsertCompleted = () => {
      displayNotification({ type: 'success', message: notificationMessage });
      setOpenImportancePoppover(false);
      //  setOpenReportPoppover(false);
    };

    const { upsertTodoAction, upsertingTodoAction } = useUpsertTodoAction({ onCompleted: onUpsertCompleted });

    const isTM = data?.item?.code === 'PLAN_MARKETING_TYPE';

    const [openPopover, setOpenPopover] = useState<boolean>(false);

    const [popoverItem, setPopoverItem] = useState<string>('');

    //  const [openReportPoppover, setOpenReportPoppover] = useState<boolean>(false);

    const [openImportancePoppover, setOpenImportancePoppover] = useState<boolean>(false);

    const [popoverSection, setPopoverSection] = useState<string>();

    const popoverId = openPopover ? popoverItem : undefined;

    const useMatriceResponsabilite = useParameterValueAsBoolean('0501');

    const disabled =
      (data?.participants && !data?.participants.length && data?.createdBy && me && data?.createdBy.id === me.id) ||
      (data?.participants &&
        data?.participants.length &&
        me &&
        data?.participants.map((assign: any) => assign.id).includes(me.id))
        ? false
        : !useMatriceResponsabilite;

    const handleDateClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
      event.stopPropagation();
      // setOpenReportPoppover(true);
      //  setNotificationMessage('Tâche reporté avec succès');
    };

    const handleImportanceClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
      event.stopPropagation();
      setOpenImportancePoppover(true);
      setNotificationMessage('Importance modifié avec succès');
    };

    const handleCommentClick = (event: React.MouseEvent<HTMLElement, MouseEvent>) => {
      // if (setOpenTaskDetail) setOpenTaskDetail(true);
      event.stopPropagation();
    };

    const handleReportTodoAction = (values: any) => {
      if (data) {
        const input = getTodoActionInput(data);
        upsertTodoAction({ variables: { input: { ...input, ...values } } });
      }
    };

    const handleEditTodoActionImportance = (importance: any) => {
      if (data && importance.id !== data?.importance?.id) {
        const input = getTodoActionInput(data);
        upsertTodoAction({ variables: { input: { ...input, idImportance: importance.id } } });
      }
    };

    if (loading) {
      return (
        <div style={style} ref={containerRef} className={classes.root}>
          <Skeleton />
        </div>
      );
    }

    const bottomItems = (
      <Box className={classes.contentLabels}>
        {data?.dateDebut && (
          <Box className={classes.labels} onClick={handleDateClick}>
            <Icon color="secondary">
              <Today />
            </Icon>
            <Typography className={classnames(classes.labelText, classes.date)} color="primary">
              {data?.dateFin
                ? `du ${moment.utc(data.dateDebut).format('DD MMM')} au ${moment.utc(data.dateFin).format('DD MMM')}`
                : moment.utc(data?.dateDebut).format('DD MMM')}
            </Typography>
          </Box>
        )}
        <Box className={classes.labels}>
          <Icon onClick={handleCommentClick}>
            <ChatBubble />
          </Icon>
          <Typography className={classnames(classes.labelText)}>{data?.nombreCommentaires}</Typography>
        </Box>
        <Box className={classes.labels} onClick={handleImportanceClick}>
          <UrgenceImportanceBadge
            importanceOrdre={data?.importance?.ordre || 2}
            urgenceCode={computeCodeFromdateDebut(data?.dateDebut)}
          />
        </Box>
        {data?.projet?.nom && (
          <Box className={classes.labels}>
            <Typography className={classnames(classes.labelOrigine)}>Fonction</Typography>:
            <Typography className={classnames(classes.labelText)}>{data?.projet?.nom}</Typography>
          </Box>
        )}
        {data?.isPrivate && (
          <Box className={classes.labels}>
            <img src={PrivateIcon} style={{ width: 28 }} />
          </Box>
        )}
        {isTM && (
          <Box className={classes.labels}>
            <Dashboard />
          </Box>
        )}
        <Box className={classes.labels}>
          <TodoActionIcon action={data} />
        </Box>
      </Box>
    );

    return (
      <>
        <Backdrop open={upsertingTodoAction.loading} value="Modification en cours..." />
        <div style={{ ...(style || {}), padding: 10 }} ref={containerRef}>
          <div
            className={classnames(classes.root, classes.tasks)}
            onClick={() => data && onRequestEditTodoAction(data)}
          >
            <Box display="flex" justifyContent="space-between" alignItems="center" width="100%">
              <Box display="flex" alignItems="center">
                <Checkbox
                  icon={disabled ? <BlockIcon fontSize="large" /> : <CircleUnchecked fontSize="large" />}
                  checkedIcon={<CircleChecked fontSize="large" />}
                  onClick={(event) => {
                    event.preventDefault();
                    event.stopPropagation();

                    data?.status && onRequestChangeStatutTodoAction(data, data?.status === 'DONE' ? 'ACTI}VE' : 'DONE');
                  }}
                  checked={data?.status === 'DONE'}
                  disabled={disabled}
                  color="primary"
                  style={{ color: '#757575' }}
                  classes={{ checked: classes.checkedBox }}
                />
                <Typography
                  className={classes.taskTitle}
                  dangerouslySetInnerHTML={{ __html: nl2br(data?.description || '') } as any}
                />
              </Box>

              <Box
                display="flex"
                justifyContent="center"
                alignItems="center"
                // tslint:disable-next-line: jsx-no-lambda
                onClick={(event) => {
                  event.stopPropagation();
                  event.preventDefault();
                }}
              >
                <Box
                  onClick={(event) => {
                    event.stopPropagation();
                    event.preventDefault();
                  }}
                  display="flex"
                >
                  {
                    <IconButton onClick={() => data && onRequestOpenParticipantModal(data)}>
                      {data?.participants && data.participants.length > 0 ? (
                        <CustomAvatarGroup users={(data.participants as any) || []} max={5} />
                      ) : isManuelAction(data) ? (
                        <PersonAddIcon />
                      ) : null}
                    </IconButton>
                  }

                  <ActionButton
                    actionMenu="TODO_ACTION_MENU"
                    todoAction={data}
                    onRequestEditTodoAction={onRequestEditTodoAction}
                    onRequestOpenDeleteDialog={onRequestOpenDeleteDialog}
                  />
                </Box>
              </Box>
            </Box>
            {bottomItems}
          </div>
          {/* <Report
            onRequestReport={handleReportTodoAction}
            todoAction={data}
            open={openReportPoppover}
            setOpen={setOpenReportPoppover}
         />*/}
          <ImportancePoppup
            onRequestEdit={handleEditTodoActionImportance}
            todoAction={data}
            open={openImportancePoppover}
            setOpen={setOpenImportancePoppover}
          />
        </div>
        <ActionContentPopover
          id={popoverId}
          anchorEl={anchorEl}
          open={openPopover}
          setOpen={setOpenPopover}
          section={popoverSection}
          action={data as any}
        />
      </>
    );
  }
);
