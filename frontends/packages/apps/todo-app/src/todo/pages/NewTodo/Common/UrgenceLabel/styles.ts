import { createStyles, Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      color: '#212121',
      fontFamily: 'Roboto',
      marginLeft: 10,
      fontSize: '0.875rem',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      '-webkit-line-clamp': 1,
      '-webkit-box-orient': 'vertical',
      maxWidth: 280,
    },
  })
);
