import { Box, Icon, List, ListItem, Typography } from '@material-ui/core';
import React, { ReactNode } from 'react';
import useStyles from './styles';

interface FavorisList {
  icon: ReactNode;
  title: string;
  loading?: boolean;
  count?: number;
  path: string;
  isActive?: boolean;
  onClick: () => void;
}

interface FarovisHeaderProps {
  headerList: FavorisList[];
  setShowMainContent?: (value: boolean) => void;
}

const FavorisHeader: React.FC<FarovisHeaderProps> = ({ headerList, setShowMainContent }) => {
  const classes = useStyles({});

  const handleClick = () => {
    if (setShowMainContent) setShowMainContent(true);
  };

  return (
    <List className={classes.root}>
      {headerList.map((favoris, index) => (
        <ListItem
          button={true}
          key={index}
          onClick={() => {
            favoris.onClick();
            handleClick();
          }}
          selected={favoris.isActive}
        >
          <Box width="100%" display="flex" alignItems="center" justifyContent="space-between">
            <Box display="flex" alignItems="center">
              <Icon color="secondary">{favoris.icon}</Icon>
              <Typography className={classes.title}>{favoris.title}</Typography>
            </Box>
            <Box>
              <Typography className={classes.count}>{favoris.loading ? '-' : favoris.count || 0}</Typography>
            </Box>
          </Box>
        </ListItem>
      ))}
    </List>
  );
};

export default FavorisHeader;
