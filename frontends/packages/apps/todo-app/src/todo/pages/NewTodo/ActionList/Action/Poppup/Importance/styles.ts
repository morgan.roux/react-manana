import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: 32,
    },
    dateContainer: {
      display: 'flex',
      '& .MuiFormControl-root:nth-child(1)': {
        marginRight: 16,
      },
    },
  })
);

export default useStyles;
