import { todoBulkCountVar, useApplicationContext } from '@lib/common';
import {
  useSearch_Todo_ProjetQuery,
  useBulk_CountQuery,
  Bulk_CountQueryVariables,
  Bulk_CountQuery,
} from '@lib/common/src/federation';
import { ListItem, ListItemText, Radio, Typography, Box, Hidden } from '@material-ui/core';
import { useTheme } from '@material-ui/core';
import { Search as SearchIcon } from '@material-ui/icons';
import React, { FC, useState } from 'react';
import Search from './Filters/Search';
import { useTodoURLParams } from './hooks/useTodoURLParams';
import Sider from './NavBar/Sider';
import useStyles from './styles';
import { CountTodo } from './util/types';
import MainContent from './MainContent';
import { ApolloQueryResult } from '@apollo/client';
import { useEffect } from 'react';
import { useQueries } from './MainContent/useQueries';
import { useMutations } from './MainContent/useMutations';

export interface CountTodosProps {
  refetchCountTodos?: (variables?: Bulk_CountQueryVariables | undefined) => Promise<ApolloQueryResult<Bulk_CountQuery>>;
  countTodos?: any;
}

const Todo: FC<{}> = () => {
  const { federation, isMd, isMobile, setShowAppBar, user } = useApplicationContext();

  const { params, redirectTo, getActionsQuery } = useTodoURLParams();

  const { loadingActions } = useQueries();

  const classes = useStyles({});

  const [showMainContent, setShowMainContent] = useState<boolean>(true);

  useEffect(() => {
    if (!isMobile) {
      setShowAppBar(true);
    } else {
      setShowAppBar(!showMainContent);
    }
  }, [isMobile, showMainContent]);

  const todoActionsCount = useBulk_CountQuery({
    client: federation,
    variables: {
      queries: {
        pastMe: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['me'], date: 'past', projet: undefined }),
        },
        todayMe: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['me'], date: 'today', projet: undefined }),
        },
        nextMe: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['me'], date: 'next', projet: undefined }),
        },
        pastTeam: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['team'], date: 'past', projet: undefined }),
        },
        todayTeam: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['team'], date: 'today', projet: undefined }),
        },
        nextTeam: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['team'], date: 'next', projet: undefined }),
        },
        active: {
          index: 'todoaction',
          ...getActionsQuery({ type: ['ACTIVE'], assignee: ['me'], date: undefined, projet: undefined }),
        },
        activeTeam: {
          index: 'todoaction',
          ...getActionsQuery({ type: ['ACTIVE'], assignee: ['team'], date: undefined, projet: undefined }),
        },
        done: {
          index: 'todoaction',
          ...getActionsQuery({ type: ['DONE'], assignee: ['me'], date: undefined, projet: undefined }),
        },
        doneTeam: {
          index: 'todoaction',
          ...getActionsQuery({ type: ['DONE'], assignee: ['team'], date: undefined, projet: undefined }),
        },
        totalMe: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['me'], date: undefined, projet: undefined }),
        },
        totalTeam: {
          index: 'todoaction',
          ...getActionsQuery({ assignee: ['team'], date: undefined, projet: undefined }),
        },
      },
    },
  });

  useEffect(() => {
    if (todoActionsCount.data?.bulkCount) {
      todoBulkCountVar({
        pastMe: todoActionsCount.data?.bulkCount.pastMe || 0,
        todayMe: todoActionsCount.data?.bulkCount.todayMe || 0,
      });
    }
  }, [todoActionsCount.data?.bulkCount]);

  const loadingProjets = useSearch_Todo_ProjetQuery({
    client: federation,
    variables: {
      index: 'todoprojet',
      query: {
        query: {
          bool: {
            must: [
              {
                term: { supprime: false },
              },
              {
                term: { 'participants.id': user.id },
              },
            ],
          },
        },
      },
    },
  });

  const { loadAggregateDataHistogramByDay, loadingAggregateDataHistogramByDay } = useQueries();

  const refetchAll = () => {
    todoActionsCount.refetch && todoActionsCount.refetch();
    loadingActions.refetch();
    loadingProjets.refetch();
    loadingAggregateDataHistogramByDay?.refetch && loadingAggregateDataHistogramByDay.refetch();
  };

  const { updateActionStatus } = useMutations({
    refetch: refetchAll,
  });

  const handleDrawerToggle = () => {
    setShowMainContent((prev) => !prev);
  };

  const countTodo: CountTodo = {
    loading: todoActionsCount.loading,
    ...(todoActionsCount.data?.bulkCount || {}),
  };

  const header = (
    <>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <ListItem
          role={undefined}
          dense={true}
          disableGutters
          button={true}
          onClick={() => redirectTo({ type: ['ACTIVE'], searchText: undefined })}
        >
          <Box display="flex" alignItems="center">
            <Radio checked={params.type?.includes('ACTIVE')} tabIndex={-1} disableRipple={true} />
            <ListItemText primary="A faire" />
          </Box>
          <Typography className={classes.count}>
            {countTodo.loading ? '-' : (countTodo.active || 0) + (countTodo.activeTeam || 0)}
          </Typography>
        </ListItem>
        <ListItem
          role={undefined}
          dense={true}
          disableGutters
          button={true}
          onClick={() => redirectTo({ type: ['DONE'], searchText: undefined })}
        >
          <Box display="flex" alignItems="center">
            <Radio checked={params.type?.includes('DONE')} tabIndex={-1} disableRipple={true} />
            <ListItemText primary="Terminée(s)" />
          </Box>
          <Typography className={classes.count}>
            {countTodo.loading ? '-' : (countTodo.done || 0) + (countTodo.doneTeam || 0)}
          </Typography>
        </ListItem>
      </Box>
      <Search
        updateActionStatus={(action, status) => {
          updateActionStatus({
            variables: {
              input: {
                idAction: action.id,
                status: status as any,
              },
            },
          });
        }}
        placeholder={'Rechercher des tâches'}
        refetchAll={refetchAll}
        popupIcon={<SearchIcon />}
      />
    </>
  );

  return (
    <Box className={classes.root}>
      {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
      <Hidden smDown={true} implementation="css">
        <nav className={classes.drawerWeb} aria-label="mailbox folders">
          <Box className={classes.drawerContentRoot}>
            {header}
            <Sider loadingProjets={loadingProjets} setShowMainContent={setShowMainContent} counting={countTodo} />
          </Box>
        </nav>
      </Hidden>
      {isMobile && !showMainContent && (
        <nav className={classes.drawer} aria-label="mailbox folders">
          <Box className={classes.drawerContentRoot}>
            {header}
            <Sider loadingProjets={loadingProjets} setShowMainContent={setShowMainContent} counting={countTodo} />
          </Box>
        </nav>
      )}

      {/*isMobile && (
        <Drawer
          container={window.document.body}
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={!showMainContent}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          <nav className={classes.drawerWeb} aria-label="mailbox folders">
            <Box className={classes.drawerContentRoot}>
              {header}
              <Sider loadingProjets={loadingProjets} setShowMainContent={setShowMainContent} counting={countTodo} />
            </Box>
          </nav>
        </Drawer>
      )*/}

      {(!isMobile || showMainContent) && (
        <main className={classes.content}>
          <MainContent
            refetchAll={refetchAll}
            loadAggregateDataHistogramByDay={(variables) => loadAggregateDataHistogramByDay({ variables })}
            loadingAggregateDataHistogramByDay={loadingAggregateDataHistogramByDay as any}
            counting={countTodo}
            refetchCounts={todoActionsCount.refetch}
            filters={[]}
            loadingProjets={loadingProjets}
            loadingActions={loadingActions}
            setShowMainContent={setShowMainContent}
            // countTodos={{}}
            parameters={[]}
            // loadingTask={fetchingMore || loadingTask || loadingTodayTask}
            // refetchTask={handleRefetchTask}
            // projectList={projectList}
            // sectionQuery={sectionQuery}
            // refetchSection={refetchSection}
            // refetchAll={refetchAll}
            handleDrawerToggle={handleDrawerToggle}
            // useMatriceResponsabilite={enableMatriceResponsabilite}
            // fetchingMoreActions={fetchingMore}
            // fetchMoreActions={handleFetchMoreActions}
            // fetchMoreTodayActions={handleFetchMoreTodayActions}
          />
        </main>
      )}

      {/* <ConcernedParticipant
        isOpen={openConcernedParticipantModal}
        setOpen={setOpenConcernedParticipantModal}
        title="Collègue(s) Concernée(s)"
        isOnEquip={true}
      /> */}
    </Box>
  );
};

export default Todo;
