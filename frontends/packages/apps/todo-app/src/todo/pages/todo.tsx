import { Typography, Box } from '@material-ui/core';

import React, { FC } from 'react';
import { Helmet } from 'react-helmet';

const Page: FC<{}> = () => {
  return (
    <Box>
      <Helmet>
        <title>To-Do</title>
      </Helmet>
      <Typography>Ajourd'hui </Typography>
    </Box>
  );
};

export default Page;
