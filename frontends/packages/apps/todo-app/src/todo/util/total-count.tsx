import {
  MeUserInfoFragment,
  Bulk_CountQuery,
  Bulk_CountDocument,
  Bulk_CountQueryVariables,
} from '@lib/common/src/federation';
import { ApolloClient, useReactiveVar } from '@apollo/client';
import { buildTodoActionsQuery } from '../pages/NewTodo/hooks/util/query';
import { todoBulkCountVar } from '@lib/common';

export const fetchTodoTotalCount = async (context: {
  user: MeUserInfoFragment;
  graphql: ApolloClient<any>;
  federation: ApolloClient<any>;
}): Promise<number> => {
  const response = await context.federation.query<Bulk_CountQuery, Bulk_CountQueryVariables>({
    query: Bulk_CountDocument,
    variables: {
      queries: {
        pastMe: {
          index: 'todoaction',
          ...buildTodoActionsQuery({
            user: context.user,
            params: {
              date: 'past',
              assignee: ['me'],
              type: ['ACTIVE'],
              projet: undefined,
            },
            urgences: [],
            options: {
              includePast: false,
            },
          }),
        },
        todayMe: {
          index: 'todoaction',
          ...buildTodoActionsQuery({
            user: context.user,
            params: {
              date: 'today',
              assignee: ['me'],
              type: ['ACTIVE'],
              projet: undefined,
            },
            urgences: [],
            options: {
              includePast: false,
            },
          }),
        },
      },
    },
    fetchPolicy: 'network-only',
  });
  todoBulkCountVar(response?.data?.bulkCount);
  return (response?.data?.bulkCount?.pastMe || 0) + (response?.data?.bulkCount?.todayMe || 0);
};
