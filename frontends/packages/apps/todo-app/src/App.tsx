import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import { ApolloProvider } from '@apollo/client';
import { useInitClient, NotificationSnackBar } from '@lib/common';
import module from './module';

const App = () => {
  const { federation } = useInitClient();

  return (
    <ApolloProvider client={federation}>
      <NotificationSnackBar />
      <HashRouter>
        <Switch>
          {module.routes?.map((route, index) => (
            <Route
              key={typeof route.path === 'string' ? route.path : `route-${index}`}
              path={route.path}
              component={route.component}
              exact={route.exact}
            />
          ))}
        </Switch>
      </HashRouter>
    </ApolloProvider>
  );
};

export default App;
