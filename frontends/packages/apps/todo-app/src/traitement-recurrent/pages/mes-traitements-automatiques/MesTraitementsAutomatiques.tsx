import { useApplicationContext } from '@lib/common';
import {
  FullTraitementInfoFragment,
  useChanger_Statut_Traitement_ExecutionMutation,
  useGet_Traitement_Automatiques_ExecutionsLazyQuery,
  useGet_Traitement_Automatiques_Executions_AggregateLazyQuery,
} from '@lib/common/src/federation';
import moment from 'moment';
import React, { FC } from 'react';
import { Helmet } from 'react-helmet';
import { useHistory } from 'react-router';
import { MesTraitementsAutomatiquesPage } from '../../components/TraitementAutomatiquePage';
import useSearch from '../../utils/useSearch';

const MesTraitementsAutomatiques: FC<{}> = () => {
  const { currentPharmacie: pharmacie, federation } = useApplicationContext();
  const history = useHistory();

  const [search, searchResult] = useSearch<FullTraitementInfoFragment>();
  const [loadPreviousAggregate, loadingPreviousAggregate] =
    useGet_Traitement_Automatiques_Executions_AggregateLazyQuery({
      client: federation,
    });
  const [loadNextAggregate, loadingNextAggregate] = useGet_Traitement_Automatiques_Executions_AggregateLazyQuery({
    client: federation,
  });

  const [searchPreviousExecutions, searchingPreviousExecutions] = useGet_Traitement_Automatiques_ExecutionsLazyQuery({
    client: federation,
  });

  const [searchNextExecutions, searchingNextExecutions] = useGet_Traitement_Automatiques_ExecutionsLazyQuery({
    client: federation,
  });

  const [changeStatut, ChangingStatut] = useChanger_Statut_Traitement_ExecutionMutation({
    client: federation,
    onCompleted: () => {
      if (loadingPreviousAggregate.refetch) loadingPreviousAggregate.refetch();
      if (loadingNextAggregate.refetch) loadingNextAggregate.refetch();
      if (searchingPreviousExecutions.refetch) searchingPreviousExecutions.refetch();
      if (searchingNextExecutions.refetch) searchingNextExecutions.refetch();
    },
  });

  const handleRequestGoBack = (): void => {
    history.push('/');
  };

  const handleConsultToDo = () => {
    history.push('/todo/aujourdhui');
  };

  return (
    <>
      <Helmet>
        <title>Mes Tâches Récurrentes</title>
      </Helmet>
      <MesTraitementsAutomatiquesPage
        previousExecutions={{
          data: searchingPreviousExecutions.data?.tATraitementExecutions.nodes as any,
          loading: searchingPreviousExecutions.loading || loadingPreviousAggregate.loading,
          error: searchingPreviousExecutions.error || loadingPreviousAggregate.error,
          rowsTotal:
            (loadingPreviousAggregate.data?.tATraitementExecutionAggregate.length || 0) > 0
              ? loadingPreviousAggregate.data?.tATraitementExecutionAggregate[0].count?.id || 0
              : 0,
          onRunSearch: (traitement, change) => {
            const now = moment().toISOString();

            loadPreviousAggregate({
              variables: {
                filter: {
                  and: [
                    {
                      idPharmacie: {
                        eq: pharmacie.id,
                      },
                    },

                    {
                      idTraitement: {
                        eq: traitement.id,
                      },
                    },
                    {
                      dateEcheance: {
                        gt: now,
                      },
                    },
                  ],
                },
              },
            });
            searchPreviousExecutions({
              variables: {
                filter: {
                  and: [
                    {
                      idPharmacie: {
                        eq: pharmacie.id,
                      },
                    },

                    {
                      idTraitement: {
                        eq: traitement.id,
                      },
                    },
                    {
                      dateEcheance: {
                        gt: now,
                      },
                    },
                  ],
                },
                paging: {
                  offset: change.skip,
                  limit: change.take,
                },
                sorting: [{ field: 'dateEcheance', direction: 'ASC' } as any],
              },
            });
          },
        }}
        nextExecutions={{
          data: searchingNextExecutions.data?.tATraitementExecutions.nodes as any,
          loading: searchingNextExecutions.loading || loadingNextAggregate.loading,
          error: searchingNextExecutions.error || loadingNextAggregate.error,
          rowsTotal:
            (loadingNextAggregate.data?.tATraitementExecutionAggregate.length || 0) > 0
              ? loadingNextAggregate.data?.tATraitementExecutionAggregate[0].count?.id || 0
              : 0,
          onRunSearch: (traitement, change) => {
            const now = moment().toISOString();

            loadNextAggregate({
              variables: {
                filter: {
                  and: [
                    {
                      idPharmacie: {
                        eq: pharmacie.id,
                      },
                    },
                    {
                      idTraitement: {
                        eq: traitement.id,
                      },
                    },
                    {
                      dateEcheance: {
                        lte: now,
                      },
                    },
                  ],
                },
              },
            });

            searchNextExecutions({
              variables: {
                filter: {
                  and: [
                    {
                      idPharmacie: {
                        eq: pharmacie.id,
                      },
                    },
                    {
                      idTraitement: {
                        eq: traitement.id,
                      },
                    },
                    {
                      dateEcheance: {
                        lte: now,
                      },
                    },
                  ],
                },
                paging: {
                  offset: change.skip,
                  limit: change.take,
                },
                sorting: [{ field: 'dateEcheance', direction: 'ASC' } as any],
              },
            });
          },
        }}
        onRequestGoBack={handleRequestGoBack}
        onConsultTodo={handleConsultToDo}
        onRequestSearch={(change) => {
          const must: any[] = [{ term: { idPharmacie: pharmacie.id } }];
          // { term: { "participants.id": currentUser.id } },
          if (change.searchText) {
            must.push({
              query_string: {
                query: `*${change.searchText}*`,
                analyze_wildcard: true,
                fields: [],
              },
            });
          }

          if (change.statut && change.statut !== 'ALL') {
            must.push({ term: { 'execution.dernierChangementStatut.status': change.statut } });
          }

          search({
            variables: {
              skip: change.skip,
              take: change.take,
              query: {
                query: {
                  bool: {
                    must_not: [{ term: { termine: true } }],
                    must,
                  },
                },
                sort: [{ 'execution.dateEcheance': { order: 'asc' } }],
              },
              index: ['tatraitement'],
            },
          });
        }}
        onRequestChangeStatut={(traitement, execution, newStatut) => {
          changeStatut({
            variables: {
              input: {
                idTraitementExecution: execution.id,
                commentaire: newStatut.commentaire || '',
                status: newStatut.status,
              },
            },
          });
        }}
        loading={searchResult.loading}
        error={searchResult.error}
        data={(searchResult.data || []) as any}
        rowsTotal={searchResult.total || 0}
        refetch={searchResult.refetch}
      />
    </>
  );
};

export default MesTraitementsAutomatiques;
