import { useApplicationContext } from '@lib/common';
import {
  FullTraitementInfoFragment,
  useCreate_Traitement_AutomatiqueMutation,
  useDelete_Traitement_AutomatiqueMutation,
  useGet_Traitement_Automatiques_TypesQuery,
  useUpdate_Traitement_AutomatiqueMutation,
} from '@lib/common/src/federation';
import React, { FC, useState } from 'react';
import { TraitementAutomatiquePage } from '../../components/TraitementAutomatiquePage';
import useSearch from '../../utils/useSearch';

const TraitementAutomatique: FC<{}> = () => {
  const [search, searchResult] = useSearch<FullTraitementInfoFragment>();
  const [saved, setSaved] = useState<boolean>(false);
  const [saving, setSaving] = useState<boolean>(false);

  const { currentPharmacie: pharmacie, federation, notify } = useApplicationContext();
  const idPharmacie = pharmacie.id;

  // const filterTraitementAutomatique = {
  //   filter: {
  //     idPharmacie: {
  //       eq: idPharmacie,
  //     },
  //   },
  // };

  const loadTraitementTypes = useGet_Traitement_Automatiques_TypesQuery({
    client: federation,
  });

  const [createTraitement] = useCreate_Traitement_AutomatiqueMutation({
    onCompleted: () => {
      notify({
        message: 'La Tâche récurrente a été créée avec succès !',
        type: 'success',
      });

      setSaved(true);
      setSaving(false);

      searchResult.refetch && searchResult.refetch();
    },
    /*update: (cache, result) => {
      if (result.data?.createOneTATraitement) {
        const previousList = cache.readQuery<
          GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_TYPE,
          GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTIONVariables
        >({
          query: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION,
          variables: filterTraitementAutomatique,
        })?.tATraitements;

        cache.writeQuery<
          GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_TYPE,
          GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTIONVariables
        >({
          query: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION,
          variables: filterTraitementAutomatique,
          data: {
            tATraitements: [...(previousList || []), result.data.createOneTATraitement],
          },
        });
      }
    },*/

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la création de Tâche Récurrente',
        type: 'error',
      });

      setSaving(false);
    },
    client: federation,
  });

  const [updateTraitement] = useUpdate_Traitement_AutomatiqueMutation({
    onCompleted: () => {
      notify({
        message: 'La Tâche Récurrente a été modifiée avec succès !',
        type: 'success',
      });

      setSaved(true);
      setSaving(false);
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la modification de Tâche Récurrente',
        type: 'error',
      });

      setSaving(false);
    },
    client: federation,
  });

  const [deleteTraitement] = useDelete_Traitement_AutomatiqueMutation({
    onCompleted: () => {
      notify({
        message: 'La Tâche Récurrente a été supprimée avec succès !',
        type: 'success',
      });

      // Temporary solution

      setTimeout(() => {
        searchResult.refetch && searchResult.refetch();
      }, 2000);
    },
    /* update: (cache, result) => {
       if (result.data?.deleteOneTATraitement) {
         const previousList = cache.readQuery<
           GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_TYPE,
           GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTIONVariables
         >({
           query: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION,
           variables: filterTraitementAutomatique,
         })?.tATraitements;
 
         cache.writeQuery<
           GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_TYPE,
           GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTIONVariables
         >({
           query: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION,
           variables: filterTraitementAutomatique,
           data: {
             tATraitements:
               previousList?.filter(
                 (traitement) => traitement.id !== result.data?.deleteOneTATraitement.id,
               ) || [],
           },
         });
       }
     },*/

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la suppression de Tâche Récurrente',
        type: 'error',
      });
    },
    client: federation,
  });

  const handleRequestSave = (traitement: any) => {
    // const projet = traitement.projet;
    setSaved(false);
    setSaving(true);
    if (traitement.id) {
      updateTraitement({
        variables: {
          id: traitement.id,
          input: {
            ...traitement,
            id: undefined,
            assignedUsers: undefined,
            idUserParticipants: traitement.assignedUsers.map(({ id }: any) => id),
          },
        },
      });
    } else {
      createTraitement({
        variables: {
          input: {
            ...traitement,
            assignedUsers: undefined,
            idUserParticipants: traitement.assignedUsers.map(({ id }: any) => id),
          },
        },
      });
    }
  };

  const handleDelete = (traitement: any) => {
    deleteTraitement({
      variables: {
        id: traitement.id,
      },
    });
  };

  return (
    <TraitementAutomatiquePage
      saved={saved}
      setSaved={setSaved}
      loading={searchResult.loading}
      saving={saving}
      error={searchResult.error}
      data={(searchResult.data || []) as any}
      rowsTotal={searchResult.total || 0}
      // traitementToEdit={traitementToEdit}
      // idUrgence={state.idUrgence || ''}
      // idImportance={state.idImportance || ''}
      // collaborateurs={assignedUsers}
      // idTache={state.idProject || ''}
      onRequestSearch={(change) => {
        const must: any[] = [{ term: { idPharmacie: idPharmacie } }];
        if (change.searchText) {
          must.push({
            query_string: {
              query: `*${change.searchText}*`,
              analyze_wildcard: true,
              fields: [],
            },
          });
        }

        if (change.statut !== 'ALL') {
          must.push({
            term: { 'execution.dernierChangementStatut.status': change.statut },
          });
        }

        search({
          variables: {
            skip: change.skip,
            take: change.take,
            query: {
              query: {
                bool: {
                  must,
                },
              },
            },
            index: ['tatraitement'],
          },
        });
      }}
      traitementTypes={{
        loading: loadTraitementTypes.loading,
        error: loadTraitementTypes.error,
        data: loadTraitementTypes.data?.tATraitementTypes.nodes as any,
      }}
      onRequestSave={handleRequestSave}
      onRequestDelete={handleDelete}
    />
  );
};

export default TraitementAutomatique;
