import { QueryLazyOptions } from '@apollo/client';
import {
  FullUserInfoFragment,
  SearchQueryVariables,
  SuiviAppelInfoFragment,
  FullTraitementInfoFragment,
  useSearchLazyQuery,
} from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';

interface SearchResult<T> {
  loading?: boolean;
  called?: boolean;
  total?: number;
  data?: T[];
  error?: Error;
  refetch?: () => void;
}

export type SEARCH_search_data = SuiviAppelInfoFragment | FullUserInfoFragment | FullTraitementInfoFragment;

const useSearch = <T extends SEARCH_search_data>(
  immediate: boolean = false
): [(options: QueryLazyOptions<SearchQueryVariables> | undefined) => void, SearchResult<T>] => {
  const { federation } = useApplicationContext();
  const [search, { loading, data, error, called, refetch }] = useSearchLazyQuery({
    client: federation,
    fetchPolicy: 'cache-and-network',
  });

  return [
    search,
    {
      loading,
      data: data?.search?.data as any,
      total: data?.search?.total,
      error,
      called,
      refetch,
    },
  ];
};

export default useSearch;
