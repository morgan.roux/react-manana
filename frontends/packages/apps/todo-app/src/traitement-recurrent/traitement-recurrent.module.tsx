import React from 'react';
import loadable from '@loadable/component';
import { ModuleDefinition } from '@lib/common';
import { SettingsApplications } from '@material-ui/icons';
import TraitementAutomatiqueIcon from '@material-ui/icons/SettingsApplications';
import { SmallLoading } from '@lib/ui-kit';

const MesTraitementsAutomatiquesPage = loadable(
  () => import('./pages/mes-traitements-automatiques/MesTraitementsAutomatiques'),
  {
    fallback: <SmallLoading />,
  }
);

const TraitementAutomatiquePage = loadable(() => import('./pages/traitement-automatique/TraitementAutomatique'), {
  fallback: <SmallLoading />,
});

const traitementRecurrentModule: ModuleDefinition = {
  features: [
    {
      id: 'FEATURE_PARAMETRE_TRAITEMENT_RECURRENT',
      location: 'PARAMETRE',
      name: 'Tâche Récurrente',
      to: '/traitement-automatique',
      icon: <SettingsApplications />,
      preferredOrder: 90,
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },

    {
      id: 'FEATURE_FAVORITE_TRAITEMENT_RECURRENT',
      location: 'FAVORITE',
      name: 'Mes Tâches Récurrentes',
      to: '/outils-digitaux/traitements-automatiques',
      icon: <TraitementAutomatiqueIcon />,
      preferredOrder: 20,
      activationParameters: {
        groupement: '0822',
        pharmacie: '0722',
      },
      authorize: {
        rolesDenied: ['PRTSERVICE']
      },
    },
  ],
  routes: [
    {
      attachTo: 'PARAMETRE',
      path: '/traitement-automatique',
      component: TraitementAutomatiquePage,
    },
    {
      attachTo: 'MAIN',
      path: '/outils-digitaux/traitements-automatiques',
      component: MesTraitementsAutomatiquesPage,
      mobileOptions: {
        topBar: {
          title: 'Mes Tâches Récurrentes',
          withBackBtn: true,
          withTopMargin: true,
          //onGoBack={onRequestGoBack}
        },
      },
    },
  ],
};

export default traitementRecurrentModule;
