import React, { FC, useState } from 'react';
import { User } from '@lib/types';
import { Box, IconButton } from '@material-ui/core';
import { Person } from '@material-ui/icons';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import { useStyles } from './styles';
import { NewCustomButton } from '@lib/ui-kit';

export interface AddUserProps {
  users: User[];
  onSubmit: (data: User[]) => void;
  setOpen: (open: boolean) => void;
}

const AddUser: FC<AddUserProps> = ({ users, onSubmit, setOpen }) => {
  const [selected, setSelected] = useState<User[]>([]);
  const [saving, setSaving] = useState<boolean>(false);
  const classes = useStyles({});

  const handleToggleUser = (user: User) => {
    const CollaborateursReduit = selected.filter((u) => user.id !== u.id);
    if (CollaborateursReduit.length === selected.length) {
      setSelected([...selected, user]);
    } else {
      setSelected(CollaborateursReduit);
    }
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    setSaving(true);
    event.preventDefault();
    event.stopPropagation();
    onSubmit(selected);
  };
  return (
    <form onSubmit={handleSubmit} className={classes.collaborateurForm}>
      {users.map((user) => {
        const checked = selected.some((u) => u.id === user.id);
        return (
          <Box key={user.id} className={classes.collaborateurBox}>
            <Person className={classes.personButton} />
            {user.userName}
            <IconButton className={classes.radioButton} onClick={() => handleToggleUser(user)}>
              {checked ? <CheckCircleOutlineIcon /> : <RadioButtonUncheckedIcon />}
            </IconButton>
          </Box>
        );
      })}
      <Box className={classes.buttonBox}>
        <NewCustomButton onClick={() => setOpen(false)} theme="transparent">
          <Box px={2}>Annuler</Box>
        </NewCustomButton>
        <NewCustomButton type="submit" disabled={saving}>
          <Box px={2}>Terminer</Box>
        </NewCustomButton>
      </Box>
    </form>
  );
};

export default AddUser;
