import React, { FC, useState } from 'react';
import { Box, Grid, Typography } from '@material-ui/core/';
import useStyles from './styles';
import classNames from 'classnames';
import { TATraitement, TATraitementExecution } from '@lib/types';
import { STATUTS } from './../../util';
import { Tab, Tabs } from '@material-ui/core';
import moment from 'moment';
import { Column, CustomAvatarGroup, CustomModal, CustomSelect, Table, TableChange, TabPanel } from '@lib/ui-kit';
interface ExecutionTableProps {
  onChangeStatut?: (
    traitement: TATraitement,
    execution: TATraitementExecution,
    newStatut: TraitementAutomatiqueChangementStatut
  ) => void;
  traitement: TATraitement;
  data?: TATraitementExecution[];
  loading?: boolean;
  error?: Error;
  rowsTotal?: number;
  onRunSearch?: (traitement: TATraitement, change: TableChange) => void;
}

const ExecutionTable: FC<ExecutionTableProps> = ({
  onChangeStatut,
  traitement,
  data,
  loading,
  error,
  rowsTotal,
  onRunSearch,
}) => {
  const columns: Column[] = [
    {
      name: 'dateEcheance',
      label: 'Date et heure',
      renderer: (row: TATraitementExecution) => {
        return row?.dateEcheance ? moment.utc(row.dateEcheance).format('DD/MM/YYYY HH:mm') : '-';
      },
    },
    {
      name: 'statut',
      label: 'Statut',
      renderer: (row: TATraitementExecution) => {
        return (
          <CustomSelect
            selectStyle={{ marginBottom: 0 }}
            noMinheight={true}
            disabled={row.dernierChangementStatut?.status === 'CLOTURE'}
            listId="value"
            index="label"
            label=""
            value={row.dernierChangementStatut.status}
            onChange={(event: any) => onChangeStatut && onChangeStatut(traitement, row, { status: event.target.value })}
            list={STATUTS}
          />
        );
      },
    },
  ];

  return (
    <Box style={{ padding: 10 }}>
      <Table
        error={error}
        search={false}
        loading={loading}
        data={data || []}
        selectable={false}
        columns={columns}
        rowsTotal={rowsTotal}
        onRunSearch={(change) => onRunSearch && onRunSearch(traitement, change)}
      />
    </Box>
  );
};

export interface TraitementAutomatiqueChangementStatut {
  status: string;
  commentaire?: string;
}

interface HistoriqueExecutionProps {
  onChangeStatut?: (
    traitement: TATraitement,
    execution: TATraitementExecution,
    newStatut: TraitementAutomatiqueChangementStatut
  ) => void;
  open: boolean;
  setOpen: (open: boolean) => void;
  traitement: TATraitement;
  previousExecutions: {
    data?: TATraitementExecution[];
    loading?: boolean;
    error?: Error;
    rowsTotal?: number;
    onRunSearch?: (traitement: TATraitement, change: TableChange) => void;
  };

  nextExecutions: {
    data?: TATraitementExecution[];
    loading?: boolean;
    error?: Error;
    rowsTotal?: number;
    onRunSearch?: (traitement: TATraitement, change: TableChange) => void;
  };
}

const HistoriqueExecution: FC<HistoriqueExecutionProps> = ({
  open,
  setOpen,
  onChangeStatut,
  traitement,
  previousExecutions,
  nextExecutions,
}) => {
  const classes = useStyles();

  const [activeTab, setActiveTab] = useState<number>(0);

  return (
    <CustomModal
      headerWithBgColor
      open={open}
      setOpen={setOpen}
      title="Historique de relance"
      withBtnsActions={false}
      closeIcon
      maxWidth="sm"
    >
      <Grid container className={classes.root}>
        <Grid container item xs={12} className={classes.gridItem}>
          <Grid item xs={12}>
            <div style={{ textAlign: 'left' }} dangerouslySetInnerHTML={{ __html: traitement.description }} />
          </Grid>
          <Grid
            container
            item
            xs={12}
            lg={12}
            sm={12}
            md={12}
            className={classNames(classes.gridItem, classes.patientGrid)}
          >
            <Box display="flex" flexDirection="row" justifyContent="space-between" flexWrap="wrap">
              {/* <Grid item xs={12} sm={6} lg={2} md={2}>
                <Typography variant="subtitle2" className={classes.subtitle1}>
                  Recurrence:
                </Typography>
                <Typography variant="body2">{traitement.recurrence}</Typography>
              </Grid> */}
              <Grid item xs={12} sm={6} lg={2} md={2}>
                <Typography variant="subtitle2" className={classes.subtitle1}>
                  Equipe:
                </Typography>
                <Box display="flex" flexDirection="row" justifyContent="flex-start">
                  <CustomAvatarGroup max={3} sizes="30px" users={(traitement.participants || []) as any} />
                </Box>
              </Grid>
              <Grid item xs={12} sm={6} lg={2} md={2}>
                <Typography variant="subtitle2" className={classes.subtitle1}>
                  En retard:
                </Typography>
                <Typography variant="body2">{traitement.nombreTraitementsEnRetard}</Typography>
              </Grid>
            </Box>
          </Grid>
        </Grid>

        <Grid container item xs={12} className={classes.gridItem}>
          <Tabs
            value={activeTab}
            indicatorColor="secondary"
            textColor="secondary"
            onChange={(_event, newValue) => setActiveTab(newValue)}
            aria-label="Historiques"
            style={{
              borderBottom: 'solid 1px #E0E0E0',
            }}
          >
            <Tab label="A venir" className={classes.titleTab} />
            <Tab label="Passés" className={classes.titleTab} />
          </Tabs>
          <TabPanel value={activeTab} index={0}>
            <ExecutionTable {...previousExecutions} traitement={traitement} onChangeStatut={onChangeStatut} />
          </TabPanel>
          <TabPanel value={activeTab} index={1}>
            <ExecutionTable {...nextExecutions} traitement={traitement} onChangeStatut={onChangeStatut} />
          </TabPanel>
        </Grid>
      </Grid>
    </CustomModal>
  );
};

export default HistoriqueExecution;
