/* eslint-disable react/jsx-no-bind */
import React, { /*ChangeEvent,*/ FC, useState, MouseEvent } from 'react';
import { Box, IconButton, Menu, MenuItem, Typography, Fade, Tooltip } from '@material-ui/core';
import { useStyles } from './styles';
import { TATraitement, TATraitementExecution, TATraitementType } from '@lib/types';
import CloseIcon from '@material-ui/icons/Close';
import moment from 'moment';
import { STATUTS } from './../util';
import { Add, FilterList } from '@material-ui/icons';
import HistoriqueExecution from './HistoriqueExecution';
import { strippedColumnStyle } from '@lib/ui-kit/src/services/Helpers';
import { CustomContainer } from '@lib/ui-kit/src/components/templates';
import {
  Column,
  CustomAvatarGroup,
  ImportanceLabel,
  NewCustomButton,
  Table,
  TableChange,
  UrgenceLabel,
} from '@lib/ui-kit';
import { TraitementAutomatiqueForm } from '../TraitementAutomatiqueForm';
import {
  useCreate_Traitement_AutomatiqueMutation,
  useGet_Traitement_Automatiques_TypesQuery,
} from '@lib/common/src/federation';
import { useApplicationContext } from '@lib/common';

export interface TraitementAutomatiqueChangementStatut {
  status: string;
  commentaire?: string;
}

interface SearchChange extends TableChange {
  statut: string;
}

export interface MesTraitementsAutomatiquesPageProps {
  loading?: boolean;
  error?: Error;
  data: TATraitement[];
  rowsTotal: number;
  onRequestChangeStatut: (
    traitement: TATraitement,
    execution: TATraitementExecution,
    newStatut: TraitementAutomatiqueChangementStatut
  ) => void;
  onRequestGoBack: (e: MouseEvent) => void;
  onRequestSearch: (change: SearchChange) => void;
  onConsultTodo?: () => void;
  refetch?: any;

  previousExecutions: {
    data?: TATraitementExecution[];
    loading?: boolean;
    error?: Error;
    rowsTotal?: number;
    onRunSearch?: (traitement: TATraitement, change: TableChange) => void;
  };

  nextExecutions: {
    data?: TATraitementExecution[];
    loading?: boolean;
    error?: Error;
    rowsTotal?: number;
    onRunSearch?: (traitement: TATraitement, change: TableChange) => void;
  };
}

const MesTraitementsAutomatiquesPage: FC<MesTraitementsAutomatiquesPageProps> = ({
  loading,
  error,
  data,
  rowsTotal,
  onRequestChangeStatut,
  onRequestGoBack,
  onRequestSearch,
  previousExecutions,
  nextExecutions,
  onConsultTodo,
  refetch,
}) => {
  const classes = useStyles();

  const [open, setOpen] = useState<boolean>(false);

  const { notify, federation } = useApplicationContext();

  const [tableChange, setTableChange] = useState<TableChange>({
    skip: 0,
    take: 12,
    searchText: '',
  });
  const [anchorFilterEl, setAnchorFilterEl] = useState<HTMLElement | null>(null);
  const [selectedStatut, setSelectedStatut] = useState<string>('ALL');
  const [traitement, setTraitement] = useState<TATraitement>();
  const [openEditDialog, setOpenEditDialog] = useState<boolean>(false);
  const [state, setState] = useState<any>({});
  const [isPrivate, setIsPrivate] = useState<boolean>(false);
  const [saved, setSaved] = useState<boolean>(false);
  const [saving, setSaving] = useState<boolean>(false);

  const openHistorique = (newTraitement: TATraitement) => {
    setTraitement(newTraitement);
    setOpen(true);
  };

  const handleShowMenuFilterClick = (event: MouseEvent<HTMLElement>): void => {
    setAnchorFilterEl(event.currentTarget);
  };

  const handleCloseFilter = (): void => {
    setAnchorFilterEl(null);
  };

  const handleChangeFilter = (newStatut: string): void => {
    setSelectedStatut(newStatut);
    onRequestSearch({
      ...tableChange,
      statut: newStatut,
    });

    handleCloseFilter();
  };

  const handleChangeTable = (change: TableChange): void => {
    setTableChange(change);
    onRequestSearch({
      ...change,
      statut: selectedStatut,
    });
  };

  const columns: Column[] = [
    {
      name: 'type',
      label: 'Type',
      renderer: (row: TATraitement) => {
        return (
          <Box onClick={() => openHistorique(row)} className={classes.cursor}>
            {row?.type ? row.type.libelle : '-'}
          </Box>
        );
      },
    },
    {
      name: 'recurrence',
      label: 'Récurrence',
      renderer: (row: TATraitement) => {
        return (
          <Box onClick={() => openHistorique(row)} className={classes.cursor}>
            {row.recurrence || '-'}
          </Box>
        );
      },
    },
    {
      name: 'date_debut',
      label: 'Date debut',
      renderer: (row: TATraitement) => {
        return row?.dateDebut ? moment(row?.dateDebut).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'urgence',
      label: 'Urgence',
      renderer: (row: TATraitement) => {
        return (
          <Box onClick={() => openHistorique(row)} className={classes.cursor}>
            <UrgenceLabel urgence={row.execution?.urgence} />
          </Box>
        );
      },
    },
    {
      name: 'importance',
      label: 'Importance',
      renderer: (row: TATraitement) => {
        return (
          <Box onClick={() => openHistorique(row)} className={classes.cursor}>
            <ImportanceLabel importance={row.execution?.importance as any} />
          </Box>
        );
      },
    },
    {
      name: 'description',
      label: 'Description Détaillée',
      renderer: (row: TATraitement) => {
        return (
          <Box onClick={() => openHistorique(row)} className={classes.cursor}>
            {row?.description ? <div dangerouslySetInnerHTML={{ __html: row.description }} /> : '-'}
          </Box>
        );
      },
      style: strippedColumnStyle,
    },

    {
      name: 'dateEcheance',
      label: "Date d'échéance",
      renderer: (row: TATraitement) => {
        return (
          <Box onClick={() => openHistorique(row)} className={classes.cursor}>
            {}
            {row?.execution?.dateEcheance ? moment.utc(row.execution.dateEcheance).format('DD/MM/YYYY HH:mm') : '-'}
          </Box>
        );
      },
    },

    {
      name: 'equipe',
      label: 'Equipe',
      renderer: (row: TATraitement) => {
        return (
          <Box
            display="flex"
            flexDirection="row"
            justifyContent="flex-start"
            onClick={() => openHistorique(row)}
            className={classes.cursor}
          >
            <CustomAvatarGroup max={3} sizes="30px" users={(row.participants || []) as any} />
          </Box>
        );
      },
    },
    {
      name: 'occurence',
      label: 'Occurence',
      renderer: (row: TATraitement) => {
        return (
          <Tooltip title="Consulter To-Do">
            <Box
              display="flex"
              flexDirection="row"
              justifyContent="flex-start"
              //  onClick={() => onConsultTodo && onConsultTodo()}
              className={classes.cursor}
            >
              {row.nombreOccurencesFin || '-'}
            </Box>
          </Tooltip>
        );
      },
    },
    {
      name: 'retart',
      label: 'Retard',
      renderer: (row: TATraitement) => {
        return (
          <Box
            display="flex"
            flexDirection="row"
            justifyContent="flex-start"
            onClick={() => openHistorique(row)}
            className={classes.cursor}
          >
            {row.nombreTraitementsEnRetard}
          </Box>
        );
      },
    },
  ];

  const loadTraitementTypes = useGet_Traitement_Automatiques_TypesQuery({
    client: federation,
  });

  const [createTraitement] = useCreate_Traitement_AutomatiqueMutation({
    onCompleted: () => {
      notify({
        message: 'La Tâche récurrente a été créée avec succès !',
        type: 'success',
      });

      setSaved(true);
      setSaving(false);

      refetch && refetch();
    },

    onError: () => {
      notify({
        message: 'Des erreurs se sont survenues pendant la création de Tâche Récurrente',
        type: 'error',
      });

      setSaving(false);
    },
    client: federation,
  });

  const handleRequestSave = (traitement: any) => {
    setSaved(false);
    setSaving(true);
    createTraitement({
      variables: {
        input: {
          ...traitement,
          assignedUsers: undefined,
          idUserParticipants: traitement.assignedUsers.map(({ id }: any) => id),
        },
      },
    });
  };

  const handleOpenAdd = () => {
    setOpenEditDialog(true);
  };

  return (
    <CustomContainer
      filled
      bannerTitle="Mes Tâches Récurrentes"
      bannerLeft={<></>}
      bannerRight={<CloseIcon onClick={onRequestGoBack} />}
      onBackClick={onRequestGoBack}
    >
      <Box className={classes.mainContainer}>
        <Table
          topBarComponent={(searchComponent) => (
            <Box className={classes.tableTopBar}>
              <NewCustomButton key={`action-button-${0}`} onClick={handleOpenAdd} startIcon={<Add />}>
                PROGRAMMER UNE TACHE RECURRENTE
              </NewCustomButton>
              <IconButton
                aria-controls="simple-menu"
                aria-haspopup="true"
                className={classes.filterButton}
                onClick={handleShowMenuFilterClick}
              >
                <FilterList /> FILTRER PAR
              </IconButton>
              <Menu
                id="fade-menu"
                anchorEl={anchorFilterEl}
                keepMounted
                open={Boolean(anchorFilterEl)}
                onClose={handleCloseFilter}
                TransitionComponent={Fade}
              >
                <MenuItem selected={selectedStatut === 'ALL'} onClick={() => handleChangeFilter('ALL')}>
                  <Typography variant="inherit">Tout</Typography>
                </MenuItem>

                {STATUTS.map((statut) => (
                  <MenuItem selected={selectedStatut === statut.value} onClick={() => handleChangeFilter(statut.value)}>
                    <Typography variant="inherit">{statut.completedLabel}</Typography>
                  </MenuItem>
                ))}
              </Menu>
              {searchComponent}
            </Box>
          )}
          error={error}
          search={false}
          loading={loading}
          data={data || []}
          selectable={false}
          columns={columns}
          rowsTotal={rowsTotal}
          onRunSearch={handleChangeTable}
        />
      </Box>
      {open && traitement && (
        <HistoriqueExecution
          open={open}
          setOpen={setOpen}
          previousExecutions={previousExecutions}
          nextExecutions={nextExecutions}
          traitement={traitement}
          onChangeStatut={onRequestChangeStatut}
        />
      )}
      <TraitementAutomatiqueForm
        open={openEditDialog}
        setOpen={setOpenEditDialog}
        mode="creation"
        onRequestSave={handleRequestSave}
        traitementTypes={(loadTraitementTypes.data?.tATraitementTypes.nodes ?? []) as TATraitementType[]}
        saved={saved}
        setSaved={setSaved}
        saving={saving}
        isPrivate={isPrivate}
        setIsPrivate={setIsPrivate}
        state={state}
        setState={setState}
      />
    </CustomContainer>
  );
};

export default MesTraitementsAutomatiquesPage;
