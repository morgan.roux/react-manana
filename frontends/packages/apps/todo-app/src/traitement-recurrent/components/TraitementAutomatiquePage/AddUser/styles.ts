import { createStyles, makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() =>
  createStyles({
    personButton: {
      marginTop: -3,
    },
    collaborateurForm: {
      width: 300,
    },
    collaborateurBox: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    radioButton: {
      position: 'relative',
      top: -15,
    },
    buttonBox: {
      display: 'flex',
      justifyContent: 'space-between',
    },
  })
);
