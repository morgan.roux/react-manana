import { createStyles } from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    collaborateurInput: {
      marginBottom: 16,
      borderRadius: 5,
      border: '1px solid',
      borderColor: 'rgba(200, 200, 200, 0.8)',

      position: 'relative',
      left: -1,

      fontSize: 14,
      color: 'rgb(100, 100, 100)',
      width: '93%',
      paddingLeft: 15,
      height: 35,
      paddingTop: 0,
    },
    collaborateurBox: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    collaborateurButton: {
      height: 20,
    },
    collaborateurButtonIcon: {
      position: 'relative',
      top: -10,
      left: 15,
    },
    buttonBox: {
      display: 'flex',
      justifyContent: 'space-between',
    },

    root: {
      flexGrow: 1,
      width: '100%',
      backgroundColor: theme.palette.background.paper,
    },
    // eslint-disable-next-line @typescript-eslint/camelcase
    PrivateTabIndicator_colorPrimary_156: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    repeteContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      width: '100%',
      marginTop: 16,
      marginBottom: 16,
    },
    recurrence: {
      marginTop: 16,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      width: '100%',
    },
    repete1: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginBottom: 16,
    },
    container1: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    containerFlex: {
      '&>div': {
        marginBottom: '0 !important',
        marginTop: '0 !important',
      },
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
    },
    formControl: {
      margin: theme.spacing(1),
      width: '100%',
      marginRight: 32,
    },
    formControl1: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    formControl2: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginTop: 16,
      // flexDirection: 'row',
      // float: 'right',
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    MuiFormControlRoot: {
      width: '100%',
      display: 'flex',
      '&.Mui-checked': {
        color: 'geen',
      },
    },
    buttonCre: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      float: 'right',
      width: '100%',
    },
    divRepete: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-end',
    },
    MuiOutlinedInputOne: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-end',
      padding: '0',
      width: '3rem',
      height: '3rem',
      marginRight: '20vh',
    },
    MuiOutlinedInputTwo: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-end',
      padding: '0',
      width: '3rem',
      height: '2.5rem',
      margin: '0 5px 0 192px',
      marginTop: '5px',
    },
    MuiBoxRoot55: {
      padding: '0 !important',
    },
    MuiOutlinedInput_input: {
      flex: '1',
      width: '25.3rem',
    },
    Jour: {
      border: '1px solid grey',
      borderRadius: '5%',
      // margin: '.1%',
      margin: '0 5px 0 5px',
      padding: '0',
      width: '2.5rem',
    },
    Semaine: {
      border: '1px solid grey',
      borderRadius: '5%',
      withWidth: '15px',
      // margin: '.1%',
      margin: '0 5px 0 5px',
      padding: '0',
    },
    Mois: {
      border: '1px solid grey',
      borderRadius: '5%',
      withWidth: '15px',
      // margin: '.1%',
      margin: '0 5px 0 5px',
      padding: '0',
    },
    Annee: {
      border: '1px solid grey',
      borderRadius: '5%',
      withWidth: '15px',
      // margin: '.1%',
      margin: '0 5px 0 5px',
      padding: '0',
    },
    title: {
      fontSize: 20,
      fontWeight: 'bold',
      marginBottom: '20px !important',
    },
    width: {
      maxWidth: 70,
      marginBottom: 0,
      maxHeight: 48,
      '& .MuiOutlinedInput-input': {
        padding: 14,
      },
    },
    tabRoot: {
      borderRadius: '4px !important',
      minWidth: 'auto !important',
      border: '1px solid #ccc !important',
      marginLeft: '12px !important',
      textTransform: 'none',
    },
    dayBtnContainer: {
      '& .MuiButton-root': {
        textTransform: 'none',
      },
    },
    tabSelected: {
      backgroundColor: `${theme.palette.primary.main} !important`,
      color: theme.palette.common.white,
      '& .MuiTab-wrapper': {
        color: theme.palette.common.white,
      },
    },
    activeDay: {
      borderRadius: '50%',
      width: 64,
      height: 64,
      background: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    nbOccurenceRoot: {
      width: 56,
    },
    recurrenceInputNumber: {
      width: 100,
    },
    inputMask: {
      padding: 16,
      borderRadius: 4,
      border: '1px solid rgba(0,0,0,0.3)',
      fontFamily: 'Roboto',
    },
    danger: {
      color: 'red',
      fontSize: 12,
    },
    boxTab: {
      display: 'flex',
      justifyContent: 'space-between',
      '& .main-MuiInputBase-root': {
        height: '48px',
      },
    },
    dateDebut: {
      display: 'flex',
      alignItems: 'center',
    },
    dateFin: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    dateFinItems: {
      display: 'flex',
    },
    checkbox: {
      '& .Mui-checked': {
        color: theme.palette.primary.main,
      },
    },
    ocurrence: {
      display: 'flex',
    },
  })
);
