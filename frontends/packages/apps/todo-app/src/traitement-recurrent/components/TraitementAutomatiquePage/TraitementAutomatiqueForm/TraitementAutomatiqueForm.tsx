import React, { FC, useState, useEffect, ChangeEvent, Dispatch } from 'react';
import {
  Box,
  Button,
  Divider,
  Tab,
  Typography,
  FormControl,
  Radio,
  RadioGroup,
  FormControlLabel,
  OutlinedInput,
  Tabs,
  Grid,
} from '@material-ui/core';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import { TATraitement, TATraitementType } from '@lib/types';
import { useStyles } from './styles';
import moment from 'moment';
// import TextMask from 'react-text-mask';
// import { createNumberMask } from 'text-mask-addons';
import { Alert } from '@material-ui/lab';
import { CustomDatePicker, CustomEditorText, CustomFormTextField, CustomModal, CustomSelect } from '@lib/ui-kit';
import CommonFieldsForm from '@lib/common/src/components/CommonFieldsForm';
import { useApplicationContext } from '@lib/common';

// const types = ['Relance', 'Inventaire'];
// const fonctions = ['fonction 1', 'fonction 2', 'fonction 3'];
// const urgences = ['A:Journée', 'B:Semaine', 'C:Mois'];
// const importances = ['Pas Important', 'Important', 'Très Important'];
// const selection = {
//   types,
//   fonctions,
//   urgences,
//   importances,
// };

// export const users: User[] = [
//   {
//     id: 'id1',
//     email: '',
//     emailConfirmed: true,
//     passwordHash: '',
//     passwordSecretAnswer: '',
//     phoneNumber: '',
//     phoneNumberConfirmed: true,
//     twoFactorEnabled: true,
//     lockoutEndDateUtc: new Date(),
//     lockoutEnabled: true,
//     accessFailedCount: 0,
//     userName: 'Cécile ZUSSY',
//     lastLoginDate: new Date(),
//     lastPasswordChangedDate: new Date(),
//     isLockedOut: false,
//     isLockedOutPermanent: false,
//     isObligationChangePassword: false,
//     accessFailedCountBeforeLockoutPermanent: 0,
//     idGroupement: 'id1',
//     status: '',
//     theme: '',
//     fullName: 'mike',
//     jourNaissance: 2,
//     moisNaissance: 7,
//     anneeNaissance: 2000,
//     createdAt: new Date(),
//     updatedAt: new Date(),
//     photo: {
//       id: '',
//       publicUrl:
//         'https://avatars0.githubusercontent.com/u/47183345?s=460&u=d325a99dd569d0759a59c5d6d80feea36451c707&v=4',
//     },
//   },
//   {
//     fullName: 'mike',
//     id: 'id2',
//     email: '',
//     emailConfirmed: true,
//     passwordHash: '',
//     passwordSecretAnswer: '',
//     phoneNumber: '',
//     phoneNumberConfirmed: true,
//     twoFactorEnabled: true,
//     lockoutEndDateUtc: new Date(),
//     lockoutEnabled: true,
//     accessFailedCount: 0,
//     userName: 'Jennifer DIDIER',
//     lastLoginDate: new Date(),
//     lastPasswordChangedDate: new Date(),
//     isLockedOut: false,
//     isLockedOutPermanent: false,
//     isObligationChangePassword: false,
//     accessFailedCountBeforeLockoutPermanent: 0,
//     idGroupement: 'id2',
//     status: '',
//     theme: '',
//     jourNaissance: 2,
//     moisNaissance: 7,
//     anneeNaissance: 2000,
//     createdAt: new Date(),
//     updatedAt: new Date(),
//     photo: {
//       id: '',
//       publicUrl:
//         'https://avatars0.githubusercontent.com/u/47183345?s=460&u=d325a99dd569d0759a59c5d6d80feea36451c707&v=4',
//     },
//   },
// ] as any;

// export const donnees = { selection, users };

export const twoDigitsAtLeaset = (value: number): string => (value < 10 ? `0${value}` : `${value}`);

const DAYS: any = {
  1: 'Lun.',
  2: 'Mar.',
  3: 'Mer.',
  4: 'Jeu.',
  5: 'Ven.',
  6: 'Sam.',
  7: 'Dim.',
};

const MONTHS: any = {
  1: 'Janvier',
  2: 'Février',
  3: 'Mars',
  4: 'Avril',
  5: 'Mai',
  6: 'Juin',
  7: 'Juillet',
  8: 'Août',
  9: 'Septembre',
  10: 'Octobre',
  11: 'Novembre',
  12: 'Décembre',
};

// const MINUTES_IN_HOUR = 60;
const MINUTES_SEPARATOR = ':';

interface RepetitionMapping {
  repetition: number;
  joursSemaine: number[];
  jourMois: number;
  moisAnnee: number;
}

const REPETITION_MAP: any = {
  JOUR: (traitement: RepetitionMapping) => {
    return traitement.repetition === 1 ? `Tous les jours` : `Tous les ${traitement.repetition} jours`;
  },
  SEMAINE: (traitement: RepetitionMapping) => {
    return `${
      traitement.repetition === 1 ? `Toutes les semaines` : `Toutes les ${traitement.repetition} semaines`
    }, ${traitement.joursSemaine.map((jour) => DAYS[jour]).join(',')}`;
  },
  MOIS: (traitement: RepetitionMapping) => {
    return `${traitement.repetition === 1 ? `Tous les mois` : `Tous les ${traitement.repetition} mois`}, le ${
      traitement.jourMois
    }`;
  },
  ANNEE: (traitement: RepetitionMapping) => {
    return `${
      traitement.repetition === 1 ? `Chaque année` : `Tous les ${traitement.repetition} ans`
    }, ${twoDigitsAtLeaset(traitement.jourMois)} ${MONTHS[traitement.moisAnnee]}`;
  },
};

const REPETITION_UNITE_TAB_INDEX: any = {
  JOUR: 0,
  SEMAINE: 1,
  MOIS: 2,
  ANNEE: 3,
};

export const computeRecurrence = (
  repetition: number,
  repetitionUnite: string,
  joursSemaine: number[],
  jourMois: number,
  moisAnnee: number,
  heure: string
): string => {
  const [hour, minute] = heure.split(MINUTES_SEPARATOR, 2);

  return `${REPETITION_MAP[repetitionUnite]({
    repetition,
    joursSemaine,
    jourMois,
    moisAnnee,
  })}, à ${twoDigitsAtLeaset(parseInt(hour))}:${minute}`;
};

export interface TraitementAutomatiqueRequestSaving {
  id?: string;
  description: string;
  idType: string;
  repetition: number | undefined;
  repetitionUnite: string;
  heure: string;
  joursSemaine?: number[];
  jourMois?: number;
  moisAnnee?: number;
  dateHeureFin?: Date;
  nombreOccurencesFin?: number;
  dateDebut?: Date;

  idUrgence: string;
  idImportance: string;
  idFonction: string;
  idTache: string;
  idEtiquette: string;
  assignedUsers: any[];
  isPrivate: boolean;
}

interface TabPanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

function a11yProps(index: any) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div role="tabpanel" hidden={value !== index} {...other} style={{ marginTop: '16px' }}>
      {value === index && <Typography>{children}</Typography>}
    </div>
  );
}

// const formatTimeToString = (time: number) => {
//   const hours = Math.floor(time / MINUTES_IN_HOUR);
//   const minutes = Math.floor(time % MINUTES_IN_HOUR);
//   return `${hours}:${minutes.toString().padStart(2, '0')}`;
// };
// const parseTimeString = (value: string) => {
//   const [hoursRaw, minutesRaw] = value.split(MINUTES_SEPARATOR, 2);
//   const hours = parseInt(hoursRaw || '00', 10);
//   const minutes = parseInt(minutesRaw || '00', 10);
//   const time = hours * MINUTES_IN_HOUR + minutes;

//   return { time, hoursRaw, minutesRaw };
// };

export interface TraitementAutomatiqueFormProps {
  mode: 'creation' | 'modification';
  open: boolean;
  defaultTypeId?: string;
  traitement?: TATraitement;
  saving?: boolean;
  saved?: boolean;
  setSaved?: (newSaved: boolean) => void;
  // collaborateurs?: any[];
  // idFonction?: string;
  // idTache?: string;
  // idImportance?: string;
  // idUrgence?: string;
  filterRecurrence?: string;
  traitementTypes: TATraitementType[];
  // commonFieldsComponent: ReactNode;
  onRequestSave?: (data: TraitementAutomatiqueRequestSaving) => void;
  onGetData?: (data: TraitementAutomatiqueRequestSaving) => void;
  setOpen: (open: boolean) => void;
  state: any;
  setState: Dispatch<any>;
  isPrivate: boolean;
  setIsPrivate: Dispatch<boolean>;
}

const TraitementAutomatiqueForm: FC<TraitementAutomatiqueFormProps> = ({
  open,
  mode,
  defaultTypeId,
  traitement,
  traitementTypes,
  saving,
  setSaved,
  saved,
  onGetData,
  onRequestSave,
  setOpen,
  filterRecurrence,
  state,
  setState,
  isPrivate,
  setIsPrivate,
}) => {
  const classes = useStyles({});
  const REGEX_HOUR = /^([1-9]|[01]?[0-9]|2[0-3]):(0|[0-5][0-9])$/;
  const defaultHeure = moment().toDate();
  defaultHeure.setHours(8);
  defaultHeure.setMinutes(0);

  const initHeure = `${defaultHeure.getHours()}:${twoDigitsAtLeaset(defaultHeure.getMinutes())}`;

  const [selectedTab, setSelectedTab] = useState<number>(0);

  const [idType, setIdType] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [repetition, setRepetition] = useState<number>(1);
  const [repetitionUnite, setRepetitionUnite] = useState<'JOUR' | 'SEMAINE' | 'MOIS' | 'ANNEE'>('JOUR');
  const [heure, setHeure] = useState<string>(initHeure);
  const [joursSemaine, setJoursSemaine] = useState<number[]>([]);
  const [jourMois, setJourMois] = useState<number>(moment().get('D'));
  const [moisAnnee, setMoisAnnee] = useState<number>(moment().get('M'));
  const [dateHeureFin, setDateHeureFin] = useState<any>(undefined);
  const [nombreOccurencesFin, setNombreOccurencesFin] = useState<number | undefined>();
  const [repetitionFin, setRepetitionFin] = useState<'JAMAIS' | 'APRES_DATE' | 'APRES_OCCURENCES'>('JAMAIS');
  const [debutTraitement, setDebutTraitement] = useState<Date | undefined>(new Date());
  // const [minDebutTraitement, setMinDebutTraitement] = useState<Date | undefined>();
  const [assignedUsers, setAssingedUsers] = useState<any[]>([]);

  useEffect(() => {
    if (traitement && mode === 'modification') {
      setIdType(traitement.type.id);
      setDescription(traitement.description);
      setRepetition(traitement.repetition);
      setRepetitionUnite(traitement.repetitionUnite as any);
      setSelectedTab(REPETITION_UNITE_TAB_INDEX[traitement.repetitionUnite]);
      const [hourPart, minutePart] = traitement.heure.split(':', 2);

      setHeure(`${hourPart}:${twoDigitsAtLeaset(parseInt(minutePart, 10))}`);
      setJoursSemaine(traitement.joursSemaine ? traitement.joursSemaine : []);
      setJourMois(traitement.jourMois ? traitement.jourMois : 1);
      setMoisAnnee(traitement.moisAnnee || moment().get('M'));
      setDateHeureFin(traitement.dateHeureFin);
      setNombreOccurencesFin(traitement.nombreOccurencesFin);
      setDebutTraitement(traitement.dateDebut);
      setAssingedUsers(traitement.participants || []);
      if (traitement.dateHeureFin) {
        setRepetitionFin('APRES_DATE');
      } else if (traitement.nombreOccurencesFin) {
        setRepetitionFin('APRES_OCCURENCES');
      } else {
        setRepetitionFin('JAMAIS');
      }
    }
  }, [traitement]);

  useEffect(() => {
    if ((open && mode === 'creation') || (!open && mode === 'modification')) {
      setSelectedTab(0);
      setIdType('');
      setDescription('');
      setRepetition(1);
      setRepetitionUnite('JOUR');
      setHeure('08:00');
      setJoursSemaine([]);
      setJourMois(moment().get('day'));
      setMoisAnnee(moment().get('M'));
      setDateHeureFin(undefined);
      setNombreOccurencesFin(undefined);
      setRepetitionFin('JAMAIS');
      if (defaultTypeId) {
        setIdType(defaultTypeId);
      }

      setAssingedUsers([]);
    }
  }, [open]);

  useEffect(() => {
    if (saved) {
      setOpen(false);
      if (setSaved) {
        setSaved(false);
        setState([]);
      }
    }
  }, [saved]);

  // useEffect(() => {
  //   const currentDate = new Date();
  //   const currentHeure = new Date();
  //   const currentDay: number = parseInt(moment(currentDate).format('D'));
  //   const currentMonth: number = parseInt(moment(currentDate).format('M'));
  //   const [hoursRaw, minutesRaw] = heure.split(MINUTES_SEPARATOR, 2);
  //   currentHeure.setHours(parseInt(hoursRaw));
  //   currentHeure.setMinutes(parseInt(minutesRaw));

  //   const defaultDebutTraitement = moment(currentHeure).isBefore(currentDate)
  //     ? moment(currentDate).add(1, 'day').toDate()
  //     : currentDate;
  //   setDebutTraitement(defaultDebutTraitement);
  //   // setMinDebutTraitement(defaultDebutTraitement);

  //   if (repetitionUnite === 'SEMAINE') {
  //     /** Recuperer le jour suivant du repetition */
  //     const weekDay: number = moment().isoWeekday();
  //     const joursSemaineSuperieurSelectionne: number[] = joursSemaine.filter(
  //       (jour) => jour >= weekDay,
  //     );
  //     const joursSemaineInferieurSelectionne: number[] = joursSemaine.filter(
  //       (jour) => jour < weekDay,
  //     );

  //     if (joursSemaineSuperieurSelectionne.length > 0) {
  //       const jourSuperieur: number = Math.min(...joursSemaineSuperieurSelectionne);

  //       if (jourSuperieur - weekDay === 0 && joursSemaineInferieurSelectionne.length === 0) {
  //         if (moment(currentHeure).isBefore(currentDate)) {
  //           const min = moment(currentDate).add(7, 'day').toDate();
  //           setDebutTraitement(min);
  //           // setMinDebutTraitement(min);
  //         } else {
  //           setDebutTraitement(currentDate);
  //           // setMinDebutTraitement(currentDate);
  //         }
  //       }
  //       if (jourSuperieur - weekDay > 0) {
  //         const min = moment(currentDate)
  //           .add(jourSuperieur - weekDay, 'day')
  //           .toDate();
  //         setDebutTraitement(min);
  //         // setMinDebutTraitement(min);
  //       }
  //     }
  //     if (
  //       joursSemaineInferieurSelectionne.length > 0 &&
  //       joursSemaineSuperieurSelectionne.length === 0
  //     ) {
  //       const jourInferieur: number = Math.min(...joursSemaineInferieurSelectionne);
  //       const min = moment(currentDate)
  //         .add(7 - (weekDay - jourInferieur), 'day')
  //         .toDate();

  //       setDebutTraitement(min);
  //       // setMinDebutTraitement(min);
  //     }
  //   }
  //   if (repetitionUnite === 'MOIS') {
  //     if (
  //       jourMois < currentDay ||
  //       (jourMois === currentDay && moment(currentHeure).isBefore(currentDate))
  //     ) {
  //       const min = moment(currentDate.setDate(jourMois)).add(1, 'month').toDate();
  //       setDebutTraitement(min);
  //       // setMinDebutTraitement(min);
  //     } else {
  //       const min = moment(currentDate.setDate(jourMois)).toDate();
  //       setDebutTraitement(min);
  //       // setMinDebutTraitement(min);
  //     }
  //   }
  //   if (repetitionUnite === 'ANNEE') {
  //     currentDate.setDate(jourMois);
  //     currentDate.setMonth(moisAnnee - 1);
  //     if (
  //       moisAnnee < currentMonth ||
  //       // eslint-disable-next-line eqeqeq
  //       (jourMois < currentDay && moisAnnee == currentMonth) ||
  //       // eslint-disable-next-line eqeqeq
  //       (jourMois <= currentDay &&
  //         moisAnnee === currentMonth &&
  //         moment(currentHeure).isBefore(currentDate))
  //     ) {
  //       const min = moment(currentDate).add(1, 'year').toDate();
  //       setDebutTraitement(min);
  //       // setMinDebutTraitement(min);
  //     } else {
  //       const min = moment(currentDate).toDate();
  //       setDebutTraitement(min);
  //       // setMinDebutTraitement(min);
  //     }
  //   }
  // }, [repetitionUnite, heure, joursSemaine, jourMois, moisAnnee]);

  const handleUserDestinationChanges = (selected: any) => {
    setAssingedUsers(selected);
  };

  const handleChange = (event: any) => {
    if (event.target) {
      const { value, name } = event.target;
      setState((prevState: any) => ({ ...prevState, [name]: value }));
    }
  };

  const handleRepetitionFin = (e: ChangeEvent<HTMLInputElement>) => {
    const value: any = e.target.value;

    setRepetitionFin(value);

    if (value === 'JAMAIS') {
      setDateHeureFin(undefined);
      setNombreOccurencesFin(undefined);
    }
    if (value === 'APRES_DATE') {
      setDateHeureFin(new Date());
      setNombreOccurencesFin(undefined);
    }
    if (value === 'APRES_OCCURENCE') {
      setDateHeureFin(undefined);
    }
  };

  const handleDateHeureFin = (date: MaterialUiPickersDate) => {
    setDateHeureFin(date);
  };

  const handleDebutTraitement = (date: MaterialUiPickersDate) => {
    setDebutTraitement(date as any);
  };

  const handleNombreOccurencesFin = (e: ChangeEvent<HTMLInputElement>) => {
    setNombreOccurencesFin(e.currentTarget.value ? parseInt(e.currentTarget.value, 10) : undefined);
  };

  const handleGetData = () => {
    const now = new Date();
    const data: TraitementAutomatiqueRequestSaving = {
      id: mode === 'modification' ? traitement?.id : undefined,
      idType,
      description,
      repetition,
      repetitionUnite: repetitionUnite,
      heure,
      joursSemaine: repetitionUnite === 'SEMAINE' && joursSemaine ? joursSemaine : undefined,
      jourMois: repetitionUnite === 'MOIS' ? jourMois : repetitionUnite === 'ANNEE' ? now.getDay() : undefined,
      moisAnnee: repetitionUnite === 'ANNEE' ? now.getMonth() : undefined,
      dateHeureFin,
      nombreOccurencesFin,
      dateDebut: debutTraitement,

      assignedUsers,
      idEtiquette: state.idEtiquette,
      idUrgence: state.idUrgence,
      idImportance: state.idImportance,
      idTache: state?.projet?.idTache,
      idFonction: state?.projet?.idFonction,
      isPrivate,
    };
    return data;
  };

  const handleSubmit = () => {
    const data = handleGetData();
    onGetData && onGetData(data);
    onRequestSave && onRequestSave(data);
    // console.log('--------------------data---------------------------: ', data);
  };

  const handleChangeHeure = (event: ChangeEvent<HTMLInputElement>) => {
    // const value = event.target.value;
    // const [hoursRaw, minutesRaw] = value.split(MINUTES_SEPARATOR, 2);
    // if (parseInt(hoursRaw) > 23) {
    //   setHeure(`00:00`);
    //   return;
    // }
    // if (parseInt(minutesRaw) > 59) {
    //   setHeure(`${hoursRaw}:00`);
    //   return;
    // }
    setHeure(event.target.value);
  };

  // const handleOnKeyDownTime = (event: KeyboardEvent<HTMLInputElement>) => {
  //   const { key, shiftKey } = event;
  //   const CHANGES: any = {
  //     ArrowUp: +1,
  //     ArrowDown: -1,
  //   };
  //   const changeDirection = CHANGES[key] || 0;

  //   if (changeDirection === 0) {
  //     return;
  //   }

  //   event.preventDefault();

  //   const { selectionStart, value } = event.currentTarget;
  //   const { time, hoursRaw, minutesRaw } = parseTimeString(value);
  //   const isChangingHours = selectionStart
  //     ? selectionStart <= hoursRaw.length || selectionStart === 0
  //     : false;

  //   const changeFactor = isChangingHours
  //     ? shiftKey
  //       ? 5 * MINUTES_IN_HOUR
  //       : MINUTES_IN_HOUR
  //     : shiftKey
  //     ? 15
  //     : 1;

  //   const newTime = Math.max(0, time + changeDirection * changeFactor);

  //   if (newTime >= 1440 || parseInt(hoursRaw, 10) > 23 || parseInt(minutesRaw, 10) > 59) {
  //     setHeure('00:00');
  //     return;
  //   }
  //   setHeure(formatTimeToString(newTime));
  // };

  // const handleReformatTime = (event: FocusEvent<HTMLInputElement>) => {
  //   const { value } = event.target;
  //   if (!value) {
  //     return;
  //   }

  //   const { time } = parseTimeString(value);
  //   setHeure(formatTimeToString(time));
  // };

  console.log('----------------HEURE---------------------: ', heure);

  const selectTimeComponent = (
    <Box display="flex" justifyContent="space-between" width="100%" alignItems="center">
      <Typography style={{ marginRight: '16px' }}>A</Typography>
      <Box>
        <CustomFormTextField type="time" label="" onChange={handleChangeHeure} value={heure} />
        {/* <TextMask
          value={heure}
          onChange={handleChangeHeure}
          placeholder="00:00"
          mask={createNumberMask({
            includeThousandsSeparator: false,
            prefix: '',
            allowDecimal: true,
            decimalSymbol: ':',
            requireDecimal: true,
          })}
          onKeyDown={handleOnKeyDownTime}
          onBlur={handleReformatTime}
          className={classes.inputMask}
        /> */}
        {/* <Typography className={classes.danger}>Heure invalide!</Typography> */}
      </Box>
    </Box>
  );

  const tabList = filterRecurrence
    ? ['JOUR', 'SEMAINE', 'MOIS', 'ANNEE'].filter((tab) => tab === filterRecurrence)
    : ['JOUR', 'SEMAINE', 'MOIS', 'ANNEE'];

  const dayList = ['Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.', 'Dim.'];

  const isFormInValid = (): boolean => {
    return (
      idType === '' ||
      (assignedUsers.length === 0 && !(state.idProject || '')) ||
      (state.idImportance || '') === '' ||
      (state.idUrgence || '') === '' ||
      !REGEX_HOUR.test(heure) ||
      (repetitionUnite === 'SEMAINE' && joursSemaine.length === 0) ||
      description === ''
    );
  };

  return (
    <CustomModal
      open={open}
      setOpen={setOpen}
      title={mode === 'creation' ? 'Nouvelle tâche récurrente' : "Modification d'une tâche récurrente"}
      withBtnsActions
      disabledButton={saving || isFormInValid()}
      closeIcon
      headerWithBgColor
      maxWidth="sm"
      fullWidth
      onClickConfirm={() => {
        handleSubmit();
        handleGetData();
      }}
      draggable
    >
      <form style={{ width: '100%' }}>
        {mode === 'creation' && (
          <Typography className={classes.title}>Programmer une nouvelle tâche Récurrente</Typography>
        )}
        <Box>
          <CustomSelect
            listId="id"
            index="libelle"
            label="Type"
            value={idType}
            onChange={(event: any) => setIdType(event.target.value)}
            list={traitementTypes}
            style={{ height: '48px', textAlign: 'left' }}
            required
            defaultValue={idType}
          />
        </Box>

        <Box mb={4}>
          <CustomEditorText
            value={description}
            placeholder="Ajouter une Description Détaillée (*)"
            onChange={(e: string) => setDescription(e)}
          />
        </Box>

        <CommonFieldsForm
          selectedUsers={assignedUsers}
          urgence={state.idUrgence}
          urgenceProps={{
            useCode: false,
          }}
          usersModalProps={{
            withNotAssigned: false,
            searchPlaceholder: 'Rechercher...',
            withAssignTeam: false,
            singleSelect: false,
          }}
          isPrivate={isPrivate}
          onChangePrivate={setIsPrivate}
          projet={state.projet}
          importance={state.idImportance ? state.idImportance : undefined}
          onChangeUsersSelection={handleUserDestinationChanges}
          onChangeUrgence={(urgence) => handleChange({ target: { name: 'idUrgence', value: urgence.id } })}
          onChangeImportance={(importance) => handleChange({ target: { name: 'idImportance', value: importance.id } })}
          onChangeProjet={(projet) => {
            handleChange({ target: { name: 'projet', value: projet } });
          }}
        />
        {/* Modification */}
        <Box className={classes.recurrence}>
          <Typography>Récurrence personnalisée</Typography>
          <Typography>
            {repetitionUnite && heure
              ? computeRecurrence(repetition, repetitionUnite, joursSemaine, jourMois, moisAnnee, heure)
              : ''}
          </Typography>
        </Box>

        <Box className={classes.repeteContainer}>
          <Box>
            <Typography> Répeter toute(s) les</Typography>
          </Box>
          <Box className={classes.boxTab}>
            <CustomFormTextField
              type="number"
              onChange={(event: any) => {
                const nouveauRepetition = event.target.value ? parseInt(event.target.value, 10) : 1;
                if (nouveauRepetition > 0) {
                  setRepetition(nouveauRepetition);
                }
              }}
              value={repetition}
              classes={{ root: classes.width }}
            />
            <Tabs
              value={selectedTab}
              onChange={(_event, newValue) => setSelectedTab(newValue)}
              className={classes.PrivateTabIndicator_colorPrimary_156}
            >
              {tabList.map((tab, index) => (
                <Tab
                  key={`tab-${index}`}
                  label={tab}
                  {...a11yProps(index)}
                  onClick={() => {
                    setRepetitionUnite(tab as any);
                  }}
                  style={{ minWidth: 'auto' }}
                  classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                />
              ))}
            </Tabs>
          </Box>
        </Box>
        <Divider />
        <TabPanel value={selectedTab} index={0}>
          <Box className={classes.container1}>{selectTimeComponent}</Box>
        </TabPanel>
        <TabPanel value={selectedTab} index={1}>
          <Box className={classes.repete1}>
            <Typography>Répeté le</Typography>

            <Box className={classes.dayBtnContainer}>
              {dayList.map((day, index) => (
                <Button
                  key={index}
                  onClick={() => {
                    if (joursSemaine.includes(index + 1)) {
                      setJoursSemaine(joursSemaine.filter((jour) => jour !== index + 1));
                    } else {
                      setJoursSemaine([...joursSemaine, index + 1]);
                    }
                  }}
                  className={joursSemaine.includes(index + 1) ? classes.activeDay : ''}
                >
                  {day}
                </Button>
              ))}
            </Box>
          </Box>
          {selectTimeComponent}
        </TabPanel>
        <TabPanel value={selectedTab} index={2}>
          <div className={classes.container1}>
            <FormControl variant="outlined" className={classes.formControl}>
              <CustomFormTextField
                type="number"
                label="Tous le"
                value={jourMois}
                onChange={(event) => {
                  const nouveauJourMois = parseInt(`${event.target.value}`, 10);
                  if (nouveauJourMois > 0 && nouveauJourMois < 32) {
                    setJourMois(nouveauJourMois);
                  }
                }}
              />
            </FormControl>
            {selectTimeComponent}
          </div>
        </TabPanel>
        <TabPanel value={selectedTab} index={3}>
          <div className={`${classes.container1} ${classes.containerFlex}`}>
            <FormControl variant="outlined" className={classes.formControl}>
              <CustomFormTextField
                type="number"
                label="Jour"
                value={jourMois}
                onChange={(event) => {
                  const nouveauJourMois = parseInt(`${event.target.value}`, 10);
                  if (nouveauJourMois > 0 && nouveauJourMois < 32) {
                    setJourMois(nouveauJourMois);
                  }
                }}
              />
            </FormControl>
            <CustomSelect
              style={{ marginBottom: 0, height: 48, marginRight: 10, textAlign: 'left' }}
              noMinheight
              listId="id"
              index="libelle"
              label="Mois"
              value={moisAnnee}
              onChange={(event: any) => setMoisAnnee(event.target.value)}
              list={Object.keys(MONTHS).map((id) => ({ id, libelle: MONTHS[id] }))}
              required
            />
            {selectTimeComponent}
          </div>
        </TabPanel>

        {mode === 'modification' &&
          computeRecurrence(repetition, repetitionUnite, joursSemaine, jourMois, moisAnnee, heure) !==
            computeRecurrence(
              traitement?.repetition || 1,
              traitement?.repetitionUnite || 'JOUR',
              traitement?.joursSemaine || [],
              traitement?.jourMois || moment().get('D'),
              traitement?.moisAnnee || moment().get('M'),
              traitement?.heure || initHeure
            ) && (
            <Box pt={1}>
              <Alert severity="info">Les modifications ne seront appliquées qu'à partir de la prochaine écheance</Alert>
            </Box>
          )}
        <Box py={2}>
          <Grid container item xs={12}>
            <Grid item xs={6} className={classes.dateDebut}>
              <Typography>A partir de: </Typography>
            </Grid>
            <Grid item xs={6}>
              <CustomDatePicker style={{ width: '100%' }} value={debutTraitement} onChange={handleDebutTraitement} />
            </Grid>
          </Grid>
        </Box>

        <div className={classes.recurrence}>
          <p>Se termine </p>
        </div>

        <FormControl component="fieldset" className={classes.MuiFormControlRoot}>
          <RadioGroup
            aria-label="repetitionFin"
            name="repetitionFin"
            onChange={handleRepetitionFin}
            value={repetitionFin}
            className={classes.checkbox}
          >
            <FormControlLabel
              value="JAMAIS"
              onClick={() => {
                setDateHeureFin(undefined);
                setNombreOccurencesFin(undefined);
              }}
              checked={repetitionFin === 'JAMAIS'}
              control={<Radio checked={repetitionFin === 'JAMAIS'} />}
              label="Jamais"
            />
            <Box py={2}>
              <Grid container item xs={12} className={classes.dateFin}>
                <Grid item xs={6} className={classes.dateFinItems}>
                  <FormControlLabel
                    value="APRES_DATE"
                    control={<Radio checked={repetitionFin === 'APRES_DATE'} />}
                    label="le"
                    checked={repetitionFin === 'APRES_DATE'}
                  />
                </Grid>
                <Grid item xs={6}>
                  <CustomDatePicker
                    style={{ width: '100%' }}
                    value={dateHeureFin}
                    onChange={handleDateHeureFin}
                    disabled={repetitionFin === 'JAMAIS' || repetitionFin === 'APRES_OCCURENCES'}
                    required={repetitionFin === 'APRES_DATE'}
                  />
                </Grid>
              </Grid>
            </Box>
            <Grid container item xs={12} className={classes.dateFin}>
              <Grid item xs={6} className={classes.dateFinItems}>
                <FormControlLabel
                  value="APRES_OCCURENCES"
                  control={<Radio checked={repetitionFin === 'APRES_OCCURENCES'} />}
                  label="Après"
                  checked={repetitionFin === 'APRES_OCCURENCES'}
                />
              </Grid>
              <Grid container item xs={6}>
                <Box display="flex" flexDirection="row" justifyContent="flex-start" alignItems="center">
                  <Grid item xs={3}>
                    <FormControl
                      variant="outlined"
                      style={{ width: '100%' }}
                      disabled={repetitionFin === 'JAMAIS' || repetitionFin === 'APRES_DATE'}
                      required={repetitionFin === 'APRES_OCCURENCES'}
                    >
                      <OutlinedInput
                        type="number"
                        onChange={handleNombreOccurencesFin}
                        value={open ? nombreOccurencesFin : ''}
                        id="outlined-adornment-weight"
                        aria-describedby="outlined-weight-helper-text"
                        inputProps={{
                          'aria-label': 'weight',
                        }}
                        labelWidth={0}
                      />
                    </FormControl>
                  </Grid>
                  <Grid item xs={9} className={classes.ocurrence}>
                    <Box mx={2}>Ocurrences</Box>
                  </Grid>
                </Box>
              </Grid>
            </Grid>
          </RadioGroup>
        </FormControl>
      </form>
    </CustomModal>
  );
};

export default TraitementAutomatiqueForm;
