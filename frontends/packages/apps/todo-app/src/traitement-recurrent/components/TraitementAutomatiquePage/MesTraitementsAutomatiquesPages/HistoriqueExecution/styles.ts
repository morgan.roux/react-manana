import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";

const style = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: '25px 0',
            wordBreak: "break-all"
        },
        gridCursor: {
            position: 'absolute',
            top: 22,
            right: 75
        },
        cursorLink: {
            padding: '0 10px',
            ' &:hover': {
                textDecoration: 'none'
            }
        },
        listItem: {
            padding: 0
        },
        gridItem: {
            padding: '10px 0',
            display: 'block'
        },
        patientGrid: {
            padding: '10px'
        },
        subtitle1: {
            color: 'gray',
            fontSize: '12',
            [theme.breakpoints.between('xs', 'sm')]: {
                marginTop: 5
            }
        },
        span: {
            color: 'black'
        },
        btnDashed: {
            border: '1px dashed #1D1D1D',
            paddingRight: 100,
            [theme.breakpoints.between('xs', 'sm')]: {
                paddingRight: 10,
                margin: '10px 5px 10px 0px'
            }
        },
        editor: {
            [theme.breakpoints.between('xs', 'sm')]: {
                width: '100%'
            },
        },
        avatar: {
            width: theme.spacing(3),
            height: theme.spacing(3)
        },
        footer: {
            padding: '0'
        },
        titleTab: {
            fontSize: 16,
            textTransform: 'none',
            [theme.breakpoints.down('sm')]: {
                fontSize: 14,
            },
        },
    })
);

export default style;
