export interface Statut {
  value: string;
  label: string;
  completedLabel?: string;
}

export const STATUTS: Statut[] = [
  {
    value: 'EN_RETARD',
    label: 'En retard',
    completedLabel: 'En retard',
  },
  {
    value: 'EN_COURS',
    label: 'En cours',
    completedLabel: 'En cours',
  } /*,
    {
      value: 'PLANIFIE',
      label: 'Planifier',
    }*/,
  {
    value: 'CLOTURE',
    label: 'Clôturée',
    completedLabel: 'Clôturée',
  },
];
