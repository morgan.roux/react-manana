import React, { FC, useState, MouseEvent, ReactNode } from 'react';

import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography, Box } from '@material-ui/core';
import { useStyles } from './styles';
import { Edit, Delete, Add, MoreHoriz, FilterList } from '@material-ui/icons';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import { green } from '@material-ui/core/colors';
import { TATraitement, TATraitementType } from '@lib/types';
import { TraitementAutomatiqueForm, TraitementAutomatiqueRequestSaving } from './TraitementAutomatiqueForm';
import { STATUTS } from './util';
import moment from 'moment';
import { Column, ConfirmDeleteDialog, NewCustomButton, Table, TableChange } from '@lib/ui-kit';
import TopBar from '@lib/ui-kit/src/components/templates/ParameterContainer/TopBar';
import { strippedColumnStyle, strippedString } from '@lib/ui-kit/src/services/Helpers';
import { useApplicationContext } from '@lib/common';
import { useTheme } from '@material-ui/styles';

export interface SearchChange extends TableChange {
  statut: string;
}

export interface TraitementAutomatiquePageProps {
  loading?: boolean;
  error?: Error;
  data?: TATraitement[];
  rowsTotal: number;

  traitementTypes: {
    loading?: boolean;
    error?: Error;
    data?: TATraitementType[];
  };

  saving?: boolean;
  saved?: boolean;
  setSaved?: (newSaved: boolean) => void;

  // commonFieldsComponent: ReactNode;
  // collaborateurs?: any[];
  // idTache?: string;
  // idImportance?: string;
  // idUrgence?: string;
  // traitementToEdit?: TATraitement;
  // onRequestEdit: (traitementToEdit: TATraitement | undefined) => void;
  onRequestSave: (traitement: TraitementAutomatiqueRequestSaving) => void;
  onRequestDelete: (traitement: TATraitement) => void;
  onRequestSearch: (searchChange: SearchChange) => void;
}

const TraitementAutomatiquePage: FC<TraitementAutomatiquePageProps> = ({
  loading,
  error,
  data,
  rowsTotal,
  traitementTypes,
  // collaborateurs,
  // idTache,
  // idImportance,
  // idUrgence,
  // traitementToEdit,
  saving,
  saved,
  setSaved,
  // onRequestEdit,
  onRequestSave,
  onRequestDelete,
  onRequestSearch,
}) => {
  const classes = useStyles({});

  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [anchorFilterEl, setAnchorFilterEl] = useState<HTMLElement | null>(null);
  const [openEditDialog, setOpenEditDialog] = useState<boolean>(false);
  const [tableChange, setTableChange] = useState<TableChange>({
    skip: 0,
    take: 12,
    searchText: '',
  });
  const [selectedStatut, setSelectedStatut] = useState<string>('ALL');
  const [traitementToEdit, setTraitementToEdit] = React.useState<TATraitement | undefined>(undefined);
  const [state, setState] = useState<any>({});
  const [isPrivate, setIsPrivate] = useState<boolean>(false);

  const open = Boolean(anchorEl);

  const toggleOpenEditDialog = () => {
    if (openEditDialog) {
      // if closed, set traitement to edit undefid
      handleRequestEdit(undefined);
    }
    setOpenEditDialog((prev) => !prev);
  };

  const handleRequestEdit = (traitementToEdit: any) => {
    setTraitementToEdit(traitementToEdit);
    // setAssingedUsers(traitementToEdit?.participants || []);
    setState((prevState: any) => ({
      ...prevState,
      projet: traitementToEdit?.tache ? { ...traitementToEdit.tache, idTache: traitementToEdit.tache.id } : undefined, // Tache or projet
      idUrgence: traitementToEdit?.urgence.id,
      idImportance: traitementToEdit?.importance.id,
    }));
    setIsPrivate(traitementToEdit?.isPrivate || false);
  };

  const handleOpenAdd = () => {
    toggleOpenEditDialog();
    handleRequestEdit(undefined);
  };

  const handleEdit = (traitement: TATraitement) => {
    toggleOpenEditDialog();
    handleRequestEdit(traitement);
  };

  const handleShowMenuClick = (reunion: TATraitement, event: MouseEvent<HTMLElement>): void => {
    event.preventDefault();
    event.stopPropagation();
    handleRequestEdit(reunion);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (): void => {
    setAnchorEl(null);
  };

  const handleShowMenuFilterClick = (event: MouseEvent<HTMLElement>): void => {
    setAnchorFilterEl(event.currentTarget);
  };

  const handleCloseFilter = (): void => {
    setAnchorFilterEl(null);
  };

  const handleChangeFilter = (newStatut: string): void => {
    setSelectedStatut(newStatut);
    onRequestSearch({
      ...tableChange,
      statut: newStatut,
    });

    handleCloseFilter();
  };

  const handleChangeTable = (change: TableChange): void => {
    setTableChange(change);
    onRequestSearch({
      ...change,
      statut: selectedStatut,
    });
  };

  const handleConfirmDeleteOne = (): void => {
    if (traitementToEdit) {
      onRequestDelete(traitementToEdit);
      setOpenDeleteDialog(false);
    }
  };

  const columns: Column[] = [
    {
      name: 'type',
      label: 'Type',
      renderer: (row: TATraitement) => {
        return row?.type?.libelle ? row.type.libelle : '-';
      },
    },
    {
      name: 'recurrence',
      label: 'Récurrence',
      renderer: (row: TATraitement) => {
        return row?.recurrence ? row.recurrence : '-';
      },
    },
    {
      name: 'date_debut',
      label: 'Date debut',
      renderer: (row: TATraitement) => {
        return row?.dateDebut ? moment(row?.dateDebut).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'description',
      label: 'Description Détaillée',
      renderer: (row: TATraitement) => {
        return row.description ? strippedString(row.description) : '-';
      },
      style: strippedColumnStyle,
    },
    /* {
      name: 'date',
      label: 'Date dernier traitement',
      renderer: (row: TATraitement) => {
        return row.execution?.createdAt ? moment(row.execution.createdAt).format('DD/MM/YYYY') : '-';
      },
      style: strippedColumnStyle

    }, */

    {
      name: 'statut',
      label: 'Statut',
      renderer: (row: TATraitement) => {
        if (row.termine || !row.execution?.dernierChangementStatut?.status) {
          return 'Terminé';
        }

        const status = STATUTS.filter((element) => element.value === row.execution.dernierChangementStatut.status);

        if (status[0].value === 'CLOTURE') {
          return (
            <Box>
              {status[0].label} &nbsp;&nbsp;&nbsp;&nbsp;
              <CheckCircleOutlineIcon style={{ color: green[500] }} />
            </Box>
          );
        }
        return status[0].label;
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: TATraitement) => {
        return (
          <div key={`row-${row.id}`}>
            <IconButton
              aria-controls="simple-menu"
              aria-haspopup="true"
              // eslint-disable-next-line react/jsx-no-bind
              onClick={(event) => handleShowMenuClick(row, event)}
            >
              <MoreHoriz />
            </IconButton>
            <Menu
              id="fade-menu"
              anchorEl={anchorEl}
              keepMounted
              open={open && row.id === traitementToEdit?.id}
              onClose={handleClose}
              TransitionComponent={Fade}
            >
              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  handleEdit(row);
                }}
              >
                <ListItemIcon>
                  <Edit />
                </ListItemIcon>
                <Typography variant="inherit">Modifier</Typography>
              </MenuItem>

              <MenuItem
                onClick={(event) => {
                  event.stopPropagation();
                  handleClose();
                  setOpenDeleteDialog(true);
                }}
              >
                <ListItemIcon>
                  <Delete />
                </ListItemIcon>
                <Typography variant="inherit">Supprimer</Typography>
              </MenuItem>
            </Menu>
          </div>
        );
      },
    },
  ];

  return (
    <>
      <Box className={classes.mainContainer}>
        <TopBar
          title="Liste des Tâches Récurrentes"
          className={classes.table}
          actionButtons={[
            <NewCustomButton key={`action-button-${0}`} onClick={handleOpenAdd} startIcon={<Add />}>
              PROGRAMMER UNE TACHE RECURRENTE
            </NewCustomButton>,
          ]}
        />
        <Table
          topBarComponent={(searchComponent) => (
            <Box className={classes.tableTopBar}>
              <IconButton
                aria-controls="simple-menu"
                aria-haspopup="true"
                className={classes.filterButton}
                onClick={handleShowMenuFilterClick}
              >
                <FilterList /> FILTRER PAR
              </IconButton>
              <Menu
                id="fade-menu"
                anchorEl={anchorFilterEl}
                keepMounted
                open={Boolean(anchorFilterEl)}
                onClose={handleCloseFilter}
                TransitionComponent={Fade}
              >
                <MenuItem selected={selectedStatut === 'ALL'} onClick={() => handleChangeFilter('ALL')}>
                  <Typography variant="inherit">Tout</Typography>
                </MenuItem>

                {STATUTS.map((statut) => (
                  <MenuItem selected={selectedStatut === statut.value} onClick={() => handleChangeFilter(statut.value)}>
                    <Typography variant="inherit">{statut.completedLabel}</Typography>
                  </MenuItem>
                ))}
              </Menu>
              {searchComponent}
            </Box>
          )}
          error={error || traitementTypes.error}
          loading={loading || traitementTypes.loading}
          search={false}
          data={data || []}
          selectable={false}
          columns={columns}
          rowsTotal={rowsTotal}
          dataIdName="id"
          onRunSearch={handleChangeTable}
        />
      </Box>

      {openEditDialog && (
        <TraitementAutomatiqueForm
          open={openEditDialog}
          setOpen={toggleOpenEditDialog}
          mode={traitementToEdit ? 'modification' : 'creation'}
          traitement={traitementToEdit}
          onRequestSave={onRequestSave}
          traitementTypes={traitementTypes.data || []}
          // collaborateurs={collaborateurs}
          // idImportance={idImportance}
          // idUrgence={idUrgence}
          // idTache={idTache}
          saved={saved}
          setSaved={setSaved}
          saving={saving}
          isPrivate={isPrivate}
          setIsPrivate={setIsPrivate}
          state={state}
          setState={setState}
          // fullScreen={isMobile}
        />
      )}
      <ConfirmDeleteDialog
        title="Suppression d'action"
        content="Etes-vous sûr de vouloir supprimer cette action ?"
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleConfirmDeleteOne}
      />
    </>
  );
};

export default TraitementAutomatiquePage;
