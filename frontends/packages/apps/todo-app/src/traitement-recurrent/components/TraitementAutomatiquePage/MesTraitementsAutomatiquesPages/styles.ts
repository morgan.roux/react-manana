import { makeStyles, Theme, createStyles } from '@material-ui/core';
export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      width: '100%',
      scrollbarColor: theme.palette.secondary.main,
      height: 'calc(100vh - 86px)',
    },
    mainContainer: {
      width: '100%',
      padding: 20,
    },
    avatar: {
      width: theme.spacing(3),
      height: theme.spacing(3),
    },
    cursor: {
      cursor: 'pointer',
    },
    inputSelect: {
      padding: '0 25px',
    },
    tableTopBar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      marginBottom: 15,
    },
    filterButton: {
      fontSize: 15,
      color: '#000',
    },
  })
);
