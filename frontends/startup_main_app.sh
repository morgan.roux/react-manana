#!/bin/sh

cd ./packages/apps/main-app

sed -i "s#http://localhost:3001#${TODO_APP_URL}#g" ./remote-modules.json
sed -i "s#http://localhost:3002#${COMMUNICATION_APP_URL}#g" ./remote-modules.json
sed -i "s#http://localhost:3003#${DEMARCHE_QUALITE_APP_URL}#g" ./remote-modules.json
sed -i "s#http://localhost:3004#${GED_APP_URL}#g" ./remote-modules.json
sed -i "s#http://localhost:3005#${TOOLS_APP_URL}#g" ./remote-modules.json
sed -i "s#http://localhost:3006#${PARTENAIRE_APP_URL}#g" ./remote-modules.json
sed -i "s#http://localhost:3007#${BASIS_APP_URL}#g" ./remote-modules.json


#env
echo "API_URL=${API_URL}" >> .env
echo "GATEWAY_FEDERATION_URL=${GATEWAY_FEDERATION_URL}" >> .env
echo "AWS_S3_BUCKET=${AWS_S3_BUCKET}" >> .env
echo "AWS_S3_REGION=${AWS_S3_REGION}" >> .env

yarn build

yarn start