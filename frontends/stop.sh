#!/bin/sh

lsof -i:3001 | grep -E '(webpack|node)' | awk '{print $2}' | (xargs kill -9) || true
lsof -i:3002 | grep -E '(webpack|node)' | awk '{print $2}' | (xargs kill -9) || true
lsof -i:3003 | grep -E '(webpack|node)' | awk '{print $2}' | (xargs kill -9) || true
lsof -i:3004 | grep -E '(webpack|node)' | awk '{print $2}' | (xargs kill -9) || true
lsof -i:3005 | grep -E '(webpack|node)' | awk '{print $2}' | (xargs kill -9) || true
lsof -i:3006 | grep -E '(webpack|node)' | awk '{print $2}' | (xargs kill -9) || true
lsof -i:3007 | grep -E '(webpack|node)' | awk '{print $2}' | (xargs kill -9) || true
lsof -i:3050 | grep -E '(webpack|node)' | awk '{print $2}' | (xargs kill -9) || true