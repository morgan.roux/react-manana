# Use Node 12
# Exemple : docker build --build-arg APP_NAME=basis-app -t basis-app:v1 -f app.Dockerfile .
FROM node:14-alpine as builder

ARG APP_NAME

ARG CURRENT_VERSION

WORKDIR /app

COPY . /app/

RUN echo "{}" >> ./packages/apps/$APP_NAME/env-config.json

RUN echo "{}" >> ./packages/apps/$APP_NAME/public/env-config.json

# main app : Copy remote modules
RUN cp ./packages/apps/main-app/remote-modules.exemple.json ./packages/apps/main-app/remote-modules.json

RUN yarn

RUN yarn bootstrap

RUN yarn $APP_NAME:build

# => Run container
FROM nginx:1.19.10-alpine

ARG APP_NAME

ARG CURRENT_VERSION

ENV NODE_ENV production

ENV CURRENT_VERSION="$CURRENT_VERSION"

# Nginx config
RUN rm -rf /etc/nginx/conf.d
COPY conf /etc/nginx

# Static build
COPY --from=builder /app/packages/apps/$APP_NAME/dist /usr/share/nginx/html/
COPY --from=builder /app/packages/apps/main-app/.env.example /usr/share/nginx/html/
COPY --from=builder /app/packages/apps/$APP_NAME/env-config.json /usr/share/nginx/html/
COPY ./env.sh /usr/share/nginx/html/


# Default port exposure
EXPOSE 80

WORKDIR /usr/share/nginx/html

# Rename exemple as env
RUN mv .env.example .env

# Add bash
RUN apk add --no-cache bash

# Make our shell script executable
RUN chmod +x env.sh

# Start Nginx server
CMD ["/bin/bash", "-c", "/usr/share/nginx/html/env.sh && nginx -g \"daemon off;\""]
