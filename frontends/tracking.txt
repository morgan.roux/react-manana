https://github.com/etienne-martin/device-detector-js

user_tracking_session
  token
  id_user
  start_date
  end_date
  auto_close
  client_type
  client_name
  client_version
  client_engine
  client_engine_version
  os_name
  os_version
  os_platform
  device_type
  device_brand
  device_model
  bot_name
  bot_category
  bot_url
  bot_producer_name
  bot_producer_url
  screen_width
  screen_height
  ip


https://github.com/nytimes/react-tracking
user_tracking_event
 type: info | error   
 id_session
 page
 path
 action
 token
 id_user
 date
 screen_width
 screen_height


https://github.com/SupremeTechnopriest/react-idle-timer
user_tracking_timer
 page
 path
 type: idle | active
 start_date
 end_date
 token
 id_session
 id_user
 remaining_time
 elasped_time
 last_idle_time
 total_idle_time
 last_active_time
 total_active_time

  
