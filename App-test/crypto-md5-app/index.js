/**
* Required External Modules
*/
const express = require("express");
const path = require("path");
const cors = require('cors');
const bodyParser = require('body-parser');
const md5 = require('md5');
const crud =  require("./CRUD/model");
/**
* App Variables
*/

const app = express();
const port = process.env.PORT || "3003";


/**
*
App Configuration
*/

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
 }));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, "public")));


/**
*
Users Details
*/

const users = [{
    name: "GCR-Pharma",
    idclient: "1",
    iduser : "1",
    crypteddata : "c28785a787",
},
{
    name: "Tokiarivelo",
    idclient: "1",
    iduser : "2",
    crypteddata : "7c1891b337"
},
]

/**
* Routes Definitions
*/
app.get("/", (req, res) => {
    res.render("index", { title: "Home", appTitle: "WELCOME TO CRYPTO MD5 APP", appMessage: "you-re not logged in" });
    crud.connection();
    crud.createTable();
});

app.post("/create-new", async (req, res) => {
    const { body } = req;
    console.log("body", body);
    await crud.createNew({
        name: body.name,
        idclient : body.idclient,
        iduser : body.iduser,
        crypteddata : body.crypteddata,
    });
    res.redirect('/configure-users');
});

app.get("/delete-user/:id?", async (req, res) => {
    const { params } = req;
    console.log(req.params);
    await crud.delete({ id: params.id });
    res.redirect('/configure-users');
});

app.get("/configure-users", async (req, res) => {
    const list = await crud.getAll();
    res.render("config", { title: "Configure users", appTitle: "CONFIGURE USERS", data: list });
});

app.get("/sso-login", async (req, res) => {
    console.log(req.query);
    if (!req.query) {
        res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "There is an error when login" });
    }
    else{
        const { query } = req;
        // const result = users.find( ({ crypteddata, idclient, iduser }) => crypteddata === req.query.tk && idclient === req.query.iclient && iduser === req.query.iduser );
        const result = await crud.getOne({ idclient: query.iclient.trim(), iduser: query.iduser.trim(), crypteddata: query.tk.trim() });
        console.log("result ", result);
        if (result) {
            res.render("index", { title: "Home", appTitle: `WELCOME ${result.name}`, appMessage: "You're logged in!!" });
        }
        else{
            res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "User not found" });
        }
    }
});

app.post("/sso-login", async (req, res) => {
    console.log(req.body);
    if (!req.body) {
        res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "There is an error when login" });
    }
    else{
        const { body } = req;
        // const result = users.find( ({ crypteddata, idclient, iduser }) => crypteddata === req.body.tk && idclient === req.body.iclient && iduser === req.body.iduser );
        const result = await crud.getOne({ idclient: body.iclient.trim(), iduser: body.iduser.trim(), crypteddata: body.tk.trim() });
        console.log("result ", result);
        if (result) {
            res.render("index", { title: "Home", appTitle: `WELCOME ${result.name}`, appMessage: "You're logged in!!" });
        }
        else{
            res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "User not found" });
        }
    }
});

app.get("/log-off", (req, res) => {
    res.render("index", { title: "Home", appTitle: "WELCOME TO CRYPTO AES APP", appMessage: "you-re not logged in" });
});


/**
* Server Activation
*/
app.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`);
});
