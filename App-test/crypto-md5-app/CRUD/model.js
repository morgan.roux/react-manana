const Sequelize = require('sequelize');
const path = require("path");

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: path.join(__dirname,'db','db.sqlite')
});

const User = sequelize.define('user', {
    // attributes
    name: {
        type: Sequelize.STRING,
    },
    idclient: {
        type: Sequelize.STRING
        // allowNull defaults to true
    },
    iduser: {
        type: Sequelize.STRING
        // allowNull defaults to true
    },
    crypteddata: {
        type: Sequelize.STRING
        // allowNull defaults to true
    }
}, {
// options
});

let crud = {};

crud.connection = () =>{
    sequelize.authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
        
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });
}

crud.createTable = () =>{
    User.sync({ force: false }).then(() => {
        console.log("table created if not exist");
    });
}

crud.getAll = () =>{
    return User.findAll().then(users => {
        console.log("All users:", JSON.stringify(users, null, 4));
        return users;
    });
}

crud.getOne = ({idclient, iduser, crypteddata}) =>{
    return User.findAll({
        limit: 1,
        where: {
            idclient: idclient,
            iduser: iduser,
            crypteddata: crypteddata
        }
    }).then(users => {
        // console.log("All users:", JSON.stringify(users, null, 4));
        console.log("user ", JSON.stringify(users, null, 4))
        return users[0];
    });
}

crud.createNew = ({ name, idclient, iduser, crypteddata }) =>{
    return User.create({ name, idclient, iduser, crypteddata }).then(user => {
        console.log("User created:", JSON.stringify(user, null, 4));
        return JSON.stringify(user, null, 4);
    });
}

crud.delete = ({ id }) =>{
    return User.destroy({
        where: {
            id: id,
        }
    }).then(() => {
        return 'deleted';
    });
}

crud.update = ({ pharmaname, cip, ip }) =>{
    return User.update({ pharmaname, cip, ip })({
        where: {
            cip: cip,
            ip: ip,
        }
    }).then(() => {
        return 'updated';
    });
}

module.exports = crud;