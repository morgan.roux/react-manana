# -----------------------------------------------------------------------------
# BUILD STAGE
FROM node:12-alpine as builder

WORKDIR /app/aes

# Copy package.json for caching node_modules
COPY package.json /app/aes/package.json

RUN npm -v

RUN node -v

# Copy source files
COPY . /app/aes

# Install packages
RUN npm install

# RUN npm run build:prod
RUN npm install sqlite3 --save

RUN npm run build:prod

RUN cp -R ./views ./dist
RUN cp -R ./public ./dist



EXPOSE 3004

CMD ["npm", "run", "start:dev"]