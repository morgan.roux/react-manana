/**
* Required External Modules
*/
const express = require("express");
const path = require("path");
const cors = require('cors');
const bodyParser = require('body-parser');
const aes256 = require('aes256');
const crud =  require("./CRUD/model");
/**
* App Variables
*/

const app = express();
const port = process.env.PORT || "3004";


/**
*
App Configuration
*/

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
 }));

app.use(bodyParser.json());
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, "public")));


/**
*
Users Details
*/

const users = [{
    pharmaname: "PHARMACIE ES ALLIER",
    cip : "2030035",
    ip : "154.126.58.74"
},
{
    pharmaname : "PHARMACIE DU TIVOLI",
    cip : "2262186",
    ip : "154.126.58.74"
},
]
// const key = "test cryptage aes"


/**
* Routes Definitions
*/
app.get("/", (req, res) => {
    res.render("index", { title: "Home", appTitle: "WELCOME TO CRYPTO AES APP", appMessage: "you-re not logged in" });
    crud.connection();
    crud.createTable();
});

app.get("/get-all", async (req, res) => {
    const all = await crud.getAll();
    res.send(all);
});

app.get("/create-new", async (req, res) => {
    const created = await crud.createNew({
        pharmaname: "PHARMACIE ES ALLIER",
        cip : "2030035",
        ip : "154.126.58.74"
    });
    res.send(created);
});

app.post("/create-new", async (req, res) => {
    const { body } = req;
    console.log("body", body);
    await crud.createNew({
        pharmaname: body.name,
        cip : body.cip,
        ip : body.ip
    });
    res.redirect('/configure-users');
});

app.get("/delete-user/:id?", async (req, res) => {
    const { params } = req;
    console.log(req.params);
    await crud.delete({ id: params.id });
    res.redirect('/configure-users');
});

app.post("/create-new-key", async (req, res) => {
    const { body } = req;
    console.log("body", body);
    await crud.createNewKey({
        key: body.key,
    });
    res.redirect('/configure-users');
});

app.get("/delete-key/:id?", async (req, res) => {
    const { params } = req;
    console.log("req.params", params);
    await crud.deleteKey({
        id: params.id
    });
    res.redirect('/configure-users');
});

app.get("/configure-users", async (req, res) => {
    const list = await crud.getAll();
    const listKey = await crud.getAllKey();
    console.log("all list", list);
    res.render("config", { title: "Configure users", appTitle: "CONFIGURE USERS", data: list, dataKey: listKey });
});

app.get("/sso-login", async (req, res) => {
    console.log("req query", req.query);
    if (!req.query) {
        res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "There is an error when login" });
    }
    else if ( req.query.Action != "internal" ) {
        res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Action unknown!!" });
    }
    else if ( req.query.Action === "internal" && req.query.Secret ) {
        try {
            const key = await crud.getOneKey();
            var cipher = aes256.createCipher(key.key);
            let buff = new Buffer(req.query.Secret, 'base64');
            let text = buff.toString('ascii');
            var decrypted = cipher.decrypt(text);
            console.log("decrypted query", decrypted);

            var data = decrypted.split(';');

            const cipdata = data[0];
            const timestampdata = data[1];
            const ipdata = data[2];
            const currentTimestamp = Date.now();

            if (timestampdata >= (currentTimestamp - 60000) && timestampdata <= (currentTimestamp + 60000)) {
                console.log("cipdata ", cipdata);
                console.log("ipdata ", ipdata);
                // const result = users.find( ({ cip, ip }) => cip.trim().toLocaleLowerCase() === cipdata.trim().toLocaleLowerCase() 
                // && ip.trim().toLocaleLowerCase() === ipdata.trim().toLocaleLowerCase());
                const result = await crud.getOne({ cip: cipdata.trim(), ip: ipdata.trim() });

                console.log("result ", result);
                if (result) {
                    res.render("index", { title: "Home", appTitle: `WELCOME ${result.pharmaname}`, appMessage: "You're logged in!!" });
                }
                else{
                    res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Officine not found" });
                }
            } else {
                res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Timestamp invalid" });
            }

        } catch (error) {
            console.log("error ", error);
            res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Decryption error!!" });
        }
        
    }
    else{
        res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "There is an error when login" });
    }
});

app.post("/sso-login", async (req, res) => {
    console.log("req body", req.body);
    if (!req.body) {
        res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "There is an error when login" });
    }
    else if ( req.body.Action != "internal" ) {
        res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Action unknown!!" });
    }
    else if ( req.body.Action === "internal" && req.body.Secret ) {
        try {
            const key = await crud.getOneKey();
            var cipher = aes256.createCipher(key.key);
            let buff = new Buffer(req.body.Secret, 'base64');
            let text = buff.toString('ascii');
            var decrypted = cipher.decrypt(text);
            console.log("decrypted body", decrypted);

            var data = decrypted.split(';');

            const cipdata = data[0];
            const timestampdata = data[1];
            const ipdata = data[2];
            const currentTimestamp = Date.now();

            if (timestampdata >= (currentTimestamp - 60000) && timestampdata <= (currentTimestamp + 60000)) {
                const result = await crud.getOne({ cip: cipdata.trim(), ip: ipdata.trim() });
                console.log("result ", result);
                if (result) {
                    res.render("index", { title: "Home", appTitle: `WELCOME ${result.pharmaname}`, appMessage: "You're logged in!!" });
                }
                else{
                    res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "User not found" });
                }
            } else {
                res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Timestamp invalid" });
            }

            
        } catch (error) {
            console.log("error ", error);
            res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Decryption error!!" });
        }
        
    }
    else{
        res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "There is an error when login" });
    }
});

app.get("/log-off", (req, res) => {
    res.render("index", { title: "Home", appTitle: "WELCOME TO CRYPTO AES APP", appMessage: "you-re not logged in" });
});


/**
* Server Activation
*/
app.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`);
});
