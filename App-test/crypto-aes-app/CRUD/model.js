const Sequelize = require('sequelize');
const path = require("path");

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: path.join(__dirname,'db','db.sqlite')
});

const User = sequelize.define('user', {
    // attributes
    pharmaname: {
        type: Sequelize.STRING,
        allowNull: false
    },
    cip: {
        type: Sequelize.STRING
        // allowNull defaults to true
    },
    ip: {
        type: Sequelize.STRING
        // allowNull defaults to true
    }
}, {
// options
});

const Key = sequelize.define('key', {
    key: {
        type: Sequelize.STRING
        // allowNull defaults to true
    },
}, {
// options
});

let crud = {};

crud.connection = () =>{
    sequelize.authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
        
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });
}

crud.createTable = () =>{
    User.sync({ force: false }).then(() => {
        console.log("table created if not exist");
    });
    Key.sync({ force: false }).then(() => {
        console.log("table key created if not exist");
    });
}

crud.getAll = () =>{
    return User.findAll().then(users => {
        console.log("All users:", JSON.stringify(users, null, 4));
        return users;
    });
}

crud.getOne = ({cip, ip}) =>{
    return User.findAll({
        limit: 1,
        where: {
            cip: cip,
            ip: ip,
        }
    }).then(users => {
        // console.log("All users:", JSON.stringify(users, null, 4));
        console.log("user ", JSON.stringify(users, null, 4))
        return users[0];
    });
}

crud.createNew = ({ pharmaname, cip, ip }) =>{
    return User.create({pharmaname, cip, ip}).then(user => {
        console.log("User created:", JSON.stringify(user, null, 4));
        return JSON.stringify(user, null, 4);
    });
}

crud.delete = ({ id }) =>{
    return User.destroy({
        where: {
            id: id,
        }
    }).then(() => {
        return 'deleted';
    });
}

crud.getAllKey = () =>{
    return Key.findAll().then(keys => {
        console.log("All keys:", JSON.stringify(keys, null, 4));
        return keys;
    });
}

crud.getOneKey = () =>{
    return Key.findAll({
        limit: 1
    }).then(key => {
        // console.log("All users:", JSON.stringify(users, null, 4));
        console.log("user ", JSON.stringify(key, null, 4))
        return key[0];
    });
}
crud.createNewKey = ({ key }) =>{
    return Key.create({ key }).then(key => {
        console.log("Key created:", JSON.stringify(key, null, 4));
        return JSON.stringify(key, null, 4);
    });
}

crud.deleteKey = ({ id }) =>{
    return Key.destroy({
        where: {
            id: id,
        }
    }).then(() => {
        return 'deleted';
    });
}

module.exports = crud;