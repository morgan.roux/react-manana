/**
* Required External Modules
*/
require('dotenv').config();
const express = require("express");
const path = require("path");
const cors = require('cors');
const bodyParser = require('body-parser');
var clientFtp = require('ftp');
const crud =  require("./CRUD/model");
/**
* App Variables
*/

const app = express();
const port = process.env.PORT || "3006";


/**
*
App Configuration
*/

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
 }));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, "public")));


/**
*
Users Details
*/

const users = [{
    name: "Tokiarivelo",
    token : "abcdefghijklmnopqrstuvwxyz"
},
{
    name: "GCR-Pharma",
    token : "klmnopqrstuvwxyzabcdefghij"
},
]

/**
* Routes Definitions
*/
app.get("/", (req, res) => {
    res.render("index", { title: "Home", appTitle: "WELCOME TO FTP TOKEN EXCHANGE APP", appMessage: "you-re not logged in" });
    crud.connection();
    crud.createTable();
});

app.post("/create-new", async (req, res) => {
    const { body } = req;
    console.log("body", body);
    await crud.createNew({
        name: body.name,
        token : body.token,
    });
    res.redirect('/configure-users');
});

app.get("/delete-user/:id?", async (req, res) => {
    const { params } = req;
    console.log(req.params);
    await crud.delete({ id: params.id });
    res.redirect('/configure-users');
});

app.get("/configure-users", async (req, res) => {
    const list = await crud.getAll();
    res.render("config", { title: "Configure users", appTitle: "CONFIGURE USERS", data: list });
});

app.get("/sso-login", async (req, res) => {
    console.log(req.query);
    if (!req.query) {
        res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "There is an error when login" });
    }
    else{
        const { query } = req;
        // const result = users.find( ({ token }) => token === req.query.token );
        const result = await crud.getOne({ token: query.token.trim()});
        console.log("user", result);
        if (result) {
            res.render("index", { title: "Home", appTitle: `WELCOME ${result.name}`, appMessage: "You're logged in!!" });
        }
        else{
            res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "User not found" });
        }
    }
});

app.post("/sso-login", async (req, res) => {
    console.log(req.body);
    if (!req.body) {
        res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "There is an error when login" });
    }
    else{
        const { body } = req;
        // const result = users.find( ({ token }) => token === req.body.token );
        const result = await crud.getOne({ token: body.token.trim()});
        console.log("user", result);
        if (result) {
            res.render("index", { title: "Home", appTitle: `WELCOME ${result.name}`, appMessage: "You're logged in!!" });
        }
        else{
            res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "User not found" });
        }
    }
});

app.get("/log-off", (req, res) => {
    res.render("index", { title: "Home", appTitle: "WELCOME TO FTP TOKEN EXCHANGE APP", appMessage: "you-re not logged in" });
});

app.get('/upload-ftp-file', async (req, res) =>{
    var c = new clientFtp();
    c.on('ready', function() {
      c.put('./JETON_V2_Pharmavie.txt', 'JETON_V2_Pharmavie.txt', function(err) {
        if (err) throw err;
        c.end();
      });
    });
    // connect to localhost:21 as anonymous
    c.connect({
      host: process.env.PUBLICHOST,
      port: parseInt(process.env.FTP_PORT),
      user: "gcr-pharma",
      password: "Pharma@123456789"
    });
    res.send("uploaded");
  });
/**
* Server Activation
*/
app.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`);
});
