// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"Focm":[function(require,module,exports) {
/**
* Required External Modules
*/
const express = require("express");

const path = require("path");

const cors = require('cors');

const bodyParser = require('body-parser');

var rp = require('request-promise');
/**
* App Variables
*/


const app = express();
const port = process.env.PORT || "3005";
/**
*
App Configuration
*/

app.use(cors());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, "public")));
/**
*
Users Details
*/

const users = [{
  name: "GCR-Pharma",
  login: "superadmin@gcr-pharma.com"
}, {
  name: "Tokiarivelo",
  login: "tokiarivelo@digitalbusiness4ever.com"
}];
/**
* Routes Definitions
*/

app.get("/", (req, res) => {
  res.render("index", {
    title: "Home",
    appTitle: "WELCOME TO TOKEN EXCHANGE APP",
    appMessage: "you-re not logged in"
  });
});
app.get("/sso-login", (req, res) => {
  console.log("req query", req.query);
  const options = {
    uri: `http://sso/confirm-sso-${req.query.token}`,
    headers: {
      'User-Agent': 'Request-Promise'
    },
    json: true // Automatically parses the JSON string in the response

  };
  rp(options).then(result => {
    console.log(result);
    const find = users.find(({
      login
    }) => login === result.email);

    if (find) {
      res.render("index", {
        title: "Home",
        appTitle: `WELCOME ${find.name}`,
        appMessage: "You're logged in!!"
      });
    } else {
      res.render("index", {
        title: "Home",
        appTitle: "ERROR",
        appMessage: "User not found"
      });
    }
  }).catch(err => {
    console.log(err);
    res.render("index", {
      title: "Home",
      appTitle: "ERROR",
      appMessage: "Error parsing json!!"
    });
  });
});
app.post("/sso-login", (req, res) => {
  console.log("req body", req.body);
  const options = {
    uri: `http://sso/confirm-sso-${req.body.token}`,
    headers: {
      'User-Agent': 'Request-Promise'
    },
    json: true // Automatically parses the JSON string in the response

  };
  rp(options).then(result => {
    console.log(result);
    const find = users.find(({
      login
    }) => login === result.email);

    if (find) {
      res.render("index", {
        title: "Home",
        appTitle: `WELCOME ${find.name}`,
        appMessage: "You're logged in!!"
      });
    } else {
      res.render("index", {
        title: "Home",
        appTitle: "ERROR",
        appMessage: "User not found"
      });
    }
  }).catch(err => {
    console.log(err);
    res.render("index", {
      title: "Home",
      appTitle: "ERROR",
      appMessage: "Error parsing json!!"
    });
  });
});
app.get("/log-off", (req, res) => {
  res.render("index", {
    title: "Home",
    appTitle: "WELCOME TO TOKEN EXCHANGE APP",
    appMessage: "you-re not logged in"
  });
});
/**
* Server Activation
*/

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});
},{}]},{},["Focm"], null)
//# sourceMappingURL=/index.js.map