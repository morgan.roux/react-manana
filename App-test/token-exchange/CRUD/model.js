const Sequelize = require('sequelize');
const path = require("path");

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: path.join(__dirname,'db','db.sqlite')
});

const User = sequelize.define('user', {
    // attributes
    name: {
        type: Sequelize.STRING,
    },
    login: {
        type: Sequelize.STRING
        // allowNull defaults to true
    },
}, {
// options
});

let crud = {};

crud.connection = () =>{
    sequelize.authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
        
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });
}

crud.createTable = () =>{
    User.sync({ force: false }).then(() => {
        console.log("table created if not exist");
    });
}

crud.getAll = () =>{
    return User.findAll().then(users => {
        console.log("All users:", JSON.stringify(users, null, 4));
        return users;
    });
}

crud.getOne = ({login}) =>{
    return User.findAll({
        limit: 1,
        where: {
            login,
        }
    }).then(users => {
        // console.log("All users:", JSON.stringify(users, null, 4));
        console.log("user ", JSON.stringify(users, null, 4))
        return users[0];
    });
}

crud.createNew = ({ name, login }) =>{
    return User.create({ name, login }).then(user => {
        console.log("User created:", JSON.stringify(user, null, 4));
        return JSON.stringify(user, null, 4);
    });
}

crud.delete = ({ id }) =>{
    return User.destroy({
        where: {
            id: id,
        }
    }).then(() => {
        return 'deleted';
    });
}

module.exports = crud;