# -----------------------------------------------------------------------------
# BUILD STAGE
FROM node:12-alpine as builder

WORKDIR /app/token-exchange

# Copy package.json for caching node_modules
COPY package.json /app/token-exchange/package.json

RUN npm -v

RUN node -v

# Copy source files
COPY . /app/token-exchange

# Install packages
RUN npm install

RUN npm install sqlite3 --save

RUN npm run build:prod

RUN cp -R ./views ./dist
RUN cp -R ./public ./dist

EXPOSE 3005

# CMD ["npm", "run", "start:dev"]
CMD ["npm", "run", "start:dev"]