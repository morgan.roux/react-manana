/**
* Required External Modules
*/
require('dotenv').config();
const express = require("express");
const path = require("path");
const cors = require('cors');
const bodyParser = require('body-parser');
var rp = require('request-promise');
const crud =  require("./CRUD/model");
/**
* App Variables
*/

const app = express();
const port = process.env.PORT || "3005";


/**
*
App Configuration
*/

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
 }));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, "public")));


/**
*
Users Details
*/

const users = [{
    name: "GCR-Pharma",
    login : "superadmin@gcr-pharma.com",
},
{
    name: "Tokiarivelo",
    login : "tokiarivelo@digitalbusiness4ever.com",
},
]

/**
* Routes Definitions
*/
app.get("/", (req, res) => {
    res.render("index", { title: "Home", appTitle: "WELCOME TO TOKEN EXCHANGE APP", appMessage: "you-re not logged in" });
    crud.connection();
    crud.createTable();
});

app.post("/create-new", async (req, res) => {
    const { body } = req;
    console.log("body", body);
    await crud.createNew({
        name: body.name,
        login : body.login,
    });
    res.redirect('/configure-users');
});

app.get("/delete-user/:id?", async (req, res) => {
    const { params } = req;
    console.log(req.params);
    await crud.delete({ id: params.id });
    res.redirect('/configure-users');
});

app.get("/configure-users", async (req, res) => {
    const list = await crud.getAll();
    res.render("config", { title: "Configure users", appTitle: "CONFIGURE USERS", data: list });
});

app.get("/sso-login", (req, res) => {
    console.log("req query", req.query);
    const options = {
        uri: `${process.env.SAML_SSO_ENDPOINT}/confirm-sso-${req.query.token}`,
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    };

    rp(options)
        .then(async (result) =>{
            console.log(result);
            // const find = users.find( ({ login }) => login === result.email );
            const find = await crud.getOne({ login: result.email.trim()});
            
            if (find) {
                res.render("index", { title: "Home", appTitle: `WELCOME ${find.name}`, appMessage: "You're logged in!!" });
            }
            else{
                res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "User not found" });
            }
        })
        .catch((err) =>{
            console.log(err);
            res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Error parsing json!!" });
        });
});

app.post("/sso-login", (req, res) => {
    console.log("req body", req.body);
    const options = {
        uri: `${process.env.SAML_SSO_ENDPOINT}/confirm-sso-${req.body.token}`,
        headers: {
            'User-Agent': 'Request-Promise'
        },
        json: true // Automatically parses the JSON string in the response
    };

    rp(options)
        .then(async (result) =>{
            console.log(result);
            // const find = users.find( ({ login }) => login === result.email );
            const find = await crud.getOne({ login: result.email.trim()});
            if (find) {
                res.render("index", { title: "Home", appTitle: `WELCOME ${find.name}`, appMessage: "You're logged in!!" });
            }
            else{
                res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "User not found" });
            }
        })
        .catch((err) =>{
            console.log(err);
            res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Error parsing json!!" });
        });
});

app.get("/log-off", (req, res) => {
    res.render("index", { title: "Home", appTitle: "WELCOME TO TOKEN EXCHANGE APP", appMessage: "you-re not logged in" });
});

/**
* Server Activation
*/
app.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`);
});
