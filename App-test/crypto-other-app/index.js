/**
* Required External Modules
*/
const express = require("express");
const path = require("path");
const cors = require('cors');
const bodyParser = require('body-parser');
const aes256 = require('aes256');
const qs = require('qs');
const crud =  require("./CRUD/model");

/**
* App Variables
*/

const app = express();
const port = process.env.PORT || "3007";


/**
*
App Configuration
*/

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
 }));

 app.use(bodyParser.json());
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.static(path.join(__dirname, "public")));
app.set('query parser', function (str) {
    return qs.parse(str, { decode: function (s) { return decodeURIComponent(s); } });
});

/**
*
Users Details
*/

const users = [{
    name: "GCR-Pharma",
    login : "superadmin@gcr-pharma.com",
},
{
    name: "Tokiarivelo",
    login : "tokiarivelo@digitalbusiness4ever.com",
},
{
    name: "Tokiarivelo Andriamandroso",
    login : "anjaniaina.toky14@gmail.com",
},
]

/**
 * utils
 */
// const key = "secretTest"
const formateDate = (date , separator) =>{
    var dd = date.getDate();
    var mm = date.getMonth()+1; 
    var yyyy = date.getFullYear();

    let daystring = dd.toString();
    let monthstring = mm.toString();
    if(dd < 10) 
        daystring ='0'+ dd; 
    
    if(mm<10) 
        monthstring = '0'+mm;

    return `${yyyy}${separator}${monthstring}${separator}${daystring}`;
}

/**
* Routes Definitions
*/
app.get("/", (req, res) => {
    res.render("index", { title: "Home", appTitle: "WELCOME TO CRYPTO OTHER APP", appMessage: "you-re not logged in" });
    crud.connection();
    crud.createTable();
});

app.post("/create-new", async (req, res) => {
    const { body } = req;
    console.log("body", body);
    await crud.createNew({
        name: body.name,
        login : body.login,
    });
    res.redirect('/configure-users');
});

app.get("/delete-user/:id?", async (req, res) => {
    const { params } = req;
    console.log(req.params);
    await crud.delete({ id: params.id });
    res.redirect('/configure-users');
});

app.post("/create-new-key", async (req, res) => {
    const { body } = req;
    console.log("body", body);
    await crud.createNewKey({
        key: body.key,
    });
    res.redirect('/configure-users');
});

app.get("/delete-key/:id?", async (req, res) => {
    const { params } = req;
    console.log("req.params", params);
    await crud.deleteKey({
        id: params.id
    });
    res.redirect('/configure-users');
});

app.get("/configure-users", async (req, res) => {
    const list = await crud.getAll();
    const listKey = await crud.getAllKey();
    res.render("config", { title: "Configure users", appTitle: "CONFIGURE USERS", data: list, dataKey: listKey });
});

app.get("/sso-login", async (req, res) => {
    try {
        console.log(req.query);
        console.log(req.originalUrl);

        const key = await crud.getOneKey();

        if (!req.query) {
            res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "There is an error when login" });
        }

        console.log('parsed ', req.query.token.replace(/\s/g, "+"));
        var decrypted = aes256.decrypt(key.key, req.query.token.replace(/\s/g, '+'));
        var splitadData = decrypted.split("&");

        console.log(decrypted)

        const username = splitadData[0].split('=')[1];
        const date = splitadData[1].split('=')[1];

        const today = formateDate(new Date(), '-');

        console.log("today", today)

        if (date.trim().toLocaleLowerCase() === today.trim().toLocaleLowerCase()) {
            // const result = users.find( ({ login }) => login === username);
            const result = await crud.getOne({login: username});
            console.log("result ", result);
            if (result) {
                res.render("index", { title: "Home", appTitle: `WELCOME ${result.name}`, appMessage: "You're logged in!!" });
            }
            else{
                res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "User not found" });
            }
        }
        else{
            res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Date expired" });
        }
    } catch (error) {
        res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Decryption failed" });
    }
    
});

app.post("/sso-login", async (req, res) => {
    try {
        console.log("req body", req.body);

        if (!req.body) {
            res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "There is an error when login" });
        }

        const key = await crud.getOneKey();

        console.log('parsed ', req.body.token.replace(/\s/g, "+"));
        var decrypted = aes256.decrypt(key.key, req.body.token.replace(/\s/g, '+'));
        var splitadData = decrypted.split("&");

        console.log(decrypted)

        const username = splitadData[0].split('=')[1];
        const date = splitadData[1].split('=')[1];

        const today = formateDate(new Date(), '-');

        console.log("today", today)

        if (date.trim().toLocaleLowerCase() === today.trim().toLocaleLowerCase()) {
            const result = await crud.getOne({login: username});
            // const result = users.find( ({ login }) => login === username);
            if (result) {
                res.render("index", { title: "Home", appTitle: `WELCOME ${result.name}`, appMessage: "You're logged in!!" });
            }
            else{
                res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "User not found" });
            }
        }
        else{
            res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Date expired" });
        }
    } catch (error) {
        res.render("index", { title: "Home", appTitle: "ERROR", appMessage: "Decryption failed" });
    }
});

app.get("/log-off", (req, res) => {
    res.render("index", { title: "Home", appTitle: "WELCOME TO CRYPTO OTHER APP", appMessage: "you-re not logged in" });
});


/**
* Server Activation
*/
app.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`);
});
