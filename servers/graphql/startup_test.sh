#!/bin/sh

npm run generate:dev

./wait-for.sh redis:6379 -t 3000

#./wait-for.sh rabbitmq:5672 -t 3000

./wait-for.sh elasticsearch:9200 -t 3000

./wait-for.sh postgres:5432 -t 3000

npm run merge:config

npm run prisma:deploy:test

nohup npm run start &

./wait-for.sh 0.0.0.0:4000 -t 3000

npm run test