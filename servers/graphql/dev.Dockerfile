FROM node:10-alpine

COPY package.json /graphql/package.json

WORKDIR /graphql

RUN npm install

COPY . /graphql

ENTRYPOINT ["./startup_dev.sh"]