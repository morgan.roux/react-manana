require('dotenv').config();
import { prisma } from '../src/generated/prisma-client';
import fs = require('fs');
import path = require('path');
import bcrypt = require('bcryptjs');
import columns from './columns';
import columnsTodelete from './columnsToDelete';
import config from './config';
import columnsToConvert from './columnsToConvert';
import { dataFromCsv, mergeArraysKeyValue } from './csv';
import { SUPER_ADMINISTRATEUR } from '../src/constants/roles';
import lodash = require('lodash');
import csv = require('fast-csv');
import { WatchTime } from '../src/services/time';

const watch = new WatchTime();

const isEnv = (env: string) => {
  return process.env.NODE_ENV && process.env.NODE_ENV === env;
};

const pathName = path.join(__dirname, true ? config.dev_path_name : config.path_name);

const converters = {
  toInteger: (value: string) => (value ? parseInt(value, 10) : null),
  toFloat: (value: string) => (value ? parseFloat(value) : null),
  toDate: (value: any) => {
    const val = value.split(' ');
    return value !== '' ? val[0] : null;
  },
  toBoolean: (value: string) => {
    if (value === '0') return false;
    else if (value === '1') return true;
    return value ? eval(value) : false;
  },
  toHash: (value: string) => (value ? bcrypt.hash(value, 10) : 'default'),
  toNull: (value: any) => (value !== '' ? value : null),
};

const saveFiles = {
  ACTE: { create: 'createActe', relGroupement: false },
  ACTIVITE_TYPE: { create: 'createActiviteType', relGroupement: false },
  ACTUALITE_ORIGINE: { create: 'createActualiteOrigine', relGroupement: false },
  AUTOMATE: { create: 'createAutomate', relGroupement: false },
  CANAL_GAMME: { create: 'createCanalGamme', relGroupement: false },
  COMMANDE_CANAL: { create: 'createCommandeCanal', relGroupement: false },
  COMMANDE_MOYEN: { create: 'createCommandeMoyen', relGroupement: false },
  COMMANDE_TYPE: { create: 'createCommandeType', relGroupement: false },
  COMMANDE_STATUT: { create: 'createCommandeStatut', relGroupement: true },
  CONCURRENT: { create: 'createConcurrent', relGroupement: false },
  CONTRAT_STATUS: { create: 'createContratStatut', relGroupement: false },
  FAMILLE: { create: 'createFamille', relGroupement: false },
  GAMME_CATALOGUE: { create: 'createGammeCatalogue', relGroupement: false },
  GAMME_SERVICE: { create: 'createGammeService', relGroupement: false },
  GENERIQUEUR: { create: 'createGeneriqueur', relGroupement: false },
  GRANDE_REGION: { create: 'createGrandeRegion', relGroupement: false },
  GROSSISTE: { create: 'createGrossiste', relGroupement: false },
  ITEM: { create: 'createItem', relGroupement: false },
  LABORATOIRE_SUITE: { create: 'createLaboratoireSuite', relGroupement: false },
  LISTE: { create: 'createListe', relGroupement: false },
  MODULE: { create: 'createModule', relGroupement: false },
  MOTIF: { create: 'createMotif', relGroupement: false },
  PARAMETER_TYPE: { create: 'createParameterType', relGroupement: false },
  PARAMETER_GROUPE: { create: 'createParameterGroupe', relGroupement: false },
  QUALITE: { create: 'createQualite', relGroupement: false },
  ROLE: { create: 'createRole', relGroupement: false },
  SERVICE: { create: 'createService', relGroupement: false },
  SERVICE_INFORMATIQUE: { create: 'createServicePharmacie', relGroupement: false },
  SMYLEY: { create: 'createSmyley', relGroupement: true },
  SSII: { create: 'createSSII', relGroupement: false },
  STATUT_LIGNE: { create: 'createStatutLigne', relGroupement: false },
  TAUX_SS: { create: 'createTauxSS', relGroupement: false },
  TRANSMISSION: { create: 'createTransmission', relGroupement: false },
  TVA: { create: 'createTVA', relGroupement: false },
  TYPOLOGIE: { create: 'createTypologie', relGroupement: false },
  USER_ORIGINE: { create: 'createUserOrigine', relGroupement: false },
  CANAL_SOUS_GAMME: { create: 'createCanalSousGamme', relGroupement: false },
  FONCTION: { create: 'createPersonnelFonction', relGroupement: false },
  TITULAIRE_FONCTION: { create: 'createTitulaireFonction', relGroupement: false },
  LABORATOIRE: { create: 'createLaboratoire', relGroupement: false },
  LOGICIEL: { create: 'createLogiciel', relGroupement: false },
  PARAMETER: { create: 'createParameter', relGroupement: false },
  REGION: { create: 'createRegion', relGroupement: false },
  SOUS_GAMME_CATALOGUE: { create: 'createSousGammeCatalogue', relGroupement: false },
  TRAITEMENT: { create: 'createTraitement', relGroupement: false },
  DEPARTEMENT: { create: 'createDepartement', relGroupement: false },
  PERSONNEL: { create: 'createPersonnel', relGroupement: true },
  AFFECTATION_PERSONNEL: { create: 'createPersonnelAffectation', relGroupement: false },
  PHARMACIE: { create: 'createPharmacie', relGroupement: true },
  TITULAIRE: { create: 'createTitulaire', relGroupement: true },
  AFFECTATION_PRESIDENT_REGION: {
    create: 'createTitulaireAffectation',
    relGroupement: false,
  },
  GROUPE_CLIENT: { create: 'createGroupeClient', relGroupement: false },
  PRODUIT: { create: 'createProduit', relGroupement: false },
  PRODUIT_CANAL: { create: 'createProduitCanal', relGroupement: false },
  PRODUIT_CODE: { create: 'createProduitCode', relGroupement: false },
  PRODUIT_FAMILLE: { create: 'createProduitFamille', relGroupement: false },
  PRODUIT_EQUIVALENT_STRICT: { create: 'createProduitEquivalentStrict', relGroupement: false },
  PRODUIT_TECH_REG: { create: 'createProduitTechReg', relGroupement: false },
  PRODUIT_FLAG: { create: 'createProduitFlag', relGroupement: false },
  PRODUIT_GROUPE_GEN: { create: 'createProduitGroupeGen', relGroupement: false },
  PRODUIT_TIPS: { create: 'createProduitTips', relGroupement: false },
  PRODUIT_UCD: { create: 'createProduitUcd', relGroupement: false },
  LIBELLE_DIVERS: { create: 'createLibelleDivers', relGroupement: false },
  GROUPE_GEN: { create: 'createGroupeGen', relGroupement: false },
  CIS_AGENCE: { create: 'createCisAgence', relGroupement: false },
  REMISE: { create: 'createRemise', relGroupement: false },
  REMISE_LIGNE: { create: 'createRemiseLigne', relGroupement: false },
  REMISE_PANACHEE: { create: 'createRemisePanachee', relGroupement: false },
  REMISE_DETAIL: { create: 'createRemiseDetail', relGroupement: false },
  PRODUIT_CATEGORIE: { create: 'createProduitCategorie', relGroupement: false },
  PRODUIT_TYPE_CATEGORIE: { create: 'createProduitTypeCategorie', relGroupement: false },
  PRODUIT_TYPE_CODE: { create: 'createProduitTypeCode', relGroupement: false },

  USER_ROLE: { create: 'createUserRole', relGroupement: false },
  USER: { create: 'createUser', relGroupement: true },
  GROUPEMENT: { create: 'createGroupement', relGroupement: false },
};

const tableRelation = ['TIT_PHARMACIE', 'GROUPE_CLIENT_CLIENT'];

async function main() {
  fs.readdir(pathName, async function(err, items) {
    // Create default groupement.
    const result = prisma.groupement({ id: 'ck5zk8hs8007b0743gzhyy2lm' });

    console.log('IS ENV DEV', isEnv('dev'));

    result.then(async groupement => {
      return new Promise(async (resolve, reject) => {
        console.log('Groupement ', groupement);

        // filter or return
        const filesArray = items;
        for (let i = 0; i < filesArray.length; i++) {
          const file = filesArray[i];
          const name = file.split('-').length > 1 ? file.split('-')[1] : file;
          const fileName = path.parse(name).name;

          if (!tableRelation.includes(fileName)) {
            // datas relation table
            let titulairePharmacies: any = [];
            let pharmacies: any = [];
            let groupeClientClients: any = [];

            /*  if (fileName === 'TITULAIRE') {
              titulairePharmacies = await dataFromCsv(
                columns.TIT_PHARMACIE,
                `${pathName}/TIT_PHARMACIE.csv`,
              );
              pharmacies = await _getIDPharmacies();
            } else if (fileName === 'GROUPE_CLIENT') {
              console.log('fileName', fileName);

              groupeClientClients = await dataFromCsv(
                columns.GROUPE_CLIENT_CLIENT,
                `${pathName}/g-GROUPE_CLIENT_CLIENT.csv`,
              );
              pharmacies = await _getIDPharmacies();
            } */

            const readable = fs.createReadStream(`${pathName}/${file}`).pipe(
              csv.parse({
                delimiter: ';',
              }),
            );
            try {
              if (['PARAMETER', 'PARAMETER_GROUPE'].includes(fileName))
                await readAndSaveData(readable, {
                  fileName,
                  columns: columns[fileName],
                  groupement,
                  titulairePharmacies,
                  pharmacies,
                  groupeClientClients,
                });
            } catch (err) {
              console.log(fileName, err);
            }
          }
        }
        resolve();
      });
    });
    /* .then(async _ => {
        // Super admin
        if (isEnv('dev') || isEnv('test')) {
          await prisma.createUserRole({
            idUser: {
              create: {
                email: process.env.SUPER_ADMIN_EMAIL,
                emailConfirmed: true,
                passwordHash: await converters.toHash(process.env.SUPER_ADMIN_PWD),
                passwordSecretAnswer: 'super_admin',
                phoneNumberConfirmed: true,
                userName: 'GCR-PHARMA',
                status: 'ACTIVATED',
              },
            },
            idRole: {
              connect: {
                code: SUPER_ADMINISTRATEUR,
              },
            },
          });
        }
      }); */
  });
}

const readAndSaveData = async (
  readable,
  { fileName, columns, groupement, titulairePharmacies, pharmacies, groupeClientClients },
) => {
  for await (const eachData of readable) {
    watch.start();

    if (eachData.length === columns.length) {
      const row = mergeArraysKeyValue(eachData, columns);
      console.log('data ====> ', row);

      await _populateDatabase({
        fileName,
        data: row,
        groupement,
        titulairePharmacies,
        pharmacies,
        groupeClientClients,
      });
    }
    watch.stop();
    watch.printElapsed(`[ SAVE DATA ] duration : `);
  }

  return new Promise((resolve, reject) => resolve());
};

interface IDATA {
  fileName: string;
  data: any;
  groupement: any;
  titulairePharmacies: any[];
  pharmacies: any[];
  groupeClientClients: any[];
}

const _populateDatabase = async ({
  fileName,
  data,
  groupement,
  titulairePharmacies,
  pharmacies,
  groupeClientClients,
}: IDATA) => {
  let value: any = data;
  const column = Object.keys(value);
  for (const key in column) {
    const converter = columnsToConvert[column[key]] || false;
    value[column[key]] = converter
      ? converters[converter](value[column[key]]) &&
        typeof (converters[converter](value[column[key]]).then === 'function')
        ? await converters[converter](value[column[key]])
        : converters[converter](value[column[key]])
      : value[column[key]];
  }

  const connects = [];
  if (titulairePharmacies.length) {
    /* relation entre pharmacie et titulaire : seed table titulairePharmacies */
    for (const res in titulairePharmacies) {
      const titulaire = parseInt(titulairePharmacies[res].titulaire, 10);
      const pharmacie = parseInt(titulairePharmacies[res].pharmacie, 10);
      if (titulaire === parseInt(value.resipIdTitulaire, 10) && pharmacies.includes(pharmacie)) {
        connects.push({ resipIdPharmacie: pharmacie });
      }
    }
  } else if (groupeClientClients.length > 0) {
    /* relation entre groupe client et pharmacie : seed table groupeClientClients */
    for (const res in groupeClientClients) {
      const reispIdGroupe = parseInt(groupeClientClients[res].groupeClient, 10);
      const resipIdPharmacie = parseInt(groupeClientClients[res].pharmacie, 10);
      if (
        reispIdGroupe === parseInt(value.reispIdGroupe, 10) &&
        pharmacies.includes(resipIdPharmacie)
      ) {
        connects.push({ resipIdPharmacie });
      }
    }
  }

  if (connects.length > 0) {
    console.log('connects', connects);

    if (groupeClientClients.length || titulairePharmacies.length) {
      value.pharmacies = {
        connect: connects,
      };
    }
  }

  switch (fileName) {
    case 'PERSONNEL':
      value = connectOne(value, 'service', 'resipIdService');
      break;
    case 'FONCTION':
      value = connectOne(value, 'service', 'resipIdService');
      break;
    case 'REGION':
      value = connectOne(value, 'grandeRegion', 'resipIdGRegion');
      break;
    case 'DEPARTEMENT':
      value = connectOne(value, 'region', 'resipIdRegion');
      break;
    case 'AFFECTATION_PRESIDENT_REGION':
      value = connectOne(value, 'departement', 'resipIdDepartement');
      value = connectOne(value, 'titulaire', 'resipIdTitulaire');
      break;
    case 'PHARMACIE':
      value = connectOne(value, 'departement', 'resipIdDepartement');
      break;
    case 'AFFECTATION_PERSONNEL':
      value = connectOne(value, 'personnel', 'resipIdPersonnel');
      value = connectOne(value, 'personnelFonction', 'resipIdFonction');
      value = connectOne(value, 'departement', 'resipIdDepartement');
      break;
    case 'PARAMETER':
      value = connectOne(value, 'idParameterType', 'code');
      value = connectOne(value, 'idParameterGroupe', 'code');
      break;
    case 'CANAL_GAMME':
      value['canal'] = {
        connect: {
          code: 'PFL',
        },
      };
      break;
    case 'CANAL_SOUS_GAMME':
      value = connectOne(value, 'canalGamme', 'codeGamme');
      break;
    case 'SOUS_GAMME_CATALOGUE':
      value = connectOne(value, 'gammeCatalogue', 'codeGamme');
      break;
    case 'REMISE':
      value['source'] = 'PERMANENT';
      value['commandeCanal'] = {
        connect: {
          code: 'PFL',
        },
      };
      break;
    case 'REMISE_DETAIL':
      value = connectTable(value, 'remise', 'resipIdRemise', value['remise']);
      break;
    case 'REMISE_LIGNE':
      value = connectTable(value, 'remise', 'resipIdRemise', value['remise']);
      value = connectTable(value, 'groupeClient', 'codeGroupe', value['groupeClient']);
      value = connectTable(
        value,
        'produitCanal',
        'id',
        await _produitCanalFromCode(value['produitCanal']),
      );
      break;
    case 'REMISE_PANACHEE':
      value = connectTable(value, 'remise', 'resipIdRemise', value['remise']);
      value = connectTable(value, 'groupeClient', 'codeGroupe', value['groupeClient']);
      value = connectTable(
        value,
        'produitCanal',
        'id',
        await _produitCanalFromCode(value['produitCanal']),
      );
      break;
    case 'TRAITEMENT':
      value = connectOne(value, 'module', 'code');
      break;
    case 'LOGICIEL':
      value = connectOne(value, 'ssii', 'resipIdSSII');
      break;
    case 'LABORATOIRE':
      if (value['resipIdLaboSuite'])
        value = connectTable(value, 'laboSuite', 'resipIdLaboSuite', value['resipIdLaboSuite']);
      break;
    case 'GROUPE_GEN':
      if (value['resipCodeGroupePere'])
        value = connectTable(
          value,
          'groupeGenPere',
          'resipCodeGroupe',
          value['resipCodeGroupePere'],
        );
      break;
    case 'LIBELLE_DIVERS':
      value['code'] = parseInt(value['code'], 10);
      break;
    case 'PRODUIT':
      value['type'] = parseInt(value['type'], 10);
      break;
    case 'PRODUIT_CANAL':
      if (value['sousGamme'])
        value = connectTable(value, 'sousGamme', 'resipIdSousGamme', value['sousGamme']);
      if (value['sousGammeCatalogue'])
        value = connectTable(
          value,
          'sousGammeCatalogue',
          'resipIdSousGamme',
          value['sousGammeCatalogue'],
        );
      value = connectTable(value, 'produit', 'id', await _produitFromCode(value['produit']));
      value = connectTable(value, 'canal', 'code', 'PFL');
      break;
    case 'PRODUIT_CODE':
      if (value['resipIdProduit'])
        value = connectTable(value, 'produit', 'resipIdProduit', value['resipIdProduit']);
      break;
    case 'PRODUIT_EQUIVALENT_STRICT':
      value = connectTable(value, 'produitRef', 'resipIdProduit', value['resipIdProduitRef']);
      value = connectTable(
        value,
        'produitEquivalent',
        'resipIdProduit',
        value['resipIdProduitEquivalent'],
      );
      break;
    case 'PRODUIT_FAMILLE':
      value['codeFamille'] = value['resipCodeFamille'];
      value = connectTable(value, 'famille', 'resipCodeFamille', value['resipCodeFamille']);
      value = connectTable(value, 'produit', 'resipIdProduit', value['resipIdProduit']);
      break;
    case 'PRODUIT_FLAG':
      value = connectTable(value, 'produit', 'resipIdProduit', value['resipIdProduit']);
      break;
    case 'PRODUIT_GROUPE_GEN':
      value['type'] = parseInt(value['type'], 10);
      value = connectTable(value, 'produit', 'resipIdProduit', value['resipIdProduit']);
      value = connectTable(value, 'groupeGen', 'resipCodeGroupe', value['resipCodeGroupe']);
      break;
    case 'PRODUIT_TIPS':
      value = connectTable(value, 'produit', 'resipIdProduit', value['resipIdProduit']);
      break;
    case 'PRODUIT_TECH_REG':
      value = connectTable(value, 'produit', 'resipIdProduit', value['resipIdProduit']);
      if (value['resipCis'])
        value = connectTable(value, 'cisAgence', 'resipCodeCis', value['resipCis']);
      if (value['resipIdLaboTitulaire'])
        value = connectTable(value, 'laboTitulaire', 'resipIdLabo', value['resipIdLaboTitulaire']);
      if (value['resipIdLaboExploitant'])
        value = connectTable(
          value,
          'laboExploitant',
          'resipIdLabo',
          value['resipIdLaboExploitant'],
        );
      if (value['resipIdLaboDistributeur'])
        value = connectTable(
          value,
          'laboDistirbuteur',
          'resipIdLabo',
          value['resipIdLaboDistributeur'],
        );
      if (value['resipCodeTVA'])
        value = connectTable(value, 'tva', 'resipCodeTVA', value['resipCodeTVA']);
      if (value['resipCodeActe'])
        value = connectTable(value, 'acte', 'resipCodeActe', value['resipCodeActe']);
      if (value['resipCodeListe'])
        value = connectTable(value, 'liste', 'resipCodeListe', value['resipCodeListe']);
      if (value['resipCodeStockage'])
        value = connectTable(value, 'libelleStockage', 'resipCodeInfo', value['resipCodeStockage']);
      if (value['resipCodeTauxSS'])
        value = connectTable(value, 'tauxss', 'resipCodeTauxSS', value['resipCodeTauxSS']);
      if (value['resipCodeStatut'])
        value = connectTable(value, 'libelleStatut', 'resipCodeInfo', value['resipCodeStatut']);
      if (value['resipAttributCasher'])
        value = connectTable(value, 'libelleCasher', 'resipCodeInfo', value['resipAttributCasher']);
      if (value['resipTypeConditionnement'])
        value = connectTable(
          value,
          'libelleConditionnement',
          'resipCodeInfo',
          value['resipTypeConditionnement'],
        );
      if (value['resipIdProduitRemplacant'])
        value = connectTable(
          value,
          'produitRemplacant',
          'resipIdProduit',
          value['resipIdProduitRemplacant'],
        );
      if (value['resipUcd7'])
        value = connectTable(value, 'produitUcd', 'resipUcd7', value['resipUcd7']);
      break;
  }

  try {
    console.log('[ SEED ]', process.env.NODE_ENV ? process.env.NODE_ENV : 'prod', fileName);
    return _saveData(fileName, value, saveFiles, columnsTodelete, groupement).catch(error =>
      console.error('[ ERROR DATA ] ::: ', value, error),
    );
  } catch (err) {
    // console.log('err', err);
    return new Error(err);
  }
};

const _saveData = async (fileName, value, saveFiles, columnsTodelete, groupement) => {
  const valueToSave = columnsTodelete[fileName]
    ? lodash.omit(value, columnsTodelete[fileName])
    : value;
  return prisma[saveFiles[fileName].create](
    saveFiles[fileName].relGroupement
      ? { ...valueToSave, groupement: { connect: { id: groupement.id } } }
      : valueToSave,
  );
};

const _getIDPharmacies = async () => {
  return prisma
    .pharmacies()
    .then(pharmacies => pharmacies.map(pharmacie => pharmacie.resipIdPharmacie));
};

const _produitFromCode = async (cip: string) => {
  const produitCodes = await prisma.produitCodes({ where: { code: cip }, first: 1 });
  return produitCodes && produitCodes.length
    ? await prisma
        .produitCode({ id: produitCodes[0].id })
        .produit()
        .id()
    : null;
};

const _produitCanalFromCode = async (cip: string) => {
  const produit = await _produitFromCode(cip);
  if (!produit) return null;

  const produitCanals = await prisma.produitCanals({
    where: { produit: { id: produit } },
    first: 1,
  });
  return produitCanals && produitCanals.length
    ? await prisma.produitCanal({ id: produitCanals[0].id }).id()
    : null;
};

const connectOne = (value: Object, toConnect: string, attr: string = null) => {
  const toStringAttr = ['module'];
  const id = attr ? attr : toConnect;
  value[toConnect] = value[toConnect]
    ? {
        connect: {
          [id]: toStringAttr.includes(toConnect)
            ? value[toConnect].toString()
            : isNaN(value[toConnect])
            ? value[toConnect]
            : parseInt(value[toConnect], 10),
        },
      }
    : null;
  return value;
};

const connectTable = (
  value: Object,
  libelleConnect: string,
  idToConnect: string,
  valueToConnect: string,
) => {
  value[libelleConnect] = {
    connect: {
      [idToConnect]: valueToConnect,
    },
  };
  return value;
};

main()
  .then(() => console.log('....wait'))
  .catch(e => console.error(e));
