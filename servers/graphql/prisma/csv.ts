import fs = require('fs');
import csv = require('fast-csv');

export const dataFromCsv = async (column: string[], pathName: string) => {
  let items: Object[] = [];

  return new Promise((resolve, reject) => {
    return createReadStream(pathName, async eachData => {
      if (eachData.length === column.length) {
        const row = mergeArraysKeyValue(eachData, column);
        console.log(` [ *${pathName}* ] :::+++++++++> `, row);
        items = [...items, row];
      }
    })
      .on('error', err => reject(err))
      .on('end', () => {
        resolve(items);
      });
  });
};

export const createReadStream = (pathName: string, callBack: (data: any) => void) => {
  const readable = fs.createReadStream(pathName);
  return readable
    .pipe(
      csv.parse({
        delimiter: ';',
      }),
    )
    .on('data', callBack);
};

export const mergeArraysKeyValue = (rows: any[], columns: any[]) => {
  return rows.reduce(function(result, field, index) {
    result[columns[index]] = field.trim();
    return result;
  }, {});
};
