import { TodoSectionCreateInput, TodoSectionWhereInput } from '../generated/prisma-client';
import { Context, TodoSectionInput } from '../types';

export const createUpdateTodoSection = async (input: TodoSectionInput, ctx: Context) => {
  const {
    id,
    ordre,
    libelle,
    isRemoved,
    idUser,
    idProject,
    isArchived,
    isInInbox,
    isInInboxTeam,
  } = input;
  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = idUser ? { connect: { id: idUser } } : null;
  const userModification = idUser ? { connect: { id: idUser } } : null;

  let defaultOrder: number = 0;
  if (id) {
    defaultOrder = await ctx.prisma.todoSection({ id }).ordre();
  } else {
    let where: TodoSectionWhereInput = { user: { id: idUser } };
    if (idProject) {
      where = { ...where, project: { id: idProject } };
    }
    defaultOrder = await ctx.prisma
      .todoSectionsConnection({ where })
      .aggregate()
      .count();
  }

  // If order and order already belongs to another todoSection,
  // Update the order of the other todoSections
  if (ordre) {
    const todoSections = await ctx.prisma.todoSections({
      where: { user: { id: ctx.userId }, ordre_gte: ordre },
    });
    if (todoSections && todoSections.length > 0) {
      await Promise.all(
        todoSections.map(async sect => {
          await ctx.prisma.updateTodoEtiquette({
            where: { id: sect.id },
            data: { ordre: sect.ordre + 1 },
          });
          await ctx.indexing.publishSave('todoSections', sect.id);
        }),
      );
    }
  }

  const data: TodoSectionCreateInput = {
    libelle,
    isRemoved,
    isArchived,
    isInInbox,
    isInInboxTeam,
    ordre: ordre ? ordre : id ? defaultOrder : defaultOrder + 1,
    user: idUser ? { connect: { id: idUser } } : null,
    project: idProject ? { connect: { id: idProject } } : null,
    userCreation,
    userModification,
  };
  const todoSection = await ctx.prisma.upsertTodoSection({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  await ctx.indexing.publishSave('todosections', todoSection.id);
  return todoSection;
};

export const softDeleteTodoSection = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateTodoSection({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      await ctx.indexing.publishSave('todosections', res.id);
      return res;
    });
};
