import { IdeeOuBonnePratiqueClassificationCreateInput } from '../generated/prisma-client';
import { Context, IdeeOuBonnePratiqueClassificationInput } from '../types';

export const createUpdateIdeeOuBonnePratiqueClassification = async (
  ctx: Context,
  input: IdeeOuBonnePratiqueClassificationInput,
) => {
  const { id, isRemoved, nom, idParent } = input;

  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;

  const data: IdeeOuBonnePratiqueClassificationCreateInput = {
    nom,
    isRemoved: isRemoved || false,
    userCreation,
    userModification,
    parent: idParent ? { connect: { id: idParent } } : null,
  };

  const result = await ctx.prisma.upsertIdeeOuBonnePratiqueClassification({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  return result;
};

export const softDeleteIdeeOuBonnePratiqueClassification = async (ctx: Context, id: string) => {
  const result = await ctx.prisma.updateIdeeOuBonnePratiqueClassification({
    where: { id },
    data: { isRemoved: true },
  });
  return result;
};
