import { ApolloError } from 'apollo-server';
import { head } from 'lodash';
import {
  Couleur,
  TodoEtiquetteCreateInput,
  TodoEtiquetteFavorisCreateInput,
} from '../generated/prisma-client';
import { Context, TodoEtiquetteInput } from '../types';

export const createUpdateTodoEtiquette = async (input: TodoEtiquetteInput, ctx: Context) => {
  const { id, codeMaj, isRemoved, ordre, nom, idCouleur, isFavoris } = input;

  console.log('createUpdateTodoEtiquette -> ordre', ordre);

  // TODO : Notion fusion etiquette

  if (!ctx.userId) {
    return new ApolloError('User ID is required', 'USER_ID_MISSING');
  }

  let defaultColor: Couleur;
  if (!idCouleur) {
    defaultColor = await ctx.prisma.couleur({ code: '2167842' });
  }

  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  console.log('createUpdateTodoEtiquette -> ctx.userId', ctx.userId);

  let defaultOrder: number = 0;
  if (id) {
    defaultOrder = await ctx.prisma.todoEtiquette({ id }).ordre();
  } else {
    defaultOrder = await ctx.prisma
      .todoEtiquettesConnection({ where: { pharmacie: { id: ctx.pharmacieId } } })
      //.todoEtiquettesConnection({ where: { userCreation: { id: ctx.userId } } })
      .aggregate()
      .count();
  }

  // If order and order already belongs to another todoEtiquette,
  // Update the order of the other todoEtiquettes
  if (ordre) {
    const todoEtiquettes = await ctx.prisma.todoEtiquettes({
      where: { pharmacie: { id: ctx.pharmacieId }, ordre_gte: ordre },
     // where: { userCreation: { id: ctx.userId }, ordre_gte: ordre },
    });
    if (todoEtiquettes && todoEtiquettes.length > 0) {
      await Promise.all(
        todoEtiquettes.map(async eti => {
          await ctx.prisma.updateTodoEtiquette({
            where: { id: eti.id },
            data: { ordre: eti.ordre + 1 },
          });
          await ctx.indexing.publishSave('todoetiquettes', eti.id);
        }),
      );
    }
  }

  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const pharmacie = ctx.pharmacieId ? {connect:{id: ctx.pharmacieId}}: null;
  const data: TodoEtiquetteCreateInput = {
    nom,
    codeMaj,
    isRemoved,
    userCreation,
    userModification,
    pharmacie,
    ordre: ordre ? ordre : id ? defaultOrder : defaultOrder + 1,
    couleur: idCouleur
      ? { connect: { id: idCouleur } }
      : defaultColor
      ? { connect: { id: defaultColor.id } }
      : null,
  };
  const todoEtiquette = await ctx.prisma.upsertTodoEtiquette({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  // Save favoris
  if (todoEtiquette) {
    console.log('todoEtiquette :>> ', todoEtiquette);

    let todoEtiquetteFavorisOrder: number = 0;
    todoEtiquetteFavorisOrder = await ctx.prisma
      .todoEtiquetteFavorisesConnection({
        where: { user: { id: ctx.userId } },
      })
      .aggregate()
      .count();

    const todoEtiquetteFavorisData: TodoEtiquetteFavorisCreateInput = {
      etiquette: { connect: { id: todoEtiquette.id } },
      ordre: todoEtiquetteFavorisOrder + 1,
      user: ctx.userId ? { connect: { id: ctx.userId } } : null,
      userCreation: ctx.userId ? { connect: { id: ctx.userId } } : null,
      userModification: ctx.userId ? { connect: { id: ctx.userId } } : null,
    };

    // Create
    if (!id && isFavoris) {
      const todoEtiquetteFavoris = await ctx.prisma.createTodoEtiquetteFavoris(
        todoEtiquetteFavorisData,
      );
      await ctx.indexing.publishSave('todoetiquettefavorises', todoEtiquetteFavoris.id);
    }

    // Edit
    if (id) {
      const existTodoEtiquetteFavorises = await ctx.prisma.todoEtiquetteFavorises({
        where: { user: { id: ctx.userId }, etiquette: { id: todoEtiquette.id } },
      });

      const existTodoEtiquetteFavoris = head(existTodoEtiquetteFavorises);
      console.log(
        'createUpdateTodoEtiquette -> existTodoEtiquetteFavoris',
        existTodoEtiquetteFavoris,
      );
      console.log('isFavoris :>> ', isFavoris);
      // If exist (existTodoEtiquetteFavoris) and isFavoris === true --> DO NOT DO ANYTHING
      // If exist (existTodoEtiquetteFavoris) and isFavoris === false --> DO DELETE
      if (existTodoEtiquetteFavoris) {
        if (isFavoris === false) {
          const deleted = await ctx.prisma.updateTodoEtiquetteFavoris({
            where: { id: existTodoEtiquetteFavoris.id },
            data: { isRemoved: true },
          });
          await ctx.indexing.publishDelete('todoetiquettefavorises', deleted.id);
        }
      } else {
        // If is not exist, create
        const todoEtiquetteFavoris = await ctx.prisma.createTodoEtiquetteFavoris(
          todoEtiquetteFavorisData,
        );
        await ctx.indexing.publishSave('todoetiquettefavorises', todoEtiquetteFavoris.id);
      }
    }
  }

  // Publish to elasticsearch
  await ctx.indexing.publishSave('todoetiquettes', todoEtiquette.id);
  return todoEtiquette;
};

export const softDeleteTodoEtiquette = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateTodoEtiquette({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      await ctx.indexing.publishSave('todoetiquettes', res.id);
      return res;
    });
};
