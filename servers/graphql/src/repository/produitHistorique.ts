import { Context } from '../types';
import moment from 'moment';

export const produitHistorique = async (
  ctx: Context,
  { idProduit, idPharmacie, year }: { idProduit: string; idPharmacie: string; year: number },
) => {
  const hitoriques = await ctx.prisma.produitHistoriques({
    where: {
      produit: {
        id: idProduit,
      },
      pharmacie: {
        id: idPharmacie,
      },
    },
  });

  if (!hitoriques || (hitoriques && !hitoriques.length)) return null;

  const historique = hitoriques[0];

  const currentMonth = historique.moisM00;

  let allMonths = [];
  let column = currentMonth - 1;
  let lastColumn = currentMonth - 1;
  if (currentMonth === 1) allMonths = [{ index: 1, date: `${year}-1-1`, column: '00' }];
  else {
    for (let index = 1; index <= currentMonth; index++) {
      allMonths.push({
        index,
        date: `${year}-${index}-1`,
        column: column > 10 ? column : `0${column}`,
      });
      column--;
    }
  }

  for (let index = 12; index >= 1; index--) {
    if (lastColumn <= 18)
      allMonths.push({
        index,
        date: `${year - 1}-${index}-1`,
        column: lastColumn >= 10 ? lastColumn : `0${lastColumn}`,
      });
    lastColumn++;
  }

  console.log('allMonths', allMonths, lastColumn);

  return {
    year,
    dateHistorique: historique.dateModification,
    data:
      allMonths && allMonths.length
        ? allMonths
            .map(month => {
              console.log(
                'historique[`venteM${month.column}`]0',
                month.column,
                historique[`venteM${month.column}`],
              );

              return {
                order: month.index,
                date: month.date,
                value: +historique[`venteM${month.column}`],
              };
            })
            .sort((a, b) => a.date.localeCompare(b.date))
        : [],
  };
};
