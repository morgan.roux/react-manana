import { IdeeOuBonnePratiqueFichierJointCreateInput } from '../generated/prisma-client';
import { Context, IdeeOuBonnePratiqueFichierJointInput } from '../types';

export const createUpdateIdeeOuBonnePratiqueFichierJoint = async (
  ctx: Context,
  input: IdeeOuBonnePratiqueFichierJointInput,
) => {
  const { id, isRemoved, idFichier, idIdeeOuBonnePratique } = input;
  const data: IdeeOuBonnePratiqueFichierJointCreateInput = {
    fichier: idFichier ? { connect: { id: idFichier } } : null,
    ideeOuBonnePratique: idIdeeOuBonnePratique ? { connect: { id: idIdeeOuBonnePratique } } : null,
    isRemoved: isRemoved || false,
  };

  const result = await ctx.prisma.upsertIdeeOuBonnePratiqueFichierJoint({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  return result;
};

export const softDeleteIdeeOuBonnePratiqueFichierJoint = async (ctx: Context, id: string) => {
  const result = await ctx.prisma.updateIdeeOuBonnePratiqueFichierJoint({
    where: { id },
    data: { isRemoved: true },
  });
  return result;
};
