import { Fichier, FichierCreateInput, RessourceCreateInput } from '../generated/prisma-client';
import { Context, RessourceInput } from '../types';

export const createUpdateRessource = async (args: RessourceInput, ctx: Context) => {
  const {
    id,
    code,
    dateChargement,
    fichier,
    libelle,
    url,
    idItem,
    typeRessource,
    idPartenaireService,
  } = args;

  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;

  // Create or update fichier
  let savedFile: Fichier;
  if (fichier) {
    const fichierData: FichierCreateInput = {
      chemin: fichier.chemin,
      nomOriginal: fichier.nomOriginal,
      type: fichier.type,
    };
    savedFile = await ctx.prisma.upsertFichier({
      where: { id: fichier.id || '' },
      create: fichierData,
      update: fichierData,
    });
  }

  const ressourceData: RessourceCreateInput = {
    code,
    dateChargement,
    libelle,
    url,
    typeRessource,
    item: idItem ? { connect: { id: idItem } } : null,
    fichier: savedFile ? { connect: { id: savedFile.id } } : null,
    partenaireService: idPartenaireService ? { connect: { id: idPartenaireService } } : null,
  };

  const ressource = await ctx.prisma.upsertRessource({
    where: { id: id || '' },
    create: ressourceData,
    update: ressourceData,
  });

  await ctx.indexing.publishSave('ressources', ressource.id);
  return ressource;
};

export const softDeleteRessource = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateRessource({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async r => {
      await ctx.indexing.publishSave('ressources', r.id);
      return r;
    });
};
