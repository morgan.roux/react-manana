import { Context } from '../types';

export const gammeCatalogue = async (ctx: Context, { codeSousGammeCategorie }) => {
  return ctx.prisma.sousGammeCatalogue({ codeSousGamme: codeSousGammeCategorie }).gammeCatalogue();
};
