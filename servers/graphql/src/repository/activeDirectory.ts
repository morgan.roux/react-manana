import { Context } from '../types';

export const ActiveDirectory = async (ctx: Context, { id }) => {
  try {
    const activeDirectory = await ctx.prisma.activeDirectoryCredential({ id });
    return activeDirectory;
  } catch (error) {
    throw new Error(error);
  }
};

export const ActivedirectoryGroupement = async (ctx: Context, { idgroupement }) => {
  try {
    const activeDirectory = await ctx.prisma.activeDirectoryCredentials({
      where: { groupement: { id: idgroupement } },
    });
    return activeDirectory[0];
  } catch (error) {
    throw new Error(error);
  }
};

export const CreateActiveDirectoryCredential = async (
  ctx: Context,
  { idGroupement, url, user, password, base, ldapType },
) => {
  try {
    return ctx.prisma.createActiveDirectoryCredential({
      url,
      user,
      password,
      base,
      ldapType,
      groupement: {
        connect: { id: idGroupement },
      },
    });
  } catch (error) {
    throw new Error(error);
  }
};

export const UpdateActiveDirectoryCredential = async (
  ctx: Context,
  { id, url, user, password, base, ldapType },
) => {
  try {
    return ctx.prisma.updateActiveDirectoryCredential({
      data: {
        url,
        user,
        password,
        base,
        ldapType,
      },
      where: { id },
    });
  } catch (error) {
    throw new Error(error);
  }
};

export const DeleteActiveDirectory = async (ctx: Context, { id }) => {
  try {
    const activeDirectory = await ctx.prisma.deleteActiveDirectoryCredential({ id });
    return activeDirectory;
  } catch (error) {
    throw new Error(error);
  }
};
