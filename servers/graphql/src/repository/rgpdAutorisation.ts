import { RgpdAutorisationCreateInput } from '../generated/prisma-client';
import { Context, RgpdAutorisationInput } from '../types';

export const createUpdateRgpdAutorisation = async (ctx: Context, input: RgpdAutorisationInput) => {
  const { id, title, description, order } = input;

  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;

  let defaultOrder: number = 0;
  if (id) {
    defaultOrder = await ctx.prisma.rgpdAutorisation({ id }).order();
  } else {
    defaultOrder = await ctx.prisma
      .rgpdAutorisationsConnection({ where: { groupement: { id: ctx.groupementId } } })
      .aggregate()
      .count();
  }

  const data: RgpdAutorisationCreateInput = {
    title,
    description,
    order: order ? order : id ? defaultOrder : defaultOrder + 1,
    groupement,
    userCreation,
    userModification,
  };

  const res = await ctx.prisma.upsertRgpdAutorisation({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  return res;
};
