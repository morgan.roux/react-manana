import { Context } from '../types';
import { ApolloError } from 'apollo-server';
import { MutationResolvers } from '../generated/graphqlgen';
import { PersonnelCreateInput, PersonnelUpdateInput } from '../generated/prisma-client';

export const getUserPersonnel = async (ctx: Context, id: string) => {
  return ctx.prisma
    .personnel({
      id,
    })
    .user();
};

export const createUpdatePersonnel = async (
  ctx: Context,
  {
    id,
    codeService,
    respHierarch,
    sortie,
    civilite,
    nom,
    prenom,
    sexe,
    contact,
    commentaire,
    dateSortie,
    idGroupement,
  }: MutationResolvers.ArgsCreateUpdatePersonnel,
) => {
  const dataCreate: PersonnelCreateInput = {
    service: { connect: { code: codeService } },
    respHierarch: respHierarch ? { connect: { id: respHierarch } } : null,
    sortie,
    dateSortie,
    civilite,
    nom,
    prenom,
    sexe,
    commentaire,
    groupement: idGroupement
      ? { connect: { id: idGroupement } }
      : { connect: { id: ctx.groupementId } },
    contact: {
      create: contact,
    },
  };

  const dataUpdate: PersonnelUpdateInput = {
    service: { connect: { code: codeService } },
    respHierarch: respHierarch ? { connect: { id: respHierarch } } : null,
    sortie,
    dateSortie,
    civilite,
    nom,
    prenom,
    sexe,
    commentaire,
    contact: {
      upsert: {
        create: contact,
        update: contact,
      },
    },
  };

  try {
    const personnel = await ctx.prisma.upsertPersonnel({
      where: {
        id: id || '',
      },
      create: dataCreate,
      update: dataUpdate,
    });

    await ctx.indexing.publishSave('personnels', personnel.id);
    return personnel;
  } catch (error) {
    const code = id ? 'ERROR_EDIT_PERSONNEL' : 'ERROR_CREATION_PERSONNEL';
    const message = id ? 'modification' : 'création';
    throw new ApolloError(`Erreur lors de la ${message} de la personnel de groupement`, code);
  }
};

export const updateSortiePersonnel = async (ctx: Context, { id }) => {
  return ctx.prisma
    .updatePersonnel({
      where: { id },
      data: {
        sortie: 1,
      },
    })
    .then(async personnel => {
      const user = await ctx.prisma.personnel({ id: personnel.id }).user();
      if (user) await ctx.indexing.publishSave('users', user.id);
      await ctx.indexing.publishSave('personnels', personnel.id);
      return personnel;
    });
};
