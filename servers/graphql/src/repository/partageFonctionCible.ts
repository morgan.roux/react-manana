import { PartageFonctionCibleCreateInput } from '../generated/prisma-client';
import { Context, PartageFonctionCibleInput } from '../types';

export const createUpdatePartageFonctionCible = async (
  input: PartageFonctionCibleInput,
  ctx: Context,
) => {
  const { id, code, libelle, codeMaj, isRemoved } = input;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: PartageFonctionCibleCreateInput = {
    code,
    libelle,
    codeMaj,
    isRemoved,
    userCreation,
    userModification,
  };
  const partageFonctionCible = await ctx.prisma.upsertPartageFonctionCible({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  return partageFonctionCible;
};

export const softDeletePartageFonctionCible = async (ctx: Context, id: string) => {
  return ctx.prisma
    .updatePartageFonctionCible({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      return res;
    });
};
