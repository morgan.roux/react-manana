import { RgpdPartenaireCreateInput } from '../generated/prisma-client';
import { Context, RgpdPartenaireInput } from '../types';

export const createUpdateRgpdPartenaire = async (ctx: Context, input: RgpdPartenaireInput) => {
  const { id, title, description, order } = input;

  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;

  let defaultOrder: number = 0;
  if (id) {
    defaultOrder = await ctx.prisma.rgpdPartenaire({ id }).order();
    console.log('defaultOrder 1111 :>> ', defaultOrder);
  } else {
    defaultOrder = await ctx.prisma
      .rgpdPartenairesConnection({ where: { groupement: { id: ctx.groupementId } } })
      .aggregate()
      .count();
    console.log('defaultOrder 2222 :>> ', defaultOrder);
  }

  const data: RgpdPartenaireCreateInput = {
    title,
    description,
    order: order ? order : id ? defaultOrder : defaultOrder + 1,
    groupement,
    userCreation,
    userModification,
  };

  const res = await ctx.prisma.upsertRgpdPartenaire({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  return res;
};
