import { Context, FichierInput, ProduitCanalInput } from '../types';
import logger from '../logging';
import { ApolloError } from 'apollo-server';
import { Produit } from '../generated/prisma-client';
import { ProduitVariableInput, ProduitTechRegInput, ProduitFormInput } from '../interfaces/produit';

export const createUpdateProduit = async (
  ctx: Context,
  {
    idProduit,
    libelle,
    dateSuppression,
    isGenerique,
    fichierImage,
    typeCodeReferent,
    codeReference,
    codeFamille,
    idCategorie,
    codeTva,
    codeActe,
    pxAchat,
    pxVente,
    codeListe,
    codeTauxss,
    codeLibelleStockage,
    codeLibelleStatut,
    codeLibelleCasher,
    codeLibelleConditionnement,
    idLaboTitulaire,
    idLaboExploitant,
    idLaboDistributaire,
    produitCanaux,
  }: ProduitFormInput,
) => {
  let produit: Produit = null;
  try {
    produit = await upsertProduit(ctx, {
      id: idProduit,
      libelle,
      isReferentGenerique: isGenerique,
      supprimer: dateSuppression,
      categorie: idCategorie,
    });
  } catch (error) {
    logger.error(`[ERROR CREATE ARTICLE] : ${JSON.stringify(error)}`);
    return idProduit
      ? new ApolloError('Erreur lors de la modification du produit', 'ERROR_UPDATE_ARTICLE')
      : new ApolloError('Erreur lors de la création du produit', 'ERROR_CREATE_ARTICLE');
  }

  if (!produit) {
    new ApolloError(
      `Erreur lors de la création du produit. Le Produit n'est pas enregistré `,
      'ERROR_CREATE_ARTICLE',
    );
  }

  try {
    await createUpdateArticlePhoto(ctx, fichierImage, { id: produit.id });
  } catch (error) {
    logger.error(`[ERROR CREATE PHOTO PRODUIT] : ${JSON.stringify(error)}`);
    if (!idProduit) {
      await _resetArticles(ctx, { id: produit.id });
    }
    return new ApolloError(
      `Erreur lors de l'enregistrement du photo du produit`,
      'ERROR_CREATE_PHOTO_ARTICLE',
    );
  }

  try {
    if (typeCodeReferent && codeReference) {
      await createProduitCode(ctx, {
        typeCodeReferent,
        codeReference,
        idProduit: produit.id,
      });
    }
  } catch (error) {
    logger.error(`[ERROR CREATE PRODUIT CODE] : ${JSON.stringify(error)}`);
    // Supprimer l'article s'il y a erreur de la création de l'histo
    if (!idProduit) {
      await _resetArticles(ctx, { id: produit.id });
    }
    return new ApolloError(
      `Erreur lors de l'enregistrement du photo de l'article`,
      'ERROR_CREATE_HISTO_ARTICLE',
    );
  }

  try {
    if (codeFamille) {
      await createUpdateProduitFamille(ctx, { codeFamille, idProduit: produit.id });
    }
  } catch (error) {
    logger.error(`[ERROR CREATE FAMILLE PRODUIT] : ${JSON.stringify(error)}`);
    if (!idProduit) {
      await _resetArticles(ctx, { id: produit.id });
    }
    return new ApolloError(
      `Erreur lors de l'enregistrement du photo de l'article`,
      'ERROR_CREATE_FAMILLE_ARTICLE',
    );
  }

  try {
    // CREATE TECH REG
    await createUpdateProduitTechReg(ctx, {
      id: produit.id,
      codeTva,
      codeActe,
      pxAchat,
      pxVente,
      codeListe,
      codeTauxss,
      codeLibelleStockage,
      codeLibelleStatut,
      codeLibelleCasher,
      codeLibelleConditionnement,
      idLaboTitulaire,
      idLaboExploitant,
      idLaboDistributaire,
    });
  } catch (error) {
    logger.error(`[ERROR CREATE FAMILLE PRODUIT] : ${JSON.stringify(error)}`);
    if (!idProduit) {
      await _resetArticles(ctx, { id: produit.id });
    }
    return new ApolloError(
      `Erreur lors de l'enregistrement du photo de l'article`,
      'ERROR_CREATE_FAMILLE_ARTICLE',
    );
  }

  try {
    if (produitCanaux && produitCanaux.length) {
      await Promise.all(
        produitCanaux.map(async (produitCanal: any) =>
          createUpdateProduitCanal(ctx, produit.id, produitCanal),
        ),
      );
    }
  } catch (error) {
    logger.error(`[CREATE ERROR PRODUIT CANAL] : ${JSON.stringify(error)}`);
    if (!idProduit) {
      await _resetArticles(ctx, { id: produit.id });
    }
    return idProduit
      ? new ApolloError(
          `Erreur lors de l'enregistrement des canaux du produit`,
          'ERROR_CANAL_UPDATE_ARTICLE',
        )
      : new ApolloError(
          'Erreur lors de la modification des canaux du produit',
          'ERROR_CANAL_CREATE_ARTICLE',
        );
  }

  await ctx.indexing.publishSave('produits', produit.id);
  return ctx.prisma.produit({ id: produit.id });
};

const upsertProduit = async (
  ctx: Context,
  { id, libelle, isReferentGenerique, supprimer, categorie }: ProduitVariableInput,
) => {
  const data = {
    libelle,
    isReferentGenerique,
    supprimer,
    categorie,
    typeCategorie: categorie && [1, 2].includes(categorie) ? 1 : categorie ? 2 : null,
  };

  return ctx.prisma.upsertProduit({
    where: { id: id || '' },
    create: data,
    update: data,
  });
};

export const createUpdateArticlePhoto = async (
  ctx: Context,
  fichierImage: FichierInput,
  { id }: { id: string },
) => {
  const produitPhotos = await ctx.prisma.produitPhotos({
    where: { produit: { id } },
  });

  const idProduitPhoto = produitPhotos && produitPhotos.length ? produitPhotos[0].id : '';

  if (!fichierImage || (fichierImage && !fichierImage.chemin)) {
    if (idProduitPhoto) await ctx.prisma.deleteProduitPhoto({ id: idProduitPhoto });
    return null;
  }

  return ctx.prisma.upsertProduitPhoto({
    where: { id: idProduitPhoto },
    create: {
      produit: {
        connect: {
          id,
        },
      },
      idFichier: {
        create: {
          chemin: fichierImage.chemin,
          nomOriginal: fichierImage.nomOriginal,
          type: fichierImage.type,
        },
      },
    },
    update: {
      idFichier: {
        update: {
          chemin: fichierImage.chemin,
          nomOriginal: fichierImage.nomOriginal,
          type: fichierImage.type,
        },
      },
    },
  });
};

const createProduitCode = async (
  ctx: Context,
  {
    typeCodeReferent,
    codeReference,
    idProduit,
  }: { typeCodeReferent: number; codeReference: string; idProduit: string },
) => {
  logger.info(
    ` [ CREATE PRODUIT CODE ] : PARAMS ${JSON.stringify({
      typeCodeReferent,
      codeReference,
      idProduit,
    })}`,
  );

  await ctx.prisma.updateManyProduitCodes({
    where: {
      produit: {
        id: idProduit,
      },
    },
    data: {
      referent: 0,
    },
  });

  const existCode = await ctx.prisma.$exists.produitCode({
    code: codeReference,
    typeCode: typeCodeReferent,
    produit: { id: idProduit },
  });

  if (existCode) {
    return ctx.prisma.updateManyProduitCodes({
      where: {
        code: codeReference,
        typeCode: typeCodeReferent,
        produit: { id: idProduit },
      },
      data: {
        referent: 1,
      },
    });
  }

  return ctx.prisma.createProduitCode({
    code: codeReference,
    typeCode: typeCodeReferent,
    produit: { connect: { id: idProduit } },
    referent: 1,
  });
};

const createUpdateProduitFamille = async (
  ctx: Context,
  { codeFamille, idProduit }: { codeFamille: string; idProduit: string },
) => {
  const familleProduits = await ctx.prisma.produitFamilles({
    where: { produit: { id: idProduit }, famille: { codeFamille } },
    first: 1,
  });

  return ctx.prisma.upsertProduitFamille({
    where: { id: familleProduits && familleProduits.length ? familleProduits[0].id : '' },
    create: {
      famille: {
        connect: {
          codeFamille,
        },
      },
      produit: {
        connect: {
          id: idProduit,
        },
      },
    },
    update: {
      famille: {
        connect: {
          codeFamille,
        },
      },
    },
  });
};

const createUpdateProduitCanal = async (
  ctx: Context,
  idProduit: string,
  {
    id,
    qteStock,
    qteMin,
    stv,
    unitePetitCond,
    prixFab,
    prixPhv,
    datePeremption,
    dateRetrait,
    dateCreation,
    dateModification,
    idCanalSousGamme,
    codeSousGammeCatalogue,
    codeCommandeCanal,
    isRemoved,
  }: ProduitCanalInput,
) => {
  // Ne pas enregetrer les canal_articles si l'id null et is_remove = true
  if (!id && isRemoved) return null;

  if (!id) {
    const inCanal = await ctx.prisma.$exists.produitCanal({
      canal: { code: codeCommandeCanal },
      produit: { id: idProduit },
    });
    if (inCanal) return null;
  }

  const data = {
    produit: {
      connect: {
        id: idProduit,
      },
    },
    canal: {
      connect: {
        code: codeCommandeCanal,
      },
    },
    qteStock: qteStock || 0,
    qteMin: qteMin || 0,
    stv: stv || 0,
    unitePetitCond: unitePetitCond || 0,
    prixFab: prixFab || 0,
    prixPhv: prixPhv || 0,
    dateCreation,
    dateModification,
    sousGamme: idCanalSousGamme
      ? {
          connect: {
            id: idCanalSousGamme,
          },
        }
      : null,
    sousGammeCatalogue: codeSousGammeCatalogue
      ? {
          connect: {
            codeSousGamme: codeSousGammeCatalogue,
          },
        }
      : null,
    dateRetrait: dateRetrait || null,
    datePeremption: datePeremption || null,
    isRemoved,
  };

  return ctx.prisma
    .upsertProduitCanal({
      where: { id: id ? id : '' },
      create: data,
      update: data,
    })
    .then(async produitCanal => {
      logger.info(` Result PRODUIT CANAL ====> ${JSON.stringify(produitCanal)}`);
      await ctx.indexing.publishSave('produitcanals', produitCanal.id);
      return produitCanal;
    })
    .catch(error => logger.error(`[ERROR PRODUIT CANAL] : ${JSON.stringify(error)}`));
};

const _resetArticles = async (ctx: Context, { id }: { id: string }) => {
  await ctx.prisma.deleteManyProduitCodes({ produit: { id } });
  await ctx.prisma.deleteManyProduitFamilles({ produit: { id } });

  return ctx.prisma.deleteProduit({ id });
};

export const produitFamille = async (ctx: Context, { id }: { id: number }) => {
  const familles = await ctx.prisma.produitFamilles({
    where: {
      produit: {
        id,
      },
    },
    first: 1,
  });

  return familles && familles.length
    ? ctx.prisma
        .produitFamille({
          id: familles[0].id,
        })
        .famille()
    : null;
};

export const createUpdateProduitTechReg = async (
  ctx: Context,
  {
    id,
    codeTva,
    codeActe,
    pxAchat,
    pxVente,
    codeListe,
    codeTauxss,
    codeLibelleStockage,
    codeLibelleStatut,
    codeLibelleCasher,
    codeLibelleConditionnement,
    idLaboTitulaire,
    idLaboExploitant,
    idLaboDistributaire,
  }: ProduitTechRegInput,
) => {
  const techRegs = await ctx.prisma.produitTechRegs({
    where: {
      produit: {
        id,
      },
    },
  });
  const data = {
    produit: { connect: { id } },
    tva: codeTva ? { connect: { codeTva } } : null,
    acte: codeActe ? { connect: { codeActe } } : null,
    prixAchatHT: pxAchat,
    prixVenteTTC: pxVente,
    liste: codeListe ? { connect: { codeListe } } : null,
    tauxss: codeTauxss ? { connect: { codeTaux: codeTauxss } } : null,
    libelleStockage: codeLibelleStockage ? { connect: { codeInfo: codeLibelleStockage } } : null,
    libelleStatut: codeLibelleStatut ? { connect: { codeInfo: codeLibelleStatut } } : null,
    libelleCasher: codeLibelleCasher ? { connect: { codeInfo: codeLibelleCasher } } : null,
    libelleConditionnement: codeLibelleConditionnement
      ? { connect: { codeInfo: codeLibelleConditionnement } }
      : null,
    laboDistirbuteur: idLaboDistributaire ? { connect: { id: idLaboDistributaire } } : null,
    laboExploitant: idLaboExploitant ? { connect: { id: idLaboExploitant } } : null,
    laboTitulaire: idLaboTitulaire ? { connect: { id: idLaboTitulaire } } : null,
  };
  return ctx.prisma.upsertProduitTechReg({
    where: {
      id: techRegs && techRegs.length ? techRegs[0].id : '',
    },
    create: data,
    update: data,
  });
};
