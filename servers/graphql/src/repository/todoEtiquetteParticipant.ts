import { TodoEtiquetteParticipantCreateInput } from '../generated/prisma-client';
import { Context, TodoEtiquetteParticipantInput } from '../types';

export const createUpdateTodoEtiquetteParticipant = async (
  input: TodoEtiquetteParticipantInput,
  ctx: Context,
) => {
  const { id, idEtiquette, idUserParticipant, codeMaj, isRemoved } = input;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: TodoEtiquetteParticipantCreateInput = {
    etiquette: idEtiquette ? { connect: { id: idEtiquette } } : null,
    userParticipant: idUserParticipant ? { connect: { id: idUserParticipant } } : null,
    codeMaj,
    isRemoved,
    userCreation,
    userModification,
  };
  const todoParticipant = await ctx.prisma.upsertTodoEtiquetteParticipant({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  return todoParticipant;
};

export const softDeleteTodoEtiquetteParticipant = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateTodoEtiquetteParticipant({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      return res;
    });
};
