import { RgpdHistoriqueCreateInput } from '../generated/prisma-client';
import { Context, RgpdHistoriqueInfoPlusInput, RgpdHistoriqueInput } from '../types';
import { createUpdateRgpdHistoriqueInfoPlus } from './rgpdHistoriqueInfoPlus';

export const createUpdateRgpdHistorique = async (ctx: Context, input: RgpdHistoriqueInput) => {
  const { id, accept_all, refuse_all, idUser, historiquesInfosPlus } = input;

  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;

  const data: RgpdHistoriqueCreateInput = {
    accept_all,
    refuse_all,
    user: idUser ? { connect: { id: idUser } } : null,
    groupement,
  };

  const res = await ctx.prisma.upsertRgpdHistorique({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  if (historiquesInfosPlus && historiquesInfosPlus.length > 0 && res) {
    await Promise.all(
      historiquesInfosPlus.map(async item => {
        const infoPlusInput: RgpdHistoriqueInfoPlusInput = {
          id: item.id,
          accepted: item.accepted,
          idHistorique: res.id,
          idItemAssocie: item.idItemAssocie,
          type: item.type,
        };
        await createUpdateRgpdHistoriqueInfoPlus(ctx, infoPlusInput);
      }),
    );
  }

  return res;
};
