import { difference, last } from 'lodash';
import moment from 'moment';
import {
  FichierCreateInput,
  InformationLiaison,
  InformationLiaisonCreateInput,
} from '../generated/prisma-client';
import { updateItemScoringForItem } from '../sequelize/shared';
import { computeDateEcheanceFromUrgence } from '../sequelize/shared/compute-date-echeance';
import {
  Context,
  FichierInput,
  InformationLiaisonFichierJointInput,
  InformationLiaisonInput,
  TakeChargeInformationLiaisonInput,
} from '../types';
import { createUpdateInformationLiaisonFichierJoint } from './informationLiaisonFichierJoint';
import { getUserPharmacie } from './user';

export const createUpdateInformationLiaison = async (
  ctx: Context,
  input: InformationLiaisonInput,
) => {
  const {
    id,
    titre,
    description,
    idColleguesConcernees,
    idDeclarant,
    idOrigine,
    idOrigineAssocie,
    isRemoved,
    statut,
    codeMaj,
    idFicheAmelioration,
    idFicheIncident,
    idFicheReclamation,
    idTodoAction,
    bloquant,
    fichiers,
    idUrgence,
    idImportance,
    idFonction,
    idTache,
    idType,
    idItem,
    idItemAssocie,
  } = input;

  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const urgence = await ctx.prisma.urgence({ id: idUrgence });
  const dateEcheance = computeDateEcheanceFromUrgence(urgence.code as any);

  const pharmacie = await getUserPharmacie(ctx.userId, ctx);

  const infoData: InformationLiaisonCreateInput = {
    bloquant,
    titre,
    description,
    idOrigine,
    idOrigineAssocie,
    statut: statut || 'EN_COURS',
    idType,
    idItem,
    idItemAssocie,
    codeMaj,
    isRemoved: isRemoved || false,
    idFicheAmelioration,
    idFicheIncident,
    todoAction: idTodoAction ? { connect: { id: idTodoAction } } : null,
    ficheReclamation: idFicheReclamation ? { connect: { id: idFicheReclamation } } : null,
    declarant: idDeclarant ? { connect: { id: idDeclarant } } : null,
    groupement: ctx.groupementId ? { connect: { id: ctx.groupementId } } : null,
    pharmacie: { connect: { id: pharmacie?.id || ctx.pharmacieId } },
    userCreation,
    userModification,
    urgence: { connect: { id: idUrgence } },
    importance: { connect: { id: idImportance } },
    idFonction,
    idTache,
    dateEcheance: moment(dateEcheance).format(),
  };

  console.log(
    '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ,',
    infoData,
  );

  const info = await ctx.prisma.upsertInformationLiaison({
    where: { id: id || '' },
    create: infoData,
    update: infoData,
  });

  // Save fichiers joints
  // If on edit, check existants, delete if exist
  if (id) {
    if (info) {
      const fichierJointExistants = await ctx.prisma.informationLiaisonFichierJoints({
        where: { informationLiaison: { id_in: info.id }, isRemoved: false },
      });
      if (fichierJointExistants.length > 0) {
        await Promise.all(
          fichierJointExistants.map(async fichierJoint => {
            if (fichierJoint) {
              const fichier = await ctx.prisma
                .informationLiaisonFichierJoint({ id: fichierJoint.id })
                .fichier();
              await ctx.prisma.deleteInformationLiaisonFichierJoint({ id: fichierJoint.id });
              if (fichier) await ctx.prisma.deleteFichier({ id: fichier.id });
            }
          }),
        );
      }
    }
  }
  // Save fichier joint
  await _saveFichierJoint(ctx, info, fichiers);

  if (idColleguesConcernees.length > 0 && info) {
    // EDIT
    if (id) {
      const existants = await ctx.prisma.informationLiaisonUserConcernees({
        where: {
          userConcernee: { id_in: idColleguesConcernees },
          isRemoved: false,
          informationLiaison: { id: info.id },
        },
      });
      const existantConcernedIds = await Promise.all(
        existants.map(existant => {
          return ctx.prisma
            .informationLiaisonUserConcernee({ id: existant.id })
            .userConcernee()
            .id();
        }),
      );
      const newsConcernedIds = difference(idColleguesConcernees, existantConcernedIds);
      const removedsConcernedIds = difference(existantConcernedIds, idColleguesConcernees);
      // Save on InformationLiaisonUserConcernees
      if (newsConcernedIds.length > 0) {
        await Promise.all(
          newsConcernedIds.map(async newConcernedId => {
            const res = await ctx.prisma.createInformationLiaisonUserConcernee({
              informationLiaison: { connect: { id: info.id } },
              userConcernee: { connect: { id: newConcernedId } },
              isRemoved: false,
              statut: 'NON_LUES',
              userCreation,
              userModification,
            });
            await ctx.indexing.publishSave('updateinformationliaisonuserconcernees', res.id);
          }),
        );
      }

      // Remove on InformationLiaisonUserConcernees
      if (removedsConcernedIds.length > 0) {
        await Promise.all(
          removedsConcernedIds.map(async removeConcernedId => {
            const res = await ctx.prisma.deleteInformationLiaison({ id: removeConcernedId });
            await ctx.indexing.publishDelete('updateinformationliaisonuserconcernees', res.id);
          }),
        );
      }

      // CREATE
    } else {
      // Save on InformationLiaisonUserConcernees
      if (idColleguesConcernees.length > 0) {
        await Promise.all(
          idColleguesConcernees.map(async idCollegueConcernee => {
            const res = await ctx.prisma.createInformationLiaisonUserConcernee({
              informationLiaison: { connect: { id: info.id } },
              userConcernee: { connect: { id: idCollegueConcernee } },
              isRemoved: false,
              statut: 'NON_LUES',
              userCreation,
              userModification,
            });
            await ctx.indexing.publishSave('updateinformationliaisonuserconcernees', res.id);
          }),
        );
      }
    }
  }
  await ctx.indexing.publishSave('informationliaisons', info.id);
  await updateItemScoringForItem('Q', info.id);
  return info;
};

export const softDeleteInformationLiaison = async (ctx: Context, id: string) => {
  return ctx.prisma
    .updateInformationLiaison({ where: { id }, data: { isRemoved: true } })
    .then(async res => {
      await ctx.indexing.publishSave('informationliaisons', res.id);
      await updateItemScoringForItem('Q', res.id);
      return res;
    });
};

export const takeChargeInformationLiaison = async (
  ctx: Context,
  input: TakeChargeInformationLiaisonInput,
) => {
  const { id, date, description, files } = input;
  let info = await ctx.prisma.informationLiaison({ id });
  const infoUserCreation = await ctx.prisma.informationLiaison({ id }).userCreation();
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  if (info) {
    const infoPrisEnCharge = await ctx.prisma.createInformationLiaisonPrisCharge({
      date,
      description,
      informationLiaison: { connect: { id: info.id } },
      userCreation,
      userModification,
    });

    // Save fichiers joints
    if (files && files.length > 0 && infoPrisEnCharge) {
      await Promise.all(
        files.map(async file => {
          const fichier = await ctx.prisma.createFichier({
            chemin: file.chemin,
            nomOriginal: file.nomOriginal,
            type: file.type,
            userCreation,
            userModification,
          });
          if (fichier) {
            await ctx.prisma.createInformationLiaisonPrisChargeFichierJoint({
              fichier: { connect: { id: fichier.id } },
              informationLiaisonPrisCharge: { connect: { id: infoPrisEnCharge.id } },
              userCreation,
              userModification,
            });
          }
        }),
      );
    }

    // For user creation
    if (infoUserCreation && infoUserCreation.id === ctx.userId) {
      info = await ctx.prisma.updateInformationLiaison({
        where: { id: info.id },
        data: { statut: 'CLOTURE' },
      });
    } else {
      // Update statut for informationLiaisonUserConcernees to EN_CHARGE
      if (info) {
        const usersConcerneds = await ctx.prisma.informationLiaisonUserConcernees({
          where: {
            informationLiaison: { id: info.id },
            userConcernee: { id: ctx.userId },
            isRemoved: false,
          },
        });
        const userConcerned = last(usersConcerneds);
        if (userConcerned) {
          const newUserConcerned = await ctx.prisma.updateInformationLiaisonUserConcernee({
            where: { id: userConcerned.id },
            data: { statut: 'CLOTURE' },
          });
          await ctx.indexing.publishSave('informationliaisonuserconcernees', newUserConcerned.id);
        }
      }
    }

    await ctx.indexing.publishSave('informationliaisons', info.id);
    await updateItemScoringForItem('Q', info.id);
  }
  return info;
};

// const _savedInformationLiaisonUserConcernee = async (
//   ctx: Context,
//   idsUsers: string[],
//   idInfo: string,
// ) => {
//   await Promise.all(
//     idsUsers.map(async item => {
//       await ctx.prisma.createInformationLiaisonUserConcernee({
//         informationLiaison: { connect: { id: idInfo } },
//         userConcernee: { connect: { id: item } },
//         userCreation: ctx.userId ? { connect: { id: ctx.userId } } : null,
//         userModification: ctx.userId ? { connect: { id: ctx.userId } } : null,
//       });
//     }),
//   );
// };

const _saveFichierJoint = async (
  ctx: Context,
  info: InformationLiaison,
  fichiers: FichierInput[],
) => {
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;

  if (info) {
    // Save fichier joint
    if (fichiers && fichiers.length > 0) {
      await Promise.all(
        fichiers.map(async f => {
          // Save fichier
          const fichierData: FichierCreateInput = {
            chemin: f.chemin,
            nomOriginal: f.nomOriginal,
            type: f.type,
            userCreation,
            userModification,
          };
          const fichier = await ctx.prisma.upsertFichier({
            where: { id: f.id || '' },
            create: fichierData,
            update: fichierData,
          });
          if (fichier) {
            // Fichier joint input
            const fichierJointInput: InformationLiaisonFichierJointInput = {
              idFichier: fichier.id,
              idInformationLiaison: info.id,
            };
            await createUpdateInformationLiaisonFichierJoint(ctx, fichierJointInput);
            await ctx.indexing.publishSave('informationliaisons', info.id);
          }
        }),
      );
    }
  }
};
