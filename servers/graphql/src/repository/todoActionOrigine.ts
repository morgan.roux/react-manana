import { TodoActionOrigineCreateInput } from '../generated/prisma-client';
import { Context, TodoActionOrigineInput } from '../types';

export const createUpdateTodoActionOrigine = async (
  input: TodoActionOrigineInput,
  ctx: Context,
) => {
  const { id, codeMaj, isRemoved, libelle, code } = input;
  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: TodoActionOrigineCreateInput = {
    codeMaj,
    isRemoved,
    libelle,
    code,
    userCreation,
    userModification,
  };
  const todoActionOrigine = await ctx.prisma.upsertTodoActionOrigine({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  await ctx.indexing.publishSave('todoactionorigines', todoActionOrigine.id);
  return todoActionOrigine;
};

export const softDeleteTodoActionOrigine = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateTodoActionOrigine({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      await ctx.indexing.publishSave('todoactionorigines', res.id);
      return res;
    });
};
