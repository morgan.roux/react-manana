import { TodoActionEtiquetteCreateInput } from '../generated/prisma-client';
import { Context, TodoActionEtiquetteInput } from '../types';

export const createUpdateTodoActionEtiquette = async (
  input: TodoActionEtiquetteInput,
  ctx: Context,
) => {
  const { id, isRemoved, codeMaj, idAction, idEtiquette } = input;
  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: TodoActionEtiquetteCreateInput = {
    codeMaj,
    isRemoved,
    userCreation,
    userModification,
    action: idAction ? { connect: { id: idAction } } : null,
    etiquette: idEtiquette ? { connect: { id: idEtiquette } } : null,
  };
  const todoActionEtiquette = await ctx.prisma.upsertTodoActionEtiquette({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  await ctx.indexing.publishSave('todoactionetiquettes', todoActionEtiquette.id);
  return todoActionEtiquette;
};

export const softDeleteTodoActionEtiquette = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateTodoActionEtiquette({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      await ctx.indexing.publishSave('todoactionetiquettes', res.id);
      return res;
    });
};
