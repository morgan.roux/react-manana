import {
  PartenaireCreateInput,
  PartenaireServicePartenaireCreateInput,
  PartenaireServiceSuiteCreateInput,
  ServicePartenaireCreateInput,
} from '../generated/prisma-client';
import { Context, PartenaireServiceInput } from '../types';
import { deleteUser } from './user';

export const getUserPartenaire = async (ctx: Context, id: string) => {
  return ctx.prisma
    .partenaire({
      id,
    })
    .user();
};

export const createUpdatePartenaireService = async (args: PartenaireServiceInput, ctx: Context) => {
  const {
    id,
    dateDebut,
    dateFin,
    nom,
    adresse1,
    ville,
    codePostal,
    idTypeService,
    servicePartenaires,
    statutPartenaire,
    typePartenaire,
    idSecteur,
    cerclePartenariat,
    telephone,
    email,
    prive,
    idPharmacie,
  } = args;

  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;

  // 1 - Save partenaire service suite (partenaireServicePartenaire)
  const parteServSuiteData: PartenaireServiceSuiteCreateInput = {
    nom,
    ville,
    adresse1,
    codePostal,
    groupement,
    telephone,
    email,
  };
  const parteServSuiteExistant = await ctx.prisma
    .partenaire({ id: id || '' })
    .partenaireServiceSuite();
  const parteServSuite = await ctx.prisma.upsertPartenaireServiceSuite({
    where: { id: (parteServSuiteExistant && parteServSuiteExistant.id) || '' },
    create: parteServSuiteData,
    update: parteServSuiteData,
  });

  // 2 - Save partenaire service (partenaire)
  const parteData: PartenaireCreateInput = {
    groupement,
    nom,
    idSecteur,
    cerclePartenariat,
    partenaireServiceSuite: parteServSuite ? { connect: { id: parteServSuite.id } } : null,
    prive,
    idPharmacie,
  };
  const parte = await ctx.prisma.upsertPartenaire({
    where: { id: id || '' },
    create: parteData,
    update: parteData,
  });

  // 3 - Save partenaire service partenaire (servicePartenairePartenaire)
  const parteServParteData: PartenaireServicePartenaireCreateInput = {
    dateDebutPartenaire: dateDebut,
    dateFinPartenaire: dateFin,
    typePartenaire: typePartenaire as any,
    statutPartenaire: statutPartenaire as any,
    partenaireService: parte ? { connect: { id: parte.id } } : null,
  };
  // Si le partenaire est un partenaire active, on fait un update si non on fait create
  const parteServParteExistants = await ctx.prisma.partenaireServicePartenaires({
    where: { partenaireService: { id: id || '' }, statutPartenaire: 'ACTIVER' },
  });

  const parteServParte = await ctx.prisma.upsertPartenaireServicePartenaire({
    where: {
      id: parteServParteExistants.length > 0 ? parteServParteExistants[0].id : '',
    },
    create: parteServParteData,
    update: parteServParteData,
  });

  console.log('parteServParte :>> ', parteServParte);

  // 4 - Save services partenaires (servicePartenaire)
  if (servicePartenaires && servicePartenaires.length > 0) {
    const ServPartesExistants = await ctx.prisma.servicePartenaires({
      where: { partenaireService: { id: id || '' } },
    });

    await Promise.all(
      servicePartenaires.map(async sp => {
        const spData: ServicePartenaireCreateInput = {
          nom: sp.nom,
          dateDemarrage: sp.dateDemarrage,
          nbCollaboQualifie: sp.nbCollaboQualifie,
          commentaire: sp.commentaire,
          serviceType: idTypeService ? { connect: { id: idTypeService } } : null,
          partenaireService: parte ? { connect: { id: parte.id } } : null,
        };
        const spExistant = ServPartesExistants.find(i => i.id === sp.id);
        const savecSp = await ctx.prisma.upsertServicePartenaire({
          where: { id: (spExistant && spExistant.id) || '' },
          create: spData,
          update: spData,
        });
        await ctx.indexing.publishSave('servicepartenaires', savecSp.id);
        // return savecSp;
      }),
    );
  }

  await ctx.indexing.publishSave('partenaires', parte.id);
  return parte;
};

export const softDeletePartenaireService = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updatePartenaire({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async p => {
      const user = await ctx.prisma.partenaire({ id: p.id }).user();
      if (user) {
        await deleteUser(ctx, user);
      }
      await ctx.indexing.publishSave('partenaires', p.id);
      return p;
    });
};
