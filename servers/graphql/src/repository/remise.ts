import { Context, RemiseDetailInput } from '../types';
import moment from 'moment';
import { RemiseDetail, ProduitCanal } from '../generated/prisma-client';
const now = moment().format();

export interface FragmentLigne {
  id: string;
  idCanalArticle: string;
  quantite: number;
  prixBase: number;
}

export interface RemiseResult {
  remise: number;
  articleSamePanachees: FragmentLigne[];
  ug: number;
  canalArticleUG: ProduitCanal;
  isPalier: boolean;
}

export const getRemise = async (
  ctx: Context,
  { idPharmacie, idCanalArticle, codeCanal, quantite, idPanier, idCommande },
) => {
  const remisePanacheesDetails = await getRemisePanacheeDetails(ctx, {
    idPharmacie,
    idCanalArticle,
    codeCanal,
  });

  if (remisePanacheesDetails && remisePanacheesDetails.length) {
    const articleSameRemisePanachees = await articleSamePanachees(ctx, {
      idCanalArticle,
      idPharmacie,
      codeCanal,
    });

    const idCanalArticles = articleSameRemisePanachees
      .filter(articleCanal => articleCanal.id !== idCanalArticle)
      .map(same => same.id);

    if (!idCanalArticles || (idCanalArticles && !idCanalArticles.length)) {
      return remisePalier(ctx, { idCanalArticle, codeCanal, idPharmacie, quantite });
    }

    // remise same panachees in panier or commande ligne without current article
    const current = await remiseArticleSamePanacheInLignes(
      ctx,
      remisePanacheesDetails,
      idCanalArticles,
      {
        idPanier,
        idCommande,
        quantite,
      },
    );

    return current && current.remise.length
      ? {
          remise: current.remise[current.remise.length - 1].pourcentageRemise,
          articleSamePanachees: current.articleSamePanachees,
          ug: current.remise[current.remise.length - 1]
            ? current.remise[current.remise.length - 1].nombreUg
            : 0,
          canalArticleUG: current.remise[current.remise.length - 1]
            ? await ctx.prisma
                .remiseDetail({
                  id: current.remise[current.remise.length - 1].id,
                })
                .produitCanalUg()
            : null,
          isPalier: false,
        }
      : {
          remise: 0,
          articleSamePanachees: null,
          ug: 0,
          canalArticleUG: null,
          isPalier: false,
        };
  } else {
    return remisePalier(ctx, { idCanalArticle, codeCanal, idPharmacie, quantite });
  }
};

const remisePalier = async (ctx: Context, { idCanalArticle, codeCanal, idPharmacie, quantite }) => {
  const remiseLignes = await remiseLigneDetails(ctx, {
    idPharmacie,
    idCanalArticle,
    codeCanal,
  });

  if (!remiseLignes || (remiseLignes && !remiseLignes.length)) {
    return {
      remise: 0,
      articleSamePanachees: null,
      ug: 0,
      canalArticleUG: null,
      isPalier: true,
    };
  }
  const remiseLigne = remiseLignes.filter(
    ligne =>
      ligne.quantiteMin <= parseInt(quantite, 10) && ligne.quantiteMax >= parseInt(quantite, 10),
  );
  const ligne = remiseLigne[remiseLigne.length - 1];
  return {
    remise: ligne ? ligne.pourcentageRemise : 0,
    articleSamePanachees: null,
    ug: ligne ? ligne.nombreUg : 0,
    canalArticleUG: ligne
      ? await ctx.prisma
          .remiseDetail({
            id: ligne.id,
          })
          .produitCanalUg()
      : null,
    isPalier: true,
  };
};

export const remiseLigneDetails = async (
  ctx: Context,
  { idPharmacie, idCanalArticle, codeCanal },
) => {
  const groupes = await _groupeClient(ctx, { idPharmacie });

  if (!groupes || groupes.length === 0) return null;

  const remiseLigne = await _remiseLignes(ctx, {
    idCanalArticle,
    codeCanal,
    codeGroupes: groupes.map(groupe => groupe.codeGroupe),
  });

  if (!remiseLigne) return null;

  const remise = await ctx.prisma.remiseLigne({ id: remiseLigne.id }).remise();

  if (!remise) return null;

  return remiseDetails(ctx, { idRemise: remise.id });
};

const _remiseLignes = async (ctx: Context, { idCanalArticle, codeCanal, codeGroupes }) => {
  return ctx.prisma
    .remiseLignes({
      where: {
        produitCanal: { id: idCanalArticle },
        groupeClient: {
          codeGroupe_in: codeGroupes,
        },
        remise: {
          commandeCanal: {
            code: codeCanal,
          },
        },
        dateDebut_lte: now,
        dateFin_gte: now,
      },
      first: 1,
    })
    .then(remiseLignes => (remiseLignes ? remiseLignes[remiseLignes.length - 1] : null));
};

export const remiseArticleSamePanacheInLignes = async (
  ctx: Context,
  remisePanacheesDetails: RemiseDetail[],
  idCanalArticles: string[], // id canal article same panache
  { idPanier, quantite, idCommande },
) => {
  let ligneArticles: FragmentLigne[] = [];
  let qteLigneSamePanache: number = 0;

  if (idPanier || idCommande) {
    ligneArticles = idPanier
      ? await getArticlePanacheesInPanier(ctx, idCanalArticles, { idPanier })
      : await getArticlePanacheesInCommande(ctx, idCanalArticles, { idCommande });

    console.log('ligneArticles ===>', ligneArticles);

    qteLigneSamePanache = ligneArticles.length
      ? ligneArticles.map(ligne => ligne.quantite).reduce((sum, qte) => sum + qte)
      : 0;
  }
  const qteTotal = qteLigneSamePanache + parseInt(quantite, 10);
  return {
    remise: remisePanacheesDetails.filter(
      panachee => panachee.quantiteMin <= qteTotal && panachee.quantiteMax >= qteTotal,
    ),
    articleSamePanachees: ligneArticles,
  };
};

export const getArticlePanacheesInCommande = async (
  ctx: Context,
  idCanalArticles: string[],
  { idCommande },
) => {
  const articlesCanals = await ctx.prisma
    .commandeLignes({
      where: {
        idCommande: { id: idCommande },
        produitCanal: { id_in: idCanalArticles },
      },
    })
    .then(commandeLignes =>
      Promise.all(
        commandeLignes.map(async ligne => {
          const article = ligne
            ? await ctx.prisma.commandeLigne({ id: ligne.id }).produitCanal()
            : null;
          if (article) {
            return {
              id: ligne.id,
              idCanalArticle: article.id,
              quantite: ligne.quantiteCdee,
              prixBase: article.prixPhv,
            };
          }
        }),
      ),
    );

  return articlesCanals.filter(articlesCanal => articlesCanal);
};

export const getArticlePanacheesInPanier = async (
  ctx: Context,
  idCanalArticles: string[],
  { idPanier },
) => {
  return ctx.prisma
    .panierLignes({
      where: {
        idPanier: { id: idPanier },
        produitCanal: { id_in: idCanalArticles },
      },
    })
    .then(panierLignes =>
      Promise.all(
        panierLignes
          .map(async ligne => {
            const article = ligne
              ? await ctx.prisma.panierLigne({ id: ligne.id }).produitCanal()
              : null;
            if (article) {
              return {
                id: ligne.id,
                idCanalArticle: article.id,
                quantite: ligne.quantite,
                prixBase: article.prixPhv,
              };
            }
          })
          .filter(panierLigne => panierLigne),
      ),
    );
};

/* recuperer la remise panachees du produit */
export const getRemisePanacheeDetails = async (
  ctx: Context,
  { idPharmacie, idCanalArticle, codeCanal },
) => {
  const groupes = await _groupeClient(ctx, { idPharmacie });

  if (!groupes || (groupes && !groupes.length)) return null;

  const remisePanache = await remisePanachee(ctx, {
    idCanalArticle,
    codesGroupe: groupes.map(groupe => groupe.codeGroupe),
    codeCanal,
  });
  if (!remisePanache) return null;

  const remise = await ctx.prisma.remisePanachee({ id: remisePanache.id }).remise();
  if (!remise) return null;

  return remiseDetails(ctx, { idRemise: remise.id });
};

const _groupeClient = async (ctx: Context, { idPharmacie }) => {
  return ctx.prisma.groupeClients({
    where: { pharmacies_some: { id: idPharmacie } },
  });
};

const remisePanachee = async (ctx: Context, { idCanalArticle, codesGroupe, codeCanal }) => {
  return ctx.prisma
    .remisePanachees({
      where: {
        produitCanal: { id: idCanalArticle },
        groupeClient: {
          codeGroupe_in: codesGroupe,
        },
        remise: {
          commandeCanal: {
            code: codeCanal,
          },
        },
        dateDebut_lte: now,
        dateFin_gte: now,
      },
      first: 1,
    })
    .then(remisePanachees =>
      remisePanachees ? remisePanachees[remisePanachees.length - 1] : null,
    );
};

const remiseDetails = async (ctx: Context, { idRemise }) => {
  return ctx.prisma.remiseDetails({
    where: { remise: { id: idRemise } },
  });
};

/* article meme remise panachees */
export const articleSamePanachees = async (
  ctx: Context,
  { idCanalArticle, codeCanal, idPharmacie },
) => {
  const groupes = await _groupeClient(ctx, { idPharmacie });

  if (!groupes || (groupes && !groupes.length)) return [];

  const remisePanache = await remisePanachee(ctx, {
    idCanalArticle,
    codesGroupe: groupes.map(groupe => groupe.codeGroupe),
    codeCanal,
  });

  if (!remisePanache) return [];

  const remise = await ctx.prisma.remisePanachee({ id: remisePanache.id }).remise();

  if (!remise) return [];

  const panachees = await ctx.prisma.remisePanachees({
    where: {
      remise: { id: remise.id },
    },
  });

  if (!panachees || (panachees && !panachees.length)) return [];

  const articles = await Promise.all(
    panachees.map(panache => ctx.prisma.remisePanachee({ id: panache.id }).produitCanal()),
  );

  if (!articles || (articles && !articles.length)) return [];

  return ctx.prisma.produitCanals({
    where: {
      id_in: articles.map(article => article.id),
    },
  });
};

export const setRemiseSamePanachees = async (
  ctx: Context,
  ligneArticles: FragmentLigne[],
  pourcentageRemise: number,
  isPanier: boolean,
) => {
  return Promise.all(
    ligneArticles.map(ligne => {
      const prixNetUnitaireHT = (ligne.prixBase * (100 - pourcentageRemise)) / 100;
      return isPanier
        ? ctx.prisma.updatePanierLigne({
            data: {
              prixBaseHT: ligne.prixBase,
              prixNetUnitaireHT,
              prixTotalHT: prixNetUnitaireHT * ligne.quantite,
              remiseLigne: 0,
              remiseGamme: pourcentageRemise,
            },
            where: { id: ligne.id },
          })
        : ctx.prisma.updateCommandeLigne({
            data: {
              prixBaseHT: ligne.prixBase,
              prixNetUnitaireHT,
              prixTotalHT: prixNetUnitaireHT * ligne.quantite,
              remiseLigne: 0,
              remiseGamme: pourcentageRemise,
            },
            where: { id: ligne.id },
          });
    }),
  );
};

export const pharmacieRemisePaliers = async (idCanalArticle, codeCanal, ctx: Context) => {
  const remiseLignes = await ctx.prisma.remiseLignes({
    where: {
      produitCanal: { id: idCanalArticle },
      remise: {
        commandeCanal: {
          code: codeCanal,
        },
      },
      dateDebut_lte: now,
      dateFin_gte: now,
    },
  });

  if (!remiseLignes || (remiseLignes && !remiseLignes.length)) return [];

  const groupeClients = await Promise.all(
    remiseLignes.map(remiseLigne => ctx.prisma.remiseLigne({ id: remiseLigne.id }).groupeClient()),
  );

  if (!groupeClients || (groupeClients && !groupeClients.length)) return [];

  return (
    await Promise.all(
      groupeClients.map(groupeClient =>
        ctx.prisma.groupeClient({ id: groupeClient.id }).pharmacies(),
      ),
    )
  ).flat();
};

export const pharmacieRemisePanachees = async ({ idCanalArticle, codeCanal }, ctx: Context) => {
  const remisePanachees = await ctx.prisma.remisePanachees({
    where: {
      produitCanal: { id: idCanalArticle },
      remise: {
        commandeCanal: {
          code: codeCanal,
        },
      },
      dateDebut_lte: now,
      dateFin_gte: now,
    },
  });

  if (!remisePanachees || (remisePanachees && !remisePanachees.length)) return [];

  const groupeClients = await Promise.all(
    remisePanachees.map(remisePanachee =>
      ctx.prisma.remisePanachee({ id: remisePanachee.id }).groupeClient(),
    ),
  );

  if (!groupeClients || (groupeClients && !groupeClients.length)) return [];

  return (
    await Promise.all(
      groupeClients.map(groupeClient =>
        ctx.prisma.groupeClient({ id: groupeClient.id }).pharmacies(),
      ),
    )
  ).flat();
};

export const deleteRemise = async (ctx: Context, { idRemise }) => {
  await ctx.prisma.deleteManyRemiseDetails({ remise: { id: idRemise } });
  await ctx.prisma.deleteManyRemiseLignes({ remise: { id: idRemise } });
  return ctx.prisma.deleteRemise({ id: idRemise });
};

export const createRemisePalier = async (
  ctx: Context,
  {
    codeGroupeClient,
    nomRemise,
    codeCanal,
    dateDebut,
    dateFin,
    active,
    nbRefMin,
    modelRemise,
    typeRemise,
    source,
  },
  add: number = 1,
) => {
  const remise = await ctx.prisma.createRemise({
    nomRemise,
    dateDebut,
    dateFin,
    dateModification: moment().format(),
    active,
    nbRefMin,
    modelRemise,
    typeRemise,
    source,
    commandeCanal: {
      connect: {
        code: codeCanal,
      },
    },
  });

  await ctx.prisma.createRemiseLigne({
    remise: {
      connect: {
        id: remise.id,
      },
    },
    groupeClient: {
      connect: {
        codeGroupe: codeGroupeClient,
      },
    },
    dateDebut,
    dateFin,
  });

  return remise;
};

export const createRemiseDetail = async (
  ctx: Context,
  idRemise: string,
  { quantiteMin, quantiteMax, pourcentageRemise, remiseSupplementaire }: RemiseDetailInput,
) => {
  return ctx.prisma.createRemiseDetail({
    remise: {
      connect: {
        id: idRemise,
      },
    },
    quantiteMin,
    quantiteMax,
    pourcentageRemise,
    remiseSupplementaire,
  });
};
