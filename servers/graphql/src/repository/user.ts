// tslint:disable-next-line: no-var-requires
require('dotenv').config();
import { ApolloError } from 'apollo-server';
import axios from 'axios';
import {
  PARTENAIRE_LABORATOIRE,
  PARTENAIRE_SERVICE,
  PRESIDENT_REGION,
  SUPER_ADMINISTRATEUR,
  TITULAIRE_PHARMACIE,
} from '../constants/roles';
import { Fichier, Prisma, User } from '../generated/prisma-client';
import { UserPersonnelIHM } from '../interfaces/user';
import * as jwt from 'jsonwebtoken';
import logger from '../logging';
import { sendMail } from '../services/mailer';
import { Context, FichierInput } from '../types';
import { createActiviteUser } from './activiteUser';
import { ParameterGroupementByCode } from './parameter';
import {
  pharmacieUserTitulaire,
  pharmacieUserPpersonnel,
  userTitulaireContact,
  userPpersonnelContact,
} from './pharmacie';
import bcrypt = require('bcryptjs');
import crypto = require('crypto');
import moment = require('moment');

export const loginUser = async (ctx: Context, { password, login, ip }: any) => {
  try {
    const user = await ctx.prisma
      .users({ where: { login } })
      .then(users => (users && users.length ? users[0] : null));

    logger.info(`user ====> ${JSON.stringify(user)}`);

    if (!user) {
      return new ApolloError(
        `Il n'y a pas encore de compte créer pour ${login}`,
        'ACCOUNT_NOT_EXIST',
      );
    }

    // Blocage user acces params
    const param = await ctx.prisma.parameter({ code: '0321' });
    const paramDefaultValue = param && param.defaultValue ? parseInt(param.defaultValue, 10) : 60;
    const paramValue = await ctx.prisma
      .parameterValues({
        where: { idParameter: { id: param && param.id } },
      })
      .then(results => {
        if (results && results.length > 0) {
          const res = results[0];
          return res.value ? parseInt(res.value, 10) : 0;
        }
      });

    const value = paramValue > 0 ? paramValue : paramDefaultValue;

    // Check last login
    if (user.lastLoginDate) {
      const now = moment();
      const lastLoginDate = moment(user.lastLoginDate);
      if (now.diff(lastLoginDate, 'days') > value) {
        return new ApolloError("Compte d'utilisateur bloqué", 'USER_BLOCKED_OLD_LAST_LOGIN');
      }
    }

    // Check user status
    if (user.status === 'BLOCKED') {
      return new ApolloError("Compte d'utilisateur bloqué", 'USER_BLOCKED');
    }

    if (user.status === 'ACTIVATION_REQUIRED') {
      return new ApolloError("Compte d'utilisateur en attente d'activation", 'ACTIVATION_REQUIRED');
    }

    const isPasswordValid = await bcrypt.compare(password.trim(), user.passwordHash);
    if (!isPasswordValid) {
      return new ApolloError('Mot de passe invalide', 'PASSWORD_INVALID');
    }

    // Check groupment
    const groupement = await ctx.prisma.user({ id: user.id }).groupement();
    if (groupement && groupement.sortie === 1) {
      return new ApolloError('Groupment is exited', 'GROUPMENT_EXITED');
    }

    // Check Personnel Groupement
    const personnels = await ctx.prisma.personnels({
      where: { user: { id: user.id } },
      first: 1,
    });
    if (personnels && personnels.length && personnels[0] && personnels[0].sortie === 1) {
      return new ApolloError('Personnel is exited', 'PERSONNEL_EXITED');
    }

    // Check Personnel Pharmacie
    const ppersonnels = await ctx.prisma.ppersonnels({
      where: { user: { id: user.id } },
    });
    if (ppersonnels && ppersonnels.length && ppersonnels[0] && ppersonnels[0].sortie === 1) {
      return new ApolloError('Ppersonnel is exited', 'PPERSONNEL_EXITED');
    }

    // Generate
    const accessToken = jwt.sign({ sub: user.id }, process.env.JWT_SECRET, {
      expiresIn: `${process.env.JWT_SIGN_OPTIONS_EXPIRES_IN_SECONDS}s`,
    }); // (await crypto.randomBytes(256)).toString('hex');
    await ctx.redis.set(accessToken, user.id, 'EX', process.env.REDIS_EXPIRE);

    // Update last login
    await ctx.prisma.updateUser({
      where: { id: user.id },
      data: { lastLoginDate: new Date().toISOString() },
    });

    return { accessToken };
  } catch (error) {
    throw new Error(error);
  }
};

export const createUser = async ({ email, login, password }: any, ctx: Context) => {
  // Check if user exist
  try {
    const existUser = await ctx.prisma.$exists.user({ login });

    if (existUser) {
      throw new Error('DUPLICATE_LOGIN');
    }
    const hashedPassword = await bcrypt.hash(password, 10);
    return ctx.prisma
      .createUser({
        email,
        login,
        passwordHash: hashedPassword,
      })
      .then(async user => {
        await ctx.indexing.publishSave('users', user.id);
        return user;
      });
  } catch (error) {
    throw new Error(error);
  }
};

export const signup = async (
  ctx: Context,
  { email, login, password }, // TODO : Create interface SignupPayload {accessToken: string}
) => {
  try {
    const newUser = await createUser({ email, login, password }, ctx);
    const accessToken = (await crypto.randomBytes(256)).toString('hex');
    await ctx.redis.set(accessToken, newUser.id);
    return { accessToken };
  } catch (error) {
    throw new Error(error);
  }
};

export const deleteUser = async (
  ctx: Context,
  { id }: any, // TODO: Create type User
) => {
  try {
    /* await Promise.all([
      ctx.prisma.deleteManyUserTraitements({
        user: {
          id,
        },
      }),
      ctx.prisma.deleteManyUserRoles({
        idUser: {
          id
        },
      }),
      ctx.prisma.deleteManyUserPhotos({
        idUser: {
          id
        },
      }),
      ctx.prisma.deleteManyUserTitulaires({
        idUser: {
          id
        },
      }),
    ]); */

    /* 
      On ne supprime directment l'user , on met juste le status a BLOQUER
      Ou prevoir dans le future une status DELETED
     */

    await ctx.prisma.updateUser({ where: { id }, data: { status: 'BLOCKED' } });
    await ctx.indexing.publishSave('users', id);
  } catch (error) {
    throw new Error(error);
  }
};

export const updatePassword = async (ctx: Context, { lastPassword, newPassword }) => {
  try {
    const user = await ctx.prisma.user({ id: ctx.userId });

    if (lastPassword) {
      const isPassword = await bcrypt.compare(lastPassword.trim(), user.passwordHash);
      if (!isPassword) {
        throw new Error('INVALID_PASSWORD');
      }
    }

    const passwordHash = await bcrypt.hash(newPassword, 10);
    const lastPasswordChangedDate = new Date().toISOString();
    const status = 'ACTIVATED';

    updateUserPassword(ctx, { id: ctx.userId, passwordHash, lastPasswordChangedDate, status });

    const success = true;
    const result = { success };
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

export const getuserByToken = async (ctx: Context, { token }) => {
  const login = await ctx.redis.get(token);

  const users = await ctx.prisma.users({
    where: { login },
    first: 1,
  });

  return users && users.length ? users[0] : null;
};

export const resetPassword = async (ctx: Context, { newPassword, token }) => {
  try {
    const login = await ctx.redis.get(token);
    if (!login) {
      throw new Error('INVALID TOKEN');
    }

    const userByLogin = await ctx.prisma
      .users({
        where: { login },
      })
      .then(users => (users && users.length ? users[0] : null));

    if (!userByLogin) {
      throw new Error('LOGIN NOT FOUND');
    }

    const passwordHash = await bcrypt.hash(newPassword, 10);
    const lastPasswordChangedDate = new Date().toISOString();
    const status = 'ACTIVATED';

    const user = await updateUserPassword(ctx, {
      id: userByLogin.id,
      passwordHash,
      lastPasswordChangedDate,
      status,
    });

    if (user && user.email) {
      const emailOwner = user.email === user.login;

      const emailParameters = await _getEmailParameters(ctx);
      const pharmacieSubstitutionData =
        (await _getUserPharmacieCoordonnees(ctx, {
          userId: user.id,
          userLogin: null,
        })) || {};
      sendMail({
        ...emailParameters,
        to: user.email,
        templateId: emailOwner
          ? process.env.RESET_PASSWORD_TEMPLATE_ID
          : process.env.COWORKER_RESET_PASSWORD_TEMPLATE_ID,
        substitutionData: { fullName: user.userName, ...pharmacieSubstitutionData },
      })
        .then(response => logger.info(`Email sent => ${response}`))
        .catch(error => logger.error(`Sending email error : ${error}`));
    }

    // check SSO
    try {
      const checkHelloidSso = await _checkSsoHelloid(ctx, userByLogin.id, '0046');

      if (checkHelloidSso.haveHelloid && checkHelloidSso.ssoParameterValue === 'true') {
        console.log('sending to helloid', checkHelloidSso.haveHelloid[0]);
        axios
          .post(`${process.env.SAML_SSO_ENDPOINT}/create-helloiduser`, {
            id: userByLogin.id,
            isEnabled: true,
            mustChangePassword: false,
            password: newPassword,
            source: 'Gcr-pharma',
            groupementId: checkHelloidSso.userGroupement.id,
          })
          .then(() => console.log('User created to helloId'))
          .catch(err => {
            console.log('Error when creating helloId user', { error: err });
          });
      }
    } catch (err) {
      logger.error(`error updating helloid`, err);
    }

    await ctx.redis.del(token);
    return token;
  } catch (error) {
    throw new Error(error);
  }
};

export const updateUserPassword = async (
  ctx: Context,
  { id, passwordHash, lastPasswordChangedDate, status },
) => {
  return ctx.prisma
    .updateUser({
      data: { passwordHash, status, lastPasswordChangedDate },
      where: { id },
    })
    .then(async updatedUser => {
      await ctx.indexing.publishSave('users', updatedUser.id);

      // Check user type and publish it
      _checkUserTypeAndPublishIt(updatedUser, ctx);

      return updatedUser;
    });
};

export const _checkSsoHelloid = async (ctx: Context, iduser: string, code: string) => {
  const groupement = await ctx.prisma.user({ id: iduser }).groupement();
  const haveHelloid = await ctx.prisma.helloIdSsoes({
    where: { groupement: { id: groupement && groupement.id } },
  });
  const ssoparameter = await ctx.prisma.parameter({ code });
  const parameterValue = await ctx.prisma
    .parameterValues({
      where: { idParameter: { code }, idSource: groupement && groupement.id },
    })
    .then(values => (values && values.length ? values[0] : null));
  const ssoParameterValue = parameterValue.value ? parameterValue.value : ssoparameter.defaultValue;

  logger.info('userGroupement ==================>', groupement);
  logger.info('parameterValue ==================>', parameterValue);
  logger.info('ssoparameter ==================>', ssoparameter);
  logger.info('ssoParameterValue ==================>', ssoParameterValue);
  logger.info('haveHelloid ==================>', haveHelloid);
  return {
    haveHelloid,
    userGroupement: groupement,
    ssoParameterValue: ssoParameterValue && ssoParameterValue.toLocaleLowerCase(),
  };
};

export const updateUserStatus = async (ctx: Context, { id, status }) => {
  try {
    const existUser = await ctx.prisma.$exists.user({ id });
    if (!existUser) {
      return new ApolloError('User not found', 'USER_NOT_FOUND');
    }

    const updatedUser = await ctx.prisma.updateUser({
      data: { status, lastLoginDate: status === 'ACTIVATED' ? new Date() : undefined },
      where: { id },
    });

    // update  sso user status
    try {
      const checkHelloidSso = await _checkSsoHelloid(ctx, updatedUser.id, '0046');
      if (checkHelloidSso.haveHelloid && checkHelloidSso.ssoParameterValue === 'true') {
        const helloidstatus = status === 'ACTIVATED';

        console.log('sending to helloid', checkHelloidSso.haveHelloid[0]);
        axios
          .post(`${process.env.SAML_SSO_ENDPOINT}/update-helloiduser`, {
            id: updatedUser.id,
            isEnabled: helloidstatus,
            mustChangePassword: false,
            isLocked: helloidstatus,
            groupementId: checkHelloidSso.userGroupement.id,
          })
          .then(() => console.log('User updated to helloId'))
          .catch(err => {
            console.log('Error when updating helloId user', { error: err });
          });
      }
    } catch (err) {
      logger.error(`error updating helloid`, err);
    }

    // Check user type and publish it
    _checkUserTypeAndPublishIt(updatedUser, ctx);
    await ctx.indexing.publishSave('users', updatedUser.id);

    return updatedUser;
  } catch (error) {
    throw new Error(error);
  }
};

export const sendMailForgotPassword = async (ctx: Context, { login }) => {
  try {
    const existUser = await ctx.prisma.$exists.user({
      login,
    });
    if (!existUser) {
      throw new Error('LOGIN NOT FOUND');
    }

    const user = await ctx.prisma.user({ login });
    if (user.email) {
      const accessToken = crypto.randomBytes(256).toString('hex');
      await ctx.redis.set(accessToken, user.email);

      const url = process.env.CLIENT_URL.replace('http://', '').replace('https://', '');
      // paramater for sparkpost template

      const pharmacieSubstitutionData =
        (await _getUserPharmacieCoordonnees(ctx, {
          userId: user.id,
          userLogin: null,
        })) || {};

      const substitutionData = {
        app_url: url,
        token: accessToken,
        fullName: user.userName,
        ...pharmacieSubstitutionData,
      };

      const emailOwner = user.email === user.login;

      const templateId = emailOwner
        ? process.env.FORGOT_PASSWORD_TEMPLATE_ID
        : process.env.COWORKER_FORGOT_PASSWORD_TEMPLATE_ID;

      try {
        const emailParameters = await _getEmailParameters(ctx);

        await sendMail({ ...emailParameters, to: user.email, templateId, substitutionData });
      } catch (error) {
        console.log('ERROR SEND MAIL', error);
        return false;
      }

      return true;
    } else {
      throw new Error('EMAIL NOT FOUND');
    }
  } catch (error) {
    throw new Error(error);
  }
};

// create user collaborateur groupement
export const createUserGroupement = async (
  {
    idGroupement,
    id,
    email,
    login,
    role,
    userId,
    status,
    day,
    month,
    year,
    userPhoto,
    codeTraitements,
  }: UserPersonnelIHM,
  ctx: Context,
) => {
  //verify if login is unique
  const existLogins = await ctx.prisma.users({ where: { login } });

  if (existLogins && existLogins.length > 0) {
    return new ApolloError('Login déjà utilisé par un utilisateur', 'LOGIN_ALREADY_EXIST');
  }

  const personnel = await _getPersonnel(id, ctx);

  const dateNaissance = year
    ? {
      jourNaissance: day,
      moisNaissance: month,
      anneeNaissance: year,
    }
    : {};

  return ctx.prisma
    .updatePersonnel({
      where: {
        id,
      },
      data: {
        user: {
          upsert: {
            create: {
              email,
              login,
              status: status ? status : 'ACTIVATION_REQUIRED',
              emailConfirmed: false,
              userName: `${personnel.prenom ? personnel.prenom.trim() : ''} ${personnel.nom ? personnel.nom.trim() : ''
                }`,
              groupement: {
                connect: {
                  id: idGroupement,
                },
              },
              ...dateNaissance,
            },
            update: {
              email,
              login,
              ...dateNaissance,
            },
          },
        },
      },
    })
    .then(async personnel => {
      const user = await ctx.prisma.personnel({ id: personnel.id }).user();
      if (user && userPhoto && userPhoto.chemin) {
        await createUpdateUserPhoto(ctx, user.id, userPhoto);
      }

      if (user)
        createUpdateUserTraitement(ctx, { userId: user.id, codeTraitements, idPharmacie: null });

      _createUserRoleAndSendEmail(
        { email, userName: user.userName, login, idGroupement, origine: 'GROUPEMENT', role },
        ctx,
      );

      const service = await ctx.prisma.personnel({ id }).service();
      if (service && service.id) await ctx.indexing.publishSave('services', service.id);

      await ctx.indexing.publishSave('personnels', personnel.id);
      return personnel;
    });
};

export const createUserPresidentRegion = async (
  {
    idGroupement,
    id,
    idPharmacie,
    email,
    login,
    userId,
    day,
    month,
    year,
    userPhoto,
    codeTraitements,
  }: {
    idGroupement: string;
    id: string;
    idPharmacie: string;
    email: string;
    login: string;
    userId?: string;
    day?: number;
    month?: number;
    year?: number;
    userPhoto?: FichierInput;
    codeTraitements?: string[];
  },
  ctx: Context,
) => {
  //verify if login is unique
  const existLogins = await ctx.prisma.users({ where: { login } });

  if (existLogins && existLogins.length > 0 && existLogins[0].id !== userId) {
    return new ApolloError('Login déjà utilisé par un utilisateur', 'LOGIN_ALREADY_EXIST');
  }

  const userPresident = userId
    ? await ctx.prisma
      .userTitulaires({ where: { idUser: { id: userId } } })
      .then(userPresidents => userPresidents[0].id)
    : '';

  const userPresidents = await ctx.prisma.userTitulaires({
    where: {
      idPharmacie: { id: idPharmacie },
      idTitulaire: { id },
    },
  });

  if (!userId && userPresidents && userPresidents.length > 0) {
    return new ApolloError(
      'Le president de région a déjà un compte créer sur cette pharmacie',
      'ACCOUNT_ALREADY_CREATED',
    );
  }

  const titulaire = await _getTitulaire(id, ctx);

  const dateNaissance = year
    ? {
      jourNaissance: day,
      moisNaissance: month,
      anneeNaissance: year,
    }
    : {};

  return ctx.prisma
    .upsertUserTitulaire({
      where: {
        id: userPresident,
      },
      create: {
        idTitulaire: {
          connect: { id },
        },
        idPharmacie: {
          connect: {
            id: idPharmacie,
          },
        },
        idUser: {
          create: {
            email,
            login,
            status: 'ACTIVATION_REQUIRED',
            emailConfirmed: false,
            userName: `${titulaire.prenom.trim()} ${titulaire.nom.trim()}`,
            groupement: {
              connect: {
                id: idGroupement,
              },
            },
            ...dateNaissance,
          },
        },
      },
      update: {
        idPharmacie: {
          connect: {
            id: idPharmacie,
          },
        },
        idUser: {
          update: {
            email,
            login,
            ...dateNaissance,
          },
        },
      },
    })
    .then(async userTitulaire => {
      const user = await ctx.prisma.userTitulaire({ id: userTitulaire.id }).idUser();
      if (user && userPhoto && userPhoto.chemin) {
        await createUpdateUserPhoto(ctx, user.id, userPhoto);
      }

      if (user)
        createUpdateUserTraitement(ctx, { userId: user.id, codeTraitements, idPharmacie: null });
      await ctx.indexing.publishSave('titulaires', id);
      return _createUserRoleAndSendEmail(
        {
          email,
          idGroupement,
          userName: user.userName,
          login,
          origine: 'PHARMACIE',
          role: PRESIDENT_REGION,
        },
        ctx,
      );
    })
    .then(_ => ctx.prisma.titulaire({ id }));
};

export const createUserTitulaire = async (
  {
    idGroupement,
    id,
    email,
    login,
    idPharmacie,
    userId,
    day,
    month,
    year,
    userPhoto,
    codeTraitements,
  }: {
    idGroupement: string;
    id: string;
    email: string;
    login: string;
    idPharmacie: string;
    userId?: string;
    day?: number;
    month?: number;
    year?: number;
    userPhoto?: FichierInput;
    codeTraitements?: string[];
  },
  ctx: Context,
) => {
  //verify if login is unique
  const existLogins = await ctx.prisma.users({ where: { login } });

  if (existLogins && existLogins.length > 0 && existLogins[0].id !== userId) {
    return new ApolloError('Login déjà utilisé par un utilisateur', 'LOGIN_ALREADY_EXIST');
  }

  const userTitulaire = userId
    ? await ctx.prisma
      .userTitulaires({ where: { idUser: { id: userId } } })
      .then(userTitulaires => userTitulaires[0].id)
    : '';

  const userTitulaires = await ctx.prisma.userTitulaires({
    where: {
      idPharmacie: { id: idPharmacie },
      idTitulaire: { id },
    },
  });

  if (!userId && userTitulaires && userTitulaires.length > 0) {
    return new ApolloError(
      'Le titulaire a déjà un compte créer sur cette pharmacie',
      'ACCOUNT_ALREADY_CREATED',
    );
  }

  const titulaire = await _getTitulaire(id, ctx);

  const dateNaissance = year
    ? {
      jourNaissance: day,
      moisNaissance: month,
      anneeNaissance: year,
    }
    : {};

  return ctx.prisma
    .upsertUserTitulaire({
      where: {
        id: userTitulaire,
      },
      create: {
        idPharmacie: {
          connect: {
            id: idPharmacie,
          },
        },
        idTitulaire: {
          connect: { id },
        },
        idUser: {
          create: {
            email,
            status: 'ACTIVATION_REQUIRED',
            login,
            emailConfirmed: false,
            userName: `${titulaire.prenom.trim()} ${titulaire.nom.trim()} `,
            groupement: {
              connect: {
                id: idGroupement,
              },
            },
            ...dateNaissance,
          },
        },
      },
      update: {
        idPharmacie: {
          connect: {
            id: idPharmacie,
          },
        },
        idUser: {
          update: {
            email,
            login,
            ...dateNaissance,
          },
        },
      },
    })
    .then(async userTitulaire => {
      const user = await ctx.prisma.userTitulaire({ id: userTitulaire.id }).idUser();
      if (user && userPhoto && userPhoto.chemin) {
        await createUpdateUserPhoto(ctx, user.id, userPhoto);
      }

      if (user)
        createUpdateUserTraitement(ctx, { userId: user.id, codeTraitements, idPharmacie: null });
      await ctx.indexing.publishSave('titulaires', id);
      return _createUserRoleAndSendEmail(
        {
          email,
          idGroupement,
          userName: user.userName,
          login,
          origine: 'PHARMACIE',
          role: TITULAIRE_PHARMACIE,
        },
        ctx,
      );
    })
    .then(_ => ctx.prisma.titulaire({ id }));
};

// personnel pharmacie
export const createUserCollaborateurOfficine = async (
  {
    idGroupement,
    id,
    email,
    login,
    idPharmacie,
    role,
    userId,
    day,
    month,
    year,
    userPhoto,
    codeTraitements,
  }: {
    idGroupement: string;
    id: string;
    email: string;
    login: string;
    idPharmacie: string;
    role: string;
    userId?: string;
    day?: number;
    month?: number;
    year?: number;
    userPhoto?: FichierInput;
    codeTraitements?: string[];
  },
  ctx: Context,
) => {
  //verify if login is unique
  const existLogins = await ctx.prisma.users({ where: { login } });

  if (existLogins && existLogins.length > 0 && existLogins[0].id !== userId) {
    return new ApolloError('Login déjà utilisé par un utilisateur', 'LOGIN_ALREADY_EXIST');
  }

  const ppersonnel = await _getPpersonnel(id, ctx);

  if (!ppersonnel) {
    logger.error(`Partenaire pharmacie n'exite pas`);
    return new ApolloError("Partenaire pharmacie n'exite pas", 'PPHARMACIE_NOT_FOUND');
  }

  logger.info(`User personnel pharmacie `, {
    param: { idGroupement, id, email, login, idPharmacie },
  });

  logger.info(`Email from ${process.env.EMAIL_FROM}`);

  const dateNaissance = year
    ? {
      jourNaissance: day,
      moisNaissance: month,
      anneeNaissance: year,
    }
    : {};

  return ctx.prisma
    .updatePpersonnel({
      where: { id },
      data: {
        user: {
          upsert: {
            create: {
              email,
              login,
              status: 'ACTIVATION_REQUIRED',
              emailConfirmed: false,
              userName: `${ppersonnel.prenom.trim()} ${ppersonnel.nom.trim()}`,
              groupement: {
                connect: {
                  id: idGroupement,
                },
              },
              ...dateNaissance,
            },
            update: {
              email,
              login,
              ...dateNaissance,
            },
          },
        },
      },
    })
    .then(async pharmaPersonnel => {
      const user = await ctx.prisma.ppersonnel({ id: pharmaPersonnel.id }).user();
      if (user && userPhoto && userPhoto.chemin) {
        await createUpdateUserPhoto(ctx, user.id, userPhoto);
      }

      if (user)
        createUpdateUserTraitement(ctx, { userId: user.id, codeTraitements, idPharmacie: null });
      // publish ppersonnel
      await ctx.indexing.publishSave('ppersonnels', id);

      await _createUserRoleAndSendEmail(
        { email, userName: user.userName, login, idGroupement, origine: 'PHARMACIE', role },
        ctx,
      )
        .then(userRole => {
          logger.info(`Role de l'user ${JSON.stringify(userRole)}`);
        })
        .catch(err => new ApolloError('Erreur lors de la création user role', 'ERROR_CREATE_ROLE'));

      return pharmaPersonnel;
    })
    .catch(
      err =>
        new ApolloError(
          "Error lors de l'enregistrement de l'user personnel pharmacie",
          'ERROR_UPSERT_USER',
        ),
    );
};

export const createUserPartenaireLaboratoire = async (
  {
    idGroupement,
    id,
    email,
    login,
    userId,
    day,
    month,
    year,
    userPhoto,
    codeTraitements,
  }: {
    idGroupement: string;
    id: string;
    email: string;
    login: string;
    userId?: string;
    day?: number;
    month?: number;
    year?: number;
    userPhoto?: FichierInput;
    codeTraitements?: string[];
  },
  ctx: Context,
) => {
  //verify if login is unique
  const existLogins = await ctx.prisma.users({ where: { login } });

  if (existLogins && existLogins.length > 0 && existLogins[0].id !== userId) {
    return new ApolloError('Login déjà utilisé par un utilisateur', 'LOGIN_ALREADY_EXIST');
  }

  const labo = await ctx.prisma.laboratoire({ id });

  const dateNaissance = year
    ? {
      jourNaissance: day,
      moisNaissance: month,
      anneeNaissance: year,
    }
    : {};

  return ctx.prisma
    .updateLaboratoire({
      where: {
        id: id || '',
      },
      data: {
        user: {
          upsert: {
            create: {
              email,
              login,
              status: 'ACTIVATION_REQUIRED',
              emailConfirmed: false,
              userName: labo && labo.id ? labo.nomLabo : '',
              groupement: {
                connect: {
                  id: idGroupement,
                },
              },
              ...dateNaissance,
            },
            update: {
              email,
              login,
              ...dateNaissance,
            },
          },
        },
      },
    })
    .then(async laboratoire => {
      const user = await ctx.prisma.laboratoire({ id: laboratoire.id }).user();
      if (user && userPhoto && userPhoto.chemin) {
        await createUpdateUserPhoto(ctx, user.id, userPhoto);
      }

      if (user)
        createUpdateUserTraitement(ctx, { userId: user.id, codeTraitements, idPharmacie: null });
      await ctx.indexing.publishSave('laboratoires', id);
      return _createUserRoleAndSendEmail(
        {
          email,
          userName: user.userName,
          idGroupement,
          login,
          origine: 'PHARMACIE',
          role: PARTENAIRE_LABORATOIRE,
        },
        ctx,
      );
    })
    .then(_ => ctx.prisma.laboratoire({ id }));
};

export const createUserPartenaireService = async (
  {
    idGroupement,
    id,
    email,
    login,
    day,
    month,
    year,
    userPhoto,
    idPharmacie,
    codeTraitements,
  }: {
    idGroupement: string;
    id: string;
    email: string;
    login: string;
    day?: number;
    month?: number;
    year?: number;
    userPhoto?: FichierInput;
    idPharmacie: string;
    codeTraitements?: string[];
  },
  ctx: Context,
) => {
  //verify if login is unique
  const existLogins = await ctx.prisma.users({ where: { login } });

  if (existLogins && existLogins.length > 0 && !id) {
    return new ApolloError('Login déjà utilisé par un utilisateur', 'LOGIN_ALREADY_EXIST');
  }

  const partenaire = await _getPartenaire(id, ctx);

  const dateNaissance = year
    ? {
      jourNaissance: day,
      moisNaissance: month,
      anneeNaissance: year,
    }
    : {};

  return ctx.prisma
    .updatePartenaire({
      where: {
        id,
      },
      data: {
        user: {
          upsert: {
            create: {
              email,
              login,
              status: 'ACTIVATION_REQUIRED',
              emailConfirmed: false,
              userName: partenaire ? partenaire.nom.trim() : null,
              groupement: {
                connect: {
                  id: idGroupement,
                },
              },
              ...dateNaissance,
            },
            update: {
              email,
              login,
              ...dateNaissance,
            },
          },
        },
      },
    })
    .then(async resultPartenaire => {
      const user = await ctx.prisma.partenaire({ id: resultPartenaire.id }).user();
      if (user && userPhoto && userPhoto.chemin) {
        await createUpdateUserPhoto(ctx, user.id, userPhoto);
      }

      if (user) createUpdateUserTraitement(ctx, { userId: user.id, codeTraitements, idPharmacie });
      await ctx.indexing.publishSave('partenaires', id);
      return _createUserRoleAndSendEmail(
        {
          login,
          email,
          idGroupement,
          origine: 'PHARMACIE',
          role: PARTENAIRE_SERVICE,
          userName: user.userName,
        },
        ctx,
      );
    })
    .then(async _ =>
      ctx.prisma.partenaire({
        id,
      }),
    );
};

const _createUserRoleAndSendEmail = async (
  { email, login, userName, idGroupement, origine, role },
  ctx: Context,
) => {
  return _getUserByLogin(login, ctx).then(async user => {
    const idUserRole = await ctx.prisma
      .userRoles({ where: { idUser: { id: user.id } } })
      .then(userRoles => (userRoles && userRoles.length ? userRoles[0].id : ''));

    logger.info(`Parametre User role `, { email, idGroupement, origine, role });

    return ctx.prisma
      .upsertUserRole({
        where: {
          id: idUserRole,
        },
        create: {
          groupement: {
            connect: {
              id: idGroupement,
            },
          },
          idUserOrigin: {
            connect: {
              code: origine,
            },
          },
          idRole: {
            connect: {
              code: role,
            },
          },
          idUser: {
            connect: {
              id: user.id,
            },
          },
        },
        update: {
          idRole: {
            connect: {
              code: role,
            },
          },
        },
      })
      .then(async userRole => {
        const user = await ctx.prisma.userRole({ id: userRole.id }).idUser();
        if (user && user.id) await ctx.indexing.publishSave('users', user.id);
        logger.info(`Création avec succès du rôle de l'utilisateur avec l'email ${email} `);
        return sendCreatedUserEmail({ login, email, userName }, ctx).then(async _ => userRole);
      });
  });
};

export const sendCreatedUserEmail = async ({ login, email, userName }, ctx: Context) => {
  const url = process.env.CLIENT_URL.replace('http://', '').replace('https://', '');
  const accessToken = crypto.randomBytes(256).toString('hex');
  ctx.redis.set(accessToken, login);

  console.log(' token sent ', accessToken);
  const pharmacieSubstitutionData =
    (await _getUserPharmacieCoordonnees(ctx, {
      userId: null,
      userLogin: login,
    })) || {};

  const substitutionData = {
    app_url: url,
    token: accessToken,
    fullName: userName,
    ...pharmacieSubstitutionData,
  };

  const emailOwner = email === login;
  const templateId = emailOwner
    ? process.env.CREATE_USER_TEMPLATE_ID
    : process.env.COWORKER_CREATE_USER_TEMPLATE_ID;

  console.log('templateId :>> ', templateId);

  const emailParameters = await _getEmailParameters(ctx);
  return sendMail({ ...emailParameters, to: email, templateId, substitutionData }).catch(err => {
    console.log('err :>> ', err);

    logger.error(`Erreur envoie de sendMail`, {
      err,
      function: 'sendMail',
      source: process.env.USE_SENDGRID === 'true' ? 'sendGrid' : 'sparkPost',
    });
    return new ApolloError("Erreur lors de l'envoie d'email", 'MAIL_NOT_SENT');
  });
};

export const CheckExistingUserMail = async (email: string, ctx: Context) => {
  const oneuser = await ctx.prisma.users({ where: { email } });
  return oneuser[0];
};

const _getUserByLogin = async (login: string, ctx: Context) => {
  return ctx.prisma
    .users({ where: { login } })
    .then(users => users.find(user => user.status === 'ACTIVATED' || 'ACTIVATION_REQUIRED'));
};

const _getPersonnel = async (id: string, ctx: Context) => {
  return ctx.prisma.personnel({ id });
};

const _getTitulaire = async (id: string, ctx: Context) => {
  return ctx.prisma.titulaire({ id });
};

const _getPpersonnel = async (id: string, ctx: Context) => {
  return ctx.prisma.ppersonnel({ id });
};

const _getPartenaire = async (id: string, ctx: Context) => {
  return ctx.prisma.partenaire({ id });
};

const _checkUserTypeAndPublishIt = async (user: User, ctx: Context) => {
  const isUserPersonnel = await ctx.prisma.$exists.personnel({ user: { id: user.id } });
  const isUserPpersonnel = await ctx.prisma.$exists.ppersonnel({ user: { id: user.id } });
  const isUserTitulaire = await ctx.prisma.$exists.userTitulaire({ idUser: { id: user.id } });
  const isUserLabo = await ctx.prisma.$exists.laboratoire({ user: { id: user.id } });
  const isUserPartenaire = await ctx.prisma.$exists.partenaire({ user: { id: user.id } });

  if (isUserPersonnel) {
    await ctx.prisma
      .personnels({
        where: { user: { id: user.id } },
      })
      .then(userPersonnels =>
        userPersonnels && userPersonnels.length
          ? ctx.indexing.publishSave('personnels', userPersonnels[0].id)
          : null,
      );
  }

  if (isUserPpersonnel) {
    await ctx.prisma
      .ppersonnels({
        where: { user: { id: user.id } },
      })
      .then(userPpersonnels =>
        userPpersonnels && userPpersonnels.length
          ? ctx.indexing.publishSave('ppersonnels', userPpersonnels[0].id)
          : null,
      );
  }

  if (isUserTitulaire) {
    await ctx.prisma
      .userTitulaires({
        where: { idUser: { id: user.id } },
      })
      .then(userTitulaires =>
        userTitulaires && userTitulaires.length
          ? ctx.prisma
            .userTitulaire({
              id: userTitulaires[0].id,
            })
            .idTitulaire()
            .then(async titulaire => ctx.indexing.publishSave('titulaires', titulaire.id))
          : null,
      );
  }

  if (isUserLabo) {
    await ctx.prisma
      .laboratoires({
        where: { user: { id: user.id } },
      })
      .then(userLaboratoires =>
        userLaboratoires && userLaboratoires.length
          ? ctx.indexing.publishSave('laboratoires', userLaboratoires[0].id)
          : null,
      );
  }

  if (isUserPartenaire) {
    await ctx.prisma
      .partenaires({
        where: { user: { id: user.id } },
      })
      .then(userPartenaires =>
        userPartenaires && userPartenaires.length
          ? ctx.indexing.publishSave('partenaires', userPartenaires[0].id)
          : null,
      );
  }

  return user;
};

export const getUserByGroupement = async (ctx: Context, parent: User) => {
  return ctx.prisma
    .users({ where: { id: parent.id || parent['_id'], groupement: { id: ctx.groupementId } } })
    .then(users => (users && users.length ? users[0] : null));
};

export const deleteUserPhotos = async (userId: string, ctx: Context) => {
  const userPhotos = await ctx.prisma.userPhotos({ where: { idUser: { id: userId } } });
  const defaults: Fichier[] = [];
  const fichiers = await userPhotos.reduce(async (promiseResults, userPhoto) => {
    const fichier = await ctx.prisma.userPhoto({ id: userPhoto.id }).idFichier();
    const avatar = fichier ? await ctx.prisma.fichier({ id: fichier.id }).avatar() : null;
    const results = await promiseResults;
    if (fichier && !avatar) return [...results, fichier];
    return [...results];
  }, Promise.resolve(defaults));

  return ctx.prisma.deleteManyUserPhotos({ idUser: { id: userId } }).then(async _ => {
    logger.info(`user Photo ==> ${userPhotos}`);
    if (fichiers && fichiers.length) {
      return ctx.prisma.deleteManyFichiers({ id_in: fichiers.map(fichier => fichier.id) });
    }
    return fichiers;
  });
};

export const usersPharmacie = async (ctx: Context, { idPharmacie }, inOther: boolean = false) => {
  let whereTitulaire: any = { idPharmacie: { id: idPharmacie } };
  let wherePpersonnel: any = {
    idPharmacie: { id: idPharmacie },
    user: {
      id_not: null || undefined || '',
    },
  };

  if (inOther) {
    whereTitulaire = {
      ...whereTitulaire,
      idPharmacie: { id_not: idPharmacie },
      idUser: { groupement: { id: ctx.groupementId } },
    };
    wherePpersonnel = {
      ...wherePpersonnel,
      idPharmacie: { id_not: idPharmacie },
      groupement: { id: ctx.groupementId },
    };
  }

  const userTitulaires = await ctx.prisma.userTitulaires({
    where: whereTitulaire,
  });

  const ppersonnels = await ctx.prisma.ppersonnels({
    where: wherePpersonnel,
  });

  const titulaireUsers =
    userTitulaires && userTitulaires.length
      ? await Promise.all(
        userTitulaires.map(userTitus => ctx.prisma.userTitulaire({ id: userTitus.id }).idUser()),
      )
      : [];

  const ppharmacieUsers =
    ppersonnels && ppersonnels.length
      ? await Promise.all(ppersonnels.map(ppers => ctx.prisma.ppersonnel({ id: ppers.id }).user()))
      : [];

  return [...titulaireUsers, ...ppharmacieUsers].filter(user => user);
};

export const usersByRole = async (ctx: Context, code: string) => {
  const userRoles = await ctx.prisma.userRoles({
    where: { idRole: { code } },
  });
  const users =
    userRoles && userRoles.length
      ? await Promise.all(
        userRoles.map(userRole => ctx.prisma.userRole({ id: userRole.id }).idUser()),
      )
      : [];

  return users;
};

export const userRole = async (ctx: Context, { id }) => {
  const userRoles = await ctx.prisma.userRoles({ where: { idUser: { id } } });
  return userRoles && userRoles.length
    ? ctx.prisma.userRole({ id: userRoles[0].id }).idRole()
    : null;
};

export const UsersGroupement = async (ctx: Context, { idGroupement }) => {
  const userSuperAdmin = await usersByRole(ctx, SUPER_ADMINISTRATEUR);
  return ctx.prisma.users({ where: { groupement: { id: idGroupement } } }).then(users => {
    // tslint:disable-next-line: no-unused-expression
    userSuperAdmin && users.push(...userSuperAdmin);
    const usersGroup = users.filter(user => user !== null);
    return usersGroup;
  });
};

export const usersPersonnelGroupement = async (
  ctx: Context,
  { idService, idGroupement }: { idService?: string; idGroupement: string },
) => {
  let whereGroupement: any = {
    groupement: { id: idGroupement },
    user: {
      id_not: null || undefined || '',
    },
  };
  if (idService) {
    whereGroupement = { ...whereGroupement, service: { id: idService } };
  }
  const personnels = await ctx.prisma.personnels({
    where: whereGroupement,
  });

  return personnels && personnels.length
    ? (
      await Promise.all(
        personnels.map(personnel => ctx.prisma.personnel({ id: personnel.id }).user()),
      )
    ).filter(item => item)
    : [];
};

export const usersRegions = async (ctx: Context, { idPharmacie }: { idPharmacie: string }) => {
  const departement = await ctx.prisma.pharmacie({ id: idPharmacie }).departement();

  const region =
    departement && departement.id
      ? await ctx.prisma.departement({ id: departement.id }).region()
      : null;

  const pharmaciesRegion =
    region && region.id
      ? await ctx.prisma.pharmacies({
        where: {
          departement: {
            region: {
              id: region.id,
            },
          },
        },
      })
      : [];

  if (pharmaciesRegion && !pharmaciesRegion.length) return [];

  const userTitulaires = await ctx.prisma.userTitulaires({
    where: {
      idPharmacie: {
        id_in: pharmaciesRegion.map(pharma => pharma.id),
      },
    },
  });

  const ppersonnels = await ctx.prisma.ppersonnels({
    where: {
      idPharmacie: {
        id_in: pharmaciesRegion.map(pharma => pharma.id),
      },
    },
  });

  const titulaireUsers =
    userTitulaires && userTitulaires.length
      ? await Promise.all(
        userTitulaires.map(userTitus => ctx.prisma.userTitulaire({ id: userTitus.id }).idUser()),
      )
      : [];

  const ppharmacieUsers =
    ppersonnels && ppersonnels.length
      ? await Promise.all(ppersonnels.map(ppers => ctx.prisma.ppersonnel({ id: ppers.id }).user()))
      : [];

  return [...titulaireUsers, ...ppharmacieUsers].filter(user => user);
};

export const usersLaboratoires = async (ctx: Context) => {
  const laboratoires = await ctx.prisma.laboratoires({
    where: {
      user: {
        id_not: null || undefined || '',
      },
    },
  });

  return laboratoires && laboratoires.length
    ? (
      await Promise.all(
        laboratoires.map(laboratoire => ctx.prisma.laboratoire({ id: laboratoire.id }).user()),
      )
    ).filter(item => item)
    : [];
};

export const usersPartenaires = async (ctx: Context) => {
  const partenaires = await ctx.prisma.partenaires({
    where: {
      user: {
        id_not: null || undefined || '',
      },
    },
  });

  return partenaires && partenaires.length
    ? (
      await Promise.all(
        partenaires.map(partenaire => ctx.prisma.partenaire({ id: partenaire.id }).user()),
      )
    ).filter(item => item)
    : [];
};

export const createUpdateUserPhoto = async (
  ctx: Context,
  userId: string,
  fichier: FichierInput,
) => {
  if (fichier && fichier.idAvatar) {
    const avatarFichier = await ctx.prisma
      .fichiers({
        where: { avatar: { id: fichier.idAvatar } },
      })
      .then(avatarFichiers => (avatarFichiers && avatarFichiers.length ? avatarFichiers[0] : null));

    if (!avatarFichier) return null;
    const userPhoto = await deleteUserPhotos(userId, ctx).then(async _ =>
      ctx.prisma.createUserPhoto({
        idFichier: {
          connect: {
            id: avatarFichier.id,
          },
        },
        idUser: { connect: { id: userId } },
      }),
    );
    await ctx.indexing.publishSave('users', userId);
    return userPhoto;
  }

  return deleteUserPhotos(userId, ctx).then(async _ => {
    const userPhoto = await ctx.prisma.createUserPhoto({
      idFichier: {
        create: {
          nomOriginal: fichier.nomOriginal,
          chemin: fichier.chemin,
          type: fichier.type,
        },
      },
      idUser: { connect: { id: userId } },
    });
    /**
     * Sauvegarde activite de l'utilisateur
     */
    createActiviteUser(
      {
        code: 'UUSERPHOTO',
        nom: 'UPDATE USER PHOTO',
      },
      ctx,
    );
    await ctx.indexing.publishSave('users', userId);
    return userPhoto;
  });
};

export const createUpdateUserTraitement = async (
  ctx: Context,
  {
    userId,
    codeTraitements,
    idPharmacie,
  }: { userId: string; codeTraitements?: string[]; idPharmacie: string | null },
) => {
  const existUser = await ctx.prisma.$exists.user({ id: userId });
  if (!existUser) return new ApolloError(`L'utilisateur n'existe pas`, 'ERROR_USER_NOT_FOUND');

  if (idPharmacie) {
    await ctx.prisma.deleteManyUserTraitements({
      user: { id: userId },
      pharmacie: { id: idPharmacie },
    });
  } else {
    await ctx.prisma.deleteManyUserTraitements({ user: { id: userId } });
  }

  if (!codeTraitements || (codeTraitements && !codeTraitements.length)) {
    await ctx.indexing.publishSave('users', userId);
    return ctx.prisma.user({ id: userId });
  }

  await Promise.all(
    codeTraitements.map(async codeTraitement => {
      const existTraitement = await ctx.prisma.$exists.traitement({ code: codeTraitement });
      console.log(`exist ${codeTraitement}: ${existTraitement}`);

      return existTraitement
        ? ctx.prisma
          .createUserTraitement({
            user: { connect: { id: userId } },
            access: true,
            traitement: { connect: { code: codeTraitement } },
            pharmacie: { connect: { id: idPharmacie } },
          })
          .then(async created => {
            const traitement = await ctx.prisma.traitement({ code: codeTraitement });
            await ctx.indexing.publishSave('traitements', traitement.id);
            return created;
          })
        : null;
    }),
  );
  return ctx.prisma.user({ id: userId }).then(async user => {
    await ctx.indexing.publishSave('users', user.id);
    return user;
  });
};

export const userCodeTraitementsByRole = async (
  ctx: Context,
  {
    idUser,
    codeRole,
    idPharmacie,
  }: { idUser: string; codeRole: string; idPharmacie: string | null },
) => {
  const roleTraitements = await ctx.prisma.roleTraitements({
    where: { role: { code: codeRole } },
  });

  // console.log('roleTraitements :>> ', roleTraitements);

  if (!roleTraitements || (roleTraitements && !roleTraitements.length)) return [];

  const traitements = await Promise.all(
    roleTraitements.map(roleTraitement =>
      ctx.prisma.roleTraitement({ id: roleTraitement.id }).traitement(),
    ),
  );

  // console.log('traitements :>> ', traitements);

  if (!traitements || (traitements && !traitements.length)) return [];

  const userTraitements = idPharmacie
    ? await ctx.prisma.userTraitements({
      where: {
        user: { id: idUser },
        traitement: { id_in: traitements.map(traitement => traitement.id) },
        pharmacie: { id: idPharmacie },
      },
    })
    : await ctx.prisma.userTraitements({
      where: {
        user: { id: idUser },
        traitement: { id_in: traitements.map(traitement => traitement.id) },
      },
    });

  // console.log('userTraitements :>> ', userTraitements);

  return userTraitements && userTraitements.length
    ? Promise.all(
      userTraitements.map(async userTraitement => {
        const code = await ctx.prisma
          .userTraitement({ id: userTraitement.id })
          .traitement()
          .code();
        return code;
      }),
    )
    : traitements.map(traitement => traitement.code);
};

export const getUserPpersonnelsByPharmacie = async (ctx: Context, idPharmacies: string[]) => {
  const ppersonnels = await ctx.prisma.ppersonnels({
    where: {
      idPharmacie: {
        id_in: idPharmacies,
      },
      user: {
        id_not: null,
      },
    },
  });

  return ppersonnels && ppersonnels.length
    ? (
      await Promise.all(
        ppersonnels.map(pperso => ctx.prisma.ppersonnel({ id: pperso.id }).user()),
      )
    ).filter(item => item)
    : [];
};

const _getEmailParameters = async (
  ctx: Context,
): Promise<{ from: string; cc?: string; bcc?: string }> => {
  const EMAIL_FROM_CODE: string = '0400';
  const EMAIL_CC_CODE: string = '0401';
  const EMAIL_BCC_CODE: string = '0402';

  return Promise.all([
    ParameterGroupementByCode(ctx, { idGroupement: ctx.groupementId, code: EMAIL_FROM_CODE }),
    ParameterGroupementByCode(ctx, { idGroupement: ctx.groupementId, code: EMAIL_CC_CODE }),
    ParameterGroupementByCode(ctx, { idGroupement: ctx.groupementId, code: EMAIL_BCC_CODE }),
  ])
    .then(([from, cc, bcc]) => {
      return { from: from?.value || process.env.EMAIL_FROM, cc: cc?.value, bcc: bcc?.value };
    })
    .catch(error => {
      return { from: process.env.EMAIL_FROM };
    });
};

const _getUserPharmacieCoordonnees = async (
  ctx: Context,
  { userLogin, userId },
): Promise<
  { nomPharmacie: string; adresse1Pharmacie: string; adresse2Pharmacie?: string } | undefined
> => {
  let pharmacie;
  let currentUserId = userId;

  if (!currentUserId && userLogin) {
    const user = await ctx.prisma
      .users({ where: { login: userLogin } })
      .then(users => (users && users.length ? users[0] : null));

    if (user) {
      currentUserId = user.id;
    }
  }

  if (currentUserId) {
    const pharmacieTitulaire = await pharmacieUserTitulaire(ctx, currentUserId);
    if (pharmacieTitulaire) {
      pharmacie = pharmacieTitulaire;
    } else {
      const pharmaciePpersonnel = await pharmacieUserPpersonnel(ctx, currentUserId);
      if (pharmaciePpersonnel) {
        pharmacie = pharmaciePpersonnel;
      }
    }
  }

  return pharmacie
    ? {
      nomPharmacie: pharmacie.nom,
      adresse1Pharmacie: pharmacie.adresse1,
      adresse2Pharmacie: pharmacie.adresse2,
    }
    : undefined;
};

export const cronUpateUserStatus = async (prisma: Prisma) => {
  const param = await prisma.parameter({ code: '0322' });
  const paramDefaultValue = param && param.defaultValue ? parseInt(param.defaultValue, 10) : 390;
  const paramValue = await prisma
    .parameterValues({
      where: { idParameter: { id: param && param.id } },
    })
    .then(results => {
      if (results && results.length > 0) {
        const res = results[0];
        return res.value ? parseInt(res.value, 10) : 0;
      }
    });

  const value = paramValue > 0 ? paramValue : paramDefaultValue;

  return cronUpdateUsersStatus(prisma, 0, 100, value);
};

const cronUpdateUsersStatus = async (
  prisma: Prisma,
  skip: number,
  first: number,
  value: number,
) => {
  return prisma.users({ skip, first }).then(async list => {
    if (!list || (list && !list.length)) return;

    logger.log('user login last date', list);

    await Promise.all(
      list.map(user => {
        if (user.lastLoginDate) {
          const now = moment();
          const lastLoginDate = moment(user.lastLoginDate);
          if (now.diff(lastLoginDate, 'days') > value) {
            logger.log('UPDATE USER TO RESET ', user.id);

            return prisma.updateUser({ where: { id: user.id }, data: { status: 'RESETED' } });
          }
        }
      }),
    );

    if (list.length >= first) {
      return cronUpdateUsersStatus(prisma, skip + first, first, value);
    }

    return;
  });
};

export const getUserPharmacie = async (idUser: string, ctx: Context) => {
  const pharmacieTitulaire = await pharmacieUserTitulaire(ctx, idUser);
  return pharmacieTitulaire || (await pharmacieUserPpersonnel(ctx, idUser));
};

export const getUserContact = async (idUser: string, ctx: Context) => {
  let currentUser: any = await userPpersonnelContact(ctx, idUser);
  if (currentUser) {
    return currentUser;
  }
  currentUser = await userTitulaireContact(ctx, idUser);
  if (currentUser) {
    return currentUser;
  }

  const clients = await ctx.prisma.clients({ where: { idUser } });
  if (clients.length > 0) {
    const contacts = await ctx.prisma.contacts({ where: { id: clients[0].idContact } });
    return contacts[0];
  }

  return null;
};
