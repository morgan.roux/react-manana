import { Context } from '../types';

/**
 * EXEMPLE QUERY RAW
 * const result: { id: string }[] = await queryRaw(
 *    ctx,
 *   `SELECT id FROM  ${DATABASE_SCHEMA}."User"`,
 * );
 */

export const queryRaw = async (ctx: Context, query: string) => {
  const response = await ctx.prisma.$graphql(`
    mutation {
    executeRaw(query: "${query
      .replace(/"/g, '\\"')
      .replace(/\n/g, ' ')
      .replace(/\s+/g, ' ')}")
    }
`);
  console.log('result raws', response && response.executeRaw ? response.executeRaw : []);

  return response && response.executeRaw ? response.executeRaw : [];
};
