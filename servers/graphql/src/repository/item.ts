import { Context } from '../types';
import { parameterItemCode } from '../constants/item';

export const publishItemSource = async (code: string, idSource: string, ctx: Context) => {
  switch (code) {
    // Produit
    case 'PROD':
      await ctx.prisma
        .produitCanal({
          id: idSource,
        })
        .then(article => ctx.indexing.publishSave('produitcanals', article.id));
      break;
    case 'LABO':
      await ctx.prisma
        .laboratoire({ id: idSource })
        .then(laboratoire => ctx.indexing.publishSave('laboratoires', laboratoire.id));
      break;
    case 'ACTUALITE':
      await ctx.prisma
        .actualite({ id: idSource })
        .then(actualite => ctx.indexing.publishSave('actualites', actualite.id));
      break;
    // Operation commercial
    case 'OPERACOM':
    case 'ACCORDCOM':
      await ctx.prisma
        .operation({ id: idSource })
        .then(operation => ctx.indexing.publishSave('operations', operation.id));
      break;
    case 'TODO':
      await ctx.prisma
        .action({ id: idSource })
        .then(action => ctx.indexing.publishSave('actions', action.id));
      break;
    case 'IDEE_AND_BEST_PRACTICE':
      await ctx.prisma
        .ideeOuBonnePratique({ id: idSource })
        .then(res => ctx.indexing.publishSave('ideeoubonnepratiques', res.id));
      break;
    // TODO : case others items
    default:
      break;
  }
};

export const fichierImageItem = async (ctx: Context, { code }) => {
  if (code) return null;
  const codeParameter = parameterItemCode[code];
  const paramValue = await ctx.prisma
    .parameterValues({
      where: { idParameter: { code: codeParameter }, idSource: ctx.groupementId },
    })
    .then(paramVals => (paramVals && paramVals.length ? paramVals[0] : null));

  if (!paramValue) {
    const parameter = await ctx.prisma.parameter({ code: codeParameter });

    return parameter && parameter.defaultValue
      ? ctx.prisma.fichier({ id: parameter.defaultValue })
      : null;
  }

  return paramValue.value ? ctx.prisma.fichier({ id: paramValue.value }) : null;
};

export const sousItems = async (ctx: Context, { codeItem }) => {
  if (!codeItem) {
    return (await ctx.prisma.items()).filter(item => item.codeItem.length === 1);
  }
  return (
    await ctx.prisma.items({
      where: { codeItem_starts_with: codeItem },
    })
  ).filter(item => item.codeItem.length === codeItem.length + 1);
};
