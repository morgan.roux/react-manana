import { Context } from '../types';
import moment from 'moment';
import { Region } from '../generated/prisma-client';
const now = moment().format();

export const regionsFromPresidents = async (ctx: Context, { presidentsIDs }) => {
  const idPresidents = await presidentsIDs.reduce(async (result, id) => {
    const titulaire = await ctx.prisma.titulaire({ id });
    const ids = await result;
    if (titulaire && titulaire.id) return [...ids, titulaire.id];
    return [...ids];
  }, []);

  if (idPresidents && idPresidents.length) {
    const idDepartements = await idPresidents.reduce(async (result, id) => {
      const affectations = await ctx.prisma.titulaireAffectations({
        where: { titulaire: { id }, dateDebut_lte: now, dateFin_gte: now },
      });
      const ids = await result;
      if (affectations && affectations.length) {
        const departement = await ctx.prisma
          .titulaireAffectation({ id: affectations[0].id })
          .departement();
        return [...ids, departement.id];
      }
      return [...ids];
    }, []);

    if (idDepartements && idDepartements.length) {
      return idDepartements.reduce(async (result: Region[], id: string): Promise<Region[]> => {
        const region = await ctx.prisma.departement({ id }).region();
        const regions = result;
        if (region && region.id && !regions.includes(region)) {
          return [...regions, region];
        }
        return [...regions];
      }, []);
    }

    return [];
  }

  return [];
};
