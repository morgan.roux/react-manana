import { Context } from '../types';

export const sousGammeCommercials = async (ctx: Context, id: number) => {
  return ctx.prisma.canalSousGammes({
    where: { canalGamme: { id } },
  });
};
