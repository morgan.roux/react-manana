import { TodoProjetFavorisCreateInput } from '../generated/prisma-client';
import { Context, TodoProjetFavorisInput } from '../types';

export const createUpdateTodoProjetFavoris = async (
  input: TodoProjetFavorisInput,
  ctx: Context,
) => {
  const { id, idProject, idUser, ordre, codeMaj, isRemoved } = input;

  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;

  const data: TodoProjetFavorisCreateInput = {
    project: idProject ? { connect: { id: idProject } } : null,
    user: idUser ? { connect: { id: idUser } } : null,
    ordre,
    codeMaj,
    isRemoved,
    userCreation,
    userModification,
  };

  const todoProjetFavoris = await ctx.prisma.upsertTodoProjetFavoris({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  // Publish to elasticsearch
  // await ctx.indexing.publishSave('todoprojetfavoris', todoProjetFavoris.id);
  return todoProjetFavoris;
};

export const softDeleteTodoProjetFavoris = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateTodoProjetFavoris({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      // await ctx.indexing.publishSave('todoprojetfavoris', res.id);
      return res;
    });
};
