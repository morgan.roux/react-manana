import { Context, ArticleCommande } from '../types';
import { createCsv } from '../services/csv';
import moment from 'moment';
import {
  Commande,
  CommandeLigne,
  Remise,
  RemiseDetail,
  ProduitCanal,
} from '../generated/prisma-client';
import {
  getRemise,
  articleSamePanachees,
  getArticlePanacheesInCommande,
  setRemiseSamePanachees,
  RemiseResult,
  getRemisePanacheeDetails,
  remiseLigneDetails,
} from './remise';
import { quantiteStv } from './panier';
import { ApolloError } from 'apollo-server';
import { ATTENTE } from '../constants/status';

export const createCommande = async (
  ctx: Context,
  {
    id /* ==> id panier */,
    idPharmacie,
    idOperation,
    commentaireInterne,
    commentaireExterne,
    articlesCommande,
    codeCanalCommande,
    idPublicite,
  },
) => {
  const now = moment().format();
  const commande = await ctx.prisma
    .createCommandeReference({
      idCommande: {
        create: {
          idPharmacie: {
            connect: {
              id: idPharmacie,
            },
          },
          idUser: {
            connect: {
              id: ctx.userId,
            },
          },
          idOperation: idOperation
            ? {
                connect: { id: idOperation },
              }
            : null,
          groupement: { connect: { id: ctx.groupementId } },
          dateCommande: now,
          commentaireInterne,
          commentaireExterne,
          idPublicite: idPublicite
            ? {
                connect: {
                  id: idPublicite,
                },
              }
            : null,
        },
      },
    })
    .then(async referenceCreated => {
      if (!referenceCreated) {
        return new ApolloError(
          `Erreur lors de la création du commande sur l'operation`,
          'ERROR_CREATE_COMMANDE',
        );
      }

      const created = await ctx.prisma.commandeReference({ id: referenceCreated.id }).idCommande();

      await ctx.prisma.createCommandeStatutStatut({
        idCommande: { connect: { id: created.id } },
        idCommandeStatut: { connect: { code: ATTENTE } },
      });

      if (id) {
        /* Commande from panier */
        await _updateCommandeFromPanier(ctx, {
          id,
          idCommande: created.id,
        }).then(_ => _addCommandeLignes(ctx, created, id, codeCanalCommande));
      } else if (idOperation && articlesCommande) {
        /* Commande from operation */
        /* const pflRemise = await ctx.prisma.operation({ id: idOperation }).remise(); */
        const canal = await ctx.prisma.operation({ id: idOperation }).idCommandeCanal();

        await ctx.prisma.updateCommande({
          data: { idCommandeCanal: { connect: { id: canal.id } } },
          where: { id: created.id },
        });

        await _addCommandeLigneOperation(ctx, created, articlesCommande, {
          idPharmacie,
        });
      }
      return created;
    });

  await ctx.indexing.publishSave('commandes', commande.id);

  if (idOperation) await ctx.indexing.publishSave('operations', idOperation);

  if (idPublicite) {
    const suivis = await ctx.prisma.suiviPublicitaires({
      where: { idPublicite: { id: idPublicite } },
    });
    if (suivis && suivis.length) {
      await ctx.prisma.updateSuiviPublicitaire({
        where: { id: suivis[0].id },
        data: {
          nbCommande: suivis[0].nbCommande + 1,
        },
      });
      await ctx.indexing.publishSave('suivipublicitaires', suivis[0].id);
    }
  }

  return commande;
};

/* const _addCommandeLigneOperationSpecialRemise = async (
  ctx: Context,
  commande: Commande,
  articlesCommande: ArticleCommande[],
  pflRemise: Remise,
) => {
  const qteTotalCommande = articlesCommande
    .map(articleCmd => articleCmd.quantite)
    .reduce((sum, qte) => sum + qte);
  const remiseDetails = await ctx.prisma.remiseDetails({
    where: {
      remise: { id: pflRemise.id },
      quantiteMin_lte: qteTotalCommande,
      quantiteMax_gte: qteTotalCommande,
    },
  });

  const createdLignes: CommandeLigne[] = await Promise.all(
    articlesCommande.map(async article => {
      const canalArticle = await ctx.prisma.canalArticle({ id: article.id });
      const quantite = await quantiteStv(ctx, {
        standard: canalArticle.stv,
        quantite: article.quantite,
      });

      const prixNetUnitaireHT =
        (canalArticle.prixPhv * (100 - remiseDetails[remiseDetails.length - 1].pourcentageRemise)) /
        100;
      const newLigne = await ctx.prisma.createCommandeLigne({
        canalArticle: { connect: { id: article.id } },
        idCommande: { connect: { id: commande.id } },
        optimiser: 0,
        quantiteCdee: quantite,
        prixBaseHT: canalArticle.prixPhv,
        prixNetUnitaireHT,
        remiseGamme: remiseDetails[remiseDetails.length - 1].pourcentageRemise,
        prixTotalHT: prixNetUnitaireHT * quantite,
      });
      return newLigne;
    }),
  );

  await _setUniteGratuitPanachees(
    ctx,
    {
      remise: 0,
      articleSamePanachees: [],
      ug: remiseDetails[remiseDetails.length - 1].nombreUg,
      canalArticleUG: await ctx.prisma
        .remiseDetail({
          id: remiseDetails[remiseDetails.length - 1].id,
        })
        .canalArticleUg(),
      isPalier: false,
    },
    { idCommande: commande.id },
  );

  const lignes = await ctx.prisma.commandeLignes({
    where: { id_in: createdLignes.map(created => created.id) },
  });

  const { prixTotalBaseQuantite, prixTotalNetQuantite } = calculPrix(lignes);
  const valeurRemiseTotal = prixTotalBaseQuantite - prixTotalNetQuantite;
  const remiseGlobale = (valeurRemiseTotal * 100) / prixTotalBaseQuantite;
  const nbrRef =
    lignes && lignes.length
      ? lignes.map(ligne => ligne.quantiteCdee).reduce((sum, qte) => sum + qte)
      : 0;

  return ctx.prisma.updateCommande({
    data: {
      nbrRef,
      prixBaseTotalHT: prixTotalBaseQuantite,
      valeurRemiseTotal,
      prixNetTotalHT: prixTotalNetQuantite,
      remiseGlobale,
      uniteGratuite:
        remiseDetails && remiseDetails.length
          ? remiseDetails[remiseDetails.length - 1].nombreUg
          : 0,
    },
    where: { id: commande.id },
  });
}; */

const _updateCommandeFromPanier = async (ctx: Context, { id, idCommande }) => {
  return ctx.prisma.panier({ id }).then(async panier => {
    const {
      nbrRef,
      quantite,
      uniteGratuite,
      prixBaseTotalHT,
      valeurRemiseTotal,
      prixNetTotalHT,
      remiseGlobale,
    } = panier;

    const canal = await ctx.prisma.panier({ id }).idCommandeCanal();

    return ctx.prisma.updateCommande({
      data: {
        nbrRef,
        quantite,
        uniteGratuite,
        prixBaseTotalHT,
        valeurRemiseTotal,
        prixNetTotalHT,
        fraisPort: 0,
        valeurFrancoPort: 0,
        remiseGlobale,
        idCommandeCanal: canal ? { connect: { id: canal.id } } : null,
      },
      where: { id: idCommande },
    });
  });
};

export const exportCommandeLigne = async (ctx: Context, { id }) => {
  const commande = await ctx.prisma.commande({ id });
  const user = await ctx.prisma.commande({ id }).idUser();
  const commandeLignes = await ctx.prisma.commandeLignes({
    where: { idCommande: { id } },
  });
  const lignes = commandeLignes.map(ligne => {
    return {
      uniteGratuite: ligne.uniteGratuite,
      remiseLigne: ligne.remiseLigne,
      remiseGamme: ligne.remiseGamme,
      quantiteLivree: ligne.quantiteLivree,
      quantiteCdee: ligne.quantiteCdee,
      prixBaseHT: ligne.prixBaseHT,
      prixNetUnitaireHT: ligne.prixNetUnitaireHT,
      prixTotalHT: ligne.prixTotalHT,
    };
  });
  const header = [
    { id: 'uniteGratuite', title: 'Unite Gratuite' },
    { id: 'remiseLigne', title: 'Remise ligne' },
    { id: 'remiseGamme', title: 'Remise Gamme' },
    { id: 'quantiteLivree', title: 'Quantité livrées' },
    { id: 'quantiteCdee', title: 'Quantité commandées' },
    { id: 'prixBaseHT', title: 'Prix base HT' },
    { id: 'prixNetUnitaireHT', title: 'Prix Net Unitaire HT' },
    { id: 'prixTotalHT', title: 'PrixTotalHT' },
  ];
  const dateCommande = moment(commande.dateCommande).format('YYYY-MM-DD-HH:mm');
  const file = createCsv(
    header,
    lignes,
    `${commande.id}_${dateCommande}.csv`,
    `commandes/${user.id}`,
  );
  await ctx.prisma.updateCommande({ data: { pathFile: file }, where: { id } });
  return commande;
};

const _addCommandeLigneOperation = async (
  ctx: Context,
  commande: Commande,
  articlesCommande: ArticleCommande[],
  { idPharmacie },
) => {
  /* Create default ligne commande */
  const createdLignes: CommandeLigne[] = await Promise.all(
    articlesCommande.map(async article => {
      const canalArticle = await ctx.prisma.produitCanal({ id: article.id });
      const quantite = await quantiteStv(ctx, {
        standard: canalArticle.stv,
        quantite: article.quantite,
      });
      const newLigne = await ctx.prisma.createCommandeLigne({
        produitCanal: { connect: { id: article.id } },
        idCommande: { connect: { id: commande.id } },
        optimiser: 0,
        quantiteCdee: quantite,
        prixBaseHT: canalArticle.prixPhv,
      });
      return newLigne;
    }),
  );

  /* Update ligne */
  const lignes: CommandeLigne[] = await createdLignes.reduce(async (promiseLines, ligne): Promise<
    CommandeLigne[]
  > => {
    const article = await ctx.prisma.commandeLigne({ id: ligne.id }).produitCanal();
    const codeCanal = await ctx.prisma
      .commandeLigne({ id: ligne.id })
      .produitCanal()
      .canal()
      .code();

    const currentRemise = await getRemise(ctx, {
      idPharmacie,
      idCanalArticle: article.id,
      codeCanal,
      quantite: ligne.quantiteCdee,
      idPanier: null,
      idCommande: commande.id,
    });

    const prixNetUnitaireHT = (ligne.prixBaseHT * (100 - currentRemise.remise)) / 100;
    const updatedLigne = await ctx.prisma.updateCommandeLigne({
      data: {
        prixNetUnitaireHT,
        prixTotalHT: prixNetUnitaireHT * ligne.quantiteCdee,
        remiseLigne: currentRemise.isPalier ? currentRemise.remise : 0,
        remiseGamme: !currentRemise.isPalier ? currentRemise.remise : 0,
        quantiteLivree: ligne.quantiteCdee,
      },
      where: { id: ligne.id },
    });

    if (
      !currentRemise.isPalier &&
      currentRemise.articleSamePanachees &&
      currentRemise.articleSamePanachees.length
    ) {
      await setRemiseSamePanachees(
        ctx,
        currentRemise.articleSamePanachees,
        currentRemise.remise,
        false,
      );
    }

    await _setUniteGratuiteCommande(ctx, currentRemise, updatedLigne, {
      idCommande: commande.id,
      idPharmacie,
    });

    const previousLines = await promiseLines;
    return ctx.prisma.commandeLigne({ id: updatedLigne.id }).then(async updated => {
      return [...previousLines, updated];
    });
  }, Promise.resolve([]));

  const { prixTotalBaseQuantite, prixTotalNetQuantite } = calculPrix(lignes);
  const valeurRemiseTotal = prixTotalBaseQuantite - prixTotalNetQuantite;
  const remiseGlobale = (valeurRemiseTotal * 100) / prixTotalBaseQuantite;
  const nbrRef =
    lignes && lignes.length
      ? lignes.map(ligne => ligne.quantiteCdee).reduce((sum, qte) => sum + qte)
      : 0;
  return ctx.prisma.updateCommande({
    data: {
      nbrRef,
      prixBaseTotalHT: prixTotalBaseQuantite,
      valeurRemiseTotal,
      prixNetTotalHT: prixTotalNetQuantite,
      remiseGlobale,
      uniteGratuite:
        lignes && lignes.length
          ? lignes.map(ligne => ligne.uniteGratuite).reduce((sum, qte) => sum + qte)
          : 0,
    },
    where: { id: commande.id },
  });
};

const _setUniteGratuiteCommande = async (
  ctx: Context,
  currentRemise: RemiseResult,
  ligne: CommandeLigne,
  { idCommande, idPharmacie },
) => {
  if (currentRemise.isPalier === false) {
    await _resetUniteGratuite(ctx, { id: ligne.id, idCommande, idPharmacie });
    if (currentRemise.canalArticleUG) {
      await _setUniteGratuitPanachees(ctx, currentRemise, { idCommande });
    }
  } else {
    if (currentRemise.ug !== 0) {
      await ctx.prisma.updateCommandeLigne({
        data: {
          uniteGratuite: currentRemise.ug,
          quantiteLivree: ligne.quantiteCdee + currentRemise.ug,
        },
        where: { id: ligne.id },
      });
    }
  }
};

const _setUniteGratuitPanachees = async (
  ctx: Context,
  currentRemise: RemiseResult,
  { idCommande },
) => {
  if (!currentRemise.canalArticleUG) return false;

  const ligne = await ctx.prisma
    .commande({ id: idCommande })
    .then(async commande =>
      ctx.prisma.commandeLignes({
        where: {
          idCommande: { id: commande.id },
          produitCanal: { id: currentRemise.canalArticleUG.id },
        },
      }),
    )
    .then(commandeLigne => commandeLigne[commandeLigne.length - 1]);

  if (ligne) {
    await ctx.prisma.updateCommandeLigne({
      data: {
        uniteGratuite: currentRemise.ug,
        quantiteLivree: ligne.quantiteCdee + currentRemise.ug,
      },
      where: { id: ligne.id },
    });
  } else {
    await ctx.prisma.createCommandeLigne({
      idCommande: {
        connect: {
          id: idCommande,
        },
      },
      produitCanal: {
        connect: {
          id: currentRemise.canalArticleUG.id,
        },
      },
      uniteGratuite: currentRemise.ug,
    });
  }
  return true;
};

const _resetUniteGratuite = async (ctx: Context, { id, idCommande, idPharmacie }) => {
  const newArticle = await ctx.prisma.commandeLigne({ id }).produitCanal();

  if (!newArticle) return false;
  const codeCanal = await ctx.prisma
    .commandeLigne({ id })
    .produitCanal()
    .canal()
    .code();
  const idArticles = await articleSamePanachees(ctx, {
    idCanalArticle: newArticle.id,
    codeCanal,
    idPharmacie,
  }).then(same => same.map(article => article.id));

  if (!idArticles || (idArticles && !idArticles.length)) return false;

  const commandeLignes = await getArticlePanacheesInCommande(ctx, idArticles, { idCommande });

  if (commandeLignes && commandeLignes.length) {
    await ctx.prisma.updateManyCommandeLignes({
      data: { uniteGratuite: 0 },
      where: { id_in: commandeLignes.map(ligne => ligne.id) },
    });
    await ctx.prisma.deleteManyCommandeLignes({
      quantiteCdee: 0,
      idCommande: { id: idCommande },
      id_in: commandeLignes.map(ligne => ligne.id),
    });
  }
  return true;
};

export const calculPrix = (commandeLignes: CommandeLigne[]) => {
  const prixBaseTotalHT = commandeLignes
    .map(ligne => ligne.prixBaseHT)
    .reduce((sum, qte) => sum + qte);
  const prixTotalBaseQuantite = commandeLignes
    .map(ligne => ligne.prixBaseHT * ligne.quantiteCdee)
    .reduce((sum, qte) => sum + qte);
  const prixNetTotalHT = commandeLignes
    .map(ligne => ligne.prixNetUnitaireHT)
    .reduce((sum, qte) => sum + qte);
  const prixTotalNetQuantite = commandeLignes
    .map(ligne => ligne.prixNetUnitaireHT * ligne.quantiteCdee)
    .reduce((sum, qte) => sum + qte);

  return { prixBaseTotalHT, prixTotalBaseQuantite, prixTotalNetQuantite, prixNetTotalHT };
};

const _addCommandeLignes = async (
  ctx: Context,
  commande: any,
  id: any,
  codeCanalCommande: string,
) => {
  return ctx.prisma
    .panierLignes({
      where: {
        idPanier: { id },
        produitCanal: { canal: { code: codeCanalCommande } },
        validate: false,
      },
    })
    .then(lignes => {
      const addLignes = lignes.map(async ligne => {
        const {
          optimiser,
          quantite,
          uniteGratuite,
          prixBaseHT,
          remiseLigne,
          remiseGamme,
          prixNetUnitaireHT,
          prixTotalHT,
        } = ligne;

        const article = await ctx.prisma.panierLigne({ id: ligne.id }).produitCanal();
        return ctx.prisma.createCommandeLigne({
          produitCanal: { connect: { id: article.id } },
          idCommande: { connect: { id: commande.id } },
          optimiser,
          quantiteCdee: quantite,
          quantiteLivree: quantite + uniteGratuite,
          uniteGratuite,
          prixBaseHT,
          remiseLigne,
          remiseGamme,
          prixNetUnitaireHT,
          prixTotalHT,
        });
      });
      Promise.all(addLignes).then(async _ => {
        const idLignes = lignes.map(ligne => ligne.id);

        if (idLignes && idLignes.length) {
          await ctx.prisma.updateManyPanierLignes({
            where: { id_in: idLignes },
            data: { validate: true },
          });
        }
      });
    });
};

export const calculPrixTotalOperation = async (
  articles: ArticleCommande[],
  { idPharmacie, idOperation },
  ctx: Context,
) => {
  const pflRemise = await ctx.prisma.operation({ id: idOperation }).remise();
  let remiseDetails: RemiseDetail[] = null;

  if (pflRemise && pflRemise.id) {
    const qteTotalCommande = articles
      .map(articleCmd => (articleCmd && articleCmd.quantite ? articleCmd.quantite : 0))
      .reduce((sum, qte) => sum + qte);
    remiseDetails = await ctx.prisma.remiseDetails({
      where: {
        remise: { id: pflRemise.id },
        quantiteMin_lte: qteTotalCommande,
        quantiteMax_gte: qteTotalCommande,
      },
    });
  }

  const currents = await Promise.all(
    articles.map(async article => {
      const canalArticle = await ctx.prisma.produitCanal({ id: article.id });
      let currentRemise = await _getRemise(ctx, articles, article, { idPharmacie });

      if (pflRemise && remiseDetails && remiseDetails.length) {
        currentRemise = {
          remise:
            remiseDetails && remiseDetails.length
              ? remiseDetails[remiseDetails.length - 1].pourcentageRemise
              : 0,
          ug: 0,
          canalArticleUG:
            remiseDetails && remiseDetails.length
              ? await ctx.prisma
                  .remiseDetail({
                    id: remiseDetails[remiseDetails.length - 1].id,
                  })
                  .produitCanalUg()
              : null,
          isPalier: false,
        };
      }
      const quantite = await quantiteStv(ctx, {
        standard: canalArticle.stv,
        quantite: article.quantite,
      });
      return {
        prixBaseQteTotal: canalArticle.prixPhv * quantite,
        quantite,
        uniteGratuit: currentRemise.ug,
        canalArticleUG: currentRemise.canalArticleUG,
        isPalier: currentRemise.isPalier,
        prixNetQteTotal: ((canalArticle.prixPhv * (100 - currentRemise.remise)) / 100) * quantite,
      };
    }),
  );

  return {
    qteTotal:
      currents && currents.length
        ? currents.map(art => art.quantite).reduce((sum, qte) => sum + qte)
        : 0,
    prixBaseTotal:
      currents && currents.length
        ? currents.map(art => art.prixBaseQteTotal).reduce((sum, qte) => sum + qte)
        : 0,
    prixNetTotal:
      currents && currents.length
        ? currents.map(art => art.prixNetQteTotal).reduce((sum, qte) => sum + qte)
        : 0,
    uniteGratuite:
      pflRemise && remiseDetails && remiseDetails.length
        ? remiseDetails[remiseDetails.length - 1].nombreUg
        : _calculUniteGratuite(currents),
  };
};

interface ResultArticle {
  prixBaseQteTotal: number;
  quantite: number;
  uniteGratuit: number;
  canalArticleUG: ProduitCanal;
  isPalier: boolean;
  prixNetQteTotal: number;
}

const _calculUniteGratuite = (currents: ResultArticle[]) => {
  const result = [];
  const idsUG = [];
  currents.map(article => {
    if (article.isPalier && article.uniteGratuit) result.push(article.uniteGratuit);
    else if (
      !article.isPalier &&
      article.uniteGratuit &&
      article.canalArticleUG &&
      !idsUG.includes(article.canalArticleUG.id)
    ) {
      result.push(article.uniteGratuit);
      idsUG.push(article.canalArticleUG.id);
    }
  });
  return result && result.length ? result.reduce((sum, qte) => sum + qte) : 0;
};

const _getRemise = async (
  ctx: Context,
  articles: ArticleCommande[],
  article: ArticleCommande,
  { idPharmacie },
) => {
  const codeCanal = await ctx.prisma
    .produitCanal({ id: article.id })
    .canal()
    .code();
  const remisePanachees = await getRemisePanacheeDetails(ctx, {
    idPharmacie,
    idCanalArticle: article.id,
    codeCanal,
  });

  if (remisePanachees && remisePanachees.length) {
    const qteTotal = await _calculQteTotalSamePanachees(ctx, articles, {
      idCanalArticle: article.id,
      codeCanal,
      idPharmacie,
    });
    const remiseFiltered = remisePanachees.filter(
      panachee => panachee.quantiteMin <= qteTotal && panachee.quantiteMax >= qteTotal,
    );
    if (remiseFiltered && !remiseFiltered.length) {
      return { remise: 0, ug: 0, canalArticleUG: null, isPalier: false };
    }

    const remiseFinal = remiseFiltered.reduce((prev, current) =>
      prev.pourcentageRemise > current.pourcentageRemise ? prev : current,
    );

    return {
      remise: remiseFinal.pourcentageRemise,
      ug: remiseFinal.nombreUg,
      canalArticleUG: await ctx.prisma
        .remiseDetail({
          id: remiseFinal.id,
        })
        .produitCanalUg(),
      isPalier: false,
    };
  } else {
    // remise palier
    const remiseLignes = await remiseLigneDetails(ctx, {
      idPharmacie,
      idCanalArticle: article.id,
      codeCanal,
    });
    if (!remiseLignes || (remiseLignes && !remiseLignes.length)) {
      return { remise: 0, ug: 0, canalArticleUG: null, isPalier: true };
    }
    const remiseLigne = remiseLignes.filter(
      ligne => ligne.quantiteMin <= article.quantite && ligne.quantiteMax >= article.quantite,
    );
    if (!remiseLigne || (remiseLigne && !remiseLigne.length)) {
      return { remise: 0, ug: 0, canalArticleUG: null, isPalier: true };
    }
    const ligne = remiseLigne[remiseLigne.length - 1];
    return {
      remise: ligne.pourcentageRemise,
      ug: ligne.nombreUg,
      canalArticleUG: null,
      isPalier: true,
    };
  }
};

const _calculQteTotalSamePanachees = async (
  ctx: Context,
  articles: ArticleCommande[],
  { idCanalArticle, codeCanal, idPharmacie },
) => {
  const samePanachees = await articleSamePanachees(ctx, { idCanalArticle, codeCanal, idPharmacie });
  const idArticles = samePanachees.map(same => same.id);
  const articleCmdSamePanachees = articles.filter(article => idArticles.includes(article.id));
  return articleCmdSamePanachees && articleCmdSamePanachees.length
    ? articleCmdSamePanachees.map(article => article.quantite).reduce((sum, qte) => sum + qte)
    : 0;
};
