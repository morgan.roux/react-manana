import {
  Context,
  MarcheInput,
  MarcheLaboratoireInput,
  GroupeClienRemisetInput,
  RemiseDetailInput,
} from '../types';
import { Marche } from '../generated/prisma-client';
import logger from '../logging';
import { ApolloError } from 'apollo-server';
import { deleteRemise, createRemisePalier, createRemiseDetail } from './remise';

export const marchePharmaciesAdherent = async (ctx: Context, { id }) => {
  const marcheGroupeClients = await ctx.prisma.marcheGroupeClients({
    where: {
      marche: {
        id,
      },
    },
  });
  if (marcheGroupeClients && marcheGroupeClients.length) {
    const groupeClients = (
      await Promise.all(
        marcheGroupeClients.map(marcheGroupe =>
          ctx.prisma.marcheGroupeClient({ id: marcheGroupe.id }).groupeClient(),
        ),
      )
    ).filter(client => client);

    if (groupeClients && groupeClients.length) {
      const pharmacies = (
        await Promise.all(
          groupeClients.map(groupe => ctx.prisma.groupeClient({ id: groupe.id }).pharmacies()),
        )
      ).flat();

      return pharmacies;
    }

    return [];
  }

  return [];
};

export const countMarcheRemiseDetails = async (ctx: Context, { id }) => {
  const marcheGroupeClients = await ctx.prisma.marcheGroupeClients({
    where: {
      marche: {
        id,
      },
    },
  });

  if (!marcheGroupeClients || (marcheGroupeClients && !marcheGroupeClients.length)) return 0;

  const remises = (
    await Promise.all(
      marcheGroupeClients.map(marcheGroupe =>
        ctx.prisma.marcheGroupeClient({ id: marcheGroupe.id }).remise(),
      ),
    )
  ).filter(remise => remise);

  if (!remises || (remises && !remises.length)) return 0;

  return ctx.prisma
    .remiseLignesConnection({
      where: {
        remise: {
          id_in: remises.map(remise => remise.id),
        },
      },
    })
    .aggregate()
    .count();
};

export const marcheArticles = async (ctx: Context, { id }) => {
  const type = await ctx.prisma
    .marche({
      id,
    })
    .marcheType();
  const canal = await ctx.prisma
    .marche({
      id,
    })
    .commandeCanal();

  if (type === 'ARTICLE') {
    const marcheArticles = await ctx.prisma.marcheArticles({
      where: { marche: { id } },
    });
    if (marcheArticles && marcheArticles.length) {
      return (
        await Promise.all(
          marcheArticles.map(marcheArticle =>
            ctx.prisma.marcheArticle({ id: marcheArticle.id }).produitCanal(),
          ),
        )
      ).filter(article => article);
    }

    return [];
  } else {
    const laboratoires = await marcheLaboratoires(ctx, { id });

    if (laboratoires && laboratoires.length) {
      const produitTechRegs = await ctx.prisma.produitTechRegs({
        where: {
          laboTitulaire: {
            id_in: laboratoires.map(labo => labo.id),
          },
        },
      });

      if (produitTechRegs && !produitTechRegs.length) return [];

      const produits = await Promise.all(
        produitTechRegs.map(produitTechRegs =>
          ctx.prisma.produitTechReg({ id: produitTechRegs.id }).produit(),
        ),
      );

      if (produits && !produits.length) return [];

      return ctx.prisma.produitCanals({
        where: {
          produit: {
            id_in: produits.map(prod => prod.id),
          },
          canal: {
            id: canal.id,
          },
        },
      });
    }

    return [];
  }
};

export const marcheLaboratoires = async (ctx: Context, { id }) => {
  const marcheLaboratoires = await ctx.prisma.marcheLaboratoires({
    where: {
      marche: {
        id,
      },
    },
  });

  if (marcheLaboratoires && marcheLaboratoires.length) {
    return (
      await Promise.all(
        marcheLaboratoires.map(marcheLabo =>
          ctx.prisma.marcheLaboratoire({ id: marcheLabo.id }).laboratoire(),
        ),
      )
    ).filter(labo => labo);
  }

  return [];
};

export const marcheGroupeClientRemise = async (ctx: Context, { id }) => {
  return ctx.prisma.marcheGroupeClients({
    where: {
      marche: {
        id,
      },
    },
  });
};

export const softDeleteMarche = async (ctx: Context, { id }) => {
  return ctx.prisma
    .updateMarche({ where: { id }, data: { isRemoved: true } })
    .then(async marche => {
      await ctx.indexing.publishSave('marches', marche.id);
      return marche;
    });
};

export const createUpdateMarche = async (
  ctx: Context,
  {
    id,
    nom,
    dateDebut,
    dateFin,
    marcheType,
    avecPalier,
    status,
    codeCanal,
    laboratoires,
    idsCanalArticle,
    groupeClients,
  }: MarcheInput,
) => {
  let marche: Marche;

  try {
    marche = await ctx.prisma.upsertMarche({
      where: { id: id ? id : '' },
      create: {
        nom,
        dateDebut,
        dateFin,
        marcheType,
        avecPalier,
        status,
        commandeCanal: {
          connect: {
            code: codeCanal,
          },
        },
        userCreated: {
          connect: {
            id: ctx.userId,
          },
        },
        userModified: {
          connect: {
            id: ctx.userId,
          },
        },
      },
      update: {
        nom,
        dateDebut,
        dateFin,
        marcheType,
        avecPalier,
        status,
        commandeCanal: {
          connect: {
            code: codeCanal,
          },
        },
        userModified: {
          connect: {
            id: ctx.userId,
          },
        },
      },
    });
  } catch (error) {
    logger.error(`[ERROR CREATE MARCHE] : ${JSON.stringify(error)}`);
    return new ApolloError(`Erreur lors de la creation marche`, 'ERROR_CREATE_MARCHE');
  }

  if (!marche) new ApolloError(`Erreur lors de la creation marche`, 'ERROR_CREATE_MARCHE');

  logger.info(`[SUCCESS CREATE MARCHE] : ${JSON.stringify(marche)}`);

  try {
    if (marche.marcheType === 'ARTICLE') {
      await createUpdateMarcheArticle(ctx, marche.id, idsCanalArticle);
    } else {
      await createUpdateMarcheLaboratoire(ctx, marche.id, laboratoires);
    }
  } catch (error) {
    logger.error(`[ERROR CREATE MARCHE ARTICLE] : ${JSON.stringify(error)}`);
    if (!id) await _resetMarche(ctx, { id: marche.id });
    return new ApolloError(
      `Erreur lors de la creation marché article`,
      'ERROR_CREATE_MARCHE_ARTICLE',
    );
  }

  try {
    await createUpdateMarcheGroupeClientRemise(ctx, marche.id, groupeClients);
  } catch (error) {
    logger.error(`[ERROR CREATE MARCHE GROUPE] : ${JSON.stringify(error)}`);
    if (!id) await _resetMarche(ctx, { id: marche.id });
    return new ApolloError(
      `Erreur lors de la creation marche groupe de client avec les paliers correspondants`,
      'ERROR_CREATE_MARCHE_GROUPE',
    );
  }

  return ctx.prisma.marche({ id: marche.id }).then(async marche => {
    await ctx.indexing.publishSave('marches', marche.id);
    return marche;
  });
};

export const createUpdateMarcheArticle = async (
  ctx: Context,
  id: string,
  idsCanalArticles: string[],
) => {
  const articlesToDeleted = await ctx.prisma.marcheArticles({
    where: { marche: { id }, produitCanal: { id_not_in: idsCanalArticles } },
  });
  if (articlesToDeleted && articlesToDeleted.length) {
    await ctx.prisma.deleteManyMarcheArticles({
      id_in: articlesToDeleted.map(toDelete => toDelete.id),
    });
  }
  logger.info(
    `[CREATE MARCHE ARTICLE] : idsCanalArticles => ${JSON.stringify(
      idsCanalArticles,
    )} | articlesToDeleted => ${JSON.stringify(articlesToDeleted)}`,
  );
  return Promise.all(
    idsCanalArticles.map(async idCanalArticle => {
      const article = await ctx.prisma.$exists.marcheArticle({
        marche: { id },
        produitCanal: { id: idCanalArticle },
      });
      if (!article) {
        return ctx.prisma.createMarcheArticle({
          marche: {
            connect: { id },
          },
          produitCanal: {
            connect: {
              id: idCanalArticle,
            },
          },
        });
      }
    }),
  );
};

export const createUpdateMarcheGroupeClientRemise = async (
  ctx: Context,
  id: string,
  marcheGroupeClientRemise: GroupeClienRemisetInput[],
) => {
  const canal = await ctx.prisma.marche({ id }).commandeCanal();
  const dateDebut = await ctx.prisma.marche({ id }).dateDebut();
  const dateFin = await ctx.prisma.marche({ id }).dateFin();
  const nom = await ctx.prisma.marche({ id }).nom();
  const avecPalier = await ctx.prisma.marche({ id }).avecPalier();

  /* Supprimer les groupes client du marché avec le remise ne sont plus selectionnées */
  const whereToDelete = {
    where: { marche: { id } },
  };

  logger.info(
    `[WHERE DELETE MARCHE GROUPE] : avecPalier => ${avecPalier} groupesToDelete => ${JSON.stringify(
      whereToDelete,
    )}`,
  );
  const groupesToDelete = await ctx.prisma.marcheGroupeClients(whereToDelete);
  logger.info(`[DELETE MARCHE GROUPE] : groupesToDelete => ${JSON.stringify(groupesToDelete)}`);

  if (groupesToDelete && groupesToDelete.length) {
    const remisesToDelete = (
      await Promise.all(
        groupesToDelete.map(groupe => ctx.prisma.marcheGroupeClient({ id: groupe.id }).remise()),
      )
    ).filter(remise => remise);
    logger.info(
      `[DELETE REMISE MARCHE GROUPE] : remisesToDelete => ${JSON.stringify(remisesToDelete)}`,
    );
    if (remisesToDelete && remisesToDelete.length) {
      await Promise.all(
        remisesToDelete.map(async remise => deleteRemise(ctx, { idRemise: remise.id })),
      );
    }

    await ctx.prisma.deleteManyMarcheGroupeClients({
      id_in: groupesToDelete.map(toDelete => toDelete.id),
    });
  }

  return Promise.all(
    marcheGroupeClientRemise.map(async (marcheGroupeRemise, index) => {
      const remise = !avecPalier
        ? null
        : marcheGroupeRemise.idRemise
        ? await ctx.prisma.remise({ id: marcheGroupeRemise.idRemise })
        : await createRemisePalier(
            ctx,
            {
              codeGroupeClient: marcheGroupeRemise.codeGroupeClient,
              codeCanal: canal.code,
              dateDebut,
              dateFin,
              nomRemise: `Remise ${nom}`,
              active: true,
              nbRefMin: 0,
              modelRemise: 'LIGNE',
              typeRemise: 'R',
              source: 'MARCHE',
            },
            index + 1,
          );
      const whereExist = avecPalier
        ? {
            marche: { id },
            groupeClient: { codeGroupe: marcheGroupeRemise.codeGroupeClient },
            remise: { id: remise.id },
          }
        : {
            marche: { id },
            groupeClient: { codeGroupe: marcheGroupeRemise.codeGroupeClient },
          };
      const exist = await ctx.prisma.$exists.marcheGroupeClient(whereExist);

      if (!exist) {
        await ctx.prisma.createMarcheGroupeClient({
          marche: {
            connect: { id },
          },
          groupeClient: {
            connect: {
              codeGroupe: marcheGroupeRemise.codeGroupeClient,
            },
          },
          remise: remise
            ? {
                connect: {
                  id: remise.id,
                },
              }
            : null,
        });
      }

      /* Create remise detail or recraite if already exist */
      console.log('..................', remise, avecPalier, marcheGroupeRemise.remiseDetails);

      if (avecPalier && remise && marcheGroupeRemise.remiseDetails.length) {
        await ctx.prisma.deleteManyRemiseDetails({
          remise: {
            id: remise.id,
          },
        });

        await Promise.all(
          marcheGroupeRemise.remiseDetails.map(async detail =>
            createRemiseDetail(ctx, remise.id, {
              pourcentageRemise: detail.pourcentageRemise,
              quantiteMax: detail.quantiteMax,
              quantiteMin: detail.quantiteMin,
              remiseSupplementaire: detail.remiseSupplementaire,
            }),
          ),
        );
      }

      return marcheGroupeRemise;
    }),
  );
};

export const createUpdateMarcheLaboratoire = async (
  ctx: Context,
  id: string,
  marcheLaboratoires: MarcheLaboratoireInput[],
) => {
  const idsLabo = marcheLaboratoires.map(marcheLabo => marcheLabo.idLaboratoire);
  const labosToDeleted = await ctx.prisma.marcheLaboratoires({
    where: {
      marche: { id },
      laboratoire: {
        id_not_in: idsLabo,
      },
    },
  });
  if (labosToDeleted && labosToDeleted.length) {
    await ctx.prisma.deleteManyMarcheLaboratoires({
      id_in: labosToDeleted.map(toDelete => toDelete.id),
    });
  }

  logger.info(
    `[CREATE MARCHE LABO] : marcheLaboratoires => ${JSON.stringify(
      marcheLaboratoires,
    )} | labosToDeleted => ${JSON.stringify(labosToDeleted)}`,
  );

  return Promise.all(
    marcheLaboratoires.map(async marcheLabo => {
      const labo = await ctx.prisma.$exists.marcheLaboratoire({
        marche: { id },
        laboratoire: {
          id_in: marcheLabo.idLaboratoire,
        },
      });
      if (!labo) {
        return ctx.prisma.createMarcheLaboratoire({
          marche: {
            connect: { id },
          },
          laboratoire: {
            connect: {
              id: marcheLabo.idLaboratoire,
            },
          },
          obligatoire: marcheLabo.obligatoire,
        });
      } else {
        return ctx.prisma.updateManyMarcheLaboratoires({
          where: {
            marche: {
              id,
            },
            laboratoire: {
              id: marcheLabo.idLaboratoire,
            },
          },
          data: {
            obligatoire: marcheLabo.obligatoire,
          },
        });
      }
    }),
  );
};

const _resetMarche = async (ctx: Context, { id }) => {
  await ctx.prisma.deleteManyMarcheArticles({ marche: { id } });
  await ctx.prisma.deleteManyMarcheLaboratoires({ marche: { id } });
  return ctx.prisma.deleteMarche({ id });
};

export const marcheRemise = async (ctx: Context, { id, idPharmacie }) => {
  if (!idPharmacie) return null;
  const marcheGroupeClients = await ctx.prisma.marcheGroupeClients({
    where: {
      marche: {
        id,
      },
      groupeClient: {
        pharmacies_some: {
          id: idPharmacie,
        },
      },
    },
    /*  last: 1, */
  });

  if (!marcheGroupeClients || (marcheGroupeClients && !marcheGroupeClients.length)) return null;

  const remises = await Promise.all(
    marcheGroupeClients.map(marcheGroupe =>
      ctx.prisma.marcheGroupeClient({ id: marcheGroupe.id }).remise(),
    ),
  );

  if (!remises || (remises && !remises.length)) return null;

  const details = await ctx.prisma.remiseDetails({
    where: {
      remise: { id_in: remises.map(remise => remise.id) },
    },
    first: 1,
    orderBy: 'quantiteMin_ASC',
  });

  if (!details || (details && !details.length)) return null;

  return ctx.prisma.remiseDetail({ id: details[0].id }).remise();
};

export const marcheRemiseDetails = async (ctx: Context, { id, idPharmacie }) => {
  const remise = await marcheRemise(ctx, { id, idPharmacie });

  return remise
    ? ctx.prisma.remiseDetails({
        where: {
          remise: { id: remise.id },
        },
      })
    : null;
};
