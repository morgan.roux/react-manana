import { Context } from '../types';
import { countSmyley } from './smyley';
import { countCommentsPharmacie } from './comment';
import { countOperationCiblePharmacies } from './operationPharmacieDetail';
import { countActualiteCiblePharmacies } from './actualite';
import { monthInYear } from './statistiqueBusiness';
import { countPartagesPharmacie } from './partage';

export const orderByFeedback = [
  'cibles',
  'lu',
  'adore',
  'content',
  'aime',
  'mecontent',
  'deteste',
  'commentaires',
  'partages',
  'recommandations',
];

export const orderByItem = ['libelle', 'dateDebut', 'dateFin'];

export const checkIdItemAssocies = async (
  ctx: Context,
  codeItem: string,
  idItemAssocies: string[],
  { skip, first }: { skip: number; first: number },
): Promise<string[]> => {
  return itemSourceAssocies(ctx, codeItem, idItemAssocies, {
    skip,
    first,
  }).then((itemAssocies: any[]) => {
    return itemAssocies && itemAssocies.length
      ? itemAssocies.map(itemAssocie => itemAssocie.id)
      : [];
  });
};

export const itemSourceAssocies = async (
  ctx: Context,
  codeItem: string,
  idItemAssocies: string[],
  { skip, first },
): Promise<any[]> => {
  const filter =
    (!skip && skip !== 0) || (!first && first !== 0)
      ? { where: { id_in: idItemAssocies } }
      : { where: { id_in: idItemAssocies }, skip, first };

  switch (codeItem) {
    case 'OPERACOM':
      const operations = await ctx.prisma.operations(filter);
      return operations && operations.length
        ? Promise.all(
            operations.map(op => {
              return {
                type: 'operation',
                ...op,
              };
            }),
          )
        : [];
    case 'ACTUALITE':
      const actualites = await ctx.prisma.actualites(filter);
      return actualites && actualites.length
        ? Promise.all(
            actualites.map(actu => {
              return {
                type: 'actualite',
                ...actu,
              };
            }),
          )
        : [];
    default:
      return [];
  }
};

export const feedbackAchieved = async (
  ctx: Context,
  pilotage: string,
  codeItem: string,
  idsItemAssocie: string[],
  idPharmacies: string[],
) => {
  const cibles = await countItemAssociePharmacieCible(ctx, {
    codeItem,
    idsAssocie: idsItemAssocie,
  });
  const lu = await luFeedbackItemAssocie(ctx, pilotage, codeItem, {
    idsItemAssocie,
    idPharmacies,
  });

  const commentaires = await countCommentsPharmacie(ctx, {
    code: codeItem,
    idPharmacies,
    idsSourceAssocie: idsItemAssocie,
  });

  const partages = await countPartagesPharmacie(ctx, {
    code: codeItem,
    idPharmacies,
    idsSourceAssocie: idsItemAssocie,
    type: 'PARTAGE',
  });

  const recommandations = await countPartagesPharmacie(ctx, {
    code: codeItem,
    idPharmacies,
    idsSourceAssocie: idsItemAssocie,
    type: 'RECOMMANDATION',
  });

  return {
    cibles,
    lu,
    adore: await countSmyley(ctx, {
      code: codeItem,
      note: 6,
      idPharmacies,
      idsSourceAssocie: idsItemAssocie,
    }),
    content: await countSmyley(ctx, {
      code: codeItem,
      note: 5,
      idPharmacies,
      idsSourceAssocie: idsItemAssocie,
    }),
    aime: await countSmyley(ctx, {
      code: codeItem,
      note: 4,
      idPharmacies,
      idsSourceAssocie: idsItemAssocie,
    }),
    mecontent: await countSmyley(ctx, {
      code: codeItem,
      note: 2,
      idPharmacies,
      idsSourceAssocie: idsItemAssocie,
    }),
    deteste: await countSmyley(ctx, {
      code: codeItem,
      note: 1,
      idPharmacies,
      idsSourceAssocie: idsItemAssocie,
    }),
    commentaires,
    partages,
    recommandations,
  };
};

export const luFeedbackItemAssocie = async (
  ctx: Context,
  pilotage: string,
  codeItem: string,
  {
    idsItemAssocie,
    idPharmacies,
    dateDebut,
    dateFin,
  }: { idsItemAssocie: string[]; idPharmacies: string[]; dateDebut?: string; dateFin?: string },
) => {
  return pilotage === 'ITEM'
    ? (
        await Promise.all(
          idsItemAssocie.map(async idAssocie => {
            const nbLikesPharmaice = await Promise.all(
              idPharmacies.map(async idPharmacie =>
                (await feedbackSeen(ctx, { codeItem, idAssocie, idPharmacie, dateDebut, dateFin }))
                  ? 1
                  : 0,
              ),
            );
            return nbLikesPharmaice && nbLikesPharmaice.length
              ? nbLikesPharmaice.reduce((a, b) => a + b, 0)
              : 0;
          }),
        )
      ).reduce((a, b) => a + b, 0)
    : (
        await Promise.all(
          idPharmacies.map(async idPharmacie => {
            const nbLikesOperation = await Promise.all(
              idsItemAssocie.map(async idAssocie =>
                (await feedbackSeen(ctx, { codeItem, idAssocie, idPharmacie, dateDebut, dateFin }))
                  ? 1
                  : 0,
              ),
            );
            return nbLikesOperation && nbLikesOperation.length
              ? nbLikesOperation.reduce((a, b) => a + b, 0)
              : 0;
          }),
        )
      ).reduce((a, b) => a + b, 0);
};

export const countItemAssociePharmacieCible = async (
  ctx: Context,
  { codeItem, idsAssocie }: { codeItem: string; idsAssocie: any[] },
) => {
  if (!idsAssocie || (idsAssocie && !idsAssocie.length)) return 0;
  switch (codeItem) {
    case 'OPERACOM':
      return countOperationCiblePharmacies(ctx, { ids: idsAssocie.map(id => String(id)) });
    case 'ACTUALITE':
      return countActualiteCiblePharmacies(ctx, { ids: idsAssocie.map(id => String(id)) });
    default:
      return 0;
  }
};

export const feedbackSeen = async (
  ctx: Context,
  {
    codeItem,
    idAssocie,
    idPharmacie,
    dateDebut,
    dateFin,
  }: {
    codeItem: string;
    idAssocie: string;
    idPharmacie: string;
    dateDebut?: string;
    dateFin?: string;
  },
) => {
  const isLike = await checkLikePharmacie(ctx, {
    codeItem,
    idAssocie,
    idPharmacie,
  });

  if (isLike) return isLike;

  switch (codeItem) {
    case 'OPERACOM':
      return ctx.prisma.$exists.operationViewer(
        dateDebut && dateFin
          ? {
              idOperation: { id: idAssocie },
              idPharmacie: { id: idPharmacie },
              dateCreation_gte: dateDebut,
              dateCreation_lt: dateFin,
            }
          : {
              idOperation: { id: idAssocie },
              idPharmacie: { id: idPharmacie },
            },
      );
    case 'ACTUALITE':
      return ctx.prisma.$exists.actualitePharmacieSeen(
        dateDebut && dateFin
          ? {
              idActualite: { id: idAssocie },
              idPharmacie: { id: idPharmacie },
              dateCreation_gte: dateDebut,
              dateCreation_lt: dateFin,
            }
          : {
              idActualite: { id: idAssocie },
              idPharmacie: { id: idPharmacie },
            },
      );
    default:
      return false;
  }
};

export const checkLikePharmacie = async (
  ctx: Context,
  {
    codeItem,
    idAssocie,
    idPharmacie,
    dateDebut,
    dateFin,
  }: {
    codeItem: string;
    idAssocie: string;
    idPharmacie: string;
    dateDebut?: string;
    dateFin?: string;
  },
) => {
  const defaultWhere = {
    item: { code: codeItem },
    idSource: idAssocie,
    isRemoved: false,
    smyley: { groupement: { id: ctx.groupementId } },
    user: {
      OR: [
        {
          userTitulaires_some: { idPharmacie: { id: idPharmacie } },
        },
        {
          userPpersonnels_some: { idPharmacie: { id: idPharmacie } },
        },
      ],
      AND: [
        {
          groupement: { id: ctx.groupementId },
        },
      ],
    },
  };
  const where =
    dateDebut && dateFin
      ? {
          ...defaultWhere,
          dateCreation_gte: dateDebut,
          dateCreation_lt: dateFin,
        }
      : defaultWhere;

  return ctx.prisma.$exists.userSmyley(where);
};

export const piloteFeedback = async (
  ctx: Context,
  pilotage: string,
  codeItem: string,
  {
    idItemAssocies,
    idPharmacies,
  }: {
    idItemAssocies: string[];
    idPharmacies: string[];
  },
) => {
  const feedback = await feedbackAchieved(ctx, pilotage, codeItem, idItemAssocies, idPharmacies);

  return {
    id: pilotage === 'ITEM' ? idItemAssocies[0] : idPharmacies[0],
    itemAssocie:
      pilotage === 'ITEM'
        ? await itemSourceAssocies(ctx, codeItem, idItemAssocies, {
            first: null,
            skip: null,
          }).then(items => (items && items.length ? items[0] : null))
        : null,
    pharmacie: pilotage === 'ITEM' ? null : await ctx.prisma.pharmacie({ id: idPharmacies[0] }),
    ...feedback,
  };
};

export const feedbackObjectifRealise = async (
  ctx: Context,
  pilotage: string,
  codeItem: string,
  indicateur: string,
  {
    idItemAssocies,
    idPharmacies,
  }: {
    idItemAssocies: string[];
    idPharmacies: string[];
  },
) => {
  const objectif = await countItemAssociePharmacieCible(ctx, {
    codeItem,
    idsAssocie: idItemAssocies,
  });

  console.log(objectif);

  const nbPharmacieSmeyley = await countSmyley(ctx, {
    code: codeItem,
    idPharmacies,
    idsSourceAssocie: idItemAssocies,
  });
  switch (indicateur) {
    case 'CIBLE':
      return {
        objectif,
        value: nbPharmacieSmeyley,
        nbPharmacieSmeyley: 0,
      };
    case 'LU':
      return {
        objectif,
        value: await luFeedbackItemAssocie(ctx, pilotage, codeItem, {
          idsItemAssocie: idItemAssocies,
          idPharmacies,
        }),
        nbPharmacieSmeyley: 0,
      };
    case 'ADORE':
      return {
        objectif,
        value: await countSmyley(ctx, {
          code: codeItem,
          note: 6,
          idPharmacies,
          idsSourceAssocie: idItemAssocies,
        }),
        nbPharmacieSmeyley,
      };
    case 'CONTENT':
      return {
        objectif,
        value: await countSmyley(ctx, {
          code: codeItem,
          note: 5,
          idPharmacies,
          idsSourceAssocie: idItemAssocies,
        }),
        nbPharmacieSmeyley,
      };
    case 'AIME':
      return {
        objectif,
        value: await countSmyley(ctx, {
          code: codeItem,
          note: 4,
          idPharmacies,
          idsSourceAssocie: idItemAssocies,
        }),
        nbPharmacieSmeyley,
      };
    case 'MECONTENT':
      return {
        objectif,
        value: await countSmyley(ctx, {
          code: codeItem,
          note: 2,
          idPharmacies,
          idsSourceAssocie: idItemAssocies,
        }),
        nbPharmacieSmeyley,
      };
    case 'DETESTE':
      return {
        objectif,
        value: await countSmyley(ctx, {
          code: codeItem,
          note: 1,
          idPharmacies,
          idsSourceAssocie: idItemAssocies,
        }),
        nbPharmacieSmeyley,
      };
    case 'COMMENTAIRE':
      return {
        objectif,
        value: await countCommentsPharmacie(ctx, {
          code: codeItem,
          idPharmacies,
          idsSourceAssocie: idItemAssocies,
        }),
        nbPharmacieSmeyley: 0,
      };
    case 'PARTAGE':
      return {
        objectif,
        value: await countPartagesPharmacie(ctx, {
          code: codeItem,
          idPharmacies,
          idsSourceAssocie: idItemAssocies,
          type: 'PARTAGE',
        }),
        nbPharmacieSmeyley: 0,
      };
    case 'RECOMMANDATION':
      return {
        objectif,
        value: await countPartagesPharmacie(ctx, {
          code: codeItem,
          idPharmacies,
          idsSourceAssocie: idItemAssocies,
          type: 'RECOMMANDATION',
        }),
        nbPharmacieSmeyley: 0,
      };
  }

  return {
    objectif: 0,
    value: 0,
    nbPharmacieSmeyley: 0,
  };
};

export const feedbackAchievedInYear = async (
  ctx: Context,
  year: string,
  codeItem: string,
  pilotage: string,
  indicateur: string,
  idItemAssocies: string[],
  idPharmacies: string[],
) => {
  const currentDate = new Date();
  const seletedYear = year ? parseInt(year, 10) : currentDate.getFullYear();
  const dateMonths = monthInYear(seletedYear);

  switch (indicateur) {
    case 'CIBLE':
      const cibles = await countItemAssociePharmacieCible(ctx, {
        codeItem,
        idsAssocie: idItemAssocies,
      });
      return Promise.all(
        dateMonths.map(async dateMonth => {
          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: cibles,
          };
        }),
      );
    case 'LU':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          const lu = await luFeedbackItemAssocie(ctx, pilotage, codeItem, {
            idsItemAssocie: idItemAssocies,
            idPharmacies,
            dateDebut: dateMonth.debut,
            dateFin: dateMonth.fin,
          });
          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: lu,
          };
        }),
      );
    case 'ADORE':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: await countSmyley(ctx, {
              code: codeItem,
              note: 6,
              idPharmacies,
              idsSourceAssocie: idItemAssocies,
              dateDebut: dateMonth.debut,
              dateFin: dateMonth.fin,
            }),
          };
        }),
      );
    case 'CONTENT':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: await countSmyley(ctx, {
              code: codeItem,
              note: 5,
              idPharmacies,
              idsSourceAssocie: idItemAssocies,
              dateDebut: dateMonth.debut,
              dateFin: dateMonth.fin,
            }),
          };
        }),
      );
    case 'AIME':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: await countSmyley(ctx, {
              code: codeItem,
              note: 4,
              idPharmacies,
              idsSourceAssocie: idItemAssocies,
              dateDebut: dateMonth.debut,
              dateFin: dateMonth.fin,
            }),
          };
        }),
      );
    case 'MECONTENT':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: await countSmyley(ctx, {
              code: codeItem,
              note: 2,
              idPharmacies,
              idsSourceAssocie: idItemAssocies,
              dateDebut: dateMonth.debut,
              dateFin: dateMonth.fin,
            }),
          };
        }),
      );
    case 'DETESTE':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: await countSmyley(ctx, {
              code: codeItem,
              note: 1,
              idPharmacies,
              idsSourceAssocie: idItemAssocies,
              dateDebut: dateMonth.debut,
              dateFin: dateMonth.fin,
            }),
          };
        }),
      );
    case 'COMMENTAIRE':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: await countCommentsPharmacie(ctx, {
              code: codeItem,
              idPharmacies,
              idsSourceAssocie: idItemAssocies,
              dateDebut: dateMonth.debut,
              dateFin: dateMonth.fin,
            }),
          };
        }),
      );
  }
};
