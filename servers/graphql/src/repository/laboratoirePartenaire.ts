import { LaboratoirePartenaireCreateInput } from '../generated/prisma-client';
import { Context, LaboratoirePartenaireInput } from '../types';

export const createUpdateLaboratoirePartenaire = async (
  args: LaboratoirePartenaireInput,
  ctx: Context,
) => {
  const {
    id,
    debutPartenaire,
    finPartenaire,
    typePartenaire,
    statutPartenaire,
    idLaboratoire,
  } = args;

  const laboraoirePartenaireData: LaboratoirePartenaireCreateInput = {
    debutPartenaire,
    finPartenaire,
    typePartenaire,
    statutPartenaire,
  };

  const laboratoire = await ctx.prisma.updateLaboratoire({
    where: { id: idLaboratoire },
    data: {
      laboratoirePartenaire: {
        upsert: { create: laboraoirePartenaireData, update: laboraoirePartenaireData },
      },
    },
  });

  await ctx.indexing.publishSave('laboratoires', laboratoire.id);
  return ctx.prisma.laboratoire({ id: laboratoire.id }).laboratoirePartenaire();
};

export const softDeleteLaboratoirePartenaire = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma.updateLaboratoirePartenaire({
    where: { id },
    data: {
      isRemoved: true,
    },
  });
};
