import { IdeeOuBonnePratiqueLuCreateInput } from '../generated/prisma-client';
import { Context, IdeeOuBonnePratiqueLuInput } from '../types';
import { publishIdeeOuBonnePratique } from './ideeOuBonnePratique';

export const createUpdateIdeeOuBonnePratiqueLu = async (
  ctx: Context,
  input: IdeeOuBonnePratiqueLuInput,
) => {
  const { id, isRemoved, idUser, idIdeeOuBonnePratique } = input;
  const data: IdeeOuBonnePratiqueLuCreateInput = {
    user: idUser ? { connect: { id: idUser } } : null,
    ideeOuBonnePratique: idIdeeOuBonnePratique ? { connect: { id: idIdeeOuBonnePratique } } : null,
    isRemoved: isRemoved || false,
  };

  const result = await ctx.prisma.upsertIdeeOuBonnePratiqueLu({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  if (idIdeeOuBonnePratique) publishIdeeOuBonnePratique(ctx, idIdeeOuBonnePratique);
  return result;
};

export const softDeleteIdeeOuBonnePratiqueLu = async (ctx: Context, id: string) => {
  const result = await ctx.prisma.updateIdeeOuBonnePratiqueLu({
    where: { id },
    data: { isRemoved: true },
  });
  const ideeBonnePratique = await ctx.prisma.ideeOuBonnePratiqueLu({ id }).ideeOuBonnePratique();
  if (ideeBonnePratique) publishIdeeOuBonnePratique(ctx, ideeBonnePratique.id);
  return result;
};
