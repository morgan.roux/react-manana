import { Context } from '../types';
import { addFile, deleteFile } from './file';
import { createUpdatePharmacie } from './pharmacie';
export const createGroupement = async (
  {
    nom,
    adresse1,
    adresse2,
    cp,
    ville,
    pays,
    telBureau,
    telMobile,
    mail,
    site,
    commentaire,
    nomResponsable,
    prenomResponsable,
    dateSortie,
    sortie,
    isReference,
  },
  ctx: Context,
) => {
  const defaultSortie = sortie ? sortie : 0;
  return ctx.prisma
    .createGroupement({
      nom,
      adresse1,
      adresse2,
      cp,
      ville,
      pays,
      telBureau,
      telMobile,
      mail,
      site,
      commentaire,
      nomResponsable,
      prenomResponsable,
      dateSortie,
      sortie: defaultSortie,
      isReference: isReference || false,
    })
    .then(createdGroupement => {
      createUpdatePharmacie({ ...ctx, groupementId: createdGroupement.id }, {
        cip: '0',
        nom,
        adresse1,
        adresse2,
        cp,
        ville,
        pays,
        commentaire,
        type: 'TOUTES_PHARMACIES',
        nbEmploye: 0,
        nbAssistant: 0,
        nbAutreTravailleur: 0,
      } as any);

      return createdGroupement;
    });
};

export const updateGroupement = async (
  {
    id,
    nom,
    adresse1,
    adresse2,
    cp,
    ville,
    pays,
    telBureau,
    telMobile,
    mail,
    site,
    commentaire,
    nomResponsable,
    prenomResponsable,
    dateSortie,
    sortie,
    isReference,
  },
  ctx: Context,
) => {
  if (isReference) {
    await ctx.prisma.updateManyGroupements({ data: { isReference: false } });
  }
  return ctx.prisma.updateGroupement({
    data: {
      nom,
      adresse1,
      adresse2,
      cp,
      ville,
      pays,
      telBureau,
      telMobile,
      mail,
      site,
      commentaire,
      nomResponsable,
      prenomResponsable,
      dateSortie,
      sortie,
      isReference: isReference || false,
    },
    where: { id },
  });
};

export const deleteGroupement = async (id: string, ctx: Context) => {
  await deleteGroupementAvatars(id, ctx);
  return ctx.prisma.deleteGroupement({ id });
};

export const deleteGroupementAvatars = async (groupementId: string, ctx: Context) => {
  const existingLogos = await ctx.prisma.groupementLogoes({
    where: { groupement: { id: groupementId } },
  });

  await Promise.all(
    existingLogos.map(async logo => ctx.prisma.deleteGroupementLogo({ id: logo.id })),
  );
};

export const createGroupementLogo = ({ id, chemin, nomOriginal }, ctx: Context) => {
  return addFile(ctx, {
    table: 'groupementLogo',
    column: 'groupement',
    id,
    chemin,
    nomOriginal,
    type: 'LOGO',
  });
};

export const deleteGroupementLogo = ({ chemin }, ctx: Context) => {
  return deleteFile(ctx, {
    table: 'groupementLogo',
    chemin,
  });
};
