import { GroupeAmisCreateInput } from '../generated/prisma-client';
import { Context, GroupeAmisInput } from '../types';

export const createUpdateGroupeAmis = async (input: GroupeAmisInput, ctx: Context) => {
  const {
    id,
    type,
    codeMaj,
    nom,
    description,
    status,
    predefine,
    idDepartement,
    idPharmacie,
    idRegion,
    isRemoved,
  } = input;
  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: GroupeAmisCreateInput = {
    type,
    nom,
    description,
    status,
    isRemoved,
    codeMaj,
    predefine,
    userCreation,
    userModification,
    groupement,
    departement: idDepartement ? { connect: { id: idDepartement } } : null,
    pharmacie: idPharmacie ? { connect: { id: idPharmacie } } : null,
    region: idRegion ? { connect: { id: idRegion } } : null,
  };
  const groupeAmis = await ctx.prisma.upsertGroupeAmis({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  await ctx.indexing.publishSave('groupeamises', groupeAmis.id);
  return groupeAmis;
};

export const softDeleteGroupeAmis = async (ctx: Context, id: string) => {
  return ctx.prisma
    .updateGroupeAmis({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      await ctx.indexing.publishSave('groupeamises', res.id);
      return res;
    });
};
