import { ApolloError } from 'apollo-server';
import { MutationResolvers } from '../generated/graphqlgen';
import {
  PartenaireRepresentantAffectationCreateInput,
  PartenaireRepresentantDemandeAffectationCreateInput,
} from '../generated/prisma-client';
import { Context } from '../types';

export const createUpdatePartenaireRepresentantAffectation = async (
  { input }: MutationResolvers.ArgsCreateUpdatePartenaireRepresentantAffectation,
  ctx: Context,
) => {
  const {
    id,
    typeAffectation,
    idsDepartements,
    fonction,
    idAffecte,
    idRemplacent,
    dateDebut,
    dateFin,
  } = input;

  const errCode = id
    ? 'ERROR_EDIT_PARTENAIRE_REPRESENTANT_AFFECTATION'
    : 'ERROR_CREATION_PARTENAIRE_REPRESENTANT_AFFECTATION';
  const errMsg = id ? 'modification' : 'création';

  if (idsDepartements.length > 0) {
    console.log('idsDepartements :>> ', idsDepartements);
    const remplacent = idRemplacent
      ? await ctx.prisma.partenaireRepresentant({ id: idRemplacent })
      : null;

    // Insert remplacent to Partenaire_representant_demande_affectation

    try {
      let partenaireRepresentantDemandeAffectation = await ctx.prisma
        .partenaireRepresentantAffectation({ id: id || '' })
        .partenaireRepresentantDemandeAffectation();

      if (remplacent) {
        const remplacentData: PartenaireRepresentantDemandeAffectationCreateInput = {
          dateHeureDemande: new Date().toISOString(),
          typeAffectation,
          fonction,
          userCreation: ctx.userId ? { connect: { id: ctx.userId } } : null,
          userModification: ctx.userId ? { connect: { id: ctx.userId } } : null,
          remplacent: remplacent && remplacent.id ? { connect: { id: remplacent.id } } : null,
        };

        console.log('remplacentData :>> ', remplacentData);

        partenaireRepresentantDemandeAffectation = await ctx.prisma.upsertPartenaireRepresentantDemandeAffectation(
          {
            where: {
              id: partenaireRepresentantDemandeAffectation
                ? partenaireRepresentantDemandeAffectation.id
                : '',
            },
            create: remplacentData,
            update: {
              ...remplacentData,
              userModification: ctx.userId ? { connect: { id: ctx.userId } } : null,
            },
          },
        );
      } else if (!remplacent && partenaireRepresentantDemandeAffectation) {
        await ctx.prisma.deletePartenaireRepresentantDemandeAffectation({
          id: partenaireRepresentantDemandeAffectation.id,
        });
      }

      const results = await Promise.all(
        idsDepartements.map(async (idDepartement: string) => {
          const departement = await ctx.prisma.departement({ id: idDepartement });

          const data: PartenaireRepresentantAffectationCreateInput = {
            typeAffectation,
            departement: { connect: { id: idDepartement } },
            partenaireRepresentant: { connect: { id: idAffecte } },
            partenaireRepresentantDemandeAffectation: partenaireRepresentantDemandeAffectation
              ? { connect: { id: partenaireRepresentantDemandeAffectation.id } }
              : null,
            dateFin,
            dateDebut,
            codeDepartement: (departement && departement.code) || null,
            userCreation: ctx.userId ? { connect: { id: ctx.userId } } : null,
            userModification: ctx.userId ? { connect: { id: ctx.userId } } : null,
          };

          console.log('data :>> ', JSON.stringify(data));

          try {
            const partenaireRepresentantAffectation = await ctx.prisma.upsertPartenaireRepresentantAffectation(
              {
                where: { id: id || '' },
                create: data,
                update: {
                  ...data,
                  userModification: ctx.userId
                    ? ctx.userId
                      ? { connect: { id: ctx.userId } }
                      : null
                    : null,
                },
              },
            );

            await ctx.indexing.publishSave(
              'partenairerepresentantaffectations',
              partenaireRepresentantAffectation.id,
            );
            return partenaireRepresentantAffectation;
          } catch (error) {
            console.log('error :>> ', error);
            throw new ApolloError(`Erreur lors de la ${errMsg} de l'affectation`, errCode);
          }
        }),
      );

      console.log('results :>> ', results);
      return results;
    } catch (error) {
      console.log('Remplacent error :>> ', error);
      throw new ApolloError(`Erreur lors de la ${errMsg} de l'affectation`, errCode);
    }
  } else {
    throw new ApolloError(`Aucun département`, 'EMPTY_IDS_DEPARTEMENTS');
  }
};
