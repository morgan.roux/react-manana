import { Context, LaboratoireSuiteInput } from '../types';

export const createUpdateLaboratoire = async (
  { id, nomLabo, suite }: { id: string; nomLabo: string; suite: LaboratoireSuiteInput },
  ctx: Context,
) => {
  const { adresse, codePostal, email, telecopie, telephone, ville, webSiteUrl } = suite;

  return ctx.prisma
    .upsertLaboratoire({
      where: { id: id || '' },
      create: {
        nomLabo,
        laboSuite: {
          create: {
            nom: nomLabo,
            adresse,
            codePostal,
            email,
            telecopie,
            telephone,
            ville,
            webSiteUrl,
          },
        },
      },
      update: {
        nomLabo,
        laboSuite: {
          update: {
            nom: nomLabo,
            adresse,
            codePostal,
            email,
            telecopie,
            telephone,
            ville,
            webSiteUrl,
          },
        },
      },
    })
    .then(async labo => {
      await ctx.indexing.publishSave('laboratoires', labo.id);
      return labo;
    });
};
