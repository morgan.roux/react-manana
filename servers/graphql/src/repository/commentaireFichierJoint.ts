import { CommentaireFichierJointCreateInput } from '../generated/prisma-client';
import { Context, CommentaireFichierJointInput } from '../types';

export const createUpdateCommentaireFichierJoint = async (
  input: CommentaireFichierJointInput,
  ctx: Context,
) => {
  const { id, idCommentaire, idFichier, codeMaj, isRemoved } = input;
  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: CommentaireFichierJointCreateInput = {
    codeMaj,
    isRemoved,
    commentaire: idCommentaire ? { connect: { id: idCommentaire } } : null,
    fichier: idFichier ? { connect: { id: idFichier } } : null,
    userCreation,
    userModification,
  };
  const commentaireFichierJoint = await ctx.prisma.upsertCommentaireFichierJoint({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  // await ctx.indexing.publishSave('commentairefichierjoint', commentaireFichierJoint.id);
  return commentaireFichierJoint;
};

export const softDeleteCommentaireFichierJoint = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateCommentaireFichierJoint({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      // await ctx.indexing.publishSave('commentairefichierjoint', res.id);
      return res;
    });
};
