import { Context } from '../types';
import { MutationResolvers } from '../generated/graphqlgen';

export const CreateExternalMapping = async (ctx: Context, { 
  extenalMappingInput
} : MutationResolvers.ArgsCreateExternalMapping) =>{
  try {
    console.log("extenalMappingInput ", extenalMappingInput);
    return ctx.prisma.createExternalMapping({
      idClient : extenalMappingInput.idClient,
      idExternaluser: extenalMappingInput.idExternaluser,
      password: extenalMappingInput.password,
      idUser : {
        connect : { id : extenalMappingInput.userID}
      },
      md5App: {
        connect : { id : extenalMappingInput.md5Id}
      }
    })
  } catch (error) {
    console.log("error ", error);
    throw new Error(error);
  } 
};

export const UpdateExternalMapping = async (ctx: Context, { 
    id, extenalMappingInput
  }: MutationResolvers.ArgsUpdateExternalMapping) =>{
    try {
      return ctx.prisma.updateExternalMapping({
        data: { 
          idClient : extenalMappingInput.idClient,
          idExternaluser: extenalMappingInput.idExternaluser,
          password: extenalMappingInput.password,
          idUser : {
            connect : { id : extenalMappingInput.userID}
          },
          md5App: {
            connect : { id : extenalMappingInput.md5Id}
          }
        },
        where: {id}
      })
      
    } catch (error) {
      throw new Error(error);
    }
};

export const DeleteExternalMapping = async (ctx: Context, { id }) =>{
  try {
    return ctx.prisma.deleteExternalMapping({id})
  } catch (error) {
    throw new Error(error);
  }
};