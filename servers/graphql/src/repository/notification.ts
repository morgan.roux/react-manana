import { Context } from '../types';
import {
  NOTIFICATION_ADDED,
  NOTIFICATION_UPDATED,
  NOTIFICATION_DELETED,
} from '../resolvers/Subscription/notification';
import logger from '../logging';
import { Notification, User } from '../generated/prisma-client';
import { usersByRole } from './user';
import { SUPER_ADMINISTRATEUR } from '../constants/roles';
import ArrayUnique = require('array-unique');
import moment from 'moment';

interface NotificationParams {
  type: string;
  message: string;
  from?: string;
  to?: string;
  targetId?: string;
  targetName?: string;
  seen?: boolean;
  toAllUsers?: boolean;
  /** Send notification to some users, this params contains user id array */
  someUsers?: string[];
}

/*
  from, to : id user
  sommeUser: array of id user
*/
export const saveAndPublishNotification = async (params: NotificationParams, ctx: Context) => {
  const { type, from, to, targetId, targetName, message, seen, toAllUsers, someUsers } = params;
  let notifications: Notification[] = [];

  logger.info(`[ SAVE ADD NOTIFICATION ] ====> type : ${type} --- targetName : ${targetName}`);

  // Send to all user
  if (toAllUsers) {
    notifications = await ctx.prisma
      .users({ where: { groupement: { id: ctx.groupementId } } })
      .then(async users => {
        const userSuperAdmin = await usersByRole(ctx, SUPER_ADMINISTRATEUR);
        const allUsers =
          userSuperAdmin && userSuperAdmin.length
            ? ArrayUnique([...users, ...userSuperAdmin])
            : ArrayUnique(users);

        const defautValue: string[] = [];
        const allUsersFiltered: string[] = allUsers.reduce((previous, current): string[] => {
          if (current && current.id) return [...previous, current.id];
          return [...previous];
        }, defautValue);

        if (allUsersFiltered && allUsersFiltered.length > 0) {
          logger.info(`[ NOTIFICATION ALL ]`);
          return Promise.all(
            allUsersFiltered
              .filter(all => all)
              .map(async idUser => {
                logger.info(`[ NOTIFICATION ALL ] ====> idUser : ${idUser}`);
                return _notifyUser(ctx, {
                  type,
                  message,
                  to: idUser,
                  from,
                  targetId,
                  targetName,
                  seen,
                });
              }),
          ).catch(errors => {
            logger.error(
              ` [ ERROR ON ALL NOTIFIED ] :: Erreur publish notification ${JSON.stringify(errors)}`,
            );
            return [];
          });
        }
        return [];
      });
  } else if (someUsers && someUsers.length > 0) {
    logger.info(`[ CREATE NOTIFICATION SOME USERS ]`);

    const defautValue: string[] = [];
    let users: string[] = someUsers.reduce((previous, current): string[] => {
      if (current) return [...previous, current];
      return [...previous];
    }, defautValue);

    if (type === 'ACTUALITE_ADDED') {
      const userSuperAdmins = await usersByRole(ctx, SUPER_ADMINISTRATEUR).then(superAdmins => {
        return superAdmins && superAdmins.length
          ? superAdmins.map(admin => (admin && admin.id ? admin.id : null)).filter(usr => usr)
          : [];
      });

      if (userSuperAdmins && userSuperAdmins.length) {
        userSuperAdmins.map(admin => (admin && !users.includes(admin) ? users.push(admin) : null));
      }
    }

    users = ArrayUnique(users);
    notifications = await Promise.all(
      users.map(async idUser => {
        logger.info(`[ NOTIFICATION SOME USERS ] ====> idUser : ${idUser}`);
        return _notifyUser(ctx, {
          type,
          message,
          to: idUser,
          from,
          targetId,
          targetName,
          seen,
        });
      }),
    ).catch(errors => {
      logger.error(
        ` [ ERROR ON SOME USERS NOTIFIED ] :: Erreur publish notification ${JSON.stringify(
          errors,
        )}`,
      );
      return [];
    });
  } else {
    logger.info(`[ CREATE NOTIFICATION CURRENT USERS ]`);
    const notification = await _notifyUser(ctx, {
      type,
      message,
      from,
      to,
      targetId,
      targetName,
      seen,
    }).catch(errors => {
      logger.error(
        ` [ ERROR ON SOME USERS NOTIFIED ] :: Erreur publish notification ${JSON.stringify(
          errors,
        )}`,
      );
      return null;
    });

    if (notification) {
      notifications = [notification];
    }
  }
  return notifications && notifications.length ? notifications : [];
};

const _notifyUser = async (ctx: Context, params: NotificationParams): Promise<Notification> => {
  const { type, message, from, to, targetId, targetName, seen } = params;
  return ctx.prisma
    .createNotification({
      type,
      from: from ? { connect: { id: from } } : null,
      to: to ? { connect: { id: to } } : null,
      targetId,
      targetName,
      message,
      seen,
    })
    .then(async notification => {
      if (notification && notification.id) {
        await ctx.pubsub
          .publish(NOTIFICATION_ADDED, { notificationAdded: notification, to })
          .then(_ => {
            logger.info(
              ` [ CREATE AND PUBLISH NOTIFICATION ] from ${from} => to ${to}, ${JSON.stringify(
                params,
              )} , notification ==> ${notification.id} - ${notification.targetName}`,
            );
          });
      }
      return notification;
    })
    .catch(errors => {
      logger.error(
        `[ ERROR CREATE NOTIFICATION ]  : Erreur create notification ${JSON.stringify(errors)}`,
      );
      return null;
    });
};

export const updateNotificationSeenByUser = async (
  ctx: Context,
  users: User[],
  id: string,
  seen: boolean,
) => {
  logger.info(`[ BEGIN UPDATE NOTIFICATION ] ==> ID : ${id} --- seen : ${seen}`);

  if (users && users.length) {
    /* some user in one groupement */
    logger.info(`[ UPDATE NOTIFICATION SOME USER ]`);

    const defautValue: string[] = [];
    let someUsers: string[] = users.reduce((previous, current): string[] => {
      if (current && current.id) return [...previous, current.id];
      return [...previous];
    }, defautValue);

    someUsers = ArrayUnique(someUsers);

    logger.info(` [** CURRENT USER **] : ${ctx.userId}`);

    const notifications = await Promise.all(
      someUsers.map(async idUser => {
        /* update notification */
        const notification = await ctx.prisma
          .notifications({ where: { to: { id: idUser }, targetId: id } })
          .then(async notifs => (notifs && notifs.length ? notifs[0] : null));

        if (notification && notification.id) {
          const updated = await ctx.prisma.updateNotification({
            where: { id: notification.id },
            data: { seen },
          });

          await ctx.pubsub
            .publish(NOTIFICATION_UPDATED, {
              notificationUpdated: updated,
              to: idUser,
            })
            .then(_ => {
              logger.info(
                `[ PUBLISH UPDATED NOTIFICATION SOME USERS ] ==> idUser : ${idUser} --- notification ID : ${notification.id}, targetName : ${notification.targetName}`,
              );
            });

          return updated;
        }
      }),
    );

    return notifications && notifications.length
      ? notifications.filter(note => note && note.id)
      : [];
  } else {
    /* notification current user */
    logger.info(`[ UPDATE NOTIFICATION CURRENT USER ] ==> id : ${id}`);
    await ctx.prisma.updateManyNotifications({
      where: { to: { id: ctx.userId }, targetId: id },
      data: { seen },
    });

    logger.info(` [** CURRENT USER **] : ${ctx.userId}`);

    const notifications = await ctx.prisma.notifications({
      where: { to: { id: ctx.userId }, targetId: id },
    });

    if (notifications && notifications.length) {
      return Promise.all(
        notifications.map(async notification => {
          await ctx.pubsub
            .publish(NOTIFICATION_UPDATED, {
              notificationUpdated: notification,
              to: ctx.userId,
            })
            .then(_ => {
              logger.info(
                `[ PUBLISH UPDATE NOTIFICATION CURRENT USER ]==> ${JSON.stringify(
                  notification.id,
                )}`,
              );
            });
          return notification;
        }),
      );
    }
    return notifications;
  }
};

export const deleteAllNotifications = async (ctx: Context, id: string) => {
  const update = await ctx.prisma.updateManyNotifications({
    where: {
      targetId: id,
    },
    data: { isRemoved: true },
  });

  if (!update || (update && parseInt(update.count, 10) === 0)) return null;

  const notifications = await ctx.prisma.notifications({ where: { targetId: id } });

  return notifications && notifications.length
    ? Promise.all(
        notifications.map(async notification => {
          const to = await ctx.prisma.notification({ id: notification.id }).to();
          return ctx.pubsub
            .publish(NOTIFICATION_DELETED, { notificationDeleted: notification, to: to.id })
            .then(_ => {
              logger.info(
                ` [ PUBLISH  DELETE NOTIFICATION ] ==> Notification : ${notification.id} - ${notification.targetName}`,
              );
            });
        }),
      )
    : null;
};

export const isActiveNotificationActualite = async (
  ctx: Context,
  { type, targetId }: { type: string; targetId: string },
) => {
  const dateNow = moment().format('YYYY-MM-DD');
  let isActive = true;
  switch (type) {
    case 'ACTUALITE_ADDED':
      const actualite = await ctx.prisma.actualite({ id: targetId });
      const momentDebutActivation = moment(actualite.dateDebut).format('YYYY-MM-DD');
      const momentFinActivation = moment(actualite.dateFin).format('YYYY-MM-DD');

      isActive =
        moment(dateNow).isSameOrAfter(momentDebutActivation) &&
        moment(dateNow).isSameOrBefore(momentFinActivation) &&
        actualite.isRemoved === false;

      logger.info(
        `[SUBSCRIPTION ACTUALITE_ADDED] isActive : ${isActive}
          - ${actualite.isRemoved}
          - dateActuDebutActivation: ${momentDebutActivation}
          - dateNow : ${dateNow}
          - dateActuDebutActivation: ${momentFinActivation}`,
      );
      break;
  }

  return isActive;
};
