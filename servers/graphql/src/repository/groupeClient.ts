import { Context } from '../types';
import moment from 'moment';
import { ApolloError } from 'apollo-server';

export const createUpdateGroupeClient = async (
  {
    id,
    nom,
    dateValiditeDebut,
    dateValiditeFin,
    idPharmacies,
  }: {
    id: string;
    nom: string;
    dateValiditeDebut: any;
    dateValiditeFin: any;
    idPharmacies: string[];
  },
  ctx: Context,
) => {
  const pharmcies = id ? await ctx.prisma.groupeClient({ id }).pharmacies() : [];
  const idPharmaciesDisconnect =
    pharmcies && pharmcies.length
      ? pharmcies.filter(pharmcie => !idPharmacies.includes(pharmcie.id))
      : [];

  const codeGroupe = !id
    ? (await codeGroupeClientMax(ctx)) + 1 + Math.floor(Math.random() * Math.floor(2))
    : 0;

  console.log({
    codeGroupe,
    nom,
    dateValiditeDebut,
    dateValiditeFin,
    idPharmacies,
  });

  try {
    return ctx.prisma
      .upsertGroupeClient({
        where: { id: id || '' },
        create: {
          codeGroupe,
          nomGroupe: nom,
          dateValiditeDebut,
          dateValiditeFin,
          sortie: 0,
          pharmacies: {
            connect: idPharmacies.map(id => {
              return {
                id,
              };
            }),
          },
        },
        update: {
          nomGroupe: nom,
          dateValiditeDebut,
          dateValiditeFin,
          pharmacies: {
            disconnect:
              idPharmaciesDisconnect && idPharmaciesDisconnect.length
                ? idPharmaciesDisconnect.map(pharmcie => {
                    return {
                      id: pharmcie.id,
                    };
                  })
                : null,
            connect: idPharmacies.map(id => {
              return {
                id,
              };
            }),
          },
        },
      })
      .then(async created => {
        await ctx.indexing.publishSave('groupeclients', created.id);
        return created;
      });
  } catch (err) {
    return new ApolloError(
      `Erreur lors de l'enregistrement du groupe client`,
      'ERROR_CREATE_UPDATE_GROUPE',
    );
  }
};

export const codeGroupeClientMax = async (ctx: Context): Promise<number> => {
  const fragment = `
      fragment codeMax on GroupeClient {
        codeGroupe
      }`;

  return ctx.prisma
    .groupeClients({ orderBy: 'codeGroupe_DESC', skip: 0, first: 1 })
    .$fragment(fragment)
    .then((groupes: any[]) =>
      groupes && groupes.length
        ? Math.max(...groupes.map(groupe => parseInt(groupe.codeGroupe, 10)))
        : 0,
    );
};

export const deleteSoftGroupeClient = async ({ id }, ctx: Context) => {
  return ctx.prisma
    .updateGroupeClient({ where: { id }, data: { sortie: 1 } })
    .then(async updated => {
      await ctx.indexing.publishSave('groupeclients', updated.id);
      return updated;
    });
};
