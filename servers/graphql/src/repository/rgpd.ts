import { RgpdCreateInput } from '../generated/prisma-client';
import { Context, RgpdInput } from '../types';

export const createUpdateRgpd = async (ctx: Context, input: RgpdInput) => {
  const { id, conditionUtilisation, informationsCookies, politiqueConfidentialite } = input;

  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;

  const data: RgpdCreateInput = {
    conditionUtilisation,
    informationsCookies,
    politiqueConfidentialite,
    groupement,
    userCreation,
    userModification,
  };

  const res = await ctx.prisma.upsertRgpd({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  return res;
};
