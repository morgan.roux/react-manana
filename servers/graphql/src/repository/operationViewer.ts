import { Context } from '../types';
import { createActiviteUser } from './activiteUser';
import { ApolloError } from 'apollo-server';
import { actualiteSeenByPharmacie } from './actualite';

export const createOperationViewer = async (
  idOperation: string,
  idPharmacie: string,
  ctx: Context,
) => {
  const operationPharmacieSeens = await ctx.prisma.operationViewers({
    where: {
      idOperation: { id: idOperation },
      idPharmacie: { id: idPharmacie },
    },
  });

  if (!operationPharmacieSeens || (operationPharmacieSeens && !operationPharmacieSeens.length)) {
    await ctx.prisma.createOperationViewer({
      idOperation: { connect: { id: idOperation } },
      idPharmacie: { connect: { id: idPharmacie } },
    });
  }

  return ctx.prisma.operation({ id: idOperation }).then(async operation => {
    /**
     *
     * Sauvegarde activite de l'utilisateur
     *
     */
    await ctx.indexing.publishSave('operations', idOperation);
    createActiviteUser(
      {
        code: 'CPERATIONVIEWER',
        nom: 'CREATE OPERATION VIEWER',
      },
      ctx,
    );
    return operation;
  });
};

export const deleteOperationViewer = async (
  idOperation: string,
  idPharmacie: string,
  ctx: Context,
) => {
  const opeationVisited = await ctx.prisma
    .operationViewers({
      where: {
        AND: [{ idOperation: { id: idOperation } }, { idPharmacie: { id: idPharmacie } }],
      },
    })
    .then(operationViewer => operationViewer[0]);

  try {
    await ctx.prisma.deleteOperationViewer({ id: opeationVisited.id });
  } catch (error) {
    return new ApolloError(`Suppresion operation erreur`);
  }
  await ctx.indexing.publishSave('operations', idOperation);
  return ctx.prisma.operation({ id: idOperation });
};
