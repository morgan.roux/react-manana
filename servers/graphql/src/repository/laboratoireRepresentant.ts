import {
  Contact,
  Fichier,
  FichierCreateInput,
  LaboratoireRepresentantCreateInput,
} from '../generated/prisma-client';
import { Context, LaboratoireRepresentantInput } from '../types';

export const createUpdateLaboratoireRepresentant = async (
  args: LaboratoireRepresentantInput,
  ctx: Context,
) => {
  const {
    id,
    idLaboratoire,
    nom,
    civilite,
    fonction,
    photo,
    prenom,
    sexe,
    afficherComme,
    contact,
  } = args;

  // Create or update photo
  let savedPhoto: Fichier;
  if (photo) {
    const fichierData: FichierCreateInput = {
      chemin: photo.chemin,
      nomOriginal: photo.nomOriginal,
      type: photo.type,
      avatar: photo.idAvatar ? { connect: { id: photo.idAvatar } } : null,
    };
    savedPhoto = await ctx.prisma.upsertFichier({
      where: { id: photo.id || '' },
      create: fichierData,
      update: fichierData,
    });
  }

  // Create or update contact
  let savedContact: Contact;
  if (contact) {
    const { id: cId, ...contactRest } = contact;
    const existContact = await ctx.prisma.laboratoireRepresentant({ id: id || '' }).contact();
    savedContact = await ctx.prisma.upsertContact({
      where: { id: (existContact && existContact.id) || '' },
      create: contactRest,
      update: contactRest,
    });
  }

  const laboratoireRepresentantData: LaboratoireRepresentantCreateInput = {
    civilite,
    nom,
    prenom,
    sexe,
    fonction,
    afficherComme,
    contact: savedContact ? { connect: { id: savedContact.id } } : null,
    laboratoire: idLaboratoire ? { connect: { id: idLaboratoire } } : null,
    photo: savedPhoto ? { connect: { id: savedPhoto.id } } : null,
  };

  const laboratoireRepresentant = await ctx.prisma.upsertLaboratoireRepresentant({
    where: { id: id || '' },
    create: laboratoireRepresentantData,
    update: laboratoireRepresentantData,
  });

  console.log('LABO PARTENAIRE REPRESENTANT', laboratoireRepresentant);

  await ctx.indexing.publishSave('laboratoirerepresentants', laboratoireRepresentant.id);
  if (idLaboratoire) await ctx.indexing.publishSave('laboratoires', idLaboratoire);
  return laboratoireRepresentant;
};

export const softDeleteLaboPartenaireRepresentant = async (
  ctx: Context,
  { id }: { id: string },
) => {
  return ctx.prisma
    .updateLaboratoireRepresentant({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async presentant => {
      const labo = await ctx.prisma.laboratoireRepresentant({ id: id || '' }).laboratoire();
      await ctx.indexing.publishSave('laboratoirerepresentants', presentant.id);

      if (labo) await ctx.indexing.publishSave('laboratoires', labo.id);
      return presentant;
    });
};
