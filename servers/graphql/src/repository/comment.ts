import { Context, CommentResult, CommentInput, CommentaireFichierJointInput } from '../types';
import { publishItemSource } from '../repository/item';
import { ApolloError } from 'apollo-server';
import logger from '../logging';
import { createUpdateCommentaireFichierJoint } from './commentaireFichierJoint';
import { FichierCreateInput } from '../generated/prisma-client';
import { head } from 'lodash';
import { getUserPpersonnelsByPharmacie } from './user';
import ArrayUniqueObject from 'unique-array-objects';

export const itemComments = async ({ take, skip, codeItem, idItemAssocie }, ctx: Context) => {
  const newIdSource =
    typeof idItemAssocie !== 'string' ? JSON.stringify(idItemAssocie) : idItemAssocie;
  return ctx.prisma
    .comments({
      last: +take,
      skip: +skip,
      orderBy: 'dateCreation_ASC',
      where: {
        item: { code: codeItem },
        idItemAssocie: newIdSource,
        groupement: { id: ctx.groupementId },
      },
    })
    .then(async comments => {
      return {
        total: await ctx.prisma
          .commentsConnection({
            where: {
              item: { code: codeItem },
              idItemAssocie: newIdSource,
              groupement: { id: ctx.groupementId },
            },
          })
          .aggregate()
          .count(),
        data: comments,
      };
    });
};

export const groupementComments = async ({ codeItem, idItemAssocie }, ctx: Context) => {
  const newIdSource =
    typeof idItemAssocie !== 'string' ? JSON.stringify(idItemAssocie) : idItemAssocie;
  return ctx.prisma
    .comments({
      where: {
        item: { code: codeItem },
        idItemAssocie: newIdSource,
      },
    })
    .then(async comments => {
      return comments && comments.length
        ? ArrayUniqueObject(
            await Promise.all(
              comments.map(comment => ctx.prisma.comment({ id: comment.id }).groupement()),
            ),
          )
        : [];
    });
};

export const createComment = async (input: CommentInput, ctx: Context) => {
  const { codeItem, idItemAssocie, content, fichiers } = input;

  const newIdSource = typeof idItemAssocie !== 'string' ? String(idItemAssocie) : idItemAssocie;
  if (!ctx.userId || !ctx.groupementId) return new ApolloError(`Token expiré : `, 'TOKEN_EXPIRED');
  const comment = await ctx.prisma
    .createComment({
      item: codeItem ? { connect: { code: codeItem } } : null,
      idItemAssocie: newIdSource,
      user: { connect: { id: ctx.userId } },
      content,
      groupement: { connect: { id: ctx.groupementId } },
    })
    .catch(err => {
      logger.error(`Erreur lors du publication du commentaire : `, { err });
      return new ApolloError(
        `Erreur lors du publication du commentaire : `,
        'PUBLISH_COMMENT_ERROR',
      );
    });

  // Save comment fichiers
  if (fichiers && fichiers.length > 0) {
    // Save fichiers
    const savedFichiers = await Promise.all(
      fichiers.map(async f => {
        const fData: FichierCreateInput = {
          chemin: f.chemin,
          nomOriginal: f.nomOriginal,
          type: f.type,
          userCreation: ctx.userId ? { connect: { id: ctx.userId } } : null,
          userModification: ctx.userId ? { connect: { id: ctx.userId } } : null,
        };
        const saved = await ctx.prisma.upsertFichier({
          where: { id: f.id || '' },
          create: fData,
          update: fData,
        });
        return saved;
      }),
    );

    // Save CommentaireFichierJoint
    if (savedFichiers && savedFichiers.length > 0) {
      await Promise.all(
        savedFichiers.map(async f => {
          const cfjExists = await ctx.prisma.commentaireFichierJoints({
            where: { fichier: { id: f.id }, commentaire: { id: comment.id } },
          });
          const existCfj = head(cfjExists);
          const cfjInput: CommentaireFichierJointInput = {
            id: (existCfj && existCfj.id) || '',
            idCommentaire: comment.id,
            idFichier: f.id,
            isRemoved: false,
          };
          await createUpdateCommentaireFichierJoint(cfjInput, ctx);
        }),
      );
    }
  }

  if (comment) {
    await publishItemSource(codeItem, newIdSource, ctx);
    ctx.indexing.publishSave('comments', comment.id);
  }

  return comment;
};

export const comments = async ({ codeItem, idItemAssocie, take, skip }, ctx: Context) => {
  const newIdSource =
    typeof idItemAssocie !== 'string' ? JSON.stringify(idItemAssocie) : idItemAssocie;
  return {
    total: await ctx.prisma
      .commentsConnection({
        where: {
          item: { code: codeItem },
          idItemAssocie: newIdSource,
          groupement: { id: ctx.groupementId },
        },
      })
      .aggregate()
      .count(),
    data: await ctx.prisma.comments({
      where: {
        item: { code: codeItem },
        idItemAssocie: newIdSource,
        groupement: { id: ctx.groupementId },
      },
      first: take,
      skip,
      orderBy: 'dateCreation_ASC',
    }),
  };
};

export const countCommentsPharmacie = async (
  ctx: Context,
  {
    code,
    idPharmacies,
    idsSourceAssocie,
    dateDebut,
    dateFin,
  }: {
    code: string;
    idPharmacies: string[];
    idsSourceAssocie: string[];
    dateDebut?: string;
    dateFin?: string;
  },
) => {
  const userPpersonnels = await getUserPpersonnelsByPharmacie(ctx, idPharmacies);
  const whereIdUsers =
    userPpersonnels && userPpersonnels.length
      ? [{ id_in: userPpersonnels.map(userPpersonnel => userPpersonnel.id) }]
      : [];

  const defaultWhere = {
    item: { code },
    idItemAssocie_in: idsSourceAssocie,
    user: {
      OR: [
        {
          userTitulaires_some: { idPharmacie: { id_in: idPharmacies } },
        },
        ...whereIdUsers,
      ],
      AND: [
        {
          groupement: { id: ctx.groupementId },
        },
      ],
    },
  };

  const where =
    dateDebut && dateFin
      ? {
          ...defaultWhere,
          dateCreation_gte: dateDebut,
          dateCreation_lt: dateFin,
        }
      : defaultWhere;

  return ctx.prisma
    .commentsConnection({
      where,
    })
    .aggregate()
    .count();
};

export const pilotageComments = async (
  {
    codeItem,
    idItemAssocie,
    idsPharmacies,
    take,
    skip,
  }: {
    codeItem: string;
    idItemAssocie: string;
    idsPharmacies: string[];
    take?: number;
    skip?: number;
  },
  ctx: Context,
) => {
  const userPpersonnels = await getUserPpersonnelsByPharmacie(ctx, idsPharmacies);
  const whereIdUsers =
    userPpersonnels && userPpersonnels.length
      ? [{ id_in: userPpersonnels.map(userPpersonnel => userPpersonnel.id) }]
      : [];

  const defaultWhere = {
    item: { code: codeItem },
    idItemAssocie_in: [idItemAssocie],
    user: {
      OR: [
        {
          userTitulaires_some: { idPharmacie: { id_in: idsPharmacies } },
        },
        ...whereIdUsers,
      ],
      AND: [
        {
          groupement: { id: ctx.groupementId },
        },
      ],
    },
  };

  const total = await ctx.prisma
    .commentsConnection({
      where: { ...defaultWhere },
      first: take,
      skip,
      orderBy: 'dateCreation_ASC',
    })
    .aggregate()
    .count();

  const data = await ctx.prisma.comments({
    where: { ...defaultWhere },
    first: take,
    skip,
    orderBy: 'dateCreation_ASC',
  });

  const result: CommentResult = { total, data: data as any };

  return result;
};
