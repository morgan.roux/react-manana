import { FichierInput, Context } from '../types';
import { Operation, OperationArticleCible } from '../generated/prisma-client';

export const createUpdateOperationArticleCible = async (
  ctx: Context,
  operation: Operation,
  fichierProduit?: FichierInput,
  globalite?: boolean,
): Promise<OperationArticleCible> => {
  const operationArticleCible = await ctx.prisma
    .operationArticleCibles({ where: { idOperation: { id: operation.id } } })
    .then(operationArticleCibles =>
      operationArticleCibles && operationArticleCibles.length ? operationArticleCibles[0] : null,
    );

  const operationArticleCibleResult = await ctx.prisma.upsertOperationArticleCible({
    where: {
      id: operationArticleCible && operationArticleCible.id ? operationArticleCible.id : '',
    },
    create: {
      fichierCible: {
        create: {
          chemin: fichierProduit.chemin,
          nomOriginal: fichierProduit.nomOriginal,
          type: fichierProduit.type,
        },
      },
      idOperation: { connect: { id: operation.id } },
      globalite,
    },
    update: {
      fichierCible: {
        update: {
          chemin: fichierProduit.chemin,
          nomOriginal: fichierProduit.nomOriginal,
          type: fichierProduit.type,
        },
      },
      idOperation: { connect: { id: operation.id } },
      globalite,
    },
  });

  return operationArticleCibleResult;
};
