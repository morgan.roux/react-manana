import {
  Fichier,
  FichierCreateInput,
  PartenaireRepresentantCreateInput,
} from '../generated/prisma-client';
import { Context, PartenaireRepresentantInput } from '../types';

export const createUpdatePartenaireRepresentant = async (
  args: PartenaireRepresentantInput,
  ctx: Context,
) => {
  const {
    id,
    nom,
    civilite,
    fonction,
    photo,
    prenom,
    sexe,
    idPartenaire,
    idPartenaireType,
    afficherComme,
    contact: { id: _cId, ...contactRest },
  } = args;

  // Create or update photo
  let savedPhoto: Fichier;
  if (photo) {
    const fichierData: FichierCreateInput = {
      chemin: photo.chemin,
      nomOriginal: photo.nomOriginal,
      type: photo.type,
      avatar: photo.idAvatar ? { connect: { id: photo.idAvatar } } : null,
    };
    savedPhoto = await ctx.prisma.upsertFichier({
      where: { id: photo.id || '' },
      create: fichierData,
      update: fichierData,
    });
  }

  // Create or update contact
  const existContact = await ctx.prisma.partenaireRepresentant({ id: id || '' }).contact();
  const savedContact = await ctx.prisma.upsertContact({
    where: { id: (existContact && existContact.id) || '' },
    create: contactRest,
    update: contactRest,
  });

  const partenaireRepresentantData: PartenaireRepresentantCreateInput = {
    civilite,
    nom,
    prenom,
    sexe,
    fonction,
    afficherComme,
    contact: savedContact ? { connect: { id: savedContact.id } } : null,
    partenaire: idPartenaire ? { connect: { id: idPartenaire } } : null,
    partenaireType: idPartenaireType ? { connect: { id: idPartenaireType } } : null,
    photo: savedPhoto ? { connect: { id: savedPhoto.id } } : null,
  };

  const partenaireRepresentant = await ctx.prisma.upsertPartenaireRepresentant({
    where: { id: id || '' },
    create: partenaireRepresentantData,
    update: partenaireRepresentantData,
  });

  await ctx.indexing.publishSave('partenairerepresentants', partenaireRepresentant.id);
  return partenaireRepresentant;
};

export const softDeletePartenaireRepresentant = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updatePartenaireRepresentant({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async pr => {
      await ctx.indexing.publishSave('partenairerepresentants', pr.id);
      return pr;
    });
};
