import { InformationLiaisonFichierJointCreateInput } from '../generated/prisma-client';
import { Context, InformationLiaisonFichierJointInput } from '../types';

export const createUpdateInformationLiaisonFichierJoint = async (
  ctx: Context,
  input: InformationLiaisonFichierJointInput,
) => {
  const { id, idFichier, idInformationLiaison, isRemoved } = input;
  const data: InformationLiaisonFichierJointCreateInput = {
    fichier: { connect: { id: idFichier } },
    informationLiaison: { connect: { id: idInformationLiaison } },
    isRemoved: isRemoved || false,
    userCreation: ctx.userId ? { connect: { id: ctx.userId } } : null,
    userModification: ctx.userId ? { connect: { id: ctx.userId } } : null,
  };

  const result = await ctx.prisma.upsertInformationLiaisonFichierJoint({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  return result;
};

export const softDeleteInformationLiaisonFichierJoint = async (ctx: Context, id: string) => {
  return ctx.prisma
    .updateInformationLiaisonFichierJoint({ where: { id }, data: { isRemoved: true } })
    .then(async res => {
      // await ctx.indexing.publishSave('informationliaisons', res.id);
      return res;
    });
};
