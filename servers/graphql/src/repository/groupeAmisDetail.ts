import { GroupeAmisDetailCreateInput } from '../generated/prisma-client';
import { Context, GroupeAmisDetailInput } from '../types';

export const createUpdateGroupeAmisDetail = async (input: GroupeAmisDetailInput, ctx: Context) => {
  const { id, idGroupeAmis, idPharmacie } = input;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: GroupeAmisDetailCreateInput = {
    userCreation,
    userModification,
    groupeAmis: idGroupeAmis ? { connect: { id: idGroupeAmis } } : null,
    pharmacie: idPharmacie ? { connect: { id: idPharmacie } } : null,
  };
  const groupeAmisDetail = await ctx.prisma.upsertGroupeAmisDetail({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  await ctx.indexing.publishSave('groupeamisdetails', groupeAmisDetail.id);
  await ctx.indexing.publishSave('groupeamises', idGroupeAmis);
  return groupeAmisDetail;
};

export const softDeleteGroupeAmisDetail = async (id: string, ctx: Context) => {
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const groupeAmisDetail = await ctx.prisma.updateGroupeAmisDetail({
    where: { id },
    data: { isRemoved: true, userCreation, userModification },
  });

  const groupeAmis = await ctx.prisma.groupeAmisDetail({ id }).groupeAmis();

  // Publish to elasticsearch
  await ctx.indexing.publishSave('groupeamisdetails', groupeAmisDetail.id);
  await ctx.indexing.publishSave('groupeamises', groupeAmis.id);
  return groupeAmisDetail;
};
