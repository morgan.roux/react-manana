import { OperationPharmacie, Operation } from '../generated/prisma-client';
import { Context, FichierInput } from '../types';
import logger from '../logging';

export const createUpdateOperationPharmacie = async (
  ctx: Context,
  operation: Operation,
  fichierPharmacie?: FichierInput,
  globalite?: boolean,
): Promise<OperationPharmacie> => {
  const operatonPharmacie = await ctx.prisma
    .operationPharmacies({ where: { idOperation: { id: operation.id } } })
    .then(operationPharmacies =>
      operationPharmacies && operationPharmacies.length ? operationPharmacies[0] : null,
    );

  if (operatonPharmacie && operatonPharmacie.id) {
    const fichier = await ctx.prisma
      .operationPharmacie({ id: operatonPharmacie.id })
      .fichierCible();
    if (fichier && fichier.id) await ctx.prisma.deleteFichier({ id: fichier.id });
  }

  return ctx.prisma
    .upsertOperationPharmacie({
      where: { id: operatonPharmacie && operatonPharmacie.id ? operatonPharmacie.id : '' },
      create: {
        fichierCible: fichierPharmacie
          ? {
              create: {
                chemin: fichierPharmacie.chemin,
                nomOriginal: fichierPharmacie.nomOriginal,
                type: fichierPharmacie.type,
              },
            }
          : null,
        idOperation: { connect: { id: operation.id } },
        globalite,
      },
      update: {
        fichierCible: fichierPharmacie
          ? {
              create: {
                chemin: fichierPharmacie.chemin,
                nomOriginal: fichierPharmacie.nomOriginal,
                type: fichierPharmacie.type,
              },
            }
          : null,
        globalite,
      },
    })
    .then(operationPharma => {
      logger.info(
        `[OPERATION] CREATE UPDATE OPERATION PHARMACIE GLOBALITE : operation => ${JSON.stringify(
          operation,
        )}`,
      );
      return operationPharma;
    });
};
