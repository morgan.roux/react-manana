import { Context } from '../types';
import { ApolloError } from 'apollo-server';

export const createUpdatePublicite = async (
  ctx: Context,
  {
    id,
    libelle,
    description,
    typeEspace,
    origine,
    url,
    ordre,
    codeItem,
    idItemSource,
    image,
    dateDebut,
    dateFin,
  },
) => {
  const data = {
    libelle,
    description,
    typeEspace,
    origine,
    url,
    ordre: ordre ? parseInt(ordre, 10) : null,
    idItem: codeItem
      ? {
          connect: {
            code: codeItem,
          },
        }
      : null,
    idItemAssocie: idItemSource,
    idFichier:
      image && image.chemin
        ? {
            create: {
              chemin: image.chemin,
              nomOriginal: image.nomOriginal,
              type: image.type,
            },
          }
        : null,
    groupement: {
      connect: {
        id: ctx.groupementId,
      },
    },
    dateDebut: dateDebut || null,
    dateFin: dateFin || null,
  };

  if (dateDebut && dateFin && ordre) {
    const exist = await ctx.prisma.$exists.publicite({
      groupement: { id: ctx.groupementId },
      ordre,
      isRemoved: false,
      dateDebut_lte: dateDebut,
      dateFin_gte: dateDebut,
      id_not: id || null,
    });
    if (exist) {
      return new ApolloError(
        'La période d’affichage sur cet emplacement est déjà prise, veuillez choisir un autre emplacement ou changer la période. Merci',
        'ERROR_PUBLICITE_DATE_ORDRE',
      );
    }
  }

  return ctx.prisma
    .upsertPublicite({
      where: {
        id: id ? id : '',
      },
      create: data,
      update: data,
    })
    .then(async publicite => {
      if (publicite && publicite.id) {
        await ctx.indexing.publishSave('publicites', publicite.id);
        const suivis = await ctx.prisma.suiviPublicitaires({
          where: { idPublicite: { id: publicite.id } },
        });
        if (suivis && suivis.length) {
          await ctx.indexing.publishSave('suivipublicitaires', suivis[0].id);
        }
      }
      return publicite;
    });
};
