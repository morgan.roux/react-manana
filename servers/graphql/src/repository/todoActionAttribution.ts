import { TodoActionAttributionCreateInput } from '../generated/prisma-client';
import { Context, TodoActionAttributionInput } from '../types';

export const createUpdateTodoActionAttribution = async (
  input: TodoActionAttributionInput,
  ctx: Context,
) => {
  const {
    id,
    codeMaj,
    isRemoved,
    idAction,
    idUserSource,
    idUserDestination,
    idGroupement,
    idPharmacie,
  } = input;
  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: TodoActionAttributionCreateInput = {
    codeMaj,
    isRemoved,
    action: idAction ? { connect: { id: idAction } } : null,
    userSource: idUserSource ? { connect: { id: idUserSource } } : null,
    userDestination: idUserDestination ? { connect: { id: idUserDestination } } : null,
    userCreation,
    userModification,
    pharmacie: { connect: { id: idPharmacie } },
    groupement: { connect: { id: idGroupement } },
  };
  const todoActionAttribution = await ctx.prisma.upsertTodoActionAttribution({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  // await ctx.indexing.publishSave('todoactionattributions', todoActionAttribution.id);
  return todoActionAttribution;
};

export const softDeleteTodoActionAttribution = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateTodoActionAttribution({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      // await ctx.indexing.publishSave('todoactionattributions', res.id);
      return res;
    });
};
