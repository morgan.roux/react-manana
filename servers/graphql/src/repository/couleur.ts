import { CouleurCreateInput } from '../generated/prisma-client';
import { Context, CouleurInput } from '../types';

export const createUpdateCouleur = async (input: CouleurInput, ctx: Context) => {
  const { id, code, libelle, isRemoved } = input;
  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: CouleurCreateInput = {
    code,
    libelle,
    isRemoved,
    userCreation,
    userModification,
  };
  const couleur = await ctx.prisma.upsertCouleur({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  await ctx.indexing.publishSave('couleurs', couleur.id);
  return couleur;
};

export const softDeleteCouleur = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateCouleur({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      await ctx.indexing.publishSave('couleurs', res.id);
      return res;
    });
};
