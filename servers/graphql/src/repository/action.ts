import { ApolloError } from 'apollo-server';
import { difference, last } from 'lodash';
import moment from 'moment';
import {
  Action,
  ActionCreateInput,
  ActionStatus,
  ActionUpdateInput,
  ActionWhereInput,
  TodoActionEtiquette,
} from '../generated/prisma-client';
import logger from '../logging';
import { publishItemSource } from '../repository/item';
import {
  ActionInput,
  Context,
  CountTodosInput,
  TodoActionAttributionInput,
  TodoActionChangementStatusInput,
  TodoActionEtiquetteInput,
  UpdateActionDateDebutFinInput,
} from '../types';
import { createUpdateActionActivite } from './actionActivite';
import { createComment } from './comment';
import { saveAndPublishNotification } from './notification';
import { createUpdateTodoActionAttribution } from './todoActionAttribution';
import { createUpdateTodoActionChangementStatus } from './todoActionChangementStatus';
import { createUpdateTodoActionEtiquette } from './todoActionEtiquette';
import { todayFilter, todayTeamFilter } from '../util/filter';
import { updateItemScoringForItem } from '../sequelize/shared';
import { federationPublishIndex } from '../sequelize/shared/federation-reindex';

const yesterday = moment()
  .add(-1, 'days')
  .endOf('day')
  .format();
const tomorrow = moment()
  .add(1, 'days')
  .endOf('day')
  .format();
const seventhDay = moment()
  .add(7, 'days')
  .endOf('day')
  .format();

/**
 * Create or Update Action
 *
 * @param {Context} ctx
 * @param {ActionInput} input
 * @return {*}  {(Promise<Action | ApolloError>)}
 */
export const upsertAction = async (
  ctx: Context,
  input: ActionInput,
): Promise<Action | ApolloError> => {
  const {
    id,
    codeItem,
    idItemAssocie,
    idProject,
    description,
    dateDebut,
    dateFin,
    idActionParent,
    codeMaj,
    commentaire,
    idActionType,
    idsEtiquettes,
    idImportance,
    idOrigine,
    idSection,
    isInInbox,
    isInInboxTeam,
    isRemoved,
    ordre,
    status,
    idsUsersDestinations,
    isPrivate,
  } = input;

  logger.info(`PARAMETER TODO ${JSON.stringify(input)}`);

  if (!ctx.userId) {
    return new ApolloError('User ID is required', 'USER_ID_ERROR');
  }

  let actionType = null;
  let item = null;
  let actionParent = null;
  let origine = null;
  let project = null;
  let etiquettes = [];

  if (!idActionType && id) {
    actionType = await ctx.prisma.action({ id }).actionType();
  }

  if (!codeItem && id) {
    item = await ctx.prisma.action({ id }).item();
  }

  if (!idActionParent && id) {
    actionParent = await ctx.prisma.action({ id }).actionParent();
  }

  if (!idOrigine && id) {
    origine = await ctx.prisma.action({ id }).origine();
  }

  if (!idProject && id) {
    project = await ctx.prisma.action({ id }).project();
  }

  /* if (!idSection && id) {
     section = await ctx.prisma.action({ id }).section();
   }*/

  if ((!idsEtiquettes || (idsEtiquettes && !idsEtiquettes.length)) && id) {
    etiquettes = await ctx.prisma
      .todoActionEtiquettes({ where: { action: { id }, isRemoved: false } })
      .then(todoActionEtiquettes => {
        return todoActionEtiquettes && todoActionEtiquettes.length
          ? Promise.all(
              (todoActionEtiquettes || []).map(todoActionEtiquette => {
                return ctx.prisma.todoActionEtiquette({ id: todoActionEtiquette.id }).etiquette();
              }),
            )
          : [];
      });
  }

  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;

  // Get default odrer on create
  let defaultOrder: number = 0;
  if (id) {
    defaultOrder = await ctx.prisma.action({ id }).ordre();
  } else {
    let where: ActionWhereInput = {
      userCreation: { id: ctx.userId },
    };
    if (idProject) where = { ...where, project: { id: idProject } };
    if (isInInbox) where = { ...where, isInInbox };
    if (isInInboxTeam) where = { ...where, isInInboxTeam };
    defaultOrder = await ctx.prisma
      .actionsConnection({ where })
      .aggregate()
      .count();
  }

  // If order and order already belongs to another action,
  // Update the order of the other actions
  if (ordre) {
    const actions = await ctx.prisma.actions({
      where: { userCreation: { id: ctx.userId }, ordre_gte: ordre },
    });
    if (actions && actions.length > 0) {
      await Promise.all(
        (actions || []).map(async act => {
          await ctx.prisma.updateAction({
            where: { id: act.id },
            data: { ordre: act.ordre + 1 },
          });
          await Promise.all([
            ctx.indexing.publishSave('actions', act.id),
            federationPublishIndex('todoaction', 'SAVE', act.id),
          ]);

          updateItemScoringForItem('L', act.id);
        }),
      );
    }
  }

  const userCreationTodo = id ? {} : { userCreation };

  const data: ActionCreateInput = {
    description,
    idItemAssocie,
    importance: { connect: { id: idImportance } },
    pharmacie: { connect: { id: ctx.pharmacieId } },
    codeMaj,
    isInInbox,
    isInInboxTeam,
    isPrivate,
    isRemoved,
    ordre: ordre ? ordre : id ? defaultOrder : defaultOrder + 1,
    ...userCreationTodo,
    userModification,
    item: codeItem ? { connect: { code: codeItem } } : null,
    dateDebut: dateDebut ? dateDebut : null,
    dateFin: dateFin ? dateFin : null,
    groupement,
    actionParent: idActionParent ? { connect: { id: idActionParent } } : null,
    actionType: idActionType ? { connect: { id: idActionType } } : null,
    origine: idOrigine ? { connect: { id: idOrigine } } : null,
    // section: idSection ? { connect: { id: idSection } } : null,
    project: idProject ? { connect: { id: idProject } } : null,
  };

  const dataUpdate: ActionUpdateInput = {
    description,
    idItemAssocie,
    importance: { connect: { id: idImportance } },
    pharmacie: { connect: { id: ctx.pharmacieId } },
    codeMaj,
    isInInbox,
    isInInboxTeam,
    isRemoved,
    isPrivate,
    ordre: ordre ? ordre : id ? defaultOrder : defaultOrder + 1,
    ...userCreationTodo,
    userModification,
    dateDebut: dateDebut ? dateDebut : null,
    dateFin: dateFin ? dateFin : null,
    item: codeItem ? { connect: { code: codeItem } } : item ? { disconnect: true } : null,
    actionParent: idActionParent
      ? { connect: { id: idActionParent } }
      : actionParent
      ? { disconnect: true }
      : null,
    actionType: idActionType
      ? { connect: { id: idActionType } }
      : actionType
      ? { disconnect: true }
      : null,
    origine: idOrigine ? { connect: { id: idOrigine } } : origine ? { disconnect: true } : null,
    // section: idSection ? { connect: { id: idSection } } : section ? { disconnect: true } : null,
    project: idProject ? { connect: { id: idProject } } : project ? { disconnect: true } : null,
  };

  const action = await ctx.prisma.upsertAction({
    where: { id: id ? id : '' },
    create: data,
    update: dataUpdate,
  });

  // Create or Update status (TodoActionChangementStatus)
  if (action) {
    await saveActionStatus(ctx, action.id, status);
  }

  if (action) {
    const subActions = await updateSubActionsProject(
      { idAction: action.id, idProject, idSection },
      ctx,
    );
    if (subActions && subActions.length)
      await Promise.all(
        (subActions || []).map(async actn => {
          await federationPublishIndex('todoaction', 'SAVE', actn.id);
          return ctx.indexing.publishSave('actions', actn.id);
        }),
      );
  }

  /// Create or Update Commentaire (Comment)
  if (action && commentaire && commentaire.content) {
    if (!id) {
      // Create comment (is on create)
      await createComment(
        {
          codeItem: codeItem || 'TODO',
          idItemAssocie: idItemAssocie || action.id,
          content: commentaire.content,
        },
        ctx,
      );
    } else {
      if (commentaire.id) {
        // Update
        await ctx.prisma.updateComment({
          where: { id: commentaire.id },
          data: { content: commentaire.content },
        });
      } else {
        // Create
        await createComment(
          {
            codeItem: codeItem || 'TODO',
            idItemAssocie: idItemAssocie || action.id,
            content: commentaire.content,
          },
          ctx,
        );
      }
    }
  }

  // Create or Update TodoActionEtiquette
  if (action) {
    // On création
    if (!id) {
      await _saveTodoActionEtiquette(ctx, idsEtiquettes, action.id);

      // On edit
    } else {
      // Check existants
      const todoActionEtiquetteExistants = await ctx.prisma.todoActionEtiquettes({
        where: {
          action: { id: action.id },
          userCreation: { id: ctx.userId },
          isRemoved: false,
        },
      });

      if (todoActionEtiquetteExistants && todoActionEtiquetteExistants.length > 0) {
        // Check idsEtiquettes
        if (idsEtiquettes && idsEtiquettes.length > 0) {
          // Compare idsEtiquettes && todoActionEtiquetteExistants
          const idsEtiquettesExistants: string[] = await Promise.all(
            (todoActionEtiquetteExistants || []).map(async item => {
              // Update todoActionEtiquette
              // Exist in DB but not in variables idsEtiquettes
              const todoEtiquette = await ctx.prisma
                .todoActionEtiquette({
                  id: item.id,
                })
                .etiquette();
              return todoEtiquette.id;
            }),
          );

          // Saveds
          const saveds = difference(idsEtiquettes, idsEtiquettesExistants);
          if (saveds && saveds.length > 0) {
            await _saveTodoActionEtiquette(ctx, saveds, action.id);
          }
          // Removeds
          // const intersect = intersection(idsEtiquettes, idsEtiquettesExistants);
          const removeds = difference(idsEtiquettesExistants, idsEtiquettes);
          if (removeds && removeds.length > 0) {
            await _saveTodoActionEtiquette(ctx, removeds, action.id, true);
            await ctx.prisma.deleteManyTodoActionEtiquettes({
              etiquette: { id_in: removeds },
            });
          }
        } else {
          // it's on edit and idsEtiquettes is empty, remove existants
          await Promise.all(
            (todoActionEtiquetteExistants || []).map(async item => {
              await ctx.prisma.updateTodoActionEtiquette({
                where: { id: item.id },
                data: {
                  isRemoved: true,
                },
              });
            }),
          );
        }
      } else {
        // Create TodoActionEtiquette
        await _saveTodoActionEtiquette(ctx, idsEtiquettes, action.id);
      }
    }
  }

  // Create Update : Attributio (Todo_action_attribution)
  if (idsUsersDestinations && action) {
    if (id) {
      // is on Create

      if (idsUsersDestinations.length > 0) {
        // Save attributions
        await _saveTodoActionAttribution(ctx, action.id, idsUsersDestinations);
      }
    } else {
      // is on Edit
      const existAttributions = await ctx.prisma.todoActionAttributions({
        where: {
          action: { id: action.id },
          userSource: { id: ctx.userId },
          userDestination: { id_in: idsUsersDestinations },
        },
      });

      // Get exists user dest ids
      let existsUserDestIds: string[] = [];
      if (existAttributions && existAttributions.length > 0) {
        existsUserDestIds = await Promise.all(
          (existAttributions || []).map(i => {
            return ctx.prisma
              .todoActionAttribution({ id: i.id })
              .userDestination()
              .id();
          }),
        );
      }

      if (idsUsersDestinations.length > 0) {
        // If existsUserDestIds === []
        if (existsUserDestIds.length === 0) {
          // Save attributions
          await _saveTodoActionAttribution(ctx, action.id, idsUsersDestinations);
        } else if (existsUserDestIds.length > 0) {
          // Edit attributions
          // Save or delete attributions
          const commonIds = idsUsersDestinations.filter(i => {
            if (existsUserDestIds.includes(i)) {
              return i;
            }
          });

          // Saved ids
          const savedDiff = difference(existsUserDestIds, commonIds);
          if (savedDiff && savedDiff.length > 0) {
            await _saveTodoActionAttribution(ctx, action.id, savedDiff);
          }

          // Deleted ids
          const deletedDiff = difference(existsUserDestIds, commonIds);
          if (deletedDiff && deletedDiff.length > 0) {
            await _saveTodoActionAttribution(ctx, action.id, savedDiff, true);
          }
        }
      } else {
        // If idsUsersDestinations === [], and  existsUserDestIds.length > 0
        // Remove attributions;
        if (existsUserDestIds.length > 0) {
          await Promise.all(
            (existAttributions || []).map(async attr => {
              const idUserDestination = await ctx.prisma
                .todoActionAttribution({ id: attr.id })
                .id();
              const input: TodoActionAttributionInput = {
                idAction: action.id,
                idUserDestination,
                idUserSource: ctx.userId,
                isRemoved: true,
                idGroupement: ctx.groupementId,
                idPharmacie: ctx.pharmacieId,
              };
              // Update (with id)
              await createUpdateTodoActionAttribution({ id: attr.id, ...input }, ctx);
            }),
          );
        }
      }
    }
  }

  // publishItemSource
  if (idItemAssocie) {
    await publishItemSource(codeItem || 'TODO', idItemAssocie || action.id, ctx);
  }

  // Save action activite if, it's EDIT
  if (id) {
    createUpdateActionActivite(ctx, {
      activiteType: 'EDIT' as any,
      idAction: id,
      idUserSource: ctx.userId,
    });
  }

  if (project && project.id && action) {
    await ctx.indexing.publishSave('projects', project.id);
    await federationPublishIndex('todoprojet', 'SAVE', project.id);
  } else if (idProject) {
    await ctx.indexing.publishSave('projects', idProject);
    await federationPublishIndex('todoprojet', 'SAVE', idProject);
  }

  /* if (section && section.id && action) {
     await ctx.indexing.publishSave('todosections', section.id);
   } else if (idSection) {
     await ctx.indexing.publishSave('todosections', idSection);
   }*/

  if (etiquettes && etiquettes.length && action) {
    await Promise.all(
      (etiquettes || []).map(etiquette =>
        etiquette && etiquette.id ? ctx.indexing.publishSave('todoetiquettes', etiquette.id) : null,
      ),
    );
  } else if (idsEtiquettes && idsEtiquettes.length) {
    await Promise.all(
      (idsEtiquettes || []).map(idEtiq =>
        idEtiq ? ctx.indexing.publishSave('todoetiquettes', idEtiq) : null,
      ),
    );
  }

  await Promise.all([
    ctx.indexing.publishSave('actions', action.id),
    federationPublishIndex('todoaction', 'SAVE', action.id),
  ]);

  updateItemScoringForItem('L', action.id);
  return action;
};

export const updateSubActionsProject = async ({ idAction, idProject, idSection }, ctx: Context) => {
  const subs = await ctx.prisma.actions({ where: { actionParent: { id: idAction } } });

  if (subs && subs.length) {
    return Promise.all(
      subs.map(sub => {
        return ctx.prisma.updateAction({
          where: { id: sub.id },
          data: {
            project: idProject ? { connect: { id: idProject } } : { disconnect: true },
            section: idSection ? { connect: { id: idSection } } : { disconnect: true },
          },
        });
      }),
    );
  }

  return [];
};

export const itemActions = async ({ take, skip, codeItem, idItemAssocie }, ctx: Context) => {
  return ctx.prisma
    .actions({
      first: take,
      skip,
      orderBy: 'dateCreation_ASC',
      where: {
        item: { code: codeItem },
        idItemAssocie,
      },
    })
    .then(async actions => {
      return {
        total: await ctx.prisma
          .actionsConnection({
            where: {
              item: { code: codeItem },
              idItemAssocie,
            },
          })
          .aggregate()
          .count(),
        data: actions,
      };
    });
};

export const assignAction = async (ctx: Context, { id, to }) => {
  return ctx.prisma
    .createActionAssignment({
      idAction: { connect: { id } },
      to: { connect: { id: to } },
      from: { connect: { id: ctx.userId } },
    })
    .then(async actionAssign => {
      const project = await ctx.prisma.action({ id }).project();
      ctx.prisma.action({ id }).then(action =>
        action && action.id && project && project.id
          ? saveAndPublishNotification(
              {
                type: 'TODO_ASSIGN',
                message: `On vient de vous assigner une tâche dans le projet "${project.name}"`,
                seen: false,
                to,
                from: ctx.userId,
                targetId: project.id,
                targetName: project.name,
              },
              ctx,
            )
          : null,
      );

      return ctx.prisma
        .actionAssignment({ id: actionAssign.id })
        .idAction()
        .project()
        .then(async project => {
          await ctx.indexing.publishSave('projects', project.id);
          await federationPublishIndex('todoprojet', 'SAVE', project.id);
          return project;
        });
    })
    .catch(err => {
      logger.error(`Erreur lors de l'affectation de l'action : `, { err });
      return new ApolloError(`Erreur lors de l'affectation de l'action : `, 'ASSIGN_ERROR');
    });
};

export const setActionDueDate = async (ctx: Context, { id, dateDebut }) => {
  return ctx.prisma
    .updateAction({ where: { id }, data: { dateDebut } })
    .then(async action => _publishProject(ctx, action))
    .catch(err => {
      logger.error(`Erreur lors du changement de la date d’échéance l'action : `, { err });
      return new ApolloError(
        `Erreur lors du changement de la date d’échéance de l'action : `,
        'EDIT_DUEDATE_ERROR',
      );
    });
};

export const changeActionStatus = async (ctx: Context, { id, status }) => {
  // TODO
  return null;
  // return ctx.prisma
  //   .updateAction({ where: { id }, data: { status } })
  //   .then(async action => _publishProject(ctx, action))
  //   .catch(err => {
  //     logger.error(`Erreur lors du changement status de l'action : `, { err });
  //     return new ApolloError(
  //       `Erreur lors du changement status de l'action : `,
  //       'EDIT_STATUS_ERROR',
  //     );
  //   });
};

export const softDeleteAction = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma.updateAction({ where: { id }, data: { isRemoved: true } }).then(async res => {
    await Promise.all([
      ctx.indexing.publishSave('actions', res.id),
      federationPublishIndex('todoaction', 'SAVE', res.id),
    ]);
    updateItemScoringForItem('L', res.id);
    return res;
  });
};

const _publishProject = async (ctx: Context, action: Action) => {
  const project = await ctx.prisma.action({ id: action.id }).project();
  await ctx.indexing.publishSave('projects', project.id);
  await federationPublishIndex('todoprojet', 'SAVE', project.id);
  return project;
};

const _saveTodoActionEtiquette = async (
  ctx: Context,
  idsEtiquettes: string[],
  idAction: string,
  isRemoved: boolean = false,
) => {
  await Promise.all(
    (idsEtiquettes || []).map(async idEtiquette => {
      let todoActionEtiquette: TodoActionEtiquette;
      const todoActionEtiquetteInput: TodoActionEtiquetteInput = {
        idAction,
        idEtiquette,
        isRemoved,
      };
      // Create todoActionEtiquette
      todoActionEtiquette = await createUpdateTodoActionEtiquette(todoActionEtiquetteInput, ctx);
      // Publish todoActionEtiquette to elasticsearch
      if (todoActionEtiquette) {
        await ctx.indexing.publishSave('todoactionetiquettes', todoActionEtiquette.id);
      }
    }),
  );
};

const _saveTodoActionAttribution = async (
  ctx: Context,
  idAction: string,
  usersDestIds: string[],
  isRemoved: boolean = false,
) => {
  await ctx.prisma.deleteManyTodoActionAttributions({ action: { id: idAction } });
  await Promise.all(
    (usersDestIds || []).map(async idUserDestination => {
      const input: TodoActionAttributionInput = {
        idAction,
        idUserDestination,
        idUserSource: ctx.userId,
        isRemoved,
        idGroupement: ctx.groupementId,
        idPharmacie: ctx.pharmacieId,
      };
      await createUpdateTodoActionAttribution(input, ctx);
    }),
  );
};

export const saveActionStatus = async (ctx: Context, idAction: string, status?: ActionStatus) => {
  const todoActionChangementStatusExistants = await ctx.prisma.todoActionChangementStatuses({
    where: {
      action: { id: idAction },
      user: { id: ctx.userId },
    },
    orderBy: 'dateCreation_ASC',
  });

  // let todoActionChangementStatus: TodoActionChangementStatus;
  const todoActionChangementStatusInput: TodoActionChangementStatusInput = {
    idAction,
    idUser: ctx.userId,
    status: status || ('ACTIVE' as any),
  };

  if (todoActionChangementStatusExistants && todoActionChangementStatusExistants.length > 0) {
    // Update todoActionChangementStatus
    const todoActionChangementStatusExistant = last(todoActionChangementStatusExistants);
    if (
      status &&
      (todoActionChangementStatusExistant.status !== status ||
        todoActionChangementStatusExistant.status === null)
    ) {
      // Create todoActionChangementStatus (insert a new line)
      await createUpdateTodoActionChangementStatus(todoActionChangementStatusInput, ctx);
    }
  } else {
    // Create todoActionChangementStatus
    await createUpdateTodoActionChangementStatus(todoActionChangementStatusInput, ctx);
  }

  const actions = await updateSubActionsStatus({ idAction, status }, ctx);

  if (actions && actions.length)
    await Promise.all(
      (actions || []).map(actn => {
        federationPublishIndex('todoaction', 'SAVE', actn.id);
        return ctx.indexing.publishSave('actions', actn.id);
      }),
    );
  return ctx.prisma.updateAction({
    where: { id: idAction },
    data: { actionStatus: status || ('ACTIVE' as any) },
  });
};

export const updateSubActionsStatus = async ({ idAction, status }, ctx: Context) => {
  const subs = await ctx.prisma.actions({ where: { actionParent: { id: idAction } } });

  if (subs && subs.length) {
    return Promise.all(
      subs.map(async sub => {
        return ctx.prisma.updateAction({
          where: { id: sub.id },
          data: { actionStatus: status || ('ACTIVE' as any) },
        });
      }),
    );
  }

  return [];
};

export const updateActionDateDebutFin = async (
  ctx: Context,
  input: UpdateActionDateDebutFinInput,
) => {
  const { idAction, dateDebut, dateFin } = input;
  const action = await ctx.prisma.action({ id: idAction });
  if (action) {
    // Save actions activites
    const oldDateDebut = await ctx.prisma.action({ id: idAction }).dateDebut();
    const oldDateFin = await ctx.prisma.action({ id: idAction }).dateFin();
    if (moment(oldDateDebut).format() !== moment(dateDebut).format()) {
      createUpdateActionActivite(ctx, {
        activiteType: 'CHANGE_DATE_DEBUT' as any,
        idAction,
        idUserSource: ctx.userId,
        oldDateDebut,
        newDateDebut: dateDebut,
      });
    }
    if (moment(oldDateFin).format() !== moment(dateFin).format()) {
      createUpdateActionActivite(ctx, {
        activiteType: 'CHANGE_DATE_FIN' as any,
        idAction,
        idUserSource: ctx.userId,
        oldDateFin,
        newDateFin: dateFin,
      });
    }
    await ctx.prisma.updateAction({ where: { id: idAction }, data: { dateDebut, dateFin } });
    await ctx.indexing.publishSave('actions', action.id);
    federationPublishIndex('todoaction', 'SAVE', action.id);
    updateItemScoringForItem('L', action.id);
  }
  return action;
};

export const countActionByUser = (ctx: Context, input: CountTodosInput) => {
  const { idUser } = input;
  const othersWhere: ActionWhereInput = _getActionWhereInput(input, false, false, false);
  const where: ActionWhereInput = {
    OR: [
      { isRemoved: false, userCreation: { id: idUser }, ...othersWhere },
      {
        actionAttributions_some: {
          isRemoved: false,
          userDestination: { id_in: [idUser] },
          action: { ...othersWhere, isRemoved: false },
        },
      },
    ],
  };
  return ctx.prisma
    .actionsConnection({ where: { ...where } })
    .aggregate()
    .count();
};

export const countActionByUserAndPriority = (
  ctx: Context,
  priority: number,
  input: CountTodosInput,
) => {
  const { idUser } = input;
  const othersWhere: ActionWhereInput = _getActionWhereInput(input, false, false, false);
  const where: ActionWhereInput = {
    OR: [
      { isRemoved: false, userCreation: { id: idUser }, ...othersWhere },
      {
        actionAttributions_some: {
          isRemoved: false,
          action: { ...othersWhere },
          userDestination: { id_in: [idUser] },
        },
      },
    ],
  };
  return ctx.prisma
    .actionsConnection({ where: { ...where } })
    .aggregate()
    .count();
};

export const countNotAssignedActions = async (ctx: Context, input: CountTodosInput) => {
  const { idUser } = input;
  // const notAssignedActions = await ctx.prisma
  //   .actions({
  //     where: { isRemoved: false, userCreation: { id: idUser } },
  //   })
  //   .then(results => {
  //     return Promise.all(
  //       results.map(async action => {
  //         const isExist = await ctx.prisma.$exists.todoActionAttribution({
  //           action: { id: action.id },
  //         });
  //         if (!isExist) return action;
  //       }),
  //     );
  //   });

  // return notAssignedActions.length;

  const othersWhere: ActionWhereInput = _getActionWhereInput(input, false, false, false);
  const where: ActionWhereInput = {
    isRemoved: false,
    userCreation: { id: idUser },
    ...othersWhere,
    actionAttributions_none: {
      isRemoved: false,
      userSource: { id: idUser },
      action: { ...othersWhere },
    },
  };
  return ctx.prisma
    .actionsConnection({ where: { ...where } })
    .aggregate()
    .count();
};

export const countActionInInbox = async (ctx: Context, input: CountTodosInput) => {
  const { idUser } = input;
  const othersWhere: ActionWhereInput = _getActionWhereInput(
    input,
    true,
    true,
    true,
    false,
    true,
    true,
  );
  const where: ActionWhereInput = {
    OR: [
      { isRemoved: false, userCreation: { id: idUser }, isInInbox: true, ...othersWhere },
      {
        actionAttributions_some: {
          isRemoved: false,
          action: { isInInbox: true, ...othersWhere },
          userDestination: { id_in: [idUser] },
        },
      },
    ],
  };
  return ctx.prisma
    .actionsConnection({ where: { ...where } })
    .aggregate()
    .count();
};

export const countActionInInboxTeam = async (ctx: Context, input: CountTodosInput) => {
  const { idUser } = input;
  const othersWhere: ActionWhereInput = _getActionWhereInput(
    input,
    true,
    true,
    true,
    false,
    true,
    true,
  );
  const where: ActionWhereInput = {
    isRemoved: false,
    userCreation: { id: idUser },
    isInInboxTeam: true,
    ...othersWhere,
    actionAttributions_some: {
      OR: [{ userDestination: { id_in: [idUser] } }, { userSource: { id_in: [idUser] } }],
      action: { isInInboxTeam: true, ...othersWhere },
      isRemoved: false,
    },
  };
  return ctx.prisma
    .actionsConnection({ where: { ...where } })
    .aggregate()
    .count();
};
//

export const countActionToday = async (ctx: Context, input: CountTodosInput) => {
  const { idUser } = input;
  const othersWhere: ActionWhereInput = _getActionWhereInput(
    input,
    true,
    true,
    true,
    false,
    true,
    true,
  );
  const where: ActionWhereInput = {
    OR: [
      {
        isRemoved: false,
        userCreation: { id: idUser },
        // dateEcheance_gt: yesterday,
        dateDebut_lt: tomorrow,
        dateDebut_not: null,
        ...othersWhere,
      },
      {
        actionAttributions_some: {
          isRemoved: false,
          userDestination: { id_in: [idUser] },
          action: {
            AND: [
              // { dateEcheance_gt: yesterday },
              { dateDebut_lt: tomorrow },
              { dateDebut_not: null },
            ],
            ...othersWhere,
          },
        },
      },
    ],
  };
  return ctx.prisma
    .actionsConnection({ where: { ...where } })
    .aggregate()
    .count();
};

export const countActionTodayTeam = async (ctx: Context, input: CountTodosInput) => {
  const { idUser } = input;
  const othersWhere: ActionWhereInput = _getActionWhereInput(
    input,
    true,
    true,
    true,
    false,
    true,
    true,
  );
  const where: ActionWhereInput = {
    isRemoved: false,
    ...othersWhere,
    actionAttributions_some: {
      OR: [{ userDestination: { id_in: [idUser] } }, { userSource: { id_in: [idUser] } }],
      isRemoved: false,
      action: { ...othersWhere },
    },
    AND: [
      // { dateEcheance_gt: yesterday },
      { dateDebut_lt: tomorrow },
      { dateDebut_not: null },
    ],
  };

  return ctx.prisma
    .actionsConnection({ where: { ...where } })
    .aggregate()
    .count();
};

export const countNextSeventDaysActions = async (ctx: Context, input: CountTodosInput) => {
  const { idUser } = input;
  const othersWhere: ActionWhereInput = _getActionWhereInput(
    input,
    true,
    true,
    true,
    false,
    true,
    true,
  );
  const where: ActionWhereInput = {
    OR: [
      {
        isRemoved: false,
        userCreation: { id: idUser },
        dateDebut_gte: tomorrow,
        dateDebut_lte: seventhDay,
        dateDebut_not: null,
        ...othersWhere,
      },
      {
        actionAttributions_some: {
          isRemoved: false,
          userDestination: { id_in: [idUser] },
          action: {
            ...othersWhere,
            AND: [
              { dateDebut_gte: tomorrow },
              { dateDebut_lte: seventhDay },
              { dateDebut_not: null },
            ],
          },
        },
      },
    ],
  };
  return ctx.prisma
    .actionsConnection({ where: { ...where } })
    .aggregate()
    .count();
};

export const countActionWithDateEcheance = async (ctx: Context, input: CountTodosInput) => {
  const { idUser } = input;
  const othersWhere: ActionWhereInput = _getActionWhereInput(input);
  const where: ActionWhereInput = {
    OR: [
      { isRemoved: false, userCreation: { id: idUser }, dateDebut_not: null, ...othersWhere },
      {
        actionAttributions_some: {
          AND: [
            {
              userDestination: {
                id_not: idUser,
                groupement: { id: ctx.groupementId },
              },
            },
            {
              userDestination: {
                id_not: '',
                groupement: { id: ctx.groupementId },
              },
            },
          ],
          isRemoved: false,
          action: { dateDebut_not: null, ...othersWhere },
          userDestination: { id_in: [idUser] },
        },
        AND: [{ dateDebut_lt: tomorrow }, { dateDebut_not: null }],
      },
    ],
  };
  return ctx.prisma
    .actionsConnection({ where: { ...where } })
    .aggregate()
    .count();
};

export const countActionWithoutDateEcheance = async (ctx: Context, input: CountTodosInput) => {
  const { idUser } = input;
  const othersWhere: ActionWhereInput = _getActionWhereInput(input);
  const where: ActionWhereInput = {
    OR: [
      { isRemoved: false, userCreation: { id: idUser }, dateDebut: null, ...othersWhere },
      {
        actionAttributions_some: {
          isRemoved: false,
          action: { dateDebut: null, ...othersWhere },
          userDestination: { id_in: [idUser] },
        },
      },
    ],
  };
  return ctx.prisma
    .actionsConnection({ where: { ...where } })
    .aggregate()
    .count();
};

export const countActionsOfOthers = async (ctx: Context, input: CountTodosInput) => {
  const { idUser } = input;
  const othersWhere: ActionWhereInput = _getActionWhereInput(input);
  const where: ActionWhereInput = { isRemoved: false, userCreation: { id_not: idUser } };
  return ctx.prisma
    .actionsConnection({ where: { ...where, ...othersWhere } })
    .aggregate()
    .count();
};

const _getActionWhereInput = (
  input: CountTodosInput,
  withPriority: boolean = false,
  withUnassignedTask: boolean = false,
  checkDate: boolean = false,
  withProjectType: boolean = false,
  withMyTask: boolean = false,
  withTaskOfOthers: boolean = false,
) => {
  const {
    idUser,
    // priorities,
    typesProjects,
    unassignedTask,
    withDate,
    withoutDate,
    myTask,
    taskOfOthers,
  } = input;

  let where: ActionWhereInput;

  if (myTask && withMyTask) {
    where = {
      ...where,
      OR: [
        { userCreation: { id: idUser } },
        {
          actionAttributions_some: {
            userSource: { id_in: [idUser] },
          },
        },
      ],
    };
  }

  if (taskOfOthers && withTaskOfOthers) {
    where = {
      ...where,
      OR: [
        {
          actionAttributions_some: {
            userDestination: { id_in: [idUser] },
          },
        },
      ],
    };
  }

  /*  if (taskStatuses && taskStatuses.length > 0) {
    where = { ...where, statuses_some: { isRemoved: false, status_in: taskStatuses } };
  } */

  /*if (priorities && priorities.length > 0 && withPriority) {
    where = { ...where};
  }*/

  if (typesProjects && typesProjects.length > 0 && withProjectType) {
    where = { ...where, project: { typeProject_in: typesProjects } };
  }

  if (unassignedTask && withUnassignedTask) {
    where = {
      ...where,
      actionAttributions_none: {
        isRemoved: false,
        OR: [{ userDestination: { id_in: [idUser] } }, { userSource: { id_in: [idUser] } }],
      },
    };
  }

  if (checkDate) {
    if (withDate && !withoutDate) {
      where = { ...where, dateDebut_not: null };
    }

    if (withoutDate && !withDate) {
      where = { ...where, dateDebut: null };
    }
  }

  return where;
};

export const countActionTeamFromSearch = async (
  ctx: Context,
  input: CountTodosInput,
  { isToday, isSevenDays }: { isToday?: boolean; isSevenDays?: boolean },
) => {
  const { idUser, /* priorities,*/ taskStatus, useMatriceResponsabilite = false } = input;

  /*const sevenDays = moment()
    .add(7, 'days')
    .startOf('day')
    .format('YYYY-MM-DD');
    */

  const termActive = taskStatus
    ? taskStatus === 'ACTIVE'
      ? [{ term: { status: 'ACTIVE' } }]
      : [{ term: { status: 'DONE' } }]
    : [{ term: { status: 'ACTIVE' } }];

  const todayMust = isToday
    ? [{ range: { dateDebut: { lte: 'now/d' } } }, { exists: { field: 'dateDebut' } }]
    : [];

  const sevenMust = isSevenDays
    ? [
        { range: { dateDebut: { gt: 'now/d' } } },
        //  { range: { dateDebut: { lte: sevenDays } } },
      ]
    : [];

  const must = [
    { term: { isRemoved: false } },
    ...termActive,
    // { terms: { priority: priorities } },
    ...todayMust,
    ...sevenMust,
  ];

  const defaultShouldOtherTeam = await getDefaultShouldTerm(
    ctx,
    idUser,
    useMatriceResponsabilite,
    isToday || isSevenDays ? false : true,
  );

  return searchTodos(ctx, must, [], [...defaultShouldOtherTeam]);
};

export const getDefaultShouldTerm = async (
  ctx: Context,
  idUser: string,
  useMatriceResponsabilite?: boolean,
  inBox?: boolean,
) => {
  const { defaultShouldAssignTeam, defaultShouldCreatedTeam } = await getUserSource(ctx, idUser);

  if (inBox)
    return [
      {
        bool: {
          must_not: [
            {
              exists: {
                field: 'assignedUsers',
              },
            },
            {
              exists: {
                field: 'project',
              },
            },
          ],
          must: [
            {
              term: { 'userCreation.id': idUser },
            },
            {
              term: {
                isInInboxTeam: true,
              },
            },
          ],
        },
      },
      {
        bool: {
          must_not: [
            {
              exists: {
                field: 'assignedUsers',
              },
            },
            {
              exists: {
                field: 'project',
              },
            },
            {
              term: { 'userCreation.id': idUser },
            },
          ],
          must: [...defaultShouldCreatedTeam],
        },
      },
      {
        bool: {
          must_not: [
            {
              exists: {
                field: 'assignedUsers',
              },
            },
          ],
          must: [
            {
              term: { 'userCreation.id': idUser },
            },
            {
              exists: {
                field: 'project',
              },
            },
            {
              exists: {
                field: 'project.participants',
              },
            },
          ],
        },
      },
      {
        bool: {
          must_not: [
            {
              exists: {
                field: 'assignedUsers',
              },
            },
          ],
          must: [
            {
              term: { 'project.participants.id': idUser },
            },
            {
              exists: {
                field: 'project',
              },
            },
            {
              exists: {
                field: 'project.participants',
              },
            },
          ],
        },
      },
    ];
  return todayTeamFilter({
    idUser,
    useMatriceResponsabilite,
    defaultShouldCreatedTeam,
    defaultShouldAssignTeam,
  });
};

export const getUserSource = async (ctx: Context, idUser: string) => {
  let idPharmacie = '';
  let idService = '';

  const personnels = await ctx.prisma.personnels({ where: { user: { id: idUser } } });
  const ppersonnels = await ctx.prisma.ppersonnels({ where: { user: { id: idUser } } });
  const userTitulaires = await ctx.prisma.userTitulaires({ where: { idUser: { id: idUser } } });

  idService =
    personnels && personnels.length
      ? await ctx.prisma
          .personnel({ id: personnels[0].id })
          .service()
          .id()
      : '';
  if (ppersonnels && ppersonnels.length) {
    idPharmacie = await ctx.prisma
      .ppersonnel({ id: ppersonnels[0].id })
      .idPharmacie()
      .id();
  }
  if (userTitulaires && userTitulaires.length) {
    idPharmacie = await ctx.prisma
      .userTitulaire({ id: userTitulaires[0].id })
      .idPharmacie()
      .id();
  }

  const defaultShouldAssignTeam = idService
    ? [
        {
          term: {
            'assignedUsers.userPersonnel.service.id': idService,
          },
        },
      ]
    : idPharmacie
    ? [
        {
          term: {
            'assignedUsers.pharmacie.id': idPharmacie,
          },
        },
      ]
    : [
        { term: { 'assignedUsers.idGroupement': ctx.groupementId } },
        { term: { 'userCreation.idGroupement': ctx.groupementId } },
      ];

  const defaultShouldCreatedTeam = idService
    ? [
        {
          term: {
            'userCreation.userPersonnel.service.id': idService,
          },
        },
      ]
    : idPharmacie
    ? [
        {
          term: {
            'userCreation.pharmacie.id': idPharmacie,
          },
        },
      ]
    : [];

  return {
    idService,
    idPharmacie,
    defaultShouldAssignTeam,
    defaultShouldCreatedTeam,
  };
};

export const countMyActionFromSearch = async (
  ctx: Context,
  input: CountTodosInput,
  {
    isToday,
    isSevenDays,
    isWithoutDates,
  }: { isToday?: boolean; isSevenDays?: boolean; isWithoutDates?: boolean },
) => {
  const { idUser, /* priorities,*/ taskStatus, useMatriceResponsabilite = false } = input;
  /* const sevenDays = moment()
     .add(7, 'days')
     .startOf('day')
     .format('YYYY-MM-DD');
 */
  const termActive = taskStatus
    ? taskStatus === 'ACTIVE'
      ? [{ term: { status: 'ACTIVE' } }]
      : [{ term: { status: 'DONE' } }]
    : [{ term: { status: 'ACTIVE' } }];

  const todayMust = isToday
    ? [{ range: { dateDebut: { lte: 'now/d' } } }, { exists: { field: 'dateDebut' } }]
    : [];

  const sevenMust = isSevenDays
    ? [
        { range: { dateDebut: { gt: 'now/d' } } },
        //  { range: { dateDebut: { lte: sevenDays } } },
      ]
    : [];
  const must = [
    { term: { isRemoved: false } },
    ...termActive,
    // { terms: { priority: priorities } },
    ...todayMust,
    ...sevenMust,
  ];

  const mustNot =
    isToday || isSevenDays ? [] : isWithoutDates ? [{ exists: { field: 'dateDebut' } }] : [];
  const defaultShould =
    isToday || isSevenDays || isWithoutDates
      ? todayFilter({ idUser, useMatriceResponsabilite })
      : [
          {
            bool: {
              must_not: [
                {
                  exists: {
                    field: 'assignedUsers',
                  },
                },
                {
                  exists: {
                    field: 'project',
                  },
                },
              ],
              must: [
                {
                  term: { 'userCreation.id': idUser },
                },
                {
                  term: {
                    isInInboxTeam: false,
                  },
                },
              ],
            },
          },
          {
            bool: {
              must_not: [
                {
                  exists: {
                    field: 'assignedUsers',
                  },
                },
                {
                  exists: {
                    field: 'project',
                  },
                },
              ],
              must: [
                {
                  term: { 'userCreation.id': idUser },
                },
                {
                  term: {
                    isInInbox: true,
                  },
                },
                {
                  term: {
                    isInInboxTeam: false,
                  },
                },
              ],
            },
          },
        ];
  return searchTodos(ctx, must, mustNot, defaultShould);
};

export const countActionFromSearch = async (
  ctx: Context,
  input: CountTodosInput,
  {
    isAll,
    isPriority,
    // priority,
    isStatus,
    status,
  }: {
    isAll?: boolean;
    isPriority?: boolean;
    priority?: number;
    isStatus?: boolean;
    status?: string;
  },
) => {
  const { idUser, /*priorities,*/ taskStatus } = input;
  const termActive = isStatus
    ? [{ term: { status } }]
    : taskStatus
    ? taskStatus === 'ACTIVE'
      ? [{ term: { status: 'ACTIVE' } }]
      : [{ term: { status: 'DONE' } }]
    : [{ term: { status: 'ACTIVE' } }];

  /*
const mustPriority = isPriority
? [{ terms: { priority: [priority] } }]
: [{ terms: { priority: priorities } }];
*/

  const must = [{ term: { isRemoved: false } }, ...termActive];

  const { defaultShouldCreatedTeam, defaultShouldAssignTeam } = await getUserSource(
    ctx,
    ctx.userId,
  );

  const should = isAll
    ? [
        ...todayFilter({ idUser: ctx.userId, useMatriceResponsabilite: true }),
        ...todayTeamFilter({
          idUser: ctx.userId,
          useMatriceResponsabilite: true,
          defaultShouldCreatedTeam,
          defaultShouldAssignTeam,
        }),
      ]
    : [
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'assignedUsers',
                },
              },
            ],
            must: {
              term: { 'userCreation.id': idUser },
            },
          },
        },
        { term: { 'assignedUsers.id': idUser } },
      ];

  return searchTodos(ctx, must, [], should, 1);
};

export const searchTodos = async (
  ctx: Context,
  must: any[],
  mustNot: any[],
  should: any[],
  minimumShouldMatch?: number,
) => {
  const searchParams = {
    body: {
      query: {
        bool: {
          must: must?.length > 0 ? must : undefined,
          must_not: mustNot?.length > 0 ? mustNot : undefined,
          should: should?.length > 0 ? should : undefined,
          minimum_should_match:
            should?.length > 0 ? (minimumShouldMatch ? minimumShouldMatch : 1) : undefined,
        },
      },
    },
    index: ['action'],
  };

  return ctx.indexing
    .getElasticSearchClient()
    .search(searchParams)
    .then(result => {
      return result.body.hits.total.value ? parseInt(result.body.hits.total.value, 10) : 0;
    });
};
