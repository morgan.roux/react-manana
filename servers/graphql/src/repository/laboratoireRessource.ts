import {
  Fichier,
  FichierCreateInput,
  LaboratoireRessourceCreateInput,
} from '../generated/prisma-client';
import { Context, LaboratoireRessourceInput } from '../types';

export const createUpdateLaboratoireRessource = async (
  args: LaboratoireRessourceInput,
  ctx: Context,
) => {
  const {
    id,
    code,
    dateChargement,
    fichier,
    libelle,
    url,
    idItem,
    typeRessource,
    idLaboratoire,
  } = args;

  console.log('_________', JSON.stringify(args));

  // Create or update fichier
  let savedFile: Fichier;
  if (fichier) {
    const fichierData: FichierCreateInput = {
      chemin: fichier.chemin,
      nomOriginal: fichier.nomOriginal,
      type: fichier.type,
    };
    savedFile = await ctx.prisma.upsertFichier({
      where: { id: fichier.id || '' },
      create: fichierData,
      update: fichierData,
    });
  }

  const ressourceData: LaboratoireRessourceCreateInput = {
    code,
    dateChargement,
    libelle,
    url,
    typeRessource,
    item: idItem ? { connect: { id: idItem } } : null,
    fichier: savedFile ? { connect: { id: savedFile.id } } : null,
    laboratoire: idLaboratoire ? { connect: { id: idLaboratoire } } : null,
  };

  const ressource = await ctx.prisma.upsertLaboratoireRessource({
    where: { id: id || '' },
    create: ressourceData,
    update: ressourceData,
  });

  await ctx.indexing.publishSave('laboratoireressources', ressource.id);
  return ressource;
};

export const softDeleteLaboratoireRessource = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateLaboratoireRessource({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async ressource => {
      await ctx.indexing.publishSave('laboratoireressources', ressource.id);
      return ressource;
    });
};
