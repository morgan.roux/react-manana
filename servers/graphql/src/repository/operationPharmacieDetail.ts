import {
  Operation,
  OperationPharmacie,
  OperationPharmacieDetail,
} from '../generated/prisma-client';
import { Context } from '../types';
import logger from '../logging';
import { publishOperation } from './operation';

export const createUpdateOperationPharmacieDetail = async (
  operation: Operation,
  ctx: Context,
  idPharmacie: string,
  operationPharmacie: OperationPharmacie,
): Promise<OperationPharmacieDetail> => {
  const operatonPharmacieDetail = await ctx.prisma
    .operationPharmacieDetails({
      where: {
        idOperation: { id: operation.id },
        idPharmacie: { id: idPharmacie },
      },
    })
    .then(operationPharmacieDetails =>
      operationPharmacieDetails && operationPharmacieDetails.length
        ? operationPharmacieDetails[0]
        : null,
    );

  return ctx.prisma
    .upsertOperationPharmacieDetail({
      where: {
        id: operatonPharmacieDetail && operatonPharmacieDetail.id ? operatonPharmacieDetail.id : '',
      },
      create: {
        idOperation: {
          connect: {
            id: operation.id,
          },
        },
        idOperationPharmacie: {
          connect: {
            id: operationPharmacie.id,
          },
        },
        idPharmacie: {
          connect: {
            id: idPharmacie,
          },
        },
      },
      update: {
        idOperation: {
          connect: {
            id: operation.id,
          },
        },
        idOperationPharmacie: {
          connect: {
            id: operationPharmacie.id,
          },
        },
        idPharmacie: {
          connect: {
            id: idPharmacie,
          },
        },
      },
    })
    .then(result => {
      logger.info(
        `[PHARMACIE DETAIL] ==> ID: ${parseInt(idPharmacie, 10)} , result: ${JSON.stringify(
          result,
        )}`,
      );
      return result;
    });
};

export const createPharmacieCibles = async (
  ctx: Context,
  operation: Operation,
  operationPharmacie: OperationPharmacie,
  idsPharmacie: any[],
) => {
  return Promise.all(
    idsPharmacie.map(idPharmacie =>
      createUpdateOperationPharmacieDetail(operation, ctx, idPharmacie, operationPharmacie),
    ),
  ).then(async _ => {
    logger.info(
      `[OPERATION] CREATE UPDATE OPERATION PHARMACIE FROM SELECT CIBLE : operation => ${JSON.stringify(
        operation,
      )}`,
    );
    await publishOperation(ctx, operation.id);
    return _;
  });
};

export const countOperationCiblePharmacies = async (ctx: Context, { ids }: { ids: string[] }) => {
  const operationPharmacie = await ctx.prisma
    .operationPharmacies({ where: { idOperation: { id_in: ids } } })
    .then(ocPharmas => (ocPharmas && ocPharmas.length ? ocPharmas[0] : null));

  if (operationPharmacie && operationPharmacie.globalite) {
    const nbPharma = await ctx.prisma
      .pharmaciesConnection({
        where: {
          groupement: {
            id: ctx.groupementId,
          },
        },
      })
      .aggregate()
      .count();

    return nbPharma;
  }

  return ctx.prisma
    .operationPharmacieDetailsConnection({
      where: {
        idOperation: { id_in: ids },
      },
    })
    .aggregate()
    .count();
};
