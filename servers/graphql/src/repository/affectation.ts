import { Context } from '../types';
import { ApolloError } from 'apollo-server';
import { MutationResolvers } from '../generated/graphqlgen';
import {
  PersonnelAffectationCreateInput,
  TitulaireAffectationCreateInput,
  PersonnelDemandeAffectationCreateInput,
  TitulaireDemandeAffectationCreateInput,
} from '../generated/prisma-client';

export const createUpdatePersonnelAffectation = async (
  ctx: Context,
  { inputs }: MutationResolvers.ArgsCreateUpdatePersonnelAffectation,
) => {
  const {
    id,
    typeAffectation,
    idsDepartements,
    idFonction,
    idAffecte,
    idRemplacent,
    dateDebut,
    dateFin,
  } = inputs;

  const errCode = id ? 'ERROR_EDIT_PERSONNEL_AFFECTATION' : 'ERROR_CREATION_PERSONNEL_AFFECTATION';
  const errMsg = id ? 'modification' : 'création';

  if (idsDepartements.length > 0) {
    console.log('idsDepartements :>> ', idsDepartements);
    const remplacent = idRemplacent ? await ctx.prisma.personnel({ id: idRemplacent }) : null;
    const service = idRemplacent
      ? await ctx.prisma.personnel({ id: idRemplacent }).service()
      : null;
    console.log('remplacent :>> ', remplacent);

    // Insert remplacent to Personnel_demande_affectation

    try {
      let personnelDemandeAffectation = await ctx.prisma
        .personnelAffectation({ id })
        .personnelDemandeAffectation();

      if (remplacent) {
        const remplacentData: PersonnelDemandeAffectationCreateInput = {
          dateHeureDemande: new Date().toISOString(),
          personnelFonction: { connect: { id: idFonction } },
          typeAffectation,
          userCreation: { connect: { id: ctx.userId } },
          service: service && service.id ? { connect: { id: service.id } } : null,
          personnelRemplacent:
            remplacent && remplacent.id ? { connect: { id: remplacent.id } } : null,
        };

        console.log('remplacentData :>> ', remplacentData);

        personnelDemandeAffectation = await ctx.prisma.upsertPersonnelDemandeAffectation({
          where: { id: personnelDemandeAffectation ? personnelDemandeAffectation.id : '' },
          create: remplacentData,
          update: { ...remplacentData, userModification: { connect: { id: ctx.userId } } },
        });
      } else if (!remplacent && personnelDemandeAffectation) {
        await ctx.prisma.deletePersonnelDemandeAffectation({ id: personnelDemandeAffectation.id });
      }

      const results = await Promise.all(
        idsDepartements.map(async (idDepartement: string) => {
          const departement = await ctx.prisma.departement({ id: idDepartement });

          const data: PersonnelAffectationCreateInput = {
            typeAffectation,
            departement: { connect: { id: idDepartement } },
            personnelFonction: { connect: { id: idFonction } },
            personnel: { connect: { id: idAffecte } },
            personnelDemandeAffectation: personnelDemandeAffectation
              ? { connect: { id: personnelDemandeAffectation.id } }
              : null,
            dateFin,
            dateDebut,
            codeDepartement: (departement && departement.code) || null,
            userCreation: { connect: { id: ctx.userId } },
          };

          console.log('data :>> ', JSON.stringify(data));

          try {
            const personnelAffectation = await ctx.prisma.upsertPersonnelAffectation({
              where: { id: id || '' },
              create: data,
              update: { ...data, userModification: { connect: { id: ctx.userId } } },
            });

            await ctx.indexing.publishSave('personnelaffectations', personnelAffectation.id);
            return personnelAffectation;
          } catch (error) {
            console.log('error :>> ', error);
            throw new ApolloError(`Erreur lors de la ${errMsg} de l'affectation`, errCode);
          }
        }),
      );

      console.log('results :>> ', results);
      return results;
    } catch (error) {
      console.log('Remplacent error :>> ', error);
      throw new ApolloError(`Erreur lors de la ${errMsg} de l'affectation`, errCode);
    }
  } else {
    throw new ApolloError(`Aucun département`, 'EMPTY_IDS_DEPARTEMENTS');
  }
};

export const createUpdateTitulaireAffectation = async (
  ctx: Context,
  { inputs }: MutationResolvers.ArgsCreateUpdateTitulaireAffectation,
) => {
  const {
    id,
    typeAffectation,
    idsDepartements,
    idFonction,
    idAffecte,
    idRemplacent,
    dateDebut,
    dateFin,
  } = inputs;

  const errCode = id ? 'ERROR_EDIT_TITULAIRE_AFFECTATION' : 'ERROR_CREATION_TITULAIRE_AFFECTATION';
  const errMsg = id ? 'modification' : 'création';

  if (idsDepartements.length > 0) {
    console.log('idsDepartements :>> ', idsDepartements);
    const remplacent = idRemplacent ? await ctx.prisma.titulaire({ id: idRemplacent }) : null;
    console.log('idRemplacent :>> ', idRemplacent);
    console.log('remplacent :>> ', remplacent);

    try {
      let titulaireDemandeAffectation = await ctx.prisma
        .titulaireAffectation({ id })
        .titulaireDemandeAffectation();
      // Insert remplacent to Titulaire_demande_affectation
      if (remplacent) {
        const remplacentData: TitulaireDemandeAffectationCreateInput = {
          dateHeureDemande: new Date().toISOString(),
          titulaireFonction: { connect: { id: idFonction } },
          typeAffectation,
          userCreation: { connect: { id: ctx.userId } },
          userModification: { connect: { id: ctx.userId } },
          // service: service && service.id ? { connect: { id: service.id } } : null,
          titulaireRemplacent:
            remplacent && remplacent.id ? { connect: { id: remplacent.id } } : null,
        };
        titulaireDemandeAffectation = await ctx.prisma.upsertTitulaireDemandeAffectation({
          where: { id: titulaireDemandeAffectation ? titulaireDemandeAffectation.id : '' },
          create: remplacentData,
          update: { ...remplacentData, userModification: { connect: { id: ctx.userId } } },
        });

        console.log(
          'titulaireDemandeAffectation :>> ',
          JSON.stringify(titulaireDemandeAffectation),
        );
      } else if (!remplacent && titulaireDemandeAffectation) {
        await ctx.prisma.deleteTitulaireDemandeAffectation({ id: titulaireDemandeAffectation.id });
      }

      const results = await Promise.all(
        idsDepartements.map(async (idDepartement: string) => {
          const departement = await ctx.prisma.departement({ id: idDepartement });

          const data: TitulaireAffectationCreateInput = {
            typeAffectation,
            departement: { connect: { id: idDepartement } },
            titulaireFonction: { connect: { id: idFonction } },
            titulaire: { connect: { id: idAffecte } },
            titulaireDemandeAffectation: titulaireDemandeAffectation
              ? { connect: { id: titulaireDemandeAffectation.id } }
              : null,
            dateFin,
            dateDebut,
            codeDepartement: (departement && departement.code) || null,
            userCreation: { connect: { id: ctx.userId } },
            userModification: { connect: { id: ctx.userId } },
          };

          console.log('data :>> ', JSON.stringify(data));

          try {
            const titulaireAffectation = await ctx.prisma.upsertTitulaireAffectation({
              where: { id: id || '' },
              create: data,
              update: data,
            });

            console.log('titulaireAffectation :>> ', titulaireAffectation);

            await ctx.indexing.publishSave('titulaireaffectations', titulaireAffectation.id);
            return titulaireAffectation;
          } catch (error) {
            console.log('error :>> ', error);
            throw new ApolloError(`Erreur lors de la ${errMsg} de l'affectation`, errCode);
          }
        }),
      );

      console.log('results :>> ', results);
      return results;
    } catch (error) {
      console.log('Remplacent error :>> ', error);
      throw new ApolloError(`Erreur lors de la ${errMsg} de l'affectation`, errCode);
    }
  } else {
    throw new ApolloError(`Aucun département`, 'EMPTY_IDS_DEPARTEMENTS');
  }
};
