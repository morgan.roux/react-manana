import { Context } from '../types';
import {
  getRemise,
  setRemiseSamePanachees,
  RemiseResult,
  articleSamePanachees,
  getArticlePanacheesInPanier,
} from './remise';
import { PanierLigne } from '../generated/prisma-client';
import { saveAndPublishNotification } from './notification';

export const validatePanier = async (ctx: Context, { id }) => {
  return ctx.prisma.updatePanier({
    data: {
      validate: true,
    },
    where: { id },
  });
};

const _updatePanierPrice = async (ctx: Context, { id, idPharmacie, validate }) => {
  const panierLignes = await ctx.prisma.panierLignes({
    where: {
      idPanier: { id },
      validate: false,
    },
  });
  if (!panierLignes.length) return ctx.prisma.panier({ id });
  const nbrRef = panierLignes.map(ligne => ligne.quantite).reduce((sum, qte) => sum + qte);
  const { prixTotalBaseQuantite, prixTotalNetQuantite } = calculPrix(panierLignes);
  const valeurRemiseTotal = prixTotalBaseQuantite - prixTotalNetQuantite;
  const remiseGlobale = (valeurRemiseTotal * 100) / prixTotalBaseQuantite;
  const qteTotal = panierLignes.map(ligne => ligne.quantite).reduce((sum, qte) => sum + qte);
  const ugTotal = panierLignes.map(ligne => ligne.uniteGratuite).reduce((sum, qte) => sum + qte);
  return ctx.prisma.updatePanier({
    data: {
      idPharmacie: { connect: { id: idPharmacie } },
      nbrRef,
      prixBaseTotalHT: prixTotalBaseQuantite,
      valeurRemiseTotal,
      prixNetTotalHT: prixTotalNetQuantite,
      remiseGlobale,
      validate: validate ? validate : false,
      quantite: qteTotal,
      uniteGratuite: ugTotal,
    },
    where: { id },
  });
};

export const panierPriceByCanal = async (ctx: Context, { id, codeCanalCommande }) => {
  const panierLignes = await ctx.prisma.panierLignes({
    where: {
      idPanier: { id },
      validate: false,
      produitCanal: { canal: { code: codeCanalCommande } },
    },
  });
  if (!panierLignes.length) return ctx.prisma.panier({ id });
  const nbrRef = panierLignes.map(ligne => ligne.quantite).reduce((sum, qte) => sum + qte);
  const { prixTotalBaseQuantite, prixTotalNetQuantite } = calculPrix(panierLignes);
  const valeurRemiseTotal = prixTotalBaseQuantite - prixTotalNetQuantite;
  const remiseGlobale = (valeurRemiseTotal * 100) / prixTotalBaseQuantite;
  const qteTotal = panierLignes.map(ligne => ligne.quantite).reduce((sum, qte) => sum + qte);
  const ugTotal = panierLignes.map(ligne => ligne.uniteGratuite).reduce((sum, qte) => sum + qte);
  return {
    nbrRef,
    prixBaseTotalHT: prixTotalBaseQuantite,
    valeurRemiseTotal,
    prixNetTotalHT: prixTotalNetQuantite,
    remiseGlobale,
    quantite: qteTotal,
    uniteGratuite: ugTotal,
  };
};

export const calculPrix = (panierLignes: PanierLigne[]) => {
  const prixBaseTotalHT = panierLignes
    .map(ligne => ligne.prixBaseHT)
    .reduce((sum, qte) => sum + qte);
  const prixTotalBaseQuantite = panierLignes
    .map(ligne => ligne.prixBaseHT * ligne.quantite)
    .reduce((sum, qte) => sum + qte);
  const prixNetTotalHT = panierLignes
    .map(ligne => ligne.prixNetUnitaireHT)
    .reduce((sum, qte) => sum + qte);
  const prixTotalNetQuantite = panierLignes
    .map(ligne => ligne.prixNetUnitaireHT * ligne.quantite)
    .reduce((sum, qte) => sum + qte);

  return { prixBaseTotalHT, prixTotalBaseQuantite, prixTotalNetQuantite, prixNetTotalHT };
};

export const addToPanier = async (
  ctx: Context,
  { idPharmacie, codeCanal, idCanalArticle, optimiser, quantite },
) => {
  const panier = await myPanier(ctx, { idPharmacie, codeCanal });
  return !panier
    ? createPanier(ctx, { idPharmacie, codeCanal }).then(created =>
        createPanierLigne(ctx, {
          idPanier: created.id,
          idCanalArticle,
          optimiser,
          quantite,
          idPharmacie,
        }),
      )
    : createPanierLigne(ctx, {
        idPanier: panier.id,
        idCanalArticle,
        optimiser,
        quantite,
        idPharmacie,
      });
};

export const createPanier = async (ctx: Context, { idPharmacie, codeCanal }) => {
  return ctx.prisma.createPanier({
    idUser: {
      connect: {
        id: ctx.userId,
      },
    },
    idCommandeCanal: { connect: { code: codeCanal } },
    idPharmacie: {
      connect: {
        id: idPharmacie,
      },
    },
  });
};

export const createPanierLigne = async (
  ctx: Context,
  { idPanier, idCanalArticle, optimiser, quantite, idPharmacie },
) => {
  const canalArticle = await ctx.prisma.produitCanal({ id: idCanalArticle });
  const codeCanal = await ctx.prisma
    .produitCanal({ id: idCanalArticle })
    .canal()
    .code();
  const ligne = await ctx.prisma
    .panierLignes({
      where: { produitCanal: { id: idCanalArticle }, idPanier: { id: idPanier } },
    })
    .then(lignes => lignes[0]);

  const qteSaisie = !quantite ? (canalArticle.stv ? canalArticle.stv : 1) : quantite;
  const qteLigne = await quantiteStv(ctx, {
    quantite: parseInt(qteSaisie, 10),
    standard: canalArticle.stv,
  });
  const newQte = ligne ? ligne.quantite + qteLigne : qteLigne;

  const currentRemise = await getRemise(ctx, {
    idPharmacie,
    idCanalArticle,
    codeCanal,
    quantite: newQte,
    idPanier,
    idCommande: null,
  });

  const prixNetUnitaireHT = (canalArticle.prixPhv * (100 - currentRemise.remise)) / 100;
  const newLigne = ligne
    ? await _updateLigne(ctx, currentRemise, {
        id: ligne.id,
        quantite: newQte,
        prixNetUnitaireHT,
        prixBaseHT: canalArticle.prixPhv,
        uniteGratuite: 0,
      })
    : await ctx.prisma.createPanierLigne({
        idPanier: {
          connect: {
            id: idPanier,
          },
        },
        produitCanal: {
          connect: {
            id: idCanalArticle,
          },
        },
        optimiser,
        quantite: newQte,
        prixNetUnitaireHT,
        prixBaseHT: canalArticle.prixPhv,
        prixTotalHT: prixNetUnitaireHT * newQte,
        remiseLigne: currentRemise.isPalier ? currentRemise.remise : null,
        remiseGamme: !currentRemise.isPalier ? currentRemise.remise : null,
      });

  if (
    !currentRemise.isPalier &&
    currentRemise.articleSamePanachees &&
    currentRemise.articleSamePanachees.length
  ) {
    await setRemiseSamePanachees(
      ctx,
      currentRemise.articleSamePanachees,
      currentRemise.remise,
      true,
    );
  }
  await _setUniteGratuitePanier(ctx, currentRemise, newLigne, { idPharmacie, idPanier });
  await ctx.indexing.publishSave('produitcanals', idCanalArticle);
  return _updatePanierPrice(ctx, { id: idPanier, idPharmacie, validate: false });
};

const _setUniteGratuitePanier = async (
  ctx: Context,
  currentRemise: RemiseResult,
  ligne: PanierLigne,
  { idPanier, idPharmacie },
) => {
  if (currentRemise.isPalier === false) {
    await _resetUniteGratuite(ctx, { id: ligne.id, idPanier, idPharmacie });
    if (currentRemise.canalArticleUG) {
      await _setUniteGratuitPanachees(ctx, currentRemise, { idPanier });
    }
  } else {
    if (currentRemise.ug !== 0) {
      await ctx.prisma.updatePanierLigne({
        data: { uniteGratuite: currentRemise.ug },
        where: { id: ligne.id },
      });
    }
  }
};

const _setUniteGratuitPanachees = async (
  ctx: Context,
  currentRemise: RemiseResult,
  { idPanier },
) => {
  const ligne = await ctx.prisma
    .panierLignes({
      where: {
        idPanier: { id: idPanier },
        produitCanal: { id: currentRemise.canalArticleUG.id },
      },
    })
    .then(panierlignes => panierlignes[panierlignes.length - 1]);

  if (ligne) {
    await ctx.prisma.updatePanierLigne({
      data: { uniteGratuite: currentRemise.ug },
      where: { id: ligne.id },
    });
  } else {
    await ctx.prisma.createPanierLigne({
      idPanier: {
        connect: {
          id: idPanier,
        },
      },
      produitCanal: {
        connect: {
          id: currentRemise.canalArticleUG.id,
        },
      },
      uniteGratuite: currentRemise.ug,
    });
  }
  return true;
};

const _resetUniteGratuite = async (ctx: Context, { id, idPanier, idPharmacie }) => {
  const newArticle = await ctx.prisma.panierLigne({ id }).produitCanal();
  if (!newArticle) return false;
  const codeCanal = await ctx.prisma
    .panierLigne({ id })
    .produitCanal()
    .canal()
    .code();
  const idArticles = await articleSamePanachees(ctx, {
    idCanalArticle: newArticle.id,
    codeCanal,
    idPharmacie,
  }).then(same => same.map(article => article.id));

  if (!idArticles || (idArticles && !idArticles.length)) return false;

  const panierLignes = await getArticlePanacheesInPanier(ctx, idArticles, { idPanier });

  if (panierLignes && panierLignes.length) {
    await ctx.prisma.updateManyPanierLignes({
      data: { uniteGratuite: 0 },
      where: { id_in: panierLignes.map(panierLigne => panierLigne.id) },
    });
    await ctx.prisma.deleteManyPanierLignes({
      quantite: 0,
      idPanier: { id: idPanier },
      id_in: panierLignes.map(panierLigne => panierLigne.id),
    });
  }
  return true;
};

export const quantiteStv = async (ctx: Context, { standard, quantite }) => {
  const qte = parseInt(quantite, 10);
  if (!standard) return qte;
  const stv = parseInt(standard, 10);
  if (qte === 0) return 0;
  return stv === 1 ? qte : qte < stv ? stv : qte - (qte % stv);
};

const _updateLigne = async (
  ctx: Context,
  currentRemise: RemiseResult,
  { id, prixNetUnitaireHT, quantite, prixBaseHT, uniteGratuite },
) => {
  return ctx.prisma.updatePanierLigne({
    data: {
      quantite,
      prixNetUnitaireHT: parseFloat(prixNetUnitaireHT),
      prixBaseHT: parseFloat(prixBaseHT),
      prixTotalHT: parseFloat(prixNetUnitaireHT) * quantite,
      remiseLigne: currentRemise.isPalier ? currentRemise.remise : null,
      remiseGamme: !currentRemise.isPalier ? currentRemise.remise : null,
      uniteGratuite: parseFloat(uniteGratuite),
    },
    where: { id },
  });
};

export const updateQtePanierLigne = async (ctx: Context, { id, quantite }) => {
  const panier = await ctx.prisma.panierLigne({ id }).idPanier();
  const pharmacie = await ctx.prisma.panier({ id: panier.id }).idPharmacie();
  const canalArticle = await ctx.prisma.panierLigne({ id }).produitCanal();
  const codeCanal = await ctx.prisma
    .panierLigne({ id })
    .produitCanal()
    .canal()
    .code();

  const currentRemise = await getRemise(ctx, {
    idPharmacie: pharmacie.id,
    idCanalArticle: canalArticle.id,
    codeCanal,
    quantite,
    idPanier: panier.id,
    idCommande: null,
  });

  const prixNetUnitaireHT = (canalArticle.prixPhv * (100 - currentRemise.remise)) / 100;
  const qteLigne = await quantiteStv(ctx, { quantite, standard: canalArticle.stv });

  const ligne = await _updateLigne(ctx, currentRemise, {
    id,
    quantite: qteLigne,
    prixNetUnitaireHT,
    prixBaseHT: canalArticle.prixPhv,
    uniteGratuite: 0,
  });

  if (
    !currentRemise.isPalier &&
    currentRemise.articleSamePanachees &&
    currentRemise.articleSamePanachees.length
  ) {
    await setRemiseSamePanachees(
      ctx,
      currentRemise.articleSamePanachees,
      currentRemise.remise,
      true,
    );
  }

  await _setUniteGratuitePanier(ctx, currentRemise, ligne, {
    idPharmacie: pharmacie.id,
    idPanier: panier.id,
  });

  await ctx.indexing.publishSave('produitcanals', canalArticle.id);
  return _updatePanierPrice(ctx, {
    id: panier.id,
    idPharmacie: pharmacie.id,
    validate: false,
  });
};

export const deletePanierLigne = async (ctx: Context, { id }) => {
  const panier = await ctx.prisma.panierLigne({ id }).idPanier();
  const pharmacie = await ctx.prisma.panier({ id: panier.id }).idPharmacie();
  const canalArticle = await ctx.prisma.panierLigne({ id }).produitCanal();
  const codeCanal = await ctx.prisma
    .panierLigne({ id })
    .produitCanal()
    .canal()
    .code();

  const currentRemise = await getRemise(ctx, {
    idPharmacie: pharmacie.id,
    idCanalArticle: canalArticle.id,
    codeCanal,
    quantite: 0,
    idPanier: panier.id,
    idCommande: null,
  });
  if (
    !currentRemise.isPalier &&
    currentRemise.articleSamePanachees &&
    currentRemise.articleSamePanachees.length
  ) {
    await setRemiseSamePanachees(
      ctx,
      currentRemise.articleSamePanachees,
      currentRemise.remise,
      true,
    );
  }

  const ligne = await ctx.prisma.panierLigne({ id });
  await _setUniteGratuitePanier(ctx, currentRemise, ligne, {
    idPharmacie: pharmacie.id,
    idPanier: panier.id,
  });
  await ctx.prisma.deletePanierLigne({ id });
  await ctx.indexing.publishSave('produitcanals', canalArticle.id);
  return _updatePanierPrice(ctx, {
    id: panier.id,
    idPharmacie: pharmacie.id,
    validate: false,
  });
};

export const clearPanier = async (ctx: Context, { idPharmacie, codeCanal }) => {
  return myPanier(ctx, { idPharmacie, codeCanal }).then(async panier => {
    if (panier && panier.id) {
      const lignes = await ctx.prisma.panierLignes({
        where: { idPanier: { id: panier.id } },
      });

      const articles =
        lignes && lignes.length
          ? await Promise.all(
              lignes.map(ligne => ctx.prisma.panierLigne({ id: ligne.id }).produitCanal()),
            )
          : null;

      await ctx.prisma.deleteManyPanierLignes({ idPanier: { id: panier.id } });

      if (articles && articles.length) {
        await Promise.all(
          articles.map(async article => ctx.indexing.publishSave('produitcanals', article.id)),
        );
      }

      return ctx.prisma.deletePanier({ id: panier.id });
    }
    return null;
  });
};

export const takeToMyPanier = async (ctx: Context, { id, idPharmacie, codeCanal }) => {
  const article = await ctx.prisma.panierLigne({ id }).produitCanal();
  const panier = await ctx.prisma.panierLigne({ id }).idPanier();

  if (!panier || !article) return null;

  const toUser = await ctx.prisma.panier({ id: panier.id }).idUser();

  return ctx.prisma
    .panierLigne({ id })
    .then(async panierLigne => {
      const currentRemise = await getRemise(ctx, {
        idPharmacie,
        idCanalArticle: article.id,
        codeCanal,
        idPanier: panier.id,
        quantite: 0,
        idCommande: null,
      });
      if (
        !currentRemise.isPalier &&
        currentRemise.articleSamePanachees &&
        currentRemise.articleSamePanachees.length
      ) {
        await setRemiseSamePanachees(
          ctx,
          currentRemise.articleSamePanachees,
          currentRemise.remise,
          true,
        );
      }
      await _setUniteGratuitePanier(ctx, currentRemise, panierLigne, {
        idPharmacie,
        idPanier: panier.id,
      });
      return addToPanier(ctx, {
        idPharmacie,
        codeCanal,
        idCanalArticle: article.id,
        optimiser: null,
        quantite: panierLigne.quantite,
      });
    })
    .then(async myPanier => {
      await ctx.prisma.deletePanierLigne({ id });
      const lignes = await ctx.prisma.panierLignes({ where: { idPanier: { id: panier.id } } });
      if (lignes && !lignes.length) await ctx.prisma.deletePanier({ id: panier.id });
      const user = await ctx.prisma.user({ id: ctx.userId });

      const produit = await ctx.prisma.produitCanal({ id: article.id }).produit();

      saveAndPublishNotification(
        {
          type: 'TAKE_ARTICLE',
          message: `${user.userName} a retiré le produit ${produit.libelle} de votre panier`,
          from: ctx.userId,
          to: toUser.id,
          targetId: produit.id,
          targetName: produit.libelle,
        },
        ctx,
      );
      return myPanier;
    });
};

export const myPanier = async (ctx: Context, { idPharmacie, codeCanal }) => {
  return ctx.prisma
    .paniers({
      where: {
        idUser: { id: ctx.userId },
        idPharmacie: { id: idPharmacie },
        idCommandeCanal: { code: codeCanal },
        validate: false,
      },
    })
    .then(paniers => paniers[paniers.length - 1]);
};
