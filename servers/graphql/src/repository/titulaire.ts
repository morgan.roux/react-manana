import { Context, ContactInput } from '../types';
import { ApolloError } from 'apollo-server';
import logger from '../logging';
import moment from 'moment';
const now = moment().format();
import { deleteUser } from './user';

export const createUpdateTitulaire = async (
  {
    id,
    civilite,
    nom,
    prenom,
    contact,
    idPharmacies,
  }: {
    id: string;
    civilite: string;
    nom: string;
    prenom: string;
    contact: ContactInput;
    idPharmacies: string[];
  },
  ctx: Context,
) => {
  const pharmcies = id ? await ctx.prisma.titulaire({ id }).pharmacies() : [];
  const idPharmaciesDisconnect =
    pharmcies && pharmcies.length
      ? pharmcies.filter((pharmcie) => !idPharmacies.includes(pharmcie.id))
      : [];

  return ctx.prisma
    .upsertTitulaire({
      where: { id: id || '' },
      create: {
        civilite,
        nom,
        prenom,
        groupement: { connect: { id: ctx.groupementId } },
        userCreation: { connect: { id: ctx.userId } },
        userModification: { connect: { id: ctx.userId } },
        pharmacies: {
          connect: idPharmacies.map((id) => {
            return {
              id,
            };
          }),
        },
        contact: {
          create: contact,
        },
      },
      update: {
        civilite,
        nom,
        prenom,
        userModification: { connect: { id: ctx.userId } },
        pharmacies: {
          disconnect:
            idPharmaciesDisconnect && idPharmaciesDisconnect.length
              ? idPharmaciesDisconnect.map((pharmcie) => {
                  return {
                    id: pharmcie.id,
                  };
                })
              : null,
          connect: idPharmacies.map((id) => {
            return {
              id,
            };
          }),
        },

        ...(contact
          ? {
              contact: {
                upsert: {
                  create: contact,
                  update: contact,
                },
              },
            }
          : {}),
      },
    })
    .then(async (created) => {
      console.log('params titulaire', created);
      await ctx.indexing.publishSave('titulaires', created.id);
      return created;
    });
};

export const updateStatusTitulaire = async (
  { id, idRegion, dateDebut, dateFin, status },
  ctx: Context,
) => {
  const departements = await ctx.prisma.departements({
    where: { region: { id: idRegion } },
  });

  if (!departements || (departements && !departements.length)) {
    return new ApolloError(
      `Il n'y a pas de departement dans cette region : `,
      'NO_DEPARTEMENT_IN_REGION',
    );
  }

  const affections = await ctx.prisma.titulaireAffectations({
    where: {
      departement: { id_in: departements.map((depart) => depart.id) },
      titulaire: { id },
      dateDebut_lte: now,
      dateFin_gte: now,
    },
  });

  if (status === true && dateDebut && dateFin) {
    if (affections && affections.length) {
      return new ApolloError(
        `Cette Titulaire est déjà un president de région: `,
        'TITULAIRE_ALREADY_PRESIDENT',
      );
    }

    return Promise.all(
      departements.map((departement, index) =>
        ctx.prisma
          .createTitulaireAffectation({
            departement: { connect: { id: departement.id } },
            titulaire: { connect: { id } },
            dateDebut,
            dateFin,
          })
          .catch((error) => {
            logger.error(
              `Erreur lors de l'affection du titulaire au poste de président de région : `,
              {
                error,
              },
            );
            return new ApolloError(
              `Erreur lors de l'affection du titulaire au poste de président de région : `,
              'ASSIGN_PRESIDENT_ERROR',
            );
          }),
      ),
    ).then(async (_) => {
      const titulaire = await ctx.prisma.titulaire({ id });
      ctx.indexing.publishSave('titulaires', titulaire.id);
      return titulaire;
    });
  }

  if (!affections || (affections && !affections.length)) {
    return new ApolloError(
      `Cette Titulaire n'est plus un président de region: `,
      'ASSIGN_TITULAIRE_NOT_PRESIDENT',
    );
  }

  return ctx.prisma
    .deleteManyTitulaireAffectations({
      departement: { id_in: departements.map((depart) => depart.id) },
      titulaire: { id },
    })
    .then(async (_) => {
      const titulaire = await ctx.prisma.titulaire({ id });
      if (titulaire) ctx.indexing.publishSave('titulaires', titulaire.id);
      return titulaire;
    })
    .catch((err) => {
      logger.error(
        `Erreur lors de l'affectation du président au poste de titulaire de pharmacie : `,
        { err },
      );
      return new ApolloError(
        `Erreur lors de l'affectation du président au poste de titulaire de pharmacie : `,
        'ASSIGN_TITULAIRE_ERROR',
      );
    });
};

export const isTitulairePresident = async ({ id }, ctx: Context) => {
  return (
    (await ctx.prisma
      .titulaireAffectationsConnection({
        where: {
          titulaire: {
            id,
          },
          dateDebut_lte: now,
          dateFin_gte: now,
        },
      })
      .aggregate()
      .count()) > 0
  );
};

export const presidentRegions = async (ctx: Context) => {
  const affectations = await ctx.prisma.titulaireAffectations({
    where: {
      dateDebut_lte: now,
      dateFin_gte: now,
    },
  });

  logger.info(`cible affectations ${JSON.stringify(affectations.length)}`);

  return affectations && affectations.length
    ? Promise.all(
        affectations.map((affectation) =>
          ctx.prisma.titulaireAffectation({ id: affectation.id }).titulaire(),
        ),
      )
    : null;
};

export const affectationPresidentRegions = async ({ id }, ctx: Context) => {
  return ctx.prisma.titulaireAffectations({
    where: {
      titulaire: {
        id,
      },
    },
  });
};

export const deleteSoftTitulaire = async ({ id }, ctx: Context) => {
  return ctx.prisma
    .updateTitulaire({
      where: {
        id: String(id),
      },
      data: {
        sortie: 1,
        dateSortie: now,
      },
    })
    .then(async (updated) => {
      // delete user
      const userTitulaires = await ctx.prisma.userTitulaires({
        where: {
          idTitulaire: {
            id,
          },
        },
      });

      if (userTitulaires) {
        await Promise.all(
          userTitulaires.map(async (userTitulaire) => {
            const user = await ctx.prisma.userTitulaire({ id: userTitulaire.id }).idUser();
            await deleteUser(ctx, user);
          }),
        );
      }

      await ctx.indexing.publishSave('titulaires', updated.id);
      return updated;
    });
};
