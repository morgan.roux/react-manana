import { TodoActionTypeCreateInput } from '../generated/prisma-client';
import { Context, TodoActionTypeInput } from '../types';

export const createUpdateTodoActionType = async (input: TodoActionTypeInput, ctx: Context) => {
  const { id, codeMaj, isRemoved, libelle, code } = input;
  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: TodoActionTypeCreateInput = {
    codeMaj,
    isRemoved,
    libelle,
    code,
    userCreation,
    userModification,
  };
  const todoActionType = await ctx.prisma.upsertTodoActionType({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  await ctx.indexing.publishSave('todoactiontypes', todoActionType.id);
  return todoActionType;
};

export const softDeleteTodoActionType = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateTodoActionType({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      await ctx.indexing.publishSave('todoactiontypes', res.id);
      return res;
    });
};
