import { ApolloError } from 'apollo-server';
import { ActionActivite, ActionActiviteCreateInput } from '../generated/prisma-client';
import { ActionActiviteInput, Context } from '../types';

/**
 * Create or Update ActionActivite
 *
 * @param {Context} ctx
 * @param {ActionActiviteInput} input
 * @return {*}  {(Promise<ActionActivite | ApolloError>)}
 */
export const createUpdateActionActivite = async (
  ctx: Context,
  input: ActionActiviteInput,
): Promise<ActionActivite | ApolloError> => {
  const {
    id,
    activiteType,
    idAction,
    idUserSource,
    idUserDestination,
    newStatus,
    oldStatus,
    oldDateDebut,
    oldDateFin,
    newDateDebut,
    newDateFin,
  } = input;

  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userSource = idUserSource ? { connect: { id: idUserSource } } : null;

  const data: ActionActiviteCreateInput = {
    activiteType,
    action: idAction ? { connect: { id: idAction } } : null,
    userSource,
    userDestination: idUserDestination ? { connect: { id: idUserDestination } } : userSource,
    newStatus,
    oldStatus,
    oldDateDebut,
    oldDateFin,
    newDateDebut,
    newDateFin,
  };

  const actionActivite = await ctx.prisma.upsertActionActivite({
    where: { id: id ? id : '' },
    create: data,
    update: data,
  });

  await ctx.indexing.publishSave('actionactivites', actionActivite.id);
  return actionActivite;
};
