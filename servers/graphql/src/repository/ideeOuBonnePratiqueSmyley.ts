import { IdeeOuBonnePratiqueSmyleyCreateInput } from '../generated/prisma-client';
import { Context, IdeeOuBonnePratiqueSmyleyInput } from '../types';
import { publishIdeeOuBonnePratique } from './ideeOuBonnePratique';

export const createUpdateIdeeOuBonnePratiqueSmyley = async (
  ctx: Context,
  input: IdeeOuBonnePratiqueSmyleyInput,
) => {
  const { id, isRemoved, idUser, idSmyley, idIdeeOuBonnePratique } = input;
  const data: IdeeOuBonnePratiqueSmyleyCreateInput = {
    user: idUser ? { connect: { id: idUser } } : null,
    smyley: idSmyley ? { connect: { id: idSmyley } } : null,
    ideeOuBonnePratique: idIdeeOuBonnePratique ? { connect: { id: idIdeeOuBonnePratique } } : null,
    isRemoved: isRemoved || false,
  };

  const result = await ctx.prisma.upsertIdeeOuBonnePratiqueSmyley({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  if (idIdeeOuBonnePratique) publishIdeeOuBonnePratique(ctx, idIdeeOuBonnePratique);
  return result;
};

export const softDeleteIdeeOuBonnePratiqueSmyley = async (ctx: Context, id: string) => {
  const result = await ctx.prisma.updateIdeeOuBonnePratiqueSmyley({
    where: { id },
    data: { isRemoved: true },
  });
  const ideeBonnePratique = await ctx.prisma
    .ideeOuBonnePratiqueSmyley({ id })
    .ideeOuBonnePratique();
  if (ideeBonnePratique) publishIdeeOuBonnePratique(ctx, ideeBonnePratique.id);
  return result;
};
