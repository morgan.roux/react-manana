import { PartagePharmacieCibleCreateInput } from '../generated/prisma-client';
import { Context, PartagePharmacieCibleInput } from '../types';

export const createUpdatePartagePharmacieCible = async (
  input: PartagePharmacieCibleInput,
  ctx: Context,
) => {
  const { id, idPartage, idPharmacie } = input;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: PartagePharmacieCibleCreateInput = {
    userCreation,
    userModification,
    partage: idPartage ? { connect: { id: idPartage } } : null,
    pharmacie: idPharmacie ? { connect: { id: idPharmacie } } : null,
  };
  const partagePharmacieCible = await ctx.prisma.upsertPartagePharmacieCible({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  // await ctx.indexing.publishSave('groupeamises', partagePharmacieCible.id);
  return partagePharmacieCible;
};
