import { TodoEtiquetteFavorisCreateInput } from '../generated/prisma-client';
import { Context, TodoEtiquetteFavorisInput } from '../types';

export const createUpdateTodoEtiquetteFavoris = async (
  input: TodoEtiquetteFavorisInput,
  ctx: Context,
) => {
  const { id, codeMaj, isRemoved, ordre, idEtiquette, idUser } = input;
  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: TodoEtiquetteFavorisCreateInput = {
    codeMaj,
    isRemoved,
    ordre,
    etiquette: idEtiquette ? { connect: { id: idEtiquette } } : null,
    user: idUser ? { connect: { id: idUser } } : null,
    userCreation,
    userModification,
  };
  const todoEtiquetteFavoris = await ctx.prisma.upsertTodoEtiquetteFavoris({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  await ctx.indexing.publishSave('todoetiquettefavorises', todoEtiquetteFavoris.id);
  return todoEtiquetteFavoris;
};

export const softDeleteTodoEtiquetteFavoris = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateTodoEtiquetteFavoris({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      await ctx.indexing.publishSave('todoetiquettefavorises', res.id);
      return res;
    });
};
