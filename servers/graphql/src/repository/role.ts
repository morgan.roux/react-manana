import { Context } from '../types';
import { ApolloError } from 'apollo-server';

export const createUpdateRoleTraitement = async (
  ctx: Context,
  { codeRole, codeTraitements }: { codeRole: string; codeTraitements?: string[] },
) => {
  const existRole = await ctx.prisma.$exists.role({ code: codeRole });
  if (!existRole) return new ApolloError(`Le Role n'existe pas`, 'ERROR_CODE_ROLE');

  await ctx.prisma.deleteManyRoleTraitements({ role: { code: codeRole } });

  if (!codeTraitements || (codeTraitements && !codeTraitements.length)) {
    return ctx.prisma.role({ code: codeRole }).then(async role => {
      await ctx.indexing.publishSave('roles', role.id);
      return role;
    });
  }

  await Promise.all(
    codeTraitements.map(async codeTraitement => {
      const existTraitement = await ctx.prisma.$exists.traitement({ code: codeTraitement });
      console.log(`exist ${codeTraitement}: ${existTraitement}`);

      return existTraitement
        ? ctx.prisma
            .createRoleTraitement({
              groupement: { connect: { id: ctx.groupementId } },
              role: { connect: { code: codeRole } },
              access: true,
              traitement: { connect: { code: codeTraitement } },
            })
            .then(async created => {
              const traitement = await ctx.prisma.traitement({ code: codeTraitement });
              await ctx.indexing.publishSave('traitements', traitement.id);
              return created;
            })
        : null;
    }),
  );
  return ctx.prisma.role({ code: codeRole }).then(async role => {
    await ctx.indexing.publishSave('roles', role.id);
    return role;
  });
};
