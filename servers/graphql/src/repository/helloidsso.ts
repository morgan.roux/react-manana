import { Context, HelloidGroupInfo } from '../types';
const rp = require('request-promise');
import { HelloIdSso } from '../generated/prisma-client';
import { HelloidGroup, Application } from '../types';
import { _checkSsoHelloid } from './user';
import logger from '../logging';
import { ApolloError } from 'apollo-server';
// import { _getImagesUrl } from './application';

const _getGroups = async (helloidsso: HelloIdSso) => {
  logger.info('Get helloid applications ', `${helloidsso.helloIdUrl}/api/v1/groups`);
  const request = [];
  const options = {
    url: `${helloidsso.helloIdUrl}/api/v1/groups`,
    auth: {
      user: `${helloidsso.apiKey}`,
      pass: `${helloidsso.apiPass}`,
    },
    json: true,
  };

  await request.push(rp(options));

  return Promise.all(request)
    .then(results => {
      return results; // Result of all resolve as an array
    })
    .catch(err => {
      logger.info(err);
      return err;
    });
};

const _deleteHelloIdGroup = async (helloidsso: HelloIdSso, groupGuid: string) => {
  const options = {
    method: `DELETE`,
    url: `${helloidsso.helloIdUrl}/api/v1/groups/${groupGuid}`,
    auth: {
      user: `${helloidsso.apiKey}`,
      pass: `${helloidsso.apiPass}`,
    },
    json: true,
  };

  return rp(options);
};

export const _getImagesUrl = async (helloidsso: HelloIdSso) => {
  logger.info('Get image url ', `${helloidsso.helloIdUrl}/api/v1/storageportal/imagesurl`);
  const request = [];
  const options = {
    url: `${helloidsso.helloIdUrl}/api/v1/storageportal/imagesurl`,
    auth: {
      user: `${helloidsso.apiKey}`,
      pass: `${helloidsso.apiPass}`,
    },
    json: true,
  };

  await request.push(rp(options));

  return Promise.all(request)
    .then(results => {
      return results; // Result of all resolve as an array
    })
    .catch(err => console.log(err));
};

const _getUserByUserName = async (username: string, helloidsso: HelloIdSso) => {
  const request = [];
  const options = {
    url: `${helloidsso.helloIdUrl}/api/v1/users/${username}`,
    auth: {
      user: `${helloidsso.apiKey}`,
      pass: `${helloidsso.apiPass}`,
    },
    json: true,
  };

  await request.push(rp(options));

  return Promise.all(request)
    .then(results => {
      return results; // Result of all resolve as an array
    })
    .catch(err => console.log(err));
};

const _getEnduserApplications = async (userGuid: string, helloidsso: HelloIdSso) => {
  const request = [];
  const options = {
    url: `${helloidsso.helloIdUrl}/api/v1/enduser/${userGuid}/applications`,
    auth: {
      user: `${helloidsso.apiKey}`,
      pass: `${helloidsso.apiPass}`,
    },
    json: true,
  };

  await request.push(rp(options));

  return Promise.all(request)
    .then(results => {
      console.log('results applications ', results);
      return results; // Result of all resolve as an array
    })
    .catch(err => {
      console.log(err);
      return err;
    });
};

const _setHelloIDurl = (usersApplications, iconurl, helloidsso: HelloIdSso): Application[] => {
  const userapps: Application[] = [];
  logger.info('helloid users applications ', usersApplications);
  usersApplications.forEach(element => {
    userapps.push({
      ...element,
      lastTimeUsed: element.lastTimeUsed && new Date(element.lastTimeUsed),
      helloIdurl: `${helloidsso.helloIdUrl}/relayservice/redirect/${element.applicationGUID}`,
      iconlink: `${iconurl.url}${element.icon}`,
    });
  });
  return userapps;
};

const _getApplications = async (helloidsso: HelloIdSso) => {
  logger.info(
    'Get helloid applications ',
    `${helloidsso.helloIdUrl}/api/v1/applications?includeIcon=true`,
  );
  const request = [];
  const options = {
    url: `${helloidsso.helloIdUrl}/api/v1/applications?includeIcon=true`,
    auth: {
      user: `${helloidsso.apiKey}`,
      pass: `${helloidsso.apiPass}`,
    },
    json: true,
  };

  await request.push(rp(options));

  return Promise.all(request)
    .then(results => {
      return results; // Result of all resolve as an array
    })
    .catch(err => {
      logger.info(err);
      return err;
    });
};

const _getOneGroup = async (groupGuid: string, helloidsso: HelloIdSso) => {
  logger.info('Get helloid applications ', `${helloidsso.helloIdUrl}/api/v1/groups/${groupGuid}`);
  const options = {
    url: `${helloidsso.helloIdUrl}/api/v1/groups/${groupGuid}`,
    auth: {
      user: `${helloidsso.apiKey}`,
      pass: `${helloidsso.apiPass}`,
    },
    json: true,
  };

  return rp(options);
};

const _getApplicationsGroup = async (helloidsso: HelloIdSso) => {
  const groups = await _getGroups(helloidsso);
  const promises = [];
  groups[0].forEach(element => {
    promises.push(_getOneGroup(element.groupGuid, helloidsso));
  });
  return Promise.all(promises)
    .then(result => {
      return result;
    })
    .catch(err => {
      logger.info(err);
      return err;
    });
};

const _linkApplicationToGroup = async (
  groupGuid: string,
  applicationGUID: string,
  helloidsso: HelloIdSso,
) => {
  logger.info(
    `POST to helloid applications ${helloidsso.helloIdUrl}/api/v1/groups/${groupGuid}/applications`,
  );
  const options = {
    method: `POST`,
    url: `${helloidsso.helloIdUrl}/api/v1/groups/${groupGuid}/applications`,
    auth: {
      user: `${helloidsso.apiKey}`,
      pass: `${helloidsso.apiPass}`,
    },
    json: true,
    body: {
      applicationGUID: `${applicationGUID}`,
    },
  };

  return rp(options);
};

const _removeApplicationToGroup = async (
  groupGuid: string,
  applicationGUID: string,
  helloidsso: HelloIdSso,
) => {
  logger.info(
    `POST to helloid applications ${helloidsso.helloIdUrl}/api/v1/groups/${groupGuid}/applications/${applicationGUID}`,
  );
  const options = {
    method: `DELETE`,
    url: `${helloidsso.helloIdUrl}/api/v1/groups/${groupGuid}/applications/${applicationGUID}`,
    auth: {
      user: `${helloidsso.apiKey}`,
      pass: `${helloidsso.apiPass}`,
    },
    json: true,
  };

  return rp(options);
};

const _executeAll = async (
  applicationsGuid: string[],
  groupGuid: string,
  helloidsso: HelloIdSso,
  add: boolean,
) => {
  const promises = [];
  if (add) {
    console.log('groupGuid ', groupGuid);
    applicationsGuid.forEach(element => {
      promises.push(_linkApplicationToGroup(groupGuid, element, helloidsso));
    });
  } else {
    applicationsGuid.forEach(element => {
      promises.push(_removeApplicationToGroup(groupGuid, element, helloidsso));
    });
  }
  return Promise.all(promises)
    .then(() => {
      console.log('applicationsGuid ', applicationsGuid);
      return applicationsGuid;
    })
    .catch(err => {
      logger.info(err);
      return err;
    });
};

const _resolveResult = async (helloidsso: HelloIdSso) => {
  const groups: any[] = await _getApplicationsGroup(helloidsso);
  const applicationsHelloid: any[] = await _getApplications(helloidsso);
  const iconStorage = await _getImagesUrl(helloidsso);
  const helloidApps: Application[] = applicationsHelloid[0];

  const groupsResult: HelloidGroup[] = [];
  groups.forEach(element => {
    const applications = [];
    element.applications.forEach(applicationGuid => {
      const app = helloidApps.find(application => application.applicationGUID === applicationGuid);
      applications.push({ ...app, iconlink: `${iconStorage[0].url}${app.icon}` });
    });
    groupsResult.push({ ...element, application: applications });
  });
  return groupsResult;
};

export const HelloidApplications = async (ctx: Context, { idgroupement }) => {
  try {
    const helloidsso = await ctx.prisma.helloIdSsoes({
      where: { groupement: { id: idgroupement } },
    });

    console.log('helloidsso ===============> ', helloidsso);
    if (helloidsso.length === 0) {
      throw new ApolloError('HelloID non configuré, veuillez le configurer');
    }

    try {
      const iconStorage = await _getImagesUrl(helloidsso[0]);
      const applications = await _getApplications(helloidsso[0]);
      const helloidApps: Application[] = applications[0].map(field => {
        return { ...field, iconlink: `${iconStorage[0].url}${field.icon}` };
      });
      return helloidApps;
    } catch (error) {
      throw new ApolloError(
        'Il y a une erreur sur la récupératin des applications, veuillez recharger la page',
      );
    }
  } catch (error) {
    throw new Error(error);
  }
};

export const HelloidGroups = async ({ idgroupement }, ctx: Context): Promise<HelloidGroup[]> => {
  try {
    const helloidssoes = await ctx.prisma.helloIdSsoes({
      where: { groupement: { id: idgroupement } },
    });
    const helloidGroups = await _resolveResult(helloidssoes[0]);
    return helloidGroups;
  } catch (error) {
    throw new Error(error);
  }
};

const _resolveGroupHelloID = async (helloIdSso: HelloIdSso): Promise<HelloidGroupInfo[]> => {
  const helloidGroups = await _getGroups(helloIdSso);
  const groups: HelloidGroupInfo[] = [];
  helloidGroups[0].forEach(element => {
    groups.push({
      groupGuid: element.groupGuid,
      name: element.name,
      immutableId: element.immutableId,
      isDefault: element.isDefault,
      isDeleted: element.isDeleted,
      isEnabled: element.isEnabled,
      isQrEnabled: element.isQrEnabled,
      managedByUserGuid: element.managedByUserGuid,
      source: element.source,
    });
  });
  return groups;
};

export const AllHelloidGroupsInfo = async (
  { idgroupement },
  ctx: Context,
): Promise<HelloidGroupInfo[]> => {
  try {
    const helloidssoes = await ctx.prisma.helloIdSsoes({
      where: { groupement: { id: idgroupement } },
    });
    const groups: HelloidGroupInfo[] = await _resolveGroupHelloID(helloidssoes[0]);
    return groups;
  } catch (error) {
    throw new Error(error);
  }
};

export const DeleteHelloIDGroup = async (
  { idgroupement, groupGuid },
  ctx: Context,
): Promise<HelloidGroupInfo[]> => {
  try {
    const helloidssoes = await ctx.prisma.helloIdSsoes({
      where: { groupement: { id: idgroupement } },
    });

    await _deleteHelloIdGroup(helloidssoes[0], groupGuid);

    const groups: HelloidGroupInfo[] = await _resolveGroupHelloID(helloidssoes[0]);
    return groups;
  } catch (error) {
    throw new Error(error);
  }
};

const _createHelloIdGroup = async (
  helloidsso: HelloIdSso,
  { Name, IsEnabled, IsDefault, IsQrEnabled, ApplicationGUIDs },
) => {
  logger.info('Create helloId group ', `${helloidsso.helloIdUrl}/api/v1/groups`);

  const options = {
    method: `POST`,
    url: `${helloidsso.helloIdUrl}/api/v1/groups`,
    auth: {
      user: `${helloidsso.apiKey}`,
      pass: `${helloidsso.apiPass}`,
    },
    json: true,
    body: {
      Name: `${Name}`,
      IsEnabled,
      IsDefault,
      IsQrEnabled,
      ApplicationGUIDs,
    },
  };

  return rp(options);
};

export const CreateHelloIdGroup = async (
  ctx: Context,
  { idgroupement, Name, IsEnabled, IsDefault, IsQrEnabled, ApplicationGUIDs },
): Promise<HelloidGroup> => {
  try {
    const helloidssoes = await ctx.prisma.helloIdSsoes({
      where: { groupement: { id: idgroupement } },
    });
    const group = await _createHelloIdGroup(helloidssoes[0], {
      Name,
      IsEnabled,
      IsDefault,
      IsQrEnabled,
      ApplicationGUIDs,
    });
    console.log('group ', group);
    const groupsResult: HelloidGroup = await _groupResult(group.groupGuid, helloidssoes[0]);
    console.log('groupsResult ', groupsResult);
    return groupsResult;
  } catch (error) {
    throw new Error(error);
  }
};

const _groupResult = async (groupGuid: string, helloidsso: HelloIdSso): Promise<HelloidGroup> => {
  const group = await _getOneGroup(groupGuid, helloidsso);

  console.log('group ', group);

  const groupsResult: HelloidGroup = await _getGroupInfo(group, helloidsso);
  console.log('groupsResult ', groupsResult);
  return groupsResult;
};

const _getGroupInfo = async (group: any, helloidsso: HelloIdSso): Promise<HelloidGroup> => {
  const applicationsHelloid: any[] = await _getApplications(helloidsso);
  const iconStorage = await _getImagesUrl(helloidsso);
  const helloidApps: Application[] = applicationsHelloid[0];

  const applications = [];
  group.applications.forEach(applicationGuid => {
    const app = helloidApps.find(application => application.applicationGUID === applicationGuid);
    applications.push({
      ...app,
      iconlink: `${iconStorage[0].url}${app.icon}`,
      helloIdurl: `${helloidsso.helloIdUrl}/relayservice/redirect/${app.applicationGUID}`,
    });
  });
  const groupsResult: HelloidGroup = {
    ...group,
    application: applications,
  };
  return groupsResult;
};

export const AddApplicationToHelloidGroup = async (
  ctx: Context,
  { idGroupement, groupGuid, applicationsGuid },
): Promise<HelloidGroup> => {
  try {
    const helloidssoesGroup = await ctx.prisma.helloIdSsoes({
      where: { groupement: { id: idGroupement } },
    });

    console.log('idgroupement ', idGroupement);
    console.log('helloidssoes ', helloidssoesGroup);

    await _executeAll(applicationsGuid, groupGuid, helloidssoesGroup[0], true);
    const groupsResult: HelloidGroup = await _groupResult(groupGuid, helloidssoesGroup[0]);

    console.log('groupsResult ', groupsResult);

    return groupsResult;
  } catch (error) {
    throw new Error(error);
  }
};

export const RemoveApplicationToHelloidGroup = async (
  ctx: Context,
  { idGroupement, groupGuid, applicationsGuid },
): Promise<HelloidGroup> => {
  try {
    const helloidssoes = await ctx.prisma.helloIdSsoes({
      where: { groupement: { id: idGroupement } },
    });
    await _executeAll(applicationsGuid, groupGuid, helloidssoes[0], false);
    const groupsResult: HelloidGroup = await _groupResult(groupGuid, helloidssoes[0]);
    return groupsResult;
  } catch (error) {
    throw new Error(error);
  }
};

export const helloIdSsoByGroupementId = async (ctx: Context, { idgroupement }) => {
  try {
    const helloidssoes = await ctx.prisma.helloIdSsoes({
      where: { groupement: { id: idgroupement } },
    });
    return helloidssoes[0];
  } catch (error) {
    throw new Error(error);
  }
};

export const CreateHelloIdSso = async (
  ctx: Context,
  { idGroupement, helloIdUrl, helloIDConsumerUrl, x509Certificate, apiKey, apiPass },
) => {
  try {
    return ctx.prisma.createHelloIdSso({
      helloIdUrl,
      helloIDConsumerUrl,
      x509Certificate,
      apiKey,
      apiPass,
      groupement: {
        connect: { id: idGroupement },
      },
    });
  } catch (error) {
    throw new Error(error);
  }
};

export const UpdateHelloIdSso = async (
  ctx: Context,
  { id, helloIdUrl, helloIDConsumerUrl, x509Certificate, apiKey, apiPass },
) => {
  try {
    return ctx.prisma.updateHelloIdSso({
      data: {
        helloIdUrl,
        helloIDConsumerUrl,
        x509Certificate,
        apiKey,
        apiPass,
      },
      where: { id },
    });
  } catch (error) {
    throw new Error(error);
  }
};

export const DeleteHelloIdSso = async (ctx: Context, { id }) => {
  try {
    return ctx.prisma.deleteHelloIdSso({ id });
  } catch (error) {
    throw new Error(error);
  }
};

export const getUserApplications = async (
  ctx: Context,
  { iduser, idGroupement },
): Promise<Application[]> => {
  try {
    const user = await ctx.prisma.user({ id: iduser });
    const userRoles = await ctx.prisma.user({ id: iduser }).userRoles();
    const userRole = await ctx.prisma.userRole({ id: userRoles[0].id }).idRole();

    console.log('userRole ===============> ', userRole);
    let helloidsso: HelloIdSso;
    if (userRole.code.trim().toUpperCase() === 'SUPADM') {
      const helloidssoes = await ctx.prisma.helloIdSsoes({
        where: { groupement: { id: idGroupement } },
      });
      helloidsso = helloidssoes[0];
    } else {
      const helloidInfo = await _checkSsoHelloid(ctx, iduser, '0046');
      helloidsso = helloidInfo.haveHelloid[0];
    }

    console.log('helloidsso ===============> ', helloidsso);
    if (!helloidsso) {
      throw new ApolloError('HelloID non configuré, veuillez le configurer');
    }

    const helloidUser = await _getUserByUserName(user.email, helloidsso);
    if (!helloidUser) {
      throw new ApolloError('User non existant dans HelloID');
    }

    try {
      const imagesurl = await _getImagesUrl(helloidsso);
      console.log(`images url `, imagesurl);

      console.log(`username ${user.email}`);

      console.log('user', helloidUser);

      const userGUID = helloidUser[0].userGUID;

      const usersApplications = await _getEnduserApplications(userGUID, helloidsso);

      console.log(`user apps `, usersApplications[0]);

      const userApps = _setHelloIDurl(usersApplications[0], imagesurl[0], helloidsso);

      console.log(`result `, userApps);

      return userApps;
    } catch (error) {
      throw new ApolloError(
        'Il y a une erreur sur la récupératin des applications, veuillez recharger la page',
      );
    }
  } catch (error) {
    throw new Error(error);
  }
};
