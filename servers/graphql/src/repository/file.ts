import { Context } from '../types';
import { deleteS3File } from '../services/file';

export const addFile = async (
  ctx: Context,
  { table, column, id, chemin, nomOriginal, type }, // id: exemple idUser, idGroupement
  toParse: boolean = false,
) => {
  const connect = { connect: toParse ? { [column]: parseInt(id, 10) } : { id } };
  return ctx.prisma[`create${table[0].toUpperCase()}${table.substring(1)}`]({
    idFichier: { create: { chemin, nomOriginal, type } },
    [column]: connect,
  }).then(created => ctx.prisma[table]({ id: created.id }).idFichier());
};

export const deleteFile = async (ctx: Context, { table, chemin }) => {
  return ctx.prisma.fichiers({ where: { chemin } }).then(async fichiers => {
    const relationTable =
      table === 'groupementLogo'
        ? `deleteMany${table[0].toUpperCase()}${table.substring(1)}es`
        : `deleteMany${table[0].toUpperCase()}${table.substring(1)}s`;
    await ctx.prisma[relationTable]({
      idFichier: { chemin },
    });
    await ctx.prisma.deleteManyFichiers({ chemin }).then(_ => deleteS3File(chemin));
    return fichiers[0];
  });
};
