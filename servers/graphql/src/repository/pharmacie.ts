import {
  Context,
  PharmacieEntreeSortieInput,
  PharmacieSegmentationInput,
  PharmacieComptaInput,
  PharmacieInformatiqueInput,
  PharmacieAchatInput,
  PharmacieConceptInput,
  PharmacieStatCAInput,
  PharmacieDigitaleInput,
  PharmacieSatisfactionInput,
  PharmacieCapInput,
} from '../types';
import { Pharmacie } from '../generated/prisma-client';
import logger from '../logging';
import { ApolloError } from 'apollo-server';
import moment from 'moment';
import { updateGroupementGroupeClients } from '../sequelize/shared';
const now = moment().format();

export const pharmacieUserTitulaire = async (ctx: Context, idUser: string) => {
  const userTitulaires = await ctx.prisma.userTitulaires({
    where: {
      idUser: {
        id: idUser,
      },
    },
  });
  return userTitulaires && userTitulaires.length
    ? ctx.prisma.userTitulaire({ id: userTitulaires[0].id }).idPharmacie()
    : null;
};

export const userTitulaireContact = async (ctx: Context, idUser: string) => {
  const userTitulaires = await ctx.prisma.userTitulaires({
    where: {
      idUser: {
        id: idUser,
      },
    },
  });
  return userTitulaires && userTitulaires.length
    ? ctx.prisma
        .userTitulaire({ id: userTitulaires[0].id })
        .idTitulaire()
        .contact()
    : null;
};

export const pharmacieUserPpersonnel = async (ctx: Context, idUser: string) => {
  const ppersonnels = await ctx.prisma.ppersonnels({
    where: {
      user: {
        id: idUser,
      },
    },
  });
  return ppersonnels && ppersonnels.length
    ? ctx.prisma.ppersonnel({ id: ppersonnels[0].id }).idPharmacie()
    : null;
};

export const userPpersonnelContact = async (ctx: Context, idUser: string) => {
  const ppersonnels = await ctx.prisma.ppersonnels({
    where: {
      user: {
        id: idUser,
      },
    },
  });
  return ppersonnels && ppersonnels.length
    ? ctx.prisma.ppersonnel({ id: ppersonnels[0].id }).contact()
    : null;
};

export const getPharmaciesWithParams = async (
  ctx: Context,
  datas: string[],
  { culumn },
  parsInt: boolean = false,
): Promise<Pharmacie[]> => {
  const defaultValue: Pharmacie[] = [];
  return datas.reduce(async (prev, data): Promise<Pharmacie[]> => {
    const result = await prev;
    if (data) {
      const pharmacie = await ctx.prisma
        .pharmacies({
          where: {
            [culumn]: parsInt ? parseInt(data, 10) : data,
          },
        })
        .then(pharmacies => {
          logger.info(
            `Opération Commerciale: file pharmacie: ${culumn}:${data} pharmacie:${JSON.stringify(
              pharmacies,
            )}`,
          );
          return pharmacies && pharmacies.length ? pharmacies[0] : null;
        });
      if (pharmacie && pharmacie.id) return [...result, pharmacie];
    }

    return [...result];
  }, Promise.resolve(defaultValue));
};

export const pharmacieIds = async (ctx: Context, ids, { first, skip }): Promise<string[]> => {
  const where =
    ids && ids.length
      ? { id_in: ids, groupement: { id: ctx.groupementId } }
      : { groupement: { id: ctx.groupementId } };

  const filter =
    (!first && first !== 0) || (!skip && skip !== 0) ? { where } : { where, skip, first };

  const fragment = `
  fragment PharmacieId on Pharmacie {
    id
  }`;

  return ctx.prisma
    .pharmacies(filter)
    .$fragment(fragment)
    .then((pharmas: any[]) => (pharmas && pharmas.length ? pharmas.map(pharma => pharma.id) : []));
};

export const createUpdatePharmacie = async (
  ctx: Context,
  {
    id,
    cip,
    type = 'PHARMACIE',
    nom,
    adresse1,
    adresse2,
    cp,
    ville,
    pays,
    longitude,
    latitude,
    nbEmploye,
    nbAssistant,
    nbAutreTravailleur,
    uga,
    commentaire,
    idGrossistes,
    idGeneriqueurs,
    contact,
    entreeSortie,
    satisfaction,
    segmentation,
    compta,
    informatique,
    achat,
    cap,
    concept,
    chiffreAffaire,
    digitales,
  },
) => {
  console.log(
    'PHARMACIE PARAMETER',
    JSON.stringify({
      id,
      cip,
      nom,
      adresse1,
      adresse2,
      cp,
      ville,
      pays,
      longitude,
      latitude,
      nbEmploye,
      nbAssistant,
      nbAutreTravailleur,
      uga,
      commentaire,
      idGrossistes,
      idGeneriqueurs,
      contact,
      entreeSortie,
      satisfaction,
      segmentation,
      compta,
      informatique,
      achat,
      cap,
      concept,
      chiffreAffaire,
      digitales,
    }),
  );

  let idDepartement: string;
  const departements = await ctx.prisma.departements({
    where: {
      code: (cip || '').substr(0, 3),
    },
  });

  if (departements.length > 0) {
    idDepartement = departements[0].id;
  }

  if (!idDepartement) {
    const firstDepartements = await ctx.prisma.departements({
      first: 1,
    });

    idDepartement = firstDepartements[0].id;
  }

  const pharmacie = await ctx.prisma.upsertPharmacie({
    where: { id: id || '' },
    create: {
      cip,
      nom,
      contact: contact
        ? {
            create: contact,
          }
        : null,
      uga,
      //  type,
      cp,
      ville,
      pays,
      adresse1,
      adresse2,
      nbEmploye,
      nbAssistant,
      nbAutreTravailleur,
      commentaire,
      longitude,
      latitude,
      departement: { connect: { id: idDepartement } },
      groupement: { connect: { id: ctx.groupementId } },
    },
    update: {
      cip,
      nom,
      contact: contact
        ? {
            upsert: {
              create: contact,
              update: contact,
            },
          }
        : null,
      uga,
      cp,
      ville,
      pays,
      adresse1,
      adresse2,
      nbEmploye,
      nbAssistant,
      nbAutreTravailleur,
      commentaire,
      longitude,
      latitude,
    },
  });

  console.log('PHARMACIE', pharmacie);

  if (!pharmacie) {
    return new ApolloError('Erreur lors de la modification du pharmacie', 'ERROR_UPDATE_PHARMACIE');
  }

  if (cip) {
    await createPharmacieHistoCip(ctx, {
      idPharmacie: pharmacie.id,
      cip,
    });
  }

  if (idGrossistes && idGrossistes.length) {
    await createUpdatePharmacieGrossiste(ctx, {
      idPharmacie: pharmacie.id,
      idGrossistes,
    });
  }

  if (idGeneriqueurs && idGeneriqueurs.length) {
    await createUpdatePharmacieGeneriqueur(ctx, {
      idPharmacie: pharmacie.id,
      idGeneriqueurs,
    });
  }

  if (segmentation) {
    await createUpdatePharmacieSegementation(ctx, {
      ...segmentation,
      idPharmacie: pharmacie.id,
    });
  }

  if (entreeSortie) {
    await createUpdatePharmacieEntreeSortie(ctx, {
      ...entreeSortie,
      idPharmacie: pharmacie.id,
    });
  }

  if (satisfaction) {
    await createUpdatePharmacieSatisfaction(ctx, {
      ...satisfaction,
      idPharmacie: pharmacie.id,
    });
  }

  if (compta) {
    await createUpdatePharmacieCompta(ctx, {
      ...compta,
      idPharmacie: pharmacie.id,
    });
  }

  if (informatique) {
    await createUpdatePharmacieInformatique(ctx, {
      ...informatique,
      idPharmacie: pharmacie.id,
    });
  }

  if (achat) {
    await createUpdatePharmacieAchat(ctx, {
      ...achat,
      idPharmacie: pharmacie.id,
    });
  }

  if (cap) {
    await createUpdatePharmacieCap(ctx, {
      ...cap,
      idPharmacie: pharmacie.id,
    });
  }

  if (concept) {
    await createUpdatePharmacieConcept(ctx, {
      ...concept,
      idPharmacie: pharmacie.id,
    });
  }

  if (chiffreAffaire) {
    await createUpdatePharmacieStatCA(ctx, {
      ...chiffreAffaire,
      idPharmacie: pharmacie.id,
    });
  }

  if (digitales && digitales.length) {
    await createUpdatePharmacieDigitale(ctx, pharmacie.id, digitales);
  }

  updateGroupementGroupeClients(ctx.groupementId);

  await ctx.indexing.publishSave('pharmacies', pharmacie.id);
  return pharmacie;
};

export const createPharmacieHistoCip = async (ctx: Context, { idPharmacie, cip }) => {
  const existCip = await ctx.prisma.$exists.pharmacieHistoCip({
    pharmacie: { id: idPharmacie },
    cip,
  });
  return !existCip
    ? ctx.prisma.createPharmacieHistoCip({
        pharmacie: { connect: { id: idPharmacie } },
        cip,
        dateChangementCip: now,
        userCreation: {
          connect: {
            id: ctx.userId,
          },
        },
        userModification: {
          connect: {
            id: ctx.userId,
          },
        },
      })
    : null;
};

export const createUpdatePharmacieGrossiste = async (
  ctx: Context,
  { idPharmacie, idGrossistes },
) => {
  await ctx.prisma.deleteManyPharmacieGrossistes({ pharmacie: { id: idPharmacie } });
  return idGrossistes && idGrossistes.length
    ? Promise.all(
        idGrossistes.map(idGrossiste =>
          ctx.prisma.createPharmacieGrossiste({
            pharmacie: { connect: { id: idPharmacie } },
            grossiste: { connect: { id: idGrossiste } },
          }),
        ),
      )
    : [];
};

export const createUpdatePharmacieGeneriqueur = async (
  ctx: Context,
  { idPharmacie, idGeneriqueurs },
) => {
  await ctx.prisma.deleteManyPharmacieGeneriqueurs({ pharmacie: { id: idPharmacie } });
  return idGeneriqueurs && idGeneriqueurs.length
    ? Promise.all(
        idGeneriqueurs.map(async (idGeneriqueur: any) =>
          ctx.prisma.createPharmacieGeneriqueur({
            pharmacie: { connect: { id: idPharmacie } },
            generiqueur: { connect: { id: idGeneriqueur } },
          }),
        ),
      )
    : [];
};

export const createUpdatePharmacieEntreeSortie = async (
  ctx: Context,
  {
    idPharmacie,
    idContrat,
    idConcurent,
    idConcurentAncien,
    dateEntree,
    idMotifEntree,
    commentaireEntree,
    dateSortieFuture,
    idMotifSortieFuture,
    commentaireSortieFuture,
    dateSortie,
    idMotifSortie,
    commentaireSortie,
    dateSignature,
  }: PharmacieEntreeSortieInput,
) => {
  const entreeSorties = await ctx.prisma.pharmacieEntreeSorties({
    where: { pharmacie: { id: idPharmacie } },
  });
  return ctx.prisma.upsertPharmacieEntreeSortie({
    where: { id: entreeSorties[0] ? entreeSorties[0].id : '' },
    create: {
      pharmacie: { connect: { id: idPharmacie } },
      dateEntree: dateEntree || null,
      commentaireEntree,
      dateSortieFuture: dateSortieFuture || null,
      commentaireSortieFuture,
      dateSortie: dateSortie || null,
      commentaireSortie,
      motifEntree: idMotifEntree ? { connect: { id: idMotifEntree } } : null,
      motifSortie: idMotifSortie ? { connect: { id: idMotifSortie } } : null,
      contrat: idContrat ? { connect: { id: idContrat } } : null,
      concurrent: idConcurent ? { connect: { id: idConcurent } } : null,
      concurrentAncien: idConcurentAncien ? { connect: { id: idConcurentAncien } } : null,
      motifSortieFuture: idMotifSortieFuture ? { connect: { id: idMotifSortieFuture } } : null,
      dateSignature: dateSignature || null,
      userCreation: {
        connect: {
          id: ctx.userId,
        },
      },
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
    update: {
      dateEntree: dateEntree || null,
      commentaireEntree,
      dateSortieFuture: dateSortieFuture || null,
      commentaireSortieFuture,
      dateSortie: dateSortie || null,
      commentaireSortie,
      motifEntree: idMotifEntree ? { connect: { id: idMotifEntree } } : null,
      motifSortie: idMotifSortie ? { connect: { id: idMotifSortie } } : null,
      contrat: idContrat ? { connect: { id: idContrat } } : null,
      concurrent: idConcurent ? { connect: { id: idConcurent } } : null,
      concurrentAncien: idConcurentAncien ? { connect: { id: idConcurentAncien } } : null,
      motifSortieFuture: idMotifSortieFuture ? { connect: { id: idMotifSortieFuture } } : null,
      dateSignature: dateSignature || null,
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
  });
};

export const createUpdatePharmacieSatisfaction = async (
  ctx: Context,
  { idPharmacie, dateSaisie, commentaire, idSmyley }: PharmacieSatisfactionInput,
) => {
  const satisfactions = await ctx.prisma.pharmacieSatisfactions({
    where: { pharmacie: { id: idPharmacie } },
  });
  return ctx.prisma.upsertPharmacieSatisfaction({
    where: { id: satisfactions[0] ? satisfactions[0].id : '' },
    create: {
      pharmacie: { connect: { id: idPharmacie } },
      dateSaisie: dateSaisie || null,
      commentaire,
      smyley: idSmyley ? { connect: { id: idSmyley } } : null,
    },
    update: {
      dateSaisie: dateSaisie || null,
      commentaire,
      smyley: idSmyley ? { connect: { id: idSmyley } } : null,
    },
  });
};

export const createUpdatePharmacieSegementation = async (
  ctx: Context,
  {
    idPharmacie,
    idTypologie,
    idTrancheCA,
    idQualite,
    idContrat,
    dateSignature,
  }: PharmacieSegmentationInput,
) => {
  const segmentations = await ctx.prisma.pharmacieSegmentations({
    where: { pharmacie: { id: idPharmacie } },
  });
  return ctx.prisma.upsertPharmacieSegmentation({
    where: { id: segmentations[0] ? segmentations[0].id : '' },
    create: {
      pharmacie: { connect: { id: idPharmacie } },
      dateSignature: dateSignature || null,
      typologie: idTypologie ? { connect: { id: idTypologie } } : null,
      trancheCA: idTrancheCA ? { connect: { id: idTrancheCA } } : null,
      qualite: idQualite ? { connect: { id: idQualite } } : null,
      contrat: idContrat ? { connect: { id: idContrat } } : null,
      userCreation: {
        connect: {
          id: ctx.userId,
        },
      },
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
    update: {
      dateSignature: dateSignature || null,
      typologie: idTypologie ? { connect: { id: idTypologie } } : null,
      trancheCA: idTrancheCA ? { connect: { id: idTrancheCA } } : null,
      qualite: idQualite ? { connect: { id: idQualite } } : null,
      contrat: idContrat ? { connect: { id: idContrat } } : null,
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
  });
};

export const createUpdatePharmacieCompta = async (
  ctx: Context,
  {
    idPharmacie,
    siret,
    codeERP,
    ape,
    tvaIntra,
    rcs,
    contactCompta,
    chequeGrp,
    valeurChequeGrp,
    commentaireChequeGrp,
    droitAccesGrp,
    valeurChequeAccesGrp,
    commentaireChequeAccesGrp,
    structureJuridique,
    denominationSociale,
    telCompta,
    modifStatut,
    raisonModif,
    dateModifStatut,
    nomBanque,
    banqueGuichet,
    banqueCompte,
    banqueRib,
    banqueCle,
    dateMajRib,
    iban,
    swift,
    dateMajIban,
  }: PharmacieComptaInput,
) => {
  const comptas = await ctx.prisma.pharmacieComptas({
    where: { pharmacie: { id: idPharmacie } },
  });
  return ctx.prisma.upsertPharmacieCompta({
    where: { id: comptas[0] ? comptas[0].id : '' },
    create: {
      siret,
      ape,
      rcs,
      tvaIntra,
      codeERP,
      structureJuridique,
      denominationSociale,
      contactCompta,
      telCompta,
      chequeGrp,
      valeurChequeGrp,
      commentaireChequeGrp,
      droitAccesGrp,
      valeurChequeAccesGrp,
      commentaireChequeAccesGrp,
      modifStatut,
      raisonModif,
      dateModifStatut: dateModifStatut || null,
      nomBanque,
      banqueRib,
      banqueGuichet,
      banqueCompte,
      banqueCle,
      dateMajRib: dateMajRib || null,
      iban,
      swift,
      dateMajIban: dateMajIban || null,
      pharmacie: { connect: { id: idPharmacie } },
      userCreation: {
        connect: {
          id: ctx.userId,
        },
      },
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
    update: {
      siret,
      ape,
      rcs,
      tvaIntra,
      codeERP,
      structureJuridique,
      denominationSociale,
      contactCompta,
      telCompta,
      chequeGrp,
      valeurChequeGrp,
      commentaireChequeGrp,
      droitAccesGrp,
      valeurChequeAccesGrp,
      commentaireChequeAccesGrp,
      modifStatut,
      raisonModif,
      dateModifStatut: dateModifStatut || null,
      nomBanque,
      banqueRib,
      banqueGuichet,
      banqueCompte,
      banqueCle,
      dateMajRib: dateMajRib || null,
      iban,
      swift,
      dateMajIban: dateMajIban || null,
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
  });
};

export const createUpdatePharmacieInformatique = async (
  ctx: Context,
  {
    idPharmacie,
    idLogiciel,
    numVersion,
    dateLogiciel,
    nbrePoste,
    nbreComptoir,
    nbreBackOffice,
    nbreBureau,
    commentaire,
    idAutomate1,
    dateInstallation1,
    idAutomate2,
    dateInstallation2,
    idAutomate3,
    dateInstallation3,
  }: PharmacieInformatiqueInput,
) => {
  const infos = await ctx.prisma.pharmacieInformatiques({
    where: { pharmacie: { id: idPharmacie } },
  });
  return ctx.prisma.upsertPharmacieInformatique({
    where: { id: infos[0] ? infos[0].id : '' },
    create: {
      pharmacie: { connect: { id: idPharmacie } },
      numVersion,
      dateLogiciel: dateLogiciel || null,
      nbrePoste,
      nbreComptoir,
      nbreBackOffice,
      nbreBureau,
      commentaire,
      dateInstallation1: dateInstallation1 || null,
      dateInstallation2: dateInstallation2 || null,
      dateInstallation3: dateInstallation3 || null,
      logiciel: idLogiciel ? { connect: { id: idLogiciel } } : null,
      automate1: idAutomate1 ? { connect: { id: idAutomate1 } } : null,
      automate2: idAutomate2 ? { connect: { id: idAutomate2 } } : null,
      automate3: idAutomate3 ? { connect: { id: idAutomate3 } } : null,
      userCreation: {
        connect: {
          id: ctx.userId,
        },
      },
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
    update: {
      numVersion,
      dateLogiciel: dateLogiciel || null,
      nbrePoste,
      nbreComptoir,
      nbreBackOffice,
      nbreBureau,
      commentaire,
      dateInstallation1: dateInstallation1 || null,
      dateInstallation2: dateInstallation2 || null,
      dateInstallation3: dateInstallation3 || null,
      logiciel: idLogiciel ? { connect: { id: idLogiciel } } : null,
      automate1: idAutomate1 ? { connect: { id: idAutomate1 } } : null,
      automate2: idAutomate2 ? { connect: { id: idAutomate2 } } : null,
      automate3: idAutomate3 ? { connect: { id: idAutomate3 } } : null,
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
  });
};

export const createUpdatePharmacieAchat = async (
  ctx: Context,
  {
    idPharmacie,
    dateDebut,
    dateFin,
    identifiantAchatCanal,
    commentaire,
    idCanal,
  }: PharmacieAchatInput,
) => {
  const achats = await ctx.prisma.pharmacieAchats({
    where: { pharmacie: { id: idPharmacie } },
  });
  return ctx.prisma.upsertPharmacieAchat({
    where: { id: achats[0] ? achats[0].id : '' },
    create: {
      pharmacie: { connect: { id: idPharmacie } },
      dateDebut: dateDebut || null,
      dateFin: dateFin || null,
      identifiantAchatCanal,
      commentaire,
      canal: idCanal ? { connect: { id: idCanal } } : null,
      userCreation: {
        connect: {
          id: ctx.userId,
        },
      },
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
    update: {
      dateDebut: dateDebut || null,
      dateFin: dateFin || null,
      commentaire,
      identifiantAchatCanal,
      canal: idCanal ? { connect: { id: idCanal } } : null,
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
  });
};

export const createUpdatePharmacieCap = async (
  ctx: Context,
  { idPharmacie, dateDebut, dateFin, identifiantCap, commentaire, cap }: PharmacieCapInput,
) => {
  const caps = await ctx.prisma.pharmacieCaps({
    where: { pharmacie: { id: idPharmacie } },
  });
  return ctx.prisma.upsertPharmacieCap({
    where: { id: caps[0] ? caps[0].id : '' },
    create: {
      pharmacie: { connect: { id: idPharmacie } },
      dateDebut: dateDebut || null,
      dateFin: dateFin || null,
      identifiantCap,
      commentaire,
      cap,
      userCreation: {
        connect: {
          id: ctx.userId,
        },
      },
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
    update: {
      dateDebut: dateDebut || null,
      dateFin: dateFin || null,
      identifiantCap,
      commentaire,
      cap,
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
  });
};

export const createUpdatePharmacieConcept = async (
  ctx: Context,
  {
    idPharmacie,
    idFournisseurMobilier,
    dateInstallConcept,
    idConcept,
    avecConcept,
    avecFacade,
    dateInstallFacade,
    idEnseigniste,
    conformiteFacade,
    idFacade,
    surfaceTotale,
    surfaceVente,
    SurfaceVitrine,
    volumeLeaflet,
    nbreLineaire,
    nbreVitrine,
    otcLibAcces,
    idLeaflet,
    commentaire,
  }: PharmacieConceptInput,
) => {
  const concepts = await ctx.prisma.pharmacieConcepts({
    where: { pharmacie: { id: idPharmacie } },
  });
  return ctx.prisma.upsertPharmacieConcept({
    where: { id: concepts[0] ? concepts[0].id : '' },
    create: {
      pharmacie: { connect: { id: idPharmacie } },
      fournisseurMobilier: idFournisseurMobilier
        ? { connect: { id: idFournisseurMobilier } }
        : null,
      dateInstallConcept: dateInstallConcept || null,
      concept: idConcept ? { connect: { id: idConcept } } : null,
      avecConcept,
      avecFacade,
      dateInstallFacade: dateInstallFacade || null,
      enseigniste: idEnseigniste ? { connect: { id: idEnseigniste } } : null,
      conformiteFacade,
      facade: idFacade ? { connect: { id: idFacade } } : null,
      surfaceTotale,
      surfaceVente,
      SurfaceVitrine,
      volumeLeaflet,
      nbreLineaire,
      nbreVitrine,
      otcLibAcces,
      leaflet: idLeaflet ? { connect: { id: idLeaflet } } : null,
      commentaire,
      userCreation: {
        connect: {
          id: ctx.userId,
        },
      },
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
    update: {
      fournisseurMobilier: idFournisseurMobilier
        ? { connect: { id: idFournisseurMobilier } }
        : null,
      dateInstallConcept: dateInstallConcept || null,
      concept: idConcept ? { connect: { id: idConcept } } : null,
      avecFacade,
      avecConcept,
      dateInstallFacade: dateInstallFacade || null,
      enseigniste: idEnseigniste ? { connect: { id: idEnseigniste } } : null,
      conformiteFacade,
      facade: idFacade ? { connect: { id: idFacade } } : null,
      surfaceTotale,
      surfaceVente,
      SurfaceVitrine,
      volumeLeaflet,
      nbreLineaire,
      nbreVitrine,
      otcLibAcces,
      leaflet: idLeaflet ? { connect: { id: idLeaflet } } : null,
      commentaire,
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
  });
};

export const createUpdatePharmacieStatCA = async (
  ctx: Context,
  {
    idPharmacie,
    exercice,
    dateDebut,
    dateFin,
    caTTC,
    caHt,
    caTVA1,
    tauxTVA1,
    caTVA2,
    tauxTVA2,
    caTVA3,
    tauxTVA3,
    caTVA4,
    tauxTVA4,
    caTVA5,
    tauxTVA5,
  }: PharmacieStatCAInput,
) => {
  const concepts = await ctx.prisma.pharmacieStatCAs({
    where: { pharmacie: { id: idPharmacie } },
  });
  return ctx.prisma.upsertPharmacieStatCA({
    where: { id: concepts[0] ? concepts[0].id : '' },
    create: {
      pharmacie: { connect: { id: idPharmacie } },
      exercice,
      dateDebut: dateDebut || null,
      dateFin: dateFin || null,
      caTTC,
      caHt,
      caTVA1,
      tauxTVA1,
      caTVA2,
      tauxTVA2,
      caTVA3,
      tauxTVA3,
      caTVA4,
      tauxTVA4,
      caTVA5,
      tauxTVA5,
      userCreation: {
        connect: {
          id: ctx.userId,
        },
      },
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
    update: {
      exercice,
      dateDebut: dateDebut || null,
      dateFin: dateFin || null,
      caTTC,
      caHt,
      caTVA1,
      tauxTVA1,
      caTVA2,
      tauxTVA2,
      caTVA3,
      tauxTVA3,
      caTVA4,
      tauxTVA4,
      caTVA5,
      tauxTVA5,
      userModification: {
        connect: {
          id: ctx.userId,
        },
      },
    },
  });
};

export const createUpdatePharmacieDigitale = async (
  ctx: Context,
  idPharmacie: string,
  digitales: PharmacieDigitaleInput[],
) => {
  // await ctx.prisma.deleteManyPharmacieGeneriqueurs({ pharmacie: { id: idPharmacie } });
  return digitales && digitales.length
    ? Promise.all(
        digitales.map(async digitale =>
          ctx.prisma.upsertPharmacieDigitale({
            where: { id: digitale.id },
            create: {
              pharmacie: { connect: { id: idPharmacie } },
              servicePharmacie: digitale.idServicePharmacie
                ? { connect: { id: digitale.idServicePharmacie } }
                : null,
              prestataire: digitale.idPrestataire
                ? { connect: { id: digitale.idPrestataire } }
                : null,
              flagService: digitale.flagService,
              dateInstallation: digitale.dateInstallation || null,
              url: digitale.url,
              userCreation: {
                connect: {
                  id: ctx.userId,
                },
              },
              userModification: {
                connect: {
                  id: ctx.userId,
                },
              },
            },
            update: {
              servicePharmacie: digitale.idServicePharmacie
                ? { connect: { id: digitale.idServicePharmacie } }
                : null,
              prestataire: digitale.idPrestataire
                ? { connect: { id: digitale.idPrestataire } }
                : null,
              flagService: digitale.flagService,
              dateInstallation: digitale.dateInstallation || null,
              url: digitale.url,
              userModification: {
                connect: {
                  id: ctx.userId,
                },
              },
            },
          }),
        ),
      )
    : [];
};

export const deleteSoftPharmacie = async ({ id }, ctx: Context) => {
  return ctx.prisma.updatePharmacie({ where: { id }, data: { sortie: 1 } }).then(async updated => {
    await ctx.indexing.publishSave('pharmacies', updated.id);
    return updated;
  });
};
