import {
  COLLABORATEUR_PHARMACIE,
  GETIONNAIRE_DE_COMMANDE,
  COMPTABLE,
  RESPONSABLE_QUALITE_PHARMACIE,
  PREPARATEUR_PHARMACIE,
  STOCK_ET_REASSORT_PHARMACIE,
  APPRENTI_PREPARATEUR_PHARMACIE,
  ETUDIANT_EN_PHARMACIE_PHARMACIE,
  ASSISTANT_DE_DIRECTION_PHARMACIE,
  AUTRE_PROFESSIONNEL_DE_SANTE_PHARMACIE,
} from '../constants/roles';
import { Context } from '../types';
import { deleteUser } from './user';

export const rolesUserPersonnelPharmacie = [
  COLLABORATEUR_PHARMACIE,
  GETIONNAIRE_DE_COMMANDE,
  COMPTABLE,
  RESPONSABLE_QUALITE_PHARMACIE,
  PREPARATEUR_PHARMACIE,
  STOCK_ET_REASSORT_PHARMACIE,
  APPRENTI_PREPARATEUR_PHARMACIE,
  ETUDIANT_EN_PHARMACIE_PHARMACIE,
  ASSISTANT_DE_DIRECTION_PHARMACIE,
  AUTRE_PROFESSIONNEL_DE_SANTE_PHARMACIE,
];

export const getUserPpesonnel = async (ctx: Context, id: string) => {
  return ctx.prisma.ppersonnel({ id }).user();
};

export const deleteSoftPpersonnel = async ({ id }, ctx: Context) => {
  return ctx.prisma.updatePpersonnel({ where: { id }, data: { sortie: 1 } }).then(async updated => {
    // delete user
    const user = await ctx.prisma.ppersonnel({ id: updated.id }).user();
    if (user) {
      await deleteUser(ctx, user);
    }

    await ctx.indexing.publishSave('ppersonnels', updated.id);
    return updated;
  });
};
