import { Context } from '../types';

export const createUpdateUrgence = async (ctx: Context, { id, code, color, libelle }) => {
  return ctx.prisma.upsertUrgence({
    where: { id: id ? id : '' },
    create: {
      code,
      color,
      libelle,
    },
    update: {
      code,
      color,
      libelle,
    },
  });
};
