import { PartageCreateInput, PartageWhereInput, PartageType } from '../generated/prisma-client';
import { Context, PartageInput } from '../types';
import { publishItemSource } from './item';
import { getUserPpersonnelsByPharmacie } from './user';

export const createUpdatePartage = async (input: PartageInput, ctx: Context) => {
  const {
    id,
    type,
    codeMaj,
    toutesPharmacies,
    toutesGroupesAmis,
    codeItem,
    idItemAssocie,
    idUserPartageant,
    idFonctionCible,
    idsGroupesAmis,
    idsPharmacies,
    dateHeurePartage,
    isRemoved,
  } = input;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: PartageCreateInput = {
    type,
    codeMaj,
    toutesPharmacies,
    toutesGroupesAmis,
    dateHeurePartage,
    idItemAssocie,
    isRemoved,
    userCreation,
    userModification,
    item: codeItem ? { connect: { code: codeItem } } : null,
    userPartageant: idUserPartageant ? { connect: { id: idUserPartageant } } : null,
    fonctionCible: idFonctionCible ? { connect: { id: idFonctionCible } } : null,
  };
  const partage = await ctx.prisma.upsertPartage({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  // Pharmacie cible
  if (idsPharmacies && idsPharmacies.length > 0) {
    await Promise.all(
      idsPharmacies.map(async idPharmacie => {
        await ctx.prisma.createPartagePharmacieCible({
          partage: { connect: { id: partage.id } },
          pharmacie: { connect: { id: idPharmacie } },
          userCreation,
          userModification,
        });
      }),
    );
  }

  // Groupe amis cible
  if (idsGroupesAmis && idsGroupesAmis.length > 0) {
    await Promise.all(
      idsGroupesAmis.map(async idGroupesAmis => {
        await ctx.prisma.createPartageGroupeAmisCible({
          partage: { connect: { id: partage.id } },
          groupeAmis: { connect: { id: idGroupesAmis } },
          userCreation,
          userModification,
        });
      }),
    );
  }

  // Publish to elasticsearch
  await ctx.indexing.publishSave('partages', partage.id);
  // Publish to elasticsearch (groupe amis)
  await _publishSaveGroupeAmis(ctx, partage.id);
  await publishItemSource(codeItem, idItemAssocie, ctx);
  return partage;
};

export const softDeletePartage = async (ctx: Context, id: string) => {
  return ctx.prisma
    .updatePartage({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      await ctx.indexing.publishSave('partages', res.id);
      // Publish to elasticsearch (groupe amis)
      await _publishSaveGroupeAmis(ctx, res.id);

      // Publish item
      const item = await ctx.prisma.partage({ id: res.id }).item();
      if (item && res.idItemAssocie) {
        await publishItemSource(item.code, res.idItemAssocie, ctx);
      }

      return res;
    });
};

const _publishSaveGroupeAmis = async (ctx: Context, idPartage: string) => {
  const groupeAmisCibles = await ctx.prisma.partage({ id: idPartage }).groupeAmisCibles();
  if (groupeAmisCibles && groupeAmisCibles.length > 0) {
    await Promise.all(
      groupeAmisCibles.map(async gac => {
        const groupeAmis = await ctx.prisma.partageGroupeAmisCible({ id: gac.id }).groupeAmis();
        if (groupeAmis) {
          await ctx.indexing.publishSave('groupeamises', groupeAmis.id);
        }
      }),
    );
  }
};

export const countPartagesPharmacie = async (
  ctx: Context,
  {
    code,
    idPharmacies,
    idsSourceAssocie,
    type,
  }: {
    code: string;
    idPharmacies: string[];
    idsSourceAssocie: string[];
    type: PartageType;
  },
) => {
  const userPpersonnels = await getUserPpersonnelsByPharmacie(ctx, idPharmacies);
  const whereIdUsers =
    userPpersonnels && userPpersonnels.length
      ? [{ id_in: userPpersonnels.map(userPpersonnel => userPpersonnel.id) }]
      : [];

  const defaultWhere: PartageWhereInput = {
    item: { code },
    idItemAssocie_in: idsSourceAssocie,
    userPartageant: {
      OR: [{ userTitulaires_some: { idPharmacie: { id_in: idPharmacies } } }, ...whereIdUsers],
      AND: [{ groupement: { id: ctx.groupementId } }],
    },
    type,
    isRemoved: false,
  };

  return ctx.prisma
    .partagesConnection({
      where: defaultWhere,
    })
    .aggregate()
    .count();
};
