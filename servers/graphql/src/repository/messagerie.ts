import { Context, MessagerieArgs } from '../types';
import { ApolloError } from 'apollo-server';
import { MutationResolvers } from '../generated/graphqlgen';
import {
  MessagerieHistoCreateInput,
  MessagerieFichierJointCreateInput,
  FichierCreateInput,
  Fichier,
  MessagerieCreateInput,
  MessagerieWhereInput,
  Messagerie,
} from '../generated/prisma-client';
import logger from '../logging';
import moment from 'moment';
import { MESSAGERIE_ENVOYEE } from '../resolvers/Subscription/messagerie';
import { MessagerieType } from '../generated/prisma-client';
import {
  usersLaboratoires,
  usersPartenaires,
  usersPersonnelGroupement,
  usersPharmacie,
  usersRegions,
} from './user';
import ArrayUniqueObject from 'unique-array-objects';

const now = moment().format();

export const createMessagerie = async (
  ctx: Context,
  { inputs }: MutationResolvers.ArgsCreateUpdateMessagerie,
) => {
  const { message, objet, recepteursIds, attachments, themeId, sourceId, typeMessagerie } = inputs;

  console.log('inputs messagerie', JSON.stringify(inputs));

  if (!recepteursIds || (recepteursIds && recepteursIds.length < 0)) {
    return new ApolloError(`Aucun recepteur`, 'EMPTY_IDS_RECEPTEURS');
  }

  const messagerieData: MessagerieCreateInput = {
    userEmetteur: { connect: { id: ctx.userId } },
    dateHeureMessagerie: now,
    message,
    objet,
    messagerieSource: sourceId ? { connect: { id: sourceId } } : null,
    messagerieTheme: themeId ? { connect: { id: themeId } } : null,
    userCreation: { connect: { id: ctx.userId } },
    userModification: { connect: { id: ctx.userId } },
  };
  let messageCreated: Messagerie = null;
  try {
    // Insert message
    messageCreated = await ctx.prisma.createMessagerie({
      ...messagerieData,
    });
  } catch (err) {
    console.log('-----------------------', err);

    return new ApolloError(`Erreur lors de l'envoi de message`, 'ERROR_SEND');
  }

  if (messageCreated) {
    await Promise.all(
      recepteursIds.map(async recepteurId => {
        // Insert Messagerie_histo
        const messagerieHistoData: MessagerieHistoCreateInput = {
          messagerie: { connect: { id: messageCreated.id } },
          typeMessagerie: typeMessagerie,
          userCreation: { connect: { id: ctx.userId } },
          userModification: { connect: { id: ctx.userId } },
          userRecepteur: { connect: { id: recepteurId } },
          lu: false,
          dateHeureLecture: now,
          isRemoved: false,
        };

        await ctx.pubsub
          .publish(MESSAGERIE_ENVOYEE, { messagerieEnvoyee: messageCreated, recepteurId })
          .then(_ => {
            logger.info(` [ CREATE AND PUBLISH MESSAGERIE ], ${JSON.stringify(messageCreated)}`);
          });

        return ctx.prisma.createMessagerieHisto({
          ...messagerieHistoData,
        });
      }),
    );

    const messagerieHistoData: MessagerieHistoCreateInput = {
      messagerie: { connect: { id: messageCreated.id } },
      userCreation: { connect: { id: ctx.userId } },
      userModification: { connect: { id: ctx.userId } },
      userRecepteur: { connect: { id: ctx.userId } },
      lu: false,
      dateHeureLecture: now,
      isRemoved: false,
      typeMessagerie: 'E',
    };

    await ctx.prisma.createMessagerieHisto({
      ...messagerieHistoData,
    });

    // Save attachements
    if (attachments && attachments.length > 0) {
      // Insert Fichier
      await Promise.all(
        attachments.map(async attachment => {
          const fichierData: FichierCreateInput = {
            chemin: attachment.chemin,
            nomOriginal: attachment.nomOriginal,
            type: 'PDF',
          };

          const fileCreated = await ctx.prisma.createFichier({
            ...fichierData,
          });

          // Insert Messagerie_fichier_joint
          const fichierJointData: MessagerieFichierJointCreateInput = {
            fichier: { connect: { id: fileCreated.id } },
            messagerie: { connect: { id: messageCreated.id } },
            userCreation: {
              connect: { id: ctx.userId },
            },
            userModification: {
              connect: { id: ctx.userId },
            },
          };

          await ctx.prisma.createMessagerieFichierJoint({
            ...fichierJointData,
          });
        }),
      );
    }

    return messageCreated;
  }

  return new ApolloError(`Erreur lors de l'envoi de message`, 'ERROR_SEND');
};

export const messageries = async (ctx: Context, args: MessagerieArgs) => {
  const { take, skip, typeMessagerie, year, isRemoved, lu, orderBy, typeFilter } = args;

  let where: MessagerieWhereInput | undefined = await getMessagerieWhere(ctx, args);

  let whereMessagerie: any = {
    where,
    orderBy: orderBy && orderBy != 'lu_ASC' && orderBy != 'lu_DESC' ? orderBy : 'dateCreation_DESC',
  };

  if (take >= 0 && skip >= 0) {
    whereMessagerie = {
      ...whereMessagerie,
      first: take,
      skip,
    };
  }

  console.log('WHERE MESSAGERIE', JSON.stringify(whereMessagerie));

  let messageries = [];
  if (orderBy === 'lu_ASC' || orderBy === 'lu_DESC') {
    const messages = await ctx.prisma
      .messagerieHistoes({
        where: {
          messagerie: where,
          typeMessagerie,
        },
        orderBy,
      })
      .then(histoes => {
        if (histoes && histoes.length)
          return Promise.all(
            histoes.map(histo => ctx.prisma.messagerieHisto({ id: histo.id }).messagerie()),
          );
        return [];
      });

    messageries = ArrayUniqueObject(messages);
  } else {
    messageries = await ctx.prisma.messageries(whereMessagerie);
  }

  console.log('++++++++++++++++++++', messageries);

  const total = await ctx.prisma
    .messageriesConnection({ where })
    .aggregate()
    .count();

  return { total, data: messageries as any };
};

const getMessagerieWhere = async (ctx: Context, args: MessagerieArgs) => {
  const { typeMessagerie, year, isRemoved, lu, typeFilter } = args;
  let where: MessagerieWhereInput | undefined = {};
  const whereRemoved = isRemoved !== undefined || isRemoved !== null ? { isRemoved } : {};
  const whereLu = lu !== undefined || lu !== null ? { lu: lu } : {};
  const whereType = typeMessagerie ? { typeMessagerie: typeMessagerie as any } : {};

  if (year) {
    const firstDate = new Date(year, 0, 1);
    const lastDate = new Date(year, 12, 0);
    where = { ...where, dateCreation_gte: firstDate, dateCreation_lt: lastDate };
  }

  if (typeFilter) {
    const users = await usersByFilterMessage(ctx, typeFilter);
    where = await whereWithTypeFilter(
      ctx,
      users,
      typeMessagerie,
      where,
      whereType,
      whereLu,
      whereRemoved,
    );
  } else {
    where = whereWithoutTypeFilter(
      ctx,
      typeMessagerie as any,
      where,
      whereType,
      whereLu,
      whereRemoved,
    );
  }

  return where;
};

const whereWithTypeFilter = (
  ctx: Context,
  users: any[],
  typeMessagerie: any,
  where: any,
  whereType: any,
  whereLu: any,
  whereRemoved: any,
) => {
  switch (typeMessagerie) {
    case 'E' as any:
      return {
        ...where,
        userEmetteur: {
          id: ctx.userId,
        },
        AND: [
          {
            messagerieHistoes_some: {
              typeMessagerie: 'R' as any,
              ...whereLu,
              userRecepteur: {
                id_in: users && users.length ? users.map(user => user.id) : [],
              },
            },
          },
          {
            messagerieHistoes_none: {
              isRemoved: true,
              typeMessagerie,
              userRecepteur: {
                id: ctx.userId,
              },
            },
          },
        ],
      };
    case 'R' as any:
      return {
        ...where,
        userEmetteur: {
          id_in:
            users && users.length
              ? users.map(user => user.id).filter(usr => usr.id !== ctx.userId)
              : [],
        },
        messagerieHistoes_some: {
          ...whereType,
          isRemoved: false,
          ...whereLu,
          userRecepteur: {
            id: ctx.userId,
          },
        },
      };
    default:
      return {
        ...where,
        OR: [
          {
            // RECEPTION
            userEmetteur: {
              id_in:
                users && users.length
                  ? users.map(user => user.id).filter(usr => usr.id !== ctx.userId)
                  : [],
            },
            messagerieHistoes_some: {
              typeMessagerie: 'R' as any,
              ...whereLu,
              isRemoved: true,
              suppressionPermanente: false,
              userRecepteur: {
                id: ctx.userId,
              },
            },
          },
          {
            // EMMETEUR
            ...where,
            userEmetteur: {
              id: ctx.userId,
            },
            messagerieHistoes_some: {
              typeMessagerie: 'E' as any,
              ...whereLu,
              isRemoved: true,
              suppressionPermanente: false,
              userRecepteur: {
                id_in: users && users.length ? users.map(user => user.id) : [],
              },
            },
          },
        ],
      };
  }
};

const usersByFilterMessage = async (ctx: Context, typeFilter: string) => {
  const isTitulaire = await ctx.prisma.$exists.userTitulaire({ idUser: { id: ctx.userId } });
  const isPpersonnel = await ctx.prisma.$exists.ppersonnel({ user: { id: ctx.userId } });
  switch (typeFilter) {
    case 'MY_PHARMACIE':
      return isTitulaire || isPpersonnel
        ? await usersPharmacie(ctx, { idPharmacie: ctx.pharmacieId })
        : [];
    case 'OTHER_PHARMACIE':
      return isTitulaire || isPpersonnel
        ? await usersPharmacie(ctx, { idPharmacie: ctx.pharmacieId }, true)
        : [];
    case 'MY_REGION':
      return isTitulaire || isPpersonnel
        ? await usersRegions(ctx, { idPharmacie: ctx.pharmacieId })
        : [];
    case 'MY_GROUPEMENT':
      return usersPersonnelGroupement(ctx, { idGroupement: ctx.groupementId });
    case 'LABORATOIRE':
      return usersLaboratoires(ctx);
  }

  return [];
};

const whereWithoutTypeFilter = (
  ctx: Context,
  typeMessagerie: MessagerieType,
  where: any,
  whereType: any,
  whereLu: any,
  whereRemoved: any,
) => {
  switch (typeMessagerie) {
    case 'E' as any:
      return {
        ...where,
        userEmetteur: {
          id: ctx.userId,
        },
        AND: [
          {
            messagerieHistoes_some: {
              ...whereType,
              ...whereLu,
              userRecepteur: {
                id: ctx.userId,
              },
            },
          },
          {
            messagerieHistoes_none: {
              isRemoved: true,
              typeMessagerie,
              userRecepteur: {
                id: ctx.userId,
              },
            },
          },
        ],
      };

    case 'R' as any:
      return {
        ...where,
        messagerieHistoes_some: {
          ...whereType,
          isRemoved: false,
          ...whereLu,
          userRecepteur: {
            id: ctx.userId,
          },
        },
      };

    default:
      return {
        ...where,
        OR: [
          {
            // EMMETEUR
            ...where,
            userEmetteur: {
              id: ctx.userId,
            },
            messagerieHistoes_some: {
              typeMessagerie: 'E' as any,
              ...whereRemoved,
              suppressionPermanente: false,
              ...whereLu,
              userRecepteur: {
                id: ctx.userId,
              },
            },
          },
          {
            // RECEPTION
            ...where,
            messagerieHistoes_some: {
              typeMessagerie: 'R' as any,
              ...whereRemoved,
              suppressionPermanente: false,
              ...whereLu,
              userRecepteur: {
                id: ctx.userId,
              },
            },
          },
        ],
      };
  }
};

/* FILTE MESSAGERIE */
export const filterMessageriesCount = async (ctx: Context, typeMessagerie: string) => {
  const removed =
    typeMessagerie == undefined || typeMessagerie == null
      ? {}
      : { isRemoved: typeMessagerie ? true : false };
  const nbMsgsPharmacie = await countMessageriesByFilter(ctx, {
    typeMessagerie: typeMessagerie as any,
    ...removed,
    typeFilter: 'MY_PHARMACIE',
  });
  const nbMsgsOtherPharmacie = await countMessageriesByFilter(ctx, {
    typeMessagerie: typeMessagerie as any,
    ...removed,
    typeFilter: 'OTHER_PHARMACIE',
  });
  const nbMsgsRegion = await countMessageriesByFilter(ctx, {
    typeMessagerie: typeMessagerie as any,
    ...removed,
    typeFilter: 'MY_REGION',
  });
  const nbMsgsGrp = await countMessageriesByFilter(ctx, {
    typeMessagerie: typeMessagerie as any,
    isRemoved: typeMessagerie ? true : false,
    typeFilter: 'MY_GROUPEMENT',
  });
  const nbMsgsLabo = await countMessageriesByFilter(ctx, {
    typeMessagerie: typeMessagerie as any,
    ...removed,
    typeFilter: 'LABORATOIRE',
  });
  /* const countPartenaireService = await countPartenaire(ctx); */

  return [
    {
      libelle: 'Ma pharmacie',
      code: 'MY_PHARMACIE',
      count: nbMsgsPharmacie,
    },
    {
      libelle: 'Mes confrères',
      code: 'OTHER_PHARMACIE',
      count: nbMsgsOtherPharmacie,
    },
    {
      libelle: 'Ma région',
      code: 'MY_REGION',
      count: nbMsgsRegion,
    },
    {
      libelle: 'Mon Groupement',
      code: 'MY_GROUPEMENT',
      count: nbMsgsGrp,
    },
    {
      libelle: 'Laboratoire Partenaire',
      code: 'LABORATOIRE',
      count: nbMsgsLabo,
    },

    {
      libelle: 'Partenaire de Service',
      code: 'PARTENAIRE_SERVICE',
      count: 0,
    },
    ,
  ] as any;
};

const countMessageriesByFilter = async (ctx: Context, args: MessagerieArgs) => {
  let where: MessagerieWhereInput | undefined = await getMessagerieWhere(ctx, args);
  return ctx.prisma
    .messageriesConnection({ where })
    .aggregate()
    .count();
};
