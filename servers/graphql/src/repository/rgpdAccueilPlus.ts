import { RgpdAccueilPlusCreateInput } from '../generated/prisma-client';
import { Context, RgpdAccueilPlusInput } from '../types';

export const createUpdateRgpdAccueilPlus = async (ctx: Context, input: RgpdAccueilPlusInput) => {
  const { id, title, description, order } = input;

  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;

  let defaultOrder: number = 0;
  if (id) {
    defaultOrder = await ctx.prisma.rgpdAccueilPlus({ id }).order();
  } else {
    defaultOrder = await ctx.prisma
      .rgpdAccueilPlusesConnection({ where: { groupement: { id: ctx.groupementId } } })
      .aggregate()
      .count();
  }

  const data: RgpdAccueilPlusCreateInput = {
    title,
    description,
    order: order ? order : id ? defaultOrder : defaultOrder + 1,
    groupement,
    userCreation,
    userModification,
  };

  const res = await ctx.prisma.upsertRgpdAccueilPlus({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  return res;
};
