import { Context, OperationArticleInput } from '../types';
import { Operation, OperationArticle } from '../generated/prisma-client';
import logger from '../logging';

export const createOperationArticle = async (
  ctx: Context,
  operation: Operation,
  article: OperationArticleInput,
): Promise<OperationArticle> => {
  const operationArticle = await ctx.prisma.createOperationArticle({
    idOperation: {
      connect: {
        id: operation.id,
      },
    },
    produitCanal: {
      connect: {
        id: article.idCanalArticle,
      },
    },
    quantite: article.quantite,
  });

  ctx.indexing.publishSave('produitcanals', article.idCanalArticle);

  return operationArticle;
};

export const createArticleCibles = async (
  ctx: Context,
  operation: Operation,
  articles: OperationArticleInput[],
) => {
  return Promise.all(articles.map(article => createOperationArticle(ctx, operation, article))).then(
    _ => {
      logger.info(
        `[OPERATION] CREATE UPDATE OPERATION ARTICLE CIBLE : operation => ${JSON.stringify(
          operation,
        )}`,
      );
      return _;
    },
  );
};
