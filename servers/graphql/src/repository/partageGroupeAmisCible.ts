import { PartageGroupeAmisCibleCreateInput } from '../generated/prisma-client';
import { Context, PartageGroupeAmisCibleInput } from '../types';

export const createUpdatePartageGroupeAmisCible = async (
  input: PartageGroupeAmisCibleInput,
  ctx: Context,
) => {
  const { id, idPartage, idGroupeAmis } = input;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: PartageGroupeAmisCibleCreateInput = {
    userCreation,
    userModification,
    partage: idPartage ? { connect: { id: idPartage } } : null,
    groupeAmis: idGroupeAmis ? { connect: { id: idGroupeAmis } } : null,
  };
  const partageGroupeAmisCible = await ctx.prisma.upsertPartageGroupeAmisCible({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  // await ctx.indexing.publishSave('groupeamises', partageGroupeAmisCible.id);
  return partageGroupeAmisCible;
};
