import {
  TicketCreateInput,
  TicketChangementCibleCreateInput,
  FichierCreateInput,
  TicketFichierJointCreateInput,
  TicketChangementStatutCreateInput,
} from '../generated/prisma-client';
import { Context, TicketInput, TicketCibleInput, StatusTicket } from '../types';

export const createUpdateTicket = async (input: TicketInput, ctx: Context) => {
  console.log('createUpdateTicket -> input', input);

  const {
    id,
    origine,
    idOrganisation,
    nomInterlocuteur,
    telephoneInterlocuteur,
    dateHeureSaisie,
    commentaire,
    idSmyley,
    visibilite,
    typeTicket,
    declarant,
    idMotif,
    fichiers,
    idsUserConcernees,
    priority,
    status,
    typeAppel,
  } = input;

  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userModification = { connect: { id: ctx.userId } };

  const ticketData: TicketCreateInput = {
    idOrganisation,
    nomInterlocuteur,
    telephoneInterlocuteur,
    commentaire,
    dateHeureSaisie: dateHeureSaisie || new Date(),
    visibilite,
    typeTicket,
    declarant: declarant ? { connect: { id: declarant } } : null,
    motif: idMotif ? { connect: { id: idMotif } } : null,
    origine: (origine as any) || null,
    smyley: idSmyley ? { connect: { id: idSmyley } } : null,
    groupement,
    userModification,
    priority,
    status: status as any,
    typeAppel,
  };

  console.log('createUpdateTicket -> ticketData', ticketData);

  // Save ticket
  const ticket = await ctx.prisma.upsertTicket({
    where: { id: id || '' },
    create: { ...ticketData, userCreation: { connect: { id: ctx.userId } } },
    update: ticketData,
  });

  if (ticket) {
    if (idsUserConcernees && idsUserConcernees.length) {
      await ctx.prisma.deleteManyTicketUserConcernees({
        ticket: { id: ticket.id },
        userConcernee: { id_not_in: idsUserConcernees },
      });
      await Promise.all(
        idsUserConcernees.map(async idUser => {
          const exist = await ctx.prisma.$exists.ticketUserConcernee({
            ticket: { id: ticket.id },
            userConcernee: { id: idUser },
          });
          if (!exist)
            await ctx.prisma.createTicketUserConcernee({
              ticket: { connect: { id: ticket.id } },
              userConcernee: { connect: { id: idUser } },
            });
        }),
      );
    }

    // Save files table (Ticket_fichier_joint)
    if (fichiers && fichiers.length > 0) {
      const savedFiles = await Promise.all(
        fichiers.map(async file => {
          const fileData: FichierCreateInput = {
            chemin: file.chemin,
            nomOriginal: file.nomOriginal,
            type: file.type,
          };
          return ctx.prisma.upsertFichier({
            where: { id: file.id || '' },
            create: fileData,
            update: fileData,
          });
        }),
      );

      const ticketFilesToDelete =
        savedFiles && savedFiles.length
          ? await ctx.prisma.ticketFichierJoints({
              where: {
                ticket: { id: ticket.id },
                fichier: { id_not_in: savedFiles.map(saved => saved.id) },
              },
            })
          : [];

      const filesToDelete =
        ticketFilesToDelete && ticketFilesToDelete.length
          ? await Promise.all(
              ticketFilesToDelete.map(ticketFichierJoint =>
                ctx.prisma.ticketFichierJoint({ id: ticketFichierJoint.id }).fichier(),
              ),
            )
          : [];
      /* delete data in table ticketFichierJoints */
      if (ticketFilesToDelete && ticketFilesToDelete.length > 0) {
        await Promise.all(
          ticketFilesToDelete.map(ticketFile =>
            ctx.prisma.ticketFichierJoint({ id: ticketFile.id }),
          ),
        );
      }

      /* delete file in table fichier */
      if (filesToDelete && filesToDelete.length > 0) {
        await Promise.all(filesToDelete.map(file => ctx.prisma.deleteFichier({ id: file.id })));
      }
      // Save Ticket_fichier_joint
      if (savedFiles && savedFiles.length > 0) {
        await Promise.all(
          savedFiles.map(async sf => {
            const fJData: TicketFichierJointCreateInput = {
              fichier: { connect: { id: sf.id } },
              ticket: { connect: { id: ticket.id } },
              userCreation: { connect: { id: ctx.userId } },
              userModification,
            };
            // If exist ticket file exist, do update else do create
            const existTicketFiles = await ctx.prisma.ticketFichierJoints({
              where: { fichier: { id: sf.id }, ticket: { id: ticket.id } },
            });

            console.log('createUpdateTicket -> existTicketFiles', existTicketFiles);

            if (existTicketFiles && existTicketFiles.length > 0) {
              // Update
              await Promise.all(
                existTicketFiles.map(async exTF => {
                  await ctx.prisma.updateTicketFichierJoint({
                    where: { id: exTF.id },
                    data: fJData,
                  });
                }),
              );
            } else {
              // Create
              await ctx.prisma.createTicketFichierJoint(fJData);
            }
          }),
        );
      }
    }

    // Publish to elasticsearch
    await ctx.indexing.publishSave('tickets', ticket.id);
  }

  return ticket;
};

/* export const createUpdateTicket = async (input: TicketInput, ctx: Context) => {
  console.log('createUpdateTicket -> input', input);

  const {
    id,
    dateHeureSaisie,
    commentaire,
    visibilite,
    typeTicket,
    idCurrentUser,
    idMotif,
    idOrigine,
    idSmyley,
    fichiers,
    cible,
  } = input;

  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = { connect: { id: idCurrentUser } };
  const userModification = { connect: { id: idCurrentUser } };

  const ticketData: TicketCreateInput = {
    commentaire,
    dateHeureSaisie: dateHeureSaisie || new Date(),
    visibilite,
    typeTicket,
    motif: { connect: { id: idMotif } },
    origine: idOrigine ? { connect: { id: idOrigine } } : null,
    smyley: idSmyley ? { connect: { id: idSmyley } } : null,
    groupement,
    userCreation,
    userModification,
  };

  console.log('createUpdateTicket -> ticketData', ticketData);

  // Save ticket
  const ticket = await ctx.prisma.upsertTicket({
    where: { id: id || '' },
    create: ticketData,
    update: ticketData,
  });

  console.log('createUpdateTicket -> ticket', ticket);

  // Save cible (table Ticket_changement_cible)
  if (ticket) {
    if (cible) {
      const cibleInput: TicketCibleInput = { ...cible, idTicket: ticket.id };
      console.log('cibleInput :>> ', cibleInput);
      const ticketCible = await updateTicketCible(cibleInput, ctx);
      console.log('createUpdateTicket -> ticketCible', ticketCible);

      // Save status
      if (ticketCible) {
        // TODO : Check code
        const defautlStatut = await ctx.prisma.ticketStatut({ code: 'NON_RESOLU' });
        // Save Ticket_change_status
        if (defautlStatut) {
          await ctx.prisma.createTicketChangementStatut({
            dateHeureChangement: new Date(),
            groupement,
            userCreation,
            userModification,
            ticket: { connect: { id: ticket.id } },
            ticketStatut: defautlStatut ? { connect: { id: defautlStatut.id } } : null,
            ticketChangementCible: { connect: { id: ticketCible.id } },
          });
        }

        // Publish ticket cible to elasticsearch
        await ctx.indexing.publishSave('ticketchangementcibles', ticketCible.id);
      }
    }

    // Save files table (Ticket_fichier_joint)
    if (fichiers && fichiers.length > 0) {
      const savedFiles = await Promise.all(
        fichiers.map(async f => {
          const fData: FichierCreateInput = {
            chemin: f.chemin,
            nomOriginal: f.nomOriginal,
            type: f.type,
          };
          const savedF = await ctx.prisma.upsertFichier({
            where: { id: f.id || '' },
            create: fData,
            update: fData,
          });
          return savedF;
        }),
      );

      console.log('createUpdateTicket -> savedFiles', savedFiles);

      // Save Ticket_fichier_joint
      if (savedFiles && savedFiles.length > 0) {
        await Promise.all(
          savedFiles.map(async sf => {
            const fJData: TicketFichierJointCreateInput = {
              fichier: { connect: { id: sf.id } },
              ticket: { connect: { id: ticket.id } },
              groupement,
              userCreation,
              userModification,
            };
            // If exist ticket file exist, do update else do create
            const existTicketFiles = await ctx.prisma.ticketFichierJoints({
              where: { fichier: { id: sf.id }, ticket: { id: ticket.id } },
            });

            console.log('createUpdateTicket -> existTicketFiles', existTicketFiles);

            if (existTicketFiles && existTicketFiles.length > 0) {
              // Update
              await Promise.all(
                existTicketFiles.map(async exTF => {
                  await ctx.prisma.updateTicketFichierJoint({
                    where: { id: exTF.id },
                    data: fJData,
                  });
                }),
              );
            } else {
              // Create
              await ctx.prisma.createTicketFichierJoint(fJData);
            }
          }),
        );
      }
    }
  }

  // Publish to elasticsearch
  await ctx.indexing.publishSave('tickets', ticket.id);
  if (cible && cible.idPharmacieCible) {
    await ctx.indexing.publishSave('pharmacies', cible.idPharmacieCible);
  }
  if (cible && cible.idUserCible) {
    await ctx.indexing.publishSave('users', cible.idUserCible);
  }
  return ticket;
}; */

export const updateTicketCible = async (input: TicketCibleInput, ctx: Context) => {
  console.log('updateTicketCible -> input', input);
  const {
    id,
    commentaire,
    dateHeureAffectation,
    idTicket,
    idUserCible,
    idRoleCible,
    idServiceCible,
    idPharmacieCible,
    idLaboratoireCible,
    idPrestataireServiceCible,
    idCurrentUser,
  } = input;
  const ticketCibleData: TicketChangementCibleCreateInput = {
    commentaire: commentaire || '',
    dateHeureAffectation: dateHeureAffectation || new Date(),
    ticket: idTicket ? { connect: { id: idTicket } } : null,
    userCible: idUserCible ? { connect: { id: idUserCible } } : null,
    roleCible: idRoleCible ? { connect: { id: idRoleCible } } : null,
    serviceCible: idServiceCible ? { connect: { id: idServiceCible } } : null,
    pharmacieCible: idPharmacieCible ? { connect: { id: idPharmacieCible } } : null,
    laboratoireCible: idLaboratoireCible ? { connect: { id: idLaboratoireCible } } : null,
    prestataireServiceCible: idPrestataireServiceCible
      ? { connect: { id: idPrestataireServiceCible } }
      : null,
    groupement: ctx.groupementId ? { connect: { id: ctx.groupementId } } : null,
    userCreation: { connect: { id: idCurrentUser } },
    userModification: { connect: { id: idCurrentUser } },
  };
  const ticketCible = await ctx.prisma.upsertTicketChangementCible({
    where: { id: id || '' },
    create: ticketCibleData,
    update: ticketCibleData,
  });

  // Publish to elasticsearch
  await ctx.indexing.publishSave('ticketchangementcibles', ticketCible.id);
  return ticketCible;
};

export const updateTicketStatut = async (
  { id, status }: { id: string; status: StatusTicket },
  ctx: Context,
) => {
  // Publish to elasticsearch
  return ctx.prisma.updateTicket({ where: { id }, data: { status } }).then(async ticket => {
    await ctx.indexing.publishSave('tickets', ticket.id);
    return ticket;
  });
};
