import { ApolloError } from 'apollo-server';
import { head } from 'lodash';
import {
  Couleur,
  ProjectCreateInput,
  TodoProjetFavorisCreateInput,
  TypeProject,
} from '../generated/prisma-client';
import logger from '../logging';
import { Context, ProjectInput } from '../types';

export const createUpdateProject = async (ctx: Context, input: ProjectInput) => {
  const {
    idPharmacie,
    idUser,
    id,
    idCouleur,
    isArchived,
    isShared,
    isFavoris,
    isRemoved,
    ordre,
    name,
    typeProject,
    idParent,
  } = input;

  // Check exist project name for current user
  const existsProjects = await ctx.prisma.projects({ where: { user: { id: idUser }, name } });

  if (!id && existsProjects.length > 0) {
    return new ApolloError(
      `Ce nom existe déjà dans la liste de vos projets, veuillez choisir un autre nom`,
      'NAME_ALREADY_TAKEN',
    );
  }

  let defaultOrder: number = 0;
  if (id) {
    defaultOrder = await ctx.prisma.project({ id }).ordre();
  } else {
    defaultOrder = await ctx.prisma
      .projectsConnection({
        where: { user: { id: idUser } },
      })
      .aggregate()
      .count();
  }

  // If order and order already belongs to another project,
  // Update the order of the other projects
  if (ordre) {
    const projects = await ctx.prisma.projects({
      where: { user: { id: ctx.userId }, ordre_gte: ordre },
    });
    if (projects && projects.length > 0) {
      await Promise.all(
        projects.map(async p => {
          await ctx.prisma.updateProject({ where: { id: p.id }, data: { ordre: p.ordre + 1 } });
          await ctx.indexing.publishSave('projects', p.id);
        }),
      );
    }
  }

  let defaultColor: Couleur;
  if (!idCouleur) {
    defaultColor = await ctx.prisma.couleur({ code: '2167842' });
  }

  try {
    const data: ProjectCreateInput = {
      name,
      isArchived,
      isRemoved,
      isShared,
      ordre: ordre ? ordre : id ? defaultOrder : defaultOrder + 1,
      typeProject: typeProject || 'PERSONNEL',
      groupement: ctx.groupementId ? { connect: { id: ctx.groupementId } } : null,
      pharmacie: idPharmacie ? { connect: { id: idPharmacie } } : null,
      user: idUser ? { connect: { id: idUser } } : null,
      couleur: idCouleur
        ? { connect: { id: idCouleur } }
        : defaultColor
        ? { connect: { id: defaultColor.id } }
        : null,
      projetParent: idParent ? { connect: { id: idParent } } : null,
      userCreation: idUser ? { connect: { id: idUser } } : null,
      userModification: idUser ? { connect: { id: idUser } } : null,
    };

    const project = await ctx.prisma.upsertProject({
      where: { id: id ? id : '' },
      create: data,
      update: data,
    });

    // Add project to Favoris
    if (project) {
      const subProjects = await updateSubProjectType({ idProject: project.id, typeProject }, ctx);
      if (subProjects && subProjects.length)
        await Promise.all(
          subProjects.map(pct => {
            return ctx.indexing.publishSave('projects', pct.id);
          }),
        );

      let favorisOrder: number = 0;
      favorisOrder = await ctx.prisma
        .todoProjetFavorisesConnection({
          where: { user: { id: idUser } },
        })
        .aggregate()
        .count();

      const todoProjetFavorisData: TodoProjetFavorisCreateInput = {
        project: { connect: { id: project.id } },
        ordre: favorisOrder + 1,
        user: idUser ? { connect: { id: idUser } } : null,
        userCreation: idUser ? { connect: { id: idUser } } : null,
        userModification: idUser ? { connect: { id: idUser } } : null,
      };

      // Create
      if (!id && isFavoris) {
        const todoProjetFavoris = await ctx.prisma.createTodoProjetFavoris(todoProjetFavorisData);
        await ctx.indexing.publishSave('todoprojetfavorises', todoProjetFavoris.id);
      }

      // Edit
      if (id) {
        const existTodoProjetFavorises = await ctx.prisma.todoProjetFavorises({
          where: { user: { id: idUser }, project: { id: project.id } },
        });
        const existTodoProjetFavoris = head(existTodoProjetFavorises);
        // If exist (existTodoProjetFavoris) and isFavoris === true --> DO NOT DO ANYTHING
        // If exist (existTodoProjetFavoris) and isFavoris === false --> DO DELETE
        if (existTodoProjetFavoris) {
          if (isFavoris === false) {
            const deleted = await ctx.prisma.deleteTodoProjetFavoris({
              id: existTodoProjetFavoris.id,
            });
            await ctx.indexing.publishDelete('todoprojetfavorises', deleted.id);
          }
        } else {
          // If is not exist create
          const todoProjetFavoris = await ctx.prisma.createTodoProjetFavoris(todoProjetFavorisData);
          await ctx.indexing.publishSave('todoprojetfavorises', todoProjetFavoris.id);
        }
      }
    }

    await ctx.indexing.publishSave('projects', project.id);
    return project;
  } catch (err) {
    console.log('err :>> ', err);
    const action = id ? ['la modification', 'EDIT_ERROR'] : ["l'insetion", 'INSERT_ERROR'];
    logger.error(`Erreur lors de ${action[0]} du projet: `, { err });
    return new ApolloError(`Erreur c\'est produite lors de ${action[0]} du projet`, action[1]);
  }
};

export const updateSubProjectType = async ({ idProject, typeProject }, ctx: Context) => {
  const subs = await ctx.prisma.projects({ where: { projetParent: { id: idProject } } });

  if (subs && subs.length) {
    return Promise.all(
      subs.map(sub => {
        return ctx.prisma.updateProject({
          where: { id: sub.id },
          data: {
            typeProject: typeProject || 'PERSONNEL',
          },
        });
      }),
    );
  }

  return [];
};

export const deleteAction = async (ctx: Context, { id }) => {
  return ctx.prisma
    .updateProject({ where: { id }, data: { isRemoved: true } })
    .then(async _ => {
      /*
        DELETE SOFT ALL SUB-PROJECT
      */
      const projects = await ctx.prisma
        .projects({
          where: { projetParent: id },
        })
        .then(async subProjects =>
          Promise.all(
            subProjects.map(async sub =>
              ctx.prisma.updateProject({ where: { id: sub.id }, data: { isRemoved: true } }),
            ),
          ),
        );

      const ids = projects && projects.length ? [id, projects.map(projet => projet.id)] : [id];
      /*
        DELETE SOFT ALL ACTIONS
       */
      await ctx.prisma
        .actions({
          where: { project: { id_in: ids } },
        })
        .then(async actions =>
          Promise.all(
            actions.map(async action =>
              ctx.prisma.updateAction({ where: { id: action.id }, data: { isRemoved: true } }),
            ),
          ),
        );

      await Promise.all(
        ids.map(async projectId => ctx.indexing.publishSave('projects', projectId)),
      );

      return ctx.prisma.project({ id });
    })
    .catch(err => {
      logger.error(`Erreur lors de la suppression du projet : `, { err });
      return new ApolloError(`Erreur lors de la suppression du projet`, 'DELETE_ERROR');
    });
};

export const setArchivedProject = async (ctx: Context, { id, archived }) => {
  return ctx.prisma
    .updateProject({ where: { id }, data: { isArchived: archived } })
    .then(async project => {
      await ctx.indexing.publishSave('projects', project.id);
      return project;
    })
    .catch(err => {
      logger.error(`Erreur lors de l'archivage du projet : `, { err });
      if (archived) {
        return new ApolloError(`Erreur lors de l'archivage du projet`, 'ARCHIVED_ERROR');
      }
      return new ApolloError(`Erreur lors du désarchivage du projet`, 'UNARCHIVED_ERROR');
    });
};

export const softDeleteProject = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma.updateProject({ where: { id }, data: { isRemoved: true } }).then(async res => {
    await ctx.indexing.publishSave('projects', res.id);
    return res;
  });
};

export const countProjectByUserAndType = (ctx: Context, type: TypeProject, idUser: string) => {
  return ctx.prisma
    .projectsConnection({
      where: {
        OR: [
          { user: { id: idUser }, isRemoved: false, typeProject: type },
          {
            participants_some: {
              isRemoved: false,
              project: { typeProject: type, userCreation: { id_not: idUser } },
              userParticipant: { id_in: [idUser] },
            },
          },
        ],
      },
    })
    .aggregate()
    .count();
};
