import { Context } from '../types';
import { ApolloError } from 'apollo-server';
import { Avatar } from '../generated/prisma-client';

export const createUpdateAvatar = async (ctx: Context, { id, description, codeSexe, fichier }) => {
  if (!fichier || (fichier && !fichier.chemin)) return null;

  let createdAvatar: Avatar = null;

  try {
    createdAvatar = await ctx.prisma.upsertAvatar({
      where: {
        id: id || '',
      },
      create: {
        codeSexe,
        description,
        groupement: {
          connect: {
            id: ctx.groupementId,
          },
        },
        userCreation: {
          connect: {
            id: ctx.userId,
          },
        },
        userModification: {
          connect: {
            id: ctx.userId,
          },
        },
      },
      update: {
        codeSexe,
        description,
        userModification: {
          connect: {
            id: ctx.userId,
          },
        },
      },
    });
  } catch (error) {
    return new ApolloError(`Erreur lors de la création de l'avatar`, 'ERROR_CREATE_AVATAR');
  }

  if (!createdAvatar) {
    return new ApolloError(`Erreur lors de la création de l'avatar`, 'ERROR_CREATE_AVATAR');
  }

  const fichiers = await ctx.prisma.fichiers({ where: { avatar: { id: createdAvatar.id } } });

  try {
    if (fichiers && fichiers.length) {
      await ctx.prisma.updateManyFichiers({
        where: {
          avatar: { id: createdAvatar.id },
        },
        data: {
          nomOriginal: fichier.nomOriginal,
          type: fichier.type,
          chemin: fichier.chemin,
        },
      });
    } else {
      await ctx.prisma.createFichier({
        nomOriginal: fichier.nomOriginal,
        type: fichier.type,
        chemin: fichier.chemin,
        avatar: {
          connect: {
            id: createdAvatar.id,
          },
        },
      });
    }
  } catch (error) {
    ctx.prisma.deleteAvatar({ id: createdAvatar.id });
    return new ApolloError(`Erreur lors de la création de l'avatar`, 'ERROR_CREATE_AVATAR');
  }

  await ctx.indexing.publishSave('avatars', createdAvatar.id);
  return createdAvatar;
};
