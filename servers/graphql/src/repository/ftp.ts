import { Context } from '../types';

export const FtpConnect = async (ctx: Context, { id }) => {
  try {
    const activeDirectory = await ctx.prisma.ftpConnect({ id });
    return activeDirectory;
  } catch (error) {
    throw new Error(error);
  }
};

export const FtpGroupement = async (ctx: Context, { idgroupement }) => {
  try {
    const activeDirectory = await ctx.prisma.ftpConnects({
      where: { groupement: { id: idgroupement } },
    });
    return activeDirectory[0];
  } catch (error) {
    throw new Error(error);
  }
};

export const CreateFtpConnect = async (
  ctx: Context,
  { idGroupement, host, port, user, password },
) => {
  try {
    return ctx.prisma.createFtpConnect({
      host,
      port,
      user,
      password,
      groupement: {
        connect: { id: idGroupement },
      },
    });
  } catch (error) {
    throw new Error(error);
  }
};

export const UpdateFtpConnect = async (ctx: Context, { id, host, port, user, password }) => {
  try {
    return ctx.prisma.updateFtpConnect({
      data: {
        host,
        port,
        user,
        password,
      },
      where: { id },
    });
  } catch (error) {
    throw new Error(error);
  }
};

export const DeleteFtp = async (ctx: Context, { id }) => {
  try {
    const ftp = await ctx.prisma.deleteFtpConnect({ id });
    return ftp;
  } catch (error) {
    throw new Error(error);
  }
};
