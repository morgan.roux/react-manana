import { Context } from '../types';

export const parentFamille = async (ctx: Context, codeFamille: string) => {
  return codeFamille.length === 1
    ? null
    : ctx.prisma
        .familles({
          where: {
            codeFamille: codeFamille.slice(0, -1),
          },
          first: 1,
        })
        .then(familles => (familles && familles.length ? familles[0] : null));
};

export const sousFamilles = async (ctx: Context, codeFamille: string) => {
  return (
    await ctx.prisma.familles({
      where: { codeFamille_starts_with: codeFamille },
    })
  ).filter(familles => familles.codeFamille.length === codeFamille.length + 1);
};
