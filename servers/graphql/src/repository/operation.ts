import { ApolloError } from 'apollo-server';
import moment from 'moment';
import { CODE_OPERATION_COMMERCIAL } from '../constants/item';
import {
  AMBASSADEUR,
  COLLABORATEUR_PHARMACIE,
  COMPTABLE,
  GETIONNAIRE_DE_COMMANDE,
  PRESIDENT_REGION,
  RESPONSABLE_QUALITE_PHARMACIE,
  TITULAIRE_PHARMACIE,
  PREPARATEUR_PHARMACIE,
  STOCK_ET_REASSORT_PHARMACIE,
  APPRENTI_PREPARATEUR_PHARMACIE,
  ETUDIANT_EN_PHARMACIE_PHARMACIE,
  ASSISTANT_DE_DIRECTION_PHARMACIE,
  AUTRE_PROFESSIONNEL_DE_SANTE_PHARMACIE,
} from '../constants/roles';
import { Commande, Operation, OperationPharmacie } from '../generated/prisma-client';
import logger from '../logging';
import s3 from '../services/s3';
import { WatchTime } from '../services/time';
import { Context, OperationArticleInput, OperationInput } from '../types';
import { actualiteAllUsersCible, upsertActualite } from './actualite';
import { deleteAllNotifications, saveAndPublishNotification } from './notification';
import { createArticleCibles } from './operationArticle';
import { createUpdateOperationArticleCible } from './operationArticleCible';
import { createUpdateOperationPharmacie } from './operationPharmacie';
import {
  createPharmacieCibles,
  createUpdateOperationPharmacieDetail,
} from './operationPharmacieDetail';
import { getPharmaciesWithParams } from './pharmacie';
import { getArticlesByCodeReferents } from './produitCanal';
import { codeServices } from './services';
import { usersPharmacie } from './user';

const watch = new WatchTime();

const ACTIONS_CIBLES = [
  'deleteManyOperationArticleCibles',
  'deleteManyOperationArticles',
  'deleteManyOperationPharmacieDetails',
];

export const createUpdateOperation = async (
  {
    idOperation,
    codeItem,
    fichierPresentations,
    typeCommande,
    idCanalCommande,
    fichierPharmacie,
    fichierProduit,
    globalite,
    accordCommercial,
    articles,
    idLaboratoire,
    dateDebut,
    libelle,
    dateFin,
    idsPharmacie,
    description,
    niveauPriorite,
    idProject,
    actionPriorite,
    actionDescription,
    actionDueDate,
    caMoyenPharmacie,
    nbPharmacieCommande,
    nbMoyenLigne,
    idMarche,
    idPromotion,
  }: OperationInput,
  ctx: Context,
): Promise<Operation | ApolloError> => {
  const formatedDateDebut = moment(dateDebut).format('YYYY-MM-DD');
  const formatedDateFin = moment(dateFin).format('YYYY-MM-DD');

  let operation: Operation;
  let cips: string[] = [];
  let codeReferents: string[] = [];
  //TODO : default importance
  const importance = await ctx.prisma
    .importances({ where: { ordre: 1 } })
    .then(importances => importances[0]);

  if (idOperation) {
    logger.info(`[OPERATION] DELETE FICHIER PRESENTATION : ID => ${idOperation}`);
    await _deleteFichier(ctx, idOperation);

    /* Delete project action */
    const project = await ctx.prisma.operation({ id: idOperation }).idProjet();
    if (project && project.id && project.id !== idProject) {
      await ctx.prisma.deleteManyActions({
        project: { id: project.id },
        idItemAssocie: idOperation,
        item: { code: CODE_OPERATION_COMMERCIAL },
      });
    }
  }

  /* Check file */
  if (fichierPharmacie && fichierPharmacie.chemin) {
    const result = await readFile(fichierPharmacie.chemin);
    if (result && result.length) {
      cips = result.map(data => String(data[0]));
    } else {
      return new ApolloError('Fichier vide', 'FICHIER_PHARMACIE_VIDE');
    }

    const cipNotExistInDB =
      cips && cips.length
        ? await checkDataExistInDb(ctx, false, cips, { culumn: 'cip', entity: 'pharmacie' })
        : [];

    if (cipNotExistInDB && cipNotExistInDB.length && cipNotExistInDB.length > 0) {
      logger.info(`${JSON.stringify(cipNotExistInDB)} n'ont pas etes trouver`);
      throw new ApolloError(
        `${JSON.stringify(cipNotExistInDB.map(code => code.value))} n'ont pas etes trouver`,
        'PHARMACIE_NOT_FOUND',
      );
    }
  }

  if (fichierProduit && fichierProduit.chemin) {
    let result;
    try {
      result = await readFile(fichierProduit.chemin);
    } catch (error) {
      return new ApolloError(`Erreur lecture du fichier produit`, 'READ_PRODUCT_FILE_ERROR');
    }

    if (result && result.length) {
      codeReferents = result.map(data => String(data[0]));
    } else {
      return new ApolloError('Fichier vide', 'FICHIER_PHARMACIE_VIDE');
    }

    const codeReferentNotInDB =
      codeReferents && codeReferents.length
        ? await checkDataExistInDb(ctx, false, codeReferents, {
            culumn: 'code',
            entity: 'ProduitCode',
          })
        : [];

    if (codeReferentNotInDB && codeReferentNotInDB.length) {
      throw new ApolloError(
        `${JSON.stringify(codeReferentNotInDB.map(code => code.value))} n'ont pas etes trouver`,
        'PRODUCT_NOT_FOUND',
      );
    }
  }

  try {
    operation = await _createOperation(
      {
        codeItem,
        fichierPresentations: [fichierPresentations[0]],
        typeCommande,
        idCanalCommande,
        accordCommercial,
        dateFin: formatedDateFin,
        dateDebut: formatedDateDebut,
        libelle,
        idLaboratoire,
        description,
        niveauPriorite,
        idProject,
        caMoyenPharmacie,
        nbPharmacieCommande,
        nbMoyenLigne,
        idMarche,
        idPromotion,
      },
      ctx,
      idOperation,
    );
  } catch (error) {
    return new ApolloError(`Erreur creation de l'operation`, 'CREATE_OPERATION_ERROR');
  }

  if (idProject) {
    const action = await ctx.prisma
      .actions({
        where: {
          project: { id: idProject },
          idItemAssocie: operation.id,
          item: { code: CODE_OPERATION_COMMERCIAL },
        },
      })
      .then(actions => (actions && actions.length ? actions[0] : null));

    await ctx.prisma.upsertAction({
      where: { id: action && action.id ? action.id : '' },
      create: {
        project: { connect: { id: idProject } },
        userCreation: { connect: { id: ctx.userId } },
        userModification: { connect: { id: ctx.userId } },
        item: { connect: { code: CODE_OPERATION_COMMERCIAL } },
        idItemAssocie: operation.id,
        importance: { connect: { id: importance.id } },
        description: actionDescription,
        dateDebut: actionDueDate ? actionDueDate : null,
      },
      update: {
        description: actionDescription,
        dateDebut: actionDueDate ? actionDueDate : null,
      },
    });
    await ctx.indexing.publishSave('projects', idProject);
  }

  /* Create pharmacie cible from file */
  const operationPharmacie = await createUpdateOperationPharmacie(
    ctx,
    operation,
    fichierPharmacie,
    globalite,
  );

  if (idOperation) {
    await _deleteAllCibles(ctx, { idOperation });
  }

  let idPharmacieCibles = idsPharmacie && idsPharmacie.length ? [...idsPharmacie] : [];
  if (operation && operationPharmacie && idsPharmacie && idsPharmacie.length) {
    logger.info(`[OPERATION] CREATE CIBLE CURRENT PHARMACIE: ID => ${ctx.pharmacieId}`);
    /* create current pharmacie as cible of OC and reomve from cible after : it make not await the creation of cible */
    if (idsPharmacie.includes(ctx.pharmacieId)) {
      await createUpdateOperationPharmacieDetail(
        operation,
        ctx,
        ctx.pharmacieId,
        operationPharmacie,
      ).then(async _ => publishOperation(ctx, operation.id));
    }
    const pharmaIds = [...idPharmacieCibles.filter(id => id !== ctx.pharmacieId)];
    createPharmacieCibles(ctx, operation, operationPharmacie, pharmaIds);
    logger.info(`[PHARMACIE CIBLE] ==> ${JSON.stringify(pharmaIds)}`);
  }

  if (operation && operationPharmacie && fichierPharmacie && fichierPharmacie.chemin) {
    const pharmacieFromFiles = await getPharmaciesWithParams(ctx, cips, {
      culumn: 'cip',
    }).then(pharmas => (pharmas && pharmas.length ? pharmas.map(pharma => pharma.id) : []));
    idPharmacieCibles = [...pharmacieFromFiles];
    if (pharmacieFromFiles && pharmacieFromFiles.length) {
      createPharmacieCibles(ctx, operation, operationPharmacie, pharmacieFromFiles);
    }
  }

  /*
   *  Actualite operation commerciale
   */
  createUpdateActualiteOperation(ctx, operationPharmacie, {
    id: idOperation ? idOperation : operation && operation.id ? operation.id : '',
    args: {
      idOperation,
      fichierPresentations,
      typeCommande,
      idCanalCommande,
      fichierPharmacie,
      fichierProduit,
      globalite,
      accordCommercial,
      articles,
      idLaboratoire,
      dateDebut,
      libelle,
      dateFin,
      idsPharmacie: idPharmacieCibles,
      description,
      niveauPriorite,
      codeItem,
      idProject,
      actionPriorite,
      actionDescription,
      actionDueDate,
    },
  });

  let paramArticles: OperationArticleInput[] = articles && articles.length ? [...articles] : [];
  if (operation && fichierProduit && fichierProduit.chemin) {
    const articlesCible =
      codeReferents && codeReferents.length
        ? await getArticlesByCodeReferents(ctx, codeReferents)
        : [];
    createUpdateOperationArticleCible(ctx, operation, fichierProduit);

    paramArticles =
      articlesCible && articlesCible.length
        ? articlesCible
            .map(article => {
              if (article && article.id) {
                return {
                  idCanalArticle: article.id,
                  quantite: 0,
                };
              }
            })
            .filter(article => article)
        : [];
  }

  const files = [
    ...fichierPresentations.filter(
      file => file.nomOriginal !== fichierPresentations[0].nomOriginal,
    ),
  ];
  /* Create other files */
  if (operation && operation.id && files && files.length) {
    logger.info(`[CREATE OTHERS FICHE PRESENTATION] ===> ${JSON.stringify(files)}`);
    await ctx.prisma.updateOperation({
      where: { id: operation.id },
      data: {
        fichierPresentations: {
          create: files.map(fichier => {
            return {
              chemin: fichier.chemin,
              nomOriginal: fichier.nomOriginal,
              type: fichier.type,
            };
          }),
        },
      },
    });
  }

  if (operation && idPromotion) {
    const promotionArticle = await ctx.prisma.promotionArticles({
      where: { promotion: { id: idPromotion } },
    });
    paramArticles =
      promotionArticle && promotionArticle.length
        ? (
            await Promise.all(
              promotionArticle.map(async promotionArticle => {
                const article = await ctx.prisma
                  .promotionArticle({ id: promotionArticle.id })
                  .produitCanal();
                if (article && article.id) {
                  return {
                    idCanalArticle: article.id,
                    quantite: 0,
                  };
                }
              }),
            )
          ).filter(article => article)
        : [];
  }

  if (operation && idMarche) {
    const marcheArticles = await ctx.prisma.marcheArticles({ where: { marche: { id: idMarche } } });
    paramArticles =
      marcheArticles && marcheArticles.length
        ? (
            await Promise.all(
              marcheArticles.map(async marcheArticle => {
                const article = await ctx.prisma
                  .marcheArticle({ id: marcheArticle.id })
                  .produitCanal();
                if (article && article.id) {
                  return {
                    idCanalArticle: article.id,
                    quantite: 0,
                  };
                }
              }),
            )
          ).filter(article => article)
        : [];
  }

  if (operation && paramArticles && paramArticles.length) {
    await createArticleCibles(ctx, operation, paramArticles);
  }

  return operation ? ctx.prisma.operation({ id: operation.id }) : null;
};

export const publishOperation = async (ctx: Context, id: string) => {
  const commandeCanalId = await ctx.prisma.operation({ id }).idCommandeCanal();
  return Promise.all([
    ctx.indexing.publishSave('operations', id),
    ctx.indexing.publishSave('commandecanals', commandeCanalId.id),
  ]);
};

export const checkDataExistInDb = async (
  ctx: Context,
  exist: boolean,
  toChecks: any[],
  { culumn, entity },
  parsInt: boolean = false,
) => {
  const is = await Promise.all(
    toChecks.map(async data => {
      return {
        value: parsInt ? parseInt(data, 10) : data,
        exist: await ctx.prisma.$exists[entity]({ [culumn]: data }),
      };
    }),
  );

  return exist ? is.filter(data => data.exist) : is.filter(data => !data.exist);
};

// creation d'une operation //

const _createOperation = async (
  {
    codeItem,
    idLaboratoire,
    libelle,
    niveauPriorite,
    description,
    dateDebut,
    dateFin,
    fichierPresentations,
    accordCommercial,
    idCanalCommande,
    typeCommande,
    idProject,
    caMoyenPharmacie,
    nbPharmacieCommande,
    nbMoyenLigne,
    idMarche,
    idPromotion,
  },
  ctx: Context,
  idOperation: string,
): Promise<Operation> => {
  logger.info(`[CREATE FICHE PRESENTATION] ===> ${JSON.stringify(fichierPresentations)}`);

  return ctx.prisma
    .upsertOperation({
      where: { id: idOperation || '' },
      create: {
        idItem: { connect: { code: codeItem } },
        libelle,
        niveauPriorite,
        description,
        dateDebut,
        dateFin,
        idLaboratoire: idLaboratoire
          ? {
              connect: {
                id: idLaboratoire,
              },
            }
          : null,
        fichierPresentations: {
          create: fichierPresentations.map(fichier => {
            return {
              chemin: fichier.chemin,
              nomOriginal: fichier.nomOriginal,
              type: fichier.type,
            };
          }),
        },
        idCommandeCanal: { connect: { code: idCanalCommande } },
        idCommandeType: typeCommande ? { connect: { code: typeCommande } } : null,
        accordCommercial,
        groupement: { connect: { id: ctx.groupementId } },
        idProjet: idProject ? { connect: { id: idProject } } : null,
        caMoyenPharmacie,
        nbPharmacieCommande,
        nbMoyenLigne,
        marche: idMarche
          ? {
              connect: {
                id: idMarche,
              },
            }
          : null,
        promotion: idPromotion
          ? {
              connect: {
                id: idPromotion,
              },
            }
          : null,
      },
      update: {
        libelle,
        idItem: { connect: { code: codeItem } },
        description,
        niveauPriorite,
        dateDebut,
        dateFin,
        idLaboratoire: idLaboratoire
          ? {
              connect: {
                id: idLaboratoire,
              },
            }
          : null,
        fichierPresentations: {
          create: fichierPresentations.map(fichier => {
            return {
              chemin: fichier.chemin,
              nomOriginal: fichier.nomOriginal,
              type: fichier.type,
            };
          }),
        },
        idCommandeCanal: { connect: { code: idCanalCommande } },
        idCommandeType: typeCommande ? { connect: { code: typeCommande } } : null,
        accordCommercial,
        groupement: { connect: { id: ctx.groupementId } },
        idProjet: idProject ? { connect: { id: idProject } } : null,
        caMoyenPharmacie,
        nbPharmacieCommande,
        nbMoyenLigne,
        marche: idMarche
          ? {
              connect: {
                id: idMarche,
              },
            }
          : null,
        promotion: idPromotion
          ? {
              connect: {
                id: idPromotion,
              },
            }
          : null,
      },
    })
    .then(operation => {
      logger.info(`[OPERATION] CREATE UPDATE OK : operation => ${JSON.stringify(operation)}`);
      return operation;
    });
};

export const readFile = async (chemin: string) => {
  return s3
    .readXlsxFromAWS(chemin)
    .then(value => {
      return value && value.length && value[0].data;
    })
    .catch(reason => {
      logger.error(`Operation Commerciale: file error: ${reason}`);
    });
};

const _deleteAllCibles = async (ctx: Context, { idOperation }) => {
  return Promise.all(
    ACTIONS_CIBLES.map(async actionDelete =>
      deleteOperationCibles(ctx, {
        idOperation,
        actionDelete,
      }),
    ),
  ).then(_ => {
    logger.info(`[OPERATION] DELETE ALL CIBLES OPERATION : ID => ${idOperation}`);
  });
};

const deleteOperationCibles = async (ctx: Context, { actionDelete, idOperation }) => {
  return ctx.prisma[actionDelete]({ idOperation: { id: idOperation } }).then(_ =>
    ctx.indexing.publishSave('operations', idOperation),
  );
};

export const createUpdateActualiteOperation = async (
  ctx: Context,
  operationPharmacie: OperationPharmacie,
  { id, args },
) => {
  let pharmacies: any[] = [];

  //TODO: default urgence & importance
  const importance = await ctx.prisma
    .importances({ where: { ordre: 1 } })
    .then(importances => importances[0]);
  const urgence = await ctx.prisma.urgences({ where: { code: 'A' } }).then(urgences => urgences[0]);

  if (operationPharmacie && operationPharmacie.globalite === true) {
    pharmacies = await ctx.prisma
      .pharmacies({ where: { groupement: { id: ctx.groupementId } } })
      .then(pharmas => (pharmas && pharmas.length ? pharmas.map(pharma => pharma.id) : []));
    logger.info(`${ctx.groupementId} : pharmacies ====> ${pharmacies.length}`);
  } else {
    pharmacies = args && args.idsPharmacie && args.idsPharmacie.length ? args.idsPharmacie : [];
    logger.info(`pharmacies ====> ${pharmacies.length}`);
  }

  logger.info(`total pharmacies ====> ${pharmacies.length}`);

  const actualiteOperation = await ctx.prisma.operation({ id }).idActualite();

  if (actualiteOperation && actualiteOperation.id) {
    logger.info('*** UPDATE OPERATION ***');

    await deleteAllNotifications(ctx, id);

    await ctx.prisma.deleteManyOperationViewers({
      idOperation: { id },
    });

    await ctx.prisma.deleteManyActualitePharmacieSeens({
      idActualite: { id: actualiteOperation.id },
    });

    const usersSeenOP = await ctx.prisma.operation({ id }).user();

    if (usersSeenOP && usersSeenOP.length) {
      await ctx.prisma.updateOperation({
        where: { id },
        data: {
          user: {
            disconnect: usersSeenOP.map(user => {
              return { id: user.id };
            }),
          },
        },
      });
    }

    const usersSeenActu = await ctx.prisma.actualite({ id: actualiteOperation.id }).user();

    if (usersSeenActu && usersSeenActu.length) {
      await ctx.prisma.updateActualite({
        where: { id: actualiteOperation.id },
        data: {
          user: {
            disconnect: usersSeenActu.map(user => {
              return { id: user.id };
            }),
          },
        },
      });
    }
  }

  const actualite = await upsertActualite(ctx, {
    id: actualiteOperation && actualiteOperation.id ? actualiteOperation.id : '',
    codeItem: args.codeItem,
    codeOrigine: 'GROUPEMENT',
    description: args.description,
    actionAuto: null,
    niveauPriorite: args.niveauPriorite,
    idLaboratoire: args.idLaboratoire,
    libelle: args.libelle,
    dateDebut: args.dateDebut,
    dateFin: args.dateFin,
    fichierPresentations: args.fichierPresentations,
    globalite: false,
    fichierCible: null,
    presidentRegions: null,
    services: codeServices,
    laboratoires: null,
    partenaires: null,
    pharmacieRoles: {
      pharmacies,
      roles: [
        PREPARATEUR_PHARMACIE,
        STOCK_ET_REASSORT_PHARMACIE,
        APPRENTI_PREPARATEUR_PHARMACIE,
        ETUDIANT_EN_PHARMACIE_PHARMACIE,
        ASSISTANT_DE_DIRECTION_PHARMACIE,
        AUTRE_PROFESSIONNEL_DE_SANTE_PHARMACIE,
        TITULAIRE_PHARMACIE,
        COLLABORATEUR_PHARMACIE,
        GETIONNAIRE_DE_COMMANDE,
        COMPTABLE,
        RESPONSABLE_QUALITE_PHARMACIE,
        PRESIDENT_REGION,
        AMBASSADEUR,
      ],
    },
    idUrgence: urgence.id,
    idImportance: importance.id,
    idProject: args && args.idProject ? args.idProject : null,
    actionPriorite: args && args.actionPriorite ? args.actionPriorite : null,
    actionDescription: args && args.actionDescription ? args.actionDescription : null,
    actionDueDate: args && args.actionDueDate ? args.actionDueDate : null,
  });

  if (actualite && actualite.id) {
    await ctx.prisma.updateOperation({
      where: { id },
      data: { idActualite: { connect: { id: actualite.id } } },
    });
  }
  logger.info(
    `[OPERATION ACTUALITE] CREATE UPDATE OPERATION ACTUALITE : actualite => ${JSON.stringify(
      actualite,
    )}`,
  );

  await ctx.indexing.publishSave('operations', id);
  if (actualite && actualite.id) await ctx.indexing.publishSave('actualites', id);
  return actualite;
};

export const sendNotificationOperation = async (ctx: Context, { id, pharmacieRoles }) => {
  watch.start();
  const operation = await ctx.prisma.operation({ id });
  const usersCible = await actualiteAllUsersCible(ctx, {
    idActualite: null,
    presidentRegions: null,
    codeServices,
    fichierCible: null,
    idLaboratoires: null,
    idPartenaires: null,
    pharmacieRoles,
  });
  if (usersCible && usersCible.length && operation && operation.id && operation.libelle) {
    return saveAndPublishNotification(
      {
        type: 'OPERATION_ADDED',
        message: getNewOperationMessage(operation.libelle),
        someUsers: usersCible.map(user => (user && user.id ? user.id : null)).filter(id => id),
        from: ctx.userId,
        targetId: operation.id,
        targetName: operation.libelle,
      },
      ctx,
    ).then(async notifiactions => {
      logger.info(`IF Operation notification ${JSON.stringify(notifiactions)}`);

      watch.stop();
      watch.printElapsed(
        `[Operation Repository] Function sendNotificationActualiteToCible duration : `,
      );

      return notifiactions && notifiactions.length ? true : false;
    });
  }

  return saveAndPublishNotification(
    {
      type: 'OPERATION_ADDED',
      message: getNewOperationMessage(operation.libelle),
      to: ctx.userId,
      from: ctx.userId,
      targetId: operation.id,
      targetName: operation.libelle,
    },
    ctx,
  ).then(async notifiactions => {
    logger.info(`ELSE Operation notification ${JSON.stringify(notifiactions)}`);

    watch.stop();
    watch.printElapsed(
      `[Operation Repository] Function sendNotificationActualiteToCible duration : `,
    );

    return notifiactions && notifiactions.length ? true : false;
  });
};

const _deleteFichier = async (ctx: Context, id: string) => {
  const filesToDelete = id ? await ctx.prisma.operation({ id }).fichierPresentations() : [];
  if (filesToDelete && filesToDelete.length) {
    return ctx.prisma.updateOperation({
      where: { id },
      data: { fichierPresentations: { deleteMany: { id_in: filesToDelete.map(file => file.id) } } },
    });
  }
  return null;
};

export const markAsSeenOperation = async (ctx: Context, { id }) => {
  watch.start();
  const usersSeen = await ctx.prisma.operation({ id }).user({ where: { id: ctx.userId } });

  logger.info(` BEGIN Mark seen OP :: id : ${id}`);

  if (!usersSeen || (usersSeen && !usersSeen.length)) {
    logger.info(` Mark seen OP :: ${id}`);
    await ctx.prisma.updateOperation({
      where: { id },
      data: { user: { connect: { id: ctx.userId } } },
    });

    /* logger.info(` Notified from OP `);
    updateNotificationSeenByUser(ctx, null, id, true); */
  }

  return ctx.prisma.operation({ id }).then(async operation => {
    await ctx.indexing.publishSave('operations', id);
    watch.stop();
    watch.printElapsed(`[Operation Repository] Function markAsSeenOperation duration : `);
    return operation;
  });
};

export const markAsNotSeenOperation = async (ctx: Context, { id, idPharmacie }) => {
  watch.start();
  logger.info(` BEGIN Mark as not seen OP :: id : ${id}`);
  const users = idPharmacie ? await usersPharmacie(ctx, { idPharmacie }) : [];
  if (users && users.length) {
    logger.info(
      `users pharmacie ${idPharmacie} ===========> ${JSON.stringify(users.map(user => user.id))}`,
    );
  }
  const usersSeen =
    users && users.length
      ? await ctx.prisma.operation({ id }).user({ where: { id_in: users.map(user => user.id) } })
      : await ctx.prisma.operation({ id }).user({ where: { id: ctx.userId } });
  if (usersSeen && usersSeen.length) {
    logger.info(`user seen ===========> ${JSON.stringify(usersSeen.map(user => user.id))}`);
  }

  if (usersSeen && usersSeen.length) {
    await Promise.all(
      usersSeen.map(user =>
        ctx.prisma.updateOperation({
          where: { id },
          data: { user: { disconnect: { id: user.id } } },
        }),
      ),
    );

    /* logger.info(` Notified from OP `);
    updateNotificationSeenByUser(ctx, usersSeen, id, false); */
  }

  if (idPharmacie) {
    logger.info(` [OPERATION VIEWER] DELETE pahrmacie :: ${idPharmacie} , actualite :: ${id}`);
    await ctx.prisma.deleteManyOperationViewers({
      idOperation: { id },
      idPharmacie: { id: idPharmacie },
    });
  }

  return ctx.prisma.operation({ id }).then(async operation => {
    await ctx.indexing.publishSave('operations', id);
    watch.stop();
    watch.printElapsed(`[Operation Repository] Function markAsNotSeenOperation duration : `);
    return operation;
  });
};

export const getNewOperationMessage = (libelle: string) => {
  return `Une nouvelle operation <b>${libelle}</b> vient d'être ajoutée.`;
};

export const deleteSoftOperation = async (ctx: Context, { id }) => {
  return ctx.prisma
    .updateOperation({
      data: { isRemoved: true },
      where: { id },
    })
    .then(async operation => {
      await ctx.indexing.publishSave('operations', operation.id);

      return operation;
    });
};

/* ****************************************************************************************************** */
export const pharmaciesCommandeeOperation = async (id: string, ctx: Context) => {
  // liste des commandes de l'operation commerciale
  const commandes = await ctx.prisma.commandes({
    where: { idOperation: { id } },
  });

  // liste des pharmacies qui ont commandees
  const pharmacies = (
    await Promise.all(
      commandes.map(async commande => {
        const pharmacie = await ctx.prisma.commande({ id: commande.id }).idPharmacie();
        return pharmacie || null;
      }),
    )
  ).filter(pharma => pharma);

  const uniquesPharmacies = [...new Set(pharmacies.map(pharmacie => pharmacie.id))];

  return ctx.prisma.pharmacies({ where: { id_in: uniquesPharmacies } });
};

export const commandesOperation = async (ctx: Context, { id }) => {
  const commandes = await ctx.prisma.commandes({
    where: { idOperation: { id }, idPharmacie: { id_not: null } },
  });

  return commandes;
};

/**
 *  fonction qui calcule le total des commandes dans une operation commerciale
 */
export const caTotalOperationCommande = async (ctx: Context, { id }) => {
  // liste des commandes de l'operation commerciale
  const commandes = await commandesOperation(ctx, { id });

  // liste des totales des commandes de chaque pharmacie
  const totalNet = await Promise.all(
    commandes.map(commande => {
      return ctx.prisma.commande({ id: commande.id }).prixNetTotalHT();
    }),
  );

  // retourne un objet qui contient le nombre des commandes des pharmacies
  // et le total globale des commandes des pharmacies
  return {
    totalNet: totalNet && totalNet.length ? totalNet.reduce((a, b) => a + b, 0) : 0,
    nbCommande: totalNet.length,
  };
};

export const remiseOperationCommande = async (ctx: Context, { id }) => {
  const commandes = await commandesOperation(ctx, { id });
  if (!commandes || (commandes && !commandes.length)) {
    return {
      totalRemise: 0,
      totalRemiseGlobal: 0,
    };
  }
  return totalRemiseCommande(ctx, commandes);
};

export const totalOperationProduitCommandees = async (
  ctx: Context,
  id: string,
  idPharmacie: string = null,
): Promise<number> => {
  let where: any = { idOperation: { id } };
  if (idPharmacie) {
    where = { idOperation: { id }, idPharmacie: { id: idPharmacie } };
  }
  const commandes = await ctx.prisma.commandes({ where });
  let tempQuant = 0;

  const commandeLignes = await Promise.all(
    commandes.map(async commande => {
      return ctx.prisma.commandeLignes({
        where: {
          AND: [{ idCommande: { id: commande.id } }],
        },
      });
    }),
  );

  const flatedCommandeLignes = commandeLignes.flat();

  flatedCommandeLignes.map(commandeLigne => {
    tempQuant += commandeLigne.quantiteCdee;
  });

  return tempQuant;
};

export const caMoyenOperations = async (ctx: Context, { id }): Promise<number> => {
  // Calacule la moyenne des commandes operation
  const totalCommande = (await caTotalOperationCommande(ctx, { id })).totalNet;
  const length = (await caTotalOperationCommande(ctx, { id })).nbCommande;

  if (length === 0) return 0;

  const result = totalCommande / length;
  return isNaN(result) ? 0 : parseFloat(result.toFixed(2));
};

/* ****************************************************************************************************** */

export const idsOperationDateNOTActive = async (ctx: Context) => {
  const fragment = `
  fragment OperationId on Operation {
    id
  }`;
  const now = moment()
    .add(1, 'days')
    .toISOString();
  return ctx.prisma
    .operations({ where: { dateDebut_gt: now } })
    .$fragment(fragment)
    .then((ocs: any[]) => (ocs && ocs.length ? ocs.map(oc => oc.id) : []));
};

export const caTotalOperationCommandeByPharmacie = async (
  ctx: Context,
  { idOperation, idPharmacie },
) => {
  // liste des commandes de l'operation commercial de la pharmacie
  const commandes = await ctx.prisma.commandes({
    where: {
      idOperation: { id: idOperation },
      idPharmacie: { id: idPharmacie },
    },
  });

  if (!commandes || (commandes && !commandes.length)) {
    return {
      totalNet: 0,
      nbCommande: 0,
    };
  }

  // liste des totale du commande de la pharmacie sur l'operation
  const total = await Promise.all(
    commandes.map(async commande => {
      return ctx.prisma.commande({ id: commande.id }).prixNetTotalHT();
    }),
  );

  return {
    totalNet: total && total.length ? total.reduce((a, b) => a + b, 0) : 0,
    nbCommande: total.length,
  };
};

export const operationPharmacieCommandeLigne = async (
  ctx: Context,
  { idOperation, idPharmacie },
) => {
  const commandes = await ctx.prisma.commandes({
    where: {
      idOperation: { id: idOperation },
      idPharmacie: { id: idPharmacie },
    },
  });

  const commandeLignes = await Promise.all(
    commandes.map(async commande => {
      return ctx.prisma.commandeLignes({
        where: {
          AND: [{ idCommande: { id: commande.id } }],
        },
      });
    }),
  );

  return commandeLignes.flat();
};

export const quantiteProduitOperationCommandeLigne = async (
  ctx: Context,
  { idOperation, idPharmacie },
) => {
  const commandesLignes = await operationPharmacieCommandeLigne(ctx, { idOperation, idPharmacie });

  let tempQuant = 0;
  commandesLignes.map(commandeLigne => {
    tempQuant += commandeLigne.quantiteCdee;
  });

  return tempQuant;
};

export const remiseOperationCommandeByPharmacie = async (
  ctx: Context,
  { idOperation, idPharmacie },
) => {
  const commandes = await ctx.prisma.commandes({
    where: {
      idOperation: { id: idOperation },
      idPharmacie: { id: idPharmacie },
    },
  });

  if (!commandes || (commandes && !commandes.length)) {
    return {
      totalRemise: 0,
      totalRemiseGlobal: 0,
    };
  }

  return totalRemiseCommande(ctx, commandes);
};

export const totalRemiseCommande = async (ctx: Context, commandes: Commande[]) => {
  const totalRemise = await Promise.all(
    commandes.map(async commande => {
      return ctx.prisma.commande({ id: commande.id }).valeurRemiseTotal();
    }),
  );

  const totalRemiseGlobal = await Promise.all(
    commandes.map(async commande => {
      return ctx.prisma.commande({ id: commande.id }).remiseGlobale();
    }),
  );

  return {
    totalRemise: totalRemise && totalRemise.length ? totalRemise.reduce((a, b) => a + b, 0) : 0,
    totalRemiseGlobal:
      totalRemiseGlobal && totalRemiseGlobal.length
        ? totalRemiseGlobal.reduce((a, b) => a + b, 0)
        : 0,
  };
};

export const operationIds = async (
  ctx: Context,
  ids: string[],
  { first, skip },
): Promise<string[]> => {
  const where =
    ids && ids.length
      ? { id_in: ids, groupement: { id: ctx.groupementId } }
      : { groupement: { id: ctx.groupementId } };

  const filter =
    (!first && first !== 0) || (!skip && skip !== 0) ? { where } : { where, skip, first };

  const fragment = `
  fragment OperationId on Operation {
    id
  }`;

  return ctx.prisma
    .operations(filter)
    .$fragment(fragment)
    .then((ocs: any[]) => (ocs && ocs.length ? ocs.map(oc => String(oc.id)) : []));
};

export const articleOperations = async ({ idCanalArticle }, ctx: Context) => {
  const operationArtcicles = await ctx.prisma.operationArticles({
    where: {
      produitCanal: { id: idCanalArticle },
    },
  });
  if (operationArtcicles && operationArtcicles.length) {
    const operations = await Promise.all(
      operationArtcicles.map(operationArticle =>
        ctx.prisma.operationArticle({ id: operationArticle.id }).idOperation(),
      ),
    );
    return operations;
  } else {
    return null;
  }
};
