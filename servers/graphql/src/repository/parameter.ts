import { Context } from '../types';
import { ParameterInput, ParameterValueType } from '../types';
import logger from '../logging';
import axios from 'axios';
import { getUserPharmacie } from './user';

export interface ParameterSso {
  groupid: string;
  code: string;
  value: string;
}

const getCurrentPharmacieId = async (ctx: Context) => {
  if (ctx.pharmacieId) {
    return ctx.pharmacieId
  }
  const pharmacie = await getUserPharmacie(ctx.userId, ctx)
  return pharmacie?.id
}


export const updateParameterValues = async (ctx: Context, parameters: [ParameterInput]) => {
  if (!parameters || (parameters && !parameters.length)) return null;

  logger.info(
    '[PARAMS]',
    parameters.map((param) => `ID: ${param.id} - VALUE ${param.value}`),
  );
  const parametersSsoes: ParameterSso[] = [];
  const idPharmacie = await getCurrentPharmacieId(ctx)
  const updateParams = await Promise.all(
    parameters.map(async (parameter) => {
      const savedParameterList = await ctx.prisma.parameters({ where: { id: parameter.id } });
      const savedParameter =
        savedParameterList && savedParameterList.length > 0
          ? savedParameterList[savedParameterList.length - 1]
          : null;

      const idSource =
        savedParameter && savedParameter.category === 'PHARMACIE'
          ? idPharmacie
          : ctx.groupementId;

      const paramValue = await ctx.prisma
        .parameterValues({
          where: { idParameter: { id: parameter.id }, idSource },
        })
        .then((values) => (values && values.length ? values[values.length - 1] : null));

      return ctx.prisma
        .upsertParameterValue({
          where: { id: paramValue && paramValue.id ? paramValue.id : '' },
          create: {
            idParameter: {
              connect: { id: parameter.id },
            },
            value: parameter.value,
            idSource,
          },
          update: {
            value: parameter.value,
          },
        })
        .then(async (res) => {
          try {
            const parameterInfo = await ctx.prisma.parameter({ id: parameter.id });
            if (
              parameterInfo &&
              (parameterInfo.code === '0049' ||
                parameterInfo.code === '0053' ||
                parameterInfo.code === '0046' ||
                parameterInfo.code === '0052')
            ) {
              if (parameter.value && parameter.value.trim() !== '') {
                parametersSsoes.push({
                  groupid: ctx.groupementId,
                  code: parameterInfo.code.trim(),
                  value: parameter.value,
                });
              }
            }
          } catch (error) {
            logger.error(`error getting sso parameter `, error);
          }

          return res;
        })
        .catch((err) => {
          throw new Error(err);
        });
    }),
  );

  if (updateParams && updateParams.length) {
    logger.info(
      '[PARAMETER VALUE]',
      await Promise.all(
        updateParams.map(
          async (param) =>
            `Nom: ${await ctx.prisma
              .parameterValue({ id: param.id })
              .idParameter()
              .name()} - VALUE ${param.value}`,
        ),
      ),
    );
  }
  // update sso job
  try {
    console.log('Parameters sso to update ', parametersSsoes);
    const activateHelloId = parametersSsoes.find((el) => el.code === '0046');
    const scheduleHelloId = parametersSsoes.find((el) => el.code === '0052');

    console.log('params ');

    if ((activateHelloId && activateHelloId.value) === 'true' && scheduleHelloId) {
      _postUpdateJob(scheduleHelloId);
    } else if ((activateHelloId && activateHelloId.value) === 'false' && scheduleHelloId) {
      _postUpdateJob(activateHelloId);
    } else if (activateHelloId && !scheduleHelloId) {
      _postUpdateJob(activateHelloId);
    } else if (!activateHelloId && scheduleHelloId) {
      _postUpdateJob(scheduleHelloId);
    }

    const activateAD = parametersSsoes.find((el) => el.code === '0049');
    const scheduleAD = parametersSsoes.find((el) => el.code === '0053');
    if ((activateAD && activateAD.value) === 'true' && scheduleAD) {
      _postUpdateJob(scheduleAD);
    } else if ((activateAD && activateAD.value) === 'false' && scheduleAD) {
      _postUpdateJob(activateAD);
    } else if (activateAD && !scheduleAD) {
      _postUpdateJob(activateAD);
    } else if (!activateAD && scheduleAD) {
      _postUpdateJob(scheduleAD);
    }
  } catch (error) {
    logger.error(`error changing sso parameter `, error);
  }
  return updateParams;
};

export const ParameterGroupementByCode = async (ctx: Context, { idGroupement, code }) => {
  const parameter = await ctx.prisma.parameter({ code });
  const parameterValue = await ctx.prisma
    .parameterValues({
      where: { idParameter: { code }, idSource: idGroupement },
    })
    .then((values) => (values && values.length ? values[0] : null));

  const parameterType = await ctx.prisma.parameter({ code }).idParameterType();

  const value =
    parameterValue && parameterValue.value ? parameterValue.value : parameter.defaultValue;

  const result: ParameterValueType = {
    value,
    type: parameterType.code,
  };

  return result;
};

const _postUpdateJob = (paramInfo: ParameterSso) => {
  axios
    .post(`${process.env.SAML_SSO_ENDPOINT}/update-job`, {
      groupid: paramInfo.groupid,
      code: paramInfo.code,
      value: paramInfo.value,
    })
    .then(() => console.log('Job updated'))
    .catch((err) => {
      console.log('Error when updating job', { error: err });
    });
};
