import { Context } from '../types';
import logger from '../logging';
import { ActiviteUser, ActiviteType } from '../generated/prisma-client';
import { ApolloError } from 'apollo-server';

interface ActiviteInput {
  code: string;
  nom?: string;
  log?: any;
}

export const createActiviteUser = async (activite: ActiviteInput, ctx: Context): Promise<void> => {
  if (ctx.userId && parseInt(ctx.userId, 10) !== -1) {
    const { code, nom, log } = activite;
    let activiteType: ActiviteType;

    activiteType = await ctx.prisma.activiteType({ code });

    if (!activiteType) {
      activiteType = await ctx.prisma.createActiviteType({
        code,
        nom,
      });
    }

    await _createUserActivite(activiteType, ctx, log).then(async created => {
      logger.info(`Activite Utilisateur: ${activiteType.nom}`);
      await ctx.indexing.publishSave('activiteusers', created.id);
      return created;
    });
  }
};

const _createUserActivite = async (
  activiteType: ActiviteType,
  ctx: Context,
  log: any,
): Promise<ActiviteUser> => {
  return ctx.prisma.createActiviteUser({
    idActiviteType: { connect: { code: activiteType.code } },
    idUser: { connect: { id: ctx.userId } },
    log,
  });
};
