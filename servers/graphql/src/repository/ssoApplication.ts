import { Context } from '../types';
import logger from '../logging';
import { MutationResolvers } from '../generated/graphqlgen';
import { ApplicationsRole } from '../generated/prisma-client';
import { ApolloError } from 'apollo-server';

export const ApplicationsGroup = async (ctx: Context, { idgroupement }) => {
  try {
    const applications = await ctx.prisma.applicationsGroups({
      where: { groupement: { id: idgroupement } },
    });
    logger.info(applications);
    return applications;
  } catch (error) {
    throw new Error(error);
  }
};

export const ApplicationsRoleFunction = async (ctx: Context, { idgroupement, coderole }) => {
  try {
    const role = await ctx.prisma.roles({ where: { code: coderole } });
    logger.info(role);
    const applications = await ctx.prisma.applicationsRoles({
      where: { groupement: { id: idgroupement }, AND: { idrole: { id: role[0].id } } },
    });
    return applications;
  } catch (error) {
    throw new Error(error);
  }
};

export const ApplicationsRoles = async (ctx: Context, { idgroupement }) => {
  try {
    const applications = await ctx.prisma.applicationsRoles({
      where: { groupement: { id: idgroupement } },
    });
    return applications;
  } catch (error) {
    throw new Error(error);
  }
};

export const SsoApplication = async (ctx: Context, { id }) => {
  try {
    const application = await ctx.prisma.ssoApplication({ id });
    logger.info(application);
    return application;
  } catch (error) {
    throw new Error(error);
  }
};

export const SamlLogin = async (ctx: Context, { id }) => {
  try {
    const saml = await ctx.prisma.samlLogin({ id });
    return saml;
  } catch (error) {
    throw new Error(error);
  }
};

export const CryptoMd5 = async (ctx: Context, { id }) => {
  try {
    const md5 = await ctx.prisma.cryptoMd5({ id });
    return md5;
  } catch (error) {
    throw new Error(error);
  }
};

export const CryptoMd5s = async (ctx: Context, { idGroupement }) => {
  try {
    const applications = await ctx.prisma
      .applicationsGroups({
        where: { groupement: { id: idGroupement }, idapplications: { ssoType: 'CRYPTAGE_MD5' } },
      })
      .then(appGroup => {
        return appGroup.map(app => {
          return ctx.prisma.applicationsGroup({ id: app.id }).idapplications();
        });
      });

    const idPromise = await applications.map(async application => (await application).id);

    const getID = await Promise.all(idPromise);

    const ssoApplications = await ctx.prisma.ssoApplications({ where: { id_in: getID } });

    const md5Id = ssoApplications.map(application => application.ssoTypeid);

    const md5s = await ctx.prisma.cryptoMd5s({
      where: { id_in: md5Id, haveExternalUserMapping: true },
    });

    return md5s;
  } catch (error) {
    throw new Error(error);
  }
};

export const CryptoAes256 = async (ctx: Context, { id }) => {
  try {
    const aes256 = await ctx.prisma.cryptoAes256({ id });
    return aes256;
  } catch (error) {
    throw new Error(error);
  }
};

export const CryptoOther = async (ctx: Context, { id }) => {
  try {
    const other = await ctx.prisma.cryptoOther({ id });
    return other;
  } catch (error) {
    throw new Error(error);
  }
};

export const TokenAuthentification = async (ctx: Context, { id }) => {
  try {
    const tokenAuthentification = await ctx.prisma.tokenAuthentification({ id });
    return tokenAuthentification;
  } catch (error) {
    throw new Error(error);
  }
};

export const TokenFtpAuthentification = async (ctx: Context, { id }) => {
  try {
    const tokenFtpAuthentification = await ctx.prisma.tokenFtpAuthentification({ id });
    return tokenFtpAuthentification;
  } catch (error) {
    throw new Error(error);
  }
};

export const CreateSsoApplication = async (
  ctx: Context,
  {
    nom,
    url,
    icon,
    ssoType,
    mappings,
    idGroupement,
    sendingType,
    aes,
    md5,
    other,
    saml,
    token,
    toknFtp,
  }: MutationResolvers.ArgsCreateSsoApplication,
) => {
  try {
    let appTypeId = '';
    switch (ssoType) {
      case 'SAML':
        const samlCreated = await ctx.prisma.createSamlLogin({
          Sendingtype: sendingType,
          consumerUrl: saml.consumerUrl,
          idpX509Certificate: saml.idpX509Certificate,
          spX509Certificate: saml.spX509Certificate,
        });
        appTypeId = samlCreated.id;
        break;
      case 'TOKEN_AUTHENTIFICATION':
        const tokenApp = await ctx.prisma.createTokenAuthentification({
          Sendingtype: sendingType,
          reqTokenUrl: token.reqTokenUrl,
          reqUserUrl: token.reqUserUrl,
        });
        appTypeId = tokenApp.id;
        break;
      case 'TOKEN_FTP_AUTHENTIFICATION':
        const ftp = await ctx.prisma.ftpConnects({ where: { groupement: { id: idGroupement } } });
        console.log('ftp ', ftp);
        if (!ftp || ftp.length === 0) {
          throw new Error(
            "Vous devez configurer un FTP pour pouvoir utiliser ce type d'application",
          );
        }
        const tokenFtpApp = await ctx.prisma.createTokenFtpAuthentification({
          Sendingtype: sendingType,
          reqUserUrl: toknFtp.requestUrl,
          ftpFileName: toknFtp.ftpFileName,
          ftpFilePath: toknFtp.ftpFilePath,
          ftpid: {
            connect: { id: ftp[0].id },
          },
        });
        appTypeId = tokenFtpApp.id;
        break;
      case 'CRYPTAGE_AES256':
        const aesApp = await ctx.prisma.createCryptoAes256({
          Sendingtype: sendingType,
          hexadecimal: aes.hexadecimal,
          key: aes.key,
          requestUrl: aes.requestUrl,
        });
        appTypeId = aesApp.id;
        break;
      case 'CRYPTAGE_MD5':
        const md5App = await ctx.prisma.createCryptoMd5({
          Sendingtype: sendingType,
          hexadecimal: md5.hexadecimal,
          requestUrl: md5.requestUrl,
          beginGet: md5.beginGet,
          endGet: md5.endGet,
          haveExternalUserMapping: md5.haveExternalUserMapping,
        });
        appTypeId = md5App.id;
        break;
      case 'OTHER_CRYPTO':
        const otherApp = await ctx.prisma.createCryptoOther({
          Sendingtype: sendingType,
          requestUrl: other.requestUrl,
          function: other.function,
        });
        appTypeId = otherApp.id;
        break;
      default:
        throw new Error('Application type unknown!!');
    }

    const iconset = icon
      ? {
          create: {
            chemin: icon.chemin,
            nomOriginal: icon.nomOriginal,
            type: icon.type,
          },
        }
      : null;

    const ssoApplication = await ctx.prisma.createSsoApplication({
      nom,
      ssoType,
      icon: iconset,
      url,
      ssoTypeid: appTypeId,
    });

    await ctx.prisma.createApplicationsGroup({
      groupement: {
        connect: { id: idGroupement },
      },
      idapplications: {
        connect: { id: ssoApplication.id },
      },
    });

    mappings &&
      (await mappings.forEach(mapping => {
        ctx.prisma.createMappingVariable({
          nom: mapping.nom,
          value: mapping.value,
          idapplication: {
            connect: { id: ssoApplication.id },
          },
        });
      }));

    return ssoApplication;
  } catch (error) {
    console.log(error);
    throw new Error(error);
  }
};

export const UpdateSsoApplication = async (
  ctx: Context,
  {
    id,
    nom,
    url,
    icon,
    ssoType,
    mappings,
    idGroupement,
    sendingType,
    aes,
    md5,
    other,
    saml,
    token,
    toknFtp,
  }: MutationResolvers.ArgsUpdateSsoApplication,
) => {
  try {
    const ssoAppToUpdate = await ctx.prisma.ssoApplication({ id });
    console.log('ssoAppToUpdate ', ssoAppToUpdate);
    let appTypeId = '';
    switch (ssoType) {
      case 'SAML':
        const samlCreated = await ctx.prisma.updateSamlLogin({
          data: {
            Sendingtype: sendingType,
            consumerUrl: saml.consumerUrl,
            idpX509Certificate: saml.idpX509Certificate,
            spX509Certificate: saml.spX509Certificate,
          },
          where: { id: ssoAppToUpdate.ssoTypeid },
        });
        appTypeId = samlCreated.id;
        break;
      case 'TOKEN_AUTHENTIFICATION':
        const tokenApp = await ctx.prisma.updateTokenAuthentification({
          data: {
            Sendingtype: sendingType,
            reqTokenUrl: token.reqTokenUrl,
            reqUserUrl: token.reqUserUrl,
          },
          where: { id: ssoAppToUpdate.ssoTypeid },
        });
        appTypeId = tokenApp.id;
        break;
      case 'TOKEN_FTP_AUTHENTIFICATION':
        const ftp = await ctx.prisma.ftpConnects({ where: { groupement: { id: idGroupement } } });
        if (!ftp) {
          throw new ApolloError(
            "Vous devez configurer un FTP pour pouvoir utiliser ce type d'application",
          );
        }
        const tokenFtpApp = await ctx.prisma.updateTokenFtpAuthentification({
          data: {
            Sendingtype: sendingType,
            reqUserUrl: toknFtp.requestUrl,
            ftpFileName: toknFtp.ftpFileName,
            ftpFilePath: toknFtp.ftpFilePath,
            ftpid: {
              connect: { id: ftp[0].id },
            },
          },
          where: { id: ssoAppToUpdate.ssoTypeid },
        });
        appTypeId = tokenFtpApp.id;
        break;
      case 'CRYPTAGE_AES256':
        const aesApp = await ctx.prisma.updateCryptoAes256({
          data: {
            Sendingtype: sendingType,
            hexadecimal: aes.hexadecimal,
            key: aes.key,
            requestUrl: aes.requestUrl,
          },
          where: { id: ssoAppToUpdate.ssoTypeid },
        });
        console.log('aesApp ', aesApp);
        appTypeId = aesApp.id;
        break;
      case 'CRYPTAGE_MD5':
        const md5App = await ctx.prisma.updateCryptoMd5({
          data: {
            Sendingtype: sendingType,
            hexadecimal: md5.hexadecimal,
            requestUrl: md5.requestUrl,
            beginGet: md5.beginGet,
            endGet: md5.endGet,
            haveExternalUserMapping: md5.haveExternalUserMapping,
          },
          where: { id: ssoAppToUpdate.ssoTypeid },
        });
        appTypeId = md5App.id;
        break;
      case 'OTHER_CRYPTO':
        const otherApp = await ctx.prisma.updateCryptoOther({
          data: {
            Sendingtype: sendingType,
            requestUrl: other.requestUrl,
            function: other.function,
          },
          where: { id: ssoAppToUpdate.ssoTypeid },
        });
        appTypeId = otherApp.id;
        break;
      default:
        throw new Error('Application type unknown!!');
    }

    let data;
    if (icon) {
      const iconToUpdate = await ctx.prisma.ssoApplication({ id }).icon();
      console.log('iconToUpdate ', iconToUpdate);
      if (iconToUpdate) {
        await ctx.prisma.updateFichier({
          data: {
            chemin: icon.chemin,
            nomOriginal: icon.nomOriginal,
            type: icon.type,
          },
          where: { id: iconToUpdate.id },
        });
        data = {
          nom,
          ssoType,
          url,
          ssoTypeid: appTypeId,
        };
      } else {
        const iconset = {
          create: {
            chemin: icon.chemin,
            nomOriginal: icon.nomOriginal,
            type: icon.type,
          },
        };
        data = {
          nom,
          ssoType,
          icon: iconset,
          url,
          ssoTypeid: appTypeId,
        };
      }
    } else {
      const iconToDelete = await ctx.prisma.ssoApplication({ id }).icon();
      if (iconToDelete) {
        await ctx.prisma.deleteFichier({
          id: iconToDelete.id,
        });
      }
      data = {
        nom,
        ssoType,
        icon: null,
        url,
        ssoTypeid: appTypeId,
      };
    }

    const ssoApplication = await ctx.prisma.updateSsoApplication({
      data,
      where: { id },
    });

    const mappingsApps = await ctx.prisma.mappingVariables({
      where: { idapplication: { id } },
    });

    mappingsApps.forEach(mapping => {
      const find = mappings.find(map => map.id === mapping.id);
      if (!find) {
        ctx.prisma.deleteMappingVariable({ id: mapping.id });
      }
    });

    mappings &&
      (await mappings.forEach(mapping => {
        ctx.prisma.upsertMappingVariable({
          create: {
            nom: mapping.nom,
            value: mapping.value,
            idapplication: {
              connect: { id: mapping.id },
            },
          },
          update: {
            nom: mapping.nom,
            value: mapping.value,
            idapplication: {
              connect: { id: ssoApplication.id },
            },
          },
          where: { id: mapping.id },
        });
      }));

    return ssoApplication;
  } catch (error) {
    console.log('error ', error);
    throw new Error(error);
  }
};

export const DeleteSsoApplication = async (
  ctx: Context,
  { id }: MutationResolvers.ArgsDeleteSsoApplication,
) => {
  try {
    const ssoAppToDelete = await ctx.prisma.ssoApplication({ id });
    switch (ssoAppToDelete.ssoType) {
      case 'SAML':
        await ctx.prisma.deleteSamlLogin({ id: ssoAppToDelete.ssoTypeid });
        break;
      case 'TOKEN_AUTHENTIFICATION':
        await ctx.prisma.deleteTokenAuthentification({ id: ssoAppToDelete.ssoTypeid });
        break;
      case 'TOKEN_FTP_AUTHENTIFICATION':
        await ctx.prisma.deleteTokenFtpAuthentification({ id: ssoAppToDelete.ssoTypeid });
        break;
      case 'CRYPTAGE_AES256':
        await ctx.prisma.deleteCryptoAes256({ id: ssoAppToDelete.ssoTypeid });
        break;
      case 'CRYPTAGE_MD5':
        await ctx.prisma.deleteManyExternalMappings({ md5App: { id: ssoAppToDelete.ssoTypeid } });
        await ctx.prisma.deleteCryptoMd5({ id: ssoAppToDelete.ssoTypeid });
        break;
      case 'OTHER_CRYPTO':
        await ctx.prisma.deleteCryptoOther({ id: ssoAppToDelete.ssoTypeid });
        break;
      default:
        console.log('application associé non supprimé');
    }

    const iconToDelete = await ctx.prisma.ssoApplication({ id }).icon();
    if (iconToDelete) await ctx.prisma.deleteFichier({ id: iconToDelete.id });

    await ctx.prisma.deleteManyMappingVariables({ idapplication: { id } });
    await ctx.prisma.deleteManyApplicationsGroups({ idapplications: { id } });
    await ctx.prisma.deleteManyApplicationsRoles({ idapplications: { id } });
    return ctx.prisma.deleteSsoApplication({ id });
  } catch (error) {
    console.log('error ', error);
    throw new Error(error);
  }
};

export const CreateApplicationRole = async (
  ctx: Context,
  { idrole, idGroupement, idApplications },
) => {
  try {
    return ctx.prisma.createApplicationsRole({
      groupement: { connect: { id: idGroupement } },
      idapplications: { connect: { id: idApplications } },
      idrole: { connect: { id: idrole } },
    });
  } catch (error) {
    throw new Error(error);
  }
};

export const CreateManyApplicationRole = async (ctx: Context, { applicationsRoleInput }) => {
  try {
    const result: ApplicationsRole[] = await Promise.all(
      applicationsRoleInput.map(async applicationRole => {
        const { idrole, idGroupement, idApplications } = applicationRole;
        const appRole = await ctx.prisma.createApplicationsRole({
          groupement: { connect: { id: idGroupement } },
          idapplications: { connect: { id: idApplications } },
          idrole: { connect: { id: idrole } },
        });
        return appRole;
      }),
    );

    console.log('result ', result);

    return result;
  } catch (error) {
    throw new Error(error);
  }
};

export const DeleteApplicationRole = async (
  ctx: Context,
  { idrole, idGroupement, idApplications },
) => {
  try {
    const applicationRoleToDelete = await ctx.prisma.applicationsRoles({
      where: {
        idrole: { id: idrole },
        groupement: { id: idGroupement },
        idapplications: { id: idApplications },
      },
    });
    if (applicationRoleToDelete) {
      return ctx.prisma.deleteApplicationsRole({ id: applicationRoleToDelete[0].id });
    } else {
      throw new Error('Application not found');
    }
  } catch (error) {
    throw new Error(error);
  }
};
