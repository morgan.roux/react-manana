import { RgpdHistoriqueInfoPlusCreateInput } from '../generated/prisma-client';
import { Context, RgpdHistoriqueInfoPlusInput } from '../types';

export const createUpdateRgpdHistoriqueInfoPlus = async (
  ctx: Context,
  input: RgpdHistoriqueInfoPlusInput,
) => {
  const { id, accepted, idHistorique, idItemAssocie, type } = input;
  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const data: RgpdHistoriqueInfoPlusCreateInput = {
    accepted,
    idItemAssocie,
    type,
    historique: idHistorique ? { connect: { id: idHistorique } } : null,
    groupement,
  };
  const res = await ctx.prisma.upsertRgpdHistoriqueInfoPlus({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  return res;
};
