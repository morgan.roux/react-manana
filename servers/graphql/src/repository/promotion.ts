import { Context, PromotionInput, GroupeClienRemisetInput } from '../types';
import { Promotion } from '../generated/prisma-client';
import logger from '../logging';
import { ApolloError } from 'apollo-server';
import { deleteRemise, createRemisePalier, createRemiseDetail } from './remise';

export const promotionArticlesAssocies = async (ctx: Context, { id }) => {
  logger.info(`*** [PROMOTION ARTICLES] ***}`);
  const promotionArticles = await ctx.prisma.promotionArticles({
    where: { promotion: { id } },
  });
  logger.info(`[PROMOTION ARTICLES] : ${JSON.stringify(promotionArticles)}`);
  if (promotionArticles && promotionArticles.length) {
    return (
      await Promise.all(
        promotionArticles.map(promotionArticle =>
          ctx.prisma.promotionArticle({ id: promotionArticle.id }).produitCanal(),
        ),
      )
    ).filter(article => article);
  }

  return [];
};

export const promotionGroupeClientRemise = async (ctx: Context, { id }) => {
  return ctx.prisma.promotionGroupeClients({
    where: {
      promotion: {
        id,
      },
    },
  });
};

export const softDeletePromotion = async (ctx: Context, { id }) => {
  return ctx.prisma
    .updatePromotion({ where: { id }, data: { isRemoved: true } })
    .then(async promotion => {
      await ctx.indexing.publishSave('promotions', promotion.id);
      return promotion;
    });
};

export const createUpdatePromotion = async (
  ctx: Context,
  {
    id,
    nom,
    dateDebut,
    dateFin,
    promotionType,
    status,
    codeCanal,
    idsCanalArticle,
    groupeClients,
  }: PromotionInput,
) => {
  let promotion: Promotion;

  try {
    promotion = await ctx.prisma.upsertPromotion({
      where: { id: id ? id : '' },
      create: {
        nom,
        dateDebut,
        dateFin,
        promotionType,
        status,
        commandeCanal: {
          connect: {
            code: codeCanal,
          },
        },
        userCreated: {
          connect: {
            id: ctx.userId,
          },
        },
        userModified: {
          connect: {
            id: ctx.userId,
          },
        },
      },
      update: {
        nom,
        dateDebut,
        dateFin,
        promotionType,
        status,
        commandeCanal: {
          connect: {
            code: codeCanal,
          },
        },
        userModified: {
          connect: {
            id: ctx.userId,
          },
        },
      },
    });
  } catch (error) {
    logger.error(`[ERROR CREATE PROMOTION] : ${JSON.stringify(error)}`);
    return new ApolloError(`Erreur lors de la creation du promotion`, 'ERROR_CREATE_PROMOTION');
  }

  try {
    await createUpdatePromotionArticles(ctx, promotion.id, idsCanalArticle);
  } catch (error) {
    logger.error(`[ERROR CREATE PROMOTION ARTICLE] : ${JSON.stringify(error)}`);
    if (!id) await _resetPromotion(ctx, { id: promotion.id });
    return new ApolloError(
      `Erreur lors de la creation du promotion article`,
      'ERROR_CREATE_PROMOTION_ARTICLE',
    );
  }

  try {
    await createUpdateOperationGroupeClientRemise(ctx, promotion.id, groupeClients);
  } catch (error) {
    logger.error(`[ERROR CREATE PROMOTION GROUPE] : ${JSON.stringify(error)}`);
    if (!id) await _resetPromotion(ctx, { id: promotion.id });
    return new ApolloError(
      `Erreur lors de la creation du promotion groupe de client avec les paliers correspondants`,
      'ERROR_CREATE_PROMOTION_GROUPE',
    );
  }

  return ctx.prisma.promotion({ id: promotion.id }).then(async promotion => {
    await ctx.indexing.publishSave('promotions', promotion.id);
    return promotion;
  });
};

const _resetPromotion = async (ctx: Context, { id }) => {
  await ctx.prisma.deleteManyPromotionArticles({ promotion: { id } });
  return ctx.prisma.deletePromotion({ id });
};

export const createUpdatePromotionArticles = async (
  ctx: Context,
  id: string,
  idsCanalArticles: string[],
) => {
  const articlesToDeleted = await ctx.prisma.promotionArticles({
    where: { promotion: { id }, produitCanal: { id_not_in: idsCanalArticles } },
  });
  if (articlesToDeleted && articlesToDeleted.length) {
    await ctx.prisma.deleteManyPromotionArticles({
      id_in: articlesToDeleted.map(toDelete => toDelete.id),
    });
  }
  logger.info(
    `[CREATE PROMOTION ARTICLE] : idsCanalArticles => ${JSON.stringify(
      idsCanalArticles,
    )} | articlesToDeleted => ${JSON.stringify(articlesToDeleted)}`,
  );
  return Promise.all(
    idsCanalArticles.map(async idCanalArticle => {
      const article = await ctx.prisma.$exists.promotionArticle({
        promotion: { id },
        produitCanal: { id: idCanalArticle },
      });
      if (!article) {
        return ctx.prisma.createPromotionArticle({
          promotion: {
            connect: { id },
          },
          produitCanal: {
            connect: {
              id: idCanalArticle,
            },
          },
        });
      }
    }),
  );
};

export const createUpdateOperationGroupeClientRemise = async (
  ctx: Context,
  id: string,
  groupeClientsRemises: GroupeClienRemisetInput[],
) => {
  const canal = await ctx.prisma.promotion({ id }).commandeCanal();
  const dateDebut = await ctx.prisma.promotion({ id }).dateDebut();
  const dateFin = await ctx.prisma.promotion({ id }).dateFin();
  const nom = await ctx.prisma.promotion({ id }).nom();

  const groupesToDelete = await ctx.prisma.promotionGroupeClients({ where: { promotion: { id } } });
  logger.info(`[DELETE PROMOTION GROUPE] : groupesToDelete => ${JSON.stringify(groupesToDelete)}`);

  if (groupesToDelete && groupesToDelete.length) {
    const remisesToDelete = (
      await Promise.all(
        groupesToDelete.map(groupe => ctx.prisma.promotionGroupeClient({ id: groupe.id }).remise()),
      )
    ).filter(remise => remise);
    logger.info(
      `[DELETE REMISE PROMOTION GROUPE] : remisesToDelete => ${JSON.stringify(remisesToDelete)}`,
    );
    if (remisesToDelete && remisesToDelete.length) {
      await Promise.all(
        remisesToDelete.map(async remise => deleteRemise(ctx, { idRemise: remise.id })),
      );
    }

    await ctx.prisma.deleteManyPromotionGroupeClients({
      id_in: groupesToDelete.map(toDelete => toDelete.id),
    });
  }

  return Promise.all(
    groupeClientsRemises.map(async (groupeClientRemise, index) => {
      const remise = groupeClientRemise.idRemise
        ? await ctx.prisma.remise({ id: groupeClientRemise.idRemise })
        : await createRemisePalier(
            ctx,
            {
              codeGroupeClient: groupeClientRemise.codeGroupeClient,
              codeCanal: canal.code,
              dateDebut,
              dateFin,
              nomRemise: `Remise ${nom}`,
              active: true,
              nbRefMin: 0,
              modelRemise: 'LIGNE',
              typeRemise: 'R',
              source: 'PROMOTION',
            },
            index + 1,
          );

      const whereExist = remise
        ? {
            promotion: { id },
            groupeClient: { codeGroupe: groupeClientRemise.codeGroupeClient },
            remise: { id: remise.id },
          }
        : {
            promotion: { id },
            groupeClient: { codeGroupe: groupeClientRemise.codeGroupeClient },
          };
      const exist = await ctx.prisma.$exists.promotionGroupeClient(whereExist);

      if (!exist) {
        await ctx.prisma.createPromotionGroupeClient({
          promotion: {
            connect: { id },
          },
          groupeClient: {
            connect: {
              codeGroupe: groupeClientRemise.codeGroupeClient,
            },
          },
          remise: remise
            ? {
                connect: {
                  id: remise.id,
                },
              }
            : null,
        });
      }

      /* Create remise detail or recraite if already exist */
      if (remise && groupeClientRemise.remiseDetails.length) {
        await ctx.prisma.deleteManyRemiseDetails({
          remise: {
            id: remise.id,
          },
        });

        await Promise.all(
          groupeClientRemise.remiseDetails.map(async detail =>
            createRemiseDetail(ctx, remise.id, {
              pourcentageRemise: detail.pourcentageRemise,
              quantiteMax: detail.quantiteMax,
              quantiteMin: detail.quantiteMin,
              remiseSupplementaire: detail.remiseSupplementaire,
            }),
          ),
        );
      }

      return groupeClientRemise;
    }),
  );
};

export const promotionRemise = async (ctx: Context, { id, idPharmacie }) => {
  console.log('idPharmacie', idPharmacie);
  if (!idPharmacie) return null;
  const promotionGroupeClients = await ctx.prisma.promotionGroupeClients({
    where: {
      promotion: {
        id,
      },
      groupeClient: {
        pharmacies_some: {
          id: idPharmacie,
        },
      },
    },
    /*  last: 1, */
  });

  console.log('promotionGroupeClients', promotionGroupeClients);

  if (!promotionGroupeClients || (promotionGroupeClients && !promotionGroupeClients.length))
    return null;

  const remises = await Promise.all(
    promotionGroupeClients.map(promoGroupe =>
      ctx.prisma.promotionGroupeClient({ id: promoGroupe.id }).remise(),
    ),
  );

  if (!remises || (remises && !remises.length)) return null;

  const details = await ctx.prisma.remiseDetails({
    where: {
      remise: { id_in: remises.map(remise => remise.id) },
    },
    first: 1,
    orderBy: 'quantiteMin_ASC',
  });

  if (!details || (details && !details.length)) return null;

  return ctx.prisma.remiseDetail({ id: details[0].id }).remise();
};

export const promotionRemiseDetails = async (ctx: Context, { id, idPharmacie }) => {
  const remise = await promotionRemise(ctx, { id, idPharmacie });

  return remise
    ? ctx.prisma.remiseDetails({
        where: {
          remise: { id: remise.id },
        },
      })
    : null;
};
