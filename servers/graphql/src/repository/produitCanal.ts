import { Context } from '../types';
import { ProduitCanal, Marche, Promotion } from '../generated/prisma-client';
import { remiseLigneDetails, getRemisePanacheeDetails, articleSamePanachees } from './remise';
import ArrayUniqueObject from 'unique-array-objects';

export const getCountProduits = async (terms: any[], ctx: Context) => {
  const searchParams = {
    body: {
      query: {
        constant_score: {
          filter: {
            bool: {
              must: [...terms, { term: { isActive: true } }, { term: { isRemoved: false } }],
            },
          },
        },
      },
    },
    index: ['produitcanal'],
  };

  return ctx.indexing
    .getElasticSearchClient()
    .search(searchParams)
    .then(result => {
      return {
        total: result.body.hits.total.value ? parseInt(result.body.hits.total.value, 10) : 0,
      };
    });
};

export const getGamme = async (id: string, ctx: Context) => {
  return ctx.prisma.canalSousGamme({ id }).canalGamme();
};

export const isPorduitCanalActive = async (ctx: Context, { id }) => {
  const produitCanal = await ctx.prisma.produitCanal({
    id,
  });
  const produit = await ctx.prisma
    .produitCanal({
      id,
    })
    .produit();

  if (!produit) return false;

  const familles = await ctx.prisma.produitFamilles({
    where: {
      produit: { id: produit.id },
    },
  });

  /* const gamme = await ctx.prisma
    .produitCanal({
      id,
    })
    .sousGamme()
    .then(async sous => (sous ? await getGamme(sous.id, ctx) : null)); */

  if (familles && familles.length && produitCanal && !produitCanal.codeBlocage) {
    return true;
  }

  return false;
};

export const articleCanalRemiseDetails = async (ctx: Context, idCanalArticle, { idPharmacie }) => {
  const codeCanal = await ctx.prisma
    .produitCanal({
      id: idCanalArticle,
    })
    .canal()
    .code();

  try {
    const panachees = await getRemisePanacheeDetails(ctx, {
      idCanalArticle,
      codeCanal,
      idPharmacie,
    });

    const samePanachees = await articleSamePanachees(ctx, {
      idCanalArticle,
      codeCanal,
      idPharmacie,
    });

    const idArticles = samePanachees
      .filter(samePanachee => samePanachee.id !== idCanalArticle)
      .map(same => same.id);

    return panachees && panachees.length && idArticles.length
      ? panachees
      : await remiseLigneDetails(ctx, {
          idCanalArticle,
          codeCanal,
          idPharmacie,
        });
  } catch (err) {
    return null;
  }
};

export const getArticlesByCodeReferents = async (
  ctx: Context,
  codeReferents: string[],
): Promise<ProduitCanal[]> => {
  const defaultValue: ProduitCanal[] = [];
  return codeReferents.reduce(async (prev, data): Promise<ProduitCanal[]> => {
    const result = await prev;
    if (data) {
      const article = await ctx.prisma
        .produitCodes({
          where: {
            code: data,
          },
        })
        .then(async histoes =>
          histoes && histoes.length
            ? ctx.prisma
                .produitCode({
                  id: histoes[0].id,
                })
                .produit()
            : null,
        );

      if (article && article.id) return [...result, article];
    }

    return [...result];
  }, Promise.resolve(defaultValue));
};

export const canalArticleMarches = async (
  ctx: Context,
  { idCanalArticle }: { idCanalArticle: string },
): Promise<Marche[]> => {
  const marcheArticles = await ctx.prisma.marcheArticles({
    where: { produitCanal: { id: idCanalArticle } },
  });

  if (marcheArticles && marcheArticles.length) {
    const marches = (
      await Promise.all(
        marcheArticles.map(async marcheArticle =>
          ctx.prisma.marcheArticle({ id: marcheArticle.id }).marche(),
        ),
      )
    ).filter(marche => marche);

    return marches && marches.length ? ArrayUniqueObject(marches) : [];
  }

  return [];
};

export const canalArticlePromotions = async (
  ctx: Context,
  { idCanalArticle }: { idCanalArticle: string },
): Promise<Promotion[]> => {
  const promotionArticles = await ctx.prisma.promotionArticles({
    where: { produitCanal: { id: idCanalArticle } },
  });

  if (promotionArticles && promotionArticles.length) {
    const promotions = (
      await Promise.all(
        promotionArticles.map(async promotionArticle =>
          ctx.prisma.promotionArticle({ id: promotionArticle.id }).promotion(),
        ),
      )
    ).filter(promotion => promotion);

    return promotions && promotions.length ? ArrayUniqueObject(promotions) : [];
  }

  return [];
};

export const softDeleteCanalArticle = async (ctx: Context, { id }) => {
  return ctx.prisma
    .updateProduitCanal({
      where: { id },
      data: { isRemoved: true },
    })
    .then(async article => {
      await ctx.indexing.publishSave('produitcanals', article.id);
      return article;
    });
};

export const canalProduit = async (ctx: Context, { id }) => {
  return ctx.prisma
    .produitCanal({
      id,
    })
    .canal();
};
