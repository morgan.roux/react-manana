import { Context } from '../types';
import crypto = require('crypto');
import { sendMail } from '../services/mailer';
import { ApolloError } from 'apollo-server';
import logger from '../logging';

export const ckeckUserIpAndSendMail = async (ctx: Context, { ip, idUser }) => {
  const isInConnexion = await ctx.prisma
    .connexions({ where: { ip, idUser: { id: idUser } } })
    .then(connexions => connexions.length > 0);

  if (isInConnexion) return { accessToken: '' };

  const url = process.env.CLIENT_URL.replace('http://', '').replace('https://', '');
  const currentUser = await ctx.prisma.user({ id: idUser });
  if (!currentUser) {
    return new ApolloError("Le compte d'utilisateur n'existe pas", 'ACCOUNT_NOT_EXIST');
  }

  const accessToken = crypto.randomBytes(256).toString('hex');
  await ctx.redis.set(accessToken, ip);
  await ctx.redis.set(ip, currentUser.id);

  const substitutionData = {
    app_url: url,
    token: accessToken,
  };

  const templateId = process.env.USE_SENDGRID === 'true' ? process.env.CONFIRM_IP_TEMPLATE_ID : '';
  if (!templateId) {
    logger.info(`Utilise sendGrid pour l'envoie du mail`);
    return new ApolloError("Utilise sendGrid pour l'envoie du mail", 'USE_SEND_GRID');
  }

  try {
    await sendMail({ to: currentUser.email, templateId, substitutionData });
  } catch (error) {
    logger.error(`Erreur envoie de mail`, {
      error,
      function: 'sendMail',
      source: process.env.USE_SENDGRID === 'true' ? 'sendGrid' : 'sparkPost',
    });
    return new ApolloError(
      "Erreur lors de l'envoie d'email de confirmation IP",
      'MAIL_IP_NOT_SENT',
    );
  }
  logger.info(`Current User '${currentUser.email}' , accessToken ==> ${accessToken}`);
  return { accessToken };
};

export const validateUserIp = async (ctx: Context, { token, status }) => {
  const ip = await ctx.redis.get(token);
  const userId = await ctx.redis.get(ip);
  if (!ip || !userId) return new ApolloError('Votre token a expiré', 'EXPIRED_TOKEN');

  const connexionStatus = status === 'AUTHORIZED' || status === 'BLOCKED' ? status : 'TO_CONFIRM';
  await saveUserIp(ctx, {
    statut: connexionStatus,
    ip,
    idUser: userId,
    appareil: null,
    emplacement: null,
  }).catch(
    error =>
      new ApolloError(`Erreur lors de la validation de l'ip de l'appareil`, 'INSERT_IP_ERROR'),
  );
  const accessToken = (await crypto.randomBytes(256)).toString('hex');
  await ctx.redis.set(accessToken, userId, 'EX', process.env.REDIS_EXPIRE);

  return { accessToken };
};

export const saveUserIp = async (ctx: Context, { statut, ip, idUser, appareil, emplacement }) => {
  const connexions = await ctx.prisma.connexions({ where: { ip, idUser: { id: idUser } } });
  if (connexions && connexions.length) return connexions[0];
  return ctx.prisma.createConnexion({
    statut,
    ip,
    appareil,
    emplacement,
    idUser: { connect: { id: idUser } },
  });
};
