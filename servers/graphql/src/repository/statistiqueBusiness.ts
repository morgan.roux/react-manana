import { Context } from '../types';
import { Pharmacie, Commande } from '../generated/prisma-client';
import ArrayUniqueObject from 'unique-array-objects';
import ArrayUnique = require('array-unique');
import { pharmacieIds } from '../repository/pharmacie';
import { operationIds } from '../repository/operation';

interface StatistiqueBusinessParameter {
  idsOperation: string[];
  idsPharmacie: string[];
  idPharmaciesSelected: string[];
  skip: number;
  take: number;
}

export const orderByBusiness = [
  'caTotal',
  'nbCommandes',
  'nbProduits',
  'nbLignes',
  'nbPharmacies',
  'caMoyenParPharmacie',
  'totalRemise',
  'globaliteRemise',
];

export const orderByPharmacie = ['nom', 'cip', 'cp', 'ville'];

export const orderByOpeartion = ['libelle', 'dateDebut', 'dateFin'];

export const pharmaciesOperationsBusiness = async (
  ctx: Context,
  pilotage: string,
  { idsOperation, idsPharmacie, idPharmaciesSelected, skip, take }: StatistiqueBusinessParameter,
) => {
  const filterOP = pilotage === 'OPERATION' ? { first: take, skip } : { first: null, skip: null };
  const filterPharma =
    pilotage !== 'OPERATION' ? { first: take, skip } : { first: null, skip: null };

  const uniqueOperationIds: any[] =
    idsOperation && idsOperation.length
      ? ArrayUnique(await operationIds(ctx, idsOperation, filterOP))
      : [];
  const uniquePharmacieIds: any[] =
    idsPharmacie && idsPharmacie.length
      ? ArrayUnique(await pharmacieIds(ctx, idsPharmacie, filterPharma))
      : [];

  console.log(
    '[GET INDICATEUR STATISTIQUE OPERATION PHARMACIE]',
    uniqueOperationIds,
    uniquePharmacieIds,
  );

  if (!uniqueOperationIds.length || !uniquePharmacieIds.length) return null;

  const business = await businessAchieved(
    ctx,
    uniqueOperationIds.map(id => String(id)),
    uniquePharmacieIds.map(id => String(id)),
  );

  const nbPharmacies = (await pharmaciesCommandeOperation(ctx, idsOperation)).length;

  let caTotalPhamacies = 0;
  if (pilotage !== 'OPERATION' && idPharmaciesSelected && idPharmaciesSelected.length) {
    caTotalPhamacies = await caPhamacies(ctx, idsOperation, idPharmaciesSelected);
  }

  const caMoyenParPharmacie =
    pilotage !== 'OPERATION' && idPharmaciesSelected && idPharmaciesSelected.length
      ? caTotalPhamacies / nbPharmacies
      : business && business.totalCA && nbPharmacies
      ? nbPharmacies === 0
        ? business.totalCA
        : business.totalCA / nbPharmacies
      : 0;

  return {
    operations: await ctx.prisma.operations({ where: { id_in: uniqueOperationIds } }),
    pharmacies: await ctx.prisma.pharmacies({ where: { id_in: uniquePharmacieIds } }),
    caTotal: business && business.totalCA ? business.totalCA : 0,
    nbCommandes: business && business.nbCommande ? business.nbCommande : 0,
    nbProduits: business && business.nbProduits ? business.nbProduits : 0,
    nbLignes: business && business.nbLignes ? business.nbLignes : 0,
    nbPharmacies,
    caMoyenParPharmacie: caMoyenParPharmacie || 0,
    totalRemise: business && business.totalRemise ? business.totalRemise : 0,
    globaliteRemise: business && business.pourcentageRemise ? business.pourcentageRemise : 0,
  };
};

const caPhamacies = async (ctx: Context, idsOperation: string[], idsPharmacie: string[]) => {
  const commandes = await ctx.prisma.commandes({
    where: {
      idOperation: { id_in: idsOperation },
      idPharmacie: { id_in: idsPharmacie },
    },
  });

  const listCAs = await Promise.all(
    commandes.map(async commande => {
      return ctx.prisma.commande({ id: commande.id }).prixNetTotalHT();
    }),
  );

  return listCAs && listCAs.length ? parseFloat(listCAs.reduce((a, b) => a + b, 0).toFixed(2)) : 0;
};

export const businessAchieved = async (
  ctx: Context,
  idsOperation: string[],
  idsPharmacie: string[],
) => {
  // liste des commandes de l'operation commercial de la pharmacie
  const commandes = await ctx.prisma.commandes({
    where: {
      idOperation: { id_in: idsOperation },
      idPharmacie: { id_in: idsPharmacie },
    },
  });

  if (!commandes || (commandes && !commandes.length)) {
    return {
      totalCA: 0,
      nbCommande: 0,
      nbLignes: 0,
      nbProduits: 0,
      totalRemise: 0,
      pourcentageRemise: 0,
    };
  }

  const nbLigneAndProduit = await businessNumberLignesAndProduits(ctx, commandes);
  const totalRemise = await businessTotalRemise(ctx, commandes);
  const totalCA = await businessTotalCA(ctx, commandes);
  const caBrut = totalRemise + totalCA;
  const pourcentageRemise = totalCA !== 0 && totalRemise !== 0 ? (totalRemise / caBrut) * 100 : 0;

  return {
    totalCA,
    nbCommande: commandes.length,
    nbLignes: nbLigneAndProduit && nbLigneAndProduit.nbLignes ? nbLigneAndProduit.nbLignes : 0,
    nbProduits:
      nbLigneAndProduit && nbLigneAndProduit.nbProduits ? nbLigneAndProduit.nbProduits : 0,
    totalRemise,
    pourcentageRemise,
  };
};

export const businessNumberLignesAndProduits = async (ctx: Context, commandes: Commande[]) => {
  const commandeLignes = (
    await Promise.all(
      commandes.map(async commande => {
        return ctx.prisma.commandeLignes({
          where: {
            AND: [{ idCommande: { id: commande.id } }],
          },
        });
      }),
    )
  ).flat();

  let nbProduits = 0;
  const uniqueLignes: string[] = await commandeLignes.reduce(async (idsCanalArticle, ligne) => {
    const canalArticle = await ctx.prisma.commandeLigne({ id: ligne.id }).produitCanal();
    nbProduits += ligne.quantiteCdee;
    const results = await idsCanalArticle;

    if (canalArticle && !results.includes(canalArticle.id)) return [...results, canalArticle.id];

    return [...results];
  }, Promise.resolve([]));

  return {
    nbLignes: uniqueLignes.length,
    nbProduits,
  };
};

export const businessTotalRemise = async (ctx: Context, commandes: Commande[]) => {
  const remiseCommandes = await Promise.all(
    commandes.map(async commande => {
      return ctx.prisma.commande({ id: commande.id }).valeurRemiseTotal();
    }),
  );

  return remiseCommandes && remiseCommandes.length
    ? parseFloat(remiseCommandes.reduce((a, b) => a + b, 0).toFixed(2))
    : 0;
};

export const businessTotalCA = async (ctx: Context, commandes: Commande[]) => {
  const listCAs = await Promise.all(
    commandes.map(async commande => {
      return ctx.prisma.commande({ id: commande.id }).prixNetTotalHT();
    }),
  );

  return listCAs && listCAs.length ? parseFloat(listCAs.reduce((a, b) => a + b, 0).toFixed(2)) : 0;
};

export const pharmaciesCommandeOperation = async (
  ctx: Context,
  idsOperation: string[],
): Promise<Pharmacie[]> => {
  const commandes = await ctx.prisma.commandes({
    where: { idOperation: { id_in: idsOperation } },
  });

  const pharmacies = (
    await Promise.all(
      commandes.map(async commande => {
        return ctx.prisma.commande({ id: commande.id }).idPharmacie();
      }),
    )
  ).filter(pharma => pharma);

  return pharmacies && pharmacies.length ? ArrayUniqueObject(pharmacies) : [];
};

export const businessOperationObjectifRealise = async (
  ctx: Context,
  pilotage: string,
  indicateur: string,
  idOperations: string[],
  idPharmacies: string[],
  idPharmaciesSelected: string[],
) => {
  const operations = await ctx.prisma.operations({ where: { id_in: idOperations } });
  const statistique = await pharmaciesOperationsBusiness(ctx, pilotage, {
    idsOperation: idOperations,
    idsPharmacie: idPharmacies,
    idPharmaciesSelected,
    take: null,
    skip: null,
  });

  switch (indicateur) {
    case 'CA':
      return {
        objectif:
          operations && operations.length
            ? operations
                .map(operation =>
                  pilotage === 'PHARMACIE' && idPharmacies.length === 1
                    ? operation.caMoyenPharmacie
                    : operation.caMoyenPharmacie * operation.nbPharmacieCommande,
                )
                .reduce((sum, value) => sum + value)
            : 0,
        value: statistique && statistique.caTotal ? statistique.caTotal : 0,
      };
    case 'CA_MOYEN':
      return {
        objectif:
          operations && operations.length
            ? operations
                .map(operation => operation.caMoyenPharmacie)
                .reduce((sum, value) => sum + value)
            : 0,
        value: statistique && statistique.caMoyenParPharmacie ? statistique.caMoyenParPharmacie : 0,
      };
    case 'NB_LIGNE':
      return {
        objectif:
          operations && operations.length
            ? operations
                .map(operation => operation.nbMoyenLigne)
                .reduce((sum, value) => sum + value)
            : 0,
        value: statistique && statistique.nbLignes ? statistique.nbLignes : 0,
      };
    case 'NB_PHARMACIE':
      return {
        objectif:
          operations && operations.length
            ? operations
                .map(operation => operation.nbPharmacieCommande)
                .reduce((sum, value) => sum + value)
            : 0,
        value: statistique && statistique.nbPharmacies ? statistique.nbPharmacies : 0,
      };

    /* Pas d'objectif on n'affiche pas le jauge */
    case 'NB_COMMANDE':
      return {
        objectif: 0,
        value: statistique && statistique.nbCommandes ? statistique.nbCommandes : 0,
      };
    case 'NB_PRODUIT':
      return {
        objectif: 0,
        value: statistique && statistique.nbProduits ? statistique.nbProduits : 0,
      };

    case 'TOTAL_REMISE':
      return {
        objectif: 0,
        value: statistique && statistique.totalRemise ? statistique.totalRemise : 0,
      };

    case 'REMISE':
      return {
        objectif: 0,
        value: statistique && statistique.globaliteRemise ? statistique.globaliteRemise : 0,
      };
  }

  return {
    objectif: 0,
    value: 0,
  };
};

export const piloteBusiness = async (
  ctx: Context,
  pilotage: string,
  {
    idsOperation,
    idsPharmacie,
    idPharmaciesSelected,
  }: {
    idsOperation: string[];
    idsPharmacie: string[];
    idPharmaciesSelected: string[];
  },
) => {
  const business = await businessAchieved(ctx, idsOperation, idsPharmacie);

  const nbPharmacies = (await pharmaciesCommandeOperation(ctx, idsOperation)).length;

  let caTotalPhamacies = 0;
  if (pilotage !== 'OPERATION' && idPharmaciesSelected && idPharmaciesSelected.length) {
    caTotalPhamacies = await caPhamacies(ctx, idsOperation, idPharmaciesSelected);
  }

  const caMoyenParPharmacie =
    pilotage !== 'OPERATION' && idPharmaciesSelected && idPharmaciesSelected.length
      ? caTotalPhamacies / nbPharmacies
      : business && business.totalCA && nbPharmacies
      ? business.totalCA / nbPharmacies
      : 0;

  return {
    id: pilotage === 'OPERATION' ? idsOperation[0] : String(idsPharmacie[0]),
    operation:
      pilotage === 'OPERATION' ? await ctx.prisma.operation({ id: idsOperation[0] }) : null,
    pharmacie:
      pilotage === 'OPERATION' ? null : await ctx.prisma.pharmacie({ id: idsPharmacie[0] }),
    canal:
      pilotage === 'OPERATION'
        ? await ctx.prisma.operation({ id: idsOperation[0] }).idCommandeCanal()
        : null,
    caTotal: business && business.totalCA ? business.totalCA : 0,
    nbCommandes: business && business.nbCommande ? business.nbCommande : 0,
    nbProduits: business && business.nbProduits ? business.nbProduits : 0,
    nbLignes: business && business.nbLignes ? business.nbLignes : 0,
    nbPharmacies,
    caMoyenParPharmacie: caMoyenParPharmacie || 0,
    totalRemise: business && business.totalRemise ? business.totalRemise : 0,
    globaliteRemise: business && business.pourcentageRemise ? business.pourcentageRemise : 0,
  };
};

export const businessAchievedInYear = async (
  ctx: Context,
  year: string,
  pilotage: string,
  indicateur: string,
  idOperations: string[],
  idPharmacies: string[],
  idPharmaciesSelected: string[],
) => {
  const currentDate = new Date();
  const seletedYear = year ? parseInt(year, 10) : currentDate.getFullYear();
  const dateMonths = monthInYear(seletedYear);

  switch (indicateur) {
    case 'NB_COMMANDE':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          const commandes = await ctx.prisma.commandes({
            where: {
              idOperation: { id_in: idOperations },
              idPharmacie: { id_in: idPharmacies },
              dateCommande_gte: dateMonth.debut,
              dateCommande_lt: dateMonth.fin,
            },
          });

          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: commandes.length,
          };
        }),
      );
    case 'CA':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          const commandes = await ctx.prisma.commandes({
            where: {
              idOperation: { id_in: idOperations },
              idPharmacie: { id_in: idPharmacies },
              dateCommande_gte: dateMonth.debut,
              dateCommande_lt: dateMonth.fin,
            },
          });

          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: await businessTotalCA(ctx, commandes),
          };
        }),
      );
    case 'CA_MOYEN':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          const commandes = await ctx.prisma.commandes({
            where: {
              idOperation: { id_in: idOperations },
              idPharmacie: { id_in: idPharmacies },
              dateCommande_gte: dateMonth.debut,
              dateCommande_lt: dateMonth.fin,
            },
          });

          if (!commandes || (commandes && !commandes.length)) {
            return {
              key: dateMonth.debut, // dateMonth.libelle
              value: 0,
            };
          }

          const pharmacies = (
            await Promise.all(
              commandes.map(async commande => {
                return ctx.prisma.commande({ id: commande.id }).idPharmacie();
              }),
            )
          ).filter(pharma => pharma);
          const nbPharmacies =
            pharmacies && pharmacies.length ? ArrayUniqueObject(pharmacies).length : 0;

          let caTotalPhamacies = 0;
          if (pilotage !== 'OPERATION' && idPharmaciesSelected && idPharmaciesSelected.length) {
            caTotalPhamacies = await caPhamacies(ctx, idOperations, idPharmaciesSelected);
          }
          const totalCA = await businessTotalCA(ctx, commandes);
          const caMoyenParPharmacie =
            pilotage !== 'OPERATION' && idPharmaciesSelected && idPharmaciesSelected.length
              ? caTotalPhamacies / nbPharmacies
              : totalCA && nbPharmacies
              ? nbPharmacies === 0
                ? totalCA
                : totalCA / nbPharmacies
              : 0;

          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: caMoyenParPharmacie,
          };
        }),
      );
    case 'NB_LIGNE':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          const commandes = await ctx.prisma.commandes({
            where: {
              idOperation: { id_in: idOperations },
              idPharmacie: { id_in: idPharmacies },
              dateCommande_gte: dateMonth.debut,
              dateCommande_lt: dateMonth.fin,
            },
          });
          if (!commandes || (commandes && !commandes.length)) {
            return {
              key: dateMonth.debut, // dateMonth.libelle
              value: 0,
            };
          }
          const nombreLignesAndProduits = await businessNumberLignesAndProduits(ctx, commandes);
          return {
            key: dateMonth.debut, // dateMonth.libelle
            value:
              nombreLignesAndProduits && nombreLignesAndProduits.nbLignes
                ? nombreLignesAndProduits.nbLignes
                : 0,
          };
        }),
      );
    case 'NB_PRODUIT':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          const commandes = await ctx.prisma.commandes({
            where: {
              idOperation: { id_in: idOperations },
              idPharmacie: { id_in: idPharmacies },
              dateCommande_gte: dateMonth.debut,
              dateCommande_lt: dateMonth.fin,
            },
          });
          if (!commandes || (commandes && !commandes.length)) {
            return {
              key: dateMonth.debut, // dateMonth.libelle
              value: 0,
            };
          }
          const nombreLignesAndProduits = await businessNumberLignesAndProduits(ctx, commandes);
          return {
            key: dateMonth.debut, // dateMonth.libelle
            value:
              nombreLignesAndProduits && nombreLignesAndProduits.nbProduits
                ? nombreLignesAndProduits.nbProduits
                : 0,
          };
        }),
      );

    case 'NB_PHARMACIE':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          const commandes = await ctx.prisma.commandes({
            where: {
              idOperation: { id_in: idOperations },
              idPharmacie: { id_in: idPharmacies },
              dateCommande_gte: dateMonth.debut,
              dateCommande_lt: dateMonth.fin,
            },
          });
          if (!commandes || (commandes && !commandes.length)) {
            return {
              key: dateMonth.debut, // dateMonth.libelle
              value: 0,
            };
          }
          const pharmacies = (
            await Promise.all(
              commandes.map(async commande => {
                return ctx.prisma.commande({ id: commande.id }).idPharmacie();
              }),
            )
          ).filter(pharma => pharma);

          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: pharmacies && pharmacies.length ? ArrayUniqueObject(pharmacies).length : 0,
          };
        }),
      );
    case 'TOTAL_REMISE':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          const commandes = await ctx.prisma.commandes({
            where: {
              idOperation: { id_in: idOperations },
              idPharmacie: { id_in: idPharmacies },
              dateCommande_gte: dateMonth.debut,
              dateCommande_lt: dateMonth.fin,
            },
          });

          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: await businessTotalRemise(ctx, commandes),
          };
        }),
      );
    case 'REMISE':
      return Promise.all(
        dateMonths.map(async dateMonth => {
          const commandes = await ctx.prisma.commandes({
            where: {
              idOperation: { id_in: idOperations },
              idPharmacie: { id_in: idPharmacies },
              dateCommande_gte: dateMonth.debut,
              dateCommande_lt: dateMonth.fin,
            },
          });
          if (!commandes || (commandes && !commandes.length)) {
            return {
              key: dateMonth.debut, // dateMonth.libelle
              value: 0,
            };
          }
          const totalCA = await businessTotalCA(ctx, commandes);
          const totalRemise = await businessTotalRemise(ctx, commandes);
          const caBrut = totalRemise + totalCA;
          const pourcentageRemise =
            totalCA !== 0 && totalRemise !== 0 ? (totalRemise / caBrut) * 100 : 0;
          return {
            key: dateMonth.debut, // dateMonth.libelle
            value: pourcentageRemise,
          };
        }),
      );
  }

  return [];
};

export const monthInYear = (seletedYear: number) => {
  return [
    {
      libelle: 'JANVIER',
      debut: `${seletedYear}-01-01`,
      fin: `${seletedYear}-02-01`,
    },
    {
      libelle: 'FEVRIER',
      debut: `${seletedYear}-02-01`,
      fin: `${seletedYear}-03-01`,
    },
    {
      libelle: 'MARS',
      debut: `${seletedYear}-03-01`,
      fin: `${seletedYear}-04-01`,
    },
    {
      libelle: 'AVRIL',
      debut: `${seletedYear}-04-01`,
      fin: `${seletedYear}-05-01`,
    },
    {
      libelle: 'MAI',
      debut: `${seletedYear}-05-01`,
      fin: `${seletedYear}-06-01`,
    },
    {
      libelle: 'JUIN',
      debut: `${seletedYear}-06-01`,
      fin: `${seletedYear}-07-01`,
    },
    {
      libelle: 'JULLET',
      debut: `${seletedYear}-07-01`,
      fin: `${seletedYear}-08-01`,
    },
    {
      libelle: 'AOUT',
      debut: `${seletedYear}-08-01`,
      fin: `${seletedYear}-09-01`,
    },
    {
      libelle: 'SEPTEMBRE',
      debut: `${seletedYear}-09-01`,
      fin: `${seletedYear}-10-01`,
    },
    {
      libelle: 'OCTOBRE',
      debut: `${seletedYear}-10-01`,
      fin: `${seletedYear}-11-01`,
    },
    {
      libelle: 'NOVEMBRE',
      debut: `${seletedYear}-11-01`,
      fin: `${seletedYear}-12-01`,
    },
    {
      libelle: 'DECEMBRE',
      debut: `${seletedYear}-12-01`,
      fin: `${seletedYear + 1}-01-01`,
    },
  ];
};
