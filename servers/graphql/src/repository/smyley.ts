import { Context } from '../types';
import { publishIdeeOuBonnePratique } from './ideeOuBonnePratique';
import { getUserPpersonnelsByPharmacie } from './user';
import ArrayUniqueObject from 'unique-array-objects';

let source: any = null;
let id: string = null;

export const createSmyley = async ({ codeItem, idSource, idSmyley }, ctx: Context) => {
  _switchItemCode(codeItem, idSource, ctx, 'create');
  // Get Item id
  const idItem = await _getItemId(codeItem, ctx);
  const existUserSmyles = await ctx.prisma.userSmyleys({
    where: {
      item: { id: idItem },
      idSource,
      user: { id: ctx.userId },
      isRemoved: false,
      groupement: { id: ctx.groupementId },
    },
  });

  console.log('existUserSmyles ========> ', existUserSmyles);

  if (existUserSmyles && existUserSmyles.length > 0) {
    return ctx.prisma
      .userSmyley({ id: existUserSmyles[0].id })
      .smyley()
      .then(smyley => {
        if (smyley.id === (idSmyley as string)) {
          console.log('smyley @delete => ', smyley);
          // Delete (mode soft)
          return ctx.prisma
            .updateUserSmyley({
              where: { id: existUserSmyles[0].id },
              data: { smyley: { connect: { id: idSmyley } }, isRemoved: true },
            })
            .then(async deletedUserSmyley => {
              if (codeItem === 'PROD') {
                await ctx.indexing.publishSave('produitcanals', deletedUserSmyley.idSource);
              }
              if (codeItem === 'OPERACOM') {
                await ctx.indexing.publishSave('operations', deletedUserSmyley.idSource);
              }
              if (codeItem === 'IDEE_AND_BEST_PRACTICE') {
                await publishIdeeOuBonnePratique(ctx, deletedUserSmyley.idSource);
              }
              console.log('deletedUserSmyley => ', deletedUserSmyley);
              return deletedUserSmyley;
            });
        } else if (smyley.id !== (idSmyley as string)) {
          // Update
          console.log('smyley @update => ', smyley);
          return ctx.prisma
            .updateUserSmyley({
              where: { id: existUserSmyles[0].id },
              data: { smyley: { connect: { id: idSmyley } } },
            })
            .then(async updatedUserSmyley => {
              console.log('updatedUserSmyley => ', updatedUserSmyley);
              if (codeItem === 'PROD') {
                await ctx.indexing.publishSave('produitcanals', updatedUserSmyley.idSource);
              }
              if (codeItem === 'OPERACOM') {
                await ctx.indexing.publishSave('operations', updatedUserSmyley.idSource);
              }
              if (codeItem === 'IDEE_AND_BEST_PRACTICE') {
                await publishIdeeOuBonnePratique(ctx, updatedUserSmyley.idSource);
              }
              return updatedUserSmyley;
            });
        }
      });
  } else {
    // Create
    return ctx.prisma
      .createUserSmyley({
        item: { connect: { id: idItem } },
        idSource,
        user: { connect: { id: ctx.userId } },
        smyley: { connect: { id: idSmyley } },
        groupement: { connect: { id: ctx.groupementId } },
      })
      .then(async createdUserSmyley => {
        if (codeItem === 'PROD') {
          await ctx.indexing.publishSave('produitcanals', createdUserSmyley.idSource);
        }
        if (codeItem === 'OPERACOM') {
          await ctx.indexing.publishSave('operations', createdUserSmyley.idSource);
        }
        if (codeItem === 'IDEE_AND_BEST_PRACTICE') {
          await publishIdeeOuBonnePratique(ctx, createdUserSmyley.idSource);
        }
        return createdUserSmyley;
      });
  }
};

export const userSmyleysByItemAndSource = async ({ codeItem, idSource }, ctx: Context) => {
  const newIdSource = typeof idSource !== 'string' ? JSON.stringify(idSource) : idSource;
  return {
    total: await ctx.prisma
      .userSmyleysConnection({
        where: {
          item: { code: codeItem },
          idSource: newIdSource,
          isRemoved: false,
          groupement: { id: ctx.groupementId },
        },
      })
      .aggregate()
      .count(),
    data: await ctx.prisma.userSmyleys({
      where: {
        item: { code: codeItem },
        idSource: newIdSource,
        isRemoved: false,
        groupement: { id: ctx.groupementId },
      },
      orderBy: 'dateCreation_ASC',
    }),
  };
};

const _switchItemCode = async (
  codeItem: string,
  idSource: string,
  ctx: Context,
  action?: string,
) => {
  switch (codeItem) {
    // Produit
    case 'PROD':
      source = await ctx.prisma.produitCanal({
        id: idSource,
      });
      if (source && source.id) id = `${source.id}`;
      // Publis source
      if (action === 'create') ctx.indexing.publishSave('produitcanals', id);
      break;
    // Operation commercial
    case 'OPERACOM':
      source = await ctx.prisma.operation({ id: idSource });
      if (source && source.id) id = source.id;
      if (action === 'create') ctx.indexing.publishSave('operations', id);
      break;
    case 'IDEE_AND_BEST_PRACTICE':
      source = await ctx.prisma.ideeOuBonnePratique({ id: idSource });
      if (source && source.id) id = source.id;
      if (action === 'create') await publishIdeeOuBonnePratique(ctx, id);
      break;
    // TODO : case others items
    default:
      break;
  }
};

const _getItemId = (codeItem: string, ctx: Context) => {
  return ctx.prisma.item({ code: codeItem }).id();
};

export const countSmyley = async (
  ctx: Context,
  {
    code,
    note,
    idPharmacies,
    idsSourceAssocie,
    dateDebut,
    dateFin,
  }: {
    code: string;
    note?: number;
    idPharmacies: string[];
    idsSourceAssocie: string[];
    dateDebut?: string;
    dateFin?: string;
  },
) => {
  const userPpersonnels = await getUserPpersonnelsByPharmacie(ctx, idPharmacies);
  const whereIdUsers =
    userPpersonnels && userPpersonnels.length
      ? [{ id_in: userPpersonnels.map(userPpersonnel => userPpersonnel.id) }]
      : [];

  const defaultWhere = {
    item: { code },
    isRemoved: false,
    idSource_in: idsSourceAssocie,
    smyley: note ? { note } : { note_in: [1, 2, 4, 5, 6] },
    user: {
      OR: [
        {
          userTitulaires_some: { idPharmacie: { id_in: idPharmacies } },
        },
        ...whereIdUsers,
      ],
      AND: [
        {
          groupement: { id: ctx.groupementId },
        },
      ],
    },
  };
  const where =
    dateDebut && dateFin
      ? {
          ...defaultWhere,
          dateCreation_gte: dateDebut,
          dateCreation_lt: dateFin,
        }
      : defaultWhere;

  return ctx.prisma
    .userSmyleysConnection({
      where,
    })
    .aggregate()
    .count();
};

export const groupementSmyleys = async ({ idSource }, ctx: Context) => {
  return ctx.prisma
    .userSmyleys({
      where: {
        idSource,
        isRemoved: false,
      },
    })
    .then(async userSmyleys => {
      return userSmyleys && userSmyleys.length
        ? ArrayUniqueObject(
            await Promise.all(
              userSmyleys.map(userSmyley =>
                ctx.prisma.userSmyley({ id: userSmyley.id }).groupement(),
              ),
            ),
          )
        : [];
    });
};
