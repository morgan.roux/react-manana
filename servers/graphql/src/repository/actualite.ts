import { ApolloError } from 'apollo-server';
import moment from 'moment';
import { CODE_ACTUALITE } from '../constants/item';
import { AMBASSADEUR, SUPER_ADMINISTRATEUR } from '../constants/roles';
import { ActionCreateInput, ActionWhereInput, Actualite, User } from '../generated/prisma-client';
import {
  ActualiteCible,
  ActualiteCibleInput,
  ActualiteFileS3,
  ActualiteFormInput,
  ActualiteInput,
  ActualiteTodo,
} from '../interfaces/actualite';
import logger from '../logging';
import { computeDateEcheanceFromUrgence } from '../sequelize/shared/compute-date-echeance';
import s3 from '../services/s3';
import { WatchTime } from '../services/time';
import { Context, TypePresidentCible } from '../types';
import {
  deleteAllNotifications,
  saveAndPublishNotification,
  updateNotificationSeenByUser,
} from './notification';
import { presidentRegions } from './titulaire';
import { usersPharmacie } from './user';
import ArrayUnique = require('array-unique');

const watchTime = new WatchTime();
const now = moment().format();

const TYPE_PRESIDENT_CIBLE = ['PRESIDENT', 'PHARMACIE'];

export const deleteActualite = async (ctx: Context, { id }) => {
  return ctx.prisma.deleteActualite({ id });
};

export const upsertActualite = async (
  ctx: Context,
  {
    id,
    codeItem,
    codeOrigine,
    description,
    actionAuto,
    niveauPriorite,
    idLaboratoire,
    libelle,
    dateDebut,
    dateFin,
    fichierPresentations,
    globalite,
    fichierCible,
    pharmacieRoles,
    presidentRegions,
    services,
    laboratoires,
    partenaires,
    idProject,
    actionPriorite,
    actionDescription,
    actionDueDate,
    idImportance,
    idUrgence,
    idFonction,
    idTache,
  }: ActualiteFormInput,
) => {
  watchTime.start();

  const currentProjet = id ? await ctx.prisma.actualite({ id }).projet() : null;
  const currentFichierCible = id ? await ctx.prisma.actualite({ id }).fichierCible() : null;

  const actualiteTime = new WatchTime();
  actualiteTime.start();
  let actualite = null;
  try {
    actualite = await createUpdateOneActualite(ctx, {
      id,
      codeItem,
      codeOrigine,
      description,
      actionAuto,
      niveauPriorite,
      idLaboratoire,
      libelle,
      dateDebut,
      dateFin,
      globalite,
      idProject,
      fichierPresentations,
      fichierCible,
      idTache,
      idFonction,
      idUrgence,
      idImportance,
    });
  } catch (error) {
    logger.error(`Erreur lors de l'upser de l'actualité ====> ${JSON.stringify(error)}`);
    if (id) {
      return new ApolloError(
        "Erreur lors de la modification de l'actualite",
        'ACTUALITE_UPDATE_ERROR',
      );
    } else {
      return new ApolloError("Erreur lors de l'insertion de l'actualite", 'ACTUALITE_INSERT_ERROR');
    }
  }

  if (!actualite) {
    return new ApolloError("Erreur lors de l'insertion de l'actualite", 'ACTUALITE_INSERT_ERROR');
  }

  if (idProject) {
    createUpdateTodoActualite(ctx, {
      idActualite: actualite.id,
      actionDescription,
      actionDueDate,
      actionPriorite,
      idCurrentProjet: currentProjet ? currentProjet.id : null,
      libelle,
      idNewProject: idProject,
    });
  }

  actualiteTime.stop();
  watchTime.printElapsed(`temps mise pour la création de l'actualite ${actualite.libelle} ====> `);

  try {
    createUpdateActualiteCible(
      ctx,
      {
        idActualite: actualite.id,
        globalite,
        newFichierCible: fichierCible,
        currentFichierCible,
        pharmacieRoles,
        presidentRegions,
        services,
        laboratoires,
        partenaires,
      },
      id ? true : false,
    ).then(async cibles => {
      await publishActualite(ctx, { id: actualite.id });
      return cibles;
    });
  } catch (errors) {
    logger.error(`Erreur lors de l'insertion des actualités cible ====> ${JSON.stringify(errors)}`);
    if (actualite && actualite.id && (!id || actualite.id !== id)) {
      await deleteActualite(ctx, { id: actualite.id });
    }
    return new ApolloError(
      "Erreur lors de l'insertion des actualités cible",
      'ACTUALITE_CIBLE_INSERT_ERROR',
    );
  }

  watchTime.stop();
  watchTime.printElapsed(`[Actualite Repository] Function upsertActualite duration : `);

  return ctx.prisma.actualite({ id: actualite.id }).then(async actu => {
    // Publish to elasticsearch
    await publishActualite(ctx, { id: actualite.id });
    return actu;
  });
};

export const createUpdateOneActualite = async (ctx: Context, actualiteInput: ActualiteInput) => {
  const {
    id,
    codeItem,
    codeOrigine,
    description,
    actionAuto,
    niveauPriorite,
    idLaboratoire,
    libelle,
    dateDebut,
    dateFin,
    globalite,
    idProject,
    fichierPresentations,
    fichierCible,
    idImportance,
    idFonction,
    idTache,
    idUrgence,
  } = actualiteInput;

  const urgenceCode = await ctx.prisma.urgence({ id: idUrgence }).code();
  const dateEcheance = await computeDateEcheanceFromUrgence(urgenceCode as any);

  const data = {
    idItem: { connect: { code: codeItem } },
    idActualiteOrigine: { connect: { code: codeOrigine } },
    groupement: { connect: { id: ctx.groupementId } },
    description,
    actionAuto,
    niveauPriorite: niveauPriorite ? niveauPriorite : null,
    laboratoire: idLaboratoire ? { connect: { id: idLaboratoire } } : null,
    libelle,
    dateDebut,
    dateFin,
    globalite,
    projet: idProject ? { connect: { id: idProject } } : null,
    urgence: idUrgence ? { connect: { id: idUrgence } } : null,
    importance: idImportance ? { connect: { id: idImportance } } : null,
    idFonction,
    idTache,
    dateEcheance: moment(dateEcheance).format(),
  };

  let usersSeen: User[] = [];
  if (id) {
    usersSeen = await ctx.prisma.actualite({ id }).user();
  }

  const allFichierPresentations = id
    ? await ctx.prisma.actualite({ id }).fichierPresentations()
    : [];

  const filesToSave =
    allFichierPresentations && allFichierPresentations.length
      ? fichierPresentations.filter(
          file => !allFichierPresentations.map(all => all.chemin).includes(file.chemin),
        )
      : [...fichierPresentations];

  const filesToDelete =
    allFichierPresentations && allFichierPresentations.length
      ? allFichierPresentations.filter(
          file => !fichierPresentations.map(all => all.chemin).includes(file.chemin),
        )
      : [];

  return ctx.prisma.upsertActualite({
    where: { id: id ? id : '' },
    create: {
      ...data,
      fichierPresentations: {
        create: filesToSave.map(fichier => {
          return {
            chemin: fichier.chemin,
            nomOriginal: fichier.nomOriginal,
            type: fichier.type,
          };
        }),
      },
      fichierCible:
        fichierCible && fichierCible.chemin
          ? {
              create: {
                chemin: fichierCible.chemin,
                nomOriginal: fichierCible.nomOriginal,
                type: fichierCible.type,
              },
            }
          : null,
    },
    update: {
      ...data,
      fichierPresentations: {
        deleteMany: filesToDelete.map(file => {
          return { id: file.id };
        }),
        create: filesToSave.map(fichier => {
          return {
            chemin: fichier.chemin,
            nomOriginal: fichier.nomOriginal,
            type: fichier.type,
          };
        }),
      },
      fichierCible: {
        update:
          fichierCible && fichierCible.chemin
            ? {
                chemin: fichierCible.chemin,
                nomOriginal: fichierCible.nomOriginal,
                type: fichierCible.type,
              }
            : null,
      },
      user:
        usersSeen && usersSeen.length
          ? {
              disconnect: usersSeen.map(user => {
                return { id: user.id };
              }),
            }
          : null,
    },
  });
};

export const createUpdateTodoActualite = async (
  ctx: Context,
  {
    idActualite,
    idNewProject,
    idCurrentProjet,
    actionPriorite,
    libelle,
    actionDescription,
    actionDueDate,
  }: ActualiteTodo,
) => {
  const dataCreate: ActionCreateInput = {
    project: { connect: { id: idNewProject } },
    userCreation: { connect: { id: ctx.userId } },
    userModification: { connect: { id: ctx.userId } },
    item: { connect: { code: CODE_ACTUALITE } },
    idItemAssocie: idActualite,
    description: actionDescription,
    dateDebut: actionDueDate ? actionDueDate : null,
  };

  const whereAction: ActionWhereInput = {
    project: { id: idCurrentProjet },
    idItemAssocie: idActualite,
    item: { code: CODE_ACTUALITE },
  };

  if (idCurrentProjet !== idNewProject) {
    await ctx.prisma.deleteManyActions(whereAction);

    return ctx.prisma.createAction(dataCreate).then(async result => {
      await ctx.indexing.publishSave('projects', idNewProject);
      return result;
    });
  }

  const action = await ctx.prisma
    .actions({
      where: whereAction,
      first: 1,
    })
    .then(actions => (actions && actions.length ? actions[0] : null));

  const upsert = await ctx.prisma.upsertAction({
    where: { id: action && action.id ? action.id : '' },
    create: dataCreate,
    update: {
      project: { connect: { id: idNewProject } },
      description: actionDescription,
      dateDebut: actionDueDate ? actionDueDate : null,
    },
  });
  await ctx.indexing.publishSave('projects', idNewProject);

  return upsert;
};

export const createUpdateActualiteCible = async (
  ctx: Context,
  {
    idActualite,
    globalite,
    newFichierCible,
    currentFichierCible, // => OLD FICHIER CIBLE
    pharmacieRoles,
    presidentRegions,
    services,
    laboratoires,
    partenaires,
  }: ActualiteCibleInput,
  isUpdate: boolean = false,
) => {
  watchTime.start();

  const actualite = await ctx.prisma.actualite({ id: idActualite });
  logger.info(`[CIBLE UPDATE OR CREATE] ====> ${isUpdate}`);

  logger.info(`globalite ====> ${globalite}`);
  if (actualite.globalite === false) {
    try {
      /*
        SAVE DATA FROM FILE CIBLE
       */
      if (
        (!currentFichierCible && newFichierCible) ||
        (currentFichierCible && newFichierCible && currentFichierCible.id !== newFichierCible.id)
      ) {
        if (currentFichierCible) {
          await _deleteAllCibles(ctx, { idActualite });
          await ctx.prisma.deleteFichier({ id: currentFichierCible.id });
        }
        // begin read fichierCible and save to database
        let cibles: ActualiteFileS3;
        try {
          logger.info(`READ FILE FROM S3`);
          cibles = await readActualiteFichierCibleFromS3({
            path: newFichierCible.chemin,
          });
        } catch (error) {
          logger.error(`Erreur lors de la lecture du fichier cible ====> ${error}`);
          return new ApolloError(
            'Erreur lors de la lecture du fichier cible',
            'ACTUALITE_READ_FILE_ERROR',
          );
        }

        if (cibles) {
          await _setActualiteCible(ctx, {
            idActualite,
            pharmacieRoles: cibles && cibles.pharmacieRoles ? cibles.pharmacieRoles : null,
            presidentRegions: cibles && cibles.presidents ? cibles.presidents : null,
            services: cibles && cibles.services ? cibles.services : [],
            laboratoires: cibles && cibles.laboratoires ? cibles.laboratoires : [],
            partenaires: cibles && cibles.partenaires ? cibles.partenaires : [],
          });
        }
      } else if (!newFichierCible) {
        await _setActualiteCible(ctx, {
          idActualite,
          pharmacieRoles,
          presidentRegions,
          services,
          laboratoires,
          partenaires,
        });
      }

      if (isUpdate) await resetSeenActualite(ctx, idActualite);
      sendNotificationActualiteToCible(
        ctx,
        actualite,
        {
          pharmacieRoles,
          presidentRegions,
          codeServices: services,
          idLaboratoires: laboratoires,
          idPartenaires: partenaires,
        },
        true,
      );
    } catch (errors) {
      logger.error(
        `Erreur lors de l'insertion des actualités cible ====> ${JSON.stringify(errors)}`,
      );
      return new ApolloError(
        "Erreur lors de l'insertion des actualités cible",
        'ACTUALITE_CIBLE_INSERT_ERROR',
      );
    }
  } else {
    /* GLOBALITE TRUE DELETE ALL CIBLE IF EXIST FOR THE ACTU */
    logger.info(`*********************** Delete All cibles *********************************`);
    await _deleteAllCibles(ctx, { idActualite });
    sendNotificationActualite(ctx, idActualite, isUpdate);
  }

  try {
    return ctx.prisma.actualite({
      id: idActualite,
    });
  } catch (errors) {
    logger.error(`Erreur lors de l'insertion de la globalité ====> ${JSON.stringify(errors)}`);
    return new ApolloError(
      "Erreur lors de l'insertion de l'actualite cible",
      'ACTUALITE_CIBLE_INSERT_ERROR',
    );
  }
};

const _setActualiteCible = async (
  ctx: Context,
  {
    idActualite,
    pharmacieRoles,
    presidentRegions,
    services,
    laboratoires,
    partenaires,
  }: ActualiteCible,
) => {
  watchTime.start();
  let serviceConnects = [];
  let pharmacieConnects = [];
  let pharmacieRoleConnects = [];
  let partenaireConnects = [];
  let laboratoireConnects = [];
  let presidentConnects = [];
  let typePresident: TypePresidentCible = null;

  if (services && services.length) {
    serviceConnects = services.map(code => {
      return {
        code,
      };
    });
  }
  if (laboratoires && laboratoires.length) {
    laboratoireConnects = laboratoires.map(id => {
      return {
        id,
      };
    });
  }
  if (partenaires && partenaires.length) {
    partenaireConnects = partenaires.map(id => {
      return {
        id,
      };
    });
  }
  if (pharmacieRoles) {
    if (pharmacieRoles.roles && pharmacieRoles.roles.length) {
      pharmacieRoleConnects = pharmacieRoles.roles.map(code => {
        return {
          code,
        };
      });
    }
    if (pharmacieRoles.pharmacies && pharmacieRoles.pharmacies.length) {
      pharmacieConnects = pharmacieRoles.pharmacies.map(id => {
        return {
          id,
        };
      });
    }
  }

  if (
    presidentRegions &&
    presidentRegions.type &&
    presidentRegions.presidents &&
    presidentRegions.presidents.length
  ) {
    typePresident = presidentRegions.type;
    presidentConnects = presidentRegions.presidents.map(id => {
      return {
        id,
      };
    });
  }

  return ctx.prisma.updateActualite({
    where: { id: idActualite },
    data: {
      typePresidentCible: typePresident as any,
      serviceCibles: {
        set: serviceConnects.length ? serviceConnects : null,
      },
      partenaireCibles: {
        set: partenaireConnects.length ? partenaireConnects : null,
      },
      rolePharmacieCibles: {
        set: pharmacieRoleConnects.length ? pharmacieRoleConnects : null,
      },
      laboratoireCibles: {
        set: laboratoireConnects.length ? laboratoireConnects : null,
      },
      pharmacieCibles: {
        set: pharmacieConnects.length ? pharmacieConnects : null,
      },
      presidentCibles: {
        set: presidentConnects.length ? presidentConnects : null,
      },
    },
  });
};

const _deleteAllCibles = async (ctx: Context, { idActualite }) => {
  return ctx.prisma.updateActualite({
    where: { id: idActualite },
    data: {
      typePresidentCible: null,
      serviceCibles: {
        set: null,
      },
      partenaireCibles: {
        set: null,
      },
      rolePharmacieCibles: {
        set: null,
      },
      laboratoireCibles: {
        set: null,
      },
      pharmacieCibles: {
        set: null,
      },
      presidentCibles: {
        set: null,
      },
    },
  });
};

export const countActualitesCibles = async (ctx: Context) => {
  return [
    {
      code: 'COLLGRP',
      name: 'Collaborateurs Groupement',
      total: await ctx.prisma
        .servicesConnection()
        .aggregate()
        .count(),
    },
    {
      code: 'PHARMA',
      name: 'Pharmacie',
      total: await ctx.prisma
        .pharmaciesConnection({ where: { groupement: { id: ctx.groupementId } } })
        .aggregate()
        .count(),
    },
    {
      code: 'PDR',
      name: 'Président de région',
      total: await _countPresidentRegions(ctx),
    },
    {
      code: 'LABO',
      name: 'Laboratoire',
      total: await ctx.prisma
        .laboratoiresConnection()
        .aggregate()
        .count(),
    },
    {
      code: 'PARTE',
      name: 'Partenaire',
      total: await ctx.prisma
        .partenairesConnection({ where: { groupement: { id: ctx.groupementId } } })
        .aggregate()
        .count(),
    },
  ];
};

export const readActualiteFichierCibleFromS3 = async ({ path }): Promise<ActualiteFileS3> => {
  watchTime.start();
  const codeServices: string[] = [];
  const idPresidents: string[] = [];
  const idPharmacies: string[] = [];
  const codeRolePharmacies: string[] = [];
  const idLaboratoires: string[] = [];
  const idPartenaires: string[] = [];
  let type: TypePresidentCible = TypePresidentCible.PRESIDENT;
  // read fichierCible
  try {
    return s3.readXlsxFromAWS(path).then(async fileData => {
      logger.info(`data from file: ${path} *********** ${JSON.stringify(fileData)}`);

      if (!fileData || (fileData && !fileData.length)) return null;

      return Promise.all(
        fileData.map(async sheet => {
          return Promise.all(
            sheet.data.map(async data => {
              logger.info(
                `***************** ${JSON.stringify({
                  name: sheet.name,
                  type: typeof data[0],
                  data: data[0],
                })} **********************`,
              );

              switch (sheet.name) {
                case 'services':
                  if (data[0]) codeServices.push(String(data[0]));
                  break;

                case 'presidents':
                  if (data[0]) idPresidents.push(String(data[0]));
                  if (data[1]) {
                    type =
                      String(data[1]) === 'PRESIDENT'
                        ? TypePresidentCible.PRESIDENT
                        : TypePresidentCible.PHARMACIE;
                  }
                  break;

                case 'pharmacies':
                  if (data[0]) idPharmacies.push(String(data[0]));
                  if (data[1]) codeRolePharmacies.push(String(data[0]));
                  break;

                case 'laboratoires':
                  if (data[0]) idLaboratoires.push(String(data[0]));
                  break;

                case 'partenaires':
                  if (data[0]) idPartenaires.push(String(data[0]));
                  break;
              }
            }),
          );
        }),
      ).then(_ => {
        if (
          [
            ...codeServices,
            ...idPresidents,
            ...idLaboratoires,
            ...idPartenaires,
            ...idPharmacies,
            ...codeRolePharmacies,
          ].length
        ) {
          const presidents =
            type && TYPE_PRESIDENT_CIBLE.includes(type as any)
              ? { type, presidents: idPresidents }
              : null;

          const pharmacieRoles =
            idPharmacies && idPharmacies.length && codeRolePharmacies && codeRolePharmacies.length
              ? { pharmacies: idPharmacies, roles: codeRolePharmacies }
              : null;

          watchTime.stop();
          watchTime.printElapsed(
            `[Actualite Repository] Function readActualiteFichierCibleFromS3 duration : `,
          );

          return {
            services: codeServices,
            presidents,
            pharmacieRoles,
            laboratoires: idLaboratoires,
            partenaires: idPartenaires,
          };
        }
        return {};
      });
    });
  } catch (error) {
    logger.error(`Erreur lors de la Lecture du fichier cibles ===> ${error}`);
    return null;
  }
  // end read
};

/* export const getCibles = async (ctx: Context, parent: Actualite, { table, column }) => {
  watchTime.start();
  return ctx.prisma[`${table}s`]({
    where: { idActualite: { id: parent['_id'] || parent.id } },
  }).then(async cibles =>
    cibles && cibles.length
      ? Promise.all(cibles.map(cible => ctx.prisma[table]({ id: cible.id })[column]())).then(
          result => {
            watchTime.stop();
            watchTime.printElapsed(`[Actualite Repository] Function getCibles duration : `);
            return result;
          },
        )
      : null,
  );
}; */

const _countPresidentRegions = async (ctx: Context) => {
  const affectations = await ctx.prisma
    .titulaireAffectations({
      where: {
        titulaire: {
          groupement: { id: ctx.groupementId },
        },
        dateDebut_lte: now,
        dateFin_gte: now,
      },
    })
    .then(presidents => presidents.map(president => president.id) as any[]);

  const idPresidents = await affectations.reduce(async (result, id) => {
    const titulaire = await ctx.prisma.titulaireAffectation({ id }).titulaire();
    const ids = await result;
    if (titulaire && titulaire.id && !ids.includes(titulaire.id)) return [...ids, titulaire.id];
    return [...ids];
  }, []);

  watchTime.stop();
  watchTime.printElapsed(`[Actualite Repository] Function _countPresidentRegions duration : `);

  return idPresidents && idPresidents.length ? +idPresidents.length : 0;
};

export const actualiteAllUsersCible = async (
  ctx: Context,
  {
    idActualite,
    codeServices,
    idPartenaires,
    idLaboratoires,
    pharmacieRoles,
    presidentRegions,
    fichierCible,
  },
) => {
  watchTime.start();
  let users = [];
  if (idActualite) {
    // Cible from ID Actualite
    const userServices = await getUserFromServiceCible(ctx, {
      idActualite,
    });
    const userPartenaires = await getUserFromPartenaireCible(ctx, {
      idActualite,
    });
    const userLabos = await getUserFromLaboratoireCible(ctx, {
      idActualite,
    });
    const userPharmacies = await getUserFromPharmacieCible(ctx, { idActualite });
    const userPresidents = await getUserFromPresidentCible(ctx, { idActualite });

    return _distinctUserValues([
      ...userServices,
      ...userPartenaires,
      ...userLabos,
      ...userPharmacies,
      ...userPresidents,
    ]);
  } else if (fichierCible && fichierCible.chemin) {
    const cibles = await readActualiteFichierCibleFromS3({
      path: fichierCible.chemin,
    });

    users = await usersFromCible(ctx, {
      codeServices: cibles && cibles.services ? cibles.services : [],
      idPartenaires: cibles && cibles.partenaires ? cibles.partenaires : [],
      idLaboratoires: cibles && cibles.laboratoires ? cibles.laboratoires : [],
      pharmacieRoles:
        cibles &&
        cibles.pharmacieRoles &&
        cibles.pharmacieRoles.pharmacies &&
        cibles.pharmacieRoles.pharmacies.length &&
        cibles.pharmacieRoles.roles &&
        cibles.pharmacieRoles.roles.length
          ? cibles.pharmacieRoles
          : null,
      presidentRegions:
        cibles &&
        cibles.presidents &&
        cibles.presidents.type &&
        cibles.presidents.presidents &&
        cibles.presidents.presidents.length
          ? cibles.presidents
          : null,
    });
  } else {
    users = await usersFromCible(ctx, {
      codeServices,
      idPartenaires,
      idLaboratoires,
      pharmacieRoles,
      presidentRegions,
    });
  }

  watchTime.stop();
  watchTime.printElapsed(`[Actualite Repository] Function actualiteAllUsersCible duration : `);
  return _distinctUserValues(users);
};

const _distinctUserValues = (users: User[]) => {
  watchTime.start();
  if (users && users.length) {
    const userIds = [];
    const list = users
      .map(user => {
        if (user && user.id && !userIds.includes(user.id)) {
          userIds.push(user.id);
          return user;
        }
      })
      .filter(user => user);

    watchTime.stop();
    watchTime.printElapsed(`[Actualite Repository] Function _distinctUserValues duration : `);

    return list.sort((a, b) => (a.userName > b.userName ? 1 : -1));
  }

  return [];
};

export const usersFromCible = async (
  ctx: Context,
  { codeServices, idPartenaires, idLaboratoires, pharmacieRoles, presidentRegions },
) => {
  watchTime.start();
  const userServices =
    codeServices && codeServices.length ? await getUserFromServices(ctx, codeServices) : [];

  const userPartenaires =
    idPartenaires && idPartenaires.length ? await getUserFromPartenaires(ctx, idPartenaires) : [];

  const userLabos =
    idLaboratoires && idLaboratoires.length
      ? await getUserFromLaboratoires(
          ctx,
          idLaboratoires.map(id => parseInt(id, 10)).filter(item => item),
        )
      : [];

  const userPharmacies =
    pharmacieRoles &&
    pharmacieRoles.pharmacies &&
    pharmacieRoles.pharmacies.length &&
    pharmacieRoles.roles &&
    pharmacieRoles.roles.length
      ? await _getUserPharmacie(
          ctx,
          pharmacieRoles.pharmacies.filter(item => item),
          pharmacieRoles.roles,
        )
      : [];

  // const userRoles = codeRoles && codeRoles.length ? await getUserFromRoles(ctx, codeRoles) : [];

  const userPresidents =
    presidentRegions &&
    presidentRegions.presidents &&
    presidentRegions.presidents.length &&
    presidentRegions.type
      ? await getUserFromPresidents(
          ctx,
          presidentRegions.presidents.map(id => parseInt(id, 10)).filter(item => item),
          presidentRegions.type,
        )
      : [];

  ctx.prisma.users({
    where: {},
  });

  watchTime.stop();
  watchTime.printElapsed(`[Actualite Repository] Function usersFromCible duration : `);
  return [...userServices, ...userPartenaires, ...userLabos, ...userPharmacies, ...userPresidents];
};

const getUserFromServiceCible = async (ctx: Context, { idActualite }) => {
  const services = await ctx.prisma
    .actualite({
      id: idActualite,
    })
    .serviceCibles();

  return services && services.length
    ? getUserFromServices(
        ctx,
        services.map(service => service.code),
      )
    : [];
};

const getUserFromServices = async (ctx: Context, codeServices: any[]) => {
  watchTime.start();
  const personnels =
    codeServices && codeServices.length
      ? await ctx.prisma.personnels({
          where: { service: { code_in: codeServices }, groupement: { id: ctx.groupementId } },
        })
      : [];

  return personnels && personnels.length
    ? Promise.all(
        personnels.map(personnel => ctx.prisma.personnel({ id: personnel.id }).user()),
      ).then(result => {
        watchTime.stop();
        watchTime.printElapsed(`[Actualite Repository] Function getUserFromServices duration : `);
        return result;
      })
    : [];
};

const getUserFromCibles = async (
  ctx: Context,
  { idActualite, entityCible, entity },
  relation: boolean = false,
) => {
  const cibles = await ctx.prisma.actualite({ id: idActualite })[entityCible]();

  if (cibles && cibles.length) {
    return Promise.all(cibles.map(cible => ctx.prisma[entity]({ id: cible.id }).user()));
  }

  return [];
};
const getUserFromPartenaireCible = async (ctx: Context, { idActualite }) => {
  return getUserFromCibles(ctx, {
    idActualite,
    entity: 'partenaires',
    entityCible: 'partenaireCibles',
  });
};

const getUserFromPartenaires = async (ctx: Context, idPartenaires: any[]) => {
  return _getUserFromEntity(ctx, idPartenaires, 'partenaire');
};

const getUserFromLaboratoireCible = async (ctx: Context, { idActualite }) => {
  return getUserFromCibles(ctx, {
    idActualite,
    entity: 'laboratoires',
    entityCible: 'laboratoireCibles',
  });
};

const getUserFromLaboratoires = async (ctx: Context, idLaboratoires: any[]) => {
  return _getUserFromEntity(ctx, idLaboratoires, 'laboratoire');
};

const _getUserFromEntity = async (ctx: Context, idItems: any[], entity: string) => {
  const items =
    idItems && idItems.length ? await ctx.prisma[`${entity}s`]({ where: { id_in: idItems } }) : [];

  return items && items.length
    ? Promise.all(items.map(item => ctx.prisma[entity]({ id: item.id }).user()))
    : [];
};

const getUserFromPharmacieCible = async (ctx: Context, { idActualite }) => {
  const pharmacies = await ctx.prisma.actualite({ id: idActualite }).pharmacieCibles();
  const roles = await ctx.prisma.actualite({ id: idActualite }).rolePharmacieCibles();

  return pharmacies && pharmacies.length && roles && roles.length
    ? _getUserPharmacie(
        ctx,
        pharmacies.map(pharma => pharma.id),
        roles.map(role => role.code),
      )
    : [];
};

// pharmacies.map(pharma => pharma.idPharmacie)
const _getUserPharmacie = async (ctx: Context, idPharmacies: any[], codeRoles?: any[]) => {
  const whereRole =
    codeRoles && codeRoles.length
      ? {
          groupement: { id: ctx.groupementId },
          userRoles_some: {
            idRole: {
              code_in: codeRoles,
            },
          },
        }
      : {
          groupement: { id: ctx.groupementId },
        };

  const userTitulaires =
    idPharmacies && idPharmacies.length
      ? await ctx.prisma.userTitulaires({
          where: {
            idPharmacie: { id_in: idPharmacies },
            idUser: whereRole,
          },
        })
      : [];

  const wherePerso = {
    idPharmacie: { id_in: idPharmacies },
    user: whereRole,
  };

  const userPpersonnels =
    idPharmacies && idPharmacies.length
      ? await ctx.prisma.ppersonnels({
          where:
            codeRoles && codeRoles.length && codeRoles.filter(code => code === AMBASSADEUR).length
              ? { ...wherePerso, estAmbassadrice: true }
              : wherePerso,
        })
      : [];

  return [
    ...(userTitulaires && userTitulaires.length
      ? await Promise.all(
          userTitulaires.map(userPharmacie =>
            ctx.prisma.userTitulaire({ id: userPharmacie.id }).idUser(),
          ),
        )
      : []),
    ...(userPpersonnels && userPpersonnels.length
      ? await Promise.all(
          userPpersonnels.map(userPpersonnel =>
            ctx.prisma.ppersonnel({ id: userPpersonnel.id }).user(),
          ),
        )
      : []),
  ];
};

const getUserFromPresidentCible = async (ctx: Context, { idActualite }) => {
  const users: User[] = [];
  const type = await ctx.prisma
    .actualite({
      id: idActualite,
    })
    .typePresidentCible();

  if (type) {
    const presidentCibles = await ctx.prisma
      .actualite({
        id: idActualite,
      })
      .presidentCibles();

    return presidentCibles && presidentCibles.length
      ? getUserFromPresidents(ctx, presidentCibles, type)
      : [];
  }

  return users;
};

const getUserFromPresidents = async (ctx: Context, idPresidents: any[], type: string) => {
  if (type === 'PRESIDENT') {
    const presidents = await presidentRegions(ctx);

    const userPresidents =
      presidents && presidents.length
        ? await ctx.prisma.userTitulaires({
            where: {
              idTitulaire: {
                id_in: ArrayUnique(presidents.map(president => president.id)),
              },
              idUser: { groupement: { id: ctx.groupementId } },
            },
          })
        : [];

    return userPresidents && userPresidents.length
      ? Promise.all(
          userPresidents.map(userPresident =>
            ctx.prisma.userTitulaire({ id: userPresident.id }).idUser(),
          ),
        )
      : [];
  } else if (type === 'PHARMACIE') {
    const affectations =
      idPresidents && idPresidents.length
        ? await ctx.prisma.titulaireAffectations({
            where: {
              titulaire: {
                id_in: idPresidents,
              },
              dateDebut_lte: now,
              dateFin_gte: now,
            },
          })
        : [];

    const departements =
      affectations && affectations
        ? await Promise.all(
            affectations.map(affectation =>
              ctx.prisma.titulaireAffectation({ id: affectation.id }).departement(),
            ),
          )
        : [];

    const pharmacies =
      departements && departements.length
        ? await ctx.prisma.pharmacies({
            where: {
              departement: { id_in: departements.map(dep => dep.id) },
            },
          })
        : [];

    return pharmacies && pharmacies.length
      ? _getUserPharmacie(
          ctx,
          pharmacies.map(pharma => pharma.id),
        )
      : [];
  }

  return [];
};

export const sendNotificationActualite = async (
  ctx: Context,
  id: string,
  isUpdate: boolean = false,
) => {
  watchTime.start();
  const actualite = await ctx.prisma.actualite({ id });

  if (!actualite) {
    logger.info(`il n'y a pas d'actualité`);
    return false;
  }

  const message = getNewActualiteMessage(actualite.libelle);

  if (isUpdate) {
    await resetSeenActualite(ctx, actualite.id);
  }

  if (actualite.globalite === true) {
    return saveAndPublishNotification(
      {
        type: 'ACTUALITE_ADDED',
        message,
        from: ctx.userId,
        toAllUsers: true,
        targetId: actualite.id,
        targetName: actualite.libelle,
      },
      ctx,
    ).then(async notifications => {
      logger.info(`if notification ${JSON.stringify(notifications)}`);

      watchTime.stop();
      watchTime.printElapsed(
        `[Actualite Repository] Function sendNotificationActualiteToCible duration : `,
      );

      return notifications && notifications.length ? true : false;
    });
  } else {
    return sendNotificationActualiteToCible(ctx, actualite, {
      presidentRegions: null,
      codeServices: null,
      idLaboratoires: null,
      idPartenaires: null,
      pharmacieRoles: null,
    });
  }
};

export const resetSeenActualite = async (ctx: Context, id: string) => {
  await deleteAllNotifications(ctx, id);
  await ctx.prisma.deleteManyActualitePharmacieSeens({
    idActualite: { id },
  });

  const usersSeenActu = await ctx.prisma.actualite({ id }).user();

  if (usersSeenActu && usersSeenActu.length) {
    await ctx.prisma.updateActualite({
      where: { id },
      data: {
        user: {
          disconnect: usersSeenActu.map(user => {
            return { id: user.id };
          }),
        },
      },
    });
  }
};

export const sendNotificationActualiteToCible = async (
  ctx: Context,
  actualite: Actualite,
  { presidentRegions, codeServices, idLaboratoires, idPartenaires, pharmacieRoles },
  isCible: boolean = false,
) => {
  const message = getNewActualiteMessage(actualite.libelle);
  const usersCible = isCible
    ? await actualiteAllUsersCible(ctx, {
        idActualite: null,
        presidentRegions,
        codeServices,
        fichierCible: null,
        idLaboratoires,
        idPartenaires,
        pharmacieRoles,
      })
    : await actualiteAllUsersCible(ctx, {
        idActualite: actualite.id,
        presidentRegions: null,
        codeServices: null,
        fichierCible: null,
        idLaboratoires: null,
        idPartenaires: null,
        pharmacieRoles: null,
      });
  if (usersCible && usersCible.length) {
    return saveAndPublishNotification(
      {
        type: 'ACTUALITE_ADDED',
        message,
        someUsers: usersCible.map(user => user.id),
        from: ctx.userId,
        targetId: actualite.id,
        targetName: actualite.libelle,
      },
      ctx,
    ).then(async notifiactions => {
      logger.info(`else if notification ${JSON.stringify(notifiactions)}`);

      watchTime.stop();
      watchTime.printElapsed(`[Actualite Repository] Function duration : `);

      return notifiactions && notifiactions.length ? true : false;
    });
  }

  return saveAndPublishNotification(
    {
      type: 'ACTUALITE_ADDED',
      message,
      to: ctx.userId,
      from: ctx.userId,
      targetId: actualite.id,
      targetName: actualite.libelle,
    },
    ctx,
  ).then(async notifiactions => {
    logger.info(`else notification ${JSON.stringify(notifiactions)}`);

    watchTime.stop();
    watchTime.printElapsed(`[Actualite Repository] Function duration : `);

    return notifiactions && notifiactions.length ? true : false;
  });
};

export const countActualiteFromSearch = async (ctx: Context, { query }) => {
  watchTime.start();
  const search = {
    body: {
      query,
    },
    index: ['actualite'],
  };

  logger.info(`----------------- ${JSON.stringify(search)} `);

  const result = await ctx.indexing
    .getElasticSearchClient()
    .search(search)
    .then(result => {
      return {
        total: result.body.hits.total.value ? parseInt(result.body.hits.total.value, 10) : 0,
      };
    });

  watchTime.stop();
  watchTime.printElapsed(`[Actualite Repository] Function countActualiteFromSearch duration : `);

  return result && result.total ? result.total : 0;
};

export const markAsSeenActualite = async (ctx: Context, { id }) => {
  watchTime.start();
  const usersSeen = await ctx.prisma.actualite({ id }).user({ where: { id: ctx.userId } });

  logger.info(` BEGIN Mark seen actu :: id : ${id}`);

  if (!usersSeen || (usersSeen && !usersSeen.length)) {
    logger.info(` Mark seen actu :: ${id}`);
    await ctx.prisma.updateActualite({
      where: { id },
      data: { user: { connect: { id: ctx.userId } } },
    });

    logger.info(` Notified Actu ${id} `);
    updateNotificationSeenByUser(ctx, null, id, true);
  }

  await ctx.indexing.publishSave('actualites', id);

  watchTime.stop();
  watchTime.printElapsed(`[Actualite Repository] Function markAsSeenActualite duration : `);

  return ctx.prisma.actualite({ id });
};

export const markAsNotSeenActualite = async (ctx: Context, { id, idPharmacie }) => {
  watchTime.start();
  logger.info(` BEGIN Mark as not seen Actu :: id : ${id}`);
  const users = idPharmacie ? await usersPharmacie(ctx, { idPharmacie }) : [];
  if (users && users.length) {
    logger.info(
      `users pharmacie ${idPharmacie} ===========> ${JSON.stringify(users.map(user => user.id))}`,
    );
  }
  const usersSeen =
    users && users.length
      ? await ctx.prisma.actualite({ id }).user({ where: { id_in: users.map(user => user.id) } })
      : await ctx.prisma.actualite({ id }).user({ where: { id: ctx.userId } });
  if (usersSeen && usersSeen.length) {
    logger.info(`user seen ===========> ${JSON.stringify(usersSeen.map(user => user.id))}`);
  }

  if (usersSeen && usersSeen.length) {
    await Promise.all(
      usersSeen.map(user =>
        ctx.prisma.updateActualite({
          where: { id },
          data: { user: { disconnect: { id: user.id } } },
        }),
      ),
    );

    logger.info(` Notified Actu ${id} `);
    updateNotificationSeenByUser(ctx, usersSeen, id, false);
  }

  if (idPharmacie) {
    logger.info(`[ACTUALITE PHARMACIE] DELETE pahrmacie :: ${idPharmacie} , actualite :: ${id}`);
    await ctx.prisma.deleteManyActualitePharmacieSeens({
      idActualite: { id },
      idPharmacie: { id: idPharmacie },
    });
  }

  await ctx.indexing.publishSave('actualites', id);

  watchTime.stop();
  watchTime.printElapsed(`[Actualite Repository] Function markAsNotSeenActualite duration : `);

  return ctx.prisma.actualite({ id });
};

export const actualiteSeenByPharmacie = async (ctx: Context, { id, idPharmacie }) => {
  watchTime.start();
  const actualitePharmacieSeens = await ctx.prisma.actualitePharmacieSeens({
    where: { idActualite: { id }, idPharmacie: { id: idPharmacie } },
  });
  if (!actualitePharmacieSeens || (actualitePharmacieSeens && !actualitePharmacieSeens.length)) {
    await ctx.prisma.createActualitePharmacieSeen({
      idActualite: { connect: { id } },
      idPharmacie: { connect: { id: idPharmacie } },
    });
  }
  return ctx.prisma.actualite({ id }).then(async actualite => {
    await ctx.indexing.publishSave('actualites', id);

    watchTime.stop();
    watchTime.printElapsed(`[Actualite Repository] Function actualiteSeenByPharmacie duration : `);

    return actualite;
  });
};

export const typePresidentCible = async (ctx: Context, { id }) => {
  return ctx.prisma.actualite({ id }).typePresidentCible();
};

export const actualiteOriginesByRole = async ({ codeRole }, ctx: Context) => {
  watchTime.start();
  if (codeRole === SUPER_ADMINISTRATEUR) {
    return ctx.prisma.actualiteOrigines({ orderBy: 'ordre_ASC' }).then(result => {
      watchTime.stop();
      watchTime.printElapsed(`[Actualite Repository] Function actualiteOriginesByRole duration : `);
      return result;
    });
  }

  return ctx.prisma
    .role({ code: codeRole })
    .actualiteOrigines()
    .then(result => {
      watchTime.stop();
      watchTime.printElapsed(`[Actualite Repository] Function actualiteOriginesByRole duration : `);
      return result;
    });
};

export const publishActualite = async (ctx: Context, { id }) => {
  // Publish to elasticsearch
  const origine = await ctx.prisma.actualite({ id }).idActualiteOrigine();
  await ctx.indexing.publishSave('actualites', id);
  await ctx.indexing.publishSave('actualiteorigines', origine.id);
  logger.info(`~~~~~~~~~~~~~~~ PUBLISH : ${id} ~~~~~~~~~~~~`);

  return Promise.resolve();
};

export const getNewActualiteMessage = (libelle: string) => {
  return `Une nouvelle actualité <b>${libelle}</b> vient d'être ajoutée.`;
};

export const deleteSoftActualite = async (ctx: Context, { id }) => {
  return ctx.prisma
    .updateActualite({ data: { isRemoved: true }, where: { id } })
    .then(async actu => {
      await ctx.indexing.publishSave('actualites', actu.id);

      return actu;
    });
};

export const idsActualiteDateNOTActive = async (ctx: Context) => {
  const fragment = `
  fragment ActualiteId on Actualite {
    id
  }`;
  const now = moment()
    .add(3, 'hours')
    .toISOString();

  return ctx.prisma
    .actualites({
      where: { OR: [{ dateDebut_gt: now }, { dateFin_lt: now }, { isRemoved: true }] },
    })
    .$fragment(fragment)
    .then((actus: any[]) => (actus && actus.length ? actus.map(actu => actu.id) : []));
};

export const operationByIDActualite = async (ctx: Context, id: string) => {
  return ctx.prisma
    .operations({ where: { idActualite: { id } } })
    .then(async operations =>
      operations && operations.length ? ctx.prisma.operation({ id: operations[0].id }) : null,
    );
};

export const pharmacieActualitesSeen = async (ctx: Context, { id, idPharmacie }) => {
  return ctx.prisma
    .actualitePharmacieSeensConnection({
      where: {
        idActualite: { id },
        idPharmacie: { id: idPharmacie },
      },
    })
    .aggregate()
    .count();
};

export const countActualiteCiblePharmacies = async (
  ctx: Context,
  { ids }: { ids: string[] },
): Promise<number> => {
  const pharmaCibles = await ctx.prisma
    .actualites({
      where: {
        id_in: ids,
      },
    })
    .then(async actualites =>
      actualites && actualites.length
        ? (
            await Promise.all(
              actualites.map(actualite =>
                ctx.prisma.actualite({ id: actualite.id }).pharmacieCibles(),
              ),
            )
          ).flat()
        : null,
    );

  const pharmaciePresidents = await pharmacieCibleFromPresidents(ctx, { ids });

  if (pharmaciePresidents && pharmaciePresidents.length) {
    return pharmaCibles && pharmaCibles.length
      ? ArrayUnique([
          ...pharmaciePresidents.map(pharmaPresident => pharmaPresident.id),
          ...pharmaCibles.map(pharma => pharma.id),
        ]).length
      : ArrayUnique(pharmaciePresidents.map(pharmaPresident => pharmaPresident.id)).length;
  }

  return pharmaCibles && pharmaCibles.length ? pharmaCibles.length : 0;
};

export const pharmacieCibleFromPresidents = async (ctx: Context, { ids }: { ids: string[] }) => {
  const titulaires = await ctx.prisma
    .actualites({
      where: {
        id_in: ids,
        typePresidentCible: 'PHARMACIE',
      },
    })
    .then(async actualites =>
      actualites && actualites.length
        ? (
            await Promise.all(
              actualites.map(actu => ctx.prisma.actualite({ id: actu.id }).presidentCibles()),
            )
          ).flat()
        : null,
    );

  if (titulaires && titulaires.length) {
    const departements = await ctx.prisma
      .titulaireAffectations({
        where: {
          titulaire: { id_in: titulaires.map(cible => cible.id) },
          dateDebut_lte: now,
          dateFin_gte: now,
        },
      })
      .then(async affections =>
        affections && affections.length
          ? Promise.all(
              affections.map(cible =>
                ctx.prisma.titulaireAffectation({ id: cible.id }).departement(),
              ),
            )
          : null,
      );
    return departements && departements.length
      ? ctx.prisma.pharmacies({
          where: {
            departement: {
              id_in: departements.map(departement => departement.id),
            },
          },
        })
      : [];
  }
  return [];
};
