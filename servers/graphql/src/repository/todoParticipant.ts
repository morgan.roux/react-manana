import { TodoParticipantCreateInput } from '../generated/prisma-client';
import { Context, TodoParticipantInput } from '../types';

export const createUpdateTodoParticipant = async (input: TodoParticipantInput, ctx: Context) => {
  console.log('TodoParticipantInput -> input', input);

  const { id, idProject, idUserParticipant, codeMaj, isRemoved } = input;

  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;

  const data: TodoParticipantCreateInput = {
    project: idProject ? { connect: { id: idProject } } : null,
    userParticipant: idUserParticipant ? { connect: { id: idUserParticipant } } : null,
    codeMaj,
    isRemoved,
    userCreation,
    userModification,
  };

  const todoParticipant = await ctx.prisma.upsertTodoParticipant({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  // Publish to elasticsearch
  // await ctx.indexing.publishSave('todoparticipants', todoParticipant.id);

  return todoParticipant;
};

export const softDeleteTodoParticipant = async (ctx: Context, { id }: { id: string }) => {
  return ctx.prisma
    .updateTodoParticipant({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      // await ctx.indexing.publishSave('todoparticipants', res.id);
      return res;
    });
};
