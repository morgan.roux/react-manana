import { last } from 'lodash';
import {
  FichierCreateInput,
  IdeeOuBonnePratiqueCreateInput,
  IdeeOuBonnePratiqueFichierJointCreateInput,
} from '../generated/prisma-client';
import { Context, IdeeOuBonnePratiqueInput, IdeeOuBonnePratiqueStatus } from '../types';

export const createUpdateIdeeOuBonnePratique = async (
  ctx: Context,
  input: IdeeOuBonnePratiqueInput,
) => {
  const {
    idAuteur,
    origine,
    status,
    title,
    beneficiaires_cles,
    codeMaj,
    concurent,
    contexte,
    contraintes,
    facteursClesDeSucces,
    id,
    idClassification,
    idFournisseur,
    idGroupeAmis,
    idService,
    isRemoved,
    idPrestataire,
    objectifs,
    resultats,
    fichiers,
  } = input;

  const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;

  const data: IdeeOuBonnePratiqueCreateInput = {
    auteur: idAuteur ? { connect: { id: idAuteur } } : null,
    origine,
    title,
    beneficiaires_cles,
    classification: idClassification ? { connect: { id: idClassification } } : null,
    codeMaj,
    concurent,
    contexte,
    contraintes,
    facteursClesDeSucces,
    fournisseur: idFournisseur ? { connect: { id: idFournisseur } } : null,
    groupeAmis: idGroupeAmis ? { connect: { id: idGroupeAmis } } : null,
    service: idService ? { connect: { id: idService } } : null,
    groupement,
    isRemoved: isRemoved || false,
    objectifs,
    prestataire: idPrestataire ? { connect: { id: idPrestataire } } : null,
    resultats,
    status: status || 'NOUVELLE',
    userCreation,
    userModification,
  };

  const result = await ctx.prisma.upsertIdeeOuBonnePratique({
    where: { id: id || '' },
    create: data,
    update: data,
  });

  // Save fichiers
  if (fichiers && fichiers.length > 0) {
    await Promise.all(
      fichiers.map(async fichier => {
        const fichierData: FichierCreateInput = {
          chemin: fichier.chemin,
          nomOriginal: fichier.nomOriginal,
          type: fichier.type,
          userCreation,
          userModification,
        };
        await ctx.prisma
          .upsertFichier({
            where: { id: fichier.id },
            create: fichierData,
            update: fichierData,
          })
          .then(async f => {
            const exists = await ctx.prisma.ideeOuBonnePratiqueFichierJoints({
              where: { fichier: { id: f.id } },
            });
            const fichierJointData: IdeeOuBonnePratiqueFichierJointCreateInput = {
              fichier: f ? { connect: { id: f.id } } : null,
              ideeOuBonnePratique: result ? { connect: { id: result.id } } : null,
              isRemoved: false,
            };

            if (exists && exists.length > 0) {
              await ctx.prisma.deleteManyIdeeOuBonnePratiqueFichierJoints({
                id_in: exists.map(i => i.id),
              });
              await ctx.prisma.createIdeeOuBonnePratiqueFichierJoint(fichierJointData);
            } else {
              await ctx.prisma.createIdeeOuBonnePratiqueFichierJoint(fichierJointData);
            }
          });
      }),
    );
  } else if (id) {
    const fichierJoints = await ctx.prisma.ideeOuBonnePratiqueFichierJoints({
      where: { ideeOuBonnePratique: { id } },
    });
    if (fichierJoints && fichierJoints.length > 0) {
      await Promise.all(
        fichierJoints.map(async f => {
          const fichier = await ctx.prisma.ideeOuBonnePratiqueFichierJoint({ id: f.id }).fichier();
          await ctx.prisma.deleteIdeeOuBonnePratiqueFichierJoint({ id: f.id });
          if (fichier) await ctx.prisma.deleteFichier({ id: fichier.id });
        }),
      );
    }
  }

  // Save change status
  await saveIdeeOuBonnePratiqueStatus(ctx, result.id, status);

  await publishIdeeOuBonnePratique(ctx, result.id);
  return result;
};

export const updateIdeeOuBonnePratiqueStatus = async (
  ctx: Context,
  id: string,
  status: IdeeOuBonnePratiqueStatus,
  commentaire?: string,
) => {
  const result = await ctx.prisma.updateIdeeOuBonnePratique({
    where: { id },
    data: { status },
  });
  await saveIdeeOuBonnePratiqueStatus(ctx, id, status, commentaire);
  await publishIdeeOuBonnePratique(ctx, result.id);
  return result;
};

export const softDeleteIdeeOuBonnePratique = async (ctx: Context, id: string) => {
  const result = await ctx.prisma.updateIdeeOuBonnePratique({
    where: { id },
    data: { isRemoved: true },
  });
  await publishIdeeOuBonnePratique(ctx, result.id);
  return result;
};

export const publishIdeeOuBonnePratique = async (ctx: Context, id: string) => {
  await ctx.indexing.publishSave('ideeoubonnepratiques', id);
};

export const saveIdeeOuBonnePratiqueStatus = async (
  ctx: Context,
  idIdeeOuBonnePratique: string,
  status: IdeeOuBonnePratiqueStatus,
  commentaire?: string,
) => {
  const statusExistants = await ctx.prisma.ideeOuBonnePratiqueChangeStatuses({
    where: {
      ideeOuBonnePratique: { id: idIdeeOuBonnePratique },
      isRemoved: false,
    },
    orderBy: 'dateCreation_ASC',
  });

  if (statusExistants && statusExistants.length > 0) {
    // Update
    const existant = last(statusExistants);
    if (status && (existant.status !== status || existant.status === null)) {
      // Create (insert a new line)
      await ctx.prisma.createIdeeOuBonnePratiqueChangeStatus({
        status,
        isRemoved: false,
        commentaire: commentaire || existant.commentaire || '',
        ideeOuBonnePratique: idIdeeOuBonnePratique
          ? { connect: { id: idIdeeOuBonnePratique } }
          : null,
        userCreation: ctx.userId ? { connect: { id: ctx.userId } } : null,
        userModification: ctx.userId ? { connect: { id: ctx.userId } } : null,
      });
    }
  } else {
    // Create
    console.log('idIdeeOuBonnePratique :>> ', idIdeeOuBonnePratique);
    await ctx.prisma.createIdeeOuBonnePratiqueChangeStatus({
      status,
      isRemoved: false,
      commentaire,
      ideeOuBonnePratique: idIdeeOuBonnePratique
        ? { connect: { id: idIdeeOuBonnePratique } }
        : null,
      userCreation: ctx.userId ? { connect: { id: ctx.userId } } : null,
      userModification: ctx.userId ? { connect: { id: ctx.userId } } : null,
    });
  }
};
