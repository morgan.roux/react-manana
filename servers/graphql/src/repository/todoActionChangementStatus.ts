import { TodoActionChangementStatusCreateInput } from '../generated/prisma-client';
import { Context, TodoActionChangementStatusInput } from '../types';

export const createUpdateTodoActionChangementStatus = async (
  input: TodoActionChangementStatusInput,
  ctx: Context,
) => {
  const { id, codeMaj, isRemoved, idAction, idUser, status } = input;
  // const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
  const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
  const data: TodoActionChangementStatusCreateInput = {
    codeMaj,
    isRemoved,
    status,
    action: idAction ? { connect: { id: idAction } } : null,
    user: idUser ? { connect: { id: idUser } } : null,
    userCreation,
    userModification,
  };
  const todoActionChangementStatus = await ctx.prisma.upsertTodoActionChangementStatus({
    where: { id: id || '' },
    create: data,
    update: data,
  });
  // Publish to elasticsearch
  // await ctx.indexing.publishSave('todoactionchangementstatuses', todoActionChangementStatus.id);
  return todoActionChangementStatus;
};

export const softDeleteTodoActionChangementStatus = async (
  ctx: Context,
  { id }: { id: string },
) => {
  return ctx.prisma
    .updateTodoActionChangementStatus({
      where: { id },
      data: {
        isRemoved: true,
      },
    })
    .then(async res => {
      // await ctx.indexing.publishSave('todoactionchangementstatuses', res.id);
      return res;
    });
};
