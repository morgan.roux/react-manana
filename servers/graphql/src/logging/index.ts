import { createLogger, transports, format } from 'winston';
import WinstonLogStash from 'winston3-logstash-transport';

const LOGGING_FORMAT = {
  json: format.json(),
  logstash: format.logstash(),
  simple: format.simple(),
};

const logger = createLogger({
  level: process.env.LOGGING_LEVEL || 'info',
  defaultMeta: {
    service: 'graphql-service',
  },
  format: format.combine(
    LOGGING_FORMAT[process.env.LOGGING_FORMAT || 'simple'],
    format.timestamp(),
  ),
});

if ('true' === process.env.LOGGING_TRANSPORT_FILE_ENABLE) {
  logger.add(
    new transports.File({
      filename: 'access.log',
      level: process.env.LOGGING_TRANSPORT_FILE_LEVEL || process.env.LOGGING_LEVEL || 'info',
      format: format.combine(
        LOGGING_FORMAT[
          process.env.LOGGING_TRANSPORT_FILE_FORMAT || process.env.LOGGING_FORMAT || 'simple'
        ],
        format.timestamp(),
      ),
    }),
  );
}

if ('true' === process.env.LOGGING_TRANSPORT_LOGSTASH_ENABLE) {
  logger.add(
    new WinstonLogStash({
      applicationName: 'api-graphql',
      mode: process.env.LOGGING_TRANSPORT_LOGSTASH_MODE || 'udp',
      host: process.env.LOGGING_TRANSPORT_LOGSTASH_HOST || 'logstash',
      port: process.env.LOGGING_TRANSPORT_LOGSTASH_PORT
        ? parseInt(process.env.LOGGING_TRANSPORT_LOGSTASH_PORT, 10)
        : 28777,
    }),
  );
}

if (
  typeof process.env.LOGGING_TRANSPORT_CONSOLE_ENABLE === 'undefined' ||
  'true' === process.env.LOGGING_TRANSPORT_CONSOLE_ENABLE
) {
  logger.add(
    new transports.Console({ format: format.combine(format.simple(), format.timestamp()) }),
  );
}

export default logger;
