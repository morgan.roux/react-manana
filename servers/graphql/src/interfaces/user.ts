import { FichierInput } from '../types';

export interface UserPersonnelIHM {
  idGroupement: string;
  id: string;
  email: string;
  login: string;
  role: string;
  userId?: string;
  status?: any;
  day?: number;
  month?: number;
  year?: number;
  userPhoto?: FichierInput;
  codeTraitements?: string[];
}
