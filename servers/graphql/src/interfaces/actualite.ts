import { Fichier } from '../generated/prisma-client';
import { FichierInput, PharmacieRolesInput, PresidentRegionsInput } from '../types';

export interface ActualiteInput {
  id?: string;
  codeItem: string;
  codeOrigine: string;
  description: string;
  actionAuto: boolean;
  niveauPriorite?: number;
  idLaboratoire?: string;
  libelle: string;
  dateDebut: string;
  dateFin: string;
  globalite: boolean;
  idProject?: string;
  fichierPresentations: FichierInput[];
  fichierCible?: FichierInput;
  idTache?: string;
  idFonction?: string;
  idImportance: string;
  idUrgence: string;
}

export interface ActualiteFormInput extends ActualiteInput {
  pharmacieRoles?: PharmacieRolesInput;
  presidentRegions?: PresidentRegionsInput;
  services?: string[];
  laboratoires?: string[];
  partenaires?: string[];
  actionPriorite?: number;
  actionDescription?: string;
  actionDueDate?: string;
}

export interface ActualiteTodo {
  idActualite: string;
  idNewProject: string;
  idCurrentProjet: string;
  actionPriorite: number;
  libelle: string;
  actionDescription: string;
  actionDueDate: string;
}

export interface ActualiteCibleInput {
  idActualite: string;
  globalite: boolean;
  newFichierCible: FichierInput;
  currentFichierCible: Fichier;
  pharmacieRoles?: PharmacieRolesInput;
  presidentRegions?: PresidentRegionsInput;
  services?: string[];
  laboratoires?: string[];
  partenaires?: string[];
}

export interface ActualiteFileS3 {
  pharmacieRoles?: PharmacieRolesInput;
  presidents?: PresidentRegionsInput;
  services?: string[];
  laboratoires?: string[];
  partenaires?: string[];
}

export interface ActualiteCible {
  idActualite?: string;
  pharmacieRoles?: PharmacieRolesInput;
  presidentRegions?: PresidentRegionsInput;
  services?: string[];
  laboratoires?: string[];
  partenaires?: string[];
}
