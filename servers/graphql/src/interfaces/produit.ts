import { FichierInput, ProduitCanalInput } from '../types';

export interface ProduitFormInput {
  idProduit?: string;
  libelle: string;
  dateSuppression?: string;
  isGenerique?: number;
  fichierImage?: FichierInput;
  typeCodeReferent?: number;
  codeReference?: string;
  codeFamille?: string;
  idCategorie?: number;
  codeTva?: number;
  codeActe?: number;
  pxAchat?: number;
  pxVente?: number;
  codeListe?: number;
  codeTauxss?: number;
  codeLibelleStockage?: number;
  codeLibelleStatut?: number;
  codeLibelleCasher?: number;
  codeLibelleConditionnement?: number;
  idLaboTitulaire?: string;
  idLaboExploitant?: string;
  idLaboDistributaire?: string;
  produitCanaux: ProduitCanalInput[];
}

export interface ProduitVariableInput {
  id?: string;
  libelle: string;
  libelle2?: string;
  type?: number;
  categorie?: number;
  typeCategorie?: number;
  isReferentGenerique?: number;
  isDopant?: number;
  codeP1?: number;
  codeP2?: number;
  codeP3?: number;
  codeP4?: number;
  codeP5?: number;
  codeP6?: number;
  medicamentException?: number;
  medicamentT2a?: number;
  supprimer?: string;
  surveillanceRenforcee?: number;
  moleculeOnereuse?: number;
}

export interface ProduitTechRegInput {
  id?: string;
  codeTva?: number;
  codeActe?: number;
  pxAchat?: number;
  pxVente?: number;
  codeListe?: number;
  codeTauxss?: number;
  codeLibelleStockage?: number;
  codeLibelleStatut?: number;
  codeLibelleCasher?: number;
  codeLibelleConditionnement?: number;
  idLaboTitulaire?: string;
  idLaboExploitant?: string;
  idLaboDistributaire?: string;
}
