export const SERVICE_ADMIN = 'ADMIN';
export const SERVICE_MARKETING = 'MARKET';
export const SERVICE_COMMUNICATION = 'COMM';

export const SERVICE_SALES = 'SALES';
export const SERVICE_QUALITE = 'QUALI';
export const SERVICE_LOGISTIQUE = 'LOGI';

export const SERVICE_FORMATION = 'FORMA';
export const SERVICE_AUTRE = 'AUTRE';
