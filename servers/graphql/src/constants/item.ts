/* fichier contenant clé valeur : key => code de l'item et valeur code => parameter */
export const CODE_OPERATION_COMMERCIAL = 'OPERACOM';
export const CODE_ACTUALITE = 'ACTUALITE';

export const parameterItemCode = {
  OPERACOM: '0080',
  OPERAPROM: '0081',
  OFFREMOIS: '0082',
  OFFRELIQ: '0083',
  OTHERPROM: '0084',
  OPERALANCE: '0085',
  OFRRELANCE: '0086',
  MARCHE: '0087',
  PRECOM: '0088',
  OPERAPART: '0089',
  OFFREGROUP: '0090',
  ENCHINVERS: '0091',
  ACTUALITE: '0092',
  LABO: '0093',
  PSERVICE: '0094',
  EVENT: '0095',
  FORMATION: '0096',
  ESPACEDOC: '0097',
  DEMQUAL: '0098',
  NEWS: '0099',
  COMMANDE: '0100',
  PROD: '0101',
  TODO: '0102',
  ACTIVITE: '0103',
};

export const codeSousItemAssocies = {
  A: ['AAA', 'AAB', 'AAC', 'ABA', 'ABB', 'ABC', 'ACA', 'ACB'],
  AA: ['AAA', 'AAB', 'AAC'],
  AB: ['ABA', 'ABB', 'ABC'],
  AC: [, 'ACA', 'ACB'],
  B: [],
  C: [],
  D: [],
  E: [],
  F: [],
  G: [],
  H: [],
  I: [],
  J: [],
  K: [],
  L: [],
  M: [],
};
