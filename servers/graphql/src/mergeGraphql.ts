import typeDefs from './typeDefs';

const fs = require('fs');
const path = require('path');

const pathSchema = path.join(__dirname, '../schema.graphql');

fs.writeFile(pathSchema, typeDefs, function(err) {
  if (err) throw 'Merge schema graphql error: ' + err;
  console.log('Merge schema graphql saved');
});
