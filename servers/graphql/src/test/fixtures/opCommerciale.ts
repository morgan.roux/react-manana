import faker from 'faker';
import moment, { min } from 'moment';
import { TypeFichier } from './actualite';
import { codeItem, typeCommande, idCanalCommande, idLaboratoire, articles } from '../utils/operation';
import { PharmaciesIdToTest } from '../utils/actualite';

export const createUpdateOperationVariables = async () => ({
    codeItem: await codeItem(),
    libelle: faker.lorem.slug(),
    niveauPriorite: 1,
    description: faker.lorem.words(),
    dateDebut: moment().toDate(),
    dateFin: moment().add(6, 'days').toDate(),
    idLaboratoire: await idLaboratoire(),
    fichierPresentations: [
        {
            type: TypeFichier.PDF,
            nomOriginal: faker.lorem.word(),
            chemin: faker.internet.url(),
        },
    ],
    globalite: true,
    typeCommande: await typeCommande(),
    idsPharmacie: null,//await PharmaciesIdToTest(),
    articles: await articles(),
    fichierPharmacie: null,
    fichierProduit: null,//FichierInput,
    idCanalCommande: await idCanalCommande(),
    accordCommercial: faker.random.boolean(),
    idProject: null,//ID,
    actionPriorite: null,//faker.random.number(10),
    actionDescription: null,//faker.lorem.words(),
    actionDueDate: null,//moment().toDate(),
    caMoyenPharmacie: parseFloat(`${faker.random.number(10)}`),
    nbPharmacieCommande: faker.random.number(10),
    nbMoyenLigne: parseFloat(`${faker.random.number(10)}`),
})