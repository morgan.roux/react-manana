import faker from 'faker';
import moment from 'moment';
import { TypeFichier } from '../utils/graphql-global-types';
import {
  trTvaCode,
  trTableauCode,
  trMargeCode,
  trFormeCode,
  trStockageCode,
  rbtArticleCode,
  trActeCode,
  laboratoireCode,
  trFamilleCode,
} from '../utils/article';

export const createUpdateArticleVariables = async () => ({
  fichierImage: {
    type: TypeFichier.AVATAR,
    nomOriginal: faker.lorem.word(),
    chemin: faker.internet.url(),
  },
  libelle: 'TestUnit',
  typeCodeReferent: 'CIP13',
  codeReference: `${faker.random.number(1000000080)}`,
  codeTva: await trTvaCode(),
  pxAchatArticle: faker.random.number(2000),
  pxVenteArticle: faker.random.number(3000),
  codeTableau: await trTableauCode(),
  codeMarge: await trMargeCode(),
  codeForme: await trFormeCode(),
  codeStockage: await trStockageCode(),
  codeRbtArticle: await rbtArticleCode(),
  codeActe: await trActeCode(),
  dateSuppression: moment()
    .add(1, 'hour')
    .toDate(),
  codeLaboratoire: await laboratoireCode(),
  codeFamille: await trFamilleCode(),
  generiqArticle: faker.random.number(1),
  carteFidelite: faker.random.number(1),
  hospitalArticle: 0,
  canauxArticle: [{ isRemoved: true }, { isRemoved: true }],
});
