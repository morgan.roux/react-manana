import faker from 'faker';
import moment from 'moment';
import {
  TITULAIRE_PHARMACIE,
  RESPONSABLE_QUALITE_PHARMACIE,
  GETIONNAIRE_DE_COMMANDE,
  COMPTABLE,
  COLLABORATEUR_PHARMACIE,
  PREPARATEUR_PHARMACIE,
  STOCK_ET_REASSORT_PHARMACIE,
  APPRENTI_PREPARATEUR_PHARMACIE,
  ETUDIANT_EN_PHARMACIE_PHARMACIE,
  ASSISTANT_DE_DIRECTION_PHARMACIE,
  AUTRE_PROFESSIONNEL_DE_SANTE_PHARMACIE,
} from '../../constants/roles';
import { PharmaciesIdToTest, presidentRegionsToTest } from '../utils/actualite';

export enum TypeFichier {
  AVATAR = 'AVATAR',
  EXCEL = 'EXCEL',
  LOGO = 'LOGO',
  PDF = 'PDF',
  PHOTO = 'PHOTO',
}

export const actuDataWithGlobaliteTrue = {
  codeItem: 'ACTUALITE',
  codeOrigine: 'GROUPEMENT',
  description: faker.lorem.lines(),
  niveauPriorite: 1,
  // idLaboratoire: "ckdkdjg4n007v0715ka3nx8ko",
  libelle: faker.lorem.word(),
  dateDebut: moment().toDate(),
  dateFin: moment()
    .add(15, 'days')
    .toDate(),
  fichierPresentations: [
    {
      type: TypeFichier.PDF,
      nomOriginal: faker.lorem.word(),
      chemin: faker.internet.url(),
    },
  ],
  globalite: true,
};

export const actuEditDataWithGlobaliteTrue = {
  codeItem: 'ACTUALITE',
  codeOrigine: 'GROUPEMENT',
  description: faker.lorem.lines(),
  niveauPriorite: 1,
  // idLaboratoire: "ckdkdjg4n007v0715ka3nx8ko",
  libelle: faker.lorem.word(),
  dateDebut: moment().toDate(),
  dateFin: moment()
    .add(15, 'days')
    .toDate(),
  fichierPresentations: [
    {
      type: TypeFichier.PDF,
      nomOriginal: faker.lorem.word(),
      chemin: faker.internet.url(),
    },
  ],
  globalite: true,
};

export const actuDataWithCible = async () => ({
  codeItem: 'ACTUALITE',
  codeOrigine: 'GROUPEMENT',
  description: faker.lorem.lines(),
  niveauPriorite: 1,
  // idLaboratoire: "ckdkdjg4n007v0715ka3nx8ko",
  libelle: faker.lorem.word(),
  dateDebut: moment().toDate(),
  dateFin: moment()
    .add(15, 'days')
    .toDate(),
  fichierPresentations: [
    {
      type: TypeFichier.PDF,
      nomOriginal: faker.lorem.word(),
      chemin: faker.internet.url(),
    },
  ],
  globalite: false,
  pharmacieRoles: {
    pharmacies: await PharmaciesIdToTest(),
    roles: [
      TITULAIRE_PHARMACIE,
      RESPONSABLE_QUALITE_PHARMACIE,
      GETIONNAIRE_DE_COMMANDE,
      COMPTABLE,
      COLLABORATEUR_PHARMACIE,
      PREPARATEUR_PHARMACIE,
      STOCK_ET_REASSORT_PHARMACIE,
      APPRENTI_PREPARATEUR_PHARMACIE,
      ETUDIANT_EN_PHARMACIE_PHARMACIE,
      ASSISTANT_DE_DIRECTION_PHARMACIE,
      AUTRE_PROFESSIONNEL_DE_SANTE_PHARMACIE,
    ],
  },
  presidentRegions: presidentRegionsToTest(),
  services: ['COMM', 'LOGI', 'SERVICE15', 'ADMIN', 'AUTRE', 'QUALI', 'MARKET', 'SALES', 'FORMA'],
});
