import moment from 'moment';
import faker from 'faker';
import { TypeFichier } from '../utils/graphql-global-types';

export const createGroupementVariables = () => ({
  nom: faker.lorem.word().toUpperCase(),
  adresse1: faker.address.streetAddress(),
  adresse2: faker.address.secondaryAddress(),
  cp: faker.address.zipCode(),
  ville: faker.address.city(),
  pays: faker.address.country(),
  telBureau: faker.phone.phoneNumber(),
  telMobile: faker.phone.phoneNumber(),
  mail: faker.internet.email(),
  site: faker.internet.url(),
  commentaire: faker.lorem.sentence(),
  nomResponsable: faker.name.lastName(),
  prenomResponsable: faker.name.firstName(),
  dateSortie: moment().format(),
  sortie: 0,
  avatar: {
    type: TypeFichier.AVATAR,
    nomOriginal: faker.lorem.slug(),
    chemin: faker.image.avatar(),
  },
});

export const createGroupementLogoVariables = () => ({
  nomOriginal: faker.lorem.slug(),
  chemin: faker.image.avatar(),
});
