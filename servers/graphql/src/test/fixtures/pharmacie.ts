import faker from 'faker';
import moment from 'moment';
import { idGrossistes, idGeneriqueurs, entreeSortie, satisfaction, segmentation, compta, informatique, achat, cap, concept, chiffreAffaire, digitale } from '../utils/pharmacie';

export const createUpdatePharmacieVariables = async () => {
    return ({
        cip: `${faker.random.number(1000000080)}`,
        nom: faker.name.firstName(),
        adresse1: faker.address.county(),
        adresse2: faker.address.county(),
        cp: faker.lorem.word(),
        ville: faker.address.city(),
        pays: faker.address.country(),
        longitude: faker.address.longitude(),
        latitude: faker.address.latitude(),
        nbEmploye: faker.random.number(200),
        nbAssistant: faker.random.number(100),
        nbAutreTravailleur: faker.random.number(200),
        uga: faker.lorem.word(),
        commentaire: faker.lorem.words(),
        idGrossistes: await idGrossistes(),
        idGeneriqueurs: await idGeneriqueurs(),
        contact: {
            adresse1: faker.address.county(),
            adresse2: faker.address.county(),
            cp: faker.lorem.word(),
            ville: faker.address.city(),
            pays: faker.address.country(),
        },
        entreeSortie: await entreeSortie(),
        satisfaction: await satisfaction(),
        segmentation: await segmentation(),
        compta: await compta(),
        informatique: await informatique(),
        achat: await achat(),
        cap: await cap(),
        concept: await concept(),
        chiffreAffaire: await chiffreAffaire(),
        // digitales: [await digitale()],
    })
};