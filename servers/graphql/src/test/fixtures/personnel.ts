import faker from 'faker';
import { Sexe } from '../../types';

export const createUpdatePersonnelVariables = () => ({
    codeService: 'ADMIN',
    sortie: 0,
    civilite: faker.address.county(),
    nom: faker.name.firstName(),
    prenom: faker.name.lastName(),
    sexe: Sexe[0],
    commentaire: faker.lorem.words(),
    contact: {
        adresse1: faker.address.streetAddress(),
    },
})