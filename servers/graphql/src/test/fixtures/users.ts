import faker from 'faker';
import { COLLABORATEUR_COMMERCIAL } from '../../constants/roles';

export const testUser = {
  email: 'superadmin-gcr-pharma@yopmail.com',
  password: 'Pharma@123456789',
};

export const fakeUserParams = () => {
  return {
    email: faker.internet.email(),
    password: faker.internet.password(),
    userName: faker.name.findName(),
    prenom: faker.name.firstName(),
    nom: faker.name.lastName(),
  };
};

export const fakePersonnel = (idGroupement: string) => {
  return {
    id: 1,
    group: faker.random.number(5),
    societe: faker.random.number(5),
    service: faker.random.number(5),
    respHierarch: faker.random.number(5),
    sortie: 0,
    civilite: 'Mr',
    nom: faker.name.lastName(),
    prenom: faker.name.firstName(),
    login: null,
    password: null,
    adresse1: faker.address.streetAddress(),
    adresse2: faker.address.secondaryAddress(),
    telBureau: faker.phone.phoneNumber(),
    telDomicile: faker.phone.phoneNumber(),
    telMobProf: faker.phone.phoneNumber(),
    telMobPerso: faker.phone.phoneNumber(),
    faxBureau: faker.phone.phoneNumber(),
    faxDomicile: faker.phone.phoneNumber(),
    cp: faker.address.zipCode(),
    ville: faker.address.city(),
    mailProf: faker.internet.email(),
    mailPerso: faker.internet.email(),
    commentaire: faker.lorem.sentence(),
    dateSortie: faker.date.future(),
    idGroupement,
  };
};

export const fakeUserGroupementParams = (
  idGroupement: string,
  userId: string | null,
  role?: string,
) => {
  return {
    idGroupement,
    id: faker.random.number({ min: 3, max: 19 }),
    email: faker.internet.email(),
    role: role || COLLABORATEUR_COMMERCIAL,
    userId,
  };
};
