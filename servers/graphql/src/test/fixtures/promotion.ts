import faker from 'faker';
import moment from 'moment';
import { PromotionType, PromotionStatus, CodeCanalToTest, GroupeClientsToTest, CanalArticlesIds } from '../utils/promotion';

export const createUpdatePromotionVariables = async () => ({
    nom: faker.name.firstName(),
    dateDebut: moment().toDate(),
    dateFin: moment().add(15, 'days').toDate(),
    promotionType: PromotionType.AUTRE_PROMOTION,
    status: PromotionStatus.NON_ACTIF,
    codeCanal: await CodeCanalToTest(),
    idsCanalArticle: await CanalArticlesIds(),
    groupeClients: [await GroupeClientsToTest(), await GroupeClientsToTest()],
})