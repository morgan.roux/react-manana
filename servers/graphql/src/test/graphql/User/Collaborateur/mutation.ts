import gql from 'graphql-tag';

export const DO_CREATE_USER_COLLABORATEUR = gql`
  mutation CREATE_USER_COLLABORATEUR(
    $idGroupement: ID!
    $id: ID!
    $idPharmacie: ID!
    $email: String!
    $role: String!
    $userId: ID
  ) {
    createUserCollaborateurOfficine(
      idGroupement: $idGroupement
      id: $id
      role: $role
      idPharmacie: $idPharmacie
      email: $email
      userId: $userId
    ) {
      type
      id
      civilite
      nom
      prenom
      telBureau
      telMobile
      mail
      estAmbassadrice
      commentaire
      sortie
      dateSortie
      idGroupement
      idPharmacie
      pharmacie {
        id
        cip
      }
      user {
        id
        email
        status
      }
      role {
        code
        nom
      }
    }
  }
`;
