import gql from 'graphql-tag';
import { ROLE_INFO_FRAGEMENT } from '../Role/fragment';

export const USER_INFO_FRAGEMENT = gql`
  fragment UserInfo on User {
    id
    email
    status
    userName
    type
    role {
      ...RoleInfo
    }
  }
  ${ROLE_INFO_FRAGEMENT}
`;
