import gql from 'graphql-tag';

export const DO_CREATE_USER_GROUPEMENT = gql`
  mutation CREATE_USER_GROUPEMENT(
    $idGroupement: ID!
    $id: ID!
    $email: String!
    $role: String!
    $userId: ID
  ) {
    createUserGroupement(
      idGroupement: $idGroupement
      id: $id
      email: $email
      role: $role
      userId: $userId
    ) {
      type
      id
      sortie
      civilite
      nom
      prenom
      adresse1
      adresse2
      telBureau
      telDomicile
      telMobProf
      telMobPerso
      faxBureau
      faxDomicile
      cp
      ville
      mailProf
      mailPerso
      commentaire
      dateSortie
      idGroupement
      user {
        id
        email
      }
      role {
        code
        nom
      }
    }
  }
`;
