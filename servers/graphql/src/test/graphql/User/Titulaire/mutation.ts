import gql from 'graphql-tag';

export const DO_CREATE_USER_TITULAIRE = gql`
  mutation CREATE_USER_TITULAIRE(
    $idGroupement: ID!
    $id: ID!
    $email: String!
    $idPharmacie: ID!
    $userId: ID
  ) {
    createUserTitulaire(
      idGroupement: $idGroupement
      id: $id
      email: $email
      idPharmacie: $idPharmacie
      userId: $userId
    ) {
      type
      id
      nom
      prenom
      adresse1
      adresse2
      mail
      fax
      tele1
      tele2
      cp
      ville
      commentaire
      dateNaissance
      estPresidentRegion
      idGroupement
      user {
        id
        email
      }
      pharmacieUser {
        id
        cip
      }
    }
  }
`;
