import gql from 'graphql-tag';

export const DO_UPDATE_USER_STATUS = gql`
  mutation UPDATE_USER_STATUS($id: ID!, $status: String!) {
    updateUserStatus(id: $id, status: $status) {
      id
      email
      status
    }
  }
`;

export const DO_RESEND_USER_EMAIL = gql`
  mutation RESEND_USER_EMAIL($email: String!) {
    resendUserEmail(email: $email)
  }
`;

export const DO_UPDATE_USER_PHOTO = gql`
  mutation UPDATE_USER_PHOTO($userPhoto: FichierInput) {
    updateUserPhoto(userPhoto: $userPhoto) {
      fichier {
        chemin
        nomOriginal
        type
      }
    }
  }
`;

export const DO_RESET_USER_PASSWORD = gql`
  mutation RESET_USER_PASSWORD($id: ID!, $password: String!) {
    resetUserPassword(id: $id, password: $password) {
      id
      email
      status
    }
  }
`;
