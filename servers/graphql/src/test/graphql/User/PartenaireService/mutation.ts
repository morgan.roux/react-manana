import gql from 'graphql-tag';

export const DO_CREATE_USER_PARTENAIRE_SERVICE = gql`
  mutation CREATE_USER_PARTENAIRE_SERVICE($idGroupement: ID!, $id: ID!, $email: String!) {
    createUserPartenaireService(idGroupement: $idGroupement, id: $id, email: $email) {
      type
      id
      nom
      adresse1
      adresse2
      cp
      ville
      pays
      telBureau
      telMobile
      mail
      site
      commentaire
      idGroupement
      user {
        id
        email
        status
      }
      role {
        code
        nom
      }
    }
  }
`;
