import gql from 'graphql-tag';

export const DO_CREATE_USER_PRSIDENT = gql`
  mutation CREATE_USER_PRSIDENT(
    $idGroupement: ID!
    $id: ID!
    $idPharmacie: ID!
    $email: String!
    $userId: ID
  ) {
    createUserPresidentRegion(
      idGroupement: $idGroupement
      id: $id
      email: $email
      idPharmacie: $idPharmacie
      userId: $userId
    ) {
      type
      id
      nom
      prenom
      adresse1
      adresse2
      mail
      fax
      tele1
      tele2
      cp
      ville
      commentaire
      dateNaissance
      estPresidentRegion
      idGroupement
      user {
        id
        email
      }
      pharmacieUser {
        id
        cip
      }
    }
  }
`;
