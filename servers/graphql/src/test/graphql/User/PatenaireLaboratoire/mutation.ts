import gql from 'graphql-tag';

export const DO_CREATE_USER_PARTENAIRE_LABORATOIRE = gql`
  mutation CREATE_USER_PARTENAIRE_LABORATOIRE(
    $idGroupement: ID!
    $id: ID!
    $email: String!
    $userId: ID
  ) {
    createUserPartenaireLaboratoire(
      idGroupement: $idGroupement
      id: $id
      email: $email
      userId: $userId
    ) {
      type
      id
      code
      nom
      codeGroupLabo
      user {
        id
        email
      }
    }
  }
`;
