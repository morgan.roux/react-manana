import gql from 'graphql-tag';
import { COMMANDECANAL_FRAGMENT } from './fragment';

export const GET_COMMANDCANALS = gql`
  query commandeCanals {
    commandeCanals {
      ...CommandeCanalInfo
    }
  }
  ${COMMANDECANAL_FRAGMENT}
`;
