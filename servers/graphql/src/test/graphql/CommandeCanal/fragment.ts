import gql from 'graphql-tag';

export const COMMANDECANAL_FRAGMENT = gql`
  fragment CommandeCanalInfo on CommandeCanal {
    type
    id
    libelle
    code
    countOperationsCommercials
    countCanalArticles
  }
`;
