import gql from 'graphql-tag';

export const ITEM_INFO_FRAGEMENT = gql`
  fragment ItemInfo on Item {
    id
    code
    name
    dateCreation
    dateModification
  }
`;
