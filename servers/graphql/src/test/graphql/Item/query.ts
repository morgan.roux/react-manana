import gql from 'graphql-tag';
import { ITEM_INFO_FRAGEMENT } from './fragment';

export const GET_ITEMS = gql`
  query ITEMS {
    items {
      ...ItemInfo
    }
  }
  ${ITEM_INFO_FRAGEMENT}
`;
