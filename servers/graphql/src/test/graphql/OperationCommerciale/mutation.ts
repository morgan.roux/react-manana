import gql from 'graphql-tag';
import { OPERATION_COMMERCIALE_INFO_FRAGEMENT } from './fragment';

export const DO_CREATE_OPERATION = gql`
  mutation createUpdateOperation(
    $idOperation: ID
    $codeItem: String!
    $libelle: String!
    $niveauPriorite: Int!
    $description: String!
    $dateDebut: DateTime!
    $dateFin: DateTime!
    $idLaboratoire: ID
    $fichierPresentations: [FichierInput]!
    $globalite: Boolean
    $typeCommande: ID!
    $idsPharmacie: [ID]
    $articles: [OperationArticleInput]
    $fichierPharmacie: FichierInput
    $fichierProduit: FichierInput
    $idCanalCommande: ID!
    $accordCommercial: Boolean!
    $idProject: ID
    $actionPriorite: Int
    $actionDescription: String
    $actionDueDate: String
    $caMoyenPharmacie: Float
    $nbPharmacieCommande: Int
    $nbMoyenLigne: Float
  ) {
    createUpdateOperation(
      idOperation: $idOperation
      codeItem: $codeItem
      libelle: $libelle
      description: $description
      niveauPriorite: $niveauPriorite
      dateDebut: $dateDebut
      dateFin: $dateFin
      idLaboratoire: $idLaboratoire
      fichierPresentations: $fichierPresentations
      globalite: $globalite
      typeCommande: $typeCommande
      idsPharmacie: $idsPharmacie
      articles: $articles
      fichierPharmacie: $fichierPharmacie
      fichierProduit: $fichierProduit
      idCanalCommande: $idCanalCommande
      accordCommercial: $accordCommercial
      idProject: $idProject
      actionPriorite: $actionPriorite
      actionDescription: $actionDescription
      actionDueDate: $actionDueDate
      caMoyenPharmacie: $caMoyenPharmacie
      nbPharmacieCommande: $nbPharmacieCommande
      nbMoyenLigne: $nbMoyenLigne
    ) {
      ...OperationCommercialeInfo
    }
  }
  ${OPERATION_COMMERCIALE_INFO_FRAGEMENT}
`;


export const DO_DELETE_OPERATION = gql`
  mutation deleteOperation($id: ID!) {
    deleteOperation(id: $id) {
      id
    }
  }
`;