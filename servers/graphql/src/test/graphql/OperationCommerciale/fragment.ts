import gql from 'graphql-tag';
import { FICHIER_FRAGMENT } from '../Fichier/fragment';
import { USER_SMYLEY_INFO_FRAGMENT } from '../Smyley/fragment';
import { ITEM_INFO_FRAGEMENT } from '../Item/fragment';

export const OPERATION_COMMERCIALE_INFO_FRAGEMENT = gql`
  fragment OperationCommercialeInfo on Operation {
    id
    accordCommercial
    commandePassee
    isRemoved
    laboratoire {
      id
      nom
    }
    idGroupement
    fichierPresentations {
      ...FichierInfo
    }
    libelle
    niveauPriorite
    description
    dateDebut
    dateModification
    dateFin
    dateCreation
    dateModification
    nombrePharmaciesConsultes
    nombrePharmaciesCommandees
    nombreProduitsCommandes
    nombreTotalTypeProduitsCommandes
    CAMoyenParPharmacie
    CATotal
    nombreProduitsMoyenParPharmacie
    qteProduitsPharmacieCommandePassee
    qteTotalProduitsCommandePassee
    nombreCommandePassee
    totalCommandePassee
    caMoyenPharmacie
    nbPharmacieCommande
    nbMoyenLigne
    item {
      ...ItemInfo
    }
    actions {
      total
      data {
        id
        description
        dueDate
        priority
        project {
          id
          name
        }
      }
    }
    commandeType {
      code
    }
    commandeCanal {
      code
    }
    
  }
  ${ITEM_INFO_FRAGEMENT}
  ${FICHIER_FRAGMENT}
`;


/*
userSmyleys {
      total
      data {
        ...UserSmyleyInfo
      }
    }
    comments {
      total
    }
pharmacieCible {
      id
    }
pharmacieVisitors {
      id
    }
operationPharmacie {
    }
    operationArticles {
    }
    operationArticleCible {
    } */