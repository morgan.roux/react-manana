import gql from 'graphql-tag';

export const GROUPEMENT_INFO = gql`
  fragment GroupementInfo on Groupement {
    id
    nom
    adresse1
    adresse2
    cp
    ville
    pays
    telBureau
    telMobile
    mail
    site
    commentaire
    nomResponsable
    prenomResponsable
    dateSortie
    sortie
    groupementLogo {
      fichier {
        chemin
      }
    }
  }
`;
