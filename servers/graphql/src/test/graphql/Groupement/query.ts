import gql from 'graphql-tag';
import { GROUPEMENT_INFO } from './fragment';

export const GET_GROUPEMENTS = gql`
  query GROUPEMENTS {
    groupements {
      id
      nom
      groupementLogo {
        fichier {
          chemin
        }
      }
    }
  }
`;
export const GET_GROUPEMENT = gql`
  query GROUPEMENT($id: ID!) {
    groupement(id: $id) {
      id
      pharmacies {
        id
        nom
        ville
        tele1
        cip
        adresse1
        type
      }
      ...GroupementInfo
    }
  }
  ${GROUPEMENT_INFO}
`;
