import gql from 'graphql-tag';

export const GET_GROSSISTES = gql`
  query GROSSISTES {
    grossistes {
        id
    }
  }
`;