import gql from 'graphql-tag';
import { GROUPE_CLIENT_INFO_FRAGMENT_WITH_PHARMA } from './fragment';

export const GET_SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT = gql`
  query SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on GroupeClient {
          ...GroupeClientInfoWithPharma
        }
      }
    }
  }
  ${GROUPE_CLIENT_INFO_FRAGMENT_WITH_PHARMA}
`;

export const GET_GROUPE_CLIENT = gql`
  query GROUPE_CLIENT($id: ID!) {
    groupeClient(id: $id) {
      ...GroupeClientInfoWithPharma
    }
  }
  ${GROUPE_CLIENT_INFO_FRAGMENT_WITH_PHARMA}
`;

export const GET_GROUPE_CLIENTS = gql`
  query GROUPE_CLIENTS {
    groupeClients {
      ...GroupeClientInfoWithPharma
    }
  }
  ${GROUPE_CLIENT_INFO_FRAGMENT_WITH_PHARMA}
`;
