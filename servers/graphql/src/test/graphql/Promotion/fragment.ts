import gql from 'graphql-tag';

export const PROMOTION_INFO_FRAGMENT = gql`
  fragment PromotionInfo on Promotion {
    type
    id
    nom
    isRemoved
  }
`;

export const PROMOTION_INFO_FRAGMENT_FOR_EDIT = gql`
  fragment PromotionInfoForEdit on Promotion {
    type
    id
    nom
    dateDebut
    dateFin
    promotionType
    status
    dateCreation
    dateModification
    commandeCanal {
      id
      code
      libelle
    }
    userCreated {
      id
      userName
    }
    canalArticles {
      id
      codeReference
      nomCanalArticle
    }
    groupeClients {
      id
      groupeClient {
        id
        codeGroupe
      }
      remise {
        id
        remiseDetails {
          id
          quantiteMin
          quantiteMax
          pourcentageRemise
          remiseSupplementaire
        }
      }
    }
  }
`;
