import gql from 'graphql-tag';
import { TR_TABLEAU_INFO } from './fragment';

export const GET_TrTableaux = gql`
  query trTableaux {
    trTableaux {
      ...TrTableauInfo
    }
  }
  ${TR_TABLEAU_INFO}
`;