import gql from 'graphql-tag';
export const TR_TABLEAU_INFO = gql`
  fragment TrTableauInfo on TrTableau {
    type
    id
    codeTableau
    libelle
    codeMaj
    dateCreation
    dateModification
  }
`;
