import gql from 'graphql-tag';
import { TR_MARGE_INFO } from './fragment';

export const GET_TrMarges = gql`
  query trMarges {
    trMarges {
      ...TrMargeInfo
    }
  }
  ${TR_MARGE_INFO}
`;