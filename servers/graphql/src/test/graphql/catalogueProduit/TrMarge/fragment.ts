import gql from 'graphql-tag';
export const TR_MARGE_INFO = gql`
  fragment TrMargeInfo on TrMarge {
    type
    id
    codeMarge
    libelle
    codeMaj
    dateCreation
    dateModification
  }
`;
