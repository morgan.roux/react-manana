import gql from 'graphql-tag';
import { TR_ACTE_INFO } from './fragment';

export const GET_TrActes = gql`
  query trActes {
    trActes {
      ...TrActeInfo
    }
  }
  ${TR_ACTE_INFO}
`;