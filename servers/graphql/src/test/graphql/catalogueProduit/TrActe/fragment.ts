import gql from 'graphql-tag';
export const TR_ACTE_INFO = gql`
  fragment TrActeInfo on TrActe {
    type
    id
    codeActe
    libelle
    codeMaj
    dateCreation
    dateModification
  }
`;
