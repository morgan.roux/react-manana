import gql from 'graphql-tag';
import { TR_FAMILLE_INFO } from './fragment';

export const GET_TrFamille = gql`
  query trFamilles {
    trFamilles {
      ...TrFamilleInfo
    }
  }
  ${TR_FAMILLE_INFO}
`;