import gql from 'graphql-tag';
export const TR_FAMILLE_INFO = gql`
  fragment TrFamilleInfo on TrFamille {
    id
    idFamille
    codeFamille
    nomFamille
    majFamille
  }
`;
