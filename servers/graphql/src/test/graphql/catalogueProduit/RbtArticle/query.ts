import gql from 'graphql-tag';
import { RBT_ARTICLE_INFO } from './fragment';

export const GET_RbtArticles = gql`
  query rbtArticles {
    rbtArticles {
      ...RbtArticleInfo
    }
  }
  ${RBT_ARTICLE_INFO}
`;