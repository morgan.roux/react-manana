import gql from 'graphql-tag';
export const RBT_ARTICLE_INFO = gql`
  fragment RbtArticleInfo on RbtArticle {
    type
    id
    codeRbt
    libelle
    codeMaj
    dateCreation
    dateModification
  }
`;
