import gql from 'graphql-tag';
import { TR_STOCKAGE_INFO } from './fragment';

export const GET_TrStockages = gql`
  query trStockages {
    trStockages {
      ...TrStockageInfo
    }
  }
  ${TR_STOCKAGE_INFO}
`;