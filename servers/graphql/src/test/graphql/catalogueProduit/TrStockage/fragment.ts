import gql from 'graphql-tag';
export const TR_STOCKAGE_INFO = gql`
  fragment TrStockageInfo on TrStockage {
    type
    id
    codeStockage
    libelle
    codeMaj
    dateCreation
    dateModification
  }
`;
