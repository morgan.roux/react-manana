import gql from 'graphql-tag';
import { TR_TVA_INFO } from './fragment';

export const GET_TrTVA = gql`
  query trTVAs {
    trTVAs {
      ...TrTvaInfo
    }
  }
  ${TR_TVA_INFO}
`;