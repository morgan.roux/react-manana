import gql from 'graphql-tag';
export const TR_TVA_INFO = gql`
  fragment TrTvaInfo on TrTVA {
    type
    id
    codeTva
    valTva
    codeMaj
    dateCreation
    dateModification
  }
`;
