import gql from 'graphql-tag';
export const TR_FORME_INFO = gql`
  fragment TrFormeInfo on TrForme {
    type
    id
    codeForme
    libelle
    codeMaj
    dateCreation
    dateModification
  }
`;
