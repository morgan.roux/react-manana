import gql from 'graphql-tag';
import { TR_FORME_INFO } from './fragment';

export const GET_TrFormes = gql`
  query trFormes {
    trFormes {
      ...TrFormeInfo
    }
  }
  ${TR_FORME_INFO}
`;