import gql from 'graphql-tag';

export const PERSONNEL_INFO_FRAGMENT = gql`
  fragment PersonnelInfo on Personnel {
    id
    service {
     code
    }
    sortie
    civilite
    nom
    prenom
    sexe
    commentaire
    idGroupement
    contact {
      id
      adresse1
    }
  }
`;
