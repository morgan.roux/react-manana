import gql from 'graphql-tag';
import {
  ACTUALITE_GROUPE_INFO_FRAGEMENT,
  ACTUALITE_ORIGINE_INFO_FRAGEMENT,
  ACTUALITE_INFO_FRAGEMENT,
} from './fragment';

export const GET_ACTUALITE_GROUPES = gql`
  query ACTUALITE_GROUPES {
    actualiteGroupes {
      ...ActualiteGroupeInfo
    }
  }
  ${ACTUALITE_GROUPE_INFO_FRAGEMENT}
`;

export const GET_ACTUALITE_ORIGINES = gql`
  query ACTUALITE_ORIGINES {
    actualiteOrigines {
      ...ActualiteOrigineInfo
    }
  }
  ${ACTUALITE_ORIGINE_INFO_FRAGEMENT}
`;

export const GET_ACTUALITE_ORIGINES_BY_USER_ROLE = gql`
  query ACTUALITE_ORIGINES_BY_USER_ROLE($codeRole: String!) {
    actualiteOriginesByRole(codeRole: $codeRole) {
      ...ActualiteOrigineInfo
    }
  }
  ${ACTUALITE_ORIGINE_INFO_FRAGEMENT}
`;

export const GET_ACTUALITE = gql`
  query ACTUALITE($id: ID!, $idPharmacieUser: ID, $userId: String) {
    actualite(id: $id) {
      ...ActualiteInfo
    }
  }
  ${ACTUALITE_INFO_FRAGEMENT}
`;

export const GET_COUNT_ACTUALITES_CIBLES = gql`
  query COUNT_ACTUALITES_CIBLES {
    countActualitesCibles {
      code
      name
      total
    }
  }
`;

export const GET_ACTUALITE_ALL_USERS_CIBLE = gql`
  query ACTUALITE_ALL_USERS_CIBLE(
    $codeServices: [String]
    $idPartenaires: [String]
    $idLaboratoires: [ID]
    $pharmacieRoles: PharmacieRolesInput
    $presidentRegions: PresidentRegionsInput
    $fichierCible: FichierInput
  ) {
    actualiteAllUsersCible(
      codeServices: $codeServices
      idPartenaires: $idPartenaires
      idLaboratoires: $idLaboratoires
      pharmacieRoles: $pharmacieRoles
      presidentRegions: $presidentRegions
      fichierCible: $fichierCible
    ) {
      id
      userPhoto {
        id
        fichier {
          id
          publicUrl
        }
      }
      userName
      userTitulaire {
        id
        isPresident
        titulaire {
          id
          nom
          prenom
          adresse1
          adresse2
          cp
          ville
        }
      }
    }
  }
`;
