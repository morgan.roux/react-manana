import gql from 'graphql-tag';
import { ITEM_INFO_FRAGEMENT } from '../Item/fragment';
import { ACTUALITE_LABORATOIRE_INFO_FRAGMENT } from '../Laboratoire/fragment';
import { ROLE_INFO_FRAGEMENT } from '../Role/fragment';
import { USER_INFO_FRAGEMENT } from '../User/fragment';
import { FICHIER_FRAGMENT } from '../Fichier/fragment';
import { USER_SMYLEY_INFO_FRAGMENT } from '../Smyley/fragment';
export const ACTUALITE_GROUPE_INFO_FRAGEMENT = gql`
  fragment ActualiteGroupeInfo on ActualiteGroupe {
    id
    code
    libelle
    ordre
    dateCreation
    dateModification
  }
`;

export const ACTUALITE_ORIGINE_INFO_FRAGEMENT = gql`
  fragment ActualiteOrigineInfo on ActualiteOrigine {
    id
    code
    libelle
    ordre
    dateCreation
    dateModification
  }
`;

export const ACTUALITE_CIBLE_INFO_FRAGEMENT = gql`
  fragment ActualiteCibleInfo on ActualiteCible {
    id
    globalite
    fichierCible {
      id
      chemin
      type
      nomOriginal
      publicUrl
      urlPresigned
    }
    dateCreation
    dateModification
  }
`;

export const ACTUALITE_INFO_FRAGEMENT = gql`
  fragment ActualiteInfo on Actualite {
    type
    id
    idGroupement
    type
    libelle
    dateDebut
    dateFin
    isRemoved
    description
    actionAuto
    niveauPriorite
    typePresidentCible
    dateCreation
    dateModification
    item {
      ...ItemInfo
    }
    origine {
      ...ActualiteOrigineInfo
    }
    laboratoire {
      ...ActualiteLaboratoireInfo
    }
    fichierPresentations {
      ...FichierInfo
    }
    actualiteCible {
      ...ActualiteCibleInfo
    }
    servicesCible {
      type
      id
      idService
      code
      nom
      countUsers
    }
    comments {
      total
    }
    userSmyleys {
      total
      data {
        ...UserSmyleyInfo
      }
    }
    pharmaciesCible {
      type
      id
      nom
      departement {
        nom
      }
      titulaires {
        id
        nom
        prenom
        fullName
      }
      idGroupement
      actived
    }
    pharmaciesRolesCible {
      id
      code
    }
    presidentsCibles {
      type
      id
      nom
      prenom
      fullName
      estPresidentRegion
      idGroupement
      pharmacies {
        id
      }
    }
    partenairesCible {
      type
      id
      nom
      idGroupement
    }
    laboratoiresCible {
      type
      id
      code
      nom
      sortie
    }
    operation {
      id
      commandePassee
    }
    action {
      id
      description
      priority
      dueDate
      project {
        id
        name
      }
    }
  }
  ${ITEM_INFO_FRAGEMENT}
  ${ACTUALITE_ORIGINE_INFO_FRAGEMENT}
  ${ACTUALITE_LABORATOIRE_INFO_FRAGMENT}
  ${FICHIER_FRAGMENT}
  ${ACTUALITE_CIBLE_INFO_FRAGEMENT}
  ${ROLE_INFO_FRAGEMENT}
  ${USER_INFO_FRAGEMENT}
  ${USER_SMYLEY_INFO_FRAGMENT}
`;
