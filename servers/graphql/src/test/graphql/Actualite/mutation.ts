import gql from 'graphql-tag';
import { ACTUALITE_INFO_FRAGEMENT } from './fragment';

export const DO_CREATE_AND_UPDATE_ACTUALITE = gql`
  mutation CREATE_AND_UPDATE_ACTUALITE(
    $id: ID
    $codeItem: String!
    $codeOrigine: String!
    $description: String
    $actionAuto: Boolean
    $niveauPriorite: Int
    $idLaboratoire: ID
    $libelle: String
    $dateDebut: String!
    $dateFin: String!
    $fichierPresentations: [FichierInput!]!
    $globalite: Boolean
    $fichierCible: FichierInput
    $pharmacieRoles: PharmacieRolesInput
    $presidentRegions: PresidentRegionsInput
    $users: [ID!]
    $roles: [String!]
    $services: [String!]
    $laboratoires: [ID!]
    $partenaires: [ID!]
    $idProject: ID
    $actionPriorite: Int
    $actionDescription: String
    $actionDueDate: String
  ) {
    createUpdateActualite(
      id: $id
      codeItem: $codeItem
      codeOrigine: $codeOrigine
      description: $description
      actionAuto: $actionAuto
      niveauPriorite: $niveauPriorite
      idLaboratoire: $idLaboratoire
      libelle: $libelle
      dateDebut: $dateDebut
      dateFin: $dateFin
      fichierPresentations: $fichierPresentations
      globalite: $globalite
      fichierCible: $fichierCible
      pharmacieRoles: $pharmacieRoles
      presidentRegions: $presidentRegions
      users: $users
      roles: $roles
      services: $services
      laboratoires: $laboratoires
      partenaires: $partenaires
      idProject: $idProject
      actionPriorite: $actionPriorite
      actionDescription: $actionDescription
      actionDueDate: $actionDueDate
    ) {
      ...ActualiteInfo
    }
  }
  ${ACTUALITE_INFO_FRAGEMENT}
`;

export const DO_DELETE_ACTUALITE = gql`
  mutation DELETE_ACTUALITE($id: ID!) {
    deleteActualite(id: $id) {
      id
    }
  }
`;

export const DO_SEND_NOTIFICATION_ACTUALITE_TO_CIBLE = gql`
  mutation SEND_NOTIFICATION_ACTUALITE_TO_CIBLE($id: ID!, $isUpdate: Boolean) {
    sendNotificationActualiteToCible(id: $id, isUpdate: $isUpdate)
  }
`;

export const DO_MARK_ACTUALITE_SEEN = gql`
  mutation MARK_ACTUALITE_SEEN($id: ID!, $idPharmacieUser: ID, $userId: String) {
    markAsSeenActualite(id: $id) {
      ...ActualiteInfo
    }
  }
  ${ACTUALITE_INFO_FRAGEMENT}
`;

export const DO_MARK_ACTUALITE_NOT_SEEN = gql`
  mutation MARK_ACTUALITE_NOT_SEEN($id: ID!, $idPharmacieUser: ID, $userId: String) {
    markAsNotSeenActualite(id: $id, idPharmacie: $idPharmacieUser) {
      ...ActualiteInfo
    }
  }
  ${ACTUALITE_INFO_FRAGEMENT}
`;

export const DO_SEEN_BY_PHARMACIE = gql`
  mutation SEEN_BY_PHARMACIE($id: ID!, $idPharmacieUser: ID!, $userId: String) {
    actualiteSeenByPharmacie(id: $id, idPharmacie: $idPharmacieUser) {
      ...ActualiteInfo
    }
  }
  ${ACTUALITE_INFO_FRAGEMENT}
`;
