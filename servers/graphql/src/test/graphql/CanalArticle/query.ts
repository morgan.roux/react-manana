import gql from 'graphql-tag';
import { ARTICLE_MIN_INFO } from './fragment';

export const GET_CANAL_ARTICLE = gql`
  query CANAL_ARTICLE($id: ID!) {
    canalArticle(id: $id) {
      ...ArticleMinInfo
    }
  }
  ${ARTICLE_MIN_INFO}
`;

export const GET_CANAL_ARTICLES = gql`
  query CANAL_ARTICLES {
    canalArticles {
      ...ArticleMinInfo
    }
  }
  ${ARTICLE_MIN_INFO}
`;