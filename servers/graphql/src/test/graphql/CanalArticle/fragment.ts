import gql from 'graphql-tag';

export const ARTICLE_MIN_INFO = gql`
  fragment ArticleMinInfo on CanalArticle {
    id
    nomCanalArticle
    codeReference
  }
`;
