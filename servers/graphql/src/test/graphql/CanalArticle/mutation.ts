import gql from 'graphql-tag';
import { ARTICLE_MIN_INFO } from './fragment';

export const DO_CREATE_UPDATE_ARTICLE = gql`
  mutation CREATE_UPDATE_ARTICLE(
    $idArticle: ID
    $qteStock: Int
    $qteMin: Int
    $stv: Int
    $unitePetitCond: Int
    $uniteSupCond: Int
    $flagBlocage: String
    $prixFab: Float
    $prixPhv: Float
    $prixTtc: Float
    $typeCodeReferent: String
    $codeReference: String
    $codeCanalCommande: String
    $dateCreation: String
    $dateModification: String
    $idCanalSousGamme: Int
    $codeGammeSousCatalogue: Int
    $libelle: String
    $codeTva: String
    $pxAchatArticle: Float
    $pxVenteArticle: Float
    $codeTableau: String
    $codeMarge: String
    $codeForme: String
    $codeStockage: String
    $codeRbtArticle: String
    $codeActe: Int
    $codeLaboratoire: String
    $codeFamille: String
    $generiqArticle: Int
    $carteFidelite: Int
  ) {
    createUpdateArticle(
      idArticle: $idArticle
      qteStock: $qteStock
      qteMin: $qteMin
      stv: $stv
      unitePetitCond: $unitePetitCond
      uniteSupCond: $uniteSupCond
      flagBlocage: $flagBlocage
      prixFab: $prixFab
      prixPhv: $prixPhv
      prixTtc: $prixTtc
      typeCodeReferent: $typeCodeReferent
      codeReference: $codeReference
      codeCanalCommande: $codeCanalCommande
      dateCreation: $dateCreation
      dateModification: $dateModification
      idCanalSousGamme: $idCanalSousGamme
      codeGammeSousCatalogue: $codeGammeSousCatalogue
      libelle: $libelle
      codeTva: $codeTva
      pxAchatArticle: $pxAchatArticle
      pxVenteArticle: $pxVenteArticle
      codeTableau: $codeTableau
      codeMarge: $codeMarge
      codeForme: $codeForme
      codeStockage: $codeStockage
      codeRbtArticle: $codeRbtArticle
      codeActe: $codeActe
      codeLaboratoire: $codeLaboratoire
      codeFamille: $codeFamille
      generiqArticle: $generiqArticle
      carteFidelite: $carteFidelite
    ) {
      ...ArticleMinInfo
    }
  }
  ${ARTICLE_MIN_INFO}
`;

export const DO_DELETE_ARTICLE = gql`
  mutation DELETE_ARTICLE($idArticle: ID!, $soft: Boolean) {
    deleteArticle(idArticle: $idArticle, soft: $soft) {
      ...ArticleMinInfo
    }
  }
  ${ARTICLE_MIN_INFO}
`;

export const DO_EDIT_CANAL_ARTICLE = gql`
  mutation EDIT_CANAL_ARTICLE($idArticle: ID!, $codeCanalCommande: String!) {
    editCanalArticle(idArticle: $idArticle, codeCanalCommande: $codeCanalCommande) {
      ...ArticleMinInfo
    }
  }
  ${ARTICLE_MIN_INFO}
`;
