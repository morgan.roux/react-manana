import gql from 'graphql-tag';

export const GET_GENERIQUEURS = gql`
  query GENERIQUEURS {
    generiqueurs {
        id
    }
  }
`;