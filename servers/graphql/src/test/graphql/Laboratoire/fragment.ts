import gql from 'graphql-tag';

export const LABORATOIRE_INFO_FRAGMENT = gql`
  fragment LaboratoireInfo on Laboratoire {
    type
    id
    code
    nom
    codeGroupLabo
    countCanalArticles
    sortie
    actualites {
      id
      libelle
      dateCreation
      description
      fichierPresentations {
        id
        nomOriginal
        publicUrl
      }
    }
  }
`;

export const FICHE_LABORATOIRE_FRAGMENT = gql`
  fragment FicheLaboratoire on Laboratoire {
    id
    code
    nom
    codeGroupLabo
    sortie
  }
`;

export const ACTUALITE_LABORATOIRE_INFO_FRAGMENT = gql`
  fragment ActualiteLaboratoireInfo on Laboratoire {
    id
    code
    nom
  }
`;
