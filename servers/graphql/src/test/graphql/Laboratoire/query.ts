import gql from 'graphql-tag';
import { LABORATOIRE_INFO_FRAGMENT, FICHE_LABORATOIRE_FRAGMENT } from './fragment';

export const GET_LABORATOIRE = gql`
  query LABORATOIRE($id: ID!) {
    laboratoire(id: $id) {
      ...LaboratoireInfo
      user {
        id
        email
        status
      }
    }
  }
  ${LABORATOIRE_INFO_FRAGMENT}
`;

export const GET_LABORATOIRES = gql`
  query LABORATOIRES {
    laboratoires {
      ...LaboratoireInfo
      user {
        id
        email
        status
      }
    }
  }
  ${LABORATOIRE_INFO_FRAGMENT}
`;

export const GET_FICHE_LABORATOIRE = gql`
  query FICHE_LABORATOIRE($id: ID!) {
    laboratoire(id: $id) {
      ...FicheLaboratoire
    }
  }
  ${FICHE_LABORATOIRE_FRAGMENT}
`;

export const DO_SEARCH_LABORATOIRE = gql`
  query SEARCH_LABORATOIRE(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Laboratoire {
          type
          id
          code
          nom
        }
      }
    }
  }
`;
