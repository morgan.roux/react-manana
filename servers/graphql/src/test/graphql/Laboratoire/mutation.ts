import gql from 'graphql-tag';

export const SET_SORTIE_LABORATOIRE = gql`
  mutation setLaboratoireSortie($id: ID!, $sortie: Int) {
    updateLaboratoireSortie(id: $id, sortie: $sortie) {
      id
      nom
      code
      codeGroupLabo
      sortie
      actualites {
        libelle
        dateCreation
        fichierPresentations {
          publicUrl
        }
        description
      }
    }
  }
`;
