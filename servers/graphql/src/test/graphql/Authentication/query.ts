import gql from 'graphql-tag';

export const GET_ME = gql`
  query ME {
    me {
      type
      id
      email
      status
      emailConfirmed
      phoneNumber
      phoneNumberConfirmed
      twoFactorEnabled
      lockoutEndDateUtc
      lockoutEnabled
      accessFailedCount
      userName
      lastLoginDate
      lastPasswordChangedDate
      isLockedOut
      isLockedOutPermanent
      isObligationChangePassword
      accessFailedCountBeforeLockoutPermanent
      dateCreation
      dateModification
      role {
        code
        nom
      }
      groupement {
        id
        nom
        groupementLogo {
          fichier {
            chemin
          }
        }
      }
      userPhoto {
        id
        fichier {
          id
          chemin
          nomOriginal
        }
      }
      userTitulaire {
        isPresident
        titulaire {
          id
          nom
          prenom
          pharmacies {
            id
            nom
          }
          pharmacieUser {
            id
            nom
          }
        }
      }
    }
  }
`;

export const GET_IP_VALIDATION = gql`
  query IP_VALIDATION($token: String, $status: ConnexionStatut) {
    validateUserIp(token: $token, status: $status) {
      accessToken
    }
  }
`;
