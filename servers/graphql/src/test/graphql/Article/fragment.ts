import gql from 'graphql-tag';
export const ARTICLE_INFO = gql`
  fragment ArticleInfo on Article {
    id
    idArticle
    articlePhoto {
      id 
      fichier {
          type
          nomOriginal
          chemin
      }
    }
    nomArticle
    typeCodeReferent
    codeReference
    pxAchatArticle
    pxVenteArticle
    dateSuppression
    hospitalArticle
    generiqArticle
    carteFidelite
    tva {
            codeTva
          }
    tableau {
            codeTableau
          }
    marge {
            codeMarge
          }
    forme {
            codeForme
          }
    stockage {
            codeStockage
          }
    rbtArticle {
            codeRbt
          }
    acte {
            codeActe
          }
    laboratoire {
            code
          }
    famille {
            codeFamille
             }
    canauxArticle {
      id
      isRemoved
    }
  }
`;
