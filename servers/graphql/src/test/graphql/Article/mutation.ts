import gql from 'graphql-tag';
import { ARTICLE_INFO } from './fragment';

export const CREATE_UPDATE_ARTICLE = gql`
  mutation createUpdateArticle(
    $idArticle: ID
    $fichierImage: FichierInput
    $libelle: String!
    $typeCodeReferent: String
    $codeReference: String
    $codeTva: String
    $pxAchatArticle: Float
    $pxVenteArticle: Float
    $codeTableau: String
    $codeMarge: String
    $codeForme: String
    $codeStockage: String
    $codeRbtArticle: String
    $codeActe: Int
    $dateSuppression: String
    $codeLaboratoire: String
    $codeFamille: String
    $generiqArticle: Int
    $carteFidelite: Int
    $hospitalArticle: Int
    $canauxArticle: [CanalArticleInput]
  ) {
    createUpdateArticle(
      idArticle: $idArticle
      fichierImage: $fichierImage
      libelle: $libelle
      typeCodeReferent: $typeCodeReferent
      codeReference: $codeReference
      codeTva: $codeTva
      pxAchatArticle: $pxAchatArticle
      pxVenteArticle: $pxVenteArticle
      codeTableau: $codeTableau
      codeMarge: $codeMarge
      codeForme: $codeForme
      codeStockage: $codeStockage
      codeRbtArticle: $codeRbtArticle
      codeActe: $codeActe
      dateSuppression: $dateSuppression
      codeLaboratoire: $codeLaboratoire
      codeFamille: $codeFamille
      generiqArticle: $generiqArticle
      carteFidelite: $carteFidelite
      hospitalArticle: $hospitalArticle
      canauxArticle: $canauxArticle
    ) {
      ...ArticleInfo
    }
  }
  ${ARTICLE_INFO}
`;