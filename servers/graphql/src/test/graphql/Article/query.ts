import gql from 'graphql-tag';
import { ARTICLE_INFO } from './fragment';
export const GET_ARTICLE = gql`
  query ARTICLE($idArticle: ID!) {
    article(idArticle: $idArticle) {
      ...ArticleInfo
    }
  }
  ${ARTICLE_INFO}
`;
