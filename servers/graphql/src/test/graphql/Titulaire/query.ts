import gql from 'graphql-tag';

export const GET_TITULAIRES = gql`
  query TITULAIRES {
    titulaires {
        id
    }
  }
`;