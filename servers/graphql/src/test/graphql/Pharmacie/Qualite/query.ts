import gql from 'graphql-tag';

export const GET_QUALITES = gql`
  query QUALITES {
    qualites  {
      id
    }
  }
`;