import gql from 'graphql-tag';

export const GET_MOTIFS = gql`
  query MOTIFS($type: TypeMotif) {
    motifs(type: $type)  {
      id
    }
  }
`;