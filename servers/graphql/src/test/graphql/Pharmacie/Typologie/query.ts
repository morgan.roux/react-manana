import gql from 'graphql-tag';

export const GET_TYPOLOGIES = gql`
  query TYPOLOGIES {
    typologies  {
      id
    }
  }
`;