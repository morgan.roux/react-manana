import gql from 'graphql-tag';

export const GET_AUTOMATES = gql`
  query AUTOMATES {
    automates {
      id
    }
  }
`;