import gql from 'graphql-tag';

export const PHARMACIE_FRAGMENT = gql`
  fragment Pharmacie on Pharmacie {
    type
    id
    cip
    nom
    adresse1
    ville
  }
`;

export const PHARMACIE_INFO_FRAGMENT = gql`
  fragment PharmacieInfo on Pharmacie {
    cip
    nom
    adresse1
    adresse2
    id
    type
    ville
    pays
    cp
    uga
    nbEmploye
    nbAssistant
    nbAutreTravailleur
    sortie
    numFiness
    nbJourOuvre
    latitude
    longitude
    commentaire
    departement {
      nom
    }
    titulaires {
      id
      civilite
      nom
      prenom
    }
    presidentRegion {
      id
      titulaire {
        id
        fullName
        nom
        prenom
      }
      departement {
        id
        nom
        region {
          id
          nom
        }
      }
    }
    contact {
      id
      cp
      ville
      pays
      adresse1
      adresse2
      faxProf
      faxPerso
      telProf
      telMobProf
      telPerso
      telMobPerso
      mailProf
      mailPerso
      siteProf
      sitePerso
    }
    grossistes {
      id
    }
    generiqueurs {
      id
    }
    entreeSortie {
      contrat {
        id
      }
      concurrent {
        id
      }
      concurrentAncien {
        id
      }
      dateEntree
      pharmacieMotifEntree {
        id
      }
      commentaireEntree 
      dateSortieFuture 
      pharmacieMotifSortieFuture {
        id
      }
      commentaireSortieFuture 
      dateSortie 
      pharmacieMotifSortie {
        id
      }
      commentaireSortie
      codeMaj
      dateCreation
      dateModification
    }
    satisfaction {
      dateSaisie
      commentaire
      smyley {
        id
      }
    }
    segmentation {
      typologie {
        id
      }
      trancheCA {
        id
      }
      qualite {
        id
      }
      contrat {
        id
      }
      dateSignature
    }
    compta {
      siret
      codeERP
      ape
      tvaIntra
      rcs
      contactCompta
      chequeGrp
      valeurChequeGrp
      commentaireChequeGrp
      droitAccesGrp
      valeurChequeAccesGrp
      commentaireChequeAccesGrp
      structureJuridique
      denominationSociale
      telCompta
      modifStatut
      raisonModif
      dateModifStatut
      nomBanque
      banqueGuichet
      banqueCompte
      banqueRib
      banqueCle
      dateMajRib
      iban
      swift
      dateMajIban
    }
    informatique {
      logiciel {
        id
      }
      numVersion
      dateLogiciel
      nbrePoste
      nbreComptoir
      nbreBackOffice
      nbreBureau
      commentaire
      automate1{
        id
      }
      dateInstallation1
      automate2{
        id
      }
      dateInstallation2
      automate3{
        id
      }
      dateInstallation3
    }
    achat {
      dateDebut
      dateFin
      identifiantAchatCanal
      commentaire
      canal {
        id
      }
    }
    cap {
      cap
      dateDebut
      dateFin
      identifiantCap
      commentaire
        }
    concept {
      fournisseurMobilier {
        id
      }
      dateInstallConcept
      avecConcept
      concept {
        id
      }
      avecFacade 
      dateInstallFacade 
      enseigniste {
            id
          }
      conformiteFacade 
      facade {
            id
          }
      surfaceTotale 
      surfaceVente 
      SurfaceVitrine 
      volumeLeaflet 
      nbreLineaire 
      nbreVitrine 
      otcLibAcces 
      leaflet {
            id
          }
      commentaire 
    }
    statCA {
      exercice 
      dateDebut 
      dateFin 
      caTTC 
      caHt 
      caTVA1 
      tauxTVA1 
      caTVA2 
      tauxTVA2 
      caTVA3 
      tauxTVA3 
      caTVA4 
      tauxTVA4 
      caTVA5 
      tauxTVA5 
    }
    digitales {
      servicePharmacie {
            id
          }
      pharmaciePestataire {
            id
          }
      flagService 
      dateInstallation 
      url 
    }
  }
`;