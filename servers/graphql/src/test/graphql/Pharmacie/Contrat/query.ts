import gql from 'graphql-tag';

export const GET_CONTRATS = gql`
  query CONTRATS {
    contrats {
      id
    }
  }
`;