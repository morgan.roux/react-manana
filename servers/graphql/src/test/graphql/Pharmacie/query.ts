import gql from 'graphql-tag';
import { PHARMACIE_FRAGMENT, PHARMACIE_INFO_FRAGMENT } from './fragment';

export const GET_PHARMACIES = gql`
  query PHARMACIES {
    pharmacies {
      ...Pharmacie
    }
  }
  ${PHARMACIE_FRAGMENT}
`;

export const GET_PHARMACIE = gql`
  query PHARMACIE_INFO($id: ID!) {
    pharmacie(id: $id) {
      ...PharmacieInfo
    }
  }
  ${PHARMACIE_INFO_FRAGMENT}
`;

