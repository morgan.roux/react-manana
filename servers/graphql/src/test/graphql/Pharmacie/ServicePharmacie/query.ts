import gql from 'graphql-tag';

export const GET_SERVICE_PHARMACIES = gql`
  query GET_SERVICE_PHARMACIES {
    servicePharmacies {
      id
    }
  }
`;