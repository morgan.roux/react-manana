import gql from 'graphql-tag';

export const GET_PRESTATAIRE_PHARMACIES = gql`
  query PRESTATAIRE_PHARMACIES($type: TypePrestataire) {
    prestatairePharmacies(type: $type)  {
      id
    }
  }
`;