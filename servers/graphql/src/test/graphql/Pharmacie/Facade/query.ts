import gql from 'graphql-tag';

export const GET_FACADES = gql`
  query FACADES {
    facades {
      id
    }
  }
`;