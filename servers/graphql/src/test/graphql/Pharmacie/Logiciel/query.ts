import gql from 'graphql-tag';

export const GET_LOGICIELS = gql`
  query LOGICIELS {
    logiciels {
      id
    }
  }
`;