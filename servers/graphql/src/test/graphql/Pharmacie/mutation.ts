import gql from 'graphql-tag';
import { PHARMACIE_INFO_FRAGMENT } from './fragment';

export const DO_CREATE_UPDATE_PHARMACIE = gql`
  mutation CREATE_UPDATE_PHARMACIE(
    $id: ID
    $cip: String!
    $nom: String!
    $adresse1: String
    $adresse2: String
    $cp: String
    $ville: String
    $pays: String
    $longitude: String
    $latitude: String
    $numFiness: String
    $sortie: Int
    $sortieFuture: Int
    $nbEmploye: Int
    $nbAssistant: Int
    $nbAutreTravailleur: Int
    $uga: String
    $commentaire: String
    $idGrossistes: [ID]
    $idGeneriqueurs: [ID]
    $contact: ContactInput
    $entreeSortie: PharmacieEntreeSortieInput
    $satisfaction: PharmacieSatisfactionInput
    $segmentation: PharmacieSegmentationInput
    $compta: PharmacieComptaInput
    $informatique: PharmacieInformatiqueInput
    $achat: PharmacieAchatInput
    $cap: PharmacieCapInput
    $concept: PharmacieConceptInput
    $chiffreAffaire: PharmacieStatCAInput
    $digitales: [PharmacieDigitaleInput]
  ) {
    createUpdatePharmacie(
        id: $id
        cip: $cip 
        nom: $nom
        adresse1: $adresse1
        adresse2: $adresse2
        cp: $cp
        ville: $ville
        pays: $pays
        longitude: $longitude
        latitude: $latitude
        numFiness: $numFiness
        sortie: $sortie
        sortieFuture: $sortieFuture
        nbEmploye: $nbEmploye
        nbAssistant: $nbAssistant
        nbAutreTravailleur: $nbAutreTravailleur
        uga: $uga
        commentaire: $commentaire
        idGrossistes: $idGrossistes
        idGeneriqueurs: $idGeneriqueurs
        contact: $contact
        entreeSortie: $entreeSortie
        satisfaction: $satisfaction
        segmentation: $segmentation
        compta: $compta
        informatique: $informatique
        achat: $achat
        cap: $cap
        concept: $concept
        chiffreAffaire: $chiffreAffaire
        digitales: $digitales
        ) {
            ...PharmacieInfo
        }
  }
  ${PHARMACIE_INFO_FRAGMENT}
`;

export const DO_DELETE_SOFT_PHARMACIES = gql`
  mutation DELETE_SOFT_PHARMACIES($ids: [ID!]!){
      deleteSoftPharmacies(ids: $ids){
          id
          sortie
      }
  }
  `;