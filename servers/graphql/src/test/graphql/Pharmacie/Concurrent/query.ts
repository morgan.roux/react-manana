import gql from 'graphql-tag';

export const GET_CONCURRENTS = gql`
  query CONCURRENTS {
    concurrents {
      id
    }
  }
`;