import gql from 'graphql-tag';
import { ROLE_INFO_FRAGEMENT } from './fragment';

export const GET_USER_ROLES = gql`
  query USER_ROLES {
    roles {
      ...RoleInfo
    }
  }
  ${ROLE_INFO_FRAGEMENT}
`;
