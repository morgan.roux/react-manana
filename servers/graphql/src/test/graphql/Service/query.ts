import gql from 'graphql-tag';

export const GET_SERVICES = gql`
  query GET_SERVICES {
    services {
      id
      code
      nom
    }
  }
`;