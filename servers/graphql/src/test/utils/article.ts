require('dotenv').config();
import { request } from 'graphql-request';
import { print } from 'graphql';
import faker from 'faker';
import { GET_TrTVA } from '../graphql/catalogueProduit/TrTVA/query';
import { GET_TrTableaux } from '../graphql/catalogueProduit/TrTableau/query';
import { GET_TrMarges } from '../graphql/catalogueProduit/TrMarge/query';
import { GET_TrFormes } from '../graphql/catalogueProduit/TrForme/query';
import { GET_TrStockages } from '../graphql/catalogueProduit/TrStockage/query';
import { GET_RbtArticles } from '../graphql/catalogueProduit/RbtArticle/query';
import { GET_TrActes } from '../graphql/catalogueProduit/TrActe/query';
import { GET_LABORATOIRES } from '../graphql/Laboratoire/query';
import { GET_TrFamille } from '../graphql/catalogueProduit/TrFamille/query';

export const trTvaCode = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_TrTVA),
).then((data: any) => {
    const result = data.trTVAs[0].codeTva;
    return result;
});

export const trTableauCode = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_TrTableaux),
).then((data: any) => {
    const result = data.trTableaux[0].codeTableau;
    return result;
});

export const trMargeCode = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_TrMarges),
).then((data: any) => {
    const result = data.trMarges[0].codeMarge;
    return result;
});

export const trFormeCode = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_TrFormes),
).then((data: any) => {
    const result = data.trFormes[0].codeForme;
    return result;
});

export const trStockageCode = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_TrStockages),
).then((data: any) => {
    const result = data.trStockages[0].codeStockage;
    return result;
});

export const rbtArticleCode = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_RbtArticles),
).then((data: any) => {
    const result = data.rbtArticles[0].codeRbt;
    return result;
});

export const trActeCode = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_TrActes),
).then((data: any) => {
    const index = data.trActes.length >= 1 ? 1 : 0;
    const result = data.trActes[index].codeActe;
    return result;
});

export const laboratoireCode = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_LABORATOIRES),
).then((data: any) => {
    const result = data.laboratoires[0].code;
    return result;
});

export const trFamilleCode = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_TrFamille),
).then((data: any) => {
    const result = data.trFamilles[0].codeFamille;
    return result;
});