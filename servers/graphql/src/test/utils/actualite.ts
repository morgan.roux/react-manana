require('dotenv').config();
import { request } from 'graphql-request';
import { print } from 'graphql';
import { GET_PHARMACIES } from '../graphql/Pharmacie/query';
import { GET_TITULAIRES } from '../graphql/Titulaire/query';
import { GET_SERVICES } from '../graphql/Service/query';


export const PharmaciesIdToTest = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_PHARMACIES),
).then((data: any) => {
    const result = data.pharmacies.map((item: any) => { return item.id }).slice(0, 6);
    return result;
});

export const presidentRegionsToTest = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_TITULAIRES),
).then((data: any) => {
    const result = data.titulaires.map((item: any) => { return item.id }).slice(0, 3);
    return ({
        presidents: result,
        type: 'PRESIDENT'
    })
});

export const servicesToTest = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_SERVICES),
).then((data: any) => {
    const result = data.services.map((item: any) => { return item.code }).slice(0, 3);
    return result;
})