import { createUser } from '../../repository/user';
import { prisma, UserStatus } from '../../generated/prisma-client';
import { ctx } from './context';

export const create = (email: string, password: string) => {
  return createUser({ email, password }, ctx);
};

export const updateStatus = (userId: string, status: UserStatus) => {
  // return updateUserStatus(ctx, { id: userId, status });
  return prisma.updateUser({ where: { id: userId }, data: { status, emailConfirmed: true } });
};

export const getUserByEmail = (email: string) => {
  return prisma.users({ where: { email } }).then(users => {
    return users && users.length > 0 ? users[0] : null;
  });
};

export const deleteUser = async (userId: string) => {
  return prisma.deleteUser({ id: userId });
};
