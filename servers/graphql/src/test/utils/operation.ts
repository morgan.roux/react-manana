require('dotenv').config();
import { request } from 'graphql-request';
import { print } from 'graphql';
import faker from 'faker';
import { GET_ITEMS } from '../graphql/Item/query';
import { GET_COMMANDETYPES } from '../graphql/CommandeType/query';
import { GET_COMMANDCANALS } from '../graphql/CommandeCanal/query';
import { GET_LABORATOIRES } from '../graphql/Laboratoire/query';
import { CanalArticlesIds } from './promotion';


export const codeItem = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_ITEMS),
).then((data: any) => {
    const result = data.items.length ? data.items[0].code : null;
    return result;
});

export const typeCommande = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_COMMANDETYPES),
).then((data: any) => {
    const result = data.commandeTypes.length ? data.commandeTypes[0].code : null;
    return result;
});

export const idCanalCommande = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_COMMANDCANALS),
).then((data: any) => {
    const result = data.commandeCanals.length ? data.commandeCanals[0].code : null;
    return result;
});

export const idLaboratoire = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_LABORATOIRES),
).then((data: any) => {
    const result = data.laboratoires.length ? data.laboratoires[0].id : null;
    return result;
});

export const articles = async () => {
    const idsArticle = await CanalArticlesIds();
    return idsArticle && idsArticle.map(id => ({ idCanalArticle: id, quantite: 12 }))
};