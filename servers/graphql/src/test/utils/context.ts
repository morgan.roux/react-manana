import { Context } from '../../types';
import { redis, indexing } from '../../server';
import { prisma } from '../../generated/prisma-client';
export const ctx: Context = {
  prisma,
  userId: null,
  role: null,
  redis,
  pubsub: null,
  indexing,
  groupementId: null,
  pharmacieId: null,
};
