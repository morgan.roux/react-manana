require('dotenv').config();
import { request } from 'graphql-request';
import { print } from 'graphql';
import faker from 'faker';
import { GET_COMMANDCANALS } from '../graphql/CommandeCanal/query';
import { GET_GROUPE_CLIENTS } from '../graphql/GroupeClient/query';
import { GET_CANAL_ARTICLES } from '../graphql/CanalArticle/query';

export enum PromotionType {
    AUTRE_PROMOTION = "AUTRE_PROMOTION",
    BONNE_AFFAIRE = "BONNE_AFFAIRE",
    LIQUIDATION = "LIQUIDATION",
    OFFRE_DU_MOIS = "OFFRE_DU_MOIS",
}

export enum PromotionStatus {
    ACTIF = "ACTIF",
    CLOTURE = "CLOTURE",
    NON_ACTIF = "NON_ACTIF",
}

export const CodeCanalToTest = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_COMMANDCANALS),
).then((data: any) => {
    const result = data.commandeCanals[0].code;
    return result;
});


const remiseDetail = () => ({
    quantiteMin: parseInt(`${faker.random.number(20)}`),
    quantiteMax: parseInt(`${faker.random.number({ max: 50, min: 21 })}`),
    pourcentageRemise: 0.4,
    remiseSupplementaire: 0.1,
});
export const GroupeClientsToTest = async () => {
    const codeGroupeClient = await request(
        `${process.env.API_URL_JEST}`,
        print(GET_GROUPE_CLIENTS),
    ).then((data: any) => {
        const result = data.groupeClients.length ? data.groupeClients[0].codeGroupe : null;
        return result;
    });

    const remiseDetails = [remiseDetail(), remiseDetail()];
    return ({
        codeGroupeClient,
        remiseDetails,
    })
};

export const CanalArticlesIds = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_CANAL_ARTICLES),
).then((data: any) => {
    const result = data.canalArticles.length ? data.canalArticles.map((item: any) => { return item.id }).slice(0, 3) : null;
    return result;
});