require('dotenv').config();
import { request } from 'graphql-request';
import { print } from 'graphql';
import faker from 'faker';
import moment from 'moment';
import { GET_GROSSISTES } from '../graphql/Grossiste/query';
import { GET_GENERIQUEURS } from '../graphql/Generiqueur/query';
import { GET_CONTRATS } from '../graphql/Pharmacie/Contrat/query';
import { GET_CONCURRENTS } from '../graphql/Pharmacie/Concurrent/query';
import { GET_MOTIFS } from '../graphql/Pharmacie/Motif/query';
import { TypeMotif } from '../../types';
import { GET_SMYLEYS } from '../graphql/Smyley/query';
import { GET_GROUPEMENTS } from '../graphql/Groupement/query';
import { GET_TYPOLOGIES } from '../graphql/Pharmacie/Typologie/query';
import { GET_TRANCHECAS } from '../graphql/Pharmacie/TrancheCA/query';
import { GET_QUALITES } from '../graphql/Pharmacie/Qualite/query';
import { GET_LOGICIELS } from '../graphql/Pharmacie/Logiciel/query';
import { GET_AUTOMATES } from '../graphql/Pharmacie/Automate/query';
import { GET_COMMANDCANALS } from '../graphql/CommandeCanal/query';
import { GET_PRESTATAIRE_PHARMACIES } from '../graphql/Pharmacie/PrestatairePharmacie/query';
import { GET_FACADES } from '../graphql/Pharmacie/Facade/query';
import { GET_SERVICE_PHARMACIES } from '../graphql/Pharmacie/ServicePharmacie/query';

export const idGrossistes = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_GROSSISTES),
).then((data: any) => {
    const result = data.grossistes.map((item: any) => { return item.id }).slice(0, 3);
    return result;
});

export const idGeneriqueurs = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_GENERIQUEURS),
).then((data: any) => {
    const result = data.generiqueurs.map((item: any) => { return item.id }).slice(0, 3);
    return result;
});

const idConcurrents = async () => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_CONCURRENTS),
).then((data: any) => {
    const result = data.concurrents.length ? data.concurrents.map((item: any) => { return item.id }).slice(0, 2) : null;
    return result;
});
const idMotifs = async (type) => await request(
    `${process.env.API_URL_JEST}`,
    print(GET_MOTIFS),
    {
        type: type,
    }
).then((data: any) => {
    const result = data.motifs.length ? data.motifs.map((item: any) => { return item.id }).slice(0, 2) : null;
    return result;
});
export const entreeSortie = async () => ({
    idContrat: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_CONTRATS),
    ).then((data: any) => {
        const result = data.contrats.length ? data.contrats[0].id : null;
        return result;
    }),
    idConcurent: await idConcurrents().then(item => item ? item[0] : item),
    idConcurentAncien: await idConcurrents().then(item => item ? item[1] : item),
    dateEntree: moment().toDate(),
    idMotifEntree: await idMotifs(TypeMotif[0]).then(item => item ? item[0] : item), // type E
    commentaireEntree: faker.lorem.words(),
    // sortieFuture: Int,
    dateSortieFuture: moment().add(6, 'days').toDate(),
    idMotifSortieFuture: await idMotifs(TypeMotif[1]).then(item => item ? item[0] : item), // type S
    commentaireSortieFuture: faker.lorem.words(),
    // sortie: Int,
    dateSortie: moment().add(5, 'days').toDate(),
    idMotifSortie: await idMotifs(TypeMotif[1]).then(item => item ? item[1] : item), // type S
    commentaireSortie: faker.lorem.words(),
    dateSignature: moment().add(4, 'days').toDate(),
});

export const satisfaction = async () => ({
    dateSaisie: moment().toDate(),
    commentaire: faker.lorem.words(),
    idSmyley: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_SMYLEYS),
        {
            idGroupement: await request(
                `${process.env.API_URL_JEST}`,
                print(GET_GROUPEMENTS),
            ).then((item: any) => item.groupements[0].id)
        }
    ).then((data: any) => {
        const result = data.smyleys.length ? data.smyleys[0].id : null;
        return result;
    }),
})

export const segmentation = async () => ({
    idTypologie: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_TYPOLOGIES),
    ).then((data: any) => {
        const result = data.typologies.length ? data.typologies[0].id : null;
        return result;
    }),
    idTrancheCA: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_TRANCHECAS),
    ).then((data: any) => {
        const result = data.trancheCAs.length ? data.trancheCAs[0].id : null;
        return result;
    }),
    idQualite: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_QUALITES),
    ).then((data: any) => {
        const result = data.qualites.length ? data.qualites[0].id : null;
        return result;
    }),
    idContrat: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_CONTRATS),
    ).then((data: any) => {
        const result = data.contrats.length ? data.contrats[0].id : null;
        return result;
    }),
    dateSignature: moment().toDate(),
})

export const compta = async () => ({
    siret: faker.random.uuid(),
    codeERP: faker.random.uuid(),
    ape: faker.random.uuid(),
    tvaIntra: faker.random.uuid(),
    rcs: faker.random.uuid(),
    contactCompta: faker.phone.phoneNumber(),
    chequeGrp: faker.random.boolean(),
    valeurChequeGrp: parseFloat(`${faker.random.number()}`),
    commentaireChequeGrp: faker.lorem.words(),
    droitAccesGrp: faker.random.boolean(),
    valeurChequeAccesGrp: parseFloat(`${faker.random.number()}`),
    commentaireChequeAccesGrp: faker.lorem.words(),
    structureJuridique: faker.lorem.word(),
    denominationSociale: faker.name.title(),
    telCompta: faker.phone.phoneNumber(),
    modifStatut: faker.random.boolean(),
    raisonModif: faker.lorem.words(),
    dateModifStatut: moment().toDate(),
    nomBanque: faker.name.title(),
    banqueGuichet: faker.random.word(),
    banqueCompte: faker.finance.account(),
    banqueRib: faker.finance.bic(),
    banqueCle: faker.finance.currencyCode(),
    dateMajRib: moment().toDate(),
    iban: faker.finance.iban(),
    swift: faker.lorem.words(),
    dateMajIban: moment().toDate(),
})

export const informatique = async () => ({
    idLogiciel: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_LOGICIELS),
    ).then((data: any) => {
        const result = data.logiciels.length ? data.logiciels[0].id : null;
        return result;
    }),
    numVersion: faker.random.uuid(),
    dateLogiciel: moment().toDate(),
    nbrePoste: faker.random.number(10),
    nbreComptoir: faker.random.number(10),
    nbreBackOffice: faker.random.number(10),
    nbreBureau: faker.random.number(10),
    commentaire: faker.lorem.words(),
    idAutomate1: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_AUTOMATES),
    ).then((data: any) => {
        const result = data.automates.length ? data.automates[0].id : null;
        return result;
    }),
    dateInstallation1: moment().toDate(),
    idAutomate2: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_AUTOMATES),
    ).then((data: any) => {
        const result = data.automates.length >= 1 ? data.automates[1].id : null;
        return result;
    }),
    dateInstallation2: moment().toDate(),
    idAutomate3: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_AUTOMATES),
    ).then((data: any) => {
        const result = data.automates.length >= 2 ? data.automates[2].id : null;
        return result;
    }),
    dateInstallation3: moment().toDate(),
})

export const achat = async () => ({
    dateDebut: moment().toDate(),
    dateFin: moment().add(6, 'days').toDate(),
    identifiantAchatCanal: faker.random.uuid(),
    commentaire: faker.lorem.words(),
    idCanal: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_COMMANDCANALS),
    ).then((data: any) => {
        const result = data.commandeCanals.length ? data.commandeCanals[0].id : null;
        return result;
    }),
})

export const cap = () => ({
    cap: faker.random.boolean(),
    dateDebut: moment().toDate(),
    dateFin: moment().add(6, 'days').toDate(),
    identifiantCap: faker.random.uuid(),
    commentaire: faker.lorem.words(),
})

export const concept = async () => ({
    idFournisseurMobilier: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_PRESTATAIRE_PHARMACIES),
        {
            type: 'M',
        }
    ).then((data: any) => {
        const result = data.prestatairePharmacies.length ? data.prestatairePharmacies[0].id : null;
        return result;
    }), // => type doit être M (Table Prestataire_pharmacie)
    dateInstallConcept: moment().toDate(),
    avecConcept: faker.random.boolean(),
    // idConcept: ID,
    avecFacade: faker.random.boolean(),
    dateInstallFacade: moment().toDate(),
    idEnseigniste: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_PRESTATAIRE_PHARMACIES),
        {
            type: 'E',
        }
    ).then((data: any) => {
        const result = data.prestatairePharmacies.length ? data.prestatairePharmacies[0].id : null;
        return result;
    }), // => type doit être E (Table Prestataire_pharmacie)
    conformiteFacade: faker.random.boolean(),
    idFacade: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_FACADES),
    ).then((data: any) => {
        const result = data.facades.length ? data.facades[0].id : null;
        return result;
    }),
    surfaceTotale: faker.random.number(),
    surfaceVente: faker.random.number(),
    SurfaceVitrine: faker.random.number(),
    volumeLeaflet: faker.random.number(),
    nbreLineaire: faker.random.number(),
    nbreVitrine: faker.random.number(),
    otcLibAcces: faker.random.boolean(),
    // idLeaflet: ID,
    commentaire: faker.lorem.words(),
})

export const chiffreAffaire = () => ({
    exercice: faker.random.number(),
    dateDebut: moment().toDate(),
    dateFin: moment().add(6, 'days').toDate(),
    caTTC: parseFloat(`${faker.random.number()}`),
    caHt: parseFloat(`${faker.random.number()}`),
    caTVA1: parseFloat(`${faker.random.number()}`),
    tauxTVA1: parseFloat(`${faker.random.number()}`),
    caTVA2: parseFloat(`${faker.random.number()}`),
    tauxTVA2: parseFloat(`${faker.random.number()}`),
    caTVA3: parseFloat(`${faker.random.number()}`),
    tauxTVA3: parseFloat(`${faker.random.number()}`),
    caTVA4: parseFloat(`${faker.random.number()}`),
    tauxTVA4: parseFloat(`${faker.random.number()}`),
    caTVA5: parseFloat(`${faker.random.number()}`),
    tauxTVA5: parseFloat(`${faker.random.number()}`),
})

export const digitale = async () => ({
    idServicePharmacie: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_SERVICE_PHARMACIES),
    ).then((data: any) => {
        const result = data.servicePharmacies.length ? data.servicePharmacies[0].id : null;
        return result;
    }),
    idPrestataire: await request(
        `${process.env.API_URL_JEST}`,
        print(GET_PRESTATAIRE_PHARMACIES),
        {
            type: 'D',
        }
    ).then((data: any) => {
        const result = data.prestatairePharmacies.length ? data.prestatairePharmacies[0].id : null;
        return result;
    }),
    flagService: false, // faker.random.boolean(),
    dateInstallation: '2020-08-25T14:08:43.344Z',   // moment().toDate(),
    url: 'https://austin.org', // faker.internet.url(),
})