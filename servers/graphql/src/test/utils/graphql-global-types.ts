/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum ActionStatus {
  ACTIVE = 'ACTIVE',
  CLOSED = 'CLOSED',
  DONE = 'DONE',
}

export enum ConnexionStatut {
  AUTHORIZED = 'AUTHORIZED',
  BLOCKED = 'BLOCKED',
  TO_CONFIRM = 'TO_CONFIRM',
}

export enum NotificationOrderByInput {
  content_ASC = 'content_ASC',
  content_DESC = 'content_DESC',
  dateCreation_ASC = 'dateCreation_ASC',
  dateCreation_DESC = 'dateCreation_DESC',
  dateDerniereModification_ASC = 'dateDerniereModification_ASC',
  dateDerniereModification_DESC = 'dateDerniereModification_DESC',
  id_ASC = 'id_ASC',
  id_DESC = 'id_DESC',
  seen_ASC = 'seen_ASC',
  seen_DESC = 'seen_DESC',
  type_ASC = 'type_ASC',
  type_DESC = 'type_DESC',
}

export enum ParamCategory {
  GROUPEMENT = 'GROUPEMENT',
  GROUPEMENT_SUPERADMIN = 'GROUPEMENT_SUPERADMIN',
  PHARMACIE = 'PHARMACIE',
  PLATEFORME = 'PLATEFORME',
  USER = 'USER',
}

export enum Sexe {
  F = 'F',
  M = 'M',
}

export enum SsoType {
  CRYPTAGE_AES256 = 'CRYPTAGE_AES256',
  CRYPTAGE_MD5 = 'CRYPTAGE_MD5',
  OTHER_CRYPTO = 'OTHER_CRYPTO',
  SAML = 'SAML',
  TOKEN_AUTHENTIFICATION = 'TOKEN_AUTHENTIFICATION',
  TOKEN_FTP_AUTHENTIFICATION = 'TOKEN_FTP_AUTHENTIFICATION',
}

export enum TypeFichier {
  AVATAR = 'AVATAR',
  EXCEL = 'EXCEL',
  LOGO = 'LOGO',
  PDF = 'PDF',
  PHOTO = 'PHOTO',
}

export enum TypePresidentCible {
  PHARMACIE = 'PHARMACIE',
  PRESIDENT = 'PRESIDENT',
}

export enum UserStatus {
  ACTIVATED = 'ACTIVATED',
  ACTIVATION_REQUIRED = 'ACTIVATION_REQUIRED',
  BLOCKED = 'BLOCKED',
  RESETED = 'RESETED',
}

export interface ArticleCommande {
  id: string;
  quantite: number;
}

export interface FichierInput {
  chemin: string;
  nomOriginal: string;
  type: TypeFichier;
}

export interface OperationArticleInput {
  idCanalArticle?: string | null;
  quantite?: number | null;
}

export interface ParameterInput {
  id: string;
  value?: string | null;
}

export interface PharmacieRolesInput {
  roles?: string[] | null;
  pharmacies?: string[] | null;
}

export interface PresidentRegionsInput {
  type?: TypePresidentCible | null;
  presidents?: string[] | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
