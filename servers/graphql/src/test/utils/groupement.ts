import {
  createGroupement,
  deleteGroupement,
  deleteGroupementLogo,
} from '../../repository/groupement';
import { ctx } from '../utils/context';
export const doCreateGroupement = ({
  nom,
  adresse1,
  adresse2,
  cp,
  ville,
  pays,
  telBureau,
  telMobile,
  mail,
  site,
  commentaire,
  nomResponsable,
  prenomResponsable,
  dateSortie,
  sortie,
}) => {
  return createGroupement(
    {
      nom,
      adresse1,
      adresse2,
      cp,
      ville,
      pays,
      telBureau,
      telMobile,
      mail,
      site,
      commentaire,
      nomResponsable,
      prenomResponsable,
      dateSortie,
      sortie,
    },
    ctx,
  );
};

export const doDeleteGroupement = (id: string) => {
  return deleteGroupement(id, ctx);
};

export const doDeleteGroupementLogo = (chemin: string) => {
  return deleteGroupementLogo({ chemin }, ctx);
};
