import { DocumentNode } from 'graphql';

export const convertGqlToString = (doc: DocumentNode) => {
  return doc.loc && doc.loc.source && doc.loc.source.body;
};
