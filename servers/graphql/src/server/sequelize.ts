require('dotenv').config();
import { Sequelize } from 'sequelize-typescript';

import * as models from '../sequelize/models';

const sequelize = new Sequelize({
  host: process.env.SEQUELIZE_HOST || 'localhost',
  port: parseInt(process.env.SEQUELIZE_PORT || process.env.POSTGRES_PORT || `5432`, 10),
  username: process.env.SEQUELIZE_USERNAME || process.env.POSTGRES_USER,
  password: process.env.SEQUELIZE_PASSWORD || process.env.POSTGRES_PASSWORD,
  database: process.env.SEQUELIZE_DATABASE || process.env.POSTGRES_DB,
  schema: process.env.SEQUELIZE_SCHEMA,
  dialect: 'postgres',
  dialectOptions: {
    requestTimeout: 3000,
  },
  models: Object.values(models),
  pool: {
    max: 10,
    min: 0,
    //   acquire: 60000,
    idle: 500,
  },
  retry: {
    max: 10,
  },
  sync: {
    force: false,
    alter: false,
  },
} as any);

export { sequelize };
