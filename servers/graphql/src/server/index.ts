require('dotenv').config();
import { sequelize } from './sequelize';

import { ApolloServer } from 'apollo-server-express';
import { makeExecutableSchema } from 'graphql-tools';
import cors from 'cors';
import { Client } from '@elastic/elasticsearch';
import Indexing from '../indexing';
import { reindexAll } from '../services/indexing';

import bodyParser from 'body-parser';
import http from 'http';

import Redis from 'ioredis';
import { isString } from 'util';
import express, { request } from 'express';

import { prisma } from '../generated/prisma-client';
import { resolvers, pubsub } from '../resolvers';
import logger from '../logging';
import { GraphQLClient } from 'graphql-request';

const PORT = process.env.API_PORT || 4000;
const redisConfig = {
  host: process.env.REDIS_HOST,
  port: +process.env.REDIS_PORT,
};

export const redis = new Redis(redisConfig);
import typeDefs from '../typeDefs';
import { cronUpateUserStatus } from '../repository/user';
const cron = require('node-cron');

/* const pathSchema = path.join(__dirname, '../schema.graphql');
fs.writeFile(pathSchema, typeDefs, function(err) {
  if (err) throw 'Merge schema graphql error: ' + err;
  console.log('Merge schema graphql saved');
}); */

const elasticClient = new Client({
  node: process.env.ELASTIC_SEARCH_NODE,
  ...(process.env.ELASTIC_SEARCH_CLOUD_ID &&
  process.env.ELASTIC_SEARCH_CLOUD_USERNAME &&
  process.env.ELASTIC_SEARCH_CLOUD_PASSWORD
    ? {
        cloud: {
          id: process.env.ELASTIC_SEARCH_CLOUD_ID,
          username: process.env.ELASTIC_SEARCH_CLOUD_USERNAME,
          password: process.env.ELASTIC_SEARCH_CLOUD_PASSWORD,
        },
      }
    : {}),
});

const getUserId = async (req, connection) => {
  if (connection) {
    return connection.context.userId;
  }
  if (isString(req)) {
    return redis.get(req);
  }
  if (req && req.headers && req.headers.authorization) {
    const userId = await redis.get(req.headers.authorization.replace('Bearer ', ''));
    return userId;
  }
  return -1;
};

const getIp = async (req, connection) => {
  if (connection) {
    return connection.context.ip;
  }
  if (req && req.headers && req.headers.ip) {
    return req.headers.ip;
  }
  return null;
};

const getGroupementId = async (req, connection) => {
  if (connection) {
    return connection.context.groupementId;
  }
  if (req && req.headers && req.headers.idgroupement) {
    return req.headers.idgroupement;
  }
  return null;
};

const getPharmacieId = async (req, connection) => {
  if (connection) {
    return connection.context.pharmacieId;
  }

  if (req && req.headers && req.headers.idpharmacie) {
    return req.headers.idpharmacie;
  }
  return null;
};

const getDomaineActiviteId = async (req, connection) => {
  if (connection) {
    return connection.context.domaineActiviteId;
  }

  if (req && req.headers && req.headers.iddomaineactivite) {
    return req.headers.iddomaineactivite;
  }
  return null;
};

const refreshToken = async (req, connection, userId) => {
  if (connection) {
    return;
  }
  if (isString(req)) {
    redis.set(req, userId);
    return;
  }
  if (req && req.headers && req.headers.authorization) {
    redis.set(req.headers.authorization.replace('Bearer ', ''), userId);
    return;
  }
};
export const schema = makeExecutableSchema({
  typeDefs,
  resolvers: { ...(resolvers as any) },
});

const sensitiveInformations = (variables: any) => {
  if (variables) {
    // Replace password by *****
    return Object.keys(variables).reduce((result, currentKey) => {
      return {
        ...result,
        [currentKey]:
          currentKey === 'password'
            ? '*'.repeat(`${variables[currentKey]}`.length)
            : variables[currentKey],
      };
    }, {});
  }

  return variables;
};

/*
  Disable rabbitmq
  connect(process.env.RABBITMQ_HOST)
  .then(connection => {})
*/

export const indexing = new Indexing(elasticClient);

indexing.start('true' === process.env.ENABLE_REINDEX_ALL);

export const context = async ({ req, connection }) => {
  const userId = await getUserId(req, connection);
  const groupementId = await getGroupementId(req, connection);
  const pharmacieId = await getPharmacieId(req, connection);
  const role = await prisma.userRoles({ where: { idUser: { id: userId } } }).then(async userRoles =>
    userRoles && userRoles.length
      ? prisma
          .userRole({ id: userRoles[0].id })
          .idRole()
          .code()
      : null,
  );
  refreshToken(req, connection, userId);
  return { prisma, redis, indexing, userId, groupementId, pharmacieId, pubsub, role };
};

const server = new ApolloServer({
  schema,
  formatResponse: (response, requestContext) => {
    const ip = requestContext?.request.http?.headers.get('ip') || '';
    const token = (requestContext?.request.http.headers.get('authorization') || '').replace(
      'Bearer ',
      '',
    );
    const userAgent = requestContext?.request.http?.headers.get('user-agent') || '';
    const idUser = (requestContext.context as any).userId;
    const idPharmacie = requestContext?.request.http?.headers.get('idPharmacie') || '';
    const idGroupement = requestContext?.request.http?.headers.get('idGroupement') || '';
    const operationName = requestContext.request.operationName;
    const status = response.errors ? 'ERROR' : 'OK';
    const variables = requestContext.request.variables;
    const error = response.errors;
    const extensions = response.extensions;
    const data = response.data;
    const query = requestContext?.request.query || ``;
    const responseTime = (requestContext.context as any).startTime
      ? new Date().getTime() - (requestContext.context as any).startTime
      : 0;

    if (status === 'ERROR') {
      const message = {
        ip,
        token,
        userAgent,
        idUser,
        idPharmacie,
        idGroupement,
        operationName,
        query,
        status,
        variables,
        error,
        extensions,
        data,
        responseTime,
      };
      logger.error(JSON.stringify(message));
    } else {
      const message = {
        ip,
        token,
        userAgent,
        idUser,
        idPharmacie,
        idGroupement,
        operationName,
        status,
        variables: sensitiveInformations(variables),
        error,
        extensions,
        responseTime,
      };
      logger.info(JSON.stringify(message));
    }

    return response;
  },
  context: async ({ req, connection }) => {
    const userId = await getUserId(req, connection);
    const groupementId = await getGroupementId(req, connection);
    const pharmacieId = await getPharmacieId(req, connection);
    const domaineActiviteId = await getDomaineActiviteId(req, connection);
    const ip = await getIp(req, connection);
    const startTime = new Date().getTime();
    const role = await prisma
      .userRoles({ where: { idUser: { id: userId } } })
      .then(async userRoles =>
        userRoles && userRoles.length
          ? prisma
              .userRole({ id: userRoles[0].id })
              .idRole()
              .code()
          : null,
      );
    refreshToken(req, connection, userId);
    return {
      prisma,
      redis,
      indexing,
      userId,
      groupementId,
      pharmacieId,
      pubsub,
      role,
      ip,
      startTime,
      domaineActiviteId,
    };
  },
  subscriptions: {
    path: '/',
    onConnect: async (connectionParams: any, webSocket, context) => {
      const userId = await redis.get(connectionParams.authToken);
      const startTime = new Date().getTime();

      const ip = context.request.socket.remoteAddress;
      logger.info(`userId ${userId}`);
      if (connectionParams.authToken) {
        redis.set(`user:${userId}:lastSeen`, 0);
        return { userId, ip, startTime };
      }

      logger.error('Missing auth token!');
      return { ip, startTime };
    },
    onDisconnect: async (connectionParams, context) => {
      const c = await context.initPromise;
      const { userId } = c;

      if (userId) {
        redis.set(`user:${userId}:lastSeen`, new Date().getTime());
        return { userId };
      }
      return {};
    },
  },
  introspection: true, // TODO : Disable it in production
  playground: true,
});

// Express initialization
const app = express();

// Routes initialization
app.use(bodyParser.json());

// app.use(
//   cors({
//     origin(origin, cb) {
//       const whitelist = process.env.CORS_ORIGIN
//         ? process.env.CORS_ORIGIN.split(',')
//         : [];
//       cb(null, whitelist.includes(origin));
//     },
//     credentials: true
//   })
// );

app.use(cors());

server.applyMiddleware({ app });
const httpServer = http.createServer(app);

server.installSubscriptionHandlers(httpServer);

if ('true' === process.env.ENABLE_REINDEX_ALL) {
  console.log('============== Start reindexing all data ==============');
  reindexAll({
    prisma,
    redis,
    userId: null,
    role: null,
    groupementId: '---',
    pharmacieId: '---',
    domaineActiviteId: '---',
    indexing,
    pubsub,
    ip: '',
    startTime: new Date().getTime(),
  })
    .then(_ => console.log('================ Reindexing all data ok =============='))
    .catch(error =>
      console.error(`===================Reindexing all data error=============== ${error.message}`),
    );
}

/*  CRON SCRIPT CHECK USER TO RESETED  */
cron.schedule('0 0 * * *', function() {
  cronUpateUserStatus(prisma);
});

export class GraphQLServer {
  graphqlEndPoint() {
    const addr = httpServer.address() as any;
    const protocol = 'http';

    if (!addr) {
      throw new Error('Server is not listening');
    }

    // If address is "unroutable" IPv4/6 address, then set to localhost
    if (addr.address === '0.0.0.0' || addr.address === '::') {
      addr.address = '127.0.0.1';
    }

    return `${protocol}://${addr.address}:${addr.port}/graphql`;
  }

  start() {
    httpServer.listen(PORT, () => {
      console.log(`🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`);
      console.log(`🚀 Subscriptions ready at ws://localhost:${PORT}${server.subscriptionsPath}`);
    });
    sequelize.sync({ force: false }).then(() => {
      console.log(`🚀 Database ready`);
    });
  }

  stop() {
    httpServer.close();
  }
}

app.get('/health', async (req, res) => {
  const endpoint = process.env.API_URL;
  const client = new GraphQLClient(endpoint, {
    credentials: 'include',
    mode: 'cors',
  });

  return res.json(
    await client
      .request(
        `query HEALTH_CHECK {
    healthCheck {
      postgresDb {
        status
        usersContains
      }
      serviceS3 {
        status
        host
      }
      error
    }
  }`,
      )
      .then(data => data)
      .catch(error => error),
  );
});
