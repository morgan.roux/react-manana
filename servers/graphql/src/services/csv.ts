import fs = require('fs');
import { uploadFileS3FromTo } from './uploadFile';
import config from './config';
import csv = require('csv-writer');
const createCsvWriter = csv.createObjectCsvWriter;

/*
header: [ {id: 'name', title: 'NAME'} ]
data = [ {name: 'gcr-pharma'} ]
*/
interface IHeader {
  id: string;
  title: string;
}

// directory => directory in s3
export const createCsv = (
  header: IHeader[],
  data: any,
  file: string,
  directory: string,
): string => {
  if (directory === '') directory = 'others';
  const path = `${config.tempDir}/${directory}/${file}`;

  fs.mkdir(`${config.tempDir}/${directory}`, { recursive: true }, err => {
    if (err) {
      console.log(`Error creating directory: ${err}`);
    } else {
      fs.openSync(path, 'w');

      const csvWriter = createCsvWriter({
        path,
        header,
        fieldDelimiter: ';',
      });
      // returns a promise
      csvWriter.writeRecords(data).then(() => {
        uploadFileS3FromTo(path, `${directory}/${file}`);
        console.log(`done ${file}`);
      });
    }
  });
  return `${directory}/${file}`;
};
