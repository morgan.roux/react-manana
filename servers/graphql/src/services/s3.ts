import v4 = require('aws-signature-v4');
import S3 from 'aws-sdk/clients/s3';
import xlsx from 'node-xlsx';
import logger from '../logging';

interface Params {
  Bucket: string;
  Key: string;
}

export default {
  createPresignedPutUrl: (filePath: any) => {
    try {
      return v4.createPresignedS3URL(filePath.replace(/ |-|\)|\(/g, ''), {
        key: process.env.AWS_S3_ACCESS_KEY,
        secret: process.env.AWS_S3_SECRET_KEY,
        expires: 3600,
        method: 'PUT',
        bucket: process.env.AWS_S3_BUCKET,
        region: process.env.AWS_S3_REGION,
      });
    } catch (error) {
      return error;
    }
  },
  createPresignedGetUrl: (filePath: any) => {
    try {
      return v4.createPresignedS3URL(filePath.replace(/ |-|\)|\(/g, ''), {
        key: process.env.AWS_S3_ACCESS_KEY,
        secret: process.env.AWS_S3_SECRET_KEY,
        expires: 3600,
        method: 'GET',
        bucket: process.env.AWS_S3_BUCKET,
        region: process.env.AWS_S3_REGION,
      });
    } catch (error) {
      return error;
    }
  },
  readXlsxFromAWS: async (file: string): Promise<any> => {
    const params = {
      Bucket: process.env.AWS_S3_BUCKET,
      Key: file,
    };
    const s3 = new S3({
      region: process.env.AWS_S3_REGION,
      credentials: {
        accessKeyId: process.env.AWS_S3_ACCESS_KEY,
        secretAccessKey: process.env.AWS_S3_SECRET_KEY,
      },
    });

    const data = s3.getObject(params).promise();

    return data
      .then(value => xlsx.parse(value.Body))
      .catch(reason => logger.error('REASON: ' + JSON.stringify(reason)));
  },
  getS3Object: (): S3 => {
    return new S3({
      region: process.env.AWS_S3_REGION,
      credentials: {
        accessKeyId: process.env.AWS_S3_ACCESS_KEY,
        secretAccessKey: process.env.AWS_S3_SECRET_KEY,
      },
    });
  },
  getS3Params: (file: string): Params => {
    const fileInfo = file.split('/');
    return {
      Bucket: process.env.AWS_S3_BUCKET,
      Key: `${fileInfo[0]}/${fileInfo[1]}`,
    };
  },
};
