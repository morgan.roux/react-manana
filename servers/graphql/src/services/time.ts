import { performance as performe, Performance } from 'perf_hooks';
import logger from '../logging';
import moment from 'moment';

export class WatchTime {
  startTime: number;
  stopTime: number;
  running: boolean;
  isPerformance: boolean;
  performance: Performance;

  constructor() {
    this.startTime = 0;
    this.stopTime = 0;
    this.running = false;
    this.isPerformance = !!performe;
    this.performance = performe;
  }

  currentTime = function() {
    return this.isPerformance ? this.performance.now() : new Date().getTime();
  };

  start = function() {
    this.startTime = this.currentTime();
    this.running = true;
  };

  stop = function() {
    this.stopTime = this.currentTime();
    this.running = false;
  };

  getElapsedMilliseconds = function() {
    if (this.running) {
      this.stopTime = this.currentTime();
    }

    return this.stopTime - this.startTime;
  };

  getElapsedSeconds = function(numberAfterVirgule?: number) {
    if (numberAfterVirgule) {
      return (this.getElapsedMilliseconds() / 1000).toFixed(numberAfterVirgule);
    }
    return this.getElapsedMilliseconds() / 1000;
  };

  printElapsed = function(name) {
    const currentName = name || 'Elapsed ====> ';
    logger.info(
      `${currentName} ${this.getElapsedSeconds()} s  ou (${moment
        .duration(this.getElapsedSeconds() / 60, 'minutes')
        .humanize()})`,
    );
  };

  printElapsedMinute = function(name) {
    const currentName = name || 'Elapsed ====>';
    logger.info(
      `${currentName} (${moment.duration(this.getElapsedSeconds() / 60, 'minutes').humanize()})`,
    );
  };

  printElapsedSeconds = function(name) {
    const currentName = name || 'Elapsed ====>';
    logger.info(`${currentName} (${this.getElapsedSeconds(2)} s)`);
  };
}
