/* Get moth two digits */
export const dateMonthTwoDigits = date => {
  const month = date.getMonth() + 1;
  return month < 10 ? '0' + month : '' + month;
};
