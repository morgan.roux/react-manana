import { Context } from './../types';
export const getComments = async (
  skip: number,
  first: number,
  idSource: string,
  context: Context,
) => {
  // Get total
  const totalCount = await context.prisma
    .commentsConnection()
    .aggregate()
    .count();

  return context.prisma
    .comments({
      skip: skip || 0,
      first: first || 10,
      orderBy: 'dateCreation_DESC',
      where: { idItemAssocie: idSource },
    })
    .then(comments => {
      return { total: totalCount, data: comments } as any;
    });
};
