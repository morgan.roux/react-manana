/* Add something before your value */
export const pad = (toPad, padChar, length) => {
  const s = String(toPad);
  return s.length < length ? new Array(length - s.length + 1).join(padChar) + s : s;
};
