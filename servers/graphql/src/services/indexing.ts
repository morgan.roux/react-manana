import { Context } from './../types';
import { WatchTime } from './time';
import { chunk } from 'lodash';

const watchTime = new WatchTime();

const ROOT_CONFIGS = [
  {
    entity: 'titulaires',
    id: 'id',
    type: 'titulaire',
    routingKey: 'titulaires',
  },
  {
    entity: 'pharmacies',
    id: 'id',
    type: 'pharmacie',
    routingKey: 'pharmacies',
  },
  {
    entity: 'produits',
    id: 'id',
    type: 'produit',
    routingKey: 'produits',
  },
];

const CONFIGS = [
  {
    entity: 'produitCanals',
    id: 'id',
    type: 'produitcanal',
    routingKey: 'produitcanals',
  },
  {
    entity: 'operations',
    id: 'id',
    type: 'operation',
    routingKey: 'operations',
  },
  {
    entity: 'personnels',
    id: 'id',
    type: 'personnel',
    routingKey: 'personnels',
  },
  {
    entity: 'ppersonnels',
    id: 'id',
    type: 'ppersonnel',
    routingKey: 'ppersonnels',
  },
  {
    entity: 'groupements',
    id: 'id',
    type: 'groupement',
    routingKey: 'groupements',
  },
  {
    entity: 'commandes',
    id: 'id',
    type: 'commande',
    routingKey: 'commandes',
  },
  {
    entity: 'commandeLignes',
    id: 'id',
    type: 'commandeligne',
    routingKey: 'commandelignes',
  },
  {
    entity: 'projects',
    id: 'id',
    type: 'project',
    routingKey: 'projects',
  },
  {
    entity: 'canalGammes',
    id: 'id',
    type: 'canalgamme',
    routingKey: 'canalgammes',
  },
  {
    entity: 'actualites',
    id: 'id',
    type: 'actualite',
    routingKey: 'actualites',
  },
  {
    entity: 'commandeCanals',
    id: 'id',
    type: 'commandecanal',
    routingKey: 'commandecanals',
  },
  {
    entity: 'commandeTypes',
    id: 'id',
    type: 'commandetype',
    routingKey: 'commandetypes',
  },
  {
    entity: 'actualiteOrigines',
    id: 'id',
    type: 'actualiteorigine',
    routingKey: 'actualiteorigines',
  },
  {
    entity: 'users',
    id: 'id',
    type: 'user',
    routingKey: 'users',
  },
  {
    entity: 'familles',
    id: 'id',
    type: 'famille',
    routingKey: 'familles',
  },
  {
    entity: 'laboratoires',
    type: 'laboratoire',
    id: 'id',
    routingKey: 'laboratoires',
  },
  {
    entity: 'services',
    type: 'service',
    id: 'id',
    routingKey: 'services',
  },
  {
    entity: 'partenaires',
    type: 'partenaire',
    id: 'id',
    routingKey: 'partenaires',
  },
  {
    entity: 'publicites',
    type: 'publicite',
    id: 'id',
    routingKey: 'publicites',
  },
  {
    entity: 'suiviPublicitaires',
    type: 'suivipublicitaire',
    id: 'id',
    routingKey: 'suivipublicitaires',
  },
  {
    entity: 'items',
    type: 'item',
    id: 'id',
    routingKey: 'items',
  },
  {
    entity: 'actes',
    type: 'acte',
    id: 'id',
    routingKey: 'actes',
  },
  {
    entity: 'tVAs',
    type: 'tva',
    id: 'id',
    routingKey: 'tvas',
  },
  {
    entity: 'groupeClients',
    type: 'groupeclient',
    id: 'id',
    routingKey: 'groupeclients',
  },
  {
    entity: 'promotions',
    type: 'promotion',
    id: 'id',
    routingKey: 'promotions',
  },
  {
    entity: 'gammeCatalogues',
    type: 'gammecatalogue',
    id: 'id',
    routingKey: 'gammecatalogues',
  },
  {
    entity: 'marches',
    type: 'marche',
    id: 'id',
    routingKey: 'marches',
  },
  {
    entity: 'avatars',
    type: 'avatar',
    id: 'id',
    routingKey: 'avatars',
  },
  {
    entity: 'roles',
    type: 'role',
    id: 'id',
    routingKey: 'roles',
  },
  {
    entity: 'traitements',
    type: 'traitement',
    id: 'id',
    routingKey: 'traitements',
  },
  {
    entity: 'listes',
    type: 'liste',
    id: 'id',
    routingKey: 'listes',
  },
  {
    entity: 'libelleDiverses',
    type: 'libelledivers',
    id: 'id',
    routingKey: 'libellediverses',
  },
  {
    entity: 'tauxsses',
    type: 'tauxss',
    id: 'id',
    routingKey: 'tauxsses',
  },
  {
    entity: 'personnelAffectations',
    type: 'personnelaffectation',
    id: 'id',
    routingKey: 'personnelaffectations',
  },
  {
    entity: 'titulaireAffectations',
    type: 'titulaireaffectation',
    id: 'id',
    routingKey: 'titulaireaffectations',
  },
  {
    entity: 'ressources',
    type: 'ressource',
    id: 'id',
    routingKey: 'ressources',
  },
  {
    entity: 'partenaireRepresentants',
    type: 'partenairerepresentant',
    id: 'id',
    routingKey: 'partenairerepresentants',
  },
  {
    entity: 'partenaireRepresentantAffectations',
    type: 'partenairerepresentantaffectation',
    id: 'id',
    routingKey: 'partenairerepresentantaffectations',
  },
  {
    entity: 'tickets',
    type: 'ticket',
    id: 'id',
    routingKey: 'tickets',
  },
  {
    entity: 'aides',
    type: 'aide',
    id: 'id',
    routingKey: 'aides',
  },
  {
    entity: 'actions',
    type: 'action',
    id: 'id',
    routingKey: 'actions',
  },
  {
    entity: 'couleurs',
    type: 'couleur',
    id: 'id',
    routingKey: 'couleurs',
  },
  {
    entity: 'todoEtiquettes',
    type: 'todoetiquette',
    id: 'id',
    routingKey: 'todoetiquettes',
  },
  {
    entity: 'todoSections',
    type: 'todosection',
    id: 'id',
    routingKey: 'todosections',
  },
  {
    entity: 'informationLiaisons',
    type: 'informationliaison',
    id: 'id',
    routingKey: 'informationliaisons',
  },
  {
    entity: 'groupeAmises',
    type: 'groupeamis',
    id: 'id',
    routingKey: 'groupeamises',
  },
  {
    entity: 'partages',
    type: 'partage',
    id: 'id',
    routingKey: 'partages',
  },
  {
    entity: 'ideeOuBonnePratiques',
    type: 'ideeoubonnepratique',
    id: 'id',
    routingKey: 'ideeoubonnepratiques',
  },
  {
    entity: 'comments',
    type: 'comment',
    id: 'id',
    routingKey: 'comments',
  },
  {
    entity: 'groupeAmisDetails',
    type: 'groupeamisdetail',
    id: 'id',
    routingKey: 'groupeamisdetails',
  },
  {
    entity: 'informationLiaisonUserConcernees',
    type: 'informationliaisonuserconcernee',
    id: 'id',
    routingKey: 'informationliaisonuserconcernees',
  },
  {
    entity: 'laboratoireRepresentants',
    type: 'laboratoirerepresentant',
    id: 'id',
    routingKey: 'laboratoirerepresentants',
  },
  {
    entity: 'laboratoireRessources',
    type: 'laboratoireressource',
    id: 'id',
    routingKey: 'laboratoireressources',
  },
];
/* REINDEX */
export const reindex = (
  context: Context,
  id: string,
  entity: string,
  routingKey: string,
  skip: number,
  first: number,
  prod: boolean = false,
  reindexAll: boolean = false,
) => {
  return context.prisma[entity]({ skip, first }).thitemsen(async (list: any) => {
    if (reindexAll) {
      return Promise.all(
        list.map((item: any) => {
          if (item && item[id]) {
            return context.indexing.publishSave(routingKey, item[id]);
          }
          return Promise.resolve();
        }),
      ).then(_ => {
        if (list.length < first) {
          return Promise.resolve();
        }
        return reindex(context, id, entity, routingKey, skip + first, first, prod, reindexAll);
      });
    } else {
      return Promise.all(
        list.map(async (item: any) => {
          if (item && item[id]) {
            return context.indexing.publishSave(routingKey, item[id]);
          }

          return Promise.resolve();
        }),
      ).then(_ => {
        if (list.length < first) {
          return Promise.resolve();
        }
        return reindex(context, id, entity, routingKey, skip + first, first, prod, reindexAll);
      });
    }
  });
};

const _doGetIndexIndsFromDatabase = async (
  context: Context,
  id: string,
  entity: string,
  skip: number,
  first: number,
): Promise<string[]> => {
  return context.prisma[entity]({ skip, first }).then(async (list: any) => {
    let ids: string[] = list.filter((item: any) => item && item[id]).map((item: any) => item[id]);
    if (list.length >= first) {
      ids = [
        ...ids,
        ...(await _doGetIndexIndsFromDatabase(context, id, entity, skip + first, first)),
      ];
    }
    return ids;
  });
};

const _getIndexIndsFromDatabase = async (
  context: Context,
  id: string,
  entity: string,
): Promise<string[]> => {
  return _doGetIndexIndsFromDatabase(context, id, entity, 0, 100);
};

const _goGetIndexIdsFromElasticSearch = async (
  context: Context,
  index: string,
  from: number,
  size: number,
  count: number,
): Promise<string[]> => {
  if (from < count) {
    return context.indexing
      .getElasticSearchClient()
      .search({ index, body: { stored_fields: [], query: { match_all: {} } } })
      .then(async result => {
        const hits = result.body.hits.hits;
        const ids = hits.map(({ _id }: any) => _id);
        return [
          ...ids,
          ...(await _goGetIndexIdsFromElasticSearch(context, index, from + size, size, count)),
        ];
      })
      .catch(err => {
        console.log('error ger id from elsaticsearch', index, ' : ', err);
        return [];
      });
  }

  return Promise.resolve([]);
};

const _getIndexIdsFromElasticSearch = async (context: Context, type: string): Promise<string[]> => {
  return context.indexing
    .getElasticSearchClient()
    .count({ index: type, body: { query: { match_all: {} } } })
    .then(result => {
      const count = result.body.count;
      return _goGetIndexIdsFromElasticSearch(context, type, 0, 500, count);
    });
};

const _publish = async (
  context: Context,
  ids: string[],
  routingKey: string,
  action: 'SAVE' | 'DELETE',
): Promise<any> => {
  // Chunked
  const chunkedIds = chunk(ids, 50);
  return chunkedIds.reduce((previous, currentIds) => {
    return previous.then(() => {
      return Promise.all(currentIds.map(id => context.indexing.publish(routingKey, action, id)));
    });
  }, Promise.resolve());
};

export const reindexWithPrefetchIds = async (
  context: Context,
  id: string,
  entity: string,
  routingKey: string,
  type: string,
): Promise<any> => {
  return Promise.all([
    _getIndexIndsFromDatabase(context, id, entity),
    _getIndexIdsFromElasticSearch(context, type),
  ]).then(([idsFromDataBase, idsFromElasticSearch]) => {
    const toDeleteIds: string[] = idsFromElasticSearch.filter(id => !idsFromDataBase.includes(id));
    return Promise.all([
      _publish(context, toDeleteIds, routingKey, 'DELETE'),
      _publish(context, idsFromDataBase, routingKey, 'SAVE'),
    ]);
  });
};

export const reindexDatas = async (LIST_CONFIGS, context: Context) => {
  return LIST_CONFIGS.reduce((previousPromise, { entity, routingKey, id, type }: any) => {
    return previousPromise.then(() => {
      watchTime.start();

      return reindexWithPrefetchIds(context, id, entity, routingKey, type).then(res => {
        watchTime.stop();
        watchTime.printElapsed(`Indexing ${type} duration : `);
        return res;
      });
    });
  }, Promise.resolve());
};

export const reindexAll = async (context: Context) => {
  return reindexDatas(ROOT_CONFIGS, context).then(_ => reindexDatas(CONFIGS, context));
};

export const getConfig = (routingKey: string) => {
  const list = [...ROOT_CONFIGS, ...CONFIGS].filter(config => config.routingKey === routingKey);
  return list && list.length ? list[0] : null;
};
