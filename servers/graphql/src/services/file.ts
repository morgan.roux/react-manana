import Minio = require('minio');
import logger from '../logging';
import S3 from 'aws-sdk/clients/s3';
import Excel = require('exceljs');

const s3Client = new Minio.Client({
  endPoint: process.env.AWS_S3_ENDPOINT,
  accessKey: process.env.AWS_S3_ACCESS_KEY,
  secretKey: process.env.AWS_S3_SECRET_KEY,
});

export const deleteS3File = (filePath: string) => {
  // tslint:disable-next-line: only-arrow-functions
  return s3Client.removeObject(process.env.AWS_S3_BUCKET, filePath, function(err) {
    if (err) {
      return logger.error(`Suppression du ficher avec error : ${err}`);
    }
    logger.info(`Fichier supprimer avec succès.`);
  });
};
