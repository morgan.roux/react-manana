import { Context } from '../types';
import logger from '../logging';

interface SearchArgs {
  query: any;
  filterBy?: any;
  sortBy?: any;
  skip?: number;
  take?: number;
  type: string[];
  include?: string[];
  exclude?: string[];
}

const isNumber = (value: string) => {
  try {
    return !isNaN(parseFloat(value));
  } catch (error) {
    return false;
  }
};

export const search = async (
  { skip, take, query, type, filterBy, sortBy, include, exclude }: SearchArgs,
  ctx: Context,
) => {
  const from = skip || 0;
  const size = take || 100;
  const searchParams = { from, size, body: {} };

  if (type) {
    // Filter by types (indices in elasticsearch).
    searchParams['index'] = type;
  }

  if (sortBy) {
    // https://www.elastic.co/guide/en/elasticsearch/reference/7.4/search-request-body.html#request-body-search-sort
    searchParams['body']['sort'] = JSON.parse(JSON.stringify(sortBy));
  }

  if (query) {
    // https://www.elastic.co/guide/en/elasticsearch/reference/current//search.html
    if (typeof query === 'string') {
      query = query.replace(/[&\/\\#,+()$~%.'":\-?<>{}]/g, ' ');
      if (isNumber(query)) query = `"${query}"`;
      searchParams['body'] = {
        ...searchParams['body'],
        query: {
          bool: {
            must: {
              query_string: {
                query: query.startsWith('*') || query.endsWith('*') ? query : `*${query}*`,
              },
            },
          },
        },
      };
    } else {
      searchParams['body'] = { ...searchParams['body'], ...JSON.parse(JSON.stringify(query)) };
    }
  }

  if (filterBy) {
    // https://www.elastic.co/guide/en/elasticsearch/reference/current//query-filter-context.html
    searchParams['body']['query'] = searchParams['body']['query'] || {};
    searchParams['body']['query']['bool'] = {
      ...((searchParams['body'] &&
        searchParams['body']['query'] &&
        searchParams['body']['query']['bool']) ||
        {}),
      filter: JSON.parse(JSON.stringify(filterBy)), // Why :to remove [Object: null prototype] { term: [Object: null prototype]
    };
  }

  if (include || exclude) {
    searchParams['body']['_source'] = {
      include: include || ['*'],
      ...(exclude ? { exclude } : {}),
    };
  }

  return ctx.indexing
    .getElasticSearchClient()
    .search(searchParams)
    .then(result => {
      return {
        total: result.body.hits.total.value,
        data: result.body.hits.hits.map(({ _index, _id, _source }) => ({
          type: _index,
          _id,
          ..._source,
        })),
      };
    });
};
