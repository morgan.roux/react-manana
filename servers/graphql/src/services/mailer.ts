require('dotenv').config();
import SparkPost = require('sparkpost');
import sendgrid = require('@sendgrid/mail');

interface MailMessage {
  from?: string;
  to: string[] | string;
  cc?: string;
  bcc?: string;
  templateId: string;
  substitutionData: any;
}

const sparky = new SparkPost(process.env.SPARKPOST_API_KEY, {
  endpoint: process.env.SPARKPOST_ORIGIN,
});

sendgrid.setApiKey(process.env.SENDGRID_API_KEY);

export const sendMail = async (message: MailMessage) => {
  console.log('message :>> ', message);
  console.log('message.templateId :>> ', message.templateId);
  console.log(
    'process.env.USE_SENDGRID :>> ',
    process.env.USE_SENDGRID,
    typeof process.env.USE_SENDGRID,
  );

  if (message.templateId) {
    return 'true' === process.env.USE_SENDGRID
      ? _sendGridEmail(message)
      : _sendSparkPostEmail(message);
  }
};

const _sendSparkPostEmail = (message: MailMessage) => {
  return sparky.transmissions.send({
    content: {
      template_id: message.templateId,
      use_draft_template: true,
    },
    options: {
      sandbox: false,
    },
    recipients: (typeof message.to === 'string' ? [message.to] : message.to).map((address) => {
      return { address };
    }),
    substitution_data: {
      ...message.substitutionData,
      clientUrl: process.env.CLIENT_URL.replace('https://', '').replace('http://', ''),
    },
  });
};

const _sendGridEmail = (message: MailMessage) => {
  const multiple = Array.isArray(message.to) ? true : false;
  const from = { email: message.from || process.env.EMAIL_FROM, name: 'Digital4win' };

  console.log('message ****::::>>>> ', message);
  console.log('message.to ****::::>>>> ', message.to);
  console.log('multiple ****::::>>>> ', multiple);
  console.log('message.from ****::::>>>> ', message.from);
  console.log('from ****::::>>>> ', from);

  return sendgrid.send(
    {
      to: message.to,
      cc: message.cc ? message.cc : undefined,
      bcc: message.bcc ? message.bcc : undefined,
      from,
      templateId: message.templateId,
      dynamicTemplateData: {
        ...message.substitutionData,
        clientUrl: process.env.CLIENT_URL.replace('https://', '').replace('http://', ''),
      },
    },
    multiple,
  );
};
