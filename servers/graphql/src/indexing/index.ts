import { Client } from '@elastic/elasticsearch';
import logger from '../logging';
import Indexer, { MappingProperties } from './Indexer';
import fs = require('fs');
import fetch = require('isomorphic-fetch');

interface IndexerDeploymentOptions {
  routingKey: string;
  index: string;
  mappingProperties?: MappingProperties;
  instances?: number | 1;
}

export default class Indexing {
  elasticSearchClient: Client;
  indexers: { [routingKey: string]: Indexer };
  constructor(elasticSearchClient: Client) {
    this.elasticSearchClient = elasticSearchClient;
    this.indexers = {};
    this._addCloseHook();
  }

  initIndex = async (routingKey: string): Promise<void> => {
    const indexer = this.indexers[routingKey];
    if (!indexer) {
      logger.error(`[initIndex] No indexer found for ${routingKey}`);
      return;
    }

    return indexer.init();
  };

  publish = async (routingKey: string, action: 'SAVE' | 'DELETE', id: any): Promise<void> => {
    const indexer = this.indexers[routingKey];
    if (!indexer) {
      logger.error(`No indexer found for ${routingKey}/${action}/${id}`);
      return;
    }
    return indexer.onMessage(action, id);
  };

  publishSave = async (routingKey: string, id: any): Promise<void> =>
    this.publish(routingKey, 'SAVE', id);

  publishDelete = async (routingKey: string, id: any): Promise<void> =>
    this.publish(routingKey, 'DELETE', id);

  getElasticSearchClient = (): Client => this.elasticSearchClient;

  start = async (initIndexers: boolean = true) => {
    logger.error(`Starting Indexing  : ${initIndexers}`);
    return this.deployIndexers(initIndexers)
      .then(() => this)
      .catch((error) => {
        logger.error(`Starting Indexing error : ${error.message}`);
        return this;
      });
  };

  close = () => {
    this.elasticSearchClient.close();
    logger.info(`ElasticSearch Connection closed`);
  };

  _addCloseHook = () => {
    process.on('SIGINT', () => {
      this.close();
      process.exit(1);
    });
  };

  parseResponse = (task: any) => {
    return (response: { ok: any; text: () => Promise<any>; body: any; status: number; statusText: any; json: () => any; }) => {
      if (!response.ok) {
        try {
          return response
            .text()
            .then((r: any) => {
              logger.error(`${task} err`, {
                response,
                function: 'parseResponseFor',
                source: 'parse-response',
              });
            })
            .catch((err: any) => {
              logger.error(`${task} err ${err}, ${JSON.stringify(response.body, null, 3)}`, {
                err,
                function: 'parseResponseFor',
                source: 'parse-response',
              });
              throw err;
            });
        } catch (err) {
          logger.error(`${task} err`, err);
          throw err;
        }
      }
      if (response.status !== 200) {
        logger.error(`response for ${task} with status ${response.status}`, {
          status: response.status,
          statusText: response.statusText,
          function: 'parseResponseFor',
          source: 'parse-response',
        });
      }
      return response.json();
    };
  };

  createIndices = async (indexerConfigs: any[]): Promise<any> => {
    const indexToCreates: any[] = await indexerConfigs.reduce(
      async (result: Promise<any[]>, indexConfig: any) => {
        const previousresult = await result;

        const index = await this.getElasticSearchClient().indices.exists({
          index: indexConfig.index,
        });
        if (index.body) return [...previousresult];
        return [...previousresult, indexConfig];
      },
      Promise.resolve([]),
    );

    if (indexToCreates && indexToCreates.length)
      logger.info(`INDEX TO CREATE : ${indexToCreates.map((indexConfig) => indexConfig.index)}`);

    return Promise.all(
      [...new Set(indexToCreates.map(({ index }: any) => index))].map(async (index: string) => {
        return this.getElasticSearchClient()
          .indices.create({ index })
          .then((_) => Promise.resolve());
      }),
    ).catch();
  };

  deployIndexer = async (
    options: IndexerDeploymentOptions,
    fetcher: (id: any) => Promise<any>,
    initIndexer: boolean,
  ): Promise<Indexing> => {
    const { routingKey, index, mappingProperties } = options;
    this.indexers[routingKey] = new Indexer({
      routingKey,
      index,
      mappingProperties,
      indexing: this,
      fetcher,
    });

    const indexer = this.indexers[routingKey];
    if (initIndexer) return indexer.init();
    return this;
  };

  deployIndexers = async (initIndexers: boolean): Promise<void> => {
    return new Promise((resolve, reject) => {
      const configPath = `${__dirname}/../config.json`;
      logger.info(`Reading config file : ${configPath}`);

      fs.readFile(configPath, { encoding: 'utf-8' }, async (error, data) => {
        if (error) {
          logger.error(`Read config elasticsearch error : ${error.message}`);
          reject(error);
          return;
        }

        try {
          const indexerConfigs = JSON.parse(data);

          logger.info(
            `ALL INDEX ${indexerConfigs.length}: ${indexerConfigs.map(
              (indexConfig: { index: any; }) => indexConfig.index,
            )}`,
          );

          return this.createIndices(indexerConfigs).then(async (_) => {
            /**
             * Every indexer config must contain :
             * Every indexer config must contain :
             * Every indexer config must contain :
             *  - routingKey : The rabbitMq channel routingKey
             *  - index : the elasticsearch index
             *  - type : the elasticsearch type
             *  - instances : The number of indexers to create.
             *  - query : The graphql query to fetch data by id.
             *  - dataKey : The response data Key
             *
             */

            for (var i = 0; i < indexerConfigs.length; i++) {
              const {
                routingKey,
                index,
                instances,
                query,
                dataKey,
                mappingProperties,
              } = indexerConfigs[i];

              logger.info(
                `Register routingKey=${routingKey}, index=${index}, instances=${instances}, dataKey=${dataKey}`,
              );

              await this.deployIndexer(
                { routingKey, index, instances, mappingProperties },
                (id: any) => {
                  const variables = { id };

                  console.log("********************************Fect***********", variables, JSON.stringify(query))
                  return fetch(process.env.API_URL, {
                    method: 'POST',
                    headers: {
                      clientId: process.env.API_CLIENT_ID,
                      clientSecret: process.env.API_CLIENT_SECRET,
                      'Content-Type': 'application/json',
                      Accept: 'application/json',
                    },
                    body: JSON.stringify({ variables, query }),
                  })
                    .then(this.parseResponse(dataKey))
                    .then((response: { data: { [x: string]: any; }; }) => response.data[dataKey]);
                },
                initIndexers,
              );

              if (i === indexerConfigs.length - 1) resolve();
            }

            /*   return Promise.all(
              indexerConfigs.map(
                ({ routingKey, index, instances, query, dataKey, mappingProperties }: any) => {
                  
                  logger.info(
                    `Register routingKey=${routingKey}, index=${index}, instances=${instances}, dataKey=${dataKey}`,
                  );

                  return this.deployIndexer(
                    { routingKey, index, instances, mappingProperties },
                    (id: any) => {
                      const variables = { id };
                      return fetch(process.env.API_URL, {
                        method: 'POST',
                        headers: {
                          clientId: process.env.API_CLIENT_ID,
                          clientSecret: process.env.API_CLIENT_SECRET,
                          'Content-Type': 'application/json',
                          Accept: 'application/json',
                        },
                        body: JSON.stringify({ variables, query }),
                      })
                        .then(this.parseResponse(dataKey))
                        .then(response => response.data[dataKey]);
                    },
                    initIndexers,
                  );
                },
              ),
            ); */
          });
        } catch (parsingError) {
          logger.error(`Register all indexers error : ${parsingError.message}`);
          reject(parsingError);
        }
      });
    });
  };
}
