import Indexing from './index';
import logger from '../logging';

export interface MappingProperties {
  [key: string]: any;
}

interface IndexerOptions {
  routingKey: string;
  index: string;
  mappingProperties: MappingProperties;
  indexing: Indexing;
  fetcher: (id: any) => Promise<any>;
}

// On AWS Elastic, We cannot open and close indices.

export default class Indexer {
  options: IndexerOptions;
  constructor(options: IndexerOptions) {
    this.options = options;
  }

  isOpenCloseAuthorized = () => process.env.INDEXING_AUTHORIZED_OPEN_CLOSE === 'true';

  init = async (): Promise<any> => {
    logger.info(
      `[indexer] Init Indexer ${
        this.options.index
      }, open-close authorized , ${this.isOpenCloseAuthorized()}, value=${
        process.env.INDEXING_AUTHORIZED_OPEN_CLOSE
      }`,
    );
    return this.deleteIndex()
      .then(() => this.createIndex())
      .then(() => this.closeIndex())
      .then(() => this.putSettings())
      .then(() => this.openIndex())
      .then(() => this.putMapping())
      .then(() => {
        logger.info(`[indexer] Success Init Indexer ${this.options.index}`);
        return Promise.resolve();
      })
      .catch(error => {
        logger.error(
          `[indexer] Init Indexer error ${this.options.index} : ${
            error.message
          } >>> ${JSON.stringify(error)}`,
        );

        return Promise.resolve();
      });
  };

  deleteIndex = async (): Promise<void> => {
    const { indexing, index } = this.options;
    logger.info(`[Delete] Indexer ${this.options.index}`);
    return indexing
      .getElasticSearchClient()
      .indices.delete({ index })
      .then(() => Promise.resolve());
  };

  createIndex = async (): Promise<any> => {
    const { indexing, index, mappingProperties } = this.options;
    logger.info(`[Create]  Indexer ${this.options.index}`);
    const properties = this._addMappingNormalizer(mappingProperties);
    const body = !this.isOpenCloseAuthorized()
      ? {
          settings: {
            analysis: {
              normalizer: {
                indexing_normalizer: {
                  type: 'custom',
                  char_filter: [],
                  filter: ['lowercase', 'asciifolding'],
                },
              },
            },
          },
          mappings: {
            properties,
          },
        }
      : undefined;

    const indice = await indexing.getElasticSearchClient().indices.exists({
      index,
    });
    if (indice.body) return Promise.resolve();

    return indexing
      .getElasticSearchClient()
      .indices.create({
        index,
        body,
      })
      .then(() => Promise.resolve())
      .catch(error => logger.error(`[Error Create] index ${index}: ${error.message}`));
  };

  closeIndex = (): Promise<void> => {
    const { indexing, index } = this.options;
    logger.info(`[Close]  Indexer ${this.options.index}`);
    return this.isOpenCloseAuthorized()
      ? indexing
          .getElasticSearchClient()
          .indices.close({ index })
          .then(() => Promise.resolve())
      : Promise.resolve();
  };

  putSettings = async (): Promise<void> => {
    const { indexing, index } = this.options;
    logger.info(`[Set setting]  ${this.options.index}`);
    return this.isOpenCloseAuthorized()
      ? indexing
          .getElasticSearchClient()
          .indices.putSettings({
            index,
            body: {
              analysis: {
                normalizer: {
                  indexing_normalizer: {
                    type: 'custom',
                    char_filter: [],
                    filter: ['lowercase', 'asciifolding'],
                  },
                },
              },
              index: {
                max_result_window: 2000000,
              },
            },
          })
          .then(() => Promise.resolve())
      : Promise.resolve();
  };

  _addMappingNormalizer = (mappingProperties: Object): Object => {
    const { index } = this.options;
    const properties = Object.keys(mappingProperties).reduce((mapping, name) => {
      if (mappingProperties[name].type && mappingProperties[name].type === 'keyword') {
        return {
          ...mapping,
          [name]: {
            ...mappingProperties[name],
            fields: {
              keyword: {
                type: 'keyword',
              },
            },
            normalizer: 'indexing_normalizer',
          },
        };
      } else if (mappingProperties[name].properties) {
        return {
          ...mapping,
          [name]: {
            properties: this._addMappingNormalizer(mappingProperties[name].properties),
          },
        };
      }
      return {
        ...mapping,
        [name]: mappingProperties[name],
      };
    }, {});

    return properties;
  };

  putMapping = async (): Promise<void> => {
    const { indexing, index, mappingProperties } = this.options;
    const properties = this._addMappingNormalizer(mappingProperties);
    if (mappingProperties) {
      logger.info(`[Set mapping]  ${this.options.index}`);
      return indexing
        .getElasticSearchClient()
        .indices.putMapping({
          index,
          body: { properties },
        })
        .then(_ => {
          return Promise.resolve();
        });
    }

    return Promise.resolve();
  };

  openIndex = async (): Promise<void> => {
    const { indexing, index } = this.options;
    logger.info(`[Open index]  ${this.options.index}`);
    return this.isOpenCloseAuthorized()
      ? indexing
          .getElasticSearchClient()
          .indices.open({ index })
          .then(() => Promise.resolve())
      : Promise.resolve();
  };

  /**
   * We consumes message here. The message must contains :
   *  - action : "DELETE" OR "SAVE"
   *  - id : The id of element.
   *
   *
   */
  onMessage = async (action: string, id: any): Promise<void> => {
    if (id) {
      const { routingKey } = this.options;

      logger.info(`[${routingKey}] Message arrived on queue : ${routingKey}`);

      switch (action) {
        case 'DELETE':
          return this._logAndAckMessageIfSucceeded({
            id,
            action,
            result: this._deleteDocument(id),
          });
        case 'SAVE':
          return this._logAndAckMessageIfSucceeded({
            id,
            action,
            result: this._saveDocument(id, 0),
          });
        default:
          logger.error(`[${routingKey}] Unknow action "${action}"`);
      }
    }

    return Promise.resolve();
  };

  _logAndAckMessageIfSucceeded = async ({ action, id, result }: any): Promise<void> =>
    result
      .then(_ => {
        const { index, routingKey } = this.options;
        logger.info(`[${routingKey}] ${action} ${index}/_doc/${id} OK`);
        return;
      })
      .catch((error: any) => {
        const { index, routingKey } = this.options;
        logger.info(`[${routingKey}] ${action} ${index}/_doc/${id} FAILED : ${error.message}`);

        return error;
      });

  _deleteDocument = async (id: any): Promise<any> => {
    const { indexing, index } = this.options;
    return indexing.getElasticSearchClient().delete({ index, id });
  };

  _saveDocument = async (id: any, retryCount: number): Promise<any> => {
    const { index, indexing, fetcher, routingKey } = this.options;
    return fetcher(id)
      .then(async body => {
        if (body) {
          delete body.id;
          const _ = await indexing.getElasticSearchClient().index({
            index,
            id,
            body,
          });
          return await indexing.getElasticSearchClient().indices.refresh({ index });
        }
      })
      .catch(error => {
        if (retryCount > 100000) {
          logger.error(`[${routingKey}] Fetching error ${index}/_doc/${id} : ${error.message}`);
          return error;
        }

        return new Promise((resolve, reject) => {
          setTimeout(() => {
            this._saveDocument(id, retryCount + 1)
              .catch(reject)
              .then(resolve);
          }, 5000); // Wait 5s
        });
      });
  };
}
