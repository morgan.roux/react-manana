const path = require('path');

import { fileLoader, mergeTypes } from 'merge-graphql-schemas';

const typesArray = fileLoader(path.join(__dirname, '../src/schemas'), { extensions: ['graphql'] });

export default mergeTypes(typesArray, { all: true });
