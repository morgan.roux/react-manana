import { TodoActionTypeResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TodoActionType: TodoActionTypeResolvers.Type = {
  ...TodoActionTypeResolvers.defaultResolvers,
  type: () => 'todoactiontype',
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionType({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionType({ id: parent['_id'] || parent.id }).userModification();
  },
};
