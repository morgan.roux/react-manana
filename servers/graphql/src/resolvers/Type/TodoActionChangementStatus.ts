import { TodoActionChangementStatusResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TodoActionChangementStatus: TodoActionChangementStatusResolvers.Type = {
  ...TodoActionChangementStatusResolvers.defaultResolvers,
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  action: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionChangementStatus({ id: parent['_id'] || parent.id }).action();
  },
  user: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionChangementStatus({ id: parent['_id'] || parent.id }).user();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionChangementStatus({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .todoActionChangementStatus({ id: parent['_id'] || parent.id })
      .userModification();
  },
};
