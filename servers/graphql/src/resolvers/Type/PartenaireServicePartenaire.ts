import { PartenaireServicePartenaireResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PartenaireServicePartenaire: PartenaireServicePartenaireResolvers.Type = {
  ...PartenaireServicePartenaireResolvers.defaultResolvers,
  type: () => 'partenaireservicepartenaire',
  id: (parent: any, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  partenaireService: (parent, _, ctx: Context) => {
    return ctx.prisma
      .partenaireServicePartenaire({ id: parent['_id'] || parent.id })
      .partenaireService();
  },
};
