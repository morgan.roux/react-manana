import { TodoParticipantResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TodoParticipant: TodoParticipantResolvers.Type = {
  ...TodoParticipantResolvers.defaultResolvers,
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  project: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoParticipant({ id: parent['_id'] || parent.id }).project();
  },
  userParticipant: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoParticipant({ id: parent['_id'] || parent.id }).userParticipant();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoParticipant({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoParticipant({ id: parent['_id'] || parent.id }).userModification();
  },
};
