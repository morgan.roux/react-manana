import { UserPhotoResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const UserPhoto: UserPhotoResolvers.Type = {
  ...UserPhotoResolvers.defaultResolvers,
  fichier: async (parent, _, ctx: Context) => {
    return parent['fichier']
      ? parent['fichier']
      : ctx.prisma.userPhoto({ id: parent.id }).idFichier();
  },
  user: async (parent, _, ctx: Context) => {
    return ctx.prisma.userPhoto({ id: parent.id }).idUser();
  },
};
