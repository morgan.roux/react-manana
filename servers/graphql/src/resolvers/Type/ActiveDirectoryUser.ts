// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { ActiveDirectoryUserResolvers } from '../../generated/graphqlgen';

export const ActiveDirectoryUser: ActiveDirectoryUserResolvers.Type = {
  ...ActiveDirectoryUserResolvers.defaultResolvers,

  activeDirectory: (parent, args, ctx) => {
    return ctx.prisma.activeDirectoryUser({id : parent.id}).activeDirectory();
  },
  user: (parent, args, ctx) => {
    return ctx.prisma.activeDirectoryUser({id : parent.id}).user();
  }
};
