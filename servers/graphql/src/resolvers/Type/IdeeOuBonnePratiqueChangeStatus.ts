import { IdeeOuBonnePratiqueChangeStatusResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const IdeeOuBonnePratiqueChangeStatus: IdeeOuBonnePratiqueChangeStatusResolvers.Type = {
  ...IdeeOuBonnePratiqueChangeStatusResolvers.defaultResolvers,
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  ideeOuBonnePratique: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ideeOuBonnePratiqueChangeStatus({ id: parent['_id'] || parent.id })
      .ideeOuBonnePratique();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ideeOuBonnePratiqueChangeStatus({ id: parent['_id'] || parent.id })
      .userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ideeOuBonnePratiqueChangeStatus({ id: parent['_id'] || parent.id })
      .userModification();
  },
};
