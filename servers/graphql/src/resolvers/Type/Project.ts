import { getDefaultShouldTerm, getUserSource, searchTodos } from '../../repository/action';
import { ProjectResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Project: ProjectResolvers.Type = {
  ...ProjectResolvers.defaultResolvers,
  type: () => 'project',
  id: async (parent, { }, ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  projetParent: async (parent, _, ctx: Context) => {
    return ctx.prisma.project({ id: parent['_id'] || parent.id }).projetParent();
  },
  user: async (parent, _, ctx: Context) => {
    return ctx.prisma.project({ id: parent['_id'] || parent.id }).user();
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.project({ id: parent['_id'] || parent.id }).groupement();
  },
  pharmacie: async (parent, _, ctx: Context) => {
    return ctx.prisma.project({ id: parent['_id'] || parent.id }).pharmacie();
  },
  couleur: async (parent, _, ctx: Context) => {
    return ctx.prisma.project({ id: parent['_id'] || parent.id }).couleur();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.project({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.project({ id: parent['_id'] || parent.id }).userModification();
  },
  participants: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .todoParticipants({
        where: { project: { id: parent['_id'] || parent.id }, isRemoved: false },
      })
      .then(todoParticipants => {
        return Promise.all(
          todoParticipants.map(todoParticipant => {
            return ctx.prisma.todoParticipant({ id: todoParticipant.id }).userParticipant();
          }),
        );
      });
  },
  isInFavoris: async (parent, _, ctx: Context) => {
    return ctx.prisma.$exists.todoProjetFavoris({
      project: { id: parent['_id'] || parent.id },
      isRemoved: false,
    });
  },
  subProjects: async (parent, _, ctx: Context) => {
    return ctx.prisma.projects({
      where: { projetParent: { id: parent['_id'] || parent.id }, isRemoved: false },
    });
  },
  nbAction: async (parent, { actionStatus }, ctx: Context) => {
    const selectedProjectIsPersonnel = !!(parent.typeProject === "PERSONNEL")


    const termActive = actionStatus
      ? actionStatus === 'ACTIVE'
        ? [{ term: { status: 'ACTIVE' } }]
        : [{ term: { status: 'DONE' } }]
      : [{ term: { status: 'ACTIVE' } }];

    const must = [
      { term: { isRemoved: false } },
      ...termActive,
      ...(selectedProjectIsPersonnel ? [] : [{ term: { 'project.id': parent['_id'] || parent.id } }])
      // { term: { 'project.id': parent['_id'] || parent.id } },
    ];

    const { defaultShouldCreatedTeam } = await getUserSource(ctx, ctx.userId);

     return searchTodos(
      ctx,
     must,
      [],
      selectedProjectIsPersonnel ?
        [
          {
            bool: {
              must: [
                {
                  exists: {
                    field: 'project',
                  },
                },
                {
                  term: { 'project.id': parent['_id'] || parent.id }
                }
              ]
            }
          },
          {
            bool: {
              must_not: [
                {
                  exists: {
                    field: 'project',
                  },
                },
              ],
              must: [
                {
                  term: { 'assignedUsers.id': ctx.userId },
                },
              ],
            },
          },


          {
            bool: {
              must_not: [
                {
                  exists: {
                    field: 'assignedUsers',
                  },
                },
                {
                  exists: {
                    field: 'project',
                  },
                },
              ],
              must: [
                {
                  term: { 'userCreation.id': ctx.userId },
                },
              ],
            },
          }
        ] :
        [
          {
            bool: {
              must_not: [
                {
                  exists: {
                    field: 'assignedUsers',
                  },
                },
              ],
              must: {
                term: { 'userCreation.id': ctx.userId },
              },
            },
          },
          { term: { 'assignedUsers.id': ctx.userId } },
          {
            bool: {
              must: [
                {
                  exists: {
                    field: 'project.participants.id',
                  },
                },
                ...defaultShouldCreatedTeam,
              ],
            },
          },
        ],
    );
  },
  isShared: async (parent, _, ctx: Context) => {
    return ctx.prisma.$exists.todoParticipant({
      project: { id: parent['_id'] || parent.id },
      isRemoved: false,
    });
  },
  activeActions: async (parent, _, ctx: Context) => {
    return ctx.prisma.$exists.action({
      project: {
        id: parent['_id'] || parent.id,
      },
      isRemoved: false,
      actionStatus: 'ACTIVE',
      OR: [
        {
          userCreation: { id: ctx.userId },
        },
        {
          actionAttributions_some: {
            userDestination: {
              id: ctx.userId,
            },
          },
        },
      ],
    });
  },
};
