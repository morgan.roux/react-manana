import { last } from 'lodash';
import { IdeeOuBonnePratiqueResolvers } from '../../generated/graphqlgen';
import { Context, IdeeOuBonnePratiqueStatus } from '../../types';

export const IdeeOuBonnePratique: IdeeOuBonnePratiqueResolvers.Type = {
  ...IdeeOuBonnePratiqueResolvers.defaultResolvers,
  type: () => 'ideeoubonnepratique',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  auteur: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratique({ id: parent['_id'] || parent.id }).auteur();
  },
  fournisseur: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratique({ id: parent['_id'] || parent.id }).fournisseur();
  },
  prestataire: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratique({ id: parent['_id'] || parent.id }).prestataire();
  },
  groupeAmis: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratique({ id: parent['_id'] || parent.id }).groupeAmis();
  },
  service: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratique({ id: parent['_id'] || parent.id }).service();
  },
  classification: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratique({ id: parent['_id'] || parent.id }).classification();
  },
  ideeOuBonnePratiqueComments: async (parent, _, ctx: Context) => {
    return ctx.prisma.comments({
      where: {
        item: { code: 'IDEE_AND_BEST_PRACTICE' },
        idItemAssocie: parent['_id'] || parent.id,
        isRemoved: false,
      },
    });
  },
  ideeOuBonnePratiqueSmyleys: async (parent, _, ctx: Context) => {
    return ctx.prisma.userSmyleys({
      where: {
        item: { code: 'IDEE_AND_BEST_PRACTICE' },
        idSource: parent['_id'] || parent.id,
        isRemoved: false,
      },
    });
  },
  ideeOuBonnePratiqueLus: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiqueLus({
      where: {
        ideeOuBonnePratique: { id: parent['_id'] || parent.id },
        isRemoved: false,
      },
    });
  },
  ideeOuBonnePratiqueFichierJoints: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiqueFichierJoints({
      where: {
        ideeOuBonnePratique: { id: parent['_id'] || parent.id },
        isRemoved: false,
      },
    });
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratique({ id: parent['_id'] || parent.id }).groupement();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratique({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratique({ id: parent['_id'] || parent.id }).userModification();
  },
  nbSmyley: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .userSmyleysConnection({
        where: {
          item: { code: 'IDEE_AND_BEST_PRACTICE' },
          idSource: parent['_id'] || parent.id,
          isRemoved: false,
        },
      })
      .aggregate()
      .count();
  },
  nbComment: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .commentsConnection({
        where: {
          item: { code: 'IDEE_AND_BEST_PRACTICE' },
          idItemAssocie: parent['_id'] || parent.id,
          isRemoved: false,
        },
      })
      .aggregate()
      .count();
  },
  nbPartage: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partagesConnection({
        where: {
          item: { code: 'IDEE_AND_BEST_PRACTICE' },
          idItemAssocie: parent['_id'] || parent.id,
          isRemoved: false,
        },
      })
      .aggregate()
      .count();
  },
  nbLu: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ideeOuBonnePratiqueLusConnection({
        where: { ideeOuBonnePratique: { id: parent['_id'] || parent.id }, isRemoved: false },
      })
      .aggregate()
      .count();
  },
  status: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ideeOuBonnePratiqueChangeStatuses({
        where: { ideeOuBonnePratique: { id: parent['_id'] || parent.id }, isRemoved: false },
        orderBy: 'dateCreation_ASC',
      })
      .then(results => {
        const res = last(results);
        if (res) {
          return res.status;
        } else {
          return parent.status;
        }
      });
  },
  ideeOuBonnePratiqueChangeStatus: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ideeOuBonnePratique({
        id: parent['_id'] || parent.id,
      })
      .ideeOuBonnePratiqueChangeStatus();
  },
  lu: async (parent, _, ctx: Context) => {
    return ctx.prisma.$exists.ideeOuBonnePratiqueLu({
      ideeOuBonnePratique: { id: parent['_id'] || parent.id },
      user: { id: ctx.userId },
      isRemoved: false,
    });
  },
};
