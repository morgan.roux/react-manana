import { PartenaireServiceSuiteResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PartenaireServiceSuite: PartenaireServiceSuiteResolvers.Type = {
  ...PartenaireServiceSuiteResolvers.defaultResolvers,
  type: () => 'partenaireservicesuite',
  id: (parent: any, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  groupement: (parent, _, ctx: Context) => {
    return ctx.prisma.partenaireServiceSuite({ id: parent['_id'] || parent.id }).groupement();
  },
};
