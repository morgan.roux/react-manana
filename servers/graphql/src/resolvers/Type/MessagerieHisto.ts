import { MessagerieHistoResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const MessagerieHisto: MessagerieHistoResolvers.Type = {
  ...MessagerieHistoResolvers.defaultResolvers,
  userRecepteur: (parent, _, ctx: Context) => {
    return ctx.prisma.messagerieHisto({ id: parent['_id'] || parent.id }).userRecepteur();
  },

  userCreation: (parent, _, ctx: Context) => {
    return ctx.prisma.messagerieHisto({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: (parent, _, ctx: Context) => {
    return ctx.prisma.messagerieHisto({ id: parent['_id'] || parent.id }).userModification();
  },
};
