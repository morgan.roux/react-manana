import { TauxSSResolvers } from '../../generated/graphqlgen';

export const TauxSS: TauxSSResolvers.Type = {
  ...TauxSSResolvers.defaultResolvers,
  type: () => 'tauxss',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
};
