import { TodoEtiquetteFavorisResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TodoEtiquetteFavoris: TodoEtiquetteFavorisResolvers.Type = {
  ...TodoEtiquetteFavorisResolvers.defaultResolvers,
  type: () => 'todoetiquettefavoris',
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoEtiquetteFavoris({ id: parent['_id'] || parent.id }).userCreation();
  },
  etiquette: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoEtiquetteFavoris({ id: parent['_id'] || parent.id }).etiquette();
  },
  user: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoEtiquetteFavoris({ id: parent['_id'] || parent.id }).user();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoEtiquetteFavoris({ id: parent['_id'] || parent.id }).userModification();
  },
};
