import { ProduitCodeResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const ProduitCode: ProduitCodeResolvers.Type = {
  ...ProduitCodeResolvers.defaultResolvers,
  type: async (parent, _, ctx: Context) => {
    return parent && parent.typeCode
      ? ctx.prisma.produitTypeCode({ resipIdTypeCode: parent.typeCode })
      : null;
  },
};
