import { LibelleDiversResolvers } from '../../generated/graphqlgen';

export const LibelleDivers: LibelleDiversResolvers.Type = {
  ...LibelleDiversResolvers.defaultResolvers,
  type: () => 'libelledivers',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
};
