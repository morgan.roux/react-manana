import { PartenaireResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { getUserPartenaire } from '../../repository/partenaire';
import { getComments } from '../../services/comment';
import { last } from 'lodash';

export const Partenaire: PartenaireResolvers.Type = {
  ...PartenaireResolvers.defaultResolvers,
  type: () => 'partenaire',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  idGroupement: async (parent, {}, ctx: Context) => {
    return ctx.prisma
      .partenaire({ id: parent['_id'] || parent.id })
      .groupement()
      .then(groupement => (groupement && groupement.id ? groupement.id : null));
  },
  comments: async (parent, { skip, first }, ctx: Context) => {
    return getComments(skip, first, parent['_id'] || parent.id, ctx);
  },
  user: async (parent, {}, ctx: Context) => {
    return getUserPartenaire(ctx, parent['_id'] || parent.id);
  },
  role: async (parent, {}, ctx: Context) => {
    const user = await getUserPartenaire(ctx, parent['_id'] || parent.id);
    return user
      ? ctx.prisma
          .userRoles({ where: { idUser: { id: user.id } } })
          .then(userRoles =>
            userRoles && userRoles.length
              ? ctx.prisma.userRole({ id: userRoles[0].id }).idRole()
              : null,
          )
      : null;
  },
  contact: async (parent, {}, ctx: Context) => {
    return ctx.prisma.partenaire({ id: parent['_id'] || parent.id }).contact();
  },
  partenaireServiceSuite: async (parent, _: any, ctx: Context) => {
    return ctx.prisma.partenaire({ id: parent['_id'] || parent.id }).partenaireServiceSuite();
  },
  services: async (parent, _: any, ctx: Context) => {
    return ctx.prisma.servicePartenaires({
      where: { partenaireService: { id: parent['_id'] || parent.id } },
    });
  },
  partenaireServicePartenaire: async (parent, _: any, ctx: Context) => {
    return ctx.prisma
      .partenaireServicePartenaires({
        where: { partenaireService: { id: parent['_id'] || parent.id } },
        orderBy: 'dateCreation_ASC',
      })
      .then(res => {
        return last(res);
      });
  },
};
