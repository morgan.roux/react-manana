import { NotificationResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import moment from 'moment';
const now = moment().format();

export const Notification: NotificationResolvers.Type = {
  ...NotificationResolvers.defaultResolvers,
  id: (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  from: async (parent, _, ctx: Context) => {
    return ctx.prisma.notification({ id: parent.id }).from();
  },
  to: async (parent, _, ctx: Context) => {
    return ctx.prisma.notification({ id: parent.id }).to();
  },
  targetName: async (parent, _, ctx: Context) => {
    switch (parent.type) {
      case 'PATENAIRE_ADDED':
        return ctx.prisma.partenaire({ id: parent.targetId }).nom();
      case 'ACTUALITE_ADDED':
        return ctx.prisma.actualite({ id: parent.targetId }).libelle();
      case 'TODO_ASSIGN':
        return ctx.prisma.project({ id: parent.targetId }).name();
      default:
        return parent.targetName;
    }
  },
  typeActualite: async (parent, {}, ctx: Context) => {
    switch (parent.type) {
      case 'ACTUALITE_ADDED':
        return ctx.prisma
          .actualite({ id: parent.targetId })
          .idItem()
          .code();
      default:
        return '';
    }
  },
  dateActive: async (parent, {}, ctx: Context) => {
    switch (parent.type) {
      case 'ACTUALITE_ADDED':
        return ctx.prisma.actualite({ id: parent.targetId }).dateDebut();
      default:
        return '';
    }
  },
  active: async (parent, _, ctx: Context) => {
    switch (parent.type) {
      case 'ACTUALITE_ADDED':
        return ctx.prisma.$exists.actualite({
          id: parent.targetId,
          dateDebut_lte: now,
          dateFin_gte: now,
        });
      default:
        return true;
    }
  },
};
