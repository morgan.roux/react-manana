import { OperationPharmacieResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const OperationPharmacie: OperationPharmacieResolvers.Type = {
  ...OperationPharmacieResolvers.defaultResolvers,
  pharmaciedetails: async (parent, _, ctx: Context) => {
    return ctx.prisma.operationPharmacieDetails({
      where: { idOperationPharmacie: { id: parent.id } },
    });
  },
  fichierCible: async (parent, _, ctx: Context) => {
    const fichierCible = await ctx.prisma.operationPharmacie({ id: parent.id }).fichierCible();
    return fichierCible;
  },
};
