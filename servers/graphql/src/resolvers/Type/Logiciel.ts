import { LogicielResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Logiciel: LogicielResolvers.Type = {
  ...LogicielResolvers.defaultResolvers,
  ssii: async (parent, {}, ctx: Context) => {
    return ctx.prisma.logiciel({ id: parent.id }).ssii();
  },
};
