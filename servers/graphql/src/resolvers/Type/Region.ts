import { RegionResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Region: RegionResolvers.Type = {
  ...RegionResolvers.defaultResolvers,
  departements: async (parent, _, ctx: Context) => {
    return ctx.prisma.departements({ where: { region: { id: parent.id } } });
  },
  grandeRegion: (parent, _, ctx: Context) => {
    return ctx.prisma.region({ id: parent['_id'] || parent.id }).grandeRegion();
  },
  userCreation: (parent, _, ctx: Context) => {
    return ctx.prisma.region({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: (parent, _, ctx: Context) => {
    return ctx.prisma.region({ id: parent['_id'] || parent.id }).userModification();
  },
};
