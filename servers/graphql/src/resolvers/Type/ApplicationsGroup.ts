// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { ApplicationsGroupResolvers } from '../../generated/graphqlgen';

export const ApplicationsGroup: ApplicationsGroupResolvers.Type = {
  ...ApplicationsGroupResolvers.defaultResolvers,

  groupement: (parent, args, ctx) => {
    return ctx.prisma.applicationsGroup({ id: parent.id }).groupement();
  },
  applications: (parent, args, ctx) => {
    return ctx.prisma.applicationsGroup({ id: parent.id }).idapplications();
  },
};
