import { MarcheGroupeClientResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const MarcheGroupeClient: MarcheGroupeClientResolvers.Type = {
  ...MarcheGroupeClientResolvers.defaultResolvers,
  groupeClient: async (parent, _, ctx: Context) => {
    return ctx.prisma.marcheGroupeClient({ id: parent.id }).groupeClient();
  },
  remise: async (parent, _, ctx: Context) => {
    return ctx.prisma.marcheGroupeClient({ id: parent.id }).remise();
  },
};
