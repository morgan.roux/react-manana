import { RoleResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Role: RoleResolvers.Type = {
  ...RoleResolvers.defaultResolvers,
  type: () => 'role',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  roleTraitements: async (parent, _, ctx: Context) => {
    return ctx.prisma.roleTraitements({
      where: { role: { id: parent['_id'] || parent.id }, groupement: { id: ctx.groupementId } },
    });
  },
};
