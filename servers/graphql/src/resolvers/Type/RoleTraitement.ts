import { RoleTraitementResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const RoleTraitement: RoleTraitementResolvers.Type = {
  ...RoleTraitementResolvers.defaultResolvers,
  codeTraitement: async (parent, _, ctx: Context) => {
    const traitement = await ctx.prisma.roleTraitement({ id: parent.id }).traitement();
    return traitement ? traitement.code : null;
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.roleTraitement({ id: parent.id }).groupement();
  },
};
