import { TicketChangementStatutResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TicketChangementStatut: TicketChangementStatutResolvers.Type = {
  ...TicketChangementStatutResolvers.defaultResolvers,
  type: () => 'ticketchangementstatut',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  ticket: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementStatut({ id: parent['_id'] || parent.id }).ticket();
  },
  ticketStatut: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementStatut({ id: parent['_id'] || parent.id }).ticketStatut();
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementStatut({ id: parent['_id'] || parent.id }).groupement();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementStatut({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementStatut({ id: parent['_id'] || parent.id }).userModification();
  },
  ticketChangementCible: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ticketChangementStatut({ id: parent['_id'] || parent.id })
      .ticketChangementCible();
  },
};
