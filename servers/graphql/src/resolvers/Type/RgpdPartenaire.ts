import { RgpdPartenaireResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const RgpdPartenaire: RgpdPartenaireResolvers.Type = {
  ...RgpdPartenaireResolvers.defaultResolvers,
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.rgpdPartenaire({ id: parent['_id'] || parent.id }).groupement();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.rgpdPartenaire({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.rgpdPartenaire({ id: parent['_id'] || parent.id }).userModification();
  },
};
