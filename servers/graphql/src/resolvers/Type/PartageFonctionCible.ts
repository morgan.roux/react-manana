import { PartageFonctionCibleResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PartageFonctionCible: PartageFonctionCibleResolvers.Type = {
  ...PartageFonctionCibleResolvers.defaultResolvers,
  id: async (parent, _: any, __: any) => {
    return (parent['_id'] || parent.id) as any;
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.partageFonctionCible({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.partageFonctionCible({ id: parent['_id'] || parent.id }).userModification();
  },
};
