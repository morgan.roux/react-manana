import { PharmacieEntreeSortieResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PharmacieEntreeSortie: PharmacieEntreeSortieResolvers.Type = {
  ...PharmacieEntreeSortieResolvers.defaultResolvers,
  userCreation: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieEntreeSortie({ id: parent.id }).userCreation();
  },
  userModification: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieEntreeSortie({ id: parent.id }).userModification();
  },
  concurrent: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieEntreeSortie({ id: parent.id }).concurrent();
  },
  concurrentAncien: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieEntreeSortie({ id: parent.id }).concurrentAncien();
  },
  contrat: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieEntreeSortie({ id: parent.id }).contrat();
  },
  pharmacieMotifEntree: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieEntreeSortie({ id: parent.id }).motifEntree();
  },
  pharmacieMotifSortie: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieEntreeSortie({ id: parent.id }).motifSortie();
  },
  pharmacieMotifSortieFuture: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieEntreeSortie({ id: parent.id }).motifSortieFuture();
  },
};
