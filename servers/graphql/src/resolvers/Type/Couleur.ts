import { CouleurResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Couleur: CouleurResolvers.Type = {
  ...CouleurResolvers.defaultResolvers,
  type: () => 'couleur',
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.couleur({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.couleur({ id: parent['_id'] || parent.id }).userModification();
  },
};
