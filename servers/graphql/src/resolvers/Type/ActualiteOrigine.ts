import { ActualiteOrigineResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { countActualiteFromSearch } from '../../repository/actualite';

export const ActualiteOrigine: ActualiteOrigineResolvers.Type = {
  ...ActualiteOrigineResolvers.defaultResolvers,
  type: () => 'actualiteorigine',
  id: (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  countActualites: async (parent, { searchParams }, ctx: Context) => {
    /* 
    console.log(`------- searchParams ---------- ${JSON.stringify(searchParams)} `); */
    if (!searchParams) {
      return ctx.prisma
        .actualitesConnection({ where: { idActualiteOrigine: { id: parent['_id'] || parent.id } } })
        .aggregate()
        .count();
    }
    const params = JSON.parse(searchParams);
    const query = {
      bool: {
        ...params,
      },
    }; /* 
    console.log(`------- query ---------- ${JSON.stringify(query)} `); */
    if (query && query.bool && query.bool.must.length) {
      query.bool.must.push({ term: { 'origine.id': parent['_id'] || parent.id } });
      return countActualiteFromSearch(ctx, {
        query,
      });
    }
    return 0;
  },
};
