import { PersonnelFonctionResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PersonnelFonction: PersonnelFonctionResolvers.Type = {
  ...PersonnelFonctionResolvers.defaultResolvers,
  type: () => 'personnelfonction',
  id: (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  service: (parent, __, ctx: Context) => {
    return ctx.prisma.personnelFonction({ id: parent['_id'] || parent.id }).service();
  },
  userCreation: (parent, __, ctx: Context) => {
    return ctx.prisma.personnelFonction({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: (parent, __, ctx: Context) => {
    return ctx.prisma.personnelFonction({ id: parent['_id'] || parent.id }).userModification();
  },
};
