import { IdeeOuBonnePratiqueFichierJointResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const IdeeOuBonnePratiqueFichierJoint: IdeeOuBonnePratiqueFichierJointResolvers.Type = {
  ...IdeeOuBonnePratiqueFichierJointResolvers.defaultResolvers,
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  ideeOuBonnePratique: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ideeOuBonnePratiqueFichierJoint({ id: parent['_id'] || parent.id })
      .ideeOuBonnePratique();
  },
  fichier: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiqueFichierJoint({ id: parent['_id'] || parent.id }).fichier();
  },
};
