import { MarcheResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import {
  marchePharmaciesAdherent,
  countMarcheRemiseDetails,
  marcheArticles,
  marcheGroupeClientRemise,
  marcheRemiseDetails,
  marcheRemise,
} from '../../repository/marche';

export const Marche: MarcheResolvers.Type = {
  ...MarcheResolvers.defaultResolvers,
  type: () => 'marche',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  commandeCanal: async (parent, _, ctx: Context) => {
    return ctx.prisma.marche({ id: parent['_id'] || parent.id }).commandeCanal();
  },
  nbAdhesion: async (parent, _, ctx: Context) => {
    return (await marchePharmaciesAdherent(ctx, { id: parent['_id'] || parent.id })).length;
  },
  nbPalier: async (parent, _, ctx: Context) => {
    return countMarcheRemiseDetails(ctx, { id: parent['_id'] || parent.id });
  },
  userCreated: async (parent, _, ctx: Context) => {
    return ctx.prisma.marche({ id: parent['_id'] || parent.id }).userCreated();
  },
  userModified: async (parent, _, ctx: Context) => {
    return ctx.prisma.marche({ id: parent['_id'] || parent.id }).userModified();
  },
  canalArticles: async (parent, _, ctx: Context) => {
    return marcheArticles(ctx, { id: parent['_id'] || parent.id });
  },
  laboratoires: async (parent, _, ctx: Context) => {
    return ctx.prisma.marcheLaboratoires({
      where: {
        marche: {
          id: parent['_id'] || parent.id,
        },
      },
    });
  },
  groupeClients: async (parent, _, ctx: Context) => {
    return marcheGroupeClientRemise(ctx, { id: parent['_id'] || parent.id });
  },
  remise: async (parent, { idPharmacie }, ctx: Context) => {
    return marcheRemise(ctx, { id: parent['_id'] || parent.id, idPharmacie });
  },
};
