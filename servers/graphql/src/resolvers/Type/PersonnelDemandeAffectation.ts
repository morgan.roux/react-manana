import { PersonnelDemandeAffectationResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PersonnelDemandeAffectation: PersonnelDemandeAffectationResolvers.Type = {
  ...PersonnelDemandeAffectationResolvers.defaultResolvers,
  type: () => 'personneldemandeaffectation',
  id: (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  service: (parent, __, ctx: Context) => {
    return ctx.prisma.personnelDemandeAffectation({ id: parent['_id'] || parent.id }).service();
  },
  personnelFonction: (parent, __, ctx: Context) => {
    return ctx.prisma
      .personnelDemandeAffectation({ id: parent['_id'] || parent.id })
      .personnelFonction();
  },
  personnelRemplacent: (parent, __, ctx: Context) => {
    return ctx.prisma
      .personnelDemandeAffectation({ id: parent['_id'] || parent.id })
      .personnelRemplacent();
  },
  userCreation: (parent, __, ctx: Context) => {
    return ctx.prisma
      .personnelDemandeAffectation({ id: parent['_id'] || parent.id })
      .userCreation();
  },
  userModification: (parent, __, ctx: Context) => {
    return ctx.prisma
      .personnelDemandeAffectation({ id: parent['_id'] || parent.id })
      .userModification();
  },
};
