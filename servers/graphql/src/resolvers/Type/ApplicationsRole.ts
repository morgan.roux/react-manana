// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { ApplicationsRoleResolvers } from '../../generated/graphqlgen';

export const ApplicationsRole: ApplicationsRoleResolvers.Type = {
  ...ApplicationsRoleResolvers.defaultResolvers,

  role: (parent, args, ctx) => {
    return ctx.prisma.applicationsRole({ id: parent.id }).idrole();
  },
  groupement: (parent, args, ctx) => {
    return ctx.prisma.applicationsRole({ id: parent.id }).groupement();
  },
  applications: (parent, args, ctx) => {
    return ctx.prisma.applicationsRole({ id: parent.id }).idapplications();
  },
};
