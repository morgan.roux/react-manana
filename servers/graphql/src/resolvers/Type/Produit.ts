import { ProduitResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { produitFamille } from '../../repository/produit';

export const Produit: ProduitResolvers.Type = {
  ...ProduitResolvers.defaultResolvers,
  //
  type: () => 'produit',
  id: async (parent, _, __) => {
    return parent['_id'] || parent.id;
  },
  produitPhoto: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .produitPhotos({
        where: {
          produit: {
            id: parent['_id'] || parent.id,
          },
        },
        first: 1,
      })
      .then(articlePhotos => (articlePhotos && articlePhotos.length ? articlePhotos[0] : null));
  },
  produitCode: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .produitCodes({
        where: {
          produit: {
            id: parent['_id'] || parent.id,
          },
          referent: 1,
        },
        first: 1,
      })
      .then(produitCodes => (produitCodes && produitCodes.length ? produitCodes[0] : null));
  },
  famille: async (parent, _, ctx: Context) => {
    return produitFamille(ctx, {
      id: parent['_id'] || parent.id,
    });
  },
  produitTechReg: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .produitTechRegs({
        where: {
          produit: {
            id: parent['_id'] || parent.id,
          },
        },
        first: 1,
      })
      .then(produitTechRegs =>
        produitTechRegs && produitTechRegs.length ? produitTechRegs[0] : null,
      );
  },
  canauxArticle: async (parent, _, ctx: Context) => {
    return ctx.prisma.produitCanals({
      where: {
        produit: {
          id: parent['_id'] || parent.id,
        },
      },
    });
  },
  categorie: async (parent, _, ctx: Context) => {
    return parent && parent.categorie
      ? ctx.prisma.produitCategorie({
          resipIdCategorie: parent.categorie,
        })
      : null;
  },
  service: async (parent, _, ctx: Context) => {
    // TODO
    return null;
  },
  qteStockPharmacie: async (parent, { idPharmacie, idLaboratoire }, ctx: Context) => {
    return ctx.prisma
      .produitStocks({
        where: { idPharmacie, /*idLaboratoire,*/ idProduit: parent['_id'] || parent.id },
      })
      .then(produitStocks => (produitStocks[0] && produitStocks[0].qteStock) || 0);
  },
  pharmacieReferences: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .produitStocks({
        where: { qteStock_gt: 0, idPharmacie_not: null, idProduit: parent['_id'] || parent.id },
      })
      .then(produitStocks => produitStocks.map(produitStock => produitStock.idPharmacie));
  },
};
