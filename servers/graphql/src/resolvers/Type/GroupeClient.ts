import { GroupeClientResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const GroupeClient: GroupeClientResolvers.Type = {
  ...GroupeClientResolvers.defaultResolvers,
  type: () => 'groupeClient',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  pharmacies: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupeClient({ id: parent['_id'] || parent.id }).pharmacies();
  },
};
