import { TodoEtiquetteParticipantResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TodoEtiquetteParticipant: TodoEtiquetteParticipantResolvers.Type = {
  ...TodoEtiquetteParticipantResolvers.defaultResolvers,
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  etiquette: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoEtiquetteParticipant({ id: parent['_id'] || parent.id }).etiquette();
  },
  userParticipant: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .todoEtiquetteParticipant({ id: parent['_id'] || parent.id })
      .userParticipant();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoEtiquetteParticipant({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .todoEtiquetteParticipant({ id: parent['_id'] || parent.id })
      .userModification();
  },
};
