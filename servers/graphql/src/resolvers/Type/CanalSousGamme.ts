import { CanalSousGammeResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { getCountProduits } from '../../repository/produitCanal';

export const CanalSousGamme: CanalSousGammeResolvers.Type = {
  ...CanalSousGammeResolvers.defaultResolvers,
  gammeCommercial: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .canalSousGamme({
        id: parent.id,
      })
      .canalGamme();
  },
  countCanalArticles: async (parent, { codeCanal }, ctx: Context) => {
    const result = await getCountProduits(
      [
        {
          term: {
            'canalSousGamme.id': parent['_id'] || parent.id,
          },
        },
        {
          term: {
            'commandeCanal.code': codeCanal ? codeCanal : 'PFL',
          },
        },
      ],
      ctx,
    );

    return result && result.total ? result.total : 0;
  },
};
