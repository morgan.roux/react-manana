import { PharmacieResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { getComments } from '../../services/comment';

export const Pharmacie: PharmacieResolvers.Type = {
  ...PharmacieResolvers.defaultResolvers,

  type: () => 'pharmacie',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  idGroupement: (parent, {}, ctx: Context) => {
    return ctx.prisma
      .pharmacie({
        id: parent['_id'] || parent.id,
      })
      .groupement()
      .then(groupement => groupement.id);
  },
  users: async (parent, {}, ctx: Context) => {
    if (parent['users']) {
      return parent['users'];
    }
    const userTitulaires = await ctx.prisma.userTitulaires({
      where: {
        idPharmacie: {
          id: parent['_id'] || parent.id,
        },
      },
    });
    const users = await Promise.all(
      userTitulaires.map(userTitulaire =>
        ctx.prisma.userTitulaire({ id: userTitulaire.id }).idUser(),
      ),
    );
    return users;
  },
  comments: (parent, { skip, first }, ctx: Context) => {
    return getComments(skip, first, parent['_id'] || parent.id, ctx);
  },
  titulaires: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .pharmacie({
        id: parent['_id'] || parent.id,
      })
      .titulaire()
      .then(titulaires => (titulaires && titulaires.length > 0 ? titulaires : null));
  },
  departement: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .pharmacie({
        id: parent['_id'] || parent.id,
      })
      .departement();
  },
  presidentRegion: async (parent, _, ctx: Context) => {
    const departement = await ctx.prisma
      .pharmacie({
        id: parent['_id'] || parent.id,
      })
      .departement();

    return departement
      ? ctx.prisma
          .titulaireAffectations({ where: { departement: { id: departement.id } } })
          .then(affections =>
            affections && affections.length ? affections[affections.length - 1] : null,
          )
      : null;
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .pharmacie({
        id: parent['_id'] || parent.id,
      })
      .userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .pharmacie({
        id: parent['_id'] || parent.id,
      })
      .userModification();
  },
  contact: async (parent, _, ctx: Context) => {
    return parent['contact']
      ? parent['contact']
      : ctx.prisma
          .pharmacie({
            id: parent['_id'] || parent.id,
          })
          .contact();
  },
  actived: async (parent, _, ctx: Context) => {
    return parent['actived'] != null && parent['actived'] !== undefined
      ? parent['actived']
      : ctx.prisma
          .pharmacie({
            id: parent['_id'] || parent.id,
          })
          .sortie()
          .then(sortie => (sortie && sortie === 1 ? false : true));
  },
  entreeSortie: async (parent, _, ctx: Context) => {
    const entreeSorties = await ctx.prisma.pharmacieEntreeSorties({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
    return entreeSorties && entreeSorties.length ? entreeSorties[0] : null;
  },
  achat: async (parent, _, ctx: Context) => {
    const achats = await ctx.prisma.pharmacieAchats({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
    return achats && achats.length ? achats[0] : null;
  },
  compta: async (parent, _, ctx: Context) => {
    const comptas = await ctx.prisma.pharmacieComptas({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
    return comptas && comptas.length ? comptas[0] : null;
  },
  concept: async (parent, _, ctx: Context) => {
    const concepts = await ctx.prisma.pharmacieConcepts({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
    return concepts && concepts.length ? concepts[0] : null;
  },
  digitales: async (parent, _, ctx: Context) => {
    return ctx.prisma.pharmacieDigitales({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
  },
  informatique: async (parent, _, ctx: Context) => {
    const informatiques = await ctx.prisma.pharmacieInformatiques({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
    return informatiques && informatiques.length ? informatiques[0] : null;
  },
  segmentation: async (parent, _, ctx: Context) => {
    const segmentations = await ctx.prisma.pharmacieSegmentations({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
    return segmentations && segmentations.length ? segmentations[0] : null;
  },
  statCA: async (parent, _, ctx: Context) => {
    const statCAs = await ctx.prisma.pharmacieStatCAs({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
    return statCAs && statCAs.length ? statCAs[0] : null;
  },
  grossistes: async (parent, _, ctx: Context) => {
    const pharmaGrossistes = await ctx.prisma.pharmacieGrossistes({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
    return pharmaGrossistes && pharmaGrossistes.length
      ? Promise.all(
          pharmaGrossistes.map(pharma =>
            ctx.prisma.pharmacieGrossiste({ id: pharma.id }).grossiste(),
          ),
        )
      : [];
  },
  generiqueurs: async (parent, _, ctx: Context) => {
    const pharmaGeneriqueurs = await ctx.prisma.pharmacieGeneriqueurs({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
    return pharmaGeneriqueurs && pharmaGeneriqueurs.length
      ? Promise.all(
          pharmaGeneriqueurs.map(pharma =>
            ctx.prisma.pharmacieGeneriqueur({ id: pharma.id }).generiqueur(),
          ),
        )
      : [];
  },
  satisfaction: async (parent, _, ctx: Context) => {
    const satisfactions = await ctx.prisma.pharmacieSatisfactions({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
    return satisfactions && satisfactions.length ? satisfactions[0] : null;
  },
  dernierCip: async (parent, _, ctx: Context) => {
    const cips = await ctx.prisma.pharmacieHistoCips({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
    return cips && cips.length ? cips[0].cip : null;
  },
  cap: async (parent, _, ctx: Context) => {
    const caps = await ctx.prisma.pharmacieCaps({
      where: { pharmacie: { id: parent['_id'] || parent.id } },
    });
    return caps && caps.length ? caps[0] : null;
  },
  nbReclamation: (parent, _, ctx: Context) => {
    return ctx.prisma
      .ticketChangementCiblesConnection({
        where: {
          pharmacieCible: { id: parent['_id'] || parent.id },
          ticket: { typeTicket: 'RECLAMATION' },
        },
      })
      .aggregate()
      .count();
  },
  nbAppel: (parent, _, ctx: Context) => {
    return ctx.prisma
      .ticketChangementCiblesConnection({
        where: {
          pharmacieCible: { id: parent['_id'] || parent.id },
          ticket: { typeTicket: 'APPEL' },
        },
      })
      .aggregate()
      .count();
  },
  nbTicket: (parent, _, ctx: Context) => {
    return ctx.prisma
      .ticketChangementCiblesConnection({
        where: {
          pharmacieCible: { id: parent['_id'] || parent.id },
        },
      })
      .aggregate()
      .count();
  },
};
