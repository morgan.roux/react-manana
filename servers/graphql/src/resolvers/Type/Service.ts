import { ServiceResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Service: ServiceResolvers.Type = {
  ...ServiceResolvers.defaultResolvers,
  type: () => 'service',
  id: (parent: any, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  countUsers: async (parent: any, { idGroupement }: { idGroupement: string }, ctx: Context) => {
    return ctx.prisma
      .personnelsConnection({
        where: {
          service: {
            id: parent['_id'] || parent.id,
          },
          groupement: { id: idGroupement },
          user: {
            id_not: null,
          },
        },
      })
      .aggregate()
      .count();
  },
  userCreation: (parent, _, ctx: Context) => {
    return ctx.prisma.service({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: (parent, _, ctx: Context) => {
    return ctx.prisma.service({ id: parent['_id'] || parent.id }).userModification();
  },
};
