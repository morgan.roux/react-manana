import { HelloIdSsoResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const HelloIdSso: HelloIdSsoResolvers.Type = {
  ...HelloIdSsoResolvers.defaultResolvers,

  groupement: (parent, args, ctx: Context) => {
    return ctx.prisma.helloIdSso({ id: parent.id }).groupement();
  },
};
