import { PartenaireRepresentantResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PartenaireRepresentant: PartenaireRepresentantResolvers.Type = {
  ...PartenaireRepresentantResolvers.defaultResolvers,
  type: () => 'partenairerepresentant',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  contact: async (parent, _, ctx: Context) => {
    return ctx.prisma.partenaireRepresentant({ id: parent['_id'] || parent.id }).contact();
  },
  partenaire: async (parent, _, ctx: Context) => {
    return ctx.prisma.partenaireRepresentant({ id: parent['_id'] || parent.id }).partenaire();
  },
  partenaireType: async (parent, _, ctx: Context) => {
    return ctx.prisma.partenaireRepresentant({ id: parent['_id'] || parent.id }).partenaireType();
  },
  photo: async (parent, _, ctx: Context) => {
    return ctx.prisma.partenaireRepresentant({ id: parent['_id'] || parent.id }).photo();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.partenaireRepresentant({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.partenaireRepresentant({ id: parent['_id'] || parent.id }).userModification();
  },
};
