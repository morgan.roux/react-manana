// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { CryptoMd5Resolvers } from "../../generated/graphqlgen";

export const CryptoMd5: CryptoMd5Resolvers.Type = {
  ...CryptoMd5Resolvers.defaultResolvers,

  externalUserMappings: (parent, args, ctx) => {
    return ctx.prisma.externalMappings({ where: {
      md5App : {id : parent.id}
    } });
  },
  SsoApplication: async (parent, args, ctx) => {
    const ssoApplication = await ctx.prisma.ssoApplications({
      where: { ssoTypeid: parent.id, ssoType : "CRYPTAGE_MD5" }
    });
    return ssoApplication[0]
  }
};
