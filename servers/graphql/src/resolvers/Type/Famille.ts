import { FamilleResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { getCountProduits } from '../../repository/produitCanal';
import { sousFamilles, parentFamille } from '../../repository/famille';

export const Famille: FamilleResolvers.Type = {
  ...FamilleResolvers.defaultResolvers,
  type: () => 'famille',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  countCanalArticles: async (parent, { codeCanal }, ctx: Context) => {
    const result = await getCountProduits(
      [
        {
          term: {
            'produit.famille.codeFamille': parent.codeFamille,
          },
        },
        {
          term: {
            'commandeCanal.code': codeCanal ? codeCanal : 'PFL',
          },
        },
      ],
      ctx,
    );

    return result && result.total ? result.total : 0;
  },
  parent: async (parent, _, ctx: Context) => {
    return parentFamille(ctx, parent.codeFamille);
  },
  sousFamilles: async (parent, _, ctx: Context) => {
    return sousFamilles(ctx, parent.codeFamille);
  },
};
