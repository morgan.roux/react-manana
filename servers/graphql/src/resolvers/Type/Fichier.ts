import { FichierResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import s3 from '../../services/s3';
import { getFilePublicUrl } from '../../sequelize/shared';

export const Fichier: FichierResolvers.Type = {
  ...FichierResolvers.defaultResolvers,
  urlPresigned: async (parent, _, ctx: Context) => {
    return parent['urlPresigned']
      ? parent['urlPresigned']
      : ctx.prisma
          .fichier({ id: parent.id })
          .then(fichier => (fichier ? s3.createPresignedGetUrl(fichier.chemin) : null));
  },
  publicUrl: async (parent, _, ctx: Context) => {
    return parent['publicUrl']
      ? parent['publicUrl']
      : ctx.prisma
          .fichier({ id: parent.id })
          .then(fichier => (fichier ? getFilePublicUrl(fichier.chemin) : null));
  },
  avatar: async (parent, _, ctx: Context) => {
    return ctx.prisma.fichier({ id: parent.id }).avatar();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.avatar({ id: parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.avatar({ id: parent.id }).userModification();
  },
};
