import { GammeCatalogueResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const GammeCatalogue: GammeCatalogueResolvers.Type = {
  ...GammeCatalogueResolvers.defaultResolvers,
  type: () => 'gammeCatalogue',
  id: async (parent, _, __) => {
    return parent['_id'] || parent.id;
  },
  sousGammes: async (parent, _, ctx: Context) => {
    return ctx.prisma.sousGammeCatalogues({
      where: {
        gammeCatalogue: { id: parent['_id'] || parent.id },
      },
    });
  },
};
