import { OperationArticleResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const OperationArticle: OperationArticleResolvers.Type = {
  ...OperationArticleResolvers.defaultResolvers,
  produitCanal: (parent, _, ctx: Context) => {
    return ctx.prisma.operationArticle({ id: parent.id }).produitCanal();
  },
};
