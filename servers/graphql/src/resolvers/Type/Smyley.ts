import { SmyleyResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
// import { getComments } from '../../services/comment';

export const Smyley: SmyleyResolvers.Type = {
  ...SmyleyResolvers.defaultResolvers,
  id: (parent, {}, ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  groupement: (parent, {}, ctx: Context) => {
    return ctx.prisma.smyley({ id: parent['_id'] || parent.id }).groupement();
  },
};
