import { GroupeAmisDetailResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const GroupeAmisDetail: GroupeAmisDetailResolvers.Type = {
  ...GroupeAmisDetailResolvers.defaultResolvers,
  type: () => 'groupeamisdetail',
  id: async (parent, _: any, __: any) => {
    return (parent['_id'] || parent.id) as any;
  },
  groupeAmis: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupeAmisDetail({ id: parent['_id'] || parent.id }).groupeAmis();
  },
  pharmacie: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupeAmisDetail({ id: parent['_id'] || parent.id }).pharmacie();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupeAmisDetail({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupeAmisDetail({ id: parent['_id'] || parent.id }).userModification();
  },
};
