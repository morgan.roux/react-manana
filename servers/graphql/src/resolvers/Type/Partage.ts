import { PartageResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Partage: PartageResolvers.Type = {
  ...PartageResolvers.defaultResolvers,
  type: () => 'partage',
  id: async (parent, _: any, __: any) => {
    return (parent['_id'] || parent.id) as any;
  },
  typePartage: async (parent, _, ctx: Context) => {
    return ctx.prisma.partage({ id: parent['_id'] || parent.id }).type();
  },
  fonctionCible: async (parent, _, ctx: Context) => {
    return ctx.prisma.partage({ id: parent['_id'] || parent.id }).fonctionCible();
  },
  pharmacieCinles: async (parent, _, ctx: Context) => {
    return ctx.prisma.partage({ id: parent['_id'] || parent.id }).pharmacieCinles();
  },
  groupeAmisCibles: async (parent, _, ctx: Context) => {
    return ctx.prisma.partage({ id: parent['_id'] || parent.id }).groupeAmisCibles();
  },
  item: async (parent, _, ctx: Context) => {
    return ctx.prisma.partage({ id: parent['_id'] || parent.id }).item();
  },
  userPartageant: async (parent, _, ctx: Context) => {
    return ctx.prisma.partage({ id: parent['_id'] || parent.id }).userPartageant();
  },

  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.partage({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.partage({ id: parent['_id'] || parent.id }).userModification();
  },
};
