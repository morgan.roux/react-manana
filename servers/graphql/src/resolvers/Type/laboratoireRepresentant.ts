import { Context } from '../../types';
import { LaboratoireRepresentantResolvers } from '../../generated/graphqlgen';

export const LaboratoireRepresentant: LaboratoireRepresentantResolvers.Type = {
  ...LaboratoireRepresentantResolvers.defaultResolvers,
  type: () => 'laboratoirerepresentant',
  id: async (parent, {}, ctx: Context) => {
    return parent['_id'] || parent.id;
  },
  contact: async (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoireRepresentant({ id: parent['_id'] || parent.id }).contact();
  },
  laboratoire: async (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoireRepresentant({ id: parent['_id'] || parent.id }).laboratoire();
  },
  photo: async (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoireRepresentant({ id: parent['_id'] || parent.id }).photo();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoireRepresentant({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .laboratoireRepresentant({ id: parent['_id'] || parent.id })
      .userModification();
  },
};
