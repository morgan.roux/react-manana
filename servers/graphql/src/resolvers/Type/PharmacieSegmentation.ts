import { PharmacieSegmentationResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PharmacieSegmentation: PharmacieSegmentationResolvers.Type = {
  ...PharmacieSegmentationResolvers.defaultResolvers,
  contrat: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieSegmentation({ id: parent.id }).contrat();
  },
  trancheCA: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieSegmentation({ id: parent.id }).trancheCA();
  },
  typologie: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieSegmentation({ id: parent.id }).typologie();
  },
  qualite: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieSegmentation({ id: parent.id }).qualite();
  },
};
