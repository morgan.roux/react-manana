import { ConnexionResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Connexion: ConnexionResolvers.Type = {
  ...ConnexionResolvers.defaultResolvers,
  user: (parent, {}, ctx: Context) => {
    return ctx.prisma.connexion({ id: parent.id }).idUser();
  },
};
