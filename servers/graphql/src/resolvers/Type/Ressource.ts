import { RessourceResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Ressource: RessourceResolvers.Type = {
  ...RessourceResolvers.defaultResolvers,
  type: () => 'ressource',
  id: (parent: any, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  groupement: (parent, _, ctx: Context) => {
    return ctx.prisma.ressource({ id: parent['_id'] || parent.id }).groupement();
  },
  item: (parent, _, ctx: Context) => {
    return ctx.prisma.ressource({ id: parent['_id'] || parent.id }).item();
  },
  fichier: (parent, _, ctx: Context) => {
    return ctx.prisma.ressource({ id: parent['_id'] || parent.id }).fichier();
  },
  partenaireService: (parent, _, ctx: Context) => {
    return ctx.prisma.ressource({ id: parent['_id'] || parent.id }).partenaireService();
  },
};
