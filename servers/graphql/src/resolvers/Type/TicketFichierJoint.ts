import { TicketFichierJointResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TicketFichierJoint: TicketFichierJointResolvers.Type = {
  ...TicketFichierJointResolvers.defaultResolvers,
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  fichier: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketFichierJoint({ id: parent['_id'] || parent.id }).fichier();
  },
  ticket: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketFichierJoint({ id: parent['_id'] || parent.id }).ticket();
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketFichierJoint({ id: parent['_id'] || parent.id }).groupement();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketFichierJoint({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketFichierJoint({ id: parent['_id'] || parent.id }).userModification();
  },
};
