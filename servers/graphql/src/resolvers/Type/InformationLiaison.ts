import { Context } from '../../types';
import { InformationLiaisonResolvers } from '../../generated/graphqlgen';
import { last } from 'lodash';
import { computeUrgenceCodeFromDateEcheance } from '../../sequelize/shared/compute-date-echeance';
import { Origine } from '../../sequelize/models';

export const InformationLiaison: InformationLiaisonResolvers.Type = {
  ...InformationLiaisonResolvers.defaultResolvers,
  type: () => 'informationliaison',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },

  origineAssocie: async (parent, _, ctx: Context) => {
    if (parent.idOrigine && parent.idOrigineAssocie) {
      const origine = await Origine.findByPk(parent.idOrigine)

      if (origine.code === 'LABORATOIRE') {
        return ctx.prisma.laboratoire({ id: parent.idOrigineAssocie })
          .then(item => item ? { ...item, type: 'laboratoire' } : null)
          .catch((error) => null)

      }

      if (origine.code === 'GROUPE_AMIS') {
        return ctx.prisma.groupeAmis({ id: parent.idOrigineAssocie })
        .then(item => item ? { ...item, type: 'groupeamis' } : null)
          .catch((error) => null)
      }

      if (origine.code === 'PRESTATAIRE_SERVICE') {
        return ctx.prisma.partenaire({ id: parent.idOrigineAssocie })
        .then(item => item ? { ...item, type: 'partenaire' } : null)

          .catch((error) => null)
      }

      if (origine.code === 'INTERNE') {
        return ctx.prisma.user({ id: parent.idOrigineAssocie })
        .then(item => item ? { ...item, type: 'user' } : null)

          .catch((error) => null)
      }

      //TODO: Add service
    }

    return null

  },

  item: async (parent, _, ctx: Context) => {
    return parent.idItem ? ctx.prisma.item({ id: parent.idItem }) : null;
  },
  declarant: async (parent, _, ctx: Context) => {
    return ctx.prisma.informationLiaison({ id: parent['_id'] || parent.id }).declarant();
  },
  colleguesConcernees: async (parent, _, ctx: Context) => {
    return ctx.prisma.informationLiaison({ id: parent['_id'] || parent.id }).colleguesConcernees();
  },
  ficheReclamation: async (parent, _, ctx: Context) => {
    return ctx.prisma.informationLiaison({ id: parent['_id'] || parent.id }).ficheReclamation();
  },
  todoAction: async (parent, _, ctx: Context) => {
    return ctx.prisma.informationLiaison({ id: parent['_id'] || parent.id }).todoAction();
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.informationLiaison({ id: parent['_id'] || parent.id }).groupement();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.informationLiaison({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.informationLiaison({ id: parent['_id'] || parent.id }).userModification();
  },
  prisEnCharge: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonPrisCharges({
        where: {
          isRemoved: false,
          informationLiaison: { id: parent['_id'] || parent.id, statut: 'CLOTURE' },
          userCreation: { id: ctx.userId },
        },
      })
      .then(results => {
        const res = last(results);
        return res;
      });
  },
  fichiersJoints: async (parent, _, ctx: Context) => {
    return ctx.prisma.informationLiaisonFichierJoints({
      where: { informationLiaison: { id: parent['_id'] || parent.id }, isRemoved: false },
    });
  },
  userSmyleys: async (parent, _, ctx: Context) => {
    return {
      total: await ctx.prisma
        .userSmyleysConnection({
          where: {
            item: { code: 'INFORMATION_LIAISON' },
            idSource: parent['_id'] ? parent['_id'] : parent.id,
            groupement: { id: ctx.groupementId },
            isRemoved: false,
          },
        })
        .aggregate()
        .count(),
      data: await ctx.prisma.userSmyleys({
        where: {
          item: { code: 'INFORMATION_LIAISON' },
          idSource: parent['_id'] ? parent['_id'] : parent.id,
          groupement: { id: ctx.groupementId },
          isRemoved: false,
        },
      }),
    } as any;
  },
  nbPartage: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partagesConnection({
        where: {
          item: { code: 'INFORMATION_LIAISON' },
          idItemAssocie: parent['_id'] || parent.id,
          isRemoved: false,
          type: 'PARTAGE',
        },
      })
      .aggregate()
      .count();
  },
  nbRecommandation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partagesConnection({
        where: {
          item: { code: 'INFORMATION_LIAISON' },
          idItemAssocie: parent['_id'] || parent.id,
          isRemoved: false,
          type: 'RECOMMANDATION',
        },
      })
      .aggregate()
      .count();
  },
  isShared: async (parent, _, ctx: Context) => {
    return ctx.prisma.$exists.partage({
      item: { code: 'INFORMATION_LIAISON' },
      idItemAssocie: parent['_id'] || parent.id,
      isRemoved: false,
      type: 'PARTAGE',
    });
  },
  urgence: async (parent, _, ctx: Context) => {
    if (parent.dateEcheance) {
      const code = computeUrgenceCodeFromDateEcheance(parent.dateEcheance);
      const urgences = await ctx.prisma.urgences({ where: { code } });
      return urgences[0];
    }
    return ctx.prisma.informationLiaison({ id: parent['_id'] || parent.id }).urgence();
  },
  importance: async (parent, _, ctx: Context) => {
    return ctx.prisma.informationLiaison({ id: parent['_id'] || parent.id }).importance();
  },
  statut: async (parent, _, ctx: Context) => {
    return ctx.prisma.informationLiaison({ id: parent['_id'] || parent.id }).statut();
  },
  coupleStatutDeclarant: async (parent, _, ctx: Context) => {
    const statut = await ctx.prisma
      .informationLiaison({
        id: parent['_id'] || parent.id,
      })
      .statut();

    const idDeclarant = await ctx.prisma
      .informationLiaison({
        id: parent['_id'] || parent.id,
      })
      .declarant()
      .id();

    return `${idDeclarant}-${statut}`;
  },
  priority: async (parent, _, ctx: Context) => {
    const urgence = await ctx.prisma
      .informationLiaison({ id: parent['_id'] || parent.id })
      .urgence();
    const importance = await ctx.prisma
      .informationLiaison({ id: parent['_id'] || parent.id })
      .importance();

    return `${importance?.ordre || '1'}${urgence?.code || 'A'}`;
  },
  pharmacie: async (parent, _, ctx: Context) => {
    return ctx.prisma.informationLiaison({ id: parent['_id'] || parent.id }).pharmacie();
  },
  nbCollegue: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonUserConcerneesConnection({
        where: { informationLiaison: { id: parent['_id'] || parent.id } },
      })
      .aggregate()
      .count();
  },
  nbLue: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonUserConcerneesConnection({
        where: {
          informationLiaison: { id: parent['_id'] || parent.id },
          statut: 'LUES',
        },
      })
      .aggregate()
      .count();
  },
  nbComment: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .commentsConnection({
        where: { idItemAssocie: parent['_id'] || parent.id, isRemoved: false },
      })
      .aggregate()
      .count();
  },
};
