import { ItemResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { fichierImageItem } from '../../repository/item';

export const Item: ItemResolvers.Type = {
  ...ItemResolvers.defaultResolvers,
  type: () => 'item',
  id: async (parent, {}, ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  fichier: async (parent, _, ctx: Context) => {
    return fichierImageItem(ctx, { code: parent.code });
  },
  parent: async (parent, _, ctx: Context) => {
    return parent.codeItem.length === 1
      ? null
      : ctx.prisma.item({ codeItem: parent.codeItem.slice(0, -1) });
  },
};
