import { UserSmyleyResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const UserSmyley: UserSmyleyResolvers.Type = {
  ...UserSmyleyResolvers.defaultResolvers,
  id: (parent, _, ctx: Context) => {
    return parent['_id'] || parent.id;
  },
  user: (parent, _, ctx: Context) => {
    return ctx.prisma.userSmyley({ id: parent['_id'] || parent.id }).user();
  },
  smyley: (parent, _, ctx: Context) => {
    return ctx.prisma.userSmyley({ id: parent['_id'] || parent.id }).smyley();
  },
  item: async (parent, _, ctx: Context) => {
    return ctx.prisma.userSmyley({ id: parent['_id'] || parent.id }).item();
  },
  groupement: (parent, _, ctx: Context) => {
    return ctx.prisma.userSmyley({ id: parent['_id'] || parent.id }).groupement();
  },
};
