import { TodoActionEtiquetteResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TodoActionEtiquette: TodoActionEtiquetteResolvers.Type = {
  ...TodoActionEtiquetteResolvers.defaultResolvers,
  type: () => 'todoactionetiquette',
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  action: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionEtiquette({ id: parent['_id'] || parent.id }).action();
  },
  etiquette: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionEtiquette({ id: parent['_id'] || parent.id }).etiquette();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionEtiquette({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionEtiquette({ id: parent['_id'] || parent.id }).userModification();
  },
};
