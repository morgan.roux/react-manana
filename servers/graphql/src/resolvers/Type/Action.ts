import { head, last } from 'lodash';
import { ActionResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

import moment from 'moment';

export const computeCodeFromdateDebut = (input: any): 'A' | 'B' | 'C' => {
  const today = moment();
  const dateDebut = moment(input);
  const nextSunday = moment()
    .endOf('isoWeek')
    .add(1, 'week');

  if (dateDebut.day() === today.day() || dateDebut.isSameOrBefore(today)) {
    return 'A';
  }
  if (dateDebut > today && dateDebut <= nextSunday) {
    return 'B';
  }
  return 'C';
};
export const Action: ActionResolvers.Type = {
  ...ActionResolvers.defaultResolvers,
  type: () => 'action',
  id: async (parent, _: any, __: any) => {
    return (parent['_id'] || parent.id) as any;
  },
  item: async (parent, _, ctx: Context) => {
    return ctx.prisma.action({ id: parent['_id'] || parent.id }).item();
  },
  project: async (parent, _, ctx: Context) => {
    return ctx.prisma.action({ id: parent['_id'] || parent.id }).project();
  },
  actionType: async (parent, _, ctx: Context) => {
    return ctx.prisma.action({ id: parent['_id'] || parent.id }).actionType();
  },
  actionParent: async (parent, _, ctx: Context) => {
    return ctx.prisma.action({ id: parent['_id'] || parent.id }).actionParent();
  },
  origine: async (parent, _, ctx: Context) => {
    return ctx.prisma.action({ id: parent['_id'] || parent.id }).origine();
  },
  section: async (parent, _, ctx: Context) => {
    return ctx.prisma.action({ id: parent['_id'] || parent.id }).section();
  },
  status: async (parent, _, ctx: Context) => {
    return ctx.prisma.action({ id: parent['_id'] || parent.id }).actionStatus();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.action({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.action({ id: parent['_id'] || parent.id }).userModification();
  },
  assignedUsers: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .todoActionAttributions({
        where: { action: { id: parent['_id'] || parent.id }, isRemoved: false },
      })
      .then(todoActionAttributions => {
        return Promise.all(
          todoActionAttributions.map(todoActionAttribution => {
            return ctx.prisma
              .todoActionAttribution({ id: todoActionAttribution.id })
              .userDestination();
          }),
        );
      });
  },
  importance: async (parent, _, ctx: Context) => {
    return ctx.prisma.action({ id: parent['_id'] || parent.id }).importance();
  },
  etiquettes: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .todoActionEtiquettes({
        where: { action: { id: parent['_id'] || parent.id }, isRemoved: false },
      })
      .then(todoActionEtiquettes => {
        return Promise.all(
          todoActionEtiquettes.map(todoActionEtiquette => {
            return ctx.prisma.todoActionEtiquette({ id: todoActionEtiquette.id }).etiquette();
          }),
        );
      });
  },
  nbComment: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .commentsConnection({
        where: { idItemAssocie: parent['_id'] || parent.id, isRemoved: false },
      })
      .aggregate()
      .count();
  },
  subActions: async (parent, _, ctx: Context) => {
    return ctx.prisma.actions({
      where: { actionParent: { id: parent['_id'] || parent.id }, isRemoved: false },
    });
  },
  isAssigned: async (parent, _, ctx: Context) => {
    return ctx.prisma.$exists.todoActionAttribution({
      action: { id: parent['_id'] || parent.id },
      isRemoved: false,
    });
  },
  firstComment: async (parent, _, ctx: Context) => {
    const userCreation = await ctx.prisma.action({ id: parent['_id'] || parent.id }).userCreation();
    return ctx.prisma
      .comments({
        where: {
          item: { code: 'TODO' },
          idItemAssocie: parent['_id'] || parent.id,
          isRemoved: false,
          // userCreation: { id: userCreation.id },
        },
        orderBy: 'dateCreation_ASC',
      })
      .then(async results => {
        if (results && results.length > 0) {
          const res = head(results);
          return ctx.prisma.comment({ id: res.id });
        }
      });
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.action({ id: parent['_id'] || parent.id }).groupement();
  },

  priority: async (parent, _, ctx: Context) => {
    const urgenceValues = { A: 1, B: 2, C: 3 };
    const urgenceCode = computeCodeFromdateDebut(parent);
    const importanceOrdre = await ctx.prisma
      .action({ id: parent['_id'] || parent.id })
      .importance()
      .ordre();
    const dateDebut = await ctx.prisma.action({ id: parent['_id'] || parent.id }).dateDebut();
    const datePriority = 1 / new Date(dateDebut).getDate();
    return urgenceValues[urgenceCode] + importanceOrdre + datePriority;
  },
  fichierJoints: async (parent, _, ctx: Context) => {
    const commentaireFichiers = await ctx.prisma
      .comments({ where: { idItemAssocie: parent['_id'] || parent.id } })
      .then(comments =>
        Promise.all(
          comments.map(comment =>
            ctx.prisma
              .commentaireFichierJoints({ where: { commentaire: { id: comment.id } } })
              .then(commentaireFichierJoints =>
                Promise.all(
                  commentaireFichierJoints.map(async commentaireFichierJoint => {
                    const fichiers = await ctx.prisma
                      .commentaireFichierJoint({ id: commentaireFichierJoint.id })
                      .fichier();
                    return fichiers;
                  }),
                ),
              ),
          ),
        ),
      );
    return [...commentaireFichiers.flat()];
  },
};
