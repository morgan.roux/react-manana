import { CommandeCanalResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import moment from 'moment';
import { getCountProduits } from '../../repository/produitCanal';

export const CommandeCanal: CommandeCanalResolvers.Type = {
  ...CommandeCanalResolvers.defaultResolvers,
  type: () => 'commandeCanal',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  countOperationsCommercials: async (parent, _, ctx: Context) => {
    const now = moment()
      .format()
      .toString();
    const operationGroupementCanal: number =
      ctx.role === 'SUPADM' || ctx.role === 'GRPADM'
        ? await ctx.prisma
            .operationPharmaciesConnection({
              where: {
                AND: [
                  {
                    idOperation: {
                      groupement: { id: ctx.groupementId },
                    },
                  },
                  {
                    idOperation: {
                      idCommandeCanal: { id: parent['_id'] || parent.id },
                    },
                  },
                  {
                    globalite: true,
                  },
                  {
                    idOperation: {
                      isRemoved: false,
                    },
                  },
                ],
              },
            })
            .aggregate()
            .count()
        : await ctx.prisma
            .operationPharmaciesConnection({
              where: {
                AND: [
                  {
                    idOperation: {
                      groupement: { id: ctx.groupementId },
                    },
                  },
                  {
                    idOperation: {
                      idCommandeCanal: { id: parent['_id'] || parent.id },
                    },
                  },
                  {
                    globalite: true,
                  },
                  {
                    idOperation: {
                      isRemoved: false,
                    },
                  },
                  {
                    idOperation: {
                      dateDebut_lte: now,
                    },
                  },
                  {
                    idOperation: {
                      dateFin_gte: now,
                    },
                  },
                ],
              },
            })
            .aggregate()
            .count();

    const operationPharmacieCanal: number =
      ctx.role === 'SUPADM' || ctx.role === 'GRPADM'
        ? await ctx.prisma
            .operationPharmacieDetailsConnection({
              where: {
                AND: [
                  {
                    idOperation: {
                      groupement: { id: ctx.groupementId },
                    },
                  },
                  {
                    idPharmacie: { id: ctx.pharmacieId },
                  },
                  {
                    idOperation: { idCommandeCanal: { id: parent['_id'] || parent.id } },
                  },
                  {
                    idOperationPharmacie: { globalite: false },
                  },
                  {
                    idOperation: {
                      isRemoved: false,
                    },
                  },
                ],
              },
            })
            .aggregate()
            .count()
        : await ctx.prisma
            .operationPharmacieDetailsConnection({
              where: {
                AND: [
                  {
                    idOperation: {
                      groupement: { id: ctx.groupementId },
                    },
                  },
                  {
                    idPharmacie: { id: ctx.pharmacieId },
                  },
                  {
                    idOperation: { idCommandeCanal: { id: parent['_id'] || parent.id } },
                  },
                  {
                    idOperationPharmacie: { globalite: false },
                  },
                  {
                    idOperation: {
                      isRemoved: false,
                    },
                  },
                  {
                    idOperation: {
                      dateDebut_lte: now,
                    },
                  },
                  {
                    idOperation: {
                      dateFin_gte: now,
                    },
                  },
                ],
              },
            })
            .aggregate()
            .count();

    return operationGroupementCanal + operationPharmacieCanal;
  },
  countCanalArticles: async (parent, _, ctx: Context) => {
    const result = await getCountProduits(
      [
        {
          term: {
            'commandeCanal.id': parent['_id'] ? parent['_id'] : parent.id,
          },
        },
      ],
      ctx,
    );

    return result && result.total ? result.total : 0;
  },
};
