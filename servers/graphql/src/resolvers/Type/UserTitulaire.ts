import { UserTitulaireResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const UserTitulaire: UserTitulaireResolvers.Type = {
  ...UserTitulaireResolvers.defaultResolvers,
  isPresident: async (parent, _, ctx: Context) => {
    if (!(await ctx.prisma.userTitulaire({ id: parent.id }))) return false;
    return (await ctx.prisma.userTitulaire({ id: parent.id }).idPharmacie()) ? false : true;
  },
  titulaire: async (parent, _, ctx: Context) => {
    return ctx.prisma.userTitulaire({ id: parent.id }).idTitulaire();
  },
};
