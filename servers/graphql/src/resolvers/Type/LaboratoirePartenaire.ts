import { LaboratoirePartenaireResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const LaboratoirePartenaire: LaboratoirePartenaireResolvers.Type = {
  ...LaboratoirePartenaireResolvers.defaultResolvers,
  type: () => 'laboratoirepartenaire',
  id: async (parent, {}, ctx: Context) => {
    return parent['_id'] || parent.id;
  },
};
