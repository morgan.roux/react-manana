import { PanierLigneResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PanierLigne: PanierLigneResolvers.Type = {
  ...PanierLigneResolvers.defaultResolvers,
  produitCanal: async (parent, {}, ctx: Context) => {
    return ctx.prisma.panierLigne({ id: parent.id }).produitCanal();
  },
};
