import { PromotionGroupeClientResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PromotionGroupeClient: PromotionGroupeClientResolvers.Type = {
  ...PromotionGroupeClientResolvers.defaultResolvers,
  groupeClient: async (parent, _, ctx: Context) => {
    return ctx.prisma.promotionGroupeClient({ id: parent.id }).groupeClient();
  },
  remise: async (parent, _, ctx: Context) => {
    return ctx.prisma.promotionGroupeClient({ id: parent.id }).remise();
  },
};
