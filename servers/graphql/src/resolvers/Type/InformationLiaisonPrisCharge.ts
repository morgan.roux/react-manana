import { Context } from '../../types';
import { InformationLiaisonPrisChargeResolvers } from '../../generated/graphqlgen';

export const InformationLiaisonPrisCharge: InformationLiaisonPrisChargeResolvers.Type = {
  ...InformationLiaisonPrisChargeResolvers.defaultResolvers,
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  informationLiaison: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonPrisCharge({ id: parent['_id'] || parent.id })
      .informationLiaison();
  },
  fichiersJoints: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonPrisCharge({ id: parent['_id'] || parent.id })
      .fichiersJoints();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonPrisCharge({ id: parent['_id'] || parent.id })
      .userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonPrisCharge({ id: parent['_id'] || parent.id })
      .userModification();
  },
};
