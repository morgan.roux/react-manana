import { getDefaultShouldTerm, getUserSource, searchTodos } from '../../repository/action';
import { TodoEtiquetteResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TodoEtiquette: TodoEtiquetteResolvers.Type = {
  ...TodoEtiquetteResolvers.defaultResolvers,
  type: () => 'todoetiquette',
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoEtiquette({ id: parent['_id'] || parent.id }).userCreation();
  },
  couleur: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoEtiquette({ id: parent['_id'] || parent.id }).couleur();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoEtiquette({ id: parent['_id'] || parent.id }).userModification();
  },
  isInFavoris: async (parent, _, ctx: Context) => {
    return ctx.prisma.$exists.todoEtiquetteFavoris({
      etiquette: { id: parent['_id'] || parent.id },
      user: { id: ctx.userId },
      isRemoved: false,
    });
  },
  nbAction: async (parent, { actionStatus }, ctx: Context) => {
    const termActive = actionStatus
      ? actionStatus === 'ACTIVE'
        ? [{ term: { status: 'ACTIVE' } }]
        : [{ term: { status: 'DONE' } }]
      : [{ term: { status: 'ACTIVE' } }];

    const must = [
      { term: { isRemoved: false } },
      ...termActive,
      { term: { 'etiquettes.id': parent['_id'] || parent.id } },
      { term: { 'project.participants.id': ctx.userId } }
    ];

    const { defaultShouldCreatedTeam } = await getUserSource(ctx, ctx.userId);

    return searchTodos(
      ctx,
      must,
      [],
      [
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'assignedUsers',
                },
              },
            ],
            must: {
              term: { 'userCreation.id': ctx.userId },
            },
          },
        },
        { term: { 'assignedUsers.id': ctx.userId } },
        {
          bool: {
            must: [
              {
                exists: {
                  field: 'project.participants.id',
                },
              },
              ...defaultShouldCreatedTeam,
            ],
          },
        },
      ],
    );
  },
  pharmacie: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoEtiquette({ id: parent['_id'] || parent.id }).pharmacie();
  },
  participants: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .todoEtiquetteParticipants({
        where: { etiquette: { id: parent['_id'] || parent.id }, isRemoved: false },
      })
      .then((todoEtiquetteParticipants) => {
        return Promise.all(
          todoEtiquetteParticipants.map((todoEtiquetteParticipant) => {
            return ctx.prisma
              .todoEtiquetteParticipant({ id: todoEtiquetteParticipant.id })
              .userParticipant();
          }),
        );
      });
  },
  isShared: async (parent, _, ctx: Context) => {
    return ctx.prisma.$exists.todoEtiquetteParticipant({
      etiquette: { id: parent['_id'] || parent.id },
      isRemoved: false,
    });
  },
  activeActions: async (parent, _, ctx: Context) => {
    return ctx.prisma.$exists.todoActionEtiquette({
      etiquette: {
        id: parent['_id'] || parent.id,
      },
      action: {
        isRemoved: false,
        actionStatus: 'ACTIVE',
        OR: [
          {
            userCreation: { id: ctx.userId },
          },
          {
            actionAttributions_some: {
              userDestination: {
                id: ctx.userId,
              },
            },
          },
        ],
      },
    });
  },
};
