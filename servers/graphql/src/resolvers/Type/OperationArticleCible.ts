import { OperationArticleCibleResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const OperationArticleCible: OperationArticleCibleResolvers.Type = {
  ...OperationArticleCibleResolvers.defaultResolvers,
  fichierCible: (parent, _, ctx: Context) => {
    return ctx.prisma.operationArticleCible({ id: parent.id }).fichierCible();
  },
};
