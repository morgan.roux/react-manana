import { OperationResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { itemActions } from '../../repository/action';
import { usersPharmacie } from '../../repository/user';
import {
  caTotalOperationCommande,
  totalOperationProduitCommandees,
  pharmaciesCommandeeOperation,
  caMoyenOperations,
  caTotalOperationCommandeByPharmacie,
  commandesOperation,
} from '../../repository/operation';
import {
  SUPER_ADMINISTRATEUR,
  ADMINISTRATEUR_GROUPEMENT,
  COLLABORATEUR_COMMERCIAL,
  COLLABORATEUR_NON_COMMERCIAL,
  GROUPEMENT_AUTRE,
} from '../../constants/roles';

const adminRoles = [
  SUPER_ADMINISTRATEUR,
  ADMINISTRATEUR_GROUPEMENT,
  COLLABORATEUR_COMMERCIAL,
  COLLABORATEUR_NON_COMMERCIAL,
  GROUPEMENT_AUTRE,
];

export const Operation: OperationResolvers.Type = {
  ...OperationResolvers.defaultResolvers,
  type: () => 'operation',
  id: async (parent, {}, ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  idGroupement: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .operation({ id: parent['_id'] || parent.id })
      .groupement()
      .then(groupement => (groupement ? groupement.id : null));
  },
  laboratoire: (parent, _, ctx: Context) => {
    return ctx.prisma.operation({ id: parent['_id'] || parent.id }).idLaboratoire();
  },
  operationArticleCible: async (parent, _, ctx: Context) => {
    const operationArticleCible = await ctx.prisma.operationArticleCibles({
      where: { idOperation: { id: parent['_id'] || parent.id } },
    });
    return operationArticleCible[0];
  },
  pharmacieCible: (parent, _, ctx: Context) => {
    return ctx.prisma
      .operationPharmacies({
        where: {
          AND: [{ idOperation: { id: parent['_id'] || parent.id } }, { globalite: true }],
        },
      })
      .then(operationPharmacies => {
        if (operationPharmacies && operationPharmacies.length > 0) {
          return null;
        } else {
          return ctx.prisma
            .operationPharmacieDetails({
              where: { idOperation: { id: parent['_id'] || parent.id } },
            })
            .then(operationPharmacieDetail => {
              return Promise.all(
                operationPharmacieDetail.map(operation => {
                  return ctx.prisma.operationPharmacieDetail({ id: operation.id }).idPharmacie();
                }),
              );
            });
        }
      });
  },
  operationPharmacie: async (parent, _, ctx: Context) => {
    const operationP = await ctx.prisma
      .operationPharmacies({
        where: { idOperation: { id: parent['_id'] || parent.id } },
      })
      .then(pharmacies => {
        return pharmacies && pharmacies.length ? pharmacies[0] : null;
      });
    return operationP;
  },
  operationArticles: async (parent, { takeArticles, skipArticles }, ctx: Context) => {
    return {
      total: await ctx.prisma
        .operationArticlesConnection({
          where: { idOperation: { id: parent['_id'] || parent.id } },
        })
        .aggregate()
        .count(),
      data: await ctx.prisma.operationArticles({
        where: { idOperation: { id: parent['_id'] || parent.id } },
        first: takeArticles,
        skip: skipArticles,
        orderBy: 'dateCreation_ASC',
      }),
    } as any;
  },
  operationArticlesWithQte: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .operationArticles({
        where: { idOperation: { id: parent['_id'] || parent.id } },
      })
      .then(operationArticles =>
        operationArticles.filter(operationArticle => operationArticle.quantite > 0),
      );
  },
  commandeCanal: (parent, _, ctx: Context) => {
    return ctx.prisma.operation({ id: parent['_id'] || parent.id }).idCommandeCanal();
  },
  commandeType: (parent, _, ctx: Context) => {
    return ctx.prisma.operation({ id: parent['_id'] || parent.id }).idCommandeType();
  },
  actions: async (parent, { take, skip }, ctx: Context) => {
    return itemActions(
      {
        take,
        skip,
        codeItem: 'OPERACOM',
        idItemAssocie: parent['_id'] ? parent['_id'] : parent.id,
      },
      ctx,
    ).then(action => action as any);
  },
  fichierPresentations: (parent, _, ctx: Context) => {
    return ctx.prisma
      .operation({ id: parent.id || parent['_id'] })
      .fichierPresentations({ orderBy: 'dateModification_ASC' });
  },
  dateCreation: (parent, _, ctx: Context) => {
    return ctx.prisma.operation({ id: parent.id || parent['_id'] }).dateCreation();
  },
  dateModification: (parent, _, ctx: Context) => {
    return ctx.prisma.operation({ id: parent.id || parent['_id'] }).dateModification();
  },
  isRemoved: (parent, _, ctx: Context) => {
    return ctx.prisma.operation({ id: parent.id || parent['_id'] }).isRemoved();
  },
  accordCommercial: (parent, _, ctx: Context) => {
    return ctx.prisma.operation({ id: parent.id || parent['_id'] }).accordCommercial();
  },
  pharmacieVisitors: async (parent, _, ctx: Context) => {
    const operationV = await ctx.prisma.operationViewers({
      where: { idOperation: { id: parent.id || parent['_id'] } },
    });

    const pharmacies = await Promise.all(
      operationV.map(operv => {
        return ctx.prisma.operationViewer({ id: operv.id }).idPharmacie();
      }),
    );
    const uniquesPharmacies = pharmacies.filter((v, i, a) => a.findIndex(t => t.id === v.id) === i);

    return uniquesPharmacies;
  },
  CAMoyenParPharmacie: async (parent, _, ctx: Context) => {
    return caMoyenOperations(ctx, { id: parent.id || parent['_id'] });
  },
  CATotal: async (parent, _, ctx: Context) => {
    // calcule le total global des commandes des pharmacies
    // de l'operation commerciale
    const result = (await caTotalOperationCommande(ctx, { id: parent.id || parent['_id'] }))
      .totalNet;
    return parseFloat(result.toFixed(2));
  },
  nombreProduitsMoyenParPharmacie: async (parent, _, ctx: Context) => {
    const tpcPharmacie = await totalOperationProduitCommandees(ctx, parent['_id'] || parent.id);
    const npCommande = (await pharmaciesCommandeeOperation(parent['_id'] || parent.id, ctx)).length;

    if (npCommande === 0) return 0;
    const result = tpcPharmacie / npCommande;
    return isNaN(result) ? 0 : parseFloat(result.toFixed(2));
  },
  comments: async (parent, { takeComments, skipComments }, ctx: Context) => {
    return {
      total: await ctx.prisma
        .commentsConnection({
          where: {
            idItemAssocie: parent['_id'] ? parent['_id'] : parent.id,
            groupement: { id: ctx.groupementId },
            isRemoved: false,
          },
        })
        .aggregate()
        .count(),
      data: await ctx.prisma.comments({
        where: {
          idItemAssocie: parent['_id'] ? parent['_id'] : parent.id,
          groupement: { id: ctx.groupementId },
          isRemoved: false,
        },
        first: takeComments,
        skip: skipComments,
        orderBy: 'dateCreation_ASC',
      }),
    } as any;
  },
  userSmyleys: async (parent, _, ctx: Context) => {
    return {
      total: await ctx.prisma
        .userSmyleysConnection({
          where: {
            idSource: parent['_id'] ? parent['_id'] : parent.id,
            groupement: { id: ctx.groupementId },
            isRemoved: false,
          },
        })
        .aggregate()
        .count(),
      data: await ctx.prisma.userSmyleys({
        where: {
          idSource: parent['_id'] ? parent['_id'] : parent.id,
          groupement: { id: ctx.groupementId },
          isRemoved: false,
        },
      }),
    } as any;
  },
  commandePassee: async (parent, _, ctx: Context) => {
    const commandes =
      ctx.role && adminRoles.includes(ctx.role)
        ? (await commandesOperation(ctx, { id: parent['_id'] || parent.id })).length
        : (
            await caTotalOperationCommandeByPharmacie(ctx, {
              idOperation: parent['_id'] || parent.id,
              idPharmacie: ctx.pharmacieId,
            })
          ).nbCommande;
    return commandes && commandes > 0 ? true : false;
  },
  actualite: async (parent, _, ctx: Context) => {
    return ctx.prisma.operation({ id: parent['_id'] || parent.id }).idActualite();
  },
  seen: async (parent, { idPharmacie, userId }, ctx: Context) => {
    const users = idPharmacie
      ? (await usersPharmacie(ctx, { idPharmacie })).filter(user => user)
      : [];

    const usersOperation =
      users && users.length
        ? await ctx.prisma
            .operation({ id: parent['_id'] || parent.id })
            .user({ where: { id_in: users.map(user => user.id) } })
        : userId
        ? await ctx.prisma
            .operation({ id: parent['_id'] || parent.id })
            .user({ where: { id: userId } })
        : [];
    return usersOperation && usersOperation.length ? true : false;
  },
  nombrePharmaciesConsultes: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .operationViewersConnection({
        where: {
          idOperation: { id: parent.id || parent['_id'] },
        },
      })
      .aggregate()
      .count();
  },
  nombrePharmaciesCommandees: async (parent, _, ctx: Context) => {
    /* nombre de pharmacie qui a commandé cette operation */
    return (await pharmaciesCommandeeOperation(parent['_id'] || parent.id, ctx)).length;
  },
  nombreProduitsCommandes: async (parent, _, ctx: Context) => {
    /* Nombre de produit commandée par le pharmacie current */
    const commandes = await ctx.prisma.commandes({
      where: {
        AND: [
          { idOperation: { id: parent['_id'] || parent.id } },
          { idPharmacie: { id: ctx.pharmacieId } },
        ],
      },
    });

    const commandeLignes = await Promise.all(
      commandes.map(commande => {
        return ctx.prisma.commandeLignes({
          where: { idCommande: { id: commande.id } },
        });
      }),
    );
    const flatedCommandeLignes = commandeLignes.flat();

    const articleCommandes = await Promise.all(
      flatedCommandeLignes.map(commandeLigne => {
        return ctx.prisma.commandeLigne({ id: commandeLigne.id }).produitCanal();
      }),
    );

    const uniquesArticle = [...new Set(articleCommandes.map(article => article.id))];

    return uniquesArticle.length;
  },
  nombreTotalTypeProduitsCommandes: async (parent, _, ctx: Context) => {
    /* Total nombre de type produit commandée */
    const commandes = await ctx.prisma.commandes({
      where: {
        AND: [{ idOperation: { id: parent['_id'] || parent.id } }],
      },
    });

    const commandeLignes = await Promise.all(
      commandes.map(commande => {
        return ctx.prisma.commandeLignes({
          where: { idCommande: { id: commande.id } },
        });
      }),
    );
    const flatedCommandeLignes = commandeLignes.flat();

    const articleCommandes = await Promise.all(
      flatedCommandeLignes.map(commandeLigne => {
        return ctx.prisma.commandeLigne({ id: commandeLigne.id }).produitCanal();
      }),
    );

    const uniquesArticle = [...new Set(articleCommandes.map(article => article.id))];

    return uniquesArticle.length;
  },
  qteProduitsPharmacieCommandePassee: async (parent, _, ctx: Context) => {
    return totalOperationProduitCommandees(ctx, parent['_id'] || parent.id, ctx.pharmacieId);
  },
  qteTotalProduitsCommandePassee: async (parent, _, ctx: Context) => {
    return totalOperationProduitCommandees(ctx, parent['_id'] || parent.id);
  },
  nombreCommandePassee: async (parent, _, ctx: Context) => {
    return (
      await caTotalOperationCommandeByPharmacie(ctx, {
        idOperation: parent['_id'] || parent.id,
        idPharmacie: ctx.pharmacieId,
      })
    ).nbCommande;
  },
  totalCommandePassee: async (parent, _, ctx: Context) => {
    return (await commandesOperation(ctx, { id: parent['_id'] || parent.id })).length;
  },
  item: async (parent, _, ctx: Context) => {
    return ctx.prisma.operation({ id: parent['_id'] || parent.id }).idItem();
  },
  marche: async (parent, _, ctx: Context) => {
    return ctx.prisma.operation({ id: parent['_id'] || parent.id }).marche();
  },
  promotion: async (parent, _, ctx: Context) => {
    return ctx.prisma.operation({ id: parent['_id'] || parent.id }).promotion();
  },
  nbPartage: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partagesConnection({
        where: {
          item: { code: 'OPERACOM' },
          idItemAssocie: parent['_id'] || parent.id,
          isRemoved: false,
          type: 'PARTAGE',
        },
      })
      .aggregate()
      .count();
  },
  nbRecommandation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partagesConnection({
        where: {
          item: { code: 'OPERACOM' },
          idItemAssocie: parent['_id'] || parent.id,
          isRemoved: false,
          type: 'RECOMMANDATION',
        },
      })
      .aggregate()
      .count();
  },
  isShared: async (parent, _, ctx: Context) => {
    return ctx.prisma.$exists.partage({
      item: { code: 'OPERACOM' },
      idItemAssocie: parent['_id'] || parent.id,
      isRemoved: false,
      type: 'PARTAGE',
    });
  },
};
