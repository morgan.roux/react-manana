import { PharmacieAchatResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PharmacieAchat: PharmacieAchatResolvers.Type = {
  ...PharmacieAchatResolvers.defaultResolvers,
  canal: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieAchat({ id: parent.id }).canal();
  },
};
