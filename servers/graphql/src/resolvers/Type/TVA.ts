import { TVAResolvers } from '../../generated/graphqlgen';

export const TVA: TVAResolvers.Type = {
  ...TVAResolvers.defaultResolvers,
  type: () => 'tva',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
};
