import { ProduitTechRegResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const ProduitTechReg: ProduitTechRegResolvers.Type = {
  ...ProduitTechRegResolvers.defaultResolvers,

  acte: async (parent, _, ctx: Context) => {
    return ctx.prisma.produitTechReg({ id: parent.id }).acte();
  },
  laboTitulaire: async (parent, _, ctx: Context) => {
    return ctx.prisma.produitTechReg({ id: parent.id }).laboTitulaire();
  },
  laboExploitant: async (parent, _, ctx: Context) => {
    return ctx.prisma.produitTechReg({ id: parent.id }).laboExploitant();
  },
  laboDistirbuteur: async (parent, _, ctx: Context) => {
    return ctx.prisma.produitTechReg({ id: parent.id }).laboDistirbuteur();
  },
  tva: async (parent, _, ctx: Context) => {
    return ctx.prisma.produitTechReg({ id: parent.id }).tva();
  },
  liste: async (parent, _, ctx: Context) => {
    return ctx.prisma.produitTechReg({ id: parent.id }).liste();
  },
  libelleStockage: async (parent, _, ctx: Context) => {
    return ctx.prisma.produitTechReg({ id: parent.id }).libelleStockage();
  },
  tauxss: async (parent, _, ctx: Context) => {
    return ctx.prisma.produitTechReg({ id: parent.id }).tauxss();
  },
};
