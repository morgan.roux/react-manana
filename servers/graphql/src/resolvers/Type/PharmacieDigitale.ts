import { PharmacieDigitaleResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PharmacieDigitale: PharmacieDigitaleResolvers.Type = {
  ...PharmacieDigitaleResolvers.defaultResolvers,
  pharmaciePestataire: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieDigitale({ id: parent.id }).prestataire();
  },
  servicePharmacie: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieDigitale({ id: parent.id }).servicePharmacie();
  },
};
