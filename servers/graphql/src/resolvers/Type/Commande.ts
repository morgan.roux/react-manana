import { CommandeResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { pad } from '../../services/number';

export const Commande: CommandeResolvers.Type = {
  ...CommandeResolvers.defaultResolvers,
  type: () => 'commande',
  id: async (parent, {}, ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  codeReference: async (parent, {}, ctx: Context) => {
    const code = await ctx.prisma
      .commandeReferences({ where: { idCommande: { id: parent['_id'] || parent.id } } })
      .then(references =>
        references && references.length ? references[references.length - 1] : null,
      );
    return code && code.id ? pad(code.id, 0, 10) : '0';
  },
  pharmacie: async (parent, {}, ctx: Context) => {
    return ctx.prisma.commande({ id: parent['_id'] || parent.id }).idPharmacie();
  },
  owner: async (parent, {}, ctx: Context) => {
    return ctx.prisma.commande({ id: parent['_id'] || parent.id }).idUser();
  },
  operation: async (parent, {}, ctx: Context) => {
    return ctx.prisma.commande({ id: parent['_id'] || parent.id }).idOperation();
  },
  commandeType: async (parent, {}, ctx: Context) => {
    return ctx.prisma.commande({ id: parent['_id'] || parent.id }).idCommandeType();
  },
  commandeStatut: async (parent, {}, ctx: Context) => {
    return ctx.prisma
      .commandeStatutStatuts({
        where: { idCommande: { id: parent['_id'] || parent.id } },
        orderBy: 'dateModification_DESC',
      })
      .then(async cmdStatutStatuts => {
        return cmdStatutStatuts && cmdStatutStatuts.length
          ? ctx.prisma.commandeStatutStatut({ id: cmdStatutStatuts[0].id }).idCommandeStatut()
          : null;
      });
  },
  commandeLignes: async (parent, {}, ctx: Context) => {
    return ctx.prisma.commandeLignes({ where: { idCommande: { id: parent['_id'] || parent.id } } });
  },
  idGroupement: async (parent, {}, ctx: Context) => {
    const groupement = await ctx.prisma.commande({ id: parent['_id'] || parent.id }).groupement();
    return groupement && groupement.id;
  },
};
