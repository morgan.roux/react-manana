import { TitulaireAffectationResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TitulaireAffectation: TitulaireAffectationResolvers.Type = {
  ...TitulaireAffectationResolvers.defaultResolvers,
  type: () => 'titulaireaffectation',
  id: (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  departement: (parent, __, ctx: Context) => {
    return ctx.prisma.titulaireAffectation({ id: parent['_id'] || parent.id }).departement();
  },
  titulaire: (parent, __, ctx: Context) => {
    return ctx.prisma.titulaireAffectation({ id: parent['_id'] || parent.id }).titulaire();
  },
  titulaireFonction: (parent, __, ctx: Context) => {
    return ctx.prisma.titulaireAffectation({ id: parent['_id'] || parent.id }).titulaireFonction();
  },
  titulaireDemandeAffectation: (parent, __, ctx: Context) => {
    return ctx.prisma
      .titulaireAffectation({ id: parent['_id'] || parent.id })
      .titulaireDemandeAffectation();
  },
  userCreation: (parent, __, ctx: Context) => {
    return ctx.prisma.titulaireAffectation({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: (parent, __, ctx: Context) => {
    return ctx.prisma.titulaireAffectation({ id: parent['_id'] || parent.id }).userModification();
  },
};
