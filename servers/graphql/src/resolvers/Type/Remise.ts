import { RemiseResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Remise: RemiseResolvers.Type = {
  ...RemiseResolvers.defaultResolvers,
  commandeCanal: async (parent, _, ctx: Context) => {
    return ctx.prisma.remise({ id: parent.id }).commandeCanal();
  },
  remiseDetails: async (parent, _, ctx: Context) => {
    return ctx.prisma.remiseDetails({
      where: {
        remise: {
          id: parent.id,
        },
      },
    });
  },
};
