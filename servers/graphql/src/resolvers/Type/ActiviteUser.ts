import { ActiviteUserResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const ActiviteUser: ActiviteUserResolvers.Type = {
  ...ActiviteUserResolvers.defaultResolvers,
  type: () => 'activiteuser',
  id: async (parent, {}, ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  idActiviteType: (parent, {}, ctx: Context) => {
    return ctx.prisma.activiteUser({ id: parent.id || parent['_id'] }).idActiviteType();
  },
  idUser: (parent, {}, ctx: Context) => {
    return ctx.prisma.activiteUser({ id: parent.id || parent['_id'] }).idUser();
  },
  dateActvite: (parent, {}, ctx: Context) => {
    return ctx.prisma.activiteUser({ id: parent.id || parent['_id'] }).dateActvite();
  },
  log: (parent, {}, ctx: Context) => {
    return ctx.prisma.activiteUser({ id: parent.id || parent['_id'] }).log();
  },
  dateCreation: (parent, {}, ctx: Context) => {
    return ctx.prisma.activiteUser({ id: parent.id || parent['_id'] }).dateCreation();
  },
};
