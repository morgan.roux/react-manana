import { GroupementResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import {
  getUserContact,
  getUserPharmacie,
  userCodeTraitementsByRole,
  userRole,
} from '../../repository/user';

export const Groupement: GroupementResolvers.Type = {
  ...GroupementResolvers.defaultResolvers,
  type: () => 'groupement',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  pharmacies: async (parent, _, ctx: Context) => {
    return ctx.prisma.pharmacies({
      where: { groupement: { id: parent['_id'] || parent.id } },
    });
  },

  defaultPharmacie: async (parent, _, ctx: Context) => {
    const userPharmacie = await getUserPharmacie(ctx.userId, ctx);
    console.log('****************************USER PHARMACIE***********', userPharmacie, ctx.userId);
    if (userPharmacie) {
      return userPharmacie;
    }
    return ctx.prisma
      .pharmacies({
        where: {
          pharmacieType: 'TOUTES_PHARMACIES',
          groupement: { id: parent['_id'] || parent.id },
        },
        skip: 0,
        first: 1,
      })
      .then(pharmacies => (pharmacies.length > 0 ? pharmacies[0] : null));
  },

  groupementLogo: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .groupementLogoes({
        where: { groupement: { id: parent['_id'] || parent.id } },
      })
      .then(groupementLogoes =>
        groupementLogoes && groupementLogoes.length
          ? groupementLogoes[groupementLogoes.length - 1]
          : null,
      );
  },
};
