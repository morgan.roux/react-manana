import { PromotionResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import {
  promotionArticlesAssocies,
  promotionGroupeClientRemise,
  promotionRemise,
  promotionRemiseDetails,
} from '../../repository/promotion';

export const Promotion: PromotionResolvers.Type = {
  ...PromotionResolvers.defaultResolvers,
  type: () => 'promotion',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  commandeCanal: async (parent, _, ctx: Context) => {
    return ctx.prisma.promotion({ id: parent['_id'] || parent.id }).commandeCanal();
  },
  userCreated: async (parent, _, ctx: Context) => {
    return ctx.prisma.promotion({ id: parent['_id'] || parent.id }).userCreated();
  },
  userModified: async (parent, _, ctx: Context) => {
    return ctx.prisma.promotion({ id: parent['_id'] || parent.id }).userModified();
  },
  canalArticles: async (parent, _, ctx: Context) => {
    return promotionArticlesAssocies(ctx, { id: parent['_id'] || parent.id });
  },
  groupeClients: async (parent, _, ctx: Context) => {
    return promotionGroupeClientRemise(ctx, { id: parent['_id'] || parent.id });
  },
  remise: async (parent, { idPharmacie }, ctx: Context) => {
    return promotionRemise(ctx, { id: parent['_id'] || parent.id, idPharmacie });
  },
};
