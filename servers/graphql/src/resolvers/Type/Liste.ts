import { ListeResolvers } from '../../generated/graphqlgen';

export const Liste: ListeResolvers.Type = {
  ...ListeResolvers.defaultResolvers,
  type: () => 'liste',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
};
