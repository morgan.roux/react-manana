import { Context } from '../../types';
import { InformationLiaisonPrisChargeFichierJointResolvers } from '../../generated/graphqlgen';

export const InformationLiaisonPrisChargeFichierJoint: InformationLiaisonPrisChargeFichierJointResolvers.Type = {
  ...InformationLiaisonPrisChargeFichierJointResolvers.defaultResolvers,
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  informationLiaisonPrisCharge: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonPrisChargeFichierJoint({ id: parent['_id'] || parent.id })
      .informationLiaisonPrisCharge();
  },
  fichier: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonPrisChargeFichierJoint({ id: parent['_id'] || parent.id })
      .fichier();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonPrisChargeFichierJoint({ id: parent['_id'] || parent.id })
      .userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonPrisChargeFichierJoint({ id: parent['_id'] || parent.id })
      .userModification();
  },
};
