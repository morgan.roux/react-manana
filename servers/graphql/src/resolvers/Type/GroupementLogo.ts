import { GroupementLogoResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const GroupementLogo: GroupementLogoResolvers.Type = {
  ...GroupementLogoResolvers.defaultResolvers,
  fichier: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupementLogo({ id: parent.id }).idFichier();
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupementLogo({ id: parent.id }).groupement();
  },
};
