import { MarcheLaboratoireResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const MarcheLaboratoire: MarcheLaboratoireResolvers.Type = {
  ...MarcheLaboratoireResolvers.defaultResolvers,
  laboratoire: async (parent, _, ctx: Context) => {
    return ctx.prisma.marcheLaboratoire({ id: parent.id }).laboratoire();
  },
};
