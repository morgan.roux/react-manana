import { TitulaireDemandeAffectationResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TitulaireDemandeAffectation: TitulaireDemandeAffectationResolvers.Type = {
  ...TitulaireDemandeAffectationResolvers.defaultResolvers,
  type: () => 'titulairedemandeaffectation',
  id: (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  service: (parent, __, ctx: Context) => {
    return ctx.prisma.titulaireDemandeAffectation({ id: parent['_id'] || parent.id }).service();
  },
  titulaireFonction: (parent, __, ctx: Context) => {
    return ctx.prisma
      .titulaireDemandeAffectation({ id: parent['_id'] || parent.id })
      .titulaireFonction();
  },
  titulaireRemplacent: (parent, __, ctx: Context) => {
    return ctx.prisma
      .titulaireDemandeAffectation({ id: parent['_id'] || parent.id })
      .titulaireRemplacent();
  },
  userCreation: (parent, __, ctx: Context) => {
    return ctx.prisma
      .titulaireDemandeAffectation({ id: parent['_id'] || parent.id })
      .userCreation();
  },
  userModification: (parent, __, ctx: Context) => {
    return ctx.prisma
      .titulaireDemandeAffectation({ id: parent['_id'] || parent.id })
      .userModification();
  },
};
