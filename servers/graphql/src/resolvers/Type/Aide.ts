import { AideResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Aide: AideResolvers.Type = {
  ...AideResolvers.defaultResolvers,
  type: () => 'aide',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.aide({ id: parent['_id'] || parent.id }).groupement();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.aide({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.aide({ id: parent['_id'] || parent.id }).userModification();
  },
};
