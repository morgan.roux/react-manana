import { TraitementResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Traitement: TraitementResolvers.Type = {
  ...TraitementResolvers.defaultResolvers,
  type: () => 'traitement',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  module: async (parent, _, ctx: Context) => {
    return ctx.prisma.traitement({ id: parent['_id'] || parent.id }).module();
  },
  roles: async (parent, _, ctx: Context) => {
    const roleTraitements = await ctx.prisma.roleTraitements({
      where: { traitement: { id: parent['_id'] || parent.id } },
    });

    return roleTraitements && roleTraitements.length
      ? Promise.all(
          roleTraitements.map(roleTraitement =>
            ctx.prisma.roleTraitement({ id: roleTraitement.id }).role(),
          ),
        )
      : null;
  },
  idGroupements: async (parent, _, ctx: Context) => {
    const groupements = await ctx.prisma.groupements();
    const defaults: string[] = [];
    if (!groupements || (groupements && !groupements.length)) return [];
    return groupements.reduce(async (promiseResults, groupement) => {
      const exist = await ctx.prisma.$exists.roleTraitement({
        traitement: { id: parent['_id'] || parent.id },
        groupement: { id: groupement.id },
      });
      const results = await promiseResults;
      if (exist && !results.includes(groupement.id)) return [...results, groupement.id];
      return [...results];
    }, Promise.resolve(defaults));
  },
};
