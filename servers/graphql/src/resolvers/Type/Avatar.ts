import { AvatarResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Avatar: AvatarResolvers.Type = {
  ...AvatarResolvers.defaultResolvers,
  type: () => 'avatar',
  id: async (parent, _, __) => {
    return (parent['_id'] ? String(parent['_id']) : parent.id) as any;
  },
  fichier: async (parent, _, ctx: Context) => {
    return parent['fichier']
      ? parent['fichier']
      : ctx.prisma
          .fichiers({ where: { avatar: { id: parent.id } } })
          .then(fichiers => (fichiers && fichiers.length ? fichiers[0] : null));
  },
  userCreation: async (parent, _, ctx: Context) => {
    return parent['userCreation']
      ? parent['userCreation']
      : ctx.prisma.avatar({ id: parent['_id'] ? String(parent['_id']) : parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return parent['userModification']
      ? parent['userModification']
      : ctx.prisma
          .avatar({ id: parent['_id'] ? String(parent['_id']) : parent.id })
          .userModification();
  },
  idGroupement: async (parent, _, ctx: Context) => {
    return parent['idGroupement']
      ? parent['idGroupement']
      : ctx.prisma
          .avatar({ id: parent['_id'] ? String(parent['_id']) : parent.id })
          .groupement()
          .id();
  },
};
