import { TodoActionAttributionResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TodoActionAttribution: TodoActionAttributionResolvers.Type = {
  ...TodoActionAttributionResolvers.defaultResolvers,
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  action: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionAttribution({ id: parent['_id'] || parent.id }).action();
  },
  userSource: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionAttribution({ id: parent['_id'] || parent.id }).userSource();
  },
  userDestination: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionAttribution({ id: parent['_id'] || parent.id }).userDestination();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionAttribution({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionAttribution({ id: parent['_id'] || parent.id }).userModification();
  },
};
