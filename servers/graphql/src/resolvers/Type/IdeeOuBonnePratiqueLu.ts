import { IdeeOuBonnePratiqueLuResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const IdeeOuBonnePratiqueLu: IdeeOuBonnePratiqueLuResolvers.Type = {
  ...IdeeOuBonnePratiqueLuResolvers.defaultResolvers,
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  ideeOuBonnePratique: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ideeOuBonnePratiqueLu({ id: parent['_id'] || parent.id })
      .ideeOuBonnePratique();
  },
  user: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiqueLu({ id: parent['_id'] || parent.id }).user();
  },
};
