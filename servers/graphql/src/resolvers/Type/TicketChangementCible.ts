import { last } from 'lodash';
import { TicketChangementCibleResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TicketChangementCible: TicketChangementCibleResolvers.Type = {
  ...TicketChangementCibleResolvers.defaultResolvers,
  type: () => 'ticketchangementcible',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  ticket: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementCible({ id: parent['_id'] || parent.id }).ticket();
  },
  laboratoireCible: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementCible({ id: parent['_id'] || parent.id }).laboratoireCible();
  },
  prestataireServiceCible: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ticketChangementCible({ id: parent['_id'] || parent.id })
      .prestataireServiceCible();
  },
  pharmacieCible: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementCible({ id: parent['_id'] || parent.id }).pharmacieCible();
  },
  serviceCible: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementCible({ id: parent['_id'] || parent.id }).serviceCible();
  },
  roleCible: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementCible({ id: parent['_id'] || parent.id }).roleCible();
  },
  userCible: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementCible({ id: parent['_id'] || parent.id }).userCible();
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementCible({ id: parent['_id'] || parent.id }).groupement();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementCible({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketChangementCible({ id: parent['_id'] || parent.id }).userModification();
  },
  statut: async (parent, _, ctx: Context) => {
    const id = parent['_id'] || parent.id;
    const ticket = await ctx.prisma.ticketChangementCible({ id }).ticket();
    return ctx.prisma
      .ticketChangementStatuts({
        where: { ticketChangementCible: { id }, ticket: { id: ticket.id } },
        orderBy: 'dateCreation_ASC',
      })
      .then(async tcs => {
        if (tcs.length > 0) {
          const ts = last(tcs);
          if (ts) {
            const statut = await ctx.prisma.ticketChangementStatut({ id: ts.id }).ticketStatut();
            console.log('statut :>> ', statut);
            return statut;
          }
        }
      });
  },
};
