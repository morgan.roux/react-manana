import { ServicePartenaireResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const ServicePartenaire: ServicePartenaireResolvers.Type = {
  ...ServicePartenaireResolvers.defaultResolvers,
  type: () => 'servicepartenaire',
  id: (parent: any, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  serviceType: (parent, _, ctx: Context) => {
    return ctx.prisma.servicePartenaire({ id: parent['_id'] || parent.id }).serviceType();
  },
  produit: (parent, _, ctx: Context) => {
    return ctx.prisma.servicePartenaire({ id: parent['_id'] || parent.id }).produit();
  },
  partenaireService: (parent, _, ctx: Context) => {
    return ctx.prisma.servicePartenaire({ id: parent['_id'] || parent.id }).partenaireService();
  },
  photo: (parent, _, ctx: Context) => {
    return null;
  },
};
