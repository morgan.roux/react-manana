import { last } from 'lodash';
import { TicketResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Ticket: TicketResolvers.Type = {
  ...TicketResolvers.defaultResolvers,
  type: () => 'ticket',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  nomOrganisation: async (parent, _, ctx: Context) => {
    let nom = '';
    switch (parent.origine) {
      case 'INTERNE':
        break;
      case 'PATIENT':
        break;
      case 'GROUPE_AMIS':
        nom = await ctx.prisma.groupeAmis({ id: parent.idOrganisation }).nom();
        break;
      case 'GROUPEMENT':
        nom = await ctx.prisma.service({ id: parent.idOrganisation }).nom();
        break;
      case 'FOURNISSEUR':
        nom = await ctx.prisma.laboratoire({ id: parent.idOrganisation }).nomLabo();
        break;
      case 'PRESTATAIRE':
        nom = await ctx.prisma.partenaire({ id: parent.idOrganisation }).nom();
        break;

      default:
        return '';
    }
    return nom;
  },
  origineType: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticket({ id: parent['_id'] || parent.id }).origine();
  },
  statusTicket: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticket({ id: parent['_id'] || parent.id }).status();
  },
  motif: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticket({ id: parent['_id'] || parent.id }).motif();
  },
  smyley: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticket({ id: parent['_id'] || parent.id }).smyley();
  },
  declarant: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticket({ id: parent['_id'] || parent.id }).declarant();
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticket({ id: parent['_id'] || parent.id }).groupement();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticket({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticket({ id: parent['_id'] || parent.id }).userModification();
  },
  fichiers: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ticketFichierJoints({
        where: { ticket: { id: parent['_id'] || parent.id } },
        orderBy: 'dateCreation_ASC',
      })
      .then(async ticketFichierJoints => {
        return ticketFichierJoints && ticketFichierJoints.length
          ? await Promise.all(
              ticketFichierJoints.map(ticketFichierJoint => {
                return ctx.prisma.ticketFichierJoint({ id: ticketFichierJoint.id }).fichier();
              }),
            )
          : [];
      });
  },
  usersConcernees: async (parent, _, ctx: Context) => {
    const concernees = await ctx.prisma
      .ticket({
        id: parent['_id'] || parent.id,
      })
      .usersConcernees();
    return concernees && concernees.length
      ? Promise.all(
          concernees.map(concernee =>
            ctx.prisma.ticketUserConcernee({ id: concernee.id }).userConcernee(),
          ),
        )
      : [];
  },
};
