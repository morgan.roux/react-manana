import { ContactResolvers } from '../../generated/graphqlgen';

export const Contact: ContactResolvers.Type = {
  ...ContactResolvers.defaultResolvers,
  type: () => 'contact',
  id: (parent: any, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
};
