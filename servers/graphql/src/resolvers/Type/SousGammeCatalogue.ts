import { SousGammeCatalogueResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const SousGammeCatalogue: SousGammeCatalogueResolvers.Type = {
  ...SousGammeCatalogueResolvers.defaultResolvers,
  gammeCatalogue: async (parent, _, ctx: Context) => {
    return ctx.prisma.sousGammeCatalogue({ id: parent.id }).gammeCatalogue();
  },
};
