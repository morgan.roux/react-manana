// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { ExternalMappingResolvers } from '../../generated/graphqlgen';

export const ExternalMapping: ExternalMappingResolvers.Type = {
  ...ExternalMappingResolvers.defaultResolvers,
  user: (parent, args, ctx) => {
    return ctx.prisma.externalMapping({ id: parent.id }).idUser();
  },
  md5App: (parent, args, ctx) => {
    return ctx.prisma.externalMapping({ id: parent.id }).md5App();
  },
};
