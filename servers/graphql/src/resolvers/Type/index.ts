import { Pharmacie } from './Pharmacie';
import { Titulaire } from './Titulaire';
import { Groupement } from './Groupement';
import { Laboratoire } from './Laboratoire';
import { Personnel } from './Personnel';
import { ProduitCanal } from './ProduitCanal';
import { Searchable } from './Searchable';
import { User } from './User';
import { Partenaire } from './Partenaire';
import { Ppersonnel } from './Ppersonnel';
import { Commande } from './Commande';
import { Panier } from './Panier';
import { PanierLigne } from './PanierLigne';
import { Notification } from './Notification';
import { CommandeLigne } from './CommandeLigne';
import { Comment } from './Comment';
import { CanalGamme } from './CanalGamme';
import { Famille } from './Famille';
import { CanalSousGamme } from './CanalSousGamme';
import { Operation } from './Operation';
import { OperationArticle } from './OperationArticle';
import { OperationPharmacie } from './OperationPharmacie';
import { OperationPharmacieDetail } from './OperationPharmacieDetail';
import { GroupementLogo } from './GroupementLogo';
import { Avatar } from './Avatar';
import { ProduitPhoto } from './ProduitPhoto';
import { UserPhoto } from './UserPhoto';
import { Fichier } from './Fichier';
import { Actualite } from './Actualite';
import { Smyley } from './Smyley';
import { UserSmyley } from './UserSmyley';
import { Action } from './Action';
import { Project } from './Project';
import { CommandeCanal } from './CommandeCanal';
import { CommandeType } from './CommandeType';
import { Parameter } from './Parameter';
import { Departement } from './Departement';
import { Region } from './Region';
import { ActualiteOrigine } from './ActualiteOrigine';
import { UserTitulaire } from './UserTitulaire';
import { Service } from './Service';
import { OperationArticleCible } from './OperationArticleCible';
import { Connexion } from './Connexion';
import { ActiviteUser } from './ActiviteUser';
import { HelloIdSso } from './HelloIdSso';
import { ApplicationsGroup } from './ApplicationsGroup';
import { ApplicationsRole } from './ApplicationsRole';
import { SsoApplication } from './SsoApplication';
import { TokenFtpAuthentification } from './TokenFtpAuthentification';
import { CryptoOther } from './CryptoOther';
import { ExternalMapping } from './ExternalMapping';
import { ActiveDirectoryCredential } from './ActiveDirectoryCredential';
import { ActiveDirectoryUser } from './ActiveDirectoryUser';
import { CryptoMd5 } from './CryptoMd5';
import { Publicite } from './Publicite';
import { SuiviPublicitaire } from './SuiviPublicitaire';
import { Item } from './Item';
import { SousGammeCatalogue } from './SousGammeCatalogue';
import { GammeCatalogue } from './GammeCatalogue';
import { Acte } from './Acte';
import { TVA } from './TVA';
import { Produit } from './Produit';
import { GroupeClient } from './GroupeClient';
import { Marche } from './Marche';
import { MarcheLaboratoire } from './MarcheLaboratoire';
import { MarcheGroupeClient } from './MarcheGroupeClient';
import { Remise } from './Remise';
import { Promotion } from './Promotion';
import { PromotionGroupeClient } from './PromotionGroupeClient';
import { Role } from './Role';
import { Traitement } from './Traitement';
import { UserTraitement } from './UserTraitement';
import { RoleTraitement } from './RoleTraitement';
import { PharmacieEntreeSortie } from './PharmacieEntreeSortie';
import { PharmacieAchat } from './PharmacieAchat';
import { PharmacieConcept } from './PharmacieConcept';
import { PharmacieDigitale } from './PharmacieDigitale';
import { PharmacieInformatique } from './PharmacieInformatique';
import { PharmacieSegmentation } from './PharmacieSegmentation';
import { Logiciel } from './Logiciel';
import { PharmacieSatisfaction } from './PharmacieSatisfaction';
import { ProduitTechReg } from './ProduitTechReg';
import { Liste } from './Liste';
import { LibelleDivers } from './LibelleDivers';
import { TauxSS } from './TauxSS';
import { PersonnelAffectation } from './PersonnelAffectation';
import { PersonnelDemandeAffectation } from './PersonnelDemandeAffectation';
import { PersonnelFonction } from './PersonnelFonction';
import { TitulaireAffectation } from './TitulaireAffectation';
import { TitulaireDemandeAffectation } from './TitulaireDemandeAffectation';
import { TitulaireFonction } from './TitulaireFonction';
import { Messagerie } from './Messagerie';
import { MessagerieHisto } from './MessagerieHisto';
import { MessagerieFichierJoint } from './MessagerieFichierJoint';
import { ProduitCode } from './ProduitCode';
import { ServiceType } from './ServiceType';
import { ServicePartenaire } from './ServicePartenaire';
import { Ressource } from './Ressource';
import { PartenaireServiceSuite } from './PartenaireServiceSuite';
import { PartenaireServicePartenaire } from './PartenaireServicePartenaire';
import { Contact } from './Contact';
import { PartenaireType } from './PartenaireType';
import { PartenaireRepresentant } from './PartenaireRepresentant';
import { PartenaireRepresentantAffectation } from './PartenaireRepresentantAffectation';
import { PartenaireRepresentantDemandeAffectation } from './PartenaireRepresentantDemandeAffectation';
import { Ticket } from './Ticket';
import { TicketFichierJoint } from './TicketFichierJoint';
import { TicketChangementCible } from './TicketChangementCible';
import { TicketChangementStatut } from './TicketChangementStatut';
import { TicketMotif } from './TicketMotif';
import { TicketOrigine } from './TicketOrigine';
import { TicketStatut } from './TicketStatut';
import { Aide } from './Aide';
import { TodoActionChangementStatus } from './TodoActionChangementStatus';
import { Couleur } from './Couleur';
import { TodoProjetFavoris } from './TodoProjetFavoris';
import { TodoParticipant } from './TodoParticipant';
import { TodoEtiquette } from './TodoEtiquette';
import { TodoEtiquetteFavoris } from './TodoEtiquetteFavoris';
import { TodoActionType } from './TodoActionType';
import { TodoActionEtiquette } from './TodoActionEtiquette';
import { TodoActionAttribution } from './TodoActionAttribution';
import { CommentaireFichierJoint } from './CommentaireFichierJoint';
import { TodoSection } from './TodoSection';
import { ActionActivite } from './ActionActivite';
import { TodoEtiquetteParticipant } from './TodoEtiquetteParticipant';
import { InformationLiaison } from './InformationLiaison';
import { InformationLiaisonPrisCharge } from './InformationLiaisonPrisCharge';
import { InformationLiaisonPrisChargeFichierJoint } from './InformationLiaisonPrisChargeFichierJoint';
import { InformationLiaisonFichierJoint } from './InformationLiaisonFichierJoint';
import { InformationLiaisonUserConcernee } from './InformationLiaisonUserConcernee';
import { GroupeAmis } from './GroupeAmis';
import { GroupeAmisDetail } from './GroupeAmisDetail';
import { Partage } from './Partage';
import { PartageFonctionCible } from './PartageFonctionCible';
import { PartagePharmacieCible } from './PartagePharmacieCible';
import { PartageGroupeAmisCible } from './PartageGroupeAmisCible';
import { IdeeOuBonnePratique } from './IdeeOuBonnePratique';
import { IdeeOuBonnePratiqueClassification } from './IdeeOuBonnePratiqueClassification';
import { IdeeOuBonnePratiqueFichierJoint } from './IdeeOuBonnePratiqueFichierJoint';
import { IdeeOuBonnePratiqueLu } from './IdeeOuBonnePratiqueLu';
import { IdeeOuBonnePratiqueChangeStatus } from './IdeeOuBonnePratiqueChangeStatus';
import { Rgpd } from './Rgpd';
import { RgpdAccueil } from './RgpdAccueil';
import { RgpdAccueilPlus } from './RgpdAccueilPlus';
import { RgpdAutorisation } from './RgpdAutorisation';
import { RgpdHistorique } from './RgpdHistorique';
import { RgpdHistoriqueInfoPlus } from './RgpdHistoriqueInfoPlus';
import { RgpdPartenaire } from './RgpdPartenaire';
import { LaboratoirePartenaire } from './LaboratoirePartenaire';
import { LaboratoireRepresentant } from './laboratoireRepresentant';
import { LaboratoireRessource } from './laboratoireRessource';

export default {
  Pharmacie,
  Titulaire,
  Groupement,
  Laboratoire,
  LaboratoirePartenaire,
  Personnel,
  ProduitCanal,
  User,
  Searchable,
  Partenaire,
  Ppersonnel,
  Commande,
  Panier,
  PanierLigne,
  CommandeLigne,
  Comment,
  Notification,
  CanalGamme,
  Famille,
  Operation,
  OperationArticle,
  OperationPharmacie,
  OperationPharmacieDetail,
  CanalSousGamme,
  GroupementLogo,
  Avatar,
  ProduitPhoto,
  UserPhoto,
  Fichier,
  Actualite,
  Action,
  Project,
  CommandeCanal,
  CommandeType,
  Smyley,
  UserSmyley,
  Parameter,
  Departement,
  Region,
  ActualiteOrigine,
  UserTitulaire,
  Service,
  OperationArticleCible,
  Connexion,
  ActiviteUser,
  HelloIdSso,
  ApplicationsGroup,
  SsoApplication,
  ApplicationsRole,
  TokenFtpAuthentification,
  CryptoOther,
  ExternalMapping,
  ActiveDirectoryCredential,
  ActiveDirectoryUser,
  CryptoMd5,
  Publicite,
  SuiviPublicitaire,
  Item,
  SousGammeCatalogue,
  GammeCatalogue,
  Acte,
  TVA,
  Produit,
  GroupeClient,
  Marche,
  MarcheLaboratoire,
  Remise,
  MarcheGroupeClient,
  Promotion,
  PromotionGroupeClient,
  Role,
  Traitement,
  UserTraitement,
  RoleTraitement,
  PharmacieEntreeSortie,
  PharmacieAchat,
  PharmacieConcept,
  PharmacieDigitale,
  PharmacieInformatique,
  PharmacieSegmentation,
  Logiciel,
  PharmacieSatisfaction,
  ProduitTechReg,
  Liste,
  TauxSS,
  LibelleDivers,
  PersonnelAffectation,
  PersonnelDemandeAffectation,
  PersonnelFonction,
  TitulaireAffectation,
  TitulaireDemandeAffectation,
  TitulaireFonction,
  Messagerie,
  MessagerieHisto,
  MessagerieFichierJoint,
  ProduitCode,
  ServiceType,
  ServicePartenaire,
  Ressource,
  PartenaireServiceSuite,
  PartenaireServicePartenaire,
  Contact,
  PartenaireType,
  PartenaireRepresentant,
  PartenaireRepresentantAffectation,
  PartenaireRepresentantDemandeAffectation,
  Ticket,
  TicketFichierJoint,
  TicketChangementCible,
  TicketChangementStatut,
  TicketMotif,
  TicketOrigine,
  TicketStatut,
  Aide,
  TodoActionChangementStatus,
  Couleur,
  TodoProjetFavoris,
  TodoParticipant,
  TodoEtiquette,
  TodoEtiquetteFavoris,
  TodoActionType,
  TodoActionEtiquette,
  TodoActionAttribution,
  CommentaireFichierJoint,
  TodoSection,
  ActionActivite,
  TodoEtiquetteParticipant,
  InformationLiaison,
  InformationLiaisonPrisCharge,
  InformationLiaisonPrisChargeFichierJoint,
  GroupeAmis,
  GroupeAmisDetail,
  Partage,
  PartageFonctionCible,
  PartagePharmacieCible,
  PartageGroupeAmisCible,
  IdeeOuBonnePratique,
  IdeeOuBonnePratiqueClassification,
  IdeeOuBonnePratiqueFichierJoint,
  IdeeOuBonnePratiqueLu,
  IdeeOuBonnePratiqueChangeStatus,
  Rgpd,
  RgpdAccueil,
  RgpdAccueilPlus,
  RgpdAutorisation,
  RgpdHistorique,
  RgpdHistoriqueInfoPlus,
  RgpdPartenaire,
  InformationLiaisonFichierJoint,
  InformationLiaisonUserConcernee,
  LaboratoireRepresentant,
  LaboratoireRessource,
};
