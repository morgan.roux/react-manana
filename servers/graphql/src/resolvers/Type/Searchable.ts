import { SearchableResolvers } from '../../generated/graphqlgen';

const types = {
  produit: 'Produit',
  produitcanal: 'ProduitCanal',
  commandeligne: 'CommandeLigne',
  famille: 'Famille',
  commandetype: 'CommandeType',
  commandecanal: 'CommandeCanal',
  canalgamme: 'CanalGamme',
  actualiteorigine: 'ActualiteOrigine',
  activiteuser: 'ActiviteUser',
  suivipublicitaire: 'SuiviPublicitaire',
  tva: 'TVA',
  acte: 'Acte',
  gammecatalogue: 'GammeCatalogue',
  groupeclient: 'GroupeClient',
  personnelaffectation: 'PersonnelAffectation',
  titulaireaffectation: 'TitulaireAffectation',
  partenairerepresentant: 'PartenaireRepresentant',
  partenairerepresentantaffectation: 'PartenaireRepresentantAffectation',
  ticketchangementcible: 'TicketChangementCible',
  ticketstatut: 'TicketStatut',
  todoetiquette: 'TodoEtiquette',
  todosection: 'TodoSection',
  actionactivite: 'ActionActivite',
  informationliaison: 'InformationLiaison',
  informationliaisonuserconcernee: 'InformationLiaisonUserConcernee',
  groupeamis: 'GroupeAmis',
  groupeamisdetail: 'GroupeAmisDetail',
  ideeoubonnepratique: 'IdeeOuBonnePratique',
  laboratoirepartenaire: 'LaboratoirePartenaire',
  laboratoirerepresentant: 'LaboratoireRepresentant',
  laboratoireressource: 'LaboratoireRessource',
};

export const Searchable: SearchableResolvers.Type = {
  __resolveType(parent: any, __): Promise<any> {
    return types[parent.type]
      ? types[parent.type]
      : (`${parent.type[0].toUpperCase()}${parent.type.substring(1)}` as any);
  },
};
