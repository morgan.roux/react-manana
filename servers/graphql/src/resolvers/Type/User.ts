import { uniqBy } from 'lodash';
import { PARTENAIRE_SERVICE } from '../../constants/roles';
import { UserResolvers } from '../../generated/graphqlgen';
import {
  getUserContact,
  getUserPharmacie,
  userCodeTraitementsByRole,
  userRole,
} from '../../repository/user';
import { Context } from '../../types';

export const User: UserResolvers.Type = {
  ...UserResolvers.defaultResolvers,
  type: () => 'user',
  id: (parent, _, __) => parent.id || parent['_id'],
  password: (parent, _, __) => parent.passwordHash,
  groupement: async (parent, _, context: Context) => {
    return context.prisma.user({ id: parent.id || parent['_id'] }).groupement();
  },
  role: async (parent, _, ctx: Context) => {
    return userRole(ctx, { id: parent.id || parent['_id'] });
  },
  contact: async (parent, _, ctx: Context) => {
    return getUserContact(parent.id || parent['_id'], ctx);
  },
  idGroupement: (parent, _, context: Context) => {
    return context.prisma
      .user({ id: parent.id || parent['_id'] })
      .groupement()
      .then(groupement => groupement.id);
  },
  notifications: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .notifications({ where: { to: { id: parent.id || parent['_id'] } } })
      .then(notifications => notifications);
  },
  pharmacie: async (parent, _, ctx: Context) => {
    return getUserPharmacie(parent['_id'] || parent.id, ctx);
  },
  userPhoto: async (parent, _, ctx: Context) => {
    return parent['userPhoto']
      ? parent['userPhoto']
      : ctx.prisma
          .userPhotos({ where: { idUser: { id: parent.id || parent['_id'] } } })
          .then(userPhotos =>
            userPhotos && userPhotos.length ? userPhotos[userPhotos.length - 1] : null,
          );
  },
  userLaboratoire: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .laboratoires({ where: { user: { id: parent.id || parent['_id'] } } })
      .then(users => (users && users.length ? users[0] : null));
  },
  userPartenaire: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partenaires({ where: { user: { id: parent.id || parent['_id'] } } })
      .then(users => (users && users.length ? users[0] : null));
  },
  userPersonnel: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .personnels({ where: { user: { id: parent.id || parent['_id'] } } })
      .then(users => (users && users.length ? users[0] : null));
  },
  userPpersonnel: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ppersonnels({ where: { user: { id: parent.id || parent['_id'] } } })
      .then(users => (users && users.length ? users[0] : null));
  },
  userTitulaire: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .userTitulaires({ where: { idUser: { id: parent.id || parent['_id'] } } })
      .then(users => (users && users.length ? users[0] : null));
  },
  externalMapping: async (parent, args, ctx) => {
    const result = await ctx.prisma.externalMappings({
      where: { idUser: { id: parent.id || parent['_id'] } },
    });
    return result[0];
  },
  codeTraitements: async (parent, _, ctx: Context) => {
    const role = await userRole(ctx, { id: parent.id || parent['_id'] });
    return role.code === PARTENAIRE_SERVICE
      ? userCodeTraitementsByRole(ctx, {
          idUser: parent.id || parent['_id'],
          codeRole: role.code,
          idPharmacie: ctx.pharmacieId,
        })
      : role
      ? userCodeTraitementsByRole(ctx, {
          idUser: parent.id || parent['_id'],
          codeRole: role.code,
          idPharmacie: null,
        })
      : [];
  },
  nbReclamation: (parent, _, ctx: Context) => {
    return ctx.prisma
      .ticketChangementCiblesConnection({
        where: {
          userCreation: { id: parent['_id'] || parent.id },
          ticket: { typeTicket: 'RECLAMATION' },
        },
      })
      .aggregate()
      .count();
  },
  nbAppel: (parent, _, ctx: Context) => {
    return ctx.prisma
      .ticketChangementCiblesConnection({
        where: {
          userCreation: { id: parent['_id'] || parent.id },
          ticket: { typeTicket: 'APPEL' },
        },
      })
      .aggregate()
      .count();
  },
  nbTicket: (parent, _, ctx: Context) => {
    return ctx.prisma
      .ticketChangementCiblesConnection({
        where: {
          userCreation: { id: parent['_id'] || parent.id },
        },
      })
      .aggregate()
      .count();
  },
  pharmaciePartenaires: async (parent, _, ctx: Context) => {
    const role = await userRole(ctx, { id: parent.id || parent['_id'] });
    if (role.code !== PARTENAIRE_SERVICE) {
      return [];
    } else {
      return ctx.prisma
        .userTraitements({ where: { user: { id: parent.id || parent['_id'] } } })
        .then(userTraitements =>
          Promise.all(
            userTraitements.map(userTraitement =>
              ctx.prisma.userTraitement({ id: userTraitement.id }).pharmacie(),
            ),
          ),
        )
        .then(pharmacies => uniqBy(pharmacies, 'id'));
    }
  },
};
