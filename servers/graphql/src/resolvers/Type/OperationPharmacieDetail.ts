import { OperationPharmacieDetailResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const OperationPharmacieDetail: OperationPharmacieDetailResolvers.Type = {
  ...OperationPharmacieDetailResolvers.defaultResolvers,
  pharmacie: async (parent, _, ctx: Context) => {
    return ctx.prisma.operationPharmacieDetail({ id: parent.id }).idPharmacie();
  },
};
