import { Context } from '../../types';
import { InformationLiaisonUserConcerneeResolvers } from '../../generated/graphqlgen';
import { last } from 'lodash';

export const InformationLiaisonUserConcernee: InformationLiaisonUserConcerneeResolvers.Type = {
  ...InformationLiaisonUserConcerneeResolvers.defaultResolvers,
  type: () => 'informationliaisonuserconcernee',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  informationLiaison: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonUserConcernee({ id: parent['_id'] || parent.id })
      .informationLiaison();
  },
  userConcernee: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonUserConcernee({ id: parent['_id'] || parent.id })
      .userConcernee();
  },
  filtre: async (parent, _, ctx: Context) => {
    const user = await ctx.prisma
      .informationLiaisonUserConcernee({ id: parent['_id'] || parent.id })
      .userConcernee();
    const statut = await ctx.prisma
      .informationLiaisonUserConcernee({ id: parent['_id'] || parent.id })
      .statut();
    return user && statut ? `${user.id}-${statut}` : '';
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonUserConcernee({ id: parent['_id'] || parent.id })
      .userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonUserConcernee({ id: parent['_id'] || parent.id })
      .userModification();
  },
  prisEnCharge: async (parent, _, ctx: Context) => {
    const informationLiaison = await ctx.prisma
      .informationLiaisonUserConcernee({ id: parent['_id'] || parent.id })
      .informationLiaison();
    const userConcernee = await await ctx.prisma
      .informationLiaisonUserConcernee({ id: parent['_id'] || parent.id })
      .userConcernee();

    if (informationLiaison && userConcernee) {
      return ctx.prisma
        .informationLiaisonPrisCharges({
          where: {
            isRemoved: false,
            informationLiaison: { id: informationLiaison.id },
            userCreation: { id: userConcernee.id },
          },
        })
        .then(results => {
          const res = last(results);
          return res;
        });
    }
  },
  coupleStatutUserConcernee: async (parent, _, ctx: Context) => {
    const statut = await ctx.prisma
      .informationLiaisonUserConcernee({ id: parent['_id'] || parent.id })
      .statut();
    const idUserConcernee = await ctx.prisma
      .informationLiaisonUserConcernee({ id: parent['_id'] || parent.id })
      .userConcernee()
      .id();
    return `${idUserConcernee}-${statut}`;
  },
};
