import { PersonnelResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { getUserPersonnel } from '../../repository/personnel';

export const Personnel: PersonnelResolvers.Type = {
  ...PersonnelResolvers.defaultResolvers,
  type: () => 'personnel',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  idGroupement: async (parent, {}, ctx: Context) => {
    return (
      parent['idGroupement'] ||
      ctx.prisma
        .personnel({ id: parent['_id'] || parent.id })
        .groupement()
        .then(groupement => (groupement && groupement.id ? groupement.id : null))
    );
  },
  fullName: async (parent, _, __) => {
    return parent['fullName'] || `${parent.prenom} ${parent.nom}`;
  },
  user: async (parent, _, ctx: Context) => {
    return parent['user'] || getUserPersonnel(ctx, parent['_id'] || parent.id);
  },
  role: async (parent, {}, ctx: Context) => {
    const user = parent['role'] ? null : await getUserPersonnel(ctx, parent['_id'] || parent.id);
    if (user) {
      const userRoles = await ctx.prisma.userRoles({ where: { idUser: { id: user.id } } });

      return userRoles && userRoles.length
        ? ctx.prisma.userRole({ id: userRoles[0].id }).idRole()
        : null;
    }
    return parent['role'];
  },
  actualites: async (parent, _, ctx: Context) => {
    const service = await ctx.prisma.personnel({ id: parent['_id'] || parent.id }).service();
    return service
      ? ctx.prisma.actualites({ where: { serviceCibles_some: { code: service.code } } })
      : null;
  },
  service: async (parent, _, ctx: Context) => {
    return ctx.prisma.personnel({ id: parent['_id'] || parent.id }).service();
  },
  respHierarch: async (parent, _, ctx: Context) => {
    return ctx.prisma.personnel({ id: parent['_id'] || parent.id }).respHierarch();
  },
  contact: async (parent, {}, ctx: Context) => {
    return ctx.prisma.personnel({ id: parent['_id'] || parent.id }).contact();
  },
  userCreation: (parent, _, ctx: Context) => {
    return ctx.prisma.personnel({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: (parent, _, ctx: Context) => {
    return ctx.prisma.personnel({ id: parent['_id'] || parent.id }).userModification();
  },
};
