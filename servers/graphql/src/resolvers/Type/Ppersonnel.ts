import { PpersonnelResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { userRole } from '../../repository/user';
import { getUserPpesonnel } from '../../repository/ppersonnel';

export const Ppersonnel: PpersonnelResolvers.Type = {
  ...PpersonnelResolvers.defaultResolvers,
  type: () => 'ppersonnel',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  idGroupement: async (parent, _, ctx: Context) => {
    return (
      parent['idGroupement'] ||
      ctx.prisma
        .ppersonnel({ id: parent['_id'] || parent.id })
        .groupement()
        .then(groupement => groupement.id)
    );
  },
  idPharmacie: async (parent, _, ctx: Context) => {
    return (
      parent['idPharmacie'] ||
      ctx.prisma
        .ppersonnel({ id: parent['_id'] || parent.id })
        .idPharmacie()
        .then(pharmacie => (pharmacie ? pharmacie.id : null))
    );
  },
  fullName: async (parent, _, __) => {
    return parent['fullName'] || `${parent.prenom} ${parent.nom}`;
  },
  user: async (parent, _, ctx: Context) => {
    return parent['user'] || getUserPpesonnel(ctx, parent['_id'] || parent.id);
  },
  role: async (parent, _, ctx: Context) => {
    const user = parent['role'] ? null : await getUserPpesonnel(ctx, parent['_id'] || parent.id);
    return parent['role'] ? parent['role'] : user ? userRole(ctx, { id: user.id }) : null;
  },
  pharmacie: async (parent, _, ctx: Context) => {
    return ctx.prisma.ppersonnel({ id: parent['_id'] || parent.id }).idPharmacie();
  },
  contact: async (parent, {}, ctx: Context) => {
    return ctx.prisma.ppersonnel({ id: parent['_id'] || parent.id }).contact();
  },
};
