import { PharmacieInformatiqueResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PharmacieInformatique: PharmacieInformatiqueResolvers.Type = {
  ...PharmacieInformatiqueResolvers.defaultResolvers,
  logiciel: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieInformatique({ id: parent.id }).logiciel();
  },
  automate1: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieInformatique({ id: parent.id }).automate1();
  },
  automate2: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieInformatique({ id: parent.id }).automate2();
  },
  automate3: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieInformatique({ id: parent.id }).automate3();
  },
};
