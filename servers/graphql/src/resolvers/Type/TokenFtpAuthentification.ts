// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { TokenFtpAuthentificationResolvers } from '../../generated/graphqlgen';

export const TokenFtpAuthentification: TokenFtpAuthentificationResolvers.Type = {
  ...TokenFtpAuthentificationResolvers.defaultResolvers,

  requestUrl: (parent, args, ctx) => {
    return ctx.prisma.tokenFtpAuthentification({ id: parent.id }).reqUserUrl();
  },
  ftpid: (parent, args, ctx) => {
    return ctx.prisma.tokenFtpAuthentification({ id: parent.id }).ftpid();
  },
};
