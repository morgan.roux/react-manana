import { GroupeAmisResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const GroupeAmis: GroupeAmisResolvers.Type = {
  ...GroupeAmisResolvers.defaultResolvers,
  type: () => 'groupeamis',
  id: async (parent, _: any, __: any) => {
    return (parent['_id'] || parent.id) as any;
  },
  typeGroupeAmis: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupeAmis({ id: parent['_id'] || parent.id }).type();
  },
  pharmacie: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupeAmis({ id: parent['_id'] || parent.id }).pharmacie();
  },
  departement: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupeAmis({ id: parent['_id'] || parent.id }).departement();
  },
  region: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupeAmis({ id: parent['_id'] || parent.id }).region();
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupeAmis({ id: parent['_id'] || parent.id }).groupement();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupeAmis({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.groupeAmis({ id: parent['_id'] || parent.id }).userModification();
  },
  nbMember: async (parent, _, ctx: Context) => {
    // TODO : check if its logic is OK
    return ctx.prisma
      .groupeAmisDetailsConnection({
        where: {
          groupeAmis: {
            id: parent['_id'] || parent.id,
            isRemoved: false,
          },
        },
      })
      .aggregate()
      .count();
  },
  nbPartage: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partagesConnection({
        where: {
          item: { code: 'GROUPE_AMIS' },
          idItemAssocie: parent['_id'] || parent.id,
          type: 'PARTAGE',
          isRemoved: false,
        },
      })
      .aggregate()
      .count();
  },
  nbRecommandation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partagesConnection({
        where: {
          item: { code: 'GROUPE_AMIS' },
          idItemAssocie: parent['_id'] || parent.id,
          type: 'RECOMMANDATION',
          isRemoved: false,
        },
      })
      .aggregate()
      .count();
  },
  nbPharmacie: async (parent, _, ctx: Context) => {
    // TODO : check if its logic is OK
    return ctx.prisma
      .groupeAmisDetailsConnection({
        where: {
          groupeAmis: { id: parent['_id'] || parent.id, isRemoved: false },
        },
      })
      .aggregate()
      .count();
  },
  pharmacieMembres: async (parent, _, ctx: Context) => {
    // TODO : check if its logic is OK
    const groupeDetails = await ctx.prisma.groupeAmisDetails({
      where: {
        groupeAmis: { id: parent['_id'] || parent.id, isRemoved: false },
      },
    });

    return Promise.all(
      groupeDetails.map(item => {
        return ctx.prisma.groupeAmisDetail({ id: item.id }).pharmacie();
      }),
    );
  },
};
