import { RgpdHistoriqueInfoPlusResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const RgpdHistoriqueInfoPlus: RgpdHistoriqueInfoPlusResolvers.Type = {
  ...RgpdHistoriqueInfoPlusResolvers.defaultResolvers,
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.rgpdHistoriqueInfoPlus({ id: parent['_id'] || parent.id }).groupement();
  },
  historique: async (parent, _, ctx: Context) => {
    return ctx.prisma.rgpdHistoriqueInfoPlus({ id: parent['_id'] || parent.id }).historique();
  },
};
