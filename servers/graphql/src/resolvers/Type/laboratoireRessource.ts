import { LaboratoireRessourceResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const LaboratoireRessource: LaboratoireRessourceResolvers.Type = {
  ...LaboratoireRessourceResolvers.defaultResolvers,
  type: () => 'laboratoireressource',
  id: (parent: any, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  groupement: (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoireRessource({ id: parent['_id'] || parent.id }).groupement();
  },
  item: (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoireRessource({ id: parent['_id'] || parent.id }).item();
  },
  fichier: (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoireRessource({ id: parent['_id'] || parent.id }).fichier();
  },
  laboratoire: (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoireRessource({ id: parent['_id'] || parent.id }).laboratoire();
  },
};
