import { IdeeOuBonnePratiqueClassificationResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const IdeeOuBonnePratiqueClassification: IdeeOuBonnePratiqueClassificationResolvers.Type = {
  ...IdeeOuBonnePratiqueClassificationResolvers.defaultResolvers,
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ideeOuBonnePratiqueClassification({ id: parent['_id'] || parent.id })
      .userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ideeOuBonnePratiqueClassification({ id: parent['_id'] || parent.id })
      .userModification();
  },
  parent: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ideeOuBonnePratiqueClassification({ id: parent['_id'] || parent.id })
      .parent();
  },
  childs: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiqueClassifications({
      where: { parent: { id: parent['_id'] || parent.id }, isRemoved: false },
    });
  },
};
