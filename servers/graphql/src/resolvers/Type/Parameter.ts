import { ParameterResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { getUserPharmacie } from './../../repository/user'


const getCurrentPharmacieId = async (ctx: Context) => {
  if (ctx.pharmacieId) {
    return ctx.pharmacieId
  }
  const pharmacie = await getUserPharmacie(ctx.userId, ctx)
  return pharmacie?.id
}

export const Parameter: ParameterResolvers.Type = {
  ...ParameterResolvers.defaultResolvers,
  parameterType: (parent, _, ctx: Context) => {
    return ctx.prisma.parameter({ id: parent.id }).idParameterType();
  },
  value: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .parameterValues({
        where: {
          idParameter: { id: parent.id },
          idSource: parent.category === 'PHARMACIE' ? await getCurrentPharmacieId(ctx) : ctx.groupementId,
        },
      })
      .then(values => (values && values.length ? values[values.length - 1] : null));
  },
  parameterGroupe: (parent, _, ctx: Context) => {
    return ctx.prisma.parameter({ id: parent.id }).idParameterGroupe();
  },
  options: async (parent, _, ctx: Context) => {
    const options = await ctx.prisma.parameter({ id: parent.id }).options();
    return options ? JSON.parse(options) : null;
  },
};
