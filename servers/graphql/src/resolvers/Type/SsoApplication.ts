// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { SsoApplicationResolvers } from '../../generated/graphqlgen';

export const SsoApplication: SsoApplicationResolvers.Type = {
  ...SsoApplicationResolvers.defaultResolvers,

  mappings: (parent, args, ctx) => {
    return ctx.prisma.ssoApplication({id : parent.id}).mappings();
  },
  samlLoginDetail: async (parent, args, ctx) => {
    const parentDetail = await ctx.prisma.ssoApplication({id : parent.id});
    if (parentDetail && parentDetail.ssoType === "SAML") {
      return ctx.prisma.samlLogin({id : parentDetail.ssoTypeid});
    }
    return null;
  },
  cryptoAesDetail: async (parent, args, ctx) => {
    const parentDetail = await ctx.prisma.ssoApplication({id : parent.id});
    if (parentDetail && parentDetail.ssoType === "CRYPTAGE_AES256") {
      return ctx.prisma.cryptoAes256({id : parentDetail.ssoTypeid});
    }
    return null;
  },
  cryptoOtherDetail: async (parent, args, ctx) => {
    const parentDetail = await ctx.prisma.ssoApplication({id : parent.id});
    if (parentDetail && parentDetail.ssoType === "OTHER_CRYPTO") {
      return ctx.prisma.cryptoOther({id : parentDetail.ssoTypeid});
    }
    return null;
  },
  cryptoMd5Detail: async (parent, args, ctx) => {
    const parentDetail = await ctx.prisma.ssoApplication({id : parent.id});
    if (parentDetail && parentDetail.ssoType === "CRYPTAGE_MD5") {
      return ctx.prisma.cryptoMd5({id : parentDetail.ssoTypeid});
    }
    return null;
  },
  tokenAuthentificationDetail: async (parent, args, ctx) => {
    const parentDetail = await ctx.prisma.ssoApplication({id : parent.id});
    if (parentDetail && parentDetail.ssoType === "TOKEN_AUTHENTIFICATION") {
      return ctx.prisma.tokenAuthentification({id : parentDetail.ssoTypeid});
    }
    return null;
  },
  tokenFtpAuthentificationDetail: async (parent, args, ctx) => {
    const parentDetail = await ctx.prisma.ssoApplication({id : parent.id});
    if (parentDetail && parentDetail.ssoType === "TOKEN_FTP_AUTHENTIFICATION") {
      return ctx.prisma.tokenFtpAuthentification({id : parentDetail.ssoTypeid});
    }
    return null;
  },
  icon: (parent, args, ctx) => {
    return ctx.prisma.ssoApplication({id : parent.id}).icon();
  },
};
