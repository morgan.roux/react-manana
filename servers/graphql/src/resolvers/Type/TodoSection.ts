import { TodoSectionResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TodoSection: TodoSectionResolvers.Type = {
  ...TodoSectionResolvers.defaultResolvers,
  type: () => 'todosection',
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  project: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoSection({ id: parent['_id'] || parent.id }).project();
  },
  user: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoSection({ id: parent['_id'] || parent.id }).user();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoSection({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoSection({ id: parent['_id'] || parent.id }).userModification();
  },
  nbAction: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .actionsConnection({
        where: { section: { id: parent['_id'] || parent.id }, isRemoved: false },
      })
      .aggregate()
      .count();
  },
};
