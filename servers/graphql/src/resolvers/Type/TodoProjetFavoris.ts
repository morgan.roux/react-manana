import { TodoProjetFavorisResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TodoProjetFavoris: TodoProjetFavorisResolvers.Type = {
  ...TodoProjetFavorisResolvers.defaultResolvers,
  type: () => 'todoprojetfavoris',
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  project: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoProjetFavoris({ id: parent['_id'] || parent.id }).project();
  },
  user: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoProjetFavoris({ id: parent['_id'] || parent.id }).user();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoProjetFavoris({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoProjetFavoris({ id: parent['_id'] || parent.id }).userModification();
  },
};
