import { PharmacieSatisfactionResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PharmacieSatisfaction: PharmacieSatisfactionResolvers.Type = {
  ...PharmacieSatisfactionResolvers.defaultResolvers,
  smyley: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieSatisfaction({ id: parent.id }).smyley();
  },
  user: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieSatisfaction({ id: parent.id }).user();
  },
};
