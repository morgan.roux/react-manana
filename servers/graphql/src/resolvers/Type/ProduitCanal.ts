import { ProduitCanalResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { groupementComments, itemComments } from '../../repository/comment';
import { itemActions } from '../../repository/action';
import {
  articleSamePanachees,
  getArticlePanacheesInPanier,
  pharmacieRemisePaliers,
  pharmacieRemisePanachees,
} from '../../repository/remise';
import { myPanier } from '../../repository/panier';
import {
  isPorduitCanalActive,
  canalArticleMarches,
  canalArticlePromotions,
  canalProduit,
  articleCanalRemiseDetails,
} from '../../repository/produitCanal';
import { articleOperations } from '../../repository/operation';
import { groupementSmyleys } from '../../repository/smyley';

export const ProduitCanal: ProduitCanalResolvers.Type = {
  ...ProduitCanalResolvers.defaultResolvers,
  type: () => 'produitcanal',
  id: async (parent, _, __) => {
    return parent['_id'] || parent.id;
  },
  produit: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .produitCanal({
        id: parent['_id'] || parent.id,
      })
      .produit();
  },
  isActive: async (parent, _, ctx: Context) => {
    return isPorduitCanalActive(ctx, { id: parent['_id'] || parent.id });
  },
  sousGammeCommercial: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .produitCanal({
        id: parent['_id'] || parent.id,
      })
      .sousGamme();
  },
  remises: async (parent, { idPharmacie, idRemiseOperation }, ctx: Context) => {
    if (!idPharmacie) return null;
    return idRemiseOperation
      ? ctx.prisma.remiseDetails({ where: { remise: { id: idRemiseOperation } } })
      : articleCanalRemiseDetails(ctx, parent['_id'] || parent.id, { idPharmacie });
  },
  articleSamePanachees: async (parent, { idPharmacie }, ctx: Context) => {
    const canal = await canalProduit(ctx, { id: parent['_id'] || parent.id });
    return idPharmacie && canal
      ? articleSamePanachees(ctx, {
          idCanalArticle: parent['_id'] || parent.id,
          codeCanal: canal.code,
          idPharmacie,
        })
      : null;
  },
  inMyPanier: async (parent, { idPharmacie }, ctx: Context) => {
    return ctx.prisma
      .paniers({
        where: {
          idUser: { id: ctx.userId },
          idPharmacie: { id: idPharmacie },
          validate: false,
        },
      })
      .then(paniers =>
        paniers && paniers.length
          ? ctx.prisma.panierLignes({
              where: {
                idPanier: { id_in: paniers.map(panier => panier.id) },
                produitCanal: {
                  id: parent['_id'] || parent.id,
                },
              },
            })
          : [],
      )
      .then(lignes => {
        return lignes && lignes.length > 0;
      });
  },
  inOtherPanier: async (parent, { idPharmacie }, ctx: Context) => {
    return idPharmacie
      ? ctx.prisma
          .paniers({
            where: {
              idPharmacie: { id: idPharmacie },
              idUser: { id_not: ctx.userId },
              validate: false,
            },
          })
          .then(async paniers =>
            paniers && paniers.length
              ? ctx.prisma
                  .panierLignes({
                    where: {
                      idPanier: { id_in: paniers.map(panier => panier.id) },
                      produitCanal: {
                        id: parent['_id'] || parent.id,
                      },
                    },
                  })
                  .then(async lignes => {
                    return lignes && lignes.length ? lignes[lignes.length - 1] : null;
                  })
              : null,
          )
      : null;
  },
  qteTotalRemisePanachees: async (parent, { idPharmacie }, ctx: Context) => {
    if (!idPharmacie) return 0;

    const canalCommande = await ctx.prisma
      .produitCanal({
        id: parent['_id'] || parent.id,
      })
      .canal();

    const panier = await myPanier(ctx, {
      idPharmacie,
      codeCanal: canalCommande && canalCommande.code,
    });
    const articles = await articleSamePanachees(ctx, {
      idCanalArticle: parent['_id'] || parent.id,
      codeCanal: canalCommande.code,
      idPharmacie,
    }).then(articles => articles.map(article => article.id));

    if (!panier || !articles || (articles && !articles.length)) return 0;

    const panierArticles = await getArticlePanacheesInPanier(ctx, articles, {
      idPanier: panier.id,
    });

    const qteLigneSamePanache =
      panierArticles && panierArticles.length
        ? panierArticles.map(panierLigne => panierLigne.quantite).reduce((sum, qte) => sum + qte)
        : 0;

    return qteLigneSamePanache;
  },
  groupementComments: async (parent, _, ctx: Context) => {
    return groupementComments(
      {
        codeItem: 'PROD',
        idItemAssocie: parent['_id'] || parent.id,
      },
      ctx,
    );
  },
  comments: async (parent, { take, skip }, ctx: Context) => {
    return itemComments(
      {
        take,
        skip,
        codeItem: 'PROD',
        idItemAssocie: parent['_id'] || parent.id,
      },
      ctx,
    ).then(comments => comments as any);
  },
  ligneInPanier: async (parent, { idPharmacie }, ctx: Context) => {
    const lignes = await ctx.prisma
      .paniers({
        where: {
          idUser: { id: ctx.userId },
          idPharmacie: { id: idPharmacie },
          validate: false,
        },
      })
      .then(paniers =>
        paniers && paniers.length
          ? ctx.prisma.panierLignes({
              where: {
                idPanier: { id_in: paniers.map(panier => panier.id) },
                produitCanal: {
                  id: parent['_id'] || parent.id,
                },
              },
            })
          : [],
      );
    if (!lignes || (lignes && !lignes.length)) return null;
    return lignes[lignes.length - 1];
  },
  actions: async (parent, { take, skip }, ctx: Context) => {
    return itemActions(
      {
        take,
        skip,
        codeItem: 'PROD',
        idItemAssocie: parent['_id'] || parent.id,
      },
      ctx,
    ).then(action => action as any);
  },
  userSmyleys: async (parent, _, ctx: Context) => {
    return {
      total: await ctx.prisma
        .userSmyleysConnection({
          where: {
            idSource: parent['_id'] || parent.id,
            groupement: { id: ctx.groupementId },
            isRemoved: false,
          },
        })
        .aggregate()
        .count(),
      data: await ctx.prisma.userSmyleys({
        where: {
          idSource: parent['_id'] || parent.id,
          groupement: { id: ctx.groupementId },
          isRemoved: false,
        },
      }),
    } as any;
  },
  groupementSmyleys: async (parent, _, ctx: Context) => {
    return groupementSmyleys({ idSource: parent['_id'] || parent.id }, ctx);
  },
  commandeCanal: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .produitCanal({
        id: parent['_id'] || parent.id,
      })
      .canal();
  },
  pharmacieRemisePaliers: async (parent, _, ctx: Context) => {
    const canal = await canalProduit(ctx, { id: parent['_id'] || parent.id });
    return canal ? pharmacieRemisePaliers(parent['_id'] || parent.id, canal.code, ctx) : [];
  },
  pharmacieRemisePanachees: async (parent, _, ctx: Context) => {
    const canal = await canalProduit(ctx, { id: parent['_id'] || parent.id });
    return canal
      ? pharmacieRemisePanachees(
          { idCanalArticle: parent['_id'] || parent.id, codeCanal: canal.code },
          ctx,
        )
      : [];
  },
  operations: async (parent, _, ctx: Context) => {
    return articleOperations({ idCanalArticle: parent['_id'] || parent.id }, ctx);
  },
  sousGammeCatalogue: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .produitCanal({
        id: parent['_id'] || parent.id,
      })
      .sousGammeCatalogue();
  },
  marches: async (parent, _, ctx: Context) => {
    return canalArticleMarches(ctx, { idCanalArticle: parent['_id'] || parent.id });
  },
  promotions: async (parent, _, ctx: Context) => {
    return canalArticlePromotions(ctx, { idCanalArticle: parent['_id'] || parent.id });
  },
  isShared: async (parent, _, ctx: Context) => {
    return ctx.prisma.$exists.partage({
      item: { code: 'PROD' },
      idItemAssocie: parent['_id'] || parent.id,
      isRemoved: false,
      type: 'PARTAGE',
    });
  },
  partage: async (parent, _, ctx: Context) => {
    const partages = await ctx.prisma.partages({
      where: {
        item: { code: 'PROD' },
        idItemAssocie: parent['_id'] || parent.id,
        isRemoved: false,
      },
    });

    return partages[0];
  },
};
