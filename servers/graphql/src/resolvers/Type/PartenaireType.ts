import { PartenaireTypeResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PartenaireType: PartenaireTypeResolvers.Type = {
  ...PartenaireTypeResolvers.defaultResolvers,
  type: () => 'partenairetype',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.partenaireType({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.partenaireType({ id: parent['_id'] || parent.id }).userModification();
  },
};
