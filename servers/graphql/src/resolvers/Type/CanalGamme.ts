import { CanalGammeResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { sousGammeCommercials } from '../../repository/pflGammeCommercial';
import { getCountProduits } from '../../repository/produitCanal';

export const CanalGamme: CanalGammeResolvers.Type = {
  ...CanalGammeResolvers.defaultResolvers,
  type: () => 'canalgamme',
  id: async (parent, {}, ctx: Context) => {
    return parent['_id'] || parent.id;
  },
  countCanalArticles: async (parent, { codeCanal }, ctx: Context) => {
    const result = await getCountProduits(
      [
        {
          term: {
            'sousGammeCommercial.gammeCommercial.id': parent['_id'] || parent.id,
          },
        },
        {
          term: {
            'commandeCanal.code': codeCanal ? codeCanal : 'PFL',
          },
        },
      ],
      ctx,
    );

    return result && result.total ? result.total : 0;
  },
  sousGamme: async (parent, {}, ctx: Context) => {
    return sousGammeCommercials(ctx, parent['_id'] || parent.id);
  },
  canal: async (parent, {}, ctx: Context) => {
    return ctx.prisma.canalGamme({ id: parent['_id'] || parent.id }).canal();
  },
};
