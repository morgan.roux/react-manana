import { SuiviPublicitaireResolvers } from '../../generated/graphqlgen';

import { Context } from '../../types';

export const SuiviPublicitaire: SuiviPublicitaireResolvers.Type = {
  ...SuiviPublicitaireResolvers.defaultResolvers,
  type: () => 'suivipublicitaire',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  publicite: async (parent, _, ctx: Context) => {
    return ctx.prisma.suiviPublicitaire({ id: parent['_id'] || parent.id }).idPublicite();
  },
  user: async (parent, _, ctx: Context) => {
    return ctx.prisma.suiviPublicitaire({ id: parent['_id'] || parent.id }).idUser();
  },
  transformCommande: async (parent, _, ctx: Context) => {
    const publicite = await ctx.prisma
      .suiviPublicitaire({ id: parent['_id'] || parent.id })
      .idPublicite();
    return publicite && publicite.id
      ? ctx.prisma.$exists.commande({ idPublicite: { id: publicite.id } })
      : false;
  },
  commande: async (parent, _, ctx: Context) => {
    const publicite = await ctx.prisma
      .suiviPublicitaire({ id: parent['_id'] || parent.id })
      .idPublicite();
    return publicite && publicite.id
      ? ctx.prisma
          .commandes({ where: { idPublicite: { id: publicite.id } } })
          .then(async cmds =>
            cmds && cmds.length ? ctx.prisma.commande({ id: cmds[0].id }) : null,
          )
      : null;
  },
  nbClick: async (parent, _, ctx: Context) => {
    return ctx.prisma.suiviPublicitaire({ id: parent['_id'] || parent.id }).nbClick();
  },
  nbView: async (parent, _, ctx: Context) => {
    return ctx.prisma.suiviPublicitaire({ id: parent['_id'] || parent.id }).nbView();
  },
  nbAccesOp: async (parent, _, ctx: Context) => {
    return ctx.prisma.suiviPublicitaire({ id: parent['_id'] || parent.id }).nbAccessOp();
  },
  nbCommande: async (parent, _, ctx: Context) => {
    return ctx.prisma.suiviPublicitaire({ id: parent['_id'] || parent.id }).nbCommande();
  },
};
