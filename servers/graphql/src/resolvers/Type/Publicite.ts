import { PubliciteResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Publicite: PubliciteResolvers.Type = {
  ...PubliciteResolvers.defaultResolvers,
  type: () => 'publicite',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  idItemSource: async (parent, _, ctx: Context) => {
    return ctx.prisma.publicite({ id: parent['_id'] || parent.id }).idItemAssocie();
  },
  item: async (parent, _, ctx: Context) => {
    return ctx.prisma.publicite({ id: parent['_id'] || parent.id }).idItem();
  },
  image: async (parent, _, ctx: Context) => {
    return ctx.prisma.publicite({ id: parent['_id'] || parent.id }).idFichier();
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.publicite({ id: parent['_id'] || parent.id }).groupement();
  },
};
