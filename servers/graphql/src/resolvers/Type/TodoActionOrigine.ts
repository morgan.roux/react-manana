import { TodoActionOrigineResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TodoActionOrigine: TodoActionOrigineResolvers.Type = {
  ...TodoActionOrigineResolvers.defaultResolvers,
  type: () => 'todoactionorigine',
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionOrigine({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.todoActionOrigine({ id: parent['_id'] || parent.id }).userModification();
  },
};
