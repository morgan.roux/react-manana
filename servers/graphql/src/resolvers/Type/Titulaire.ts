import { TitulaireResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import moment from 'moment';
import { isTitulairePresident, affectationPresidentRegions } from '../../repository/titulaire';

export const Titulaire: TitulaireResolvers.Type = {
  ...TitulaireResolvers.defaultResolvers,
  type: () => 'titulaire',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  estPresidentRegion: async (parent, _, ctx: Context) => {
    return isTitulairePresident({ id: parent['_id'] || parent.id }, ctx);
  },
  idGroupement: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .titulaire({
        id: parent['_id'] || parent.id,
      })
      .groupement()
      .then(groupement => groupement.id);
  },
  fullName: (parent, _, __) => {
    return `${parent.prenom} ${parent.nom}`;
  },
  users: async (parent, _, ctx: Context) => {
    const usersTitulaires = await ctx.prisma.userTitulaires({
      where: {
        idTitulaire: {
          id: parent['_id'] || parent.id,
        },
      },
    });

    if (usersTitulaires && usersTitulaires.length > 0) {
      return Promise.all(usersTitulaires.map(i => ctx.prisma.userTitulaire({ id: i.id }).idUser()));
    } else {
      return [];
    }
  },
  pharmacieUser: async (parent, {}, ctx: Context) => {
    return ctx.prisma
      .userTitulaires({
        where: {
          idTitulaire: {
            id: parent['_id'] || parent.id,
          },
        },
      })
      .then(async userTitulaires =>
        userTitulaires && userTitulaires.length
          ? ctx.prisma.userTitulaire({ id: userTitulaires[0].id }).idPharmacie()
          : null,
      );
  },
  pharmacies: async (parent, _, ctx: Context) => {
    return parent['pharmacies']
      ? parent['pharmacies']
      : ctx.prisma
          .titulaire({
            id: parent['_id'] || parent.id,
          })
          .pharmacies();
  },
  dateDebut: async (parent, _, ctx: Context) => {
    const affectations = await affectationPresidentRegions({ id: parent['_id'] || parent.id }, ctx);

    const date =
      affectations && affectations.length
        ? Math.max.apply(
            null,
            affectations.map(affectation => new Date(affectation.dateDebut)),
          )
        : null;

    return date ? moment(date).format() : null;
  },
  dateFin: async (parent, _, ctx: Context) => {
    const affectations = await affectationPresidentRegions({ id: parent['_id'] || parent.id }, ctx);
    const date =
      affectations && affectations.length
        ? Math.max.apply(
            null,
            affectations.map(affectation => new Date(affectation.dateFin)),
          )
        : null;

    return date ? moment(date).format() : null;
  },
  contact: async (parent, _, ctx: Context) => {
    return parent['contact']
      ? parent['contact']
      : ctx.prisma
          .titulaire({
            id: parent['_id'] || parent.id,
          })
          .contact();
  },
  userCreation: (parent, _, ctx: Context) => {
    return ctx.prisma.titulaire({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: (parent, _, ctx: Context) => {
    return ctx.prisma.titulaire({ id: parent['_id'] || parent.id }).userModification();
  },
};
