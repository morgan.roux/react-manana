import { CommandeTypeResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import moment from 'moment';
export const CommandeType: CommandeTypeResolvers.Type = {
  ...CommandeTypeResolvers.defaultResolvers,
  type: () => 'commandeType',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  countOperationsCommercials: async (parent, _, ctx: Context) => {
    const now = moment()
      .format()
      .toString();
    const operationGroupementType: number =
      ctx.role === 'SUPADM' || ctx.role === 'GRPADM'
        ? await ctx.prisma
            .operationPharmaciesConnection({
              where: {
                AND: [
                  {
                    idOperation: {
                      groupement: { id: ctx.groupementId },
                    },
                  },
                  {
                    idOperation: {
                      idCommandeType: { id: parent['_id'] || parent.id },
                    },
                  },
                  {
                    globalite: true,
                  },
                  {
                    idOperation: {
                      isRemoved: false,
                    },
                  },
                ],
              },
            })
            .aggregate()
            .count()
        : await ctx.prisma
            .operationPharmaciesConnection({
              where: {
                AND: [
                  {
                    idOperation: {
                      groupement: { id: ctx.groupementId },
                    },
                  },
                  {
                    idOperation: {
                      idCommandeType: { id: parent['_id'] || parent.id },
                    },
                  },
                  {
                    globalite: true,
                  },
                  {
                    idOperation: {
                      isRemoved: false,
                    },
                  },
                  {
                    idOperation: {
                      dateDebut_lte: now,
                    },
                  },
                  {
                    idOperation: {
                      dateFin_gte: now,
                    },
                  },
                ],
              },
            })
            .aggregate()
            .count();

    const operationPharmacieType: number =
      ctx.role === 'SUPADM' || ctx.role === 'GRPADM'
        ? await ctx.prisma
            .operationPharmacieDetailsConnection({
              where: {
                AND: [
                  {
                    idPharmacie: { id: ctx.pharmacieId },
                  },
                  {
                    idOperation: { idCommandeType: { id: parent['_id'] || parent.id } },
                  },
                  {
                    idOperationPharmacie: { globalite: false },
                  },
                  {
                    idOperation: {
                      isRemoved: false,
                    },
                  },
                ],
              },
            })
            .aggregate()
            .count()
        : await ctx.prisma
            .operationPharmacieDetailsConnection({
              where: {
                AND: [
                  {
                    idPharmacie: { id: ctx.pharmacieId },
                  },
                  {
                    idOperation: { idCommandeType: { id: parent['_id'] || parent.id } },
                  },
                  {
                    idOperationPharmacie: { globalite: false },
                  },
                  {
                    idOperation: {
                      isRemoved: false,
                    },
                  },
                  {
                    idOperation: {
                      dateDebut_lte: now,
                    },
                  },
                  {
                    idOperation: {
                      dateFin_gte: now,
                    },
                  },
                ],
              },
            })
            .aggregate()
            .count();

    return operationGroupementType + operationPharmacieType;
  },
};
