import { ServiceTypeResolvers } from '../../generated/graphqlgen';

export const ServiceType: ServiceTypeResolvers.Type = {
  ...ServiceTypeResolvers.defaultResolvers,
  type: () => 'servicetype',
  id: (parent: any, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
};
