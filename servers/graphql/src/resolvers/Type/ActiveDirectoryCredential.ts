// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

import { ActiveDirectoryCredentialResolvers } from '../../generated/graphqlgen';

export const ActiveDirectoryCredential: ActiveDirectoryCredentialResolvers.Type = {
  ...ActiveDirectoryCredentialResolvers.defaultResolvers,

  groupement: (parent, args, ctx) => {
    return ctx.prisma.activeDirectoryCredential({ id: parent.id }).groupement();
  },
};
