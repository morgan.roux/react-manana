import { MessagerieResolvers } from '../../generated/graphqlgen';
import { Context, MessagerieType } from '../../types';

export const Messagerie: MessagerieResolvers.Type = {
  ...MessagerieResolvers.defaultResolvers,
  messagerieTheme: (parent, _, ctx: Context) => {
    return ctx.prisma.messagerie({ id: parent['_id'] || parent.id }).messagerieTheme();
  },
  messagerieSource: (parent, _, ctx: Context) => {
    return ctx.prisma.messagerie({ id: parent['_id'] || parent.id }).messagerieSource();
  },
  userEmetteur: (parent, _, ctx: Context) => {
    return ctx.prisma.messagerie({ id: parent['_id'] || parent.id }).userEmetteur();
  },
  recepteurs: (parent, _, ctx: Context) => {
    return ctx.prisma
      .messagerie({
        id: parent['_id'] || parent.id,
      })
      .messagerieHistoes({
        where: {
          typeMessagerie: 'R',
        },
      });
  },
  attachments: (parent, _, ctx: Context) => {
    return ctx.prisma.messagerieFichierJoints({
      where: { messagerie: { id: parent['_id'] || parent.id } },
    });
  },
  messagerieDepart: async (parent, _, ctx: Context) => {
    const messagesReponses = await ctx.prisma.messagerieReponseHistoes({
      where: {
        messagerieReponse: {
          id: parent['_id'] || parent.id,
        },
      },
    });

    return messagesReponses && messagesReponses.length
      ? ctx.prisma.messagerieReponseHisto({ id: messagesReponses[0].id }).messagerieDepart()
      : null;
  },
  isRemoved: async (parent, _, ctx: Context) => {
    const isEmetteur = await ctx.prisma.$exists.messagerie({
      id: parent['_id'] || parent.id,
      userEmetteur: {
        id: ctx.userId,
      },
    });

    return isEmetteur
      ? ctx.prisma.$exists.messagerieHisto({
          messagerie: {
            id: parent['_id'] || parent.id,
          },
          isRemoved: true,
          typeMessagerie: 'E',
          userRecepteur: {
            id: ctx.userId,
          },
        })
      : ctx.prisma.$exists.messagerieHisto({
          messagerie: {
            id: parent['_id'] || parent.id,
          },
          isRemoved: true,
          typeMessagerie: 'R',
          userRecepteur: {
            id: ctx.userId,
          },
        });
  },
  lu: async (parent, _, ctx: Context) => {
    const isEmetteur = await ctx.prisma.$exists.messagerie({
      id: parent['_id'] || parent.id,
      userEmetteur: {
        id: ctx.userId,
      },
    });

    return isEmetteur
      ? ctx.prisma.$exists.messagerieHisto({
          messagerie: {
            id: parent['_id'] || parent.id,
          },
          lu: true,
          typeMessagerie: 'E',
          userRecepteur: {
            id: ctx.userId,
          },
        })
      : ctx.prisma.$exists.messagerieHisto({
          messagerie: {
            id: parent['_id'] || parent.id,
          },
          lu: true,
          typeMessagerie: 'R',
          userRecepteur: {
            id: ctx.userId,
          },
        });
  },
  typeMessagerie: async (parent, _, ctx: Context) => {
    return (await ctx.prisma.$exists.messagerie({
      id: parent['_id'] || parent.id,
      userEmetteur: {
        id: ctx.userId,
      },
    }))
      ? 'E'
      : 'R';
  },
  typeFilter: async (parent, _, ctx: Context) => {
    const emetteur = await ctx.prisma
      .messagerie({
        id: parent['_id'] || parent.id,
      })
      .userEmetteur();

    if (!emetteur || (emetteur && !emetteur.id)) return '';

    const isTitulaire = await ctx.prisma.$exists.userTitulaire({
      idUser: { id: emetteur.id },
      idPharmacie: { id: ctx.pharmacieId },
    });
    const isPpersonnel = await ctx.prisma.$exists.ppersonnel({
      user: { id: emetteur.id },
      idPharmacie: { id: ctx.pharmacieId },
    });
    const isOthersTitulaire = await ctx.prisma.$exists.userTitulaire({
      idUser: { id: emetteur.id, groupement: { id: ctx.groupementId } },
      idPharmacie: { id_not: ctx.pharmacieId },
    });
    const isOthersPpersonnel = await ctx.prisma.$exists.ppersonnel({
      user: { id: emetteur.id, groupement: { id: ctx.groupementId } },
      idPharmacie: { id_not: ctx.pharmacieId },
    });
    const isPersonnel = await ctx.prisma.$exists.personnel({
      user: { id: emetteur.id },
      groupement: { id: ctx.groupementId },
    });
    const isLabo = await ctx.prisma.$exists.laboratoire({
      user: { id: emetteur.id },
    });

    if (isTitulaire || isPpersonnel) {
      return 'MY_PHARMACIE';
    } else if (isOthersTitulaire || isOthersPpersonnel) {
      return 'OTHER_PHARMACIE';
    } else if (isPersonnel) {
      return 'MY_GROUPEMENT';
    } else if (isLabo) {
      return 'LABORATOIRE';
    } else {
      const departement = await ctx.prisma.pharmacie({ id: ctx.pharmacieId }).departement();
      const region =
        departement && departement.id
          ? await ctx.prisma.departement({ id: departement.id }).region()
          : null;

      /* REGION */
      if (region && region.id) {
        const existTitulaire = await ctx.prisma.$exists.userTitulaire({
          idUser: { id: emetteur.id },
          idPharmacie: {
            departement: {
              region: {
                id: region.id,
              },
            },
          },
        });
        const existPperso = await ctx.prisma.$exists.ppersonnel({
          user: { id: emetteur.id },
          idPharmacie: {
            departement: {
              region: {
                id: region.id,
              },
            },
          },
        });
        if (existTitulaire || existPperso) {
          return 'MY_REGION';
        }
      }
    }

    return '';
  },
};
