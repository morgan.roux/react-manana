import { MessagerieFichierJointResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const MessagerieFichierJoint: MessagerieFichierJointResolvers.Type = {
  ...MessagerieFichierJointResolvers.defaultResolvers,
  fichier: (parent, _, ctx: Context) => {
    return ctx.prisma.messagerieFichierJoint({ id: parent['_id'] || parent.id }).fichier();
  },
};
