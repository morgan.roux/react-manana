import { PartagePharmacieCibleResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PartagePharmacieCible: PartagePharmacieCibleResolvers.Type = {
  ...PartagePharmacieCibleResolvers.defaultResolvers,
  id: async (parent, _: any, __: any) => {
    return (parent['_id'] || parent.id) as any;
  },
  partage: async (parent, _, ctx: Context) => {
    return ctx.prisma.partagePharmacieCible({ id: parent['_id'] || parent.id }).partage();
  },
  pharmacie: async (parent, _, ctx: Context) => {
    return ctx.prisma.partagePharmacieCible({ id: parent['_id'] || parent.id }).pharmacie();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.partagePharmacieCible({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.partagePharmacieCible({ id: parent['_id'] || parent.id }).userModification();
  },
};
