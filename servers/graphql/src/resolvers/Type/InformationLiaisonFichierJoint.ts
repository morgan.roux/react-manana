import { Context } from '../../types';
import { InformationLiaisonFichierJointResolvers } from '../../generated/graphqlgen';

export const InformationLiaisonFichierJoint: InformationLiaisonFichierJointResolvers.Type = {
  ...InformationLiaisonFichierJointResolvers.defaultResolvers,
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  informationLiaison: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonFichierJoint({ id: parent['_id'] || parent.id })
      .informationLiaison();
  },
  fichier: async (parent, _, ctx: Context) => {
    return ctx.prisma.informationLiaisonFichierJoint({ id: parent['_id'] || parent.id }).fichier();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonFichierJoint({ id: parent['_id'] || parent.id })
      .userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .informationLiaisonFichierJoint({ id: parent['_id'] || parent.id })
      .userModification();
  },
};
