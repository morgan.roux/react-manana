import { PersonnelAffectationResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PersonnelAffectation: PersonnelAffectationResolvers.Type = {
  ...PersonnelAffectationResolvers.defaultResolvers,
  type: () => 'personnelaffectation',
  id: (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  departement: (parent, __, ctx: Context) => {
    return ctx.prisma.personnelAffectation({ id: parent['_id'] || parent.id }).departement();
  },
  personnel: (parent, __, ctx: Context) => {
    return ctx.prisma.personnelAffectation({ id: parent['_id'] || parent.id }).personnel();
  },
  personnelFonction: (parent, __, ctx: Context) => {
    return ctx.prisma.personnelAffectation({ id: parent['_id'] || parent.id }).personnelFonction();
  },
  personnelDemandeAffectation: (parent, __, ctx: Context) => {
    return ctx.prisma
      .personnelAffectation({ id: parent['_id'] || parent.id })
      .personnelDemandeAffectation();
  },
  userCreation: (parent, __, ctx: Context) => {
    return ctx.prisma.personnelAffectation({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: (parent, __, ctx: Context) => {
    return ctx.prisma.personnelAffectation({ id: parent['_id'] || parent.id }).userModification();
  },
};
