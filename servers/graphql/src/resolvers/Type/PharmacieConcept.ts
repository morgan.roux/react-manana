import { PharmacieConceptResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PharmacieConcept: PharmacieConceptResolvers.Type = {
  ...PharmacieConceptResolvers.defaultResolvers,
  concept: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieConcept({ id: parent.id }).concept();
  },
  enseigniste: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieConcept({ id: parent.id }).enseigniste();
  },
  fournisseurMobilier: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieConcept({ id: parent.id }).fournisseurMobilier();
  },
  facade: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieConcept({ id: parent.id }).facade();
  },
  leaflet: async (parent, {}, ctx: Context) => {
    return ctx.prisma.pharmacieConcept({ id: parent.id }).leaflet();
  },
};
