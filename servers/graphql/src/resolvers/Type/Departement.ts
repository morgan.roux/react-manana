import { DepartementResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Departement: DepartementResolvers.Type = {
  ...DepartementResolvers.defaultResolvers,
  region: async (parent, _, ctx: Context) => {
    return ctx.prisma.departement({ id: parent.id }).region();
  },
  userCreation: (parent, _, ctx: Context) => {
    return ctx.prisma.departement({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: (parent, _, ctx: Context) => {
    return ctx.prisma.departement({ id: parent['_id'] || parent.id }).userModification();
  },
};
