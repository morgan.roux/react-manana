import { CommentResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const Comment: CommentResolvers.Type = {
  ...CommentResolvers.defaultResolvers,
  type: () => 'comment',
  id: (parent, {}, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  user: (parent, {}, ctx: Context) => {
    return ctx.prisma.comment({ id: parent['_id'] || parent.id }).user();
  },
  comments: (_parent, {}, _ctx: Context) => {
    // TODO
    // return getComments(skip, first, parent.id, ctx);
    return null;
  },
  smyleys: (_parent, {}, _ctx: Context) => {
    // TODO
    return null;
  },
  item: (parent, {}, ctx: Context) => {
    return ctx.prisma.comment({ id: parent['_id'] || parent.id }).item();
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.comment({ id: parent['_id'] || parent.id }).groupement();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.comment({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.comment({ id: parent['_id'] || parent.id }).userModification();
  },
  fichiers: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .commentaireFichierJoints({
        where: { commentaire: { id: parent['_id'] || parent.id }, isRemoved: false },
      })
      .then(async cfjs => {
        if (cfjs && cfjs.length > 0) {
          return Promise.all(
            cfjs.map(cfj => {
              return ctx.prisma.commentaireFichierJoint({ id: cfj.id }).fichier();
            }),
          );
        } else {
          return [];
        }
      });
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.comment({ id: parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.comment({ id: parent['_id'] || parent.id }).userModification();
  },
  fichiers: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .commentaireFichierJoints({
        where: { commentaire: { id: parent['_id'] || parent.id }, isRemoved: false },
      })
      .then(async cfjs => {
        if (cfjs && cfjs.length > 0) {
          return Promise.all(
            cfjs.map(cfj => {
              return ctx.prisma.commentaireFichierJoint({ id: cfj.id }).fichier();
            }),
          );
        } else {
          return [];
        }
      });
  },
};
