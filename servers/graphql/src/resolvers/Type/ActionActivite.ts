import { ActionActiviteResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const ActionActivite: ActionActiviteResolvers.Type = {
  ...ActionActiviteResolvers.defaultResolvers,
  type: () => 'actionactivite',
  id: async (parent, _: any, __: any) => {
    return (parent['_id'] || parent.id) as any;
  },
  action: async (parent, _, ctx: Context) => {
    return ctx.prisma.actionActivite({ id: parent['_id'] || parent.id }).action();
  },
  userSource: async (parent, _, ctx: Context) => {
    return ctx.prisma.actionActivite({ id: parent['_id'] || parent.id }).userSource();
  },
  userDestination: async (parent, _, ctx: Context) => {
    return ctx.prisma.actionActivite({ id: parent['_id'] || parent.id }).userDestination();
  },
};
