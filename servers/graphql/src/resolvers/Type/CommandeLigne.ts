import { CommandeLigneResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const CommandeLigne: CommandeLigneResolvers.Type = {
  ...CommandeLigneResolvers.defaultResolvers,
  type: () => 'commandeligne',
  id: async (parent, {}, ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  pharmacie: (parent, {}, ctx: Context) => {
    return ctx.prisma.commandeLigne({ id: parent['_id'] || parent.id }).idPharmacie();
  },
  produitCanal: (parent, {}, ctx: Context) => {
    return ctx.prisma.commandeLigne({ id: parent['_id'] || parent.id }).produitCanal();
  },
  status: (parent, {}, ctx: Context) => {
    return ctx.prisma.commandeLigne({ id: parent['_id'] || parent.id }).idStatutLigne();
  },
};
