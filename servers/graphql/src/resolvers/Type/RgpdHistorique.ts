import { RgpdHistoriqueResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const RgpdHistorique: RgpdHistoriqueResolvers.Type = {
  ...RgpdHistoriqueResolvers.defaultResolvers,
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.rgpdHistorique({ id: parent['_id'] || parent.id }).groupement();
  },
  user: async (parent, _, ctx: Context) => {
    return ctx.prisma.rgpdHistorique({ id: parent['_id'] || parent.id }).user();
  },
  historiquesInfosPluses: async (parent, _, ctx: Context) => {
    return ctx.prisma.rgpdHistorique({ id: parent['_id'] || parent.id }).historiquesInfosPluses();
  },
};
