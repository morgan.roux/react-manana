import { PanierResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { getRemisePanacheeDetails } from '../../repository/remise';

export const Panier: PanierResolvers.Type = {
  ...PanierResolvers.defaultResolvers,
  pharmacie: async (parent, {}, ctx: Context) => {
    return ctx.prisma.panier({ id: parent.id }).idPharmacie();
  },
  owner: async (parent, {}, ctx: Context) => {
    return ctx.prisma.panier({ id: parent.id }).idUser();
  },
  operation: async (parent, {}, ctx: Context) => {
    return ctx.prisma.panier({ id: parent.id }).idOperation();
  },
  panierLignes: async (parent, {}, ctx: Context) => {
    return ctx.prisma.panierLignes({ where: { idPanier: { id: parent.id }, validate: false } });
  },
};
