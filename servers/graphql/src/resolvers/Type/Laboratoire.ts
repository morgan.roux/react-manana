import { LaboratoireResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';
import { getComments } from '../../services/comment';
import { getCountProduits } from '../../repository/produitCanal';
import { itemActions } from '../../repository/action';

export const Laboratoire: LaboratoireResolvers.Type = {
  ...LaboratoireResolvers.defaultResolvers,
  type: () => 'laboratoire',
  id: async (parent, {}, ctx: Context) => {
    return parent['_id'] || parent.id;
  },
  laboSuite: async (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoire({ id: parent['_id'] || parent.id }).laboSuite();
  },
  user: async (parent, {}, ctx: Context) => {
    return ctx.prisma.laboratoire({ id: parent['_id'] || parent.id }).user();
  },
  comments: async (parent, { first, skip }, ctx: Context) => {
    return getComments(first, skip, parent.id, ctx);
  },
  countCanalArticles: async (parent, { codeCanal }, ctx: Context) => {
    const result = await getCountProduits(
      [
        {
          term: {
            'produit.produitTechReg.laboExploitant.id': parent['_id'] || parent.id,
          },
        },
        {
          term: {
            'commandeCanal.code': codeCanal ? codeCanal : 'PFL',
          },
        },
      ],
      ctx,
    );

    return result && result.total ? result.total : 0;
  },
  actualites: async (parent, _, ctx: Context) => {
    return ctx.prisma.actualites({
      where: {
        OR: [
          {
            laboratoire: {
              id: parent['_id'] || parent.id,
            },
          },
          {
            laboratoireCibles_some: {
              id: parent['_id'] || parent.id,
            },
          },
        ],
      },
    });
  },
  actions: async (parent, { take, skip }, ctx: Context) => {
    return itemActions(
      {
        take,
        skip,
        codeItem: 'LABO',
        idItemAssocie: parent['_id'] || parent.id,
      },
      ctx,
    ).then(action => action as any);
  },
  laboratoirePartenaire: async (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoire({ id: parent['_id'] || parent.id }).laboratoirePartenaire();
  },
  laboratoireRepresentants: async (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoire({ id: parent['_id'] || parent.id }).laboratoireRepresentants();
  },
  laboratoireRessources: async (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoire({ id: parent['_id'] || parent.id }).laboratoireRessources();
  },
  photo: async (parent, _, ctx: Context) => {
    return ctx.prisma.laboratoire({ id: parent['_id'] || parent.id }).photo();
  },
};
