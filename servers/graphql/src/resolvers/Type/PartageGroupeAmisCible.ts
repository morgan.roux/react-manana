import { PartageGroupeAmisCibleResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PartageGroupeAmisCible: PartageGroupeAmisCibleResolvers.Type = {
  ...PartageGroupeAmisCibleResolvers.defaultResolvers,
  id: async (parent, _: any, __: any) => {
    return (parent['_id'] || parent.id) as any;
  },
  partage: async (parent, _, ctx: Context) => {
    return ctx.prisma.partageGroupeAmisCible({ id: parent['_id'] || parent.id }).partage();
  },
  groupeAmis: async (parent, _, ctx: Context) => {
    return ctx.prisma.partageGroupeAmisCible({ id: parent['_id'] || parent.id }).groupeAmis();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.partageGroupeAmisCible({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.partageGroupeAmisCible({ id: parent['_id'] || parent.id }).userModification();
  },
};
