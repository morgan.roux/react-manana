import { CommentaireFichierJointResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const CommentaireFichierJoint: CommentaireFichierJointResolvers.Type = {
  ...CommentaireFichierJointResolvers.defaultResolvers,
  id: async (parent, _: any, _ctx: Context) => {
    return (parent['_id'] || parent.id) as any;
  },
  commentaire: async (parent, _, ctx: Context) => {
    return ctx.prisma.commentaireFichierJoint({ id: parent['_id'] || parent.id }).commentaire();
  },
  fichier: async (parent, _, ctx: Context) => {
    return ctx.prisma.commentaireFichierJoint({ id: parent['_id'] || parent.id }).fichier();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.commentaireFichierJoint({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .commentaireFichierJoint({ id: parent['_id'] || parent.id })
      .userModification();
  },
};
