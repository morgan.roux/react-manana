import { TitulaireFonctionResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TitulaireFonction: TitulaireFonctionResolvers.Type = {
  ...TitulaireFonctionResolvers.defaultResolvers,
  type: () => 'titulairefonction',
  id: (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  userCreation: (parent, __, ctx: Context) => {
    return ctx.prisma.titulaireAffectation({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: (parent, __, ctx: Context) => {
    return ctx.prisma.titulaireAffectation({ id: parent['_id'] || parent.id }).userModification();
  },
};
