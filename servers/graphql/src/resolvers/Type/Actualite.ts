import { ActualiteResolvers } from '../../generated/graphqlgen';
import { itemActions } from '../../repository/action';
import {
  operationByIDActualite,
  pharmacieCibleFromPresidents,
  typePresidentCible,
} from '../../repository/actualite';
import { usersPharmacie } from '../../repository/user';
import { WatchTime } from '../../services/time';
import { Context } from '../../types';

const watchTime = new WatchTime();

export const Actualite: ActualiteResolvers.Type = {
  ...ActualiteResolvers.defaultResolvers,
  type: () => 'actualite',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  item: async (parent, _, ctx: Context) => {
    return ctx.prisma.actualite({ id: parent['_id'] || parent.id }).idItem();
  },
  origine: async (parent, _, ctx: Context) => {
    return ctx.prisma.actualite({ id: parent['_id'] || parent.id }).idActualiteOrigine();
  },
  laboratoire: async (parent, _, ctx: Context) => {
    return ctx.prisma.actualite({ id: parent['_id'] || parent.id }).laboratoire();
  },
  actualiteCible: async (parent, _, ctx: Context) => {
    const actualite = await ctx.prisma.actualite({ id: parent['_id'] || parent.id });
    return {
      id: `${actualite.id}-${actualite.dateCreation}`,
      globalite: actualite.globalite,
      fichierCible: await ctx.prisma.actualite({ id: parent['_id'] || parent.id }).fichierCible(),
      dateCreation: actualite.dateCreation,
      dateModification: actualite.dateModification,
    };
  },
  laboratoiresCible: async (parent, _, ctx: Context) => {
    return ctx.prisma.actualite({ id: parent['_id'] || parent.id }).laboratoireCibles();
  },
  partenairesCible: async (parent, _, ctx: Context) => {
    return ctx.prisma.actualite({ id: parent['_id'] || parent.id }).partenaireCibles();
  },
  pharmaciesCible: async (parent, _, ctx: Context) => {
    return ctx.prisma.actualite({ id: parent['_id'] || parent.id }).pharmacieCibles();
  },
  pharmaciesRolesCible: async (parent, _, ctx: Context) => {
    return ctx.prisma.actualite({ id: parent['_id'] || parent.id }).rolePharmacieCibles();
  },
  servicesCible: async (parent, _, ctx: Context) => {
    return ctx.prisma.actualite({ id: parent['_id'] || parent.id }).serviceCibles();
  },
  typePresidentCible: async (parent, _, ctx: Context) => {
    return typePresidentCible(ctx, { id: parent['_id'] || parent.id });
  },
  presidentsCibles: async (parent, _, ctx: Context) => {
    return ctx.prisma.actualite({ id: parent['_id'] || parent.id }).presidentCibles();
  },
  pharmaciesPresidentCible: async (parent, _, ctx: Context) => {
    return pharmacieCibleFromPresidents(ctx, { ids: [parent['_id'] || parent.id] });
  },
  actions: async (parent, { take, skip }, ctx: Context) => {
    watchTime.start();
    return itemActions(
      {
        take,
        skip,
        codeItem: 'ACTUALITE',
        idItemAssocie: parent['_id'] ? parent['_id'] : parent.id,
      },
      ctx,
    ).then(action => {
      watchTime.stop();
      watchTime.printElapsed(`[Actualite] resolve actions duration : `);
      return action as any;
    });
  },
  action: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .actions({
        where: { idItemAssocie: parent['_id'] || parent.id },
      })
      .then(results => {
        if (results && results.length > 0) {
          return results[0];
        } else {
          return null;
        }
      });
  },
  fichierPresentations: (parent, _, ctx: Context) => {
    return ctx.prisma
      .actualite({ id: parent.id || parent['_id'] })
      .fichierPresentations({ orderBy: 'dateModification_ASC' });
  },
  comments: async (parent, { takeComments, skipComments }, ctx: Context) => {
    return {
      total: await ctx.prisma
        .commentsConnection({
          where: {
            idItemAssocie: parent['_id'] ? parent['_id'] : parent.id,
            groupement: { id: ctx.groupementId },
            isRemoved: false,
          },
        })
        .aggregate()
        .count(),
      data: await ctx.prisma.comments({
        where: {
          idItemAssocie: parent['_id'] ? parent['_id'] : parent.id,
          groupement: { id: ctx.groupementId },
          isRemoved: false,
        },
        first: takeComments,
        skip: skipComments,
        orderBy: 'dateCreation_ASC',
      }),
    } as any;
  },
  userSmyleys: async (parent, _, ctx: Context) => {
    return {
      total: await ctx.prisma
        .userSmyleysConnection({
          where: {
            idSource: parent['_id'] ? parent['_id'] : parent.id,
            groupement: { id: ctx.groupementId },
            isRemoved: false,
          },
        })
        .aggregate()
        .count(),
      data: await ctx.prisma.userSmyleys({
        where: {
          idSource: parent['_id'] ? parent['_id'] : parent.id,
          groupement: { id: ctx.groupementId },
          isRemoved: false,
        },
      }),
    } as any;
  },
  idGroupement: async (parent, _, ctx: Context) => {
    watchTime.start();
    return ctx.prisma
      .actualite({ id: parent['_id'] || parent.id })
      .groupement()
      .then(groupement => {
        watchTime.stop();
        watchTime.printElapsed(`[Actualite] resolve idGroupement duration : `);
        return groupement ? groupement.id : null;
      });
  },
  seen: async (parent, { idPharmacie, userId }, ctx: Context) => {
    watchTime.start();
    const users = idPharmacie ? await usersPharmacie(ctx, { idPharmacie }) : [];
    const usersActualite =
      users && users.length
        ? await ctx.prisma
            .actualite({ id: parent['_id'] || parent.id })
            .user({ where: { id_in: users.map(user => user.id) } })
        : userId
        ? await ctx.prisma
            .actualite({ id: parent['_id'] || parent.id })
            .user({ where: { id: userId } })
        : [];

    watchTime.stop();
    watchTime.printElapsed(`[Actualite] resolve seen duration : `);
    return usersActualite && usersActualite.length ? true : false;
  },
  nbSeenByPharmacie: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .actualitePharmacieSeensConnection({
        where: { idActualite: { id: parent['_id'] || parent.id } },
      })
      .aggregate()
      .count();
  },
  operation: async (parent, _, ctx: Context) => {
    return operationByIDActualite(ctx, parent['_id'] || parent.id);
  },
  nbPartage: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partagesConnection({
        where: {
          item: { code: 'ACTUALITE' },
          idItemAssocie: parent['_id'] || parent.id,
          isRemoved: false,
          type: 'PARTAGE',
        },
      })
      .aggregate()
      .count();
  },
  nbRecommandation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partagesConnection({
        where: {
          item: { code: 'ACTUALITE' },
          idItemAssocie: parent['_id'] || parent.id,
          isRemoved: false,
          type: 'RECOMMANDATION',
        },
      })
      .aggregate()
      .count();
  },
  isShared: async (parent, _, ctx: Context) => {
    return ctx.prisma.$exists.partage({
      item: { code: 'ACTUALITE' },
      idItemAssocie: parent['_id'] || parent.id,
      isRemoved: false,
      type: 'PARTAGE',
    });
  },
  urgence: async (parent, _, ctx: Context) => {
    return ctx.prisma.actualite({ id: parent['_id'] || parent.id }).urgence();
  },
  importance: async (parent, _, ctx: Context) => {
    return ctx.prisma.actualite({ id: parent['_id'] || parent.id }).importance();
  },
};
