import { TicketOrigineResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const TicketOrigine: TicketOrigineResolvers.Type = {
  ...TicketOrigineResolvers.defaultResolvers,
  type: () => 'ticketorigine',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  groupement: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketOrigine({ id: parent['_id'] || parent.id }).groupement();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketOrigine({ id: parent['_id'] || parent.id }).userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma.ticketOrigine({ id: parent['_id'] || parent.id }).userModification();
  },
};
