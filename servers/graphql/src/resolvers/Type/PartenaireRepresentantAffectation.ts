import { PartenaireRepresentantAffectationResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PartenaireRepresentantAffectation: PartenaireRepresentantAffectationResolvers.Type = {
  ...PartenaireRepresentantAffectationResolvers.defaultResolvers,
  type: () => 'partenairerepresentantaffectation',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  departement: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partenaireRepresentantAffectation({ id: parent['_id'] || parent.id })
      .departement();
  },
  partenaireRepresentant: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partenaireRepresentantAffectation({ id: parent['_id'] || parent.id })
      .partenaireRepresentant();
  },
  partenaireRepresentantDemandeAffectation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partenaireRepresentantAffectation({ id: parent['_id'] || parent.id })
      .partenaireRepresentantDemandeAffectation();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partenaireRepresentantAffectation({ id: parent['_id'] || parent.id })
      .userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partenaireRepresentantAffectation({ id: parent['_id'] || parent.id })
      .userModification();
  },
};
