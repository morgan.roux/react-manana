import { ActeResolvers } from '../../generated/graphqlgen';

export const Acte: ActeResolvers.Type = {
  ...ActeResolvers.defaultResolvers,
  type: () => 'trActe',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
};
