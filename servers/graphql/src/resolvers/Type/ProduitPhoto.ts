import { ProduitPhotoResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const ProduitPhoto: ProduitPhotoResolvers.Type = {
  ...ProduitPhotoResolvers.defaultResolvers,
  fichier: async (parent, _, ctx: Context) => {
    return ctx.prisma.produitPhoto({ id: parent.id }).idFichier();
  },
  produit: async (parent, _, ctx: Context) => {
    return ctx.prisma.produitPhoto({ id: parent.id }).produit();
  },
};
