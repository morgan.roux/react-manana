import { PartenaireRepresentantDemandeAffectationResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const PartenaireRepresentantDemandeAffectation: PartenaireRepresentantDemandeAffectationResolvers.Type = {
  ...PartenaireRepresentantDemandeAffectationResolvers.defaultResolvers,
  type: () => 'partenairerepresentantdemandeaffectation',
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  remplacent: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partenaireRepresentantDemandeAffectation({ id: parent['_id'] || parent.id })
      .remplacent();
  },
  userCreation: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partenaireRepresentantDemandeAffectation({ id: parent['_id'] || parent.id })
      .userCreation();
  },
  userModification: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .partenaireRepresentantDemandeAffectation({ id: parent['_id'] || parent.id })
      .userModification();
  },
};
