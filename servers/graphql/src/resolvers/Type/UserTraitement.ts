import { UserTraitementResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const UserTraitement: UserTraitementResolvers.Type = {
  ...UserTraitementResolvers.defaultResolvers,
  codeTraitement: async (parent, _, ctx: Context) => {
    const traitement = await ctx.prisma.userTraitement({ id: parent.id }).traitement();
    return traitement ? traitement.code : null;
  },
};
