import { IdeeOuBonnePratiqueSmyleyResolvers } from '../../generated/graphqlgen';
import { Context } from '../../types';

export const IdeeOuBonnePratiqueSmyley: IdeeOuBonnePratiqueSmyleyResolvers.Type = {
  ...IdeeOuBonnePratiqueSmyleyResolvers.defaultResolvers,
  id: async (parent, _, __) => {
    return (parent['_id'] || parent.id) as any;
  },
  ideeOuBonnePratique: async (parent, _, ctx: Context) => {
    return ctx.prisma
      .ideeOuBonnePratiqueSmyley({ id: parent['_id'] || parent.id })
      .ideeOuBonnePratique();
  },
  user: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiqueSmyley({ id: parent['_id'] || parent.id }).user();
  },
  smyley: async (parent, _, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiqueSmyley({ id: parent['_id'] || parent.id }).smyley();
  },
};
