// This resolver file was scaffolded by github.com/prisma/graphqlgen, DO NOT EDIT.
// Please do not import this file directly but copy & paste to your application code.

// import { Resolvers } from '../generated/graphqlgen';

import { Query } from './Query';
import Type from './Type';

import { Mutation } from './Mutation';
import { GraphQLDateTime } from 'graphql-iso-date';
import GraphQLJSON, { GraphQLJSONObject } from 'graphql-type-json';
import { Subscription } from './Subscription';
import { RedisPubSub } from 'graphql-redis-subscriptions';
import Redis from 'ioredis';

export const resolvers = {
  DateTime: GraphQLDateTime,
  JSON: GraphQLJSON,
  JSONObject: GraphQLJSONObject,
  Query,
  Mutation,
  Subscription,
  ...Type,
};

const redisConfig = {
  host: process.env.REDIS_HOST,
  port: +process.env.REDIS_PORT,
};

export const pubsub = new RedisPubSub({
  publisher: new Redis(redisConfig),
  subscriber: new Redis(redisConfig),
});
