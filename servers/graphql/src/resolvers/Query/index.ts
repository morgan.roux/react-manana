import { QueryResolvers } from '../../generated/graphqlgen';
import Pharmacie from './pharmacie';
import Titulaire from './titulaire';
import Groupement from './groupement';
import Laboratoire from './laboratoire';
import Personnel from './personnel';
import User from './user';
import Search from './search';
import Ppersonnel from './ppersonnel';
import Partenaire from './partenaire';
import Role from './role';
import Panier from './panier';
import Commande from './commande';
import Operation from './operation';
import Notification from './notification';
import ProduiCanal from './produitCanal';
import Comment from './comment';
import canalGamme from './canalGamme';
import Famille from './Famille';
import Sale from './sale';
import Indexes from './indexes';
import Avatar from './avatar';
import Actualite from './actualite';
import Action from './action';
import Project from './project';
import commandeCanal from './commandeCanal';
import commandeType from './commandeType';
import Smyley from './smyley';
import Parameter from './parameter';
import Region from './region';
import PresidentRegion from './presidentRegion';
import Departement from './departement';
import ActualiteOrigine from './actualiteOrigine';
import Services from './service';
import Helloidsso from './helloidsso';
import Connexion from './connexion';
import ActiviteUser from './activiteUser';
import PanierPriceByCanal from './panierPriceByCanal';
import ActiveDirecoryUser from './activeDirecoryUser';
import ActiveDirecory from './activeDirectory';
import Ftp from './ftp';
import SsoApplication from './ssoApplication';
import Publicite from './publicite';
import SuiviPublicitaire from './suiviPublicitaire';
import Item from './item';
import StatistiqueBusiness from './statistiqueBusiness';
import StatistiqueFeedback from './statistiqueFeedback';
import GammeCatalogue from './gammeCatalogue';
import SousGammeCatalogue from './sousGammeCatalogue';
import Acte from './Acte';
import TVA from './TVA';
import Produit from './produit';
import Marche from './marche';
import GroupeClient from './groupeClient';
import Promotion from './promotion';
import Traitement from './traitement';
import HealtCheck from './healthCheck';
import Pilotage from './pilotage';
import Automate from './automate';
import Facade from './facade';
import Concurrent from './concurrent';
import Contrat from './contrat';
import Generiqueur from './generiqueur';
import Grossiste from './grossiste';
import Logiciel from './logiciel';
import Motif from './motif';
import PrestatairePharmacie from './prestatairePharmacie';
import Qualite from './qualite';
import ServicePharmacie from './servicePharmacie';
import SSII from './ssii';
import TrancheCA from './trancheCA';
import Typologie from './typologie';
import Liste from './liste';
import LibelleDivers from './libelleDivers';
import TauxSS from './tauxSS';
import PersonnelAffectation from './personnelAffectation';
import PersonnelFonction from './personnelFonction';
import PersonnelDemandeAffectation from './personnelDemandeAffectation';
import TitulaireAffectation from './titulaireAffectation';
import TitulaireFonction from './titulaireFonction';
import TitulaireDemandeAffectation from './titulaireDemandeAffectation';
import Messagerie from './messagerie';
import Contact from './contact';
import PartenaireServiceSuitePartenaire from './partenaireServiceSuitePartenaire';
import PartenaireServiceSuite from './partenaireServiceSuite';
import Ressource from './ressource';
import ServiceType from './serviceType';
import ServicePartenaire from './servicePartenaire';
import PartenaireRepresentant from './partenaireRepresentant';
import PartenaireRepresentantDemandeAffectation from './partenaireRepresentantDemandeAffectation';
import PartenaireRepresentantAffectation from './partenaireRepresentantAffectation';
import Ticket from './ticket';
import ticketFichierJoint from './ticketFichierJoint';
import ticketChangementStatut from './ticketChangementStatut';
import ticketChangementCible from './ticketChangementCible';
import TicketMotif from './ticketMotif';
import TicketOrigine from './ticketOrigine';
import TicketStatut from './ticketStatut';
import Aide from './aide';
import TodoParticipant from './todoParticipant';
import TodoProjetFavoris from './todoProjetFavoris';
import Couleur from './couleur';
import TodoSection from './todoSection';
import CommentaireFichierJoint from './commentaireFichierJoint';
import TodoActionAttribution from './todoActionAttribution';
import TodoActionEtiquette from './todoActionEtiquette';
import TodoActionOrigine from './todoActionOrigine';
import TodoActionType from './todoActionType';
import TodoEtiquetteFavoris from './todoEtiquetteFavoris';
import TodoEtiquette from './todoEtiquette';
import TodoActionChangementStatus from './todoActionChangementStatus';
import ActionActivite from './actionActivite';
import TodoEtiquetteParticipant from './todoEtiquetteParticipant';
import InformationLiaison from './informationLiaison';
import InformationLiaisonFichierJoint from './informationLiaisonFichierJoint';
import GroupeAmis from './groupeAmis';
import GroupeAmisDetails from './groupeAmisDetails';
import Partage from './partage';
import PartageFonctionCible from './partageFonctionCible';
import PartagePharmacieCible from './partagePharmacieCible';
import PartageGroupeAmisCible from './partageGroupeAmisCible';
import IdeeOuBonnePratique from './ideeOuBonnePratique';
import IdeeOuBonnePratiqueClassification from './ideeOuBonnePratiqueClassification';
import IdeeOuBonnePratiqueFichierJoint from './ideeOuBonnePratiqueFichierJoint';
import IdeeOuBonnePratiqueLu from './ideeOuBonnePratiqueLu';
import Rgpd from './rgpd';
import RgpdAccueil from './rgpdAccueil';
import RgpdAccueilPlus from './rgpdAccueilPlus';
import RgpdAutorisation from './rgpdAutorisation';
import RgpdHistorique from './rgpdHistorique';
import RgpdHistoriqueInfo from './rgpdHistoriqueInfoPlus';
import RgpdPartenaire from './rgpdPartenaire';
import LaboratoirePartenaire from './laboratoirePartenaire';
import LaboratoireRepresentant from './laboratoireRepresentant';
import LaboratoireRessource from './laboratoireRessource';
import Urgence from './urgence';

export const Query: QueryResolvers.Type | any = {
  ...QueryResolvers.defaultResolvers,
  ...Pharmacie,
  ...Titulaire,
  ...Groupement,
  ...Laboratoire,
  ...LaboratoirePartenaire,
  ...Personnel,
  ...User,
  ...Search,
  ...Ppersonnel,
  ...Partenaire,
  ...Role,
  ...Panier,
  ...Commande,
  ...Operation,
  ...Notification,
  ...ProduiCanal,
  ...Comment,
  ...canalGamme,
  ...Famille,
  ...Sale,
  ...Indexes,
  ...Avatar,
  ...Actualite,
  ...Action,
  ...Project,
  ...commandeCanal,
  ...commandeType,
  ...Smyley,
  ...Parameter,
  ...Region,
  ...PresidentRegion,
  ...Departement,
  ...ActualiteOrigine,
  ...Services,
  ...Helloidsso,
  ...Connexion,
  ...ActiviteUser,
  ...ActiveDirecory,
  ...PanierPriceByCanal,
  ...Publicite,
  ...SuiviPublicitaire,
  ...ActiveDirecoryUser,
  ...Ftp,
  ...SsoApplication,
  ...Item,
  ...StatistiqueBusiness,
  ...StatistiqueFeedback,
  ...GammeCatalogue,
  ...SousGammeCatalogue,
  ...Acte,
  ...TVA,
  ...Produit,
  ...Marche,
  ...GroupeClient,
  ...Promotion,
  ...Traitement,
  ...HealtCheck,
  ...Pilotage,
  ...Automate,
  ...Facade,
  ...Concurrent,
  ...Contrat,
  ...Generiqueur,
  ...Grossiste,
  ...Logiciel,
  ...Motif,
  ...PrestatairePharmacie,
  ...Qualite,
  ...ServicePharmacie,
  ...SSII,
  ...TrancheCA,
  ...Typologie,
  ...Liste,
  ...LibelleDivers,
  ...TauxSS,
  ...PersonnelAffectation,
  ...PersonnelFonction,
  ...PersonnelDemandeAffectation,
  ...TitulaireAffectation,
  ...TitulaireFonction,
  ...TitulaireDemandeAffectation,
  ...Messagerie,
  ...Contact,
  ...PartenaireServiceSuitePartenaire,
  ...PartenaireServiceSuite,
  ...Ressource,
  ...ServiceType,
  ...ServicePartenaire,
  ...PartenaireRepresentant,
  ...PartenaireRepresentantDemandeAffectation,
  ...PartenaireRepresentantAffectation,
  ...Ticket,
  ...ticketFichierJoint,
  ...ticketChangementStatut,
  ...ticketChangementCible,
  ...TicketMotif,
  ...TicketOrigine,
  ...TicketStatut,
  ...Aide,
  ...TodoParticipant,
  ...TodoProjetFavoris,
  ...Couleur,
  ...TodoSection,
  ...CommentaireFichierJoint,
  ...TodoActionAttribution,
  ...TodoActionEtiquette,
  ...TodoActionOrigine,
  ...TodoActionType,
  ...TodoEtiquetteFavoris,
  ...TodoEtiquette,
  ...TodoActionChangementStatus,
  ...ActionActivite,
  ...TodoEtiquetteParticipant,
  ...InformationLiaison,
  ...InformationLiaisonFichierJoint,
  ...GroupeAmis,
  ...GroupeAmisDetails,
  ...Partage,
  ...PartageFonctionCible,
  ...PartagePharmacieCible,
  ...IdeeOuBonnePratique,
  ...IdeeOuBonnePratiqueClassification,
  ...IdeeOuBonnePratiqueFichierJoint,
  ...IdeeOuBonnePratiqueLu,
  ...PartageGroupeAmisCible,
  ...Rgpd,
  ...RgpdAccueil,
  ...RgpdAccueilPlus,
  ...RgpdAutorisation,
  ...RgpdHistorique,
  ...RgpdHistoriqueInfo,
  ...RgpdPartenaire,
  ...LaboratoireRepresentant,
  ...LaboratoireRessource,
  ...Urgence,
};
