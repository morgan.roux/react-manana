import { Context } from '../../types';

export default {
  servicePartenaire: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.servicePartenaire({ id });
  },
  servicePartenaires: async (_: any, __: any, ctx: Context) => {
    return ctx.prisma.servicePartenaires();
  },
};
