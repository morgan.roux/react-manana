import { Context } from '../../types';

export default {
  qualites: async (_, __, ctx: Context) => {
    return ctx.prisma.qualites();
  },
};
