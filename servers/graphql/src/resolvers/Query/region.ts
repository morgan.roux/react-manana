import { Context } from '../../types';

export default {
  regions: async (_, __, ctx: Context) => {
    return ctx.prisma.regions();
  },
};
