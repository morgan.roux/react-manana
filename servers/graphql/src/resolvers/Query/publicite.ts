import { Context } from '../../types';

export default {
  publicite: async (_, { id }, ctx: Context) => {
    return ctx.prisma.publicite({ id });
  },
};
