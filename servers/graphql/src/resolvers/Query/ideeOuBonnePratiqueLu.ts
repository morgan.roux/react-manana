import { Context } from '../../types';

export default {
  ideeOuBonnePratiqueLu: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiqueLu({ id });
  },
  ideeOuBonnePratiqueLus: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiqueLus({ where: { isRemoved } });
  },
};
