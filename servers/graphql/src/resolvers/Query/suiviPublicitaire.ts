import { Context } from '../../types';

export default {
  suiviPublicitaire: async (_, { id }, ctx: Context) => {
    return ctx.prisma.suiviPublicitaire({ id });
  },
};
