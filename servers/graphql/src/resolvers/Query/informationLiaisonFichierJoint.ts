import { Context } from '../../types';

export default {
  informationLiaisonFichierJoint: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.informationLiaisonFichierJoint({ id });
  },
  informationLiaisonFichierJoints: async (
    _: any,
    { isRemoved }: { isRemoved: boolean },
    ctx: Context,
  ) => {
    return ctx.prisma.informationLiaisonFichierJoints({ where: { isRemoved } });
  },
};
