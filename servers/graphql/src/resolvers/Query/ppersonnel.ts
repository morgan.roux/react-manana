import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';
export default {
  ppersonnel: (_, { id }, ctx: Context) => {
    return ctx.prisma.ppersonnel({ id });
  },
  ppersonnels: (parent, args, ctx: Context) => {
    return ctx.prisma.ppersonnels();
  },
};
