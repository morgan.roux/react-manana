import { Context } from '../../types';

export default {
  servicePharmacies: async (_, __, ctx: Context) => {
    return ctx.prisma.servicePharmacies();
  },
};
