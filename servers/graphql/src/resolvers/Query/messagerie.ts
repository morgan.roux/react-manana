import { Context, FilterMessageriesCount, MessagerieArgs, MessagerieResult } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';
import { messageries, filterMessageriesCount } from '../../repository/messagerie';

export default {
  messagerie: async (_: any, { id }: { id: string }, ctx: Context) => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser({ code: 'GMESSAGERIE', nom: 'GET MESSAGERIE' }, ctx);
    const res = await ctx.prisma.messagerie({ id });
    return res;
  },
  messageries: async (_: any, args: MessagerieArgs, ctx: Context): Promise<MessagerieResult> => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser({ code: 'GMESSAGERIES', nom: 'GET MESSAGERIES' }, ctx);
    return messageries(ctx, args);
  },
  messagerieNonLus: async (_: any, __, ctx: Context): Promise<MessagerieResult> => {
    return messageries(ctx, { typeMessagerie: 'R' as any, isRemoved: false, lu: false });
  },
  filterMessageriesCount: async (
    _: any,
    { typeMessagerie },
    ctx: Context,
  ): Promise<FilterMessageriesCount> => {
    return filterMessageriesCount(ctx, typeMessagerie);
  },
  messagerieThemes: async (_: any, __, ctx: Context) => {
    return ctx.prisma.messagerieThemes();
  },
};
