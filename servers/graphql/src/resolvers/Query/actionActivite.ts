import { Context } from '../../types';

export default {
  actionActivite: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.actionActivite({ id });
  },
  actionActivites: async (_: any, { idAction }: { idAction: string }, ctx: Context) => {
    return ctx.prisma.actionActivites({ where: { action: { id: idAction } } });
  },
};
