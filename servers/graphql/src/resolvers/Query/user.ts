import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';
import { getuserByToken, CheckExistingUserMail, UsersGroupement } from '../../repository/user';

export default {
  me: async (_, __, ctx: Context) => {
    console.log('************* ctx.userId **************** ', ctx.userId);
    return ctx.prisma.user({ id: ctx.userId });
  },
  user: async (_, { id }, ctx: Context) => {
    return ctx.prisma.user({ id }).then(value => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      if (ctx && ctx.userId) {
        createActiviteUser(
          {
            code: 'GUSER',
            nom: 'GET USEr',
          },
          ctx,
        );
      }

      return value;
    });
  },
  users: async (_, __, ctx: Context) => {
    return ctx.prisma.users().then(value => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      if (ctx && ctx.userId) {
        createActiviteUser(
          {
            code: 'GUSERS',
            nom: 'GET USERS',
          },
          ctx,
        );
      }

      return value;
    });
  },
  userByToken: async (_, { token }, ctx: Context) => {
    return getuserByToken(ctx, { token });
  },
  checkExistingUserMail: async (_, { email }, ctx: Context) => {
    return CheckExistingUserMail(email, ctx);
  },
  usersGroupement: async (_, { idGroupement }, ctx: Context) => {
    return UsersGroupement(ctx, { idGroupement });
  },
};
