import { Context } from '../../types';

export default {
  marche: async (_, { id }, ctx: Context) => {
    return ctx.prisma.marche({ id });
  },
};
