import { Context } from '../../types';
import { reindexAll, getConfig, reindexDatas } from '../../services/indexing';
import { ApolloError } from 'apollo-server';
import { WatchTime } from '../../services/time';

const watchTime = new WatchTime();

const execReindexAll = async (ctx: Context) => {
  watchTime.start();

  return reindexAll(ctx).then(() => {
    watchTime.stop();
    watchTime.printElapsedSeconds(`***** Temps mise pour refreshIndexes ====> `);
    return true;
  });
};

export default {
  refreshIndexes: async (_, { withMapping }, ctx: Context) => {
    if (withMapping) {
      return ctx.indexing.start().then(() => execReindexAll(ctx));
    }
    return execReindexAll(ctx);
  },
  reindex: async (_, { routingKey }, ctx: Context) => {
    const config = getConfig(routingKey);
    watchTime.start();
    return config
      ? reindexDatas([config], ctx)
          .then(() => {
            watchTime.stop();
            watchTime.printElapsedSeconds(`***** Temps mise pour reindex ====> `);
            return true;
          })
          .catch((err) => {
            console.log('err', err);
            return false;
          })
      : new ApolloError('RoutingKey nout found ', 'ROUTING_KEY_NOT_FOUND');
  },

  mappingIndex: async (_, { routingKey }, ctx: Context) => {
    return ctx.indexing.initIndex(routingKey).then(() => true);
  },

  publishIndex: async (_, { routingKey, action, id }, ctx: Context) => {
    return ctx.indexing.publish(routingKey, action, id).then(() => true);
  },
};
