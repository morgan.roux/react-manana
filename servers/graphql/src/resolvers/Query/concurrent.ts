import { Context } from '../../types';

export default {
  concurrents: async (_, __, ctx: Context) => {
    return ctx.prisma.concurrents();
  },
};
