import { Context } from '../../types';

export default {
  rgpdPartenaire: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.rgpdPartenaire({ id });
  },
  rgpdPartenaires: async (_: any, { idGroupement }: { idGroupement: string }, ctx: Context) => {
    return ctx.prisma.rgpdPartenaires({ where: { groupement: { id: idGroupement } } });
  },
};
