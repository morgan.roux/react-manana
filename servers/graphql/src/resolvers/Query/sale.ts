import { Context } from '../../types';
export default {
  sales: (_, { nom }, ctx: Context) => {
    const data = [
      {
        date: '2019-01-16T00:00:00.000Z',
        value: 4000,
      },
      {
        date: '2019-02-16T00:00:00.000Z',
        value: 3000,
      },
      {
        date: '2019-03-16T00:00:00.000Z',
        value: 2000,
      },
      {
        date: '2019-04-16T00:00:00.000Z',
        value: 2780,
      },
      {
        date: '2019-05-16T00:00:00.000Z',
        value: 1890,
      },
      {
        date: '2019-06-16T00:00:00.000Z',
        value: 2390,
      },
      {
        date: '2019-07-16T00:00:00.000Z',
        value: 100,
      },
      {
        date: '2019-08-16T00:00:00.000Z',
        value: 500,
      },
      {
        date: '2019-09-16T00:00:00.000Z',
        value: 18000,
      },
      {
        date: '2019-10-16T00:00:00.000Z',
        value: 10000,
      },
      {
        date: '2019-11-16T00:00:00.000Z',
        value: 20000,
      },
      {
        date: '2019-12-16T00:00:00.000Z',
        value: 0,
      },
    ];

    return data;
  },
};
