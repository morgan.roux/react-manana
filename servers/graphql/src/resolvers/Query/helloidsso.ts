import { Context } from '../../types';
import {
  helloIdSsoByGroupementId,
  HelloidGroups,
  HelloidApplications,
  AllHelloidGroupsInfo,
  getUserApplications,
} from '../../repository/helloidsso';

export default {
  userApplications: async (_, { iduser, idGroupement }, ctx: Context) => {
    return getUserApplications(ctx, { iduser, idGroupement });
  },
  helloIdSsoByGroupementId: async (_, { idgroupement }, ctx: Context) => {
    return helloIdSsoByGroupementId(ctx, { idgroupement });
  },
  helloidApplications: async (_, { idgroupement }, ctx: Context) => {
    return HelloidApplications(ctx, { idgroupement });
  },
  helloidGroups: async (_, { idgroupement }, ctx: Context) => {
    return HelloidGroups({ idgroupement }, ctx);
  },
  allHelloidGroupsInfo: async (_, { idgroupement }, ctx: Context) => {
    return AllHelloidGroupsInfo({ idgroupement }, ctx);
  },
};
