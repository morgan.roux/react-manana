import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';
export default {
  role: async (_, { id }, ctx: Context) => {
    return ctx.prisma.role({ id });
  },
  roles: async (_, __, ctx: Context) => {
    return ctx.prisma.roles();
  },
};
