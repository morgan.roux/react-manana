import { Context } from '../../types';
import { comments } from '../../repository/comment';
import { WatchTime } from '../../services/time';

const watchTime = new WatchTime();

export default {
  comment: async (_: any, { id }: { id: string }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.comment({ id }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** COMMENT ** Le temps de récuperation du comment avec l'ID => ${id} : `,
      );
      return value;
    });
  },
  comments: async (_: any, { codeItem, idItemAssocie, take, skip }, ctx: Context) => {
    watchTime.start();
    return comments({ codeItem, idItemAssocie, take, skip }, ctx).then(data => {
      watchTime.stop();
      watchTime.printElapsed(
        `** COMMENT ** Le temps de récuperation de toutes les Commentaires : `,
      );

      return data;
    });
  },
};
