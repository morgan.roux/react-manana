import { Context } from '../../types';

export default {
  logiciels: async (_, __, ctx: Context) => {
    return ctx.prisma.logiciels();
  },
};
