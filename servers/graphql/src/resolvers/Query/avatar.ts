import { Context } from '../../types';

export default {
  avatar: async (_, { id }, ctx: Context) => {
    return ctx.prisma.avatar({ id });
  },
  avatars: async (_, __, ctx: Context) => {
    return ctx.prisma.avatars();
  },
  groupementAvatars: async (_, { idGroupement }, ctx: Context) => {
    if (idGroupement) return ctx.prisma.avatars({ where: { groupement: { id: idGroupement } } });
    if (ctx.groupementId) {
      return ctx.prisma.avatars({
        where: {
          groupement: {
            id: ctx.groupementId,
          },
        },
      });
    }

    console.log('You should specify groupement id or run query in the client');

    return null;
  },
};
