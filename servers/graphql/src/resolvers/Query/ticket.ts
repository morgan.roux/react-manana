import { Context } from '../../types';

export default {
  ticket: (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ticket({ id });
  },
  tickets: (_: any, __: any, ctx: Context) => {
    return ctx.prisma.tickets();
  },
};
