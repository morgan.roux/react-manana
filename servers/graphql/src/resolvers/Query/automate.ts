import { Context } from '../../types';

export default {
  automates: async (_, __, ctx: Context) => {
    return ctx.prisma.automates();
  },
};
