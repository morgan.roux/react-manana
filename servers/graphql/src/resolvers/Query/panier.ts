import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';
import { myPanier } from '../../repository/panier';
import { WatchTime } from '../../services/time';

const watchTime = new WatchTime();

export default {
  panier: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.panier({ id }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** PANIER ** Le temps de récuperation du panier avec l'ID => ${id} : `,
      );

      return value;
    });
  },
  panierLigne: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.panierLigne({ id }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** PANIER LIGNE ** Le temps de récuperation de tous les lignes du panier avec l'ID => ${id}  : `,
      );

      return value;
    });
  },
  myPanier: async (_, { idPharmacie, codeCanal }, ctx: Context) => {
    watchTime.start();
    const myCurrentPanier = ctx.userId ? await myPanier(ctx, { idPharmacie, codeCanal }) : null;

    watchTime.stop();
    watchTime.printElapsed(`** PANIER  ** Le temps de récuperation de mon current panier  : `);

    return myCurrentPanier;
  },
  myPaniers: async (_, { idPharmacie }, ctx: Context) => {
    watchTime.start();
    const myPaniers = ctx.userId
      ? await ctx.prisma.paniers({
          where: {
            idUser: { id: ctx.userId },
            idPharmacie: { id: idPharmacie },
            validate: false,
          },
        })
      : [];

    watchTime.stop();
    watchTime.printElapsed(
      `** PANIER CANAL PRIX ** Le temps de récuperation de tous mes paniers : `,
    );

    return myPaniers;
  },
  pharmaciePanier: async (_, { idPharmacie }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma
      .paniers({
        where: { idPharmacie: { id: idPharmacie }, validate: false },
      })
      .then(value => {
        watchTime.stop();
        watchTime.printElapsed(
          `** PANIER CANAL PRIX ** Le temps de récuperation de tous les panier du pharmacie avec l'ID ${idPharmacie}  : `,
        );

        return value;
      });
  },
};
