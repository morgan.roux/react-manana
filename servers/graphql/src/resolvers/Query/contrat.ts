import { Context } from '../../types';

export default {
  contrats: async (_, __, ctx: Context) => {
    return ctx.prisma.contrats();
  },
};
