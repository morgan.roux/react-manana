import { Context } from '../../types';

export default {
  grossistes: async (_, __, ctx: Context) => {
    return ctx.prisma.grossistes();
  },
};
