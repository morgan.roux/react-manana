import { Context } from '../../types';

export default {
  ressource: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ressource({ id });
  },
  ressources: async (_: any, __: any, ctx: Context) => {
    return ctx.prisma.ressources();
  },
};
