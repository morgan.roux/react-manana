import { Context } from '../../types';

export default {
  activiteUser: async (_, { id }, ctx: Context) => {
    return ctx.prisma.activiteUser({ id });
  },
  activiteUsers: async (_, __, ctx: Context) => {
    return ctx.prisma.activiteUsers();
  },
  userActiviteUser: async (_, { idUser }, ctx: Context) => {
    return ctx.prisma.activiteUsers({ where: { idUser: { id: idUser } } });
  },
};
