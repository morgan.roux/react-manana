import { Context } from '../../types';

export default {
  ticketStatut: (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ticketStatut({ id });
  },
  ticketStatuts: (_: any, __: any, ctx: Context) => {
    return ctx.prisma.ticketStatuts();
  },
};
