import {
  countActionByUser,
  countActionsOfOthers,
  countMyActionFromSearch,
  countActionTeamFromSearch,
  countActionFromSearch,
} from '../../repository/action';
import { Context, CountTodoResult, CountTodosInput } from '../../types';

export default {
  project: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.project({ id });
  },
  projects: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.projects({ where: { isRemoved } });
  },
  projectsByUser: async (
    _: any,
    { idUser, isRemoved }: { idUser: string; isRemoved: boolean },
    ctx: Context,
  ) => {
    const projects = await ctx.prisma.projects({ where: { user: { id: idUser }, isRemoved } });
    const not_in =
      projects && projects.length
        ? {
          project: { id_not_in: projects.map(project => project.id) },
        }
        : [];
    const othersProjects = await ctx.prisma
      .todoParticipants({
        where: {
          userParticipant: { id_in: [idUser] },
          isRemoved: false,
          ...not_in,
        },
      })
      .then(async results => {
        return Promise.all(
          results.map(res => {
            return ctx.prisma.todoParticipant({ id: res.id }).project();
          }),
        );
      });

    const allProjects = [...projects, ...othersProjects];
    return allProjects;
  },
  countTodos: async (_: any, { input }: { input: CountTodosInput }, ctx: Context) => {
    const { idUser, taskStatus } = input;

    const results: CountTodoResult = {
      project: {
        groupement:0, //await countProjectByUserAndType(ctx, 'GROUPEMENT', idUser),
        personnel:0, //await countProjectByUserAndType(ctx, 'PERSONNEL', idUser),
        professionnel:0 //await countProjectByUserAndType(ctx, 'PROFESSIONNEL', idUser),
      },
      action: {
        actionsOfOthers: await countActionsOfOthers(ctx, input),
        myActions: await countActionByUser(ctx, input),
        notAssignedActions:0, //await countNotAssignedActions(ctx, input),
        priorityOne: 0, //await countActionFromSearch(ctx, input, { isPriority: true, priority: 1 }),
        priorityTwo: 0, //await countActionFromSearch(ctx, input, { isPriority: true, priority: 2 }),
        priorityThree: 0, //await countActionFromSearch(ctx, input, { isPriority: true, priority: 3 }),
        priorityFour: 0, //await countActionFromSearch(ctx, input, { isPriority: true, priority: 4 }),
        withDateEcheance:0, //await countActionWithDateEcheance(ctx, input),
        withoutDateEcheance:0 /*await countMyActionFromSearch(ctx, input, {
          isWithoutDates: true,
        })*/,
        inbox: 0,//await countMyActionFromSearch(ctx, input, {}),
        inboxTeam: 0,//await countActionTeamFromSearch(ctx, input, {}),
        today: await countMyActionFromSearch(ctx, input, { isToday: true }),
        todayTeam: await countActionTeamFromSearch(ctx, input, { isToday: true }),
        nextSeventDays: await countMyActionFromSearch(ctx, input, { isSevenDays: true }),
        nextSeventDaysTeam: await countActionTeamFromSearch(ctx, input, { isSevenDays: true }),
        all: await countActionFromSearch(ctx, input, { isAll: true }),
        taskActive: await countActionFromSearch(ctx, input, {
          isStatus: true,
          status: 'ACTIVE',
        }),
        taskDone: await countActionFromSearch(ctx, input, {
          isStatus: true,
          status: 'DONE',
        }),
      },
    };
    return results;
  },
};
