import { Context } from '../../types';

export default {
  partageFonctionCible: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.partageFonctionCible({ id });
  },
  partageFonctionCibles: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.partageFonctionCibles({ where: { isRemoved } });
  },
};
