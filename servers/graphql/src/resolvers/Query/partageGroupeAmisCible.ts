import { Context } from '../../types';

export default {
  partageGroupeAmisCible: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.partageGroupeAmisCible({ id });
  },
  partageGroupeAmisCibles: async (_: any, {}, ctx: Context) => {
    return ctx.prisma.partageGroupeAmisCibles();
  },
};
