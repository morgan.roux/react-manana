import { Context } from '../../types';

export default {
  personnel: async (_, { id }, ctx: Context) => {
    return ctx.prisma.personnel({ id });
  },
};
