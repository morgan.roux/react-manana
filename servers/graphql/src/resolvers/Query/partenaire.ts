import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  partenaire: async (_, { id }, ctx: Context) => {
    return ctx.prisma.partenaire({ id });
  },
};
