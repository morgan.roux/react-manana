import { Context } from '../../types';
import { WatchTime } from '../../services/time';

const watchTime = new WatchTime();

export default {
  commande: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.commande({ id }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** COMMANDE ** Le temps de récuperation d'une commande avec l'ID => ${id} : `,
      );
      return value;
    });
  },
  myCommande: async (_, __, ctx: Context) => {
    watchTime.start();
    const me = await ctx.prisma.user({ id: ctx.userId });
    return ctx.userId && me
      ? ctx.prisma.commandes({ where: { idUser: { id: me.id } } }).then(commandes => {
          watchTime.stop();
          watchTime.printElapsed(`** COMMANDE ** Le temps de récuperation de mes commandes  `);

          return commandes[0];
        })
      : null;
  },
  commandeLigne: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.commandeLigne({ id }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** COMMANDE ** Le temps de récuperation de mes lignes de commandes :  `,
      );

      return value;
    });
  },
  pharmacieCommandes: async (_, { idPharmacie }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma
      .commandes({
        where: { idPharmacie: { id: idPharmacie } },
      })
      .then(value => {
        watchTime.stop();
        watchTime.printElapsed(
          `** COMMANDE ** Le temps de récuperation des commdes du pharmacie avec l'ID ${idPharmacie} : `,
        );

        return value;
      });
  },
};
