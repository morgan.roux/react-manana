import { Context } from '../../types';

export default {
  partenaireRepresentant: (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.partenaireRepresentant({ id });
  },
  partenaireRepresentants: async (_: any, __: any, ctx: Context) => {
    return ctx.prisma.partenaireRepresentants();
  },
};
