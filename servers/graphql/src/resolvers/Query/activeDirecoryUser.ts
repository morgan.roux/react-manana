import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';
export default {
  activeDirectoryUser: (_, { activeDirectoryId }, ctx: Context) => {
    return ctx.prisma
      .activeDirectoryUsers({ where: { activeDirectory: { id: activeDirectoryId } } })
      .then(value => {
        /**
         *
         * Sauvegarde activite de l'utilisateur
         *
         */

        createActiviteUser(
          {
            code: 'GACTIONS',
            nom: 'GET ACTIONS',
          },
          ctx,
        );

        return value;
      });
  },
};
