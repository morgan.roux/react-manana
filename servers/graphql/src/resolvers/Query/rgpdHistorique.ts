import { Context } from '../../types';

export default {
  rgpdHistorique: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.rgpdHistorique({ id });
  },
  rgpdHistoriques: async (_: any, { idGroupement }: { idGroupement: string }, ctx: Context) => {
    return ctx.prisma.rgpdHistoriques({ where: { groupement: { id: idGroupement } } });
  },
};
