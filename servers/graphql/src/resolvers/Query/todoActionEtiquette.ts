import { Context } from '../../types';

export default {
  todoActionEtiquette: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.todoActionEtiquette({ id });
  },
  todoActionEtiquettes: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.todoActionEtiquettes({ where: { isRemoved } });
  },
};
