import { Context } from '../../types';

export default {
  ideeOuBonnePratiqueClassification: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiqueClassification({ id });
  },
  ideeOuBonnePratiqueClassifications: async (
    _: any,
    { isRemoved }: { isRemoved: boolean },
    ctx: Context,
  ) => {
    return ctx.prisma.ideeOuBonnePratiqueClassifications({ where: { isRemoved } });
  },
};
