import { Context } from '../../types';
import { FtpConnect, FtpGroupement } from '../../repository/ftp';

export default {
  ftpConnect: async (_, { id }, ctx: Context) => {
    return FtpConnect(ctx, { id });
  },
  ftpGroupement: async (_, { idgroupement }, ctx: Context) => {
    return FtpGroupement(ctx, { idgroupement });
  },
};
