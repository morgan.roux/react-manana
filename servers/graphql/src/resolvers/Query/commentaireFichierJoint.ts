import { Context } from '../../types';

export default {
  commentaireFichierJoint: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.commentaireFichierJoint({ id });
  },
  commentaireFichierJoints: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.commentaireFichierJoints({ where: { isRemoved } });
  },
};
