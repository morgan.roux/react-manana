import { Context } from '../../types';

export default {
  rgpdAccueil: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.rgpdAccueil({ id });
  },
  rgpdAccueils: async (_: any, { idGroupement }: { idGroupement: string }, ctx: Context) => {
    return ctx.prisma.rgpdAccueils({ where: { groupement: { id: idGroupement } } });
  },
};
