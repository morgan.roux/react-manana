import { Context } from '../../types';

export default {
  todoEtiquetteParticipant: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.todoEtiquetteParticipant({ id });
  },
  todoEtiquetteParticipants: async (
    _: any,
    { isRemoved }: { isRemoved: boolean },
    ctx: Context,
  ) => {
    return ctx.prisma.todoEtiquetteParticipants({ where: { isRemoved } });
  },
};
