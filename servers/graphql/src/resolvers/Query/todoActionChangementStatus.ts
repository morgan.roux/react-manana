import { Context } from '../../types';

export default {
  todoActionChangementStatus: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.todoActionChangementStatus({ id });
  },
  todoActionChangementStatuses: async (
    _: any,
    { isRemoved }: { isRemoved: boolean },
    ctx: Context,
  ) => {
    return ctx.prisma.todoActionChangementStatuses({ where: { isRemoved } });
  },
};
