import { Context } from '../../types';

export default {
  typologies: async (_, __, ctx: Context) => {
    return ctx.prisma.typologies();
  },
};
