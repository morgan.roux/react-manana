import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  presidentRegions: async (_, __, ctx: Context) => {
    return ctx.prisma.titulaireAffectations();
  },
};
