import { Context } from '../../types';

export default {
  ssii: (_, { idLogiciel }, ctx: Context) => {
    return ctx.prisma.logiciel({ id: idLogiciel }).ssii();
  },
};
