import { Context } from '../../types';

export default {
  ticketMotif: (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ticketMotif({ id });
  },
  ticketMotifs: (_: any, __: any, ctx: Context) => {
    return ctx.prisma.ticketMotifs();
  },
};
