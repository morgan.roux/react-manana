import { Context } from '../../types';

export default {
  action: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.action({ id });
  },
  actions: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.actions({ where: { isRemoved } });
  },
};
