import { Context } from '../../types';

export default {
  couleur: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.couleur({ id });
  },
  couleurs: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.couleurs({ where: { isRemoved } });
  },
};
