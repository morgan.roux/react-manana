import { Context } from '../../types';

export default {
  ticketOrigine: (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ticketOrigine({ id });
  },
  ticketOrigines: (_: any, __: any, ctx: Context) => {
    return ctx.prisma.ticketOrigines();
  },
};
