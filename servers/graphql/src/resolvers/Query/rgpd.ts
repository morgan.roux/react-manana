import { last } from 'lodash';
import moment from 'moment';
import { Context } from '../../types';

export default {
  rgpd: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.rgpd({ id });
  },
  rgpds: async (_: any, { idGroupement }: { idGroupement: string }, ctx: Context) => {
    if (idGroupement) {
      return ctx.prisma.rgpds({ where: { groupement: { id: idGroupement } } });
    }

    return ctx.prisma.rgpds();
  },
  checkAcceptRgpd: async (_: any, { idUser }: { idUser: string }, ctx: Context) => {
    const param = await ctx.prisma.parameter({ code: '0320' });
    const paramDefaultValue = param && param.defaultValue ? parseInt(param.defaultValue, 10) : 360;
    const paramValue = await ctx.prisma
      .parameterValues({
        where: { idParameter: { id: param && param.id } },
      })
      .then(results => {
        if (results && results.length > 0) {
          const res = results[0];
          return res.value ? parseInt(res.value, 10) : 0;
        }
      });

    const value = paramValue > 0 ? paramValue : paramDefaultValue;

    const historiques = await ctx.prisma.rgpdHistoriques({ where: { user: { id: idUser } } });
    if (historiques && historiques.length > 0) {
      const histo = last(historiques);
      const dateCreation = moment(histo.dateCreation);
      const now = moment();

      console.log('now :>> ', now.toString());
      console.log('dateCreation :>> ', dateCreation.toString());

      console.log('*************** now.diff(dateCreation) :>>', now.diff(dateCreation, 'days'));

      if (histo && now.diff(dateCreation, 'days') < value) {
        // S'il existe une hisorique (histo) et que la diff est inférieur à 6 mois
        // Il a déjà accepté le RGPD
        return true;
      }
    }

    return false;
  },
};
