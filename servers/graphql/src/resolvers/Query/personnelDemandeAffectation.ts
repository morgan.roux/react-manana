import { Context, PersonnelDemandeAffectationResult } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  personnelDemandeAffectations: async (
    _: any,
    { take, skip }: { take?: number; skip?: number },
    ctx: Context,
  ): Promise<PersonnelDemandeAffectationResult> => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser(
      { code: 'GPERSONNELDEMANDEAFFECTATIONS', nom: 'GET PERSONNELDEMANDEAFFECTATIONS' },
      ctx,
    );
    const total = await ctx.prisma
      .personnelDemandeAffectationsConnection()
      .aggregate()
      .count();
    const data = await ctx.prisma.personnelDemandeAffectations({ first: take, skip });
    return { total, data: data as any };
  },
  personnelDemandeAffectation: (_: any, { id }: { id: string }, ctx: Context) => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser(
      { code: 'GPERSONNELDEMANDEAFFECTATION', nom: 'GET PERSONNELDEMANDEAFFECTATION' },
      ctx,
    );
    return ctx.prisma.personnelDemandeAffectation({ id });
  },
};
