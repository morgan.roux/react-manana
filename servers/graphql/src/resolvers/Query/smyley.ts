import { Context } from '../../types';
import { userSmyleysByItemAndSource } from '../../repository/smyley';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  smyleys: async (_, { idGroupement }, ctx: Context) => {
    // return ctx.prisma.smyleys({ where: { idGroupement: { id: idGroupement } } });
    // A MODIFIER
    return ctx.prisma.smyleys();
  },
  userSmyleys: async (_, { codeItem, idSource }, ctx: Context) => {
    return userSmyleysByItemAndSource({ codeItem, idSource }, ctx);
  },
};
