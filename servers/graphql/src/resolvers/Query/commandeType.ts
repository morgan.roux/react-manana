import { Context } from '../../types';
import { WatchTime } from '../../services/time';

const watchTime = new WatchTime();

export default {
  commandeType: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.commandeType({ id }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** TYPE COMMANDE ** Le temps de récuperation type avec l'ID => ${id}  : `,
      );

      return value;
    });
  },
  commandeTypes: async (_, args, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.commandeTypes().then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** TYPE COMMANDE ** Le temps de récuperation de tous les types de commandes  : `,
      );

      return value;
    });
  },
};
