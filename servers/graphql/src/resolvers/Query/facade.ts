import { Context } from '../../types';

export default {
  facades: async (_, __, ctx: Context) => {
    return ctx.prisma.facades();
  },
};
