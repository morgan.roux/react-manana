import { Context } from '../../types';

export default {
  groupeClient: async (_, { id }, ctx: Context) => {
    return ctx.prisma.groupeClient({ id });
  },

  groupeClients: async (_, { }, ctx: Context) => {
    return ctx.prisma.groupeClients();
  },
};
