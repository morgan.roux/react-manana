import { Context } from '../../types';

export default {
  groupeAmis: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.groupeAmis({ id });
  },
  groupeAmises: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.groupeAmises({ where: { isRemoved } });
  },
};
