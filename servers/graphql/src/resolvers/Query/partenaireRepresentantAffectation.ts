import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  partenaireRepresentantAffectations: async (_: any, __: any, ctx: Context) => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser(
      {
        code: 'G_PARTENAIRE_REPRESENTANT_AFFECTATIONS',
        nom: 'GET PARTENAIRE_REPRESENTANT_AFFECTATIONS',
      },
      ctx,
    );
    return ctx.prisma.partenaireRepresentantAffectations();
  },
  partenaireRepresentantAffectation: (_: any, { id }: { id: string }, ctx: Context) => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser(
      {
        code: 'G_PARTENAIRE_REPRESENTANT_AFFECTATION',
        nom: 'GET PARTENAIRE_REPRESENTANT_AFFECTATION',
      },
      ctx,
    );
    return ctx.prisma.partenaireRepresentantAffectation({ id });
  },
};
