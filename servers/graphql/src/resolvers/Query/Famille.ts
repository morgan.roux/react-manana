import { Context } from '../../types';
import { Famille } from '../../generated/prisma-client';

export default {
  famille: async (_, { id }, ctx: Context) => {
    return ctx.prisma.famille({ id });
  },
  familles: async (_, __, ctx: Context) => {
    return (await ctx.prisma.familles()).reduce(
      async (promiseFamille: Promise<Famille[]>, famille: Famille): Promise<Famille[]> => {
        const previousFamille = await promiseFamille;
        if (famille.codeFamille.length === 1) return [...previousFamille, famille];
        return [...previousFamille];
      },
      Promise.resolve([]),
    );
  },
};
