import { Context } from '../../types';

export default {
  laboratoireRessource: async (_, { id }, ctx: Context) => {
    return ctx.prisma.laboratoireRessource({ id });
  },
};
