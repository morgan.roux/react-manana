import { Context, TitulaireAffectationResult } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  titulaireAffectations: async (
    _: any,
    { take, skip }: { take?: number; skip?: number },
    ctx: Context,
  ): Promise<TitulaireAffectationResult> => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser({ code: 'GTITULAIREAFFECTATIONS', nom: 'GET TITULAIREAFFECTATIONS' }, ctx);
    const total = await ctx.prisma
      .titulaireAffectationsConnection()
      .aggregate()
      .count();
    const data = await ctx.prisma.titulaireAffectations({ first: take, skip });
    return { total, data: data as any };
  },
  titulaireAffectation: (_: any, { id }: { id: string }, ctx: Context) => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser({ code: 'GTITULAIREAFFECTATION', nom: 'GET TITULAIREAFFECTATION' }, ctx);
    return ctx.prisma.titulaireAffectation({ id });
  },
};
