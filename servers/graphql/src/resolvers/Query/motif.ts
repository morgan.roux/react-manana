import { Context } from '../../types';

export default {
  motifs: async (_, { type }, ctx: Context) => {
    return ctx.prisma.motifs({ where: { typeMotif: type } });
  },
};
