import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  operation: async (_, { id }, ctx: Context) => {
    return ctx.prisma.operation({ id });
  },
  operations: async (_, __, ctx: Context) => {
    return ctx.prisma.operations();
  },
};
