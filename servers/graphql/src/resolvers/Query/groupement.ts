import { Context } from '../../types';

export default {
  groupements: async (_, __, ctx: Context) => {
    return ctx.prisma.groupements();
  },
  groupement: async (_, { id }, ctx: Context) => {
    return ctx.prisma.groupement({ id });
  },
};
