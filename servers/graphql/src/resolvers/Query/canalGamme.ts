import { Context } from '../../types';
import { WatchTime } from '../../services/time';

const watchTime = new WatchTime();

export default {
  canalGamme: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.canalGamme({ id }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** GAMME COMMERCIAL ** Le temps de récuperation du Gamme commercial avec l'ID => ${id} : `,
      );
      return value;
    });
  },
  canalGammes: async (_, __, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.canalGammes().then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** GAMME COMMERCIAL ** Le temps de récuperation de tous les gammes commercials : `,
      );

      return value;
    });
  },
};
