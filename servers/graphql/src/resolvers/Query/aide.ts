import { Context } from '../../types';

export default {
  aide: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.aide({ id });
  },
  aides: async (_: any, __: any, ctx: Context) => {
    return ctx.prisma.aides();
  },
};
