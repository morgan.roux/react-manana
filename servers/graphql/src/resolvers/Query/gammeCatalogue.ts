import { Context } from '../../types';

export default {
  gammeCatalogue: async (_, { id }, ctx: Context) => {
    return ctx.prisma.gammeCatalogue({ id });
  },
  gammeCatalogues: async (_, __, ctx: Context) => {
    return ctx.prisma.gammeCatalogues();
  },
};
