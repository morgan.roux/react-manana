import { Context } from '../../types';
import { WatchTime } from '../../services/time';

const watchTime = new WatchTime();

export default {
  commandeCanal: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.commandeCanal({ id }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** CANAL COMMANDE ** Le temps de récuperation d'un canal avec l'ID => ${id}  : `,
      );

      return value;
    });
  },
  commandeCanals: async (_, args, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.commandeCanals().then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** CANAL COMMANDE ** Le temps de récuperation de tous les canal de commandes  : `,
      );

      return value;
    });
  },
};
