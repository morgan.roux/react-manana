import { Context } from '../../types';

export default {
  todoActionAttribution: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.todoActionAttribution({ id });
  },
  todoActionAttributions: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.todoActionAttributions({ where: { isRemoved } });
  },
};
