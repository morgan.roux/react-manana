import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  service: async (_, { id }, ctx: Context) => {
    return ctx.prisma.service({ id }).then(value => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */
      if (ctx && ctx.userId) {
        createActiviteUser(
          {
            code: 'GSERVICE',
            nom: 'GET SERVICE',
          },
          ctx,
        );
      }

      return value;
    });
  },
  services: async (_, __, ctx: Context) => {
    return ctx.prisma.services();
  },
};
