import { Context } from '../../types';
import { WatchTime } from '../../services/time';

const watchTime = new WatchTime();

export default {
  produitCanals: async (_, __, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.produitCanals().then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** PRODUIT CANALS ** Le temps de récuperation de toutes les produits : `,
      );
      return value;
    });
  },
  produitCanal: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.produitCanal({ id }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** PRODUIT CANAL ** Le temps de récuperation du produit produit avec l'ID = ${id} : `,
      );
      return value;
    });
  },
};
