import { Context } from '../../types';

export default {
  ticketFichierJoint: (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ticketFichierJoint({ id });
  },
  ticketFichierJoints: (_: any, __: any, ctx: Context) => {
    return ctx.prisma.ticketFichierJoints();
  },
};
