import { Context, PersonnelFonctionResult } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  personnelFonctions: async (
    _: any,
    { take, skip }: { take?: number; skip?: number },
    ctx: Context,
  ): Promise<PersonnelFonctionResult> => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser({ code: 'GPERSONNELFONCTIONS', nom: 'GET PERSONNELFONCTIONS' }, ctx);
    const total = await ctx.prisma
      .personnelFonctionsConnection()
      .aggregate()
      .count();
    const data = await ctx.prisma.personnelFonctions({ first: take, skip });
    return { total, data: data as any };
  },
  personnelFonction: (_: any, { id }: { id: string }, ctx: Context) => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser({ code: 'GPERSONNELFONCTION', nom: 'GET PERSONNELFONCTION' }, ctx);
    return ctx.prisma.personnelFonction({ id });
  },
};
