import { Context } from '../../types';

export default {
  ideeOuBonnePratiqueSmyley: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiqueSmyley({ id });
  },
  ideeOuBonnePratiqueSmyleys: async (
    _: any,
    { isRemoved }: { isRemoved: boolean },
    ctx: Context,
  ) => {
    return ctx.prisma.ideeOuBonnePratiqueSmyleys({ where: { isRemoved } });
  },
};
