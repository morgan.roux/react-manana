import { Context } from '../../types';

export default {
  ticketChangementStatut: (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ticketChangementStatut({ id });
  },
  ticketChangementStatuts: (_: any, __: any, ctx: Context) => {
    return ctx.prisma.ticketChangementStatuts();
  },
};
