import { Context } from '../../types';
import {
  checkIdItemAssocies,
  feedbackAchieved,
  itemSourceAssocies,
  piloteFeedback,
  feedbackObjectifRealise,
  orderByFeedback,
  orderByItem,
  feedbackAchievedInYear,
} from '../../repository/statistiqueFeedback';
import ArrayUnique = require('array-unique');
import { pharmacieIds } from '../../repository/pharmacie';

import { WatchTime } from '../../services/time';
import { orderByPharmacie } from '../../repository/statistiqueBusiness';

const watchTime = new WatchTime();

export default {
  indicateurStatistiqueFeedback: async (
    _: any,
    { codeItem, pilotage, idItemAssocies, idPharmacies },
    ctx: Context,
  ) => {
    const filter = { first: null, skip: null };

    const idAssocies = await checkIdItemAssocies(
      ctx,
      String(codeItem),
      idItemAssocies && idItemAssocies.length ? idItemAssocies.map((id: any) => String(id)) : [],
      filter,
    );

    const uniqueidAssocies: any[] = ArrayUnique(idAssocies);
    const uniquePharmacieIds: any[] = ArrayUnique(
      await pharmacieIds(
        ctx,
        idPharmacies && idPharmacies.length ? idPharmacies.map((id: any) => String(id)) : [],
        filter,
      ),
    );

    console.log(
      '[GET STATISTIQUE INDICATION ITEM PHARMACIE]',
      uniqueidAssocies,
      uniquePharmacieIds,
    );

    if (!uniqueidAssocies.length || !uniquePharmacieIds.length) return null;

    const statistiques = await feedbackAchieved(
      ctx,
      pilotage,
      codeItem,
      uniqueidAssocies.map((id: any) => String(id)),
      uniquePharmacieIds.map(id => String(id)),
    );
    return {
      itemAssocies: await itemSourceAssocies(ctx, codeItem, uniqueidAssocies, {
        skip: null,
        first: null,
      }),
      pharmacies: await ctx.prisma.pharmacies({
        where: { id_in: uniquePharmacieIds.map(id => String(id)) },
      }),
      ...statistiques,
    };
  },
  pharmacieItemAssocieStatistiqueFeedback: async (
    _: any,
    { codeItem, pilotage, idItemAssocies, idPharmacies, orderBy, isAsc, skip, take }: any,
    ctx: Context,
  ) => {
    const filter = { first: null, skip: null };
    const toSkip = skip ? skip : 0;
    const toTake = take ? take + skip : 5;

    let order = pilotage === 'ITEM' ? 'libelle' : 'nom';
    order = orderBy ? orderBy : order;

    /* check if item exist */
    const idAssocies = await checkIdItemAssocies(
      ctx,
      String(codeItem),
      idItemAssocies && idItemAssocies.length ? idItemAssocies.map((id: any) => String(id)) : [],
      filter,
    );

    const uniqueidAssocies: string[] = ArrayUnique(idAssocies);
    /* check if pharmacie exist */
    const uniquePharmacieIds: string[] = ArrayUnique(
      await pharmacieIds(
        ctx,
        idPharmacies && idPharmacies.length ? idPharmacies.map((id: any) => String(id)) : [],
        filter,
      ),
    );

    console.log('[GET STATISTIQUE ITEM PHARMACIE]', uniqueidAssocies, uniquePharmacieIds);

    if (!uniqueidAssocies.length || !uniquePharmacieIds.length) return null;

    const result = await Promise.all(
      uniqueidAssocies.map(id =>
        piloteFeedback(ctx, pilotage, codeItem, {
          idItemAssocies: pilotage === 'ITEM' ? [id] : uniqueidAssocies,
          idPharmacies: pilotage === 'ITEM' ? uniquePharmacieIds : [id],
        }),
      ),
    );

    console.log('after ==> ', order, isAsc);

    if (orderByFeedback.includes(order)) {
      result.sort((a, b) => (isAsc ? a[order] - b[order] : b[order] - a[order]));
    } else if (orderByPharmacie.includes(order)) {
      result.sort((a, b) =>
        isAsc
          ? a.pharmacie[order].localeCompare(b.pharmacie[order])
          : b.pharmacie[order].localeCompare(a.pharmacie[order]),
      );
    } else if (orderByItem.includes(order)) {
      result.sort((a, b) =>
        isAsc
          ? a.itemAssocie[order].localeCompare(b.itemAssocie[order])
          : b.itemAssocie[order].localeCompare(a.itemAssocie[order]),
      );
    }

    return {
      total:
        pilotage === 'ITEM'
          ? (await checkIdItemAssocies(ctx, codeItem, idItemAssocies, filter)).length
          : (await pharmacieIds(ctx, idPharmacies, filter)).length,
      data: result.slice(toSkip, toTake),
    };
  },
  statistiqueFeedback: async (
    _: any,
    { codeItem, pilotage, idItemAssocies, idPharmacies, indicateur },
    ctx: Context,
  ) => {
    if (
      !idPharmacies ||
      (idPharmacies && !idPharmacies.length) ||
      !idItemAssocies ||
      (idItemAssocies && !idItemAssocies.length)
    ) {
      return null;
    }

    return feedbackObjectifRealise(ctx, pilotage, codeItem, indicateur, {
      idItemAssocies: idItemAssocies.map((id: any) => String(id)),
      idPharmacies: idPharmacies.map((id: any) => String(id)),
    });
  },
  feedbackAchievedInYear: async (
    _,
    { codeItem, pilotage, year, idItemAssocies, idPharmacies, indicateur },
    ctx: Context,
  ) => {
    if (
      !idItemAssocies ||
      (idItemAssocies && !idItemAssocies.length) ||
      !idPharmacies ||
      (idPharmacies && !idPharmacies.length)
    ) {
      return null;
    }

    return feedbackAchievedInYear(
      ctx,
      year,
      codeItem,
      pilotage,
      indicateur,
      idItemAssocies.map((id: any) => String(id)),
      idPharmacies.map((id: any) => String(id)),
    );
  },
};
