import { Context } from '../../types';
import { idsActualiteDateNOTActive } from '../../repository/actualite';

export default {
  notification: (_, { id }, ctx: Context) => {
    return ctx.prisma.notification({ id });
  },
  notifications: async (
    _,
    { first, skip, orderBy, dateFilterStart, dateFilterEnd },
    ctx: Context,
  ) => {
    const actuIds = await idsActualiteDateNOTActive(ctx);

    const targetItems = [...actuIds];

    console.log('[IDS ACTU INACTIVE] ==>', actuIds);

    const notifications = await ctx.prisma.notifications({
      where: {
        to: { id: ctx.userId },
        dateCreation_gte: dateFilterStart,
        dateCreation_lte: dateFilterEnd,
        targetId_not_in: targetItems,
      },
      first,
      skip,
      orderBy: orderBy || 'dateCreation_DESC',
    });

    const totalCount = await ctx.prisma
      .notificationsConnection()
      .aggregate()
      .count();

    const notSeenCount = await ctx.prisma
      .notifications({ where: { to: { id: ctx.userId }, targetId_not_in: targetItems } })
      .then(notifications => notifications.filter(item => item.seen === false).length);

    return {
      total: totalCount,
      notSeen: notSeenCount,
      data: notifications,
    };
  },
};
