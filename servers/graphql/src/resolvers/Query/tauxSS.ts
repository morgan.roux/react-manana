import { Context } from '../../types';

export default {
  tauxss: async (_, { id }, ctx: Context) => {
    return ctx.prisma.tauxSS({ id });
  },
  tauxsses: async (_, __, ctx: Context) => {
    return ctx.prisma.tauxsses();
  },
};
