import { Context } from '../../types';
import { ckeckUserIpAndSendMail, validateUserIp } from '../../repository/connexion';

export default {
  checkUserIp: async (_, { ip }, ctx: Context) => {
    return ckeckUserIpAndSendMail(ctx, { ip, idUser: ctx.userId });
  },
  validateUserIp: async (_, { token, status }, ctx: Context) => {
    return validateUserIp(ctx, { token, status });
  },
};
