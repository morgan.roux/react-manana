import { Context } from '../../types';

export default {
  ideeOuBonnePratique: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratique({ id });
  },
  ideeOuBonnePratiques: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiques({ where: { isRemoved } });
  },
};
