import { Context } from '../../types';

export default {
  ideeOuBonnePratiqueFichierJoint: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ideeOuBonnePratiqueFichierJoint({ id });
  },
  ideeOuBonnePratiqueFichierJoints: async (
    _: any,
    { isRemoved }: { isRemoved: boolean },
    ctx: Context,
  ) => {
    return ctx.prisma.ideeOuBonnePratiqueFichierJoints({ where: { isRemoved } });
  },
};
