import moment from 'moment';
import { Context, InformationLiaisonFilter } from '../../types';



const countInformationLiaisonByStatus = async (ctx: Context,
  input: InformationLiaisonFilter, statut: string): Promise<number> => {
  const { dateDebut, dateFin, idsCollegueConcernee, idPharmacie, idsDeclarant } = input;
  const must: any = [
    {
      term: {
        isRemoved: false,
      },
    },
    {
      term: {
        'pharmacie.id': idPharmacie,
      },
    },
  ];

  if (dateDebut && dateFin) {
    must.push({
      range: { dateCreation: { gte: moment(dateDebut), lte: moment(dateFin) } },
    });
  }

  const should = idsDeclarant
    ? [
      {
        terms: {
          coupleStatutDeclarant: idsDeclarant.map(id => `${id}-${statut}`),
        },
      }
    ]
    : idsCollegueConcernee
      ? [
        {
          terms: {
            'colleguesConcernees.coupleStatutUserConcernee': idsCollegueConcernee.map(
              id => `${id}-${statut}`,
            ),
          },
        }
      ]
      : [];


  const searchParams = {
    body: {
      query: {
        bool: {
          must: must,
          should: should?.length > 0 ? should : undefined,
          minimum_should_match: should?.length > 0 ? 1 : undefined,
        },
      },
    },
    index: ['informationliaison'],
  };


  return ctx.indexing
    .getElasticSearchClient()
    .count(searchParams)
    .then(result => {
      return result.body.count;
    })
}

export default {
  informationLiaison: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.informationLiaison({ id });
  },
  informationLiaisons: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.informationLiaisons({ where: { isRemoved } });
  },
  informationLiaisonUserConcernee: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.informationLiaisonUserConcernee({ id });
  },
  informationLiaisonUserConcernees: async (
    _: any,
    { isRemoved }: { isRemoved: boolean },
    ctx: Context,
  ) => {
    return ctx.prisma.informationLiaisonUserConcernees({ where: { isRemoved } });
  },
  countInformationLiaisons: async (
    _: any,
    { input }: { input: InformationLiaisonFilter },
    ctx: Context,
  ) => {

    const collegueStatuts = ['NON_LUES', 'LUES', 'CLOTURE'];
    const declarantStatuts = ['EN_COURS', 'CLOTURE']

    return {
      statuts:
        (input.idsCollegueConcernee ?
          collegueStatuts
          : input.idsDeclarant
            ? declarantStatuts
            : []).map(statut => ({ statut, count: countInformationLiaisonByStatus(ctx, input, statut) })),

      urgences: [],
      importances: [],
    };
  },
};
