import { Context } from '../../types';

export default {
  contact: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.contact({ id });
  },
};
