import { Context } from '../../types';

export default {
  todoProjetFavoris: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.todoProjetFavoris({ id });
  },
  todoProjetFavorises: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.todoProjetFavorises({ where: { isRemoved } });
  },
};
