import { Context } from '../../types';

export default {
  groupeAmisDetail: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.groupeAmisDetail({ id });
  },
  groupeAmisDetails: async (_: any, {}, ctx: Context) => {
    return ctx.prisma.groupeAmisDetails();
  },
};
