import { Context } from '../../types';

export default {
  partagePharmacieCible: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.partagePharmacieCible({ id });
  },
  partagePharmacieCibles: async (_: any, {}, ctx: Context) => {
    return ctx.prisma.partagePharmacieCibles();
  },
};
