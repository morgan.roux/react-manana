import { Context, TitulaireFonctionResult } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  titulaireFonctions: async (
    _: any,
    { take, skip }: { take?: number; skip?: number },
    ctx: Context,
  ): Promise<TitulaireFonctionResult> => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser({ code: 'GTITULAIREFONCTIONS', nom: 'GET TITULAIREFONCTIONS' }, ctx);
    const total = await ctx.prisma
      .titulaireFonctionsConnection()
      .aggregate()
      .count();
    const data = await ctx.prisma.titulaireFonctions({ first: take, skip });
    return { total, data: data as any };
  },
  titulaireFonction: (_: any, { id }: { id: string }, ctx: Context) => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser({ code: 'GTITULAIREFONCTION', nom: 'GET TITULAIREFONCTION' }, ctx);
    return ctx.prisma.titulaireFonction({ id });
  },
};
