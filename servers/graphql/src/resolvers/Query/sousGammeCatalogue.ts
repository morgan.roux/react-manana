import { Context } from '../../types';

export default {
  sousGammeCatalogue: async (_, { id }, ctx: Context) => {
    return ctx.prisma.sousGammeCatalogue({ id });
  },
};
