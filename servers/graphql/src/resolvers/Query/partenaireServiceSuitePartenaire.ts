import { Context } from '../../types';

export default {
  partenaireServicePartenaire: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.partenaireServicePartenaire({ id });
  },
  partenaireServicePartenaires: async (_: any, __: any, ctx: Context) => {
    return ctx.prisma.partenaireServicePartenaires();
  },
};
