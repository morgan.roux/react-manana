import { Context } from '../../types';
import {
  pharmaciesOperationsBusiness,
  businessOperationObjectifRealise,
  piloteBusiness,
  orderByBusiness,
  orderByPharmacie,
  orderByOpeartion,
  businessAchievedInYear,
} from '../../repository/statistiqueBusiness';
import ArrayUnique = require('array-unique');
import { pharmacieIds } from '../../repository/pharmacie';
import { operationIds } from '../../repository/operation';

export default {
  indicateurStatistiqueBusiness: async (
    _,
    { pilotage, idOperations, idPharmacies, idPharmaciesSelected },
    ctx: Context,
  ) => {
    if (
      !idOperations ||
      (idOperations && !idOperations.length) ||
      !idPharmacies ||
      (idPharmacies && !idPharmacies.length)
    ) {
      return null;
    }

    return pharmaciesOperationsBusiness(ctx, pilotage, {
      idsOperation: idOperations.map((id: any) => String(id)),
      idsPharmacie: idPharmacies.map((id: any) => String(id)),
      idPharmaciesSelected:
        idPharmaciesSelected && idPharmaciesSelected.length
          ? idPharmaciesSelected.map((id: any) => String(id))
          : [],
      take: null,
      skip: null,
    });
  },
  pharmacieOperationStatistiqueBusiness: async (
    _,
    { pilotage, idOperations, idPharmacies, orderBy, isAsc, skip, take },
    ctx: Context,
  ) => {
    const filter = { first: null, skip: null };
    const toSkip = skip ? skip : 0;
    const toTake = take ? take + skip : 5;

    let order = pilotage === 'OPERATION' ? 'libelle' : 'nom';
    order = orderBy ? orderBy : order;

    const uniqueOperationIds: any[] =
      idOperations && idOperations.length
        ? ArrayUnique(await operationIds(ctx, idOperations, filter))
        : [];
    const uniquePharmacieIds: any[] =
      idPharmacies && idPharmacies.length
        ? ArrayUnique(await pharmacieIds(ctx, idPharmacies, filter))
        : [];

    if (!uniqueOperationIds.length || !uniquePharmacieIds.length) return null;

    const result = await Promise.all(
      pilotage === 'OPERATION'
        ? uniqueOperationIds.map(id =>
            piloteBusiness(ctx, pilotage, {
              idsOperation: [id],
              idsPharmacie: idPharmacies.map(id => String(id)),
              idPharmaciesSelected: uniquePharmacieIds,
            }),
          )
        : uniquePharmacieIds.map(id =>
            piloteBusiness(ctx, pilotage, {
              idsOperation: idOperations.map(id => String(id)),
              idsPharmacie: [id],
              idPharmaciesSelected: uniquePharmacieIds,
            }),
          ),
    );

    console.log('after ==> ', order, isAsc);
    if (order === 'canal') {
      result.sort((a, b) =>
        isAsc ? a.canal.code.localeCompare(b.canal.code) : b.canal.code.localeCompare(a.canal.code),
      );
    } else if (orderByBusiness.includes(order)) {
      result.sort((a, b) => (isAsc ? a[order] - b[order] : b[order] - a[order]));
    } else if (orderByPharmacie.includes(order)) {
      result.sort((a, b) =>
        isAsc
          ? a.pharmacie[order].localeCompare(b.pharmacie[order])
          : b.pharmacie[order].localeCompare(a.pharmacie[order]),
      );
    } else if (orderByOpeartion.includes(order)) {
      result.sort((a, b) =>
        isAsc
          ? a.operation[order].localeCompare(b.operation[order])
          : b.operation[order].localeCompare(a.operation[order]),
      );
    }

    return {
      total:
        pilotage === 'OPERATION'
          ? (await operationIds(ctx, idOperations, filter)).length
          : (await pharmacieIds(ctx, idPharmacies, filter)).length,
      data: result.slice(toSkip, toTake),
    };
  },
  statistiqueBusiness: async (
    _,
    { pilotage, idOperations, idPharmacies, idPharmaciesSelected, indicateur },
    ctx: Context,
  ) => {
    if (
      !idPharmacies ||
      (idPharmacies && !idPharmacies.length) ||
      !idOperations ||
      (idOperations && !idOperations.length)
    ) {
      return null;
    }

    return businessOperationObjectifRealise(
      ctx,
      pilotage,
      indicateur,
      idOperations.map((id: any) => String(id)),
      idPharmacies.map((id: any) => String(id)),
      idPharmaciesSelected && idPharmaciesSelected.length
        ? idPharmaciesSelected.map((id: any) => String(id))
        : [],
    );
  },
  businessAchievedInYear: async (
    _,
    { pilotage, year, indicateur, idOperations, idPharmacies, idPharmaciesSelected },
    ctx: Context,
  ) => {
    if (
      !idOperations ||
      (idOperations && !idOperations.length) ||
      !idPharmacies ||
      (idPharmacies && !idPharmacies.length)
    ) {
      return null;
    }

    return businessAchievedInYear(
      ctx,
      year,
      pilotage,
      indicateur,
      idOperations.map((id: any) => String(id)),
      idPharmacies.map((id: any) => String(id)),
      idPharmaciesSelected && idPharmaciesSelected.length
        ? idPharmaciesSelected.map((id: any) => String(id))
        : [],
    );
  },
};
