import { Context } from '../../types';

export default {
  serviceType: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.serviceType({ id });
  },
  serviceTypes: async (_: any, __: any, ctx: Context) => {
    return ctx.prisma.serviceTypes();
  },
};
