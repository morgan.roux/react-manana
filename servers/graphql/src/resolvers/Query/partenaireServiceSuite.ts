import { Context } from '../../types';

export default {
  partenaireServiceSuite: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.partenaireServiceSuite({ id });
  },
  partenaireServiceSuites: async (_: any, __: any, ctx: Context) => {
    return ctx.prisma.partenaireServiceSuites();
  },
};
