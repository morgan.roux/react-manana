import { Context } from '../../types';

export default {
  rgpdAccueilPlus: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.rgpdAccueilPlus({ id });
  },
  rgpdAccueilPluses: async (_: any, { idGroupement }: { idGroupement: string }, ctx: Context) => {
    return ctx.prisma.rgpdAccueilPluses({ where: { groupement: { id: idGroupement } } });
  },
};
