import { Context } from '../../types';
import { ActivedirectoryGroupement } from '../../repository/activeDirectory';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  activedirectoryGroupement: (_, _args, ctx: Context) => {
    return ActivedirectoryGroupement(ctx, { ..._args });
  },
  activedirectories: (_, __, ctx: Context) => {
    return ctx.prisma.activeDirectoryCredentials().then(value => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'GACTIONS',
          nom: 'GET ACTIONS',
        },
        ctx,
      );

      return value;
    });
  },
};
