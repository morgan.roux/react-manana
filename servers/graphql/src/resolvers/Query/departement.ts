import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  departements: async (_, __, ctx: Context) => {
    return ctx.prisma.departements();
  },
};
