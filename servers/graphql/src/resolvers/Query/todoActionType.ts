import { Context } from '../../types';

export default {
  todoActionType: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.todoActionType({ id });
  },
  todoActionTypes: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.todoActionTypes({ where: { isRemoved } });
  },
};
