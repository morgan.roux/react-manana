import { Context } from '../../types';
import { WatchTime } from '../../services/time';
import { produitHistorique } from '../../repository/produitHistorique';

const watchTime = new WatchTime();

export default {
  produit: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.produit({ id }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(`** PRODUIT ** Le temps de récuperation de toutes les produits : `);
      return value;
    });
  },
  produitCategories: async (_, __, ctx: Context) => {
    return ctx.prisma.produitCategories();
  },
  produitTypeCodes: async (_, __, ctx: Context) => {
    return ctx.prisma.produitTypeCodes();
  },
  produitHistorique: async (
    _,
    { idProduit, idPharmacie, year }: { idProduit: string; idPharmacie: string; year: number },
    ctx: Context,
  ) => {
    return produitHistorique(ctx, { idProduit, idPharmacie, year });
  },
};
