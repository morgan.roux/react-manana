import { Context } from '../../types';

export default {
  todoActionOrigine: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.todoActionOrigine({ id });
  },
  todoActionOrigines: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.todoActionOrigines({ where: { isRemoved } });
  },
};
