import { Context } from '../../types';

export default {
  urgence: async (_, { id }, ctx: Context) => {
    return ctx.prisma.urgence({ id });
  },
  urgences: async (_, {}, ctx: Context) => {
    return ctx.prisma.urgences({ where: { isRemoved: false } });
  },
};
