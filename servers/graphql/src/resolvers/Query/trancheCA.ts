import { Context } from '../../types';

export default {
  trancheCAs: async (_, { id }, ctx: Context) => {
    return ctx.prisma.trancheCAs();
  },
};
