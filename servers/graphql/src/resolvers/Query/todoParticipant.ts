import { Context } from '../../types';

export default {
  todoParticipant: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.todoParticipant({ id });
  },
  todoParticipants: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.todoParticipants({ where: { isRemoved } });
  },
};
