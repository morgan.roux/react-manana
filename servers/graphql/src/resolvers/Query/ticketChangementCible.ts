import { Context } from '../../types';

export default {
  ticketChangementCible: (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.ticketChangementCible({ id });
  },
  ticketChangementCibles: (_: any, __: any, ctx: Context) => {
    return ctx.prisma.ticketChangementCibles();
  },
};
