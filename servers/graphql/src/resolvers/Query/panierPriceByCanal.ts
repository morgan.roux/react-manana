import { Context } from '../../types';
import { panierPriceByCanal } from '../../repository/panier';
import { WatchTime } from '../../services/time';

const watchTime = new WatchTime();

export default {
  panierPriceByCanal: async (_, { idPanier, codeCanalCommande }, ctx: Context) => {
    watchTime.start();
    return panierPriceByCanal(ctx, { id: idPanier, codeCanalCommande }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(
        `** PANIER CANAL PRIX ** Le temps de récuperation de la total du prix du panier  : `,
      );
      return value;
    });
  },
};
