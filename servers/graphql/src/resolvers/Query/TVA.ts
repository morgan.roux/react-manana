import { Context } from '../../types';

export default {
  tva: async (_, { id }, ctx: Context) => {
    return ctx.prisma.tVA({ id });
  },
  tvas: async (_, __, ctx: Context) => {
    return ctx.prisma.tVAs();
  },
};
