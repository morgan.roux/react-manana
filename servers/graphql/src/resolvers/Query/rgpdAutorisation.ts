import { Context } from '../../types';

export default {
  rgpdAutorisation: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.rgpdAutorisation({ id });
  },
  rgpdAutorisations: async (_: any, { idGroupement }: { idGroupement: string }, ctx: Context) => {
    return ctx.prisma.rgpdAutorisations({ where: { groupement: { id: idGroupement } } });
  },
};
