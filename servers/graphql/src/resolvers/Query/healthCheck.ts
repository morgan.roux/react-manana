import { Context } from '../../types';
import { prisma } from '../../generated/prisma-client';
import S3 from '../../services/s3';

export default {
  healthCheck: async (_, __, ctx: Context) => {
    const result = {
      postgresDb: { status: false, usersContains: 0 },
      serviceS3: { status: false, host: '' },
      error: true,
    };
    // db check connexion db
    await prisma.users().then(results => {
      result.postgresDb.status = !!results;
      result.postgresDb.usersContains = results ? results.length : 0;
    });
    // s3 check connexion
    const connectedS3 = await S3.getS3Object().endpoint.host;
    result.serviceS3.status = !!connectedS3;
    result.serviceS3.host = connectedS3 ? connectedS3 : '';

    // error
    result.error = !result.postgresDb.status || !result.serviceS3.status;
    return result;
  },
};
