import { Context } from '../../types';
import { sousItems } from '../../repository/item';

export default {
  item: async (_, { id }, ctx: Context) => {
    return ctx.prisma.item({ id });
  },
  items: async (_, {}, ctx: Context) => {
    return ctx.prisma.items();
  },
  sousItems: async (_, { codeItemItem }, ctx: Context) => {
    return sousItems(ctx, { codeItem: codeItemItem });
  },
  sousItemsByCode: async (_, { code }, ctx: Context) => {
    const item = await ctx.prisma.item({ code });

    if (!item) return sousItems(ctx, { codeItem: null });

    return sousItems(ctx, { codeItem: item.codeItem });
  },
};
