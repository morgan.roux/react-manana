import { Context } from '../../types';

export default {
  prestatairePharmacies: async (_, { type }, ctx: Context) => {
    return ctx.prisma.prestatairePharmacies({ where: { typePrestataire: type } });
  },
};
