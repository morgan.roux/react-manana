import { Context } from '../../types';

export default {
  todoSection: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.todoSection({ id });
  },
  todoSections: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.todoSections({ where: { isRemoved } });
  },
};
