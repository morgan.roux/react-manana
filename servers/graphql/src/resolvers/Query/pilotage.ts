import { pilotageComments } from '../../repository/comment';
import { WatchTime } from '../../services/time';
import { Context } from '../../types';

const watchTime = new WatchTime();

export default {
  pilotageComments: (_, { codeItem, idItemAssocie, idsPharmacies, take, skip }, ctx: Context) => {
    watchTime.start();
    return pilotageComments({ codeItem, idItemAssocie, idsPharmacies, take, skip }, ctx).then(
      data => {
        watchTime.stop();
        watchTime.printElapsed(
          `** COMMENT ** Le temps de récuperation de toutes les Commentaires de pilotage : `,
        );

        return data;
      },
    );
  },
};
