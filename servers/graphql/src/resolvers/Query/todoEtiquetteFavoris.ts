import { Context } from '../../types';

export default {
  todoEtiquetteFavoris: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.todoEtiquetteFavoris({ id });
  },
  todoEtiquetteFavorises: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.todoEtiquetteFavorises({ where: { isRemoved } });
  },
};
