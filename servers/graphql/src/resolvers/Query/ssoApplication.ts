import { Context } from '../../types';
import {
  ApplicationsGroup,
  SsoApplication,
  SamlLogin,
  CryptoMd5,
  CryptoAes256,
  CryptoOther,
  ApplicationsRoleFunction,
  TokenAuthentification,
  TokenFtpAuthentification,
  ApplicationsRoles,
  CryptoMd5s,
} from '../../repository/ssoApplication';

export default {
  applicationsGroup: async (_, { idgroupement }, ctx: Context) => {
    return ApplicationsGroup(ctx, { idgroupement });
  },
  applicationsRole: async (_, { idgroupement, coderole }, ctx: Context) => {
    return ApplicationsRoleFunction(ctx, { idgroupement, coderole });
  },
  applicationsRoles: async (_, { idgroupement }, ctx: Context) => {
    return ApplicationsRoles(ctx, { idgroupement });
  },
  ssoApplication: async (_, { id }, ctx: Context) => {
    return SsoApplication(ctx, { id });
  },
  samlLogin: async (_, { id }, ctx: Context) => {
    return SamlLogin(ctx, { id });
  },
  cryptoMd5: async (_, { id }, ctx: Context) => {
    return CryptoMd5(ctx, { id });
  },
  cryptoAes256: async (_, { id }, ctx: Context) => {
    return CryptoAes256(ctx, { id });
  },
  cryptoOther: async (_, { id }, ctx: Context) => {
    return CryptoOther(ctx, { id });
  },
  tokenAuthentification: async (_, { id }, ctx: Context) => {
    return TokenAuthentification(ctx, { id });
  },
  tokenFtpAuthentification: async (_, { id }, ctx: Context) => {
    return TokenFtpAuthentification(ctx, { id });
  },
  cryptoMd5s: async (_, _args, ctx: Context) => {
    return CryptoMd5s(ctx, { ..._args });
  },
};
