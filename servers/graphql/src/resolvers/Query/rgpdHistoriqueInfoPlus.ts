import { Context } from '../../types';

export default {
  rgpdHistoriqueInfoPlus: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.rgpdHistoriqueInfoPlus({ id });
  },
  rgpdHistoriqueInfoPluses: async (
    _: any,
    { idGroupement }: { idGroupement: string },
    ctx: Context,
  ) => {
    return ctx.prisma.rgpdHistoriqueInfoPluses({ where: { groupement: { id: idGroupement } } });
  },
};
