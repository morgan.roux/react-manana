import { Context, PersonnelAffectationResult } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  personnelAffectations: async (
    _: any,
    { take, skip }: { take?: number; skip?: number },
    ctx: Context,
  ): Promise<PersonnelAffectationResult> => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser({ code: 'GPERSONNELAFFECTATIONS', nom: 'GET PERSONNELAFFECTATIONS' }, ctx);
    const total = await ctx.prisma
      .personnelAffectationsConnection()
      .aggregate()
      .count();
    const data = await ctx.prisma.personnelAffectations({ first: take, skip });
    return { total, data: data as any };
  },
  personnelAffectation: (_: any, { id }: { id: string }, ctx: Context) => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser({ code: 'GPERSONNELAFFECTATION', nom: 'GET PERSONNELAFFECTATION' }, ctx);
    return ctx.prisma.personnelAffectation({ id });
  },
};
