import { Context } from '../../types';
import { actualiteOriginesByRole } from '../../repository/actualite';
import { WatchTime } from '../../services/time';

const watchTime = new WatchTime();

export default {
  actualiteOrigine: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.actualiteOrigine({ id }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(`[ActualiteOrigine] query actualiteOrigine duration : `);
      return value;
    });
  },
  actualiteOrigines: async (_, __, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.actualiteOrigines({ orderBy: 'ordre_ASC' }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(`[ActualiteOrigine] query actualiteOrigines duration : `);
      return value;
    });
  },
  actualiteOriginesByRole: async (_, { codeRole }, ctx: Context) => {
    watchTime.start();
    return actualiteOriginesByRole({ codeRole }, ctx).then(values => {
      watchTime.stop();
      watchTime.printElapsed(`[ActualiteOrigine] query actualiteOriginesByRole duration : `);
      return values;
    });
  },
};
