import { Context } from '../../types';

export default {
  laboratoireRepresentant: async (_, { id }, ctx: Context) => {
    return ctx.prisma.laboratoireRepresentant({ id });
  },
};
