import { Context } from '../../types';

export default {
  promotion: async (_, { id }, ctx: Context) => {
    return ctx.prisma.promotion({ id });
  },
};
