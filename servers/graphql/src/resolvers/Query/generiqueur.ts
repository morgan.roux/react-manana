import { Context } from '../../types';

export default {
  generiqueurs: async (_, __, ctx: Context) => {
    return ctx.prisma.generiqueurs();
  },
};
