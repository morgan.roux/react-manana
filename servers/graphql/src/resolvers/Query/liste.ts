import { Context } from '../../types';

export default {
  liste: async (_, { id }, ctx: Context) => {
    return ctx.prisma.liste({ id });
  },
  listes: async (_, __, ctx: Context) => {
    return ctx.prisma.listes();
  },
};
