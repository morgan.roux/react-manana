import { Context } from '../../types';

export default {
  libelleDivers: async (_, { id }, ctx: Context) => {
    return ctx.prisma.libelleDivers({ id });
  },
  libelleDiverses: async (_, { code }, ctx: Context) => {
    return ctx.prisma.libelleDiverses({ where: { code } });
  },
};
