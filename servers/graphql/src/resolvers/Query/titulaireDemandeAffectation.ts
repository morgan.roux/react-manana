import { Context, TitulaireDemandeAffectationResult } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  titulaireDemandeAffectations: async (
    _: any,
    { take, skip }: { take?: number; skip?: number },
    ctx: Context,
  ): Promise<TitulaireDemandeAffectationResult> => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser(
      { code: 'GTITULAIREDEMANDEAFFECTATIONS', nom: 'GET TITULAIREDEMANDEAFFECTATIONS' },
      ctx,
    );
    const total = await ctx.prisma
      .titulaireDemandeAffectationsConnection()
      .aggregate()
      .count();
    const data = await ctx.prisma.titulaireDemandeAffectations({ first: take, skip });
    return { total, data: data as any };
  },
  titulaireDemandeAffectation: (_: any, { id }: { id: string }, ctx: Context) => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser(
      { code: 'GTITULAIREDEMANDEAFFECTATION', nom: 'GET TITULAIREDEMANDEAFFECTATION' },
      ctx,
    );
    return ctx.prisma.titulaireDemandeAffectation({ id });
  },
};
