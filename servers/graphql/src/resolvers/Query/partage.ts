import { Context } from '../../types';

export default {
  partage: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.partage({ id });
  },
  partages: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.partages({ where: { isRemoved } });
  },
};
