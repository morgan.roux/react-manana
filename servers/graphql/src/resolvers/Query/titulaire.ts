import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  titulaires: (_, __, ctx: Context) => {
    return ctx.prisma.titulaires();
  },
  titulaire: (_, { id }, ctx: Context) => {
    return ctx.prisma.titulaire({ id });
  },
  titulairePharmacies: async (_, { id }, ctx: Context) => {
    return ctx.prisma.titulaire({ id }).pharmacies();
  },
};
