import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  partenaireRepresentantDemandeAffectations: async (_: any, __: any, ctx: Context) => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser(
      {
        code: 'G_PARTENAIRE_REPRESENTANT_DEMANDE_AFFECTATIONS',
        nom: 'GET PARTENAIRE_REPRESENTANT_DEMANDE_AFFECTATIONS',
      },
      ctx,
    );
    return ctx.prisma.partenaireRepresentantDemandeAffectations();
  },
  partenaireRepresentantDemandeAffectation: (_: any, { id }: { id: string }, ctx: Context) => {
    // Sauvegarde activite de l'utilisateur
    createActiviteUser(
      {
        code: 'G_PARTENAIRE_REPRESENTANT_DEMANDE_AFFECTATION',
        nom: 'GET PARTENAIRE_REPRESENTANT_DEMANDE_AFFECTATION',
      },
      ctx,
    );
    return ctx.prisma.partenaireRepresentantDemandeAffectation({ id });
  },
};
