import { Context } from '../../types';
export default {
  laboratoirePartenaire: async (_, { id }, ctx: Context) => {
    return ctx.prisma.laboratoirePartenaire({ id });
  },
  laboratoirePartenaires: async (_, __, ctx: Context) => {
    return ctx.prisma.laboratoirePartenaires();
  },
};
