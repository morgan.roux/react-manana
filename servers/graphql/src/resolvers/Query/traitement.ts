import { Context } from '../../types';

export default {
  traitement: async (_, { id }, ctx: Context) => {
    return ctx.prisma.traitement({
      id,
    });
  },
};
