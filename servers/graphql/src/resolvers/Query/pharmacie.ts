import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';
export default {
  pharmacie: async (_, { id }, ctx: Context) => {
    return ctx.prisma.pharmacie({ id });
  },
  pharmacies: async (_, { nom }, ctx: Context) => {
    return ctx.prisma.pharmacies({ where: { nom } });
  },
};
