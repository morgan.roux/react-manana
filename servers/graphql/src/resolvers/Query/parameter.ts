import { Context } from '../../types';
import logger from '../../logging';
import { ParameterGroupementByCode } from '../../repository/parameter';

export default {
  parameter: async (_, { id }, ctx: Context) => {
    return ctx.prisma.parameter({ id });
  },
  parameterByCode: async (_, { code }, ctx: Context) => {
    logger.info(`[************** PARAMETERS] GET PARAMETER BY CODE : nombre => ${code}`);
    return ctx.prisma.parameter({ code }).then(value => {
      logger.info(`[PARAMETERS] GET PARAMETER BY CODE : nombre => ${JSON.stringify(value)}`);

      return value;
    });
  },
  parameters: async (_, __, ctx: Context) => {
    return ctx.prisma.parameters().then(value => {
      logger.info(`[PARAMETERS] GET ALL PARAMETERS : nombre => ${value.length}`);
      return value;
    });
  },
  parametersByCategories: async (_, { categories }, ctx: Context) => {
    return ctx.prisma.parameters({ where: { category_in: categories } }).then(value => {
      logger.info(`[PARAMETERS] GET ALL PARAMETERS BY CATEGORY : nombre => ${value.length}`);
      return value;
    });
  },
  parameterGroupementByCode: async (_, _args, ctx: Context) => {
    return ParameterGroupementByCode(ctx, { ..._args }).then(value => {
      logger.info(
        '*********************Réception de paramètres dans Query/parameter******************',
        value,
      );
      return value;
    });
  },
  parametersGroupesCategories: async (_, { categories, groupes }, ctx: Context) => {
    return ctx.prisma
      .parameters({
        where: {
          category_in: categories,
          idParameterGroupe: {
            code_in: groupes,
          },
        },
      })
      .then(value => {
        logger.info(
          `[PARAMETERS] GET ALL PARAMETERS BY CATEGORY AND GROUPES : nombre => ${value.length}`,
        );
        return value;
      });
  },
};
