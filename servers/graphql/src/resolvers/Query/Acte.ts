import { Context } from '../../types';

export default {
  acte: async (_, { id }, ctx: Context) => {
    return ctx.prisma.acte({ id });
  },
  actes: async (_, __, ctx: Context) => {
    return ctx.prisma.actes();
  },
};
