import { Context } from '../../types';

export default {
  todoEtiquette: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.todoEtiquette({ id });
  },
  todoEtiquettes: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.todoEtiquettes({ where: { isRemoved } });
  },
  todoEtiquettesByUser: async (
    _: any,
    { idUser, isRemoved }: { idUser: string; isRemoved: boolean },
    ctx: Context,
  ) => {
    const todoEtiquettes = await ctx.prisma.todoEtiquettes({
      where: { userCreation: { id: idUser }, isRemoved },
    });
    const othersEtiquettes = await ctx.prisma
      .todoEtiquetteParticipants({
        where: {
          userParticipant: { id_in: [idUser] },
          isRemoved: false,
        },
      })
      .then(async results => {
        return Promise.all(
          results.map(res => {
            return ctx.prisma.todoEtiquetteParticipant({ id: res.id }).etiquette();
          }),
        );
      });

    const allEtiquettes = [...todoEtiquettes, ...othersEtiquettes];
    return allEtiquettes;
  },
};
