import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';
export default {
  laboratoire: async (_, { id }, ctx: Context) => {
    return ctx.prisma.laboratoire({ id });
  },
  laboratoires: async (_, __, ctx: Context) => {
    return ctx.prisma.laboratoires();
  },
  getFicheLaboratoires: async (_: any, { isRemoved }: { isRemoved: boolean }, ctx: Context) => {
    return ctx.prisma.laboratoires({ where: { isRemoved } });
  },
};
