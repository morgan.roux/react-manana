import { Context } from '../../types';
import { countActualitesCibles, actualiteAllUsersCible } from '../../repository/actualite';
import { WatchTime } from '../../services/time';

const watchTime = new WatchTime();

export default {
  actualite: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.actualite({ id }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(`[Actualite] query actualite duration : `);
      return value;
    });
  },
  actualiteOrigines: async (_, __, ctx: Context) => {
    watchTime.start();
    return ctx.prisma.actualiteOrigines().then(value => {
      watchTime.stop();
      watchTime.printElapsed(`[Actualite] query actualiteOrigine duration : `);
      return value;
    });
  },
  countActualitesCibles: async (_, __, ctx: Context) => {
    return countActualitesCibles(ctx);
  },
  actualiteAllUsersCible: async (
    _,
    { codeServices, idPartenaires, idLaboratoires, pharmacieRoles, presidentRegions, fichierCible },
    ctx: Context,
  ) => {
    watchTime.start();
    return actualiteAllUsersCible(ctx, {
      idActualite: null,
      codeServices,
      idPartenaires,
      idLaboratoires,
      pharmacieRoles,
      presidentRegions,
      fichierCible,
    }).then(value => {
      watchTime.stop();
      watchTime.printElapsed(`[Actualite] query actualiteAllUsersCible duration : `);
      return value;
    });
  },
};
