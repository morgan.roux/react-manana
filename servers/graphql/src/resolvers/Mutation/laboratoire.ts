import { FichierCreateInput } from '../../generated/prisma-client';
import { createUpdateLaboratoire } from '../../repository/laboratoire';
import { Context, FichierInput, LaboratoireSuiteInput } from '../../types';

export default {
  createUpdateLaboratoire: (
    _,
    { id, nomLabo, suite }: { id: string; nomLabo: string; suite: LaboratoireSuiteInput },
    ctx: Context,
  ) => {
    return createUpdateLaboratoire({ id, nomLabo, suite }, ctx);
  },
  updateLaboratoireSortie: (_, { id, sortie }, ctx: Context) => {
    return ctx.prisma.updateLaboratoire({ where: { id }, data: { sortie } }).then(async updated => {
      await ctx.indexing.publishSave('laboratoires', updated.id);
      return updated;
    });
  },
  updatePhotoLaboratoire: (_, { id, photo }: { id: string; photo: FichierInput }, ctx: Context) => {
    const data: FichierCreateInput = {
      chemin: photo.chemin,
      nomOriginal: photo.nomOriginal,
      type: photo.type,
    };

    return ctx.prisma
      .updateLaboratoire({
        where: { id },
        data: {
          photo: {
            upsert: {
              create: data,
              update: data,
            },
          },
        },
      })
      .then(async updated => {
        await ctx.indexing.publishSave('laboratoires', updated.id);
        return updated;
      });
  },
};
