import { createUpdateTicket, updateTicketCible, updateTicketStatut } from '../../repository/ticket';
import { Context, TicketInput, TicketCibleInput, StatusTicket } from '../../types';

export default {
  createUpdateTicket: async (_: any, { input }: { input: TicketInput }, ctx: Context) => {
    console.log('Mutation/ticket.ts --> createUpdateTicket --> input :>> ', input);
    return createUpdateTicket(input, ctx);
  },
  updateTicketCible: async (_: any, { input }: { input: TicketCibleInput }, ctx: Context) => {
    console.log('Mutation/ticket.ts --> updateTicketCible --> input :>> ', input);
    return updateTicketCible(input, ctx);
  },
  updateTicketStatut: async (
    _: any,
    { id, status }: { id: string; status: StatusTicket },
    ctx: Context,
  ) => {
    return updateTicketStatut({ id, status }, ctx);
  },
};
