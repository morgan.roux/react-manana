import { createUpdatePartageGroupeAmisCible } from '../../repository/partageGroupeAmisCible';
import { Context, PartageGroupeAmisCibleInput } from '../../types';

export default {
  createUpdatePartageGroupeAmisCible: async (
    _: any,
    { input }: { input: PartageGroupeAmisCibleInput },
    ctx: Context,
  ) => {
    return createUpdatePartageGroupeAmisCible(input, ctx);
  },
};
