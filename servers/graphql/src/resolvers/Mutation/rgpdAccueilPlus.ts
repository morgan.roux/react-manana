import { createUpdateRgpdAccueilPlus } from '../../repository/rgpdAccueilPlus';
import { Context, RgpdAccueilPlusInput } from '../../types';

export default {
  createUpdateRgpdAccueilPlus: async (
    _: any,
    { input }: { input: RgpdAccueilPlusInput },
    ctx: Context,
  ) => {
    return createUpdateRgpdAccueilPlus(ctx, input);
  },
  deleteRgpdAccueilPlus: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.deleteRgpdAccueilPlus({ id });
  },
};
