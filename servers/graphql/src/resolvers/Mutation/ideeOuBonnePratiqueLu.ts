import {
  createUpdateIdeeOuBonnePratiqueLu,
  softDeleteIdeeOuBonnePratiqueLu,
} from '../../repository/ideeOuBonnePratiqueLu';
import { Context, IdeeOuBonnePratiqueLuInput } from '../../types';

export default {
  createUpdateIdeeOuBonnePratiqueLu: async (
    _: any,
    { input }: { input: IdeeOuBonnePratiqueLuInput },
    ctx: Context,
  ) => {
    return createUpdateIdeeOuBonnePratiqueLu(ctx, input);
  },
  softDeleteIdeeOuBonnePratiqueLus: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteIdeeOuBonnePratiqueLu(ctx, id)));
  },
};
