import { Context } from '../../types';
import {
  addToPanier,
  updateQtePanierLigne,
  deletePanierLigne,
  clearPanier,
  takeToMyPanier,
} from '../../repository/panier';
import { createActiviteUser } from '../../repository/activiteUser';
import { WatchTime } from '../../services/time';

const watchTime = new WatchTime();

export default {
  addToPanier: async (
    _,
    { idPharmacie, codeCanal, idCanalArticle, optimiser, quantite },
    ctx: Context,
  ) => {
    watchTime.start();
    return addToPanier(ctx, {
      idPharmacie,
      codeCanal,
      idCanalArticle,
      optimiser,
      quantite,
    }).then(added => {
      watchTime.stop();
      watchTime.printElapsed(
        `** PANIER MUTATION ** Le temps de l'ajout au panier de l'article avec l'ID => ${idCanalArticle} : `,
      );

      return added;
    });
  },
  deletePanier: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma
      .updatePanier({ data: { validate: true }, where: { id } })
      .then(async update => {
        await ctx.indexing.publishSave('paniers', update.id);

        watchTime.stop();
        watchTime.printElapsed(
          `** PANIER MUTATION ** Le temps de la suppression du panier avec l'ID => ${id} : `,
        );
        return update;
      });
  },
  clearMyPanier: async (_, { idPharmacie, codeCanal }, ctx: Context) => {
    watchTime.start();
    return clearPanier(ctx, { idPharmacie, codeCanal }).then(panier => {
      watchTime.stop();
      watchTime.printElapsed(`** PANIER MUTATION ** Le temps mise pour vider la mon panier : `);

      return panier;
    });
  },
  updateQtePanierLigne: (_, { id, quantite }, ctx: Context) => {
    watchTime.start();
    return updateQtePanierLigne(ctx, { id, quantite }).then(panier => {
      watchTime.stop();
      watchTime.printElapsed(
        `** PANIER MUTATION ** Le temps mise pour modifier le quantite de mon panier ligne : `,
      );

      return panier;
    });
  },
  deletePanierLigne: (_, { id }, ctx: Context) => {
    watchTime.start();
    return deletePanierLigne(ctx, { id }).then(panier => {
      createActiviteUser(
        {
          code: 'DPANIERLIGNE',
          nom: 'DELETE PANIER LIGNE',
          log: JSON.stringify({ id: panier.id }),
        },
        ctx,
      );

      watchTime.stop();
      watchTime.printElapsed(
        `** PANIER MUTATION ** Le temps de la suppression du panier ligne avec l'ID => ${id} : `,
      );

      return panier;
    });
  },
  deletePanierLignes: (_, { ids }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma
      .deleteManyPanierLignes({ id_in: ids })
      .then(data => {
        ids.map((id: string) => ctx.indexing.publishDelete('panierlignes', id));

        watchTime.stop();
        watchTime.printElapsed(
          `** PANIER MUTATION ** Le temps de suppression de tous mes panier lignes : `,
        );

        return parseInt(data.count, 10) ? true : false;
      })
      .catch(err => false);
  },
  // param 'id' => id panierligne
  takeProductToMyPanier: async (_, { id, idPharmacie, codeCanal }, ctx: Context) => {
    watchTime.start();
    return takeToMyPanier(ctx, { id, idPharmacie, codeCanal }).then(panier => {
      watchTime.stop();
      watchTime.printElapsed(
        `** PANIER MUTATION ** Le temps mise pour prendre un produit dans un autre panier sur mon panier : `,
      );

      return panier;
    });
  },
};
