import logger from '../../logging';
import {
  calculPrixTotalOperation,
  createCommande,
  exportCommandeLigne,
} from '../../repository/commande';
import { validatePanier } from '../../repository/panier';
import { WatchTime } from '../../services/time';
import { Context } from '../../types';

const watchTime = new WatchTime();

export default {
  validatePanierAndCreateCommande: async (
    _,
    { idPanier, idPharmacie, commentaireInterne, commentaireExterne, codeCanalCommande },
    ctx: Context,
  ) => {
    watchTime.start();
    return ctx.prisma.panier({ id: idPanier }).then(async panier =>
      createCommande(ctx, {
        id: panier.id,
        idPharmacie,
        idOperation: null,
        commentaireInterne,
        commentaireExterne,
        articlesCommande: null,
        codeCanalCommande,
        idPublicite: null,
      }).then(async commande => {
        const panierLignes = await ctx.prisma.panierLignes({
          where: { idPanier: { id: panier.id }, validate: false },
        });

        if (!panierLignes || (panierLignes && !panierLignes.length)) {
          validatePanier(ctx, { id: panier.id });
        }

        const returnCommande = commande
          ? await exportCommandeLigne(ctx, { id: commande.id })
          : null;

        watchTime.stop();
        watchTime.printElapsed(`** COMMANDE PANIER ** Le temps mise pour valider panier : `);

        return returnCommande;
      }),
    );
  },
  createOperationCommande: async (
    _,
    {
      idOperation,
      idPharmacie,
      commentaireInterne,
      commentaireExterne,
      articlesCommande,
      idPublicite,
    },
    ctx: Context,
  ) => {
    watchTime.start();
    return createCommande(ctx, {
      id: null,
      idPharmacie,
      idOperation,
      commentaireInterne,
      commentaireExterne,
      articlesCommande,
      codeCanalCommande: null,
      idPublicite,
    }).then(async commande => {
      watchTime.stop();
      watchTime.printElapsed(
        `** COMMANDE OPERATION ** Le temps mise pour la création commande operation : `,
      );

      return commande && commande.id
        ? exportCommandeLigne(ctx, { id: commande.id }).then(async cmd => {
            await ctx.indexing.publishSave('commandes', commande.id);
            await ctx.indexing.publishSave('operations', idOperation);
            return cmd;
          })
        : null;
    });
  },
  updateStatutCommande: async (_, { id, statut }, ctx: Context) => {
    watchTime.start();
    const commandeStatut = await ctx.prisma.commandeStatut({ code: statut });
    return commandeStatut && commandeStatut.id
      ? ctx.prisma
          .createCommandeStatutStatut({
            idCommande: { connect: { id } },
            idCommandeStatut: { connect: { id: commandeStatut.id } },
          })
          .then(async _ => {
            logger.info(`[INDEX] COMMANDE : ${id}`);
            await ctx.indexing.publishSave('commandes', id);

            watchTime.stop();
            watchTime.printElapsed(
              `** COMMANDE STATUS ** Le temps mise pour la mise à jour du status commane: `,
            );

            return ctx.prisma.commande({ id });
          })
      : null;
  },
  updateCommandeLigneStatut: async (_, { id, statut }, ctx: Context) => {
    watchTime.start();
    return ctx.prisma
      .updateCommandeLigne({
        data: { idStatutLigne: { connect: { code: statut } } },
        where: { id },
      })
      .then(async updated => {
        await ctx.indexing.publishSave('commandelignes', updated.id);

        watchTime.stop();
        watchTime.printElapsed(
          `** COMMANDE LIGNE STATUS ** Le temps mise pour la mise à jour du status ligne commande : `,
        );

        return updated;
      });
  },
  calculPrixTotalOperation: async (_, { idPharmacie, idOperation, articles }, ctx: Context) => {
    watchTime.start();
    return calculPrixTotalOperation(articles, { idPharmacie, idOperation }, ctx).then(values => {
      watchTime.stop();
      watchTime.printElapsed(
        `** COMMANDE OPERATION ** Le temps mise pour calculer le prix total du commande operation : `,
      );

      return values;
    });
  },
};
