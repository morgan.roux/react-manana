import { Context, PromotionInput } from '../../types';
import { createUpdatePromotion, softDeletePromotion } from '../../repository/promotion';

export default {
  createUpdatePromotion: async (
    _,
    {
      id,
      nom,
      dateDebut,
      dateFin,
      promotionType,
      status,
      codeCanal,
      idsCanalArticle,
      groupeClients,
    }: PromotionInput,
    ctx: Context,
  ) => {
    return createUpdatePromotion(ctx, {
      id,
      nom,
      dateDebut,
      dateFin,
      promotionType,
      status,
      codeCanal,
      idsCanalArticle,
      groupeClients,
    });
  },
  softDeletePromotion: async (_, { id }, ctx: Context) => {
    return softDeletePromotion(ctx, { id });
  },
};
