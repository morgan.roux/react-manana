import { Context } from '../../types';
import { MutationResolvers } from '../../generated/graphqlgen';
import { createMessagerie } from '../../repository/messagerie';
import {
  MESSAGERIE_LU,
  MESSAGERIE_RESTAUREE,
  MESSAGERIE_SUPPRIMEE,
} from '../Subscription/messagerie';
import logger from '../../logging';
import { MessagerieType } from '../../generated/prisma-client';

export default {
  createUpdateMessagerie: async (
    _: any,
    { inputs }: MutationResolvers.ArgsCreateUpdateMessagerie,
    ctx: Context,
  ) => {
    return createMessagerie(ctx, { inputs });
  },

  markMessageAsSeen: async (
    _: any,
    { idMessageHisto }: { idMessageHisto: string },
    ctx: Context,
  ) => {
    await ctx.prisma.updateMessagerieHisto({ where: { id: idMessageHisto }, data: { lu: true } });
    const messagerie = await ctx.prisma.messagerieHisto({ id: idMessageHisto }).messagerie();
    if (!messagerie) return null;
    const recepteur = await ctx.prisma.messagerieHisto({ id: idMessageHisto }).userRecepteur();
    await ctx.pubsub
      .publish(MESSAGERIE_LU, { messagerieLu: messagerie, recepteurId: recepteur.id })
      .then(_ => {
        logger.info(` [ PUBLISH MESSAGERIE LU ], ${JSON.stringify(messagerie)}`);
      });

    return messagerie;
  },

  markMessageAsUnseen: async (
    _: any,
    { idMessageHisto }: { idMessageHisto: string },
    ctx: Context,
  ) => {
    await ctx.prisma.updateMessagerieHisto({ where: { id: idMessageHisto }, data: { lu: false } });
    const messagerie = await ctx.prisma.messagerieHisto({ id: idMessageHisto }).messagerie();
    if (!messagerie) return null;
    const recepteur = await ctx.prisma.messagerieHisto({ id: idMessageHisto }).userRecepteur();
    await ctx.pubsub
      .publish(MESSAGERIE_LU, { messagerieLu: messagerie, recepteurId: recepteur.id })
      .then(_ => {
        logger.info(` [ PUBLISH MESSAGERIE LU ], ${JSON.stringify(messagerie)}`);
      });

    return messagerie;
  },

  deleteMessages: async (
    _: any,
    {
      ids,
      typeMessagerie,
      permanent,
    }: { ids: string[]; typeMessagerie: MessagerieType; permanent: boolean },
    ctx: Context,
  ) => {
    const dataDelete = permanent ? { suppressionPermanente: true } : {};

    const userRecepteur =
      typeMessagerie === 'R'
        ? {
            userRecepteur: {
              id: ctx.userId,
            },
          }
        : {};

    await ctx.prisma.updateManyMessagerieHistoes({
      where: {
        messagerie: {
          id_in: ids,
        },
        typeMessagerie: typeMessagerie,
        ...userRecepteur,
      },
      data: {
        isRemoved: true,
        ...dataDelete,
      },
    });

    const histoes = await ctx.prisma.messagerieHistoes({
      where: {
        messagerie: {
          id_in: ids,
        },
        typeMessagerie: typeMessagerie,
        ...userRecepteur,
        isRemoved: true,
        ...dataDelete,
      },
    });

    if (!histoes || (histoes && !histoes.length)) return [];

    histoes.map(async histo => {
      const messagerie = await ctx.prisma.messagerieHisto({ id: histo.id }).messagerie();
      if (!messagerie) return null;
      const recepteur = await ctx.prisma.messagerieHisto({ id: histo.id }).userRecepteur();
      ctx.pubsub
        .publish(MESSAGERIE_SUPPRIMEE, {
          messagerieSupprimee: messagerie,
          recepteurId: recepteur.id,
        })
        .then(_ => {
          logger.info(` [ PUBLISH MESSAGERIE DELETED ], ${JSON.stringify(messagerie)}`);
        });
    });

    return ctx.prisma.messageries({
      where: {
        id_in: ids,
      },
    });
  },

  restoreMessages: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    await ctx.prisma.updateManyMessagerieHistoes({
      where: {
        messagerie: {
          id_in: ids,
        },
      },
      data: {
        isRemoved: false,
      },
    });
    const histoes = await ctx.prisma.messagerieHistoes({
      where: {
        messagerie: {
          id_in: ids,
        },
        isRemoved: true,
      },
    });

    if (!histoes || (histoes && !histoes.length)) return [];

    histoes.map(async histo => {
      const messagerie = await ctx.prisma.messagerieHisto({ id: histo.id }).messagerie();
      if (!messagerie) return null;
      const recepteur = await ctx.prisma.messagerieHisto({ id: histo.id }).userRecepteur();
      ctx.pubsub
        .publish(MESSAGERIE_RESTAUREE, {
          messagerieRestauree: messagerie,
          recepteurId: recepteur.id,
        })
        .then(_ => {
          logger.info(` [ PUBLISH MESSAGERIE RESTAUREE ], ${JSON.stringify(messagerie)}`);
        });
    });

    return ctx.prisma.messageries({
      where: {
        id_in: ids,
      },
    });
  },

  deleteMessagesInDB: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    await ctx.prisma.deleteManyMessagerieFichierJoints({
      messagerie: { id_in: ids },
    });

    await ctx.prisma.deleteManyMessagerieHistoes({
      messagerie: { id_in: ids },
    });

    await ctx.prisma.deleteManyMessagerieReponseHistoes({
      OR: [
        {
          messagerieDepart: {
            id_in: ids,
          },
        },
        {
          messagerieReponse: {
            id_in: ids,
          },
        },
      ],
    });
    return ctx.prisma
      .messageries({
        where: {
          id_in: ids,
        },
      })
      .then(messageries => {
        return messageries && messageries.length
          ? messageries.map(message => ctx.prisma.deleteMessagerie({ id: message.id }))
          : [];
      });
  },
};
