import { Context } from '../../types';
import { updateParameterValues } from '../../repository/parameter';
import { ApolloError } from 'apollo-server';
import logger from '../../logging';

export default {
  updateParameterValues: async (_, { parameters }, ctx: Context) => {
    return updateParameterValues(ctx, parameters)
      .then(async _ => {
        logger.info(
          '****************Paramètres dans resolvers/mutation*****************',
          parameters,
        );
        const ids = parameters && parameters.length ? parameters.map(param => param.id) : [];
        return ids && ids.length ? ctx.prisma.parameters({ where: { id_in: ids } }) : null;
      })
      .catch(error => {
        logger.error(`Erreur lors de 'enregistrement des parametres : `, { error });
        return new ApolloError(
          `Erreur lors de 'enregistrement des parametres' : `,
          'UPDATE_PARAM_VALUES_ERROR',
        );
      });
  },
};
