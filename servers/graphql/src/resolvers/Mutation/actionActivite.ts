import { createUpdateActionActivite } from '../../repository/actionActivite';
import { ActionActiviteInput, Context } from '../../types';

export default {
  createUpdateActionActivite: async (
    _: any,
    { input }: { input: ActionActiviteInput },
    ctx: Context,
  ) => {
    return createUpdateActionActivite(ctx, input);
  },
};
