import { Context } from '../../types';
import { createUpdatePublicite } from '../../repository/publicite';

export default {
  createUpdatePublicite: async (
    _,
    {
      id,
      libelle,
      description,
      typeEspace,
      origine,
      url,
      ordre,
      codeItem,
      idItemSource,
      image,
      dateDebut,
      dateFin,
    },
    ctx: Context,
  ) => {
    return createUpdatePublicite(ctx, {
      id,
      libelle,
      description,
      typeEspace,
      origine,
      url,
      ordre,
      codeItem,
      idItemSource,
      image,
      dateDebut,
      dateFin,
    });
  },
  updateOrdrePublicite: async (_, { id, ordre }, ctx: Context) => {
    return ctx.prisma.updatePublicite({ where: { id }, data: { ordre } }).then(async publicite => {
      if (publicite && publicite.id) await ctx.indexing.publishSave('publicites', publicite.id);
      return publicite;
    });
  },
  deletePublicite: async (_, { id }, ctx: Context) => {
    return ctx.prisma
      .updatePublicite({ where: { id }, data: { isRemoved: true } })
      .then(async publicite => {
        if (publicite && publicite.id) await ctx.indexing.publishSave('publicites', publicite.id);
        return publicite;
      });
  },
};
