import { createUpdateRessource, softDeleteRessource } from '../../repository/ressource';
import { createActiviteUser } from '../../repository/activiteUser';
import { Context, RessourceInput } from '../../types';

export default {
  createUpdateRessource: async (_: any, args: { input: RessourceInput }, ctx: Context) => {
    return createUpdateRessource(args.input, ctx).then(ressource => {
      createActiviteUser(
        {
          code: args.input.id ? 'CRESSOURCE' : 'ERESSOURCE',
          nom: args.input.id ? 'CREATE RESSOURCE' : 'EDIT RESSOURCE',
          log: JSON.stringify({ id: ressource.id }),
        },
        ctx,
      );
      return ressource;
    });
  },
  softDeleteRessource: async (_: any, { id }: { id: string }, ctx: Context) => {
    return softDeleteRessource(ctx, { id });
  },
  softDeleteRessources: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(id => softDeleteRessource(ctx, { id })));
  },
};
