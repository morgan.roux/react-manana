import { RgpdAccueilCreateInput } from '../../generated/prisma-client';
import { Context, RgpdAccueilInput } from '../../types';

export default {
  createUpdateRgpdAccueil: async (_: any, { input }: { input: RgpdAccueilInput }, ctx: Context) => {
    const { id, description, title } = input;
    const groupement = ctx.groupementId ? { connect: { id: ctx.groupementId } } : null;
    const userCreation = ctx.userId ? { connect: { id: ctx.userId } } : null;
    const userModification = ctx.userId ? { connect: { id: ctx.userId } } : null;
    const data: RgpdAccueilCreateInput = {
      title,
      description,
      groupement,
      userCreation,
      userModification,
    };
    const res = await ctx.prisma.upsertRgpdAccueil({
      where: { id: id || '' },
      create: data,
      update: data,
    });
    return res;
  },
  deleteRgpdAccueil: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.deleteRgpdAccueil({ id });
  },
};
