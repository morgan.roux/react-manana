import { Context } from '../../types';
import { MutationResolvers } from '../../generated/graphqlgen';
import {
  createUpdatePersonnelAffectation,
  createUpdateTitulaireAffectation,
} from '../../repository/affectation';

export default {
  createUpdatePersonnelAffectation: async (
    _: any,
    { inputs }: MutationResolvers.ArgsCreateUpdatePersonnelAffectation,
    ctx: Context,
  ) => {
    return createUpdatePersonnelAffectation(ctx, { inputs });
  },
  createUpdateTitulaireAffectation: async (
    _: any,
    { inputs }: MutationResolvers.ArgsCreateUpdateTitulaireAffectation,
    ctx: Context,
  ) => {
    return createUpdateTitulaireAffectation(ctx, { inputs });
  },
};
