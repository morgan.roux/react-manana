import { Context } from '../../types';
import { createUpdatePersonnel, updateSortiePersonnel } from '../../repository/personnel';
import { MutationResolvers } from '../../generated/graphqlgen';

export default {
  createUpdatePersonnel: async (
    _,
    {
      id,
      codeService,
      respHierarch,
      sortie,
      civilite,
      nom,
      prenom,
      sexe,
      contact,
      commentaire,
      dateSortie,
      idGroupement,
    }: MutationResolvers.ArgsCreateUpdatePersonnel,
    ctx: Context,
  ) => {
    const defaultSortie = sortie ? sortie : 0;
    return createUpdatePersonnel(ctx, {
      id,
      codeService,
      respHierarch,
      sortie: defaultSortie,
      civilite,
      nom,
      prenom,
      sexe,
      commentaire,
      dateSortie,
      idGroupement,
      contact,
    });
  },
  deletePersonnel: async (_, { id }, ctx: Context) => {
    return updateSortiePersonnel(ctx, { id });
  },
  deletePersonnels: async (_, { ids }: { ids: any[] }, ctx: Context) => {
    return Promise.all(ids.map(async (id: any) => updateSortiePersonnel(ctx, { id })));
  },
};
