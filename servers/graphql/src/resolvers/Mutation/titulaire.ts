import { Context } from '../../types';
import {
  createUpdateTitulaire,
  updateStatusTitulaire,
  deleteSoftTitulaire,
} from '../../repository/titulaire';
import { createActiviteUser } from '../../repository/activiteUser';
import moment from 'moment';
const now = moment().format();

export default {
  createUpdateTitulaire: async (
    _,
    { id, civilite, nom, prenom, contact, idPharmacies },
    ctx: Context,
  ) => {
    return createUpdateTitulaire(
      {
        id,
        civilite,
        nom,
        prenom,
        contact,
        idPharmacies:
          idPharmacies && idPharmacies.length ? idPharmacies.map((id: any) => String(id)) : [],
      },
      ctx,
    ).then(created => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */
      createActiviteUser(
        {
          code: 'CTITULAIRE',
          nom: 'CREATE TITULAIRE',
          log: JSON.stringify({ id }),
        },
        ctx,
      );

      return created;
    });
  },
  updateStatusTitulaire: async (_, { id, idRegion, dateDebut, dateFin, status }, ctx: Context) => {
    return updateStatusTitulaire({ id, idRegion, dateDebut, dateFin, status }, ctx).then(
      updated => {
        /**
         *
         * Sauvegarde activite de l'utilisateur
         *
         */
        createActiviteUser(
          {
            code: 'USTATUSTITULAIRE',
            nom: 'UPDATE STATUS TITULAIRE',
            log: JSON.stringify({ id }),
          },
          ctx,
        );

        return updated;
      },
    );
  },
  deleteSoftTitulaire: async (_, { id }, ctx: Context) => {
    return deleteSoftTitulaire({ id }, ctx);
  },

  deleteSoftTitulaires: async (_, { ids }, ctx: Context) => {
    return Promise.all(ids.map(id => deleteSoftTitulaire({ id }, ctx)));
  },
};
