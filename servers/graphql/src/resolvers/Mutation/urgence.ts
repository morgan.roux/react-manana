import { Context } from '../../types';
import { createUpdateUrgence } from '../../repository/urgence';
import { MutationResolvers } from '../../generated/graphqlgen';

export default {
  createUpdateUrgence: async (
    _,
    { id, code, color, libelle }: MutationResolvers.ArgsCreateUpdateUrgence,
    ctx: Context,
  ) => {
    return createUpdateUrgence(ctx, {
      id,
      code,
      color,
      libelle,
    });
  },
  deleteUrgence: async (_, { id }, ctx: Context) => {
    return ctx.prisma.updateUrgence({ where: { id }, data: { isRemoved: true } });
  },
};
