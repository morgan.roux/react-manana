import { createUpdateRgpdHistorique } from '../../repository/rgpdHistorique';
import { Context, RgpdHistoriqueInput } from '../../types';

export default {
  createUpdateRgpdHistorique: async (
    _: any,
    { input }: { input: RgpdHistoriqueInput },
    ctx: Context,
  ) => {
    return createUpdateRgpdHistorique(ctx, input);
  },
  deleteRgpdHistorique: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.deleteRgpdHistorique({ id });
  },
};
