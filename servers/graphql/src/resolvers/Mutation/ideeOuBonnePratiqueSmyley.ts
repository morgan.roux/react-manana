import {
  createUpdateIdeeOuBonnePratiqueSmyley,
  softDeleteIdeeOuBonnePratiqueSmyley,
} from '../../repository/ideeOuBonnePratiqueSmyley';
import { Context, IdeeOuBonnePratiqueSmyleyInput } from '../../types';

export default {
  createUpdateIdeeOuBonnePratiqueSmyley: async (
    _: any,
    { input }: { input: IdeeOuBonnePratiqueSmyleyInput },
    ctx: Context,
  ) => {
    return createUpdateIdeeOuBonnePratiqueSmyley(ctx, input);
  },
  softDeleteIdeeOuBonnePratiqueSmyleys: async (
    _: any,
    { ids }: { ids: string[] },
    ctx: Context,
  ) => {
    return Promise.all(ids.map(async id => softDeleteIdeeOuBonnePratiqueSmyley(ctx, id)));
  },
};
