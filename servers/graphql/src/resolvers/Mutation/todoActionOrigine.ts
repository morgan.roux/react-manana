import {
  createUpdateTodoActionOrigine,
  softDeleteTodoActionOrigine,
} from '../../repository/todoActionOrigine';
import { Context, TodoActionOrigineInput } from '../../types';

export default {
  createUpdateTodoActionOrigine: async (
    _: any,
    { input }: { input: TodoActionOrigineInput },
    ctx: Context,
  ) => {
    return createUpdateTodoActionOrigine(input, ctx);
  },
  softDeleteTodoActionOrigines: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteTodoActionOrigine(ctx, { id })));
  },
};
