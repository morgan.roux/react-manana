import {
  createUpdateTodoActionChangementStatus,
  softDeleteTodoActionChangementStatus,
} from '../../repository/todoActionChangementStatus';
import { Context, TodoActionChangementStatusInput } from '../../types';

export default {
  createUpdateTodoActionChangementStatus: async (
    _: any,
    { input }: { input: TodoActionChangementStatusInput },
    ctx: Context,
  ) => {
    return createUpdateTodoActionChangementStatus(input, ctx);
  },
  softDeleteTodoActionChangementStatuses: async (
    _: any,
    { ids }: { ids: string[] },
    ctx: Context,
  ) => {
    return Promise.all(ids.map(async id => softDeleteTodoActionChangementStatus(ctx, { id })));
  },
};
