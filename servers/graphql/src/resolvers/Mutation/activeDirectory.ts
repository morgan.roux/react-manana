import { Context } from '../../types';
import {
  CreateActiveDirectoryCredential,
  UpdateActiveDirectoryCredential,
  DeleteActiveDirectory,
} from '../../repository/activeDirectory';
export default {
  createActiveDirectoryCredential: async (_, _args, ctx: Context) => {
    return CreateActiveDirectoryCredential(ctx, { ..._args });
  },
  updateActiveDirectoryCredential: async (_, _args, ctx: Context) => {
    return UpdateActiveDirectoryCredential(ctx, { ..._args });
  },
  deleteActiveDirectoryCredential: async (_, { id }, ctx: Context) => {
    return DeleteActiveDirectory(ctx, { id });
  },
};
