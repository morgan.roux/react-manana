import { Context } from '../../types';
import { createUpdateRoleTraitement } from '../../repository/role';

export default {
  createUpdateRoleTraitement: async (_, { codeRole, codeTraitements }, ctx: Context) => {
    return createUpdateRoleTraitement(ctx, {
      codeRole,
      codeTraitements:
        codeTraitements && codeTraitements.length
          ? codeTraitements.map((code: any) => String(code))
          : [],
    });
  },
};
