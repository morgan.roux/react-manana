import {
  createUpdateGroupeAmisDetail,
  softDeleteGroupeAmisDetail,
} from '../../repository/groupeAmisDetail';
import { Context, GroupeAmisDetailInput } from '../../types';

export default {
  createUpdateGroupeAmisDetail: async (
    _: any,
    { input }: { input: GroupeAmisDetailInput },
    ctx: Context,
  ) => {
    return createUpdateGroupeAmisDetail(input, ctx);
  },
  softDeleteGroupeAmisDetails: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(
      ids.map(async id => {
        return softDeleteGroupeAmisDetail(id, ctx);
      }),
    );
  },
};
