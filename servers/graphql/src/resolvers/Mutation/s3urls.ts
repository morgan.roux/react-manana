import s3 from '../../services/s3';

export default {
  createPutPresignedUrls: async (_, { filePaths }, __) => {
    return filePaths.map((filePath: any) => {
      return {
        filePath: filePath.replace(/ |-|\)|\(/g, ''),
        presignedUrl: s3.createPresignedPutUrl(filePath),
      };
    });
  },
  createGetPresignedUrls: async (_, { filePaths }, __) => {
    return filePaths.map((filePath: any) => {
      return {
        filePath: filePath.replace(/ |-|\)|\(/g, ''),
        presignedUrl: s3.createPresignedGetUrl(filePath),
      };
    });
  },
};
