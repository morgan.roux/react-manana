import { createUpdateRgpd } from '../../repository/rgpd';
import { Context, RgpdInput } from '../../types';

export default {
  createUpdateRgpd: async (_: any, { input }: { input: RgpdInput }, ctx: Context) => {
    return createUpdateRgpd(ctx, input);
  },
  deleteRgpd: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.deleteRgpd({ id });
  },
  updateRgpdPolitiqueConfidentialite: async (
    _: any,
    { id, politiqueConfidentialite }: { id: string; politiqueConfidentialite: string },
    ctx: Context,
  ) => {
    return ctx.prisma.updateRgpd({
      where: { id },
      data: {
        politiqueConfidentialite,
      },
    });
  },
  updateRgpdConditionUtilisation: async (
    _: any,
    { id, conditionUtilisation }: { id: string; conditionUtilisation: string },
    ctx: Context,
  ) => {
    return ctx.prisma.updateRgpd({
      where: { id },
      data: {
        conditionUtilisation,
      },
    });
  },
  updateRgpdInformationsCookies: async (
    _: any,
    { id, informationsCookies }: { id: string; informationsCookies: string },
    ctx: Context,
  ) => {
    return ctx.prisma.updateRgpd({
      where: { id },
      data: {
        informationsCookies,
      },
    });
  },
};
