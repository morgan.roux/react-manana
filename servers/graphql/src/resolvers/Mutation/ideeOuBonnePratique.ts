import {
  createUpdateIdeeOuBonnePratique,
  softDeleteIdeeOuBonnePratique,
  updateIdeeOuBonnePratiqueStatus,
} from '../../repository/ideeOuBonnePratique';
import { Context, IdeeOuBonnePratiqueInput, IdeeOuBonnePratiqueStatus } from '../../types';

export default {
  createUpdateIdeeOuBonnePratique: async (
    _: any,
    { input }: { input: IdeeOuBonnePratiqueInput },
    ctx: Context,
  ) => {
    return createUpdateIdeeOuBonnePratique(ctx, input);
  },
  updateIdeeOuBonnePratiqueStatus: async (
    _: any,
    {
      id,
      status,
      commentaire,
    }: { id: string; status: IdeeOuBonnePratiqueStatus; commentaire?: string },
    ctx: Context,
  ) => {
    return updateIdeeOuBonnePratiqueStatus(ctx, id, status, commentaire);
  },
  softDeleteIdeeOuBonnePratiques: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteIdeeOuBonnePratique(ctx, id)));
  },
};
