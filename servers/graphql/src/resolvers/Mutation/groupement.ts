import { Context } from '../../types';
import logger from '../../logging';
import {
  deleteGroupementAvatars,
  createGroupement,
  deleteGroupement,
  updateGroupement,
  createGroupementLogo,
  deleteGroupementLogo,
} from '../../repository/groupement';

export default {
  createGroupement: async (
    _,
    {
      nom,
      adresse1,
      adresse2,
      cp,
      ville,
      pays,
      telBureau,
      telMobile,
      mail,
      site,
      commentaire,
      nomResponsable,
      prenomResponsable,
      dateSortie,
      sortie,
      avatar,
      isReference,
    },
    ctx: Context,
  ) => {
    const groupement = await createGroupement(
      {
        nom,
        adresse1,
        adresse2,
        cp,
        ville,
        pays,
        telBureau,
        telMobile,
        mail,
        site,
        commentaire,
        nomResponsable,
        prenomResponsable,
        dateSortie,
        sortie,
        isReference,
      },
      ctx,
    );

    if (groupement && groupement.id) {
      ctx.indexing.publishSave('groupements', groupement.id);

      if (avatar && avatar.chemin && avatar.nomOriginal && avatar.type) {
        await deleteGroupementAvatars(groupement.id, ctx);
        const logo = await ctx.prisma.createGroupementLogo({
          idFichier: {
            create: { chemin: avatar.chemin, nomOriginal: avatar.nomOriginal, type: avatar.type },
          },
          groupement: { connect: { id: groupement.id } },
        });
        logger.info('LOGO CREATED', logo);
      }
    }
    return groupement;
  },
  updateGroupement: async (
    _,
    {
      id,
      nom,
      adresse1,
      adresse2,
      cp,
      ville,
      pays,
      telBureau,
      telMobile,
      mail,
      site,
      commentaire,
      nomResponsable,
      prenomResponsable,
      dateSortie,
      sortie,
      avatar,
      isReference,
    },
    ctx: Context,
  ) => {
    const groupement = await updateGroupement(
      {
        id,
        nom,
        adresse1,
        adresse2,
        cp,
        ville,
        pays,
        telBureau,
        telMobile,
        mail,
        site,
        commentaire,
        nomResponsable,
        prenomResponsable,
        dateSortie,
        sortie,
        isReference,
      },
      ctx,
    );
    if (groupement && groupement.id) {
      ctx.indexing.publishSave('groupements', groupement.id);

      if (avatar && avatar.chemin && avatar.nomOriginal && avatar.type) {
        // Create new avatar and delete the existing one
        await deleteGroupementAvatars(groupement.id, ctx);
        const logo = await ctx.prisma.createGroupementLogo({
          idFichier: {
            create: { chemin: avatar.chemin, nomOriginal: avatar.nomOriginal, type: avatar.type },
          },
          groupement: { connect: { id: groupement.id } },
        });
        logger.info('LOGO CREATED', logo);
      } else if (avatar !== undefined && avatar === null) {
        // Clear avatar
        await deleteGroupementAvatars(groupement.id, ctx);
      }
    }
    return groupement;
  },
  deleteGroupement: async (_, { id }, ctx: Context) => {
    const groupement = await deleteGroupement(id, ctx);
    if (groupement && groupement.id) {
      await ctx.indexing.publishDelete('groupements', groupement.id);
    }
    return groupement;
  },
  createGroupementLogo: async (_, { id, chemin, nomOriginal }, ctx: Context) => {
    return createGroupementLogo({ id, chemin, nomOriginal }, ctx);
  },
  deleteGroupementLogo: async (_, { chemin }, ctx: Context) => {
    return deleteGroupementLogo({ chemin }, ctx);
  },
};
