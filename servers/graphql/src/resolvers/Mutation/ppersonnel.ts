import { ApolloError } from 'apollo-server';
import { PpersonnelCreateInput } from '../../generated/prisma-client';
import { deleteSoftPpersonnel } from '../../repository/ppersonnel';
import { Context, PpersonnelInput } from '../../types';

export default {
  createUpdatePpersonnel: async (_: any, { input }: { input: PpersonnelInput }, ctx: Context) => {
    const {
      id,
      idPharmacie,
      idGroupement,
      civilite,
      nom,
      prenom,
      telBureau,
      telMobile,
      mail,
      estAmbassadrice,
      sortie,
      commentaire,
    } = input;

    const data: PpersonnelCreateInput = {
      idPharmacie: {
        connect: {
          id: idPharmacie,
        },
      },
      groupement: {
        connect: {
          id: idGroupement,
        },
      },
      civilite,
      nom,
      prenom,
      // TODO : contact
      // contact: {
      //   connect: {
      //     id,
      //   },
      // },
      estAmbassadrice,
      sortie,
      commentaire,
    };

    const result = await ctx.prisma
      .upsertPpersonnel({
        where: { id: id || '' },
        create: data,
        update: data,
      })
      .then(async created => {
        await ctx.indexing.publishSave('ppersonnels', created.id);
        return created;
      });

    return result;
  },
  clearPpersonnels: async (_: any, __: any, ctx: Context) => {
    const ppersonnels = await ctx.prisma.ppersonnels();
    ppersonnels.map(async ppersonnel => ctx.prisma.deletePpersonnel({ id: ppersonnel.id }));
    return true;
  },
  deleteSoftPpersonnel: async (_: any, { id }, ctx: Context) => {
    return deleteSoftPpersonnel({ id }, ctx);
  },
  deleteSoftPpersonnels: async (_: any, { ids }, ctx: Context) => {
    if (ids && ids.length) {
      return Promise.all(ids.map((id: string) => deleteSoftPpersonnel({ id }, ctx)));
    }
    return new ApolloError(
      `Erreur lors de la suppression des personnels pharmacies`,
      'DELETE_MULTIPLE_PPERSONNEL_ERROR',
    );
  },
};
