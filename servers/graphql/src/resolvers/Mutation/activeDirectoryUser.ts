import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';
import logger from '../../logging';

export default {
  createActiveDirectoryUser: async (_, { activeDirectoryId, userid }, ctx: Context) => {
    console.log('activeDirectoryId', activeDirectoryId, 'userId', userid);
    return ctx.prisma
      .createActiveDirectoryUser({
        activeDirectory: {
          connect: {
            id: activeDirectoryId,
          },
        },
        user: {
          connect: {
            id: userid,
          },
        },
      })
      .catch(err => {
        logger.error(err);
        throw new Error('Error when creating active directory user');
      });
  },
};
