import { Context } from '../../types';
import { createOperationViewer, deleteOperationViewer } from '../../repository/operationViewer';
import logger from '../../logging';
import { actualiteSeenByPharmacie } from '../../repository/actualite';

export default {
  createOperationViewer: async (parent, { idOperation, idPharmacie }, ctx: Context) => {
    return createOperationViewer(idOperation, idPharmacie, ctx).then(async operation => {
      if (operation) {
        logger.info(` BEGIN Mark seen pharmacie actu in OP :: ${idOperation}`);
        const actualite = await ctx.prisma.operation({ id: idOperation }).idActualite();

        if (actualite && actualite.id) {
          actualiteSeenByPharmacie(ctx, { id: actualite.id, idPharmacie });
        }
      }

      return operation;
    });
  },
  deleteOperationViewer: async (parent, { idOperation, idPharmacie }, ctx: Context) => {
    return deleteOperationViewer(idOperation, idPharmacie, ctx);
  },
};
