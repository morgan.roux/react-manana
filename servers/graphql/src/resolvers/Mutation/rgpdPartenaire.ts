import { createUpdateRgpdPartenaire } from '../../repository/rgpdPartenaire';
import { Context, RgpdPartenaireInput } from '../../types';

export default {
  createUpdateRgpdPartenaire: async (
    _: any,
    { input }: { input: RgpdPartenaireInput },
    ctx: Context,
  ) => {
    return createUpdateRgpdPartenaire(ctx, input);
  },
  deleteRgpdPartenaire: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.deleteRgpdPartenaire({ id });
  },
};
