import {
  createUpdateInformationLiaisonFichierJoint,
  softDeleteInformationLiaisonFichierJoint,
} from '../../repository/informationLiaisonFichierJoint';
import { Context, InformationLiaisonFichierJointInput } from '../../types';

export default {
  createUpdateInformationLiaisonFichierJoint: async (
    _: any,
    { input }: { input: InformationLiaisonFichierJointInput },
    ctx: Context,
  ) => {
    return createUpdateInformationLiaisonFichierJoint(ctx, input);
  },
  softDeleteInformationLiaisonFichierJoints: async (
    _: any,
    { ids }: { ids: string[] },
    ctx: Context,
  ) => {
    const informationLiaisonFichierJoints = await Promise.all(
      ids.map(async id => {
        return softDeleteInformationLiaisonFichierJoint(ctx, id);
      }),
    );
    return informationLiaisonFichierJoints;
  },
};
