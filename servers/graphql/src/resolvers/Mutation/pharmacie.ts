import { Context } from '../../types';
import { ApolloError } from 'apollo-server';
import logger from '../../logging';
import { createActiviteUser } from '../../repository/activiteUser';
import { createUpdatePharmacie, deleteSoftPharmacie } from '../../repository/pharmacie';

export default {
  changePharmacieStatus: async (_, { id, status }, ctx: Context) => {
    return ctx.prisma
      .updatePharmacie({
        where: { id },
        data: { sortie: status ? 0 : 1 },
      })
      .catch(err => {
        logger.error(`Erreur lors d'activation/desactivation du pharmacie: `, { err });
        return new ApolloError(
          `Erreur lors d'activation/desactivation du pharmacie : `,
          'ACTIVE_PHARMACIE_ERROR',
        );
      });
  },
  createUpdatePharmacie: async (
    _,
    {
      id,
      cip,
      nom,
      adresse1,
      adresse2,
      cp,
      ville,
      pays,
      longitude,
      latitude,
      nbEmploye,
      nbAssistant,
      nbAutreTravailleur,
      uga,
      commentaire,
      idGrossistes,
      idGeneriqueurs,
      contact,
      entreeSortie,
      satisfaction,
      segmentation,
      compta,
      informatique,
      achat,
      cap,
      concept,
      chiffreAffaire,
      digitales,
    },
    ctx: Context,
  ) => {
    return createUpdatePharmacie(ctx, {
      id,
      cip,
      nom,
      adresse1,
      adresse2,
      longitude,
      latitude,
      cp,
      ville,
      pays,
      nbEmploye,
      nbAssistant,
      nbAutreTravailleur,
      uga,
      commentaire,
      idGrossistes,
      idGeneriqueurs,
      contact,
      entreeSortie,
      satisfaction,
      segmentation,
      compta,
      informatique,
      achat,
      cap,
      concept,
      chiffreAffaire,
      digitales,
    });
  },

  deleteSoftPharmacie: async (_, { id }, ctx: Context) => {
    return deleteSoftPharmacie({ id }, ctx);
  },
  deleteSoftPharmacies: async (_, { ids }, ctx: Context) => {
    if (ids && ids.length) {
      return Promise.all(ids.map(id => deleteSoftPharmacie({ id }, ctx)));
    }
    return new ApolloError(
      `Erreur lors de la suppression des pharmacies`,
      'DELETE_MULTIPLE_PHARMACIE_ERROR',
    );
  },
};
