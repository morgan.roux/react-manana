import { Context, LaboratoireRepresentantInput } from '../../types';
import {
  createUpdateLaboratoireRepresentant,
  softDeleteLaboPartenaireRepresentant,
} from '../../repository/laboratoireRepresentant';

export default {
  createUpdateLaboratoireRepresentant: async (
    _: any,
    { input }: { input: LaboratoireRepresentantInput },
    ctx: Context,
  ) => {
    return createUpdateLaboratoireRepresentant(input, ctx);
  },
  softDeleteLaboratoireRepresentants: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteLaboPartenaireRepresentant(ctx, { id })));
  },
};
