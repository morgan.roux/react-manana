import {
  createUpdateIdeeOuBonnePratiqueClassification,
  softDeleteIdeeOuBonnePratiqueClassification,
} from '../../repository/ideeOuBonnePratiqueClassification';
import { Context, IdeeOuBonnePratiqueClassificationInput } from '../../types';

export default {
  createUpdateIdeeOuBonnePratiqueClassification: async (
    _: any,
    { input }: { input: IdeeOuBonnePratiqueClassificationInput },
    ctx: Context,
  ) => {
    return createUpdateIdeeOuBonnePratiqueClassification(ctx, input);
  },
  softDeleteIdeeOuBonnePratiqueClassifications: async (
    _: any,
    { ids }: { ids: string[] },
    ctx: Context,
  ) => {
    return Promise.all(ids.map(async id => softDeleteIdeeOuBonnePratiqueClassification(ctx, id)));
  },
};
