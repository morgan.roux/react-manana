import {
  createUpdateTodoActionType,
  softDeleteTodoActionType,
} from '../../repository/todoActionType';
import { Context, TodoActionTypeInput } from '../../types';

export default {
  createUpdateTodoActionType: async (
    _: any,
    { input }: { input: TodoActionTypeInput },
    ctx: Context,
  ) => {
    return createUpdateTodoActionType(input, ctx);
  },
  softDeleteTodoActionTypes: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteTodoActionType(ctx, { id })));
  },
};
