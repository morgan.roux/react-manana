import {
  createUpdatePartageFonctionCible,
  softDeletePartageFonctionCible,
} from '../../repository/partageFonctionCible';
import { Context, PartageFonctionCibleInput } from '../../types';

export default {
  createUpdatePartageFonctionCible: async (
    _: any,
    { input }: { input: PartageFonctionCibleInput },
    ctx: Context,
  ) => {
    return createUpdatePartageFonctionCible(input, ctx);
  },
  softDeletePartageFonctionCibles: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(id => softDeletePartageFonctionCible(ctx, id)));
  },
};
