import {
  createUpdatePartenaireRepresentant,
  softDeletePartenaireRepresentant,
} from '../../repository/partenaireRepresentant';
import { createActiviteUser } from '../../repository/activiteUser';
import { Context, PartenaireRepresentantInput } from '../../types';

export default {
  createUpdatePartenaireRepresentant: async (
    _: any,
    args: { input: PartenaireRepresentantInput },
    ctx: Context,
  ) => {
    return createUpdatePartenaireRepresentant(args.input, ctx).then(ressource => {
      createActiviteUser(
        {
          code: args.input.id ? 'C_PARTENAI_REREPRESENTANT' : 'E_PARTENAIRE_REPRESENTANT',
          nom: args.input.id ? 'CREATE PARTENAIRE_REPRESENTANT' : 'EDIT PARTENAIRE_REPRESENTANT',
          log: JSON.stringify({ id: ressource.id }),
        },
        ctx,
      );
      return ressource;
    });
  },
  softDeletePartenaireRepresentant: async (_: any, { id }: { id: string }, ctx: Context) => {
    return softDeletePartenaireRepresentant(ctx, { id });
  },
  softDeletePartenaireRepresentants: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeletePartenaireRepresentant(ctx, { id })));
  },
};
