import { createUpdateProject, softDeleteProject } from '../../repository/project';
import { createUpdateTodoParticipant } from '../../repository/todoParticipant';
import { AddParticipantsInput, Context, ProjectInput, RemoveParticipantInput } from '../../types';
import lodash from 'lodash';

export default {
  createUpdateProject: async (_, { input }: { input: ProjectInput }, ctx: Context) => {
    return createUpdateProject(ctx, input);
  },
  softDeleteProjects: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteProject(ctx, { id })));
  },
  addParticipantsToProject: async (
    _: any,
    { input }: { input: AddParticipantsInput },
    ctx: Context,
  ) => {
    const { idProject, idsParticipants } = input;

    if (!idsParticipants || (idsParticipants && !idsParticipants.length)) {
      await ctx.prisma.deleteManyTodoParticipants({
        project: {
          id: idProject,
        },
      });
    } else if (idsParticipants && idsParticipants.length) {
      const existParticipants = await ctx.prisma.todoParticipants({
        where: {
          project: {
            id: idProject,
          },
          isRemoved: false,
        },
      });

      const idsExist =
        existParticipants && existParticipants.length
          ? await Promise.all(
              existParticipants.map(async participant => {
                const userParticipant = await ctx.prisma
                  .todoParticipant({ id: participant.id })
                  .userParticipant();
                return userParticipant ? userParticipant.id : '';
              }),
            )
          : [];

      const idsToCreate = idsParticipants.filter(id => !idsExist.includes(id));
      const idsToDelete = lodash.differenceBy(idsExist, idsParticipants);

      console.log('idsExist', idsExist);
      console.log('idsParticipants', idsParticipants);
      console.log('idsToDelete', idsToDelete);

      if (idsToDelete && idsToDelete.length) {
        await ctx.prisma.deleteManyTodoParticipants({
          project: {
            id: idProject,
          },
          userParticipant: {
            id_in: idsToDelete,
          },
        });
      }

      if (idsToCreate && idsToCreate.length)
        await Promise.all(
          idsToCreate.map(async id => {
            await createUpdateTodoParticipant({ idProject, idUserParticipant: id }, ctx);
          }),
        );
    }
    const project = await ctx.prisma.project({ id: idProject });
    await ctx.indexing.publishSave('projects', project.id);

    const actions = await ctx.prisma.actions({
      where: { project: { id: idProject }, isRemoved: false },
    });

    if (actions && actions.length)
      await Promise.all(actions.map(action => ctx.indexing.publishSave('actions', action.id)));
    return project;
  },
  removeParticipantFromProject: async (
    _: any,
    { input }: { input: RemoveParticipantInput },
    ctx: Context,
  ) => {
    const { idProject, idParticipant } = input;

    const todoParticipants = await ctx.prisma.todoParticipants({
      where: { project: { id: idProject }, userParticipant: { id: idParticipant } },
    });

    if (todoParticipants && todoParticipants.length > 0) {
      await Promise.all(
        todoParticipants.map(async todoParticipant => {
          await createUpdateTodoParticipant(
            {
              id: todoParticipant.id,
              idProject,
              idUserParticipant: idParticipant,
              isRemoved: true,
            },
            ctx,
          );
        }),
      );
    }

    const project = await ctx.prisma.project({ id: idProject });
    await ctx.indexing.publishSave('projects', project.id);
    return project;
  },
};
