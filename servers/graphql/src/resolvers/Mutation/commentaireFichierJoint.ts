import {
  createUpdateCommentaireFichierJoint,
  softDeleteCommentaireFichierJoint,
} from '../../repository/commentaireFichierJoint';
import { Context, CommentaireFichierJointInput } from '../../types';

export default {
  createUpdateCommentaireFichierJoint: async (
    _: any,
    { input }: { input: CommentaireFichierJointInput },
    ctx: Context,
  ) => {
    return createUpdateCommentaireFichierJoint(input, ctx);
  },
  softDeleteCommentaireFichierJoints: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteCommentaireFichierJoint(ctx, { id })));
  },
};
