import { createUpdatePartagePharmacieCible } from '../../repository/partagePharmacieCible';
import { Context, PartagePharmacieCibleInput } from '../../types';

export default {
  createUpdatePartagePharmacieCible: async (
    _: any,
    { input }: { input: PartagePharmacieCibleInput },
    ctx: Context,
  ) => {
    return createUpdatePartagePharmacieCible(input, ctx);
  },
};
