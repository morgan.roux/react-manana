import { Context } from '../../types';
import {
  CreateHelloIdSso,
  UpdateHelloIdSso,
  DeleteHelloIdSso,
  AddApplicationToHelloidGroup,
  RemoveApplicationToHelloidGroup,
  CreateHelloIdGroup,
  DeleteHelloIDGroup,
} from '../../repository/helloidsso';

export default {
  createHelloIdSso: async (_, _args, ctx: Context) => {
    return CreateHelloIdSso(ctx, { ..._args });
  },
  updateHelloIdSso: async (_, _args, ctx: Context) => {
    return UpdateHelloIdSso(ctx, { ..._args });
  },
  deleteHelloIdSso: async (_, _args, ctx: Context) => {
    return DeleteHelloIdSso(ctx, { ..._args });
  },
  addApplicationToHelloidGroup: async (_, _args, ctx: Context) => {
    return AddApplicationToHelloidGroup(ctx, { ..._args });
  },
  removeApplicationToHelloidGroup: async (_, _args, ctx: Context) => {
    return RemoveApplicationToHelloidGroup(ctx, { ..._args });
  },
  createHelloIdGroup: async (_, _args, ctx: Context) => {
    return CreateHelloIdGroup(ctx, { ..._args });
  },
  deleteHelloIdGroupInfo: async (_, _args, ctx: Context) => {
    return DeleteHelloIDGroup({ ..._args }, ctx);
  },
};
