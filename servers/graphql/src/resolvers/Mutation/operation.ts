import { Context, OperationInput } from '../../types';
import {
  createUpdateOperation,
  markAsNotSeenOperation,
  markAsSeenOperation,
  deleteSoftOperation,
  publishOperation,
} from '../../repository/operation';
import logger from '../../logging';
import { createActiviteUser } from '../../repository/activiteUser';
import { WatchTime } from '../../services/time';
import {
  markAsSeenActualite,
  markAsNotSeenActualite,
  deleteSoftActualite,
} from '../../repository/actualite';
import { deleteAllNotifications } from '../../repository/notification';

const watch = new WatchTime();

export default {
  createUpdateOperation: async (_, args: OperationInput, ctx: Context) => {
    watch.start();
    const operation = await createUpdateOperation(args, ctx);

    logger.info('1 ===++++>');
    if (operation && operation.id) await publishOperation(ctx, operation.id);
    logger.info('2 ===++++>');

    watch.stop();
    watch.printElapsed(`Opération Commerciale: Temps mise pour la creation`);

    return operation;
  },
  deleteOperation: async (_, { id }, ctx: Context) => {
    const operation = await deleteSoftOperation(ctx, { id });

    if (operation && operation.id) {
      deleteAllNotifications(ctx, operation.id);
      const actualite = await ctx.prisma.operation({ id }).idActualite();

      if (actualite && actualite.id) deleteSoftActualite(ctx, { id: actualite.id });
    }

    return operation;
  },
  addPharmacieToOperation: (
    _,
    { idOperationPharmacie, idPharmacie, idOperation },
    ctx: Context,
  ) => {
    return ctx.prisma.createOperationPharmacieDetail({
      idOperation: { connect: { id: idOperation } },
      idOperationPharmacie: { connect: { id: idOperationPharmacie } },
      idPharmacie: { connect: { id: idPharmacie } },
    });
  },
  addArticleToOperation: (_, { idCanalArticle, idOperation }, ctx: Context) => {
    return ctx.prisma.createOperationArticle({
      idOperation: { connect: { id: idOperation } },
      produitCanal: { connect: { id: idCanalArticle } },
    });
  },
  changePharmacieInOperation: (_, { id, idPharmacie }, ctx: Context) => {
    return ctx.prisma.updateOperationPharmacieDetail({
      data: {
        idPharmacie: { connect: { id: idPharmacie } },
      },
      where: { id },
    });
  },
  changeArticleInOperation: (_, { id, idCanalArticle }, ctx: Context) => {
    return ctx.prisma.updateOperationArticle({
      data: {
        produitCanal: { connect: { id: idCanalArticle } },
      },
      where: { id },
    });
  },
  markAsNotSeenOperation: async (_, { id, idPharmacie }, ctx: Context) => {
    return markAsNotSeenOperation(ctx, { id, idPharmacie }).then(async operation => {
      if (operation) {
        logger.info(` BEGIN Mark as not seen Actu in OP :: ${id}`);
        const actualite = await ctx.prisma.operation({ id }).idActualite();

        if (actualite && actualite.id) {
          logger.info(` Mark not seen actualite :: ${actualite.id}`);
          markAsNotSeenActualite(ctx, { id: actualite.id, idPharmacie });
        }
      }

      return operation;
    });
  },
  markAsSeenOperation: async (_, { id }, ctx: Context) => {
    return markAsSeenOperation(ctx, { id }).then(async operation => {
      if (operation) {
        logger.info(` BEGIN Mark seen Actu in OP :: ${id}`);
        const actualite = await ctx.prisma.operation({ id }).idActualite();

        if (actualite && actualite.id) {
          logger.info(` Mark seen actualite :: ${actualite.id}`);
          markAsSeenActualite(ctx, { id: actualite.id });
        }
      }

      return operation;
    });
  },
};
