import { Context, MarcheInput } from '../../types';
import { createUpdateMarche, softDeleteMarche } from '../../repository/marche';

export default {
  createUpdateMarche: async (
    _,
    {
      id,
      nom,
      dateDebut,
      dateFin,
      marcheType,
      avecPalier,
      status,
      codeCanal,
      laboratoires,
      idsCanalArticle,
      groupeClients,
    }: MarcheInput,
    ctx: Context,
  ) => {
    return createUpdateMarche(ctx, {
      id,
      nom,
      dateDebut,
      dateFin,
      marcheType,
      avecPalier,
      status,
      codeCanal,
      laboratoires,
      idsCanalArticle,
      groupeClients,
    });
  },
  softDeleteMarche: async (_, { id }, ctx: Context) => {
    return softDeleteMarche(ctx, { id });
  },
};
