import { Context } from '../../types';
import {
  upsertActualite,
  sendNotificationActualite,
  markAsSeenActualite,
  markAsNotSeenActualite,
  actualiteSeenByPharmacie,
  deleteSoftActualite,
  operationByIDActualite,
} from '../../repository/actualite';
import { WatchTime } from '../../services/time';
import logger from '../../logging';
import { markAsSeenOperation, markAsNotSeenOperation } from '../../repository/operation';
import { createOperationViewer } from '../../repository/operationViewer';
import { deleteAllNotifications } from '../../repository/notification';
import { ActualiteFormInput } from '../../interfaces/actualite';

const watchTime = new WatchTime();

export default {
  createUpdateActualite: async (_, args: ActualiteFormInput, ctx: Context) => {
    watchTime.start();
    return upsertActualite(ctx, args).then(value => {
      watchTime.stop();
      watchTime.printElapsed(`[Actualite] mutation createUpdateActualite duration : `);
      return value;
    });
  },
  deleteActualite: async (_, { id }, ctx: Context) => {
    watchTime.start();
    return deleteSoftActualite(ctx, { id }).then(async actualite => {
      if (actualite && actualite.id) deleteAllNotifications(ctx, actualite.id);

      watchTime.stop();
      watchTime.printElapsed(`[Actualite] mutation deleteActualite duration : `);
      return actualite;
    });
  },
  sendNotificationActualiteToCible: async (_, { id, isUpdate }, ctx: Context) => {
    const isUpdated = isUpdate ? isUpdate : false;
    return sendNotificationActualite(ctx, id, isUpdated);
  },
  markAsNotSeenActualite: async (_, { id, idPharmacie }, ctx: Context) => {
    return markAsNotSeenActualite(ctx, { id, idPharmacie }).then(async actualite => {
      if (actualite) {
        logger.info(` BEGIN Mark not seen OP in Actu :: ${id}`);
        const operation = await operationByIDActualite(ctx, id);

        if (operation && operation.id) {
          logger.info(` Mark seen not operation :: ${operation.id}`);
          markAsNotSeenOperation(ctx, { id: operation.id, idPharmacie });
        }
      }

      return actualite;
    });
  },
  markAsSeenActualite: async (_, { id }, ctx: Context) => {
    return markAsSeenActualite(ctx, { id }).then(async actualite => {
      if (actualite) {
        logger.info(` BEGIN Mark seen OP in Actu :: ${id}`);
        const operation = await operationByIDActualite(ctx, id);

        if (operation && operation.id) {
          logger.info(` Mark seen operation :: ${operation.id}`);
          markAsSeenOperation(ctx, { id: operation.id });
        }
      }

      return actualite;
    });
  },
  actualiteSeenByPharmacie: async (_, { id, idPharmacie }, ctx: Context) => {
    return actualiteSeenByPharmacie(ctx, { id, idPharmacie }).then(async actualite => {
      if (actualite) {
        logger.info(` BEGIN Mark seen OP in Actu :: ${id}`);
        const operation = await operationByIDActualite(ctx, id);

        if (operation && operation.id) {
          logger.info(` Mark seen operation :: ${operation.id}`);
          createOperationViewer(operation.id, idPharmacie, ctx);
        }
      }

      return actualite;
    });
  },
};
