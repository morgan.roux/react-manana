import { MutationResolvers } from '../../generated/graphqlgen';
import Pharmacie from './pharmacie';
import Titulaire from './titulaire';
import Groupement from './groupement';
import Laboratoire from './laboratoire';
import Personnel from './personnel';
import User from './user';
import Ppersonnel from './ppersonnel';
import Partenaire from './partenaire';
import Commande from './commande';
import Panier from './panier';
import Operation from './operation';
import Notification from './notification';
import Comment from './comment';
import S3Urls from './s3urls';
import ProduitCanal from './produitCanal';
import Actualite from './actualite';
import Action from './action';
import Project from './project';
import Smyley from './smyley';
import Parameter from './parameter';
import OperationViewer from './operationViewer';
import Avatar from './avatar';
import ActiveDirectoryUser from './activeDirectoryUser';
import Helloidsso from './helloidsso';
import ActiveDirectory from './activeDirectory';
import Ftp from './ftp';
import SsoApplication from './ssoApplication';
import ExternalMapping from './externalMapping';
import Publicite from './publicite';
import SuiviPublicitaire from './suiviPublicitaire';
import Produi from './produit';
import Marche from './marche';
import Promotion from './promotion';
import Role from './role';
import GroupeClient from './groupeClient';
import Affectation from './affectation';
import Messagerie from './messagerie';
import Ressource from './ressource';
import PartenaireRepresentant from './partenaireRepresentant';
import PartenaireRepresentantAffectation from './partenaireRepresentantAffectation';
import Ticket from './ticket';
import Aide from './aide';
import TodoParticipant from './todoParticipant';
import TodoProjetFavoris from './todoProjetFavoris';
import TodoActionChangementStatus from './todoActionChangementStatus';
import TodoSection from './todoSection';
import TodoActionAttribution from './todoActionAttribution';
import TodoActionOrigine from './todoActionOrigine';
import TodoActionType from './todoActionType';
import TodoEtiquetteFavoris from './todoEtiquetteFavoris';
import TodoEtiquette from './todoEtiquette';
import TodoActionEtiquette from './todoActionEtiquette';
import CommentaireFichierJoint from './commentaireFichierJoint';
import Couleur from './couleur';
import ActionActivite from './actionActivite';
import TodoEtiquetteParticipant from './todoEtiquetteParticipant';
import InformationLiaison from './informationLiaison';
import InformationLiaisonFichierJoint from './informationLiaisonFichierJoint';
import GroupeAmis from './groupeAmis';
import GroupeAmisDetail from './groupeAmisDetail';
import Partage from './partage';
import PartageFonctionCible from './partageFonctionCible';
import PartagePharmacieCible from './partagePharmacieCible';
import PartageGroupeAmisCible from './partageGroupeAmisCible';
import IdeeOuBonnePratique from './ideeOuBonnePratique';
import IdeeOuBonnePratiqueClassification from './ideeOuBonnePratiqueClassification';
import IdeeOuBonnePratiqueFichierJoint from './ideeOuBonnePratiqueFichierJoint';
import IdeeOuBonnePratiqueLu from './ideeOuBonnePratiqueLu';
import Rgpd from './rgpd';
import RgpdAccueil from './rgpdAccueil';
import RgpdAccueilPlus from './rgpdAccueilPlus';
import RgpdAutorisation from './rgpdAutorisation';
import RgpdPartenaire from './rgpdPartenaire';
import RgpdHistorique from './rgpdHistorique';
import RgpdHistoriqueInfoPlus from './rgpdHistoriqueInfoPlus';
import LaboratoirePartenaire from './laboratoirePartenaire';
import LaboratoireRepresentant from './laboratoireRepresentant';
import LaboratoireRessource from './laboratoireRessource';
import Urgence from './urgence';

export const Mutation: MutationResolvers.Type | any = {
  ...MutationResolvers.defaultResolvers,
  ...Pharmacie,
  ...Titulaire,
  ...Groupement,
  ...Laboratoire,
  ...LaboratoirePartenaire,
  ...User,
  ...Personnel,
  ...Ppersonnel,
  ...Partenaire,
  ...Comment,
  ...Notification,
  ...Panier,
  ...Commande,
  ...Operation,
  ...S3Urls,
  ...ProduitCanal,
  ...Actualite,
  ...Action,
  ...Project,
  ...Smyley,
  ...Parameter,
  ...OperationViewer,
  ...Avatar,
  ...ActiveDirectoryUser,
  ...Publicite,
  ...SuiviPublicitaire,
  ...Helloidsso,
  ...ActiveDirectory,
  ...Ftp,
  ...SsoApplication,
  ...ExternalMapping,
  ...Produi,
  ...Marche,
  ...Promotion,
  ...Role,
  ...GroupeClient,
  ...Affectation,
  ...Messagerie,
  ...Ressource,
  ...PartenaireRepresentant,
  ...PartenaireRepresentantAffectation,
  ...Ticket,
  ...Aide,
  ...TodoParticipant,
  ...TodoProjetFavoris,
  ...TodoActionChangementStatus,
  ...TodoSection,
  ...TodoActionAttribution,
  ...TodoActionOrigine,
  ...TodoActionType,
  ...TodoEtiquetteFavoris,
  ...TodoEtiquette,
  ...TodoActionEtiquette,
  ...CommentaireFichierJoint,
  ...Couleur,
  ...ActionActivite,
  ...TodoEtiquetteParticipant,
  ...InformationLiaison,
  ...InformationLiaisonFichierJoint,
  ...GroupeAmis,
  ...GroupeAmisDetail,
  ...Partage,
  ...PartageFonctionCible,
  ...PartagePharmacieCible,
  ...IdeeOuBonnePratique,
  ...IdeeOuBonnePratiqueClassification,
  ...IdeeOuBonnePratiqueFichierJoint,
  ...IdeeOuBonnePratiqueLu,
  ...PartageGroupeAmisCible,
  ...Rgpd,
  ...RgpdAccueil,
  ...RgpdAccueilPlus,
  ...RgpdAutorisation,
  ...RgpdPartenaire,
  ...RgpdHistorique,
  ...RgpdHistoriqueInfoPlus,
  ...LaboratoireRepresentant,
  ...LaboratoireRessource,
  ...Urgence,
};
