import { AideCreateInput } from '../../generated/prisma-client';
import { AideInput, Context } from '../../types';

export default {
  createUpdateAide: async (_: any, { input }: { input: AideInput }, ctx: Context) => {
    console.log('input :>> ', input);
    const { id, title, description } = input;
    const aideData: AideCreateInput = {
      title,
      description,
      groupement: ctx.groupementId ? { connect: { id: ctx.groupementId } } : null,
      userCreation: ctx.userId ? { connect: { id: ctx.userId } } : null,
      userModification: ctx.userId ? { connect: { id: ctx.userId } } : null,
    };
    const aide = await ctx.prisma.upsertAide({
      where: { id: id || '' },
      create: aideData,
      update: aideData,
    });
    console.log('aide :>> ', aide);
    await ctx.indexing.publishSave('aides', aide.id);
    return aide;
  },
  softDeleteAides: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    const aides = await Promise.all(
      ids.map(async id => {
        const aide = await ctx.prisma.updateAide({ where: { id }, data: { isRemoved: true } });
        await ctx.indexing.publishSave('aides', aide.id);
        return aide;
      }),
    );
    return aides;
  },
};
//
