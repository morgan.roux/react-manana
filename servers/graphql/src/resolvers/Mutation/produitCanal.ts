import { Context } from '../../types';
import { addFile, deleteFile } from '../../repository/file';
import { createActiviteUser } from '../../repository/activiteUser';
import { softDeleteCanalArticle } from '../../repository/produitCanal';

export default {
  createArticlePhoto: async (_, { id, chemin, nomOriginal }, ctx: Context) => {
    return addFile(
      ctx,
      {
        table: 'produitPhoto',
        column: 'produit',
        id,
        chemin,
        nomOriginal,
        type: 'LOGO',
      },
      true,
    );
  },
  deleteArticlePhoto: async (_, { chemin }, ctx: Context) => {
    return deleteFile(ctx, {
      table: 'articlePhoto',
      chemin,
    });
  },
  softDeleteCanalArticle: async (_, { id }, ctx: Context) => {
    return softDeleteCanalArticle(ctx, { id });
  },
  softDeleteCanalArticles: async (_, { ids }: { ids: any[] }, ctx: Context) => {
    if (!ids || (ids && !ids.length)) return [];
    return Promise.all(ids.map(async (id: string) => await softDeleteCanalArticle(ctx, { id })));
  },
};
