import { createUpdatePartage, softDeletePartage } from '../../repository/partage';
import { Context, PartageInput } from '../../types';

export default {
  createUpdatePartage: async (_: any, { input }: { input: PartageInput }, ctx: Context) => {
    return createUpdatePartage(input, ctx);
  },
  softDeletePartages: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(id => softDeletePartage(ctx, id)));
  },
};
