import {
  createUpdateTodoActionEtiquette,
  softDeleteTodoActionEtiquette,
} from '../../repository/todoActionEtiquette';
import { Context, TodoActionEtiquetteInput } from '../../types';

export default {
  createUpdateTodoActionEtiquette: async (
    _: any,
    { input }: { input: TodoActionEtiquetteInput },
    ctx: Context,
  ) => {
    return createUpdateTodoActionEtiquette(input, ctx);
  },
  softDeleteTodoActionEtiquettes: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteTodoActionEtiquette(ctx, { id })));
  },
};
