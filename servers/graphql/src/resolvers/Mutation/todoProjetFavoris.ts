import {
  createUpdateTodoProjetFavoris,
  softDeleteTodoProjetFavoris,
} from '../../repository/todoProjetFavoris';
import { Context, TodoProjetFavorisInput } from '../../types';

export default {
  createUpdateTodoProjetFavoris: async (
    _: any,
    { input }: { input: TodoProjetFavorisInput },
    ctx: Context,
  ) => {
    return createUpdateTodoProjetFavoris(input, ctx);
  },
  softDeleteTodoProjetFavorises: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteTodoProjetFavoris(ctx, { id })));
  },
};
