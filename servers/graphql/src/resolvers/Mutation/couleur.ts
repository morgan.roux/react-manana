import { createUpdateCouleur, softDeleteCouleur } from '../../repository/couleur';
import { Context, CouleurInput } from '../../types';

export default {
  createUpdateCouleur: async (_: any, { input }: { input: CouleurInput }, ctx: Context) => {
    return createUpdateCouleur(input, ctx);
  },
  softDeleteCouleurs: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteCouleur(ctx, { id })));
  },
};
