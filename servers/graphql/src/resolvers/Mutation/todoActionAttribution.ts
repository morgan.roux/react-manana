import {
  createUpdateTodoActionAttribution,
  softDeleteTodoActionAttribution,
} from '../../repository/todoActionAttribution';
import { Context, TodoActionAttributionInput } from '../../types';

export default {
  createUpdateTodoActionAttribution: async (
    _: any,
    { input }: { input: TodoActionAttributionInput },
    ctx: Context,
  ) => {
    return createUpdateTodoActionAttribution(input, ctx);
  },
  softDeleteTodoActionAttributions: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteTodoActionAttribution(ctx, { id })));
  },
};
