import { Context } from '../../types';
import moment from 'moment';
import suiviPublicitaire from '../Query/suiviPublicitaire';

export default {
  createSuiviPublicitaire: async (_, { idPublicite }, ctx: Context) => {
    return ctx.prisma
      .createSuiviPublicitaire({
        idPublicite: {
          connect: { id: idPublicite },
        },
        idUser: {
          connect: { id: ctx.userId },
        },
        dateClick: moment().format(),
      })
      .then(async suivi => {
        if (suivi && suivi.id) await ctx.indexing.publishSave('suivipublicitaires', suivi.id);
        return suivi;
      });
  },
  incrementPubliciteViewCount: async (_, { idPublicite }, ctx: Context) => {
    const suiviPublicitaires = await ctx.prisma.suiviPublicitaires({
      where: { idPublicite: { id: idPublicite } },
    });
    const suivi = suiviPublicitaires && suiviPublicitaires[0];
    const now = moment().toISOString();
    return ctx.prisma
      .upsertSuiviPublicitaire({
        where: {
          id: suivi ? suivi.id : '',
        },
        create: {
          idPublicite: { connect: { id: idPublicite } },
          idUser: { connect: { id: ctx.userId } },
          dateClick: now,
          nbView: 1,
        },
        update: {
          nbView: (suivi && suivi.nbView && suivi.nbView + 1) || 1,
        },
      })
      .then(suiviPublicitaire => {
        ctx.indexing.publishSave('suivipublicitaires', suiviPublicitaire.id);
        return suiviPublicitaire;
      });
  },
  incrementPubliciteClickCount: async (_, { idPublicite }, ctx: Context) => {
    const suiviPublicitaires = await ctx.prisma.suiviPublicitaires({
      where: { idPublicite: { id: idPublicite } },
    });
    const suivi = suiviPublicitaires && suiviPublicitaires[0];
    const item = await ctx.prisma.publicite({ id: idPublicite }).idItem();
    const isOperation = item && item.codeItem && item.codeItem.startsWith('A');

    const now = moment().toISOString();
    return ctx.prisma
      .upsertSuiviPublicitaire({
        where: {
          id: suivi ? suivi.id : '',
        },
        create: {
          idPublicite: { connect: { id: idPublicite } },
          idUser: { connect: { id: ctx.userId } },
          dateClick: now,
          nbClick: 1,
          nbAccessOp: isOperation ? 1 : 0,
        },
        update: {
          nbClick: (suivi && suivi.nbClick && suivi.nbClick + 1) || 1,
          nbAccessOp: isOperation ? suivi.nbAccessOp + 1 : suivi.nbAccessOp,
        },
      })
      .then(suiviPublicitaire => {
        ctx.indexing.publishSave('suivipublicitaires', suiviPublicitaire.id);
        return suiviPublicitaire;
      });
  },
  incrementPubliciteOperationClickCount: async (_, { idPublicite }, ctx: Context) => {
    const suiviPublicitaires = await ctx.prisma.suiviPublicitaires({
      where: { idPublicite: { id: idPublicite } },
    });
    const now = moment().toISOString();
    if (suiviPublicitaires && suiviPublicitaires.length) {
      return ctx.prisma
        .updateSuiviPublicitaire({
          where: { id: suiviPublicitaires[0].id },
          data: {
            nbAccessOp: suiviPublicitaires[0].nbAccessOp + 1,
          },
        })
        .then(suiviPublicitaire => {
          ctx.indexing.publishSave('suivipublicitaires', suiviPublicitaire.id);
          return suiviPublicitaire;
        });
    } else {
      return ctx.prisma
        .createSuiviPublicitaire({
          idPublicite: { connect: { id: idPublicite } },
          idUser: { connect: { id: ctx.userId } },
          dateClick: now,
          nbAccessOp: 1,
        })
        .then(async suiviPublicitaire => {
          ctx.indexing.publishSave('suivipublicitaires', suiviPublicitaire.id);
          return suiviPublicitaire;
        });
    }
  },
};
