import {
  createUpdateTodoEtiquetteParticipant,
  softDeleteTodoEtiquetteParticipant,
} from '../../repository/todoEtiquetteParticipant';
import {
  AddEtiquetteParticipantsInput,
  Context,
  RemoveEtiquetteParticipantInput,
  TodoEtiquetteParticipantInput,
} from '../../types';
import lodash from 'lodash';

export default {
  createUpdateTodoEtiquetteParticipant: async (
    _: any,
    { input }: { input: TodoEtiquetteParticipantInput },
    ctx: Context,
  ) => {
    return createUpdateTodoEtiquetteParticipant(input, ctx);
  },
  softDeleteTodoEtiquetteParticipants: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteTodoEtiquetteParticipant(ctx, { id })));
  },
  addParticipantsToEtiquette: async (
    _: any,
    { input }: { input: AddEtiquetteParticipantsInput },
    ctx: Context,
  ) => {
    const { idEtiquette, idsParticipants } = input;
    if (!idsParticipants || (idsParticipants && !idsParticipants.length)) {
      await ctx.prisma.deleteManyTodoEtiquetteParticipants({
        etiquette: {
          id: idEtiquette,
        },
      });
    } else if (idsParticipants && idsParticipants.length) {
      const existParticipants = await ctx.prisma.todoEtiquetteParticipants({
        where: {
          etiquette: {
            id: idEtiquette,
          },
          isRemoved: false,
        },
      });

      const idsExist =
        existParticipants && existParticipants.length
          ? await Promise.all(
              existParticipants.map(async participant => {
                const userParticipant = await ctx.prisma
                  .todoParticipant({ id: participant.id })
                  .userParticipant();
                return userParticipant ? userParticipant.id : '';
              }),
            )
          : [];

      const idsToCreate = idsParticipants.filter(id => !idsExist.includes(id));
      const idsToDelete = lodash.differenceBy(idsExist, idsParticipants);

      console.log('idsExist', idsExist);
      console.log('idsParticipants', idsParticipants);
      console.log('idsToDelete', idsToDelete);

      if (idsToDelete && idsToDelete.length) {
        await ctx.prisma.deleteManyTodoEtiquetteParticipants({
          etiquette: {
            id: idEtiquette,
          },
          userParticipant: {
            id_in: idsToDelete,
          },
        });
      }

      if (idsToCreate && idsToCreate.length)
        await Promise.all(
          idsToCreate.map(async id => {
            await createUpdateTodoEtiquetteParticipant({ idEtiquette, idUserParticipant: id }, ctx);
          }),
        );
    }
    const todoEtiquette = await ctx.prisma.todoEtiquette({ id: idEtiquette });
    await ctx.indexing.publishSave('todoetiquettes', todoEtiquette.id);
    return todoEtiquette;
  },
  removeParticipantFromEtiquette: async (
    _: any,
    { input }: { input: RemoveEtiquetteParticipantInput },
    ctx: Context,
  ) => {
    const { idEtiquette, idParticipant } = input;

    const todoEtiquetteParticipants = await ctx.prisma.todoEtiquetteParticipants({
      where: { etiquette: { id: idEtiquette }, userParticipant: { id: idParticipant } },
    });

    if (todoEtiquetteParticipants && todoEtiquetteParticipants.length > 0) {
      await Promise.all(
        todoEtiquetteParticipants.map(async todoParticipant => {
          await createUpdateTodoEtiquetteParticipant(
            {
              id: todoParticipant.id,
              idEtiquette,
              idUserParticipant: idParticipant,
              isRemoved: true,
            },
            ctx,
          );
        }),
      );
    }

    const todoEtiquette = await ctx.prisma.todoEtiquette({ id: idEtiquette });
    await ctx.indexing.publishSave('todoetiquettes', todoEtiquette.id);
    return todoEtiquette;
  },
};
