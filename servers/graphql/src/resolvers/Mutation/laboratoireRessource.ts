import { Context, LaboratoireRessourceInput } from '../../types';
import {
  createUpdateLaboratoireRessource,
  softDeleteLaboratoireRessource,
} from '../../repository/laboratoireRessource';

export default {
  createUpdateLaboratoireRessource: async (
    _: any,
    { input }: { input: LaboratoireRessourceInput },
    ctx: Context,
  ) => {
    return createUpdateLaboratoireRessource(input, ctx);
  },
  softDeleteLaboratoireRessources: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    console.log('ids', ids);

    return Promise.all(ids.map(async id => softDeleteLaboratoireRessource(ctx, { id })));
  },
};
