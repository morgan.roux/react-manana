import { expect } from '@jest/globals'
import { createUpdatePharmacieVariables } from '../../../test/fixtures/pharmacie';
import { DO_CREATE_UPDATE_PHARMACIE, DO_DELETE_SOFT_PHARMACIES } from '../../../test/graphql/Pharmacie/mutation';


const expectPharmacie = (expected, result) => {
    expect(expected.nom).toEqual(result.nom);
    expect(expected.cip).toEqual(result.cip);
    expect(expected.adresse1).toEqual(result.adresse1);
    expect(expected.adresse2).toEqual(result.adresse2);
    expect(expected.cp).toEqual(result.cp);
    expect(expected.ville).toEqual(result.ville);
    expect(expected.pays).toEqual(result.pays);
    expect(expected.longitude).toEqual(result.longitude);
    expect(expected.latitude).toEqual(result.latitude);
    expect(expected.nbEmploye).toEqual(result.nbEmploye);
    expect(expected.nbAssistant).toEqual(result.nbAssistant);
    expect(expected.nbAutreTravailleur).toEqual(result.nbAutreTravailleur);
    expect(expected.uga).toEqual(result.uga);
    expect(expected.commentaire).toEqual(result.commentaire);
    expect(expected.contact.adresse1).toEqual(result.contact.adresse1);
    expect(expected.contact.adresse2).toEqual(result.contact.adresse2);
    expect(expected.contact.cp).toEqual(result.contact.cp);
    expect(expected.contact.ville).toEqual(result.contact.ville);
    expect(expected.contact.pays).toEqual(result.contact.pays);
    for (const [i, grossiste] of (result.idGrossistes).entries()) {
        expect(expected.grossistes[i].id).toEqual(grossiste);
    }
    for (const [i, generiqueur] of (result.idGeneriqueurs).entries()) {
        expect(expected.generiqueurs[i].id).toEqual(generiqueur);
    }
    // entreeSortie
    expect(expected.entreeSortie.contrat ? expected.entreeSortie.contrat.id : expected.entreeSortie.contrat).toEqual(result.entreeSortie.idContrat);
    expect(expected.entreeSortie.concurrent ? expected.entreeSortie.concurrent.id : expected.entreeSortie.concurrent).toEqual(result.entreeSortie.idConcurent);
    expect(expected.entreeSortie.concurrentAncien ? expected.entreeSortie.concurrentAncien.id : expected.entreeSortie.concurrentAncien).toEqual(result.entreeSortie.idConcurentAncien);
    expect(new Date(expected.entreeSortie.dateEntree)).toEqual(result.entreeSortie.dateEntree);
    expect(expected.entreeSortie.pharmacieMotifEntree ? expected.entreeSortie.pharmacieMotifEntree.id : expected.entreeSortie.pharmacieMotifEntree).toEqual(result.entreeSortie.idMotifEntree);
    expect(expected.entreeSortie.commentaireEntree).toEqual(result.entreeSortie.commentaireEntree);
    expect(new Date(expected.entreeSortie.dateSortieFuture)).toEqual(result.entreeSortie.dateSortieFuture);
    expect(expected.entreeSortie.pharmacieMotifSortieFuture ? expected.entreeSortie.pharmacieMotifSortieFuture.id : expected.entreeSortie.pharmacieMotifSortieFuture).toEqual(result.entreeSortie.idMotifSortieFuture);
    expect(expected.entreeSortie.commentaireSortieFuture).toEqual(result.entreeSortie.commentaireSortieFuture);
    expect(new Date(expected.entreeSortie.dateSortie)).toEqual(result.entreeSortie.dateSortie);
    expect(expected.entreeSortie.pharmacieMotifSortie ? expected.entreeSortie.pharmacieMotifSortie.id : expected.entreeSortie.pharmacieMotifSortie).toEqual(result.entreeSortie.idMotifSortie);
    expect(expected.entreeSortie.commentaireSortie).toEqual(result.entreeSortie.commentaireSortie);
    // expect(expected.entreeSortie).toEqual(result.entreeSortie.dateSignature);
    // satisfaction
    expect(new Date(expected.satisfaction.dateSaisie)).toEqual(result.satisfaction.dateSaisie);
    expect(expected.satisfaction.commentaire).toEqual(result.satisfaction.commentaire);
    expect(expected.satisfaction.smyley ? expected.satisfaction.smyley.id : expected.satisfaction.smyley).toEqual(result.satisfaction.idSmyley);
    // segmentation 
    expect(expected.segmentation.typologie ? expected.segmentation.typologie.id : expected.segmentation.typologie).toEqual(result.segmentation.idTypologie);
    expect(expected.segmentation.trancheCA ? expected.segmentation.trancheCA.id : expected.segmentation.trancheCA).toEqual(result.segmentation.idTrancheCA);
    expect(expected.segmentation.qualite ? expected.segmentation.qualite.id : expected.segmentation.qualite).toEqual(result.segmentation.idQualite);
    expect(expected.segmentation.contrat ? expected.segmentation.contrat.id : expected.segmentation.contrat).toEqual(result.segmentation.idContrat);
    expect(new Date(expected.segmentation.dateSignature)).toEqual(result.segmentation.dateSignature);
    // compta
    expect(expected.compta.siret).toEqual(result.compta.siret);
    expect(expected.compta.codeERP).toEqual(result.compta.codeERP);
    expect(expected.compta.ape).toEqual(result.compta.ape);
    expect(expected.compta.tvaIntra).toEqual(result.compta.tvaIntra);
    expect(expected.compta.rcs).toEqual(result.compta.rcs);
    expect(expected.compta.contactCompta).toEqual(result.compta.contactCompta);
    expect(expected.compta.chequeGrp).toEqual(result.compta.chequeGrp);
    expect(expected.compta.valeurChequeGrp).toEqual(result.compta.valeurChequeGrp);
    expect(expected.compta.commentaireChequeGrp).toEqual(result.compta.commentaireChequeGrp);
    expect(expected.compta.droitAccesGrp).toEqual(result.compta.droitAccesGrp);
    expect(expected.compta.valeurChequeAccesGrp).toEqual(result.compta.valeurChequeAccesGrp);
    expect(expected.compta.commentaireChequeAccesGrp).toEqual(result.compta.commentaireChequeAccesGrp);
    expect(expected.compta.structureJuridique).toEqual(result.compta.structureJuridique);
    expect(expected.compta.denominationSociale).toEqual(result.compta.denominationSociale);
    expect(expected.compta.telCompta).toEqual(result.compta.telCompta);
    expect(expected.compta.modifStatut).toEqual(result.compta.modifStatut);
    expect(expected.compta.raisonModif).toEqual(result.compta.raisonModif);
    expect(new Date(expected.compta.dateModifStatut)).toEqual(result.compta.dateModifStatut);
    expect(expected.compta.nomBanque).toEqual(result.compta.nomBanque);
    expect(expected.compta.banqueGuichet).toEqual(result.compta.banqueGuichet);
    expect(expected.compta.banqueCompte).toEqual(result.compta.banqueCompte);
    expect(expected.compta.banqueRib).toEqual(result.compta.banqueRib);
    expect(expected.compta.banqueCle).toEqual(result.compta.banqueCle);
    expect(new Date(expected.compta.dateMajRib)).toEqual(result.compta.dateMajRib);
    expect(expected.compta.iban).toEqual(result.compta.iban);
    expect(expected.compta.swift).toEqual(result.compta.swift);
    expect(new Date(expected.compta.dateMajIban)).toEqual(result.compta.dateMajIban);
    // informatique
    expect(expected.informatique.logiciel ? expected.informatique.logiciel.id : expected.informatique.logiciel).toEqual(result.informatique.idLogiciel);
    expect(expected.informatique.numVersion).toEqual(result.informatique.numVersion);
    expect(new Date(expected.informatique.dateLogiciel)).toEqual(result.informatique.dateLogiciel);
    expect(expected.informatique.nbrePoste).toEqual(result.informatique.nbrePoste);
    expect(expected.informatique.nbreComptoir).toEqual(result.informatique.nbreComptoir);
    expect(expected.informatique.nbreBackOffice).toEqual(result.informatique.nbreBackOffice);
    expect(expected.informatique.nbreBureau).toEqual(result.informatique.nbreBureau);
    expect(expected.informatique.commentaire).toEqual(result.informatique.commentaire);
    expect(expected.informatique.automate1 ? expected.informatique.automate1.id : expected.informatique.automate1).toEqual(result.informatique.idAutomate1);
    expect(new Date(expected.informatique.dateInstallation1)).toEqual(result.informatique.dateInstallation1);
    expect(expected.informatique.automate2 ? expected.informatique.automate2.id : expected.informatique.automate2).toEqual(result.informatique.idAutomate2);
    expect(new Date(expected.informatique.dateInstallation2)).toEqual(result.informatique.dateInstallation2);
    expect(expected.informatique.automate3 ? expected.informatique.automate3.id : expected.informatique.automate3).toEqual(result.informatique.idAutomate3);
    expect(new Date(expected.informatique.dateInstallation3)).toEqual(result.informatique.dateInstallation3);
    // achat
    expect(new Date(expected.achat.dateDebut)).toEqual(result.achat.dateDebut);
    expect(new Date(expected.achat.dateFin)).toEqual(result.achat.dateFin);
    expect(expected.achat.identifiantAchatCanal).toEqual(result.achat.identifiantAchatCanal);
    expect(expected.achat.commentaire).toEqual(result.achat.commentaire);
    expect(expected.achat.canal ? expected.achat.canal.id : expected.achat.canal).toEqual(result.achat.idCanal);
    // cap
    expect(expected.cap.cap).toEqual(result.cap.cap);
    expect(new Date(expected.cap.dateDebut)).toEqual(result.cap.dateDebut);
    expect(new Date(expected.cap.dateFin)).toEqual(result.cap.dateFin);
    expect(expected.cap.identifiantCap).toEqual(result.cap.identifiantCap);
    expect(expected.cap.commentaire).toEqual(result.cap.commentaire);
    // concept
    expect(expected.concept.fournisseurMobilier ? expected.concept.fournisseurMobilier.id : expected.concept.fournisseurMobilier).toEqual(result.concept.idFournisseurMobilier);
    expect(new Date(expected.concept.dateInstallConcept)).toEqual(result.concept.dateInstallConcept);
    expect(expected.concept.avecConcept).toEqual(result.concept.avecConcept);
    // expect(expected.concept.concept ? expected.concept.concept.id : expected.concept.concept).toEqual(result.concept.idConcept);
    expect(expected.concept.avecFacade).toEqual(result.concept.avecFacade);
    expect(new Date(expected.concept.dateInstallFacade)).toEqual(result.concept.dateInstallFacade);
    expect(expected.concept.enseigniste ? expected.concept.enseigniste.id : expected.concept.enseigniste).toEqual(result.concept.idEnseigniste);
    expect(expected.concept.conformiteFacade).toEqual(result.concept.conformiteFacade);
    expect(expected.concept.facade ? expected.concept.facade.id : expected.concept.facade).toEqual(result.concept.idFacade);
    expect(expected.concept.surfaceTotale).toEqual(result.concept.surfaceTotale);
    expect(expected.concept.surfaceVente).toEqual(result.concept.surfaceVente);
    expect(expected.concept.SurfaceVitrine).toEqual(result.concept.SurfaceVitrine);
    expect(expected.concept.volumeLeaflet).toEqual(result.concept.volumeLeaflet);
    expect(expected.concept.nbreLineaire).toEqual(result.concept.nbreLineaire);
    expect(expected.concept.nbreVitrine).toEqual(result.concept.nbreVitrine);
    expect(expected.concept.otcLibAcces).toEqual(result.concept.otcLibAcces);
    // expect(expected.concept.leaflet? expected.concept.leaflet.id : expected.concept.leaflet).toEqual(result.concept.idLeaflet);
    expect(expected.concept.commentaire).toEqual(result.concept.commentaire);
    // statCA
    expect(expected.statCA.exercice).toEqual(result.chiffreAffaire.exercice);
    expect(new Date(expected.statCA.dateDebut)).toEqual(result.chiffreAffaire.dateDebut);
    expect(new Date(expected.statCA.dateFin)).toEqual(result.chiffreAffaire.dateFin);
    expect(expected.statCA.caTTC).toEqual(result.chiffreAffaire.caTTC);
    expect(expected.statCA.caHt).toEqual(result.chiffreAffaire.caHt);
    expect(expected.statCA.caTVA1).toEqual(result.chiffreAffaire.caTVA1);
    expect(expected.statCA.tauxTVA1).toEqual(result.chiffreAffaire.tauxTVA1);
    expect(expected.statCA.caTVA2).toEqual(result.chiffreAffaire.caTVA2);
    expect(expected.statCA.tauxTVA2).toEqual(result.chiffreAffaire.tauxTVA2);
    expect(expected.statCA.caTVA3).toEqual(result.chiffreAffaire.caTVA3);
    expect(expected.statCA.tauxTVA3).toEqual(result.chiffreAffaire.tauxTVA3);
    expect(expected.statCA.caTVA4).toEqual(result.chiffreAffaire.caTVA4);
    expect(expected.statCA.tauxTVA4).toEqual(result.chiffreAffaire.tauxTVA4);
    expect(expected.statCA.caTVA5).toEqual(result.chiffreAffaire.caTVA5);
    expect(expected.statCA.tauxTVA5).toEqual(result.chiffreAffaire.tauxTVA5);
    // digitale
    /* if (expected.digitales.length) {
        for (const [i, digitale] of (result.digitales).entries()) {
            expect(expected.digitales[i].servicePharmacie ? expected.digitales[i].servicePharmacie.id : expected.digitales[i].servicePharmacie).toEqual(digitale.idServicePharmacie);
            expect(expected.digitales[i].pharmaciePestataire ? expected.digitales[i].pharmaciePestataire.id : expected.digitales[i].pharmaciePestataire).toEqual(digitale.idPrestataire);
            expect(expected.digitales[i].flagService).toEqual(digitale.flagService);
            expect(expected.digitales[i].dateInstallation).toEqual(digitale.dateInstallation);
            expect(expected.digitales[i].url).toEqual(digitale.url);
        }
    } */
};

export const pharmacieToTest = async (graphQLClient: any, print: any) => {
    const pharmacieVariables = await createUpdatePharmacieVariables();
    const pharmacieEditVariables = await createUpdatePharmacieVariables();
    let pharmacie;
    let pharmacieEdit;
    let deletedPharmacie;
    // create pharmacie
    pharmacie = await graphQLClient.request(print(DO_CREATE_UPDATE_PHARMACIE), pharmacieVariables);
    expect(typeof pharmacie.createUpdatePharmacie.id === "string").toBeTruthy();
    expectPharmacie(pharmacie.createUpdatePharmacie, pharmacieVariables);

    // update pharmacie 
    pharmacieEdit = await graphQLClient.request(print(DO_CREATE_UPDATE_PHARMACIE), { id: pharmacie.createUpdatePharmacie.id, ...pharmacieEditVariables });
    expect(typeof pharmacieEdit.createUpdatePharmacie.id === "string").toBeTruthy();
    expectPharmacie(pharmacieEdit.createUpdatePharmacie, pharmacieEditVariables);

    // delet pharmacie
    deletedPharmacie = await graphQLClient.request(print(DO_DELETE_SOFT_PHARMACIES), { ids: [pharmacieEdit.createUpdatePharmacie.id] });
    expect(deletedPharmacie.deleteSoftPharmacies[0].id).toBeTruthy();
    expect(deletedPharmacie.deleteSoftPharmacies[0].sortie).toEqual(1);

};