require('dotenv').config();
import { print } from 'graphql';
import { GraphQLClient, request } from 'graphql-request';

import { USER_SIGNIN } from '../../../test/graphql/Authentication/mutation';
import { GET_GROUPEMENTS } from '../../../test/graphql/Groupement/query';
import { GET_PHARMACIES } from '../../../test/graphql/Pharmacie/query';
import { authenticationVariables } from '../../../test/fixtures/authentication';

import { actualiteToTest } from './actualite';
import { groupementToTest } from './groupement';
import { personnelToTest } from './personnel';
import { promotionToTest } from './promotion';
import { articleToTest } from './article';
import { operationToTest } from './operation';
import { pharmacieToTest } from './pharmacie';

describe('test with Jest', () => {
    let graphQLClient;

    beforeEach(async () => {
        const variablesAuthentication = authenticationVariables();
        const userId = await request(
            `${process.env.API_URL_JEST}`,
            print(USER_SIGNIN),
            variablesAuthentication,
        ).then((item: any) => item.login.accessToken);
        const groupementId = await request(
            `${process.env.API_URL_JEST}`,
            print(GET_GROUPEMENTS),
        ).then((item: any) => item.groupements[0].id);

        const pharmacieId = await request(
            `${process.env.API_URL_JEST}`,
            print(GET_PHARMACIES),
        ).then((item: any) => item.pharmacies[0].id);
        graphQLClient = new GraphQLClient(`${process.env.API_URL_JEST}`, {
            headers: {
                authorization: `Bearer ${userId}`,
                idGroupement: `${groupementId}`,
                idPharmacie: `${pharmacieId}`,
            },

        });
    });

    test('Actualite', async done => {
        try {
            await actualiteToTest(graphQLClient, print);

        } catch (error) {
            console.log('>>>>>>>>>>>>>>>>>>>>>>>>ERROR>>>>>>>>>>>>>>', error);
            done.fail(error);
        }
        done();
    });

    test('Groupement', async done => {
        try {
            await groupementToTest(graphQLClient, print);
        } catch (error) {
            console.log('>>>>>>>>>>>>>>>>>>>>>>>>ERROR>>>>>>>>>>>>>>', error);
            done.fail(error);
        }
        done();
    });

    test('Personnel', async done => {
        try {
            await personnelToTest(graphQLClient, print);
        } catch (error) {
            console.log('>>>>>>>>>>>>>>>>>>>>>>>>ERROR>>>>>>>>>>>>>>', error);
            done.fail(error);
        }
        done();
    });

    test('Promotion', async done => {
        try {
            await promotionToTest(graphQLClient, print);
        } catch (error) {
            console.log('>>>>>>>>>>>>>>>>>>>>>>>>ERROR>>>>>>>>>>>>>>', error);
            done.fail(error);
        }
        done();
    }, 20000);

    test('OpCommmerciale', async done => {
        try {
            await operationToTest(graphQLClient, print);
        } catch (error) {
            console.log('>>>>>>>>>>>>>>>>>>>>>>>>ERROR>>>>>>>>>>>>>>', error);
            done.fail(error);
        }
        done();
    }, 30000);
    test('Article', async done => {
        try {
            await articleToTest(graphQLClient, print);
        } catch (error) {
            console.log('>>>>>>>>>>>>>>>>>>>>>>>>ERROR>>>>>>>>>>>>>>', error);
            done.fail(error);
        }
        done();
    }, 20000);

    test('Pharmacie', async done => {
        try {
            await pharmacieToTest(graphQLClient, print);
        } catch (error) {
            console.log('>>>>>>>>>>>>>>>>>>>>>>>>ERROR>>>>>>>>>>>>>>', error);
            done.fail(error);
        }
        done();
    }, 20000);

})