import { createUpdatePersonnelVariables } from '../../../test/fixtures/personnel';
import { DO_CREATE_PERSONNEL, DO_DELETE_PERSONNELS } from '../../../test/graphql/Personnel/mutation';

const expectPersonnel = (expected, result) => {
    expect(expected.sortie).toEqual(result.sortie);
    expect(expected.civilite).toEqual(result.civilite);
    expect(expected.nom).toEqual(result.nom);
    expect(expected.prenom).toEqual(result.prenom);
    expect(expected.sexe).toEqual(result.sexe);
    expect(expected.commentaire).toEqual(result.commentaire);
    expect(expected.service.code).toEqual(result.codeService);
    expect(expected.contact.adresse1).toEqual(result.contact.adresse1);

};

export const personnelToTest = async (graphQLClient: any, print: any) => {
    const personnelToCreate = createUpdatePersonnelVariables();
    const personnelToUpdate = createUpdatePersonnelVariables();
    let personnel;
    let updatePersonnel;
    let deletedPersonnel;

    // create Personnel
    personnel = await graphQLClient.request(
        print(DO_CREATE_PERSONNEL),
        personnelToCreate,
    );
    expect(typeof personnel.createUpdatePersonnel.id === 'string').toBeTruthy();
    expectPersonnel(personnel.createUpdatePersonnel, personnelToCreate);

    // update Personnel
    updatePersonnel = await graphQLClient.request(
        print(DO_CREATE_PERSONNEL),
        {
            id: personnel.createUpdatePersonnel.id,
            ...personnelToUpdate,
        },
    );
    expect(updatePersonnel.createUpdatePersonnel).toBeTruthy();
    expectPersonnel(updatePersonnel.createUpdatePersonnel, personnelToUpdate);

    // delete
    deletedPersonnel = await graphQLClient.request(
        print(DO_DELETE_PERSONNELS),
        {
            ids: [...personnel.createUpdatePersonnel.id]
        }
    );
    expect(deletedPersonnel.deletePersonnels).toBeTruthy();

}