import { actuDataWithCible, actuDataWithGlobaliteTrue } from '../../../test/fixtures/actualite';
import { DO_CREATE_AND_UPDATE_ACTUALITE, DO_DELETE_ACTUALITE } from '../../../test/graphql/Actualite/mutation';

const expectActualite = (expected, result) => {
    expect(expected.item.code).toEqual(result.codeItem);
    expect(expected.origine.code).toEqual(result.codeOrigine);
    expect(expected.description).toEqual(result.description);
    expect(expected.niveauPriorite).toEqual(result.niveauPriorite);
    expect(expected.libelle).toEqual(result.libelle);
    expect(new Date(expected.dateDebut)).toEqual(result.dateDebut);
    expect(new Date(expected.dateFin)).toEqual(result.dateFin);
    expect(expected.fichierPresentations[0].type).toEqual(result.fichierPresentations[0].type);
    expect(expected.fichierPresentations[0].nomOriginal).toEqual(result.fichierPresentations[0].nomOriginal);
    expect(expected.fichierPresentations[0].chemin).toEqual(result.fichierPresentations[0].chemin);
}


export const actualiteToTest = async (graphQLClient: any, print: any) => {
    const actualiteEditWithCibleVariables = await actuDataWithCible();
    let actualite;
    let actualiteEditWithCible;
    let deletedActualite;

    // create actu
    actualite = await graphQLClient.request(print(DO_CREATE_AND_UPDATE_ACTUALITE), actuDataWithGlobaliteTrue);
    expect(typeof actualite.createUpdateActualite.id === "string").toBeTruthy();
    expectActualite(actualite.createUpdateActualite, actuDataWithGlobaliteTrue);

    // update actu with cible
    actualiteEditWithCible = await graphQLClient.request(print(DO_CREATE_AND_UPDATE_ACTUALITE), { id: actualite.createUpdateActualite.id, ...actualiteEditWithCibleVariables });
    expect(typeof actualiteEditWithCible.createUpdateActualite.id === "string").toBeTruthy();
    expectActualite(actualiteEditWithCible.createUpdateActualite, actualiteEditWithCibleVariables);

    // delet actu
    deletedActualite = await graphQLClient.request(print(DO_DELETE_ACTUALITE), { id: actualiteEditWithCible.createUpdateActualite.id });
    expect(deletedActualite.deleteActualite.id).toBeTruthy();

}