import { expect } from '@jest/globals'

import { createUpdatePromotionVariables } from '../../../test/fixtures/promotion';
import { DO_CREATE_UPDATE_PROMOTION, DO_DELETE_PROMOTION } from '../../../test/graphql/Promotion/mutation'

const expectPromotion = (expected, result) => {
    expect(expected.nom).toEqual(result.nom);
    expect(new Date(expected.dateDebut)).toEqual(result.dateDebut);
    expect(new Date(expected.dateFin)).toEqual(result.dateFin);
    expect(expected.promotionType).toEqual(result.promotionType);
    expect(expected.status).toEqual(result.status);
    expect(expected.commandeCanal.code).toEqual(result.codeCanal);
    // if (result.idsCanalArticle && result.idsCanalArticle.length) {
    //     for (const [index, idCanalArticle] of (result.idsCanalArticle).entries()) {
    //         expect(expected.canalArticles[index].id).toEqual(idCanalArticle);

    //     }
    // }
    if (result.groupeClients && result.groupeClients.length) {
        for (const [index, groupeClient] of (result.groupeClients).entries()) {
            expect(expected.groupeClients[index].groupeClient.codeGroupe).toEqual(groupeClient.codeGroupeClient);
            if (groupeClient.remiseDetails && groupeClient.remiseDetails.length) {
                for (const [i, remiseDetail] of (groupeClient.remiseDetails).entries()) {
                    expect(expected.groupeClients[index].remise.remiseDetails[i].quantiteMin).toEqual(remiseDetail.quantiteMin);
                    expect(expected.groupeClients[index].remise.remiseDetails[i].quantiteMax).toEqual(remiseDetail.quantiteMax);
                    expect(expected.groupeClients[index].remise.remiseDetails[i].pourcentageRemise).toEqual(remiseDetail.pourcentageRemise);
                    expect(expected.groupeClients[index].remise.remiseDetails[i].remiseSupplementaire).toEqual(remiseDetail.remiseSupplementaire);
                }
            }


        }
    }

};

export const promotionToTest = async (graphQLClient: any, print: any) => {
    const promotionVariables = await createUpdatePromotionVariables();
    const promotionEditVariables = await createUpdatePromotionVariables();
    let promotion;
    let promotionEdit;
    let deletedPromotion;

    // create promotion
    promotion = await graphQLClient.request(print(DO_CREATE_UPDATE_PROMOTION), promotionVariables);
    expect(typeof promotion.createUpdatePromotion.id === "string").toBeTruthy();
    expectPromotion(promotion.createUpdatePromotion, promotionVariables);

    // update promotion 
    promotionEdit = await graphQLClient.request(print(DO_CREATE_UPDATE_PROMOTION), { id: promotion.createUpdatePromotion.id, ...promotionEditVariables });
    expect(typeof promotionEdit.createUpdatePromotion.id === "string").toBeTruthy();
    expectPromotion(promotionEdit.createUpdatePromotion, promotionEditVariables);

    // delet promotion
    deletedPromotion = await graphQLClient.request(print(DO_DELETE_PROMOTION), { id: promotionEdit.createUpdatePromotion.id });
    expect(deletedPromotion.softDeletePromotion.id).toBeTruthy();
    expect(deletedPromotion.softDeletePromotion.isRemoved).toBeTruthy();

};