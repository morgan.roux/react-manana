import { expect } from '@jest/globals';
import { createUpdateArticleVariables } from '../../../test/fixtures/article';
import { CREATE_UPDATE_ARTICLE } from '../../../test/graphql/Article/mutation';

const expectArticle = (expected, result) => {
    expect(expected.nomArticle).toEqual(result.libelle);
    expect(expected.typeCodeReferent).toEqual(result.typeCodeReferent);
    expect(expected.codeReference).toEqual(result.codeReference);
    expect(expected.pxAchatArticle).toEqual(result.pxAchatArticle);
    expect(expected.pxVenteArticle).toEqual(result.pxVenteArticle);
    expect(new Date(expected.dateSuppression)).toEqual(result.dateSuppression);
    expect(expected.generiqArticle).toEqual(result.generiqArticle);
    expect(expected.tva.codeTva).toEqual(result.codeTva);
    expect(expected.tableau.codeTableau).toEqual(result.codeTableau);
    expect(expected.marge.codeMarge).toEqual(result.codeMarge);
    expect(expected.forme.codeForme).toEqual(result.codeForme);
    expect(expected.stockage.codeStockage).toEqual(result.codeStockage);
    expect(expected.rbtArticle.codeRbt).toEqual(result.codeRbtArticle);
    expect(expected.acte.codeActe).toEqual(result.codeActe);
    expect(expected.laboratoire.code).toEqual(result.codeLaboratoire);
    expect(expected.famille.codeFamille).toEqual(result.codeFamille);
    if (result.fichierImage) {
        expect(expected.articlePhoto.fichier.type).toEqual(result.fichierImage.type);
        expect(expected.articlePhoto.fichier.nomOriginal).toEqual(result.fichierImage.nomOriginal);
        expect(expected.articlePhoto.fichier.chemin).toEqual(result.fichierImage.chemin);
    }

};

export const articleToTest = async (graphQLClient: any, print: any) => {
    const articleVariables = await createUpdateArticleVariables();
    const articleEditVariables = await createUpdateArticleVariables();
    let article;
    let articleEdit;

    // create article
    article = await graphQLClient.request(print(CREATE_UPDATE_ARTICLE), articleVariables);
    expect(typeof article.createUpdateArticle.id === "string").toBeTruthy();
    expectArticle(article.createUpdateArticle, articleVariables);

    // // update article 
    articleEdit = await graphQLClient.request(print(CREATE_UPDATE_ARTICLE), { id: article.createUpdateArticle.id, ...articleEditVariables });
    expect(typeof articleEdit.createUpdateArticle.id === "string").toBeTruthy();
    expectArticle(articleEdit.createUpdateArticle, articleEditVariables);


};