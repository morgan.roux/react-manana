import { createUpdateOperationVariables } from "../../../test/fixtures/opCommerciale";
import { DO_CREATE_OPERATION, DO_DELETE_OPERATION } from "../../../test/graphql/OperationCommerciale/mutation";


const expectOperation = (expected, result) => {
    expect(expected.libelle).toEqual(result.libelle);
    expect(expected.niveauPriorite).toEqual(result.niveauPriorite);
    expect(expected.description).toEqual(result.description);
    expect((`${new Date(expected.dateDebut)}`).slice(0, 10)).toEqual((`${result.dateDebut}`).slice(0, 10));
    expect((`${new Date(expected.dateFin)}`).slice(0, 10)).toEqual((`${result.dateFin}`).slice(0, 10));
    expect(expected.nbMoyenLigne).toEqual(result.nbMoyenLigne);
    expect(expected.nbPharmacieCommande).toEqual(result.nbPharmacieCommande);
    expect(expected.caMoyenPharmacie).toEqual(result.caMoyenPharmacie);
    expect(expected.accordCommercial).toEqual(result.accordCommercial);
    expect(expected.item.code).toEqual(result.codeItem);
    expect(expected.laboratoire.id).toEqual(result.idLaboratoire);
    expect(expected.commandeType.code).toEqual(result.typeCommande);
    expect(expected.commandeCanal.code).toEqual(result.idCanalCommande);
    expect(expected.fichierPresentations[0].type).toEqual(result.fichierPresentations[0].type);
    expect(expected.fichierPresentations[0].nomOriginal).toEqual(result.fichierPresentations[0].nomOriginal);
    expect(expected.fichierPresentations[0].chemin).toEqual(result.fichierPresentations[0].chemin);

};

export const operationToTest = async (graphQLClient: any, print: any) => {
    const operationToCreate = await createUpdateOperationVariables();
    const operationToUpdate = await createUpdateOperationVariables();
    let operation;
    let updateOperation;
    let deletedOperation;

    // create Operation
    operation = await graphQLClient.request(
        print(DO_CREATE_OPERATION),
        operationToCreate,
    );
    expect(typeof operation.createUpdateOperation.id === 'string').toBeTruthy();
    expectOperation(operation.createUpdateOperation, operationToCreate);

    // update Operation
    updateOperation = await graphQLClient.request(
        print(DO_CREATE_OPERATION),
        {
            idOperation: operation.createUpdateOperation.id,
            ...operationToUpdate,
        },
    );
    expect(updateOperation.createUpdateOperation).toBeTruthy();
    expectOperation(updateOperation.createUpdateOperation, operationToUpdate);

    // delete
    deletedOperation = await graphQLClient.request(
        print(DO_DELETE_OPERATION),
        {
            id: updateOperation.createUpdateOperation.id
        }
    );
    expect(deletedOperation.deleteOperation).toBeTruthy();

}