import {
  createGroupementVariables,
  createGroupementLogoVariables,
} from '../../../test/fixtures/groupement';
import {
  DO_CREATE_GROUPEMENT,
  DO_UPDATE_GROUPEMENT,
  DO_DELETE_GROUPEMENT,
  DO_CREATE_GROUPEMENT_LOGO,
  DO_DELETE_GROUPEMENT_LOGO,
} from '../../../test/graphql/Groupement/mutation';

const expectGroupement = (expected, result) => {
  expect(expected.nom).toEqual(result.nom);
  expect(expected.adresse1).toEqual(result.adresse1);
  expect(expected.adresse2).toEqual(result.adresse2);
  expect(expected.cp).toEqual(result.cp);
  expect(expected.ville).toEqual(result.ville);
  expect(expected.pays).toEqual(result.pays);
  expect(expected.telBureau).toEqual(result.telBureau);
  expect(expected.telMobile).toEqual(result.telMobile);
  expect(expected.mail).toEqual(result.mail);
  expect(expected.site).toEqual(result.site);
  expect(expected.commentaire).toEqual(result.commentaire);
  expect(expected.nomResponsable).toEqual(result.nomResponsable);
  expect(expected.prenomResponsable).toEqual(result.prenomResponsable);
};

export const groupementToTest = async (graphQLClient: any, print: any) => {
  const groupementToCreate = createGroupementVariables();
  const groupementToUpdate = createGroupementVariables();
  const groupementLogoToUpadate = createGroupementLogoVariables();

  let groupement;
  let updatedGroupement;
  let createdGroupementLogo;
  let deletedGroupement;
  let deletedGroupementLogo;

  // 1- should groupement created
  groupement = await graphQLClient.request(
    print(DO_CREATE_GROUPEMENT),
    groupementToCreate,
  );

  expect(typeof groupement.createGroupement.id === 'string').toBeTruthy();
  expectGroupement(groupement.createGroupement, groupementToCreate);

  // 2- should update created groupement
  updatedGroupement = await graphQLClient.request(print(DO_UPDATE_GROUPEMENT), {
    id: groupement.createGroupement.id,
    ...groupementToUpdate,
  });
  expect(updatedGroupement.updateGroupement).toBeTruthy();
  expectGroupement(updatedGroupement.updateGroupement, groupementToUpdate);

  // 3- should update created groupement logo
  createdGroupementLogo = await graphQLClient.request(
    print(DO_CREATE_GROUPEMENT_LOGO),
    {
      id: groupement.createGroupement.id,
      ...groupementLogoToUpadate,
    },
  );
  expect(createdGroupementLogo.createGroupementLogo).toBeTruthy();

  // 4- should delete groupement logo

  deletedGroupementLogo = await graphQLClient.request(
    print(DO_DELETE_GROUPEMENT_LOGO),
    {
      chemin: createdGroupementLogo.createGroupementLogo.chemin,
    },
  );
  expect(deletedGroupementLogo.deleteGroupementLogo).toBeTruthy();

  // 5- should delete created groupement
  deletedGroupement = await graphQLClient.request(print(DO_DELETE_GROUPEMENT), {
    id: groupement.createGroupement.id,
  });
  expect(deletedGroupement.deleteGroupement).toBeTruthy();

} 
