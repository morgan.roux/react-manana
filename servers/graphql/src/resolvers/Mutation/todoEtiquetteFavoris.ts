import {
  createUpdateTodoEtiquetteFavoris,
  softDeleteTodoEtiquetteFavoris,
} from '../../repository/todoEtiquetteFavoris';
import { Context, TodoEtiquetteFavorisInput } from '../../types';

export default {
  createUpdateTodoEtiquetteFavoris: async (
    _: any,
    { input }: { input: TodoEtiquetteFavorisInput },
    ctx: Context,
  ) => {
    return createUpdateTodoEtiquetteFavoris(input, ctx);
  },
  softDeleteTodoEtiquetteFavorises: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteTodoEtiquetteFavoris(ctx, { id })));
  },
};
