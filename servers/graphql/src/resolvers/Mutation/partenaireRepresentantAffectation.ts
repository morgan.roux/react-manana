import { MutationResolvers } from '../../generated/graphqlgen';
import { createUpdatePartenaireRepresentantAffectation } from '../../repository/partenaireRepresentantAffectation';
import { Context } from '../../types';

export default {
  createUpdatePartenaireRepresentantAffectation: async (
    _: any,
    { input }: MutationResolvers.ArgsCreateUpdatePartenaireRepresentantAffectation,
    ctx: Context,
  ) => {
    return createUpdatePartenaireRepresentantAffectation({ input }, ctx);
  },
};
