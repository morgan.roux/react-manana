import { createUpdateActionActivite } from '../../repository/actionActivite';
import { createComment } from '../../repository/comment';
import { CommentInput, Context } from '../../types';

export default {
  createComment: async (_: any, { input }: { input: CommentInput }, ctx: Context) => {
    const { codeItem, idItemAssocie } = input;
    // Save action activite (if codeItem === "TODO")
    if (codeItem === 'TODO') {
      createUpdateActionActivite(ctx, {
        activiteType: 'COMMENT',
        idAction: idItemAssocie,
        idUserSource: ctx.userId,
      });
    }
    return createComment(input, ctx);
  },
  deleteComment: async (_: any, { id }, ctx: Context) => {
    const deleted = await ctx.prisma.updateComment({ where: { id }, data: { isRemoved: true } });
    await ctx.indexing.publishSave('produitcanals', id);
    return deleted;
  },
};
