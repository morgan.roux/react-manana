import { Context } from '../../types';
import { createActiviteUser } from '../../repository/activiteUser';
import { createUpdateAvatar } from '../../repository/avatar';
import { ApolloError } from 'apollo-server';
import { deleteS3File } from '../../services/file';

export default {
  createUpdateAvatar: async (_, { id, description, codeSexe, fichier }, ctx: Context) => {
    // An avatar shoud be owned by a valid groupement
    return createUpdateAvatar(ctx, { id, description, codeSexe, fichier });
  },
  deleteAvatars: async (_, { ids }: { ids: string[] }, ctx: Context) => {
    const avatars = await ctx.prisma.avatars({ where: { id_in: ids } });

    if (!avatars || (avatars && !avatars.length)) {
      return new ApolloError(`Aucune avatar a supprimé`, 'NO_AVATAR_TO_DELETE');
    }

    const idAvatars = avatars.map(avatar => avatar.id);
    const fichiers = await ctx.prisma.fichiers({ where: { avatar: { id_in: idAvatars } } });

    console.log('fichiers :>>', fichiers);

    const descriptionsAvatars = await fichiers.reduce(async (results, fichier) => {
      const avatar = await ctx.prisma.fichier({ id: fichier.id }).avatar();
      console.log('avatar :>> ', avatar);

      const isUsedByUser = await ctx.prisma.$exists.userPhoto({
        idFichier: { id: fichier.id },
      });

      console.log('isUsedByUser :>> ', isUsedByUser);

      const descriptions = await results;
      console.log('descriptions :>> ', descriptions);

      if (avatar && isUsedByUser) return [...descriptions, avatar.description];
      return [...descriptions];
    }, Promise.resolve([]));

    console.log('descriptionsAvatars :>> ', descriptionsAvatars);

    if (descriptionsAvatars && descriptionsAvatars.length) {
      return new ApolloError(
        `Ces avatars sont déjà utilisés par des utilisateurs, impossible de les supprimés : ${descriptionsAvatars.join(
          ' - ',
        )}`,
        'AVATAR_ALREADY_USED',
      );
    }

    await Promise.all(
      fichiers.map(async fichier => {
        await deleteS3File(fichier.chemin);
        return ctx.prisma.deleteFichier({ id: fichier.id });
      }),
    );

    await ctx.prisma.deleteManyAvatars({ id_in: idAvatars });

    await Promise.all(idAvatars.map(id => ctx.indexing.publishDelete('avatars', id)));

    return avatars;
  },
};
