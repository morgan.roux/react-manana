import { createUpdateRgpdHistoriqueInfoPlus } from '../../repository/rgpdHistoriqueInfoPlus';
import { Context, RgpdHistoriqueInfoPlusInput } from '../../types';

export default {
  createUpdateRgpdHistoriqueInfoPlus: async (
    _: any,
    { input }: { input: RgpdHistoriqueInfoPlusInput },
    ctx: Context,
  ) => {
    return createUpdateRgpdHistoriqueInfoPlus(ctx, input);
  },
  deleteRgpdHistoriqueInfoPlus: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.deleteRgpdHistoriqueInfoPlus({ id });
  },
};
