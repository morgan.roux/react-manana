import { ApolloError } from 'apollo-server';
import { last } from 'lodash';
import {
  saveActionStatus,
  softDeleteAction,
  updateActionDateDebutFin,
  upsertAction,
} from '../../repository/action';
import { createUpdateActionActivite } from '../../repository/actionActivite';
import { createUpdateTodoActionAttribution } from '../../repository/todoActionAttribution';
import {
  ActionInput,
  AssignOrUnAssignActionInput,
  Context,
  UpdateActionStatusInput,
  UpdateActionDateDebutFinInput,
} from '../../types';
import { changementStatutActionTodoItemAssocie } from '../../sequelize/MigrationUtil';
import { federationPublishIndex } from '../../sequelize/shared/federation-reindex';

export default {
  createUpdateAction: async (_: any, { input }: { input: ActionInput }, ctx: Context) => {
    return upsertAction(ctx, input);
  },
  softDeleteActions: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteAction(ctx, { id })));
  },
  assignActionToUser: async (
    _: any,
    { input }: { input: AssignOrUnAssignActionInput },
    ctx: Context,
  ) => {
    const { idAction, idUserDestination } = input;
    await createUpdateTodoActionAttribution(
      {
        idAction,
        idUserDestination,
        idUserSource: ctx.userId,
        idGroupement: ctx.groupementId,
        idPharmacie: ctx.pharmacieId,
      },
      ctx,
    );

    // Save action activit
    createUpdateActionActivite(ctx, {
      activiteType: 'ASSIGN_USER' as any,
      idAction,
      idUserSource: ctx.userId,

      idUserDestination,
    });

    const action = await ctx.prisma.action({ id: idAction });
    await Promise.all([
      ctx.indexing.publishSave('actions', action.id),
      federationPublishIndex('todoaction', 'SAVE', action.id),
    ]);
    return action;
  },
  assignActionUsers: async (
    _: any,
    { idAction, idUserDestinations }: { idAction: string; idUserDestinations: string[] },
    ctx: Context,
  ) => {
    await Promise.all(
      idUserDestinations.map(async idUserDestination => {
        const attributions = await ctx.prisma.todoActionAttributions({
          where: {
            action: { id: idAction },
            userDestination: {
              id: idUserDestination,
            },
            isRemoved: false,
          },
        });
        if ((attributions && !attributions.length) || !attributions)
          createUpdateTodoActionAttribution(
            {
              idAction,
              idUserDestination,
              idUserSource: ctx.userId,
              idGroupement: ctx.groupementId,
              idPharmacie: ctx.pharmacieId,
            },
            ctx,
          );
      }),
    );

    const action = await ctx.prisma.action({ id: idAction });
    await Promise.all([
      ctx.indexing.publishSave('actions', action.id),
      federationPublishIndex('todoaction', 'SAVE', action.id),
    ]);
    return action;
  },
  unAssignUserInAction: async (
    _: any,
    { input }: { input: AssignOrUnAssignActionInput },
    ctx: Context,
  ) => {
    const { idAction, idUserDestination } = input;

    const todoActionAttributions = await ctx.prisma.todoActionAttributions({
      where: { action: { id: idAction }, userDestination: { id: idUserDestination } },
    });

    if (todoActionAttributions && todoActionAttributions.length > 0) {
      await Promise.all(
        todoActionAttributions.map(async todoActionAttribution => {
          await createUpdateTodoActionAttribution(
            {
              id: todoActionAttribution.id,
              idAction,
              idUserDestination,
              idUserSource: ctx.userId,
              isRemoved: true,
              idGroupement: ctx.groupementId,
              idPharmacie: ctx.pharmacieId,
            },
            ctx,
          );
        }),
      );
    }
    // Save action activite
    createUpdateActionActivite(ctx, {
      activiteType: 'UNASSIGN_USER' as any,
      idAction,
      idUserSource: ctx.userId,
      idUserDestination,
    });

    const action = await ctx.prisma.action({ id: idAction });
    await Promise.all([
      ctx.indexing.publishSave('actions', action.id),
      federationPublishIndex('todoaction', 'SAVE', action.id),
    ]);
    return action;
  },
  unAssignActionUsers: async (
    _: any,
    { idAction, idUserDestinations }: { idAction: string; idUserDestinations: string[] },
    ctx: Context,
  ) => {
    const todoActionAttributions = await ctx.prisma.todoActionAttributions({
      where: { action: { id: idAction }, userDestination: { id_in: idUserDestinations } },
    });

    if (todoActionAttributions && todoActionAttributions.length > 0) {
      await Promise.all(
        todoActionAttributions.map(async todoActionAttribution => {
          const source = await ctx.prisma
            .todoActionAttribution({ id: todoActionAttribution.id })
            .userSource();
          const destination = await ctx.prisma
            .todoActionAttribution({ id: todoActionAttribution.id })
            .userDestination();
          await createUpdateTodoActionAttribution(
            {
              id: todoActionAttribution.id,
              idAction,
              idUserDestination: destination.id,
              idUserSource: source.id,
              isRemoved: true,
              idGroupement: ctx.groupementId,
              idPharmacie: ctx.pharmacieId,
            },
            ctx,
          );
        }),
      );
    }

    const action = await ctx.prisma.action({ id: idAction });
    await Promise.all([
      ctx.indexing.publishSave('actions', action.id),
      federationPublishIndex('todoaction', 'SAVE', action.id),
    ]);
    return action;
  },
  updateManyActions: async (_: any, { inputs }: { inputs: ActionInput[] }, ctx: Context) => {
    return Promise.all(
      inputs.map(input => {
        if (!input.id) throw new ApolloError('ID is require', 'MISSING_ID');
        return upsertAction(ctx, input);
      }),
    );
  },
  updateActionStatus: async (
    _: any,
    { input }: { input: UpdateActionStatusInput },
    ctx: Context,
  ) => {
    const { idAction, status } = input;
    const action = await ctx.prisma.action({ id: idAction });
    if (action) {
      // Save actions activites
      let oldStatus: any;
      const actionChangementStatus = await ctx.prisma.todoActionChangementStatuses({
        where: { action: { id: idAction } },
        orderBy: 'dateCreation_ASC',
      });
      if (actionChangementStatus && actionChangementStatus.length > 0) {
        oldStatus = await ctx.prisma
          .todoActionChangementStatus({
            id: last(actionChangementStatus).id,
          })
          .status();
      }

      if (status && oldStatus !== status) {
        await createUpdateActionActivite(ctx, {
          activiteType: 'CHANGE_STATUS' as any,
          idAction,
          idUserSource: ctx.userId,
          oldStatus,
          newStatus: status,
        });
      }
      await saveActionStatus(ctx, action.id, status);
      changementStatutActionTodoItemAssocie(action.id, status, ctx.userId, ctx.groupementId);
      await Promise.all([
        ctx.indexing.publishSave('actions', action.id),
        federationPublishIndex('todoaction', 'SAVE', action.id),
      ]);
    }

    return action;
  },
  updateActionDateDebutFin: (
    _: any,
    { input }: { input: UpdateActionDateDebutFinInput },
    ctx: Context,
  ) => {
    return updateActionDateDebutFin(ctx, input);
  },
  updateActionDateDebutFins: (
    _: any,
    { inputs }: { inputs: UpdateActionDateDebutFinInput[] },
    ctx: Context,
  ) => {
    return Promise.all(inputs.map(input => updateActionDateDebutFin(ctx, input)));
  },
  removeActionAssignations: async (_: any, { idAction }: { idAction: string }, ctx: Context) => {
    const action = await ctx.prisma.action({ id: idAction });
    if (action) {
      await ctx.prisma.deleteManyTodoActionAttributions({ action: { id: idAction } });
      await Promise.all([
        ctx.indexing.publishSave('actions', action.id),
        federationPublishIndex('todoaction', 'SAVE', action.id),
      ]);
    }
    return action;
  },
};
