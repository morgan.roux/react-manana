import { Context } from '../../types';
import bcrypt = require('bcryptjs');
import {
  loginUser,
  resetPassword,
  sendMailForgotPassword,
  updatePassword,
  createUserCollaborateurOfficine,
  createUserGroupement,
  createUserPartenaireLaboratoire,
  createUserPartenaireService,
  createUserPresidentRegion,
  createUserTitulaire,
  updateUserStatus,
  sendCreatedUserEmail,
  updateUserPassword,
  deleteUserPhotos,
  createUpdateUserPhoto,
} from '../../repository/user';
import { deleteFile } from '../../repository/file';
import { saveUserIp } from '../../repository/connexion';
import logger from '../../logging';
import { ApolloError } from 'apollo-server';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  login: async (_, { login, password, ip }, ctx: Context) => {
    return loginUser(ctx, { login, password, ip }).then(accessToken => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'LOGIN',
          nom: 'LOGIN',
        },
        ctx,
      );

      return accessToken;
    });
  },
  resetPassword: async (_, { password, token, ip }, ctx: Context) => {
    return resetPassword(ctx, { newPassword: password, token }).then(async accessToken => {
      const login = await ctx.redis.get(token);
      if (login && ip) {
        const user = await ctx.prisma
          .users({
            where: { login },
          })
          .then(users => (users && users.length ? users[0] : null));
        if (user && user.id) {
          saveUserIp(ctx, {
            statut: 'AUTHORIZED',
            ip,
            idUser: user.id,
            appareil: null,
            emplacement: null,
          });
        }
      }
      ctx.redis.del(token);

      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'RPASSWORD',
          nom: 'RESET PASSWORD',
        },
        ctx,
      );

      return accessToken;
    });
  },
  forgotPassword: async (_, { login }, ctx: Context) => {
    return sendMailForgotPassword(ctx, { login }).then(accessToken => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'FPASSWORD',
          nom: 'FORGOTPASSWORD',
        },
        ctx,
      );

      return accessToken;
    });
  },
  createUserGroupement: async (
    _,
    {
      idGroupement,
      id,
      email,
      login,
      role,
      userId,
      day,
      month,
      year,
      status,
      userPhoto,
      codeTraitements,
    },
    ctx: Context,
  ) => {
    return createUserGroupement(
      {
        idGroupement,
        id,
        email,
        login,
        role,
        userId,
        day,
        month,
        year,
        status,
        userPhoto,
        codeTraitements,
      },
      ctx,
    ).then(value => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'CUSERGROUPEMENT',
          nom: 'CREATE USER GROUPEMENT',
        },
        ctx,
      );

      return value;
    });
  },
  createUserPresidentRegion: async (
    _,
    {
      idGroupement,
      id,
      idPharmacie,
      email,
      login,
      userId,
      day,
      month,
      year,
      userPhoto,
      codeTraitements,
    },
    ctx: Context,
  ) => {
    return createUserPresidentRegion(
      {
        idGroupement,
        id,
        idPharmacie,
        email,
        login,
        userId,
        day,
        month,
        year,
        userPhoto,
        codeTraitements,
      },
      ctx,
    ).then(created => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'CUSERPRESIDENTREGION',
          nom: 'CREATE USER PRESIDENT REGION',
        },
        ctx,
      );

      return created;
    });
  },
  createUserTitulaire: async (
    _,
    {
      idGroupement,
      id,
      email,
      login,
      idPharmacie,
      userId,
      day,
      month,
      year,
      userPhoto,
      codeTraitements,
    },
    ctx: Context,
  ) => {
    return createUserTitulaire(
      {
        idGroupement,
        id,
        email,
        login,
        idPharmacie,
        userId,
        day,
        month,
        year,
        userPhoto,
        codeTraitements,
      },
      ctx,
    ).then(created => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'CUSERTITULAIRE',
          nom: 'CREATE USER TITULAIRE',
        },
        ctx,
      );

      return created;
    });
  },
  createUserCollaborateurOfficine: async (
    _,
    {
      idGroupement,
      id,
      email,
      login,
      idPharmacie,
      role,
      userId,
      day,
      month,
      year,
      userPhoto,
      codeTraitements,
    },
    ctx: Context,
  ) => {
    return createUserCollaborateurOfficine(
      {
        idGroupement,
        id,
        email,
        login,
        idPharmacie,
        role,
        userId,
        day,
        month,
        year,
        userPhoto,
        codeTraitements,
      },
      ctx,
    ).then(created => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'CUSERCOLLABORATEUROFFICINE',
          nom: 'CREATE USER COLLABORATEUR OFFICINE',
        },
        ctx,
      );

      return created;
    });
  },
  createUserPartenaireLaboratoire: async (
    _,
    { idGroupement, id, email, login, userId, day, month, year, userPhoto, codeTraitements },
    ctx: Context,
  ) => {
    return createUserPartenaireLaboratoire(
      { idGroupement, id, email, login, userId, day, month, year, userPhoto, codeTraitements },
      ctx,
    ).then(created => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'CUSERPARTENAIRELABORATOIRE',
          nom: 'CREATE USER PARTENAIRE LABORATOIRE',
        },
        ctx,
      );

      return created;
    });
  },
  createUserPartenaireService: async (
    _,
    { idGroupement, id, email, login, day, month, year, userPhoto, codeTraitements, idPharmacie },
    ctx: Context,
  ) => {
    return createUserPartenaireService(
      { idGroupement, id, email, login, day, month, year, userPhoto, codeTraitements, idPharmacie },
      ctx,
    ).then(created => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'CUSERPARTENAIRESERVICE',
          nom: 'CREATE USER PARTENAIRE SERVICE',
        },
        ctx,
      );

      return created;
    });
  },
  updatePassword: async (_, { lastPassword, newPassword }, ctx: Context) => {
    return updatePassword(ctx, { lastPassword, newPassword }).then(success => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'UPASSWORD',
          nom: 'UPDATE PASSWORD',
        },
        ctx,
      );

      return success;
    });
  },
  updateUserStatus: async (_, { id, status }, ctx: Context) => {
    return updateUserStatus(ctx, { id, status }).then(user => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'UUSERSTATUS',
          nom: 'UPDATE USER STATUS',
        },
        ctx,
      );

      return user;
    });
  },
  updateUserPhoto: async (_, { userPhoto }, ctx: Context) => {
    if (userPhoto && userPhoto.chemin && userPhoto.type && userPhoto.nomOriginal) {
      return createUpdateUserPhoto(ctx, ctx.userId, userPhoto);
    } else if (userPhoto !== undefined && userPhoto === null) {
      await deleteUserPhotos(ctx.userId, ctx).then(async _ => {
        /**
         * Sauvegarde activite de l'utilisateur
         */
        createActiviteUser(
          {
            code: 'DUSERPHOTOS',
            nom: 'DELETE USER PHOTOS',
          },
          ctx,
        );
        await ctx.indexing.publishSave('users', ctx.userId);
      });
    }
    return null;
  },
  deleteUserPhoto: async (_, { chemin }, ctx: Context) => {
    const fichiers = await ctx.prisma.fichiers({ where: { chemin } });
    if (!fichiers || (fichiers && !fichiers.length)) {
      return new ApolloError('Pas de photo à supprimé', 'ERROR_DELETE_USER_PHOTO');
    }

    const userPhotos = await ctx.prisma.userPhotos({
      where: { idFichier: { id_in: fichiers.map(fichier => fichier.id) } },
      first: 1,
    });

    return userPhotos && userPhotos.length
      ? ctx.prisma
          .userPhoto({ id: userPhotos[0].id })
          .idFichier()
          .then(async toDeleted => {
            const user = await ctx.prisma.userPhoto({ id: userPhotos[0].id }).idUser();
            await ctx.prisma.deleteUserPhoto({
              id: userPhotos[0].id,
            });
            await ctx.prisma.deleteFichier({ id: toDeleted.id });
            /**
             *
             * Sauvegarde activite de l'utilisateur
             *
             */
            createActiviteUser(
              {
                code: 'DUSERPHOTOS',
                nom: 'DELETE USER PHOTOS',
              },
              ctx,
            );
            await ctx.indexing.publishSave('users', user.id);
            return toDeleted;
          })
      : null;
  },
  resendUserEmail: async (_, { login, email }, ctx: Context) => {
    const existLogins = await ctx.prisma.users({ where: { login } });

    return sendCreatedUserEmail({ login, email, userName: existLogins[0].userName }, ctx)
      .then(_ => {
        /**
         *
         * Sauvegarde activite de l'utilisateur
         *
         */

        createActiviteUser(
          {
            code: 'RUSEREMAIL',
            nom: 'RESEND USER EMAIL',
          },
          ctx,
        );

        return true;
      })
      .catch(_ => false); // send mail
  },
  resetUserPassword: async (_, { id, password }, ctx: Context) => {
    const passwordHash = await bcrypt.hash(password, 10);
    const status = 'RESETED';
    return updateUserPassword(ctx, { id, passwordHash, lastPasswordChangedDate: null, status });
  },
  updateUserTheme: async (_, { theme }, ctx: Context) => {
    const user = await ctx.prisma.updateUser({ where: { id: ctx.userId }, data: { theme } });
    return user;
  },
};
