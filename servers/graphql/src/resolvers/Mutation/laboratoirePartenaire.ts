import {
  createUpdateLaboratoirePartenaire,
  softDeleteLaboratoirePartenaire,
} from '../../repository/laboratoirePartenaire';
import { Context, LaboratoirePartenaireInput } from '../../types';

export default {
  createUpdateLaboratoirePartenaire: async (
    _: any,
    { input }: { input: LaboratoirePartenaireInput },
    ctx: Context,
  ) => {
    return createUpdateLaboratoirePartenaire(input, ctx);
  },
  softDeleteLaboratoirePartenaire: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteLaboratoirePartenaire(ctx, { id })));
  },
};
