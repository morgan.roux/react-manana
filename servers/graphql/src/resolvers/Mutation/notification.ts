import { Context } from '../../types';

export default {
  markNotificationAsSeen: async (_, { id, seen }, ctx: Context) => {
    const notification = await ctx.prisma.notification({ id });
    if (!notification) throw new Error('Notification Not Found');

    return ctx.prisma
      .updateNotification({ data: { seen }, where: { id } })
      .then(updated => updated);
  },
};
