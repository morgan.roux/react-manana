import { Context } from '../../types';
import { createUpdateGroupeClient, deleteSoftGroupeClient } from '../../repository/groupeClient';

export default {
  createUpdateGroupeClient: async (
    _,
    { id, nom, dateValiditeDebut, dateValiditeFin, idPharmacies },
    ctx: Context,
  ) => {
    return createUpdateGroupeClient(
      {
        id,
        nom,
        dateValiditeDebut,
        dateValiditeFin,
        idPharmacies:
          idPharmacies && idPharmacies.length ? idPharmacies.map((id: any) => String(id)) : [],
      },
      ctx,
    );
  },
  deleteSoftGroupeClient: async (_, { id }, ctx: Context) => {
    return deleteSoftGroupeClient({ id }, ctx);
  },
  deleteSoftGroupeClients: async (_, { ids }, ctx: Context) => {
    return Promise.all(ids.map(id => deleteSoftGroupeClient({ id }, ctx)));
  },
};
