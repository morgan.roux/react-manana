import { createUpdateRgpdAutorisation } from '../../repository/rgpdAutorisation';
import { Context, RgpdAutorisationInput } from '../../types';

export default {
  createUpdateRgpdAutorisation: async (
    _: any,
    { input }: { input: RgpdAutorisationInput },
    ctx: Context,
  ) => {
    return createUpdateRgpdAutorisation(ctx, input);
  },
  deleteRgpdAutorisation: async (_: any, { id }: { id: string }, ctx: Context) => {
    return ctx.prisma.deleteRgpdAutorisation({ id });
  },
};
