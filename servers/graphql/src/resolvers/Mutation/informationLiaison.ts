import moment from 'moment';
import { updateItemScoringForItem } from '../../sequelize/shared';
import {
  createUpdateInformationLiaison,
  softDeleteInformationLiaison,
  takeChargeInformationLiaison,
} from '../../repository/informationLiaison';
import { Context, InformationLiaisonInput, TakeChargeInformationLiaisonInput } from '../../types';

export default {
  createUpdateInformationLiaison: async (
    _: any,
    { input }: { input: InformationLiaisonInput },
    ctx: Context,
  ) => {
    return createUpdateInformationLiaison(ctx, input);
  },
  softDeleteInformationLiaisons: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    const informationLiaisons = await Promise.all(
      ids.map(async id => {
        return softDeleteInformationLiaison(ctx, id);
      }),
    );
    return informationLiaisons;
  },
  updateInformationLiaisonStatut: async (
    _: any,
    { id, statut }: { id: string; statut: string },
    ctx: Context,
  ) => {
    const informationLiaison = await ctx.prisma.updateInformationLiaison({
      where: { id },
      data: { statut },
    });
    await ctx.indexing.publishSave('informationliaisons', informationLiaison.id);
    await updateItemScoringForItem('Q', informationLiaison.id);

    return informationLiaison;
  },
  takeChargeInformationLiaison: async (
    _: any,
    { input }: { input: TakeChargeInformationLiaisonInput },
    ctx: Context,
  ) => {
    return takeChargeInformationLiaison(ctx, input);
  },
  updateInformationLiaisonStatutByUserConcerned: async (
    _: any,
    { id, statut }: { id: string; statut: string },
    ctx: Context,
  ) => {
    const infoUserConcernee = await ctx.prisma.updateInformationLiaisonUserConcernee({
      where: { id },
      data: { statut, dateStatutModification: moment().format() },
    });
    if (infoUserConcernee) {
      const informationLiaison = await ctx.prisma
        .informationLiaisonUserConcernee({
          id: infoUserConcernee.id,
        })
        .informationLiaison();
      if (informationLiaison) {
        await ctx.indexing.publishSave('informationliaisons', informationLiaison.id);
      }
      await ctx.indexing.publishSave(
        'updateinformationliaisonuserconcernees',
        infoUserConcernee.id,
      );

      await updateItemScoringForItem('Q', informationLiaison.id);
    }
    return infoUserConcernee;
  },
  readInformationLiaisonByUserConcerned: async (
    _: any,
    { id, lu }: { id: string; lu: boolean },
    ctx: Context,
  ) => {
    const infoUserConcernee = await ctx.prisma.updateInformationLiaisonUserConcernee({
      where: { id },
      data: { statut: lu ? 'LUES' : 'NON_LUES' },
    });
    if (infoUserConcernee) {
      const informationLiaison = await ctx.prisma
        .informationLiaisonUserConcernee({
          id: infoUserConcernee.id,
        })
        .informationLiaison();
      if (informationLiaison) {
        await ctx.indexing.publishSave('informationliaisons', informationLiaison.id);
      }
      await ctx.indexing.publishSave(
        'updateinformationliaisonuserconcernees',
        infoUserConcernee.id,
      );

      await updateItemScoringForItem('Q', informationLiaison.id);
    }
    return infoUserConcernee;
  },
};
