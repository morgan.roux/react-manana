import {
  createUpdateIdeeOuBonnePratiqueFichierJoint,
  softDeleteIdeeOuBonnePratiqueFichierJoint,
} from '../../repository/ideeOuBonnePratiqueFichierJoint';
import { Context, IdeeOuBonnePratiqueFichierJointInput } from '../../types';

export default {
  createUpdateIdeeOuBonnePratiqueFichierJoint: async (
    _: any,
    { input }: { input: IdeeOuBonnePratiqueFichierJointInput },
    ctx: Context,
  ) => {
    return createUpdateIdeeOuBonnePratiqueFichierJoint(ctx, input);
  },
  softDeleteIdeeOuBonnePratiqueFichierJoints: async (
    _: any,
    { ids }: { ids: string[] },
    ctx: Context,
  ) => {
    return Promise.all(ids.map(async id => softDeleteIdeeOuBonnePratiqueFichierJoint(ctx, id)));
  },
};
