import { createUpdateTodoSection, softDeleteTodoSection } from '../../repository/todoSection';
import { Context, TodoSectionInput } from '../../types';

export default {
  createUpdateTodoSection: async (_: any, { input }: { input: TodoSectionInput }, ctx: Context) => {
    return createUpdateTodoSection(input, ctx);
  },
  softDeleteTodoSections: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteTodoSection(ctx, { id })));
  },
};
