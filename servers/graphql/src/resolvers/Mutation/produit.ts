import { Context } from '../../types';
import { createUpdateProduit } from '../../repository/produit';
import { ProduitFormInput } from '../../interfaces/produit';

export default {
  createUpdateProduit: async (_, args: ProduitFormInput, ctx: Context) => {
    return createUpdateProduit(ctx, args);
  },
  updateProduitSupprimer: async (_, { id, date }: { id: string; date: string }, ctx: Context) => {
    return ctx.prisma.updateProduit({ where: { id }, data: { supprimer: date } }).then(produit => {
      ctx.indexing.publishSave('produits', produit.id);
      return produit;
    });
  },
};
