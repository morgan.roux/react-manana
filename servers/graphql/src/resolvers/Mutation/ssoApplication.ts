import { Context } from '../../types';
import {
  CreateSsoApplication,
  UpdateSsoApplication,
  DeleteSsoApplication,
  CreateApplicationRole,
  DeleteApplicationRole,
  CreateManyApplicationRole,
} from '../../repository/ssoApplication';
import { MutationResolvers } from '../../generated/graphqlgen';

export default {
  createSsoApplication: (_, _args: MutationResolvers.ArgsCreateSsoApplication, ctx: Context) => {
    return CreateSsoApplication(ctx, { ..._args });
  },
  updateSsoApplication: (_, _args: MutationResolvers.ArgsUpdateSsoApplication, ctx: Context) => {
    return UpdateSsoApplication(ctx, { ..._args });
  },
  deleteSsoApplication: (_, _args: MutationResolvers.ArgsDeleteSsoApplication, ctx: Context) => {
    return DeleteSsoApplication(ctx, { ..._args });
  },
  createApplicationRole: (_, _args, ctx: Context) => {
    return CreateApplicationRole(ctx, { ..._args });
  },
  deleteApplicationRole: (_, _args, ctx: Context) => {
    return DeleteApplicationRole(ctx, { ..._args });
  },
  createManyApplicationRole: (_, _args, ctx: Context) => {
    return CreateManyApplicationRole(ctx, { ..._args });
  },
};
