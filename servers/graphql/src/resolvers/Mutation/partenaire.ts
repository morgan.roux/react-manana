import {
  createUpdatePartenaireService,
  softDeletePartenaireService,
} from '../../repository/partenaire';
import { createActiviteUser } from '../../repository/activiteUser';
import { Context, PartenaireServiceInput } from '../../types';

export default {
  createPartenaire: async (
    _: any,
    {
      idGroupement,
      nom,
      adresse1,
      adresse2,
      cp,
      ville,
      pays,
      telBureau,
      telMobile,
      mail,
      site,
      commentaire,
      idSecteur,
      cerclePartenariat,
      telephone,
      email,
      prive,
      idPharmacie,
    },
    ctx: Context,
  ) => {
    return ctx.prisma
      .createPartenaire({
        groupement: {
          connect: {
            id: idGroupement,
          },
        },
        nom,
        commentaire,
        idSecteur,
        cerclePartenariat,
        prive,
        idPharmacie,
      })
      .then(async created => {
        await ctx.indexing.publishSave('partenaires', created.id);
        createActiviteUser(
          {
            code: 'CPARTENAIRE',
            nom: 'CREATE PARTENAIRE',
            log: JSON.stringify({ id: created.id }),
          },
          ctx,
        );

        return created;
      });
  },
  createUpdatePartenaireService: async (
    _: any,
    args: { input: PartenaireServiceInput },
    ctx: Context,
  ) => {
    console.log('createUpdatePartenaireService args :>> ', args);
    return createUpdatePartenaireService(args.input, ctx).then(p => {
      console.log('createUpdatePartenaireService p :>> ', p);
      createActiviteUser(
        {
          code: args.input.id ? 'CPARTENAIRE' : 'EPARTENAIRE',
          nom: args.input.id ? 'CREATE PARTENAIRE' : 'EDIT PARTENAIRE',
          log: JSON.stringify({ id: p.id }),
        },
        ctx,
      );
      return p;
    });
  },
  softDeletePartenaireService: async (_: any, { id }: { id: string }, ctx: Context) => {
    return softDeletePartenaireService(ctx, { id });
  },
  softDeletePartenaireServices: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeletePartenaireService(ctx, { id })));
  },
};
