import { Context } from '../../types';
import { createSmyley } from '../../repository/smyley';
import { createActiviteUser } from '../../repository/activiteUser';

export default {
  createUserSmyley: async (_, { codeItem, idSource, idSmyley }, ctx: Context) => {
    return createSmyley({ codeItem, idSource, idSmyley }, ctx).then(smyley => {
      /**
       *
       * Sauvegarde activite de l'utilisateur
       *
       */

      createActiviteUser(
        {
          code: 'CUSERSMYLEY',
          nom: 'CREATE USER SMYLEY',
        },
        ctx,
      );

      return smyley;
    });
  },
};
