import { createUpdateTodoEtiquette, softDeleteTodoEtiquette } from '../../repository/todoEtiquette';
import { Context, TodoEtiquetteInput } from '../../types';

export default {
  createUpdateTodoEtiquette: async (
    _: any,
    { input }: { input: TodoEtiquetteInput },
    ctx: Context,
  ) => {
    return createUpdateTodoEtiquette(input, ctx);
  },
  softDeleteTodoEtiquettes: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteTodoEtiquette(ctx, { id })));
  },
};
