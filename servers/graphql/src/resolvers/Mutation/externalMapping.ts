import { Context } from '../../types';
import {
  CreateExternalMapping,
  UpdateExternalMapping,
  DeleteExternalMapping,
} from '../../repository/externalMapping';

export default {
  createExternalMapping: async (_, _args, ctx: Context) => {
    return CreateExternalMapping(ctx, { ..._args });
  },
  updateExternalMapping: async (_, _args, ctx: Context) => {
    return UpdateExternalMapping(ctx, { ..._args });
  },
  deleteExternalMapping: async (_, { id }, ctx: Context) => {
    return DeleteExternalMapping(ctx, { id });
  },
};
