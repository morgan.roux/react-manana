import {
  createUpdateTodoParticipant,
  softDeleteTodoParticipant,
} from '../../repository/todoParticipant';
import { Context, TodoParticipantInput } from '../../types';

export default {
  createUpdateTodoParticipant: async (
    _: any,
    { input }: { input: TodoParticipantInput },
    ctx: Context,
  ) => {
    return createUpdateTodoParticipant(input, ctx);
  },
  softDeleteTodoParticipants: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(async id => softDeleteTodoParticipant(ctx, { id })));
  },
};
