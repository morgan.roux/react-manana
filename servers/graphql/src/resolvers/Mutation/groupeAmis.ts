import { createUpdateGroupeAmisDetail } from '../../repository/groupeAmisDetail';
import { createUpdateGroupeAmis, softDeleteGroupeAmis } from '../../repository/groupeAmis';
import { Context, GroupeAmisInput, GroupeAmisStatus } from '../../types';

export default {
  createUpdateGroupeAmis: async (_: any, { input }: { input: GroupeAmisInput }, ctx: Context) => {
    return createUpdateGroupeAmis(input, ctx);
  },
  softDeleteGroupeAmises: async (_: any, { ids }: { ids: string[] }, ctx: Context) => {
    return Promise.all(ids.map(id => softDeleteGroupeAmis(ctx, id)));
  },
  updateGroupeAmisStatus: async (
    _: any,
    { id, status }: { id: string; status: GroupeAmisStatus },
    ctx: Context,
  ) => {
    const groupeAmis = await ctx.prisma.updateGroupeAmis({ where: { id }, data: { status } });
    await ctx.indexing.publishSave('groupeamises', groupeAmis.id);
    return groupeAmis;
  },
  leaveGroupeAmis: async (
    _: any,
    { id, idsPharmacies }: { id: string; idsPharmacies: string[] },
    ctx: Context,
  ) => {
    const groupeAmisDetails = await ctx.prisma.groupeAmisDetails({
      where: { groupeAmis: { id }, pharmacie: { id_in: idsPharmacies }, isRemoved: false },
    });

    console.log('+++++++++++++++groupeAmisDetails', groupeAmisDetails);

    if (groupeAmisDetails && groupeAmisDetails.length > 0) {
      const results = await Promise.all(
        groupeAmisDetails.map(async gad => {
          await ctx.indexing.publishSave('groupeamisdetails', gad.id);
          return ctx.prisma.updateGroupeAmisDetail({
            where: { id: gad.id },
            data: { isRemoved: true },
          });
        }),
      );

      await ctx.indexing.publishSave('groupeamises', id);
      return results;
    } else {
      return [];
    }
  },
  invitePharmacies: async (
    _: any,
    { id, idsPharmacies }: { id: string; idsPharmacies: string[] },
    ctx: Context,
  ) => {
    if (idsPharmacies && idsPharmacies.length > 0) {
      const results = await Promise.all(
        idsPharmacies.map(async idPharmacie => {
          const groupeAmisDetail = await createUpdateGroupeAmisDetail(
            { idGroupeAmis: id, idPharmacie, confirmedInvitation: false, isRemoved: false },
            ctx,
          );
          if (groupeAmisDetail) {
            await ctx.indexing.publishSave('groupeamisdetails', groupeAmisDetail.id);
          }
          return groupeAmisDetail;
        }),
      );

      await ctx.indexing.publishSave('groupeamises', id);
      return results;
    } else {
      return [];
    }
  },
};
