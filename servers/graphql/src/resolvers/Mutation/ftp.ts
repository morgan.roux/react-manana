import { Context } from '../../types';
import { CreateFtpConnect, UpdateFtpConnect, DeleteFtp } from '../../repository/ftp';

export default {
  createFtpConnect: async (_, _args, ctx: Context) => {
    return CreateFtpConnect(ctx, { ..._args });
  },
  updateFtpConnect: async (_, _args, ctx: Context) => {
    return UpdateFtpConnect(ctx, { ..._args });
  },
  deleteFtpConnect: async (_, { id }, ctx: Context) => {
    return DeleteFtp(ctx, { id });
  },
};
