import { withFilter } from 'apollo-server';
import logger from '../../logging';
import { isActiveNotificationActualite } from '../../repository/notification';
import { Context } from '../../types';
import { pubsub } from '../index';

export const NOTIFICATION_ADDED = 'NOTIFICATION_ADDED';
export const NOTIFICATION_UPDATED = 'NOTIFICATION_UPDATED';
export const NOTIFICATION_DELETED = 'NOTIFICATION_DELETED';

export const notification = {
  notificationAdded: {
    subscribe: withFilter(
      () => pubsub.asyncIterator(NOTIFICATION_ADDED),
      async (payload, _, ctx: Context) => {
        if (!payload || !payload.notificationAdded || !payload.to) {
          return false;
        }
        const isActive = await isActiveNotificationActualite(ctx, {
          type: payload.notificationAdded.type,
          targetId: payload.notificationAdded.targetId,
        });

        return payload.to === ctx.userId && isActive;
      },
    ),
  },
  notificationUpdated: {
    subscribe: withFilter(
      () => pubsub.asyncIterator(NOTIFICATION_UPDATED),
      async (payload, _, ctx: Context) => {
        if (!payload || !payload.notificationUpdated || !payload.to) {
          return false;
        }
        const isActive = await isActiveNotificationActualite(ctx, {
          type: payload.notificationAdded.type,
          targetId: payload.notificationAdded.targetId,
        });
        logger.info(
          `[[ UPDATE SUBSCRIPTION NOTIFICATION ]] ID: ${
            payload.notificationUpdated.targetName
          }, to ==> ${payload.to}, current user ==> ${ctx.userId} : sent ==> ${payload.to ===
            ctx.userId}`,
        );

        return payload.to === ctx.userId && isActive;
      },
    ),
  },
  notificationDeleted: {
    subscribe: withFilter(
      () => pubsub.asyncIterator(NOTIFICATION_DELETED),
      async (payload, _, ctx: Context) => {
        if (!payload || !payload.notificationDeleted || !payload.to) {
          return false;
        }

        logger.info(
          `[[ DELETE SUBSCRIPTION NOTIFICATION ]] ID: ${
            payload.notificationDeleted.targetName
          }, to ==> ${payload.to}, current user ==> ${ctx.userId} : sent ==> ${payload.to ===
            ctx.userId}`,
        );

        return payload.to === ctx.userId;
      },
    ),
  },
};
