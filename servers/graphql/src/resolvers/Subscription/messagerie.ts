import { withFilter } from 'apollo-server';
import { Context } from '../../types';
import { pubsub } from '../index';

export const MESSAGERIE_ENVOYEE = 'MESSAGERIE_ENVOYEE';
export const MESSAGERIE_LU = 'MESSAGERIE_LU';
export const MESSAGERIE_SUPPRIMEE = 'MESSAGERIE_SUPPRIMEE';
export const MESSAGERIE_RESTAUREE = 'MESSAGERIE_RESTAUREE';

export const messagerie = {
  messagerieEnvoyee: {
    subscribe: withFilter(
      () => pubsub.asyncIterator(MESSAGERIE_ENVOYEE),
      async (payload, _, ctx: Context) => {
        if (!payload || !payload.messagerieEnvoyee) {
          return false;
        }

        console.log('[ MESSAGERIE ENVOYE ]', payload);
        // test si le current user est dans la liste des recepteurs
        return payload && payload.recepteurId === ctx.userId;
      },
    ),
  },
  messagerieLu: {
    subscribe: withFilter(
      () => pubsub.asyncIterator(MESSAGERIE_LU),
      async (payload, _, ctx: Context) => {
        if (!payload || !payload.messagerieLu) {
          return false;
        }

        console.log('[ MESSAGERIE LU ]', payload);
        // test si le current user est dans la liste des recepteurs
        return payload && payload.recepteurId === ctx.userId;
      },
    ),
  },
  messagerieSupprimee: {
    subscribe: withFilter(
      () => pubsub.asyncIterator(MESSAGERIE_SUPPRIMEE),
      async (payload, _, ctx: Context) => {
        if (!payload || !payload.messagerieSupprimee) {
          return false;
        }

        console.log('[ MESSAGERIE SUPPRIMER ]', payload);
        // test si le current user est dans la liste des recepteurs
        return payload && payload.recepteurId === ctx.userId;
      },
    ),
  },

  messagerieRestauree: {
    subscribe: withFilter(
      () => pubsub.asyncIterator(MESSAGERIE_RESTAUREE),
      async (payload, _, ctx: Context) => {
        if (payload || !payload.messagerieRestauree) {
          return false;
        }
        console.log('[MESSAGERIE RESTAURE]', payload);
        return payload;
      },
    ),
  },
};
