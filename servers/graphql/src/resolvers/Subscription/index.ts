import { SubscriptionResolvers } from '../../generated/graphqlgen';
import { notification } from './notification';
import { messagerie } from './messagerie';

export const Subscription: SubscriptionResolvers.Type = {
  ...SubscriptionResolvers.defaultResolvers,
  ...notification,
  ...messagerie,
};
