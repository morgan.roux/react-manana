import Redis = require('ioredis');

import { RedisPubSub } from 'graphql-redis-subscriptions';
import { Prisma } from './generated/prisma-client';
import Indexing from './indexing';

export interface Context {
  prisma: Prisma;
  userId: string;
  role: string;
  redis: Redis.Redis;
  pubsub: RedisPubSub;
  indexing: Indexing;
  groupementId: string;
  pharmacieId: string;
  ip: string;
  domaineActiviteId: string;
  startTime: number;
}

interface Searchable {
  type: string;
}

export enum UserStatus {
  ACTIVATED = 'ACTIVATED',
  BLOCKED = 'BLOCKED',
  ACTIVATION_REQUIRED = 'ACTIVATION_REQUIRED',
  RESETED = 'RESETED',
  ARCHIVED = 'ARCHIVED',
}

export enum TypeFichier {
  PHOTO = 'PHOTO',
  AVATAR = 'AVATAR',
  LOGO = 'LOGO',
  PDF = 'PDF',
  EXCEL = 'EXCEL',
}

export enum Priorite {
  BLOQUANT = 'BLOQUANT',
  IMPORTANT = 'IMPORTANT',
  NORMAL = 'NORMAL',
}

export enum Type {
  COMMANDE_CANAL = 'COMMANDE_CANAL',
  COMMANDE_TYPE = 'COMMANDE_TYPE',
}

export enum TypePresidentCible {
  PRESIDENT = 'PRESIDENT',
  PHARMACIE = 'PHARMACIE',
}

export enum ConnexionStatut {
  BLOCKED = 'BLOCKED',
  AUTHORIZED = 'AUTHORIZED',
  TO_CONFIRM = 'TO_CONFIRM',
}

export enum TypeEspace {
  BANNIERE = 'BANNIERE',
  PUBLICITE = 'PUBLICITE',
}

export enum OriginePublicite {
  INTERNE = 'INTERNE',
  EXTERNE = 'EXTERNE',
}

export enum MarcheType {
  LABORATOIRE = 'LABORATOIRE',
  ARTICLE = 'ARTICLE',
}

export enum MarcheStatus {
  NON_ACTIF = 'NON_ACTIF',
  ACTIF = 'ACTIF',
  CLOTURE = 'CLOTURE',
}

export enum SourceRemise {
  PERMANENT = 'PERMANENT',
  MARCHE = 'MARCHE',
  PROMOTION = 'PROMOTION',
}

export enum PromotionType {
  OFFRE_DU_MOIS = 'OFFRE_DU_MOIS',
  BONNE_AFFAIRE = 'BONNE_AFFAIRE',
  LIQUIDATION = 'LIQUIDATION',
  AUTRE_PROMOTION = 'AUTRE_PROMOTION',
}

export enum PromotionStatus {
  NON_ACTIF = 'NON_ACTIF',
  ACTIF = 'ACTIF',
  CLOTURE = 'CLOTURE',
}

enum SsoType {
  SAML = 'SAML',
  TOKEN_AUTHENTIFICATION = 'TOKEN_AUTHENTIFICATION',
  TOKEN_FTP_AUTHENTIFICATION = 'TOKEN_FTP_AUTHENTIFICATION',
  CRYPTAGE_AES256 = 'CRYPTAGE_AES256',
  CRYPTAGE_MD5 = 'CRYPTAGE_MD5',
  OTHER_CRYPTO = 'OTHER_CRYPTO',
}

export enum Sexe {
  M = 'M',
  F = 'F',
}

export enum GroupeRole {
  SU = 'SU',
  GROUPEMENT = 'GROUPEMENT',
  PHARMACIE = 'PHARMACIE',
  REGION = 'REGION',
  LABO = 'LABO',
  SERVICE = 'SERVICE',
}

export enum TypeRole {
  SU = 'SU',
  GROUPEMENT = 'GROUPEMENT',
  PHARMACIE = 'PHARMACIE',
  TITULAIRE = 'TITULAIRE',
  REGION = 'REGION',
  LABO = 'LABO',
  SERVICE = 'SERVICE',
  STATUT = 'STATUT',
}

export enum TypeMotif {
  E = 'E',
  S = 'S',
}

export enum TypeService {
  GROUPEMENT = 'GROUPEMENT',
  PARTENAIRE = 'PARTENAIRE',
  LIBRE = 'LIBRE',
}

export enum TypePrestataire {
  D = 'D',
  M = 'M',
  E = 'E',
  A = 'A',
}

export enum TypeAffectation {
  REMPLACEMENT_ARRET_SANS = 'REMPLACEMENT_ARRET_SANS',
  REMPLACEMENT_ATTRIBUTION = 'REMPLACEMENT_ATTRIBUTION',
  DEPARTEMENT = 'DEPARTEMENT',
}

export enum MessagerieType {
  E = 'E',
  R = 'R',
}

export enum MessagerieCategory {
  MY_REGION = 'MY_REGION',
  PHARMACIES = 'PHARMACIES',
  GROUPEMENT = 'GROUPEMENT',
  LABO_PARTENAIRE = 'LABO_PARTENAIRE',
  PARTENAIRE_SERVICE = 'PARTENAIRE_SERVICE',
}

export enum TypeEmetteurRecepteur {
  Pharmacie = 'Pharmacie',
  Groupement = 'Groupement',
  Partenaire = 'Partenaire',
  Laboratoire = 'Laboratoire',
  President = 'President',
}

export enum TypePartenaire {
  NON_PARTENAIRE = 'NON_PARTENAIRE',
  NEGOCIATION_EN_COURS = 'NEGOCIATION_EN_COURS',
  NOUVEAU_REFERENCEMENT = 'NOUVEAU_REFERENCEMENT',
  PARTENARIAT_ACTIF = 'PARTENARIAT_ACTIF',
}

export enum StatutPartenaire {
  ACTIVER = 'ACTIVER',
  BLOQUER = 'BLOQUER',
}

export enum TicketAppel {
  ENTRANT = 'ENTRANT',
  SORTANT = 'SORTANT',
}

export enum OrigineType {
  INTERNE = 'INTERNE',
  PATIENT = 'PATIENT',
  GROUPE_AMIS = 'GROUPE_AMIS',
  GROUPEMENT = 'GROUPEMENT',
  FOURNISSEUR = 'FOURNISSEUR',
  PRESTATAIRE = 'PRESTATAIRE',
}

export enum StatusTicket {
  NOUVELLE = 'NOUVELLE',
  EN_COURS = 'EN_COURS',
  EN_CHARGE = 'EN_CHARGE',
  CLOTURE = 'CLOTURE',
}

export interface Role extends Searchable {
  type: 'role';
  id: any;
  code: string;
  nom: string;
  typeRole: TypeRole;
  groupeRole: GroupeRole;
  dateCreation: string;
  dateModification: string;
  roleTraitements: RoleTraitement[];
}

export interface GroupeOption {
  id: any;
  code: string;
  nom: string;
  type: string;
  options: Option[];
}

export interface Option {
  id: any;
  code: string;
  nom: string;
  groupe: GroupeOption;
}

export interface User extends Searchable {
  type: 'user';
  id: any;
  email: string;
  jourNaissance: number;
  moisNaissance: number;
  anneeNaissance: number;
  status: string;
  emailConfirmed: boolean;
  password: string;
  passwordSecretAnswer: string;
  phoneNumber: string;
  phoneNumberConfirmed: boolean;
  twoFactorEnabled: boolean;
  lockoutEndDateUtc: string;
  lockoutEnabled: boolean;
  accessFailedCount: number;
  userName: string;
  lastLoginDate: string;
  lastPasswordChangedDate: string;
  isLockedOut: boolean;
  isLockedOutPermanent: boolean;
  isObligationChangePassword: boolean;
  accessFailedCountBeforeLockoutPermanent: number;
  dateCreation: string;
  dateModification: string;
  role: Role;
  groupement: Groupement;
  idGroupement: string;
  notifications: NotificationResult;
  pharmacie: Pharmacie;
  userPhoto: UserPhoto;
  userPersonnel: Personnel;
  userPpersonnel: Ppersonnel;
  userLaboratoire: Laboratoire;
  userPartenaire: Partenaire;
  userTitulaire: UserTitulaire;
  theme: string;
  codeTraitements: string[];
  nbReclamation: number;
  nbAppel: number;
  nbTicket: number;
}

export interface Groupement extends Searchable {
  type: 'groupement';
  id: string;
  nom: string;
  adresse1: string;
  adresse2: string;
  cp: string;
  ville: string;
  pays: string;
  telBureau: string;
  telMobile: string;
  mail: string;
  site: string;
  commentaire: string;
  nomResponsable: string;
  prenomResponsable: string;
  dateSortie: string;
  sortie: number;
  dateCreation: string;
  dateModification: string;
  pharmacies: Pharmacie[];
  groupementLogo: GroupementLogo;
}

export interface Pharmacie extends Searchable {
  type: 'pharmacie';
  id: number;
  nom: string;
  adresse1: string;
  adresse2: string;
  cp: string;
  ville: string;
  pays: string;
  cip: string;
  dernierCip: string;
  numFiness: string;
  ancienFiness: string;
  commentaire: string;
  uga: string;
  latitude: string;
  longitude: string;
  nbEmploye: number;
  nbAssistant: number;
  nbAutreTravailleur: number;
  nbJourOuvre: number;
  sortie: number;
  userCreation: User;
  userModification: User;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
  actived: boolean;
  idGroupement: string;
  contact: Contact;
  users: User[];
  titulaires: Titulaire[];
  departement: Departement;
  presidentRegion: TitulaireAffectation;
  entreeSortie: PharmacieEntreeSortie;
  satisfaction: PharmacieSatisfaction;
  compta: PharmacieCompta;
  informatique: PharmacieInformatique;
  segmentation: PharmacieSegmentation;
  achat: PharmacieAchat;
  cap: PharmacieCap;
  concept: PharmacieConcept;
  statCA: PharmacieStatCA;
  digitales: PharmacieDigitale[];
  grossistes: Grossiste[];
  generiqueurs: Generiqueur[];
  nbReclamation: number;
  nbAppel: number;
  nbTicket: number;
  comments(take: number, skip: number): CommentResult;
}

export interface Generiqueur {
  id: string;
  nom: string;
}

export interface Grossiste {
  id: string;
  nom: string;
}

export interface PharmacieEntreeSortie {
  id: string;
  contrat: Contrat;
  concurrent: Concurrent;
  concurrentAncien: Concurrent;
  dateEntree: string;
  pharmacieMotifEntree: Motif;
  commentaireEntree: string;
  dateSortieFuture: string;
  pharmacieMotifSortieFuture: Motif;
  commentaireSortieFuture: string;
  dateSortie: string;
  pharmacieMotifSortie: Motif;
  commentaireSortie: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
  userCreation: User;
  userModification: User;
}

export interface PharmacieCompta {
  id: string;
  siret: string;
  codeERP: string;
  ape: string;
  tvaIntra: string;
  rcs: string;
  contactCompta: string;
  chequeGrp: boolean;
  valeurChequeGrp: number;
  commentaireChequeGrp: string;
  droitAccesGrp: boolean;
  valeurChequeAccesGrp: number;
  commentaireChequeAccesGrp: string;
  structureJuridique: string;
  denominationSociale: string;
  telCompta: string;
  modifStatut: boolean;
  raisonModif: string;
  dateModifStatut: string;
  nomBanque: string;
  banqueGuichet: string;
  banqueCompte: string;
  banqueRib: string;
  banqueCle: string;
  dateMajRib: string;
  iban: string;
  swift: string;
  dateMajIban: string;
}

export interface PharmacieInformatique {
  id: string;
  logiciel: Logiciel;
  numVersion: string;
  dateLogiciel: string;
  nbrePoste: number;
  nbreComptoir: number;
  nbreBackOffice: number;
  nbreBureau: number;
  commentaire: string;
  automate1: Automate;
  dateInstallation1: string;
  automate2: Automate;
  dateInstallation2: string;
  automate3: Automate;
  dateInstallation3: string;
}

export interface PharmacieSegmentation {
  id: string;
  typologie: Typologie;
  trancheCA: TrancheCA;
  qualite: Qualite;
  contrat: Contrat;
  dateSignature: string;
}

export interface PharmacieAchat {
  id: string;
  dateDebut: string;
  dateFin: string;
  identifiantAchatCanal: string;
  commentaire: string;
  canal: CommandeCanal;
}

export interface PharmacieCap {
  id: string;
  cap: boolean;
  dateDebut: string;
  dateFin: string;
  identifiantCap: string;
  commentaire: string;
}

export interface PharmacieConcept {
  id: string;
  fournisseurMobilier: PrestatairePharmacie;
  dateInstallConcept: string;
  concept: Concept;
  avecFacade: boolean;
  avecConcept: boolean;
  dateInstallFacade: string;
  enseigniste: PrestatairePharmacie;
  conformiteFacade: boolean;
  facade: Facade;
  surfaceTotale: number;
  surfaceVente: number;
  SurfaceVitrine: number;
  volumeLeaflet: number;
  nbreLineaire: number;
  nbreVitrine: number;
  otcLibAcces: boolean;
  leaflet: Leaflet;
  commentaire: string;
}

export interface PharmacieStatCA {
  id: string;
  exercice: number;
  dateDebut: string;
  dateFin: string;
  caTTC: number;
  caHt: number;
  caTVA1: number;
  tauxTVA1: number;
  caTVA2: number;
  tauxTVA2: number;
  caTVA3: number;
  tauxTVA3: number;
  caTVA4: number;
  tauxTVA4: number;
  caTVA5: number;
  tauxTVA5: number;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface PharmacieDigitale {
  id: string;
  servicePharmacie: ServicePharmacie;
  pharmaciePestataire: PrestatairePharmacie;
  flagService: boolean;
  dateInstallation: string;
  url: string;
}

export interface PrestatairePharmacie {
  id: string;
  typePrestataire: TypePrestataire;
  nom: string;
  adresse1: string;
  adresse2: string;
  cp: string;
  ville: string;
  telephone1: string;
  telephone2: string;
  fax: string;
  commentaire: string;
  partenaire: boolean;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface Concept {
  id: string;
  typeConcept: string;
  modele: string;
}

export interface Facade {
  id: string;
  modele: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface Leaflet {
  id: string;
  modele: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface Qualite {
  id: string;
  libelle: string;
}

export interface Typologie {
  id: string;
  code: string;
  libelle: string;
}

export interface TrancheCA {
  id: string;
  libelle: string;
  valMin: number;
  valMax: number;
}

export interface PharmacieSatisfaction {
  id: string;
  dateSaisie: string;
  commentaire: string;
  smyley: Smyley;
  user: User;
}

export interface PharmacieSatisfactionInput {
  idPharmacie?: string;
  dateSaisie: string;
  commentaire: string;
  idSmyley: string;
}

export interface PharmacieEntreeSortieInput {
  idPharmacie?: string;
  idContrat?: string;
  idConcurent?: string;
  idConcurentAncien: string;
  dateEntree: string;
  idMotifEntree: string;
  commentaireEntree?: string;
  sortieFuture?: number;
  dateSortieFuture?: string;
  idMotifSortieFuture: string;
  commentaireSortieFuture?: string;
  sortie?: number;
  dateSortie?: string;
  idMotifSortie?: string;
  commentaireSortie?: string;
  dateSignature: string;
}
export interface LaboratoirePartenaireInput {
  id: string;
  debutPartenaire?: string;
  finPartenaire?: string;
  typePartenaire?: TypePartenaire;
  idLaboratoire?: string;
  statutPartenaire: StatutPartenaire;
  adresse: string;
  codePostal: string;
  ville: string;
  Pays: string;
}
export interface PharmacieSegmentationInput {
  idPharmacie?: string;
  idTypologie: string;
  idTrancheCA: string;
  idQualite: string;
  idContrat: string;
  dateSignature: string;
}

export interface PharmacieComptaInput {
  idPharmacie?: string;
  siret: string;
  codeERP: string;
  ape: string;
  tvaIntra: string;
  rcs: string;
  contactCompta: string;
  chequeGrp: boolean;
  valeurChequeGrp: number;
  commentaireChequeGrp: string;
  droitAccesGrp: boolean;
  valeurChequeAccesGrp: number;
  commentaireChequeAccesGrp: string;
  structureJuridique: string;
  denominationSociale: string;
  telCompta: string;
  modifStatut: boolean;
  raisonModif: string;
  dateModifStatut: string;
  nomBanque: string;
  banqueGuichet: string;
  banqueCompte: string;
  banqueRib: string;
  banqueCle: string;
  dateMajRib: string;
  iban: string;
  swift: string;
  dateMajIban: string;
}

export interface PharmacieInformatiqueInput {
  idPharmacie?: string;
  idLogiciel: string;
  numVersion: string;
  dateLogiciel: string;
  nbrePoste: number;
  nbreComptoir: number;
  nbreBackOffice: number;
  nbreBureau: number;
  commentaire: string;
  idAutomate1: string;
  dateInstallation1: string;
  idAutomate2: string;
  dateInstallation2: string;
  idAutomate3: string;
  dateInstallation3: string;
}

export interface PharmacieAchatInput {
  idPharmacie?: string;
  dateDebut: string;
  dateFin: string;
  identifiantAchatCanal: string;
  commentaire: string;
  idCanal: string;
}

export interface PharmacieCapInput {
  idPharmacie?: string;
  cap: boolean;
  dateDebut: string;
  dateFin: string;
  identifiantCap: string;
  commentaire: string;
}
export interface PharmacieConceptInput {
  idPharmacie?: string;
  idFournisseurMobilier: string;
  dateInstallConcept: string;
  avecConcept: boolean;
  idConcept: string;
  avecFacade: boolean;
  dateInstallFacade: string;
  idEnseigniste: string;
  conformiteFacade: boolean;
  idFacade: string;
  surfaceTotale: number;
  surfaceVente: number;
  SurfaceVitrine: number;
  volumeLeaflet: number;
  nbreLineaire: number;
  nbreVitrine: number;
  otcLibAcces: boolean;
  idLeaflet: string;
  commentaire: string;
}

export interface PharmacieStatCAInput {
  idPharmacie?: string;
  exercice: number;
  dateDebut: string;
  dateFin: string;
  caTTC: number;
  caHt: number;
  caTVA1: number;
  tauxTVA1: number;
  caTVA2: number;
  tauxTVA2: number;
  caTVA3: number;
  tauxTVA3: number;
  caTVA4: number;
  tauxTVA4: number;
  caTVA5: number;
  tauxTVA5: number;
}

export interface PharmacieDigitaleInput {
  id?: string;
  idServicePharmacie: string;
  idPrestataire: string;
  flagService: boolean;
  dateInstallation: string;
  url: string;
}

export interface Motif {
  id: string;
  typeMotif: TypeMotif;
  libelle: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface PharmacieEntreeSortie {
  id: string;
  dateEntree: string;
  commentaireEntree: string;
  dateSortieFuture: string;
  commentaireSortieFuture: string;
  dateSortie: string;
  commentaireSortie: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
  userCreation: User;
  userModification: User;
  contrat: Contrat;
  concurrent: Concurrent;
  pharmacieMotifSortie: Motif;
  pharmacieMotifEntree: Motif;
}

export interface Motif {
  id: string;
  typeMotif: TypeMotif;
  libelle: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface Contrat {
  id: string;
  nom: string;
  numeroVersion: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface Concurrent {
  id: string;
  nom: string;
  adresse1: string;
  adresse2: string;
  cp: string;
  ville: string;
  telephone1: string;
  telephone2: string;
  fax: string;
  commentaire: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface Logiciel {
  id: string;
  ssii: SSII;
  nom: string;
  os: string;
  osVersion: string;
  commentaire: string;
}

export interface SSII {
  id: string;
  nom: string;
  adresse1: string;
  adresse2: string;
  cp: string;
  ville: string;
  telephone1: string;
  telephone2: string;
  fax: string;
  mail: string;
  web: string;
  commentaire: string;
}

export interface Automate {
  id: string;
  prestataire: PrestatairePharmacie;
  modeleAutomate: string;
  dateCommercialisation: string;
}

export interface Titulaire extends Searchable {
  type: 'titulaire';
  id: number;
  civilite: string;
  nom: string;
  prenom: string;
  fullName: string;
  estPresidentRegion: boolean;
  sortie: number;
  dateSortie: string;
  contact: Contact;
  idGroupement: string;
  users: User[];
  pharmacies: Pharmacie[];
  pharmacieUser: Pharmacie;
  userCreation: string;
  userModification: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}
export interface Laboratoire extends Searchable {
  type: 'laboratoire';
  id: string;
  nom: string;
  laboSuite: LaboratoireSuite;
  laboratoirePartenaire: LaboratoirePartenaire;
  sortie: number;
  user: User;
  photo: Fichier;
  laboratoireRessources: [LaboratoireRessource];
  laboratoireRepresentants: [LaboratoireRepresentant];
  actualites: Actualite[];
  countCanalArticles(codeCanal: string): number;
  actions(take: number, skip: number): ActionResult;
  comments(take: number, skip: number): CommentResult;
}

export interface LaboratoirePartenaire {
  id: string;
  debutPartenaire: string;
  finPartenaire: string;
  typePartenaire: TypePartenaire;
  statutPartenaire: StatutPartenaire;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}
export interface LaboratoireSuite {
  id: string;
  nom: string;
  adresse: string;
  codePostal: string;
  telephone: string;
  ville: string;
  telecopie: string;
  email: string;
  webSiteUrl: string;
  dateCreation: string;
  dateModification: string;
}

export interface Personnel extends Searchable {
  type: 'personnel';
  id: string;
  service: Service;
  respHierarch: Personnel;
  civilite: string;
  nom: string;
  prenom: string;
  fullName: string;
  sexe: Sexe;
  commentaire: string;
  sortie: number;
  dateSortie: Date;
  contact: Contact;
  idGroupement: string;
  role: Role;
  user: User | null;
  actualites: Actualite[];
  userCreation: User;
  userModification: User;
}

export interface Ppersonnel extends Searchable {
  type: 'ppersonnel';
  id: string;
  civilite: string;
  nom: string;
  prenom: string;
  fullName: string;
  estAmbassadrice: boolean;
  commentaire: string;
  sortie: number;
  dateSortie: string;
  contact: Contact;
  idPharmacie: string;
  pharmacie: Pharmacie;
  idGroupement: string;
  role: Role;
  user: User | null;
}
export interface PpersonnelInput {
  id: string;
  idPharmacie: string;
  idGroupement: string;
  civilite: string;
  nom: string;
  prenom: string;
  telBureau: string;
  telMobile: string;
  mail: string;
  estAmbassadrice: boolean;
  sortie: number;
  commentaire: string;
}
export interface Partenaire extends Searchable {
  type: 'partenaire';
  id: string;
  nom: string;
  commentaire: string;
  codeMaj: string;
  isRemoved: boolean;
  contact: Contact;
  idGroupement: string;
  user: User | null;
  role: Role;
  partenaireServiceSuite: PartenaireServiceSuite;
  services: ServicePartenaire[];
  partenaireServicePartenaire: PartenaireServicePartenaire;
  comments(take: number, skip: number): CommentResult;
}

export interface LoginPayload {
  accessToken: string;
}

export interface SearchResult {
  total: number;
  data: Searchable[];
}

export interface ProduitCanal extends Searchable {
  type: 'produitcanal';
  id: string;
  produit: Produit;
  qteStock: number;
  qteMin: number;
  stv: number;
  unitePetitCond: number;
  uniteSupCond: number;
  prixFab: number;
  prixPhv: number;
  prixTtc: number;
  dateCreation: string;
  dateModification: string;
  dateRetrait: string;
  datePeremption: string;
  sousGammeCommercial: CanalSousGamme;
  sousGammeCatalogue: SousGammeCatalogue;
  isRemoved: boolean;
  commandeCanal: CommandeCanal;
  pharmacieRemisePaliers: Pharmacie[];
  pharmacieRemisePanachees: Pharmacie[];
  operations: Operation[];
  marches: Marche[];
  promotions: Promotion[];
  isShared: boolean;
  partage?: Partage;
  groupementComments: Groupement[];
  groupementSmyleys: Groupement[];
  comments(take: number, skip: number): CommentResult;
  inMyPanier(idPharmacie: string): boolean;
  articleSamePanachees(idPharmacie: string): ProduitCanal[];
  qteTotalRemisePanachees(idPharmacie: string): number;
  inOtherPanier(idPharmacie: string): PanierLigne;
  remises(idPharmacie: string, idRemiseOperation: string): RemiseDetail[];
  ligneInPanier(idPharmacie: string): PanierLigne;
  actions(take: number, skip: number): ActionResult;
}

export interface Produit extends Searchable {
  type: 'produit';
  id: string;
  resipIdProduit: number;
  libelle: string;
  libelle2: string;
  isReferentGenerique: number;
  supprimer: string;
  surveillanceRenforcee: number;
  dateCreation: string;
  dateModification: string;
  produitPhoto: ProduitPhoto;
  produitCode: ProduitCode;
  famille: Famille;
  techReg: ProduitTechReg;
  canauxArticle: ProduitCanal[];
  service: Service;
}

export interface ProduitPhoto {
  id: string;
  produit: Produit;
  fichier: Fichier;
  dateCreation: string;
  dateModification: string;
}

export interface ProduitCode {
  id: string;
  code: string;
  typeCode: number;
  referent: number;
  dateCreation: string;
  dateModification: string;
}

export interface ProduitTechReg {
  id: string;
  isTipsLpp: number;
  isHomeo: number;
  isTfr: number;
  isStupefiant: number;
  laboTitulaire: Laboratoire;
  laboExploitant: Laboratoire;
  laboDistirbuteur: Laboratoire;
  tva: TVA;
  prixAchat: number;
  prixAchatHT: number;
  prixVenteTTC: number;
  acte: Acte;
  liste: Liste;
  libelleStockage: LibelleDivers;
  tauxss: TauxSS;
  baseRemboursementSS: string;
  codePoidsVolume: string;
  poidsVolume: string;
  tpn: number;
  peremption: number;
  multipleVente: number;
  dateCreation: string;
  dateModification: string;
}

export interface Liste extends Searchable {
  type: 'liste';
  id: string;
  codeListe: number;
  libelle: string;
  dateCreation: string;
  dateModification: string;
}

export interface LibelleDivers extends Searchable {
  type: 'libelledivers';
  id: string;
  codeInfo: number;
  code: number;
  libelle: string;
  dateCreation: string;
  dateModification: string;
}

export interface TauxSS extends Searchable {
  type: 'tauxss';
  id: string;
  codeTaux: number;
  tauxSS: number;
  dateCreation: string;
  dateModification: string;
}

export interface CanalSousGamme {
  id: string;
  codeSousGamme: number;
  libelle: string;
  estDefaut: number;
  gammeCommercial: CanalGamme;
  countCanalArticles(codeCanal: string): number;
}

export interface CanalGamme extends Searchable {
  type: 'canalgamme';
  id: string;
  codeGamme: number;
  libelle: string;
  estDefaut: number;
  canal: CommandeCanal;
  sousGamme: CanalSousGamme[];
  countCanalArticles(codeCanal: string): number;
}

export interface Famille extends Searchable {
  type: 'famille';
  id: string;
  codeFamille: string;
  libelleFamille: string;
  parent: Famille;
  sousFamilles: Famille[];
  countCanalArticles(codeCanal: string): number;
}

export interface Commande extends Searchable {
  type: 'commande';
  id: string;
  codeReference: string;
  pharmacie: Pharmacie;
  owner: User;
  operation: Operation;
  commandeType: StatutTypePayload;
  commandeStatut: StatutTypePayload;
  dateCommande: string;
  nbrRef: number;
  quantite: number;
  uniteGratuite: number;
  prixBaseTotalHT: number;
  valeurRemiseTotal: number;
  prixNetTotalHT: number;
  fraisPort: number;
  valeurFrancoPort: number;
  remiseGlobale: number;
  commentaireInterne: string;
  commentaireExterne: string;
  pathFile: string;
  dateCreation: string;
  dateModification: string;
  commandeLigens: CommandeLigne[];
  idGroupement: string;
}

export interface CommandeLigne extends Searchable {
  type: 'commandeligne';
  id: string;
  pharmacie: Pharmacie;
  produitCanal: ProduitCanal;
  status: StatutTypePayload;
  optimiser: number;
  quantiteCdee: number;
  quantiteLivree: number;
  uniteGratuite: number;
  prixBaseHT: number;
  remiseLigne: number;
  remiseGamme: number;
  prixNetUnitaireHT: number;
  prixTotalHT: number;
  dateCreation: string;
  dateModification: string;
}

export interface Panier {
  id: string;
  pharmacie: Pharmacie;
  owner: User;
  operation: Operation;
  nbrRef: number;
  quantite: number;
  uniteGratuite: number;
  prixBaseTotalHT: number;
  valeurRemiseTotal: number;
  prixNetTotalHT: number;
  remiseGlobale: number;
  fraisPort: number;
  valeurFrancoPort: number;
  dateCreation: string;
  dateModification: string;
  panierLignes: PanierLigne[];
}

export interface PanierLigne {
  id: string;
  produitCanal: ProduitCanal;
  optimiser: number;
  validate: boolean;
  quantite: number;
  uniteGratuite: number;
  prixBaseHT: number;
  remiseLigne: number;
  remiseGamme: number;
  prixNetUnitaireHT: number;
  prixTotalHT: number;
  dateCreation: string;
  dateModification: string;
}

export interface Operation extends Searchable {
  type: 'operation';
  id: string;
  pharmacieCible: Pharmacie[];
  actualite: Actualite;
  accordCommercial: boolean;
  fichierPresentations: Fichier[];
  isRemoved: boolean;
  pharmacieVisitors: Pharmacie[];
  libelle: string;
  niveauPriorite: number;
  description: string;
  dateDebut: string;
  dateFin: string;
  dateCreation: string;
  dateModification: string;
  operationPharmacie: OperationPharmacie;
  commandeType: StatutTypePayload;
  commandeCanal: StatutTypePayload;
  nombrePharmaciesConsultes: number;
  nombrePharmaciesCommandees: number;
  nombreProduitsCommandes: number;
  nombreTotalTypeProduitsCommandes: number;
  CAMoyenParPharmacie: number;
  CATotal: number;
  nombreProduitsMoyenParPharmacie: number;
  idGroupement: string;
  userSmyleys: UserSmyleyResult;
  commandePassee: boolean;
  qteProduitsPharmacieCommandePassee: number;
  qteTotalProduitsCommandePassee: number;
  nombreCommandePassee: number;
  totalCommandePassee: number;
  operationArticlesWithQte: OperationArticle[];
  caMoyenPharmacie: number;
  nbPharmacieCommande: number;
  nbMoyenLigne: number;
  item: Item;
  marche: Marche;
  operation: Operation;
  nbPartage: number;
  nbRecommandation: number;
  isShared: boolean;
  operationArticles(takeArticles: number, skipArticles: number): OperationArticleResult;
  actions(take: number, skip: number): ActionResult;
  comments(takeComments: number, skipComments: number): CommentResult;
  seen(idPharmacie: string, userId: string): boolean;
}

export interface OperationPharmacie {
  id: string;
  fichierCible: Fichier;
  globalite: boolean;
  dateCreation: string;
  dateModification: string;
  pharmaciedetails: OperationPharmacieDetail[];
}

export interface OperationPharmacieDetail {
  id: string;
  pharmacie: Pharmacie;
  dateCreation: string;
  dateModification: string;
}

export interface OperationArticle {
  id: string;
  produitCanal: ProduitCanal;
  dateCreation: string;
  dateModification: string;
}

export interface OperationArticleCible {
  id: string;
  fichierCible: Fichier;
  dateCreation: string;
  dateModification: string;
}

export interface StatutTypePayload {
  id: string;
  code: string;
  libelle: string;
  dateCreation: string;
  dateModification: string;
}

export interface UpdatePasswordPayload {
  success: boolean;
}

export interface Notification {
  id: string;
  type: string;
  from: User;
  to: User;
  targetId: string;
  targetName: string;
  message: string;
  seen: boolean;
  typeActualite: string;
  active: boolean;
  isRemoved: boolean;
  dateActive: string;
  dateCreation: string;
  dateModification: string;
}
export interface Comment extends Searchable {
  type: string;
  id: string;
  content: string;
  item: Item;
  idItemAssocie: string;
  user: User;
  groupement: Groupement;
  isRemoved: boolean;
  codeMaj: string;
  smyleys: [Smyley];
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
  fichiers: Fichier[];
  comments(skip: number, first: number): CommentResult;
}

export interface CommentResult {
  total: number;
  data: Comment[];
}

export interface OperationArticleResult {
  total: number;
  data: OperationArticle[];
}

export interface NotificationResult {
  total: number;
  notSeen: number;
  data: Notification[];
}

export interface Item extends Searchable {
  type: 'item';
  id: string;
  code: string;
  name: string;
  codeItem: string;
  fichier: Fichier;
  parent: Item;
  dateCreation: string;
  dateModification: string;
}

export interface Smyley {
  id: string;
  groupement: Groupement;
  nom: string;
  photo: string;
  note: number;
  dateCreation: string;
  dateModification: string;
}

export interface RemiseDetail {
  id: string;
  quantiteMin: number;
  quantiteMax: number;
  pourcentageRemise: number;
  nombreUg: number;
  codeCipUg: string;
  codeCip13Ug: string;
}

export interface ArticleCommande {
  id: string;
  quantite: number;
}

// A MODIFIER
export interface Sale {
  date: string;
  value: number;
}

export interface OperationResult {
  total: number;
  data: Operation[];
}

export interface PrixTotalOperation {
  qteTotal: number;
  prixBaseTotal: number;
  prixNetTotal: number;
  uniteGratuite: number;
}

export interface PresignedUrl {
  filePath: string;
  presignedUrl: string;
}

export interface Fichier {
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  avatar: Avatar;
  urlPresigned: string;
  publicUrl: string;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface Avatar extends Searchable {
  type: string;
  id: string;
  description: string;
  codeSexe: string;
  fichier: Fichier;
  userCreation: User;
  dateCreation: string;
  dateModification: string;
  userModification: User;
  idGroupement: string;
}

export interface UserPhoto {
  id: string;
  user: User;
  fichier: Fichier;
  dateCreation: string;
  dateModification: string;
}

export interface GroupementLogo {
  id: string;
  groupement: Groupement;
  fichier: Fichier;
  dateCreation: string;
  dateModification: string;
}

export interface Actualite extends Searchable {
  type: 'actualite';
  id: string;
  item: Item;
  origine: ActualiteOrigine;
  description: string;
  actionAuto: boolean;
  niveauPriorite: number;
  laboratoire: Laboratoire;
  libelle: string;
  dateDebut: string;
  dateFin: string;
  fichierPresentations: Fichier[];
  isRemoved: boolean;
  actualiteCible: ActualiteCible;
  laboratoiresCible: Laboratoire[];
  partenairesCible: Partenaire[];
  pharmaciesCible: Pharmacie[];
  pharmaciesRolesCible: Role[];
  typePresidentCible: TypePresidentCible;
  presidentsCibles: Titulaire[];
  servicesCible: Service[];
  pharmaciesPresidentCible: Pharmacie[];
  dateCreation: string;
  dateModification: string;
  userSmyleys: UserSmyleyResult;
  idGroupement: string;
  nbSeenByPharmacie: number;
  operation: Operation;
  action: Action;
  nbPartage: number;
  nbRecommandation: number;
  isShared: boolean;
  seen(idPharmacie: string, userId: string): boolean;
  actions(take: number, skip: number): ActionResult;
  comments(takeComments: number, skipComments: number): CommentResult;
}

export interface ActualiteCible {
  id: string;
  globalite: boolean;
  fichierCible: Fichier;
  dateCreation: string;
  dateModification: string;
}

export interface ActualiteOrigine {
  id: string;
  code: string;
  libelle: string;
  ordre: string;
  dateCreation: string;
  dateModification: string;
  countActualites: number;
  type: string;
}

export interface Service extends Searchable {
  type: 'service';
  id: string;
  code: string;
  nom: string;
  dateCreation: string;
  dateModification: string;
  userCreation: User;
  userModification: User;
  countUsers(idGroupement: string): number;
}

export interface CommandeType extends Searchable {
  type: 'commandeType';
  id: string;
  libelle: string;
  code: string;
  countOperationsCommercials: number;
}

export interface CommandeCanal extends Searchable {
  type: 'commandeCanal';
  id: string;
  libelle: string;
  code: string;
  countOperationsCommercials: number;
  countCanalArticles: number;
}

export interface UserSmyley {
  id: string;
  item: Item;
  idSource: string;
  user: User;
  smyley: Smyley;
  isRemoved: boolean;
  dateCreation: string;
  dateModification: string;
  groupement: Groupement;
}

export interface UserSmyleyResult {
  total: number;
  data: UserSmyley[];
}

export interface Parameter {
  id: string;
  code: string;
  name: string;
  parameterType: ParameterType;
  parameterGroupe: ParameterGroupe;
  category: string;
  defaultValue: string;
  value: ParameterValue;
  dateCreation: string;
  dateModification: string;
}

export interface ParameterType {
  id: string;
  code: string;
  nom: string;
}

export interface ParameterGroupe {
  id: string;
  code: string;
  nom: string;
}

export interface ParameterValue {
  id: string;
  value: string;
}

export interface ParameterInput {
  id: string;
  value: string;
}

export interface Departement {
  id: string;
  code: string;
  nom: string;
  region: Region;
  dateCreation: string;
  dateModification: string;
  userCreation: User;
  userModification: User;
}

export interface Region {
  id: string;
  nom: string;
  departements: Departement[];
  grandeRegion: GrandeRegion;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
  userCreation: User;
  userModification: User;
}

export interface GrandeRegion {
  id: string;
  nom: string;
}

export interface TitulaireAffectation extends Searchable {
  type: 'titulaireaffectation';
  id: string;
  codeDepartement?: string;
  dateDebut?: string;
  dateFin?: string;
  typeAffectation?: TypeAffectation;
  departement?: Departement;
  titulaire?: Titulaire;
  titulaireDemandeAffectation?: TitulaireDemandeAffectation;
  titulaireFonction?: TitulaireFonction;
  codeMaj?: string;
  dateCreation: string;
  dateModification: string;
  userCreation?: User;
  userModification?: User;
}

export interface TitulaireDemandeAffectation extends Searchable {
  type: 'titulairedemandeaffectation';
  id: string;
  dateHeureDemande: string;
  demandeAppliquee: boolean;
  typeAffectation?: TypeAffectation;
  titulaireFonction?: TitulaireFonction;
  titulaireRemplacent?: Titulaire;
  codeMaj?: string;
  dateCreation: string;
  dateModification: string;
  service?: Service;
  userCreation?: User;
  userModification?: User;
}

export interface TitulaireFonction extends Searchable {
  type: 'titulairefonction';
  id: string;
  nom: string;
  code: string;
  codeMaj?: string;
  dateCreation: string;
  dateModification: string;
  userCreation?: User;
  userModification?: User;
}

export interface OperationViewer {
  id: string;
  idOperation: Operation;
  idPharmacie: Pharmacie;
  idUser: User;
}

export interface Application {
  applicationGUID: string;
  name: string;
  type: string;
  url: string;
  icon: string;
  needConfiguration: boolean;
  options: number;
  settingOptions: number;
  isNew: boolean;
  lastTimeUsed: Date;
  nrofTimesUsed: number;
  helloIdurl: string;
  iconlink: string;
}

export interface OperationInput {
  idOperation?: string;
  codeItem: string;
  libelle: string;
  niveauPriorite: number;
  description: string;
  dateDebut: string;
  dateFin: string;
  idLaboratoire?: string;
  fichierPresentations: FichierInput[];
  globalite?: boolean;
  typeCommande?: string;
  idsPharmacie?: string[];
  articles?: OperationArticleInput[];
  fichierPharmacie?: FichierInput;
  fichierProduit?: FichierInput;
  idCanalCommande: string;
  accordCommercial: boolean;
  idProject: string;
  actionPriorite: string;
  actionDescription: string;
  actionDueDate: string;
  caMoyenPharmacie: number;
  nbPharmacieCommande: number;
  nbMoyenLigne: number;
  idMarche?: string;
  idPromotion?: string;
}

export interface FichierInput {
  id?: string;
  idAvatar?: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface ActualitesCiblesCountResult {
  code: string;
  name: string;
  total: number;
}

export interface UserTitulaire {
  id: string;
  isPresident: boolean;
  titulaire: Titulaire;
}

export interface PharmacieRolesInput {
  roles: string[];
  pharmacies: string[];
}

export interface PresidentRegionsInput {
  type: TypePresidentCible;
  presidents: string[];
}

export interface HelloIdSso {
  id: string;
  groupement: Groupement;
  helloIdUrl: string;
  helloIDConsumerUrl: string;
  x509Certificate: string;
  apiKey: string;
  apiPass: string;
  dateCreation: Date;
  dateModification: Date;
}

export interface Connexion {
  id: string;
  ip: string;
  emplacement: string;
  statut: ConnexionStatut;
  appareil: string;
  user: User;
}

export interface CheckIpPayload {
  accessToken: string;
}

interface ActiviteUser extends Searchable {
  type: 'activiteUser';
  id: string;
  dateCreation: string;
  idUser: User;
  dateActvite: string;
  idActiviteType: ActiviteType;
  log: string;
}

export interface ActiviteType {
  id: string;
  code: string;
  nom: string;
}

export interface OperationArticleInput {
  idCanalArticle: string;
  quantite: number;
}

export interface PanierPriceByCanal {
  nbrRef: number | null;
  prixBaseTotalHT: number | null;
  valeurRemiseTotal: number | null;
  prixNetTotalHT: number | null;
  remiseGlobale: number | null;
  quantite: number | null;
  uniteGratuite: number | null;
}

export interface HelloidGroup {
  name: string;
  groupGuid: string;
  managedByUserGuid: string;
  immutableId: string;
  isEnabled: boolean;
  isDefault: boolean;
  isQrEnabled: boolean;
  isDeleted: boolean;
  source: string;
  application: Application[];
}
export interface HelloidGroupInfo {
  name: string;
  groupGuid: string;
  managedByUserGuid: string;
  immutableId: string;
  isEnabled: boolean;
  isDefault: boolean;
  isQrEnabled: boolean;
  isDeleted: boolean;
  source: string;
}
export interface ExternalMapping {
  id: string;
  idExternaluser: string;
  idClient: string;
  password: string;
  dateCreation: string;
  dateModification: string;
}

export interface SsoApplication {
  id: string;
  nom: string;
  url: string;
  icon?: string;
  ssoType: SsoType;
  ssoTypeid: string;
  mappings?: MappingVariable[];
  dateCreation: string;
  dateModification: string;
}

export interface MappingVariable {
  id: string;
  nom: string;
  value: string;
  ssoApplication: SsoApplication;
  dateCreation: string;
  dateModification: string;
}

export interface ParameterValueType {
  value: string;
  type: string;
}

export interface Publicite extends Searchable {
  type: 'publicite';
  id: string;
  libelle: string;
  description: string;
  typeEspace: TypeEspace;
  origine: OriginePublicite;
  url: string;
  ordre: number;
  dateDebut: string;
  dateFin: string;
  item: Item;
  idItemSource: string;
  image: Fichier;
  isRemoved: boolean;
  groupement: Groupement;
  dateCreation: string;
  dateModification: string;
}

export interface SuiviPublicitaire extends Searchable {
  type: 'suiviPublicitaire';
  id: string;
  publicite: Publicite;
  user: User;
  transformCommande: boolean;
  commande: Commande;
  dateClick: string;
  dateCreation: string;
  dateModification: string;
  nbClick: number;
  nbView: number;
  nbAccesOp: number;
  nbCommande: number;
}

export interface ResultStatistiqueBusiness {
  total: number;
  data: PharmacieStatistiqueBusiness[];
}

export interface ResultStatistiqueFeedback {
  total: number;
  data: PharmacieStatistiqueFeedback[];
}

export interface PharmacieStatistiqueBusiness {
  id: string;
  operation: Operation;
  pharmacie: Pharmacie;
  caTotal: number;
  nbCommandes: number;
  nbPharmacies: number;
  nbProduits: number;
  nbLignes: number;
  caMoyenParPharmacie: number;
  totalRemise: number;
  globaliteRemise: number;
}

export interface IndicateurStatistiqueBusiness {
  operation: Operation[];
  pharmacie: Pharmacie[];
  caTotal: number;
  nbCommandes: number;
  nbPharmacies: number;
  nbProduits: number;
  nbLignes: number;
  caMoyenParPharmacie: number;
  totalRemise: number;
  globaliteRemise: number;
}

export interface PharmacieStatistiqueFeedback {
  id: string;
  itemAssocie: Searchable;
  pharmacie: Pharmacie;
  cibles: number;
  lu: number;
  adore: number;
  content: number;
  aime: number;
  mecontent: number;
  deteste: number;
  commentaires: number;
  partages: number;
  recommandations: number;
}

export interface IndicateurStatistiqueFeedback {
  itemAssocies: Searchable[];
  pharmacies: Pharmacie[];
  cibles: number;
  lu: number;
  adore: number;
  content: number;
  aime: number;
  mecontent: number;
  deteste: number;
  commentaires: number;
  partages: number;
  recommandations: number;
}

export interface Acte extends Searchable {
  type: 'acte';
  id: string;
  codeActe: number;
  libelleActe: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface TVA extends Searchable {
  type: 'tva';
  id: string;
  codeTva: number;
  tauxTva: number;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface SousGammeCatalogue {
  id: string;
  codeGammeSous: number;
  libelle: string;
  numOrdre: number;
  estDefaut: number;
  gammeCatalogue: GammeCatalogue;
  dateCreation: string;
  dateModification: string;
}

export interface GammeCatalogue extends Searchable {
  type: 'gammeCatalogue';
  id: string;
  codeGamme: number;
  libelle: string;
  numOrdre: number;
  estDefaut: number;
  sousGammes: SousGammeCatalogue[];
  dateCreation: string;
  dateModification: string;
}

export interface ProduitCanalInput {
  id?: string;
  qteStock?: number;
  qteMin?: number;
  stv?: number;
  unitePetitCond?: number;
  prixFab: number;
  prixPhv?: number;
  datePeremption?: string;
  dateRetrait?: string;
  dateCreation?: string;
  dateModification?: string;
  idCanalSousGamme?: string;
  codeSousGammeCatalogue?: number;
  codeCommandeCanal?: string;
  isRemoved: boolean;
}

export interface Marche extends Searchable {
  type: 'marche';
  id: string;
  nom: string;
  dateDebut: string;
  dateFin: string;
  marcheType: MarcheType;
  avecPalier: boolean;
  commandeCanal: CommandeCanal;
  status: MarcheStatus;
  nbAdhesion: number;
  nbPalier: number;
  userCreated: User;
  userModified: User;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
  isRemoved: boolean;
  canalArticles: ProduitCanal[];
  laboratoires: MarcheLaboratoire[];
  groupeClients: MarcheGroupeClient[];
  remise(idPharmacie: string): Remise;
}

export interface MarcheGroupeClient {
  id: string;
  groupeClient: GroupeClient;
  dateEngagement: string;
  commentaire: string;
  remise: Remise;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}
export interface GroupeClient extends Searchable {
  type: 'groupeClient';
  id: string;
  codeGroupe: number;
  nomGroupe: string;
  nomCommercial: string;
  dateValiditeDebut: string;
  dateValiditeFin: string;
  source: number;
  sortie: number;
  pharmacies: Pharmacie[];
  dateCreation: string;
  dateModification: string;
}

export interface Remise {
  id: string;
  dateDebut: string;
  dateFin: string;
  nbRefMin: number;
  nomRemise: string;
  modelRemise: string;
  typeRemise: string;
  source: SourceRemise;
  active: boolean;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
  commandeCanal: CommandeCanal;
  remiseDetails: RemiseDetail[];
}

export interface MarcheLaboratoire {
  id: string;
  laboratoire: Laboratoire;
  numOrdre: number;
  obligatoire: boolean;
  dateCreation: string;
  dateModification: string;
}

export interface MarcheLaboratoireInput {
  idLaboratoire: string;
  obligatoire: boolean;
}

export interface GroupeClienRemisetInput {
  codeGroupeClient: number;
  idRemise: string;
  remiseDetails: RemiseDetailInput[];
}

export interface RemiseDetailInput {
  quantiteMin: number;
  quantiteMax: number;
  pourcentageRemise: number;
  remiseSupplementaire: number;
}

export interface MarcheInput {
  id: string;
  nom: string;
  dateDebut: string;
  dateFin: string;
  marcheType: any;
  avecPalier: boolean;
  status: any;
  codeCanal: string;
  laboratoires: MarcheLaboratoireInput[];
  idsCanalArticle: string[];
  groupeClients: GroupeClienRemisetInput[];
}

export interface Promotion extends Searchable {
  type: 'promotion';
  id: string;
  nom: string;
  dateDebut: string;
  dateFin: string;
  promotionType: MarcheType;
  commandeCanal: CommandeCanal;
  status: MarcheStatus;
  userCreated: User;
  userModified: User;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
  isRemoved: boolean;
  canalArticles: ProduitCanal[];
  groupeClients: PromotionGroupeClient[];
  remise(idPharmacie: string): Remise;
}

export interface PromotionGroupeClient {
  id: string;
  groupeClient: GroupeClient;
  dateAchat: string;
  remise: Remise;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface PromotionInput {
  id: string;
  nom: string;
  dateDebut: string;
  dateFin: string;
  promotionType: any;
  status: any;
  codeCanal: string;
  idsCanalArticle: string[];
  groupeClients: GroupeClienRemisetInput[];
}

export interface StatistiquePayload {
  objectif: string;
  value: string;
}

export interface FeedbakPayload {
  objectif: number;
  value: number;
  nbPharmacieSmeyley: number;
}

export interface ResultPayload {
  key: string;
  value: string;
}

export interface Traitement extends Searchable {
  type: 'traitement';
  id: string;
  code: string;
  nom: string;
  module: Module;
  roles: Role[];
  idGroupements: string[];
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface Module {
  id: string;
  code: string;
  nom: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface UserTraitement {
  id: string;
  codeTraitement: string;
  access: boolean;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface RoleTraitement {
  id: string;
  codeTraitement: string;
  access: boolean;
  groupement: Groupement;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface Contact extends Searchable {
  type: string;
  id: string;
  adresse1: string;
  adresse2: string;
  cp: string;
  ville: string;
  pays: string;
  telProf: string;
  faxProf: string;
  faxPerso: string;
  telMobProf: string;
  telPerso: string;
  telMobPerso: string;
  mailProf: string;
  mailPerso: string;
  siteProf: string;
  sitePerso: string;
  whatsAppMobProf: string;
  whatsappMobPerso: string;
  compteSkypeProf: string;
  compteSkypePerso: string;
  urlLinkedinProf: string;
  urlLinkedinPerso: string;
  urlTwitterProf: string;
  urlTwitterPerso: string;
  urlFacebookProf: string;
  urlFacebookPerso: string;
  urlMessenger: string;
  urlYoutube: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface HealthCheck {
  error: boolean | null;
}
export interface PostgresDb {
  status: boolean | null;
  usersContains: number | null;
}
export interface ServiceS3 {
  status: boolean | null;
  host: string | null;
}

export interface ContactInput {
  id?: string;
  adresse1: string;
  adresse2?: string;
  cp?: string;
  ville?: string;
  pays?: string;
  telProf?: string;
  faxProf?: string;
  faxPerso?: string;
  telMobProf?: string;
  telPerso?: string;
  telMobPerso?: string;
  mailProf?: string;
  mailPerso?: string;
  siteProf?: string;
  sitePerso?: string;
  whatsAppMobProf?: string;
  whatsappMobPerso?: string;
  compteSkypeProf?: string;
  compteSkypePerso?: string;
  urlLinkedinProf?: string;
  urlLinkedinPerso?: string;
  urlTwitterProf?: string;
  urlTwitterPerso?: string;
  urlFacebookProf?: string;
  urlFacebookPerso?: string;
  urlMessenger?: string;
  urlYoutube?: string;
}

export interface ServicePharmacie {
  id: string;
  nom: string;
  typeService: TypeService;
}

export interface PersonnelAffectation extends Searchable {
  type: 'personnelaffectation';
  id: string;
  codeDepartement?: string;
  dateDebut?: string;
  dateFin?: string;
  typeAffectation?: TypeAffectation;
  departement?: Departement;
  personnel?: Personnel;
  personnelFonction?: PersonnelFonction;
  personnelDemandeAffectation?: PersonnelDemandeAffectation;
  codeMaj?: string;
  dateCreation: string;
  dateModification: string;
  userCreation?: User;
  userModification?: User;
}

export interface PersonnelFonction extends Searchable {
  type: 'personnelfonction';
  id: string;
  nom: string;
  code: string;
  codeMaj?: string;
  dateCreation: string;
  dateModification: string;
  service?: Service;
  userCreation?: User;
  userModification?: User;
}

export interface PersonnelDemandeAffectation extends Searchable {
  type: 'personneldemandeaffectation';
  id: string;
  dateHeureDemande: string;
  demandeAppliquee: boolean;
  typeAffectation?: TypeAffectation;
  personnelFonction?: PersonnelFonction;
  personnelRemplacent?: Personnel;
  codeMaj?: string;
  dateCreation: string;
  dateModification: string;
  service?: Service;
  userCreation?: User;
  userModification?: User;
}

export interface PersonnelAffectationResult {
  total: number;
  data: PersonnelAffectation[];
}

export interface PersonnelFonctionResult {
  total: number;
  data: PersonnelFonction[];
}

export interface PersonnelDemandeAffectationResult {
  total: number;
  data: PersonnelDemandeAffectation[];
}

export interface TitulaireAffectationResult {
  total: number;
  data: TitulaireAffectation[];
}

export interface TitulaireFonctionResult {
  total: number;
  data: TitulaireFonction[];
}

export interface TitulaireDemandeAffectationResult {
  total: number;
  data: TitulaireDemandeAffectation[];
}

export interface AffectationInput {
  id?: string;
  typeAffectation: TypeAffectation;
  idFonction: string;
  idsDepartements: string[];
  idAffecte: string;
  idRemplacent?: string;
  dateDebut?: string;
  dateFin?: string;
}

//  Messagerie
export interface Messagerie {
  id: string;
  messagerieTheme?: MessagerieTheme;
  messagerieSource: MessagerieSource;
  typeMessagerie: MessagerieType;
  objet?: string;
  message?: string;
  typeFilter: string;
  messagerieDepart: Messagerie;
  userEmetteur: User;
  recepteurs: [MessagerieHisto];
  lu: boolean;
  isRemoved: boolean;
  dateHeureMessagerie: string;
  attachments: [MessagerieFichierJoint];
  codeMaj?: string;
  dateCreation: string;
  dateModification: string;
}

export interface MessagerieHisto {
  id: string;
  userRecepteur: User;
  dateHeureLecture?: string;
  codeMaj?: string;
  userCreation?: User;
  userModification?: User;
  dateCreation: string;
  dateModification: string;
}

export interface MessagerieFichierJoint {
  id: string;
  messagerie: Messagerie;
  fichier: Fichier;
  codeMaj?: string;
  dateCreation: string;
  dateModification: string;
}

export interface MessagerieTheme {
  id: string;
  nom: string;
  codeMaj?: string;
  dateCreation: string;
  dateModification: string;
}

export interface MessagerieSource {
  id: string;
  nom: string;
  codeMaj?: string;
  dateCreation: string;
  dateModification: string;
}

export interface MessagerieResult {
  total: number;
  data: Messagerie[];
}

// End messagerie

export interface MessagerieAttachmentInput {
  chemin: string;
  nomOriginal: string;
}

export interface MessagerieInput {
  recepteursIds: string[];
  message: string;
  objet: string;
  typeMessagerie?: MessagerieType;
  id?: string;
  themeId?: string;
  sourceId?: string;
  attachments?: MessagerieAttachmentInput[];
}

export interface ProduitCategorie {
  id: string;
  libelle: string;
  dateCreation: string;
  dateModification: string;
}

export interface ProduitTypeCode {
  id: string;
  libelle: string;
  resipIdTypeCode: number;
  dateCreation: string;
  dateModification: string;
}

export interface ServiceType extends Searchable {
  type: string;
  id: string;
  libelle: string;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface ServicePartenaire extends Searchable {
  type: string;
  id: string;
  nom: string;
  dateDemarrage: string;
  nbCollaboQualifie: number;
  prixHtInstallation: number;
  serviceType: ServiceType;
  produit: Produit;
  partenaireService: Partenaire;
  codeMaj: string;
  commentaire: string;
  isRemoved: boolean;
  dateCreation: string;
  dateModification: string;
}

export interface Ressource extends Searchable {
  type: string;
  id: string;
  typeRessource: string;
  item: Item;
  code: string;
  libelle: string;
  codeMaj: string;
  fichier: Fichier;
  url: string;
  partenaireService: Partenaire;
  groupement: Groupement;
  dateChargement: string;
  isRemoved: boolean;
  dateCreation: string;
  dateModification: string;
}

export interface PartenaireServiceSuite extends Searchable {
  type: string;
  id: string;
  nom: string;
  adresse1: string;
  adresse2: string;
  codePostal: string;
  telephone: string;
  ville: string;
  telecopie: string;
  email: string;
  webSiteUrl: string;
  codeMaj: string;
  groupement: Groupement;
  dateCreation: string;
  dateModification: string;
}

export interface PartenaireServicePartenaire extends Searchable {
  type: string;
  id: string;
  dateDebutPartenaire: string;
  dateFinPartenaire: string;
  typePartenaire: TypePartenaire;
  statutPartenaire: StatutPartenaire;
  codeMaj: string;
  partenaireService: Partenaire;
  dateCreation: string;
  dateModification: string;
}

export interface ServicePartenaireInput {
  id?: string;
  nom: string;
  dateDemarrage: string;
  nbCollaboQualifie?: number;
  commentaire?: string;
  produit: ProduitInput; // TODO
}

export interface PartenaireServiceInput {
  id?: string;
  nom: string;
  dateDebut: string;
  dateFin: string;
  idTypeService?: string;
  typePartenaire?: TypePartenaire;
  statutPartenaire?: StatutPartenaire;
  adresse1?: string;
  codePostal?: string;
  ville?: string;
  servicePartenaires?: ServicePartenaireInput[];
  idSecteur: string;
  cerclePartenariat: string;
  telephone: string;
  email: string;
}

export interface ProduitInput {
  id?: string;
}

export interface RessourceInput {
  id?: string;
  idPartenaireService?: string;
  typeRessource?: string;
  idItem?: string;
  code: string;
  libelle: string;
  dateChargement: string;
  fichier: FichierInput;
  url: string;
}

export interface PartenaireType extends Searchable {
  type: string;
  id: string;
  libelle: string;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface PartenaireRepresentant extends Searchable {
  type: string;
  id: string;
  civilite: string;
  nom: string;
  prenom: string;
  sexe: string;
  fonction: string;
  commentaire: string;
  afficherComme: string;
  contact: Contact;
  partenaireType: PartenaireType;
  partenaire: Partenaire;
  photo: Fichier;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface PartenaireRepresentantAffectation extends Searchable {
  type: string;
  id: string;
  codeDepartement: string;
  dateDebut: string;
  dateFin: string;
  typeAffectation: TypeAffectation;
  departement: Departement;
  partenaireRepresentant: PartenaireRepresentant;
  partenaireRepresentantDemandeAffectation: PartenaireRepresentantDemandeAffectation;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface PartenaireRepresentantDemandeAffectation extends Searchable {
  type: string;
  id: string;
  dateHeureDemande: string;
  demandeAppliquee: boolean;
  typeAffectation: TypeAffectation;
  fonction: string;
  remplacent: PartenaireRepresentant;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface PartenaireRepresentantInput {
  id?: string;
  idPartenaire: string;
  idPartenaireType: string;
  civilite?: string;
  nom: string;
  prenom?: string;
  sexe?: string;
  fonction?: string;
  photo?: FichierInput;
  afficherComme?: string;
  contact: ContactInput;
}

export interface PartenaireRepresentantAffectationInput {
  id?: string;
  typeAffectation: TypeAffectation;
  fonction?: string;
  idsDepartements: string[];
  idAffecte: string;
  idRemplacent?: string;
  dateDebut?: string;
  dateFin?: string;
}

export interface ServicePartenairePhoto {
  id: string;
  dateCreation: string;
  dateModification: string;
}

export interface Ticket extends Searchable {
  type: string;
  id: string;
  typeTicket: TicketType;
  typeAppel: TicketAppel;
  origineType: OrigineType;
  visibilite: TicketVisibilite;
  idOrganisation: string;
  nomOrganisation: string;
  nomInterlocuteur: string;
  telephoneInterlocuteur: string;
  declarant: User;
  usersConcernees: User[];
  smyley: Smyley;
  dateHeureSaisie: string;
  commentaire: string;
  priority: number;
  motif: TicketMotif;
  statusTicket: StatusTicket;
  fichiers: Fichier[];
  codeMaj: string;
  userCreation: User;
  userModification: User;
  groupement: Groupement;
  dateCreation: string;
  dateModification: string;
}

export interface TicketOrigine extends Searchable {
  type: string;
  id: string;
  code: string;
  nom: string;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  groupement: Groupement;
  dateCreation: string;
  dateModification: string;
}

export interface TicketMotif extends Searchable {
  type: string;
  id: string;
  code: string;
  nom: string;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  groupement: Groupement;
  dateCreation: string;
  dateModification: string;
}

export interface TicketStatut extends Searchable {
  type: string;
  id: string;
  code: string;
  nom: string;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  groupement: Groupement;
  dateCreation: string;
  dateModification: string;
}

export interface TicketFichierJoint {
  id: string;
  fichier: Fichier;
  ticket: Ticket;
  userCreation: User;
  userModification: User;
  groupement: Groupement;
  dateCreation: string;
  dateModification: string;
}

export interface TicketChangementStatut extends Searchable {
  type: string;
  id: string;
  dateHeureChangement: string;
  commentaire: string;
  codeMaj: string;
  ticket: Ticket;
  ticketStatut: TicketStatut;
  ticketChangementCible: TicketChangementCible;
  userCreation: User;
  userModification: User;
  groupement: Groupement;
  dateCreation: string;
  dateModification: string;
}

export interface TicketChangementCible extends Searchable {
  type: string;
  id: string;
  dateHeureAffectation: string;
  commentaire: string;
  codeMaj: string;
  roleCible: Role;
  serviceCible: Service;
  userCible: User;
  ticket: Ticket;
  laboratoireCible: Laboratoire;
  prestataireServiceCible: Partenaire;
  pharmacieCible: Pharmacie;
  statut: TicketStatut;
  userCreation: User;
  userModification: User;
  groupement: Groupement;
  dateCreation: string;
  dateModification: string;
}

export interface TicketInput {
  // Ticket
  id?: string;
  origine: string;
  idOrganisation: string;
  nomInterlocuteur?: string;
  telephoneInterlocuteur?: string;
  dateHeureSaisie?: string;
  commentaire?: string;
  idSmyley?: string;
  visibilite: TicketVisibilite;
  typeTicket: TicketType;
  typeAppel: TicketAppel;
  declarant?: string;
  idMotif?: string;
  fichiers?: FichierInput[];
  idsUserConcernees?: string[];
  priority: number;
  status: StatusTicket;
}

export interface TicketCibleInput {
  id?: string;
  idTicket: string;
  idCurrentUser: string;
  idUserCible?: string;
  idRoleCible?: string;
  idServiceCible?: string;
  idPharmacieCible?: string;
  idLaboratoireCible?: string;
  idPrestataireServiceCible?: string;
  dateHeureAffectation?: string;
  commentaire?: string;
}

export enum TicketVisibilite {
  OUVERT = 'OUVERT',
  PRIVE = 'PRIVE',
}

export enum TicketType {
  RECLAMATION = 'RECLAMATION',
  APPEL = 'APPEL',
}

export interface Aide extends Searchable {
  type: string;
  id: string;
  title: string;
  description: string;
  isRemoved: boolean;
  groupement: Groupement;
  userCreation: User;
  userModification: User;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface AideInput {
  id?: string;
  title: string;
  description: string;
}

export interface ProduitHistorique {
  id: string;
  produit: Produit;
  pharmacie: Pharmacie;
  prixVenteTTC: number;
  prixAchatBaseHT: number;
  prixAchatDernierHT: number;
  prixAchatMoyen: number;
  anneeM00: number;
  moisM00: number;
  venteM00: number;
  venteM01: number;
  venteM02: number;
  venteM03: number;
  venteM04: number;
  venteM05: number;
  venteM06: number;
  venteM07: number;
  venteM08: number;
  venteM09: number;
  venteM10: number;
  venteM11: number;
  venteM12: number;
  venteM13: number;
  venteM14: number;
  venteM15: number;
  venteM16: number;
  venteM17: number;
  venteM18: number;
  codeMaj: string;
  dateCreation: string;
  dateModification: string;
}

export interface ResultProduitHistorique {
  year: number;
  dateHistorique: string;
  data: [GraphDatePayload];
}

export interface GraphDatePayload {
  order: number;
  date: string;
  value: number;
}

export interface MessagerieArgs {
  take?: number;
  skip?: number;
  typeMessagerie?: MessagerieType;
  category?: string;
  year?: number;
  isRemoved?: boolean;
  lu?: boolean;
  orderBy?: string;
  typeFilter?: string;
}

export interface FilterMessageriesCount {
  libelle: string;
  code: string;
  count: number;
}

export interface Project extends Searchable {
  type: string;
  id: string;
  ordre: number;
  name: string;
  isArchived: boolean;
  isRemoved: boolean;
  isShared: boolean;
  typeProject: TypeProject;
  user: User;
  projetParent: Project;
  groupement: Groupement;
  pharmacie: Pharmacie;
  couleur: Couleur;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
  participants: User[];
  isInFavoris: boolean;
  subProjects: Project[];
  activeActions: boolean;
  nbAction(actionStatus: string): number;
}

export interface Action extends Searchable {
  type: string;
  id: string;
  ordre: number;
  description: string;
  dateEcheance: string;
  priority: number;
  isInInbox: boolean;
  isInInboxTeam: boolean;
  isRemoved: boolean;
  actionType: TodoActionType;
  actionParent: Action;
  item: Item;
  idItemAssocie: string;
  project: Project;
  origine: TodoActionOrigine;
  section: TodoSection;
  status: ActionStatus;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
  assignedUsers: User[];
  etiquettes: TodoEtiquette[];
  nbComment: number;
  subActions: Action[];
  isAssigned: boolean;
  firstComment: Comment;
  groupement: Groupement;
}

export interface Couleur extends Searchable {
  type: string;
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface TodoSection extends Searchable {
  type: string;
  id: string;
  ordre: number;
  libelle: string;
  isInInbox: boolean;
  isInInboxTeam: boolean;
  project: Project;
  user: User;
  isRemoved: boolean;
  isArchived: boolean;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
  nbAction: number;
}

export interface CommentaireFichierJoint {
  id: string;
  commentaire: Comment;
  fichier: Fichier;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface CommentaireFichierJointInput {
  id?: string;
  idCommentaire: string;
  idFichier: string;
  codeMaj?: string;
  isRemoved?: boolean;
}

export interface TodoActionAttribution {
  id: string;
  action: Action;
  userSource: User;
  userDestination: User;
  isRemoved: boolean;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface TodoActionChangementStatus {
  id: string;
  action: Action;
  user: User;
  status: ActionStatus;
  isRemoved: boolean;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface ActionResult {
  total: number;
  data: Action[];
}

export enum ActionStatus {
  ACTIVE = 'ACTIVE',
  DONE = 'DONE',
  CLOSED = 'CLOSED',
}

export enum ActionType {
  AMELIORATION = 'AMELIORATION',
  CORRECTIF = 'CORRECTIF',
}

export enum TypeProject {
  PERSONNEL = 'PERSONNEL',
  PROFESSIONNEL = 'PROFESSIONNEL',
  GROUPEMENT = 'GROUPEMENT',
}

export interface ProjectInput {
  id?: string;
  ordre?: number;
  name: string;
  isArchived?: boolean;
  isShared?: boolean;
  isFavoris?: boolean;
  isRemoved?: boolean;
  typeProject: TypeProject;
  idUser: string;
  idParent?: string;
  idPharmacie: string;
  idCouleur?: string;
}

export interface ActionCommentaireInput {
  id?: string;
  content: string;
}

export interface ActionInput {
  id?: string;
  description: string;
  ordre?: number;
  idProject?: string;
  idsEtiquettes?: string[];
  idImportance: string;
  idOrigine?: string;
  idActionType?: string;
  codeItem?: string;
  idItemAssocie?: string;
  idSection?: string;
  status?: ActionStatus;
  dateDebut?: string;
  dateFin?: string;
  commentaire?: ActionCommentaireInput;
  isInInbox?: boolean;
  isInInboxTeam?: boolean;
  isPrivate?: boolean;
  isRemoved?: boolean;
  idActionParent?: string;
  codeMaj?: string;
  idsUsersDestinations?: string[];
}

export interface CouleurInput {
  id?: string;
  code: string;
  libelle: string;
  isRemoved?: boolean;
}

export interface TodoSectionInput {
  id?: string;
  ordre?: number;
  libelle: string;
  isInInbox?: boolean;
  isInInboxTeam?: boolean;
  idProject?: string;
  idUser: string;
  isRemoved?: boolean;
  isArchived?: boolean;
}

export interface TodoActionAttributionInput {
  id?: string;
  idAction: string;
  idUserSource: string;
  idUserDestination: string;
  idPharmacie: string;
  idGroupement: string;
  isRemoved?: boolean;
  codeMaj?: string;
}

export interface TodoActionChangementStatusInput {
  id?: string;
  idAction: string;
  idUser: string;
  status: ActionStatus;
  isRemoved?: boolean;
  codeMaj?: string;
}

export interface TodoActionEtiquette extends Searchable {
  type: string;
  id: string;
  action: Action;
  etiquette: TodoEtiquette;
  isRemoved: boolean;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface TodoActionEtiquetteInput {
  id?: string;
  idAction: string;
  idEtiquette: string;
  isRemoved?: boolean;
  codeMaj?: string;
}

export interface TodoActionOrigine extends Searchable {
  type: string;
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface TodoActionOrigineInput {
  id?: string;
  code: string;
  libelle: string;
  isRemoved?: boolean;
  codeMaj?: string;
}

export interface TodoActionType extends Searchable {
  type: string;
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean;
  codeMaj: string;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface TodoActionTypeInput {
  id?: string;
  code: string;
  libelle: string;
  isRemoved?: boolean;
  codeMaj?: string;
}

export interface TodoEtiquetteFavoris extends Searchable {
  type: string;
  id: string;
  ordre: number;
  etiquette: TodoEtiquette;
  user: User;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface TodoEtiquetteFavorisInput {
  id?: string;
  ordre?: number;
  idEtiquette: string;
  idUser: string;
  codeMaj?: string;
  isRemoved?: boolean;
}

export interface TodoEtiquette extends Searchable {
  type: string;
  id: string;
  ordre: number;
  nom: string;
  couleur: Couleur;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
  isInFavoris: boolean;
  participants: User[];
  isShared: boolean;
  activeActions: boolean;
  nbAction(actionStatus: string): number;
}

export interface TodoEtiquetteInput {
  id?: string;
  ordre?: number;
  nom: string;
  idCouleur?: string;
  codeMaj?: string;
  isRemoved?: boolean;
  isFavoris?: boolean;
}

export interface TodoParticipant {
  id: string;
  project: Project;
  userParticipant: User;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface TodoParticipantInput {
  id?: string;
  idProject: string;
  idUserParticipant: string;
  codeMaj?: string;
  isRemoved?: boolean;
}

export interface TodoProjetFavoris extends Searchable {
  type: string;
  id: string;
  ordre: number;
  project: Project;
  user: User;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface TodoProjetFavorisInput {
  id?: string;
  ordre?: number;
  idProject: string;
  idUser: string;
  codeMaj?: string;
  isRemoved?: boolean;
}

export interface AddParticipantsInput {
  idProject: string;
  idsParticipants: string[];
}

export interface AssignOrUnAssignActionInput {
  idAction: string;
  idUserDestination: string;
}

export interface RemoveParticipantInput {
  idProject: string;
  idParticipant: string;
}

export interface CommentInput {
  id?: string;
  codeItem: string;
  idItemAssocie: string;
  content: string;
  fichiers?: FichierInput[];
}

export interface ActionActivite extends Searchable {
  type: string;
  id: string;
  activiteType: ActionActiviteType;
  action: Action;
  userSource: User;
  userDestination?: User;
  oldStatus?: ActionStatus;
  newStatus?: ActionStatus;
  oldDateEcheance?: string;
  newDateEcheance?: string;
  dateCreation: string;
  dateModification: string;
}

export enum ActionActiviteType {
  EDIT = 'EDIT',
  COMMENT = 'COMMENT',
  ASSIGN_USER = 'ASSIGN_USER',
  UNASSIGN_USER = 'UNASSIGN_USER',
  CHANGE_STATUS = 'CHANGE_STATUS',
  CHANGE_DATE_ECHEANCE = 'CHANGE_DATE_ECHEANCE',
}

export interface ActionActiviteInput {
  id?: string;
  activiteType: ActionActiviteType;
  idAction: string;
  idUserSource: string;
  idUserDestination?: string;
  oldStatus?: ActionStatus;
  newStatus?: ActionStatus;
  oldDateDebut?: string;
  oldDateFin?: string;
  newDateDebut?: string;
  newDateFin?: string;
}

export interface TodoEtiquetteParticipant {
  id: string;
  etiquette: TodoEtiquette;
  userParticipant: User;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface TodoEtiquetteParticipantInput {
  id?: string;
  idEtiquette: string;
  idUserParticipant: string;
  codeMaj?: string;
  isRemoved?: boolean;
}

export interface AddEtiquetteParticipantsInput {
  idEtiquette: string;
  idsParticipants: string[];
}

export interface RemoveEtiquetteParticipantInput {
  idEtiquette: string;
  idParticipant: string;
}

export interface UpdateActionStatusInput {
  idAction: string;
  status: ActionStatus;
}

export interface UpdateActionDateEcheanceInput {
  idAction: string;
  dateEcheance: string;
}

export interface CountProjectResult {
  personnel: number;
  groupement: number;
  professionnel: number;
}

export interface CountTodoResult {
  project: CountProjectResult;
  action: CountActionResult;
}

export interface CountActionResult {
  myActions: number;
  actionsOfOthers: number;
  notAssignedActions: number;
  priorityOne: number;
  priorityTwo: number;
  priorityThree: number;
  priorityFour: number;
  withDateEcheance: number;
  withoutDateEcheance: number;
  inbox: number;
  inboxTeam: number;
  today: number;
  todayTeam: number;
  nextSeventDays: number;
  nextSeventDaysTeam: number;
  all: number;
  taskActive: number;
  taskDone: number;
}

export interface CountTodosInput {
  idUser: string;
  typesProjects: TypeProject[];
  taskStatus?: ActionStatus;
  myTask?: boolean;
  taskOfOthers?: boolean;
  unassignedTask?: boolean;
  priorities: number[];
  withDate?: boolean;
  withoutDate?: boolean;
  useMatriceResponsabilite?: boolean;
}

export enum InformationLiaisonType {
  INCIDENT = 'INCIDENT',
  AMELIORATION = 'AMELIORATION',
  RECLAMATION = 'RECLAMATION',
  ACTUALITE = 'ACTUALITE',
}

export enum InformationLiaisonOrigine {
  INTERNE = 'INTERNE',
  PATIENT = 'PATIENT',
  GROUPE_AMIS = 'GROUPE_AMIS',
  GROUPEMENT = 'GROUPEMENT',
  FOURNISSEUR = 'FOURNISSEUR',
  PRESTATAIRE = 'PRESTATAIRE',
}

export enum InformationLiaisonStatus {
  NOUVELLE = 'NOUVELLE',
  LUES = 'LUES',
  EN_CHARGE = 'EN_CHARGE',
  CLOTURE = 'CLOTURE',
}

export interface InformationLiaison extends Searchable {
  type: string;
  id: string;
  typeInfo: InformationLiaisonType;
  origine: InformationLiaisonOrigine;
  status: InformationLiaisonStatus;
  priority: number;
  description: string;
  declarant: User;
  colleguesConcernees: InformationLiaisonUserConcernee[];
  idFicheIncident: string;
  idFicheAmelioration: string;
  ficheReclamation: Ticket;
  todoAction: Action;
  groupeAmis: GroupeAmis;
  service: Service;
  fournisseur: Laboratoire;
  prestataire: Partenaire;
  groupement: Groupement;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
  prisEnCharge: InformationLiaisonPrisCharge;
  bloquant: boolean;
  fichiersJoints: InformationLiaisonFichierJoint[];
  userSmyleys: UserSmyleyResult;
  nbPartage: number;
  nbRecommandation: number;
  isShared: boolean;
}

export interface InformationLiaisonInput {
  id?: string;
  idItem: string;
  idItemAssocie: string;
  idType: string;
  idOrigine: string;
  idOrigineAssocie: string;
  statut: string;
  priority: number;
  titre: string;
  description: string;
  idDeclarant: string;
  idColleguesConcernees: string[];
  idFicheIncident?: string;
  idFicheAmelioration?: string;
  idFicheReclamation?: string;
  idTodoAction?: string;
  idGroupeAmis?: string;
  idService?: string;
  idFournisseur?: string;
  idPrestataire?: string;
  codeMaj?: string;
  isRemoved?: boolean;
  bloquant?: boolean;
  fichiers?: FichierInput[];
  idUrgence: string;
  idImportance: string;
  idFonction: string;
  idTache: string;
}

export interface TakeChargeInformationLiaisonInput {
  id: string;
  date: string;
  description: string;
  files?: FichierInput[];
}

export interface InformationLiaisonPrisCharge {
  id: string;
  date: string;
  description: string;
  informationLiaison: InformationLiaison;
  fichiersJoints: InformationLiaisonPrisChargeFichierJoint[];
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface InformationLiaisonPrisChargeFichierJoint {
  id: string;
  informationLiaisonPrisCharge: InformationLiaisonPrisCharge;
  fichier: Fichier;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface InformationLiaisonUserConcernee extends Searchable {
  type: string;
  id: string;
  informationLiaison: InformationLiaison;
  userConcernee: User;
  statut: string;
  filtre: string;
  lu: boolean;
  prisEnCharge: InformationLiaisonPrisCharge;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export enum GroupeAmisStatus {
  ACTIVER = 'ACTIVER',
  BLOQUER = 'BLOQUER',
}

export enum GroupeAmisType {
  PHARMACIE = 'PHARMACIE',
  DEPARTEMENT = 'DEPARTEMENT',
  REGION = 'REGION',
  PERSONNALISE = 'PERSONNALISE',
}

export interface GroupeAmisInput {
  id?: string;
  nom: string;
  description?: string;
  status: GroupeAmisStatus;
  type: GroupeAmisType;
  predefine?: boolean;
  codeMaj?: string;
  isRemoved?: boolean;
  idPharmacie?: string;
  idDepartement?: string;
  idRegion?: string;
}

export interface GroupeAmis extends Searchable {
  type: string;
  id: string;
  nom: string;
  description: string;
  status: GroupeAmisStatus;
  typeGroupeAmis: GroupeAmisType;
  predefine: boolean;
  pharmacie: Pharmacie;
  departement: Departement;
  region: Region;
  codeMaj: string;
  groupement: Groupement;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
  nbPharmacie: number;
  nbMember: number;
  nbPartage: number;
  nbRecommandation: number;
  pharmacieMembres: Pharmacie[];
}

export interface GroupeAmisDetail extends Searchable {
  type: string;
  id: string;
  groupeAmis: GroupeAmis;
  pharmacie: Pharmacie;
  confirmedInvitation: boolean;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface GroupeAmisDetailInput {
  id?: string;
  idGroupeAmis: string;
  idPharmacie: string;
  confirmedInvitation?: boolean;
  isRemoved?: boolean;
}

export enum PartageType {
  RECOMMANDATION = 'RECOMMANDATION',
  PARTAGE = 'PARTAGE',
}

export interface PartageInput {
  id?: string;
  type: PartageType;
  toutesPharmacies?: boolean;
  toutesGroupesAmis?: boolean;
  codeItem: string;
  idItemAssocie: string;
  idUserPartageant?: string;
  idFonctionCible?: string;
  idsGroupesAmis?: string[];
  idsPharmacies?: string[];
  dateHeurePartage: string;
  codeMaj?: string;
  isRemoved?: boolean;
}

export interface Partage extends Searchable {
  type: string;
  id: string;
  typePartage: PartageType;
  toutesPharmacies: boolean;
  toutesGroupesAmis: boolean;
  item: Item;
  idItemAssocie: string;
  userPartageant: User;
  fonctionCible: PartageFonctionCible;
  groupeAmisCibles: PartageGroupeAmisCible[];
  pharmacieCinles: PartagePharmacieCible[];
  dateHeurePartage: string;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface PartageFonctionCibleInput {
  id?: string;
  code: string;
  libelle: string;
  codeMaj?: string;
  isRemoved?: boolean;
}

export interface PartageFonctionCible {
  id: string;
  code: string;
  libelle: string;
  codeMaj: string;
  isRemoved: boolean;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface PartagePharmacieCibleInput {
  id?: string;
  idPartage: string;
  idPharmacie: string;
}

export interface PartagePharmacieCible {
  id: string;
  partage: Partage;
  pharmacie: Pharmacie;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface PartageGroupeAmisCibleInput {
  id?: string;
  idPartage: string;
  idGroupeAmis: string;
}

export interface PartageGroupeAmisCible {
  id: string;
  partage: Partage;
  groupeAmis: GroupeAmis;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export enum IdeeOuBonnePratiqueOrigine {
  INTERNE = 'INTERNE',
  PATIENT = 'PATIENT',
  GROUPE_AMIS = 'GROUPE_AMIS',
  GROUPEMENT = 'GROUPEMENT',
  FOURNISSEUR = 'FOURNISSEUR',
  PRESTATAIRE = 'PRESTATAIRE',
}

export enum IdeeOuBonnePratiqueStatus {
  NOUVELLE = 'NOUVELLE',
  EN_COURS = 'EN_COURS',
  VALIDEE = 'VALIDEE',
  REJETEE = 'REJETEE',
}

export interface IdeeOuBonnePratique extends Searchable {
  type: string;
  id: string;
  origine: IdeeOuBonnePratiqueOrigine;
  status: IdeeOuBonnePratiqueStatus;
  classification?: IdeeOuBonnePratiqueClassification;
  title: string;
  auteur: User;
  concurent?: string;
  fournisseur?: Laboratoire;
  prestataire?: Partenaire;
  groupeAmis?: GroupeAmis;
  service?: Service;
  beneficiaires_cles?: string;
  contexte?: string;
  objectifs?: string;
  resultats?: string;
  facteursClesDeSucces?: string;
  contraintes?: string;
  groupement?: Groupement;
  codeMaj?: string;
  isRemoved?: boolean;
  userCreation?: User;
  userModification?: User;
  dateCreation: string;
  dateModification: string;
  ideeOuBonnePratiqueComments: Comment[];
  ideeOuBonnePratiqueFichierJoints: IdeeOuBonnePratiqueFichierJoint[];
  ideeOuBonnePratiqueLus: IdeeOuBonnePratiqueLu[];
  ideeOuBonnePratiqueSmyleys: UserSmyley[];
  nbSmyley: number;
  nbComment: number;
  nbPartage: number;
  nbLu: number;
  commentaire?: string;
  lu: boolean;
}

export interface IdeeOuBonnePratiqueInput {
  id?: string;
  origine: IdeeOuBonnePratiqueOrigine;
  status: IdeeOuBonnePratiqueStatus;
  idClassification?: string;
  title: string;
  idAuteur: string;
  concurent?: string;
  idFournisseur?: string;
  idPrestataire?: string;
  idGroupeAmis?: string;
  idService?: string;
  beneficiaires_cles?: string;
  contexte?: string;
  objectifs?: string;
  resultats?: string;
  facteursClesDeSucces?: string;
  contraintes?: string;
  codeMaj?: string;
  isRemoved?: boolean;
  fichiers?: FichierInput[];
}

export interface IdeeOuBonnePratiqueLuInput {
  id?: string;
  idIdeeOuBonnePratique: string;
  idUser: string;
  isRemoved?: boolean;
}

export interface IdeeOuBonnePratiqueLu {
  id: string;
  ideeOuBonnePratique: IdeeOuBonnePratique;
  user: User;
  isRemoved?: boolean;
  dateCreation: string;
  dateModification: string;
}

export interface IdeeOuBonnePratiqueFichierJointInput {
  id?: string;
  idIdeeOuBonnePratique: string;
  idFichier: string;
  isRemoved?: boolean;
}

export interface IdeeOuBonnePratiqueFichierJoint {
  id: string;
  ideeOuBonnePratique: IdeeOuBonnePratique;
  fichier: Fichier;
  isRemoved?: boolean;
  dateCreation: string;
  dateModification: string;
}

export interface IdeeOuBonnePratiqueClassificationInput {
  id?: string;
  nom: string;
  isRemoved?: boolean;
  idParent?: string;
}

export interface IdeeOuBonnePratiqueClassification {
  id: string;
  nom: string;
  userCreation?: User;
  userModification?: User;
  isRemoved?: boolean;
  dateCreation: string;
  dateModification: string;
  parent?: IdeeOuBonnePratiqueClassification;
  childs?: IdeeOuBonnePratiqueClassification[];
}

export interface IdeeOuBonnePratiqueChangeStatus {
  id: string;
  status: IdeeOuBonnePratiqueStatus;
  ideeOuBonnePratique: IdeeOuBonnePratique;
  isRemoved?: boolean;
  userCreation?: User;
  userModification?: User;
  dateCreation: string;
  dateModification: string;
}

export interface IdeeOuBonnePratiqueSmyley {
  id: string;
  isRemoved: boolean | null;
  dateCreation: string;
  dateModification: string;
}

export interface RgpdInput {
  id?: string;
  politiqueConfidentialite?: string;
  conditionUtilisation?: string;
  informationsCookies?: string;
}

export interface Rgpd {
  id: string;
  politiqueConfidentialite?: string;
  conditionUtilisation?: string;
  informationsCookies?: string;
  groupement?: Groupement;
  userCreation?: User;
  userModification?: User;
  dateCreation: string;
  dateModification: string;
}

export interface RgpdAccueilInput {
  id?: string;
  title: string;
  description: string;
}

export interface RgpdAccueil {
  id: string;
  title: string;
  description: string;
  groupement?: Groupement;
  userCreation?: User;
  userModification?: User;
  dateCreation: string;
  dateModification: string;
}

export interface RgpdAccueilPlusInput {
  id?: string;
  title: string;
  description: string;
  order?: number;
}

export interface RgpdAccueilPlus {
  id: string;
  title: string;
  description: string;
  order?: number;
  groupement?: Groupement;
  userCreation?: User;
  userModification?: User;
  dateCreation: string;
  dateModification: string;
}

export interface RgpdPartenaireInput {
  id?: string;
  title: string;
  description: string;
  order?: number;
}

export interface RgpdPartenaire {
  id: string;
  title: string;
  description: string;
  order?: number;
  groupement?: Groupement;
  userCreation?: User;
  userModification?: User;
  dateCreation: string;
  dateModification: string;
}

export interface RgpdAutorisationInput {
  id?: string;
  title: string;
  description: string;
  order?: number;
}

export interface RgpdAutorisation {
  id: string;
  title: string;
  description: string;
  order?: number;
  groupement?: Groupement;
  userCreation?: User;
  userModification?: User;
  dateCreation: string;
  dateModification: string;
}

export interface RgpdHistoriqueInput {
  id?: string;
  accept_all?: boolean;
  refuse_all?: boolean;
  idUser: string;
  historiquesInfosPlus?: RgpdHistoriqueInfoPlusInput[];
}

export interface RgpdHistorique {
  id: string;
  accept_all?: boolean;
  refuse_all?: boolean;
  user: string;
  groupement?: Groupement;
  dateCreation: string;
  dateModification: string;
  historiquesInfosPluses: RgpdHistoriqueInfoPlus[];
}

export interface RgpdHistoriqueInfoPlusInput {
  id?: string;
  type: RgpdHistoriqueInfoPlusType;
  idHistorique?: string;
  idItemAssocie: string;
  accepted: boolean;
}

export interface RgpdHistoriqueInfoPlus {
  id: string;
  type: RgpdHistoriqueInfoPlusType;
  accepted: boolean;
  historique: RgpdHistorique;
  idItemAssocie: string;
  groupement?: Groupement;
  dateCreation: string;
  dateModification: string;
}

export enum RgpdHistoriqueInfoPlusType {
  ACCUEIL = 'ACCUEIL',
  AUTORISATION = 'AUTORISATION',
  PARTENAIRE = 'PARTENAIRE',
}

export interface InformationLiaisonFichierJoint {
  id: string;
  informationLiaison: InformationLiaison;
  fichier: Fichier;
  isRemoved?: boolean;
  userCreation?: User;
  userModification?: User;
  dateCreation: string;
  dateModification: string;
}

export interface InformationLiaisonFichierJointInput {
  id?: string;
  idInformationLiaison: string;
  idFichier: string;
  isRemoved?: boolean;
}

export interface ParameterOption {
  value: string | null;
  label: string | null;
}
export interface LaboratoireRepresentantInput {
  id: string | null;
  idLaboratoire: string;
  civilite: string | null;
  nom: string;
  prenom: string | null;
  sexe: string | null;
  fonction: string | null;
  afficherComme: string | null;
  photo?: FichierInput;
  contact: ContactInput;
}

export interface LaboratoireRepresentant extends Searchable {
  type: 'laboratoirerepresentant';
  id: string;
  civilite: string;
  nom: string;
  prenom: string;
  sexe: string;
  fonction: string;
  commentaire: string;
  afficherComme: string;
  codeMaj: string;
  isRemoved: boolean;
  contact: Contact;
  laboratoire: Laboratoire;
  photo: Fichier;
  userCreation: User;
  userModification: User;
  dateCreation: string;
  dateModification: string;
}

export interface LaboratoireRessource extends Searchable {
  type: 'laboratoireressource';
  id: string;
  typeRessource: string;
  item: Item;
  code: string;
  libelle: string;
  codeMaj: string;
  fichier: Fichier;
  url: string;
  laboratoire: Laboratoire;
  groupement: Groupement;
  dateChargement: string;
  isRemoved: boolean;
  dateCreation: string;
  dateModification: string;
}

export interface LaboratoireRessourceInput {
  id: string;
  idLaboratoire: string;
  typeRessource: string;
  idItem: string;
  code: string;
  libelle: string;
  dateChargement: string;
  fichier: FichierInput;
  url: string;
}

export interface LaboratoireSuiteInput {
  adresse: string | null;
  codePostal: string | null;
  telephone: string | null;
  ville: string | null;
  telecopie: string | null;
  email: string | null;
  webSiteUrl: string | null;
}

export interface Importance {
  id: string;
  ordre: number | null;
  libelle: string | null;
  color: string | null;
  isRemoved: boolean | null;
  dateCreation: string;
  dateModification: string;
}

export interface UpdateActionDateDebutFinInput {
  idAction: string;
  dateDebut: string;
  dateFin: string;
}

export interface InformationLiaisonFilter {
  idPharmacie: string;
  idsCollegueConcernee: string[];
  idsDeclarant: string[];
  dateDebut: string | null;
  dateFin: string | null;
}

export interface StatutCount {
  count: number;
}
export interface UrgenceCount {
  id: string;
  count: number;
}
export interface ImportanceCount {
  id: string;
  count: number;
}

export interface InformationLiaisonCount {
  statuts: StatutCount[];
  urgences: UrgenceCount[];
  importances: ImportanceCount[];
}
