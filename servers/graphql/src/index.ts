import { GraphQLServer } from './server';

export const server = new GraphQLServer();

server.start();
process.on('SIGINT', () => {
  server.stop();
});
