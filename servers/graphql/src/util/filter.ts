export const todayFilter = ({ idUser, useMatriceResponsabilite }) => {
  return useMatriceResponsabilite
    ? [
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'assignedUsers',
                },
              },
              {
                term: { 'project.typeProject': 'PERSONNEL' },
              },
            ],
            must: [
              {
                term: { 'project.participants.id': idUser },
              } /*,
            {
              term: {
                isInInboxTeam: false,
              },
            },*/,
            ],
          },
        },
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'assignedUsers',
                },
              },
              {
                exists: {
                  field: 'project.participants',
                },
              },
            ],
            must: [
              {
                term: { 'userCreation.id': idUser },
              },
              /*
            {
              term: { 'project.typeProject': 'PERSONNEL' },
            },*/
              /*{
              term: {
                isInInboxTeam: false,
              },
            },*/
            ],
          },
        },
        { term: { 'assignedUsers.id': idUser } },
      ]
    : [
        {
          bool: {
            must_not: [
              {
                exists: {
                  field: 'assignedUsers',
                },
              },
            ],
            must: [
              {
                term: { 'userCreation.id': idUser },
              },
              {
                term: {
                  isInInboxTeam: false,
                },
              },
            ],
          },
        },
        { term: { 'assignedUsers.id': idUser } },
      ];
};

export const todayTeamFilter = ({
  idUser,
  useMatriceResponsabilite,
  defaultShouldCreatedTeam,
  defaultShouldAssignTeam,
}) => {
  return useMatriceResponsabilite
    ? [
        {
          bool: {
            must_not: [
              {
                term: { 'project.typeProject': 'PERSONNEL' },
              },
              /*{
              term: { 'project.participants.id': idUser },
            },*/
              //{ term: { 'assignedUsers.id': idUser } },
            ],
            must: [...defaultShouldCreatedTeam],
            should: [
              {
                bool: {
                  must_not: todayFilter({ idUser, useMatriceResponsabilite }),
                  must: [
                    {
                      term: {
                        'userCreation.id': idUser,
                      },
                    },
                    {
                      bool: {
                        should: privateShould({ idUser }),
                      },
                    },
                  ],
                },
              },
              {
                bool: {
                  must: [
                    {
                      exists: {
                        field: 'assignedUsers',
                      },
                    },
                    {
                      bool: {
                        should: privateShould({ idUser }),
                      },
                    },
                  ],
                  must_not: [{ term: { 'assignedUsers.id': idUser } }],
                },
              },
              {
                bool: {
                  must_not: [
                    ,
                    {
                      exists: {
                        field: 'assignedUsers',
                      },
                    },
                    { term: { 'project.participants.id': idUser } },
                  ],
                  must: [
                    {
                      exists: {
                        field: 'project',
                      },
                    },
                    {
                      bool: {
                        should: privateShould({ idUser }),
                      },
                    },
                    // { term: { 'assignedUsers.id': myId } }
                  ],
                },
              },
            ],
            minimum_should_match: 1,
          },
        },
      ]
    : [
        {
          bool: {
            must_not: [
              {
                term: {
                  'assignedUsers.id': idUser,
                },
              },
            ],
            must: [
              {
                exists: {
                  field: 'project.participants.id',
                },
              },
              ...defaultShouldCreatedTeam,
            ],
          },
        },
        {
          bool: {
            must_not: [
              {
                term: {
                  'assignedUsers.id': idUser,
                },
              },
            ],
            must: [
              {
                term: {
                  'userCreation.id': idUser,
                },
              },
              ...defaultShouldAssignTeam,
            ],
          },
        },
        {
          bool: {
            must: [
              {
                term: {
                  isInInboxTeam: true,
                },
              },
              {
                term: {
                  'userCreation.id': idUser,
                },
              },
            ],
          },
        },
      ];
};

export const privateShould = ({ idUser }) => {
  return [
    {
      bool: {
        must: [
          {
            term: { isPrivate: false },
          },
        ],
      },
    },
    {
      bool: {
        should: [
          {
            bool: {
              must: [{ term: { isPrivate: true } }, { term: { 'assignedUsers.id': idUser } }],
            },
          },
          {
            bool: {
              must: [
                { term: { isPrivate: true } },
                {
                  term: {
                    'userCreation.id': idUser,
                  },
                },
              ],
            },
          },
          {
            bool: {
              must: [
                { term: { isPrivate: true } },
                { term: { 'project.participants.id': idUser } },
              ],
              must_not: [
                {
                  exists: {
                    field: 'assignedUsers',
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ];
};
