import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { Fichier } from './Fichier';
import { GedSousCategorie } from './GedSousCategorie';

@Table({ tableName: 'GED_document' })
export class GedDocument extends Model<GedDocument, Partial<GedDocument>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'description', type: DataType.TEXT })
  description: string;

  @Column({ field: 'nomenclature', type: DataType.STRING(255) })
  nomenclature: string;

  @Column({ field: 'numero_version', type: DataType.STRING(255) })
  numeroVersion: string;

  @Column({ field: 'mot_cle1', type: DataType.STRING(255) })
  motCle1: string;

  @Column({ field: 'mot_cle2', type: DataType.STRING(255) })
  motCle2: string;

  @Column({ field: 'mot_cle3', type: DataType.STRING(255) })
  motCle3: string;

  @Column({ field: 'date_heure_parution' })
  dateHeureParution: Date;

  @Column({ field: 'date_heure_debut_validite' })
  dateHeureDebutValidite: Date;

  @Column({ field: 'date_heure_fin_validite' })
  dateHeureFinValidite: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_redacteur', type: DataType.STRING(25) })
  idUserRedacteur: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_verificateur', type: DataType.STRING(25) })
  idUserVerificateur: string;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_fichier', type: DataType.STRING(25) })
  idFichier: string;

  @ForeignKey(() => GedSousCategorie)
  @Column({ field: 'id_sous_categorie', type: DataType.STRING(25) })
  idSousCategorie: string;

  @BelongsTo(() => GedSousCategorie, { onDelete: 'cascade' })
  sousCategorie: GedSousCategorie;

  @Column({ field: 'id_document_a_remplacer', type: DataType.STRING(25) })
  idDocumentARemplacer: string;

  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({field : 'id_origine', type: DataType.STRING(25)})
  idOrigine: string;

  @Column({field: 'id_origine_associe', type: DataType.STRING(25)})
  idOrigineAssocie: string;

  @Column({field: 'facture_total_ht', type: DataType.FLOAT})
  factureTotalHt: number;

  @Column({ field: 'type', type: DataType.STRING(100)})
  type: string; //FACTURE

  @Column({field: 'facture_tva', type: DataType.FLOAT})
  factureTva: number;

  @Column({ field: 'facture_date' })
  factureDate: Date;

  @Column({field: 'facture_total_ttc', type: DataType.FLOAT})
  factureTotalTtc: number;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
