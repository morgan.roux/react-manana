import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { ActiviteType } from './ActiviteType';
import { User } from './User';
import { Item } from './Item';
@Table({ tableName: 'Activite_User' })
export class Activite extends Model<Activite, Partial<Activite>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => ActiviteType)
  @Column({ field: 'id_activite_type', type: DataType.STRING(25) })
  idActiviteType: string;

  @ForeignKey(() => Item)
  @Column({ field: 'id_item', type: DataType.STRING(25) })
  idItem: string;

  @Column({ field: 'id_item_associe', type: DataType.STRING(25) })
  idItemAssocie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @Column({ field: 'log', type: DataType.TEXT })
  log: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
