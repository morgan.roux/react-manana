import {
  Column,
  Table,
  Model,
  DataType,
  ForeignKey,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';
import { COSourceAppro } from './COSourceAppro';

@Table({ tableName: 'CO_source_approvisionnement_attribut' })
export class COSourceApproAttribut extends Model<COSourceApproAttribut, Partial<COSourceApproAttribut>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'identifiant', type: DataType.STRING(255) })
  identifiant: string;

  @Column({ field: 'valeur', type: DataType.STRING(255) })
  valeur: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => COSourceAppro)
  @Column({ field: 'idSourceAppro', type: DataType.STRING(25) })
  idSourceAppro: string;
}
