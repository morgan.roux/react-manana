import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsTo
} from 'sequelize-typescript';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';
import { RTCapteur } from './RTCapteur'
import { RTFrigo } from './RTFrigo'

@Table({ tableName: 'RT_frigo_capteur' })
export class RTFrigoCapteur extends Model<RTFrigoCapteur, Partial<RTFrigoCapteur>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => RTCapteur)
  @Column({ field: 'id_capteur', type: DataType.STRING(25) })
  idCapteur: string;

  @ForeignKey(() => RTFrigo)
  @Column({ field: 'id_frigo', type: DataType.STRING(25) })
  idFrigo: string;

  @BelongsTo(() => RTCapteur, { onDelete: 'CASCADE' })
  capteur: RTCapteur

  @BelongsTo(() => RTFrigo, { onDelete: 'CASCADE' })
  frigo: RTFrigo

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
