import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'User_photo' })
export class UserPhoto extends Model<UserPhoto, Partial<UserPhoto>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'id_fichier', type: DataType.STRING(25) })
  idFichier: string;

  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
