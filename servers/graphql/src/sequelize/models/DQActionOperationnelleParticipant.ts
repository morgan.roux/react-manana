import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { DQActionOperationnelle } from './DQActionOperationnelle';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'DQ_action_operationnelle_participant' })
export class DQActionOperationnelleParticipant extends Model<DQActionOperationnelleParticipant, Partial<DQActionOperationnelleParticipant>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @ForeignKey(() => DQActionOperationnelle)
  @Column({ field: 'id_action_operationnelle', type: DataType.STRING(25) })
  idActionOperationnelle: string;

  @Column({ field: 'type', type: DataType.STRING(100) })
  type: string; // PARTICIPANT_EN_CHARGE | PARTICIPANT_A_INFORMER

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
