import { Column, DataType, Model, Table, ForeignKey } from 'sequelize-typescript';
import { SuiviAppel, User } from '.';

@Table({ tableName: 'suivi_appel_participant' })
export class SuiviAppelParticipant extends Model<SuiviAppelParticipant, Partial<SuiviAppelParticipant>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => SuiviAppel)
  @Column({ field: 'id_suivi_appel', type: DataType.STRING(25) })
  idSuiviAppel: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;
}
