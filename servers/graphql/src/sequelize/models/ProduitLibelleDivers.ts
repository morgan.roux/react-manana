import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Libelle_divers' })
export class ProduitLibelleDivers extends Model<ProduitLibelleDivers, Partial<ProduitLibelleDivers>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_code_info', type: DataType.SMALLINT })
  resipCodeInfo: number;

  @Column({ field: 'code_info', type: DataType.SMALLINT })
  codeInfo: number;

  @Column({ field: 'code', type: DataType.INTEGER })
  code: number;

  @Column(DataType.STRING(255))
  libelle: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
