import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement } from './Groupement';

@Table({ tableName: 'User' })
export class User extends Model<User, Partial<User>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ type: DataType.STRING(255) })
  email: string;

  @Column({ field: 'email_confirmed', type: DataType.BOOLEAN })
  emailConfirmed: boolean;

  @Column({ field: 'password_hash', type: DataType.STRING(255) })
  passwordHash: string;

  @Column({ field: 'password_secret_answer', type: DataType.STRING(255) })
  passwordSecretAnswer: string;

  @Column({ field: 'phone_number', type: DataType.STRING(20) })
  phoneNumber: string;

  @Column({ field: 'phone_number_confirmed', type: DataType.BOOLEAN })
  phoneNumberConfirmed: boolean;

  @Column({ field: 'two_factor_enabled', type: DataType.BOOLEAN })
  twoFactorEnabled: boolean;

  @Column({ field: 'lockout_end_date_utc', type: DataType.DATE })
  lockoutEndDateUtc: Date;

  @Column({ field: 'lockout_enabled', type: DataType.BOOLEAN })
  lockoutEnabled: boolean;

  @Column({ field: 'access_failed_count', type: DataType.INTEGER })
  accessFailedCount: number;

  @Column({ field: 'user_name', type: DataType.STRING(120) })
  userName: string;

  @Column({ field: 'last_login_date', type: DataType.DATE })
  lastLoginDate: Date;

  @Column({ field: 'last_password_changed_date', type: DataType.DATE })
  lastPasswordChangedDate: Date;

  @Column({ field: 'is_locked_out', type: DataType.BOOLEAN })
  isLockedOut: boolean;

  @Column({ field: 'is_locked_out_permanent', type: DataType.BOOLEAN })
  isLockedOutPermanent: boolean;

  @Column({ field: 'is_obligation_change_password', type: DataType.BOOLEAN })
  isObligationChangePassword: boolean;

  @Column({ field: 'access_failed_count_before_lockout_permanent', type: DataType.INTEGER })
  accessFailedCountBeforeLockoutPermanent: number;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ type: DataType.STRING(100) })
  status: string;

  @Column({ type: DataType.STRING(100) })
  theme: string;

  @Column({ field: 'jour_naissance', type: DataType.INTEGER })
  jourNaissance: number;

  @Column({ field: 'mois_naissance', type: DataType.INTEGER })
  moisNaissance: number;

  @Column({ field: 'annee_naissance', type: DataType.INTEGER })
  anneeNaissance: number;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
