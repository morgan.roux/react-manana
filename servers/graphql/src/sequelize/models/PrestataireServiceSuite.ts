import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Prestataire_service_suite' })
export class PrestataireServiceSuite extends Model<PrestataireServiceSuite, Partial<PrestataireServiceSuite>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'nom' })
  nom: string;

  @Column
  adresse1: string;

  @Column
  adresse2: string;

  @Column({ field: 'code_postal' })
  codePostal: string;

  @Column
  telephone: string;

  @Column
  ville: string;

  @Column
  telecopie: string;

  @Column
  email: string;

  @Column({ field: 'web_site_url' })
  webSiteUrl: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
