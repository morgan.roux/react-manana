import { Model, Table, Column, CreatedAt, UpdatedAt, DataType, ForeignKey } from 'sequelize-typescript';
import { Avatar } from './Avatar'

@Table({ tableName: 'Fichier' })
export class Fichier extends Model<Fichier, Partial<Fichier>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'chemin', type: DataType.STRING(255) })
  chemin: string;

  @Column({ field: 'nom_original', type: DataType.STRING(255) })
  nomOriginal: string;

  @Column({ field: 'type', type: DataType.STRING(255) })
  type: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Avatar)
  @Column({ field: 'id_avatar', type: DataType.STRING(25) })
  idAvatar: string;

}
