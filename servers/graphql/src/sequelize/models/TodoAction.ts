import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Item } from '.';
import { TodoProjet } from './TodoProjet';
import { TodoActionType } from './TodoActionType';

@Table({ tableName: 'Todo_action' })
export class TodoAction extends Model<TodoAction, Partial<TodoAction>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'ordre', type: DataType.INTEGER })
  ordre: number;

  @Column({ field: 'description', type: DataType.TEXT })
  description: string;

  @Column({ field: 'date_debut', type: DataType.DATE })
  dateDebut: Date;

  @Column({ field: 'date_fin', type: DataType.DATE })
  dateFin: Date;

  @Column({ field: 'boite_reception', type: DataType.BOOLEAN })
  isInInbox: boolean;

  @Column({ field: 'boite_reception_equipe', type: DataType.BOOLEAN })
  isInInboxTeam: boolean;

  @ForeignKey(() => TodoActionType)
  @Column({ field: 'id_action_type', type: DataType.STRING(25) })
  idActionType: string;

  @ForeignKey(() => TodoAction)
  @Column({ field: 'id_action_parent', type: DataType.STRING(25) })
  idActionParent: string;

  @ForeignKey(() => Item)
  @Column({ field: 'id_item', type: DataType.STRING(25) })
  idItem: string;

  @Column({ field: 'id_item_associe', type: DataType.STRING(25) })
  idItemAssocie: string;

  @ForeignKey(() => TodoProjet)
  @Column({ field: 'id_projet', type: DataType.STRING(25) })
  idProjet: string;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  supprime: boolean;

  @Column({ field: 'id_importance', type: DataType.STRING(25) })
  idImportance: string;

  // DONE
  // EN_COURS
  // CLOSED
  @Column({ field: 'status', type: DataType.STRING })
  status: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
