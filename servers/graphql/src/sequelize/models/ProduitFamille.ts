import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  DataType,
} from 'sequelize-typescript';
import { Produit } from './Produit';
import { Famille } from './Famille';

@Table({ tableName: 'Produit_famille' })
export class ProduitFamille extends Model<ProduitFamille, Partial<ProduitFamille>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_id_produit', type: DataType.INTEGER })
  resipIdProduit: number;

  @Column({ field: 'resip_code_famille', type: DataType.STRING(5) })
  resipCodeFamille: string;

  @ForeignKey(() => Produit)
  @Column({ field: 'id_produit', type: DataType.STRING(25) })
  idProduit: string;

  @ForeignKey(() => Famille)
  @Column({ field: 'id_famille', type: DataType.STRING(25) })
  idFamille: string;

  @Column({ field: 'code_famille', type: DataType.STRING(5) })
  codeFamille: string;

  @Column({ type: DataType.INTEGER })
  ordre: number;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
