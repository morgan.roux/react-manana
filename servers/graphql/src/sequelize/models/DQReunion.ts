import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsToMany,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { DQReunionParticipant } from './DQReunionParticipant';
import { DQReunionAction } from './DQReunionAction';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'DQ_reunion' })
export class DQReunion extends Model<DQReunion, Partial<DQReunion>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'description', type: DataType.TEXT })
  description: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_animateur', type: DataType.STRING(25) })
  idUserAnimateur: string;

  @BelongsTo(() => User)
  animateur: User;

  @ForeignKey(() => User)
  @Column({ field: 'id_responsable', type: DataType.STRING(25) })
  idResponsable: string;

  @BelongsTo(() => User)
  responsable: User;

  @BelongsToMany(
    () => User,
    () => DQReunionParticipant,
  )
  participants: User[];

  @HasMany(() => DQReunionAction)
  actions: DQReunionAction[];

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
