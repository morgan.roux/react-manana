import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  DataType,
} from 'sequelize-typescript';
import { Produit } from './Produit';
import { GroupeGen } from './GroupeGen';

@Table({ tableName: 'Produit_groupe_gen' })
export class ProduitGroupeGen extends Model<ProduitGroupeGen, Partial<ProduitGroupeGen>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_id_produit', type: DataType.INTEGER })
  resipIdProduit: number;

  @Column({ field: 'resip_code_groupe', type: DataType.INTEGER })
  resipCodeGroupe: number;

  @ForeignKey(() => Produit)
  @Column({ field: 'id_produit', type: DataType.STRING(25) })
  idProduit: string;

  @ForeignKey(() => GroupeGen)
  @Column({ field: 'id_groupe_gen', type: DataType.STRING(25) })
  idGroupeGen: string;

  // BCB : id_type
  @Column({ field: 'type', type: DataType.INTEGER })
  type: number;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
