import {
  Column,
  DataType,
  ForeignKey,
  Model,
  Table,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';
import { COGroupe } from './COGroupeSourceAppro';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'CO_source_appro' })
export class COSourceAppro extends Model<COSourceAppro, Partial<COSourceAppro>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'nom', type: DataType.STRING(25) })
  nom: string;

  @Column({ field: 'ordre', type: DataType.INTEGER })
  ordre: number;

  @Column({ field: 'telephone', type: DataType.STRING(25) })
  tel: string;

  @Column({ field: 'commentaire', type: DataType.TEXT })
  commentaire?: string;

  @ForeignKey(() => COGroupe)
  @Column({ field: 'id_groupe_source_appro', type: DataType.STRING(25) })
  idGroupeSourceAppro: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;
}
