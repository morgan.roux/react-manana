import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Tva' })
export class Tva extends Model<Tva, Partial<Tva>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_code_tva', type: DataType.INTEGER })
  resipCodeTva: number;

  @Column({ field: 'code_tva', type: DataType.INTEGER })
  codeTva: number;

  @Column({ field: 'taux_tva', type: DataType.DECIMAL(4, 2) })
  tauxTva: number;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
