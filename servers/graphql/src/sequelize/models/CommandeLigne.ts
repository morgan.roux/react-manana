import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Commande_ligne' })
export class CommandeLigne extends Model<CommandeLigne, Partial<CommandeLigne>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'optimiser', type: DataType.INTEGER })
  optimiser: number;

  @Column({ field: 'quantite_commandee', type: DataType.INTEGER })
  quantiteCommandee: number;

  @Column({ field: 'quantite_Livree', type: DataType.INTEGER })
  quantiteLivree: number;

  @Column({ field: 'unite_gratuite', type: DataType.INTEGER })
  uniteGratuite: number;

  @Column({ field: 'prix_base_HT', type: DataType.DECIMAL })
  prixBaseHT: number;

  @Column({ field: 'remise_ligne', type: DataType.DECIMAL })
  remiseLigne: number;

  @Column({ field: 'remise_gamme', type: DataType.DECIMAL })
  remiseGamme: number;

  @Column({ field: 'prix_net_unitaire_HT', type: DataType.DECIMAL })
  prixNetUnitaireHT: number;

  @Column({ field: 'prix_total_HT', type: DataType.DECIMAL })
  prixTotalHT: number;

  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'id_statut_Ligne', type: DataType.STRING(25) })
  idStatutLigne: string;

  @Column({ field: 'id_commande', type: DataType.STRING(25) })
  idCommande: string;

  @Column({ field: 'id_produit_canal', type: DataType.STRING(25) })
  idProduitCanal: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
