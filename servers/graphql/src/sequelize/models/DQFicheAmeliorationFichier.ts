import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { Fichier } from './Fichier';
import { DQFicheAmelioration } from './DQFicheAmelioration';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'DQ_fiche_amelioration_fichier' })
export class DQFicheAmeliorationFichier extends Model<DQFicheAmeliorationFichier, Partial<DQFicheAmeliorationFichier>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => DQFicheAmelioration)  
  @Column({ field: 'id_fiche_amelioration', type: DataType.STRING(25) })
  idFicheAmelioration: string;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_fichier', type: DataType.STRING(25) })
  idFichier: string;


  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
