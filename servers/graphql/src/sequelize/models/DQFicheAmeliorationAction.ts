import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsToMany,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { Fichier } from './Fichier';
import { DQFicheAmeliorationActionFichier } from './DQFicheAmeliorationActionFichier';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'DQ_fiche_amelioration_action' })
export class DQFicheAmeliorationAction extends Model<DQFicheAmeliorationAction, Partial<DQFicheAmeliorationAction>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'date_heure_mise_en_place' })
  dateHeureMiseEnplace: Date;

  @Column({ field: 'description', type: DataType.TEXT })
  description: string;

  @BelongsToMany(
    () => Fichier,
    () => DQFicheAmeliorationActionFichier,
  )
  fichiers: Fichier[];


  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
