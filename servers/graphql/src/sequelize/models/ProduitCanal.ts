import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Produit_canal' })
export class ProduitCanal extends Model<ProduitCanal, Partial<ProduitCanal>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'qte_stock', type: DataType.INTEGER })
  qteStock: number;

  @Column({ field: 'qte_min', type: DataType.INTEGER })
  qteMin: number;

  @Column({ field: 'stv', type: DataType.INTEGER })
  stv: number;

  @Column({ field: 'unite_petit_cond', type: DataType.INTEGER })
  unite_petit_cond: number;

  @Column({ field: 'unite_sup_cond', type: DataType.INTEGER })
  unite_sup_cond: number;

  @Column({ field: 'prix_fab', type: DataType.DECIMAL })
  prixFAB: number;

  @Column({ field: 'prix_phv', type: DataType.DECIMAL })
  prixPHV: number;

  @Column({ field: 'prix_ttc', type: DataType.DECIMAL })
  prixTTC: number;

  @Column({ field: 'date_retrait', type: DataType.DATE })
  dateRetrait: Date;

  @Column({ field: 'date_peremption', type: DataType.DATE })
  datePeremption: Date;

  @Column({ field: 'flag', type: DataType.STRING })
  flag: string;

  @Column({ field: 'code_blocage', type: DataType.BOOLEAN })
  codeBlocage: boolean;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  isRemoved: boolean;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'id_produit', type: DataType.STRING(25) })
  idProduit: string;

  @Column({ field: 'id_canal_sous_gamme', type: DataType.STRING(25) })
  idCanalSousGamme: string;

  @Column({ field: 'id_sous_gamme_catalogue', type: DataType.STRING(25) })
  idSousGammeCatalogue: string;

  @Column({ field: 'id_canal', type: DataType.STRING(25) })
  idCanal: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
