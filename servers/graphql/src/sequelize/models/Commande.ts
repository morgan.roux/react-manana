import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Commande' })
export class Commande extends Model<Commande, Partial<Commande>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'id_laboratoire', type: DataType.STRING(25) })
  idLaboratoire: string;

  @Column({ field: 'reference', type: DataType.STRING(255) })
  reference: string;

  @Column({ field: 'titre', type: DataType.STRING })
  titre: string;

  @Column({ field: 'date_commande', type: DataType.DATE })
  dateCommande: Date;

  @Column({ field: 'nbr_Ref', type: DataType.INTEGER })
  nbrRef: number;

  @Column({ field: 'quantite', type: DataType.INTEGER })
  quantite: number;

  @Column({ field: 'unite_gratuite', type: DataType.INTEGER })
  uniteGratuite: number;

  @Column({ field: 'prix_base_total_HT', type: DataType.DECIMAL })
  prixBaseTotalHT: number;

  @Column({ field: 'valeur_remise_total', type: DataType.DECIMAL })
  valeurRemiseTotal: number;

  @Column({ field: 'prix_net_total_HT', type: DataType.DECIMAL })
  prixNetTotalHT: number;

  @Column({ field: 'frais_port', type: DataType.DECIMAL })
  fraisPort: number;

  @Column({ field: 'valeur_franco_port', type: DataType.DECIMAL })
  ValeurFrancoPort: number;

  @Column({ field: 'remise_globale', type: DataType.DECIMAL })
  remiseGlobale: number;

  @Column({ field: 'commentaire_interne', type: DataType.STRING })
  commentaireInterne: string;

  @Column({ field: 'commentaire_externe', type: DataType.STRING })
  commentaireExterne: string;

  @Column({ field: 'path_file', type: DataType.STRING })
  pathFile: string;

  @Column({ field: 'id_operation', type: DataType.STRING(25) })
  idOperation: string;

  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @Column({ field: 'id_commande_type', type: DataType.STRING(25) })
  idCommandeType: string;

  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'id_publicite', type: DataType.STRING(25) })
  idPublicite: string;

  @Column({ field: 'id_statut', type: DataType.STRING(25) })
  idStatut: string;

  @Column({ field: 'id_canal', type: DataType.STRING(25) })
  idCanal: string;

  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
