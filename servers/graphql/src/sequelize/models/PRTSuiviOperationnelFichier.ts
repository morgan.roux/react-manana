import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { Fichier } from './Fichier';
import { PRTSuiviOperationnel } from './PRTSuiviOperationnel';

@Table({ tableName: 'PRT_suivi_operationnel_fichier' })
export class PRTSuiviOperationnelFichier extends Model<PRTSuiviOperationnelFichier, Partial<PRTSuiviOperationnelFichier>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;


  @ForeignKey(() => PRTSuiviOperationnel)
  @Column({ field: 'id_suivi_operationnel', type: DataType.STRING(25) })
  idSuiviOperationnel: string;

  @BelongsTo(() => PRTSuiviOperationnel, { onDelete: 'CASCADE' })
  suiviOperationnel: PRTSuiviOperationnel;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_fichier', type: DataType.STRING(25) })
  idFichier: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
