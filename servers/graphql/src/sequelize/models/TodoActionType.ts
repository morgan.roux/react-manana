import {
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { User } from './User';

@Table({ tableName: 'Todo_action_type' })
export class TodoActionType extends Model<TodoActionType, Partial<TodoActionType>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'code', type: DataType.STRING })
  code: string;

  @Column({ field: 'libelle', type: DataType.TEXT })
  libelle: string;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  supprime: boolean;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
