import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Module } from './Module';

@Table({ tableName: 'Traitement' })
export class Traitement extends Model<Traitement, Partial<Traitement>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'code', type: DataType.STRING(100) })
  code: string;

  @Column({ field: 'nom', type: DataType.STRING(255) })
  nom: string;

  @ForeignKey(() => Module)
  @Column({ field: 'id_module', type: DataType.STRING(25) })
  idModule: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
