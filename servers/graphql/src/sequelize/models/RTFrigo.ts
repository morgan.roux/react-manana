import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'RT_frigo' })
export class RTFrigo extends Model<RTFrigo, Partial<RTFrigo>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'nom', type: DataType.STRING(50) })
  nom: string;

  @Column({ field: 'temperature_basse', type: DataType.FLOAT })
  temperatureBasse: number;

  @Column({ field: 'temperature_haute', type: DataType.FLOAT })
  temperatureHaute: number;

  @Column({ field: 'temporisation', type: DataType.INTEGER })
  temporisation: number; // En minute

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'type_releve', type: DataType.STRING(25) })
  typeReleve: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
