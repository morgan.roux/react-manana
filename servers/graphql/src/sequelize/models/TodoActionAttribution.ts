import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { TodoAction } from './TodoAction';

@Table({ tableName: 'Todo_action_attribution' })
export class TodoActionAttribution extends Model<TodoActionAttribution, Partial<TodoActionAttribution>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  supprime: boolean;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_destination', type: DataType.STRING(25) })
  idUserDestination: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_source', type: DataType.STRING(25) })
  idUserSource: string;

  @ForeignKey(() => TodoAction)
  @Column({ field: 'id_action', type: DataType.STRING(25) })
  idAction: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
