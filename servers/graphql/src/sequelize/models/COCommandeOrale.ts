import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement } from '.';
import { COPassation } from './COPassation';
import { COReception } from './COReception';
import { Pharmacie } from './Pharmacie';
import { User } from './User';

@Table({ tableName: 'CO_commande_orale' })
export class COCommandeOrale extends Model<COCommandeOrale, Partial<COCommandeOrale>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @Column({ field: 'designation', type: DataType.STRING(25) })
  designation: string;

  @Column({ field: 'quantite', type: DataType.INTEGER })
  quantite: number;

  @Column({ field: 'forme', type: DataType.STRING(25) })
  forme: string;

  @Column({ field: 'date_heure', type: DataType.DATE })
  dateHeure: Date;

  @Column({ field: 'commentaire', type: DataType.TEXT })
  commentaire: string;

  @Column({ field: 'cloturee', type: DataType.BOOLEAN })
  cloturee: boolean;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => COPassation)
  @Column({ field: 'id_passation', type: DataType.STRING(25) })
  idPassation: string;

  @ForeignKey(() => COReception)
  @Column({ field: 'id_reception', type: DataType.STRING(25) })
  idReception: string;
}

//    id?: string;
//   idUser: string;
//   dateHeure: Date | null;
//   quantite: number;
//   forme: string;
//   designation: string;
//   commentaire?: string;`
