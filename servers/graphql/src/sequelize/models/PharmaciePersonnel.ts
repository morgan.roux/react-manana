import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'Ppersonnel' })
export class PharmaciePersonnel extends Model<PharmaciePersonnel, Partial<PharmaciePersonnel>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'sortie', type: DataType.SMALLINT })
  sortie: number;

  @Column({ field: 'civilite', type: DataType.STRING(20) })
  civilite: string;

  @Column({ field: 'nom', type: DataType.STRING(255) })
  nom: string;

  @Column
  prenom: string;

  @Column({ field: 'est_ambassadrice', type: DataType.BOOLEAN })
  estAmbassadrice: boolean;

  @Column({ type: DataType.TEXT })
  commentaire: string;

  @Column({ field: 'date_sortie', type: DataType.DATE })
  dateSortie: Date;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  // TODO : Relation with Contact
  @Column({ field: 'id_contact', type: DataType.STRING(25) })
  idContact: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
