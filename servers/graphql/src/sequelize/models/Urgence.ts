import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
} from 'sequelize-typescript';

@Table({ tableName: 'Urgence' })
export class Urgence extends Model<Urgence, Partial<Urgence>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'code', type: DataType.STRING(5) })
  code: string;

  @Column({ field: 'libelle', type: DataType.STRING(255) })
  libelle: string;

  @Column({ field: 'couleur', type: DataType.STRING(20) })
  couleur: string;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  supprime: boolean;


  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

}
