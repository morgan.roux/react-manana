import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { DQReunion } from './DQReunion';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'DQ_reunion_action' })
export class DQReunionAction extends Model<DQReunionAction, Partial<DQReunionAction>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'type', type: DataType.STRING(100) })
  type: string; //'INCIDENT' | 'AMELIORATION' | OPERATIONNELLE;

  @Column({ field: 'id_type_associe', type: DataType.STRING(25) })
  idTypeAssocie: string;

  //TODO : Add relation with action todo
  @Column({ field: 'id_todo_action', type: DataType.STRING(25) })
  idTodoAction: string;

  @ForeignKey(() => DQReunion)
  @Column({ field: 'id_reunion', type: DataType.STRING(25) })
  idReunion: string;

  @BelongsTo(() => DQReunion)
  reunion: DQReunion;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
