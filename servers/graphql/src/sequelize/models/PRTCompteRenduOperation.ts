import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';
import { PRTCompteRendu } from './PRTCompteRendu';
import { PRTSuiviOperationnel } from './PRTSuiviOperationnel';

@Table({ tableName: 'PRT_compte_rendu_operation' })
export class PRTCompteRenduOperation extends Model<PRTCompteRenduOperation> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => PRTSuiviOperationnel)
  @Column({ field: 'id_suivi_operationnel', type: DataType.STRING(25) })
  idSuiviOperationnel: string;

  @Column({ field: 'id_todo_action', type: DataType.STRING(25) })
  idTodoAction: string;

  @ForeignKey(() => PRTCompteRendu)
  @Column({ field: 'id_compte_rendu', type: DataType.STRING(25) })
  idCompteRendu: string;

  @Column({ field: 'code_maj', type: DataType.STRING(255) })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
