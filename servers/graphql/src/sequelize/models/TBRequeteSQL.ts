import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement } from './Groupement';
import { TBSousModule } from './TBSousModule';
import { User } from './User';

@Table({ tableName: 'TB_requete' })
export class TBRequeteSQL extends Model<TBRequeteSQL, Partial<TBRequeteSQL>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'ordre', type: DataType.INTEGER })
  ordre: number;

  @Column({ field: 'titre', type: DataType.TEXT })
  titre: string;

  @Column({ field: 'requete', type: DataType.TEXT })
  requete: string;

  @Column({ field: 'visible_client', type: DataType.BOOLEAN })
  visibleClient: boolean;

  @ForeignKey(() => TBSousModule)
  @Column({ field: 'id_sous_module', type: DataType.STRING(25) })
  idSousModule: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
