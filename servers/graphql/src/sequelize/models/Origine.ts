import { Column, CreatedAt, DataType, Model, PrimaryKey, Table, UpdatedAt } from "sequelize-typescript";

@Table({ tableName: 'Origine' })
export class Origine extends Model<Origine, Partial<Origine>> {
    @Column({primaryKey: true, type:DataType.STRING(25)})
    id: string;

    @Column({ field: 'code', type: DataType.STRING(60)})
    code: string;

    @Column({ field:'libelle', type: DataType.STRING(60)})
    libelle: string;

    @Column({ field: 'date_creation' })
    @CreatedAt
    createdAt: Date;
  
    @Column({ field: 'date_modification' })
    @UpdatedAt
    updatedAt: Date;
}