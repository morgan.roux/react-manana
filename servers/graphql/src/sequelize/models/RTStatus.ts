import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';

@Table({ tableName: 'RT_status' })
export class RTStatus extends Model<RTStatus, Partial<RTStatus>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'code', type: DataType.STRING(20) })
  code: string;

  @Column({ field: 'couleur', type: DataType.STRING(8) })
  couleur: string;

  @Column({ field: 'libelle', type: DataType.STRING(25) })
  libelle: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
