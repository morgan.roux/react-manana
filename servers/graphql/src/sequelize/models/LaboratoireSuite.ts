import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Laboratoire_suite' })
export class LaboratoireSuite extends Model<LaboratoireSuite, Partial<LaboratoireSuite>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_id_labo_suite', type: DataType.INTEGER })
  resipIdLaboSuite: number;

  @Column({ field: 'nom_labo' })
  nom: string;

  @Column
  adresse: string;

  @Column({ field: 'code_postal' })
  codePostal: string;

  @Column
  telephone: string;

  @Column
  ville: string;

  @Column
  telecopie: string;

  @Column
  email: string;

  @Column({ field: 'web_site_url' })
  webSiteUrl: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
