import { Model, Table, Column, CreatedAt, UpdatedAt, DataType, ForeignKey } from 'sequelize-typescript';
import { Parametre } from './Parametre';

@Table({ tableName: 'Parameter_Value' })
export class ParametreValeur extends Model<ParametreValeur, Partial<ParametreValeur>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'id_source_associe', type: DataType.STRING(25) })
  idCategorieAssocie: string;

  @Column({ field: 'value', type: DataType.STRING(255) })
  valeur: string;


  @ForeignKey(() => Parametre)
  @Column({ field: 'id_parameter', type: DataType.STRING(25) })
  idParametre: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
