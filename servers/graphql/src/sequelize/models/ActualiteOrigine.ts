import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Actualite_origine' })
export class ActualiteOrigine extends Model<ActualiteOrigine, Partial<ActualiteOrigine>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'code', type: DataType.STRING(50), unique: true })
  code: string;

  @Column({ field: 'libelle', type: DataType.STRING(255) })
  libelle: string;

  @Column({ field: 'ordre', type: DataType.INTEGER })
  ordre: number;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
