import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Logiciel' })
export class Logiciel extends Model<Logiciel, Partial<Logiciel>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'nom', type: DataType.STRING(255) })
  nom: string;

  @Column({ field: 'os', type: DataType.STRING(255) })
  os: string;

  @Column({ field: 'os_version', type: DataType.STRING(100) })
  osVersion: string;

  @Column({ field: 'commentaire', type: DataType.TEXT })
  commentaire: string;

  @Column({ field: 'id_ssii', type: DataType.STRING(25) })
  idSsii: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
