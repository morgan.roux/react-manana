import { Model, Table, Column, CreatedAt, UpdatedAt, DataType, ForeignKey } from 'sequelize-typescript';
import { Fichier } from './Fichier';

@Table({ tableName: 'Laboratoire' })
export class Laboratoire extends Model<Laboratoire, Partial<Laboratoire>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_id_labo', type: DataType.STRING(4) })
  resipIdLabo: string;

  @Column({ field: 'resip_id_labo_suite', type: DataType.INTEGER })
  resipIdLaboSuite: number;

  @Column({ field: 'id_labo_suite', type: DataType.STRING(25) })
  idLaboSuite: string;

  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  supprime: boolean;

  @Column({ field: 'nom_labo', type: DataType.STRING(25) })
  nom: string;

  @Column({ field: 'sortie', type: DataType.SMALLINT })
  sortie: number;

  @Column({ field: 'id_laboratoire_rattachement', type: DataType.STRING(25) })
  idLaboratoireRattachement: string;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_photo', type: DataType.STRING(25) })
  idPhoto: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
