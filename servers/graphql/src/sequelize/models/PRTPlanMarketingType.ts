import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'PRT_plan_marketing_type' })
export class PRTPlanMarketingType extends Model<PRTPlanMarketingType, Partial<PRTPlanMarketingType>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'code', type: DataType.STRING(50), unique: true })
  code: string;

  @Column({ field: 'libelle', type: DataType.STRING(50) })
  libelle: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
