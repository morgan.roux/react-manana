import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement } from './Groupement';
import { User } from './User';
import { Item } from './Item';
import { Smyley } from './Smyley';

@Table({ tableName: 'UserSmyley' })
export class UserSmyley extends Model<UserSmyley, Partial<UserSmyley>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  supprime: boolean;

  @ForeignKey(() => Item)
  @Column({ field: 'id_item', type: DataType.STRING(25) })
  idItem: string;

  @Column({ field: 'id_source', type: DataType.STRING(25) })
  idItemAssocie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @ForeignKey(() => Smyley)
  @Column({ field: 'id_smyley', type: DataType.STRING(25) })
  idSmyley: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;
}
