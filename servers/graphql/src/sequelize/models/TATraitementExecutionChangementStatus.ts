import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { TATraitementExecution } from './TATraitementExecution';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'TA_traitement_execution_changement_status' })
export class TATraitementExecutionChangementStatus extends Model<
  TATraitementExecutionChangementStatus, Partial<TATraitementExecutionChangementStatus>
> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => TATraitementExecution)
  @Column({ field: 'id_traitement_execution', type: DataType.STRING(25) })
  idTraitementExecution: string;

  @BelongsTo(() => TATraitementExecution, { onDelete: 'cascade' })
  traitementExecution: TATraitementExecution;

  @Column({ field: 'status', type: DataType.STRING(25) })
  status: string; // EN_COURS, CLOTURE, PLANNIFIE

  @Column({ field: 'commentaire', type: DataType.TEXT })
  commentaire: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
