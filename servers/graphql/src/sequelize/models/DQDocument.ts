import { Model, Table, Column, CreatedAt, UpdatedAt, DataType, ForeignKey } from 'sequelize-typescript';
import { Fichier } from './Fichier';
import { User } from './User'
import { Groupement } from './Groupement'

@Table({ tableName: 'DQ_document' })
export class DQDocument extends Model<DQDocument, Partial<DQDocument>> {
    @Column({ primaryKey: true, type: DataType.STRING(25) })
    id: string;

    @Column({ field: 'ordre', type: DataType.INTEGER })
    ordre: number;

    @Column({ field: 'libelle', type: DataType.TEXT })
    libelle: string;

    @ForeignKey(() => Fichier)
    @Column({ field: 'id_fichier', type: DataType.STRING(25) })
    idFichier: string;

    @Column({ field: 'code_maj', type: DataType.STRING })
    codeMaj: string;

    @Column({ field: 'date_creation' })
    @CreatedAt
    createdAt: Date;

    @Column({ field: 'date_modification' })
    @UpdatedAt
    updatedAt: Date;

    @ForeignKey(() => Groupement)
    @Column({ field: 'id_groupement', type: DataType.STRING(25) })
    idGroupement: string;

    @ForeignKey(() => User)
    @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
    updatedBy: string;

    @ForeignKey(() => User)
    @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
    createdBy: string;
}
