import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Concept' })
export class Concept extends Model<Concept, Partial<Concept>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'typeConcept', type: DataType.STRING(255) })
  typeConcept: string;

  @Column({ field: 'modele', type: DataType.STRING(255) })
  modele: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
