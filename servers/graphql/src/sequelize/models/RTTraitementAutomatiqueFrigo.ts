import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { TATraitement } from './TATraitement';
import { RTFrigo } from './RTFrigo';

@Table({ tableName: 'RT_traitement_automatique_frigo' })
export class RTTraitementAutomatiqueFrigo extends Model<RTTraitementAutomatiqueFrigo, Partial<RTTraitementAutomatiqueFrigo>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => RTFrigo)
  @Column({ field: 'id_frigo', type: DataType.STRING(25) })
  idFrigo: string;

  @ForeignKey(() => TATraitement)
  @Column({ field: 'id_traitement_automatique', type: DataType.STRING(25) })
  idTraitementAutomatique: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
