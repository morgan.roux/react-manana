import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Item' })
export class Item extends Model<Item, Partial<Item>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'code', type: DataType.STRING(50), unique: true })
  code: string;

  @Column({ field: 'code_item', type: DataType.STRING(20) })
  codeItem: string;

  @Column({ field: 'name', type: DataType.STRING(255) })
  name: string;

  @Column({ field: 'id_parameter_groupement', type: DataType.STRING(25) })
  idParameterGroupement: string;

  @Column({ field: 'id_parameter_pharmacie', type: DataType.STRING(25) })
  idParameterPharmacie: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
