import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'RT_point_acces' })
export class RTPointAcces extends Model<RTPointAcces, Partial<RTPointAcces>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'nom', type: DataType.STRING(50) })
  nom: string;

  @Column({ field: 'numero_serie', type: DataType.STRING(25) })
  numeroSerie: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
