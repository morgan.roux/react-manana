import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement, User } from '.';
import { Contact } from './Contact';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'Client' })
export class Client extends Model<Client, Partial<Client>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'nom', type: DataType.STRING })
  nom: string;

  @Column({ field: 'prenom', type: DataType.STRING })
  prenom: string;

  @Column({ field: 'ville', type: DataType.STRING })
  ville: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @ForeignKey(() => Contact)
  @Column({ field: 'id_contact', type: DataType.STRING(25) })
  idContact: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
