import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Produit' })
export class Produit extends Model<Produit, Partial<Produit>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_id_produit', type: DataType.INTEGER })
  resipIdProduit: number;

  @Column({ type: DataType.STRING(34) })
  libelle: string;

  @Column({ type: DataType.STRING(255) })
  libelle2: string;

  @Column({ field: 'type' })
  type: number;

  @Column({ field: 'categorie' })
  categorie: number;

  @Column({ field: 'type_categorie' })
  typeCategorie: number;

  @Column({ field: 'is_referent_generique', type: DataType.SMALLINT })
  isReferentGenerique: number;

  @Column({ field: 'is_dopant', type: DataType.SMALLINT })
  isDopant: number;

  @Column({ field: 'code_p1', type: DataType.INTEGER })
  codePr1: number;

  @Column({ field: 'code_p2', type: DataType.INTEGER })
  codePr2: number;

  @Column({ field: 'code_p3', type: DataType.INTEGER })
  codePr3: number;

  @Column({ field: 'code_p4', type: DataType.INTEGER })
  codePr4: number;

  @Column({ field: 'code_p5', type: DataType.INTEGER })
  codePr5: number;

  @Column({ field: 'code_p6', type: DataType.INTEGER })
  codePr6: number;

  @Column({ field: 'medicament_exception', type: DataType.SMALLINT })
  medicamentException: number;

  @Column({ field: 'medicament_t2a', type: DataType.SMALLINT })
  medicamentT2a: number;

  @Column({ field: 'supprimer', type: DataType.DATE })
  supprimer: Date;

  @Column({ field: 'surveillance_renforcee' })
  surveillanceRenforcee: number;

  @Column({ field: 'molecule_onereuse' })
  moleculeOnereuse: number;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
