import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';
import { PRTAnalyse } from './PRTAnalyse';
import {
  ForeignKey,
} from 'sequelize-typescript';
import { Fichier } from './Fichier';

@Table({ tableName: 'PRT_fichier_analyse' })
export class PRTAnalyseFichier extends Model<PRTAnalyseFichier, Partial<PRTAnalyseFichier>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => PRTAnalyse)
  @Column({ field: 'id_analyse', type: DataType.STRING(25) })
  idAnalyse: string;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_fichier', type: DataType.STRING(25) })
  idFichier: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
