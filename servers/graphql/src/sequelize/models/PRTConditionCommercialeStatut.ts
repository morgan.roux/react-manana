import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'PRT_condition_commerciale_statut' })
export class PRTConditionCommercialeStatut extends Model<PRTConditionCommercialeStatut, Partial<PRTConditionCommercialeStatut>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'libelle', type: DataType.STRING })
  libelle: string;

  @Column({ field: 'code', type: DataType.STRING })
  code: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
