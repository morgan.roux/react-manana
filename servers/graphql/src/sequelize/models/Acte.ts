import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Acte' })
export class Acte extends Model<Acte, Partial<Acte>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_code_acte', type: DataType.INTEGER })
  resipCodeActe: number;

  @Column({ field: 'code_acte', type: DataType.INTEGER })
  codeActe: number;

  @Column({ field: 'libelle_acte', type: DataType.STRING(50) })
  libelleActe: string;

  @Column({ field: 'norme_b2', type: DataType.STRING(3) })
  normeB2: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
