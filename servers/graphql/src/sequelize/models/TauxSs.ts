import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Taux_ss' })
export class TauxSs extends Model<TauxSs, Partial<TauxSs>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_code_taux_ss', type: DataType.INTEGER })
  resipCodeTauxSs: number;

  @Column({ field: 'code_taux_ss', type: DataType.INTEGER })
  codeTauxSs: number;

  @Column({ field: 'taux_ss', type: DataType.INTEGER })
  tauxSs: number;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
