import { Model, Table, Column, CreatedAt, UpdatedAt, DataType, ForeignKey } from 'sequelize-typescript';
import { Groupement } from './Groupement'
import { DQSousTheme } from './DQSousTheme'
import { User } from './User'

@Table({ tableName: 'DQ_exigence' })
export class DQExigence extends Model<DQExigence, Partial<DQExigence>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'ordre', type: DataType.INTEGER })
  ordre: number;

  @Column({ field: 'titre', type: DataType.TEXT })
  titre: string;

  @Column({ field: 'finalite', type: DataType.TEXT })
  finalite: string;

  @ForeignKey(() => DQSousTheme)
  @Column({ field: 'id_sous_theme', type: DataType.STRING(25) })
  idSousTheme: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
