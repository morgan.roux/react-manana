import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';

@Table({ tableName: 'DQ_checklist_evaluation_cloture' })
export class DQChecklistEvaluationCloture extends Model<DQChecklistEvaluationCloture, Partial<DQChecklistEvaluationCloture>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'commentaire', type: DataType.TEXT })
  commentaire: string;

  @ForeignKey(() => DQChecklistEvaluationCloture)
  @Column({ field: 'id_checklist_evaluation', type: DataType.STRING(25) })
  idChecklistEvaluation: string;

  //TODO: Relation with Pharmacie
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
