import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement, User } from '.';
import { OptionPartageType } from './OptionPartageType';
import { Parametre } from './Parametre';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'Option_partage' })
export class OptionPartage extends Model<OptionPartage, Partial<OptionPartage>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => Parametre)
  @Column({ field: 'id_parametre', type: DataType.STRING(25) })
  idParametre: string;

  @ForeignKey(() => OptionPartageType)
  @Column({ field: 'id_type', type: DataType.STRING(25) })
  idType: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
