import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsTo
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { GedDocument } from './GedDocument';

@Table({ tableName: 'GED_document_changement_status' })
export class GedDocumentChangementStatus extends Model<GedDocumentChangementStatus, Partial<GedDocumentChangementStatus>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => GedDocument)
  @Column({ field: 'id_document', type: DataType.STRING(25) })
  idDocument: string;

  @BelongsTo(() => GedDocument, { onDelete: 'cascade' })
  document: GedDocument;

    // En attente d'approbation (EN_ATTENTE_APPROBATION)
    //  à consulter/ transmis pour consultation / transmis pour approbation / Approuvé / Refusé
  @Column({ field: 'status', type: DataType.STRING(100) })
  status: string;

  @Column({ field: 'commentaire', type: DataType.TEXT })
  commentaire: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
