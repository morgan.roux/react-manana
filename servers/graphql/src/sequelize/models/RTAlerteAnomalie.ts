import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { RTReleve } from './RTReleve';
import { User } from './User';

@Table({ tableName: 'RT_alerte_anomalie' })
export class RTAlerteAnomalie extends Model<RTAlerteAnomalie> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'type', type: DataType.STRING(25) })
  type: string;

  @ForeignKey(() => RTReleve)
  @Column({ field: 'id_releve', type: DataType.STRING(25) })
  idReleve: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @Column({ field: 'message', type: DataType.TEXT })
  message: string;

  @Column({ field: 'status', type: DataType.STRING(255) })
  status: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
