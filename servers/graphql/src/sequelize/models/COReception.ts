import {
  Column,
  DataType,
  Model,
  Table,
  ForeignKey,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';
import { User } from './User';
import { COCommandeOrale } from './COCommandeOrale';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'CO_reception' })
export class COReception extends Model<COReception, Partial<COReception>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => COCommandeOrale)
  @Column({ field: 'id_commande_orale', type: DataType.STRING(25) })
  idCommandeOrale: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @Column({ field: 'date_heure', type: DataType.DATE })
  dateHeure: Date;

  @Column({ field: 'commentaire', type: DataType.TEXT })
  commentaire: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}

//   id?: string;
//   idUser: string;
//   dateHeure: Date | null;
//   commentaire?: string;
//   idCommandeOrale: string;
