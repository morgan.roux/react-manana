import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from '.';

@Table({ tableName: 'Contact' })
export class Contact extends Model<Contact, Partial<Contact>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'adresse_1', type: DataType.STRING })
  adresse1: string;

  @Column({ field: 'adresse_2', type: DataType.STRING })
  adresse2: string;

  @Column({ field: 'cp', type: DataType.STRING })
  cp: string;

  @Column({ field: 'ville', type: DataType.STRING })
  ville: string;

  @Column({ field: 'pays', type: DataType.STRING })
  pays: string;

  @Column({ field: 'tel_prof', type: DataType.STRING })
  telProf: string;

  @Column({ field: 'tel_perso', type: DataType.STRING })
  telPerso: string;

  @Column({ field: 'tel_mob_prof', type: DataType.STRING })
  telMobProf: string;

  @Column({ field: 'tel_mob_perso', type: DataType.STRING })
  telMobPerso: string;

  @Column({ field: 'fax_prof', type: DataType.STRING })
  faxProf: string;

  @Column({ field: 'fax_perso', type: DataType.STRING })
  faxPerso: string;

  @Column({ field: 'mail_prof', type: DataType.STRING })
  mailProf: string;

  @Column({ field: 'mail_perso', type: DataType.STRING })
  mailPerso: string;

  @Column({ field: 'site_perso', type: DataType.STRING })
  sitePerso: string;

  @Column({ field: 'site_prof', type: DataType.STRING })
  siteProf: string;

  @Column({ field: 'compte_skype_prof', type: DataType.STRING })
  compteSkypeProf: string;

  @Column({ field: 'compte_skype_perso', type: DataType.STRING })
  compteSkypePerso: string;

  @Column({ field: 'url_linkedin_prof', type: DataType.STRING })
  urlLinkedInProf: string;

  @Column({ field: 'url_linkedin_perso', type: DataType.STRING })
  urlLinkedInPerso: string;

  @Column({ field: 'url_twitter_perso', type: DataType.STRING })
  urlTwitterPerso: string;

  @Column({ field: 'url_twitter_prof', type: DataType.STRING })
  urlTwitterProf: string;

  @Column({ field: 'url_facebook_prof', type: DataType.STRING })
  urlFacebookProf: string;

  @Column({ field: 'url_facebook_perso', type: DataType.STRING })
  urlFacebookPerso: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  idUserModification: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  idUserCreation: string;

  @Column({ field: 'url_messenger', type: DataType.STRING })
  urlMessenger: string;

  @Column({ field: 'url_youtube', type: DataType.STRING })
  urlYoutube: string;

  @Column({ field: 'whatsapp_mob_prof', type: DataType.STRING })
  whatsappMobProf: string;

  @Column({ field: 'whatsapp_mob_perso', type: DataType.STRING })
  whatsappMobPerso: string;
}
