import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  HasMany,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { GedSousCategorie } from './GedSousCategorie';
import { PrestataireService } from './PrestataireService';

@Table({ tableName: 'GED_categorie' })
export class GedCategorie extends Model<GedCategorie, Partial<GedCategorie>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'libelle', type: DataType.STRING(255) })
  libelle: string;

  /**
   * Egal à true si la categorie fait partie categorie de base comme, Operation commerciales, Catalogues produits, ...
   *
   */
  @Column({ field: 'base', type: DataType.BOOLEAN })
  base: boolean;

  /**
   * Si id_pharmacie!=null, ça veut dire que cette catégorie est spécifique pour cette pharmacie.
   * Tous les documents dans cette catégorie doivent être appouvés par les titulaires
   * Si id_pharmacie=null, les documents doivent être approuvés par les responsables qualités du groupement
   */

  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @HasMany(() => GedSousCategorie)
  sousCategories: GedSousCategorie[];

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @Column({ field: 'type', type: DataType.STRING(100) })
  type: string; //FACTURE

  @Column({ field: 'workflow_validation', type: DataType.STRING(25) })
  workflowValidation: string;

  @ForeignKey(() => PrestataireService)
  @Column({ field: 'id_partenaire_validateur', type: DataType.STRING(25) })
  idPartenaireValidateur: string;

  @Column({ field: 'validation', type: DataType.BOOLEAN })
  validation: boolean;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
