import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement } from './Groupement';
@Table({ tableName: 'Smyley' })
export class Smyley extends Model<Smyley, Partial<Smyley>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'nom', type: DataType.STRING(255) })
  nom: string;

  @Column({ field: 'photo', type: DataType.STRING(255) })
  photo: string;

  @Column({ field: 'note', type: DataType.SMALLINT })
  note: number;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
