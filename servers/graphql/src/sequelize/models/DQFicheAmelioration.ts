import {
  BelongsTo,
  BelongsToMany,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { DQActivite, DQMTFonction, DQMTTache, DQSolution, Importance, Origine, Urgence } from '.';
import { DQFicheAmeliorationAction } from './DQFicheAmeliorationAction';
import { DQFicheAmeliorationFichier } from './DQFicheAmeliorationFichier';
import { DQFicheCause } from './DQFicheCause';
import { DQFicheType } from './DQFicheType';
import { DQReunion } from './DQReunion';
import { DQStatut } from './DQStatut';
import { Fichier } from './Fichier';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';
import { User } from './User';
@Table({ tableName: 'DQ_fiche_amelioration' })
export class DQFicheAmelioration extends Model<DQFicheAmelioration, Partial<DQFicheAmelioration>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @BelongsToMany(() => Fichier, () => DQFicheAmeliorationFichier)
  fichiers: Fichier[];

  @ForeignKey(() => DQStatut)
  @Column({ field: 'id_statut', type: DataType.STRING(25) })
  idStatut: string;

  @ForeignKey(() => DQFicheType)
  @Column({ field: 'id_type', type: DataType.STRING(25) })
  idType: string;

  @ForeignKey(() => DQFicheCause)
  @Column({ field: 'id_cause', type: DataType.STRING(25) })
  idCause: string;

  @Column({ field: 'date_amelioration', type: DataType.DATE })
  dateAmelioration: Date;

  @Column({ field: 'description', type: DataType.TEXT })
  description: string;

  @ForeignKey(() => Origine)
  @Column({ field: 'id_origine', type: DataType.STRING(25) })
  idOrigine: string;

  @Column({ field: 'id_origine_associe', type: DataType.STRING(25) })
  idOrigineAssocie: string;

  @BelongsTo(() => Origine)
  origine: Origine;

  @ForeignKey(() => Urgence)
  @Column({ field: 'id_urgence', type: DataType.STRING(25) })
  idUrgence: string;

  @Column({ field: 'date_echeance', type: DataType.DATE })
  dateEcheance: Date;

  @ForeignKey(() => Importance)
  @Column({ field: 'id_importance', type: DataType.STRING(25) })
  idImportance: string;

  @ForeignKey(() => DQSolution)
  @Column({ field: 'id_solution', type: DataType.STRING(25) })
  idSolution: string;

  @ForeignKey(() => DQMTFonction)
  @Column({ field: 'id_fonction', type: DataType.STRING(25) })
  idFonction: string;

  @BelongsTo(() => DQMTFonction)
  fonction: DQMTFonction;

  @ForeignKey(() => DQActivite)
  @Column({ field: 'id_activite', type: DataType.STRING(25) })
  idActivite: string;

  @ForeignKey(() => DQMTTache)
  @Column({ field: 'id_tache', type: DataType.STRING(25) })
  idTache: string;

  @BelongsTo(() => DQMTTache)
  tache: DQMTTache;

  @ForeignKey(() => DQMTFonction)
  @Column({ field: 'id_fonction_a_informer', type: DataType.STRING(25) })
  idFonctionAInformer: string;


  @BelongsTo(() => DQMTFonction)
  fonctionAInformer: DQMTFonction;

  @ForeignKey(() => DQMTTache)
  @Column({ field: 'id_tache_a_informer', type: DataType.STRING(25) })
  idTacheAInformer: string;

  @BelongsTo(() => DQMTTache)
  tacheAInformer: DQMTTache;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_auteur', type: DataType.STRING(25) })
  idUserAuteur: string;

  @BelongsTo(() => User)
  auteur: User;

  @ForeignKey(() => DQFicheAmeliorationAction)
  @Column({ field: 'id_action', type: DataType.STRING(25) })
  idAction: string;

  @BelongsTo(() => DQFicheAmeliorationAction)
  action: DQFicheAmeliorationAction;

  @ForeignKey(() => DQReunion)
  @Column({ field: 'id_reunion', type: DataType.STRING(25) })
  idReunion: string;

  @BelongsTo(() => DQReunion)
  reunion: DQReunion;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
