import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'PRT_plan_marketing_statut' })
export class PRTPlanMarketingStatut extends Model<PRTPlanMarketingStatut, Partial<PRTPlanMarketingStatut>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'code', type: DataType.STRING(50) })
  code: string;

  @Column({ field: 'libelle', type: DataType.STRING(50) })
  libelle: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
