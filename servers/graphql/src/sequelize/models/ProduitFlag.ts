import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  DataType,
} from 'sequelize-typescript';
import { Produit } from './Produit';

@Table({ tableName: 'Produit_flag' })
export class ProduitFlag extends Model<ProduitFlag, Partial<ProduitFlag>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_id_produit', type: DataType.INTEGER })
  resipIdProduit: number;

  @ForeignKey(() => Produit)
  @Column({ field: 'id_produit', type: DataType.STRING(25) })
  idProduit: string;


  @Column({ field: 'derive_sang', type: DataType.SMALLINT })
  deriveSang: number;

  @Column(DataType.SMALLINT)
  anticoagulant: number;

  @Column(DataType.SMALLINT)
  avk: number;

  @Column(DataType.SMALLINT)
  vaccin: number;

  @Column({ field: 'solute_dilution', type: DataType.SMALLINT })
  soluteDilution: number;

  @Column({ field: 'contraception_urgence', type: DataType.SMALLINT })
  contraceptionUrgence: number;

  @Column({ field: 'contraceptif_mineurs', type: DataType.SMALLINT })
  contraceptifMineurs: number;

  @Column(DataType.SMALLINT)
  frigo: number;

  @Column(DataType.SMALLINT)
  stupefiant: number;

  @Column({ field: 'antibio_humain', type: DataType.SMALLINT })
  antibioHumain: number;

  @Column({ field: 'antibio_veterinaire', type: DataType.SMALLINT })
  antibioVeterinaire: number;

  @Column(DataType.SMALLINT)
  deconditionnement: number;

  @Column({ field: 'vaccin_grippe_saisonniere', type: DataType.SMALLINT })
  vaccinGrippeSaisonniere: number;

  @Column(DataType.SMALLINT)
  ghs: number;

  @Column({ field: 'hors_ghs', type: DataType.SMALLINT })
  horsGhs: number;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
