import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement, Pharmacie, User } from '.';

@Table({ tableName: 'PRT_rendez_vous' })
export class PRTRendezVous extends Model<PRTRendezVous, Partial<PRTRendezVous>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'ordre_jour', type: DataType.STRING(25) })
  ordreJour: string;

  @Column({ field: 'date_rendez_vous', type: DataType.DATE })
  dateRendezVous: Date;

  @Column({ field: 'heure_debut', type: DataType.TIME })
  heureDebut: string;

  @Column({ field: 'heure_fin', type: DataType.TIME })
  heureFin: string;

  @Column({ field: 'statut', type: DataType.STRING(100) })
  statut: string;// REPORTE | REALISE | EN_COURS

  @Column({ field: 'id_partenaire_type_associe', type: DataType.STRING(25) })
  idPartenaireTypeAssocie: string;

  @Column({ field: 'partenaire_type', type: DataType.STRING(60) })
  partenaireType: string; // 'LABORATOIRE' | 'PRESTATAIRE_SERVICE'

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
