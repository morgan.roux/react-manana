import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { DQMTTache } from './DQMTTache';
import { DQMTFonction } from './DQMTFonction';
import { DQMTResponsableType } from './DQMTResponsableType';

@Table({ tableName: 'DQ_mt_tache_responsable' })
export class DQMTTacheResponsable extends Model<DQMTTacheResponsable, Partial<DQMTTacheResponsable>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => DQMTResponsableType)
  @Column({ field: 'id_type', type: DataType.STRING(25) })
  idType: string;

  @BelongsTo(() => DQMTResponsableType, { onDelete: 'cascade' })
  type: DQMTResponsableType;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  // Null pour la matrice des fonctions niveau 1
  @ForeignKey(() => DQMTTache)
  @Column({ field: 'id_tache', type: DataType.STRING(25) })
  idTache: string;

  @BelongsTo(() => DQMTTache, { onDelete: 'cascade' })
  tache: DQMTTache;

  @ForeignKey(() => DQMTFonction)
  @Column({ field: 'id_fonction', type: DataType.STRING(25) })
  idFonction: string;

  @BelongsTo(() => DQMTFonction, { onDelete: 'cascade' })
  fonction: DQMTTache;

  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
