import {
  Column,
  DataType,
  Model,
  Table,
  ForeignKey,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';
import { User } from './User';

@Table({ tableName: 'Information_liaison_user_concernee' })
export class InformationLiaisonUserConcernee extends Model<InformationLiaisonUserConcernee, Partial<InformationLiaisonUserConcernee>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'id_information_liaison', type: DataType.STRING(25) })
  idInformationLiaison: string;

  @Column({ field: 'id_user_concernee', type: DataType.STRING(25) })
  idUserConcernee: string;

  @Column({ field: 'statut', type: DataType.STRING(25) })
  statut: string;

  // @Column({ field: 'code_maj', type: DataType.STRING })
  // codeMaj: string;

  @Column({ field: 'dateStatutModification' })
  dateStatutModification: Date;

  // @ForeignKey(() => Pharmacie)
  // @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  // idPharmacie: string;

  // @ForeignKey(() => Groupement)
  // @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  // idGroupement: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
