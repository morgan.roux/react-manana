import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Fichier } from './Fichier';
import { PRTConditionCommerciale } from './PRTConditionCommerciale';

@Table({ tableName: 'PRT_condition_commerciale_fichier' })
export class PRTConditionCommercialeFichier extends Model<PRTConditionCommercialeFichier, Partial<PRTConditionCommercialeFichier>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => PRTConditionCommerciale)
  @Column({ field: 'id_condition_commerciale', type: DataType.STRING(25) })
  idConditionCommerciale: string;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_fichier', type: DataType.STRING(25) })
  idFichier: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
