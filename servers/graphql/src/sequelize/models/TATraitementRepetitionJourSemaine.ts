import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';
import { TATraitement } from './TATraitement';

@Table({ tableName: 'TA_traitement_repetition_jour_semaine' })
export class TATraitementRepetitionJourSemaine extends Model<TATraitementRepetitionJourSemaine, Partial<TATraitementRepetitionJourSemaine>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => TATraitement)
  @Column({ field: 'id_traitement', type: DataType.STRING(25) })
  idTraitement: string;

  @BelongsTo(() => TATraitement, { onDelete: 'CASCADE' })
  traitement: string;

  @Column({ field: 'jour_semaine', type: DataType.SMALLINT }) // pour la repetition par semaine
  jourSemaine: number; // LUNDI (1), MARDI (2), MERCREDI (3), JEUDI (4), VENDREDI (5), SAMEDI (6), DIMANCHE (7)

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
