import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  HasMany,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { DQMTTache } from './DQMTTache';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'DQ_mt_personnalisation' })
export class DQMTPersonnalisation extends Model<DQMTPersonnalisation, Partial<DQMTPersonnalisation>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'type', type: DataType.STRING(20) })
  type: string; // FONCTION ou TACHE
  
  @Column({ field: 'type', type: DataType.STRING(25) })
  idTypeAssocie: string; 


  @Column({ field: 'ordre', type: DataType.INTEGER })
  ordre: number;

  @Column({ field: 'libelle', type: DataType.STRING(255) })
  libelle: string;


  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
