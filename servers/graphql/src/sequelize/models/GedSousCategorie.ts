import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { GedCategorie } from './GedCategorie';
import { GedDocument } from './GedDocument';
import { PrestataireService } from './PrestataireService';

@Table({ tableName: 'GED_sous_categorie' })
export class GedSousCategorie extends Model<GedSousCategorie, Partial<GedSousCategorie>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'libelle', type: DataType.STRING(255) })
  libelle: string;

  @Column({ field: 'base', type: DataType.BOOLEAN })
  base: boolean;

  @Column({field: 'workflow_validation', type: DataType.STRING(25)})
  workflowValidation: string;

  @ForeignKey(() => PrestataireService)
  @Column({ field: 'id_partenaire_validateur', type: DataType.STRING(25)})
  idPartenaireValidateur: string
;
  @Column({ field: 'validation', type: DataType.BOOLEAN })
  validation: boolean;

  @ForeignKey(() => GedCategorie)
  @Column({ field: 'id_categorie', type: DataType.STRING(25) })
  idCategorie: string;

  @BelongsTo(() => GedCategorie, { onDelete: 'cascade' })
  categorie: GedCategorie;

  @HasMany(() => GedDocument)
  documents: GedDocument[];

  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
