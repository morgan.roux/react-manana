import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  DataType,
} from 'sequelize-typescript';
import { Produit } from './Produit';

@Table({ tableName: 'Produit_tips' })
export class ProduitTips extends Model<ProduitTips, Partial<ProduitTips>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_id_produit', type: DataType.INTEGER })
  resipIdProduit: number;

  @ForeignKey(() => Produit)
  @Column({ field: 'id_produit', type: DataType.STRING(25) })
  idProduit: string;

  @Column({ type: DataType.INTEGER })
  ordre: number;

  @Column({ type: DataType.DECIMAL(19, 2) })
  coefficient: number;

  @Column(DataType.STRING(1))
  qualifiant: string;

  @Column({ field: 'code_tips', type: DataType.STRING(15) })
  codeTips: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
