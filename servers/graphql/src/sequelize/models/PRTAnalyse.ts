import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';

import { Fichier } from './Fichier';
import { Groupement, Pharmacie, User } from '.';
import { PRTAnalyseType } from './PRTAnalyseType';

@Table({ tableName: 'PRT_analyse' })
export class PRTAnalyse extends Model<PRTAnalyse, Partial<PRTAnalyse>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => PRTAnalyseType)
  @Column({ field: 'id_type', type: DataType.STRING(25) })
  idType: string;

  @Column({ field: 'titre', type: DataType.STRING(50) })
  titre: string;

  @Column({ field: 'date_debut', type: DataType.DATE })
  dateDebut: Date;

  @Column({ field: 'date_fin', type: DataType.DATE })
  dateFin: Date;

  @Column({ field: 'date_chargement', type: DataType.DATE })
  dateChargement: Date;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_fichier', type: DataType.STRING(25) })
  idFichier: string;

  @Column({ field: 'partenaire_type', type: DataType.STRING(60) })
  partenaireType: string; // LABORATOIRE | PRESTATAIRE_SERVICE

  @Column({ field: 'id_partenaire_type_associe', type: DataType.STRING(25) })
  idPartenaireTypeAssocie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
