import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Liste' })
export class Liste extends Model<Liste, Partial<Liste>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_code_liste', type: DataType.INTEGER })
  resipCodeListe: number;

  @Column({ field: 'code_liste', type: DataType.INTEGER })
  codeListe: number;

  @Column(DataType.STRING(20))
  libelle: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
