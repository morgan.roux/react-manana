import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';
import { Importance } from './Importance';

@Table({ tableName: 'PRT_suivi_operationnel' })
export class PRTSuiviOperationnel extends Model<
  PRTSuiviOperationnel,
  Partial<PRTSuiviOperationnel>
> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'id_type', type: DataType.STRING(25) })
  idType: string;

  @Column({ field: 'id_statut', type: DataType.STRING(25) })
  idStatut: string;

  @Column({ field: 'date_heure', type: DataType.DATE })
  dateHeure: Date;

  @Column({ field: 'titre', type: DataType.STRING(255) })
  titre: string;

  @Column({ field: 'description', type: DataType.TEXT })
  description: string;

  @Column({ field: 'montant', type: DataType.DECIMAL })
  montant: number;

  @ForeignKey(() => Importance)
  @Column({ field: 'id_importance', type: DataType.STRING(25) })
  idImportance: string;

  @Column({ field: 'partenaire_type', type: DataType.STRING(60) })
  partenaireType: string;

  @Column({ field: 'id_type_associe', type: DataType.STRING(25) })
  idTypeAssocie: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
