import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Contrat_statut' })
export class ContratStatut extends Model<ContratStatut, Partial<ContratStatut>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'nom', type: DataType.STRING(100) })
  nom: string;

  @Column({ field: 'numero_ordre', type: DataType.INTEGER })
  numeroOrdre: number;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
