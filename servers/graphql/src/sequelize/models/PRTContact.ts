import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Contact } from './Contact';
import { Fichier } from './Fichier';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'PRT_contact' })
export class PRTContact extends Model<PRTContact, Partial<PRTContact>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'civilite', type: DataType.STRING(4) })
  civilite: string;

  @Column({ field: 'nom', type: DataType.STRING(255) })
  nom: string;

  @Column({ field: 'prenom', type: DataType.STRING(255) })
  prenom: string;

  @Column({ field: 'fonction', type: DataType.STRING(255) })
  fonction: string;

  @Column({ field: 'code_maj', type: DataType.STRING(25) })
  codeMaj: string;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  supprime: boolean;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @Column({ field: 'id_partenaire_type_associe', type: DataType.STRING(25) })
  idPartenaireTypeAssocie: string;

  @Column({ field: 'partenaire_type', type: DataType.STRING(60) })
  partenaireType: string; // 'LABORATOIRE' | 'PRESTATAIRE_SERVICE'

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_photo', type: DataType.STRING(25) })
  idPhoto: string;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_avatar', type: DataType.STRING(25) })
  idAvatar: string;

  @ForeignKey(() => Contact)
  @Column({ field: 'id_contact', type: DataType.STRING(25) })
  idContact: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;
}
