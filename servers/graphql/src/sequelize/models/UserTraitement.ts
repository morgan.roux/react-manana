import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Traitement } from './Traitement';
import { User } from './User';

@Table({ tableName: 'User_traitement' })
export class UserTraitement extends Model<UserTraitement, Partial<UserTraitement>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  // This colonne is unused. Always true.
  @Column({ field: 'access', type: DataType.BOOLEAN })
  access: boolean;

  @ForeignKey(() => Traitement)
  @Column({ field: 'id_traitement', type: DataType.STRING(25) })
  idTraitement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
