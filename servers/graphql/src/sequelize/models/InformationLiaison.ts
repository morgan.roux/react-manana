import {
  Column,
  DataType,
  Model,
  Table,
  ForeignKey,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';
import { Item } from '.';
import { Groupement } from './Groupement';
import { Origine } from './Origine';
import { Pharmacie } from './Pharmacie';
import { User } from './User';

@Table({ tableName: 'Information_liaison' })
export class InformationLiaison extends Model<InformationLiaison, Partial<InformationLiaison>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'type', type: DataType.STRING })
  type: string;


  @ForeignKey(() => Origine)
  @Column({ field: 'id_origine', type: DataType.STRING(25) })
  idOrigine: string;

  @ForeignKey(() => Origine)
  @Column({ field: 'id_origine', type: DataType.STRING(25) })
  idOrigineAssocie: string;

  @Column({ field: 'statut', type: DataType.STRING })
  statut: string;

  @Column({ field: 'description', type: DataType.STRING })
  description: string;

  @Column({ field: 'id_fiche_incident', type: DataType.STRING(25) })
  idFicheIncident: string;

  @Column({ field: 'id_fiche_amelioration', type: DataType.STRING(25) })
  idFicheAmelioration: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  isRemoved: boolean;

  @ForeignKey(() => Item)
  @Column({ field: 'id_item', type: DataType.STRING(25) })
  idItem: string;

  @Column({ field: 'id_item_associe', type: DataType.STRING(25) })
  idItemAssocie: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'id_fiche_reclamation', type: DataType.STRING(25) })
  idFicheReclamation: string;

  @Column({ field: 'id_user_declarant', type: DataType.STRING(25) })
  idUserDeclarant: string;

  @Column({ field: 'id_todo_action', type: DataType.STRING(25) })
  idTodoAction: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;

  @Column({ field: 'bloquant', type: DataType.BOOLEAN })
  bloquant: boolean;

  @Column({ field: 'id_urgence', type: DataType.STRING(25) })
  idUrgence: string;

  @Column({ field: 'id_importance', type: DataType.STRING(25) })
  idImportance: string;

  @Column({ field: 'idTache', type: DataType.STRING(25) })
  idTache: string;

  @Column({ field: 'idFonction', type: DataType.STRING(25) })
  idFonction: string;

  @Column({ field: 'titre', type: DataType.STRING })
  titre: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'dateEcheance', type: DataType.DATE })
  dateEcheance: Date;
}
