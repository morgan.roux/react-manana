import { Model, Table, Column, DataType, ForeignKey } from 'sequelize-typescript';
import { PRTCompteRendu } from './PRTCompteRendu';
import { PRTPlanMarketing } from './PRTPLanMarketing';

@Table({ tableName: 'PRT_compte_rendu_plan_marketing' })
export class PRTCompteRenduPlanMarketing extends Model<
  PRTCompteRenduPlanMarketing,
  Partial<PRTCompteRenduPlanMarketing>
> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => PRTPlanMarketing)
  @Column({ field: 'id_plan_marketing', type: DataType.STRING(25) })
  idPlanMarketing: string;

  @ForeignKey(() => PRTCompteRendu)
  @Column({ field: 'id_compte_rendu', type: DataType.STRING(25) })
  idCompteRendu: string;
}
