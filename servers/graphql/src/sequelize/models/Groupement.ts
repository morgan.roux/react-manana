import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Groupement' })
export class Groupement extends Model<Groupement, Partial<Groupement>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'nom', type: DataType.STRING(255) })
  nom: string;

  @Column({ field: 'adresse1', type: DataType.STRING(255) })
  adresse1: string;

  @Column({ field: 'adresse2', type: DataType.STRING(255) })
  adresse2: string;

  @Column
  cp: string;

  @Column
  ville: string;

  @Column
  pays: string;

  @Column({ field: 'tel_bureau', type: DataType.STRING(20) })
  telBureau: string;

  @Column({ field: 'tel_mobile', type: DataType.STRING(20) })
  telMobile: string;

  @Column({ field: 'mail', type: DataType.STRING(255) })
  mail: string;

  @Column({ type: DataType.STRING(255) })
  site: string;

  @Column({ type: DataType.TEXT })
  commentaire: string;

  @Column({ field: 'nom_responsable', type: DataType.STRING(255) })
  nomResponsable: string;

  @Column({ field: 'prenom_responsable', type: DataType.STRING(255) })
  prenomResponsable: string;

  @Column({ field: 'date_sortie', type: DataType.DATE })
  dateSortie: Date;

  @Column({ field: 'sortie', type: DataType.INTEGER })
  sortie: number;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
