import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement } from './Groupement';
import { User } from './User';
import { Pharmacie } from './Pharmacie';
import { TodoCouleur } from './TodoCouleur';
import { DQMTFonction } from './DQMTFonction';
import { DQMTTache } from './DQMTTache';

@Table({ tableName: 'Todo_projet' })
export class TodoProjet extends Model<TodoProjet, Partial<TodoProjet>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'ordre', type: DataType.INTEGER })
  ordre: number;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @Column({ field: 'name', type: DataType.STRING(255) })
  nom: string;

  @Column({ field: 'is_archived', type: DataType.BOOLEAN })
  archive: boolean;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  supprime: boolean;

  @Column({ field: 'is_shared', type: DataType.BOOLEAN })
  partage: boolean;

  @Column({ field: 'typeProject', type: DataType.STRING(100) })
  type: string;

  // Generated from matrice de responsabilité
  @ForeignKey(() => DQMTFonction)
  @Column({ field: 'id_fonction', type: DataType.STRING(25) })
  idFonction: string;

  // Generated from matrice de responsabilité
  @ForeignKey(() => DQMTTache)
  @Column({ field: 'id_tache', type: DataType.STRING(25) })
  idTache: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => TodoCouleur)
  @Column({ field: 'id_couleur', type: DataType.STRING(25) })
  idCouleur: string;

  @Column({ field: 'id_projet_parent', type: DataType.STRING(25) })
  idParent: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
