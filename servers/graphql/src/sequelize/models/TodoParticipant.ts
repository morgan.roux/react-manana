import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { TodoProjet } from './TodoProjet';

@Table({ tableName: 'Todo_participant' })
export class TodoParticipant extends Model<TodoParticipant, Partial<TodoParticipant>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  supprime: boolean;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_participant', type: DataType.STRING(25) })
  idUserParticipant: string;

  @ForeignKey(() => TodoProjet)
  @Column({ field: 'id_projet', type: DataType.STRING(25) })
  idProjet: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
