import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Traitement } from './Traitement';
import { Groupement } from './Groupement';
import { Role } from './Role';

@Table({ tableName: 'Role_traitement' })
export class RoleTraitement extends Model<RoleTraitement, Partial<RoleTraitement>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  // This colonne is unused. Always true.
  @Column({ field: 'access', type: DataType.BOOLEAN })
  access: boolean;

  @ForeignKey(() => Traitement)
  @Column({ field: 'id_traitement', type: DataType.STRING(25) })
  idTraitement: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Role)
  @Column({ field: 'id_role', type: DataType.STRING(25) })
  idRole: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
