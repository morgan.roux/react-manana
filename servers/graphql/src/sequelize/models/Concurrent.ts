import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Concurrent' })
export class Concurrent extends Model<Concurrent, Partial<Concurrent>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'nom', type: DataType.STRING(255) })
  nom: string;

  @Column({ field: 'adresse_1', type: DataType.STRING(255) })
  adresse1: string;

  @Column({ field: 'adresse_2', type: DataType.STRING(255) })
  adresse2: string;

  @Column
  cp: string;

  @Column
  ville: string;

  @Column
  telephone1: string;

  @Column
  telephone2: string;

  @Column
  fax: string;

  @Column({ type: DataType.TEXT })
  commentaire: string;

  @Column({ field: 'nombre_pharmacies' })
  nombrePharmacies: string;

  @Column({ field: 'ca_moyen_pharmacie_TTC' })
  caMoyenPharmacieTTC: string;

  @Column({ field: 'ca_sell_out_TTC' })
  caSellOutTTC: string;

  @Column({ field: 'points_forts', type: DataType.TEXT })
  pointsForts: string;

  @Column({ field: 'points_faibles', type: DataType.TEXT })
  pointsFaibles: string;

  @Column({ field: 'commentaire_services', type: DataType.TEXT })
  commentaireServices: string;

  @Column({ field: 'commentaire_plateforme', type: DataType.TEXT })
  commentairePlateforme: string;

  @Column({ field: 'commentaire_general', type: DataType.TEXT })
  commentaireGeneral: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
