import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Groupe_gen' })
export class GroupeGen extends Model<GroupeGen, Partial<GroupeGen>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_code_groupe', type: DataType.INTEGER })
  resipCodeGroupe: number;

  @Column({ field: 'code_groupe', type: DataType.INTEGER })
  codeGroupe: number;

  @Column({ field: 'libelle_groupe', type: DataType.STRING(200) })
  libelleGroupe: string;

  @Column({ field: 'resip_code_groupe_pere', type: DataType.INTEGER })
  resipCodeGroupePere: number;

  @Column({ field: 'id_groupe_gen_pere', type: DataType.STRING(25) })
  idGroupeGenPere: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
