import { Model, Table, Column, CreatedAt, UpdatedAt, DataType, ForeignKey } from 'sequelize-typescript';
import { User } from './User'
import { Groupement } from './Groupement'
import { DQChecklistSection } from './DQChecklistSection'

@Table({ tableName: 'DQ_checklist_section_item' })
export class DQChecklistSectionItem extends Model<DQChecklistSectionItem, Partial<DQChecklistSectionItem>> {
    @Column({ primaryKey: true, type: DataType.STRING(25) })
    id: string;

    @Column({ field: 'ordre', type: DataType.INTEGER })
    ordre: number;

    @Column({ field: 'libelle', type: DataType.TEXT })
    libelle: string;

    @ForeignKey(() => DQChecklistSection)
    @Column({ field: 'id_checklist_section', type: DataType.STRING(25) })
    idChecklistSection: string;


    @Column({ field: 'code_maj', type: DataType.STRING })
    codeMaj: string;

    @Column({ field: 'date_creation' })
    @CreatedAt
    createdAt: Date;

    @Column({ field: 'date_modification' })
    @UpdatedAt
    updatedAt: Date;

    @ForeignKey(() => Groupement)
    @Column({ field: 'id_groupement', type: DataType.STRING(25) })
    idGroupement: string;

    @ForeignKey(() => User)
    @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
    updatedBy: string;

    @ForeignKey(() => User)
    @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
    createdBy: string;
}
