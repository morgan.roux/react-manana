import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { DQMTFonction } from './DQMTFonction';
import { DQMTTache } from './DQMTTache';
import { Importance } from './Importance';
import { Pharmacie } from './Pharmacie';
import { TATraitementType } from './TATraitementType';

@Table({ tableName: 'TA_traitement' })
export class TATraitement extends Model<TATraitement, Partial<TATraitement>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'description', type: DataType.TEXT })
  description: string;

  @ForeignKey(() => TATraitementType)
  @Column({ field: 'id_type', type: DataType.STRING(25) })
  idType: string;

  @ForeignKey(() => DQMTFonction)
  @Column({ field: 'id_fonction', type: DataType.STRING(25) })
  idFonction: string;

  @ForeignKey(() => DQMTTache)
  @Column({ field: 'id_tache', type: DataType.STRING(25) })
  idTache: string;

  @ForeignKey(() => Importance)
  @Column({ field: 'id_importance', type: DataType.STRING(25) })
  idImportance: string;

  // TODO : Id of urgence
  @Column({ field: 'id_urgence', type: DataType.STRING(25) })
  idUrgence: string;

  // Recurrence

  @Column({ field: 'repetition', type: DataType.INTEGER })
  repetition: number;

  @Column({ field: 'repetition_unite', type: DataType.STRING(10) })
  repetitionUnite: string; // JOUR, SEMAINE, MOIS, ANNEE

  /**
   * Explication : 
   *  - Pour les traitements repetés par Jour, les  colonnes, heure et repetition doivent être renseignées
   *   Par exemple : 
   *    
   *  - Pour les traitements repetés par Semaine, les colonnes repetition, heure doivent être renseignées et les jours de repetions doivent être stocker dans la table TAtraitementRepetitionJourSemaine
   *  - Pour les traitements repetés par Mois, les colonnes repetition, heure et jourMois doivent être renseignées
   *  - Pour les traitements repetés par Année, les colonnes repetition, heure,jourMois et moisAnnee  doivent être renseignées
   * 
   * 
   */

  @Column({ field: 'heure', type: DataType.TIME })
  heure: any; // hh:mm

  @Column({ field: 'jour_mois', type: DataType.SMALLINT }) // pour la repetition par mois
  jourMois: number; // 1-31 (31: fin du mois)

  @Column({ field: 'mois_annee', type: DataType.SMALLINT }) // pour la repetition par année
  moisAnnee: number; // 1-12

  //**************** Les 2 colonnes doivent être null pour les traitements qui ne se terminent jamais ***********
  @Column({ field: 'date_heure_fin', type: DataType.DATE })
  dateHeureFin: Date;

  @Column({ field: 'nombre_occurences_fin', type: DataType.INTEGER })
  nombreOccurencesFin: number;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
