import {
  BelongsToMany,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { DQMTFonction, DQMTTache, Importance, Urgence } from '.';
import { DQActionOperationnelleFichier } from './DQActionOperationnelleFichier';
import { DQFicheCause } from './DQFicheCause';
import { DQFicheType } from './DQFicheType';
import { DQStatut } from './DQStatut';
import { Fichier } from './Fichier';
import { Groupement } from './Groupement';
import { Origine } from './Origine';
import { Pharmacie } from './Pharmacie';
import { User } from './User';

@Table({ tableName: 'DQ_action_operationnelle' })
export class DQActionOperationnelle extends Model<DQActionOperationnelle, Partial<DQActionOperationnelle>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @BelongsToMany(() => Fichier, () => DQActionOperationnelleFichier)
  fichiers: Fichier[];

  @Column({ field: 'description', type: DataType.TEXT })
  description: string;

  @ForeignKey(() => Origine)
  @Column({ field: 'id_origine', type: DataType.STRING(25) })
  idOrigine: string;

  @ForeignKey(() => DQFicheType)
  @Column({ field: 'id_type', type: DataType.STRING(25) })
  idType: string;

  @ForeignKey(() => DQFicheCause)
  @Column({ field: 'id_cause', type: DataType.STRING(25) })
  idCause: string;

  @Column({ field: 'date_action', type: DataType.DATE })
  dateAction: Date;

  @Column({ field: 'id_origine_associe', type: DataType.STRING(25) })
  idOrigineAssocie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_auteur', type: DataType.STRING(25) })
  idUserAuteur: string;

  @ForeignKey(() => Urgence)
  @Column({ field: 'id_urgence', type: DataType.STRING(25) })
  idUrgence: string;

  @Column({ field: 'priorite', type: DataType.SMALLINT })
  priorite: number;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_suivi', type: DataType.STRING(25) })
  idUserSuivi: string;

  @ForeignKey(() => DQStatut)
  @Column({ field: 'id_statut', type: DataType.STRING(25) })
  idStatut: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;

  @Column({ field: 'date_echeance', type: DataType.DATE })
  dateEcheance: Date;

  @ForeignKey(() => Importance)
  @Column({ field: 'id_importance', type: DataType.STRING(25) })
  idImportance: string;

  @ForeignKey(() => DQMTFonction)
  @Column({ field: 'id_fonction', type: DataType.STRING(25) })
  idFonction: string;

  @ForeignKey(() => DQMTTache)
  @Column({ field: 'id_tache', type: DataType.STRING(25) })
  idTache: string;

  @ForeignKey(() => DQMTFonction)
  @Column({ field: 'id_fonction_a_informer', type: DataType.STRING(25) })
  idFonctionAInformer: string;

  @ForeignKey(() => DQMTTache)
  @Column({ field: 'id_tache_a_informer', type: DataType.STRING(25) })
  idTacheAInformer: string;
}
