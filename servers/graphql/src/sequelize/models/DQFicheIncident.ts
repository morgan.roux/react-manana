import {
  BelongsToMany,
  Column,
  CreatedAt,
  DataType,
  ForeignKey,
  Model,
  Table,
  UpdatedAt,
} from 'sequelize-typescript';
import { DQMTFonction, DQMTTache, Importance, Urgence } from '.';
import { DQFicheAmelioration } from './DQFicheAmelioration';
import { DQFicheCause } from './DQFicheCause';
import { DQFicheIncidentFichier } from './DQFicheIncidentFichier';
import { DQFicheType } from './DQFicheType';
import { DQSolution } from './DQSolution';
import { DQStatut } from './DQStatut';
import { Fichier } from './Fichier';
import { Groupement } from './Groupement';
import { Origine } from './Origine';
import { Pharmacie } from './Pharmacie';
import { User } from './User';

@Table({ tableName: 'DQ_fiche_incident' })
export class DQFicheIncident extends Model<DQFicheIncident, Partial<DQFicheIncident>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @BelongsToMany(() => Fichier, () => DQFicheIncidentFichier)
  fichiers: Fichier[];

  @Column({ field: 'description', type: DataType.TEXT })
  description: string;

  @ForeignKey(() => DQFicheAmelioration)
  @Column({ field: 'id_fiche_amelioration', type: DataType.STRING(25) })
  idFicheAmelioration: string;

  // TODO: Foreign key reclamation
  @Column({ field: 'id_ticket_reclamation', type: DataType.STRING(25) })
  idTicketReclamation: string;

  @ForeignKey(() => DQFicheType)
  @Column({ field: 'id_type', type: DataType.STRING(25) })
  idType: string;

  @ForeignKey(() => DQFicheCause)
  @Column({ field: 'id_cause', type: DataType.STRING(25) })
  idCause: string;

  @Column({ field: 'date_incident', type: DataType.DATE })
  dateIncident: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_declarant', type: DataType.STRING(25) })
  idUserDeclarant: string;

  @ForeignKey(() => DQStatut)
  @Column({ field: 'id_statut', type: DataType.STRING(25) })
  idStatut: string;

  @ForeignKey(() => Origine)
  @Column({ field: 'id_origine', type: DataType.STRING(25) })
  idOrigine: string;

  @Column({ field: 'id_origine_associe', type: DataType.STRING(25) })
  idOrigineAssocie: string;

  @ForeignKey(() => DQSolution)
  @Column({ field: 'id_solution', type: DataType.STRING(25) })
  idSolution: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;

  @Column({ field: 'date_echeance', type: DataType.DATE })
  dateEcheance: Date;

  @ForeignKey(() => Importance)
  @Column({ field: 'id_importance', type: DataType.STRING(25) })
  idImportance: string;

  @ForeignKey(() => Urgence)
  @Column({ field: 'id_urgence', type: DataType.STRING(25) })
  idUrgence: string;

  // En charge de l'incident
  @ForeignKey(() => DQMTFonction)
  @Column({ field: 'id_fonction', type: DataType.STRING(25) })
  idFonction: string;

  @ForeignKey(() => DQMTTache)
  @Column({ field: 'id_tache', type: DataType.STRING(25) })
  idTache: string;


  // à informer de l'incident
  @ForeignKey(() => DQMTFonction)
  @Column({ field: 'id_fonction_a_informer', type: DataType.STRING(25) })
  idFonctionAInformer: string;

  @ForeignKey(() => DQMTTache)
  @Column({ field: 'id_tache_a_informer', type: DataType.STRING(25) })
  idTacheAInformer: string;
}
