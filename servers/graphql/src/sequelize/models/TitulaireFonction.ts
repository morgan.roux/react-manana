import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Titulaire_fonction' })
export class TitulaireFonction extends Model<TitulaireFonction, Partial<TitulaireFonction>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'code_fonction', type: DataType.STRING(255) })
  code: string;

  @Column({ field: 'nom_fonction', type: DataType.STRING(255) })
  nom: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
