import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';

@Table({ tableName: 'Pharmacie' })
export class Pharmacie extends Model<Pharmacie, Partial<Pharmacie>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'nom', type: DataType.STRING(255) })
  nom: string;

  @Column({ field: 'adresse_1', type: DataType.STRING(255) })
  adresse1: string;

  @Column({ field: 'adresse_2', type: DataType.STRING(255) })
  adresse2: string;

  @Column
  cp: string;

  @Column
  ville: string;

  @Column
  pays: string;

  @Column
  cip: string;

  @Column({ field: 'num_finess' })
  numFiness: string;

  @Column({ field: 'ancien_finess' })
  ancienFiness: string;

  @Column
  uga: string;

  @Column({ type: DataType.TEXT })
  commentaire: string;

  @Column({ field: 'nb_jour_ouvre', type: DataType.INTEGER })
  nbJourOuvre: number;

  // TODO : change type to decimal
  @Column({ field: 'latitude', type: DataType.STRING })
  latitude: string;

  // TODO : change type to decimal
  @Column({ field: 'longitude', type: DataType.STRING })
  longitude: string;

  @Column({ field: 'nb_employe', type: DataType.INTEGER })
  nbEmploye: number;

  @Column({ field: 'nb_assistant', type: DataType.INTEGER })
  nbAssistant: number;

  @Column({ field: 'nb_autre_travailleur', type: DataType.INTEGER })
  nbAutreTravailleur: number;

  // TODO : change type to  boolean
  @Column({ type: DataType.INTEGER, defaultValue: 0 })
  sortie: number;

  // TODO : change to kebab_case
  @Column({ field: 'sortieFuture', type: DataType.INTEGER, defaultValue: 0 })
  sortieFuture: number;

  @Column({ field: 'date_sortie', type: DataType.DATE })
  dateSortie: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  // TODO : Relation with Contact
  @Column({ field: 'id_contact', type: DataType.STRING(25) })
  idContact: string;

  // TODO : Relation with Departement
  @Column({ field: 'id_departement', type: DataType.STRING(25) })
  idDepartement: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
