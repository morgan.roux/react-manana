import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  DataType,
} from 'sequelize-typescript';
import { Produit } from './Produit';
import { Laboratoire } from './Laboratoire';
import { Tva } from './Tva';
import { Acte } from './Acte';
import { Liste } from './Liste';
import { ProduitLibelleDivers } from './ProduitLibelleDivers';
import { TauxSs } from './TauxSs';
import { CisAgence } from './CisAgence';
import { ProduitUcd } from './ProduitUcd';

@Table({ tableName: 'Produit_tech_reg' })
export class ProduitTechReg extends Model<ProduitTechReg, Partial<ProduitTechReg>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;
  @ForeignKey(() => Produit)
  @Column({ field: 'id_produit', type: DataType.STRING(25) })
  idProduit: string;

  @Column({ field: 'resip_id_produit', type: DataType.INTEGER })
  resipIdProduit: number;


  @Column(DataType.STRING(34))
  libelle: string;

  @Column(DataType.STRING(34))
  libelle2: string;

  @Column(DataType.STRING(34))
  libelle3: string;

  @Column(DataType.STRING(7))
  cip7: string;

  @Column({ field: 'cip7_bcb', type: DataType.SMALLINT })
  cip7Bcb: number;

  @Column(DataType.STRING(13))
  cip13: string;

  @Column(DataType.STRING(7))
  ucd7: string;

  @Column(DataType.STRING(13))
  ucd13: string;

  @Column({ field: 'resip_cis', type: DataType.STRING(8) })
  resipCis: number;

  @ForeignKey(() => CisAgence)
  @Column({ field: 'id_cis_agence', type: DataType.STRING(25) })
  idCisAgence: string;

  @Column({ field: 'is_tips_lpp', type: DataType.SMALLINT })
  isTipsLpp: number;

  @Column({ field: 'is_homeo', type: DataType.SMALLINT })
  isHomeo: number;

  @Column({ field: 'is_tfr', type: DataType.SMALLINT })
  isTfr: number;

  @Column({ field: 'is_stupefiant', type: DataType.SMALLINT })
  isStupefiant: number;

  @Column({ field: 'resip_id_labo_titulaire', type: DataType.STRING(4) })
  resipIdLaboTitulaire: string;

  @ForeignKey(() => Laboratoire)
  @Column({ field: 'id_labo_titulaire', type: DataType.STRING(25) })
  idLaboTitulaire: string;

  @Column({ field: 'resip_id_labo_exploitant', type: DataType.STRING(4) })
  resipIdLaboExploitant: string;

  @ForeignKey(() => Laboratoire)
  @Column({ field: 'id_labo_exploitant', type: DataType.STRING(25) })
  idLaboExploitant: string;

  @Column({ field: 'resip_id_labo_distributeur', type: DataType.STRING(4) })
  resipIdLaboDistributeur: string;

  @ForeignKey(() => Laboratoire)
  @Column({ field: 'id_labo_distributeur', type: DataType.STRING(25) })
  idLaboDistributeur: string;

  @Column({ field: 'resip_code_tva', type: DataType.INTEGER })
  resipCodeTva: number;

  @ForeignKey(() => Tva)
  @Column({ field: 'id_tva', type: DataType.STRING(25) })
  idTva: string;

  // BCB : prix_achat_ht
  @Column({ field: 'prix_achat', type: DataType.DECIMAL(19, 3) })
  prixAchat: number;

  // BCB : prix_achat_ht_fabricant
  @Column({ field: 'prix_achat_ht', type: DataType.DECIMAL(19, 3) })
  prixAchatHt: number;

  @Column({ field: 'prix_vente_ttc', type: DataType.DECIMAL(19, 3) })
  prixVenteTtc: number;

  @Column({ field: 'resip_code_acte', type: DataType.INTEGER })
  resipCodeActe: number;

  @ForeignKey(() => Acte)
  @Column({ field: 'id_acte', type: DataType.STRING(25) })
  idActe: string;

  @Column({ field: 'resip_code_liste', type: DataType.INTEGER })
  resipCodeListe: number;

  @ForeignKey(() => Liste)
  @Column({ field: 'id_liste', type: DataType.STRING(25) })
  idListe: string;

  @Column({ field: 'resip_code_stockage', type: DataType.INTEGER })
  resipCodeStockage: number;

  @ForeignKey(() => ProduitLibelleDivers)
  @Column({ field: 'id_libelle_stockage', type: DataType.STRING(25) })
  idLibelleStockage: string;

  @Column({ field: 'resip_code_taux_ss', type: DataType.INTEGER })
  resipCodeTauxSs: number;

  @Column({ field: 'id_taux_ss', type: DataType.STRING(25) })
  @ForeignKey(() => TauxSs)
  idTauxSs: string;

  @Column({ field: 'base_remboursement_ss', type: DataType.DECIMAL(19, 3) })
  baseRemboursementSs: number;

  @Column({ field: 'code_poids_volume', type: DataType.STRING(1) })
  codePoidsVolume: string;

  // BCB : No correspondance
  @Column({ field: 'poids_volume', type: DataType.STRING })
  poidsVolume: string;

  @Column(DataType.INTEGER)
  tpn: number;

  @Column(DataType.INTEGER)
  peremption: number;

  @Column({ field: 'multiple_vente', type: DataType.INTEGER })
  multipleVente: number;

  @Column(DataType.INTEGER)
  hauteur: number;

  @Column(DataType.INTEGER)
  largeur: number;

  @Column(DataType.INTEGER)
  longueur: number;

  @Column({ field: 'resip_statut', type: DataType.INTEGER })
  resipStatut: number;

  // BCB : status
  @Column({ field: 'id_libelle_statut', type: DataType.STRING(25) })
  idLibelleStatut: string;

  @Column({ field: 'numero_amm', type: DataType.STRING })
  numeroAmm: string;

  @Column({ field: 'date_amm', type: DataType.DATE })
  dateAmm: Date;

  // BCB : agrement_collectivites
  @Column({ field: 'agrement_collectivite', type: DataType.SMALLINT })
  agrementCollectivite: number;

  @Column(DataType.SMALLINT)
  otc: number;

  @Column({ field: 'resip_attribut_casher', type: DataType.INTEGER })
  resipAttributCasher: number;

  // BCB : attribut_casher
  @Column({ field: 'id_libelle_casher', type: DataType.STRING(25) })
  idLibelleCasher: string;

  @Column({ field: 'resip_type_conditionnement', type: DataType.INTEGER })
  resipTypeConditionnement: number;

  // BCB : type_conditionnement
  @Column({ field: 'id_libelle_conditionnement', type: DataType.STRING(25) })
  idLibelleConditionnement: string;

  @Column({ field: 'code_13_referent', type: DataType.SMALLINT })
  code13Referent: number;

  @Column({ field: 'resip_id_produit_remplacant', type: DataType.INTEGER })
  resipIdProduitRemplacant: number;

  @Column({ field: 'id_produit_remplacant', type: DataType.STRING(25) })
  @ForeignKey(() => Produit)
  idProduitRemplacant: string;

  @Column({ field: 'resip_ucd7', type: DataType.STRING(7) })
  resipUcd7: string;

  // BCB : Extract from ucd7 and ucd13
  @ForeignKey(() => ProduitUcd)
  @Column({ field: 'id_produit_ucd', type: DataType.STRING(25) })
  idProduitUcd: string;

  @Column({ field: 'code_index_acl', type: DataType.STRING(7) })
  codeIndexAcl: number;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
