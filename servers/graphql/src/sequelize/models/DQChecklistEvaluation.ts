import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { DQChecklist } from './DQChecklist';
import { DQChecklistEvaluationCloture } from './DQChecklistEvaluationCloture';

@Table({ tableName: 'DQ_checklist_evaluation' })
export class DQChecklistEvaluation extends Model<DQChecklistEvaluation, Partial<DQChecklistEvaluation>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'ordre', type: DataType.INTEGER })
  ordre: number;

  @Column({ field: 'libelle', type: DataType.TEXT })
  libelle: string;

  @ForeignKey(() => DQChecklistEvaluationCloture)
  @Column({ field: 'id_checklist_evaluation_cloture', type: DataType.STRING(25) })
  idChecklistEvaluationCloture: string;

  @ForeignKey(() => DQChecklist)
  @Column({ field: 'id_checklist', type: DataType.STRING(25) })
  idChecklist: string;

  //TODO: Relation with Pharmacie
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
