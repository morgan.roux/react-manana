import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { RTReleve } from './RTReleve';
import { RTFrigo } from './RTFrigo';
import { DQFicheIncident } from './DQFicheIncident';

@Table({ tableName: 'RT_alerte_anomalie' })
export class RTAlerteAnomalie extends Model<RTAlerteAnomalie, Partial<RTAlerteAnomalie>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'type', type: DataType.STRING(25) })
  type: string;

  @ForeignKey(() => RTReleve)
  @Column({ field: 'id_releve', type: DataType.STRING(25) })
  idReleve: string;

  @ForeignKey(() => RTFrigo)
  @Column({ field: 'id_frigo', type: DataType.STRING(25) })
  idFrigo: string;

  @ForeignKey(() => DQFicheIncident)
  @Column({ field: 'id_fiche_incident', type: DataType.STRING(25) })
  idFicheIncident: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
