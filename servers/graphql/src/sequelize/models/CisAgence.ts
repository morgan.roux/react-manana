import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

//TODO : Relation avec techReg
@Table({ tableName: 'Cis_agence' })
export class CisAgence extends Model<CisAgence, Partial<CisAgence>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_code_cis', type: DataType.STRING(8) })
  resipCodeCis: string;

  @Column({ field: 'code_cis', type: DataType.STRING(8) })
  codeCis: string;

  @Column
  libelle: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
