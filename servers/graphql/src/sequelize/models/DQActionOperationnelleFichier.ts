import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { Fichier } from './Fichier';
import { DQActionOperationnelle } from './DQActionOperationnelle';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'DQ_action_operationnelle_fichier' })
export class DQActionOperationnelleFichier extends Model<DQActionOperationnelleFichier, Partial<DQActionOperationnelleFichier>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => DQActionOperationnelle)
  @Column({ field: 'id_action_operationnelle', type: DataType.STRING(25) })
  idActionOperationnelle: string;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_fichier', type: DataType.STRING(25) })
  idFichier: string;


  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
