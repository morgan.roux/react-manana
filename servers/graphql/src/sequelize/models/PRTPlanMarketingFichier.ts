import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Fichier } from './Fichier';
import { PRTPlanMarketing } from './PRTPLanMarketing';

@Table({ tableName: 'PRT_plan_marketing_fichier' })
export class PRTPlanMarketingFichier extends Model<PRTPlanMarketingFichier, Partial<PRTPlanMarketingFichier>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => PRTPlanMarketing)
  @Column({ field: 'id_plan_marketing', type: DataType.STRING(25) })
  idPlanMarketing: string;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_fichier', type: DataType.STRING(25) })
  idFichier: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
