import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement, User } from '.';
import { Pharmacie } from './Pharmacie';
import { PRTPlanMarketingStatut } from './PRTPlanMarketingStatut';
import { PRTPlanMarketingType } from './PRTPlanMarketingType';

@Table({ tableName: 'PRT_plan_marketing' })
export class PRTPlanMarketing extends Model<PRTPlanMarketing, Partial<PRTPlanMarketing>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => PRTPlanMarketingType)
  @Column({ field: 'id_type', type: DataType.STRING(25) })
  idType: string;

  @Column({ field: 'titre', type: DataType.STRING(100) })
  titre: string;

  @Column({ field: 'description', type: DataType.TEXT })
  description: string;

  @Column({ field: 'date_debut', type: DataType.DATE })
  dateDebut: Date;

  @Column({ field: 'date_fin', type: DataType.DATE })
  dateFin: Date;

  @ForeignKey(() => PRTPlanMarketingStatut)
  @Column({ field: 'id_statut', type: DataType.STRING(25) })
  idStatut: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;

  @Column({ field: 'partenaire_type', type: DataType.STRING(60) })
  partenaireType: string; // LABORATOIRE | PRESTATAIRE_SERVICE

  @Column({ field: 'id_partenaire_type_associe', type: DataType.STRING(25) })
  idPartenaireTypeAssocie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
