import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { RTFrigo } from './RTFrigo';
import { User } from './User';
import { RTStatus } from './RTStatus';
import { RTCapteur } from './RTCapteur'
import { RTPointAcces } from './RTPointAcces'

@Table({ tableName: 'RT_releve' })
export class RTReleve extends Model<RTReleve, Partial<RTReleve>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'temperature', type: DataType.FLOAT })
  temperature: number;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @ForeignKey(() => RTStatus)
  @Column({ field: 'id_status', type: DataType.STRING(25) })
  idStatus: string;

  @ForeignKey(() => RTFrigo)
  @Column({ field: 'id_frigo', type: DataType.STRING(25) })
  idFrigo: string;

  @ForeignKey(() => RTCapteur)
  @Column({ field: 'id_capteur', type: DataType.STRING(25) })
  idCapteur: string;

  @ForeignKey(() => RTPointAcces)
  @Column({ field: 'id_point_acces', type: DataType.STRING(25) })
  idPointAcces: string;

  @Column({ field: 'type', type: DataType.STRING(25) })
  type: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
