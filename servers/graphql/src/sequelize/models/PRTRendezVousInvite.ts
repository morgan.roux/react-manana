import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement, Pharmacie, User } from '.';
import { PRTRendezVous } from './PRTRendezVous';
import { PRTContact } from './PRTContact';

@Table({ tableName: 'PRT_rendez_vous_invite' })
export class PRTRendezVousInvite extends Model<PRTRendezVousInvite, Partial<PRTRendezVousInvite>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => PRTRendezVous)
  @Column({ field: 'id_rendez_vous', type: DataType.STRING(25) })
  idRendezVous: string;

  @ForeignKey(() => PRTContact)
  @Column({ field: 'id_contact', type: DataType.STRING(25) })
  idContact: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
