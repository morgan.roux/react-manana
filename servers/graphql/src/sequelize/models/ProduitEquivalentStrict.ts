import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  DataType,
} from 'sequelize-typescript';
import { Produit } from './Produit';

@Table({ tableName: 'Produit_equivalent_strict' })
export class ProduitEquivalentStrict extends Model<ProduitEquivalentStrict, Partial<ProduitEquivalentStrict>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_id_produit_ref', type: DataType.INTEGER })
  resipIdProduitRef: number;

  @Column({ field: 'resip_id_produit_equivalent', type: DataType.INTEGER })
  resipIdProduitEquivalent: number;

  @ForeignKey(() => Produit)
  @Column({ field: 'id_produit_ref', type: DataType.STRING(25) })
  idProduitRef: string;

  @ForeignKey(() => Produit)
  @Column({ field: 'id_produit_equivalent', type: DataType.STRING(25) })
  idProduitEquivalent: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
