import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';
@Table({ tableName: 'PRT_compte_rendu' })
export class PRTCompteRendu extends Model<PRTCompteRendu, Partial<PRTCompteRendu>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'titre', type: DataType.STRING })
  titre: string;

  @Column({ field: 'remise_echantillon', type: DataType.BOOLEAN })
  remiseEchantillon: boolean;

  @Column({ field: 'gestion_perime', type: DataType.TEXT })
  gestionPerime: string;

  @Column({ field: 'rapport_visite', type: DataType.TEXT })
  rapportVisite: string;

  @Column({ field: 'conclusion', type: DataType.TEXT })
  conclusion: string;

  @Column({ field: 'id_partenaire_type_associe', type: DataType.STRING(25) })
  idPartenaireTypeAssocie: string;

  @Column({ field: 'partenaire_type', type: DataType.STRING(60) })
  partenaireType: string; // 'LABORATOIRE' | 'PRESTATAIRE_SERVICE'

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
