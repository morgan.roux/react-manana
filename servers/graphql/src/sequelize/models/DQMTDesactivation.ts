import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  HasMany,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';

@Table({ tableName: 'DQ_mt_desactivation' })
export class DQMTDesactivation extends Model<DQMTDesactivation, Partial<DQMTDesactivation>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'type', type: DataType.STRING(20) })
  type: string; // FONCTION ou TACHE
  
  @Column({ field: 'type', type: DataType.STRING(25) })
  idTypeAssocie: string; 

  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
