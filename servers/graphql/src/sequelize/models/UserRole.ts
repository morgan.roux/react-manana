import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement } from './Groupement';
import { User } from './User';
import { UserOrigine } from './UserOrigine';
import { Role } from './Role';

@Table({ tableName: 'User_role' })
export class UserRole extends Model<UserRole, Partial<UserRole>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Role)
  @Column({ field: 'id_role', type: DataType.STRING(25) })
  idRole: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  //TODO : Rename to id_user_origine
  @ForeignKey(() => UserOrigine)
  @Column({ field: 'id_user_origin', type: DataType.STRING(25) })
  idUserOrigine: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
