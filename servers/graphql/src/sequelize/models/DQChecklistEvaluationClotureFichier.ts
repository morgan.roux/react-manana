import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { User } from './User';
import { Groupement } from './Groupement';
import { Fichier } from './Fichier';
import { Pharmacie } from './Pharmacie';
import { DQChecklistEvaluationCloture } from './DQChecklistEvaluationCloture';

@Table({ tableName: 'DQ_checklist_evaluation_cloture_fichier' })
export class DQChecklistEvaluationClotureFichier extends Model<DQChecklistEvaluationClotureFichier, Partial<DQChecklistEvaluationClotureFichier>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => DQChecklistEvaluationCloture)
  @Column({ field: 'id_checklist_evaluation_cloture', type: DataType.STRING(25) })
  idChecklistEvaluationCloture: string;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_fichier', type: DataType.STRING(25) })
  idFichier: string;


  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
