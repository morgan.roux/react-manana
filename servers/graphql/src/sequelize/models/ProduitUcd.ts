import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Produit_ucd' })
export class ProduitUcd extends Model<ProduitUcd, Partial<ProduitUcd>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_ucd7', type: DataType.STRING(7) })
  resipUcd7: string;

  @Column({ field: 'ucd7', type: DataType.STRING(7) })
  ucd7: string;

  @Column(DataType.STRING(13))
  ucd13: string;

  @Column(DataType.STRING(30))
  libelle: string;

  @Column({ field: 'libelle_bcb', type: DataType.STRING(100) })
  libelleBcb: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
