import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement, Pharmacie, User } from '.';
import { PRTContact } from './PRTContact';
import { PRTCompteRendu } from './PRTCompteRendu';

@Table({ tableName: 'PRT_compte_rendu_responsable' })
export class PRTCompteRenduResponsable extends Model<
  PRTCompteRenduResponsable,
  Partial<PRTCompteRenduResponsable>
> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => PRTCompteRendu)
  @Column({ field: 'id_compte_rendu', type: DataType.STRING(25) })
  idCompteRendu: string;

  @ForeignKey(() => PRTContact)
  @Column({ field: 'id_contact', type: DataType.STRING(25) })
  idContact: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
