import { Model, Table, Column, DataType, ForeignKey } from 'sequelize-typescript';
import { PRTCompteRendu } from './PRTCompteRendu';
import { PRTConditionCommerciale } from './PRTConditionCommerciale';

@Table({ tableName: 'PRT_compte_rendu_condition_commerciale' })
export class PRTCompteRenduConditionCommerciale extends Model<
  PRTCompteRenduConditionCommerciale,
  Partial<PRTCompteRenduConditionCommerciale>
> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => PRTConditionCommerciale)
  @Column({ field: 'id_condition_commerciale', type: DataType.STRING(25) })
  idCondtionCommerciale: string;

  @ForeignKey(() => PRTCompteRendu)
  @Column({ field: 'id_compte_rendu', type: DataType.STRING(25) })
  idCompteRendu: string;
}
