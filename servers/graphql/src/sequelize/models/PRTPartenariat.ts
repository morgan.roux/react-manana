import { Model, Table, Column, CreatedAt, UpdatedAt, DataType, ForeignKey } from 'sequelize-typescript';
import { Groupement } from '.';
import { Pharmacie } from './Pharmacie';

@Table({ tableName: 'PRT_partenariat' })
export class PRTPartenariat extends Model<PRTPartenariat, Partial<PRTPartenariat>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'date_debut', type: DataType.DATE })
  dateDebut: Date;

  @Column({ field: 'date_fin', type: DataType.DATE })
  dateFin: Date;

  @Column({ field: 'id_type', type: DataType.STRING(25) })
  idType: string;

  @Column({ field: 'id_statut', type: DataType.STRING(25) })
  idStatut: string;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  supprime: boolean;

  @Column({ field: 'id_partenaire_type_associe', type: DataType.STRING(25) })
  idPartenaireTypeAssocie: string;

  @Column({ field: 'partenaire_type', type: DataType.STRING(60) })
  partenaireType: string; // 'LABORATOIRE' | 'PRESTATAIRE_SERVICE'

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string; // Non null : si le laboratoire est un partenaire de la pharmacie, null si il est partenaire du groupement

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
