import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Sel' })
export class Sel extends Model<Sel, Partial<Sel>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ type: DataType.STRING(255) })
  nom: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
