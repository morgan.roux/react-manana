import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { Groupement, Pharmacie, User } from '.';
import { Canal } from './Canal';
import { PRTConditionCommercialeStatut } from './PRTConditionCommercialeStatut';
import { PRTConditionCommercialeType } from './PRTConditionCommercialeType';

@Table({ tableName: 'PRT_condition_commerciale' })
export class PRTConditionCommerciale extends Model<
  PRTConditionCommerciale,
  Partial<PRTConditionCommerciale>
> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'id_partenaire_type_associe', type: DataType.STRING(25) })
  idPartenaireTypeAssocie: string;

  @Column({ field: 'partenaire_type', type: DataType.STRING(60) })
  partenaireType: string;

  @ForeignKey(() => Canal)
  @Column({ field: 'id_canal', type: DataType.STRING(25) })
  idCanal: string;

  @ForeignKey(() => PRTConditionCommercialeType)
  @Column({ field: 'id_type', type: DataType.STRING(25) })
  idType: string;

  @ForeignKey(() => PRTConditionCommercialeStatut)
  @Column({ field: 'id_statut', type: DataType.STRING(25) })
  idStatut: string;

  @Column({ field: 'titre', type: DataType.STRING(255) })
  titre: string;

  @Column({ field: 'date_debut', type: DataType.DATE })
  dateDebut: Date;

  @Column({ field: 'date_fin', type: DataType.DATE })
  dateFin: Date;

  @Column({ field: 'description', type: DataType.TEXT })
  description: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
