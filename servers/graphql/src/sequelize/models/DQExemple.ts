import { Model, Table, Column, CreatedAt, UpdatedAt, DataType, ForeignKey } from 'sequelize-typescript';
import { Groupement } from './Groupement'
import { DQExigence } from './DQExigence'
import { User } from './User'

@Table({ tableName: 'DQ_exemple' })
export class DQExemple extends Model<DQExemple, Partial<DQExemple>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'ordre', type: DataType.INTEGER })
  ordre: number;

  @Column({ field: 'exemple', type: DataType.TEXT })
  exemple: string;

  @ForeignKey(() => DQExigence)
  @Column({ field: 'id_exigence', type: DataType.STRING(25) })
  idExigence: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;
}
