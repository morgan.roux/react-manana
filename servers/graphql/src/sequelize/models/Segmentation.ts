import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Segmentation' })
export class Segmentation extends Model<Segmentation, Partial<Segmentation>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'code', type: DataType.STRING(50), unique: true })
  code: string;

  @Column({ field: 'libelle', type: DataType.STRING(255) })
  libelle: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
