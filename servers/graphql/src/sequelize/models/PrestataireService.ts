import {
  Model,
  Column,
  DataType,
  Table,
  ForeignKey,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';
import { Fichier } from './Fichier';

@Table({ tableName: 'Prestataire_service' })
export class PrestataireService extends Model<PrestataireService, Partial<PrestataireService>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'id_prestataire_service_suite', type: DataType.STRING(25) })
  idPrestataireServiceSuite: string;

  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'is_removed', type: DataType.BOOLEAN })
  supprime: boolean;

  @Column({ field: 'nom', type: DataType.STRING(25) })
  nom: string;

  @Column({ field: 'sortie', type: DataType.SMALLINT })
  sortie: number;

  @ForeignKey(() => Fichier)
  @Column({ field: 'id_photo', type: DataType.STRING(25) })
  idPhoto: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
