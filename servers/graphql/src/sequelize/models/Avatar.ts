import { Model, Table, Column, CreatedAt, UpdatedAt, DataType, ForeignKey } from 'sequelize-typescript';
import { Groupement } from './Groupement';
import { User } from './User'

@Table({ tableName: 'Avatar' })
export class Avatar extends Model<Avatar, Partial<Avatar>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'description', type: DataType.STRING(255) })
  description: string;

  @Column({ field: 'code_sexe', type: DataType.STRING(100) })
  codeSexe: string;

  @Column({ field: 'type', type: DataType.STRING(255) })
  type: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
  updatedBy: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
  createdBy: string;

}
