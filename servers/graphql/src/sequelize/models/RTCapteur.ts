import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
  BelongsTo
} from 'sequelize-typescript';
import { Groupement } from './Groupement';
import { Pharmacie } from './Pharmacie';
import { RTPointAcces } from './RTPointAcces'

@Table({ tableName: 'RT_capteur' })
export class RTCapteur extends Model<RTCapteur, Partial<RTCapteur>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'nom', type: DataType.STRING(50) })
  nom: string;

  @Column({ field: 'numero_serie', type: DataType.STRING(25) })
  numeroSerie: string;

  @ForeignKey(() => RTPointAcces)
  @Column({ field: 'id_point_acces', type: DataType.STRING(25) })
  idPointAcces: string;

  @BelongsTo(() => RTPointAcces, { onDelete: 'CASCADE' })
  pointAcces: RTPointAcces

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
