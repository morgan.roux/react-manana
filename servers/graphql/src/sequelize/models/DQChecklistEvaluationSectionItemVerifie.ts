import { Model, Table, Column, CreatedAt, UpdatedAt, DataType, ForeignKey } from 'sequelize-typescript';
import { User } from './User'
import { Groupement } from './Groupement'
import { DQChecklistEvaluation } from './DQChecklistEvaluation'
import { DQChecklistSectionItem } from './DQChecklistSectionItem'

@Table({ tableName: 'DQ_checklist_evaluation_section_item_verifie' })
export class DQChecklistEvaluationSectionItemVerifie extends Model<DQChecklistEvaluationSectionItemVerifie, Partial<DQChecklistEvaluationSectionItemVerifie>> {
    @Column({ primaryKey: true, type: DataType.STRING(25) })
    id: string;

    @ForeignKey(() => DQChecklistEvaluation)
    @Column({ field: 'id_checklist_evaluation', type: DataType.STRING(25) })
    idChecklistEvaluation: string;


    @ForeignKey(() => DQChecklistSectionItem)
    @Column({ field: 'id_checklist_section_item', type: DataType.STRING(25) })
    idChecklistSectionItem: string;


    @Column({ field: 'code_maj', type: DataType.STRING })
    codeMaj: string;

    @Column({ field: 'date_creation' })
    @CreatedAt
    createdAt: Date;

    @Column({ field: 'date_modification' })
    @UpdatedAt
    updatedAt: Date;

    @ForeignKey(() => Groupement)
    @Column({ field: 'id_groupement', type: DataType.STRING(25) })
    idGroupement: string;

    @ForeignKey(() => User)
    @Column({ field: 'id_user_modification', type: DataType.STRING(25) })
    updatedBy: string;

    @ForeignKey(() => User)
    @Column({ field: 'id_user_creation', type: DataType.STRING(25) })
    createdBy: string;
}
