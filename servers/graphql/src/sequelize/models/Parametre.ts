import { Model, Table, Column, CreatedAt, UpdatedAt, DataType, ForeignKey } from 'sequelize-typescript';
import { ParametreGroupe } from './ParametreGroupe';
import { ParametreType } from './ParametreType';

@Table({ tableName: 'Parameter' })
export class Parametre extends Model<Parametre, Partial<Parametre>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'code', type: DataType.STRING(255) })
  code: string;

  @Column({ field: 'name', type: DataType.STRING(255) })
  nom: string;

  @Column({ field: 'default_value', type: DataType.STRING(255) })
  valeurParDefaut: string;

  @Column({ field: 'category', type: DataType.STRING(255) })
  categorie: string;

  @Column({ field: 'options', type: DataType.TEXT })
  options: string;


  @ForeignKey(() => ParametreGroupe)
  @Column({ field: 'id_groupe_parameter', type: DataType.STRING(25) })
  idParametreGroupe: string;

  @ForeignKey(() => ParametreType)
  @Column({ field: 'id_type_parameter', type: DataType.STRING(25) })
  idParametreType: string;


  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
