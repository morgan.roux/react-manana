import {
  Column,
  DataType,
  Model,
  Table,
  ForeignKey,
  CreatedAt,
  UpdatedAt,
} from 'sequelize-typescript';
import { User } from './User';
import { Pharmacie } from './Pharmacie';
import { Importance, Item, Urgence } from '.';

@Table({ tableName: 'Item_scoring' })
export class ItemScoring extends Model<ItemScoring, Partial<ItemScoring>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user', type: DataType.STRING(25) })
  idUser: string;

  @Column({ field: 'nom_item', type: DataType.STRING(255) })
  nomItem: string;

  @ForeignKey(() => Item)
  @Column({ field: 'id_item', type: DataType.STRING(25) })
  idItem: string;

  @Column({ field: 'id_item_associe', type: DataType.STRING(25) })
  idItemAssocie: string;

  @Column({ field: 'date_echeance', type: DataType.DATE })
  dateEcheance: Date;

  @ForeignKey(() => Urgence)
  @Column({ field: 'id_urgence', type: DataType.STRING(25) })
  idUrgence: string;

  @ForeignKey(() => Importance)
  @Column({ field: 'id_importance', type: DataType.STRING(25) })
  idImportance: string;

  @Column({ field: 'description', type: DataType.TEXT })
  description: string;

  @Column({ field: 'statut', type: DataType.STRING(100) })
  statut: string; // NOUVEAU | EN_COURS | CLOTURE

  @Column({ field: 'ordre_statut', type: DataType.INTEGER })
  ordreStatut: number;

  @Column({ field: 'coefficient_urgence', type: DataType.INTEGER })
  coefficientUrgence: number;

  @Column({ field: 'coefficient_importance', type: DataType.INTEGER })
  coefficientImportance: number;

  @Column({ field: 'coefficient_appetance', type: DataType.INTEGER })
  coefficientAppetance: number;

  @Column({ field: 'nombre_jours_retard', type: DataType.INTEGER })
  nombreJoursRetard: number;

  @Column({ field: 'score', type: DataType.DECIMAL })
  score: number;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
