import { Model, Table, Column, CreatedAt, UpdatedAt, DataType } from 'sequelize-typescript';

@Table({ tableName: 'Famille' })
export class Famille extends Model<Famille, Partial<Famille>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ field: 'resip_code_famille', type: DataType.STRING(5) })
  resipCodeFamille: string;

  @Column({ field: 'code_famille', type: DataType.STRING(5) })
  codeFamille: string;

  @Column({ field: 'libelle_famille', type: DataType.STRING(50) })
  libelleFamille: string;

  @Column({ field: 'code_maj', type: DataType.STRING })
  codeMaj: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
