import {
  Column,
  DataType,
  Model,
  Table,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
} from 'sequelize-typescript';
import { DQMTFonction, DQMTTache, Importance, User, Groupement, Pharmacie } from '.';
import { Urgence } from './Urgence';

@Table({ tableName: 'suivi_appel' })
export class SuiviAppel extends Model<SuiviAppel, Partial<SuiviAppel>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_declarant', type: DataType.STRING(25) })
  idUserDeclarant: string;

  @ForeignKey(() => User)
  @Column({ field: 'id_user_interlocuteur', type: DataType.STRING(25) })
  idUserInterlocuteur: string;

  @ForeignKey(() => DQMTFonction)
  @Column({ field: 'id_fonction', type: DataType.STRING(25) })
  idFonction: string;

  @ForeignKey(() => DQMTTache)
  @Column({ field: 'id_tache', type: DataType.STRING(25) })
  idTache: string;

  @ForeignKey(() => Importance)
  @Column({ field: 'id_importance', type: DataType.STRING(25) })
  idImportance: string;

  @ForeignKey(() => Urgence)
  @Column({ field: 'id_urgence', type: DataType.STRING(25) })
  idUrgence: string;

  @Column({ field: 'commentaire', type: DataType.TEXT })
  commentaire: string;

  @Column({ field: 'status', type: DataType.STRING(25) })
  status: string;

  @Column({ field: 'id_fichiers', type: DataType.ARRAY(DataType.STRING) })
  idFichiers: string[];

  @ForeignKey(() => Groupement)
  @Column({ field: 'id_groupement', type: DataType.STRING(25) })
  idGroupement: string;

  @ForeignKey(() => Pharmacie)
  @Column({ field: 'id_pharmacie', type: DataType.STRING(25) })
  idPharmacie: string;

  @Column({ field: 'date_creation' })
  @CreatedAt
  createdAt: Date;

  @Column({ field: 'date_modification' })
  @UpdatedAt
  updatedAt: Date;
}
