import { executeGraphql } from './federation-reindex';

export const updateItemScoringForItem = async (
  idItemAssocie: string,
  codeItem: string,
): Promise<number> => {
  return executeGraphql(
    `query updateItemScoringForItem($idItemAssocie: String!, $codeItem: String!) {
        updateItemScoringForItem(idItemAssocie:$idItemAssocie, codeItem: $codeItem)
  }
  `,
    { idItemAssocie, codeItem },
  ).then(({ data }) => data && data.updateItemScoringForItem);
};

export const updateItemScoringForUser = async (
  idUser: string,
  codeItem: string,
): Promise<number> => {
  return executeGraphql(
    `query updateItemScoringForUser($idUser: String, $codeItem: String) {
          updateItemScoringForItem(idUser:$idUser, codeItem: $codeItem)
    }
    `,
    { idUser, codeItem },
  ).then(({ data }) => data.updateItemScoringForUser);
};

export const updateItemScoringForAllUser = async (): Promise<number> => {
  return executeGraphql(
    `query updateItemScoringForAllUser {
        updateItemScoringForAllUser
      }
      `,
    {},
  ).then(({ data }) => data.updateItemScoringForAllUser);
};
