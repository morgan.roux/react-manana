import { executeGraphql } from './federation-reindex';

export const updateGroupementGroupeClients = async (idGroupement?: string): Promise<boolean> => {
  return executeGraphql(
    `query updateGroupementGroupeClients($idGroupement: String) {
        updateGroupementGroupeClients(idGroupement:$idItemAssocie)
    }
    `,
    { idGroupement },
  ).then(({ data }) => data && data.updateGroupementGroupeClients);
};
