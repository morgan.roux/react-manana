// https://storage.de.cloud.ovh.net/v1/AUTH_f7c07388720f441f9e6aba2e84bd179f/d4win-prod/avatar.jpeg

export const getFilePublicUrl = (filePath: string): string => {
  if (process.env.STORAGE_PROVIDER === 'OVH_OBJECT_STORAGE') {
    return `https://storage.de.cloud.ovh.net/v1/AUTH_${process.env.OS_TENANT_ID}/${process.env.OS_CONTAINER}/${filePath}`;
  }
  return `https://${process.env.AWS_S3_BUCKET}.s3.${process.env.AWS_S3_REGION}.amazonaws.com/${filePath}`;
};
