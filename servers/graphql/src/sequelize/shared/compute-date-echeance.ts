import moment from 'moment';

export const computeDateEcheanceFromUrgence = (code: 'A' | 'B' | 'C'): string => {
  switch (code) {
    case 'A':
      return moment().toString();
    case 'B':
      return moment()
        .endOf('isoWeek')
        .add(1, 'week')
        .toISOString();
    default:
      return moment()
        .startOf('isoWeek')
        .add(2, 'week')
        .toISOString();
  }
};

export const computeUrgenceCodeFromDateEcheance = (dateEcheance: any): string => {
  const today = moment();
  const dateDebut = moment(dateEcheance);
  const nextSunday = moment()
    .endOf('isoWeek')
    .add(1, 'week');

  if (dateDebut.day() === today.day() || dateDebut.isSameOrBefore(today)) {
    return 'A';
  }
  if (dateDebut > today && dateDebut <= nextSunday) {
    return 'B';
  }
  return 'C';
};
