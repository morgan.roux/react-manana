import axios from 'axios';

export const executeGraphql = async (
  query: string,
  variables: { [key: string]: any },
): Promise<any> => {
  if (!process.env.GATEWAY_FEDERATION_URL) {
    return Promise.reject('Please, add GATEWAY_FEDERATION_URL in environment variable.');
  }

  return axios({
    url: `${process.env.GATEWAY_FEDERATION_URL}/graphql`,
    method: 'post',
    data: {
      variables,
      query,
    },
  }).catch(error => {
    console.error(
      `[executeGraphql] Unexpected error while executing query=${query}, variables=${JSON.stringify(
        variables,
      )} : `,
      error,
    );

    return error;
  });
};

export const federationPublishIndex = async (
  index: string,
  action: 'SAVE' | 'DELETE',
  id: any,
): Promise<boolean> => {
  return executeGraphql(
    `query publishIndex($index: String,$action:String,$id:ID!) {
          publishIndex(index:$index,action:$action,id:$id)
}
`,
    {
      index,
      action,
      id,
    },
  ).then(({ data }) => data.publishIndex);
};
