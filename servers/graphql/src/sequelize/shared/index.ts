import { customAlphabet } from 'nanoid';

const nanoid = customAlphabet('1234567890abcdefghijklmnopqrstuvwxyz', 25);
export const generateId = (): string => {
  return nanoid();
};

export * from './update-item-scoring';
export * from './update-groupement-pharmacies';
export * from './storage-util';
