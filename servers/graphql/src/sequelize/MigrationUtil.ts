import {
  TodoAction,
  TATraitementExecutionChangementStatus,
  TATraitementExecution,
  Item,
  DQStatut,
  DQActionOperationnelle,
  DQFicheAmelioration,
  DQFicheIncident,
  PRTSuiviOperationnel,
  PRTSuiviOperationnelStatut,
} from './models';
import { generateId } from './shared';

let _executionItem: Item = null;
let _ameliorationItem: Item = null;
let _incidentItem: Item = null;
let _actionItem: Item = null; //
let _suiviOperationnel: Item = null;

const getItemExecution = async (): Promise<Item> => {
  if (!_executionItem) {
    _executionItem = await Item.findOne({ where: { code: 'TA_EXECUTION' } });
  }

  return _executionItem;
};
const getItemFicheAmelioration = async (): Promise<Item> => {
  if (!_ameliorationItem) {
    _ameliorationItem = await Item.findOne({ where: { code: 'DQ_AMELIORATION' } });
  }

  return _ameliorationItem;
};
const getItemFicheIncident = async (): Promise<Item> => {
  if (!_incidentItem) {
    _incidentItem = await Item.findOne({ where: { code: 'DQ_INCIDENT' } });
  }

  return _incidentItem;
};
const getItemActionOperationnelle = async (): Promise<Item> => {
  if (!_actionItem) {
    _actionItem = await Item.findOne({ where: { code: 'DQ_ACTION_OPERATIONNELLE' } });
  }

  return _actionItem;
};

const getItemActionSuiviOperationnel = async (): Promise<Item> => {
  if (!_suiviOperationnel) {
    _suiviOperationnel = await Item.findOne({ where: { code: 'SUIVI_OPERATIONNEL' } });
  }
  return _suiviOperationnel;
};

export const changementStatutActionTodoItemAssocie = async (
  id: string,
  status: string,
  userId: string,
  groupementId: string,
) => {
  const action = await TodoAction.findByPk(id);

  if (action.idItemAssocie) {
    const executionItem = await getItemExecution();
    const actionOperationnelleItem = await getItemActionOperationnelle();
    const ficheAmeliorationItem = await getItemFicheAmelioration();
    const ficheIncidentItem = await getItemFicheIncident();
    const suiviOperationnelItem = await getItemActionSuiviOperationnel();

    if (action.idItem === executionItem.id) {
      const traitementExecution = await TATraitementExecution.findByPk(action.idItemAssocie);

      await new TATraitementExecutionChangementStatus({
        id: generateId(),
        idTraitementExecution: traitementExecution.id,
        idGroupement: traitementExecution.idGroupement,
        idPharmacie: traitementExecution.idPharmacie,
        status: status === 'DONE' ? 'CLOTURE' : 'EN_COURS',
        commentaire: '',
        updatedBy: userId,
        createdBy: userId,
      } as any).save();
    } else if (action.idItem === actionOperationnelleItem.id) {
      const actionOperationnelle = await DQActionOperationnelle.findByPk(action.idItemAssocie);
      if (actionOperationnelle) {
        await actionOperationnelle.update({
          idStatut: (
            await DQStatut.findOne({
              where: {
                code: status === 'DONE' ? 'CLOTURE' : 'EN_COURS',
                idGroupement: groupementId,
                type: 'OPERATION',
              },
            })
          )?.id,
        });
      }
    } else if (action.idItem === ficheAmeliorationItem.id) {
      const fiche = await DQFicheAmelioration.findByPk(action.idItemAssocie);
      if (fiche) {
        await fiche.update({
          idStatut: (
            await DQStatut.findOne({
              where: {
                code: status === 'DONE' ? 'CLOTURE' : 'EN_COURS',
                idGroupement: groupementId,
                type: 'AMELIORATION',
              },
            })
          )?.id,
        });
      }
    } else if (action.idItem === ficheIncidentItem.id) {
      const fiche = await DQFicheIncident.findByPk(action.idItemAssocie);
      if (fiche) {
        await fiche.update({
          idStatut: (
            await DQStatut.findOne({
              where: {
                code: status === 'DONE' ? 'CLOTURE' : 'EN_COURS',
                idGroupement: groupementId,
                type: 'INCIDENT',
              },
            })
          )?.id,
        });
      }
    } else if (action.idItem === suiviOperationnelItem.id) {
      const suiviOperationnel = await PRTSuiviOperationnel.findByPk(action.idItemAssocie);
      const cloturerSuiviStatut = await PRTSuiviOperationnelStatut.findOne({
        where: { code: status === 'DONE' ? 'CLOTURE' : 'EN_COURS' },
      });
      if (suiviOperationnel && cloturerSuiviStatut) {
        await (
          await suiviOperationnel.update({
            idStatut: cloturerSuiviStatut.id,
          })
        ).save();
      }
    }
  }
};
