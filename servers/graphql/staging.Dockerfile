FROM node:10-alpine as builder

COPY package.json /graphql/package.json

WORKDIR /graphql

RUN yarn

COPY . /graphql

ENV PRISMA_END_POINT=http://prisma:4466

EXPOSE 4000


ENTRYPOINT ["npm", "run", "start" ]