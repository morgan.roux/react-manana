const fs = require('fs');
const path = require('path');

// an array of filenames to concat
const files = [];
const theDirectory = path.join(__dirname, '../src/elasticsearch/configs');
const outputFile = path.join(__dirname, '../config.json');

fs.readdirSync(theDirectory).forEach(file => {
  // you may want to filter these by extension, etc. to make sure they are JSON files

  let rawdata = fs.readFileSync(theDirectory + '/' + file);
  let data = JSON.parse(rawdata);

  files.push(data);
});

const printableJson = JSON.stringify(files, null, 2);

fs.writeFile(outputFile, printableJson, function(err) {
  if (err) throw 'Merge config elasticsearch save error: ' + err;
  console.log('Merge config elasticsearch saved');
});
