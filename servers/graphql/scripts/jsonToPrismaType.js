const fs = require('fs');
const db_schema = require('../db_schema.json');

const path_script = './scripts';
const MAPPING_TYPE = {
  int: 'Int',
  bit: 'Boolean',
  tinyint: 'Int',
  smallint: 'Int',
  bigint: 'Int',
  money: 'Float',
  float: 'Float',
  real: 'Float',
  char: 'String',
  varchar: 'String',
  text: 'String',
  nchar: 'String',
  nvarchar: 'String',
  ntext: 'String',
  uniqueidentifier: 'String',
  date: 'DateTime',
  datetimeoffset: 'DateTime',
  datetime2: 'DateTime',
  smalldatetime: 'DateTime',
  datetime: 'DateTime',
  time: 'DateTime',
};

/**
 * input :
 *
 * [
 *  {"TABLE_NAME":"foo", COLUMN_NAME:"col1"} ,
 *  {"TABLE_NAME":"foo", COLOMN_NAME:"col2"} ,
 *
 *  {"TABLE_NAME":"bar", COLUMN_NAME:"col1"} ,
 *  {"TABLE_NAME":"bar", COLOMN_NAME:"col2"}
 * ]
 *
 * output :
 *
 * {
 *  "foo":[{"TABLE_NAME":"foo", COLUMN_NAME:"col1"} ,{"TABLE_NAME":"foo", COLOMN_NAME:"col2"}]
 *   "bar" : [{"TABLE_NAME":"bar", COLUMN_NAME:"col1"},{"TABLE_NAME":"bar", COLOMN_NAME:"col2"} ]
 *
 * }
 *
 * schema  parsed JSON
 */
const groupByTableName = schema =>
  schema.reduce((groupedByTableName, columnSchema) => {
    return groupedByTableName[columnSchema.TABLE_NAME]
      ? {
          ...groupedByTableName,
          [columnSchema.TABLE_NAME]: [...groupedByTableName[columnSchema.TABLE_NAME], columnSchema],
        }
      : { ...groupedByTableName, [columnSchema.TABLE_NAME]: [columnSchema] };
  }, {});

fs.mkdir('./scripts/prisma/', { recursive: true }, err => {
  if (err) {
    console.log(`Error creating directory: ${err}`);
  } else {
    const groupedSchema = groupByTableName(db_schema);
    for (const tableName in groupedSchema) {
      const name = tableName.toLowerCase();
      createPrismaType(name, groupedSchema[tableName]);
    }
  }
});

function createPrismaType(table_name, data) {
  const stream = fs.createWriteStream(`${path_script}/prisma/${table_name}.prisma`);
  let model = table_name.charAt(0).toUpperCase() + table_name.slice(1);
  model =
    snakeToCamel(model) !== model
      ? `${snakeToCamel(model)} @db(name: "${model}")`
      : snakeToCamel(model);
  stream.once('open', function(fd) {
    stream.write(`type ${model} { \n`);
    data.forEach(element => {
      const column = element.COLUMN_NAME.toLowerCase();
      const nullable = element.IS_NULLABLE === 'NO' ? '!' : '';
      let type = MAPPING_TYPE[element.DATA_TYPE];
      const db_column = snakeToCamel(column) !== column ? `@db(name: "${column}")` : '';
      stream.write(`\t ${snakeToCamel(column)}: ${type}${nullable} ${db_column} \n`);
    });
    stream.write('}');
    console.log(`==> ${table_name}.prisma successfully created`);
    stream.end();
  });
}

function snakeToCamel(str) {
  return str.replace(/([-_]\w)/g, g => g[1].toUpperCase());
}
