#!/bin/sh

./wait-for.sh redis:6379 -t 3000

./wait-for.sh elasticsearch:9200 -t 3000

./wait-for.sh postgres:5432 -t 3000

host_ip=$(eval /sbin/ip route | awk '/default/ { print $3 }')
export GATEWAY_FEDERATION_URL="http://${host_ip}:5000"

npm run merge:config

npm run prisma:deploy:dev

npm run start