FROM node:10-alpine

COPY package.json /graphql/package.json

WORKDIR /graphql

RUN yarn install

COPY . /graphql

ENTRYPOINT ["./startup_test.sh"]