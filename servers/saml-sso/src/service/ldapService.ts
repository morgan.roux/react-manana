import SimpleLDAP from 'simple-ldap-search';

export interface LdapUser {
	cn: string;
	sn: string;
	uid: string;
	mail: string;
	objectClass: string;
	userPassword: String;
}
export interface ldapCredential {
	url: string;
	user: string;
	pass: string;
	base: String;
}

export class LdapService {
	config: any;
	user: string;
	pass: string;
	url: string;
	attributes = [
		'dn', // Distinguished Name
		'sn', // Last name
		'givenname', // First Name
		'cn', // Full  Name
		'mail', // mail
		'userAccountControl', // 514 disable
		'telephonenumber', // telephone
		'co', // Country
		'userPrincipalName', // Logon Name
		'streetAddress', // Street
		'homePhone', // Home phone
		'mobile', // mobile phone
		'facsimileTelephoneNumber', // fax
		'postalCode', // postal code
		'l', // city
		'memberOf' // Add to Groups
	];

	// constructor
	constructor({ url, user, pass, base }: ldapCredential) {
		// this.ldapClient = ldapjs.createClient({
		//     url: url
		//   });
		this.url = url;
		this.user = user;
		this.pass = pass;
		// for ldapSearch
		this.config = {
			url,
			base,
			dn: user,
			password: pass
		};
		// this.
	}

	// get all user
	_getLdapUsers = async (filter) => {
		const ldapsearch = new SimpleLDAP(this.config);
		const users = await ldapsearch.search(filter, this.attributes);
		console.log(users);
		ldapsearch.destroy();
		return users;
	};

	getAllLdapUsers = async (ldapType: string) => {
		if (ldapType === 'WINDOWS_LDAP') return this._getLdapUsers('(&(objectClass=user))');
		return this._getLdapUsers('(&(objectClass=person))');
	};

	getActiveUser = async (ldapType: string) => {
		if (ldapType === 'WINDOWS_LDAP') {
			return this._getLdapUsers('(&(objectClass=user)(!(userAccountControl:1.2.840.113556.1.4.803:=2)))');
		}
		return this._getLdapUsers('(&(objectClass=person))');
	};

	getDisabledUser = async (ldapType: string) => {
		if (ldapType === 'WINDOWS_LDAP') {
			return this._getLdapUsers('(&(objectClass=user)(userAccountControl:1.2.840.113556.1.4.803:=2))');
		}
		return null;
	};

	getOneDisabledUser = async (ldapType: string, mail: string) => {
		if (ldapType === 'WINDOWS_LDAP') {
			return this._getLdapUsers(
				`(&(objectClass=user)(mail=${mail})(userAccountControl:1.2.840.113556.1.4.803:=2))`
			);
		}
		return this._getLdapUsers(`(&(mail=${mail}))`);
	};

	getOneActiveUser = async (ldapType: string, mail: string) => {
		if (ldapType === 'WINDOWS_LDAP') {
			return this._getLdapUsers(
				`(&(objectClass=user)(mail=${mail})(!(userAccountControl:1.2.840.113556.1.4.803:=2)))`
			);
		}
		return this._getLdapUsers(`(&(mail=${mail}))`);
	};

	// get one user
	getOneLdapUser = async (ldapType: string, email: string) => {
		const ldapsearch = new SimpleLDAP(this.config);
		console.log('Search one AD user ', email);
		// setup a filter and attributes for your LDAP query
		let filter;
		if (ldapType === 'WINDOWS_LDAP') filter = `(&(objectClass=user)(mail=${email}))`;
		filter = `(&(objectClass=person)(mail=${email}))`;

		const user = await ldapsearch.search(filter, this.attributes);
		console.log(user);
		ldapsearch.destroy();
		return user[0];
	};
}
