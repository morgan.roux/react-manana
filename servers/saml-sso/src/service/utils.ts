var clientFtp = require('ftp');
const axios = require('axios');

export const formateDate = (date: Date, separator: String) => {
	var dd = date.getDate();
	var mm = date.getMonth() + 1;
	var yyyy = date.getFullYear();

	let daystring = dd.toString();
	let monthstring = mm.toString();
	if (dd < 10) daystring = '0' + dd;

	if (mm < 10) monthstring = '0' + mm;

	return `${yyyy}${separator}${monthstring}${separator}${daystring}`;
};

export const permut = (a: number, b: number): { begin: number; end: number } => {
	if (a <= b) {
		return {
			begin: a,
			end: b
		};
	} else {
		return {
			begin: b,
			end: a
		};
	}
};

export const getPublicIp = async () => {
	if(process.env.PUBLIC_IP){
		return process.env.PUBLIC_IP
	}
	
	var options = {
		method: `get`,
		url: `https://ipinfo.io/ip`
	};

	const response = await axios(options)
		.then((result) => {
			return result.data;
		})
		.catch((err) => {
			console.log('error ', err);
			throw new Error(err);
		});

	return response;
};

export const getFtpFileToken = (
	loginToFind: string,
	{ host, port, username, password, ftpFilePath, ftpFileName },
	callback
) => {
	var c = new clientFtp();
	c.on('ready', function() {
		console.log(`./${ftpFilePath}/${ftpFileName}`);
		c.get(`./${ftpFilePath}/${ftpFileName}`, function(err, stream) {
			if (err) {
				console.log('there is an error FTP...', err);
				callback(new Error(err));
				return false;
			}
			stream.once('close', function() {
				c.end();
			});
			//stream.pipe(fs.createWriteStream('JETON_V2_Pharmavie.txt'));
			let users = [];
			stream.on('data', async function(data) {
				var chunk = data.toString();
				var spliteddata = chunk.split('\n');
				console.log('users count ', spliteddata.length);
				await spliteddata.forEach(async (element) => {
					const user = element.split('|');

					const startDate = user[6];
					const endtDate = user[7];

					users.push({
						login: user[0],
						cip: user[1],
						adherant_code: user[2],
						No_finess: user[3],
						Code_qualipharma: user[4],
						token: user[5],
						Date_debut: new Date(startDate && startDate.replace(/-/g, '/')),
						Date_fin: new Date(endtDate && endtDate.replace(/-/g, '/'))
					});
				});
				console.log('users into file', users);
				const found = await users.find((user) => user.login.trim() === loginToFind.trim());
				console.log('users found into file', found);
				callback(found);
				c.end();
				return true;
			});
		});
	});
	// connect to localhost:21 as anonymous
	c.connect({
		host: host,
		port: port,
		user: username,
		password: password
	});

	c.on('error', function(err, stream) {
		console.log('error ftp, ', err);
		callback(new Error(err));
		return false;
	});
};
