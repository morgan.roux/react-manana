import { GraphQLClient } from 'graphql-request';
import gql from 'graphql-tag';
const axios = require('axios');

export interface DataToSend {
  id: string;
  username: string;
  email: string;
  firstname: string;
  lastname: string;
  company: string;
  phone: string;
  cip: string;
}

export const initialData: DataToSend = {
  id: '',
  username: '',
  email: '',
  firstname: '',
  lastname: '',
  company: '',
  phone: '',
  cip: '',
};

export const userQuery: string = `
query UserByToken ($token: String!){
  userByToken(token: $token){
    id
    email
    phoneNumber
    userName
    status
    role{
      id
      nom
    }
    groupement{
      id
      nom
    }
  }
}`;

export const ActualUser: string = `
query actualUser{
  me {
    id
    email
    phoneNumber
    userName
    status
    role {
      id
      nom
    }
    pharmacie{
      cip
      nom
    }
    externalMapping {
      id
      idExternaluser
      idClient
      password
    }
    groupement{
      id
      nom
    }
  }
}`;

export const ApplicationDetail: string = `
query GetApplication ($id: ID!){
  ssoApplication(id: $id){
    id
    nom
    url
    ssoType
    ssoTypeid
    mappings{
      id
      nom
      value
    }
  }
}`;

export const SamlLogin: string = `
query SamlLogin ($id: ID!){
  samlLogin(id: $id){
    id
    idpX509Certificate
    spX509Certificate
    consumerUrl
    Sendingtype
  }
}`;
export const CryptoMd5: string = `
query CryptoMd5 ($id: ID!){
  cryptoMd5(id: $id){
    id
    beginGet
    endGet
    hexadecimal
    Sendingtype
    haveExternalUserMapping
    requestUrl
  }
}`;

export const CryptoAes256: string = `
query CryptoAes256 ($id: ID!){
  cryptoAes256(id: $id){
    id
    key
    hexadecimal
    Sendingtype
    requestUrl
  }
}`;

export const CryptoOther: string = `
query CryptoOther ($id: ID!){
  cryptoOther(id: $id){
    id
    function
    Sendingtype
    requestUrl
  }
}`;

export const ActiveDirectoryGroupement: string = `
query ActiveDirectoryGroupement ($idgroupement: ID!){
  activedirectoryGroupement(idgroupement: $idgroupement){
    id
    url
    user
    password
    base
    ldapType
    groupement{
      id
      nom
    }
  }
}`;

export const TokenAuthentification: string = `
query TokenAuthentification ($id: ID!){
  tokenAuthentification(id: $id){
    id
    reqTokenUrl
    reqUserUrl
    Sendingtype
  }
}`;

export const TokenFtpAuthentification: string = `
query TokenFtpAuthentification ($id: ID!){
  tokenFtpAuthentification(id: $id){
    id
    requestUrl
    Sendingtype
    ftpFilePath
    ftpFileName
    ftpid{
      id
      host
      port
      user
      password
    }
  }
}`;

export const FtpConnect: string = `
query FtpConnect ($id: ID!){
  ftpConnect(id: $id){
    id
    host
    port
    user
    password
  }
}`;

export const usersQuery: string = `
query Users {
  users {
    id
    email
    phoneNumber
    userName
    status
    role {
      id
      nom
    }
    pharmacie{
      cip
      nom
    }
    externalMapping {
      id
      idExternaluser
      idClient
      password
    }
    groupement{
      id
      nom
    }
  }
}`;

export const userQueryById: string = `
query UserById ($id: ID!){
  user(id: $id){
    id
    email
    phoneNumber
    userName
    status
    role{
      id
      nom
    }
    groupement{
      id
      nom
    }
  }
}`;

export const userSearchById: string = `
query UserById( $query: JSON){
  search(
    type: ["personnel", "titulaire", "ppersonnel"]
    query: $query
  ) {
    total
    data {
      ... on Personnel {
        type
        id
        nom
        prenom
        civilite
        user {
          id
          email
          status
          phoneNumber
          role {
            code
            nom
          }
        }
      }
      ... on Titulaire {
        type
        nom
        prenom
        civilite
        user {
          id
          email
          status
          phoneNumber
          role {
            code
            nom
          }
        }
      }
      ... on Ppersonnel {
        type
        id
        nom
        prenom
        civilite
        user {
          id
          email
          status
          phoneNumber
          role {
            code
            nom
          }
        }
      }
    }
  }
}
`;

export const UsersGroupement: string = `
  query UsersGroupement($idGroupement: ID!, $skip: Int, $take: Int) {
    search(
      type: ["personnel", "titulaire", "ppersonnel"]
      skip: $skip
      take: $take
      query: {
        query: {
          bool: {
            filter: [{ term: { idGroupement: $idGroupement } }]
            must: { exists: { field: "user" } }
          }
        }
      }
    ) {
      total
      data {
        ... on Personnel {
          type
          id
          nom
          prenom
          civilite
          user {
            id
            email
            status
            phoneNumber
            role {
              code
              nom
            }
          }
        }
        ... on Titulaire {
          type
          nom
          prenom
          civilite
          user {
            id
            email
            status
            phoneNumber
            role {
              code
              nom
            }
          }
        }
        ... on Ppersonnel {
          type
          id
          nom
          prenom
          civilite
          user {
            id
            email
            status
            phoneNumber
            role {
              code
              nom
            }
          }
        }
      }
    }
  }
`;

export const ssologinMutation: string = `
mutation SsoLogin ($email: String!, $issuer: String, $sessionIndex: String, $nameIDFormat: String, $username: String) {
  ssologin(email: $email, issuer: $issuer, sessionIndex: $sessionIndex, nameIDFormat: $nameIDFormat, username: $username) { accessToken }
}`;

export const helloIdSsoByGroupId: string = `
query HelloIdSsoByGroup ($idgroupement: ID!) {
  helloIdSsoByGroupementId(idgroupement: $idgroupement) {
      id
      helloIdUrl
      helloIDConsumerUrl
      x509Certificate
      apiKey
      apiPass
      groupement{
        nom
      }
   }
}`;

export const CreatePersonnel: string = `
mutation CreateUpdatePersonnel (
  $codeService: Int
  $sortie: Int
  $dateSortie: DateTime
  $civilite: String
  $nom: String
  $prenom: String
  $sexe: Sexe
  $adresse1: String
  $adresse2: String
  $telBureau: String
  $telDomicile: String
  $telMobProf: String
  $telMobPerso: String
  $faxBureau: String
  $faxDomicile: String
  $cp: String
  $ville: String
  $mailProf: String
  $mailPerso: String
  $commentaire: String
  $idGroupement: String) {
    createUpdatePersonnel(
      codeService: $codeService
      sortie: $sortie
      dateSortie: $dateSortie
      civilite: $civilite
      nom:  $nom
      prenom: $prenom
      sexe: $sexe
      adresse1: $adresse1
      adresse2: $adresse2
      telBureau: $telBureau
      telDomicile: $telDomicile
      telMobProf: $telMobProf
      telMobPerso: $telMobPerso
      faxBureau: $faxBureau
      faxDomicile: $faxDomicile
      cp: $cp
      ville: $ville
      mailProf: $mailProf
      mailPerso: $mailPerso
      commentaire: $commentaire
      idGroupement: $idGroupement) {
        id
        type
        respHierarch
        sortie
        civilite
        nom
        prenom
        adresse1
        adresse2
        telBureau
        telDomicile
        telMobProf
        telMobPerso
        faxBureau
        faxDomicile
        cp
        ville
        mailProf
        mailPerso
        commentaire
        user {
          id
          email
          status
        }
        role {
          code
          nom
        }
      }
}`;

export const CreateUserGroupement: string = `
mutation createUserGroupement (
  $idGroupement: ID!
  $id: ID!
  $email: String!
  $role: String!
  $userId: ID,
  $status: String) {
    createUserGroupement(
      idGroupement: $idGroupement
      id: $id
      email: $email
      role: $role
      userId: $userId
      status: $status) {
        type
        id
        group
        societe
        respHierarch
        sortie
        civilite
        nom
        prenom
        login
        password
        adresse1
        adresse2
        telBureau
        telDomicile
        telMobProf
        telMobPerso
        faxBureau
        faxDomicile
        cp
        ville
        mailProf
        mailPerso
        commentaire
        user {
          id
          email
          status
        }
        role {
          code
          nom
        }
    }
}`;

export const UpdateUserStatus: string = `
mutation UpdateUserStatus ($id: ID!, $status: String!) {
  updateUserStatus(id: $id, status: $status) {
    id
    email
    status
  }
}`;

export const CreateActiveDirectoryUser: string = `
mutation CreateActiveDirectoryUser ($activeDirectoryId: String, $userid: String) {
  createActiveDirectoryUser(activeDirectoryId: $activeDirectoryId, userid: $userid) {
    id
    activeDirectory{
      id
      url
      user
      password
      base
      ldapType
    }
    user{
      id
      email
      status
    }
  }
}`;

export const ActiveDirectoryUser: string = `
query ActiveDirectoryUser ($activeDirectoryId: String) {
  activeDirectoryUser(activeDirectoryId: $activeDirectoryId){
    id
    activeDirectory{
      id
      url
      user
      password
      base
      ldapType
    }
    user{
      id
      email
      status
    }
  }
}`;

export const CheckExistingUserMail: string = `
query CheckExistingUserMail ($email: String) {
  checkExistingUserMail(email: $email){
    id
    email
    status
    userName
    phoneNumber
    emailConfirmed
  }
}`;

export const Activedirectories: string = `
query Activedirectories {
  activedirectories {
    id
    url
    user
    password
    base
    ldapType
    groupement{
      id
      nom
    }
  }
}`;

export const ParameterGroupementByCode: string = `
query ParameterGroupementByCode($idGroupement: ID!, $code: String){
  parameterGroupementByCode(idGroupement: $idGroupement, code: $code){
    value
    type
  }
}`;

export const Groupements: string = `
query Groupements{
  groupements{
    id
    nom
    adresse1
    adresse2
    cp
    ville
    pays
    telBureau
    telMobile
  }
}`;

export const Groupement: string = `
query Groupement($id: ID!){
  groupement(id: $id){
    id
    nom
    adresse1
    adresse2
    cp
    ville
    pays
    telBureau
    telMobile
  }
}`;

export class ApiService {
  apiUrl: string;
  constructor(_apiUrl: string) {
    this.apiUrl = _apiUrl;
  }
  async getUserData(token: string, query: string) {
    const graphQLClient = new GraphQLClient(this.apiUrl);
    const user = await graphQLClient.request(query, { token });
    return user;
  }

  async getUserDataByid(id: string, query: string) {
    const graphQLClient = new GraphQLClient(this.apiUrl);
    const user = await graphQLClient.request(query, { id });
    return user;
  }

  ssoLogin = async (
    email: string,
    issuer: string,
    sessionIndex: string,
    nameIDFormat: string,
    username: string,
    query: string,
  ) => {
    const graphQLClient = new GraphQLClient(this.apiUrl);
    const token = await graphQLClient.request(query, {
      email,
      issuer,
      sessionIndex,
      nameIDFormat,
      username,
    });
    return token;
  };

  helloIdSsoDetailsByGroup = async (query: string, idgroupement: String) => {
    const graphQLClient = new GraphQLClient(this.apiUrl);
    return graphQLClient.request(query, { idgroupement });
  };

  queryNotHaveparams = async (query: string) => {
    const graphQLClient = new GraphQLClient(this.apiUrl);
    return graphQLClient.request(query);
  };

  application = async (query: string, id: string) => {
    const graphQLClient = new GraphQLClient(this.apiUrl);
    return graphQLClient.request(query, { id });
  };

  actualuser = async (query: string, token: string) => {
    console.log('token ', token);
    const graphQLClient = new GraphQLClient(this.apiUrl, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return graphQLClient.request(query);
  };

  queries = async (query, _args) => {
    const graphQLClient = new GraphQLClient(this.apiUrl);
    const result = await graphQLClient.request(query, { ..._args });
    return result;
  };

  querySearch = async (query, { iduser, idGroupement }) => {
    const querySearchVariable: any = {
      query: {
        bool: {
          must: [{ term: { 'user.id': iduser } }, { term: { idGroupement } }],
        },
      },
    };

    console.log('querySearchVariable ', querySearchVariable);
    console.log('querySearchVariable ', querySearchVariable.query.bool.must);

    const options = {
      method: `post`,
      url: this.apiUrl,
      data: {
        query,
        variables: {
          query: querySearchVariable,
        },
      },
    };

    const response = await axios(options)
      .then(result => {
        return result.data;
      })
      .catch(err => {
        console.log(`error ${err}`);
        throw new Error(err);
      });

    return response;
  };
}
