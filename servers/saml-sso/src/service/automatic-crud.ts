/***********
 *
 * IMPORT STATEMENT
 *
 ***********/
import moment = require('moment');
import {
  ApiService,
  Activedirectories,
  ActiveDirectoryUser,
  UpdateUserStatus,
  CreatePersonnel,
  CheckExistingUserMail,
  CreateUserGroupement,
  CreateActiveDirectoryUser,
  userQueryById,
  helloIdSsoByGroupId,
  ActiveDirectoryGroupement,
  UsersGroupement,
  ParameterGroupementByCode,
  Groupements,
} from './ApiService';
import { apiUrl } from '../config/config';
import { ldapCredential, LdapService } from './ldapService';
import {
  CreateHelloIdUser,
  getHelloIdGroup,
  linkToGroup,
  createGroup,
  GetOneHelloidUSer,
  UpdateHelloIdUser,
  DeleteHelloIdUser,
  userGroups,
  unlinkToGroup,
} from './helloID';
const schedule = require('node-schedule');

// AUTOMATIC CRUD

// const getFirstTime = () => {
// 	const currentTime = new Date();
// 	const hourMinute = moment(currentTime).format('ss');
// 	return (60 - parseInt(hourMinute)) * 1000;
// };

// let interval = getFirstTime();
// let first = true;

export interface ISchedule {
  id: string;
  schedulTask: any;
}

const groupementHelloIDSchedule: ISchedule[] = [];
const groupementADSchedule: ISchedule[] = [];

const getTime = () => {
  const currentTime = new Date();
  const hourMinute = moment(currentTime).format('hh:mm');
  return hourMinute;
};

export const InitSchedule = () => {
  const service = new ApiService(apiUrl);
  service.queryNotHaveparams(Groupements).then((result: any) => {
    if (result && result.groupements) {
      result.groupements.map(async groupement => {
        // HelloID
        jobAD(service, groupement.id, false);
        jobHelloID(service, groupement.id, false);
      });
    }
  });
};

export const UpdateOrAddSchedule = async (groupementID: string, code: string, value: string) => {
  // Active Directory
  const service = new ApiService(apiUrl);
  if (value) {
    if (code.trim() === '0049' || code.trim() === '0053') {
      const findJob = groupementADSchedule.find(job => job.id.trim() === groupementID.trim());
      console.log('Active Directory job found ', findJob);
      if (findJob) {
        if (code.trim() === '0049') {
          if (value && value.trim() === 'false') {
            console.log('stop job 0049');
            findJob.schedulTask.cancel();
          } else {
            console.log('reschedule job 0049');
            jobAD(service, groupementID, true);
          }
        }
        if (code.trim() === '0053') {
          const parameterHelloID: any = await service.queries(ParameterGroupementByCode, {
            code: '0046',
            idGroupement: groupementID.trim(),
          });
          if (
            parameterHelloID &&
            parameterHelloID.parameterGroupementByCode &&
            parameterHelloID.parameterGroupementByCode.value &&
            parameterHelloID.parameterGroupementByCode.value == 'true'
          ) {
            console.log('reschedule job 0053');
            const parameterHour = value.split(':');
            const rescheduled = findJob.schedulTask.reschedule(
              { hour: parseInt(parameterHour[0], 10), minute: parseInt(parameterHour[1], 10) },
              () => CreateActiveDirectoryGroupement(groupementID),
            );
            console.log('rescheduled ', rescheduled);
            const nextInvocation = findJob.schedulTask.nextInvocation();
            console.log('next invocation ', nextInvocation);
          }
        }
      } else {
        console.log('create job active directory');
        jobAD(service, groupementID, false);
      }
    }

    // HelloID
    if (code.trim() === '0046' || code.trim() === '0052') {
      const findJob = groupementHelloIDSchedule.find(job => job.id.trim() === groupementID.trim());
      console.log('HelloID job found ', findJob);
      if (findJob) {
        if (code.trim() === '0046') {
          if (value.trim() === 'false') {
            console.log('stop job 0046');
            findJob.schedulTask.cancel();
          } else {
            console.log('reschedule job 0046');
            jobHelloID(service, groupementID, true);
          }
        }
        if (code.trim() === '0052') {
          const parameterHelloID: any = await service.queries(ParameterGroupementByCode, {
            code: '0046',
            idGroupement: groupementID.trim(),
          });
          if (
            parameterHelloID &&
            parameterHelloID.parameterGroupementByCode &&
            parameterHelloID.parameterGroupementByCode.value &&
            parameterHelloID.parameterGroupementByCode.value == 'true'
          ) {
            console.log('reschedule job 0052', value);
            const parameterHour = value.split(':');

            const rescheduled = findJob.schedulTask.reschedule(
              { hour: parseInt(parameterHour[0], 10), minute: parseInt(parameterHour[1], 10) },
              () => CreateActiveDirectoryGroupement(groupementID),
            );
            console.log('rescheduled ', rescheduled);
            const nextInvocation = findJob.schedulTask.nextInvocation();
            console.log('next invocation ', nextInvocation);
          }
        }
      } else {
        console.log('create job helloid');
        jobHelloID(service, groupementID, false);
      }
    }
  }
};

export const automaticCrud = () => {
  // if (first) {
  // 	interval = 60000;
  // 	clearInterval(task);
  // 	task = setInterval(automaticCrud, interval);
  // 	first = false;
  // }
  const service = new ApiService(apiUrl);
  service.queryNotHaveparams(Groupements).then((result: any) => {
    if (result && result.groupements) {
      result.groupements.map(async groupement => {
        console.log('groupement ', groupement);
        getParameterAndCreateUSersAD(service, groupement.id);
        getParameterAndCreateUSersHelloID(service, groupement.id);
      });
    }
  });
};

const jobAD = async (service: ApiService, idGroupement: string, reschedule: boolean) => {
  // const helloidParam = useValueParameterAsBoolean('0046');
  // const activeDirectoryParam = useValueParameterAsBoolean('0049');
  // const ftpParam = useValueParameterAsBoolean('0050');
  // const applicationsParam = useValueParameterAsBoolean('0051');

  const parameterAD: any = await service.queries(ParameterGroupementByCode, {
    code: '0049',
    idGroupement,
  });
  if (
    parameterAD &&
    parameterAD.parameterGroupementByCode &&
    parameterAD.parameterGroupementByCode.value &&
    parameterAD.parameterGroupementByCode.value == 'true'
  ) {
    const parameterADHour: any = await service.queries(ParameterGroupementByCode, {
      code: '0053',
      idGroupement,
    });

    // configure schedule
    const parameterHour =
      parameterADHour.parameterGroupementByCode &&
      parameterADHour.parameterGroupementByCode.value &&
      parameterADHour.parameterGroupementByCode.value;
    const parameterHourSplited = parameterHour && parameterHour.split(':');
    if (parameterHourSplited) {
      if (reschedule) {
        const findJob = groupementADSchedule.find(job => job.id.trim() === idGroupement.trim());
        if (findJob) {
          const rescheduled = findJob.schedulTask.reschedule(
            {
              hour: parseInt(parameterHourSplited[0], 10),
              minute: parseInt(parameterHourSplited[1], 10),
            },
            () => CreateActiveDirectoryGroupement(idGroupement),
          );

          console.log('rescheduled ', rescheduled);
          console.log('next invocation ', findJob.schedulTask.nextInvocation());
        }
      } else {
        const scheduleJob = schedule.scheduleJob(
          idGroupement,
          {
            hour: parseInt(parameterHourSplited[0], 10),
            minute: parseInt(parameterHourSplited[1], 10),
          },
          () => CreateActiveDirectoryGroupement(idGroupement),
        );
        groupementADSchedule.push({
          id: idGroupement,
          schedulTask: scheduleJob,
        });

        console.log('next invocation ', scheduleJob.nextInvocation());
      }
    }
  }
};

const jobHelloID = async (service: ApiService, idGroupement: string, reschedule: boolean) => {
  // const helloidParam = useValueParameterAsBoolean('0046');
  // const activeDirectoryParam = useValueParameterAsBoolean('0049');
  // const ftpParam = useValueParameterAsBoolean('0050');
  // const applicationsParam = useValueParameterAsBoolean('0051');

  const parameterHelloID: any = await service.queries(ParameterGroupementByCode, {
    code: '0046',
    idGroupement,
  });
  if (
    parameterHelloID &&
    parameterHelloID.parameterGroupementByCode &&
    parameterHelloID.parameterGroupementByCode.value &&
    parameterHelloID.parameterGroupementByCode.value == 'true'
  ) {
    const parameterHelloIDHour: any = await service.queries(ParameterGroupementByCode, {
      code: '0052',
      idGroupement,
    });
    const parameterHour =
      parameterHelloIDHour.parameterGroupementByCode &&
      parameterHelloIDHour.parameterGroupementByCode.value &&
      parameterHelloIDHour.parameterGroupementByCode.value;

    console.log('parameterHour ', parameterHour);

    const parameterHourSplited = parameterHour && parameterHour.split(':');

    // configure schedule
    if (parameterHourSplited) {
      if (reschedule) {
        const findJob = groupementHelloIDSchedule.find(
          job => job.id.trim() === idGroupement.trim(),
        );
        if (findJob) {
          const rescheduled = findJob.schedulTask.reschedule(
            {
              hour: parseInt(parameterHourSplited[0], 10),
              minute: parseInt(parameterHourSplited[1], 10),
            },
            () => createUpdateAllUsersToHelloid(idGroupement),
          );

          console.log('rescheduled ', rescheduled);
          console.log('next invocation ', findJob.schedulTask.nextInvocation());
        }
      } else {
        const scheduleJob = schedule.scheduleJob(
          idGroupement,
          { hour: parameterHourSplited[0], minute: parameterHourSplited[1] },
          () => createUpdateAllUsersToHelloid(idGroupement),
        );
        groupementHelloIDSchedule.push({
          id: idGroupement,
          schedulTask: scheduleJob,
        });

        console.log('next invocation ', scheduleJob.nextInvocation());
      }
    }
  }
};

// let task = setInterval(automaticCrud, interval);

const getParameterAndCreateUSersAD = async (service: ApiService, idGroupement: string) => {
  // const helloidParam = useValueParameterAsBoolean('0046');
  // const activeDirectoryParam = useValueParameterAsBoolean('0049');
  // const ftpParam = useValueParameterAsBoolean('0050');
  // const applicationsParam = useValueParameterAsBoolean('0051');

  const parameterAD: any = await service.queries(ParameterGroupementByCode, {
    code: '0049',
    idGroupement,
  });
  if (
    parameterAD &&
    parameterAD.parameterGroupementByCode &&
    parameterAD.parameterGroupementByCode.value &&
    parameterAD.parameterGroupementByCode.value == 'true'
  ) {
    const parameterADHour: any = await service.queries(ParameterGroupementByCode, {
      code: '0053',
      idGroupement,
    });
    const currentTime = getTime();
    console.log('currentTime ', currentTime);
    // if hour and minute CRUD
    if (
      (
        parameterADHour &&
        parameterADHour.parameterGroupementByCode &&
        parameterADHour.parameterGroupementByCode.value &&
        parameterADHour.parameterGroupementByCode.value
      )
        .trim()
        .toLowerCase() === currentTime.trim().toLocaleLowerCase()
    ) {
      CreateActiveDirectoryGroupement(idGroupement);
    }
  }
};

const getParameterAndCreateUSersHelloID = async (service: ApiService, idGroupement: string) => {
  // const helloidParam = useValueParameterAsBoolean('0046');
  // const activeDirectoryParam = useValueParameterAsBoolean('0049');
  // const ftpParam = useValueParameterAsBoolean('0050');
  // const applicationsParam = useValueParameterAsBoolean('0051');

  const parameterHelloID: any = await service.queries(ParameterGroupementByCode, {
    code: '0046',
    idGroupement,
  });
  if (
    parameterHelloID &&
    parameterHelloID.parameterGroupementByCode &&
    parameterHelloID.parameterGroupementByCode.value &&
    parameterHelloID.parameterGroupementByCode.value == 'true'
  ) {
    const parameterHelloIDHour: any = await service.queries(ParameterGroupementByCode, {
      code: '0052',
      idGroupement,
    });
    const currentTime = getTime();
    console.log('currentTime ', currentTime);
    // if hour and minute CRUD
    if (
      (
        parameterHelloIDHour &&
        parameterHelloIDHour.parameterGroupementByCode &&
        parameterHelloIDHour.parameterGroupementByCode.value &&
        parameterHelloIDHour.parameterGroupementByCode.value
      )
        .trim()
        .toLowerCase() === currentTime.trim().toLocaleLowerCase()
    ) {
      createUpdateAllUsersToHelloid(idGroupement);
    }
  }
};

// ACTIVE DIRECTORY

export const getActiveDirectoryAndCreate = async () => {
  const service = new ApiService(apiUrl);
  let i = 1;
  const activeDirectories: any = await service.queryNotHaveparams(Activedirectories);
  const allUpdated = activeDirectories.activedirectories.map(async activeDirectory => {
    const created = await createADUser(activeDirectory, i);
    i = created.number;
    return created;
  });

  return Promise.all(allUpdated);
};

export const CreateActiveDirectoryGroupement = async (idGroupement: string) => {
  const i = 1;
  const service = new ApiService(apiUrl);
  const activeDirectoryDetails: any = await service.queries(ActiveDirectoryGroupement, {
    idgroupement: idGroupement,
  });
  const created = await createADUser(activeDirectoryDetails.activedirectoryGroupement, i);
  return created;
};

export const createADUser = async (activeDirectory: any, num: number) => {
  const config: ldapCredential = {
    url: activeDirectory.url,
    user: activeDirectory.user,
    pass: activeDirectory.password,
    base: activeDirectory.base,
  };
  let i = num;
  const service = new ApiService(apiUrl);

  const process = [];
  const allusersActive = await new LdapService(config).getActiveUser(activeDirectory.ldapType);

  console.log('all users ', allusersActive);
  // create all active users
  const alluserActiveCreated = await insertAll(
    service,
    new LdapService(config),
    activeDirectory,
    allusersActive,
    'ACTIVATION_REQUIRED',
    i,
  );
  process.push(alluserActiveCreated);
  i = alluserActiveCreated.num;

  if (activeDirectory.ldapType === 'WINDOWS_LDAP') {
    const allusersDisabled = await new LdapService(config).getDisabledUser(
      activeDirectory.ldapType,
    );
    // create all disable users
    const alluserDisabledCreated = await insertAll(
      service,
      new LdapService(config),
      activeDirectory,
      allusersDisabled,
      'BLOCKED',
      i,
    );
    i = alluserDisabledCreated.num;
    process.push(alluserDisabledCreated);
  }
  // disable all users removed
  const disabledusers = disableUsers(service, new LdapService(config), activeDirectory);
  process.push(disabledusers);

  const allProcess = await Promise.all(process);

  return {
    data: allProcess,
    number: i,
  };
};

export const disableUsers = async (
  service: ApiService,
  ldapservice: LdapService,
  activeDirectory: any,
) => {
  const users: any = await service.queries(ActiveDirectoryUser, {
    activeDirectoryId: activeDirectory.id,
  });

  const checkAdDbuser = users.activeDirectoryUser.map(async aduserDB => {
    if (aduserDB.user) {
      const aduser = await ldapservice.getOneLdapUser(
        activeDirectory.ldapType,
        aduserDB.user.email,
      );
      if (!aduser) {
        const disableduser: any = await service.queries(UpdateUserStatus, {
          id: aduserDB.user.id,
          status: 'BLOCKED',
        });
        return Promise.resolve(
          `user ${disableduser.updateUserStatus && disableduser.updateUserStatus.mail} updated`,
        );
      }
      return Promise.resolve(`user ${aduserDB.user.email} not changed`);
    }
    return Promise.resolve(`No user for activeDirectoryUser ${aduserDB.id}`);
  });

  return checkAdDbuser;
};

export const insertAll = async (
  service: ApiService,
  ldapservice: LdapService,
  activeDirectory: any,
  data: any,
  status: string,
  i: number,
) => {
  const allusersActive = data;
  if (allusersActive) {
    const alluserCreated = allusersActive.map(async user => {
      if (user.mail) {
        try {
          console.log('creating/updating ', user.mail);
          const personneldata = {
            civilite: '',
            sortie: 0,
            codeService: 'AUTRE',
            nom: user.givenName,
            prenom: user.sn,
            adresse1: user.streetAddress,
            telBureau: user.telephoneNumber ? `${user.telephoneNumber}` : null,
            telDomicile: user.homePhone ? `${user.homePhone}` : null,
            telMobProf: user.mobile ? `${user.mobile}` : null,
            faxBureau: user.facsimileTelephoneNumber,
            cp: user.postalCode,
            ville: user.l,
            mailProf: user.mail,
            mailPerso: user.mail,
            idGroupement: activeDirectory.groupement.id,
          };

          const personnel: any = await service.queries(CreatePersonnel, personneldata);
          console.log('personnel created', personnel);

          if (
            !personnel ||
            (personnel && !personnel.createUpdatePersonnel) ||
            (personnel && personnel.createUpdatePersonnel && !personnel.createUpdatePersonnel.id)
          ) {
            return Promise.reject(`user ${user.mail} not created`);
          }

          const created = await insertOrUpdateUser(
            service,
            ldapservice,
            activeDirectory,
            user,
            personnel,
            personneldata,
            status,
          );

          return Promise.resolve(created);
        } catch (error) {
          console.log('error ', error);
          return Promise.reject(`user ${user.mail} not created`);
        }
      }
    });

    const usersCreated = await Promise.all(alluserCreated);

    return {
      created: usersCreated,
      num: i,
    };
  }
};

export const insertOrUpdateUser = async (
  service: ApiService,
  ldapservice: LdapService,
  activeDirectory: any,
  user: any,
  personnel: any,
  personneldata: any,
  status: string,
) => {
  const checkuserexist: any = await service.queries(CheckExistingUserMail, { email: user.mail });

  if (!(checkuserexist && checkuserexist.checkExistingUserMail)) {
    console.log(`user not exist ===> creating `, checkuserexist.checkExistingUserMail);
    const userGroupementData = {
      idGroupement: personneldata.idGroupement,
      id: personnel.createUpdatePersonnel ? personnel.createUpdatePersonnel.id : '',
      email: user.mail,
      role: 'GRPAUTRE',
      status, // 'ACTIVATION_REQUIRED' //BLOCKED
    };
    const userGroupement: any = await service.queries(CreateUserGroupement, userGroupementData);
    console.log('collaborateur created ============>', userGroupement);
    // create activeDirectoryUser

    const activeDirectoryUser = await service.queries(CreateActiveDirectoryUser, {
      activeDirectoryId: activeDirectory.id,
      userid: userGroupement.createUserGroupement.user.id,
    });
    console.log('activedirectoryuser created', activeDirectoryUser);
    return Promise.resolve(`user ${user.mail} created`);
  } else {
    console.log(`user ${user.mail} already exist`);
    const userldap = await ldapservice.getOneActiveUser(activeDirectory.ldapType, user.mail);

    if (
      (checkuserexist.checkExistingUserMail.status === 'ACTIVATED' ||
        checkuserexist.checkExistingUserMail.status === 'ACTIVATION_REQUIRED') &&
      userldap.length === 0
    ) {
      const updating = await service.queries(UpdateUserStatus, {
        id: checkuserexist.checkExistingUserMail.id,
        status: 'BLOCKED',
      });
      console.log('updated user ', updating);
      return updating;
    } else if (
      userldap.length !== 0 &&
      (checkuserexist.checkExistingUserMail.status === 'BLOCKED' ||
        checkuserexist.checkExistingUserMail.status === 'RESETED')
    ) {
      const updateTo = checkuserexist.checkExistingUserMail.emailConfirmed
        ? 'ACTIVATED'
        : 'ACTIVATION_REQUIRED';
      const updating = await service.queries(UpdateUserStatus, {
        id: checkuserexist.checkExistingUserMail.id,
        status: updateTo,
      });
      console.log('updated user ', updating);
      return updating;
    }

    console.log(`user ${user.mail} updated`);
    return Promise.resolve(`user ${user.mail} updated`);
  }
};

// HelloID

export const _updateCreateEachUser = async (service: ApiService, groupementId: string) => {
  let skip = 0;
  const take = 25;
  const promises = [];
  let allusers: any = await service.queries(UsersGroupement, {
    idGroupement: groupementId,
    skip,
    take,
  });

  console.log('allusers ', allusers);

  if (allusers && allusers.search.data) {
    while (skip <= allusers.search.total) {
      const helloidInfo: any = await service.helloIdSsoDetailsByGroup(
        helloIdSsoByGroupId,
        groupementId,
      );

      const groups = allusers.search.data
        .map(user => user && user.user.role && user.user.role.nom)
        .filter((value, index, self) => {
          return self.indexOf(value) === index;
        });
      await Promise.all(
        groups.map(async group => {
          await getHelloIdGroup(`${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`, {
            groupName: group,
            helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
            helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
          })
            .then(() => {
              console.log(`group ${group} already exist`);
            })
            .catch(async () => {
              await createGroup(`${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`, {
                groupName: group,
                userGuid: null,
                helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
                helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
              })
                .then(result => {
                  console.log(`Group created : ${group}`);
                })
                .catch(err => {
                  console.log(`Group not created : ${err}`);
                  throw new Error(err);
                });
            });
        }),
      );

      const result = allusers.search.data.map(user => {
        // console.log('user ', user);
        // check helloiduser if exist
        return GetOneHelloidUSer(`${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`, {
          contactEmail: user.user.email,
          helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
          helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
        })
          .then(result => {
            // update
            console.log('helloID user already exist [result] ', result);
            const body = {
              id: user.user.id,
              isEnabled: user.user.status === 'ACTIVATED',
              mustChangePassword: false,
              phoneNumber: user.user.phoneNumber,
              isLocked: user.user.status === 'ACTIVATED',
              groupementId,
            };
            return updateHelloidUser({ ...body }, service, helloidInfo)
              .then(resultUpdate => {
                // console.log("user updated ", resultUpdate);
                return resultUpdate;
              })
              .catch(error => {
                throw new Error(error);
              });
            // return updating;
          })
          .catch(error => {
            // create
            console.log('user not exist =====> create ', user);
            const body = {
              id: user.user.id,
              isEnabled: user.user.status === 'ACTIVATED',
              mustChangePassword: true,
              password: 'default-password',
              source: 'Gcr-Pharma',
              groupementId,
            };
            return createUserToHelloid({ ...body }, service, helloidInfo, user);
          });

        // console.log("get user ", getUSer)
      });
      const promiseResult = await Promise.all(result);
      promises.push(promiseResult);

      skip = skip + take;
      if (skip <= allusers.search.total) {
        allusers = await service.queries(UsersGroupement, {
          idGroupement: groupementId,
          skip,
          take,
        });
      }
    }
    return promises;
  }

  return null;
};

export const createUpdateAllUsersToHelloid = async (groupementId: string) => {
  const service = new ApiService(apiUrl);
  const promises = await _updateCreateEachUser(service, groupementId);

  if (promises) {
    const results = await Promise.all(promises);
    return results;
  }
  return null;
};

export const createUserToHelloid = async (
  _body: any,
  service: ApiService,
  helloidInfo: any,
  me: any,
) => {
  let userguid = '';
  // let service = new ApiService(apiUrl);
  // const me = await service.queries(userQueryById, { id: _body.id });
  console.log('me', me);
  if (helloidInfo) {
    const created = await CreateHelloIdUser(
      `${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`,
      {
        firstName: me.nom,
        lastName: me.prenom,
        PhoneNumber: me.user.phoneNumber,
        contactEmail: me.user.email,
        isEnabled: _body.isEnabled,
        mustChangePassword: _body.mustChangePassword,
        password: _body.password,
        source: _body.source,
        helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
        helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
      },
    )
      .then(async function(body) {
        console.log('[CreateHelloIdUser] ', body);
        userguid = body.userGUID;
        const getGroup = await getHelloIdGroup(
          `${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`,
          {
            groupName: me.user.role.nom,
            helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
            helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
          },
        )
          .then(async body => {
            console.log('Group ', body);
            const addToGroup = await linkToGroup(
              `${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`,
              {
                groupNameOrGuid: me.user.role.nom,
                userGuid: userguid,
                helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
                helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
              },
            )
              .then(result => {
                console.log(`Utilisateur ${me.user.email} ajouté au groupe ${me.user.role.nom}`);
                return `Utilisateur ${me.user.email} ajouté au groupe ${me.user.role.nom}`;
              })
              .catch(err => {
                console.log(
                  `Utilisateur ${me.user.email} non ajouté au groupe ${me.user.role.nom}`,
                );
                throw new Error(err);
              });
            return addToGroup;
          })
          .catch(async err => {
            console.log(`Group not exist ${err}`);
            const groupCreated = await createGroup(
              `${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`,
              {
                groupName: me.user.role.nom,
                userGuid: userguid,
                helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
                helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
              },
            )
              .then(result => {
                console.log(`Group created : ${me.user.role.nom}`);
                return `Group created : ${me.user.role.nom}`;
              })
              .catch(err => {
                console.log(`Group not created : ${err}`);
                throw new Error(err);
              });
            return groupCreated;
          });
        return getGroup;
      })
      .catch(function(err) {
        console.log(`HelloId user not created : ${err}`);
        // throw new Error(err);
        return 'User not created ' + me.user.email;
      });

    return created;
  } else {
    console.log(`Groupement User not have Helloid`);
    return null;
  }
};

export const updateHelloidUser = async (_body: any, service: ApiService, helloidInfo: any) => {
  console.log('updating user...');
  // console.log("req body", _body);
  const me: any = await service.getUserDataByid(_body.id, userQueryById);
  // console.log("me", me);

  const updateuser = await UpdateHelloIdUser(
    `${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`,
    {
      firstName: me.user.userName,
      lastName: '',
      PhoneNumber: me.user.phoneNumber,
      contactEmail: me.user.email,
      isEnabled: _body.isEnabled,
      mustChangePassword: _body.mustChangePassword,
      isLocked: _body.isLocked,
      helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
      helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
    },
  )
    .then(async function(result) {
      const execute = async () => {
        const getGroup = await getHelloIdGroup(
          `${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`,
          {
            groupName: me.user.role.nom,
            helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
            helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
          },
        )
          .then(async body => {
            console.log('Group ', body);
            const addToGroup = await linkToGroup(
              `${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`,
              {
                groupNameOrGuid: me.user.role.nom,
                userGuid: result.userGUID,
                helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
                helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
              },
            )
              .then(result => {
                console.log(`Group created : ${me.user.email}`);
                return result;
              })
              .catch(err => {
                console.log(`Group not created : ${err}`);
                throw new Error(err);
              });
            return addToGroup;
          })
          .catch(async err => {
            console.log(`Group error ${err}`);
            const groupCreated = await createGroup(
              `${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`,
              {
                groupName: me.user.role.nom,
                userGuid: result.userGUID,
                helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
                helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
              },
            )
              .then(result => {
                console.log(`Group created : ${me.user.contactEmail}`);
                return result;
              })
              .catch(err => {
                console.log(`Group not created : ${err}`);
                throw new Error(err);
              });
            return groupCreated;
          });
        console.log(`user linked to group `, getGroup);
      };

      console.log(`HelloId user updated : `, result);
      await userGroups(`${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`, {
        userGuid: result.userGUID,
        helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
        helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
      })
        .then(async resultGroup => {
          console.log('groups result ', resultGroup);
          const icludes = resultGroup.some(
            group =>
              group.name.trim().toLocaleLowerCase() === me.user.role.nom.trim().toLocaleLowerCase(),
          );
          console.log('icludes ', icludes);
          if (!icludes) {
            console.log('result.length ', resultGroup.length);
            if (resultGroup && resultGroup.length !== 0) {
              resultGroup.map(async group => {
                const unlinked = await unlinkToGroup(
                  `${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`,
                  {
                    groupNameOrGuid: group.groupGuid,
                    userGuid: result.userGUID,
                    helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
                    helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
                  },
                );

                return unlinked;
              });
              return execute();
            } else {
              return execute();
            }
          }
          return result;
        })
        .catch(err => {
          console.log(`groups error ${err}`);
        });
      return Promise.resolve(result);
    })
    .catch(function(err) {
      console.log(`HelloId user not updated ${err}`);
      return Promise.reject(err);
    });

  return updateuser;
};

export const deleteHelloidUSer = async (_body: any) => {
  console.log('req body', _body);
  const service = new ApiService(apiUrl);
  const me: any = await service.getUserDataByid(_body.id, userQueryById);
  console.log('me', me);
  const helloidInfo: any = await service.helloIdSsoDetailsByGroup(
    helloIdSsoByGroupId,
    me.userById.groupement.id,
  );
  console.log('helloidInfo', helloidInfo);

  DeleteHelloIdUser(`${helloidInfo.helloIdSsoByGroupementId.helloIdUrl}/api/v1`, {
    contactEmail: me.userById.email,
    helloidApiUser: helloidInfo.helloIdSsoByGroupementId.apiKey,
    helloidApiPass: helloidInfo.helloIdSsoByGroupementId.apiPass,
  })
    .then(function(results) {
      console.log(`HelloId user deleted : ${_body.contactEmail}`);
    })
    .catch(function(err) {
      console.log(`HelloId user not deleted : ${err}`);
    });
};
