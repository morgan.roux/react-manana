require('dotenv').config();
// import {helloidApiUser, helloidApiPass} from '../config/config'
const rp = require('request-promise');
const axios = require('axios');

export const getHelloIdGroup = async (helloidApiBaseUrl, { groupName, helloidApiUser, helloidApiPass }) => {
	// var options = {
	//     method: `GET`,
	//     url: `${helloidApiBaseUrl}/groups/${groupName}`,
	//     auth: {
	//         'user': `${helloidApiUser}`,
	//         'pass': `${helloidApiPass}`
	//     },
	//     json: true,
	// };
	// return rp(options);
	const options = {
		method: `get`,
		url: `${helloidApiBaseUrl}/groups/${groupName}`,
		auth: {
			username: `${helloidApiUser}`,
			password: `${helloidApiPass}`
		}
	};

	const response = await axios(options)
		.then((result) => {
			return result.data;
		})
		.catch((err) => {
			console.log(`error ${err}`);
			throw new Error(err);
		});

	return response;
};

export const createGroup = async (helloidApiBaseUrl, { groupName, userGuid, helloidApiUser, helloidApiPass }) => {
	// var options = {
	//     method: `POST`,
	//     url: `${helloidApiBaseUrl}/groups/`,
	//     auth: {
	//         'user': `${helloidApiUser}`,
	//         'pass': `${helloidApiPass}`
	//     },
	//     json: true,
	//     body: {
	//         "Name": `${groupName}`,
	//         "IsEnabled": true,
	//         "IsDefault": false,
	//         "IsQrEnabled": false,
	//         "UserGUIDs": [`${userGuid}`],
	//     }
	// };
	// return rp(options);

	const options = {
		method: `post`,
		url: `${helloidApiBaseUrl}/groups/`,
		auth: {
			username: `${helloidApiUser}`,
			password: `${helloidApiPass}`
		},
		data: {
			Name: `${groupName}`,
			IsEnabled: true,
			IsDefault: false,
			IsQrEnabled: false,
			UserGUIDs: [ `${userGuid}` ]
		}
	};

	const response = await axios(options)
		.then((result) => {
			return result.data;
		})
		.catch((err) => {
			console.log(`error ${err}`);
			throw new Error(err);
		});

	return response;
};

export const linkToGroup = async (helloidApiBaseUrl, { groupNameOrGuid, userGuid, helloidApiUser, helloidApiPass }) => {
	// var options = {
	//     method: `POST`,
	//     url: `${helloidApiBaseUrl}/groups/${groupNameOrGuid}/users`,
	//     auth: {
	//         'user': `${helloidApiUser}`,
	//         'pass': `${helloidApiPass}`
	//     },
	//     json: true,
	//     body: {
	//         'UserGuid': `${userGuid}`
	//     }
	// };
	// return rp(options);
	const options = {
		method: `post`,
		url: `${helloidApiBaseUrl}/groups/${groupNameOrGuid}/users`,
		auth: {
			username: `${helloidApiUser}`,
			password: `${helloidApiPass}`
		},
		data: {
			UserGuid: `${userGuid}`
		}
	};

	const response = await axios(options)
		.then((result) => {
			return result.data;
		})
		.catch((err) => {
			console.log(`error ${err}`);
			throw new Error(err);
		});

	return response;
};

export const unlinkToGroup = async (
	helloidApiBaseUrl,
	{ groupNameOrGuid, userGuid, helloidApiUser, helloidApiPass }
) => {
	const options = {
		method: `delete`,
		url: `${helloidApiBaseUrl}/groups/${groupNameOrGuid}/users/${userGuid}`,
		auth: {
			username: `${helloidApiUser}`,
			password: `${helloidApiPass}`
		}
	};

	const response = await axios(options)
		.then((result) => {
			return result.data;
		})
		.catch((err) => {
			console.log(`error ${err}`);
			throw new Error(err);
		});

	return response;
};

export const userGroups = async (helloidApiBaseUrl, { userGuid, helloidApiUser, helloidApiPass }) => {
	const options = {
		method: `get`,
		url: `${helloidApiBaseUrl}/users/${userGuid}/groups`,
		auth: {
			username: `${helloidApiUser}`,
			password: `${helloidApiPass}`
		}
	};

	const response = await axios(options)
		.then((result) => {
			return result.data;
		})
		.catch((err) => {
			console.log(`error ${err}`);
			throw new Error(err);
		});

	return response;
};

export const assignRole = async (helloidApiBaseUrl, { userNameOrGuid, RoleNames, helloidApiUser, helloidApiPass }) => {
	const options = {
		method: `POST`,
		url: `${helloidApiBaseUrl}/users/${userNameOrGuid}/roles`,
		auth: {
			user: `${helloidApiUser}`,
			pass: `${helloidApiPass}`
		},
		json: true,
		data: {
			RoleNames: [ `${RoleNames}` ]
		}
	};
	return rp(options);
};

export const GetOneHelloidUSer = async (helloidApiBaseUrl, { contactEmail, helloidApiUser, helloidApiPass }) => {
	//     console.log(`${contactEmail} ${helloidApiBaseUrl} ${helloidApiPass}`);
	//     var options = {
	//         method: `GET`,
	//         url: `${helloidApiBaseUrl}/users/${contactEmail}`,
	//         auth: {
	//             'user': `${helloidApiUser}`,
	//             'pass': `${helloidApiPass}`
	//         },
	//         json: true,
	//     };
	//     const result = await rp(options);
	// return  Promise.resolve(result);
	const response = await axios
		.get(`${helloidApiBaseUrl}/users/${contactEmail}`, {
			auth: {
				username: `${helloidApiUser}`,
				password: `${helloidApiPass}`
			}
		})
		.then((result) => {
			// console.log("result ", result.data);
			return result.data;
		})
		.catch((err) => {
			console.log(`error ${err}`);
			throw new Error(err);
		});

	return response;
};

export const CreateHelloIdUser = async (
	helloidApiBaseUrl,
	{
		firstName,
		lastName,
		password,
		isEnabled,
		mustChangePassword,
		source,
		contactEmail,
		PhoneNumber,
		helloidApiUser,
		helloidApiPass
	}
) => {
	// console.log(`${firstName} ${contactEmail} ${password} ${helloidApiBaseUrl} ${helloidApiPass}`);

	//     var options = {
	//         method: `POST`,
	//         url: `${helloidApiBaseUrl}/users/`,
	//         auth: {
	//             'user': `${helloidApiUser}`,
	//             'pass': `${helloidApiPass}`
	//         },
	//         json: true,
	//         body: {
	//             "firstName": `${firstName}`,
	//             "lastName": `${lastName}`,
	//             "userName": `${contactEmail}`,
	//             "password": `${password}`,
	//             "isEnabled": isEnabled,
	//             "mustChangePassword": mustChangePassword,
	//             "source": `${source}`,
	//             "contactEmail": `${contactEmail}`,
	//             "userAttributes": {
	//                 "PhoneNumber": `${PhoneNumber}`,
	//                 "nameid" : `${contactEmail}`
	//             }
	//         }
	//     };
	// return rp(options);
	const options = {
		method: `post`,
		url: `${helloidApiBaseUrl}/users/`,
		auth: {
			username: `${helloidApiUser}`,
			password: `${helloidApiPass}`
		},
		data: {
			firstName: `${firstName}`,
			lastName: `${lastName}`,
			userName: `${contactEmail}`,
			password: `${password}`,
			isEnabled,
			mustChangePassword,
			source: `${source}`,
			contactEmail: `${contactEmail}`,
			userAttributes: {
				PhoneNumber: `${PhoneNumber}`,
				nameid: `${contactEmail}`
			}
		}
	};
	const response = await axios(options)
		.then((result) => {
			return result.data;
		})
		.catch((err) => {
			console.log(`error ${err}`);
			throw new Error(err);
		});

	return response;
};

export const UpdateHelloIdUser = async (
	helloidApiBaseUrl,
	{
		contactEmail,
		firstName,
		lastName,
		isEnabled,
		isLocked,
		mustChangePassword,
		PhoneNumber,
		helloidApiUser,
		helloidApiPass
	}
) => {
	// var options = {
	//     method: `PUT`,
	//     url: `${helloidApiBaseUrl}/users/${contactEmail}`,
	//     auth: {
	//         'user': `${helloidApiUser}`,
	//         'pass': `${helloidApiPass}`
	//     },
	//     json: true,
	//     body: {
	//         "firstName": firstName,
	//         "lastName": lastName,
	//         "isEnabled": isEnabled,
	//         "isLocked": isLocked,
	//         "mustChangePassword": mustChangePassword,
	//         "userAttributes": {
	//             "PhoneNumber": `${PhoneNumber}`
	//         }
	//     }
	// };
	// return rp(options);

	const response = await axios({
		method: 'put',
		url: `${helloidApiBaseUrl}/users/${contactEmail}`,
		auth: {
			username: `${helloidApiUser}`,
			password: `${helloidApiPass}`
		},
		data: {
			firstName,
			lastName,
			isEnabled,
			isLocked,
			mustChangePassword,
			userAttributes: {
				PhoneNumber: `${PhoneNumber}`
			}
		}
	})
		.then((result) => {
			return result.data;
		})
		.catch((err) => {
			console.log(`error ${err}`);
			throw new Error(err);
		});

	return response;
};

export const DeleteHelloIdUser = async (helloidApiBaseUrl, { contactEmail, helloidApiUser, helloidApiPass }) => {
	//     var options = {
	//         method: `DELETE`,
	//         url: `${helloidApiBaseUrl}/users/${contactEmail}`,
	//         auth: {
	//             'user': `${helloidApiUser}`,
	//             'pass': `${helloidApiPass}`
	//         },
	//         json: true,
	//     };
	//    return rp(options);
	const options = {
		method: `delete`,
		url: `${helloidApiBaseUrl}/users/${contactEmail}`,
		auth: {
			username: `${helloidApiUser}`,
			password: `${helloidApiPass}`
		}
	};

	const response = await axios(options)
		.then((result) => {
			return result.data;
		})
		.catch((err) => {
			console.log(`error ${err}`);
			throw new Error(err);
		});

	return response;
};
