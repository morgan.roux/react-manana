// functions for Active directory
const AD = require('ad');

export interface Aduser {
	userName: string;
	password: string;
	commonName: string;
	firstName: string;
	lastName?: string;
	email: string;
	title?: string;
	location?: string;
	enable?: boolean;
	phone?: number;
}
export interface ADCredential {
	url: string;
	user: string;
	pass: string;
}

export class ActiveDirectoryService {
	ad: any;

	// constructor
	constructor({ url, user, pass }: ADCredential) {
		this.ad = new AD({
			url,
			user,
			pass
		});
	}

	// get all users
	getAdusers = async ({ username }, callback) => {
		if (username && username != '') {
			this.ad
				.user(username)
				.get()
				.then((users) => {
					console.log('AD users', users);
					callback(users);
				})
				.catch((err) => {
					console.log('error AD users', err);
					callback(err);
				});
		} else {
			this.ad
				.user()
				.get()
				.then((users) => {
					console.log('AD users', users);
					callback(users);
				})
				.catch((err) => {
					console.log('error AD users', err);
					callback(err);
				});
		}
	};

	// create user
	createAduser = async (_args: Aduser, callback) => {
		try {
			console.log(_args);
			const created = await this.ad.user().add({ ..._args });
			console.log('user created ', created);
			callback(null, created);
		} catch (err) {
			// console.log(`create user error `, err)
			callback(`error when creating ${err.message}`);
		}
	};

	// enable or not user
	enableOrNotUser = async ({ userName, enable }, callback) => {
		enable ? await this.ad.user(userName).enable() : await this.ad.user(userName).disable();
		enable ? callback('user enabled') : callback('user disabled');
	};

	// unlock user blocked by error password
	unlockUser = async ({ userName }, callback) => {
		await this.ad.user(userName).unlock();
		callback('user unlocked');
	};

	// delete user
	removeUser = async ({ userName }, callback) => {
		await this.ad.user(userName).remove();
		callback('user removed');
	};
}
