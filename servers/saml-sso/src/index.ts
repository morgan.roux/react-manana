import { port } from './config/config';
import { InitSchedule } from './service/automatic-crud';
let cors = require('cors');
let express = require('express');
let cookieParser = require('cookie-parser');
let passport = require('passport');
const bodyParser = require('body-parser');
let exphbs = require('express-handlebars');

let app = express();
app.use(cors({ origin: '*' }));
app.use(bodyParser.json());
app.set('etag', false); // turn off

app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');

app.use('/', require('./routes/sso-idp'));
app.use('/', require('./routes/sso-sp'));
app.use('/', require('./routes/extensions'));

app.listen(port, () => {
	InitSchedule();
	console.log(`Sso service listening on port ${port}!`);
});
