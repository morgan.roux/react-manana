let express = require('express');

let router = express.Router();

router.get('/extension/download/:browser', function(req, res) {
  let file;
  switch (req.params.browser) {
    case 'chrome':
      file = './extensions-files/gcr-pharma-chrome.zip';
      break;
    case 'firefox':
      file = './extensions-files/gcr-pharma-firefox.zip';
      break;
    case 'edge':
      file = './extensions-files/gcr-pharma-edge.zip';
      break;
    default:
      file = null;
      break;
  }
  if (file) return res.download(file);
  res.send('browser unknown');
});
module.exports = router;
