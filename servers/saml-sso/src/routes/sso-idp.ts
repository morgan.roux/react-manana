/*****************
 *
 * Import statements
 *
 *****************/

import {
  ApiService,
  helloIdSsoByGroupId,
  ApplicationDetail,
  SamlLogin,
  CryptoMd5,
  CryptoAes256,
  ActualUser,
  CryptoOther,
  TokenAuthentification,
  TokenFtpAuthentification,
  DataToSend,
  initialData,
  ActiveDirectoryGroupement,
  Groupement,
  userSearchById,
} from '../service/ApiService';
import { apiUrl, ssourl, clientUrl } from '../config/config';
import { getHelloIdGroup, GetOneHelloidUSer, userGroups } from '../service/helloID';
import { formateDate, getFtpFileToken, getPublicIp, permut } from '../service/utils';
import { LdapService, ldapCredential } from '../service/ldapService';
import {
  getActiveDirectoryAndCreate,
  createUserToHelloid,
  updateHelloidUser,
  deleteHelloidUSer,
  createUpdateAllUsersToHelloid,
  CreateActiveDirectoryGroupement,
  automaticCrud,
  UpdateOrAddSchedule,
} from '../service/automatic-crud';

/*****************
 *
 * Required modules
 *
 *****************/

const crypto = require('crypto');
const md5 = require('md5');
const aes256 = require('aes256');
const vm = require('vm');
const express = require('express');
const fs = require('fs');
const uuid = require('uuid');
const validator = require('@authenio/samlify-xsd-schema-validator');
const saml = require('samlify');
const url = require('url');
const clientFtp = require('ftp');

/*****************
 *
 * SERVER CONFIGURATIONS
 *
 *****************/

if (process.env.NODE_ENV !== 'production') {
  require('longjohn');
}

const router = express.Router();
const binding = saml.Constants.wording.binding;
saml.setSchemaValidator(validator);

/*****************
 *
 * ROUTES
 *
 *****************/

router.get('/idp-metadata', async function(req, res) {
  const cert = fs
    .readFileSync('./src/key/idp/gcr-pharma-certificate.cer', 'utf8')
    .replace(/-----BEGIN CERTIFICATE-----/g, ``)
    .replace(/-----END CERTIFICATE-----/g, ``)
    .trim();
  const assoIdp = idp(cert, [
    {
      name: 'email',
      valueTag: 'user.email',
      nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
      valueXsiType: 'xs:string',
    },
    {
      name: 'firstname',
      valueTag: 'user.firstname',
      nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
      valueXsiType: 'xs:string',
    },
    {
      name: 'phoneNumber',
      valueTag: 'user.phoneNumber',
      nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
      valueXsiType: 'xs:string',
    },
    {
      name: 'memberOf',
      valueTag: 'user.memberOf',
      nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
      valueXsiType: 'xs:string',
    },
  ]);
  res.header('Content-Type', 'text/xml').send(assoIdp.getMetadata());
});

router.get('/open-helloid/:groupementid/:token', async function(req, res) {
  const service = new ApiService(apiUrl);
  const me = await service.actualuser(ActualUser, req.params.token);
  const helloidInfo = await service.helloIdSsoDetailsByGroup(
    helloIdSsoByGroupId,
    req.params.groupementid,
  );

  const attribute = [
    {
      name: 'email',
      valueTag: 'user.email',
      nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
      valueXsiType: 'xs:string',
    },
    {
      name: 'firstname',
      valueTag: 'user.firstname',
      nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
      valueXsiType: 'xs:string',
    },
    {
      name: 'phoneNumber',
      valueTag: 'user.phoneNumber',
      nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
      valueXsiType: 'xs:string',
    },
    {
      name: 'memberOf',
      valueTag: 'user.memberOf',
      nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
      valueXsiType: 'xs:string',
    },
    {
      name: 'lastname',
      valueTag: 'user.lastname',
      nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
      valueXsiType: 'xs:string',
    },
  ];

  const tagReplacement = function(template) {
    console.log('flag 2');
    console.log('[debug]', req.user);
    const nowdate = new Date();
    const spEntityID = targetSP.entityMeta.getEntityID();
    const idpSetting = assoIdp.entitySetting;
    const datenow = new Date(nowdate.getTime());
    datenow.setMinutes(datenow.getMinutes() + 5);
    const fiveMinutesLater = new Date(datenow).toISOString();
    const now = nowdate.toISOString();
    const acl = targetSP.entityMeta.getAssertionConsumerService(binding.post);
    const id = uuid.v4();
    const tvalue = {
      ID: idpSetting.generateID ? idpSetting.generateID() : uuid.v4(),
      AssertionID: idpSetting.generateID ? idpSetting.generateID() : uuid.v4(),
      Destination: acl,
      Audience: spEntityID,
      SubjectRecipient: acl,
      NameIDFormat: 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
      NameID: req.user.email,
      Issuer: assoIdp.entityMeta.getEntityID(),
      IssueInstant: now,
      ConditionsNotBefore: now,
      ConditionsNotOnOrAfter: fiveMinutesLater,
      SubjectConfirmationDataNotOnOrAfter: fiveMinutesLater,
      AssertionConsumerServiceURL: acl,
      EntityID: spEntityID,
      StatusCode: 'urn:oasis:names:tc:SAML:2.0:status:Success',
      // replace attribute
      attrUserEmail: req.user.email,
      attrUserPhoneNumber: req.user.phoneNumber,
      attrUserFirstname: req.user.firstname,
      attrUserLastname: req.user.lastname,
      attrUserMemberOf: req.user.memberOf,
    };

    const response = saml.SamlLib.replaceTagsByValue(template, tvalue);
    console.log('***** response ******', response);
    // replace tag
    return {
      id,
      context: response,
    };
  };

  const cert = fs
    .readFileSync('./src/key/idp/gcr-pharma-certificate.cer', 'utf8')
    .replace(/-----BEGIN CERTIFICATE-----/g, ``)
    .replace(/-----END CERTIFICATE-----/g, ``)
    .trim();

  console.log('cert ==============>');
  console.log(cert);

  const assoIdp = idp(cert, attribute);
  const targetSP = sp(
    helloidInfo.helloIdSsoByGroupementId.helloIdUrl,
    helloidInfo.helloIdSsoByGroupementId.helloIDConsumerUrl,
    helloidInfo.helloIdSsoByGroupementId.x509Certificate,
  );
  assoIdp
    .parseLoginRequest(targetSP, 'redirect', req)
    .then(parseResult => {
      req.user = {};
      req.user.email = me.me.email;
      req.user.firstname = me.me.userName;
      req.user.phoneNumber = me.me.phoneNumber;
      req.user.lastname = me.me.userName.split(' ')[1];
      req.user.memberOf = me.me.role && me.me.role.nom;

      return assoIdp.createLoginResponse(targetSP, parseResult, 'post', req.user, tagReplacement);
    })
    .then(response => {
      console.log(`login response sent to ${response.entityEndpoint}`);

      const buff = new Buffer(response.context, 'base64');
      const text = buff.toString('ascii');
      const respTxt = text.replace(
        /EncryptedAssertion xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"/g,
        'EncryptedAssertion',
      );

      const buff64 = new Buffer(respTxt);
      const resp64 = buff64.toString('base64');

      response.context = resp64;
      res.render('actions', response);
    })
    .catch(err => {
      console.log(err);
      res.send(err);
    });
});

/*****************
 *
 * HELLOID gestion des utilisateurs
 *
 *****************/

router.post('/create-helloiduser', async (req, res) => {
  const { body } = req;
  console.log('body ', body);
  const service = new ApiService(apiUrl);
  const helloidInfo = await service.helloIdSsoDetailsByGroup(
    helloIdSsoByGroupId,
    body.groupementId,
  );
  console.log('helloidInfo', helloidInfo);
  const me = await service.querySearch(userSearchById, {
    idGroupement: body.groupementId,
    iduser: body.id,
  });

  console.log('me ', me.data.search.data);

  await createUserToHelloid({ ...body }, service, helloidInfo, me.data.search.data[0]);
  res.send('create helloid user process finished.....');
});

router.post('/update-helloiduser', async (req, res) => {
  const body = req.body;
  const service = new ApiService(apiUrl);
  const helloidInfo = await service.helloIdSsoDetailsByGroup(
    helloIdSsoByGroupId,
    body.groupementId,
  );
  console.log('helloidInfo', helloidInfo);
  await updateHelloidUser({ ...body }, service, helloidInfo);
  res.send('update helloid users process finished.....');
});

router.post('/delete-helloiduser', async (req, res) => {
  const body = req.body;
  await deleteHelloidUSer({ ...body });
  res.send('delete helloid users process finished.....');
});

router.post('/create-update-allusers-helloid', (req, res) => {
  const body = req.body;
  console.log('req body ', body);
  createUpdateAllUsersToHelloid(body.groupementId)
    .then(result => {
      console.log('result request ', result);
      res.send('Create all helloid users process finished.....');
    })
    .catch(err => {
      console.log(err);
      res.send('Error when creating users.....');
    });
});

/*****************
 *
 * LDAP gestion des utilisateurs
 *
 *****************/

router.get('/ldap-create-all-users', (req, res) => {
  getActiveDirectoryAndCreate()
    .then(result => {
      console.log('process finished ', result);
      res.send('process finished');
    })
    .catch(err => {
      console.log(err);
      throw new Error('Error when creating/updating users.....');
    });
});

router.post('/ldap-create-users', (req, res) => {
  console.log('req body =================>', req.body);
  CreateActiveDirectoryGroupement(req.body.groupementId)
    .then(result => {
      console.log('process finished ', result);
      res.send('process finished');
    })
    .catch(err => {
      console.log(err);
      throw new Error('Error when creating/updating users.....');
    });
});

/*****************
 *
 * Schedule task
 *
 *****************/

router.post('/update-job', (req, res) => {
  const { body } = req;
  console.log('req body =================>', body);
  UpdateOrAddSchedule(body.groupid, body.code, body.value)
    .then(() => {
      res.send('process finished');
    })
    .catch(err => {
      console.log(err);
      throw new Error('Error when creating/updating users.....');
    });
});

/*****************
 *
 * SSO par type
 *
 *****************/

router.get('/go-to/:groupementid/:cip/:applicationid/:token', async (req, res) => {
  const service = new ApiService(apiUrl);
  console.log(req.params);
  const actualuser = await service.actualuser(ActualUser, req.params.token);
  const applicationDetail = await service.application(ApplicationDetail, req.params.applicationid);
  const groupementInfo = await service.queries(Groupement, { id: req.params.groupementid });
  console.log('application detail ', applicationDetail);
  console.log('actual user ', actualuser);

  switch (applicationDetail.ssoApplication.ssoType) {
    case 'SAML': {
      console.log('saml');
      const samlapp = await service.application(
        SamlLogin,
        applicationDetail.ssoApplication.ssoTypeid,
      );
      const attribute = [
        {
          name: 'email',
          valueTag: 'user.email',
          nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
          valueXsiType: 'xs:string',
        },
        {
          name: 'firstname',
          valueTag: 'user.firstname',
          nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
          valueXsiType: 'xs:string',
        },
        {
          name: 'lastname',
          valueTag: 'user.lastname',
          nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
          valueXsiType: 'xs:string',
        },
        {
          name: 'phoneNumber',
          valueTag: 'user.phoneNumber',
          nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
          valueXsiType: 'xs:string',
        },
        {
          name: 'memberOf',
          valueTag: 'user.memberOf',
          nameFormat: 'urn:oasis:names:tc:SAML:2.0:attrname-format:basic',
          valueXsiType: 'xs:string',
        },
      ];

      const idpconfig = idp(samlapp.samlLogin.idpX509Certificate, attribute);
      const targetSP = sp(
        applicationDetail.ssoApplication.url,
        samlapp.samlLogin.consumerUrl,
        samlapp.samlLogin.spX509Certificate,
      );
      const tagReplacement = function(template) {
        console.log('flag 2');
        console.log('[debug]', req.user);
        const nowdate = new Date();
        const spEntityID = targetSP.entityMeta.getEntityID();
        const idpSetting = idpconfig.entitySetting;
        const datenow = new Date(nowdate.getTime());
        datenow.setMinutes(datenow.getMinutes() + 5);
        const fiveMinutesLater = new Date(datenow).toISOString();
        const now = nowdate.toISOString();
        const acl = targetSP.entityMeta.getAssertionConsumerService(binding.post);
        const id = uuid.v4();

        const tvalue = {
          ID: idpSetting.generateID ? idpSetting.generateID() : uuid.v4(),
          AssertionID: idpSetting.generateID ? idpSetting.generateID() : uuid.v4(),
          Destination: acl,
          Audience: spEntityID,
          SubjectRecipient: acl,
          NameIDFormat: 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',
          NameID: req.user.email,
          Issuer: idpconfig.entityMeta.getEntityID(),
          IssueInstant: now,
          ConditionsNotBefore: now,
          ConditionsNotOnOrAfter: fiveMinutesLater,
          SubjectConfirmationDataNotOnOrAfter: fiveMinutesLater,
          AssertionConsumerServiceURL: acl,
          EntityID: spEntityID,
          StatusCode: 'urn:oasis:names:tc:SAML:2.0:status:Success',
          // replace attribute
          attrUserEmail: req.user.email,
          attrUserPhoneNumber: req.user.phoneNumber,
          attrUserFirstname: req.user.firstname,
          attrUserLastname: req.user.lastname,
          attrUserMemberOf: req.user.memberOf,
        };

        const response = saml.SamlLib.replaceTagsByValue(template, tvalue);
        console.log('***********', response);
        // replace tag
        return {
          id,
          context: response,
        };
      };

      req.user = {};
      req.user.email = actualuser.me.email;
      req.user.firstname = actualuser.me.userName;
      req.user.phoneNumber = actualuser.me.phoneNumber;
      req.user.lastname = actualuser.me.userName.split(' ')[1] || '';
      req.user.memberOf = actualuser.me.role && actualuser.me.role.nom;

      idpconfig
        .createLoginResponse(targetSP, null, 'post', req.user, tagReplacement)
        .then(response => {
          console.log(`login response sent to ${response.entityEndpoint}`);

          const buff = new Buffer(response.context, 'base64');
          const text = buff.toString('ascii');
          const respTxt = text.replace(
            /EncryptedAssertion xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion"/g,
            'EncryptedAssertion',
          );

          const buff64 = new Buffer(respTxt);
          const resp64 = buff64.toString('base64');

          response.context = resp64;
          res.render('actions', response);
        })
        .catch(err => {
          console.log(err);
          res.send(err);
        });
      // res.header('Content-Type', 'text/xml').send(idpconfig.getMetadata());
      break;
    }
    case 'CRYPTAGE_MD5': {
      console.log('md5');
      const cryptomd5app = await service.application(
        CryptoMd5,
        applicationDetail.ssoApplication.ssoTypeid,
      );

      console.log(cryptomd5app);
      const beginEnd = permut(cryptomd5app.cryptoMd5.beginGet, cryptomd5app.cryptoMd5.endGet);
      console.log('beginEnd ', beginEnd);

      let cryptedurl = '';
      let urlRedirect = '';
      let toPost = [];
      if (cryptomd5app.cryptoMd5.haveExternalUserMapping) {
        const actualMapping = actualuser.me.externalMapping;
        const externaluserid =
          actualMapping && actualMapping.idExternaluser ? actualMapping.idExternaluser : '';
        const idClient = actualMapping && actualMapping.idClient ? actualMapping.idClient : '';
        const password = actualMapping && actualMapping.password ? actualMapping.password : '';

        console.log(
          `donnée à crypter ?iduser=${externaluserid}&iclient=${idClient}&mdp=${password}`,
        );
        cryptedurl = md5(`?iduser=${externaluserid}&iclient=${idClient}&mdp=${password}`);

        cryptedurl = cryptedurl.substring(beginEnd.begin - 1, beginEnd.end);

        if (!cryptomd5app.cryptoMd5.hexadecimal) {
          const buff = new Buffer(cryptedurl, 'base64');
          cryptedurl = buff.toString('ascii');
        }

        if (cryptomd5app.cryptoMd5.Sendingtype == 'GET') {
          urlRedirect = `${cryptomd5app.cryptoMd5.requestUrl}?iduser=${externaluserid}&iclient=${idClient}&tk=${cryptedurl}`;
          console.log('urlRedirect ', urlRedirect);
        }
        if (cryptomd5app.cryptoMd5.Sendingtype == 'POST') {
          toPost = [
            {
              inputname: 'iduser',
              inputvalue: externaluserid,
            },
            {
              inputname: 'iclient',
              inputvalue: idClient,
            },
            {
              inputname: 'tk',
              inputvalue: cryptedurl,
            },
          ];
          console.log('To post ', toPost);
        }
      } else {
        cryptedurl = md5(`?login=${actualuser.me.email}`);
        cryptedurl = cryptedurl.substring(beginEnd.begin - 1, beginEnd.end);
        if (cryptomd5app.cryptoMd5.Sendingtype == 'GET') {
          urlRedirect = `${cryptomd5app.cryptoMd5.requestUrl}?login=${actualuser.me.email}&tk=${cryptedurl}`;
          console.log('urlRedirect ', urlRedirect);
        }
        if (cryptomd5app.cryptoMd5.Sendingtype == 'POST') {
          toPost = [
            {
              inputname: 'login',
              inputvalue: actualuser.me.email,
            },
            {
              inputname: 'tk',
              inputvalue: cryptedurl,
            },
          ];
          console.log('To post ', toPost);
        }
      }

      if (cryptomd5app.cryptoMd5.Sendingtype == 'GET') {
        console.log('GET');
        res.redirect(urlRedirect);
      } else {
        console.log('POST');
        const response = {
          entityEndpoint: cryptomd5app.cryptoMd5.requestUrl,
          inputs: toPost,
        };
        res.render('post', response);
      }
      break;
    }
    case 'CRYPTAGE_AES256': {
      console.log('aes');
      const cryptoAes256 = await service.application(
        CryptoAes256,
        applicationDetail.ssoApplication.ssoTypeid,
      );

      const myPublicIP = await getPublicIp();

      const isBase64URL  = !cryptoAes256.cryptoAes256.key || "base64Url" === cryptoAes256.cryptoAes256.key
      const timestampInSeconds = Math.round(+new Date()/1000)
      const info = `${isBase64URL && process.env.CIP?process.env.CIP:req.params.cip};${timestampInSeconds};${myPublicIP}`;

      console.log('info ', info);

      if(isBase64URL){ // For example, pnr
        const encodedData= Buffer.from(info).toString('base64')

        console.log('Encoded data',encodedData)
        res.redirect(
          `${cryptoAes256.cryptoAes256.requestUrl}/${encodedData}`,
        );
          return;
      }


      const cipher = aes256.createCipher(cryptoAes256.cryptoAes256.key);
      let encrypted = cipher.encrypt(info);

      if (cryptoAes256.cryptoAes256.hexadecimal) {
        const buff = new Buffer(encrypted);
        encrypted = buff.toString('base64');
      }
      console.log(encrypted);

      if (cryptoAes256.cryptoAes256.Sendingtype == 'GET') {
        console.log('GET');
        res.redirect(
          `${cryptoAes256.cryptoAes256.requestUrl}?Action=${'internal'}&Secret=${encrypted}`,
        );
      } else {
        console.log('POST');
        const response = {
          entityEndpoint: cryptoAes256.cryptoAes256.requestUrl,
          inputs: [
            {
              inputname: 'Action',
              inputvalue: 'internal',
            },
            {
              inputname: 'Secret',
              inputvalue: encrypted,
            },
          ],
        };
        res.render('post', response);
      }
      break;
    }
    case 'OTHER_CRYPTO': {
      console.log('other');
      const cryptoOther = await service.application(
        CryptoOther,
        applicationDetail.ssoApplication.ssoTypeid,
      );
      const activeDirectoryDetails = await service.queries(ActiveDirectoryGroupement, {
        idgroupement: req.params.groupementid,
      });
      const config: ldapCredential = {
        url: activeDirectoryDetails.activedirectoryGroupement.url,
        user: activeDirectoryDetails.activedirectoryGroupement.user,
        pass: activeDirectoryDetails.activedirectoryGroupement.password,
        base: activeDirectoryDetails.activedirectoryGroupement.base,
      };
      const user = await new LdapService(config).getOneLdapUser(
        activeDirectoryDetails.activedirectoryGroupement.ldapType,
        actualuser.me.email,
      );
      if (user && user != null) {
        console.log('crypto detail ', cryptoOther);
        console.log('user AD details  ', user);
        const cryptageFunction = cryptoOther.cryptoOther.function;

        const today = new Date();
        const todayString = formateDate(today, '-');
        console.log('date ', todayString);
        const toHash = `username=${user.mail}&date=${todayString}`;
        console.log('to hash ', toHash);
        const secret = 'decrypt key';
        const hash = await vm.runInThisContext(cryptageFunction)(
          crypto,
          md5,
          aes256,
          toHash,
          secret,
        );
        console.log('hashed ', hash);
        if (cryptoOther.cryptoOther.Sendingtype == 'GET') {
          console.log('GET', cryptoOther.cryptoOther.requestUrl);
          res.redirect(`${cryptoOther.cryptoOther.requestUrl}?token=${hash}`);
        } else {
          console.log('POST');
          const response = {
            entityEndpoint: cryptoOther.cryptoOther.requestUrl,
            inputs: [
              {
                inputname: 'token',
                inputvalue: hash,
              },
            ],
          };
          res.render('post', response);
        }
      } else {
        res.send(`user not found into active directory ${actualuser.me.email}`);
      }
      break;
    }
    case 'TOKEN_AUTHENTIFICATION': {
      console.log('token');
      const tokenAuthentification = await service.application(
        TokenAuthentification,
        applicationDetail.ssoApplication.ssoTypeid,
      );
      console.log(tokenAuthentification);

      const myURL = url.parse(
        tokenAuthentification.tokenAuthentification.reqUserUrl
          .toLowerCase()
          .replace('${token}', ':token'),
      );
      console.log('url path name ', myURL.pathname);

      router.get(myURL.pathname, async (req, res) => {
        // let service = new ApiService(apiUrl);
        // let actualuser = await service.actualuser(ActualUser, req.params.token);
        const datatoSend: DataToSend = {
          ...initialData,
          id: actualuser.me.id,
          email: actualuser.me.email,
          firstname: actualuser.me.userName.split(' ')[1],
          lastname: actualuser.me.userName.split(' ')[0],
          username: actualuser.me.userName,
          phone: actualuser.me.phoneNumber,
          company: groupementInfo && groupementInfo.groupement && groupementInfo.groupement.nom,
        };

        console.log('data to send ', datatoSend);

        res.header('Content-Type', 'application/json').send(datatoSend);
      });

      if (tokenAuthentification.tokenAuthentification.Sendingtype === 'GET') {
        console.log('get');
        res.redirect(
          `${tokenAuthentification.tokenAuthentification.reqTokenUrl}?token=${req.params.token}`,
        );
      } else {
        console.log('post');

        const response = {
          entityEndpoint: tokenAuthentification.tokenAuthentification.reqTokenUrl,
          inputs: [
            {
              inputname: 'token',
              inputvalue: req.params.token,
            },
          ],
        };

        res.render('post', response);
      }
      // const token = await service.extractToken()
      break;
    }
    case 'TOKEN_FTP_AUTHENTIFICATION': {
      try {
        console.log('Token ftp');
        const tokenFtpAuthentification = await service.application(
          TokenFtpAuthentification,
          applicationDetail.ssoApplication.ssoTypeid,
        );
        console.log('TokenFtpAuthentification ', tokenFtpAuthentification.tokenFtpAuthentification);
        const ftp =
          tokenFtpAuthentification.tokenFtpAuthentification &&
          tokenFtpAuthentification.tokenFtpAuthentification.ftpid;
        console.log('ftp details ', ftp);
        getFtpFileToken(
          actualuser.me.email,
          {
            host: ftp.host,
            port: ftp.port,
            username: ftp.user,
            password: ftp.password,
            ftpFilePath:
              tokenFtpAuthentification.tokenFtpAuthentification &&
              tokenFtpAuthentification.tokenFtpAuthentification.ftpFilePath,
            ftpFileName:
              tokenFtpAuthentification.tokenFtpAuthentification &&
              tokenFtpAuthentification.tokenFtpAuthentification.ftpFileName,
          },
          function(result) {
            console.log('Get ftp file');
            if (result && result.login && result.token) {
              if (result.Date_debut <= Date.now() && result.Date_fin >= Date.now()) {
                if (tokenFtpAuthentification.tokenFtpAuthentification.Sendingtype === 'GET') {
                  console.log(
                    'redirect to ',
                    `${
                      tokenFtpAuthentification.tokenFtpAuthentification.requestUrl
                    }?token=${result.token.trim()}`,
                  );
                  res.redirect(
                    `${
                      tokenFtpAuthentification.tokenFtpAuthentification.requestUrl
                    }?token=${result.token.trim()}`,
                  );
                  return;
                } else {
                  console.log(
                    'post to',
                    tokenFtpAuthentification.tokenFtpAuthentification.requestUrl,
                  );

                  const response = {
                    entityEndpoint: tokenFtpAuthentification.tokenFtpAuthentification.requestUrl,
                    inputs: [
                      {
                        inputname: 'token',
                        inputvalue: result.token.trim(),
                      },
                    ],
                  };
                  res.render('post', response);
                  return;
                }
              } else {
                console.log('error : Token not valid at this time or expired!');
                res.send('Token not valid at this time or expired!');
                return;
              }
            } else {
              console.log('error : getting user token');
              res.send('error when getting user token or user not in file');
            }
          },
        );
      } catch (error) {
        console.log('error : ', error);
        res.send(error);
        return;
      }

      break;
    }
    default: {
      console.log('Authentification unknown ');
      res.send('Authentification unknown');
    }
  }
});

/*****************
 *
 * PRIVATE FUNCTIONS
 *
 *****************/

// SAML

const idp = (idpX509Certificate, samlatributes) => {
  return saml.IdentityProvider({
    privateKey: fs.readFileSync('./src/key/idp/gcr-pharma-private-key.pem'),
    isAssertionEncrypted: true,
    // encPrivateKey: fs.readFileSync('./src/key/idp/encryptKey.pem'),
    // encPrivateKeyPass: 'g7hGcRmp8PxT5QeP2q9Ehf1bWe9zTALN',
    privateKeyPass: 'Pharma@123456789',
    dataEncryptionAlgorithm: 'http://www.w3.org/2001/04/xmlenc#aes128-cbc',
    keyEncryptionAlgorithm: 'http://www.w3.org/2001/04/xmlenc#rsa-1_5',
    metadata: fs
      .readFileSync('./src/metadata/for-idp/metadata_idp1.xml', 'utf8')
      .replace(/{SAML_SSO_ENDPOINT}/g, `${ssourl}`)
      .replace(/{CLIENT_URL}/g, `${clientUrl}`)
      .replace(/{X509CERTIFICATE}/g, `${idpX509Certificate}`),
    loginResponseTemplate: {
      context: fs.readFileSync('./src/xml-template/login-response.xml', 'utf8'),
      attributes: samlatributes,
    },
    signatureConfig: {
      prefix: 'ds',
      location: {
        reference: "/*[local-name(.)='Response']/*[local-name(.)='Issuer']",
        action: 'after',
      },
    },
  });
};

const sp = (applicationUrl, consumerUrl, spX509Certificate) => {
  return saml.ServiceProvider({
    // privateKey: fs.readFileSync('./src/key/sp/privkey.pem'),
    // encPrivateKeyPass: 'g7hGcRmp8PxT5QeP2q9Ehf1bWe9zTALN',
    metadata: fs
      .readFileSync('./src/metadata/for-idp/metadata_sp1.xml', 'utf8')
      .replace(/{APPLICATION_URL}/g, `${applicationUrl}`)
      .replace(/{CONSUMER_URL}/g, `${consumerUrl}`)
      .replace(/{X509CERTIFICATE}/g, `${spX509Certificate}`),
  });
};

/*****************
 *
 * TESTS
 *
 *****************/

// FTP
// FTP_USER_NAME: gcr-pharma
// FTP_USER_PASS: Pharma@123456789

router.get('/list-ftp-dir', async (req, res) => {
  const c = new clientFtp();
  c.on('ready', function() {
    c.list(function(err, list) {
      if (err) throw err;
      console.dir(list);
      c.end();
    });
  });
  // connect to localhost:21 as anonymous
  c.connect({
    host: '151.80.45.49',
    port: 21,
    user: 'gcr-pharma',
    password: 'Pharma@123456789',
  });
  res.send('listed');
});

// LDAP

router.get('/ldap-all-users', async (req, res) => {
  // linux
  // const config : ldapCredential = {
  //   url : 'ldap://openldap',
  //   user : "cn=admin,dc=example,dc=org",
  //   pass : "Pharma@123456789",
  //   base : "dc=example,dc=org",
  // }

  // //windows
  const config: ldapCredential = {
    url: 'ldap://172.16.205.128',
    user: 'Administrator@gcr-pharma.ad',
    pass: 'Qd;in123456',
    base: 'dc=gcr-pharma,dc=ad',
  };
  console.log('get users');
  const allusers = await new LdapService(config).getActiveUser('OPENLDAP');
  res.send(allusers);
});

// test get one user
router.get('/ldap-one-user', async (req, res) => {
  // using async/await
  const config: ldapCredential = {
    url: 'ldap://172.16.137.128',
    user: 'Administrator@gcr-pharma.ad',
    pass: 'Qd;in123456',
    base: 'dc=gcr-pharma,dc=ad',
  };
  const oneuser = await new LdapService(config).getOneLdapUser(
    'OPENLDAP',
    'anjaniaina@example.org',
  );
  console.log('one user ', oneuser);
  res.send(oneuser);
});

// HELLOIID
router.get('/helloid-one-user', (req, res) => {
  // using async/await
  GetOneHelloidUSer(`https://moi.helloid.com/api/v1`, {
    contactEmail: 'test@test.com',
    helloidApiPass: 'HEGDdxKCtQrNefjZdYqXbpwUPBVAZnrS',
    helloidApiUser: 'ZZTKAQPCJNSUSSZXEAENQELRPZQBNRZA',
  })
    .then(result => {
      console.log('then');
      res.send(result);
    })
    .catch(error => {
      console.log('catch');
      res.send(error);
    });
});

router.get('/get-group', async (req, res) => {
  console.log('get group');
  const groups = await getHelloIdGroup(`https://moi.helloid.com/api/v1`, {
    groupName: '',
    helloidApiPass: 'HEGDdxKCtQrNefjZdYqXbpwUPBVAZnrS',
    helloidApiUser: 'ZZTKAQPCJNSUSSZXEAENQELRPZQBNRZA',
  });

  res.send(groups);
});

router.get('/getuser-group', async (req, res) => {
  console.log('get group');
  const groups = await userGroups(`https://gcr-pharma.helloid.com/api/v1`, {
    userGuid: '2606cc65-ba4a-4249-a045-48729c954c11',
    helloidApiPass: 'mvShYFMenhRUQcxwhqWxjdadUgqFPzza',
    helloidApiUser: 'FZBUPPGBEAVWTSZJTYLFECEUDHZHCEDL',
  });

  res.send(groups);
});

import moment = require('moment');
router.get('/get-moment', (req, res) => {
  const momentTime = moment().format('YYYY-MM-DD HH:mm');
  res.send(momentTime);
});

// automaticCrud

router.get('/automatic-crud', (req, res) => {
  automaticCrud();
  res.send('automatic crud');
});

router.post('/get-user', async (req, res) => {
  const { body } = req;
  console.log('body ', body);
  const service = new ApiService(apiUrl);
  const me = await service.querySearch(userSearchById, {
    idGroupement: body.groupementId,
    iduser: body.id,
  });
  console.log('me ', me.data.search.data);
  res.send(me.data);
});

module.exports = router;
