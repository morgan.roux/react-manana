import {
  ApiService,
  ssologinMutation,
  ActualUser,
  helloIdSsoByGroupId,
} from '../service/ApiService';
import { apiUrl, clientUrl } from '../config/config';
const express = require('express');
const passport = require('passport');
const router = express.Router();
const bodyParser = require('body-parser');

const SamlStrategy = require('passport-saml').Strategy;
const MultiSamlStrategy = require('passport-saml/multiSamlStrategy');
passport.serializeUser(function(user, done) {
  done(null, user);
});
passport.deserializeUser(function(user, done) {
  done(null, user);
});

// passport.use(new SamlStrategy(
//   {
//     path: '/sp-acs',
//     entryPoint: `${helloidUrl}`,
//     issuer: 'passport-saml',
//   },
//   async function(profile, done) {
//     console.log(profile);
//     try{
//       const ssologin = await new ApiService(apiUrl).ssoLogin(profile.nameID,
//           profile.issuer, profile.sessionIndex, profile.nameIDFormat, profile.givenname, ssologinMutation);
//       return done(null, {
//           token : ssologin.ssologin.accessToken
//       });
//     }
//     catch(err){
//       done(null, false, {message: err});
//     }
//   })
// );

passport.use(
  new MultiSamlStrategy(
    {
      passReqToCallback: true, // makes req available in callback
      async getSamlOptions(req, done) {
        const provider = {
          path: '/sp-acs',
          entryPoint: `https://moi.helloid.com`,
          issuer: 'passport-saml',
          // cert: res.helloIdSso.x509Certificate,
        };
        return done(null, provider);
      },
    },
    async function(req, profile, done) {
      const ssologin = await new ApiService(apiUrl).ssoLogin(
        profile.nameID,
        profile.issuer,
        profile.sessionIndex,
        profile.nameIDFormat,
        profile.givenname,
        ssologinMutation,
      );
      return done(null, {
        token: ssologin.ssologin.accessToken,
      });
    },
  ),
);

// router.post(
// 	'/sp-acs',
// 	bodyParser.urlencoded({ extended: false }),
// 	passport.authenticate('saml', { failureRedirect: `/error-login/sso-moi` }),
// 	function(req, res) {
// 		console.log(req.user);
// 		res.redirect(`${clientUrl}/sso-signin/${req.user.token}`);
// 	}
// );

router.post('/sp-acs', bodyParser.urlencoded({ extended: false }), function(req, res, next) {
  passport.authenticate('saml', function(err, user, info) {
    console.log('info ', info);
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.redirect(`/error-login/idapp1`);
    }
    req.logIn(user, function(err) {
      if (err) {
        return next(err);
      }
      return res.redirect(`${clientUrl}/sso-signin/${req.user.token}`);
    });
  })(req, res, next);
});

router.get(
  '/sp-login',
  passport.authenticate('saml', { failureRedirect: `/error-login` }),
  function(req, res) {
    res.redirect('/');
  },
);

router.get('/error-login/:subdomain', function(req, res) {
  res.redirect(`/log-out/${req.params.subdomain}`);
});

// router.get('/log-out/:subdomain',
//   function(req, res) {
//     console.log("userid ", req.params.token);
//     console.log("query params ", req.query);
//     const service =  new ApiService(apiUrl);
//     let me = await service.actualuser(ActualUser, req.params.token);
//     let helloidInfo = await service.helloIdSsoDetailsByGroup(helloIdSsoByGroupId, me.me.groupement.id);

//     .then(async result =>{
//       res.redirect(`${result.helloIdSso.helloIdUrl}/authentication/signoff`);
//     })
//     .catch(err =>{
//       console.log(err);
//       res.send("error log off");
//     });

//   }
// );

// router.get('/get-sso-details', async (req, res) =>{
//   const helloidinfo = await new ApiService(apiUrl).helloIdSsoDetails(helloIdSsoByDomain, "sso-moi");
//   res.send(helloidinfo);
// });

module.exports = router;
