FROM openjdk:latest
USER root

RUN apk update
# RUN apk fetch openjdk10
# RUN apk add openjdk10


# RUN apk add openjdk11 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community

# ENV JAVA_HOME=/usr/java/jdk-11.0.6
# ENV PATH="$JAVA_HOME/bin:${PATH}"

COPY package.json /saml-sso/package.json

WORKDIR /saml-sso

ARG REFRESHED_AT
ENV REFRESHED_AT $REFRESHED_AT

COPY zulu11.37.17-ca-jdk11.0.6-linux_x64.tar.gz /saml-sso

RUN mkdir jdk-11

RUN tar zxf zulu11.37.17-ca-jdk11.0.6-linux_x64.tar.gz -C jdk-11 \
    && ln -s jdk-11/zulu11.37.17-ca-jdk11.0.6-linux_x64/bin/java \
    && rm -f zulu11.37.17-ca-jdk11.0.6-linux_x64.tar.gz

# ENV JAVA_HOME=/saml-sso/jdk-11/zulu11.37.17-ca-jdk11.0.6-linux_x64
# ENV JAVA_TOOL_OPTIONS="-XX:+IgnoreUnrecognizedVMOptions -XX:+UseContainerSupport -XX:+IdleTuningCompactOnIdle -XX:+IdleTuningGcOnIdle"
# ENV PATH="$JAVA_HOME/bin:$PATH"  

# RUN apk add --repository http://dl-cdn.alpinelinux.org/alpine/edge/main/ --no-cache \
#   nodejs \
#   nodejs-npm \
#   yarn \
#   curl \
#   lftp 



RUN java -version
RUN javac -version

# RUN yarn install

# COPY . /saml-sso
ENTRYPOINT ["tail", "-f", "/dev/null"]

# ENTRYPOINT ["npm", "run", "start:dev"]
