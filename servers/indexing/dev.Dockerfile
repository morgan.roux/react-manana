FROM node:10-alpine

COPY package.json /indexing/package.json

WORKDIR /indexing

RUN npm install

COPY . /indexing

ENTRYPOINT [ "./startup_dev.sh" ]
