import { Client } from '@elastic/elasticsearch'
import { connect } from 'amqplib'
import { Indexing } from './indexing'

import logger from './logging'
import fs = require('fs')
import fetch = require('isomorphic-fetch')

require('dotenv').config()

const elasticClient = new Client({
  node: process.env.ELASTIC_SEARCH_NODE
});


const parseResponse = (task) => {
  return (response) => {
    if (!response.ok) {
      try {
        return response
          .text()
          .then(r => {
            logger.error(`${task} err`, {
              response,
              function: 'parseResponseFor',
              source: 'parse-response'
            })
          })
          .catch(err => {
            logger.error(`${task} err ${err}, ${JSON.stringify(response.body, null, 3)}`, {
              err,
              function: 'parseResponseFor',
              source: 'parse-response'
            })
            throw err
          })
      } catch (err) {
        logger.error(`${task} err`, err)
        throw err
      }
    }
    if (response.status !== 200) logger.error(`response for ${task} with status ${response.status}`, {
      status: response.status,
      statusText: response.statusText,
      function: 'parseResponseFor',
      source: 'parse-response'
    })
    return response.json()
  }
}


const createIndices = (indexing: Indexing, indexerConfigs: any): Promise<any> => {
  return Promise.all([...new Set(indexerConfigs.map(({ index }: any) => index))].map((index: string) => {
    return indexing.getElasticSearchClient()

      .indices
      .exists({ index })
      .then(response => {
        return response.body ? Promise.resolve() : indexing.getElasticSearchClient().indices.create({ index }).then(_ => Promise.resolve())
      })
  }))
}

const deployIndexers = (indexing: Indexing) => {

  fs.readFile('./config.json', { encoding: 'utf-8' }, (error, data) => {
    if (error) {
      logger.error(`Register all indexers error : ${error.message}`)
      return
    }

    try {
      const indexerConfigs = JSON.parse(data)

      createIndices(indexing, indexerConfigs)
        .then(creationIndices => {
          indexerConfigs.forEach(({ routingKey, index, instances, query, dataKey, mappingProperties }: any) => {
            /**
             * Every indexer config must contain : 
                   * Every indexer config must contain : 
             * Every indexer config must contain : 
             *  - routingKey : The rabbitMq channel routingKey
             *  - index : the elasticsearch index
             *  - type : the elasticsearch type
             *  - instances : The number of indexers to create.
             *  - query : The graphql query to fetch data by id.
             *  - dataKey : The response data Key
             *  */

            logger.info(`Register routingKey=${routingKey}, index=${index}, instances=${instances}, dataKey=${dataKey}`)

            indexing.deployIndexer({ routingKey, index, instances, mappingProperties }, (id: any) => {
              const variables = { id }
              return fetch(process.env.API_URL, {
                method: 'POST',
                headers: {
                  clientId: process.env.API_CLIENT_ID,
                  clientSecret: process.env.API_CLIENT_SECRET,
                  'Content-Type': 'application/json',
                  Accept: 'application/json'
                },
                body: JSON.stringify({ variables, query })
              }).then(parseResponse(dataKey))
                .then(response => response.data[dataKey])

            })

          })

        })

    }
    catch (parsingError) {
      logger.error(`Register all indexers error : ${parsingError.message}`)
    }
  })


}


connect(process.env.RABBITMQ_HOST)
  .then(connection => {
    logger.info(`RebbitMq connected.`)
    new Indexing(elasticClient, connection).start().then(indexing => deployIndexers(indexing))

    return Promise.resolve()
  })
  .catch(error => logger.error(`Something wrong, please check config.json? ${error.message}`))