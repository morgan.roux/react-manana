import { createLogger, transports, format } from 'winston'

export default createLogger({
    level: 'info',
    format: format.json(),
    defaultMeta: { service: 'indexer-service' },
    transports: [
        // new transports.File({ filename: 'error.log', level: 'error' }),
        // new transports.File({ filename: 'combined.log' })
        new transports.Console({ format: format.simple() })
    ]
})