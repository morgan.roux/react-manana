import Indexing from './Indexing'
import { Channel, Message } from 'amqplib'
import logger from '../logging'


export interface MappingProperties {
    [key: string]: any
}

interface IndexerOptions {
    routingKey: string
    index: string
    mappingProperties: MappingProperties
    indexing: Indexing
    fetcher: (id: any) => Promise<any>

}


export default class Indexer {
    options: IndexerOptions
    constructor(options: IndexerOptions) {
        this.options = options
        this._init()
    }

    _init = () => {
        const { indexing, routingKey, index, mappingProperties } = this.options
        indexing.addConsumer(routingKey, this.onMessage)

        if (mappingProperties) {
            indexing.getElasticSearchClient()
                .indices.putMapping({
                    index,
                    body: {
                        properties: mappingProperties

                    }
                })
        }
    }

    /**
     * We consumes message here. The message must contains : 
     *  - action : "DELETE" OR "SAVE"
     *  - id : The id of element.
     * 
     * 
     */
    onMessage = (channel: Channel, message: Message) => {
        if (message != null) {
            const { routingKey } = this.options
            const { content, fields } = message
            if (routingKey !== fields.routingKey) {
                return
            }

            logger.info(`[${routingKey}] Message arrived on queue : ${content.toString()}`)
            const { action, id }: any = JSON.parse(content.toString())

            switch (action) {
                case "DELETE":
                    this._logAndAckMessageIfSucceeded({ channel, message, id, action, result: this._deleteDocument(id) })
                    break;
                case "SAVE":
                    this._logAndAckMessageIfSucceeded({ channel, message, id, action, result: this._saveDocument(id) })
                    break;
                default:
                    logger.error(`[${routingKey}] Unknow action "${action}"`)
            }

        }
    }

    _logAndAckMessageIfSucceeded = ({ channel, message, action, id, result }: any) => result.then(_ => {
        const { index, routingKey } = this.options
        logger.info(`[${routingKey}] ${action} ${index}/_doc/${id} OK`)
        channel.ack(message)
    })
        .catch((error: any) => {
            const { index, routingKey } = this.options
            logger.info(`[${routingKey}] ${action} ${index}/_doc/${id} FAILED : ${error.message}`)
        })

    _deleteDocument = (id: any): Promise<any> => {
        const { indexing, index } = this.options
        return indexing.getElasticSearchClient().delete({ index, id })
    }

    _saveDocument = (id: any): Promise<any> => {
        const { index, indexing, fetcher } = this.options
        return fetcher(id).then(body => {
            delete body.id
            return indexing.getElasticSearchClient()
                .index({
                    index, id, body
                }).then(_ => indexing.getElasticSearchClient().indices.refresh({ index }))
        })
    }
} 