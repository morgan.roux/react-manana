import { describe, it, before, after } from 'mocha'
import { expect } from 'chai'
import faker = require('faker')

import { Client } from '@elastic/elasticsearch'
import { connect } from 'amqplib'
import { Indexing } from '..'

require('dotenv').config()

describe('Indexing Test', function () {
    let indexingTest: Indexing
    before(done => {
        const elasticClient = new Client({
            node: process.env.ELASTIC_SEARCH_NODE
        })
        connect(process.env.RABBITMQ_HOST)
            .then(connection => {


                new Indexing(elasticClient, connection).start().then(indexing => {
                    indexingTest = indexing
                    done()
                })

            })
    })

    after(done => {
        indexingTest.close()
        done()
    })




    it('Test publisher/consumer', done => {

        const TEST_ROUTING_KEY = 'test_routing_key'
        const TEST_MESSAGE = JSON.stringify({ action: faker.random.arrayElement(['SAVE', 'DELETE']), id: faker.random.number() })
        indexingTest.addConsumer(TEST_ROUTING_KEY, (channel, message) => {
            if (message != null) {
                expect(message.content.toString(), TEST_MESSAGE)
                expect(message.fields.exchange, process.env.RABBITMQ_EXCHANGE)
                expect(message.fields.routingKey, TEST_ROUTING_KEY)
                channel.ack(message)
                done()
            }
        })


        indexingTest.getRabbitMqConnection().createChannel()
            .then(channel => {
                channel.publish(process.env.RABBITMQ_EXCHANGE, TEST_ROUTING_KEY, Buffer.from(TEST_MESSAGE))
            })

    });


    it.only('Test indexing', done => {
        const indexTest = 'index_test'
        const routingKeyTest = 'routing_key_test'
        const idTest = '12'

        const fetcherTest = (id: any): Promise<any> => Promise.resolve({ id, name: faker.random.word(), firstName: faker.random.word() })
        indexingTest.deployIndexer({ routingKey: routingKeyTest, index: indexTest }, fetcherTest)

        indexingTest.getRabbitMqConnection().createChannel()
            .then(channel => {
                channel.assertQueue(routingKeyTest)
                channel.sendToQueue(routingKeyTest, Buffer.from(JSON.stringify({ action: 'SAVE', id: idTest })))
            })

        indexingTest.getElasticSearchClient()
            .get({ index: indexTest, id: idTest })
            .then(result => {
                console.log(result)
                //  done()
            })
    })
});