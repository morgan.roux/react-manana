import { Client } from '@elastic/elasticsearch'
import { Connection } from 'amqplib'
import { Channel, Message } from 'amqplib'
import logger from '../logging'
import Indexer, { MappingProperties } from './Indexer'

interface IndexerDeploymentOptions {
    routingKey: string
    index: string
    mappingProperties?: MappingProperties
    instances?: number | 1
}

export default class Indexing {
    elasticSearchClient: Client
    rabbitMqConnection: Connection
    channel: Channel
    constructor(elasticSearchClient: Client, rabbitMqConnection: Connection) {
        this.elasticSearchClient = elasticSearchClient
        this.rabbitMqConnection = rabbitMqConnection
        this._addCloseHook()
    }


    deployIndexer = (options: IndexerDeploymentOptions, fetcher: (id: any) => Promise<any>): Indexing => {
        const { routingKey, index, instances, mappingProperties } = options

        for (let i = 0; i < instances; i++) {
            new Indexer({ routingKey, index, mappingProperties, indexing: this, fetcher })
        }
        return this
    }

    getElasticSearchClient = (): Client => this.elasticSearchClient

    getRabbitMqConnection = (): Connection => this.rabbitMqConnection

    getChannel = (): Channel => this.channel

    start = (): Promise<Indexing> => {
        return this.rabbitMqConnection.createChannel()
            .then(channel => {
                channel.assertExchange(process.env.RABBITMQ_EXCHANGE, process.env.RABBITMQ_EXCHANGE_TYPE, { durable: false })
                this.channel = channel
                logger.info(`Starting Indexing declare exchange ok : exchange=${process.env.RABBITMQ_EXCHANGE}, type=${process.env.RABBITMQ_EXCHANGE_TYPE}`)
                return this
            })
    }
    close = () => {
        this.rabbitMqConnection.close()
        this.elasticSearchClient.close()
        logger.info(`RabbitMq and ElasticSearch Connection closed`)
    }

    addConsumer = (routingKey: string, consumer: (channel: Channel, message: Message) => void): Indexing => {
        logger.info(`Add new consumer on channel ${routingKey}`)
        this.channel.assertQueue('', { exclusive: true })
            .then(queue => {
                this.channel.bindQueue(queue.queue, process.env.RABBITMQ_EXCHANGE, routingKey)
                this.channel.consume(queue.queue, message => consumer(this.channel, message), { noAck: false })

                logger.info(`[${routingKey}] Binding queue ok`)
            })
            .catch(error => logger.error(`[${routingKey}] Failed to add new consumer : ${error.message}`))

        return this
    }

    _addCloseHook = () => {
        process.on(
            'SIGINT',
            () => {
                this.close()
                process.exit(1)
            }
        )
    }




} 