import { readdirSync, lstatSync } from 'fs';
import { resolve } from 'path';
import {isImageFile} from './FileHelper'

class PhotoFinder {
  find = (rootPath: string): string[] => {
        if (!lstatSync(rootPath).isDirectory()) {
          return isImageFile(rootPath) ? [rootPath] : [];
        }

        const fileNames = readdirSync(rootPath);
        return fileNames.reduce((result, currentFileName) => {
          return [...result, ...this.find(resolve(rootPath, currentFileName))];
        }, []);
  };

}

export default PhotoFinder