// tslint:disable-next-line: no-var-requires
require('dotenv').config();

import { resolve } from 'path';
import PhotoFinder from './PhotoFinder';
import DAO from './DAO';
import { extractProductCode } from './FileHelper';
import { writeFileSync } from 'fs';
import { uploadFile } from './Uploader';

// Change it if needed
const PRODUCT_PHOTOS_DIR = resolve(__dirname, '../product-photos');
const REPORT_DIR = resolve(__dirname, '../reports');
const PRODUCT_S3_DIRECTORY = 'produits';

const dao = new DAO();

const doUploadAndSave = async (code: string, filePath: string): Promise<any> => {
  const s3FileKey = `${PRODUCT_S3_DIRECTORY}/${filePath.substring(filePath.lastIndexOf('/') + 1)}`;

  console.log('Uploading photo ' + filePath + ' for ' + code);
  return uploadFile(filePath, s3FileKey).then(uploading => {
    return dao.saveProductPhoto(code, s3FileKey);
  });
};

const updatePhotos = async () => {
  console.log('*******************', process.env.DATABASE);
  try {
    await dao.connect();
  } catch (error) {
    console.log('*************************Error connect', error);
  }
  const productPhotoAbsPaths = new PhotoFinder().find(PRODUCT_PHOTOS_DIR);
  const found = [];
  const notFound = [];
  for (const photo of productPhotoAbsPaths) {
    try {
      const code = extractProductCode(photo);

      const isExists = await dao.isProductExists(code);

      if (isExists) {
        found.push(code);
        await doUploadAndSave(code, photo);
      } else {
        notFound.push(code);
      }
    } catch (error) {
      console.log('************************************Error', error);
    }
  }

  // Reports
  writeFileSync(resolve(REPORT_DIR, 'found-product.txt'), found.join('\n'), { encoding: 'utf-8' });
  writeFileSync(resolve(REPORT_DIR, 'not-found-product.txt'), notFound.join('\n'), {
    encoding: 'utf-8',
  });
};

// doUploadAndSave("3401021343195","/home/tonig/PROJECT/repositories/gcr-pharma/servers/ops/product-photos/Accessoires et orthopédies (codage non valide)/3401021343195.jpg" )
updatePhotos();
