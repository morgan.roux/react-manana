import {S3} from 'aws-sdk';
import {readFileSync} from 'fs';

const s3 = new S3({
    accessKeyId:process.env.AWS_S3_ACCESS_KEY,
    secretAccessKey:process.env.AWS_S3_SECRET_KEY,
    region:process.env.AWS_S3_REGION
}) 

export const uploadFile=(filePath:string,s3FileKey:string): Promise<string> => {
    const Body = readFileSync(filePath);

    return new Promise((resolve,reject)=>{
         s3.upload(
           {
             Bucket: process.env.AWS_S3_BUCKET,
             Key: s3FileKey,
             Body,
           },
           (err, data) => {
             if (err) {
               reject(err);
             } else {
               resolve(data.Location);
             }
           },
         );
    })
}