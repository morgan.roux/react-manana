export const isImageFile = (path: string): boolean => {
  return path.endsWith('jpg') || path.endsWith('png') || path.endsWith('jpeg');
};

export const extractProductCode = (file:string):string => {
    const lastIndexDirSepataror = file.lastIndexOf('/')
    const indexOfDot= file.lastIndexOf('.')
    return file.substring(lastIndexDirSepataror+1,indexOfDot)
}