import Pool = require('pg-pool');
// tslint:disable-next-line: no-var-requires
const uniqid = require('uniqid');
import moment = require('moment');

interface Query {
  name: string;
  text: string;
  values?: any[];
}

const generateId = (): string => {
  return `auto${uniqid()}${uniqid()}`.substring(0, 25);
};

const getNow = (): string => {
  return moment().format('YYYY-MM-DD HH:mm');
};

export default class DAO {
  /**

SEQUELIZE_HOST=localhost
SEQUELIZE_PORT=5432
SEQUELIZE_USERNAME=pharma
SEQUELIZE_PASSWORD=Pharma@123456789
SEQUELIZE_DATABASE=prisma
SEQUELIZE_SCHEMA=public

 */
  private connection = new Pool({
    host: process.env.SEQUELIZE_HOST,
    port: +process.env.SEQUELIZE_PORT,
    user: process.env.SEQUELIZE_USERNAME,
    password: process.env.SEQUELIZE_PASSWORD,
    database: process.env.SEQUELIZE_DATABASE,
  });

  async connect() {
    return this.connection.connect();
  }

  isProductExists = async (code: string): Promise<boolean> => {
    return (await this.getProduct(code)) !== null;
  };

  getProduct = async (code: string): Promise<any> => {
    const query: Query = {
      name: `find-product-${code}`,
      text: `SELECT public."Produit"."id" FROM public."Produit"	INNER JOIN public."Produit_code" ON public."Produit_code"."id_produit"=public."Produit"."id"	WHERE public."Produit_code"."code"=$1`,
      values: [code],
    };

    try {
      const result = await this.connection.query(query);

      console.log(
        '****************************************query',
        `SELECT public."Produit"."id" FROM public."Produit"	INNER JOIN public."Produit_code" ON public."Produit_code"."id_produit"=public."Produit"."id"	WHERE public."Produit_code"."code"=$1`,
        result,
      );
      return result.rowCount > 0 ? result.rows[0] : null;
    } catch (error) {
      console.log('***************************************error', error);
      return null;
    }
  };

  saveProductPhoto = async (code: string, filePath: string): Promise<any> => {
    try {
      const product = await this.getProduct(code);
      const fileId: string = await this.saveFile(filePath);
      const articlePhotoId: string = await this.saveArticlePhoto(product.id, fileId);

      return articlePhotoId;
    } catch (error) {
      return error;
    }
  };

  private saveArticlePhoto = async (articleId: string, fileId: string): Promise<string> => {
    const id = generateId();
    const query: Query = {
      name: `save-article-photo-${articleId}`,
      text: `INSERT INTO public."Produit_photo"(id, date_creation, date_modification, id_fichier, id_produit)	VALUES ($1, $2, $2, $3,$4)`,
      values: [id, getNow(), fileId, articleId],
    };

    await this.connection.query(query);
    return id;
  };

  private saveFile = async (filePath: string): Promise<string> => {
    const id = generateId();
    const query: Query = {
      name: `save-file-${filePath}`,
      text: `INSERT INTO public."Fichier"(id, chemin, nom_original, type, date_creation, date_modification, id_avatar)	VALUES ($1, $2, $3, $4, $5, $5, NULL)`,
      values: [id, filePath, filePath.substring(filePath.lastIndexOf('/') + 1), 'PHOTO', getNow()],
    };

    await this.connection.query(query);
    return id;
  };
}
