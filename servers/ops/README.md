## How to use

1. Install dependencies
```sh
 yarn install
```

2. Copy photos into "product-photos" directory

3. Copy .env.example into .env then edit it.

4. Run upload

```sh
yarn run yarn update-product-photos
```

5. Show reports into "reports" directory