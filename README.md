Stack: Nodejs, React, Apollo, Prisma, Typescript, Postgres

### Lancement du backend

Aller dans le dossier `servers/graphql` et copier le fichier `.env.example` comme `.env`

```
$ docker-compose up -d graphql
```

API

```
$ cd servers/graphql
$ npm install
$ npm run generate:dev
```

### Populate database

Supprimer tous les données de la base

```
$ npm run prisma:reset
```

Supprimer le schema de la base

```
$ npm run prisma:delete
```

Remplir la base de données

```
$ npm run prisma:seed
```

### Edit schema.graphql

En cas d'ajout de query ou mutation graphql, lancer la commande suivante pour mettre à jour les types géneré

```
$ npm run graphql:schema
```

### Search API

- Sort : https://www.elastic.co/guide/en/elasticsearch/reference/7.4/search-request-body.html#request-body-search-sort
- Filter : https://www.elastic.co/guide/en/elasticsearch/reference/current//query-filter-context.html
- Query : https://www.elastic.co/guide/en/elasticsearch/reference/current//search.html

```
query {
  search(
    type: ["laboratoire"]
    query: "CHIM",
    skip:2
    take:2,
    sortBy:[{code:{order:"desc"}}],
    filterBy:[{term:{ codeGroupLabo:"9999"}}]
  ) {
    total
    data {
      ... on Laboratoire {
        type
        id
        code
        nom
        codeGroupLabo
      }
    }
  }
}
```

```
query {
  search(
    type: ["laboratoire"]
    query: "CHIM",
    skip:2
    take:2
  ) {
    total
    data {
      ... on Groupement {
        id
        type
      }
      ... on Utilisateur {
        id
        email
        type
      }
      ... on Laboratoire {
        type
        id
        code
        nom
        codeGroupLabo
      }
    }
  }
}

```

### Lancement du front

Aller dans le dossier `client/web` et copier le fichier `.env.example` en `.env.local`

```
$ npm install
$ npm run start
```

En cas d'ajout de query graphql, lancer la commande suivante pour mettre à jour les types géneré (voir notre wiki http://wiki.digitalbusiness4ever.com)

```
$ npm run graphql:type
```

### Extension pour le formatage automatique des codes

#### WebStorm

https://prettier.io/docs/en/webstorm.html#running-prettier-on-save-using-file-watcher

#### VSCode

https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode

### Note

Avant la mise en production, il faut changer l'url des liens dans les templates des mails dans sparkpost
