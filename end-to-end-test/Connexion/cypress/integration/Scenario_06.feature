Feature: Connexion

    Je veux créer un virement sans récurrence

    Background: Résolution
        Given Résolution : 1366 x 768

    Scenario: Créer un virement sans récurrence
        Given Je suis sur la page de connexion
        When Je saisis "beneficiaire@yopmail.com" dans le champ E-mail et "Abcd1234!!" dans le champ mot de passe
        When Je clique sur le bouton Se connecter
    
    
        Given Je vais dans le formulaire virement
        When Je choisis un bénéficiaire
        When Je met un intitulé "Virement sans récurrence" et un montant "2" Euro
        When Je choisis une catégorie
        When Je saisis le code validation