/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion du BO`,() =>{
            cy.visit("https://office-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(7000)
        })
    //Scenario: Consulter les détails d'une transaction
        Given (`Je vais dans le menu Transactions`,() =>{
            cy.xpath("//strong[contains(text(),'Transaction')]").click({force: true})
            cy.wait(3000)
        })
        When (`J'effectue des recherches dans la liste`,() =>{           
            // Recherche par type
            cy.get('input[id="searchfield"]').type('Sortant')
            cy.wait(2000)
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
            cy.wait(3000)
            // Vider le champ
            cy.get('input[id="searchfield"]').clear()
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
            cy.wait(3000)
            // Recherche par date
            cy.get('input[id="searchfield"]').type('21 août 2020')
            cy.wait(2000)
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
            cy.wait(3000)
            // Vider le champ
            cy.get('input[id="searchfield"]').clear()
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
            cy.wait(3000)
            // Recherche par montant
            cy.get('input[id="searchfield"]').type('23,988')
            cy.wait(2000)
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
            cy.wait(3000)
            // Vider le champ
            cy.get('input[id="searchfield"]').clear()
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
            cy.wait(3000)
            // Recherche par état
            cy.get('input[id="searchfield"]').type('En attente')
            cy.wait(2000)
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
            cy.wait(3000)
            // Vider le champ
            cy.get('input[id="searchfield"]').clear()
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
            cy.wait(3000)
            // Recherche par émetteur
            cy.get('input[id="searchfield"]').type('FRANCE RAVAL')
            cy.wait(2000)
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
            cy.wait(3000)
             // Vider le champ
             cy.get('input[id="searchfield"]').clear()
             cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
             cy.wait(3000)
             // Recherche par bénéficiaire
            cy.get('input[id="searchfield"]').type('CHALVIN PARIS')
            cy.wait(2000)
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
            cy.wait(3000)
            // Vider le champ
            cy.get('input[id="searchfield"]').clear()
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
            cy.wait(3000)
             // Recherche par modalité
             cy.get('input[id="searchfield"]').type('Virement')
             cy.wait(2000)
             cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
             cy.wait(3000)



        })
        When (`Je consulte les détails de la transaction`,() =>{
            cy.xpath("//tr[4]//td[1]").click({force: true})
            cy.wait(3000)
        })