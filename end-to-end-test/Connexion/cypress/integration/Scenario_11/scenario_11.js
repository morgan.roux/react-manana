/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion`,() =>{
            cy.visit("https://dashboard-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(15000)
        })
    //Scenario : Créer un virement sans récurrence
        Given (`Je clique sur l'icône cloche`,() =>{
             // cloche notification
            cy.get('button[aria-label="notification"]').click()
            cy.wait(2000)
        })
        When (`Je consulte mes 3 dernière notifications`,() =>{
            cy.xpath("//span[1]//div[1]//div[1]//p[1]").click({force: true})
            cy.wait(4000)
            cy.xpath("//span[2]//div[1]//div[1]//p[1]").click({force: true})
            cy.wait(4000)
            cy.xpath("//span[3]//div[1]//div[1]//p[1]").click({force: true})
            cy.wait(2000)
        })