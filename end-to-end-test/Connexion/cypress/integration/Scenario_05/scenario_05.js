/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Ouvrir la page de connexion
        Given (`Je visite la page de connexion`,() =>{
            cy.visit("https://preprod.gcr-pharma.com/");
        })
        
        Then (`La page de connexion s'affiche`,() =>{
            cy.wait(2000)
        })

    //Scenario: Se connecter
         
        Given (`Je suis sur la page de connexion`,() =>{
            cy.wait(2000)
        })
        
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ Mot de passe`,(mail,mdp) =>{
            cy.get('input[name="email"]').type(mail)
            cy.get('input[name="password"]').type(mdp)
        })
             
          
             
            When (`Je clique sur le bouton Se connecter`,() =>{
                cy.xpath("//body/div[@id='root']/div/div/div/div/form/button[1]").click({force: true})
                cy.wait(2000)
            })
         
         Then (`Je suis connecté et je vois la page principale s'afficher`,() =>{
             cy.wait(5000)
        })
        //Scenario: Ajouter une promotion
         
        Given (`Je vais dans paramètres puis gestion des promotions`,() =>{
            cy.xpath("//body/div[@id='root']/div/header/div/div/button[2]/span[1]").click({force: true})
            cy.wait(2000)
            cy.xpath("//div[@id='primary-search-account-menu']//div//li[contains(text(),'Gestion des promotions')]").click({force: true})
            cy.wait(2000)
            cy.xpath("//span[contains(text(),'Ajouter une promotion')]").click({force: true})
            cy.wait(2000)        

        })
        When (`Je remplis les informations de base`,() =>{
            
            cy.get('input[name="nom"]').type('promotion test')
            cy.wait(1000)
            cy.get('input[name="dateDebut"]').type('03/09/2020')
            cy.wait(1000)
            cy.get('input[name="dateFin"]').type('03/09/2021')
            cy.wait(1000)
            cy.get('#mui-component-select-promotionType').click({force: true})
            cy.wait(1000)
            cy.contains("BONNE AFFAIRE").click({force: true})
            cy.wait(1000)
            cy.get('#mui-component-select-codeCanal').click({force: true})
            cy.wait(1000)
            cy.contains("PLATEFORME").click({force: true})
            cy.wait(1000)
            cy.xpath("//span[contains(text(),'Suivant')]").click({force: true})
            cy.wait(10000) 

        })
        When (`Je sélectionne des articles`,() =>{
            // filtre laboratoire
            cy.get('input[placeholder="Rechercher laboratoire"]').type('essity')
            cy.wait(1000)
            cy.xpath("//div[@id='root']//div//main//div//div//div//div//div//div//div//div//div//li//div//div//span//span//input").click({force: true})
            cy.wait(5000) 
            cy.xpath("/tr[1]//td[1]//span[1]//span[1]//input[1]").click({force: true})
            cy.wait(5000) 
            // filtre famille
            cy.contains("Réinitialiser les filtres").click({force: true})
            cy.wait(3000)
            cy.get('input[placeholder="Rechercher famille"]').type('solaire')
            cy.wait(1000)
            cy.xpath("//body//div[@id='root']//div//div//div//div//div//div//div//div//div//div//li[1]//div[1]//div[1]//span[1]//span[1]//input[1]").click({force: true})
            cy.wait(1000) 
            cy.xpath("//tr[1]//td[1]//span[1]//span[1]//input[1]").click({force: true})
            cy.wait(3000)
            // champ de recherche
            cy.contains("Réinitialiser les filtres").click({force: true})
            cy.wait(5000)
            cy.get('input[placeholder="Rechercher un article"]').type('vit c')
            cy.wait(1000)
            cy.xpath("//tr[1]//td[1]//span[1]//span[1]//input[1]").click({force: true})
            cy.wait(3000)
            // Récap des articles
            cy.contains("Afficher les éléments selectionnés").click({force: true})
            cy.wait(5000)
            cy.contains("Suivant").click({force: true})
            cy.wait(5000)


        })
        When (`Je définis un palier`,() =>{
            cy.xpath("//tr[1]//td[6]//button[1]").click({force: true})
            cy.wait(2000)

        })
        When (`Je sélectionne des groupes de client`,() =>{
            cy.xpath("//body/div[@id='root']/div/div/div/div/form/button[1]").click({force: true})
            cy.wait(2000)
            // entre 150 et 299
            cy.get('input[name="quantiteMin"]').type('150')
            cy.wait(1000)
            cy.get('input[name="quantiteMax"]').type('299')
            cy.wait(1000)
            cy.get('input[name="pourcentageRemise"]').type('0')
            cy.wait(1000)
            cy.get('input[name="remiseSupplementaire"]').type('5')
            cy.wait(1000)
            cy.contains("Ajouter").click({force: true})
            cy.wait(1000)
            // entre 300 et 599
            cy.get('input[name="quantiteMin"]').type('300')
            cy.wait(1000)
            cy.get('input[name="quantiteMax"]').type('599')
            cy.wait(1000)
            cy.get('input[name="pourcentageRemise"]').type('5')
            cy.wait(1000)
            cy.get('input[name="remiseSupplementaire"]').type('5,25')
            cy.wait(1000)
            cy.contains("Ajouter").click({force: true})
            cy.wait(1000)
            cy.xpath("//html//body//div//div//div//header//div//div//button").click({force: true})
            cy.wait(2000)

        })
        
        