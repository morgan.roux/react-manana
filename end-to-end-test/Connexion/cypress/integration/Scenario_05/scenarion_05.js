/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion`,() =>{
            cy.visit("https://dashboard-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(15000)
        })

        // Relevé de compte
        Given (`Je vais sur la modale relevé de compte`,() =>{
            cy.xpath("//strong[contains(text(),'Transactions')]").click()
            cy.wait(10000)
            cy.contains("Télécharger relevé").click({force: true})
            cy.wait(2000)
        })
        When (`Je filtre par modalité`,() =>{
            // Modalité
            cy.get('#modality').click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Virement')]").click({force: true})
            cy.wait(2000)
        })
        When (`Je filtre par type`,() =>{
            // Type
            cy.get('#modalityType').click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Entrant')]").click({force: true})
            cy.wait(2000)
        })
        When (`Je filtre par membre`,() =>{
            // Membre
            cy.get('#statementMember').click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'CANDICE VICTOIRE DELPHINE CHALVIN')]").click({force: true})
            cy.wait(2000)
        })
        When (`Je filtre par membre`,() =>{
            // Membre
            cy.get('#statementMember').click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'CANDICE VICTOIRE DELPHINE CHALVIN')]").click({force: true})
            cy.wait(2000)
        })
        When (`Je filtre par période`,() =>{
            // Période
        cy.get('#statementPeriod').click({force: true})
        cy.wait(2000)
        cy.contains("Date").click({force: true})
        cy.wait(2000)
        cy.get('#date-picker-inline')
        .eq(0)
        .click({force: true})
        cy.wait(2000)
        cy.xpath("//body/div/div/div/div/div/div/div[2]/div[5]/button[1]").click({force: true})
        cy.wait(2000)
        cy.xpath("//body/div/div/div/div/div/div/div/div[2]/div[1]/div[1]/input[1]").click({force: true})
        cy.wait(2000)
        cy.xpath("//body/div/div/div/div/div/div/div[2]/div[5]/button[1]").click({force: true})
        cy.wait(2000)
        cy.xpath("//body/div/div/div/div/div/button[2]").click({force: true})
        cy.wait(2000)
        })
    
    
    
    
    
    
