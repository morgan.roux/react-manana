Feature: Connexion

    Je veux filtrer mes statistiques

    Background: Résolution
        Given Résolution : 1366 x 768

    Scenario: Se connecter
        Given Je suis sur la page de connexion
        When Je saisis "beneficiaire@yopmail.com" dans le champ E-mail et "Abcd1234!!" dans le champ mot de passe
        When Je clique sur le bouton Se connecter
    
    Scenario: Statistiques dashboard
        Given Je filtre par période
        When Je filtre par catégorie
        When Je filtre par flux
        When Je filtre l'état de compte
       




