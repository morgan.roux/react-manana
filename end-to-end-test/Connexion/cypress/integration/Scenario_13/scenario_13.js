/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion`,() =>{
            cy.visit("https://dashboard-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(15000)
        })
    //Scenario : Consulter les détails d'une transaction
        Given (`Je clique sur la première carte à gauche`,() =>{
            // Voir verso transaction
            cy.xpath("//body/div[@id='root']/div/div/div/div/div[1]/div[1]/button[1]").click({force: true})
            cy.wait(2000)
        })
        When (`Je consulte les détails de la transaction`,() =>{
            cy.contains("Détails").click({force: true})
            cy.wait(15000)
        })
        When (`Je modifie la catégorie de la transaction`,(keyword) =>{
            // Changer la catégorie
            cy.get('#selectCategoryIdDetails').click()
            cy.wait(2000)
            cy.contains("Alimentation").click({force: true})
            cy.wait(2000)
        })
        
