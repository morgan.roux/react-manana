/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion`,() =>{
            cy.visit("https://dashboard-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(15000)
        })
    //Scenario : Créer un virement sans récurrence
        Given (`Je vais dans le menu Carte`,() =>{
            // Menu carte
            cy.contains("Cartes").click({force: true})
            cy.wait(7000)
        })
        When (`Je filtre la liste par le statut virtuelle`,() =>{
            // Modale filtre carte
            cy.contains("Filtre").click({force: true})
            cy.wait(2000)
            cy.get('#searchCard').click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Virtuelle')]").click({force: true})
            cy.wait(2000)
            cy.contains("Appliquer").click({force: true})
            cy.wait(2000)
        })
        When (`Je saisis le mot {string} dans le champ de recherche`,(keyword) =>{
            // Zone de recherche
            cy.get('input[placeholder="Rechercher une carte"]').type(keyword)
            cy.wait(2000)
            cy.get('button[aria-label="Rechercher"]').click({force: true})
            cy.wait(2000)
        })
        When (`Je consulte les détails de la carte`,(keyword) =>{
            // Voir détails
            cy.xpath("//p[contains(text(),'CANDICE VICTOIR...')]").click({force: true})
            cy.wait(2000)
            cy.xpath("//span[contains(text(),'Voir détails')]").click({force: true})
            cy.wait(2000)
            cy.get('input[name="password"]').type('Abcd1234!!')
            cy.wait(2000)
            cy.get('button[label="Valider"]').click({force: true})
            cy.wait(2000)
        })
