/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Ouvrir la page de connexion
        Given (`Je visite la page de connexion`,() =>{
            cy.visit("https://dashboard-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        
        Then (`La page de connexion s'affiche`,() =>{
            cy.wait(2000)
        })

    //Scenario: Se connecter
         
        Given (`Je suis sur la page de connexion`,() =>{
            cy.wait(2000)
        })
        
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
             
          
             
            When (`Je clique sur le bouton Se connecter`,() =>{
                cy.get('button[label=signin]').click()
                cy.wait(15000)
            })
         
         Then (`Je suis connecté et je vois la page principale s'afficher`,() =>{
             cy.wait(5000)
        })