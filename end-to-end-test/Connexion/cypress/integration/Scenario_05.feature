Feature: Connexion

    Je veux télécharger un relevé de compte

    Background: Résolution
        Given Résolution : 1366 x 768

    Scenario: Se connecter
        Given Je suis sur la page de connexion
        When Je saisis "beneficiaire@yopmail.com" dans le champ E-mail et "Abcd1234!!" dans le champ mot de passe
        When Je clique sur le bouton Se connecter
    
    
        Given Je vais sur la modale relevé de compte
        When Je filtre par modalité
        When Je filtre par type
        When Je filtre par membre
        When Je filtre par période