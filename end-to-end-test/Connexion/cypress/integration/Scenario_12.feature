Feature: Connexion

    Je veux consulter les détails d'une carte

    Background: Résolution
        Given Résolution : 1366 x 768

    Scenario: Se connecter
        Given Je suis sur la page de connexion
        When Je saisis "beneficiaire@yopmail.com" dans le champ E-mail et "Abcd1234!!" dans le champ mot de passe
        When Je clique sur le bouton Se connecter
    
    
        Given Je vais dans le menu Carte
        When Je filtre la liste par le statut virtuelle
        When Je saisis le mot "CANDICE" dans le champ de recherche
        When Je consulte les détails de la carte
        

        




