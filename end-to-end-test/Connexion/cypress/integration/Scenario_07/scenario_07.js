/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion`,() =>{
            cy.visit("https://dashboard-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(15000)
        })
    //Scenario : Créer un virement avec récurrence
        Given (`Je vais sur le formulaire virement`,() =>{
            // Menu transactions
                cy.xpath("//strong[contains(text(),'Transactions')]").click()
                cy.wait(10000)
            //Ouvrir formulaire virement
                cy.contains("Créer une transaction").click({force: true})
                cy.wait(2000)
                cy.contains("Créer virement").click({force: true})
                cy.wait(2000)
        })  
        When (`Je choisis un bénéficiaire`,() =>{
                // beneficiaire
                cy.xpath("//body//ul//div[2]").click()
                cy.wait(2000)
        })
        When (`Je met un intitulé "Virement sans récurrence" et un montant "2" Euro`,(title,amount) =>{
                // titre et montant
                cy.get('input[name="title"]').type('Virement avec récurrence')
                cy.wait(2000)
                cy.get('input[name="amount"]').type('2')
                cy.wait(2000)
        })
        When (`Je choisis une catégorie`,() =>{
                // catégorie
                cy.xpath("//body/div/div/div/div/div/form/div/div/div/div[2]/div[2]/div[1]/div[1]").click()
                cy.wait(2000)
                cy.xpath("//li[contains(text(),'Alimentation')]").click()
                cy.wait(2000)
        })
        When (`Je choisis le type de récurrence`,() =>{
                // catégorie
                 //Récurrence
                cy.xpath("//body/div/div/div/div/div/form/div/div/div/div[3]/div[1]/div[1]/div[1]").click()
                cy.wait(2000)
                cy.xpath("//li[contains(text(),'Hebdomadaire')]").click()
                cy.wait(2000)
                cy.xpath("//body/div/div/div/div/div/form/div/div/div/div/div[3]/div[1]").click()
                cy.wait(2000)
                cy.xpath("//body/div/div/div/div/div/div/button[2]").click()
                cy.wait(2000)
                cy.xpath("//div[4]//div[7]//button[1]").click()
                cy.wait(2000)
                cy.get('button[label="Créer"]').click()
                cy.wait(2000)
        })
        When (`Je saisis le code validation`,() =>{
                // catégorie
                // Code de validation
                cy.get('input[data-id="0"]').type('5')
                cy.wait(2000)
                cy.get('input[data-id="1"]').type('4')
                cy.wait(2000)
                cy.get('input[data-id="2"]').type('3')
                cy.wait(2000)
                cy.get('input[data-id="3"]').type('2')
                cy.wait(2000)
                cy.get('input[data-id="4"]').type('1')
                cy.wait(2000)
                cy.get('input[data-id="5"]').type('0')
                cy.wait(2000)
                cy.get('button[label="dialog.button.create"]').click()
                cy.wait(2000)
        })
