/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion du BO`,() =>{
            cy.visit("https://office-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(7000)
        })
    //Scenario: Modiier les infos entreprises
        When (`J'effectue une recherche avec l'entrepise {string}`,(word) =>{
            cy.get('input[id="searchfield"]').type(word)
            cy.wait(2000)
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//form//button").click({force: true})
            cy.wait(7000)
        })
        When (`Je consulte les détails`,() =>{
            cy.contains("Détails").click({force: true})
            cy.wait(5000)
        })
        When (`Je modifie les infos mail {string} phone {string} secteur {string} adresse {string} ville {string} CP {string}`,(mail,phn,act,ad,cty,cp) =>{
            cy.get('input[name="email"]')
            .clear()
            .type(mail)
            cy.wait(2000)
            cy.get('input[placeholder="1 (702) 123-4567"]')
            .clear()
            .type(phn)
            cy.wait(2000)
            cy.get('input[name="legalSector"]')
            .clear()
            .type(act)
            cy.wait(2000)
            cy.xpath("//div[@id='legalNumberOfEmployeeRange']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'10-99')]").click({force: true})
            cy.wait(2000)
            cy.xpath("//div[@id='legalAnnualTurnOver']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'100-249')]").click({force: true})
            cy.wait(2000)
            cy.xpath("//div[@id='legalNetIncomeRange']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'10-49')]").click({force: true})
            cy.wait(2000)
            cy.get('input[name="address"]')
            .clear()
            .type(ad)
            cy.wait(2000)
            cy.get('input[name="city"]')
            .clear()
            .type(cty)
            cy.wait(2000)
            cy.get('input[name="postCode"]')
            .clear()
            .type(cp)
            cy.wait(2000)
            cy.get('button[label="Enregistrer"]').click()
            cy.wait(5000)
        })
        When (`J'uploade une pièce justificative entreprise`,() =>{
            cy.xpath("//body/div[@id='root']/div/div/div/div/div/div/div/div/div/div/div/div/div[1]/div[1]/div[1]/div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
            cy.wait(5000)
            cy.xpath("//body/div[@id='root']/div/div/div/div/div/div/div/div/div/div/div/div[5]").click({force: true})
            cy.wait(2000)
        })
        When (`Je consulte les détails d'un membre`,() =>{
            cy.xpath("//tr[7]//td[1]//div[1]//button[1]").click({force: true})
            cy.wait(2000)

        })
        When (`Je modifie les infos adresse {string} ville {string} CP {string}`,(ad2,cty2,cp2) =>{
            cy.xpath("//body/div/div[3]/div[1]/div[2]")
            .within($bloc_etat =>{
                cy.get('input[name="address"]')
                .clear()
                .type(ad2)
                cy.wait(2000)
                cy.get('input[name="city"]')
                .clear()
                .type(cty2)
                cy.get('input[name="postCode"]')
                .clear({force: true})
                .type(cp2)
                cy.wait(2000)
                cy.xpath("//strong[contains(text(),'Enregistrer')]").click({force: true})
                cy.wait(5000)
            })
            
        })
        When (`J'ajoute des documents`,() =>{
            cy.xpath("//body/div/div[3]/div[1]/div[2]")
            .within($bloc_etat =>{
                cy.xpath("//body/div/div/div/div/div/div/div/div[2]/button[1]").click({force: true})
                cy.wait(2000)
                cy.xpath("//body/div/div[3]/div[1]/div[2]")
                .within($bloc_etat =>{
                    // Identité
                    cy.xpath("//div[@id='identity']").click({force: true})
                    cy.wait(2000)
                    cy.xpath("//li[contains(text(),\"Carte Nationale d'identité\")]").click({force: true})
                    cy.wait(2000)
                    cy.xpath("//body/div/div/div/div/div/div/div/div/div[4]/div[1]/div[1]/div[1]/div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
                    cy.wait(5000)
                    cy.xpath("//div//div[4]//div[2]//div[1]//div[1]//div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
                    cy.wait(5000)
                    // Vigilance
                    cy.xpath("//div[@id='identity2']").click({force: true})
                    cy.wait(2000)
                    cy.xpath("//li[contains(text(),'Permis de conduire')]").click({force: true})
                    cy.wait(2000)
                    cy.xpath("//div[8]//div[1]//div[1]//div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
                    cy.wait(5000)
                    // Justificatif domicile
                    cy.xpath("//div[@id='addressProof']").click({force: true})
                    cy.wait(2000)
                    cy.xpath("//li[contains(text(),\"Dernier avis d'imposition sur le revenu\")]").click({force: true})
                    cy.wait(2000)
                    cy.xpath("//div[12]//div[1]//div[1]//div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
                    cy.wait(5000)
                    // Hebergée
                    cy.xpath("//input[@name='agree']").click({force: true})
                    cy.wait(2000)
                    // Identité hebergeur
                    cy.xpath("//html//body//div//div//div//div//div//div//div//div//div//div//div//div[@id='identity']").click({force: true})
                    cy.wait(2000)
                    cy.xpath("//li[contains(text(),\"Carte Nationale d'identité\")]").click({force: true})
                    cy.wait(2000)
                    cy.xpath("//body//div//div//div//div//div//div//div//div//div[2]//div[2]//div[1]//div[1]//div[1]//div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
                    cy.wait(5000)
                    cy.xpath("//body/div/div/div/div/div/div/div/div/div/div/div[2]/div[1]/div[1]/div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
                    cy.wait(5000)
                    // Justificatif domicile hebergeur
                    cy.xpath("//div[@id='address']").click({force: true})
                    cy.wait(2000)
                    cy.xpath("//li[contains(text(),'Facture ou quittance de loyer')]").click({force: true})
                    cy.wait(2000)
                    cy.xpath("//body/div/div/div/div/div/div/div/div/div/div[4]/div[1]/div[1]/div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
                    cy.wait(5000)
                    cy.xpath("//div[5]//div[1]//div[1]//div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
                    cy.wait(5000)
                    cy.xpath("//body/div/div/div/div/div/div/div[2]/button[1]").click({force: true})
                    cy.wait(2000)
                })



            })

        })




    