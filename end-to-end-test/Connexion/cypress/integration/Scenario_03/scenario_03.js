/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion`,() =>{
            cy.visit("https://dashboard-sapheerbank-staging.digitalbusiness4ever.com/");
            cy.wait(15000)
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(15000)
        })


    //Scenario: Filtre période
        Given (`Je suis sur la page transaction`,() =>{
            // Menu transactions
            cy.xpath("//strong[contains(text(),'Transactions')]").click()
            cy.wait(20000)

        }) 

        When (`Je filtre la liste par période`,() =>{
            // vue tableau
            cy.xpath("//body//div[@id='root']//div//div//div//div//div//div//div//button[2]").click({force: true})
            cy.wait(10000)
            // Filtre période
            cy.get('#totalDay').click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'dernier mois')]").click({force: true})
            cy.wait(2000)
        })
        When (`Je vais dans la modale filtre`,() =>{
            // Filtre transaction
            cy.xpath("//span[contains(text(),'Filtre')]").click({force: true})
            cy.wait(2000)
            // Catégorie
            cy.get('#categoryId').click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Autres dépenses')]").click({force: true})
            cy.wait(2000)
            // Modalité
            cy.get('#modality').click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Virement')]").click({force: true})
            cy.wait(2000)
            // Type
            cy.get('#modalityType').click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Entrant')]").click({force: true})
            cy.wait(2000)
            cy.xpath("//span[contains(text(),'Appliquer')]").click({force: true})
            cy.wait(2000)
        })    
        
        When (`Je recherche par le mot {string} et je vais sur le détail transaction`,(word) =>{
            // Saisie de mot clé dans la zone de recherche
            cy.get('input[placeholder="Rechercher un bénéficiaire"]').type(word)
            cy.wait(5000)
            cy.get('button[aria-label="Rechercher"]').click()
            cy.wait(2000)

        })  

     


