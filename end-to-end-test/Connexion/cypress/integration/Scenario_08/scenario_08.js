/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion`,() =>{
            cy.visit("https://dashboard-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(15000)
        })
    //Scenario : Ajouter un memmbre
        Given (`Je vais sur le formulaire d'ajout de membre`,() =>{
            // Menu équipe
            cy.contains("Equipe").click({force: true})
            cy.wait(2000)
            cy.contains("Ajouter un membre").click({force: true})
            cy.wait(2000)
        })
        When (`Je choisis un rôle`,() =>{
            // Chois de rôle
            cy.xpath("//html//body//div//div//div//div//div//form//div//div//div//div//div//div//div[@id='role']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Collaborateur')]").click({force: true})
            cy.wait(2000)
        })
        When (`Je remplis prénom {string}, nom {string} et mail {string}`,(fname,name,mail) =>{
            // Prénom
             cy.get('input[name="firstName"]').type(fname)
            cy.wait(2000)
            // Nom
            cy.get('input[name="name"]').type(name)
            cy.wait(2000)
            // Mail
            cy.get('input[name="email"]').type(mail)
            cy.wait(2000)
        })
        When (`Je choisis une équipe et les droits du membre`,() =>{
            // Equipe
            cy.xpath("/html//body//div//div//div//div//div//form//div//div//div//div//div//div//div[@id='team']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Informatique')]").click({force: true})
            cy.wait(2000)
            // Droits
            cy.xpath("//span[contains(text(),'Voir le solde du compte')]").click({force: true})
            cy.wait(2000)
            cy.xpath("//span[contains(text(),'Initier des virements')]").click({force: true})
            cy.wait(2000)
            cy.contains("Créer et inviter").click({force: true})
        })
