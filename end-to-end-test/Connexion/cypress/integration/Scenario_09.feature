Feature: Connexion

    Je veux modifier un membre

    Background: Résolution
        Given Résolution : 1366 x 768

    Scenario: Modifier un membre
        Given Je suis sur la page de connexion
        When Je saisis "beneficiaire@yopmail.com" dans le champ E-mail et "Abcd1234!!" dans le champ mot de passe
        When Je clique sur le bouton Se connecter
    
   
        Given Je vais dans la liste des membres
        When Je fltre la liste par statut, rôle, équipe
        When Je saisis le mot "Nante" dans le champ de recherche
        When Je clique sur Réinviter

        Given Je clique sur Modifier
        When Je modifie rôle, prénom par "Nante62", nom par "Rava62" et mail par "nantenaina62@yopmail.com"
        When Je modifie l'équipe et les droits du membre
        When Je saisis mon mot de passe
       




