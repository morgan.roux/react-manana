/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion`,() =>{
            cy.visit("https://dashboard-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(15000)
        })
    
        Given (`Je vais dans la liste des membres`,() =>{
            // Menu équipe
            cy.contains("Equipe").click({force: true})
            cy.wait(5000)
            // Vue tableau
            cy.xpath("//*[name()='path' and contains(@d,'M4 14h4v-4')]").click({force: true})
            cy.wait(2000)
        })
        When (`Je fltre la liste par statut, rôle, équipe`,() =>{
            // Filtre
            cy.xpath("//span[contains(text(),'Filtre')]").click({force: true})
            cy.wait(2000)
            // Statut
            cy.xpath("//div[@id='state']").click({force: true})
            cy.wait(2000)
            cy.contains("Désactivé").click({force: true})
            cy.wait(2000)
            // Rôle
            cy.xpath("//body/div/div/div/div/div/div[2]/div[2]/div[1]/div[1]/div[1]").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Collaborateur')]").click({force: true})
            cy.wait(2000)
            // Equipe
            cy.xpath("//div[contains(text(),'Toutes')]").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Informatique')]").click({force: true})
            cy.wait(2000)
            cy.contains("Appliquer").click({force: true})
            cy.wait(2000)
        })
        When (`Je saisis le mot {string} dans le champ de recherche`,(keyword) =>{
            // Zone de recherche
            cy.get('input[name="teamSearch"]').type(keyword)
            cy.wait(2000)
            cy.get('button[aria-label="Rechercher"]').click({force: true})
            cy.wait(2000)
        })
        When (`Je clique sur Réinviter`,() =>{
            // Réinviter
            cy.contains("Réinviter").click({force: true})
            cy.wait(10000)
        })
    // Scenario : Modification membre
        Given (`Je clique sur Modifier`,() =>{
            // Modifier
            cy.contains("Modifier").click({force: true})
            cy.wait(5000)
        })
        When (`Je modifie rôle, prénom par "Nante62", nom par "Rava62" et mail par "nantenaina62@yopmail.com"`,() =>{
            // Chois de rôle
            cy.xpath("//html//body//div//div//div//div//div//form//div//div//div//div//div//div//div[@id='role']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Comptable')]").click({force: true})
            cy.wait(2000)
            // Prénom
            cy.get('input[name="firstName"]')
            .clear()
            .type('Nante62')
            cy.wait(2000)
            // Nom
            cy.get('input[name="name"]')
            .clear()
            .type('Rava62')
            cy.wait(2000)
            // Mail
            cy.get('input[name="email"]')
            .clear()
            .type('nantenaina62@yopmail.com')
            cy.wait(2000)
        })
        When (`Je modifie l'équipe et les droits du membre`,() =>{
            // Equipe
            cy.xpath("/html//body//div//div//div//div//div//form//div//div//div//div//div//div//div[@id='team']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Finance')]").click({force: true})
            cy.wait(2000)
            // Droits
            cy.xpath("//span[contains(text(),'Gérer les cartes')]").click({force: true})
            cy.wait(2000)
            cy.xpath("//html//body//div//div//div//div//div//form//div//button//span//span[contains(text(),'Valider')]").click({force: true})
            cy.wait(2000)
        })
        When (`Je saisis mon mot de passe`,() =>{
            // Mot de passe
            cy.get('input[name="password"]').type('Abcd1234!!')
            cy.wait(2000)
            cy.get('button[label="Valider"]').click()
            cy.wait(2000)
        })

