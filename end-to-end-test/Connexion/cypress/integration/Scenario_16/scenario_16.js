/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion du BO`,() =>{
            cy.visit("https://office-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(7000)
        })
    //Scenario: Envoyer une notification de type service
        Given (`Je vais dans le menu Notifications`,() =>{
            cy.xpath("//strong[contains(text(),'Notification')]").click({force: true})
            cy.wait(3000)
        })
        When (`Je remplis le formulaire avec le type service`,() =>{
            // Titre
            cy.get('input[name="title"]').type('notif service')
            cy.wait(2000)
            // Type
            cy.xpath("//div[@id='type']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Service')]").click({force: true})
            cy.wait(2000)
            // Entreprise
            cy.xpath("//div[@id='company']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'CHALVIN PARIS c')]").click({force: true})
            cy.wait(2000)
            // Utilisateurs
            cy.xpath("//div[@id='demo-mutiple-chip']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'CHALVIN')]")
            .click({force: true})
            .type('{esc}')
            cy.wait(5000)
            // Message
            cy.get('div[aria-label="rdw-editor"]').type('Ceci est une notification de type service')
            cy.wait(2000)
            

        })
        When (`J'uploade une PJ`,() =>{
            cy.xpath("//div[8]//div[1]//div[1]//div[1]//div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
            cy.wait(2000)
            cy.xpath("//body/div[@id='root']/div/div/div/div/div/div/div/button[1]").click({force: true})
            cy.wait(2000)
        })