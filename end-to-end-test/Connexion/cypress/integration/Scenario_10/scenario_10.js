/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion`,() =>{
            cy.visit("https://dashboard-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(15000)
        })
    //Scenario : Modifier les infos entreprise
        Given (`Je vais sur le formulaire entreprise`,() =>{
            // Menu paramètre
            cy.xpath("//div[@id='root']//div//div//div//header//div//div//div//div//div[contains(text(),'CC')]").click({force: true})
            cy.wait(2000)
            cy.xpath("//span[contains(text(),'Paramètres')]").click({force: true})
            cy.wait(5000)
            // Ouvrir la modale de modification
            cy.contains("Modifier").click({force: true})
            cy.wait(5000)
        })
        When (`Je modifie le logo entreprise`,() =>{
            cy.get('input[name="file"]')
            .eq(1)
            .attachFile('logo.jpg');
            cy.wait(10000)
        })
        When (`Je modifie slogan {string}, les adresses {string} {string}, CP {string}, ville {string}, activité {string}`,(slg,ad2,ad1,cp,city,act) =>{
            cy.get('textarea[name="slogan"]')
            .clear()
            .type(slg)
            cy.wait(2000)
            cy.get('input[name="address2"]')
            .clear()
            .type(ad2)
            cy.wait(2000)
            cy.get('input[name="address"]')
            .clear()
            .type(ad1)
            cy.wait(2000)
            cy.get('input[name="postCode"]')
            .clear()
            .type(cp)
            cy.wait(2000)
            cy.get('input[name="city"]')
            .clear()
            .type(city)
            cy.wait(2000)
            cy.xpath("//body/div/div/div/div/div/div/div/div[3]/div[1]/div[1]/div[1]").click({force: true})
            cy.wait(2000)
            cy.contains("EI").click({force: true})
            cy.wait(2000)
            cy.get('input[name="activity"]')
            .clear()
            .type(act)
            cy.wait(2000)
            cy.contains("Enregistrer").click({force: true})
            cy.wait(2000)
        })