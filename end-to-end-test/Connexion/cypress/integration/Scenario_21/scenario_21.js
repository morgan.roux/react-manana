/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Etape 1
        Given (`Je suis sur la page d'inscription`,() =>{           
            cy.visit("https://sapheerbank-staging.digitalbusiness4ever.com/");
            cy.clearCookies()
        })
        When (`Je remplis l'étape 1 mail {string} phone {string} mdp {string} confirm {string}`,(mail,phn,pwd1,pwd2) =>{
            cy.get('input[name=email]').type(mail)
            cy.wait(2000)
            cy.get('input[placeholder="1 (702) 123-4567"]').type(phn)
            cy.wait(2000)
            cy.get('input[name="password"]').type(pwd1)
            cy.wait(2000)
            cy.get('input[name="passwordConfirm"]').type(pwd2)
            cy.wait(2000)
            cy.xpath("//span[contains(text(),'suivant')]").click({force: true})
            cy.wait(5000)
        })
        When (`Je saisis le code validation`,() =>{
            cy.get('input[data-id="0"]').type('5')
            cy.wait(1000)
            cy.get('input[data-id="1"]').type('4')
            cy.wait(1000)
            cy.get('input[data-id="2"]').type('3')
            cy.wait(1000)
            cy.get('input[data-id="3"]').type('2')
            cy.wait(1000)
            cy.get('input[data-id="4"]').type('1')
            cy.wait(1000)
            cy.get('input[data-id="5"]').type('0')
            cy.wait(2000)
        })
        
    //Scenario: Etape 2
        Given (`Je vais sur l'étape 2`,() =>{
            cy.contains("Valider").click({force: true})
            cy.wait(2000)
        })
    
        When (`Je choisis un forfait`,() =>{
            // Choix de forfait
            cy.contains("Start").click({force: true})
            cy.wait(2000)
            cy.xpath("//div[@id='root']//div//div//div//div//div//label//span//div").click({force: true})
            cy.wait(2000)
            cy.xpath("//span[contains(text(),'suivant')]").click({force: true})
            cy.wait(5000)
        })

     //Scenario: Etape 3
        Given (`Je vais sur l'étape 3`,() =>{
            // Mentions
            cy.xpath("//span[contains(text(),'suivant')]").click({force: true})
            cy.wait(5000)
        })
        When (`Je remplis raison sociale {string} adresse {string} CP {string} Ville {string}`,(rs,add,cp,city) =>{
            cy.get('input[name="legalName"]')
            .clear()
            .type(rs)
            cy.wait(2000)
            cy.xpath("//body/div[@id='root']/div/div/div/div/form/div/div/div/div/div/div[1]").click({force: true})
            cy.wait(2000)
            cy.contains("SEL").click({force: true})
            
            cy.wait(2000)
            cy.get('input[name="address"]')
            .clear()
            .type(add)
            cy.wait(2000)
            cy.get('input[name="postCode"]')
            .clear()
            .type(cp)
            cy.wait(2000)
            cy.get('input[name="city"]')
            .clear()
            .type(city)
            cy.wait(2000)
        })
     //Scenario: Etape 3
        Given (`Je vais sur l'étape 4`,() =>{
            cy.xpath("//span[contains(text(),'suivant')]").click({force: true})
            cy.wait(5000)
        })
        When (`Je remplis les infos dirigeants prénom {string} nom {string} CP {string} Ville {string}`,(fname,name,cp,city) =>{
            // Informations direction
            cy.contains("Directeur").click({force: true})
            cy.wait(2000)
            cy.xpath("//div[@id='mui-component-select-title']").click({force: true})
            cy.wait(2000)
            cy.contains("Mr").click({force: true})
            cy.wait(2000)
            cy.get('input[name="firstName"]').type('Nante')
            cy.wait(2000)
            cy.get('input[name="name"]').type('Rava')
            cy.wait(2000)
            cy.contains("Non résident US actuel ou passé").click({force: true})
            cy.wait(2000)
            cy.get('#date-picker-inline').click({force: true})
            cy.wait(2000)
            cy.get('input[id="date-picker-inline"]')
            .clear()
            .type('01011985')
            cy.wait(2000)
            cy.get('input[name="birthPostCode"]').type('101')
            cy.wait(2000)
            cy.get('input[name="placeOfBirth"]').type('Tana')
            cy.wait(2000)
        })
        When (`Je remplis la pièce d'identité`,() =>{
            // Pièce d'identité
            cy.xpath("//body/div[@id='root']/div/div/div/div/div/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),\"Carte Nationale d'identité\")]").click({force: true})
            cy.wait(4000)
            // Fichier CIN 
            cy.xpath("//div[3]//div[1]//div[1]//div[1]//div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
            cy.wait(4000)
            // Fichier CIN verso
            cy.xpath("//div[3]//div[2]//div[1]//div[1]//div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
            cy.wait(4000)
            
        })
        When (`Je remplis la vigilance complémentaire`,() =>{
            // Pièce d'identité
            cy.xpath("//div[@id='mui-component-select-identity2']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Permis de conduire')]").click({force: true})
            cy.wait(2000)
            // Fichier
            cy.xpath("//div[contains(text(),\"Justificatif de pièce d'identité\")]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
            cy.wait(2000)           
            
        })
        When (`Je remplis l'adresse`,() =>{
            // Adresse personnelle
            cy.get('input[name="address"]').type('Tana')
            cy.wait(2000)
            cy.get('input[name="postCode"]').type('101')
            cy.wait(2000)
            cy.get('input[name="city"]').type('Tana')
            cy.wait(2000)          
            
        })
        When (`Je remplis le justificatif de domicile`,() =>{
            cy.xpath("//div[@id='mui-component-select-addressProof']").click({force: true})
            cy.wait(2000) 
            cy.xpath("//li[contains(text(),'Facture de télécommunication')]").click({force: true})
            cy.wait(2000) 
            // Fichier
            cy.xpath("//div[contains(text(),'Justificatif de domicile')]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
            cy.wait(2000) 
                        
        })
        When (`Je remplis la pièce d'identité hébergeur`,() =>{
            // Hebergeur
            cy.get('input[name="agree"]')
            .eq(0)
            .click({force: true})
            cy.wait(2000)
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//div//div//form//div//div//div[@id='mui-component-select-identity']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),\"Carte Nationale d'identité\")]").click({force: true})
            cy.wait(2000)
            // Fichier CIN 
            cy.xpath("//body//div[@id='root']//div//div//div//div//div//div//div//div//div//div[1]//div[1]//div[1]//div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
            cy.wait(2000)
            // Fichier CIN verso
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//div//div//div//div//div//div//div[contains(text(),\"Carte d'Identité Nationale Verso (Optionnelle)\")]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
            cy.wait(2000)
                        
        })
        When (`Je remplis le justificatif de domicile hébergeur`,() =>{
            cy.xpath("//div[@id='mui-component-select-address']").click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Facture ou quittance de loyer')]").click({force: true})
            cy.wait(2000)
            // Fichier Justificatif domicile hébergeur
            cy.xpath("//div[contains(text(),\"Justificatif de domicile de l'hébergeur\")]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
            cy.wait(2000)
            // Fichier attestation hébergeur
            cy.xpath("//div[contains(text(),'Attestation hébergeur')]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
            cy.wait(2000)
            // Je certifie sur l'honneur
            cy.get('input[name="agree"]')
            .eq(1)
            .click({force: true})            
        })
       
    
    Given (`Je vais sur l'étape 5`,() =>{
        cy.xpath("//span[contains(text(),'suivant')]").click({force: true})
        cy.wait(10000)
    })
    When (`Je remplis les infos actionnaires mail {string} prénom {string} nom {string} CP {string} Ville {string}`,(mail,fname,name,cp,city) =>{
        cy.xpath("//body/div[@id='root']/div/div/div/button[1]").click({force: true})
        cy.wait(2000)
        // Informations actionnires
        cy.get('input[name="email"]').type(mail)
        cy.wait(2000)
        cy.get('input[placeholder="1 (702) 123-4567"]').type('123456789')
        cy.wait(2000)
        cy.xpath("//div[@id='mui-component-select-title']").click({force: true})
        cy.wait(2000)
        cy.contains("Mr").click({force: true})
        cy.wait(2000)
        cy.get('input[name="firstName"]').type('Nante')
        cy.wait(2000)
        cy.get('input[name="name"]').type('Rava')
        cy.wait(2000)
        cy.contains("Non résident US actuel ou passé").click({force: true})
        cy.wait(2000)
        cy.get('#date-picker-inline').click({force: true})
        cy.wait(2000)
        cy.get('input[id="date-picker-inline"]')
        .clear()
        .type('01011985')
        cy.wait(2000)
        cy.get('input[name="birthPostCode"]').type('101')
        cy.wait(2000)
        cy.get('input[name="placeOfBirth"]').type('Tana')
        cy.wait(2000)
    })
    When (`Je remplis la pièce d'identité actionnaire`,() =>{
        // Pièce d'identité actionnaire
        cy.xpath("//div[@id='mui-component-select-identityType']").click({force: true})
        cy.wait(2000)
        cy.xpath("//li[contains(text(),\"Carte Nationale d'identité\")]").click({force: true})
        cy.wait(4000)
        // Fichier CIN 
        cy.xpath("//div[4]//div[1]//div[1]//div[1]//div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
        cy.wait(4000)
        // Fichier CIN verso
        cy.xpath("//div[5]//div[1]//div[1]//div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
        cy.wait(4000)
        
    })
    When (`Je remplis la vigilance complémentaire actionnaire`,() =>{
        // vigilance actionnaire
        cy.xpath("//div[@id='mui-component-select-identityType2']").click({force: true})
        cy.wait(2000)
        cy.xpath("//li[contains(text(),'Permis de conduire')]").click({force: true})
        cy.wait(2000)
        // Fichier
        cy.xpath("//div[contains(text(),\"Justificatif de pièce d'identité\")]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
        cy.wait(2000)           
        
    })
    When (`Je remplis l'adresse actionnaire`,() =>{
        // Adresse personnelle actionnaire
        cy.get('input[name="address"]').type('Tana')
        cy.wait(2000)
        cy.get('input[name="postCode"]').type('101')
        cy.wait(2000)
        cy.get('input[name="city"]').type('Tana')
        cy.wait(2000)          
        
    })
    When (`Je remplis le justificatif de domicile actionnaire`,() =>{
        cy.xpath("//div[@id='mui-component-select-addressType']").click({force: true})
        cy.wait(2000) 
        cy.xpath("//li[contains(text(),'Facture de télécommunication')]").click({force: true})
        cy.wait(2000) 
        // Fichier
        cy.xpath("//div[contains(text(),'Justificatif de domicile')]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
        cy.wait(2000) 
                    
    })
    When (`Je remplis la pièce d'identité hébergeur actionnaire`,() =>{
        // Hebergeur actionnaire
        cy.get('input[name="agree"]')
        .eq(0)
        .click({force: true})
        cy.wait(2000)
        cy.xpath("//div[@id='mui-component-select-identity']").click({force: true})
        cy.wait(2000)
        cy.xpath("//li[contains(text(),\"Carte Nationale d'identité\")]").click({force: true})
        cy.wait(2000)
        // Fichier CIN actionnaire
        cy.xpath("//body//div[@id='root']//div//div//div//div//div//div//div//div//div[1]//div[1]//div[1]//div[1]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
        cy.wait(2000)
        // Fichier CIN verso actionnaire
        cy.xpath("//div[contains(text(),\"Carte d'Identité Nationale Verso (Optionnelle)\")]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
        cy.wait(2000)
                    
    })
    When (`Je remplis le justificatif de domicile hébergeur actionnaire`,() =>{
        cy.xpath("//div[@id='mui-component-select-address']").click({force: true})
        cy.wait(2000)
        cy.xpath("//li[contains(text(),'Facture ou quittance de loyer')]").click({force: true})
        cy.wait(2000)
        // Fichier Justificatif domicile hébergeur actionnaire
        cy.xpath("//div[contains(text(),\"Justificatif de domicile de l'hébergeur\")]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
        cy.wait(2000)
        // Fichier attestation hébergeur actionnaire
        cy.xpath("//div[contains(text(),'Attestation hébergeur')]").attachFile('logo.jpg', { subjectType: 'drag-n-drop' })
        cy.wait(2000)
        // Je certifie sur l'honneur actionnaire
        cy.get('input[name="agree"]')
        .eq(1)
        .click({force: true})    
        cy.xpath("//span[contains(text(),'suivant')]").click({force: true})
        cy.wait(10000)        
    })
    When (`Je saisis les montants des souscritpions`,() =>{
        cy.get('input[name="capital"]')
        .eq(0)
        .clear()
        .type('10')     
        cy.wait(2000)
        cy.get('input[name="capital"]')
        .eq(1)
        .clear()
        .type('10')     
        cy.wait(2000)
        cy.xpath("//div[@id='root']//div//div//div//div//div//div//label//span//span//input").click({force: true})
        cy.wait(2000)

    })
    Given (`Je vais sur l'étape 6`,() =>{
        cy.xpath("//span[contains(text(),'suivant')]").click({force: true})
        cy.wait(7000)
             
    })
             
    
        When (`J'enregistre ma signature`,() =>{
            // Approuver contrat
            cy.xpath("//span[contains(text(),'Contrat Sapheer')]").click({force: true})
            cy.wait(2000)
            cy.xpath("//div//div//div//div[1]//label[1]//span[1]//span[1]//input[1]").click({force: true})
            cy.wait(2000)
            cy.xpath("//div//div//div//div//div//div[2]//label[1]//span[1]//span[1]//input[1]").click({force: true})
            cy.wait(2000)
            // Signature
            cy.xpath("//span[contains(text(),'Signature virtuelle')]").click({force: true})
            cy.wait(2000)
            cy.xpath("//body/div/div/div/div/button[2]").click({force: true})
            cy.wait(2000)
            cy.xpath("//span[contains(text(),'Enregistrer')]").click({force: true})
            cy.wait(20000)

                 
        })
        When (`Je télécharge mon contrat`,() =>{
            // Télécharger contrat
            cy.xpath("//span[contains(text(),'Télécharger le contrat')]").click({force: true})
            cy.wait(2000)
            
                 
        })
        When (`Jaccède au dashboard`,() =>{
            // Dashboard
            cy.xpath("//span[contains(text(),'Accéder au dashboard')]").click({force: true})
            cy.wait(2000)
            
                 
        })










