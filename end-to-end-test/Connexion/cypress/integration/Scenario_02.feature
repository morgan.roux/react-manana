Feature: Connexion

    Je veux me connecter sur Sapheer

    Background: Résolution
        Given Résolution : 1366 x 768

    Scenario: Ouvrir la page de connexion
        Given Je visite la page de connexion
        Then La page de connexion s'affiche

    Scenario: Se connecter
        Given Je suis sur la page de connexion
        When Je saisis "beneficiaire@yopmail.com" dans le champ E-mail et "Abcd1234!!" dans le champ mot de passe
        When Je clique sur le bouton Se connecter
        Then Je suis connecté et je vois la page principale s'afficher