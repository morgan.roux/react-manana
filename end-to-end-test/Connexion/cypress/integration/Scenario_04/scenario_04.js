/// <reference types="cypress" />
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps"


    //Scenario: Se connecter
        Given (`Je suis sur la page de connexion`,() =>{
            cy.visit("https://dashboard-sapheerbank-staging.digitalbusiness4ever.com/");
        })
        When (`Je saisis {string} dans le champ E-mail et {string} dans le champ mot de passe`,(mail,mdp) =>{
            cy.get('input[name=email]').type(mail)
            cy.get('input[name=password]').type(mdp)
        })
        When (`Je clique sur le bouton Se connecter`,() =>{
            cy.get('button[label=signin]').click()
            cy.wait(15000)
        })
    //Scenario: Filtre période
        Given (`Je filtre par période`,() =>{
        // filtre période
            cy.xpath("//body/div[@id='root']/div/div/div/div/div/div/div/div/div/div/div[@id='param1']/span[1]").click({force: true})
            cy.wait(2000)
            cy.xpath("//span[contains(text(),'3 derniers mois')]")
            .eq(0)
            .click({force: true})
            cy.wait(2000)
        })  
        When (`Je filtre par catégorie`,() =>{
            // filtre catégorie
            cy.get('#param2').click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Flux')]").click({force: true})
            cy.wait(2000)
        })

        When (`Je filtre par flux`,() =>{
            // filtre catégorie
            cy.get('#param3').click({force: true})
            cy.wait(2000)
            cy.xpath("//li[contains(text(),'Sortant')]").click({force: true})
            cy.wait(2000)
        })
            
        When (`Je filtre l'état de compte`,() =>{
            cy.xpath("//body/div[@id='root']/div/div/div/div/div[2]/div[1]/div[1]/div[1]")
            .within($bloc_etat =>{
            cy.xpath("//div[@id='root']//div//div//div//div//div//div//div//div//div//div//div//div[@id='param1']//span[contains(text(),'10 derniers jours')]").click({force: true})
            cy.wait(2000)
            cy.xpath("//span[contains(text(),'3 derniers mois')]")
            .eq(1)
            .click({force: true})
            cy.wait(2000)
            })
            
        })


     