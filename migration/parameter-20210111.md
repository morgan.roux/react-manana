## Parameter_groupe

```sql

INSERT INTO public."Parameter_groupe"(
	id, code, nom, date_creation, date_modification)
	VALUES ('ckgk80omzewn80744zdelxtry', 'GROUPEMENTRELEVETEMPERATURE', 'Relevé des températures', NOW(), NOW());

```

## Parameter

```sql

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80844xdeaat12', '0727','login', null, NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTRELEVETEMPERATURE'), (SELECT id FROM public."Parameter_Type" WHERE code='TXT'), 'PHARMACIE', NULL);

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744xdeaat13', '0728','password', null, NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTRELEVETEMPERATURE'), (SELECT id FROM public."Parameter_Type" WHERE code='TXT'), 'PHARMACIE', NULL);

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744xdeaatjk', '0827','login', null, NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTRELEVETEMPERATURE'), (SELECT id FROM public."Parameter_Type" WHERE code='TXT'), 'GROUPEMENT', NULL);

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744xdeaatmp', '0828','password', null, NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTRELEVETEMPERATURE'), (SELECT id FROM public."Parameter_Type" WHERE code='TXT'), 'GROUPEMENT', NULL);

```