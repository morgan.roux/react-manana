
## Groupement parameter

```sql

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744xdeaat12', '0826', 'GED : Envoi des documents par email', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='SENDINGEMAIL'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT', NULL);

```

## Pharmacie parameter

```sql

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744ydeaat12', '0726', 'GED : Envoi des documents par email', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='SENDINGEMAIL'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'),'PHARMACIE', NULL);

```
