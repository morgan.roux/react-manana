## Parameter

```sql

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80844x1eaat12', '0730','Niveau de la Matrice des fonctinos', NULL, NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='OTHER'), (SELECT id FROM public."Parameter_Type" WHERE code='INT'), 'PHARMACIE', NULL);

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80844x2eaat12', '0830','Niveau de la Matrice des fonctinos', '2', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='OTHER'), (SELECT id FROM public."Parameter_Type" WHERE code='INT'), 'GROUPEMENT', NULL);

```
