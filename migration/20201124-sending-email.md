## New parameter group

```sql
INSERT INTO public."Parameter_groupe"(
	id, code, nom, date_creation, date_modification)
	VALUES ('ckgk80omzewn80744zdelxt11', 'SENDINGEMAIL', 'Envoi d''email', NOW(), NOW());

```

## Paramaters

```sql
INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelx120', '0400', 'FROM', 'support@d4win.site', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='SENDINGEMAIL'), (SELECT id FROM public."Parameter_Type" WHERE code='TXT'), 'GROUPEMENT_SUPERADMIN');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelx122', '0401', 'CC', NULL, NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='SENDINGEMAIL'), (SELECT id FROM public."Parameter_Type" WHERE code='TXT'), 'GROUPEMENT_SUPERADMIN');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelx123', '0402', 'BCC', NULL, NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='SENDINGEMAIL'), (SELECT id FROM public."Parameter_Type" WHERE code='TXT'), 'GROUPEMENT_SUPERADMIN');
```