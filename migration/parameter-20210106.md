## Groupement parameter

```sql

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744zdeaat12', '0825', 'Rélevé des températures', 'disabled', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='SELECT'), 'GROUPEMENT','[{"value" : "disabled", "label" : "Désactivé"},{"value": "mobile", "label": "Mobile"},{"value": "web", "label": "Web"}, {"value" : "all", "label": "Tout"}]');

```

## Pharmacie parameter

```sql

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744zdblxt27', '0724', 'Rélevé des températures', 'disabled', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='SELECT'), 'PHARMACIE','[{"value" : "disabled", "label" : "Désactivé"},{"value": "mobile", "label": "Mobile"},{"value": "web", "label": "Web"}, {"value" : "all", "label": "Tout"}]');

```
