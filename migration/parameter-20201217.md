## Pharmacie parameter

```sql
INSERT INTO public."Parameter_groupe"(
	id, code, nom, date_creation, date_modification)
	VALUES ('akgk80omzewn80744zdelxtg1', 'PHARMACIEFONCTION', 'Fonctionnalités de la pharmacie', NOW(), NOW());

```

## Pharmacie fonctions 

```sql
INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxtg0', '0700', 'Publicité', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxtg1', '0701', 'Promotion en cours', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxtg7', '0702', 'To-Do', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxtg6', '0703', 'Partage des idées', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt10', '0704', 'Actualités', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt11', '0705', 'Opérations commerciales', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt12', '0706', 'Catalogue des Produits', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt13', '0707', 'Suivi des commandes', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt14', '0708', 'Laboratoires Partenaires', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt15', '0709', 'Partenaires de services', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt16', '0710', 'Services aux Pharmacies ', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt17', '0711', 'Evènement', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt18', '0712', 'Formation', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt19', '0713', 'Newsletter', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt20', '0714', 'Achat Groupé', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt21', '0715', 'Cahier de Liaison', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt22', '0716', 'Démarche Qualité', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt23', '0717', 'GED', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt24', '0718', 'Gestion du feedback', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt25', '0719', 'Groupe de Pharmacies', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('akgk80omzewn80744zdelxt26', '0720', 'Messagerie', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE');

UPDATE public."Parameter"
SET options = '[{"value" : "disabled", "label" : "Désactivé"},{"value": "mobile", "label": "Mobile"},{"value": "web", "label": "Web"}, {"value" : "all", "label": "Tout"}]',
default_value = 'all',
id_type_parameter = (SELECT id FROM public."Parameter_Type" WHERE code='SELECT')
WHERE id_groupe_parameter=(SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION')

```