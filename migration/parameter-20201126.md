# Add select type on Parameter_Type

```sql
ALTER TABLE public."Parameter" ADD COLUMN options TEXT DEFAULT NULL;

INSERT INTO public."Parameter_Type"(
	id, code, nom, date_creation, date_modification)
	VALUES ('ckgk80omzewn80744zzelxt27', 'SELECT', 'Selection',  NOW(), NOW());

UPDATE public."Parameter"
SET options = '[{"value" : "disabled", "label" : "Désactivé"},{"value": "mobile", "label": "Mobile"},{"value": "web", "label": "Web"}, {"value" : "all", "label": "Tout"}]',
default_value = 'disabled',
id_type_parameter = (SELECT id FROM public."Parameter_Type" WHERE code='SELECT')
WHERE id_groupe_parameter=(SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION')

DELETE FROM public."Parameter_Value" WHERE id_parameter IN (
	SELECT id FROM public."Parameter" WHERE id_groupe_parameter=(SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION')
)
```
