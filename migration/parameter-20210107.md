
## module outils

``` sql
    INSERT INTO public."Module"(id,code,nom,code_maj,date_creation,date_modification) VALUES ('ckd8mck3600o0077174tq1hxs','21','Outil',null,NOW(),NOW());
```

## traitement outil releve temperature

``` sql
    INSERT INTO public."Traitement"(id,code,nom,code_maj,date_creation,date_modification,id_module) VALUES ('ckd8mcl29013u0771d0u8a4fd','21_01','Relevé température',null,NOW(),NOW(),'ckd8mck3600o0077174tq1hxs');
```

## RT Status

``` sql
    
    INSERT INTO public."RT_status"(id, code, couleur, libelle, date_creation, date_modification) VALUES ('ckd8mcl29013u0771d0u8a4ty','001', '#e9161a', 'En arret', NOW(), NOW());


    INSERT INTO public."RT_status"(id, code, couleur, libelle, date_creation, date_modification) VALUES ('ckd8mcl29013u0771d0u8a4tr','002', '#1de27c', 'En marche', NOW(), NOW());

```