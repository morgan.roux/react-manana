## New parameter group

```sql
INSERT INTO public."Parameter_groupe"(
	id, code, nom, date_creation, date_modification)
	VALUES ('ckgk80omzewn80744zdelxtg1', 'GROUPEMENTFONCTION', 'Fonctionnalités du groupement', NOW(), NOW());

```

## Groupement fonctions

```sql
INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxtg0', '0200', 'Publicité', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxtg1', '0201', 'Promotion en cours', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxtg7', '0207', 'To-Do', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');


INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxtg6', '0206', 'Partage des idées', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt10', '0301', 'Actualités', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');


INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt11', '0302', 'Opérations commerciales', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt12', '0303', 'Catalogue des Produits', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');
INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt13', '0304', 'Suivi des commandes', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt14', '0305', 'Laboratoires Partenaires', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');
INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt15', '0306', 'Partenaires de services', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');
INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt16', '0307', 'Services aux Pharmacies ', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');
INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt17', '0308', 'Evènement', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt18', '0309', 'Formation', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt19', '0310', 'Newsletter', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt20', '0311', 'Achat Groupé', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');
INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt21', '0312', 'Cahier de Liaison', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');
INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt22', '0313', 'Démarche Qualité', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');
INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt23', '0314', 'GED', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');
INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt24', '0315', 'Gestion du feedback', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt25', '0316', 'Groupe de Pharmacies', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');
INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
	VALUES ('ckgk80omzewn80744zdelxt26', '0317', 'Messagerie', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT');
```
    