# Add RGPD Parameter

```sql
INSERT INTO public."Parameter_groupe"(
	id, code, nom, date_creation, date_modification)
	VALUES ('ckgk80omzewn80744zdelxtxx', 'RGPD', 'RGPD', NOW(), NOW());


INSERT INTO public."Parameter"(
id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
VALUES ('ckgk80omzewn80744zdelxX21', '0320', 'Renouvellement du Consentement', '390', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='RGPD'), (SELECT id FROM public."Parameter_Type" WHERE code='INT'), 'GROUPEMENT');


INSERT INTO public."Parameter"(
id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
VALUES ('ckgk80omzewn80744zdelxX22', '0321', 'Blocage accès Users', '60', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='RGPD'), (SELECT id FROM public."Parameter_Type" WHERE code='INT'), 'GROUPEMENT');


INSERT INTO public."Parameter"(
id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category)
VALUES ('ckgk80omzewn80744zdelxX23', '0322', 'Archivage des données des utilisateurs', '390', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='RGPD'), (SELECT id FROM public."Parameter_Type" WHERE code='INT'), 'GROUPEMENT');
```
