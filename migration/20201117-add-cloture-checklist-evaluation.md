
# Add colonne cloture

```sql
ALTER TABLE public."DQ_checklist_evaluation"
ADD COLUMN cloture BOOLEAN DEFAULT false;

ALTER TABLE public."DQ_fiche_amelioration"
ADD COLUMN cloture BOOLEAN DEFAULT false;

```