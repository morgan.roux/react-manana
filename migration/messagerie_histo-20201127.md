# Add select type on Parameter_Type

```sql
ALTER TABLE public."Messagerie_histo" ADD COLUMN suppression_permanente TEXT DEFAULT NULL;

UPDATE public."Messagerie_histo"
SET suppression_permanente=false

```
