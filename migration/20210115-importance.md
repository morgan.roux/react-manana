## New importance

```sql
INSERT INTO public."Importance"(
	id, ordre, libelle, color, is_removed, date_creation, date_modification)
	VALUES ('ckj13ct2q04ax07214gm8gs4d', 2, 'Pas important', '#C1CAD6', 'false', NOW(), NOW());
INSERT INTO public."Importance"(
	id, ordre, libelle, color, is_removed, date_creation, date_modification)
	VALUES ('ckj13ct2q04ax07214gm8gnc7', 1, 'Important', '#FF3A20', 'false', NOW(), NOW());
```

### Update urgences
Must run before prisma deploy

```sql
ALTER TABLE public."Urgence"
  ADD COLUMN couleur VARCHAR(20);
	
UPDATE  public."Urgence"
SET couleur=color;

ALTER TABLE public."Urgence" 
 DROP COLUMN color;

```

### Update todo actions
Must run before prisma deploy

```sql
ALTER TABLE public."Todo_action"
  ADD COLUMN date_debut timestamp(3) without time zone,
  ADD COLUMN date_fin timestamp(3) without time zone;
	
UPDATE public."Todo_action"
SET date_debut=date_echeance;
```

## Migration des information de liaison

```sql

ALTER TABLE public."Information_liaison" 
 ADD COLUMN id_importance VARCHAR(25),
 ADD FOREIGN KEY (id_importance) REFERENCES  public."Importance"(id);
 
UPDATE public."Information_liaison"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=1)
WHERE id_etiquette IN (SELECT id FROM public."Todo_etiquette" WHERE ordre=1);

UPDATE public."Information_liaison"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=2)
WHERE id_etiquette IN (SELECT id FROM public."Todo_etiquette" WHERE ordre=2);

UPDATE public."Information_liaison"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=2)
WHERE id_etiquette IS NULL;


ALTER TABLE public."Information_liaison" 
 DROP COLUMN id_etiquette;
```

## Action operationnelle

```sql


ALTER TABLE public."DQ_action_operationnelle"
 ADD COLUMN id_importance VARCHAR(25),
 ADD FOREIGN KEY (id_importance) REFERENCES  public."Importance"(id);
 
UPDATE public."DQ_action_operationnelle"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=1)
WHERE id_etiquette IN (SELECT id FROM public."Todo_etiquette" WHERE ordre=1);

UPDATE public."DQ_action_operationnelle"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=2)
WHERE id_etiquette IN (SELECT id FROM public."Todo_etiquette" WHERE ordre=2);

UPDATE public."DQ_action_operationnelle"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=2)
WHERE id_etiquette IS NULL;


ALTER TABLE public."DQ_action_operationnelle"
 DROP COLUMN id_etiquette;
```

## Fiches ameliorations

```sql

ALTER TABLE public."DQ_fiche_amelioration"
 ADD COLUMN id_importance VARCHAR(25),
 ADD FOREIGN KEY (id_importance) REFERENCES  public."Importance"(id);
 
UPDATE public."DQ_fiche_amelioration"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=1)
WHERE id_etiquette IN (SELECT id FROM public."Todo_etiquette" WHERE ordre=1);

UPDATE public."DQ_fiche_amelioration"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=2)
WHERE id_etiquette IN (SELECT id FROM public."Todo_etiquette" WHERE ordre=2);

UPDATE public."DQ_fiche_amelioration"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=2)
WHERE id_etiquette IS NULL;


ALTER TABLE public."DQ_fiche_amelioration"
 DROP COLUMN id_etiquette;
```

## Fiches incidents

```sql

ALTER TABLE public."DQ_fiche_incident"
 ADD COLUMN id_importance VARCHAR(25),
 ADD FOREIGN KEY (id_importance) REFERENCES  public."Importance"(id);
 
UPDATE public."DQ_fiche_incident"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=1)
WHERE id_etiquette IN (SELECT id FROM public."Todo_etiquette" WHERE ordre=1);

UPDATE public."DQ_fiche_incident"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=2)
WHERE id_etiquette IN (SELECT id FROM public."Todo_etiquette" WHERE ordre=2);

UPDATE public."DQ_fiche_incident"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=2)
WHERE id_etiquette IS NULL;


ALTER TABLE public."DQ_fiche_incident"
 DROP COLUMN id_etiquette;
```


## Traitements automatiques

```sql
ALTER TABLE public."TA_traitement"
 ADD COLUMN id_importance VARCHAR(25),
 ADD FOREIGN KEY (id_importance) REFERENCES  public."Importance"(id);
 
UPDATE public."TA_traitement"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=1)
WHERE id_etiquette IN (SELECT id FROM public."Todo_etiquette" WHERE ordre=1);

UPDATE public."TA_traitement"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=2)
WHERE id_etiquette IN (SELECT id FROM public."Todo_etiquette" WHERE ordre=2);

UPDATE public."TA_traitement"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=2)
WHERE id_etiquette IS NULL;


ALTER TABLE public."TA_traitement"
 DROP COLUMN id_etiquette;
```


## Suivi appels

```sql 

ALTER TABLE "public"."suivi_appel"
 ADD COLUMN id_importance VARCHAR(25),
 ADD FOREIGN KEY (id_importance) REFERENCES  public."Importance"(id);
 
UPDATE "public"."suivi_appel"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=1)
WHERE id_etiquette IN (SELECT id FROM public."Todo_etiquette" WHERE ordre=1);

UPDATE "public"."suivi_appel"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=2)
WHERE id_etiquette IN (SELECT id FROM public."Todo_etiquette" WHERE ordre=2);

UPDATE "public"."suivi_appel"
SET id_importance=(SELECT id FROM public."Importance" WHERE ordre=2)
WHERE id_etiquette IS NULL;


ALTER TABLE "public"."suivi_appel"
 DROP COLUMN id_etiquette;
```

## Temporaries columns
- id_importance sur la table information_liaison