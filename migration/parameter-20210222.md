## Groupement parameter

```sql

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744zdeayx12', '0210', 'Dashboard', 'disabled', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='SELECT'), 'GROUPEMENT','[{"value" : "disabled", "label" : "Désactivé"},{"value": "mobile", "label": "Mobile"},{"value": "web", "label": "Web"}, {"value" : "all", "label": "Tout"}]');

```

## Pharmacie parameter

```sql

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744zdblyx13', '0745', 'Dashboard', 'disabled', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='SELECT'), 'PHARMACIE','[{"value" : "disabled", "label" : "Désactivé"},{"value": "mobile", "label": "Mobile"},{"value": "web", "label": "Web"}, {"value" : "all", "label": "Tout"}]');

```

## Parameter

```sql

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80844x1eaat78', '0378','Afficher le dashboard au démarrage', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='OTHER'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE', NULL);

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80844x2eaat79', '0779','Afficher le dashboard au démarrage', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='OTHER'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT', NULL);



INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80844x1aaat78', '0379','Afficher le pilotage de la démarche qualité', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='OTHER'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'PHARMACIE', NULL);

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80844x2baat79', '0780','Afficher le pilotage de la démarche qualité', 'false', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='OTHER'), (SELECT id FROM public."Parameter_Type" WHERE code='BOOLEAN'), 'GROUPEMENT', NULL);


```
