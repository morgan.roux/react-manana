## Pharmacie parameter

```sql

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744zdalxt27', '0721', 'Commande orale', 'disabled', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='SELECT'), 'PHARMACIE','[{"value" : "disabled", "label" : "Désactivé"},{"value": "mobile", "label": "Mobile"},{"value": "web", "label": "Web"}, {"value" : "all", "label": "Tout"}]');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744zdalxt28', '0722', 'Mes Traitements automatiques', 'disabled', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='SELECT'), 'PHARMACIE'),'[{"value" : "disabled", "label" : "Désactivé"},{"value": "mobile", "label": "Mobile"},{"value": "web", "label": "Web"}, {"value" : "all", "label": "Tout"}]';

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744zdalxt32', '0723', 'Suivi appel', 'disabled', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='PHARMACIEFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='SELECT'), 'PHARMACIE','[{"value" : "disabled", "label" : "Désactivé"},{"value": "mobile", "label": "Mobile"},{"value": "web", "label": "Web"}, {"value" : "all", "label": "Tout"}]');

```
## Groupement parameter

```sql

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akgk80omzewn80744zdelat12', '0821', 'Commande orale', 'disabled', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='SELECT'), 'GROUPEMENT','[{"value" : "disabled", "label" : "Désactivé"},{"value": "mobile", "label": "Mobile"},{"value": "web", "label": "Web"}, {"value" : "all", "label": "Tout"}]');


INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('akmk80omzewn80744zdelat13', '0822', 'Mes Traitements automatiques', 'disabled', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='SELECT'), 'GROUPEMENT','[{"value" : "disabled", "label" : "Désactivé"},{"value": "mobile", "label": "Mobile"},{"value": "web", "label": "Web"}, {"value" : "all", "label": "Tout"}]');

INSERT INTO public."Parameter"(
	id, code, name, default_value, date_creation, date_modification, id_groupe_parameter, id_type_parameter, category,options)
	VALUES ('aknk80omzewn80744zdelat13', '0824', 'Suivi appel', 'disabled', NOW(), NOW(), (SELECT id FROM public."Parameter_groupe" WHERE code='GROUPEMENTFONCTION'), (SELECT id FROM public."Parameter_Type" WHERE code='SELECT'), 'GROUPEMENT','[{"value" : "disabled", "label" : "Désactivé"},{"value": "mobile", "label": "Mobile"},{"value": "web", "label": "Web"}, {"value" : "all", "label": "Tout"}]');

```