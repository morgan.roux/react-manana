# Change todo and messagerie to parameter groupement

```sql
UPDATE public."Parameter"
SET category='GROUPEMENT'
WHERE id_groupe_parameter IN (SELECT id FROM public."Parameter_groupe" WHERE code IN ('TODO','MESSAGERIE'))

DELETE FROM public."Parameter"
	WHERE code IN ('0104','0105','0106','0107','0108','0109','0117','0116');

DELETE FROM public."Parameter_Value" WHERE id_parameter IN (
	SELECT id FROM public."Parameter" WHERE id_groupe_parameter IN (SELECT id FROM public."Parameter_groupe" WHERE code IN ('TODO','MESSAGERIE'))
)

```
