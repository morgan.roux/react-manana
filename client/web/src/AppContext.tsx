import React, {
  FC,
  createContext,
  Dispatch,
  SetStateAction,
  useState,
  useContext,
  ReactNode,
  ReactElement,
} from 'react';
import { Column } from './components/Dashboard/Content/Interface';

export interface ContentInterface {
  type: string;
  searchPlaceholder: string;
  filterBy?: any;
  sortBy?: any;
  columns?: Column[];
  buttons?: any;
  query?: string;
  page: number;
  rowsPerPage: number;
  variables?: any;
  operationName?: any;
  refetch?: any;
  contextQuery?: any;
}

export interface ContentStateInterface {
  content: ContentInterface;
  setContent: Dispatch<SetStateAction<ContentInterface>>;
}

export interface ContentContextProviderProps {
  defaults?: Partial<ContentInterface>;
  children?: ReactNode;
}

export const defaultContent: ContentInterface = {
  type: '',
  searchPlaceholder: '',
  filterBy: '',
  sortBy: '',
  columns: [],
  buttons: '',
  query: '',
  page: 0,
  rowsPerPage: 25,
};

export const useContent = (overrides?: Partial<ContentInterface>): ContentStateInterface => {
  const [content, setContent] = useState<ContentInterface>({
    ...defaultContent,
    ...overrides,
  });
  return { content, setContent };
};

const defaultContentState: ContentStateInterface = {
  content: defaultContent,
  setContent: (): void => {},
};

export const ContentContext = createContext<ContentStateInterface>(defaultContentState);

export const useContentContext = (): ContentStateInterface => {
  return useContext(ContentContext);
};

export const ContentContextProvider: FC<ContentContextProviderProps> = ({
  children,
  defaults,
}): ReactElement => {
  const [content, setContent] = useState<ContentInterface>({
    ...defaultContent,
    ...defaults,
  });
  return (
    <ContentContext.Provider value={{ content, setContent }}>{children}</ContentContext.Provider>
  );
};
