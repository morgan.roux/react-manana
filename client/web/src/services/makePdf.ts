// eslint-disable-next-line eslint-comments/disable-enable-pair
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { savePDF } from '@progress/kendo-react-pdf';
import { drawDOM, exportPDF } from '@progress/kendo-drawing';

// class DocService {
//   createPdf = (html: any, fileName: string) => {
//     savePDF(html, {
//       paperSize: 'auto',
//       fileName: `${fileName}.pdf`,
//       margin: 3,
//     });
//   };
// }

export const makePdf = (html: any, fileName: string) => {
  return savePDF(html, {
    paperSize: 'auto',
    fileName: `${fileName}.pdf`,
    margin: 3,
  });
};

export const getBase64pdf = (element: any) => {
  return drawDOM(element, {
    paperSize: 'auto',
  })
    .then(group => {
      console.log('pdf: ', exportPDF(group));
      return exportPDF(group);
    })
    .then(dataUri => {
      return dataUri.split(';base64,')[1];
    });
};
