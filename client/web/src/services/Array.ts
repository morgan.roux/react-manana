export const uniqueArrayObject = (array: any[], itemId: string): any[] => {
  return array.reduce((prev: any[], current: any) => {
    if (prev && current) {
      const x = prev.find((item: any) => item && item[itemId] === current[itemId]);
      if (!x) {
        return prev.concat([current]);
      } else {
        return prev;
      }
    }
  }, []);
};
