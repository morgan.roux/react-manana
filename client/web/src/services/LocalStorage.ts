const ACCESS_TOKEN_KEY = 'access_token';
const GROUPEMENT_KEY = 'groupement';
const PHARMACIE_KEY = 'pharmacie';
const USER = 'user';
const ISSSO = 'isSso';
const IP_KEY = 'ip';

export const setAccessToken = (token: string) => localStorage.setItem(ACCESS_TOKEN_KEY, token);
export const getAccessToken = (): string | null => localStorage.getItem(ACCESS_TOKEN_KEY);
export const isAuthenticated = (): boolean => !!localStorage.getItem(ACCESS_TOKEN_KEY);
export const setGroupement = (groupement: any) =>
  localStorage.setItem(GROUPEMENT_KEY, JSON.stringify(groupement));
export const getGroupement = (): any => {
  const item = localStorage.getItem(GROUPEMENT_KEY);
  try {
    return item !== null ? JSON.parse(item) : null;
  } catch (error) {
    return null;
  }
};

export const setPharmacie = (pharmacie: any) => {
  localStorage.setItem(PHARMACIE_KEY, JSON.stringify(pharmacie));
};

export const getPharmacie = () => {
  const item = localStorage.getItem(PHARMACIE_KEY);
  try {
    return item !== null ? JSON.parse(item) : null;
  } catch (error) {
    return null;
  }
};

export const setUser = (user: any) => {
  const token = localStorage.getItem(ACCESS_TOKEN_KEY);
  if (!!token) {
    try {
      localStorage.setItem(USER, JSON.stringify({ ...user, accessToken: token }));
    } catch (error) {
      // Ignore
    }
  }
};

export const getUser = () => {
  const token = localStorage.getItem(ACCESS_TOKEN_KEY);
  if (token) {
    try {
      const item = localStorage.getItem(USER);
      if (item) {
        const user = JSON.parse(item);
        if (user.accessToken === token) return user;
      }
    } catch (error) {}
  }

  return null;
};

export const clearLocalStorage = () => localStorage.clear();

export const setIp = (ip: string) => {
  localStorage.setItem(IP_KEY, ip);
};

export const getIp = () => {
  return localStorage.getItem(IP_KEY) || '';
};
