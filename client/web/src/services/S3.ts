import axios from 'axios';

export const uploadToS3 = (file: File, presignedUrl: string) => {
  // console.log('file : ', file);
  const options = {
    headers: {
      'Content-Type': file.type,
    },
  };

  return axios.put(presignedUrl, file, options);
};
