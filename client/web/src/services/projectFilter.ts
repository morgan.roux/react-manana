import { ME_me } from '../graphql/Authentication/types/ME';
import { SUPER_ADMINISTRATEUR, ADMINISTRATEUR_GROUPEMENT, AMBASSADEUR } from '../Constant/roles';
import { TypeProject } from '../components/Main/Content/Todo/TodoCreateProject/AlertDialogProjectComponent/AlertDialogProjectComponent';

export const adminFilter = (idGroupement: string) => {
  const must: any[] = [{ term: { isRemoved: false } }, { term: { 'groupement.id': idGroupement } }];
  return {
    must,
  };
};

export const noAdminFilter = (currentUser: ME_me, idGroupement: string) => {
  const isCollaboGroupment = currentUser && currentUser.userPersonnel !== null;
  const isCollaboPharmacie =
    currentUser && (currentUser.userPpersonnel !== null || currentUser.userTitulaire !== null);
  let moreShould: any[] = [];
  // const moreMust: any[] = [];

  const must = [{ term: { 'groupement.id': idGroupement } }, { term: { isRemoved: false } }];

  const should: any[] = [{ term: { 'user.id': currentUser && currentUser.id } }];

  if (isCollaboGroupment) {
    moreShould = moreShould.concat([{ term: { typeProject: TypeProject.GROUPEMENT } }]);
  }

  if (isCollaboPharmacie) {
    moreShould = moreShould.concat([
      { terms: { typeProject: [TypeProject.PHARMACIE, TypeProject.GROUPEMENT] } },
    ]);
  }

  return {
    must,
    should: [...should, ...moreShould],
    minimum_should_match: 1,
  };
};

export const projectFilter = (currentUser: ME_me, idGroupement: string) => {
  return currentUser &&
    currentUser.role &&
    (currentUser.role.code === SUPER_ADMINISTRATEUR ||
      currentUser.role.code === ADMINISTRATEUR_GROUPEMENT)
    ? adminFilter(idGroupement)
    : noAdminFilter(currentUser, idGroupement);
};
