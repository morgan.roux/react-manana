import { useState } from 'react'

const useLocaleStorage = <T extends string>(
  key: string,
  initialValue: T
): [T, (newValue: T | ((previous: T) => T)) => void] => {
  const [storedValue, setStoredValue] = useState<T>(() => {
    return (window.localStorage.getItem(key) as T) || initialValue
  })

  const setValue = (value: T | ((previous: T) => T)) => {
    try {
      const valueToStore =
        value instanceof Function ? value(storedValue) : value
      setStoredValue(valueToStore)
      window.localStorage.setItem(key, valueToStore)
    } catch (error) {
      // A more advanced implementation would handle the error case
      console.log(error)
    }
  }

  return [storedValue, setValue]
}

export default useLocaleStorage
