/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_COLLABORATEURS
// ====================================================

export interface GET_COLLABORATEURS_collaborateurs_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_COLLABORATEURS_collaborateurs_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_COLLABORATEURS_collaborateurs {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: GET_COLLABORATEURS_collaborateurs_role | null;
  photo: GET_COLLABORATEURS_collaborateurs_photo | null;
  phoneNumber: string | null;
}

export interface GET_COLLABORATEURS {
  collaborateurs: (GET_COLLABORATEURS_collaborateurs | null)[] | null;
}

export interface GET_COLLABORATEURSVariables {
  idPharmacie?: string | null;
}
