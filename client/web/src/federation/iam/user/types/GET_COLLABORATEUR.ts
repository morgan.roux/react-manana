/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_COLLABORATEUR
// ====================================================

export interface GET_COLLABORATEUR_user_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_COLLABORATEUR_user_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_COLLABORATEUR_user {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: GET_COLLABORATEUR_user_role | null;
  photo: GET_COLLABORATEUR_user_photo | null;
  phoneNumber: string | null;
}

export interface GET_COLLABORATEUR {
  user: GET_COLLABORATEUR_user | null;
}

export interface GET_COLLABORATEURVariables {
  id: string;
}
