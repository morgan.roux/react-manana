import gql from 'graphql-tag';
import { MINIMAL_USER_INFO, FULL_USER_INFO } from './fragment';

export const GET_COLLABORATEURS = gql`
  query GET_COLLABORATEURS($idPharmacie: ID) {
    collaborateurs(idPharmacie: $idPharmacie) {
      ...UserInfo
    }
  }

  ${MINIMAL_USER_INFO}
`;

export const GET_FULL_COLLABORATEURS = gql`
  query GET_FULL_COLLABORATEURS($idPharmacie: ID) {
    collaborateurs(idPharmacie: $idPharmacie) {
      ...UserInfo
    }
  }

  ${FULL_USER_INFO}
`;

export const GET_COLLABORATEUR = gql`
  query GET_COLLABORATEUR($id: ID!) {
    user(id: $id) {
      ...UserInfo
    }
  }

  ${MINIMAL_USER_INFO}
`;
