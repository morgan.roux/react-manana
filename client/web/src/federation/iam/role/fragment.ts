import gql from 'graphql-tag';

export const ROLE_INFO = gql`
  fragment RoleInfo on Role {
    id
    code
    nom
  }
`;
