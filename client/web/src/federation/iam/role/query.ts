import gql from 'graphql-tag';
import { ROLE_INFO } from './fragment';

export const GET_ROLES = gql`
  query GET_ROLES {
    roles {
      ...RoleInfo
    }
  }

  ${ROLE_INFO}
`;
