/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RoleInfo
// ====================================================

export interface RoleInfo {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}
