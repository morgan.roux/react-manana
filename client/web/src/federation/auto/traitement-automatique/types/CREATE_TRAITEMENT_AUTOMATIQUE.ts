/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TATraitementInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_TRAITEMENT_AUTOMATIQUE
// ====================================================

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_type {
  __typename: "TATraitementType";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_participants_photo | null;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_fonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_tache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_derniereExecutionCloturee_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_derniereExecutionCloturee_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_derniereExecutionCloturee_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_derniereExecutionCloturee_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_derniereExecutionCloturee {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_derniereExecutionCloturee_dernierChangementStatut | null;
  changementStatuts: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_derniereExecutionCloturee_changementStatuts[];
  urgence: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_derniereExecutionCloturee_urgence;
  importance: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_derniereExecutionCloturee_importance;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_execution_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_execution_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_execution_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_execution_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_execution {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_execution_dernierChangementStatut | null;
  changementStatuts: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_execution_changementStatuts[];
  urgence: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_execution_urgence;
  importance: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_execution_importance;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement {
  __typename: "TATraitement";
  id: string;
  dataType: string;
  description: string;
  termine: boolean;
  nombreTraitementsCloture: number;
  nombreTraitementsEnRetard: number;
  joursSemaine: number[] | null;
  dateDebut: any | null;
  isPrivate: boolean;
  type: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_type;
  participants: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_participants[];
  importance: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_importance;
  fonction: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_fonction | null;
  tache: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_tache | null;
  repetition: number;
  repetitionUnite: string;
  recurrence: string;
  heure: string;
  dateHeureFin: any | null;
  nombreOccurencesFin: number | null;
  urgence: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_urgence;
  derniereExecutionCloturee: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_derniereExecutionCloturee | null;
  execution: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement_execution;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUE {
  createOneTATraitement: CREATE_TRAITEMENT_AUTOMATIQUE_createOneTATraitement;
}

export interface CREATE_TRAITEMENT_AUTOMATIQUEVariables {
  input: TATraitementInput;
}
