/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TATraitementAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_TRAITEMENT_AUTOMATIQUES_AGGREGATE
// ====================================================

export interface GET_TRAITEMENT_AUTOMATIQUES_AGGREGATE_tATraitementAggregate_count {
  __typename: "TATraitementCountAggregate";
  id: number | null;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_AGGREGATE_tATraitementAggregate {
  __typename: "TATraitementAggregateResponse";
  count: GET_TRAITEMENT_AUTOMATIQUES_AGGREGATE_tATraitementAggregate_count | null;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_AGGREGATE {
  tATraitementAggregate: GET_TRAITEMENT_AUTOMATIQUES_AGGREGATE_tATraitementAggregate[];
}

export interface GET_TRAITEMENT_AUTOMATIQUES_AGGREGATEVariables {
  filter?: TATraitementAggregateFilter | null;
}
