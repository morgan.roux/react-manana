/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, TATraitementFilter, TATraitementSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION
// ====================================================

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_type {
  __typename: "TATraitementType";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_participants_photo | null;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_fonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_tache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_derniereExecutionCloturee_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_derniereExecutionCloturee_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_derniereExecutionCloturee_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_derniereExecutionCloturee_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_derniereExecutionCloturee {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_derniereExecutionCloturee_dernierChangementStatut | null;
  changementStatuts: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_derniereExecutionCloturee_changementStatuts[];
  urgence: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_derniereExecutionCloturee_urgence;
  importance: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_derniereExecutionCloturee_importance;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_execution_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_execution_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_execution_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_execution_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_execution {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_execution_dernierChangementStatut | null;
  changementStatuts: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_execution_changementStatuts[];
  urgence: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_execution_urgence;
  importance: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_execution_importance;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes {
  __typename: "TATraitement";
  id: string;
  dataType: string;
  description: string;
  termine: boolean;
  nombreTraitementsCloture: number;
  nombreTraitementsEnRetard: number;
  joursSemaine: number[] | null;
  dateDebut: any | null;
  isPrivate: boolean;
  type: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_type;
  participants: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_participants[];
  importance: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_importance;
  fonction: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_fonction | null;
  tache: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_tache | null;
  repetition: number;
  repetitionUnite: string;
  recurrence: string;
  heure: string;
  dateHeureFin: any | null;
  nombreOccurencesFin: number | null;
  urgence: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_urgence;
  derniereExecutionCloturee: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_derniereExecutionCloturee | null;
  execution: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes_execution;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements {
  __typename: "TATraitementConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements_nodes[];
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION {
  tATraitements: GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION_tATraitements;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTIONVariables {
  paging?: OffsetPaging | null;
  filter?: TATraitementFilter | null;
  sorting?: TATraitementSort[] | null;
}
