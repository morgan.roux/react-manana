import gql from 'graphql-tag';
import { PARAMETRE_TRAITEMENT_AUTO_INFO, FULL_TRAITEMENT_AUTO_INFO } from './fragment';

export const GET_TRAITEMENT_AUTOMATIQUES = gql`
  query GET_TRAITEMENT_AUTOMATIQUES(
    $paging: OffsetPaging
    $filter: TATraitementFilter
    $sorting: [TATraitementSort!]
  ) {
    tATraitements(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
         ...TraitementInfo
      }
    }
  }

  ${PARAMETRE_TRAITEMENT_AUTO_INFO}
`;

export const GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION = gql`
  query GET_TRAITEMENT_AUTOMATIQUES_WITH_EXECUTION(
    $paging: OffsetPaging
    $filter: TATraitementFilter
    $sorting: [TATraitementSort!]
  ) {
    tATraitements(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
         ...TraitementInfo
      }
    }
  }

  ${FULL_TRAITEMENT_AUTO_INFO}
`;

export const GET_TRAITEMENT_AUTOMATIQUES_AGGREGATE = gql`
  query GET_TRAITEMENT_AUTOMATIQUES_AGGREGATE(
    $filter: TATraitementAggregateFilter
  ) {
    tATraitementAggregate(filter: $filter) {
      count{
        id
      }
    }
  }

`;


export const GET_TRAITEMENT_AUTOMATIQUES_TYPES = gql`
  query GET_TRAITEMENT_AUTOMATIQUES_TYPES(
    $paging: OffsetPaging
    $filter: TATraitementTypeFilter
    $sorting: [TATraitementTypeSort!]
  ) {
    tATraitementTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
        id
        code
        libelle
      }
    }
  }

`;