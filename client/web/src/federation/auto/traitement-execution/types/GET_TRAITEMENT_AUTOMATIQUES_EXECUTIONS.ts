/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, TATraitementExecutionFilter, TATraitementExecutionSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS
// ====================================================

export interface GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_tATraitementExecutions_nodes_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_tATraitementExecutions_nodes_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_tATraitementExecutions_nodes_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_tATraitementExecutions_nodes_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_tATraitementExecutions_nodes {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_tATraitementExecutions_nodes_dernierChangementStatut | null;
  changementStatuts: GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_tATraitementExecutions_nodes_changementStatuts[];
  urgence: GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_tATraitementExecutions_nodes_urgence;
  importance: GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_tATraitementExecutions_nodes_importance;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_tATraitementExecutions {
  __typename: "TATraitementExecutionConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_tATraitementExecutions_nodes[];
}

export interface GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS {
  tATraitementExecutions: GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_tATraitementExecutions;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONSVariables {
  paging?: OffsetPaging | null;
  filter?: TATraitementExecutionFilter | null;
  sorting?: TATraitementExecutionSort[] | null;
}
