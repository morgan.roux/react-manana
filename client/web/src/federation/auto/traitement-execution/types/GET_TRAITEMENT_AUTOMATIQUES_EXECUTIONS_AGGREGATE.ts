/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TATraitementExecutionAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE
// ====================================================

export interface GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE_tATraitementExecutionAggregate_count {
  __typename: "TATraitementExecutionCountAggregate";
  id: number | null;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE_tATraitementExecutionAggregate {
  __typename: "TATraitementExecutionAggregateResponse";
  count: GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE_tATraitementExecutionAggregate_count | null;
}

export interface GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE {
  tATraitementExecutionAggregate: GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATE_tATraitementExecutionAggregate[];
}

export interface GET_TRAITEMENT_AUTOMATIQUES_EXECUTIONS_AGGREGATEVariables {
  filter?: TATraitementExecutionAggregateFilter | null;
}
