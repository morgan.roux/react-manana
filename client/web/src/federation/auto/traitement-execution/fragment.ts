import gql from 'graphql-tag';

export const TRAITEMENT_EXECUTION_AUTO_INFO = gql`
  fragment TraitementExecutionInfo on TATraitementExecution {
      id
      prochainement
      dateEcheance
      dernierChangementStatut {
        id
        status
      }
      changementStatuts {
        id
        status
      }
      urgence {
        id
        code
        libelle
        couleur
      }
      importance {
        id
        ordre
        libelle
        couleur
      }
    
  }
`;
