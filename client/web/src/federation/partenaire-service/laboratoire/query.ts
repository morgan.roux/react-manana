import gql from 'graphql-tag';
import { MINIMAL_USER_INFO } from '../../iam/user/fragment';

export const GET_LABORATOIRES = gql`
  query GET_LABORATOIRES(
    $paging: OffsetPaging
    $filter: LaboratoireFilter
    $sorting: [LaboratoireSort!]
    $idPharmacie: String!
  ) {
    laboratoires(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        id
        nom
        partenariat(idPharmacie: $idPharmacie) {
          id
          dateDebut
          dateFin
          idPharmacie
          type {
            id
            libelle
            code
          }
          responsables(idPharmacie: $idPharmacie) {
            ...UserInfo
          }
          statut {
            id
            libelle
            code
          }
        }
      }
    }
  }
  ${MINIMAL_USER_INFO}
`;

export const GET_LABORATOIRE = gql`
  query GET_LABORATOIRE($id: ID!, $idPharmacie: String!) {
    laboratoire(id: $id) {
      id
      nom
      partenariat(idPharmacie: $idPharmacie) {
        id
        dateDebut
        dateFin
        idPharmacie
        typeReunion
        type {
          id
          libelle
          code
        }
        statut {
          id
          libelle
          code
        }
        responsables(idPharmacie: $idPharmacie) {
          ...UserInfo
        }
      }

      laboratoireSuite {
        id
        adresse
        codePostal
        telephone
        ville
        email
      }
    }
  }
  ${MINIMAL_USER_INFO}
`;

export const DO_PUBLISH_PRODUIT_REFERENCES = gql`
  query PUBLISH_PRODUIT_REFERENCES($idPharmacie: String) {
    publishProduitReferences(idPharmacie: $idPharmacie)
  }
`;
