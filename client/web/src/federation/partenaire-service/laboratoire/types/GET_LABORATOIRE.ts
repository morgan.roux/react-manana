/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_LABORATOIRE
// ====================================================

export interface GET_LABORATOIRE_laboratoire_partenariat_type {
  __typename: "PRTPartenariatType";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_LABORATOIRE_laboratoire_partenariat_statut {
  __typename: "PRTPartenariatStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_LABORATOIRE_laboratoire_partenariat_responsables_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_LABORATOIRE_laboratoire_partenariat_responsables_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_LABORATOIRE_laboratoire_partenariat_responsables {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: GET_LABORATOIRE_laboratoire_partenariat_responsables_role | null;
  photo: GET_LABORATOIRE_laboratoire_partenariat_responsables_photo | null;
  phoneNumber: string | null;
}

export interface GET_LABORATOIRE_laboratoire_partenariat {
  __typename: "PRTPartenariat";
  id: string;
  dateDebut: any;
  dateFin: any | null;
  idPharmacie: string;
  typeReunion: string;
  type: GET_LABORATOIRE_laboratoire_partenariat_type;
  statut: GET_LABORATOIRE_laboratoire_partenariat_statut;
  responsables: GET_LABORATOIRE_laboratoire_partenariat_responsables[];
}

export interface GET_LABORATOIRE_laboratoire_laboratoireSuite {
  __typename: "LaboratoireSuite";
  id: string;
  adresse: string;
  codePostal: string | null;
  telephone: string | null;
  ville: string | null;
  email: string | null;
}

export interface GET_LABORATOIRE_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nom: string;
  partenariat: GET_LABORATOIRE_laboratoire_partenariat | null;
  laboratoireSuite: GET_LABORATOIRE_laboratoire_laboratoireSuite | null;
}

export interface GET_LABORATOIRE {
  laboratoire: GET_LABORATOIRE_laboratoire | null;
}

export interface GET_LABORATOIREVariables {
  id: string;
  idPharmacie: string;
}
