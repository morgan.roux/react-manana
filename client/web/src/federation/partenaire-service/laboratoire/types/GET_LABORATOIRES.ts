/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, LaboratoireFilter, LaboratoireSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_LABORATOIRES
// ====================================================

export interface GET_LABORATOIRES_laboratoires_nodes_partenariat_type {
  __typename: "PRTPartenariatType";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_LABORATOIRES_laboratoires_nodes_partenariat_responsables_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_LABORATOIRES_laboratoires_nodes_partenariat_responsables_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_LABORATOIRES_laboratoires_nodes_partenariat_responsables {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: GET_LABORATOIRES_laboratoires_nodes_partenariat_responsables_role | null;
  photo: GET_LABORATOIRES_laboratoires_nodes_partenariat_responsables_photo | null;
  phoneNumber: string | null;
}

export interface GET_LABORATOIRES_laboratoires_nodes_partenariat_statut {
  __typename: "PRTPartenariatStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_LABORATOIRES_laboratoires_nodes_partenariat {
  __typename: "PRTPartenariat";
  id: string;
  dateDebut: any;
  dateFin: any | null;
  idPharmacie: string;
  type: GET_LABORATOIRES_laboratoires_nodes_partenariat_type;
  responsables: GET_LABORATOIRES_laboratoires_nodes_partenariat_responsables[];
  statut: GET_LABORATOIRES_laboratoires_nodes_partenariat_statut;
}

export interface GET_LABORATOIRES_laboratoires_nodes {
  __typename: "Laboratoire";
  id: string;
  nom: string;
  partenariat: GET_LABORATOIRES_laboratoires_nodes_partenariat | null;
}

export interface GET_LABORATOIRES_laboratoires {
  __typename: "LaboratoireConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_LABORATOIRES_laboratoires_nodes[];
}

export interface GET_LABORATOIRES {
  laboratoires: GET_LABORATOIRES_laboratoires;
}

export interface GET_LABORATOIRESVariables {
  paging?: OffsetPaging | null;
  filter?: LaboratoireFilter | null;
  sorting?: LaboratoireSort[] | null;
  idPharmacie: string;
}
