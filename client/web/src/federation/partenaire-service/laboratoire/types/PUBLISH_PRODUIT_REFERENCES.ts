/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PUBLISH_PRODUIT_REFERENCES
// ====================================================

export interface PUBLISH_PRODUIT_REFERENCES {
  publishProduitReferences: boolean;
}

export interface PUBLISH_PRODUIT_REFERENCESVariables {
  idPharmacie?: string | null;
}
