/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTConditionCommercialeStatutFilter, PRTConditionCommercialeStatutSort } from "./../../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_STATUS_CONDITION
// ====================================================

export interface GET_STATUS_CONDITION_pRTConditionCommercialeStatuts_nodes {
  __typename: "PRTConditionCommercialeStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_STATUS_CONDITION_pRTConditionCommercialeStatuts {
  __typename: "PRTConditionCommercialeStatutConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_STATUS_CONDITION_pRTConditionCommercialeStatuts_nodes[];
}

export interface GET_STATUS_CONDITION {
  pRTConditionCommercialeStatuts: GET_STATUS_CONDITION_pRTConditionCommercialeStatuts;
}

export interface GET_STATUS_CONDITIONVariables {
  paging?: OffsetPaging | null;
  filter?: PRTConditionCommercialeStatutFilter | null;
  sorting?: PRTConditionCommercialeStatutSort[] | null;
}
