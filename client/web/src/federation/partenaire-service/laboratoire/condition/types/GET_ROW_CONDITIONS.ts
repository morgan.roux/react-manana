/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTConditionCommercialeAggregateFilter } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ROW_CONDITIONS
// ====================================================

export interface GET_ROW_CONDITIONS_pRTConditionCommercialeAggregate_count {
  __typename: "PRTConditionCommercialeCountAggregate";
  id: number | null;
}

export interface GET_ROW_CONDITIONS_pRTConditionCommercialeAggregate {
  __typename: "PRTConditionCommercialeAggregateResponse";
  count: GET_ROW_CONDITIONS_pRTConditionCommercialeAggregate_count | null;
}

export interface GET_ROW_CONDITIONS {
  pRTConditionCommercialeAggregate: GET_ROW_CONDITIONS_pRTConditionCommercialeAggregate[];
}

export interface GET_ROW_CONDITIONSVariables {
  filter?: PRTConditionCommercialeAggregateFilter | null;
}
