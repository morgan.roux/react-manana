/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CHANGE_STATUS
// ====================================================

export interface CHANGE_STATUS_changeStatutPRTConditionCommerciale_type {
  __typename: "PRTConditionCommercialeType";
  id: string;
  libelle: string;
  code: string;
}

export interface CHANGE_STATUS_changeStatutPRTConditionCommerciale_statut {
  __typename: "PRTConditionCommercialeStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CHANGE_STATUS_changeStatutPRTConditionCommerciale_canal {
  __typename: "Canal";
  id: string;
  libelle: string;
  code: string;
}

export interface CHANGE_STATUS_changeStatutPRTConditionCommerciale_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CHANGE_STATUS_changeStatutPRTConditionCommerciale {
  __typename: "PRTConditionCommerciale";
  id: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  idCanal: string | null;
  idType: string;
  idStatut: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  idGroupement: string;
  idPharmacie: string;
  description: string | null;
  type: CHANGE_STATUS_changeStatutPRTConditionCommerciale_type;
  statut: CHANGE_STATUS_changeStatutPRTConditionCommerciale_statut;
  canal: CHANGE_STATUS_changeStatutPRTConditionCommerciale_canal | null;
  fichiers: CHANGE_STATUS_changeStatutPRTConditionCommerciale_fichiers[] | null;
}

export interface CHANGE_STATUS {
  changeStatutPRTConditionCommerciale: CHANGE_STATUS_changeStatutPRTConditionCommerciale;
}

export interface CHANGE_STATUSVariables {
  idStatut: string;
  id: string;
}
