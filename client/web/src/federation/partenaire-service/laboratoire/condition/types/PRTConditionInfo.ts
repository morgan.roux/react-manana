/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTConditionInfo
// ====================================================

export interface PRTConditionInfo_type {
  __typename: "PRTConditionCommercialeType";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTConditionInfo_statut {
  __typename: "PRTConditionCommercialeStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTConditionInfo_canal {
  __typename: "Canal";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTConditionInfo_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRTConditionInfo {
  __typename: "PRTConditionCommerciale";
  id: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  idCanal: string | null;
  idType: string;
  idStatut: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  idGroupement: string;
  idPharmacie: string;
  description: string | null;
  type: PRTConditionInfo_type;
  statut: PRTConditionInfo_statut;
  canal: PRTConditionInfo_canal | null;
  fichiers: PRTConditionInfo_fichiers[] | null;
}
