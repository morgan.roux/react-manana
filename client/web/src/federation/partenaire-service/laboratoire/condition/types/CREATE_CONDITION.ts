/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTConditionCommercialeInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_CONDITION
// ====================================================

export interface CREATE_CONDITION_createOnePRTConditionCommerciale_type {
  __typename: "PRTConditionCommercialeType";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_CONDITION_createOnePRTConditionCommerciale_statut {
  __typename: "PRTConditionCommercialeStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_CONDITION_createOnePRTConditionCommerciale_canal {
  __typename: "Canal";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_CONDITION_createOnePRTConditionCommerciale_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_CONDITION_createOnePRTConditionCommerciale {
  __typename: "PRTConditionCommerciale";
  id: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  idCanal: string | null;
  idType: string;
  idStatut: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  idGroupement: string;
  idPharmacie: string;
  description: string | null;
  type: CREATE_CONDITION_createOnePRTConditionCommerciale_type;
  statut: CREATE_CONDITION_createOnePRTConditionCommerciale_statut;
  canal: CREATE_CONDITION_createOnePRTConditionCommerciale_canal | null;
  fichiers: CREATE_CONDITION_createOnePRTConditionCommerciale_fichiers[] | null;
}

export interface CREATE_CONDITION {
  createOnePRTConditionCommerciale: CREATE_CONDITION_createOnePRTConditionCommerciale;
}

export interface CREATE_CONDITIONVariables {
  input: PRTConditionCommercialeInput;
}
