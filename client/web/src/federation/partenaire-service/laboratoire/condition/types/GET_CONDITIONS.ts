/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTConditionCommercialeFilter, PRTConditionCommercialeSort } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_CONDITIONS
// ====================================================

export interface GET_CONDITIONS_pRTConditionCommerciales_nodes_type {
  __typename: "PRTConditionCommercialeType";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_CONDITIONS_pRTConditionCommerciales_nodes_statut {
  __typename: "PRTConditionCommercialeStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_CONDITIONS_pRTConditionCommerciales_nodes_canal {
  __typename: "Canal";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_CONDITIONS_pRTConditionCommerciales_nodes_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_CONDITIONS_pRTConditionCommerciales_nodes {
  __typename: "PRTConditionCommerciale";
  id: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  idCanal: string | null;
  idType: string;
  idStatut: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  idGroupement: string;
  idPharmacie: string;
  description: string | null;
  type: GET_CONDITIONS_pRTConditionCommerciales_nodes_type;
  statut: GET_CONDITIONS_pRTConditionCommerciales_nodes_statut;
  canal: GET_CONDITIONS_pRTConditionCommerciales_nodes_canal | null;
  fichiers: GET_CONDITIONS_pRTConditionCommerciales_nodes_fichiers[] | null;
}

export interface GET_CONDITIONS_pRTConditionCommerciales {
  __typename: "PRTConditionCommercialeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_CONDITIONS_pRTConditionCommerciales_nodes[];
}

export interface GET_CONDITIONS {
  pRTConditionCommerciales: GET_CONDITIONS_pRTConditionCommerciales;
}

export interface GET_CONDITIONSVariables {
  paging?: OffsetPaging | null;
  filter?: PRTConditionCommercialeFilter | null;
  sorting?: PRTConditionCommercialeSort[] | null;
}
