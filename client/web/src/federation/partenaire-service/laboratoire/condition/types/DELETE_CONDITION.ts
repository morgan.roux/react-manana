/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_CONDITION
// ====================================================

export interface DELETE_CONDITION {
  deleteOnePRTConditionCommerciale: string;
}

export interface DELETE_CONDITIONVariables {
  id: string;
}
