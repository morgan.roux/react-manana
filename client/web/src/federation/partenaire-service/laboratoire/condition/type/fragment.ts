import gql from 'graphql-tag';

export const TYPE_CONDITION_INFO = gql`
  fragment ConditionTypeInfo on PRTConditionCommercialeType {
    id
    libelle
    code
  }
`;
