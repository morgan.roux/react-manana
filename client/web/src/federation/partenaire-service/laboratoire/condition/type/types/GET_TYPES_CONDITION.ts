/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTConditionCommercialeTypeFilter, PRTConditionCommercialeTypeSort } from "./../../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_TYPES_CONDITION
// ====================================================

export interface GET_TYPES_CONDITION_pRTConditionCommercialeTypes_nodes {
  __typename: "PRTConditionCommercialeType";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_TYPES_CONDITION_pRTConditionCommercialeTypes {
  __typename: "PRTConditionCommercialeTypeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_TYPES_CONDITION_pRTConditionCommercialeTypes_nodes[];
}

export interface GET_TYPES_CONDITION {
  pRTConditionCommercialeTypes: GET_TYPES_CONDITION_pRTConditionCommercialeTypes;
}

export interface GET_TYPES_CONDITIONVariables {
  paging?: OffsetPaging | null;
  filter?: PRTConditionCommercialeTypeFilter | null;
  sorting?: PRTConditionCommercialeTypeSort[] | null;
}
