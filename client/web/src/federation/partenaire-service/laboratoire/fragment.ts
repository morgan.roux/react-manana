import gql from 'graphql-tag';

export const PARAMETRE_LABORATOIRE_INFO = gql`
  fragment LaboratoireInfo on Laboratoire {
    id
    nom
    idLaboSuite
    sortie
    laboratoireRattachement {
      id
      nom
    }
    laboratoireRattaches {
      id
      nom
    }
    laboratoireSuite {
      id
      adresse
      codePostal
      telephone
      ville
      email
    }
    pharmacieInfos
    createdAt
    updatedAt
  }
`;
