/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTRendezVousInfo
// ====================================================

export interface PRTRendezVousInfo_invites_fonction {
  __typename: "Fonction";
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}

export interface PRTRendezVousInfo_invites_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface PRTRendezVousInfo_invites_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface PRTRendezVousInfo_invites_departements {
  __typename: "Departement";
  id: string;
  code: string;
  nom: string;
}

export interface PRTRendezVousInfo_invites {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  idFonction: string | null;
  fonction: PRTRendezVousInfo_invites_fonction | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: PRTRendezVousInfo_invites_photo | null;
  contact: PRTRendezVousInfo_invites_contact | null;
  departements: PRTRendezVousInfo_invites_departements[] | null;
}

export interface PRTRendezVousInfo_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PRTRendezVousInfo_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface PRTRendezVousInfo_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: PRTRendezVousInfo_participants_role | null;
  photo: PRTRendezVousInfo_participants_photo | null;
  phoneNumber: string | null;
}

export interface PRTRendezVousInfo_subject {
  __typename: "PRTRendezVousSujetVisite";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTRendezVousInfo {
  __typename: "PRTRendezVous";
  id: string;
  idSubject: string | null;
  ordreJour: string | null;
  heureDebut: string;
  heureFin: string | null;
  dateRendezVous: any;
  idPartenaireTypeAssocie: string;
  typeReunion: string;
  invites: PRTRendezVousInfo_invites[];
  participants: PRTRendezVousInfo_participants[];
  statut: string;
  partenaireType: string;
  subject: PRTRendezVousInfo_subject | null;
  note: string | null;
}
