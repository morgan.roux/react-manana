/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRendezVousAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ROW_RENDEZ_VOUS
// ====================================================

export interface GET_ROW_RENDEZ_VOUS_pRTRendezVousAggregate_count {
  __typename: "PRTRendezVousCountAggregate";
  id: number | null;
}

export interface GET_ROW_RENDEZ_VOUS_pRTRendezVousAggregate {
  __typename: "PRTRendezVousAggregateResponse";
  count: GET_ROW_RENDEZ_VOUS_pRTRendezVousAggregate_count | null;
}

export interface GET_ROW_RENDEZ_VOUS {
  pRTRendezVousAggregate: GET_ROW_RENDEZ_VOUS_pRTRendezVousAggregate[];
}

export interface GET_ROW_RENDEZ_VOUSVariables {
  filter?: PRTRendezVousAggregateFilter | null;
}
