/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTRendezVousSujetVisiteFilter, PRTRendezVousSujetVisiteSort } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_RENDEZ_VOUS_SUJETS_VISITES
// ====================================================

export interface GET_RENDEZ_VOUS_SUJETS_VISITES_pRTRendezVousSujetVisites_nodes {
  __typename: "PRTRendezVousSujetVisite";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_RENDEZ_VOUS_SUJETS_VISITES_pRTRendezVousSujetVisites {
  __typename: "PRTRendezVousSujetVisiteConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_RENDEZ_VOUS_SUJETS_VISITES_pRTRendezVousSujetVisites_nodes[];
}

export interface GET_RENDEZ_VOUS_SUJETS_VISITES {
  pRTRendezVousSujetVisites: GET_RENDEZ_VOUS_SUJETS_VISITES_pRTRendezVousSujetVisites;
}

export interface GET_RENDEZ_VOUS_SUJETS_VISITESVariables {
  paging?: OffsetPaging | null;
  filter?: PRTRendezVousSujetVisiteFilter | null;
  sorting?: PRTRendezVousSujetVisiteSort[] | null;
}
