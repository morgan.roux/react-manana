import gql from 'graphql-tag';

export const GET_RENDEZ_VOUS_SUJETS_VISITES = gql`
  query GET_RENDEZ_VOUS_SUJETS_VISITES(
    $paging: OffsetPaging
    $filter: PRTRendezVousSujetVisiteFilter
    $sorting: [PRTRendezVousSujetVisiteSort!]
  ) {
    pRTRendezVousSujetVisites(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
        id
        libelle
        code
      }
    }
  }
`;
