import gql from 'graphql-tag';
import { RENDEZ_VOUS_INFO } from './fragment';

export const GET_RENDEZ_VOUS = gql`
  query GET_RENDEZ_VOUS(
    $paging: OffsetPaging
    $filter: PRTRendezVousFilter
    $sorting: [PRTRendezVousSort!]
  ) {
    pRTRendezVous(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...PRTRendezVousInfo
      }
    }
  }
  ${RENDEZ_VOUS_INFO}
`;

export const GET_ROW_RENDEZ_VOUS = gql`
  query GET_ROW_RENDEZ_VOUS($filter: PRTRendezVousAggregateFilter) {
    pRTRendezVousAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;
