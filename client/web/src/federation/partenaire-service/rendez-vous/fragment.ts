import gql from 'graphql-tag';
import { PRT_CONTACT_FULL_INFO } from '../contact/fragment';
import { FULL_USER_INFO } from './../../iam/user/fragment';
export const RENDEZ_VOUS_INFO = gql`
  fragment PRTRendezVousInfo on PRTRendezVous {
    id
    idSubject
    ordreJour
    heureDebut
    heureFin
    dateRendezVous
    idPartenaireTypeAssocie
    typeReunion
    invites {
      ...PRTContactFullInfo
    }
    participants {
      ...UserInfo
    }
    statut
    partenaireType
    subject {
      id
      libelle
      code
    }
    note
  }

  ${PRT_CONTACT_FULL_INFO}
  ${FULL_USER_INFO}
`;
