import gql from 'graphql-tag';

export const FULL_MISE_AVANT_INFO = gql`
  fragment MiseAvantInfo on PRTMiseAvant {
    id
    code
    libelle
    couleur
    idPharmacie
  }
`;
