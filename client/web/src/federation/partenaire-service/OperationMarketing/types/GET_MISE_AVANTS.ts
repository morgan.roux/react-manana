/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTMiseAvantFilter, OffsetPaging, PRTMiseAvantSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_MISE_AVANTS
// ====================================================

export interface GET_MISE_AVANTS_pRTMiseAvants_nodes {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
  idPharmacie: string | null;
}

export interface GET_MISE_AVANTS_pRTMiseAvants {
  __typename: "PRTMiseAvantConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_MISE_AVANTS_pRTMiseAvants_nodes[];
}

export interface GET_MISE_AVANTS {
  pRTMiseAvants: GET_MISE_AVANTS_pRTMiseAvants;
}

export interface GET_MISE_AVANTSVariables {
  filter?: PRTMiseAvantFilter | null;
  paging?: OffsetPaging | null;
  sorting?: PRTMiseAvantSort[] | null;
}
