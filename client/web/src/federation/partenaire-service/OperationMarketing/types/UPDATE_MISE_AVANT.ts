/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateOnePRTMiseAvantInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_MISE_AVANT
// ====================================================

export interface UPDATE_MISE_AVANT_updateOnePRTMiseAvant {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
  idPharmacie: string | null;
}

export interface UPDATE_MISE_AVANT {
  updateOnePRTMiseAvant: UPDATE_MISE_AVANT_updateOnePRTMiseAvant;
}

export interface UPDATE_MISE_AVANTVariables {
  update: UpdateOnePRTMiseAvantInput;
}
