/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreateOnePRTMiseAvantInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_MISE_AVANT
// ====================================================

export interface CREATE_MISE_AVANT_createOnePRTMiseAvant {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
  idPharmacie: string | null;
}

export interface CREATE_MISE_AVANT {
  createOnePRTMiseAvant: CREATE_MISE_AVANT_createOnePRTMiseAvant;
}

export interface CREATE_MISE_AVANTVariables {
  input: CreateOnePRTMiseAvantInput;
}
