import gql from 'graphql-tag';
import { FULL_MISE_AVANT_INFO } from './fragment';

export const CREATE_MISE_AVANT = gql`
  mutation CREATE_MISE_AVANT($input: CreateOnePRTMiseAvantInput!) {
    createOnePRTMiseAvant(input: $input) {
      ...MiseAvantInfo
    }
  }
  ${FULL_MISE_AVANT_INFO}
`;

export const UPDATE_MISE_AVANT = gql`
  mutation UPDATE_MISE_AVANT($update: UpdateOnePRTMiseAvantInput!) {
    updateOnePRTMiseAvant(input: $update) {
      ...MiseAvantInfo
    }
  }
  ${FULL_MISE_AVANT_INFO}
`;

export const DELETE_MISE_AVANT = gql`
  mutation DELETE_MISE_AVANT($input: DeleteOnePRTMiseAvantInput!) {
    deleteOnePRTMiseAvant(input: $input) {
      id
    }
  }
`;
