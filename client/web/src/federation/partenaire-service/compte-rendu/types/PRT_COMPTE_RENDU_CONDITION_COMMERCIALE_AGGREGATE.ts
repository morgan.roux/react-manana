/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTCompteRenduConditionCommercialeAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: PRT_COMPTE_RENDU_CONDITION_COMMERCIALE_AGGREGATE
// ====================================================

export interface PRT_COMPTE_RENDU_CONDITION_COMMERCIALE_AGGREGATE_pRTCompteRenduConditionCommercialeAggregate_count {
  __typename: "PRTCompteRenduConditionCommercialeCountAggregate";
  id: number | null;
}

export interface PRT_COMPTE_RENDU_CONDITION_COMMERCIALE_AGGREGATE_pRTCompteRenduConditionCommercialeAggregate {
  __typename: "PRTCompteRenduConditionCommercialeAggregateResponse";
  count: PRT_COMPTE_RENDU_CONDITION_COMMERCIALE_AGGREGATE_pRTCompteRenduConditionCommercialeAggregate_count | null;
}

export interface PRT_COMPTE_RENDU_CONDITION_COMMERCIALE_AGGREGATE {
  pRTCompteRenduConditionCommercialeAggregate: PRT_COMPTE_RENDU_CONDITION_COMMERCIALE_AGGREGATE_pRTCompteRenduConditionCommercialeAggregate[];
}

export interface PRT_COMPTE_RENDU_CONDITION_COMMERCIALE_AGGREGATEVariables {
  filter?: PRTCompteRenduConditionCommercialeAggregateFilter | null;
}
