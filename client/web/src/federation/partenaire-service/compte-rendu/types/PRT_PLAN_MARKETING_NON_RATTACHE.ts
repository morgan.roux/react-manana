/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTCompteRenduPlanMarketingInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: PRT_PLAN_MARKETING_NON_RATTACHE
// ====================================================

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_actions_importance | null;
  urgence: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_actions_urgence | null;
  nombreCommentaires: number | null;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_produits_produit_famille {
  __typename: "ProduitFamille";
  id: string;
  libelleFamille: string;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_produits_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_produits_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_produits_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_produits_produit_produitPhoto_fichier;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_produits_produit {
  __typename: "ProduitInfo";
  id: string;
  libelle: string;
  famille: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_produits_produit_famille;
  produitCode: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_produits_produit_produitCode;
  produitPhoto: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_produits_produit_produitPhoto | null;
  quantitePrevue: number | null;
  quantiteRealise: number | null;
  prixUnitaire: number | null;
  remisePrevue: number | null;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_produits {
  __typename: "PlanMarketingProduitInfo";
  id: string;
  produit: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_produits_produit;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_modeReglement {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  remise: number | null;
  montant: number | null;
  typeRemuneration: string;
  remuneration: string | null;
  type: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_type;
  statut: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_statut;
  fichiers: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_fichiers[] | null;
  miseAvants: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_miseAvants[] | null;
  actions: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_actions[] | null;
  produits: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_produits[] | null;
  modeReglement: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing_modeReglement | null;
  dateStatutCloture: any | null;
  montantStatutCloture: number | null;
  montantStatutRealise: number | null;
  dateStatutRealise: any | null;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHE {
  compteRenduPlanMarketing: PRT_PLAN_MARKETING_NON_RATTACHE_compteRenduPlanMarketing[] | null;
}

export interface PRT_PLAN_MARKETING_NON_RATTACHEVariables {
  idPharmacie?: string | null;
  input: PRTCompteRenduPlanMarketingInput;
}
