/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTCompteRenduInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PRT_COMPTE_RENDU
// ====================================================

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_collaborateurs_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_collaborateurs_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_collaborateurs {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_collaborateurs_role | null;
  photo: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_collaborateurs_photo | null;
  phoneNumber: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_responsables_fonction {
  __typename: "Fonction";
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_responsables_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_responsables_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_responsables_departements {
  __typename: "Departement";
  id: string;
  code: string;
  nom: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_responsables {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  idFonction: string | null;
  fonction: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_responsables_fonction | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_responsables_photo | null;
  contact: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_responsables_contact | null;
  departements: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_responsables_departements[] | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_contacts_fonction {
  __typename: "Fonction";
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_contacts_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_contacts_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_contacts_departements {
  __typename: "Departement";
  id: string;
  code: string;
  nom: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_contacts {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  idFonction: string | null;
  fonction: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_contacts_fonction | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_contacts_photo | null;
  contact: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_contacts_contact | null;
  departements: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_contacts_departements[] | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_participants_role | null;
  photo: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_participants_photo | null;
  phoneNumber: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_type {
  __typename: "PRTSuiviOperationnelType";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_statut {
  __typename: "PRTSuiviOperationnelStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels {
  __typename: "PRTSuiviOperationnel";
  id: string;
  titre: string;
  description: string | null;
  dateHeure: any;
  createdBy: string;
  partenaireType: string;
  idTypeAssocie: string;
  idGroupement: string;
  montant: number;
  idImportance: string | null;
  idTache: string | null;
  idFonction: string | null;
  contacts: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_contacts[] | null;
  importance: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_importance | null;
  idPharmacie: string;
  participants: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_participants[];
  type: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_type;
  statut: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_statut;
  fichiers: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels_fichiers[] | null;
  nombreCommentaires: number | null;
  nombreReaction: number | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_actions_importance | null;
  urgence: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_actions_urgence | null;
  nombreCommentaires: number | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_produits_produit_famille {
  __typename: "ProduitFamille";
  id: string;
  libelleFamille: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_produits_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_produits_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_produits_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_produits_produit_produitPhoto_fichier;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_produits_produit {
  __typename: "ProduitInfo";
  id: string;
  libelle: string;
  famille: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_produits_produit_famille;
  produitCode: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_produits_produit_produitCode;
  produitPhoto: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_produits_produit_produitPhoto | null;
  quantitePrevue: number | null;
  quantiteRealise: number | null;
  prixUnitaire: number | null;
  remisePrevue: number | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_produits {
  __typename: "PlanMarketingProduitInfo";
  id: string;
  produit: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_produits_produit;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_modeReglement {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  remise: number | null;
  montant: number | null;
  typeRemuneration: string;
  remuneration: string | null;
  type: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_type;
  statut: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_statut;
  fichiers: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_fichiers[] | null;
  miseAvants: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_miseAvants[] | null;
  actions: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_actions[] | null;
  produits: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_produits[] | null;
  modeReglement: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings_modeReglement | null;
  dateStatutCloture: any | null;
  montantStatutCloture: number | null;
  montantStatutRealise: number | null;
  dateStatutRealise: any | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_conditionsCommerciales_type {
  __typename: "PRTConditionCommercialeType";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_conditionsCommerciales_statut {
  __typename: "PRTConditionCommercialeStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_conditionsCommerciales_canal {
  __typename: "Canal";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_conditionsCommerciales_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_conditionsCommerciales {
  __typename: "PRTConditionCommerciale";
  id: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  idCanal: string | null;
  idType: string;
  idStatut: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  idGroupement: string;
  idPharmacie: string;
  description: string | null;
  type: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_conditionsCommerciales_type;
  statut: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_conditionsCommerciales_statut;
  canal: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_conditionsCommerciales_canal | null;
  fichiers: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_conditionsCommerciales_fichiers[] | null;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_notificationLogs {
  __typename: "NotificationLogs";
  id: string;
  titre: string;
  message: string;
  type: string;
  expediteur: string | null;
  createdAt: any;
  updatedAt: any;
}

export interface CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu {
  __typename: "PRTCompteRendu";
  id: string;
  titre: string;
  remiseEchantillon: boolean;
  gestionPerime: string | null;
  rapportVisite: string | null;
  conclusion: string | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  createdAt: any;
  idNotificationLogs: string | null;
  idPharmacie: string;
  collaborateurs: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_collaborateurs[] | null;
  responsables: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_responsables[] | null;
  suiviOperationnels: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_suiviOperationnels[] | null;
  planMarketings: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_planMarketings[] | null;
  conditionsCommerciales: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_conditionsCommerciales[] | null;
  notificationLogs: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu_notificationLogs | null;
}

export interface CREATE_PRT_COMPTE_RENDU {
  createOnePRTCompteRendu: CREATE_PRT_COMPTE_RENDU_createOnePRTCompteRendu;
}

export interface CREATE_PRT_COMPTE_RENDUVariables {
  idPharmacie?: string | null;
  input: PRTCompteRenduInput;
}
