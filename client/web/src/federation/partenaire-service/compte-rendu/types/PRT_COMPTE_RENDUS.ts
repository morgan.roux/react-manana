/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTCompteRenduFilter, PRTCompteRenduSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: PRT_COMPTE_RENDUS
// ====================================================

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_collaborateurs_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_collaborateurs_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_collaborateurs {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_collaborateurs_role | null;
  photo: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_collaborateurs_photo | null;
  phoneNumber: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_responsables_fonction {
  __typename: "Fonction";
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_responsables_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_responsables_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_responsables_departements {
  __typename: "Departement";
  id: string;
  code: string;
  nom: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_responsables {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  idFonction: string | null;
  fonction: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_responsables_fonction | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_responsables_photo | null;
  contact: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_responsables_contact | null;
  departements: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_responsables_departements[] | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_contacts_fonction {
  __typename: "Fonction";
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_contacts_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_contacts_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_contacts_departements {
  __typename: "Departement";
  id: string;
  code: string;
  nom: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_contacts {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  idFonction: string | null;
  fonction: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_contacts_fonction | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_contacts_photo | null;
  contact: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_contacts_contact | null;
  departements: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_contacts_departements[] | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_participants_role | null;
  photo: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_participants_photo | null;
  phoneNumber: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_type {
  __typename: "PRTSuiviOperationnelType";
  id: string;
  libelle: string;
  code: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_statut {
  __typename: "PRTSuiviOperationnelStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels {
  __typename: "PRTSuiviOperationnel";
  id: string;
  titre: string;
  description: string | null;
  dateHeure: any;
  createdBy: string;
  partenaireType: string;
  idTypeAssocie: string;
  idGroupement: string;
  montant: number;
  idImportance: string | null;
  idTache: string | null;
  idFonction: string | null;
  contacts: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_contacts[] | null;
  importance: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_importance | null;
  idPharmacie: string;
  participants: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_participants[];
  type: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_type;
  statut: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_statut;
  fichiers: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels_fichiers[] | null;
  nombreCommentaires: number | null;
  nombreReaction: number | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_actions_importance | null;
  urgence: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_actions_urgence | null;
  nombreCommentaires: number | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_produits_produit_famille {
  __typename: "ProduitFamille";
  id: string;
  libelleFamille: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_produits_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_produits_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_produits_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_produits_produit_produitPhoto_fichier;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_produits_produit {
  __typename: "ProduitInfo";
  id: string;
  libelle: string;
  famille: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_produits_produit_famille;
  produitCode: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_produits_produit_produitCode;
  produitPhoto: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_produits_produit_produitPhoto | null;
  quantitePrevue: number | null;
  quantiteRealise: number | null;
  prixUnitaire: number | null;
  remisePrevue: number | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_produits {
  __typename: "PlanMarketingProduitInfo";
  id: string;
  produit: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_produits_produit;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_modeReglement {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  remise: number | null;
  montant: number | null;
  typeRemuneration: string;
  remuneration: string | null;
  type: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_type;
  statut: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_statut;
  fichiers: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_fichiers[] | null;
  miseAvants: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_miseAvants[] | null;
  actions: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_actions[] | null;
  produits: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_produits[] | null;
  modeReglement: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings_modeReglement | null;
  dateStatutCloture: any | null;
  montantStatutCloture: number | null;
  montantStatutRealise: number | null;
  dateStatutRealise: any | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_conditionsCommerciales_type {
  __typename: "PRTConditionCommercialeType";
  id: string;
  libelle: string;
  code: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_conditionsCommerciales_statut {
  __typename: "PRTConditionCommercialeStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_conditionsCommerciales_canal {
  __typename: "Canal";
  id: string;
  libelle: string;
  code: string;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_conditionsCommerciales_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_conditionsCommerciales {
  __typename: "PRTConditionCommerciale";
  id: string;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  idCanal: string | null;
  idType: string;
  idStatut: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  idGroupement: string;
  idPharmacie: string;
  description: string | null;
  type: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_conditionsCommerciales_type;
  statut: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_conditionsCommerciales_statut;
  canal: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_conditionsCommerciales_canal | null;
  fichiers: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_conditionsCommerciales_fichiers[] | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_notificationLogs {
  __typename: "NotificationLogs";
  id: string;
  titre: string;
  message: string;
  type: string;
  expediteur: string | null;
  createdAt: any;
  updatedAt: any;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus_nodes {
  __typename: "PRTCompteRendu";
  id: string;
  titre: string;
  remiseEchantillon: boolean;
  gestionPerime: string | null;
  rapportVisite: string | null;
  conclusion: string | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  createdAt: any;
  idNotificationLogs: string | null;
  idPharmacie: string;
  collaborateurs: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_collaborateurs[] | null;
  responsables: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_responsables[] | null;
  suiviOperationnels: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_suiviOperationnels[] | null;
  planMarketings: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_planMarketings[] | null;
  conditionsCommerciales: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_conditionsCommerciales[] | null;
  notificationLogs: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes_notificationLogs | null;
}

export interface PRT_COMPTE_RENDUS_pRTCompteRendus {
  __typename: "PRTCompteRenduConnection";
  /**
   * Array of nodes.
   */
  nodes: PRT_COMPTE_RENDUS_pRTCompteRendus_nodes[];
}

export interface PRT_COMPTE_RENDUS {
  pRTCompteRendus: PRT_COMPTE_RENDUS_pRTCompteRendus;
}

export interface PRT_COMPTE_RENDUSVariables {
  idPharmacie?: string | null;
  paging?: OffsetPaging | null;
  filter?: PRTCompteRenduFilter | null;
  sorting?: PRTCompteRenduSort[] | null;
}
