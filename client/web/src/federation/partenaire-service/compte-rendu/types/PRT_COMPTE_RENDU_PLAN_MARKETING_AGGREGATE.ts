/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTCompteRenduPlanMarketingAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: PRT_COMPTE_RENDU_PLAN_MARKETING_AGGREGATE
// ====================================================

export interface PRT_COMPTE_RENDU_PLAN_MARKETING_AGGREGATE_pRTCompteRenduPlanMarketingAggregate_count {
  __typename: "PRTCompteRenduPlanMarketingCountAggregate";
  id: number | null;
}

export interface PRT_COMPTE_RENDU_PLAN_MARKETING_AGGREGATE_pRTCompteRenduPlanMarketingAggregate {
  __typename: "PRTCompteRenduPlanMarketingAggregateResponse";
  count: PRT_COMPTE_RENDU_PLAN_MARKETING_AGGREGATE_pRTCompteRenduPlanMarketingAggregate_count | null;
}

export interface PRT_COMPTE_RENDU_PLAN_MARKETING_AGGREGATE {
  pRTCompteRenduPlanMarketingAggregate: PRT_COMPTE_RENDU_PLAN_MARKETING_AGGREGATE_pRTCompteRenduPlanMarketingAggregate[];
}

export interface PRT_COMPTE_RENDU_PLAN_MARKETING_AGGREGATEVariables {
  filter?: PRTCompteRenduPlanMarketingAggregateFilter | null;
}
