import gql from 'graphql-tag';
import { PRT_CONTACT_FULL_INFO } from '../contact/fragment';
import { FULL_USER_INFO } from './../../iam/user/fragment';
import { FULL_SUIVI_OPERATIONNEL_INFO } from '../suivi-operationnel/fragment';
import { FULL_PLAN_MARKETING_INFO } from '../planMarketing/fragment';
import { CONDITION_INFO } from '../laboratoire/condition/fragment';
import { NOTIFICATION_LOGS_INFO } from '../../notification/notification-email/notification-logs/fragment';

export const FULL_COMPTE_RENDU_INFO = gql`
  fragment PRTCompteRenduInfo on PRTCompteRendu {
    id
    titre
    remiseEchantillon
    gestionPerime
    rapportVisite
    conclusion
    idPartenaireTypeAssocie
    partenaireType
    createdAt
    idNotificationLogs
    idPharmacie
    collaborateurs {
      ...UserInfo
    }
    responsables {
      ...PRTContactFullInfo
    }
    suiviOperationnels {
      ...PRTSuiviOperationnelInfo
    }
    planMarketings {
      ...PRTPlanMarketingInfo
    }
    conditionsCommerciales {
      ...PRTConditionInfo
    }
    notificationLogs {
      ...NotificationLogsInfo
    }
  }
  ${CONDITION_INFO}
  ${FULL_PLAN_MARKETING_INFO}
  ${PRT_CONTACT_FULL_INFO}
  ${FULL_USER_INFO}
  ${FULL_SUIVI_OPERATIONNEL_INFO}
  ${NOTIFICATION_LOGS_INFO}
`;
