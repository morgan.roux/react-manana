/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPartenariatInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PARTENARIAT
// ====================================================

export interface CREATE_PARTENARIAT_createOnePRTPartenariat_type {
  __typename: "PRTPartenariatType";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_PARTENARIAT_createOnePRTPartenariat_statut {
  __typename: "PRTPartenariatStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_PARTENARIAT_createOnePRTPartenariat_responsables_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_PARTENARIAT_createOnePRTPartenariat_responsables_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface CREATE_PARTENARIAT_createOnePRTPartenariat_responsables {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: CREATE_PARTENARIAT_createOnePRTPartenariat_responsables_role | null;
  photo: CREATE_PARTENARIAT_createOnePRTPartenariat_responsables_photo | null;
  phoneNumber: string | null;
}

export interface CREATE_PARTENARIAT_createOnePRTPartenariat {
  __typename: "PRTPartenariat";
  id: string;
  dateDebut: any;
  dateFin: any | null;
  isEnvoiEmail: boolean;
  typeReunion: string;
  type: CREATE_PARTENARIAT_createOnePRTPartenariat_type;
  statut: CREATE_PARTENARIAT_createOnePRTPartenariat_statut;
  responsables: CREATE_PARTENARIAT_createOnePRTPartenariat_responsables[];
}

export interface CREATE_PARTENARIAT {
  createOnePRTPartenariat: CREATE_PARTENARIAT_createOnePRTPartenariat;
}

export interface CREATE_PARTENARIATVariables {
  input: PRTPartenariatInput;
}
