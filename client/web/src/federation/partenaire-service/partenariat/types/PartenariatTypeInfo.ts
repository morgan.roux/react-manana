/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PartenariatTypeInfo
// ====================================================

export interface PartenariatTypeInfo {
  __typename: "PRTPartenariatType";
  id: string;
  code: string;
  libelle: string;
}
