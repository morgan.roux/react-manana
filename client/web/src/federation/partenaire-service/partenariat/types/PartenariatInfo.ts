/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PartenariatInfo
// ====================================================

export interface PartenariatInfo_type {
  __typename: "PRTPartenariatType";
  id: string;
  code: string;
  libelle: string;
}

export interface PartenariatInfo_statut {
  __typename: "PRTPartenariatStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface PartenariatInfo_responsables_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PartenariatInfo_responsables_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface PartenariatInfo_responsables {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: PartenariatInfo_responsables_role | null;
  photo: PartenariatInfo_responsables_photo | null;
  phoneNumber: string | null;
}

export interface PartenariatInfo {
  __typename: "PRTPartenariat";
  id: string;
  dateDebut: any;
  dateFin: any | null;
  isEnvoiEmail: boolean;
  typeReunion: string;
  type: PartenariatInfo_type;
  statut: PartenariatInfo_statut;
  responsables: PartenariatInfo_responsables[];
}
