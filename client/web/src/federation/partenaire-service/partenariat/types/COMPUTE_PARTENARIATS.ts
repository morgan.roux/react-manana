/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: COMPUTE_PARTENARIATS
// ====================================================

export interface COMPUTE_PARTENARIATS_computePartenariats_currentYear {
  __typename: "ComputePartenariatOutputByYear";
  pharmacie: number;
  groupement: number;
}

export interface COMPUTE_PARTENARIATS_computePartenariats_lastYear {
  __typename: "ComputePartenariatOutputByYear";
  pharmacie: number;
  groupement: number;
}

export interface COMPUTE_PARTENARIATS_computePartenariats {
  __typename: "ComputePartenariatOutput";
  currentYear: COMPUTE_PARTENARIATS_computePartenariats_currentYear;
  lastYear: COMPUTE_PARTENARIATS_computePartenariats_lastYear;
}

export interface COMPUTE_PARTENARIATS {
  computePartenariats: COMPUTE_PARTENARIATS_computePartenariats;
}
