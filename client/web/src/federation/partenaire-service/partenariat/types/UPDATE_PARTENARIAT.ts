/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPartenariatInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_PARTENARIAT
// ====================================================

export interface UPDATE_PARTENARIAT_updateOnePRTPartenariat_type {
  __typename: "PRTPartenariatType";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_PARTENARIAT_updateOnePRTPartenariat_statut {
  __typename: "PRTPartenariatStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_PARTENARIAT_updateOnePRTPartenariat_responsables_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UPDATE_PARTENARIAT_updateOnePRTPartenariat_responsables_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface UPDATE_PARTENARIAT_updateOnePRTPartenariat_responsables {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: UPDATE_PARTENARIAT_updateOnePRTPartenariat_responsables_role | null;
  photo: UPDATE_PARTENARIAT_updateOnePRTPartenariat_responsables_photo | null;
  phoneNumber: string | null;
}

export interface UPDATE_PARTENARIAT_updateOnePRTPartenariat {
  __typename: "PRTPartenariat";
  id: string;
  dateDebut: any;
  dateFin: any | null;
  isEnvoiEmail: boolean;
  typeReunion: string;
  type: UPDATE_PARTENARIAT_updateOnePRTPartenariat_type;
  statut: UPDATE_PARTENARIAT_updateOnePRTPartenariat_statut;
  responsables: UPDATE_PARTENARIAT_updateOnePRTPartenariat_responsables[];
}

export interface UPDATE_PARTENARIAT {
  updateOnePRTPartenariat: UPDATE_PARTENARIAT_updateOnePRTPartenariat;
}

export interface UPDATE_PARTENARIATVariables {
  update: PRTPartenariatInput;
  id: string;
}
