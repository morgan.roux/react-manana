/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTPartenariatStatutFilter, PRTPartenariatStatutSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_LIST_PARTENARIAT_STATUTS
// ====================================================

export interface GET_LIST_PARTENARIAT_STATUTS_pRTPartenariatStatuts_nodes {
  __typename: "PRTPartenariatStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_LIST_PARTENARIAT_STATUTS_pRTPartenariatStatuts {
  __typename: "PRTPartenariatStatutConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_LIST_PARTENARIAT_STATUTS_pRTPartenariatStatuts_nodes[];
}

export interface GET_LIST_PARTENARIAT_STATUTS {
  pRTPartenariatStatuts: GET_LIST_PARTENARIAT_STATUTS_pRTPartenariatStatuts;
}

export interface GET_LIST_PARTENARIAT_STATUTSVariables {
  paging?: OffsetPaging | null;
  filter?: PRTPartenariatStatutFilter | null;
  sorting?: PRTPartenariatStatutSort[] | null;
}
