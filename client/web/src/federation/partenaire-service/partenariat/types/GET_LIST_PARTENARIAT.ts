/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTPartenariatFilter, PRTPartenariatSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_LIST_PARTENARIAT
// ====================================================

export interface GET_LIST_PARTENARIAT_pRTPartenariats_nodes_type {
  __typename: "PRTPartenariatType";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_LIST_PARTENARIAT_pRTPartenariats_nodes_statut {
  __typename: "PRTPartenariatStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_LIST_PARTENARIAT_pRTPartenariats_nodes_responsables_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_LIST_PARTENARIAT_pRTPartenariats_nodes_responsables_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_LIST_PARTENARIAT_pRTPartenariats_nodes_responsables {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: GET_LIST_PARTENARIAT_pRTPartenariats_nodes_responsables_role | null;
  photo: GET_LIST_PARTENARIAT_pRTPartenariats_nodes_responsables_photo | null;
  phoneNumber: string | null;
}

export interface GET_LIST_PARTENARIAT_pRTPartenariats_nodes {
  __typename: "PRTPartenariat";
  id: string;
  dateDebut: any;
  dateFin: any | null;
  isEnvoiEmail: boolean;
  typeReunion: string;
  type: GET_LIST_PARTENARIAT_pRTPartenariats_nodes_type;
  statut: GET_LIST_PARTENARIAT_pRTPartenariats_nodes_statut;
  responsables: GET_LIST_PARTENARIAT_pRTPartenariats_nodes_responsables[];
}

export interface GET_LIST_PARTENARIAT_pRTPartenariats {
  __typename: "PRTPartenariatConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_LIST_PARTENARIAT_pRTPartenariats_nodes[];
}

export interface GET_LIST_PARTENARIAT {
  pRTPartenariats: GET_LIST_PARTENARIAT_pRTPartenariats;
}

export interface GET_LIST_PARTENARIATVariables {
  paging?: OffsetPaging | null;
  filter?: PRTPartenariatFilter | null;
  sorting?: PRTPartenariatSort[] | null;
}
