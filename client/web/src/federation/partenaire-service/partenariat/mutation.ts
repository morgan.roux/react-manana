import gql from 'graphql-tag';
import { FULL_PARTENARIAT_INFO } from './fragment';

export const CREATE_PARTENARIAT = gql`
  mutation CREATE_PARTENARIAT($input: PRTPartenariatInput!) {
    createOnePRTPartenariat(input: $input) {
      ...PartenariatInfo
    }
  }
  ${FULL_PARTENARIAT_INFO}
`;

export const UPDATE_PARTENARIAT = gql`
  mutation UPDATE_PARTENARIAT($update: PRTPartenariatInput!,$id:String!) {
    updateOnePRTPartenariat(update: $update,id:$id) {
      ...PartenariatInfo
    }
  }
  ${FULL_PARTENARIAT_INFO}
`;
