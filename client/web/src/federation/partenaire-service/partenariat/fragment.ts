import gql from 'graphql-tag';
import { MINIMAL_USER_INFO } from '../../iam/user/fragment';

export const FULL_PARTENARIAT_STATUT_INFO = gql`
  fragment PartenariatStatutInfo on PRTPartenariatStatut {
    id
    code
    libelle
  }
`;

export const FULL_PARTENARIAT_TYPE_INFO = gql`
  fragment PartenariatTypeInfo on PRTPartenariatType {
    id
    code
    libelle
  }
`;

export const FULL_PARTENARIAT_INFO = gql`
  fragment PartenariatInfo on PRTPartenariat {
    id
    dateDebut
    dateFin
    isEnvoiEmail
    typeReunion
    type {
      ...PartenariatTypeInfo
    }
    statut {
      ...PartenariatStatutInfo
    }
    responsables {
      ...UserInfo
    }
  }
  ${FULL_PARTENARIAT_STATUT_INFO}
  ${FULL_PARTENARIAT_TYPE_INFO}
  ${MINIMAL_USER_INFO}
`;
