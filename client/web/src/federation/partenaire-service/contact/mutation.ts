import gql from 'graphql-tag';
import { PRT_CONTACT_FULL_INFO } from './fragment';

export const CREATE_PRT_CONTACT = gql `
    mutation CREATE_PRT_CONTACT($input: PRTContactInput!) {
        createOnePRTContact(input: $input) {
            ...PRTContactFullInfo
        }
    }
    ${PRT_CONTACT_FULL_INFO}
`;

export const UPDATE_PRT_CONTACT = gql`
    mutation UPDATE_PRT_CONTACT($id: String!, $input: PRTContactInput!) {
        updateOnePRTContact(id: $id, input: $input) {
            ...PRTContactFullInfo
        }
    }
    ${PRT_CONTACT_FULL_INFO}
`;

export const DELETE_PRT_CONTACT = gql`
    mutation DELETE_PRT_CONTACT($id: String!) {
        deleteOnePRTContact(id: $id) {
            id
        }
    }
`;