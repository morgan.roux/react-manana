/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTContactInfo
// ====================================================

export interface PRTContactInfo_fonction {
  __typename: "Fonction";
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}

export interface PRTContactInfo_departements {
  __typename: "Departement";
  id: string;
  code: string;
  nom: string;
}

export interface PRTContactInfo_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface PRTContactInfo_contact {
  __typename: "ContactType";
  id: string;
  mailProf: string | null;
  mailPerso: string | null;
}

export interface PRTContactInfo {
  __typename: "PRTContact";
  id: string;
  dataType: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  idFonction: string | null;
  fonction: PRTContactInfo_fonction | null;
  departements: PRTContactInfo_departements[] | null;
  photo: PRTContactInfo_photo | null;
  contact: PRTContactInfo_contact | null;
}
