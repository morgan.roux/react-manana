/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTContactInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PRT_CONTACT
// ====================================================

export interface CREATE_PRT_CONTACT_createOnePRTContact_fonction {
  __typename: "Fonction";
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}

export interface CREATE_PRT_CONTACT_createOnePRTContact_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface CREATE_PRT_CONTACT_createOnePRTContact_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface CREATE_PRT_CONTACT_createOnePRTContact_departements {
  __typename: "Departement";
  id: string;
  code: string;
  nom: string;
}

export interface CREATE_PRT_CONTACT_createOnePRTContact {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  idFonction: string | null;
  fonction: CREATE_PRT_CONTACT_createOnePRTContact_fonction | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: CREATE_PRT_CONTACT_createOnePRTContact_photo | null;
  contact: CREATE_PRT_CONTACT_createOnePRTContact_contact | null;
  departements: CREATE_PRT_CONTACT_createOnePRTContact_departements[] | null;
}

export interface CREATE_PRT_CONTACT {
  createOnePRTContact: CREATE_PRT_CONTACT_createOnePRTContact;
}

export interface CREATE_PRT_CONTACTVariables {
  input: PRTContactInput;
}
