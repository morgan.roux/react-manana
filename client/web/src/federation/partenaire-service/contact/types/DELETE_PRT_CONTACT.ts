/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_PRT_CONTACT
// ====================================================

export interface DELETE_PRT_CONTACT_deleteOnePRTContact {
  __typename: "PRTContact";
  id: string;
}

export interface DELETE_PRT_CONTACT {
  deleteOnePRTContact: DELETE_PRT_CONTACT_deleteOnePRTContact;
}

export interface DELETE_PRT_CONTACTVariables {
  id: string;
}
