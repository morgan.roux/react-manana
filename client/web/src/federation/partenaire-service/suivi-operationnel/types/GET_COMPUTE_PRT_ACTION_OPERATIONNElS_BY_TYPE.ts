/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ComputeActionOperationnelsInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_COMPUTE_PRT_ACTION_OPERATIONNElS_BY_TYPE
// ====================================================

export interface GET_COMPUTE_PRT_ACTION_OPERATIONNElS_BY_TYPE_computeActionOperationnelsByType_type {
  __typename: "PRTSuiviOperationnelType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface GET_COMPUTE_PRT_ACTION_OPERATIONNElS_BY_TYPE_computeActionOperationnelsByType {
  __typename: "ComputeTypeActionOperationnelsByType";
  type: GET_COMPUTE_PRT_ACTION_OPERATIONNElS_BY_TYPE_computeActionOperationnelsByType_type;
  count: number;
}

export interface GET_COMPUTE_PRT_ACTION_OPERATIONNElS_BY_TYPE {
  computeActionOperationnelsByType: GET_COMPUTE_PRT_ACTION_OPERATIONNElS_BY_TYPE_computeActionOperationnelsByType[];
}

export interface GET_COMPUTE_PRT_ACTION_OPERATIONNElS_BY_TYPEVariables {
  input: ComputeActionOperationnelsInput;
}
