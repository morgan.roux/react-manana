/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTSuiviOperationnelInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_SUIVI_OPERATIONNEL
// ====================================================

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_contacts_fonction {
  __typename: "Fonction";
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_contacts_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_contacts_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_contacts_departements {
  __typename: "Departement";
  id: string;
  code: string;
  nom: string;
}

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_contacts {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  idFonction: string | null;
  fonction: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_contacts_fonction | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_contacts_photo | null;
  contact: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_contacts_contact | null;
  departements: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_contacts_departements[] | null;
}

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_participants_role | null;
  photo: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_participants_photo | null;
  phoneNumber: string | null;
}

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_type {
  __typename: "PRTSuiviOperationnelType";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_statut {
  __typename: "PRTSuiviOperationnelStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel {
  __typename: "PRTSuiviOperationnel";
  id: string;
  titre: string;
  description: string | null;
  dateHeure: any;
  createdBy: string;
  partenaireType: string;
  idTypeAssocie: string;
  idGroupement: string;
  montant: number;
  idImportance: string | null;
  idTache: string | null;
  idFonction: string | null;
  contacts: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_contacts[] | null;
  importance: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_importance | null;
  idPharmacie: string;
  participants: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_participants[];
  type: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_type;
  statut: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_statut;
  fichiers: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel_fichiers[] | null;
  nombreCommentaires: number | null;
  nombreReaction: number | null;
}

export interface CREATE_SUIVI_OPERATIONNEL {
  createOnePRTSuiviOperationnel: CREATE_SUIVI_OPERATIONNEL_createOnePRTSuiviOperationnel;
}

export interface CREATE_SUIVI_OPERATIONNELVariables {
  input: PRTSuiviOperationnelInput;
}
