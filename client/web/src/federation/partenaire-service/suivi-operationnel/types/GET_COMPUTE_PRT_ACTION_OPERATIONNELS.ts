/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ComputeActionOperationnelsInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_COMPUTE_PRT_ACTION_OPERATIONNELS
// ====================================================

export interface GET_COMPUTE_PRT_ACTION_OPERATIONNELS_computeActionOperationnels_currentYear {
  __typename: "ComputeActionOperationnelByYear";
  enCours: number;
  cloture: number;
}

export interface GET_COMPUTE_PRT_ACTION_OPERATIONNELS_computeActionOperationnels_lastYear {
  __typename: "ComputeActionOperationnelByYear";
  enCours: number;
  cloture: number;
}

export interface GET_COMPUTE_PRT_ACTION_OPERATIONNELS_computeActionOperationnels {
  __typename: "ComputeTypePRTActionOperationnels";
  total: number;
  currentYear: GET_COMPUTE_PRT_ACTION_OPERATIONNELS_computeActionOperationnels_currentYear;
  lastYear: GET_COMPUTE_PRT_ACTION_OPERATIONNELS_computeActionOperationnels_lastYear;
}

export interface GET_COMPUTE_PRT_ACTION_OPERATIONNELS {
  computeActionOperationnels: GET_COMPUTE_PRT_ACTION_OPERATIONNELS_computeActionOperationnels;
}

export interface GET_COMPUTE_PRT_ACTION_OPERATIONNELSVariables {
  input: ComputeActionOperationnelsInput;
}
