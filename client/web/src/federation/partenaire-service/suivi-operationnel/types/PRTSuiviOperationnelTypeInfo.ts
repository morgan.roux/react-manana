/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTSuiviOperationnelTypeInfo
// ====================================================

export interface PRTSuiviOperationnelTypeInfo {
  __typename: "PRTSuiviOperationnelType";
  id: string;
  code: string;
  libelle: string;
}
