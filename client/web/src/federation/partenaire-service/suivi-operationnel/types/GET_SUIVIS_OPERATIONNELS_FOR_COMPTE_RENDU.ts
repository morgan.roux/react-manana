/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU
// ====================================================

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_contacts_fonction {
  __typename: "Fonction";
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_contacts_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_contacts_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_contacts_departements {
  __typename: "Departement";
  id: string;
  code: string;
  nom: string;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_contacts {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  idFonction: string | null;
  fonction: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_contacts_fonction | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_contacts_photo | null;
  contact: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_contacts_contact | null;
  departements: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_contacts_departements[] | null;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_participants_role | null;
  photo: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_participants_photo | null;
  phoneNumber: string | null;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_type {
  __typename: "PRTSuiviOperationnelType";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_statut {
  __typename: "PRTSuiviOperationnelStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu {
  __typename: "PRTSuiviOperationnel";
  id: string;
  titre: string;
  description: string | null;
  dateHeure: any;
  createdBy: string;
  partenaireType: string;
  idTypeAssocie: string;
  idGroupement: string;
  montant: number;
  idImportance: string | null;
  idTache: string | null;
  idFonction: string | null;
  contacts: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_contacts[] | null;
  importance: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_importance | null;
  idPharmacie: string;
  participants: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_participants[];
  type: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_type;
  statut: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_statut;
  fichiers: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu_fichiers[] | null;
  nombreCommentaires: number | null;
  nombreReaction: number | null;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU {
  getSuiviOperationnelForCompteRendu: GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDU_getSuiviOperationnelForCompteRendu[] | null;
}

export interface GET_SUIVIS_OPERATIONNELS_FOR_COMPTE_RENDUVariables {
  idCompteRendu?: string | null;
  searchText?: string | null;
  take?: number | null;
  skip?: number | null;
  idTypeAssocie: string;
}
