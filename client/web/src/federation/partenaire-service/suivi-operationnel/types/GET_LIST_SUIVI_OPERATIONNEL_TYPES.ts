/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTSuiviOperationnelTypeFilter, PRTSuiviOperationnelTypeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_LIST_SUIVI_OPERATIONNEL_TYPES
// ====================================================

export interface GET_LIST_SUIVI_OPERATIONNEL_TYPES_pRTSuiviOperationnelTypes_nodes {
  __typename: "PRTSuiviOperationnelType";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_LIST_SUIVI_OPERATIONNEL_TYPES_pRTSuiviOperationnelTypes {
  __typename: "PRTSuiviOperationnelTypeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_LIST_SUIVI_OPERATIONNEL_TYPES_pRTSuiviOperationnelTypes_nodes[];
}

export interface GET_LIST_SUIVI_OPERATIONNEL_TYPES {
  pRTSuiviOperationnelTypes: GET_LIST_SUIVI_OPERATIONNEL_TYPES_pRTSuiviOperationnelTypes;
}

export interface GET_LIST_SUIVI_OPERATIONNEL_TYPESVariables {
  paging?: OffsetPaging | null;
  filter?: PRTSuiviOperationnelTypeFilter | null;
  sorting?: PRTSuiviOperationnelTypeSort[] | null;
}
