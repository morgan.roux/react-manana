/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CHANGE_SUIVI_OPERTIONNEL_STATUT
// ====================================================

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_contacts_fonction {
  __typename: "Fonction";
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_contacts_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_contacts_contact {
  __typename: "ContactType";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsappMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedInProf: string | null;
  urlLinkedInPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  idUserCreation: string | null;
  idUserModification: string | null;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_contacts_departements {
  __typename: "Departement";
  id: string;
  code: string;
  nom: string;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_contacts {
  __typename: "PRTContact";
  id: string;
  civilite: string;
  nom: string;
  prenom: string | null;
  idFonction: string | null;
  fonction: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_contacts_fonction | null;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  photo: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_contacts_photo | null;
  contact: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_contacts_contact | null;
  departements: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_contacts_departements[] | null;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_participants_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_participants {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_participants_role | null;
  photo: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_participants_photo | null;
  phoneNumber: string | null;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_type {
  __typename: "PRTSuiviOperationnelType";
  id: string;
  libelle: string;
  code: string;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_statut {
  __typename: "PRTSuiviOperationnelStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut {
  __typename: "PRTSuiviOperationnel";
  id: string;
  titre: string;
  description: string | null;
  dateHeure: any;
  createdBy: string;
  partenaireType: string;
  idTypeAssocie: string;
  idGroupement: string;
  montant: number;
  idImportance: string | null;
  idTache: string | null;
  idFonction: string | null;
  contacts: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_contacts[] | null;
  importance: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_importance | null;
  idPharmacie: string;
  participants: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_participants[];
  type: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_type;
  statut: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_statut;
  fichiers: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut_fichiers[] | null;
  nombreCommentaires: number | null;
  nombreReaction: number | null;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUT {
  changePRTSuiviOperationnelStatut: CHANGE_SUIVI_OPERTIONNEL_STATUT_changePRTSuiviOperationnelStatut;
}

export interface CHANGE_SUIVI_OPERTIONNEL_STATUTVariables {
  id: string;
  idStatut: string;
}
