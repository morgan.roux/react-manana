/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTSuiviOperationnelAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ROW_SUIVI_OPERATIONNEL
// ====================================================

export interface GET_ROW_SUIVI_OPERATIONNEL_pRTSuiviOperationnelAggregate_count {
  __typename: "PRTSuiviOperationnelCountAggregate";
  id: number | null;
}

export interface GET_ROW_SUIVI_OPERATIONNEL_pRTSuiviOperationnelAggregate {
  __typename: "PRTSuiviOperationnelAggregateResponse";
  count: GET_ROW_SUIVI_OPERATIONNEL_pRTSuiviOperationnelAggregate_count | null;
}

export interface GET_ROW_SUIVI_OPERATIONNEL {
  pRTSuiviOperationnelAggregate: GET_ROW_SUIVI_OPERATIONNEL_pRTSuiviOperationnelAggregate[];
}

export interface GET_ROW_SUIVI_OPERATIONNELVariables {
  filter?: PRTSuiviOperationnelAggregateFilter | null;
}
