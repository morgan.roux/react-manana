import gql from 'graphql-tag';
import { FULL_SUIVI_OPERATIONNEL_INFO } from './fragment';

export const CREATE_SUIVI_OPERATIONNEL = gql`
  mutation CREATE_SUIVI_OPERATIONNEL($input: PRTSuiviOperationnelInput!) {
    createOnePRTSuiviOperationnel(input: $input) {
      ...PRTSuiviOperationnelInfo
    }
  }
  ${FULL_SUIVI_OPERATIONNEL_INFO}
`;

export const UPDATE_SUIVI_OPERATIONNEL = gql`
  mutation UPDATE_SUIVI_OPERATIONNEL($id: String!, $input: PRTSuiviOperationnelInput!) {
    updateOnePRTSuiviOperationnel(id: $id, input: $input) {
      ...PRTSuiviOperationnelInfo
    }
  }
  ${FULL_SUIVI_OPERATIONNEL_INFO}
`;

export const DELETE_ONE_SUIVI_OPERATIONNEL = gql`
  mutation DELETE_ONE_SUIVI_OPERATIONNEL($input: DeleteOnePRTSuiviOperationnelInput!) {
    deleteOnePRTSuiviOperationnel(input: $input) {
      id
    }
  }
`;

export const CHANGE_SUIVI_OPERTIONNEL_STATUT = gql`
  mutation CHANGE_SUIVI_OPERTIONNEL_STATUT($id: String!, $idStatut: String!) {
    changePRTSuiviOperationnelStatut(id: $id, idStatut: $idStatut) {
      ...PRTSuiviOperationnelInfo
    }
  }
  ${FULL_SUIVI_OPERATIONNEL_INFO}
`;

export const UPDATE_PARTICIPANTS_SUIVI_OPERATIONNEL = gql`
  mutation UPDATE_PARTICIPANTS_SUIVI_OPERATIONNEL($id: String!, $idParticipants: [String!]!) {
    updateParticipantsSuiviOperationnel(id: $id, idParticipants: $idParticipants) {
      ...PRTSuiviOperationnelInfo
    }
  }
  ${FULL_SUIVI_OPERATIONNEL_INFO}
`;
