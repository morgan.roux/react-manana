/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteManyRemisesInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_MANY_REMISE
// ====================================================

export interface DELETE_MANY_REMISE_deleteManyRemises {
  __typename: "DeleteManyResponse";
  /**
   * The number of records deleted.
   */
  deletedCount: number;
}

export interface DELETE_MANY_REMISE {
  deleteManyRemises: DELETE_MANY_REMISE_deleteManyRemises;
}

export interface DELETE_MANY_REMISEVariables {
  input: DeleteManyRemisesInput;
}
