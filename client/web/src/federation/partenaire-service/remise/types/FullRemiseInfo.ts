/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FullRemiseInfo
// ====================================================

export interface FullRemiseInfo {
  __typename: "Remise";
  id: string;
  nom: string;
  model: string;
  type: string | null;
  utilisateur: string | null;
  source: string;
  active: boolean | null;
  dateDebut: any;
  dateFin: any;
}
