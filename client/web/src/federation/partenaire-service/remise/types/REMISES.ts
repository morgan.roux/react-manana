/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, RemiseFilter, RemiseSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: REMISES
// ====================================================

export interface REMISES_remises_nodes {
  __typename: "Remise";
  id: string;
  nom: string;
  model: string;
  type: string | null;
  utilisateur: string | null;
  source: string;
  active: boolean | null;
  dateDebut: any;
  dateFin: any;
}

export interface REMISES_remises {
  __typename: "RemiseConnection";
  /**
   * Array of nodes.
   */
  nodes: REMISES_remises_nodes[];
  /**
   * Fetch total count of records
   */
  totalCount: number;
}

export interface REMISES {
  remises: REMISES_remises;
}

export interface REMISESVariables {
  paging?: OffsetPaging | null;
  filter?: RemiseFilter | null;
  sorting?: RemiseSort[] | null;
}
