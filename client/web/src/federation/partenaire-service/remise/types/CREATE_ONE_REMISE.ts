/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RemiseInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_ONE_REMISE
// ====================================================

export interface CREATE_ONE_REMISE_createOneRemise {
  __typename: "Remise";
  id: string;
  nom: string;
  model: string;
  type: string | null;
  source: string;
  active: boolean | null;
  dateDebut: any;
  dateFin: any;
}

export interface CREATE_ONE_REMISE {
  createOneRemise: CREATE_ONE_REMISE_createOneRemise;
}

export interface CREATE_ONE_REMISEVariables {
  input: RemiseInput;
}
