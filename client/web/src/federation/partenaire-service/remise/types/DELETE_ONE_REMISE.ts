/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneRemiseInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_ONE_REMISE
// ====================================================

export interface DELETE_ONE_REMISE_deleteOneRemise {
  __typename: "RemiseDeleteResponse";
  id: string | null;
}

export interface DELETE_ONE_REMISE {
  deleteOneRemise: DELETE_ONE_REMISE_deleteOneRemise;
}

export interface DELETE_ONE_REMISEVariables {
  input: DeleteOneRemiseInput;
}
