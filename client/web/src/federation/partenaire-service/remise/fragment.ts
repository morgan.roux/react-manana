import gql from 'graphql-tag';

export const MINIMAL_REMISE_INFO = gql`
  fragment RemiseInfo on Remise {
    id
    nom
    model
    type
    source
    active
    dateDebut
    dateFin
  }
`;

export const FULL_REMISE_INFO = gql`
  fragment FullRemiseInfo on Remise {
    id
    nom
    model
    type
    utilisateur
    source
    active
    dateDebut
    dateFin
  }
`;
