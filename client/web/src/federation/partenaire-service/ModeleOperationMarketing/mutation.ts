import gql from 'graphql-tag';
import { FULL_MODELE_OPERATION_MARKETING_INFO } from './fragment';

export const CREATE_MODELE_OPERATION_MARKETING = gql`
  mutation CREATE_MODELE_OPERATION_MARKETING(
    $input: PRTPlanMarketingTypeActionInput!
    $idPharmacie: String
  ) {
    createOnePRTPlanMarketingTypeAction(input: $input) {
      ...ModeleOperationMarketingInfo
    }
  }
  ${FULL_MODELE_OPERATION_MARKETING_INFO}
`;

export const UPDATE_MODELE_OPERATION_MARKETING = gql`
  mutation UPDATE_MODELE_OPERATION_MARKETING(
    $idPharmacie: String
    $input: PRTPlanMarketingTypeActionInput!
    $id: String!
  ) {
    updateOnePRTPlanMarketingTypeAction(input: $input, id: $id) {
      ...ModeleOperationMarketingInfo
    }
  }
  ${FULL_MODELE_OPERATION_MARKETING_INFO}
`;

export const DELETE_MODELE_OPERATION_MARKETING = gql`
  mutation DELETE_MODELE_OPERATION_MARKETING($id: String!) {
    deleteOnePRTPlanMarketingTypeAction(id: $id) {
      id
    }
  }
`;

export const DO_CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS = gql`
  mutation CHANGE_PLAN_MARKETIN_TYPE_ACTION_ACTIVATION_STATUS(
    $idPharmacie: String!
    $idPlanMarketingTypeAction: String!
    $active: Boolean!
  ) {
    changePRTPlanMarketingTypeActionActivationStatus(
      idPlanMarketingTypeAction: $idPlanMarketingTypeAction
      active: $active
      idPharmacie: $idPharmacie
    ) {
      ...ModeleOperationMarketingInfo
    }
  }
  ${FULL_MODELE_OPERATION_MARKETING_INFO}
`;

export const DO_PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION = gql`
  mutation PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_ACTION(
    $idPharmacie: String!
    $input: PRTPlanMarketingTypeActionInput!
    $idPlanMarketingTypeAction: String!
  ) {
    personnaliserPRTPlanMarketingTypeAction(
      input: $input
      idPlanMarketingTypeAction: $idPlanMarketingTypeAction
      idPharmacie: $idPharmacie
    ) {
      ...ModeleOperationMarketingInfo
    }
  }
  ${FULL_MODELE_OPERATION_MARKETING_INFO}
`;
