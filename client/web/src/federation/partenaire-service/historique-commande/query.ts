import gql from 'graphql-tag';
import { COMMANDE_LIGNE, HISTORIQUE_COMMANDE_INFO } from './fragment';

export const GET_HISTORIQUES_COMMADES = gql`
  query GET_HISTORIQUES_COMMADES(
    $paging: OffsetPaging
    $filter: PRTCommandeFilter
    $sorting: [PRTCommandeSort!]
  ) {
    pRTCommandes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...HistoriqueCommandeInfo
      }
    }
  }
  ${HISTORIQUE_COMMANDE_INFO}
`;

export const GET_COMMANDES_LIGNES = gql`
  query GET_COMMANDES_LIGNES($idCommande: String!) {
    getCommandeLignesByIdCommande(idCommande: $idCommande) {
      ...CommandeLigneInfo
    }
  }
  ${COMMANDE_LIGNE}
`;

export const GET_ROWS_TOTAL = gql`
  query GET_ROWS_TOTAL($filter: PRTCommandeAggregateFilter) {
    pRTCommandeAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_TOTAUX_COMMANDES = gql`
  query GET_TOTAUX_COMMANDES($idPharmacie: String!, $idLaboratoire: String!) {
    getTotauxCommandes(idPharmacie: $idPharmacie, idLaboratoire: $idLaboratoire) {
      totalHT
      totalTTC
      totalTVA
    }
  }
`;

export const GET_COMMANDES_FOR_DOCUMENT = gql`
  query COMMANDES_FOR_DOCUMENT(
    $idDocument: String!
    $searchText: String
    $take: Int
    $skip: Int
    $idTypeAssocie: String!
  ) {
    getCommandeForDocument(
      idDocument: $idDocument
      searchText: $searchText
      take: $take
      skip: $skip
      idTypeAssocie: $idTypeAssocie
    ) {
      ...HistoriqueCommandeInfo
    }
  }
  ${HISTORIQUE_COMMANDE_INFO}
`;

export const GET_COMMANDE_FOR_DOCUMENT_TOTAL_ROWS = gql`
  query COMMANDE_FOR_DOCUMENT_TOTAL_ROWS(
    $idDocument: String!
    $searchText: String
    $idTypeAssocie: String!
  ) {
    getCommandeForDocumentRowsTotal(
      idDocument: $idDocument
      searchText: $searchText
      idTypeAssocie: $idTypeAssocie
    )
  }
`;

export const GET_TOTAUX_COMMANDES_FOR_DOCUMENT = gql`
  query TOTAUX_COMMANDES_FOR_DOCUMENT(
    $idDocument: String
    $searchText: String
    $idTypeAssocie: String!
  ) {
    getCommandesForDocumentTotaux(
      idDocument: $idDocument
      searchText: $searchText
      idTypeAssocie: $idTypeAssocie
    ) {
      totalHT
      totalTVA
      totalTTC
    }
  }
`;

export const GET_PRT_COMMANDE_STATUTS = gql`
  query PRT_COMMANDE_STATUTS(
    $paging: OffsetPaging
    $filter: PRTCommandeStatutFilter
    $sorting: [PRTCommandeStatutSort!]
  ) {
    pRTCommandeStatuts(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        id
        code
        libelle
      }
    }
  }
`;
