/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CHANGE_PRT_COMMANDE_STATUT
// ====================================================

export interface CHANGE_PRT_COMMANDE_STATUT_changePRTCommandeStatut {
  __typename: "PRTCommande";
  id: string;
  dateCommande: any | null;
  nbrRef: number | null;
  quantite: number | null;
  uniteGratuite: number | null;
  prixBaseTotalHT: number | null;
  valeurRemiseTotal: number | null;
  prixNetTotalHT: number | null;
  fraisPort: number | null;
  ValeurFrancoPort: number | null;
  remiseGlobale: number | null;
  commentaireInterne: string | null;
  commentaireExterne: string | null;
  pathFile: string | null;
  idOperation: string | null;
  idUser: string | null;
  idCommandeType: string | null;
  idPharmacie: string | null;
  idPublicite: string | null;
  statut: string | null;
  idCanal: string | null;
  idGroupement: string | null;
  prixNetTotalTTC: number | null;
  totalTVA: number | null;
  source: string | null;
}

export interface CHANGE_PRT_COMMANDE_STATUT {
  changePRTCommandeStatut: CHANGE_PRT_COMMANDE_STATUT_changePRTCommandeStatut;
}

export interface CHANGE_PRT_COMMANDE_STATUTVariables {
  id: string;
  statut: string;
}
