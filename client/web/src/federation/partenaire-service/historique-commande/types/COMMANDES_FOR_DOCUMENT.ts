/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: COMMANDES_FOR_DOCUMENT
// ====================================================

export interface COMMANDES_FOR_DOCUMENT_getCommandeForDocument {
  __typename: "PRTCommande";
  id: string;
  dateCommande: any | null;
  nbrRef: number | null;
  quantite: number | null;
  uniteGratuite: number | null;
  prixBaseTotalHT: number | null;
  valeurRemiseTotal: number | null;
  prixNetTotalHT: number | null;
  fraisPort: number | null;
  ValeurFrancoPort: number | null;
  remiseGlobale: number | null;
  commentaireInterne: string | null;
  commentaireExterne: string | null;
  pathFile: string | null;
  idOperation: string | null;
  idUser: string | null;
  idCommandeType: string | null;
  idPharmacie: string | null;
  idPublicite: string | null;
  statut: string | null;
  idCanal: string | null;
  idGroupement: string | null;
  prixNetTotalTTC: number | null;
  totalTVA: number | null;
  source: string | null;
}

export interface COMMANDES_FOR_DOCUMENT {
  getCommandeForDocument: COMMANDES_FOR_DOCUMENT_getCommandeForDocument[] | null;
}

export interface COMMANDES_FOR_DOCUMENTVariables {
  idDocument: string;
  searchText?: string | null;
  take?: number | null;
  skip?: number | null;
  idTypeAssocie: string;
}
