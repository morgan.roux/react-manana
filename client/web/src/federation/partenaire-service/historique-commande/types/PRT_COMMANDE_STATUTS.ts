/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTCommandeStatutFilter, PRTCommandeStatutSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: PRT_COMMANDE_STATUTS
// ====================================================

export interface PRT_COMMANDE_STATUTS_pRTCommandeStatuts_nodes {
  __typename: "PRTCommandeStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface PRT_COMMANDE_STATUTS_pRTCommandeStatuts {
  __typename: "PRTCommandeStatutConnection";
  /**
   * Array of nodes.
   */
  nodes: PRT_COMMANDE_STATUTS_pRTCommandeStatuts_nodes[];
}

export interface PRT_COMMANDE_STATUTS {
  pRTCommandeStatuts: PRT_COMMANDE_STATUTS_pRTCommandeStatuts;
}

export interface PRT_COMMANDE_STATUTSVariables {
  paging?: OffsetPaging | null;
  filter?: PRTCommandeStatutFilter | null;
  sorting?: PRTCommandeStatutSort[] | null;
}
