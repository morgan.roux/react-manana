/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTCommandeDocumentInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: JOINDRE_COMMANDE_FACTURE
// ====================================================

export interface JOINDRE_COMMANDE_FACTURE {
  joindreCommandeFacture: string | null;
}

export interface JOINDRE_COMMANDE_FACTUREVariables {
  input: PRTCommandeDocumentInput;
}
