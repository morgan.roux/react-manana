/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateOnePRTCommandeInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_PRT_COMMANDE
// ====================================================

export interface UPDATE_PRT_COMMANDE_updateOnePRTCommande {
  __typename: "PRTCommande";
  id: string;
  dateCommande: any | null;
  nbrRef: number | null;
  quantite: number | null;
  uniteGratuite: number | null;
  prixBaseTotalHT: number | null;
  valeurRemiseTotal: number | null;
  prixNetTotalHT: number | null;
  fraisPort: number | null;
  ValeurFrancoPort: number | null;
  remiseGlobale: number | null;
  commentaireInterne: string | null;
  commentaireExterne: string | null;
  pathFile: string | null;
  idOperation: string | null;
  idUser: string | null;
  idCommandeType: string | null;
  idPharmacie: string | null;
  idPublicite: string | null;
  statut: string | null;
  idCanal: string | null;
  idGroupement: string | null;
  prixNetTotalTTC: number | null;
  totalTVA: number | null;
  source: string | null;
}

export interface UPDATE_PRT_COMMANDE {
  updateOnePRTCommande: UPDATE_PRT_COMMANDE_updateOnePRTCommande;
}

export interface UPDATE_PRT_COMMANDEVariables {
  input: UpdateOnePRTCommandeInput;
}
