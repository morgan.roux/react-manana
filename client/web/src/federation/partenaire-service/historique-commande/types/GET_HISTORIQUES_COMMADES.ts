/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTCommandeFilter, PRTCommandeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_HISTORIQUES_COMMADES
// ====================================================

export interface GET_HISTORIQUES_COMMADES_pRTCommandes_nodes {
  __typename: "PRTCommande";
  id: string;
  dateCommande: any | null;
  nbrRef: number | null;
  quantite: number | null;
  uniteGratuite: number | null;
  prixBaseTotalHT: number | null;
  valeurRemiseTotal: number | null;
  prixNetTotalHT: number | null;
  fraisPort: number | null;
  ValeurFrancoPort: number | null;
  remiseGlobale: number | null;
  commentaireInterne: string | null;
  commentaireExterne: string | null;
  pathFile: string | null;
  idOperation: string | null;
  idUser: string | null;
  idCommandeType: string | null;
  idPharmacie: string | null;
  idPublicite: string | null;
  statut: string | null;
  idCanal: string | null;
  idGroupement: string | null;
  prixNetTotalTTC: number | null;
  totalTVA: number | null;
  source: string | null;
}

export interface GET_HISTORIQUES_COMMADES_pRTCommandes {
  __typename: "PRTCommandeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_HISTORIQUES_COMMADES_pRTCommandes_nodes[];
}

export interface GET_HISTORIQUES_COMMADES {
  pRTCommandes: GET_HISTORIQUES_COMMADES_pRTCommandes;
}

export interface GET_HISTORIQUES_COMMADESVariables {
  paging?: OffsetPaging | null;
  filter?: PRTCommandeFilter | null;
  sorting?: PRTCommandeSort[] | null;
}
