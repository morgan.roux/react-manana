/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TOTAUX_COMMANDES_FOR_DOCUMENT
// ====================================================

export interface TOTAUX_COMMANDES_FOR_DOCUMENT_getCommandesForDocumentTotaux {
  __typename: "PRTTotauxCommande";
  totalHT: number | null;
  totalTVA: number | null;
  totalTTC: number | null;
}

export interface TOTAUX_COMMANDES_FOR_DOCUMENT {
  getCommandesForDocumentTotaux: TOTAUX_COMMANDES_FOR_DOCUMENT_getCommandesForDocumentTotaux | null;
}

export interface TOTAUX_COMMANDES_FOR_DOCUMENTVariables {
  idDocument?: string | null;
  searchText?: string | null;
  idTypeAssocie: string;
}
