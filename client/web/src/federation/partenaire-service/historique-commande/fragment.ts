import gql from 'graphql-tag';

export const HISTORIQUE_COMMANDE_INFO = gql`
  fragment HistoriqueCommandeInfo on PRTCommande {
    id
    dateCommande
    nbrRef
    quantite
    uniteGratuite
    prixBaseTotalHT
    valeurRemiseTotal
    prixNetTotalHT
    fraisPort
    ValeurFrancoPort
    remiseGlobale
    commentaireInterne
    commentaireExterne
    pathFile
    idOperation
    idUser
    idCommandeType
    idPharmacie
    idPublicite
    statut
    idCanal
    idGroupement
    prixNetTotalTTC
    totalTVA
    source
  }
`;

export const COMMANDE_LIGNE = gql`
  fragment CommandeLigneInfo on PRTCommandeLigne {
    id
    optimiser
    quantiteCommandee
    quantiteLivree
    uniteGratuite
    prixBaseHT
    remiseLigne
    remiseGamme
    prixNetUnitaireHT
    prixTotalHT
    idStatutLigne
    idCommande
    idProduitCanal
    createdAt
    updatedAt
    produit {
      id
      libelle
    }
  }
`;
