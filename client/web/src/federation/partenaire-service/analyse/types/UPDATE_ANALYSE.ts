/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTAnalyseInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_ANALYSE
// ====================================================

export interface UPDATE_ANALYSE_updateOnePRTAnalyse_fichiers {
  __typename: "Fichier";
  id: string;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface UPDATE_ANALYSE_updateOnePRTAnalyse_partenaireTypeAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  nom: string;
}

export interface UPDATE_ANALYSE_updateOnePRTAnalyse_partenaireTypeAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export type UPDATE_ANALYSE_updateOnePRTAnalyse_partenaireTypeAssocie = UPDATE_ANALYSE_updateOnePRTAnalyse_partenaireTypeAssocie_Laboratoire | UPDATE_ANALYSE_updateOnePRTAnalyse_partenaireTypeAssocie_PrestataireService;

export interface UPDATE_ANALYSE_updateOnePRTAnalyse_type {
  __typename: "PRTAnalyseType";
  id: string;
  libelle: string;
  code: string;
}

export interface UPDATE_ANALYSE_updateOnePRTAnalyse {
  __typename: "PRTAnalyse";
  id: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  dateChargement: any;
  partenaireType: string;
  fichiers: UPDATE_ANALYSE_updateOnePRTAnalyse_fichiers[] | null;
  partenaireTypeAssocie: UPDATE_ANALYSE_updateOnePRTAnalyse_partenaireTypeAssocie | null;
  type: UPDATE_ANALYSE_updateOnePRTAnalyse_type;
}

export interface UPDATE_ANALYSE {
  updateOnePRTAnalyse: UPDATE_ANALYSE_updateOnePRTAnalyse;
}

export interface UPDATE_ANALYSEVariables {
  id: string;
  input: PRTAnalyseInput;
}
