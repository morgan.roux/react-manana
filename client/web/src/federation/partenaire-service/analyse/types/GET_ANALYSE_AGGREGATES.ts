/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTAnalyseAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ANALYSE_AGGREGATES
// ====================================================

export interface GET_ANALYSE_AGGREGATES_pRTAnalyseAggregate_count {
  __typename: "PRTAnalyseCountAggregate";
  id: number | null;
}

export interface GET_ANALYSE_AGGREGATES_pRTAnalyseAggregate {
  __typename: "PRTAnalyseAggregateResponse";
  count: GET_ANALYSE_AGGREGATES_pRTAnalyseAggregate_count | null;
}

export interface GET_ANALYSE_AGGREGATES {
  pRTAnalyseAggregate: GET_ANALYSE_AGGREGATES_pRTAnalyseAggregate[];
}

export interface GET_ANALYSE_AGGREGATESVariables {
  filter?: PRTAnalyseAggregateFilter | null;
}
