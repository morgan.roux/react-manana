/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOnePRTAnalyseInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_ONE_ANALYSE
// ====================================================

export interface DELETE_ONE_ANALYSE_deleteOnePRTAnalyse {
  __typename: "PRTAnalyseDeleteResponse";
  id: string | null;
}

export interface DELETE_ONE_ANALYSE {
  deleteOnePRTAnalyse: DELETE_ONE_ANALYSE_deleteOnePRTAnalyse;
}

export interface DELETE_ONE_ANALYSEVariables {
  input: DeleteOnePRTAnalyseInput;
}
