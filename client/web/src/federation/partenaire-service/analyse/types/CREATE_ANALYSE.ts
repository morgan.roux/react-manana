/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTAnalyseInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_ANALYSE
// ====================================================

export interface CREATE_ANALYSE_createOnePRTAnalyse_fichiers {
  __typename: "Fichier";
  id: string;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface CREATE_ANALYSE_createOnePRTAnalyse_partenaireTypeAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  nom: string;
}

export interface CREATE_ANALYSE_createOnePRTAnalyse_partenaireTypeAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export type CREATE_ANALYSE_createOnePRTAnalyse_partenaireTypeAssocie = CREATE_ANALYSE_createOnePRTAnalyse_partenaireTypeAssocie_Laboratoire | CREATE_ANALYSE_createOnePRTAnalyse_partenaireTypeAssocie_PrestataireService;

export interface CREATE_ANALYSE_createOnePRTAnalyse_type {
  __typename: "PRTAnalyseType";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_ANALYSE_createOnePRTAnalyse {
  __typename: "PRTAnalyse";
  id: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  dateChargement: any;
  partenaireType: string;
  fichiers: CREATE_ANALYSE_createOnePRTAnalyse_fichiers[] | null;
  partenaireTypeAssocie: CREATE_ANALYSE_createOnePRTAnalyse_partenaireTypeAssocie | null;
  type: CREATE_ANALYSE_createOnePRTAnalyse_type;
}

export interface CREATE_ANALYSE {
  createOnePRTAnalyse: CREATE_ANALYSE_createOnePRTAnalyse;
}

export interface CREATE_ANALYSEVariables {
  input: PRTAnalyseInput;
}
