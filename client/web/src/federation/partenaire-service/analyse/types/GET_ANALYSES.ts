/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTAnalyseFilter, PRTAnalyseSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ANALYSES
// ====================================================

export interface GET_ANALYSES_pRTAnalyses_nodes_fichiers {
  __typename: "Fichier";
  id: string;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_ANALYSES_pRTAnalyses_nodes_partenaireTypeAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  nom: string;
}

export interface GET_ANALYSES_pRTAnalyses_nodes_partenaireTypeAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export type GET_ANALYSES_pRTAnalyses_nodes_partenaireTypeAssocie = GET_ANALYSES_pRTAnalyses_nodes_partenaireTypeAssocie_Laboratoire | GET_ANALYSES_pRTAnalyses_nodes_partenaireTypeAssocie_PrestataireService;

export interface GET_ANALYSES_pRTAnalyses_nodes_type {
  __typename: "PRTAnalyseType";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_ANALYSES_pRTAnalyses_nodes {
  __typename: "PRTAnalyse";
  id: string;
  titre: string;
  dateDebut: any;
  dateFin: any;
  dateChargement: any;
  partenaireType: string;
  fichiers: GET_ANALYSES_pRTAnalyses_nodes_fichiers[] | null;
  partenaireTypeAssocie: GET_ANALYSES_pRTAnalyses_nodes_partenaireTypeAssocie | null;
  type: GET_ANALYSES_pRTAnalyses_nodes_type;
}

export interface GET_ANALYSES_pRTAnalyses {
  __typename: "PRTAnalyseConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_ANALYSES_pRTAnalyses_nodes[];
}

export interface GET_ANALYSES {
  pRTAnalyses: GET_ANALYSES_pRTAnalyses;
}

export interface GET_ANALYSESVariables {
  paging?: OffsetPaging | null;
  filter?: PRTAnalyseFilter | null;
  sorting?: PRTAnalyseSort[] | null;
}
