/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTAnalyseTypeInfo
// ====================================================

export interface PRTAnalyseTypeInfo {
  __typename: "PRTAnalyseType";
  id: string;
  code: string;
  libelle: string;
}
