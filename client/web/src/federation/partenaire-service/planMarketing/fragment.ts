import gql from 'graphql-tag';
import { FULL_ACTION } from '../../basis/todo/action/fragment';

export const FULL_PLAN_MARKETING_INFO = gql`
  fragment PRTPlanMarketingInfo on PRTPlanMarketing {
    id
    titre
    description
    dateDebut
    dateFin
    partenaireType
    idPartenaireTypeAssocie
    idGroupement
    idPharmacie
    remise
    montant
    typeRemuneration
    remuneration
    type {
      id
      code(idPharmacie: $idPharmacie)
      libelle(idPharmacie: $idPharmacie)
      couleur(idPharmacie: $idPharmacie)
    }
    statut {
      id
      libelle
      code
    }
    fichiers {
      id
      chemin
      nomOriginal
      type
      publicUrl
    }
    miseAvants {
      id
      code
      libelle
    }
    actions {
      ...ActionInfo
    }
    produits {
      id
      produit {
        id
        libelle
        famille {
          id
          libelleFamille
        }
        produitCode {
          id
          code
        }
        produitPhoto {
          id
          fichier {
            id
            publicUrl
          }
        }
        quantitePrevue
        quantiteRealise
        prixUnitaire
        remisePrevue
      }
    }
    modeReglement {
      id
      code
      libelle
    }
    dateStatutCloture
    montantStatutCloture
    montantStatutRealise
    dateStatutRealise
  }
  ${FULL_ACTION}
`;

export const PLAN_MARKETING_TYPE_INFO = gql`
  fragment PRTPlanMarketingTypeInfo on PRTPlanMarketingType {
    id
    code(idPharmacie: $idPharmacie)
    libelle(idPharmacie: $idPharmacie)
    couleur(idPharmacie: $idPharmacie)
    active(idPharmacie: $idPharmacie)
    idPharmacie(idPharmacie: $idPharmacie)
    operationMarketings {
      id
    }
  }
`;

export const PLAN_MARKETING_STATUT_INFO = gql`
  fragment PRTPlanMarketingStatutInfo on PRTPlanMarketingStatut {
    id
    code
    libelle
  }
`;

export const MISE_AVANT_INFO = gql`
  fragment PRTMiseAvantInfo on PRTMiseAvant {
    id
    code
    libelle
    couleur
  }
`;
