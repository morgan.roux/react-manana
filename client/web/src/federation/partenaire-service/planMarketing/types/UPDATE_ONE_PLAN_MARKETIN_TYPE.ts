/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateOnePRTPlanMarketingTypeInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_ONE_PLAN_MARKETIN_TYPE
// ====================================================

export interface UPDATE_ONE_PLAN_MARKETIN_TYPE_updateOnePRTPlanMarketingType_operationMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
}

export interface UPDATE_ONE_PLAN_MARKETIN_TYPE_updateOnePRTPlanMarketingType {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
  active: boolean | null;
  idPharmacie: string | null;
  operationMarketings: UPDATE_ONE_PLAN_MARKETIN_TYPE_updateOnePRTPlanMarketingType_operationMarketings[] | null;
}

export interface UPDATE_ONE_PLAN_MARKETIN_TYPE {
  updateOnePRTPlanMarketingType: UPDATE_ONE_PLAN_MARKETIN_TYPE_updateOnePRTPlanMarketingType;
}

export interface UPDATE_ONE_PLAN_MARKETIN_TYPEVariables {
  idPharmacie?: string | null;
  input: UpdateOnePRTPlanMarketingTypeInput;
}
