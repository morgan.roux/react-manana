/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanMarketingTypeInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: PERSONNALISE_ONE_PLAN_MARKETIN_TYPE
// ====================================================

export interface PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_personnaliserPRTPlanMarketingType_operationMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
}

export interface PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_personnaliserPRTPlanMarketingType {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
  active: boolean | null;
  idPharmacie: string | null;
  operationMarketings: PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_personnaliserPRTPlanMarketingType_operationMarketings[] | null;
}

export interface PERSONNALISE_ONE_PLAN_MARKETIN_TYPE {
  personnaliserPRTPlanMarketingType: PERSONNALISE_ONE_PLAN_MARKETIN_TYPE_personnaliserPRTPlanMarketingType;
}

export interface PERSONNALISE_ONE_PLAN_MARKETIN_TYPEVariables {
  idPharmacie: string;
  input: PRTPlanMarketingTypeInput;
  idPlanMarketingType: string;
}
