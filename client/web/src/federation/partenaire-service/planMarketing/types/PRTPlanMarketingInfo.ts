/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTPlanMarketingInfo
// ====================================================

export interface PRTPlanMarketingInfo_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface PRTPlanMarketingInfo_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PRTPlanMarketingInfo_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PRTPlanMarketingInfo_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface PRTPlanMarketingInfo_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PRTPlanMarketingInfo_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface PRTPlanMarketingInfo_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: PRTPlanMarketingInfo_actions_importance | null;
  urgence: PRTPlanMarketingInfo_actions_urgence | null;
  nombreCommentaires: number | null;
}

export interface PRTPlanMarketingInfo_produits_produit_famille {
  __typename: "ProduitFamille";
  id: string;
  libelleFamille: string;
}

export interface PRTPlanMarketingInfo_produits_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string;
}

export interface PRTPlanMarketingInfo_produits_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface PRTPlanMarketingInfo_produits_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: PRTPlanMarketingInfo_produits_produit_produitPhoto_fichier;
}

export interface PRTPlanMarketingInfo_produits_produit {
  __typename: "ProduitInfo";
  id: string;
  libelle: string;
  famille: PRTPlanMarketingInfo_produits_produit_famille;
  produitCode: PRTPlanMarketingInfo_produits_produit_produitCode;
  produitPhoto: PRTPlanMarketingInfo_produits_produit_produitPhoto | null;
  quantitePrevue: number | null;
  quantiteRealise: number | null;
  prixUnitaire: number | null;
  remisePrevue: number | null;
}

export interface PRTPlanMarketingInfo_produits {
  __typename: "PlanMarketingProduitInfo";
  id: string;
  produit: PRTPlanMarketingInfo_produits_produit;
}

export interface PRTPlanMarketingInfo_modeReglement {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface PRTPlanMarketingInfo {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  remise: number | null;
  montant: number | null;
  typeRemuneration: string;
  remuneration: string | null;
  type: PRTPlanMarketingInfo_type;
  statut: PRTPlanMarketingInfo_statut;
  fichiers: PRTPlanMarketingInfo_fichiers[] | null;
  miseAvants: PRTPlanMarketingInfo_miseAvants[] | null;
  actions: PRTPlanMarketingInfo_actions[] | null;
  produits: PRTPlanMarketingInfo_produits[] | null;
  modeReglement: PRTPlanMarketingInfo_modeReglement | null;
  dateStatutCloture: any | null;
  montantStatutCloture: number | null;
  montantStatutRealise: number | null;
  dateStatutRealise: any | null;
}
