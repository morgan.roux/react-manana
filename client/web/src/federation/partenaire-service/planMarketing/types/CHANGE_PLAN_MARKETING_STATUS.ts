/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanMarketingChangeStatutInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CHANGE_PLAN_MARKETING_STATUS
// ====================================================

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_actions_importance | null;
  urgence: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_actions_urgence | null;
  nombreCommentaires: number | null;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_produits_produit_famille {
  __typename: "ProduitFamille";
  id: string;
  libelleFamille: string;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_produits_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_produits_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_produits_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_produits_produit_produitPhoto_fichier;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_produits_produit {
  __typename: "ProduitInfo";
  id: string;
  libelle: string;
  famille: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_produits_produit_famille;
  produitCode: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_produits_produit_produitCode;
  produitPhoto: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_produits_produit_produitPhoto | null;
  quantitePrevue: number | null;
  quantiteRealise: number | null;
  prixUnitaire: number | null;
  remisePrevue: number | null;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_produits {
  __typename: "PlanMarketingProduitInfo";
  id: string;
  produit: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_produits_produit;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_modeReglement {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  remise: number | null;
  montant: number | null;
  typeRemuneration: string;
  remuneration: string | null;
  type: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_type;
  statut: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_statut;
  fichiers: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_fichiers[] | null;
  miseAvants: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_miseAvants[] | null;
  actions: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_actions[] | null;
  produits: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_produits[] | null;
  modeReglement: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut_modeReglement | null;
  dateStatutCloture: any | null;
  montantStatutCloture: number | null;
  montantStatutRealise: number | null;
  dateStatutRealise: any | null;
}

export interface CHANGE_PLAN_MARKETING_STATUS {
  changePRTPlanMarketingStatut: CHANGE_PLAN_MARKETING_STATUS_changePRTPlanMarketingStatut;
}

export interface CHANGE_PLAN_MARKETING_STATUSVariables {
  idPharmacie?: string | null;
  id: string;
  input: PRTPlanMarketingChangeStatutInput;
}
