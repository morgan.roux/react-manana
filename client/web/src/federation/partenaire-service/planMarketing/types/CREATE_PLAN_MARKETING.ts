/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanMarketingInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PLAN_MARKETING
// ====================================================

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_actions_importance | null;
  urgence: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_actions_urgence | null;
  nombreCommentaires: number | null;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_produits_produit_famille {
  __typename: "ProduitFamille";
  id: string;
  libelleFamille: string;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_produits_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_produits_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_produits_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_produits_produit_produitPhoto_fichier;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_produits_produit {
  __typename: "ProduitInfo";
  id: string;
  libelle: string;
  famille: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_produits_produit_famille;
  produitCode: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_produits_produit_produitCode;
  produitPhoto: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_produits_produit_produitPhoto | null;
  quantitePrevue: number | null;
  quantiteRealise: number | null;
  prixUnitaire: number | null;
  remisePrevue: number | null;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_produits {
  __typename: "PlanMarketingProduitInfo";
  id: string;
  produit: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_produits_produit;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_modeReglement {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_PLAN_MARKETING_createOnePRTPlanMarketing {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  remise: number | null;
  montant: number | null;
  typeRemuneration: string;
  remuneration: string | null;
  type: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_type;
  statut: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_statut;
  fichiers: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_fichiers[] | null;
  miseAvants: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_miseAvants[] | null;
  actions: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_actions[] | null;
  produits: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_produits[] | null;
  modeReglement: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing_modeReglement | null;
  dateStatutCloture: any | null;
  montantStatutCloture: number | null;
  montantStatutRealise: number | null;
  dateStatutRealise: any | null;
}

export interface CREATE_PLAN_MARKETING {
  createOnePRTPlanMarketing: CREATE_PLAN_MARKETING_createOnePRTPlanMarketing;
}

export interface CREATE_PLAN_MARKETINGVariables {
  idPharmacie?: string | null;
  input: PRTPlanMarketingInput;
}
