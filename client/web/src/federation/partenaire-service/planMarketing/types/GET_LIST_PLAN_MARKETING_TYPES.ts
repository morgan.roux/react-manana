/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTPlanMarketingTypeFilter, PRTPlanMarketingTypeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_LIST_PLAN_MARKETING_TYPES
// ====================================================

export interface GET_LIST_PLAN_MARKETING_TYPES_pRTPlanMarketingTypes_nodes_operationMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
}

export interface GET_LIST_PLAN_MARKETING_TYPES_pRTPlanMarketingTypes_nodes {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
  active: boolean | null;
  idPharmacie: string | null;
  operationMarketings: GET_LIST_PLAN_MARKETING_TYPES_pRTPlanMarketingTypes_nodes_operationMarketings[] | null;
}

export interface GET_LIST_PLAN_MARKETING_TYPES_pRTPlanMarketingTypes {
  __typename: "PRTPlanMarketingTypeConnection";
  /**
   * Fetch total count of records
   */
  totalCount: number;
  /**
   * Array of nodes.
   */
  nodes: GET_LIST_PLAN_MARKETING_TYPES_pRTPlanMarketingTypes_nodes[];
}

export interface GET_LIST_PLAN_MARKETING_TYPES {
  pRTPlanMarketingTypes: GET_LIST_PLAN_MARKETING_TYPES_pRTPlanMarketingTypes;
}

export interface GET_LIST_PLAN_MARKETING_TYPESVariables {
  idPharmacie?: string | null;
  paging?: OffsetPaging | null;
  filter?: PRTPlanMarketingTypeFilter | null;
  sorting?: PRTPlanMarketingTypeSort[] | null;
}
