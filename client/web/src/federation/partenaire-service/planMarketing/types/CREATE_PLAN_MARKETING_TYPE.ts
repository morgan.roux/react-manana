/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreateOnePRTPlanMarketingTypeInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PLAN_MARKETING_TYPE
// ====================================================

export interface CREATE_PLAN_MARKETING_TYPE_createOnePRTPlanMarketingType_operationMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
}

export interface CREATE_PLAN_MARKETING_TYPE_createOnePRTPlanMarketingType {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
  active: boolean | null;
  idPharmacie: string | null;
  operationMarketings: CREATE_PLAN_MARKETING_TYPE_createOnePRTPlanMarketingType_operationMarketings[] | null;
}

export interface CREATE_PLAN_MARKETING_TYPE {
  createOnePRTPlanMarketingType: CREATE_PLAN_MARKETING_TYPE_createOnePRTPlanMarketingType;
}

export interface CREATE_PLAN_MARKETING_TYPEVariables {
  idPharmacie?: string | null;
  input: CreateOnePRTPlanMarketingTypeInput;
}
