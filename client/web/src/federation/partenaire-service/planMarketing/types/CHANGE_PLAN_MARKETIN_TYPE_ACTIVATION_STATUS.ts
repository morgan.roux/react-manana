/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS
// ====================================================

export interface CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS_changePlanMarketingTypeActivationStatus_operationMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
}

export interface CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS_changePlanMarketingTypeActivationStatus {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
  active: boolean | null;
  idPharmacie: string | null;
  operationMarketings: CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS_changePlanMarketingTypeActivationStatus_operationMarketings[] | null;
}

export interface CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS {
  changePlanMarketingTypeActivationStatus: CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUS_changePlanMarketingTypeActivationStatus;
}

export interface CHANGE_PLAN_MARKETIN_TYPE_ACTIVATION_STATUSVariables {
  idPharmacie: string;
  idPlanMarketingType: string;
  active: boolean;
}
