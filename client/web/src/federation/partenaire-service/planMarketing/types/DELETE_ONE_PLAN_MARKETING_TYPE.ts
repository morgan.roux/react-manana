/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_ONE_PLAN_MARKETING_TYPE
// ====================================================

export interface DELETE_ONE_PLAN_MARKETING_TYPE {
  deleteOnePRTPlanMarketingType: boolean;
}

export interface DELETE_ONE_PLAN_MARKETING_TYPEVariables {
  id: string;
}
