/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTPlanMarketingStatutInfo
// ====================================================

export interface PRTPlanMarketingStatutInfo {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  code: string;
  libelle: string;
}
