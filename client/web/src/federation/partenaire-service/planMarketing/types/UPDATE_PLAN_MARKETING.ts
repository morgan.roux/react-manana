/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanMarketingInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_PLAN_MARKETING
// ====================================================

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_actions_importance | null;
  urgence: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_actions_urgence | null;
  nombreCommentaires: number | null;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_produits_produit_famille {
  __typename: "ProduitFamille";
  id: string;
  libelleFamille: string;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_produits_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_produits_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_produits_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_produits_produit_produitPhoto_fichier;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_produits_produit {
  __typename: "ProduitInfo";
  id: string;
  libelle: string;
  famille: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_produits_produit_famille;
  produitCode: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_produits_produit_produitCode;
  produitPhoto: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_produits_produit_produitPhoto | null;
  quantitePrevue: number | null;
  quantiteRealise: number | null;
  prixUnitaire: number | null;
  remisePrevue: number | null;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_produits {
  __typename: "PlanMarketingProduitInfo";
  id: string;
  produit: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_produits_produit;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_modeReglement {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  remise: number | null;
  montant: number | null;
  typeRemuneration: string;
  remuneration: string | null;
  type: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_type;
  statut: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_statut;
  fichiers: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_fichiers[] | null;
  miseAvants: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_miseAvants[] | null;
  actions: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_actions[] | null;
  produits: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_produits[] | null;
  modeReglement: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing_modeReglement | null;
  dateStatutCloture: any | null;
  montantStatutCloture: number | null;
  montantStatutRealise: number | null;
  dateStatutRealise: any | null;
}

export interface UPDATE_PLAN_MARKETING {
  updateOnePRTPlanMarketing: UPDATE_PLAN_MARKETING_updateOnePRTPlanMarketing;
}

export interface UPDATE_PLAN_MARKETINGVariables {
  idPharmacie?: string | null;
  id: string;
  input: PRTPlanMarketingInput;
}
