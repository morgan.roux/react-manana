/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTPlanMarketingTypeInfo
// ====================================================

export interface PRTPlanMarketingTypeInfo_operationMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
}

export interface PRTPlanMarketingTypeInfo {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
  active: boolean | null;
  idPharmacie: string | null;
  operationMarketings: PRTPlanMarketingTypeInfo_operationMarketings[] | null;
}
