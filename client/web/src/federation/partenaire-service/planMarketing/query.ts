import gql from 'graphql-tag';
import {
  FULL_PLAN_MARKETING_INFO,
  PLAN_MARKETING_TYPE_INFO,
  PLAN_MARKETING_STATUT_INFO,
  MISE_AVANT_INFO,
} from './fragment';

export const GET_PLAN_MARKETINGS = gql`
  query GET_PLAN_MARKETINGS(
    $idPharmacie: String
    $paging: OffsetPaging
    $filter: PRTPlanMarketingFilter
    $sorting: [PRTPlanMarketingSort!]
  ) {
    pRTPlanMarketings(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTPlanMarketingInfo
      }
    }
  }

  ${FULL_PLAN_MARKETING_INFO}
`;

export const GET_PLAN_MARKETING_AGGREGATES = gql`
  query GET_PLAN_MARKETING_AGGREGATES($filter: PRTPlanMarketingAggregateFilter) {
    pRTPlanMarketingAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_LIST_PLAN_MARKETING_TYPES = gql`
  query GET_LIST_PLAN_MARKETING_TYPES(
    $idPharmacie: String
    $paging: OffsetPaging
    $filter: PRTPlanMarketingTypeFilter
    $sorting: [PRTPlanMarketingTypeSort!]
  ) {
    pRTPlanMarketingTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      totalCount
      nodes {
        ...PRTPlanMarketingTypeInfo
      }
    }
  }
  ${PLAN_MARKETING_TYPE_INFO}
`;
export const GET_LIST_PLAN_MARKETING_STATUT = gql`
  query GET_LIST_PLAN_MARKETING_STATUT(
    $paging: OffsetPaging
    $filter: PRTPlanMarketingStatutFilter
    $sorting: [PRTPlanMarketingStatutSort!]
  ) {
    pRTPlanMarketingStatuts(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTPlanMarketingStatutInfo
      }
    }
  }
  ${PLAN_MARKETING_STATUT_INFO}
`;

export const GET_LIST_MISE_AVANT = gql`
  query LIST_MISE_AVANT(
    $paging: OffsetPaging
    $filter: PRTMiseAvantFilter
    $sorting: [PRTMiseAvantSort!]
  ) {
    pRTMiseAvants(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...PRTMiseAvantInfo
      }
    }
  }
  ${MISE_AVANT_INFO}
`;
