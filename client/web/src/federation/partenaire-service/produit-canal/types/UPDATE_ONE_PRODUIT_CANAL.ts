/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateOneProduitCanalInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_ONE_PRODUIT_CANAL
// ====================================================

export interface UPDATE_ONE_PRODUIT_CANAL_updateOneProduitCanal {
  __typename: "ProduitCanal";
  id: string;
  qteStock: number | null;
  qteMin: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  uniteSupCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  prixTtc: number | null;
  dateRetrait: any | null;
  datePeremption: any | null;
  idProduit: string;
  idCanal: string;
}

export interface UPDATE_ONE_PRODUIT_CANAL {
  updateOneProduitCanal: UPDATE_ONE_PRODUIT_CANAL_updateOneProduitCanal;
}

export interface UPDATE_ONE_PRODUIT_CANALVariables {
  input: UpdateOneProduitCanalInput;
}
