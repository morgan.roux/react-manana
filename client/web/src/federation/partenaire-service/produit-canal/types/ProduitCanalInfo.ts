/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ProduitCanalInfo
// ====================================================

export interface ProduitCanalInfo {
  __typename: "ProduitCanal";
  id: string;
  qteStock: number | null;
  qteMin: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  uniteSupCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  prixTtc: number | null;
  dateRetrait: any | null;
  datePeremption: any | null;
  idProduit: string;
  idCanal: string;
}
