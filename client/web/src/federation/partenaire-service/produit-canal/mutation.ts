import gql from 'graphql-tag';
import { PRODUIT_CANAL_INFO } from './fragment';

export const UPDATE_ONE_PRODUIT_CANAL = gql`
  mutation UPDATE_ONE_PRODUIT_CANAL($input: UpdateOneProduitCanalInput!) {
    updateOneProduitCanal(input: $input) {
      ...ProduitCanalInfo
    }
  }
  ${PRODUIT_CANAL_INFO}
`;
