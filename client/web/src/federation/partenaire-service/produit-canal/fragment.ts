import gql from 'graphql-tag';

export const PRODUIT_CANAL_INFO = gql`
  fragment ProduitCanalInfo on ProduitCanal {
    id
    qteStock
    qteMin
    stv
    unitePetitCond
    uniteSupCond
    prixFab
    prixPhv
    prixTtc
    dateRetrait
    datePeremption
    idProduit
    idCanal
  }
`;
