/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PrtPlanningMarketingInfo
// ====================================================

export interface PrtPlanningMarketingInfo_partenaireTypeAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  nom: string;
}

export interface PrtPlanningMarketingInfo_partenaireTypeAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export type PrtPlanningMarketingInfo_partenaireTypeAssocie = PrtPlanningMarketingInfo_partenaireTypeAssocie_Laboratoire | PrtPlanningMarketingInfo_partenaireTypeAssocie_PrestataireService;

export interface PrtPlanningMarketingInfo_planMarketings_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface PrtPlanningMarketingInfo_planMarketings_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PrtPlanningMarketingInfo_planMarketings_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PrtPlanningMarketingInfo_planMarketings_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface PrtPlanningMarketingInfo_planMarketings_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PrtPlanningMarketingInfo_planMarketings_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface PrtPlanningMarketingInfo_planMarketings_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: PrtPlanningMarketingInfo_planMarketings_actions_importance | null;
  urgence: PrtPlanningMarketingInfo_planMarketings_actions_urgence | null;
  nombreCommentaires: number | null;
}

export interface PrtPlanningMarketingInfo_planMarketings_produits_produit_famille {
  __typename: "ProduitFamille";
  id: string;
  libelleFamille: string;
}

export interface PrtPlanningMarketingInfo_planMarketings_produits_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string;
}

export interface PrtPlanningMarketingInfo_planMarketings_produits_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface PrtPlanningMarketingInfo_planMarketings_produits_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: PrtPlanningMarketingInfo_planMarketings_produits_produit_produitPhoto_fichier;
}

export interface PrtPlanningMarketingInfo_planMarketings_produits_produit {
  __typename: "ProduitInfo";
  id: string;
  libelle: string;
  famille: PrtPlanningMarketingInfo_planMarketings_produits_produit_famille;
  produitCode: PrtPlanningMarketingInfo_planMarketings_produits_produit_produitCode;
  produitPhoto: PrtPlanningMarketingInfo_planMarketings_produits_produit_produitPhoto | null;
  quantitePrevue: number | null;
  quantiteRealise: number | null;
  prixUnitaire: number | null;
  remisePrevue: number | null;
}

export interface PrtPlanningMarketingInfo_planMarketings_produits {
  __typename: "PlanMarketingProduitInfo";
  id: string;
  produit: PrtPlanningMarketingInfo_planMarketings_produits_produit;
}

export interface PrtPlanningMarketingInfo_planMarketings_modeReglement {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface PrtPlanningMarketingInfo_planMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  remise: number | null;
  montant: number | null;
  typeRemuneration: string;
  remuneration: string | null;
  type: PrtPlanningMarketingInfo_planMarketings_type;
  statut: PrtPlanningMarketingInfo_planMarketings_statut;
  fichiers: PrtPlanningMarketingInfo_planMarketings_fichiers[] | null;
  miseAvants: PrtPlanningMarketingInfo_planMarketings_miseAvants[] | null;
  actions: PrtPlanningMarketingInfo_planMarketings_actions[] | null;
  produits: PrtPlanningMarketingInfo_planMarketings_produits[] | null;
  modeReglement: PrtPlanningMarketingInfo_planMarketings_modeReglement | null;
  dateStatutCloture: any | null;
  montantStatutCloture: number | null;
  montantStatutRealise: number | null;
  dateStatutRealise: any | null;
}

export interface PrtPlanningMarketingInfo {
  __typename: "PRTPlanningMarketing";
  id: string;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  partenaireTypeAssocie: PrtPlanningMarketingInfo_partenaireTypeAssocie | null;
  planMarketings: PrtPlanningMarketingInfo_planMarketings[] | null;
}
