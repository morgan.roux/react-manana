/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTPlanningMarketingInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: PLANNING_MARKETINGS
// ====================================================

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_partenaireTypeAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  nom: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_partenaireTypeAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export type PLANNING_MARKETINGS_pRTPlanningMarketing_partenaireTypeAssocie = PLANNING_MARKETINGS_pRTPlanningMarketing_partenaireTypeAssocie_Laboratoire | PLANNING_MARKETINGS_pRTPlanningMarketing_partenaireTypeAssocie_PrestataireService;

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_type {
  __typename: "PRTPlanMarketingType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_statut {
  __typename: "PRTPlanMarketingStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_miseAvants {
  __typename: "PRTMiseAvant";
  id: string;
  code: string;
  libelle: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_actions_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_actions_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_actions {
  __typename: "TodoAction";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_actions_importance | null;
  urgence: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_actions_urgence | null;
  nombreCommentaires: number | null;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_produits_produit_famille {
  __typename: "ProduitFamille";
  id: string;
  libelleFamille: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_produits_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_produits_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_produits_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_produits_produit_produitPhoto_fichier;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_produits_produit {
  __typename: "ProduitInfo";
  id: string;
  libelle: string;
  famille: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_produits_produit_famille;
  produitCode: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_produits_produit_produitCode;
  produitPhoto: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_produits_produit_produitPhoto | null;
  quantitePrevue: number | null;
  quantiteRealise: number | null;
  prixUnitaire: number | null;
  remisePrevue: number | null;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_produits {
  __typename: "PlanMarketingProduitInfo";
  id: string;
  produit: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_produits_produit;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_modeReglement {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings {
  __typename: "PRTPlanMarketing";
  id: string;
  titre: string;
  description: string | null;
  dateDebut: any;
  dateFin: any;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  idGroupement: string;
  idPharmacie: string;
  remise: number | null;
  montant: number | null;
  typeRemuneration: string;
  remuneration: string | null;
  type: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_type;
  statut: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_statut;
  fichiers: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_fichiers[] | null;
  miseAvants: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_miseAvants[] | null;
  actions: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_actions[] | null;
  produits: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_produits[] | null;
  modeReglement: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings_modeReglement | null;
  dateStatutCloture: any | null;
  montantStatutCloture: number | null;
  montantStatutRealise: number | null;
  dateStatutRealise: any | null;
}

export interface PLANNING_MARKETINGS_pRTPlanningMarketing {
  __typename: "PRTPlanningMarketing";
  id: string;
  partenaireType: string;
  idPartenaireTypeAssocie: string;
  partenaireTypeAssocie: PLANNING_MARKETINGS_pRTPlanningMarketing_partenaireTypeAssocie | null;
  planMarketings: PLANNING_MARKETINGS_pRTPlanningMarketing_planMarketings[] | null;
}

export interface PLANNING_MARKETINGS {
  pRTPlanningMarketing: PLANNING_MARKETINGS_pRTPlanningMarketing[];
}

export interface PLANNING_MARKETINGSVariables {
  idPharmacie?: string | null;
  input: PRTPlanningMarketingInput;
}
