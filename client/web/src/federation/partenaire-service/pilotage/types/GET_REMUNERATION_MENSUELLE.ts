/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRemunerationMensuelleInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_REMUNERATION_MENSUELLE
// ====================================================

export interface GET_REMUNERATION_MENSUELLE_remunerationMensuelle_data_prestation {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface GET_REMUNERATION_MENSUELLE_remunerationMensuelle_data {
  __typename: "PRTRemunerationMensuelleMontant";
  prestation: GET_REMUNERATION_MENSUELLE_remunerationMensuelle_data_prestation | null;
  totalRemunerationPrevu: number;
  totalRemunerationReglement: number;
  differenceTotalRemuneration: number;
  month: any;
}

export interface GET_REMUNERATION_MENSUELLE_remunerationMensuelle {
  __typename: "PRTRemunerationMensuelle";
  data: GET_REMUNERATION_MENSUELLE_remunerationMensuelle_data[];
}

export interface GET_REMUNERATION_MENSUELLE {
  remunerationMensuelle: GET_REMUNERATION_MENSUELLE_remunerationMensuelle;
}

export interface GET_REMUNERATION_MENSUELLEVariables {
  input: PRTRemunerationMensuelleInput;
}
