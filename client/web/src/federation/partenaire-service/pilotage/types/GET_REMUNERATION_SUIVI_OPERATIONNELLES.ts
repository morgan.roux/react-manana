/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRemunerationsEvolutionsInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_REMUNERATION_SUIVI_OPERATIONNELLES
// ====================================================

export interface GET_REMUNERATION_SUIVI_OPERATIONNELLES_remunerationsSuiviOperationnelles_types_prestation {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface GET_REMUNERATION_SUIVI_OPERATIONNELLES_remunerationsSuiviOperationnelles_types {
  __typename: "PRTRemunerationSuiviOperationnelMontant";
  prestation: GET_REMUNERATION_SUIVI_OPERATIONNELLES_remunerationsSuiviOperationnelles_types_prestation;
  totalMontantPrevuPrestation: number;
  totalMontantReglementPrestation: number;
  pourcentagePrestation: number;
  idPartenaireTypeAssocie: string;
  nomPartenaire: string;
}

export interface GET_REMUNERATION_SUIVI_OPERATIONNELLES_remunerationsSuiviOperationnelles {
  __typename: "PRTRemunerationSuiviOperationnel";
  types: GET_REMUNERATION_SUIVI_OPERATIONNELLES_remunerationsSuiviOperationnelles_types[];
  totalMontantPrevues: number;
  totalMontantReglements: number;
  pourcentageReelReglement: number;
}

export interface GET_REMUNERATION_SUIVI_OPERATIONNELLES {
  remunerationsSuiviOperationnelles: GET_REMUNERATION_SUIVI_OPERATIONNELLES_remunerationsSuiviOperationnelles[];
}

export interface GET_REMUNERATION_SUIVI_OPERATIONNELLESVariables {
  input: PRTRemunerationsEvolutionsInput;
}
