/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RemunerationMensuelleInfo
// ====================================================

export interface RemunerationMensuelleInfo_data_prestation {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface RemunerationMensuelleInfo_data {
  __typename: "PRTRemunerationMensuelleMontant";
  prestation: RemunerationMensuelleInfo_data_prestation | null;
  totalRemunerationPrevu: number;
  totalRemunerationReglement: number;
  differenceTotalRemuneration: number;
  month: any;
}

export interface RemunerationMensuelleInfo {
  __typename: "PRTRemunerationMensuelle";
  data: RemunerationMensuelleInfo_data[];
}
