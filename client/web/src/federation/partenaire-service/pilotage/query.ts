import gql from 'graphql-tag';
import { REMUNERATION_MENSUELLE_INFO, REMUNERATION_SUIVI_OPERATIONNEL_INFO } from './fragment';

export const GET_REMUNERATION_MENSUELLE = gql`
  query GET_REMUNERATION_MENSUELLE($input: PRTRemunerationMensuelleInput!) {
    remunerationMensuelle(input: $input) {
      ...RemunerationMensuelleInfo
    }
  }
  ${REMUNERATION_MENSUELLE_INFO}
`;

export const GET_REMUNERATION_SUIVI_OPERATIONNEL = gql`
  query GET_REMUNERATION_SUIVI_OPERATIONNEL($input: PRTRemunerationEvolutionInput!) {
    remunerationSuiviOperationnel(input: $input) {
      ...RemunerationSuiviOperationnelInfo
    }
  }
  ${REMUNERATION_SUIVI_OPERATIONNEL_INFO}
`;

export const GET_REMUNERATION_SUIVI_OPERATIONNELLES = gql`
  query GET_REMUNERATION_SUIVI_OPERATIONNELLES($input: PRTRemunerationsEvolutionsInput! ) {
    remunerationsSuiviOperationnelles(input: $input) {
      ...RemunerationSuiviOperationnelInfo
    }
  }
  ${REMUNERATION_SUIVI_OPERATIONNEL_INFO}
`;