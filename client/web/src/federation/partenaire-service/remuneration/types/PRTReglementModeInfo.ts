/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PRTReglementModeInfo
// ====================================================

export interface PRTReglementModeInfo {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}
