/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PrestationTypeFilter, PrestationTypeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_PRESTATION_TYPE
// ====================================================

export interface GET_PRESTATION_TYPE_prestationTypes_nodes {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface GET_PRESTATION_TYPE_prestationTypes {
  __typename: "PrestationTypeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_PRESTATION_TYPE_prestationTypes_nodes[];
}

export interface GET_PRESTATION_TYPE {
  prestationTypes: GET_PRESTATION_TYPE_prestationTypes;
}

export interface GET_PRESTATION_TYPEVariables {
  paging?: OffsetPaging | null;
  filter?: PrestationTypeFilter | null;
  sorting?: PrestationTypeSort[] | null;
}
