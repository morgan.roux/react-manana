/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRemunerationReglementAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_PRT_REMUNERATION_REGLEMENTS_AGGREGATES
// ====================================================

export interface GET_PRT_REMUNERATION_REGLEMENTS_AGGREGATES_pRTRemunerationReglementAggregate_count {
  __typename: "PRTRemunerationReglementCountAggregate";
  id: number | null;
}

export interface GET_PRT_REMUNERATION_REGLEMENTS_AGGREGATES_pRTRemunerationReglementAggregate {
  __typename: "PRTRemunerationReglementAggregateResponse";
  count: GET_PRT_REMUNERATION_REGLEMENTS_AGGREGATES_pRTRemunerationReglementAggregate_count | null;
}

export interface GET_PRT_REMUNERATION_REGLEMENTS_AGGREGATES {
  pRTRemunerationReglementAggregate: GET_PRT_REMUNERATION_REGLEMENTS_AGGREGATES_pRTRemunerationReglementAggregate[];
}

export interface GET_PRT_REMUNERATION_REGLEMENTS_AGGREGATESVariables {
  filter?: PRTRemunerationReglementAggregateFilter | null;
}
