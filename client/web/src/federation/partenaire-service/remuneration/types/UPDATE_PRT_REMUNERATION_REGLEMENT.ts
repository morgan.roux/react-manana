/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRemunerationReglementInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_PRT_REMUNERATION_REGLEMENT
// ====================================================

export interface UPDATE_PRT_REMUNERATION_REGLEMENT_updateOnePRTRemunerationReglement_reglementMode {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_PRT_REMUNERATION_REGLEMENT_updateOnePRTRemunerationReglement_remuneration_prestationType {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface UPDATE_PRT_REMUNERATION_REGLEMENT_updateOnePRTRemunerationReglement_remuneration {
  __typename: "PRTRemuneration";
  id: string;
  dateEcheance: any;
  montantPrevu: number;
  montantRealise: number;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  prestationType: UPDATE_PRT_REMUNERATION_REGLEMENT_updateOnePRTRemunerationReglement_remuneration_prestationType;
}

export interface UPDATE_PRT_REMUNERATION_REGLEMENT_updateOnePRTRemunerationReglement_prestationType {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface UPDATE_PRT_REMUNERATION_REGLEMENT_updateOnePRTRemunerationReglement_fichiers {
  __typename: "Fichier";
  id: string;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface UPDATE_PRT_REMUNERATION_REGLEMENT_updateOnePRTRemunerationReglement {
  __typename: "PRTRemunerationReglement";
  id: string;
  idPrestationType: string;
  idModeReglement: string;
  idRemuneration: string;
  montant: number;
  description: string | null;
  dateReglement: any;
  reglementMode: UPDATE_PRT_REMUNERATION_REGLEMENT_updateOnePRTRemunerationReglement_reglementMode;
  remuneration: UPDATE_PRT_REMUNERATION_REGLEMENT_updateOnePRTRemunerationReglement_remuneration;
  prestationType: UPDATE_PRT_REMUNERATION_REGLEMENT_updateOnePRTRemunerationReglement_prestationType;
  fichiers: UPDATE_PRT_REMUNERATION_REGLEMENT_updateOnePRTRemunerationReglement_fichiers[] | null;
}

export interface UPDATE_PRT_REMUNERATION_REGLEMENT {
  updateOnePRTRemunerationReglement: UPDATE_PRT_REMUNERATION_REGLEMENT_updateOnePRTRemunerationReglement;
}

export interface UPDATE_PRT_REMUNERATION_REGLEMENTVariables {
  id: string;
  input: PRTRemunerationReglementInput;
}
