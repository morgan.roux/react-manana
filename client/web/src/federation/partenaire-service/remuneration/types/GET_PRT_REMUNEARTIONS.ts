/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTRemunerationFilter, PRTRemunerationSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_PRT_REMUNEARTIONS
// ====================================================

export interface GET_PRT_REMUNEARTIONS_pRTRemunerations_nodes_prestationType {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface GET_PRT_REMUNEARTIONS_pRTRemunerations_nodes {
  __typename: "PRTRemuneration";
  id: string;
  dateEcheance: any;
  montantPrevu: number;
  montantRealise: number;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  prestationType: GET_PRT_REMUNEARTIONS_pRTRemunerations_nodes_prestationType;
}

export interface GET_PRT_REMUNEARTIONS_pRTRemunerations {
  __typename: "PRTRemunerationConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_PRT_REMUNEARTIONS_pRTRemunerations_nodes[];
}

export interface GET_PRT_REMUNEARTIONS {
  pRTRemunerations: GET_PRT_REMUNEARTIONS_pRTRemunerations;
}

export interface GET_PRT_REMUNEARTIONSVariables {
  paging?: OffsetPaging | null;
  filter?: PRTRemunerationFilter | null;
  sorting?: PRTRemunerationSort[] | null;
}
