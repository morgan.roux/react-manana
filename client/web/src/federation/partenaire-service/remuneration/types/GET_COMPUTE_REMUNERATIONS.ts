/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ComputeRemunerationsInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_COMPUTE_REMUNERATIONS
// ====================================================

export interface GET_COMPUTE_REMUNERATIONS_computeRemunerations_currentYear {
  __typename: "ComputeRemunerationByYear";
  annee: number;
  montantConfirme: number;
  montantPrevu: number;
  montantRealise: number;
}

export interface GET_COMPUTE_REMUNERATIONS_computeRemunerations_lastYear {
  __typename: "ComputeRemunerationByYear";
  annee: number;
  montantConfirme: number;
  montantPrevu: number;
  montantRealise: number;
}

export interface GET_COMPUTE_REMUNERATIONS_computeRemunerations {
  __typename: "ComputeRemunerationsOutput";
  currentYear: GET_COMPUTE_REMUNERATIONS_computeRemunerations_currentYear;
  lastYear: GET_COMPUTE_REMUNERATIONS_computeRemunerations_lastYear;
}

export interface GET_COMPUTE_REMUNERATIONS {
  computeRemunerations: GET_COMPUTE_REMUNERATIONS_computeRemunerations;
}

export interface GET_COMPUTE_REMUNERATIONSVariables {
  input: ComputeRemunerationsInput;
}
