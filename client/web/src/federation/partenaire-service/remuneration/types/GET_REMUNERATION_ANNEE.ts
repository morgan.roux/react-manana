/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRemunerationAnneeInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_REMUNERATION_ANNEE
// ====================================================

export interface GET_REMUNERATION_ANNEE_remunerationAnnee_dataAnnee_data_types_prestation {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface GET_REMUNERATION_ANNEE_remunerationAnnee_dataAnnee_data_types {
  __typename: "PRTRemunerationSuiviOperationnelMontant";
  prestation: GET_REMUNERATION_ANNEE_remunerationAnnee_dataAnnee_data_types_prestation;
  totalMontantPrevuPrestation: number;
  totalMontantReglementPrestation: number;
  pourcentagePrestation: number;
  idPartenaireTypeAssocie: string;
  nomPartenaire: string;
}

export interface GET_REMUNERATION_ANNEE_remunerationAnnee_dataAnnee_data {
  __typename: "PRTRemunerationSuiviOperationnel";
  types: GET_REMUNERATION_ANNEE_remunerationAnnee_dataAnnee_data_types[];
  totalMontantPrevues: number;
  totalMontantReglements: number;
  pourcentageReelReglement: number;
}

export interface GET_REMUNERATION_ANNEE_remunerationAnnee_dataAnnee {
  __typename: "PRTRemunerationAnneeValue";
  indexMois: number;
  data: GET_REMUNERATION_ANNEE_remunerationAnnee_dataAnnee_data;
}

export interface GET_REMUNERATION_ANNEE_remunerationAnnee {
  __typename: "PRTRemunerationAnnee";
  nomPartenaire: string;
  composition: string;
  totalPrevues: number;
  totalRealises: number;
  pourcentageRealises: number;
  dataAnnee: GET_REMUNERATION_ANNEE_remunerationAnnee_dataAnnee[];
}

export interface GET_REMUNERATION_ANNEE {
  remunerationAnnee: GET_REMUNERATION_ANNEE_remunerationAnnee[];
}

export interface GET_REMUNERATION_ANNEEVariables {
  input: PRTRemunerationAnneeInput;
}
