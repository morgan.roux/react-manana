/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_PRT_REMUNERATION
// ====================================================

export interface DELETE_PRT_REMUNERATION_deleteOnePRTRemuneration {
  __typename: "PRTRemuneration";
  id: string;
}

export interface DELETE_PRT_REMUNERATION {
  deleteOnePRTRemuneration: DELETE_PRT_REMUNERATION_deleteOnePRTRemuneration;
}

export interface DELETE_PRT_REMUNERATIONVariables {
  id: string;
}
