/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, PRTReglementModeFilter, PRTReglementModeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_PRT_REGLEMENT_MODE
// ====================================================

export interface GET_PRT_REGLEMENT_MODE_pRTReglementModes_nodes {
  __typename: "PRTReglementMode";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_PRT_REGLEMENT_MODE_pRTReglementModes {
  __typename: "PRTReglementModeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_PRT_REGLEMENT_MODE_pRTReglementModes_nodes[];
}

export interface GET_PRT_REGLEMENT_MODE {
  pRTReglementModes: GET_PRT_REGLEMENT_MODE_pRTReglementModes;
}

export interface GET_PRT_REGLEMENT_MODEVariables {
  paging?: OffsetPaging | null;
  filter?: PRTReglementModeFilter | null;
  sorting?: PRTReglementModeSort[] | null;
}
