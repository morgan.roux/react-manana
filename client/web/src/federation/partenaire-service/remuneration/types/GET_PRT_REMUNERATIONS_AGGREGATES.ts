/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRemunerationAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_PRT_REMUNERATIONS_AGGREGATES
// ====================================================

export interface GET_PRT_REMUNERATIONS_AGGREGATES_pRTRemunerationAggregate_count {
  __typename: "PRTRemunerationCountAggregate";
  id: number | null;
}

export interface GET_PRT_REMUNERATIONS_AGGREGATES_pRTRemunerationAggregate {
  __typename: "PRTRemunerationAggregateResponse";
  count: GET_PRT_REMUNERATIONS_AGGREGATES_pRTRemunerationAggregate_count | null;
}

export interface GET_PRT_REMUNERATIONS_AGGREGATES {
  pRTRemunerationAggregate: GET_PRT_REMUNERATIONS_AGGREGATES_pRTRemunerationAggregate[];
}

export interface GET_PRT_REMUNERATIONS_AGGREGATESVariables {
  filter?: PRTRemunerationAggregateFilter | null;
}
