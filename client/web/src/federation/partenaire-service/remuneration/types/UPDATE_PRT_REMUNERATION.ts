/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PRTRemunerationInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_PRT_REMUNERATION
// ====================================================

export interface UPDATE_PRT_REMUNERATION_updateOnePRTRemuneration_prestationType {
  __typename: "PrestationType";
  id: string;
  code: string;
  libelle: string;
  couleur: string | null;
}

export interface UPDATE_PRT_REMUNERATION_updateOnePRTRemuneration {
  __typename: "PRTRemuneration";
  id: string;
  dateEcheance: any;
  montantPrevu: number;
  montantRealise: number;
  idPartenaireTypeAssocie: string;
  partenaireType: string;
  prestationType: UPDATE_PRT_REMUNERATION_updateOnePRTRemuneration_prestationType;
}

export interface UPDATE_PRT_REMUNERATION {
  updateOnePRTRemuneration: UPDATE_PRT_REMUNERATION_updateOnePRTRemuneration;
}

export interface UPDATE_PRT_REMUNERATIONVariables {
  id: string;
  input: PRTRemunerationInput;
}
