/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DepartementFilter, DepartementSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: DEPARTEMENTS
// ====================================================

export interface DEPARTEMENTS_departements_nodes {
  __typename: "Departement";
  id: string;
  code: string;
  nom: string;
}

export interface DEPARTEMENTS_departements {
  __typename: "DepartementConnection";
  /**
   * Fetch total count of records
   */
  totalCount: number;
  /**
   * Array of nodes.
   */
  nodes: DEPARTEMENTS_departements_nodes[];
}

export interface DEPARTEMENTS {
  departements: DEPARTEMENTS_departements;
}

export interface DEPARTEMENTSVariables {
  paging?: OffsetPaging | null;
  filter?: DepartementFilter | null;
  sorting?: DepartementSort[] | null;
}
