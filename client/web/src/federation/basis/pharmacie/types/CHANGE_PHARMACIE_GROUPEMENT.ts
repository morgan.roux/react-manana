/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CHANGE_PHARMACIE_GROUPEMENT
// ====================================================

export interface CHANGE_PHARMACIE_GROUPEMENT {
  changePharmacieGroupement: boolean;
}

export interface CHANGE_PHARMACIE_GROUPEMENTVariables {
  idPharmacie: string;
  idGroupement: string;
}
