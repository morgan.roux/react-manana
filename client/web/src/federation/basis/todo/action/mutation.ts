import gql from 'graphql-tag';
import { FULL_ACTION } from './fragment';

export const CHANGE_STATUS_ACTION = gql`
  mutation CHANGE_STATUS_ACTION($id: String!, $status: String!) {
    changeStatutAction(id: $id, status: $status) {
      ...ActionInfo
    }
  }

  ${FULL_ACTION}
`;
