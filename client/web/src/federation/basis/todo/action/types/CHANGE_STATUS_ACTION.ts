/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CHANGE_STATUS_ACTION
// ====================================================

export interface CHANGE_STATUS_ACTION_changeStatutAction_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CHANGE_STATUS_ACTION_changeStatutAction_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CHANGE_STATUS_ACTION_changeStatutAction {
  __typename: "TodoAction";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any;
  dateFin: any | null;
  idParent: string | null;
  idItemAssocie: string;
  idImportance: string | null;
  status: string;
  importance: CHANGE_STATUS_ACTION_changeStatutAction_importance | null;
  urgence: CHANGE_STATUS_ACTION_changeStatutAction_urgence | null;
  nombreCommentaires: number | null;
}

export interface CHANGE_STATUS_ACTION {
  changeStatutAction: CHANGE_STATUS_ACTION_changeStatutAction | null;
}

export interface CHANGE_STATUS_ACTIONVariables {
  id: string;
  status: string;
}
