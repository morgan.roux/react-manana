/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UPDATE_INFORMATION_LIAISON
// ====================================================

export interface UPDATE_INFORMATION_LIAISON_updateInformationLiaisonStatut {
  __typename: "InformationLiaison";
  id: string;
  description: string;
  idTache: string | null;
  idFonction: string | null;
  idImportance: string;
  idUrgence: string;
  statut: string;
}

export interface UPDATE_INFORMATION_LIAISON {
  updateInformationLiaisonStatut: UPDATE_INFORMATION_LIAISON_updateInformationLiaisonStatut | null;
}

export interface UPDATE_INFORMATION_LIAISONVariables {
  code: string;
  id: string;
}
