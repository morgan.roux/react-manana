/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: InformationLiaisonInfo
// ====================================================

export interface InformationLiaisonInfo {
  __typename: "InformationLiaison";
  id: string;
  description: string;
  idTache: string | null;
  idFonction: string | null;
  idImportance: string;
  idUrgence: string;
  statut: string;
}
