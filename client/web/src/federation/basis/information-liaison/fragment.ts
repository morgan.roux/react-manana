import gql from 'graphql-tag';

export const INFORMATION_LIAISON_INFO = gql`
  fragment InformationLiaisonInfo on InformationLiaison {
    id
    description
    idTache
    idFonction
    idImportance
    idUrgence
    statut
  }
`;

export const INFORMATION_LIAISON_TYPE_INFO = gql`
  fragment InformationLiaisonTypeInfo on InformationLiaisonType {
    id
    code
    libelle
  }
`;
