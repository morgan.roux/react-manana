import gql from 'graphql-tag';
import { INFORMATION_LIAISON_INFO } from './fragment';

export const UPDATE_INFORMATION_LIAISON = gql`
    mutation UPDATE_INFORMATION_LIAISON($code: String!,$id: String!){
        updateInformationLiaisonStatut(code:$code,id:$id){
            ...InformationLiaisonInfo
        }
    }
    ${INFORMATION_LIAISON_INFO}
`;