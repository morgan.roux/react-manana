import gql from 'graphql-tag';
import { INFORMATION_LIAISON_TYPE_INFO } from './fragment';

export const GET_INFORMATION_LIAISON_TYPES = gql`
  query INFORMATION_LIAISON_TYPES(
    $paging: OffsetPaging
    $filter: InformationLiaisonTypeFilter
    $sorting: [InformationLiaisonTypeSort!]
  ) {
    informationLiaisonTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...InformationLiaisonTypeInfo
      }
    }
  }
  ${INFORMATION_LIAISON_TYPE_INFO}
`;

export const GET_INFORMATION_LIAISON_TYPE = gql`
  query INFORMATION_LIAISON_TYPE($id: ID!) {
    informationLiaisonType(id: $id) {
      ...InformationLiaisonTypeInfo
    }
  }
  ${INFORMATION_LIAISON_TYPE_INFO}
`;
