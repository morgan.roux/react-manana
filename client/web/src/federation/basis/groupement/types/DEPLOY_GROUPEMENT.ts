/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DEPLOY_GROUPEMENT
// ====================================================

export interface DEPLOY_GROUPEMENT {
  deployGroupement: string[];
}

export interface DEPLOY_GROUPEMENTVariables {
  idGroupement: string;
}
