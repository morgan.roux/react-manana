import gql from 'graphql-tag';

export const FONCTION_INFO = gql`
  fragment BasisFonctionInfo on Fonction {
    id
    libelle
    competenceTerritoriale
  }
`;
