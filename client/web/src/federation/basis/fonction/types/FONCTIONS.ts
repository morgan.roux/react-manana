/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, FonctionFilter, FonctionSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: FONCTIONS
// ====================================================

export interface FONCTIONS_fonctions_nodes {
  __typename: "Fonction";
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}

export interface FONCTIONS_fonctions {
  __typename: "FonctionConnection";
  /**
   * Array of nodes.
   */
  nodes: FONCTIONS_fonctions_nodes[];
}

export interface FONCTIONS {
  fonctions: FONCTIONS_fonctions;
}

export interface FONCTIONSVariables {
  paging?: OffsetPaging | null;
  filter?: FonctionFilter | null;
  sorting?: FonctionSort[] | null;
}
