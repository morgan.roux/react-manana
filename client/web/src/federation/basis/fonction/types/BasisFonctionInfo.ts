/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: BasisFonctionInfo
// ====================================================

export interface BasisFonctionInfo {
  __typename: "Fonction";
  id: string;
  libelle: string;
  competenceTerritoriale: boolean;
}
