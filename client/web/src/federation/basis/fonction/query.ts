import gql from 'graphql-tag';
import { FONCTION_INFO } from './fragment';

export const FONCTIONS = gql`
  query FONCTIONS($paging: OffsetPaging, $filter: FonctionFilter, $sorting: [FonctionSort!]) {
    fonctions(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...BasisFonctionInfo
      }
    }
  }
  ${FONCTION_INFO}
`;
