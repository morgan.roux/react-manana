import gql from 'graphql-tag';

export const ORIGINE_INFO = gql`
fragment origineInfo on Origine {
    id
    code
    libelle
    createdAt
    updatedAt
}
`;
