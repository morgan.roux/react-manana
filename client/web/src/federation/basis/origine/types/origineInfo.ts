/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: origineInfo
// ====================================================

export interface origineInfo {
  __typename: "Origine";
  id: string;
  code: string;
  libelle: string;
  createdAt: any;
  updatedAt: any;
}
