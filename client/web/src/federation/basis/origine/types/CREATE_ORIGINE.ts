/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreateOneOrigineInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_ORIGINE
// ====================================================

export interface CREATE_ORIGINE_createOneOrigine {
  __typename: "Origine";
  id: string;
  code: string;
  libelle: string;
  createdAt: any;
  updatedAt: any;
}

export interface CREATE_ORIGINE {
  createOneOrigine: CREATE_ORIGINE_createOneOrigine;
}

export interface CREATE_ORIGINEVariables {
  input: CreateOneOrigineInput;
}
