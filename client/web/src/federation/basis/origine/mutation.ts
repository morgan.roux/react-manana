import gql from 'graphql-tag';
import { ORIGINE_INFO } from './fragment';

export const CREATE_ORIGINE = gql`
mutation CREATE_ORIGINE($input: CreateOneOrigineInput!) {
    createOneOrigine(input: $input) {
      ...origineInfo
    }
} 
${ORIGINE_INFO}
`;