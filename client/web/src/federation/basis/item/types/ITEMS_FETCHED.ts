/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ITEMS_FETCHED
// ====================================================

export interface ITEMS_FETCHED_itemsFetched {
  __typename: "Item";
  id: string;
  code: string;
  name: string;
}

export interface ITEMS_FETCHED {
  itemsFetched: ITEMS_FETCHED_itemsFetched[] | null;
}
