/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, ItemFilter, ItemSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: ITEMS
// ====================================================

export interface ITEMS_items_nodes {
  __typename: "Item";
  id: string;
  code: string;
  codeItem: string;
  name: string;
  createdAt: any;
  updatedAt: any;
  active: boolean | null;
}

export interface ITEMS_items {
  __typename: "ItemConnection";
  /**
   * Array of nodes.
   */
  nodes: ITEMS_items_nodes[];
}

export interface ITEMS {
  items: ITEMS_items;
}

export interface ITEMSVariables {
  paging?: OffsetPaging | null;
  filter?: ItemFilter | null;
  sorting?: ItemSort[] | null;
  idPharmacie?: string | null;
}
