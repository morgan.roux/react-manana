/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ItemAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: ITEM_AGGREGATE
// ====================================================

export interface ITEM_AGGREGATE_itemAggregate_count {
  __typename: "ItemCountAggregate";
  id: number | null;
}

export interface ITEM_AGGREGATE_itemAggregate {
  __typename: "ItemAggregateResponse";
  count: ITEM_AGGREGATE_itemAggregate_count | null;
}

export interface ITEM_AGGREGATE {
  itemAggregate: ITEM_AGGREGATE_itemAggregate[];
}

export interface ITEM_AGGREGATEVariables {
  filter?: ItemAggregateFilter | null;
}
