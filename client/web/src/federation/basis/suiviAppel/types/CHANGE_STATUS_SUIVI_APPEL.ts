/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CHANGE_STATUS_SUIVI_APPEL
// ====================================================

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_interlocuteur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_interlocuteur_role {
  __typename: "Role";
  id: string;
  dataType: string;
  code: string | null;
  nom: string | null;
}

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_interlocuteur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_interlocuteur_photo | null;
  phoneNumber: string | null;
  role: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_interlocuteur_role | null;
}

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_declarant_photo | null;
  phoneNumber: string | null;
}

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_concernes_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_concernes {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_concernes_photo | null;
  phoneNumber: string | null;
}

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel {
  __typename: "SuiviAppelType";
  id: string;
  dataType: string;
  idUserDeclarant: string;
  idUserInterlocuteur: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idUrgence: string;
  commentaire: string | null;
  idFichiers: string[];
  idGroupement: string;
  status: string | null;
  idPharmacie: string;
  createdAt: any;
  updatedAt: any;
  fonction: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_fonction | null;
  tache: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_tache | null;
  interlocuteur: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_interlocuteur;
  declarant: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_declarant;
  fichiers: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_fichiers[] | null;
  concernes: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_concernes[] | null;
  urgence: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_urgence;
  importance: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel_importance;
}

export interface CHANGE_STATUS_SUIVI_APPEL {
  changeStatusSuiviAppel: CHANGE_STATUS_SUIVI_APPEL_changeStatusSuiviAppel;
}

export interface CHANGE_STATUS_SUIVI_APPELVariables {
  status: string;
  id: string;
}
