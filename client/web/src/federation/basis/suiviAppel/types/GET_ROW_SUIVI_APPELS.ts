/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SuiviAppelTypeAggregateFilter } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ROW_SUIVI_APPELS
// ====================================================

export interface GET_ROW_SUIVI_APPELS_suiviAppelTypeAggregate_count {
  __typename: "SuiviAppelTypeCountAggregate";
  id: number | null;
}

export interface GET_ROW_SUIVI_APPELS_suiviAppelTypeAggregate {
  __typename: "SuiviAppelTypeAggregateResponse";
  count: GET_ROW_SUIVI_APPELS_suiviAppelTypeAggregate_count | null;
}

export interface GET_ROW_SUIVI_APPELS {
  suiviAppelTypeAggregate: GET_ROW_SUIVI_APPELS_suiviAppelTypeAggregate[];
}

export interface GET_ROW_SUIVI_APPELSVariables {
  filter?: SuiviAppelTypeAggregateFilter | null;
}
