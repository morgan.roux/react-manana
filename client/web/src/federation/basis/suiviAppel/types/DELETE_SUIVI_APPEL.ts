/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_SUIVI_APPEL
// ====================================================

export interface DELETE_SUIVI_APPEL {
  deleteOneSuiviAppel: string;
}

export interface DELETE_SUIVI_APPELVariables {
  id: string;
}
