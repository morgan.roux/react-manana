/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SuiviAppelInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_SUIVI_APPEL
// ====================================================

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel_interlocuteur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel_interlocuteur_role {
  __typename: "Role";
  id: string;
  dataType: string;
  code: string | null;
  nom: string | null;
}

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel_interlocuteur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_SUIVI_APPEL_updateOneSuiviAppel_interlocuteur_photo | null;
  phoneNumber: string | null;
  role: UPDATE_SUIVI_APPEL_updateOneSuiviAppel_interlocuteur_role | null;
}

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_SUIVI_APPEL_updateOneSuiviAppel_declarant_photo | null;
  phoneNumber: string | null;
}

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel_concernes_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel_concernes {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_SUIVI_APPEL_updateOneSuiviAppel_concernes_photo | null;
  phoneNumber: string | null;
}

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_SUIVI_APPEL_updateOneSuiviAppel {
  __typename: "SuiviAppelType";
  id: string;
  dataType: string;
  idUserDeclarant: string;
  idUserInterlocuteur: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idUrgence: string;
  commentaire: string | null;
  idFichiers: string[];
  idGroupement: string;
  status: string | null;
  idPharmacie: string;
  createdAt: any;
  updatedAt: any;
  fonction: UPDATE_SUIVI_APPEL_updateOneSuiviAppel_fonction | null;
  tache: UPDATE_SUIVI_APPEL_updateOneSuiviAppel_tache | null;
  interlocuteur: UPDATE_SUIVI_APPEL_updateOneSuiviAppel_interlocuteur;
  declarant: UPDATE_SUIVI_APPEL_updateOneSuiviAppel_declarant;
  fichiers: UPDATE_SUIVI_APPEL_updateOneSuiviAppel_fichiers[] | null;
  concernes: UPDATE_SUIVI_APPEL_updateOneSuiviAppel_concernes[] | null;
  urgence: UPDATE_SUIVI_APPEL_updateOneSuiviAppel_urgence;
  importance: UPDATE_SUIVI_APPEL_updateOneSuiviAppel_importance;
}

export interface UPDATE_SUIVI_APPEL {
  updateOneSuiviAppel: UPDATE_SUIVI_APPEL_updateOneSuiviAppel;
}

export interface UPDATE_SUIVI_APPELVariables {
  input: SuiviAppelInput;
  id: string;
}
