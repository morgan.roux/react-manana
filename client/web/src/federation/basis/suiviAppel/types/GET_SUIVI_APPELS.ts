/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, SuiviAppelTypeFilter, SuiviAppelTypeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_SUIVI_APPELS
// ====================================================

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes_interlocuteur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes_interlocuteur_role {
  __typename: "Role";
  id: string;
  dataType: string;
  code: string | null;
  nom: string | null;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes_interlocuteur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_SUIVI_APPELS_suiviAppelTypes_nodes_interlocuteur_photo | null;
  phoneNumber: string | null;
  role: GET_SUIVI_APPELS_suiviAppelTypes_nodes_interlocuteur_role | null;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_SUIVI_APPELS_suiviAppelTypes_nodes_declarant_photo | null;
  phoneNumber: string | null;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes_concernes_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes_concernes {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_SUIVI_APPELS_suiviAppelTypes_nodes_concernes_photo | null;
  phoneNumber: string | null;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes_nodes {
  __typename: "SuiviAppelType";
  id: string;
  dataType: string;
  idUserDeclarant: string;
  idUserInterlocuteur: string;
  idFonction: string | null;
  idTache: string | null;
  idImportance: string;
  idUrgence: string;
  commentaire: string | null;
  idFichiers: string[];
  idGroupement: string;
  status: string | null;
  idPharmacie: string;
  createdAt: any;
  updatedAt: any;
  fonction: GET_SUIVI_APPELS_suiviAppelTypes_nodes_fonction | null;
  tache: GET_SUIVI_APPELS_suiviAppelTypes_nodes_tache | null;
  interlocuteur: GET_SUIVI_APPELS_suiviAppelTypes_nodes_interlocuteur;
  declarant: GET_SUIVI_APPELS_suiviAppelTypes_nodes_declarant;
  fichiers: GET_SUIVI_APPELS_suiviAppelTypes_nodes_fichiers[] | null;
  concernes: GET_SUIVI_APPELS_suiviAppelTypes_nodes_concernes[] | null;
  urgence: GET_SUIVI_APPELS_suiviAppelTypes_nodes_urgence;
  importance: GET_SUIVI_APPELS_suiviAppelTypes_nodes_importance;
}

export interface GET_SUIVI_APPELS_suiviAppelTypes {
  __typename: "SuiviAppelTypeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_SUIVI_APPELS_suiviAppelTypes_nodes[];
}

export interface GET_SUIVI_APPELS {
  suiviAppelTypes: GET_SUIVI_APPELS_suiviAppelTypes;
}

export interface GET_SUIVI_APPELSVariables {
  paging?: OffsetPaging | null;
  filter?: SuiviAppelTypeFilter | null;
  sorting?: SuiviAppelTypeSort[] | null;
}
