import gql from 'graphql-tag';
import { FULL_SUIVI_APPEL } from './fragment';

export const GET_SUIVI_APPELS = gql`
  query GET_SUIVI_APPELS(
    $paging: OffsetPaging
    $filter: SuiviAppelTypeFilter
    $sorting: [SuiviAppelTypeSort!]
  ) {
    suiviAppelTypes(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...suiviAppelInfo
      }
    }
  }

  ${FULL_SUIVI_APPEL}
`;

export const GET_ROW_SUIVI_APPELS = gql`
  query GET_ROW_SUIVI_APPELS($filter: SuiviAppelTypeAggregateFilter) {
    suiviAppelTypeAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_SUIVI_APPELS_BY_INTERLOCUTEUR = gql`
  query GET_SUIVI_APPELS_BY_INTERLOCUTEUR($idUserInterlocuteur: String!) {
    getSuivisAppelsByIdInterlocuteur(idUserInterlocuteur: $idUserInterlocuteur) {
      ...suiviAppelInfo
    }
  }
  ${FULL_SUIVI_APPEL}
`;
