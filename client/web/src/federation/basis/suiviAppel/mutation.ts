import gql from 'graphql-tag';
import { FULL_SUIVI_APPEL } from './fragment';

export const CREATE_SUIVI_APPEL = gql`
  mutation CREATE_SUIVI_APPEL($input: SuiviAppelInput!) {
    createOneSuiviAppel(input: $input) {
      ...suiviAppelInfo
    }
  }
  ${FULL_SUIVI_APPEL}
`;

export const UPDATE_SUIVI_APPEL = gql`
  mutation UPDATE_SUIVI_APPEL($input: SuiviAppelInput!, $id: String!) {
    updateOneSuiviAppel(input: $input, id: $id) {
      ...suiviAppelInfo
    }
  }
  ${FULL_SUIVI_APPEL}
`;

export const DELETE_SUIVI_APPEL = gql`
  mutation DELETE_SUIVI_APPEL($id: String!) {
    deleteOneSuiviAppel(id: $id)
  }
`;

export const CHANGE_STATUS_SUIVI_APPEL = gql`
  mutation CHANGE_STATUS_SUIVI_APPEL($status: String!, $id: String!) {
    changeStatusSuiviAppel(status: $status, id: $id) {
      ...suiviAppelInfo
    }
  }
  ${FULL_SUIVI_APPEL}
`;
