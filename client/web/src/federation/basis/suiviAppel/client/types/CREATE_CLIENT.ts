/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ClientInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_CLIENT
// ====================================================

export interface CREATE_CLIENT_createOneClient_user_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface CREATE_CLIENT_createOneClient_user {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: CREATE_CLIENT_createOneClient_user_role | null;
  phoneNumber: string | null;
}

export interface CREATE_CLIENT_createOneClient_contact {
  __typename: "ContactType";
  id: string;
  adresse1: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
}

export interface CREATE_CLIENT_createOneClient {
  __typename: "ClientType";
  id: string;
  idUser: string;
  idContact: string;
  idGroupement: string;
  idPharmacie: string;
  createdAt: any;
  updatedAt: any;
  user: CREATE_CLIENT_createOneClient_user;
  contact: CREATE_CLIENT_createOneClient_contact;
}

export interface CREATE_CLIENT {
  createOneClient: CREATE_CLIENT_createOneClient;
}

export interface CREATE_CLIENTVariables {
  input: ClientInput;
}
