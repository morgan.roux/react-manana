import gql from 'graphql-tag';

export const FULL_SUIVI_APPEL = gql`
  fragment suiviAppelInfo on SuiviAppelType {
    id
    dataType
    idUserDeclarant
    idUserInterlocuteur
    idFonction
    idTache
    idImportance
    idUrgence
    commentaire
    idFichiers
    idGroupement
    status
    idPharmacie
    createdAt
    updatedAt
    fonction {
      id
      libelle
    }
    tache {
      id
      libelle
    }
    interlocuteur {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
      phoneNumber
      role {
        id
        dataType
        code
        nom
      }
    }
    declarant {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
      phoneNumber
    }
    fichiers {
      id
      chemin
      nomOriginal
      type
      publicUrl
    }
    concernes {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
      phoneNumber
    }
    urgence {
      id
      code
      libelle
      couleur
    }
    importance {
      id
      ordre
      libelle
      couleur
    }
  }
`;
