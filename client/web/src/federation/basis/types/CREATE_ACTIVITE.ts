/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActiviteInput } from "./../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_ACTIVITE
// ====================================================

export interface CREATE_ACTIVITE_createOneActivite_user {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface CREATE_ACTIVITE_createOneActivite_type {
  __typename: "ActiviteType";
  id: string;
  code: string;
  nom: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface CREATE_ACTIVITE_createOneActivite {
  __typename: "Activite";
  id: string;
  idActiviteType: string;
  idItem: string;
  idItemAssocie: string;
  idUser: string | null;
  log: string | null;
  createdAt: any;
  updatedAt: any;
  user: CREATE_ACTIVITE_createOneActivite_user;
  type: CREATE_ACTIVITE_createOneActivite_type;
}

export interface CREATE_ACTIVITE {
  createOneActivite: CREATE_ACTIVITE_createOneActivite;
}

export interface CREATE_ACTIVITEVariables {
  input: ActiviteInput;
}
