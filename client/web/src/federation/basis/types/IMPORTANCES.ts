/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, ImportanceFilter, ImportanceSort } from "./../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: IMPORTANCES
// ====================================================

export interface IMPORTANCES_importances_nodes {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface IMPORTANCES_importances {
  __typename: "ImportanceConnection";
  /**
   * Array of nodes.
   */
  nodes: IMPORTANCES_importances_nodes[];
}

export interface IMPORTANCES {
  importances: IMPORTANCES_importances;
}

export interface IMPORTANCESVariables {
  paging?: OffsetPaging | null;
  filter?: ImportanceFilter | null;
  sorting?: ImportanceSort[] | null;
}
