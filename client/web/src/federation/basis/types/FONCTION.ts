/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FONCTION
// ====================================================

export interface FONCTION_dQMTFonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface FONCTION {
  dQMTFonction: FONCTION_dQMTFonction | null;
}

export interface FONCTIONVariables {
  id: string;
}
