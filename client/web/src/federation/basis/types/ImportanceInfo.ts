/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ImportanceInfo
// ====================================================

export interface ImportanceInfo {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}
