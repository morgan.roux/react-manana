/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, UrgenceFilter, UrgenceSort } from "./../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_URGENCES
// ====================================================

export interface GET_URGENCES_urgences_nodes {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_URGENCES_urgences {
  __typename: "UrgenceConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_URGENCES_urgences_nodes[];
}

export interface GET_URGENCES {
  urgences: GET_URGENCES_urgences;
}

export interface GET_URGENCESVariables {
  paging?: OffsetPaging | null;
  filter?: UrgenceFilter | null;
  sorting?: UrgenceSort[] | null;
}
