import gql from 'graphql-tag';
import { FICHE_INCIDENT_INFO, SOLUTION_INFO } from './fragment';

export const GET_FICHE_INCIDENTS = gql`
  query GET_FICHE_INCIDENTS(
    $paging: OffsetPaging
    $filter: DQFicheIncidentFilter
    $sorting: [DQFicheIncidentSort!]
  ) {
    dQFicheIncidents(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...FicheIncidentInfo
      }
    }
  }

  ${FICHE_INCIDENT_INFO}
`;

export const GET_ENCOURS_FICHE_INCIDENTS = gql`
  query GET_ENCOURS_FICHE_INCIDENTS {
    dQEncoursFicheIncidents {
      ...FicheIncidentInfo
    }
  }

  ${FICHE_INCIDENT_INFO}
`;

export const GET_FICHE_INCIDENT_AGGREGATE = gql`
  query GET_FICHE_INCIDENT_AGGREGATE($filter: DQFicheIncidentAggregateFilter) {
    dQFicheIncidentAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_FICHE_INCIDENT = gql`
  query GET_FICHE_INCIDENT($id: ID!) {
    dQFicheIncident(id: $id) {
      ...FicheIncidentInfo
    }
  }
  ${FICHE_INCIDENT_INFO}
`;

export const GET_SOLUTION = gql`
  query GET_SOLUTION($id: ID!) {
    dQSolution(id: $id) {
      ...SolutionInfo
    }
  }

  ${SOLUTION_INFO}
`;
