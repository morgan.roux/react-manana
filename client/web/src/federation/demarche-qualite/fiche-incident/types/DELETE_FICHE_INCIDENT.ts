/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneDQFicheIncidentInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_FICHE_INCIDENT
// ====================================================

export interface DELETE_FICHE_INCIDENT_deleteOneDQFicheIncident {
  __typename: "DQFicheIncidentDeleteResponse";
  id: string | null;
  description: string | null;
}

export interface DELETE_FICHE_INCIDENT {
  deleteOneDQFicheIncident: DELETE_FICHE_INCIDENT_deleteOneDQFicheIncident;
}

export interface DELETE_FICHE_INCIDENTVariables {
  input: DeleteOneDQFicheIncidentInput;
}
