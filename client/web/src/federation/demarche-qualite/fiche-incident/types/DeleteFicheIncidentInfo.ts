/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: DeleteFicheIncidentInfo
// ====================================================

export interface DeleteFicheIncidentInfo {
  __typename: "DQFicheIncidentDeleteResponse";
  id: string | null;
  description: string | null;
}
