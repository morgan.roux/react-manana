/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQFicheIncidentInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_FICHE_INCIDENT
// ====================================================

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_declarant_photo | null;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_statut {
  __typename: "DQStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_origineAssocie_User_photo | null;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type CREATE_FICHE_INCIDENT_createOneDQFicheIncident_origineAssocie = CREATE_FICHE_INCIDENT_createOneDQFicheIncident_origineAssocie_User | CREATE_FICHE_INCIDENT_createOneDQFicheIncident_origineAssocie_Laboratoire | CREATE_FICHE_INCIDENT_createOneDQFicheIncident_origineAssocie_PrestataireService;

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_origine {
  __typename: "Origine";
  id: string;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_participants_photo | null;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_participantsAInformer_photo | null;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface CREATE_FICHE_INCIDENT_createOneDQFicheIncident {
  __typename: "DQFicheIncident";
  id: string;
  description: string;
  idFicheAmelioration: string | null;
  idTicketReclamation: string | null;
  idSolution: string | null;
  idUserDeclarant: string;
  idType: string | null;
  idCause: string | null;
  dateIncident: any | null;
  dateEcheance: any | null;
  idOrigine: string;
  idOrigineAssocie: string | null;
  createdAt: any;
  updatedAt: any;
  isPrivate: boolean;
  declarant: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_declarant;
  statut: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_statut;
  origineAssocie: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_origineAssocie | null;
  origine: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_origine;
  urgence: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_urgence;
  importance: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_importance;
  fonction: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_fonction | null;
  tache: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_tache | null;
  fonctionAInformer: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_fonctionAInformer | null;
  tacheAInformer: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_tacheAInformer | null;
  type: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_type | null;
  cause: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_cause | null;
  participants: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_participants[];
  participantsAInformer: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_participantsAInformer[];
  reunion: CREATE_FICHE_INCIDENT_createOneDQFicheIncident_reunion | null;
}

export interface CREATE_FICHE_INCIDENT {
  createOneDQFicheIncident: CREATE_FICHE_INCIDENT_createOneDQFicheIncident;
}

export interface CREATE_FICHE_INCIDENTVariables {
  input: DQFicheIncidentInput;
}
