/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SolutionInfo
// ====================================================

export interface SolutionInfo_user_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface SolutionInfo_user {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: SolutionInfo_user_photo | null;
}

export interface SolutionInfo {
  __typename: "DQSolution";
  id: string;
  description: string;
  user: SolutionInfo_user;
  createdAt: any;
}
