/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQFicheIncidentInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_FICHE_INCIDENT
// ====================================================

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_declarant_photo | null;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_statut {
  __typename: "DQStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_origineAssocie_User_photo | null;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_origineAssocie = UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_origineAssocie_User | UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_origineAssocie_Laboratoire | UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_origineAssocie_PrestataireService;

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_origine {
  __typename: "Origine";
  id: string;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_participants_photo | null;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_participantsAInformer_photo | null;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident {
  __typename: "DQFicheIncident";
  id: string;
  description: string;
  idFicheAmelioration: string | null;
  idTicketReclamation: string | null;
  idSolution: string | null;
  idUserDeclarant: string;
  idType: string | null;
  idCause: string | null;
  dateIncident: any | null;
  dateEcheance: any | null;
  idOrigine: string;
  idOrigineAssocie: string | null;
  createdAt: any;
  updatedAt: any;
  isPrivate: boolean;
  declarant: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_declarant;
  statut: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_statut;
  origineAssocie: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_origineAssocie | null;
  origine: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_origine;
  urgence: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_urgence;
  importance: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_importance;
  fonction: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_fonction | null;
  tache: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_tache | null;
  fonctionAInformer: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_fonctionAInformer | null;
  tacheAInformer: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_tacheAInformer | null;
  type: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_type | null;
  cause: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_cause | null;
  participants: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_participants[];
  participantsAInformer: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_participantsAInformer[];
  reunion: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident_reunion | null;
}

export interface UPDATE_FICHE_INCIDENT {
  updateOneDQFicheIncident: UPDATE_FICHE_INCIDENT_updateOneDQFicheIncident;
}

export interface UPDATE_FICHE_INCIDENTVariables {
  id: string;
  update: DQFicheIncidentInput;
}
