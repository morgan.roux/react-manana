/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_ENCOURS_FICHE_INCIDENTS
// ====================================================

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_declarant_photo | null;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_statut {
  __typename: "DQStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_origineAssocie_User_photo | null;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_origineAssocie = GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_origineAssocie_User | GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_origineAssocie_Laboratoire | GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_origineAssocie_PrestataireService;

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_origine {
  __typename: "Origine";
  id: string;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_participants_photo | null;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_participantsAInformer_photo | null;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents {
  __typename: "DQFicheIncident";
  id: string;
  description: string;
  idFicheAmelioration: string | null;
  idTicketReclamation: string | null;
  idSolution: string | null;
  idUserDeclarant: string;
  idType: string | null;
  idCause: string | null;
  dateIncident: any | null;
  dateEcheance: any | null;
  idOrigine: string;
  idOrigineAssocie: string | null;
  createdAt: any;
  updatedAt: any;
  isPrivate: boolean;
  declarant: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_declarant;
  statut: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_statut;
  origineAssocie: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_origineAssocie | null;
  origine: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_origine;
  urgence: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_urgence;
  importance: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_importance;
  fonction: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_fonction | null;
  tache: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_tache | null;
  fonctionAInformer: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_fonctionAInformer | null;
  tacheAInformer: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_tacheAInformer | null;
  type: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_type | null;
  cause: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_cause | null;
  participants: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_participants[];
  participantsAInformer: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_participantsAInformer[];
  reunion: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents_reunion | null;
}

export interface GET_ENCOURS_FICHE_INCIDENTS {
  dQEncoursFicheIncidents: GET_ENCOURS_FICHE_INCIDENTS_dQEncoursFicheIncidents[];
}
