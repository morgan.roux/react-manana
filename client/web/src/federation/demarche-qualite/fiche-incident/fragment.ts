import gql from 'graphql-tag';

export const FICHE_INCIDENT_INFO = gql`
  fragment FicheIncidentInfo on DQFicheIncident {
    id
    description
    idFicheAmelioration
    idTicketReclamation
    idSolution
    idUserDeclarant
    idType
    idCause
    dateIncident
    dateEcheance
    idOrigine
    idOrigineAssocie
    createdAt
    updatedAt
    isPrivate
    declarant {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }
    statut {
      id
      code
      libelle
    }
    origineAssocie {
      ... on User {
        id
        dataType
        fullName
        photo {
          id
          publicUrl
          chemin
          nomOriginal
          type
        }
      }
      ... on Laboratoire {
        id
        dataType
        nom
      }
      ... on PrestataireService {
        id
        dataType
        nom
      }
    }
    origine {
      id
    }
    urgence {
      id
      code
      libelle
      couleur
    }
    importance {
      id
      ordre
      libelle
      couleur
    }
    fonction {
      id
      libelle
    }
    tache {
      id
      libelle
    }
    fonctionAInformer {
      id
      libelle
    }
    tacheAInformer {
      id
      libelle
    }
    type {
      id
      libelle
    }
    cause {
      id
      libelle
    }
    participants {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }
    participantsAInformer {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }
    reunion {
      id
    }
  }
`;

export const DELETE_FICHE_INCIDENT_INFO = gql`
  fragment DeleteFicheIncidentInfo on DQFicheIncidentDeleteResponse {
    id
    description
  }
`;

export const SOLUTION_INFO = gql`
  fragment SolutionInfo on DQSolution {
    id
    description
    user {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }
    createdAt
  }
`;
