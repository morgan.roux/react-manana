import gql from 'graphql-tag';
import { FULL_REUNION_INFO, MINIMAL_REUNION_INFO, FULL_REUNION_ACTION_INFO } from './fragment';

export const GET_REUNIONS = gql`
  query GET_REUNIONS($paging: OffsetPaging, $filter: DQReunionFilter, $sorting: [DQReunionSort!]) {
    dQReunions(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...ReunionInfo
      }
    }
  }

  ${FULL_REUNION_INFO}
`;

export const GET_REUNION_AGGREGATE = gql`
  query GET_REUNION_AGGREGATE($filter: DQReunionAggregateFilter) {
    dQReunionAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_REUNION = gql`
  query GET_REUNION($id: ID!) {
    dQReunion(id: $id) {
      ...ReunionInfo
    }
  }

  ${FULL_REUNION_INFO}
`;

export const GET_REUNION_ACTIONS = gql`
  query GET_REUNION_ACTIONS(
    $paging: OffsetPaging
    $filter: DQReunionActionFilter
    $sorting: [DQReunionActionSort!]
  ) {
    dQReunionActions(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...ReunionActionInfo
      }
    }
  }

  ${FULL_REUNION_ACTION_INFO}
`;

export const GET_REUNION_ACTION_AGGREGATE = gql`
  query GET_REUNION_ACTION_AGGREGATE($filter: DQReunionActionAggregateFilter) {
    dQReunionActionAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;
