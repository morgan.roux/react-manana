/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ReunionActionInfo
// ====================================================

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: ReunionActionInfo_typeAssocie_DQFicheAmelioration_origineAssocie_User_photo | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type ReunionActionInfo_typeAssocie_DQFicheAmelioration_origineAssocie = ReunionActionInfo_typeAssocie_DQFicheAmelioration_origineAssocie_User | ReunionActionInfo_typeAssocie_DQFicheAmelioration_origineAssocie_Laboratoire | ReunionActionInfo_typeAssocie_DQFicheAmelioration_origineAssocie_PrestataireService;

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_origine {
  __typename: "Origine";
  id: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: ReunionActionInfo_typeAssocie_DQFicheAmelioration_participants_photo | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: ReunionActionInfo_typeAssocie_DQFicheAmelioration_participantsAInformer_photo | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheAmelioration {
  __typename: "DQFicheAmelioration";
  id: string;
  description: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAmelioration: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  idAction: string | null;
  createdAt: any;
  updatedAt: any;
  origineAssocie: ReunionActionInfo_typeAssocie_DQFicheAmelioration_origineAssocie | null;
  statut: ReunionActionInfo_typeAssocie_DQFicheAmelioration_statut;
  cause: ReunionActionInfo_typeAssocie_DQFicheAmelioration_cause | null;
  type: ReunionActionInfo_typeAssocie_DQFicheAmelioration_type | null;
  fichiers: ReunionActionInfo_typeAssocie_DQFicheAmelioration_fichiers[];
  auteur: ReunionActionInfo_typeAssocie_DQFicheAmelioration_auteur;
  origine: ReunionActionInfo_typeAssocie_DQFicheAmelioration_origine;
  urgence: ReunionActionInfo_typeAssocie_DQFicheAmelioration_urgence;
  importance: ReunionActionInfo_typeAssocie_DQFicheAmelioration_importance;
  fonction: ReunionActionInfo_typeAssocie_DQFicheAmelioration_fonction | null;
  tache: ReunionActionInfo_typeAssocie_DQFicheAmelioration_tache | null;
  fonctionAInformer: ReunionActionInfo_typeAssocie_DQFicheAmelioration_fonctionAInformer | null;
  tacheAInformer: ReunionActionInfo_typeAssocie_DQFicheAmelioration_tacheAInformer | null;
  participants: ReunionActionInfo_typeAssocie_DQFicheAmelioration_participants[];
  participantsAInformer: ReunionActionInfo_typeAssocie_DQFicheAmelioration_participantsAInformer[];
  reunion: ReunionActionInfo_typeAssocie_DQFicheAmelioration_reunion | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_declarant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_declarant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: ReunionActionInfo_typeAssocie_DQFicheIncident_declarant_photo | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_statut {
  __typename: "DQStatut";
  id: string;
  code: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: ReunionActionInfo_typeAssocie_DQFicheIncident_origineAssocie_User_photo | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type ReunionActionInfo_typeAssocie_DQFicheIncident_origineAssocie = ReunionActionInfo_typeAssocie_DQFicheIncident_origineAssocie_User | ReunionActionInfo_typeAssocie_DQFicheIncident_origineAssocie_Laboratoire | ReunionActionInfo_typeAssocie_DQFicheIncident_origineAssocie_PrestataireService;

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_origine {
  __typename: "Origine";
  id: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: ReunionActionInfo_typeAssocie_DQFicheIncident_participants_photo | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: ReunionActionInfo_typeAssocie_DQFicheIncident_participantsAInformer_photo | null;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface ReunionActionInfo_typeAssocie_DQFicheIncident {
  __typename: "DQFicheIncident";
  id: string;
  description: string;
  idFicheAmelioration: string | null;
  idTicketReclamation: string | null;
  idSolution: string | null;
  idUserDeclarant: string;
  idType: string | null;
  idCause: string | null;
  dateIncident: any | null;
  dateEcheance: any | null;
  idOrigine: string;
  idOrigineAssocie: string | null;
  createdAt: any;
  updatedAt: any;
  isPrivate: boolean;
  declarant: ReunionActionInfo_typeAssocie_DQFicheIncident_declarant;
  statut: ReunionActionInfo_typeAssocie_DQFicheIncident_statut;
  origineAssocie: ReunionActionInfo_typeAssocie_DQFicheIncident_origineAssocie | null;
  origine: ReunionActionInfo_typeAssocie_DQFicheIncident_origine;
  urgence: ReunionActionInfo_typeAssocie_DQFicheIncident_urgence;
  importance: ReunionActionInfo_typeAssocie_DQFicheIncident_importance;
  fonction: ReunionActionInfo_typeAssocie_DQFicheIncident_fonction | null;
  tache: ReunionActionInfo_typeAssocie_DQFicheIncident_tache | null;
  fonctionAInformer: ReunionActionInfo_typeAssocie_DQFicheIncident_fonctionAInformer | null;
  tacheAInformer: ReunionActionInfo_typeAssocie_DQFicheIncident_tacheAInformer | null;
  type: ReunionActionInfo_typeAssocie_DQFicheIncident_type | null;
  cause: ReunionActionInfo_typeAssocie_DQFicheIncident_cause | null;
  participants: ReunionActionInfo_typeAssocie_DQFicheIncident_participants[];
  participantsAInformer: ReunionActionInfo_typeAssocie_DQFicheIncident_participantsAInformer[];
  reunion: ReunionActionInfo_typeAssocie_DQFicheIncident_reunion | null;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: ReunionActionInfo_typeAssocie_DQActionOperationnelle_origineAssocie_User_photo | null;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type ReunionActionInfo_typeAssocie_DQActionOperationnelle_origineAssocie = ReunionActionInfo_typeAssocie_DQActionOperationnelle_origineAssocie_User | ReunionActionInfo_typeAssocie_DQActionOperationnelle_origineAssocie_Laboratoire | ReunionActionInfo_typeAssocie_DQActionOperationnelle_origineAssocie_PrestataireService;

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_fichiers {
  __typename: "Fichier";
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_suiviPar {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_origine {
  __typename: "Origine";
  id: string;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: ReunionActionInfo_typeAssocie_DQActionOperationnelle_participants_photo | null;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: ReunionActionInfo_typeAssocie_DQActionOperationnelle_participantsAInformer_photo | null;
}

export interface ReunionActionInfo_typeAssocie_DQActionOperationnelle {
  __typename: "DQActionOperationnelle";
  id: string;
  description: string;
  statut: ReunionActionInfo_typeAssocie_DQActionOperationnelle_statut;
  idUserAuteur: string;
  idUserSuivi: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAction: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  origineAssocie: ReunionActionInfo_typeAssocie_DQActionOperationnelle_origineAssocie | null;
  fichiers: ReunionActionInfo_typeAssocie_DQActionOperationnelle_fichiers[];
  suiviPar: ReunionActionInfo_typeAssocie_DQActionOperationnelle_suiviPar;
  auteur: ReunionActionInfo_typeAssocie_DQActionOperationnelle_auteur;
  origine: ReunionActionInfo_typeAssocie_DQActionOperationnelle_origine;
  type: ReunionActionInfo_typeAssocie_DQActionOperationnelle_type | null;
  cause: ReunionActionInfo_typeAssocie_DQActionOperationnelle_cause | null;
  urgence: ReunionActionInfo_typeAssocie_DQActionOperationnelle_urgence;
  importance: ReunionActionInfo_typeAssocie_DQActionOperationnelle_importance;
  fonction: ReunionActionInfo_typeAssocie_DQActionOperationnelle_fonction | null;
  tache: ReunionActionInfo_typeAssocie_DQActionOperationnelle_tache | null;
  fonctionAInformer: ReunionActionInfo_typeAssocie_DQActionOperationnelle_fonctionAInformer | null;
  tacheAInformer: ReunionActionInfo_typeAssocie_DQActionOperationnelle_tacheAInformer | null;
  reunion: ReunionActionInfo_typeAssocie_DQActionOperationnelle_reunion | null;
  participants: ReunionActionInfo_typeAssocie_DQActionOperationnelle_participants[];
  participantsAInformer: ReunionActionInfo_typeAssocie_DQActionOperationnelle_participantsAInformer[];
}

export type ReunionActionInfo_typeAssocie = ReunionActionInfo_typeAssocie_DQFicheAmelioration | ReunionActionInfo_typeAssocie_DQFicheIncident | ReunionActionInfo_typeAssocie_DQActionOperationnelle;

export interface ReunionActionInfo {
  __typename: "DQReunionAction";
  id: string;
  type: string;
  idTodoAction: string | null;
  createdAt: any;
  updatedAt: any;
  idTypeAssocie: string;
  typeAssocie: ReunionActionInfo_typeAssocie | null;
}
