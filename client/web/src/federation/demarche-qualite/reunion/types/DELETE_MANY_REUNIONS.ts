/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteManyDQReunionsInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_MANY_REUNIONS
// ====================================================

export interface DELETE_MANY_REUNIONS_deleteManyDQReunions {
  __typename: "DeleteManyResponse";
  /**
   * The number of records deleted.
   */
  deletedCount: number;
}

export interface DELETE_MANY_REUNIONS {
  deleteManyDQReunions: DELETE_MANY_REUNIONS_deleteManyDQReunions;
}

export interface DELETE_MANY_REUNIONSVariables {
  input: DeleteManyDQReunionsInput;
}
