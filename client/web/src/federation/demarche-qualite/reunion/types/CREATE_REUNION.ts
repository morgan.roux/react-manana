/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQReunionInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_REUNION
// ====================================================

export interface CREATE_REUNION_createOneDQReunion_animateur_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface CREATE_REUNION_createOneDQReunion_animateur {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: CREATE_REUNION_createOneDQReunion_animateur_role | null;
}

export interface CREATE_REUNION_createOneDQReunion_responsable_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface CREATE_REUNION_createOneDQReunion_responsable {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: CREATE_REUNION_createOneDQReunion_responsable_role | null;
}

export interface CREATE_REUNION_createOneDQReunion_participants_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface CREATE_REUNION_createOneDQReunion_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: CREATE_REUNION_createOneDQReunion_participants_role | null;
}

export interface CREATE_REUNION_createOneDQReunion {
  __typename: "DQReunion";
  id: string;
  description: string;
  idUserAnimateur: string;
  idResponsable: string;
  createdAt: any;
  updatedAt: any;
  nombreTotalActions: number;
  nombreTotalClotureActions: number;
  animateur: CREATE_REUNION_createOneDQReunion_animateur;
  responsable: CREATE_REUNION_createOneDQReunion_responsable;
  participants: CREATE_REUNION_createOneDQReunion_participants[];
}

export interface CREATE_REUNION {
  createOneDQReunion: CREATE_REUNION_createOneDQReunion;
}

export interface CREATE_REUNIONVariables {
  input: DQReunionInput;
}
