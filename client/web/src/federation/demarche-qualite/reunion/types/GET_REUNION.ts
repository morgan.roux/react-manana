/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_REUNION
// ====================================================

export interface GET_REUNION_dQReunion_animateur_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface GET_REUNION_dQReunion_animateur {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: GET_REUNION_dQReunion_animateur_role | null;
}

export interface GET_REUNION_dQReunion_responsable_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface GET_REUNION_dQReunion_responsable {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: GET_REUNION_dQReunion_responsable_role | null;
}

export interface GET_REUNION_dQReunion_participants_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface GET_REUNION_dQReunion_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: GET_REUNION_dQReunion_participants_role | null;
}

export interface GET_REUNION_dQReunion {
  __typename: "DQReunion";
  id: string;
  description: string;
  idUserAnimateur: string;
  idResponsable: string;
  createdAt: any;
  updatedAt: any;
  nombreTotalActions: number;
  nombreTotalClotureActions: number;
  animateur: GET_REUNION_dQReunion_animateur;
  responsable: GET_REUNION_dQReunion_responsable;
  participants: GET_REUNION_dQReunion_participants[];
}

export interface GET_REUNION {
  dQReunion: GET_REUNION_dQReunion | null;
}

export interface GET_REUNIONVariables {
  id: string;
}
