/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQReunionFilter, DQReunionSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_REUNIONS
// ====================================================

export interface GET_REUNIONS_dQReunions_nodes_animateur_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface GET_REUNIONS_dQReunions_nodes_animateur {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: GET_REUNIONS_dQReunions_nodes_animateur_role | null;
}

export interface GET_REUNIONS_dQReunions_nodes_responsable_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface GET_REUNIONS_dQReunions_nodes_responsable {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: GET_REUNIONS_dQReunions_nodes_responsable_role | null;
}

export interface GET_REUNIONS_dQReunions_nodes_participants_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface GET_REUNIONS_dQReunions_nodes_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  email: string | null;
  role: GET_REUNIONS_dQReunions_nodes_participants_role | null;
}

export interface GET_REUNIONS_dQReunions_nodes {
  __typename: "DQReunion";
  id: string;
  description: string;
  idUserAnimateur: string;
  idResponsable: string;
  createdAt: any;
  updatedAt: any;
  nombreTotalActions: number;
  nombreTotalClotureActions: number;
  animateur: GET_REUNIONS_dQReunions_nodes_animateur;
  responsable: GET_REUNIONS_dQReunions_nodes_responsable;
  participants: GET_REUNIONS_dQReunions_nodes_participants[];
}

export interface GET_REUNIONS_dQReunions {
  __typename: "DQReunionConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_REUNIONS_dQReunions_nodes[];
}

export interface GET_REUNIONS {
  dQReunions: GET_REUNIONS_dQReunions;
}

export interface GET_REUNIONSVariables {
  paging?: OffsetPaging | null;
  filter?: DQReunionFilter | null;
  sorting?: DQReunionSort[] | null;
}
