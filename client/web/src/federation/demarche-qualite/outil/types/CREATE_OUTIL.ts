/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQOutilInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_OUTIL
// ====================================================

export interface CREATE_OUTIL_createDQOutil_DQAffiche {
  __typename: "DQAffiche";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface CREATE_OUTIL_createDQOutil_DQDocument {
  __typename: "DQDocument";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface CREATE_OUTIL_createDQOutil_DQEnregistrement {
  __typename: "DQEnregistrement";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface CREATE_OUTIL_createDQOutil_DQMemo {
  __typename: "DQMemo";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface CREATE_OUTIL_createDQOutil_DQProcedure {
  __typename: "DQProcedure";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export type CREATE_OUTIL_createDQOutil = CREATE_OUTIL_createDQOutil_DQAffiche | CREATE_OUTIL_createDQOutil_DQDocument | CREATE_OUTIL_createDQOutil_DQEnregistrement | CREATE_OUTIL_createDQOutil_DQMemo | CREATE_OUTIL_createDQOutil_DQProcedure;

export interface CREATE_OUTIL {
  createDQOutil: CREATE_OUTIL_createDQOutil | null;
}

export interface CREATE_OUTILVariables {
  typologie: string;
  outilInput: DQOutilInput;
}
