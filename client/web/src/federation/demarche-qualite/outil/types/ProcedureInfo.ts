/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ProcedureInfo
// ====================================================

export interface ProcedureInfo {
  __typename: "DQProcedure";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}
