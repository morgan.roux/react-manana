/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_OUTILS
// ====================================================

export interface GET_OUTILS_dqOutils_DQAffiche {
  __typename: "DQAffiche";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTILS_dqOutils_DQDocument {
  __typename: "DQDocument";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTILS_dqOutils_DQEnregistrement {
  __typename: "DQEnregistrement";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTILS_dqOutils_DQMemo {
  __typename: "DQMemo";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface GET_OUTILS_dqOutils_DQProcedure {
  __typename: "DQProcedure";
  id: string;
  typologie: string;
  code: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
}

export type GET_OUTILS_dqOutils = GET_OUTILS_dqOutils_DQAffiche | GET_OUTILS_dqOutils_DQDocument | GET_OUTILS_dqOutils_DQEnregistrement | GET_OUTILS_dqOutils_DQMemo | GET_OUTILS_dqOutils_DQProcedure;

export interface GET_OUTILS {
  dqOutils: (GET_OUTILS_dqOutils | null)[] | null;
}

export interface GET_OUTILSVariables {
  typologie: string;
}
