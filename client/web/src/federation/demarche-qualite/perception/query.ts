import gql from 'graphql-tag';
import { PERCEPTION_INFO } from './fragment';

export const GET_PERCEPTIONS = gql`
  query GET_PERCEPTIONS(
    $paging: OffsetPaging
    $filter: DQPerceptionFilter
    $sorting: [DQPerceptionSort!]
  ) {
    dQPerceptions(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...PerceptionInfo
      }
    }
  }

  ${PERCEPTION_INFO}
`;
