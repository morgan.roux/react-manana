/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PerceptionInfo
// ====================================================

export interface PerceptionInfo {
  __typename: "DQPerception";
  id: string;
  libelle: string;
  nombreFicheAmeliorations: number;
}
