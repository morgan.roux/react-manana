/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQPerceptionFilter, DQPerceptionSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_PERCEPTIONS
// ====================================================

export interface GET_PERCEPTIONS_dQPerceptions_nodes {
  __typename: "DQPerception";
  id: string;
  libelle: string;
  nombreFicheAmeliorations: number;
}

export interface GET_PERCEPTIONS_dQPerceptions {
  __typename: "DQPerceptionConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_PERCEPTIONS_dQPerceptions_nodes[];
}

export interface GET_PERCEPTIONS {
  dQPerceptions: GET_PERCEPTIONS_dQPerceptions;
}

export interface GET_PERCEPTIONSVariables {
  paging?: OffsetPaging | null;
  filter?: DQPerceptionFilter | null;
  sorting?: DQPerceptionSort[] | null;
}
