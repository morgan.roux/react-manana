/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CLOTURE_CHECKLIST_EVALUATIONS
// ====================================================

export interface CLOTURE_CHECKLIST_EVALUATIONS_dqClotureChecklistEvaluations_cloture {
  __typename: "DQChecklistEvaluationCloture";
  id: string;
}

export interface CLOTURE_CHECKLIST_EVALUATIONS_dqClotureChecklistEvaluations {
  __typename: "DQChecklistEvaluation";
  id: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
  cloture: CLOTURE_CHECKLIST_EVALUATIONS_dqClotureChecklistEvaluations_cloture | null;
}

export interface CLOTURE_CHECKLIST_EVALUATIONS {
  dqClotureChecklistEvaluations: (CLOTURE_CHECKLIST_EVALUATIONS_dqClotureChecklistEvaluations | null)[] | null;
}
