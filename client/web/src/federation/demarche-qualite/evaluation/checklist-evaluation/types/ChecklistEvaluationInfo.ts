/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ChecklistEvaluationInfo
// ====================================================

export interface ChecklistEvaluationInfo_cloture {
  __typename: "DQChecklistEvaluationCloture";
  id: string;
}

export interface ChecklistEvaluationInfo {
  __typename: "DQChecklistEvaluation";
  id: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
  cloture: ChecklistEvaluationInfo_cloture | null;
}
