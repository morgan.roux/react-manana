/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION
// ====================================================

export interface TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION_toggleDQChecklistSectionItemEvaluation_cloture {
  __typename: "DQChecklistEvaluationCloture";
  id: string;
}

export interface TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION_toggleDQChecklistSectionItemEvaluation {
  __typename: "DQChecklistEvaluation";
  id: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
  cloture: TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION_toggleDQChecklistSectionItemEvaluation_cloture | null;
}

export interface TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION {
  toggleDQChecklistSectionItemEvaluation: TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATION_toggleDQChecklistSectionItemEvaluation | null;
}

export interface TOGGLE_CHECKLIST_SECTION_ITEM_EVALUATIONVariables {
  idChecklistEvaluation: string;
  idChecklistSectionItem: string;
}
