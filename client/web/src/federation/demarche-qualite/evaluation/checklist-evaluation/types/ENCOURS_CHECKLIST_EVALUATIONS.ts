/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ENCOURS_CHECKLIST_EVALUATIONS
// ====================================================

export interface ENCOURS_CHECKLIST_EVALUATIONS_dqEncoursChecklistEvaluations_cloture {
  __typename: "DQChecklistEvaluationCloture";
  id: string;
}

export interface ENCOURS_CHECKLIST_EVALUATIONS_dqEncoursChecklistEvaluations {
  __typename: "DQChecklistEvaluation";
  id: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
  cloture: ENCOURS_CHECKLIST_EVALUATIONS_dqEncoursChecklistEvaluations_cloture | null;
}

export interface ENCOURS_CHECKLIST_EVALUATIONS {
  dqEncoursChecklistEvaluations: (ENCOURS_CHECKLIST_EVALUATIONS_dqEncoursChecklistEvaluations | null)[] | null;
}
