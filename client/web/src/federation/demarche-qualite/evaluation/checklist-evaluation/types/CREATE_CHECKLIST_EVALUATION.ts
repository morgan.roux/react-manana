/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQChecklistEvaluationInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_CHECKLIST_EVALUATION
// ====================================================

export interface CREATE_CHECKLIST_EVALUATION_createDQChecklistEvaluation_cloture {
  __typename: "DQChecklistEvaluationCloture";
  id: string;
}

export interface CREATE_CHECKLIST_EVALUATION_createDQChecklistEvaluation {
  __typename: "DQChecklistEvaluation";
  id: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
  cloture: CREATE_CHECKLIST_EVALUATION_createDQChecklistEvaluation_cloture | null;
}

export interface CREATE_CHECKLIST_EVALUATION {
  createDQChecklistEvaluation: CREATE_CHECKLIST_EVALUATION_createDQChecklistEvaluation | null;
}

export interface CREATE_CHECKLIST_EVALUATIONVariables {
  idChecklist: string;
  checklistEvaluationInput: DQChecklistEvaluationInput;
}
