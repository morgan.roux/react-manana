/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQChecklistEvaluationClotureInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CLOTURE_CHECKLIST_EVALUATION
// ====================================================

export interface CLOTURE_CHECKLIST_EVALUATION_clotureDQChecklistEvaluation_cloture {
  __typename: "DQChecklistEvaluationCloture";
  id: string;
}

export interface CLOTURE_CHECKLIST_EVALUATION_clotureDQChecklistEvaluation {
  __typename: "DQChecklistEvaluation";
  id: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
  cloture: CLOTURE_CHECKLIST_EVALUATION_clotureDQChecklistEvaluation_cloture | null;
}

export interface CLOTURE_CHECKLIST_EVALUATION {
  clotureDQChecklistEvaluation: CLOTURE_CHECKLIST_EVALUATION_clotureDQChecklistEvaluation | null;
}

export interface CLOTURE_CHECKLIST_EVALUATIONVariables {
  id: string;
  checklistEvaluationInput: DQChecklistEvaluationClotureInput;
}
