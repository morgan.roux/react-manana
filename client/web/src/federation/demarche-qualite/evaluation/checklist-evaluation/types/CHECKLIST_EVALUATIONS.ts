/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CHECKLIST_EVALUATIONS
// ====================================================

export interface CHECKLIST_EVALUATIONS_dqChecklistEvaluations_cloture {
  __typename: "DQChecklistEvaluationCloture";
  id: string;
}

export interface CHECKLIST_EVALUATIONS_dqChecklistEvaluations {
  __typename: "DQChecklistEvaluation";
  id: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
  cloture: CHECKLIST_EVALUATIONS_dqChecklistEvaluations_cloture | null;
}

export interface CHECKLIST_EVALUATIONS {
  dqChecklistEvaluations: (CHECKLIST_EVALUATIONS_dqChecklistEvaluations | null)[] | null;
}

export interface CHECKLIST_EVALUATIONSVariables {
  idChecklist: string;
}
