/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_CHECKLIST_EVALUATION
// ====================================================

export interface DELETE_CHECKLIST_EVALUATION_deleteDQChecklistEvaluation_cloture {
  __typename: "DQChecklistEvaluationCloture";
  id: string;
}

export interface DELETE_CHECKLIST_EVALUATION_deleteDQChecklistEvaluation {
  __typename: "DQChecklistEvaluation";
  id: string;
  ordre: number;
  libelle: string;
  createdAt: any | null;
  updatedAt: any | null;
  cloture: DELETE_CHECKLIST_EVALUATION_deleteDQChecklistEvaluation_cloture | null;
}

export interface DELETE_CHECKLIST_EVALUATION {
  deleteDQChecklistEvaluation: DELETE_CHECKLIST_EVALUATION_deleteDQChecklistEvaluation | null;
}

export interface DELETE_CHECKLIST_EVALUATIONVariables {
  id: string;
}
