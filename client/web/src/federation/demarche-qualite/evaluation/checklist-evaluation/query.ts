import gql from 'graphql-tag';

import { FULL_EVALUATION_INFO } from './fragment';

export const CHECKLIST_EVALUATION = gql`
query CHECKLIST_EVALUATION ($id:ID!){
  dqChecklistEvaluation(id:$id) {
      ...ChecklistEvaluationInfo
    }
  }
  ${FULL_EVALUATION_INFO}
`;

export const CHECKLIST_EVALUATIONS= gql`
query CHECKLIST_EVALUATIONS ($idChecklist:ID!) {
  dqChecklistEvaluations(idChecklist:$idChecklist) {
      ...ChecklistEvaluationInfo
    }
  }
  ${FULL_EVALUATION_INFO}
`;

export const CLOTURE_CHECKLIST_EVALUATIONS= gql`
query CLOTURE_CHECKLIST_EVALUATIONS {
  dqClotureChecklistEvaluations {
      ...ChecklistEvaluationInfo
    }
  }
  ${FULL_EVALUATION_INFO}
`;

export const ENCOURS_CHECKLIST_EVALUATIONS= gql`
query ENCOURS_CHECKLIST_EVALUATIONS {
  dqEncoursChecklistEvaluations {
      ...ChecklistEvaluationInfo
    }
  }
  ${FULL_EVALUATION_INFO}
`;