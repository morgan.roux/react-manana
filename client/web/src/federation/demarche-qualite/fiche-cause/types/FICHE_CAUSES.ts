/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQFicheCauseFilter, DQFicheCauseSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: FICHE_CAUSES
// ====================================================

export interface FICHE_CAUSES_dQFicheCauses_nodes {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface FICHE_CAUSES_dQFicheCauses {
  __typename: "DQFicheCauseConnection";
  /**
   * Array of nodes.
   */
  nodes: FICHE_CAUSES_dQFicheCauses_nodes[];
}

export interface FICHE_CAUSES {
  dQFicheCauses: FICHE_CAUSES_dQFicheCauses;
}

export interface FICHE_CAUSESVariables {
  paging?: OffsetPaging | null;
  filter?: DQFicheCauseFilter | null;
  sorting?: DQFicheCauseSort[] | null;
}
