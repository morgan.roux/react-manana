/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FicheCauseInfo
// ====================================================

export interface FicheCauseInfo {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}
