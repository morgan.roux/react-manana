import gql from 'graphql-tag';
import { FICHE_CAUSE_INFO } from './fragment';

export const GET_FICHE_CAUSES = gql`
  query FICHE_CAUSES($paging: OffsetPaging, $filter: DQFicheCauseFilter, $sorting: [DQFicheCauseSort!]) {
    dQFicheCauses(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...FicheCauseInfo
      }
    }
  }

  ${FICHE_CAUSE_INFO}
`;
