import gql from 'graphql-tag';

export const FICHE_CAUSE_INFO = gql`
  fragment FicheCauseInfo on DQFicheCause {
    id
    libelle
  }
`;
