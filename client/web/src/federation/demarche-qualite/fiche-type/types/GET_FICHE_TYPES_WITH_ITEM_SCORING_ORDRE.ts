/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQFicheTypeFilter, DQFicheTypeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE
// ====================================================

export interface GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE_dQFicheTypes_nodes {
  __typename: "DQFicheType";
  id: string;
  code: string;
  libelle: string;
  itemScoringOrdre: number | null;
  itemScoringIdPersonnalisation: string | null;
}

export interface GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE_dQFicheTypes {
  __typename: "DQFicheTypeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE_dQFicheTypes_nodes[];
}

export interface GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE {
  dQFicheTypes: GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE_dQFicheTypes;
}

export interface GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDREVariables {
  paging?: OffsetPaging | null;
  filter?: DQFicheTypeFilter | null;
  sorting?: DQFicheTypeSort[] | null;
  idPharmacie?: string | null;
}
