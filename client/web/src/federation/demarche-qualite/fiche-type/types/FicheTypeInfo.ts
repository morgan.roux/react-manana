/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FicheTypeInfo
// ====================================================

export interface FicheTypeInfo {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}
