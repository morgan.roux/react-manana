import gql from 'graphql-tag';
import { ACTIVITE_WITH_NOMBRE_FICHE_AMELIORATIONS_INFO, ACTIVITE_INFO } from './fragment';

export const GET_ACTIVITES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS = gql`
  query GET_ACTIVITES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS($searchText: String) {
    dQActivitesOrderByNombreFicheAmeliorations(searchText: $searchText) {
      ...ActiviteInfo
    }
  }

  ${ACTIVITE_WITH_NOMBRE_FICHE_AMELIORATIONS_INFO}
`;

export const GET_ACTIVITES = gql`
  query GET_ACTIVITES(
    $paging: OffsetPaging
    $filter: DQActiviteFilter
    $sorting: [DQActiviteSort!]
  ) {
    dQActivites(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...ActiviteInfo
      }
    }
  }

  ${ACTIVITE_INFO}
`;
