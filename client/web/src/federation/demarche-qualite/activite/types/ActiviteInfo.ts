/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ActiviteInfo
// ====================================================

export interface ActiviteInfo {
  __typename: "DQActivite";
  id: string;
  libelle: string;
  nombreFicheAmeliorations: number;
}
