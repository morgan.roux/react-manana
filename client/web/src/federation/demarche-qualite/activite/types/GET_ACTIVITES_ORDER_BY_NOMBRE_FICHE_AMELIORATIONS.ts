/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_ACTIVITES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS
// ====================================================

export interface GET_ACTIVITES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS_dQActivitesOrderByNombreFicheAmeliorations {
  __typename: "DQActivite";
  id: string;
  libelle: string;
  nombreFicheAmeliorations: number;
}

export interface GET_ACTIVITES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS {
  dQActivitesOrderByNombreFicheAmeliorations: GET_ACTIVITES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS_dQActivitesOrderByNombreFicheAmeliorations[];
}

export interface GET_ACTIVITES_ORDER_BY_NOMBRE_FICHE_AMELIORATIONSVariables {
  searchText?: string | null;
}
