/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_TACHE_USER_RESPONSABLES
// ====================================================

export interface GET_TACHE_USER_RESPONSABLES_dQMTTacheUserResponsables_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_TACHE_USER_RESPONSABLES_dQMTTacheUserResponsables_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_TACHE_USER_RESPONSABLES_dQMTTacheUserResponsables {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: GET_TACHE_USER_RESPONSABLES_dQMTTacheUserResponsables_role | null;
  photo: GET_TACHE_USER_RESPONSABLES_dQMTTacheUserResponsables_photo | null;
  phoneNumber: string | null;
}

export interface GET_TACHE_USER_RESPONSABLES {
  dQMTTacheUserResponsables: GET_TACHE_USER_RESPONSABLES_dQMTTacheUserResponsables[];
}

export interface GET_TACHE_USER_RESPONSABLESVariables {
  idTache: string;
  idPharmacie: string;
}
