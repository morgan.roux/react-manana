/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: GENERATE_FONCTIONS
// ====================================================

export interface GENERATE_FONCTIONS_generateDQMTFonctions_taches {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface GENERATE_FONCTIONS_generateDQMTFonctions {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
  nombreTaches: number;
  taches: GENERATE_FONCTIONS_generateDQMTFonctions_taches[];
}

export interface GENERATE_FONCTIONS {
  generateDQMTFonctions: GENERATE_FONCTIONS_generateDQMTFonctions[];
}

export interface GENERATE_FONCTIONSVariables {
  idPharmacie: string;
}
