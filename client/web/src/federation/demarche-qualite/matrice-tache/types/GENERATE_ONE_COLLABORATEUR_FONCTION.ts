/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: GENERATE_ONE_COLLABORATEUR_FONCTION
// ====================================================

export interface GENERATE_ONE_COLLABORATEUR_FONCTION {
  generateOneCollaborateurDQMTFonction: number;
}

export interface GENERATE_ONE_COLLABORATEUR_FONCTIONVariables {
  idPharmacie: string;
  idUser: string;
}
