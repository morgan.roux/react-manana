/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_TACHE
// ====================================================

export interface GET_TACHE_dQMTTache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface GET_TACHE {
  dQMTTache: GET_TACHE_dQMTTache | null;
}

export interface GET_TACHEVariables {
  id: string;
}
