/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: TOGGLE_ONE_TACHE_ACTIVATION
// ====================================================

export interface TOGGLE_ONE_TACHE_ACTIVATION_toggleOneDQMtTacheActivation {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
  active: boolean;
}

export interface TOGGLE_ONE_TACHE_ACTIVATION {
  toggleOneDQMtTacheActivation: TOGGLE_ONE_TACHE_ACTIVATION_toggleOneDQMtTacheActivation;
}

export interface TOGGLE_ONE_TACHE_ACTIVATIONVariables {
  idPharmacie: string;
  idTache: string;
}
