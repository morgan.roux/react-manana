/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQMTTacheResponsableFilter, DQMTTacheResponsableSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_TACHE_RESPONSABLES
// ====================================================

export interface GET_TACHE_RESPONSABLES_dQMTTacheResponsables_nodes {
  __typename: "DQMTTacheResponsable";
  id: string;
  idType: string;
  idUser: string;
  idTache: string | null;
  idFonction: string;
  idPharmacie: string;
  idGroupement: string;
}

export interface GET_TACHE_RESPONSABLES_dQMTTacheResponsables {
  __typename: "DQMTTacheResponsableConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_TACHE_RESPONSABLES_dQMTTacheResponsables_nodes[];
}

export interface GET_TACHE_RESPONSABLES {
  dQMTTacheResponsables: GET_TACHE_RESPONSABLES_dQMTTacheResponsables;
}

export interface GET_TACHE_RESPONSABLESVariables {
  paging?: OffsetPaging | null;
  filter?: DQMTTacheResponsableFilter | null;
  sorting?: DQMTTacheResponsableSort[] | null;
}
