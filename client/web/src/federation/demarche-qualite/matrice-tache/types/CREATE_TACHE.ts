/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreateOneDQMTTacheInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_TACHE
// ====================================================

export interface CREATE_TACHE_createOneDQMTTache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface CREATE_TACHE {
  createOneDQMTTache: CREATE_TACHE_createOneDQMTTache;
}

export interface CREATE_TACHEVariables {
  input: CreateOneDQMTTacheInput;
}
