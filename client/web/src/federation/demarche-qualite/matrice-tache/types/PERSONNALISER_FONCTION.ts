/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQMTFonctionInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: PERSONNALISER_FONCTION
// ====================================================

export interface PERSONNALISER_FONCTION_personnaliserDQMTFonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
  nombreTaches: number;
  active: boolean;
}

export interface PERSONNALISER_FONCTION {
  personnaliserDQMTFonction: PERSONNALISER_FONCTION_personnaliserDQMTFonction;
}

export interface PERSONNALISER_FONCTIONVariables {
  input: DQMTFonctionInput;
  idFonction: string;
  idPharmacie: string;
}
