/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CreateOneDQMTFonctionInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_FONCTION
// ====================================================

export interface CREATE_FONCTION_createOneDQMTFonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
  nombreTaches: number;
  active: boolean;
}

export interface CREATE_FONCTION {
  createOneDQMTFonction: CREATE_FONCTION_createOneDQMTFonction;
}

export interface CREATE_FONCTIONVariables {
  input: CreateOneDQMTFonctionInput;
  idPharmacie: string;
}
