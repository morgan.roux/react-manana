/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateOneDQMTFonctionInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_FONCTION
// ====================================================

export interface UPDATE_FONCTION_updateOneDQMTFonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
  nombreTaches: number;
  active: boolean;
}

export interface UPDATE_FONCTION {
  updateOneDQMTFonction: UPDATE_FONCTION_updateOneDQMTFonction;
}

export interface UPDATE_FONCTIONVariables {
  input: UpdateOneDQMTFonctionInput;
  idPharmacie: string;
}
