/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQMTTacheResponsableInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_MANY_TACHE_RESPONSABLES
// ====================================================

export interface UPDATE_MANY_TACHE_RESPONSABLES_updateManyDQMTTacheResponsables {
  __typename: "DQMTTacheResponsable";
  id: string;
  idType: string;
  idUser: string;
  idTache: string | null;
  idFonction: string;
  idPharmacie: string;
  idGroupement: string;
}

export interface UPDATE_MANY_TACHE_RESPONSABLES {
  updateManyDQMTTacheResponsables: UPDATE_MANY_TACHE_RESPONSABLES_updateManyDQMTTacheResponsables[];
}

export interface UPDATE_MANY_TACHE_RESPONSABLESVariables {
  idUser: string;
  idPharmacie: string;
  taches: DQMTTacheResponsableInput[];
}
