/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_FONCTION_USER_RESPONSABLES
// ====================================================

export interface GET_FONCTION_USER_RESPONSABLES_dQMTFonctionUserResponsables_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_FONCTION_USER_RESPONSABLES_dQMTFonctionUserResponsables_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_FONCTION_USER_RESPONSABLES_dQMTFonctionUserResponsables {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  role: GET_FONCTION_USER_RESPONSABLES_dQMTFonctionUserResponsables_role | null;
  photo: GET_FONCTION_USER_RESPONSABLES_dQMTFonctionUserResponsables_photo | null;
  phoneNumber: string | null;
}

export interface GET_FONCTION_USER_RESPONSABLES {
  dQMTFonctionUserResponsables: GET_FONCTION_USER_RESPONSABLES_dQMTFonctionUserResponsables[];
}

export interface GET_FONCTION_USER_RESPONSABLESVariables {
  idFonction: string;
  idPharmacie: string;
}
