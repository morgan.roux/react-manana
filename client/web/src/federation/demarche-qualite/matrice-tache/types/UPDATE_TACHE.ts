/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateOneDQMTTacheInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_TACHE
// ====================================================

export interface UPDATE_TACHE_updateOneDQMTTache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface UPDATE_TACHE {
  updateOneDQMTTache: UPDATE_TACHE_updateOneDQMTTache;
}

export interface UPDATE_TACHEVariables {
  input: UpdateOneDQMTTacheInput;
}
