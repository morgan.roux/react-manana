/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQMTTacheInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: PERSONNALISER_TACHE
// ====================================================

export interface PERSONNALISER_TACHE_personnaliserDQMTTache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
  active: boolean;
}

export interface PERSONNALISER_TACHE {
  personnaliserDQMTTache: PERSONNALISER_TACHE_personnaliserDQMTTache;
}

export interface PERSONNALISER_TACHEVariables {
  input: DQMTTacheInput;
  idTache: string;
  idPharmacie: string;
}
