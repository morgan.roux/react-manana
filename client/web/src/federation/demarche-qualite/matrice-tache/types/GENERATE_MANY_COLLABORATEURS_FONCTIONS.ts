/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: GENERATE_MANY_COLLABORATEURS_FONCTIONS
// ====================================================

export interface GENERATE_MANY_COLLABORATEURS_FONCTIONS {
  generateManyCollaborateursDQMTFonctions: number;
}

export interface GENERATE_MANY_COLLABORATEURS_FONCTIONSVariables {
  idPharmacie: string;
}
