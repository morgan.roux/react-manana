/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQMTFonctionFilter, DQMTFonctionSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_FONCTIONS
// ====================================================

export interface GET_FONCTIONS_dQMTFonctions_nodes {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
  nombreTaches: number;
  active: boolean;
}

export interface GET_FONCTIONS_dQMTFonctions {
  __typename: "DQMTFonctionConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_FONCTIONS_dQMTFonctions_nodes[];
}

export interface GET_FONCTIONS {
  dQMTFonctions: GET_FONCTIONS_dQMTFonctions;
}

export interface GET_FONCTIONSVariables {
  paging?: OffsetPaging | null;
  filter?: DQMTFonctionFilter | null;
  sorting?: DQMTFonctionSort[] | null;
  idPharmacie: string;
}
