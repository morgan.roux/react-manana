/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQMTResponsableTypeFilter, DQMTResponsableTypeSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_RESPONSABLE_TYPES
// ====================================================

export interface GET_RESPONSABLE_TYPES_dQMTResponsableTypes_nodes {
  __typename: "DQMTResponsableType";
  id: string;
  code: string;
  ordre: number;
  couleur: string;
  libelle: string;
}

export interface GET_RESPONSABLE_TYPES_dQMTResponsableTypes {
  __typename: "DQMTResponsableTypeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_RESPONSABLE_TYPES_dQMTResponsableTypes_nodes[];
}

export interface GET_RESPONSABLE_TYPES {
  dQMTResponsableTypes: GET_RESPONSABLE_TYPES_dQMTResponsableTypes;
}

export interface GET_RESPONSABLE_TYPESVariables {
  paging?: OffsetPaging | null;
  filter?: DQMTResponsableTypeFilter | null;
  sorting?: DQMTResponsableTypeSort[] | null;
}
