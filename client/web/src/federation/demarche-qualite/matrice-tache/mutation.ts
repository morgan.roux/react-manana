import gql from 'graphql-tag';

import {
  MINIMAL_FONCTION_INFO,
  MINIMAL_TACHE_INFO,
  MINIMAL_TACHE_RESPONSABLE_INFO,
} from './fragment';

export const CREATE_FONCTION = gql`
  mutation CREATE_FONCTION($input: CreateOneDQMTFonctionInput!, $idPharmacie: String!) {
    createOneDQMTFonction(input: $input) {
      id
      ordre
      libelle
      nombreTaches
      active(idPharmacie: $idPharmacie)
    }
  }
`;

export const UPDATE_FONCTION = gql`
  mutation UPDATE_FONCTION($input: UpdateOneDQMTFonctionInput!, $idPharmacie: String!) {
    updateOneDQMTFonction(input: $input) {
      id
      ordre
      libelle
      nombreTaches
      active(idPharmacie: $idPharmacie)
    }
  }
`;

export const DELETE_FONCTION = gql`
  mutation DELETE_FONCTION($input: DeleteOneDQMTFonctionInput!) {
    deleteOneDQMTFonction(input: $input) {
      id
    }
  }
`;

export const CREATE_TACHE = gql`
  mutation CREATE_TACHE($input: CreateOneDQMTTacheInput!) {
    createOneDQMTTache(input: $input) {
      ...TacheInfo
    }
  }
  ${MINIMAL_TACHE_INFO}
`;

export const UPDATE_TACHE = gql`
  mutation UPDATE_TACHE($input: UpdateOneDQMTTacheInput!) {
    updateOneDQMTTache(input: $input) {
      ...TacheInfo
    }
  }
  ${MINIMAL_TACHE_INFO}
`;

export const DELETE_TACHE = gql`
  mutation DELETE_TACHE($input: DeleteOneDQMTTacheInput!) {
    deleteOneDQMTTache(input: $input) {
      id
    }
  }
`;

export const UPDATE_MANY_TACHE_RESPONSABLES = gql`
  mutation UPDATE_MANY_TACHE_RESPONSABLES(
    $idUser: String!
    $idPharmacie: String!
    $taches: [DQMTTacheResponsableInput!]!
  ) {
    updateManyDQMTTacheResponsables(idUser: $idUser, idPharmacie: $idPharmacie, taches: $taches) {
      ...TacheResponsableInfo
    }
  }
  ${MINIMAL_TACHE_RESPONSABLE_INFO}
`;

export const GENERATE_FONCTIONS = gql`
  mutation GENERATE_FONCTIONS($idPharmacie: String!) {
    generateDQMTFonctions(idPharmacie: $idPharmacie) {
      ...FonctionInfo
    }
  }
  ${MINIMAL_FONCTION_INFO}
`;

export const GENERATE_ONE_COLLABORATEUR_FONCTION = gql`
  mutation GENERATE_ONE_COLLABORATEUR_FONCTION($idPharmacie: String!, $idUser: String!) {
    generateOneCollaborateurDQMTFonction(idPharmacie: $idPharmacie, idUser: $idUser)
  }
`;

export const GENERATE_MANY_COLLABORATEURS_FONCTIONS = gql`
  mutation GENERATE_MANY_COLLABORATEURS_FONCTIONS($idPharmacie: String!) {
    generateManyCollaborateursDQMTFonctions(idPharmacie: $idPharmacie)
  }
`;

export const TOGGLE_ONE_FONCTION_ACTIVATION = gql`
  mutation TOGGLE_ONE_FONCTION_ACTIVATION($idPharmacie: String!, $idFonction: String!) {
    toggleOneDQMtFonctionActivation(idPharmacie: $idPharmacie, idFonction: $idFonction) {
      id
      ordre
      libelle
      nombreTaches
      active(idPharmacie: $idPharmacie)
    }
  }
`;

export const TOGGLE_ONE_TACHE_ACTIVATION = gql`
  mutation TOGGLE_ONE_TACHE_ACTIVATION($idPharmacie: String!, $idTache: String!) {
    toggleOneDQMtTacheActivation(idPharmacie: $idPharmacie, idTache: $idTache) {
      id
      ordre
      libelle
      active(idPharmacie: $idPharmacie)
    }
  }
`;

export const PERSONNALISER_FONCTION = gql`
  mutation PERSONNALISER_FONCTION(
    $input: DQMTFonctionInput!
    $idFonction: String!
    $idPharmacie: String!
  ) {
    personnaliserDQMTFonction(input: $input, idFonction: $idFonction, idPharmacie: $idPharmacie) {
      id
      ordre(idPharmacie: $idPharmacie)
      libelle(idPharmacie: $idPharmacie)
      nombreTaches(idPharmacie: $idPharmacie)
      active(idPharmacie: $idPharmacie)
    }
  }
`;

export const PERSONNALISER_TACHE = gql`
  mutation PERSONNALISER_TACHE($input: DQMTTacheInput!, $idTache: String!, $idPharmacie: String!) {
    personnaliserDQMTTache(input: $input, idTache: $idTache, idPharmacie: $idPharmacie) {
      id
      ordre(idPharmacie: $idPharmacie)
      libelle(idPharmacie: $idPharmacie)
      active(idPharmacie: $idPharmacie)
    }
  }
`;
