import gql from 'graphql-tag';

export const MINIMAL_FONCTION_INFO = gql`
  fragment FonctionInfo on DQMTFonction {
    id
    ordre
    libelle
    nombreTaches
  }
`;

export const FULL_FONCTION_INFO = gql`
  fragment FonctionInfo on DQMTFonction {
    id
    ordre
    libelle
    nombreTaches
    taches {
      id
      ordre
      libelle
    }
  }
`;

export const MINIMAL_TACHE_INFO = gql`
  fragment TacheInfo on DQMTTache {
    id
    ordre
    libelle
  }
`;

export const RESPONSABLE_TYPE_INFO = gql`
  fragment ResponsableTypeInfo on DQMTResponsableType {
    id
    code
    ordre
    couleur
    libelle
  }
`;

export const MINIMAL_TACHE_RESPONSABLE_INFO = gql`
  fragment TacheResponsableInfo on DQMTTacheResponsable {
    id
    idType
    idUser
    idTache
    idFonction
    idPharmacie
    idGroupement
  }
`;