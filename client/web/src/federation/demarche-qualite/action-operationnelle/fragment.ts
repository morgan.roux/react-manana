import gql from 'graphql-tag';

export const ACTION_OPERATIONNELLE = gql`
  fragment ActionOperationnelle on DQActionOperationnelle {
    id
    description
    statut {
      id
      libelle
      code
    }
    idUserAuteur
    idUserSuivi
    idOrigine
    idOrigineAssocie
    dateAction
    dateEcheance
    idType
    idCause
    origineAssocie {
      ... on User {
        id
        dataType
        fullName
        photo {
          id
          publicUrl
          chemin
          nomOriginal
          type
        }
      }
      ... on Laboratoire {
        id
        dataType
        nom
      }
      ... on PrestataireService {
        id
        dataType
        nom
      }
    }
    fichiers {
      chemin
      nomOriginal
      type
      publicUrl
    }
    suiviPar {
      id
      fullName
    }
    auteur {
      id
      fullName
    }
    origine {
      id
    }
    type {
      id
      libelle
    }
    cause {
      id
      libelle
    }
    urgence {
      id
      code
      libelle
      couleur
    }
    importance {
      id
      ordre
      libelle
      couleur
    }
    fonction {
      id
      libelle
    }
    tache {
      id
      libelle
    }
    fonctionAInformer {
      id
      libelle
    }
    tacheAInformer {
      id
      libelle
    }
    reunion{
      id
    }
    participants {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }
    participantsAInformer {
      id
      fullName
      photo {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
    }
  }
`;
