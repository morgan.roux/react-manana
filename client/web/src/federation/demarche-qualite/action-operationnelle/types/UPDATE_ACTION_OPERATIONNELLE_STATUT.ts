/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UPDATE_ACTION_OPERATIONNELLE_STATUT
// ====================================================

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_origineAssocie_User_photo | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_origineAssocie = UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_origineAssocie_User | UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_origineAssocie_Laboratoire | UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_origineAssocie_PrestataireService;

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_fichiers {
  __typename: "Fichier";
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_suiviPar {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_origine {
  __typename: "Origine";
  id: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_participants_photo | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_participantsAInformer_photo | null;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut {
  __typename: "DQActionOperationnelle";
  id: string;
  description: string;
  statut: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_statut;
  idUserAuteur: string;
  idUserSuivi: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAction: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  origineAssocie: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_origineAssocie | null;
  fichiers: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_fichiers[];
  suiviPar: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_suiviPar;
  auteur: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_auteur;
  origine: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_origine;
  type: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_type | null;
  cause: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_cause | null;
  urgence: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_urgence;
  importance: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_importance;
  fonction: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_fonction | null;
  tache: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_tache | null;
  fonctionAInformer: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_fonctionAInformer | null;
  tacheAInformer: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_tacheAInformer | null;
  reunion: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_reunion | null;
  participants: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_participants[];
  participantsAInformer: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut_participantsAInformer[];
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUT {
  updateActionOperationnelleStatut: UPDATE_ACTION_OPERATIONNELLE_STATUT_updateActionOperationnelleStatut;
}

export interface UPDATE_ACTION_OPERATIONNELLE_STATUTVariables {
  id: string;
  code: string;
}
