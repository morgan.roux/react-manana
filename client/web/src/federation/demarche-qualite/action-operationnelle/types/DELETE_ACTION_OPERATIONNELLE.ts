/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneDQActionOperationnelleInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_ACTION_OPERATIONNELLE
// ====================================================

export interface DELETE_ACTION_OPERATIONNELLE_deleteOneDQActionOperationnelle {
  __typename: "DQActionOperationnelleDeleteResponse";
  id: string | null;
}

export interface DELETE_ACTION_OPERATIONNELLE {
  deleteOneDQActionOperationnelle: DELETE_ACTION_OPERATIONNELLE_deleteOneDQActionOperationnelle;
}

export interface DELETE_ACTION_OPERATIONNELLEVariables {
  input: DeleteOneDQActionOperationnelleInput;
}
