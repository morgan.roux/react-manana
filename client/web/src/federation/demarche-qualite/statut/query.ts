import gql from 'graphql-tag';
import {
  STATUT_INFO,
  STATUT_WITH_NOMBRE_FICHE_INCIDENTS_INFO,
  STATUT_WITH_NOMBRE_FICHE_AMELIORATIONS_INFO,
  STATUT_WITH_NOMBRE_ACTION_OPERATIONNELLES_INFO,
} from './fragment';

export const GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTS = gql`
  query GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTS($searchText: String) {
    dQStatutsOrderByNombreFicheIncidents(searchText: $searchText) {
      ...StatutInfo
    }
  }

  ${STATUT_WITH_NOMBRE_FICHE_INCIDENTS_INFO}
`;

export const GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS = gql`
  query GET_STATUTS_ORDER_BY_NOMBRE_FICHE_AMELIORATIONS($searchText: String) {
    dQStatutsOrderByNombreFicheAmeliorations(searchText: $searchText) {
      ...StatutInfo
    }
  }

  ${STATUT_WITH_NOMBRE_FICHE_AMELIORATIONS_INFO}
`;

export const GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLES = gql`
  query GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLES($searchText: String) {
    dQStatutsOrderByNombreActionOperationnelles(searchText: $searchText) {
      ...StatutInfo
    }
  }

  ${STATUT_WITH_NOMBRE_ACTION_OPERATIONNELLES_INFO}
`;

export const GET_STATUTS = gql`
  query GET_STATUTS($paging: OffsetPaging, $filter: DQStatutFilter, $sorting: [DQStatutSort!]) {
    dQStatuts(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...StatutInfo
      }
    }
  }

  ${STATUT_INFO}
`;
