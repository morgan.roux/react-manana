/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: StatutInfo
// ====================================================

export interface StatutInfo {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  type: string;
  code: string;
  nombreActionOperationnelles: number;
}
