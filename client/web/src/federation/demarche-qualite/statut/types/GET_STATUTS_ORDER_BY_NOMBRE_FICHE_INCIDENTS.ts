/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTS
// ====================================================

export interface GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTS_dQStatutsOrderByNombreFicheIncidents {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  type: string;
  code: string;
  nombreActionOperationnelles: number;
}

export interface GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTS {
  dQStatutsOrderByNombreFicheIncidents: GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTS_dQStatutsOrderByNombreFicheIncidents[];
}

export interface GET_STATUTS_ORDER_BY_NOMBRE_FICHE_INCIDENTSVariables {
  searchText?: string | null;
}
