/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQStatutFilter, DQStatutSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_STATUTS
// ====================================================

export interface GET_STATUTS_dQStatuts_nodes {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  type: string;
  code: string;
  nombreActionOperationnelles: number;
}

export interface GET_STATUTS_dQStatuts {
  __typename: "DQStatutConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_STATUTS_dQStatuts_nodes[];
}

export interface GET_STATUTS {
  dQStatuts: GET_STATUTS_dQStatuts;
}

export interface GET_STATUTSVariables {
  paging?: OffsetPaging | null;
  filter?: DQStatutFilter | null;
  sorting?: DQStatutSort[] | null;
}
