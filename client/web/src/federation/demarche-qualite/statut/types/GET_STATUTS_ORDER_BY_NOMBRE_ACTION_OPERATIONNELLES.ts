/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLES
// ====================================================

export interface GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLES_dQStatutsOrderByNombreActionOperationnelles {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  type: string;
  code: string;
  nombreActionOperationnelles: number;
}

export interface GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLES {
  dQStatutsOrderByNombreActionOperationnelles: GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLES_dQStatutsOrderByNombreActionOperationnelles[];
}

export interface GET_STATUTS_ORDER_BY_NOMBRE_ACTION_OPERATIONNELLESVariables {
  searchText?: string | null;
}
