import gql from 'graphql-tag';

export const STATUT_INFO = gql`
  fragment StatutInfo on DQStatut {
    id
    code
    type
    libelle
  }
`;

export const STATUT_WITH_NOMBRE_FICHE_INCIDENTS_INFO = gql`
  fragment StatutInfo on DQStatut {
    id
    libelle
    type
    code
    nombreFicheIncidents
  }
`;


export const STATUT_WITH_NOMBRE_FICHE_AMELIORATIONS_INFO = gql`
  fragment StatutInfo on DQStatut {
    id
    libelle
    type
    code
    nombreFicheAmeliorations
  }
`;

export const STATUT_WITH_NOMBRE_ACTION_OPERATIONNELLES_INFO = gql`
  fragment StatutInfo on DQStatut {
    id
    libelle
    type
    code
    nombreActionOperationnelles
  }
`;
