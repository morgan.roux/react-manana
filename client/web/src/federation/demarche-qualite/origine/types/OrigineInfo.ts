/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: OrigineInfo
// ====================================================

export interface OrigineInfo {
  __typename: "Origine";
  id: string;
  libelle: string;
  code: string;
  nombreFicheIncidents: number;
}
