/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS
// ====================================================

export interface GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS_origines_nodes {
  __typename: "Origine";
  id: string;
  libelle: string;
  code: string;
  nombreFicheIncidents: number;
}

export interface GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS_origines {
  __typename: "OrigineConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS_origines_nodes[];
}

export interface GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS {
  origines: GET_ORIGINES_ORDER_BY_NOMBRE_FICHE_INCIDENTS_origines;
}
