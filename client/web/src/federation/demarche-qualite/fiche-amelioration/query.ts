import gql from 'graphql-tag';
import { FICHE_AMELIORATION_ACTION_INFO, FICHE_AMELIORATION_INFO } from './fragment';

export const GET_FICHE_AMELIORATION_ACTIONS = gql`
  query GET_FICHE_AMELIORATION_ACTIONS(
    $paging: OffsetPaging
    $filter: DQFicheAmeliorationActionFilter
    $sorting: [DQFicheAmeliorationActionSort!]
  ) {
    dQFicheAmeliorationActions(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...FicheAmeliorationActionInfo
      }
    }
  }

  ${FICHE_AMELIORATION_ACTION_INFO}
`;

export const GET_FICHE_AMELIORATIONS = gql`
  query GET_FICHE_AMELIORATIONS(
    $paging: OffsetPaging
    $filter: DQFicheAmeliorationFilter
    $sorting: [DQFicheAmeliorationSort!]
  ) {
    dQFicheAmeliorations(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...FicheAmeliorationInfo
      }
    }
  }

  ${FICHE_AMELIORATION_INFO}
`;

export const GET_FICHE_AMELIORATION_AGGREGATE = gql`
  query GET_FICHE_AMELIORATION_AGGREGATE($filter: DQFicheAmeliorationAggregateFilter) {
    dQFicheAmeliorationAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;

export const GET_FICHE_AMELIORATION = gql`
  query GET_FICHE_AMELIORATION($id: ID!) {
    dQFicheAmelioration(id: $id) {
      ...FicheAmeliorationInfo
    }
  }
  ${FICHE_AMELIORATION_INFO}
`;

export const GET_FICHE_AMELIORATION_ACTION = gql`
  query GET_FICHE_AMELIORATION_ACTION($id: ID!) {
    dQFicheAmeliorationAction(id: $id) {
      ...FicheAmeliorationActionInfo
    }
  }

  ${FICHE_AMELIORATION_ACTION_INFO}
`;

export const GET_ENCOURS_FICHE_AMELIORATIONS = gql`
  query GET_ENCOURS_FICHE_AMELIORATIONS {
    dQEncoursFicheAmeliorations {
      ...FicheAmeliorationInfo
    }
  }

  ${FICHE_AMELIORATION_INFO}
`;
