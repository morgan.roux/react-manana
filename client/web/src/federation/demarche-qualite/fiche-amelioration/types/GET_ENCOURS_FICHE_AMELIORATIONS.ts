/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_ENCOURS_FICHE_AMELIORATIONS
// ====================================================

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_origineAssocie_User_photo | null;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_origineAssocie = GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_origineAssocie_User | GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_origineAssocie_Laboratoire | GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_origineAssocie_PrestataireService;

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_origine {
  __typename: "Origine";
  id: string;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_participants_photo | null;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_participantsAInformer_photo | null;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations {
  __typename: "DQFicheAmelioration";
  id: string;
  description: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAmelioration: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  idAction: string | null;
  createdAt: any;
  updatedAt: any;
  origineAssocie: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_origineAssocie | null;
  statut: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_statut;
  cause: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_cause | null;
  type: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_type | null;
  fichiers: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_fichiers[];
  auteur: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_auteur;
  origine: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_origine;
  urgence: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_urgence;
  importance: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_importance;
  fonction: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_fonction | null;
  tache: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_tache | null;
  fonctionAInformer: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_fonctionAInformer | null;
  tacheAInformer: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_tacheAInformer | null;
  participants: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_participants[];
  participantsAInformer: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_participantsAInformer[];
  reunion: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations_reunion | null;
}

export interface GET_ENCOURS_FICHE_AMELIORATIONS {
  dQEncoursFicheAmeliorations: GET_ENCOURS_FICHE_AMELIORATIONS_dQEncoursFicheAmeliorations[];
}
