/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneDQFicheAmeliorationInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_FICHE_AMELIORATION
// ====================================================

export interface DELETE_FICHE_AMELIORATION_deleteOneDQFicheAmelioration {
  __typename: "DQFicheAmeliorationDeleteResponse";
  id: string | null;
  description: string | null;
}

export interface DELETE_FICHE_AMELIORATION {
  deleteOneDQFicheAmelioration: DELETE_FICHE_AMELIORATION_deleteOneDQFicheAmelioration;
}

export interface DELETE_FICHE_AMELIORATIONVariables {
  input: DeleteOneDQFicheAmeliorationInput;
}
