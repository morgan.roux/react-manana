/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQFicheAmeliorationActionInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_FICHE_AMELIORATION_ACTION
// ====================================================

export interface CREATE_FICHE_AMELIORATION_ACTION_createOneDQFicheAmeliorationAction_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  type: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface CREATE_FICHE_AMELIORATION_ACTION_createOneDQFicheAmeliorationAction {
  __typename: "DQFicheAmeliorationAction";
  id: string;
  description: string;
  dateHeureMiseEnplace: any;
  createdAt: any;
  updatedAt: any;
  fichiers: CREATE_FICHE_AMELIORATION_ACTION_createOneDQFicheAmeliorationAction_fichiers[];
}

export interface CREATE_FICHE_AMELIORATION_ACTION {
  createOneDQFicheAmeliorationAction: CREATE_FICHE_AMELIORATION_ACTION_createOneDQFicheAmeliorationAction;
}

export interface CREATE_FICHE_AMELIORATION_ACTIONVariables {
  idFicheAmelioration: string;
  action: DQFicheAmeliorationActionInput;
}
