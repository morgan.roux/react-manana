/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, DQFicheAmeliorationFilter, DQFicheAmeliorationSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_FICHE_AMELIORATIONS
// ====================================================

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_origineAssocie_User_photo | null;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_origineAssocie = GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_origineAssocie_User | GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_origineAssocie_Laboratoire | GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_origineAssocie_PrestataireService;

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_origine {
  __typename: "Origine";
  id: string;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_participants_photo | null;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_participantsAInformer_photo | null;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes {
  __typename: "DQFicheAmelioration";
  id: string;
  description: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAmelioration: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  idAction: string | null;
  createdAt: any;
  updatedAt: any;
  origineAssocie: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_origineAssocie | null;
  statut: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_statut;
  cause: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_cause | null;
  type: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_type | null;
  fichiers: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_fichiers[];
  auteur: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_auteur;
  origine: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_origine;
  urgence: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_urgence;
  importance: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_importance;
  fonction: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_fonction | null;
  tache: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_tache | null;
  fonctionAInformer: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_fonctionAInformer | null;
  tacheAInformer: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_tacheAInformer | null;
  participants: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_participants[];
  participantsAInformer: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_participantsAInformer[];
  reunion: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes_reunion | null;
}

export interface GET_FICHE_AMELIORATIONS_dQFicheAmeliorations {
  __typename: "DQFicheAmeliorationConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations_nodes[];
}

export interface GET_FICHE_AMELIORATIONS {
  dQFicheAmeliorations: GET_FICHE_AMELIORATIONS_dQFicheAmeliorations;
}

export interface GET_FICHE_AMELIORATIONSVariables {
  paging?: OffsetPaging | null;
  filter?: DQFicheAmeliorationFilter | null;
  sorting?: DQFicheAmeliorationSort[] | null;
}
