/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQFicheAmeliorationInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_FICHE_AMELIORATION
// ====================================================

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_origineAssocie_User_photo | null;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_origineAssocie = CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_origineAssocie_User | CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_origineAssocie_Laboratoire | CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_origineAssocie_PrestataireService;

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_origine {
  __typename: "Origine";
  id: string;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_participants_photo | null;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_participantsAInformer_photo | null;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration {
  __typename: "DQFicheAmelioration";
  id: string;
  description: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAmelioration: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  idAction: string | null;
  createdAt: any;
  updatedAt: any;
  origineAssocie: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_origineAssocie | null;
  statut: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_statut;
  cause: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_cause | null;
  type: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_type | null;
  fichiers: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_fichiers[];
  auteur: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_auteur;
  origine: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_origine;
  urgence: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_urgence;
  importance: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_importance;
  fonction: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_fonction | null;
  tache: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_tache | null;
  fonctionAInformer: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_fonctionAInformer | null;
  tacheAInformer: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_tacheAInformer | null;
  participants: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_participants[];
  participantsAInformer: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_participantsAInformer[];
  reunion: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration_reunion | null;
}

export interface CREATE_FICHE_AMELIORATION {
  createOneDQFicheAmelioration: CREATE_FICHE_AMELIORATION_createOneDQFicheAmelioration;
}

export interface CREATE_FICHE_AMELIORATIONVariables {
  idFicheIncident?: string | null;
  input: DQFicheAmeliorationInput;
}
