/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_FICHE_AMELIORATION
// ====================================================

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_origineAssocie_User_photo {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
  photo: GET_FICHE_AMELIORATION_dQFicheAmelioration_origineAssocie_User_photo | null;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export type GET_FICHE_AMELIORATION_dQFicheAmelioration_origineAssocie = GET_FICHE_AMELIORATION_dQFicheAmelioration_origineAssocie_User | GET_FICHE_AMELIORATION_dQFicheAmelioration_origineAssocie_Laboratoire | GET_FICHE_AMELIORATION_dQFicheAmelioration_origineAssocie_PrestataireService;

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  code: string;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_cause {
  __typename: "DQFicheCause";
  id: string;
  libelle: string;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_type {
  __typename: "DQFicheType";
  id: string;
  libelle: string;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_auteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_origine {
  __typename: "Origine";
  id: string;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_fonction {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_tache {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_fonctionAInformer {
  __typename: "DQMTFonction";
  id: string;
  libelle: string;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_tacheAInformer {
  __typename: "DQMTTache";
  id: string;
  libelle: string;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_FICHE_AMELIORATION_dQFicheAmelioration_participants_photo | null;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_participantsAInformer_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_participantsAInformer {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_FICHE_AMELIORATION_dQFicheAmelioration_participantsAInformer_photo | null;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration_reunion {
  __typename: "DQReunion";
  id: string;
}

export interface GET_FICHE_AMELIORATION_dQFicheAmelioration {
  __typename: "DQFicheAmelioration";
  id: string;
  description: string;
  idOrigine: string;
  idOrigineAssocie: string | null;
  dateAmelioration: any | null;
  dateEcheance: any | null;
  idType: string | null;
  idCause: string | null;
  idAction: string | null;
  createdAt: any;
  updatedAt: any;
  origineAssocie: GET_FICHE_AMELIORATION_dQFicheAmelioration_origineAssocie | null;
  statut: GET_FICHE_AMELIORATION_dQFicheAmelioration_statut;
  cause: GET_FICHE_AMELIORATION_dQFicheAmelioration_cause | null;
  type: GET_FICHE_AMELIORATION_dQFicheAmelioration_type | null;
  fichiers: GET_FICHE_AMELIORATION_dQFicheAmelioration_fichiers[];
  auteur: GET_FICHE_AMELIORATION_dQFicheAmelioration_auteur;
  origine: GET_FICHE_AMELIORATION_dQFicheAmelioration_origine;
  urgence: GET_FICHE_AMELIORATION_dQFicheAmelioration_urgence;
  importance: GET_FICHE_AMELIORATION_dQFicheAmelioration_importance;
  fonction: GET_FICHE_AMELIORATION_dQFicheAmelioration_fonction | null;
  tache: GET_FICHE_AMELIORATION_dQFicheAmelioration_tache | null;
  fonctionAInformer: GET_FICHE_AMELIORATION_dQFicheAmelioration_fonctionAInformer | null;
  tacheAInformer: GET_FICHE_AMELIORATION_dQFicheAmelioration_tacheAInformer | null;
  participants: GET_FICHE_AMELIORATION_dQFicheAmelioration_participants[];
  participantsAInformer: GET_FICHE_AMELIORATION_dQFicheAmelioration_participantsAInformer[];
  reunion: GET_FICHE_AMELIORATION_dQFicheAmelioration_reunion | null;
}

export interface GET_FICHE_AMELIORATION {
  dQFicheAmelioration: GET_FICHE_AMELIORATION_dQFicheAmelioration | null;
}

export interface GET_FICHE_AMELIORATIONVariables {
  id: string;
}
