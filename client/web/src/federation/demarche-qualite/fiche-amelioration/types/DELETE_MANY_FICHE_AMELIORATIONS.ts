/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteManyDQFicheAmeliorationsInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_MANY_FICHE_AMELIORATIONS
// ====================================================

export interface DELETE_MANY_FICHE_AMELIORATIONS_deleteManyDQFicheAmeliorations {
  __typename: "DeleteManyResponse";
  /**
   * The number of records deleted.
   */
  deletedCount: number;
}

export interface DELETE_MANY_FICHE_AMELIORATIONS {
  deleteManyDQFicheAmeliorations: DELETE_MANY_FICHE_AMELIORATIONS_deleteManyDQFicheAmeliorations;
}

export interface DELETE_MANY_FICHE_AMELIORATIONSVariables {
  input: DeleteManyDQFicheAmeliorationsInput;
}
