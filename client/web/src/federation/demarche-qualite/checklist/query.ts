import gql from 'graphql-tag';
import { FULL_CHECKLIST_INFO, MINIMAL_CHECKLIST_INFO } from './fragment';

export const GET_CHECKLISTS = gql`
query GET_CHECKLISTS($idPharmacie:ID,$onlyChecklistsPharmacie:Boolean) {
  dqChecklists(idPharmacie:$idPharmacie,onlyChecklistsPharmacie:$onlyChecklistsPharmacie) {
    ...ChecklistInfo
  }
}

  ${MINIMAL_CHECKLIST_INFO}
`;
export const GET_CHECKLIST = gql`
  query GET_CHECKLIST($id: ID!) {
    dqChecklist(id: $id) {
      ...ChecklistInfo
    }
  }

  ${FULL_CHECKLIST_INFO}
`;
