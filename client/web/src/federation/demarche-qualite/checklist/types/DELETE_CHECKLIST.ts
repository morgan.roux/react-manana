/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_CHECKLIST
// ====================================================

export interface DELETE_CHECKLIST_deleteDQChecklist_sections_items {
  __typename: "DQChecklistSectionItem";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
}

export interface DELETE_CHECKLIST_deleteDQChecklist_sections {
  __typename: "DQChecklistSection";
  id: string;
  ordre: number;
  libelle: string;
  items: (DELETE_CHECKLIST_deleteDQChecklist_sections_items | null)[] | null;
}

export interface DELETE_CHECKLIST_deleteDQChecklist {
  __typename: "DQChecklist";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
  createdAt: any | null;
  updatedAt: any | null;
  sections: (DELETE_CHECKLIST_deleteDQChecklist_sections | null)[] | null;
}

export interface DELETE_CHECKLIST {
  deleteDQChecklist: DELETE_CHECKLIST_deleteDQChecklist | null;
}

export interface DELETE_CHECKLISTVariables {
  id: string;
}
