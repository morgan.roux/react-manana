/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ChecklistInfo
// ====================================================

export interface ChecklistInfo_sections_items {
  __typename: "DQChecklistSectionItem";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
}

export interface ChecklistInfo_sections {
  __typename: "DQChecklistSection";
  id: string;
  ordre: number;
  libelle: string;
  items: (ChecklistInfo_sections_items | null)[] | null;
}

export interface ChecklistInfo {
  __typename: "DQChecklist";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
  createdAt: any | null;
  updatedAt: any | null;
  sections: (ChecklistInfo_sections | null)[] | null;
}
