/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_CHECKLISTS
// ====================================================

export interface GET_CHECKLISTS_dqChecklists_sections_items {
  __typename: "DQChecklistSectionItem";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
}

export interface GET_CHECKLISTS_dqChecklists_sections {
  __typename: "DQChecklistSection";
  id: string;
  ordre: number;
  libelle: string;
  items: (GET_CHECKLISTS_dqChecklists_sections_items | null)[] | null;
}

export interface GET_CHECKLISTS_dqChecklists {
  __typename: "DQChecklist";
  id: string;
  ordre: number;
  libelle: string;
  nombreEvaluations: number;
  createdAt: any | null;
  updatedAt: any | null;
  sections: (GET_CHECKLISTS_dqChecklists_sections | null)[] | null;
}

export interface GET_CHECKLISTS {
  dqChecklists: (GET_CHECKLISTS_dqChecklists | null)[] | null;
}

export interface GET_CHECKLISTSVariables {
  idPharmacie?: string | null;
  onlyChecklistsPharmacie?: boolean | null;
}
