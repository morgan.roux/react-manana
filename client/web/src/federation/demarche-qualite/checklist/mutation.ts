import gql from 'graphql-tag';
import { MINIMAL_CHECKLIST_INFO } from './fragment';

export const CREATE_CHECKLIST = gql`
  mutation CREATE_CHECKLIST($checklistInput: DQChecklistInput!) {
    createDQChecklist(checklistInput: $checklistInput) {
      ...ChecklistInfo
    }
  }
  ${MINIMAL_CHECKLIST_INFO}
`;

export const UPDATE_CHECKLIST = gql`
  mutation UPDATE_CHECKLIST($id: ID!, $checklistInput: DQChecklistInput!) {
    updateDQChecklist(id: $id, checklistInput: $checklistInput) {
      ...ChecklistInfo
    }
  }
  ${MINIMAL_CHECKLIST_INFO}
`;

export const DELETE_CHECKLIST = gql`
  mutation DELETE_CHECKLIST($id: ID!) {
    deleteDQChecklist(id: $id) {
      ...ChecklistInfo
    }
  }
  ${MINIMAL_CHECKLIST_INFO}
`;
