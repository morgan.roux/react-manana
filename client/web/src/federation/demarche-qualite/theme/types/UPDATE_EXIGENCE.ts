/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DQExigenceInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_EXIGENCE
// ====================================================

export interface UPDATE_EXIGENCE_updateDQExigence {
  __typename: "DQExigence";
  id: string;
  ordre: number | null;
  finalite: string | null;
  titre: string | null;
}

export interface UPDATE_EXIGENCE {
  updateDQExigence: UPDATE_EXIGENCE_updateDQExigence | null;
}

export interface UPDATE_EXIGENCEVariables {
  id: string;
  exigenceInput?: DQExigenceInput | null;
}
