/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_THEME
// ====================================================

export interface GET_THEME_dqTheme {
  __typename: "DQTheme";
  id: string;
  ordre: number | null;
  nom: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  nombreSousThemes: number;
}

export interface GET_THEME {
  dqTheme: GET_THEME_dqTheme | null;
}

export interface GET_THEMEVariables {
  id: string;
}
