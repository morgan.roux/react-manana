/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SousThemeInfo
// ====================================================

export interface SousThemeInfo_exigences {
  __typename: "DQExigence";
  id: string;
  ordre: number | null;
  finalite: string | null;
  titre: string | null;
  nombreOutils: number;
}

export interface SousThemeInfo {
  __typename: "DQSousTheme";
  id: string;
  ordre: number | null;
  nom: string | null;
  createdAt: any | null;
  updatedAt: any | null;
  nombreExigences: number;
  exigences: (SousThemeInfo_exigences | null)[] | null;
}
