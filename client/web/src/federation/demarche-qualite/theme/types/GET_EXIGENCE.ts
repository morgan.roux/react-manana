/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_EXIGENCE
// ====================================================

export interface GET_EXIGENCE_dqExigence {
  __typename: "DQExigence";
  id: string;
  ordre: number | null;
  finalite: string | null;
  titre: string | null;
}

export interface GET_EXIGENCE {
  dqExigence: GET_EXIGENCE_dqExigence | null;
}

export interface GET_EXIGENCEVariables {
  id: string;
}
