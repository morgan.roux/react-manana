import gql from 'graphql-tag';

import {
  MINIMAL_THEME_INFO,
  MINIMAL_SOUS_THEME_INFO,
  FULL_EXIGENCE_INFO,
  MINIMAL_EXIGENCE_INFO,
  FULL_THEME_INFO,
} from './fragment';

export const CREATE_THEME = gql`
  mutation CREATE_THEME($themeInput: DQThemeInput!) {
    createDQTheme(themeInput: $themeInput) {
      ...ThemeInfo
    }
  }
  ${FULL_THEME_INFO}
`;

export const UPDATE_THEME = gql`
  mutation UPDATE_THEME($id: ID!, $themeInput: DQThemeInput!) {
    updateDQTheme(id: $id, themeInput: $themeInput) {
      ...ThemeInfo
    }
  }
  ${FULL_THEME_INFO}
`;

export const DELETE_THEME = gql`
  mutation DELETE_THEME($id: ID!) {
    deleteDQTheme(id: $id) {
      ...ThemeInfo
    }
  }
  ${MINIMAL_THEME_INFO}
`;

export const CREATE_SOUS_THEME = gql`
  mutation CREATE_SOUS_THEME($idTheme: ID!, $sousThemeInput: DQSousThemeInput!) {
    createDQSousTheme(idTheme: $idTheme, sousThemeInput: $sousThemeInput) {
      ...SousThemeInfo
    }
  }
  ${MINIMAL_SOUS_THEME_INFO}
`;

export const UPDATE_SOUS_THEME = gql`
  mutation UPDATE_SOUS_THEME($id: ID!, $sousThemeInput: DQSousThemeInput!) {
    updateDQSousTheme(id: $id, sousThemeInput: $sousThemeInput) {
      ...SousThemeInfo
    }
  }
  ${MINIMAL_SOUS_THEME_INFO}
`;

export const DELETE_SOUS_THEME = gql`
  mutation deleteDQSousTheme($id: ID!) {
    deleteDQSousTheme(id: $id) {
      ...SousThemeInfo
    }
  }
  ${MINIMAL_SOUS_THEME_INFO}
`;

export const CREATE_EXIGENCE = gql`
  mutation CREATE_EXIGENCE($idSousTheme: ID!, $exigenceInput: DQExigenceInput) {
    createDQExigence(idSousTheme: $idSousTheme, exigenceInput: $exigenceInput) {
      ...ExigenceInfo
    }
  }
  ${FULL_EXIGENCE_INFO}
`;

export const UPDATE_EXIGENCE = gql`
  mutation UPDATE_EXIGENCE($id: ID!, $exigenceInput: DQExigenceInput) {
    updateDQExigence(id: $id, exigenceInput: $exigenceInput) {
      ...ExigenceInfo
    }
  }
  ${FULL_EXIGENCE_INFO}
`;

export const DELETE_EXIGENCE = gql`
  mutation DELETE_EXIGENCE($id: ID!) {
    deleteDQExigence(id: $id) {
      ...ExigenceInfo
    }
  }
  ${MINIMAL_EXIGENCE_INFO}
`;
