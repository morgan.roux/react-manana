import gql from 'graphql-tag';
import { FULL_DOCUMENT_CATEGORIE_INFO, SEARCH_DOCUMENT_RESULT } from './fragment';

export const GET_DOCUMENTS_CATEGORIE = gql`
  query GET_DOCUMENTS_CATEGORIE(
    $paging: OffsetPaging
    $filter: GedDocumentFilter
    $sorting: [GedDocumentSort!]
    $idUser: String
  ) {
    gedDocuments(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        id
        description
        nomenclature
        numeroVersion
        motCle1
        motCle2
        motCle3
        dateHeureParution
        dateHeureDebutValidite
        dateHeureFinValidite
        idDocumentARemplacer
        idUserRedacteur
        idUserVerificateur
        idFichier
        idSousCategorie
        idPharmacie
        idGroupement
        favoris
        createdAt
        updatedAt
        createdBy {
          id
        }
        nombreConsultations
        nombreTelechargements
        nombreCommentaires
        nombreReactions
        sousCategorie {
          id
          libelle
        }
        fichier {
          id
          chemin
          nomOriginal
          type
          publicUrl
        }
        verificateur {
          id
          fullName
        }
        redacteur {
          id
          fullName
        }
        categorie {
          id
          libelle
        }
        dernierChangementStatut(idUser: $idUser) {
          id
          idDocument
          status
          commentaire
          idGroupement
          createdBy {
            id
          }
          updatedBy {
            id
          }
          createdAt
          updatedAt
        }
        documentARemplacer {
          id
          description
          nomenclature
          numeroVersion
          motCle1
          motCle2
          motCle3
          dateHeureParution
          dateHeureDebutValidite
          dateHeureFinValidite
          idDocumentARemplacer
          idUserRedacteur
          idUserVerificateur
          idFichier
          idSousCategorie
          idPharmacie
          idGroupement
          createdAt
          updatedAt
          nombreConsultations
          nombreTelechargements
          nombreCommentaires
          nombreReactions
          createdBy {
            id
          }
          sousCategorie {
            id
            libelle
          }
          fichier {
            id
            chemin
            nomOriginal
            type
            publicUrl
          }
          verificateur {
            id
            fullName
          }
          redacteur {
            id
            fullName
          }
          categorie {
            id
            libelle
          }
          dernierChangementStatut {
            id
            idDocument
            status
            commentaire
            idGroupement
            createdBy {
              id
            }
            updatedBy {
              id
            }
            createdAt
            updatedAt
          }
        }
      }
    }
  }
`;

export const GET_DOCUMENT_CATEGORIE = gql`
  query GET_DOCUMENT_CATEGORIE($id: ID!) {
    gedDocument(id: $id) {
      ...DocumentCategorie
    }
  }

  ${FULL_DOCUMENT_CATEGORIE_INFO}
`;

export const GET_GET_DOCUMENT_FROM_EMAIL = gql`
  query GET_GET_DOCUMENT_FROM_EMAIL {
    getEmailData
  }
`;

export const GET_DOCUMENTS = gql`
  query GET_DOCUMENTS(
    $paging: OffsetPaging
    $filter: GedDocumentFilter
    $sorting: [GedDocumentSort!]
  ) {
    gedDocuments(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        ...DocumentCategorie
      }
      totalCount
    }
  }

  ${FULL_DOCUMENT_CATEGORIE_INFO}
`;

export const SEARCH_DOCUMENT = gql`
  query SEARCH_DOCUMENT($input: GedSearchDocumentInput!) {
    searchGedDocuments(input: $input) {
      ...SearchDocumentResult
    }
  }

  ${SEARCH_DOCUMENT_RESULT}
`;
export const GET_DOCUMENT_CHANGEMENT_STATUS = gql`
  query GET_DOCUMENT_CHANGEMENT_STATUS(
    $paging: OffsetPaging
    $filter: GedDocumentChangementStatutFilter
    $sorting: [GedDocumentChangementStatutSort!]
  ) {
    gedDocumentChangementStatuts(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes {
        id
        idDocument
        status
        commentaire
      }
    }
  }
`;

export const GET_OCR_IS_CONFIGURE = gql`
  query OCR_IS_CONFIGURE {
    oCRIsConfigure
  }
`;

export const GET_OCR_PHARMACIE_EMAIL_DATA = gql`
  query OCR_PHARMACIE_EMAIL_DATA {
    oCRPharmacieEmailData
  }
`;

export const GET_OCR_DOCUMENTS = gql`
  query OCR_DOCUMENTS($idDocument: String) {
    oCRDocuments(idDocument: $idDocument) {
      ...DocumentCategorie
    }
  }

  ${FULL_DOCUMENT_CATEGORIE_INFO}
`;

export const GET_GED_DOCUMENT_FOURNISSEURS = gql`
  query GED_DOCUMENT_FOURNISSEURS($input: GedDocumentFournisseurInput!) {
    gedDocumentFournisseurs(input: $input) {
      ... on Laboratoire {
        id
        nom
        dataType
      }
      ... on PrestataireService {
        id
        nom
        dataType
      }
    }
  }
`;

export const GET_GED_CATEGORIES_ANNEE_MOIS = gql`
  query GET_GED_CATEGORIES_ANNEE_MOIS($input: GedCategorieAnneeMoisInput!) {
    gedCategoriesAnneeMois(input: $input) {
      annee
      mois
    }
  }
`;

export const GET_TOTAUX_FACTURES = gql`
  query TOTAUX_FACTURES(
    $idOrigine: String!
    $idPharmacie: String!
    $idOrigineAssocie: String!
    $noAvoir: Boolean
    $searchText: String
  ) {
    getTotauxFactures(
      idPharmacie: $idPharmacie
      idOrigine: $idOrigine
      idOrigineAssocie: $idOrigineAssocie
      noAvoir: $noAvoir
      searchText: $searchText
    ) {
      totalHT
      totalTVA
      totalTTC
    }
  }
`;

export const GET_MAX_FACTURE_TOTAL_TTC = gql`
  query MAX_FACTURE_TOTAL_TTC($input: GedSearchDocumentInput!) {
    maxFactureTotalTtc(input: $input)
  }
`;
