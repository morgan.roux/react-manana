/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: OCR_IS_CONFIGURE
// ====================================================

export interface OCR_IS_CONFIGURE {
  oCRIsConfigure: boolean;
}
