/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TOTAUX_FACTURES
// ====================================================

export interface TOTAUX_FACTURES_getTotauxFactures {
  __typename: "GedTotauxFacture";
  totalHT: number | null;
  totalTVA: number | null;
  totalTTC: number | null;
}

export interface TOTAUX_FACTURES {
  getTotauxFactures: TOTAUX_FACTURES_getTotauxFactures | null;
}

export interface TOTAUX_FACTURESVariables {
  idOrigine: string;
  idPharmacie: string;
  idOrigineAssocie: string;
  noAvoir?: boolean | null;
  searchText?: string | null;
}
