/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: TOGGLE_DOCUMENT_TO_FAVORITE
// ====================================================

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
}

export type TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_origineAssocie = TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_origineAssocie_Laboratoire | TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_origineAssocie_PrestataireService | TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_origineAssocie_User;

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_facturesTva {
  __typename: "GedFactureTva";
  idDocument: string;
  idTva: string;
  montantHt: number;
  montantTva: number;
  montantTtc: number;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
  presignedUrl: string | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_dernierChangementStatut_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_dernierChangementStatut_createdBy_prestataireService | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_dernierChangementStatut_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_dernierChangementStatut_updatedBy_prestataireService | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_dernierChangementStatut_createdBy | null;
  updatedBy: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_changementStatuts_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_changementStatuts_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_changementStatuts_createdBy_prestataireService | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_changementStatuts_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_changementStatuts_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_changementStatuts_updatedBy_prestataireService | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_changementStatuts {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_changementStatuts_createdBy | null;
  updatedBy: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_changementStatuts_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_facturesTva {
  __typename: "GedFactureTva";
  idDocument: string;
  idTva: string;
  montantHt: number;
  montantTva: number;
  montantTtc: number;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_createdBy {
  __typename: "User";
  id: string;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_dernierChangementStatut_createdBy | null;
  updatedBy: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer {
  __typename: "GedDocument";
  id: string;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  createdAt: any;
  updatedAt: any;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  nombreJoursPreavis: number | null;
  isRenouvellementTacite: boolean | null;
  isMultipleTva: boolean | null;
  facturesTva: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_facturesTva[] | null;
  typeAvoirAssociations: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_typeAvoirAssociations[] | null;
  commande: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_commande | null;
  idCommandes: string[] | null;
  type: string | null;
  createdBy: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_createdBy;
  sousCategorie: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_sousCategorie | null;
  fichier: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_fichier;
  verificateur: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_verificateur | null;
  redacteur: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_redacteur | null;
  categorie: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_categorie | null;
  dernierChangementStatut: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer_dernierChangementStatut | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris {
  __typename: "GedDocument";
  id: string;
  type: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_origineAssocie | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  nombreJoursPreavis: number | null;
  isRenouvellementTacite: boolean | null;
  isMultipleTva: boolean | null;
  facturesTva: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_facturesTva[] | null;
  typeAvoirAssociations: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_typeAvoirAssociations[] | null;
  commande: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_commande | null;
  idCommandes: string[] | null;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  favoris: boolean;
  createdAt: any;
  updatedAt: any;
  isOcr: boolean | null;
  createdBy: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_createdBy;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  sousCategorie: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_sousCategorie | null;
  fichier: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_fichier;
  verificateur: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_verificateur | null;
  redacteur: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_redacteur | null;
  categorie: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_categorie | null;
  dernierChangementStatut: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_dernierChangementStatut | null;
  changementStatuts: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_changementStatuts[] | null;
  documentARemplacer: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris_documentARemplacer | null;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITE {
  toggleToGedDocumentFavoris: TOGGLE_DOCUMENT_TO_FAVORITE_toggleToGedDocumentFavoris;
}

export interface TOGGLE_DOCUMENT_TO_FAVORITEVariables {
  idDocument: string;
}
