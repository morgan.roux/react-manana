/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GedSearchDocumentInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: MAX_FACTURE_TOTAL_TTC
// ====================================================

export interface MAX_FACTURE_TOTAL_TTC {
  maxFactureTotalTtc: number | null;
}

export interface MAX_FACTURE_TOTAL_TTCVariables {
  input: GedSearchDocumentInput;
}
