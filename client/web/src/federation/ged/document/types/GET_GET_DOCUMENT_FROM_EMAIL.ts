/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_GET_DOCUMENT_FROM_EMAIL
// ====================================================

export interface GET_GET_DOCUMENT_FROM_EMAIL {
  getEmailData: number;
}
