/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, GedDocumentFilter, GedDocumentSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_DOCUMENTS
// ====================================================

export interface GET_DOCUMENTS_gedDocuments_nodes_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
}

export type GET_DOCUMENTS_gedDocuments_nodes_origineAssocie = GET_DOCUMENTS_gedDocuments_nodes_origineAssocie_Laboratoire | GET_DOCUMENTS_gedDocuments_nodes_origineAssocie_PrestataireService | GET_DOCUMENTS_gedDocuments_nodes_origineAssocie_User;

export interface GET_DOCUMENTS_gedDocuments_nodes_facturesTva {
  __typename: "GedFactureTva";
  idDocument: string;
  idTva: string;
  montantHt: number;
  montantTva: number;
  montantTtc: number;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
  presignedUrl: string | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_dernierChangementStatut_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: GET_DOCUMENTS_gedDocuments_nodes_dernierChangementStatut_createdBy_prestataireService | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_dernierChangementStatut_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: GET_DOCUMENTS_gedDocuments_nodes_dernierChangementStatut_updatedBy_prestataireService | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: GET_DOCUMENTS_gedDocuments_nodes_dernierChangementStatut_createdBy | null;
  updatedBy: GET_DOCUMENTS_gedDocuments_nodes_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_changementStatuts_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_changementStatuts_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: GET_DOCUMENTS_gedDocuments_nodes_changementStatuts_createdBy_prestataireService | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_changementStatuts_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_changementStatuts_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: GET_DOCUMENTS_gedDocuments_nodes_changementStatuts_updatedBy_prestataireService | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_changementStatuts {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: GET_DOCUMENTS_gedDocuments_nodes_changementStatuts_createdBy | null;
  updatedBy: GET_DOCUMENTS_gedDocuments_nodes_changementStatuts_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_facturesTva {
  __typename: "GedFactureTva";
  idDocument: string;
  idTva: string;
  montantHt: number;
  montantTva: number;
  montantTtc: number;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_createdBy {
  __typename: "User";
  id: string;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_dernierChangementStatut_createdBy | null;
  updatedBy: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer {
  __typename: "GedDocument";
  id: string;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  createdAt: any;
  updatedAt: any;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  nombreJoursPreavis: number | null;
  isRenouvellementTacite: boolean | null;
  isMultipleTva: boolean | null;
  facturesTva: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_facturesTva[] | null;
  typeAvoirAssociations: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_typeAvoirAssociations[] | null;
  commande: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_commande | null;
  idCommandes: string[] | null;
  type: string | null;
  createdBy: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_createdBy;
  sousCategorie: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_sousCategorie | null;
  fichier: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_fichier;
  verificateur: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_verificateur | null;
  redacteur: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_redacteur | null;
  categorie: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_categorie | null;
  dernierChangementStatut: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer_dernierChangementStatut | null;
}

export interface GET_DOCUMENTS_gedDocuments_nodes {
  __typename: "GedDocument";
  id: string;
  type: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: GET_DOCUMENTS_gedDocuments_nodes_origineAssocie | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  nombreJoursPreavis: number | null;
  isRenouvellementTacite: boolean | null;
  isMultipleTva: boolean | null;
  facturesTva: GET_DOCUMENTS_gedDocuments_nodes_facturesTva[] | null;
  typeAvoirAssociations: GET_DOCUMENTS_gedDocuments_nodes_typeAvoirAssociations[] | null;
  commande: GET_DOCUMENTS_gedDocuments_nodes_commande | null;
  idCommandes: string[] | null;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  favoris: boolean;
  createdAt: any;
  updatedAt: any;
  isOcr: boolean | null;
  createdBy: GET_DOCUMENTS_gedDocuments_nodes_createdBy;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  sousCategorie: GET_DOCUMENTS_gedDocuments_nodes_sousCategorie | null;
  fichier: GET_DOCUMENTS_gedDocuments_nodes_fichier;
  verificateur: GET_DOCUMENTS_gedDocuments_nodes_verificateur | null;
  redacteur: GET_DOCUMENTS_gedDocuments_nodes_redacteur | null;
  categorie: GET_DOCUMENTS_gedDocuments_nodes_categorie | null;
  dernierChangementStatut: GET_DOCUMENTS_gedDocuments_nodes_dernierChangementStatut | null;
  changementStatuts: GET_DOCUMENTS_gedDocuments_nodes_changementStatuts[] | null;
  documentARemplacer: GET_DOCUMENTS_gedDocuments_nodes_documentARemplacer | null;
}

export interface GET_DOCUMENTS_gedDocuments {
  __typename: "GedDocumentConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_DOCUMENTS_gedDocuments_nodes[];
  /**
   * Fetch total count of records
   */
  totalCount: number;
}

export interface GET_DOCUMENTS {
  gedDocuments: GET_DOCUMENTS_gedDocuments;
}

export interface GET_DOCUMENTSVariables {
  paging?: OffsetPaging | null;
  filter?: GedDocumentFilter | null;
  sorting?: GedDocumentSort[] | null;
}
