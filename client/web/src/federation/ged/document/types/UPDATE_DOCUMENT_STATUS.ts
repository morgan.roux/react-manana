/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GedDocumentChangementStatutInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_DOCUMENT_STATUS
// ====================================================

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  dataType: string;
  nom: string;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_origineAssocie_PrestataireService {
  __typename: "PrestataireService";
  id: string;
  dataType: string;
  nom: string;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_origineAssocie_User {
  __typename: "User";
  id: string;
  dataType: string;
  fullName: string | null;
}

export type UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_origineAssocie = UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_origineAssocie_Laboratoire | UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_origineAssocie_PrestataireService | UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_origineAssocie_User;

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_facturesTva {
  __typename: "GedFactureTva";
  idDocument: string;
  idTva: string;
  montantHt: number;
  montantTva: number;
  montantTtc: number;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
  presignedUrl: string | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_dernierChangementStatut_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_dernierChangementStatut_createdBy_prestataireService | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_dernierChangementStatut_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_dernierChangementStatut_updatedBy_prestataireService | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_dernierChangementStatut_createdBy | null;
  updatedBy: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_changementStatuts_createdBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_changementStatuts_createdBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_changementStatuts_createdBy_prestataireService | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_changementStatuts_updatedBy_prestataireService {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_changementStatuts_updatedBy {
  __typename: "User";
  id: string;
  fullName: string | null;
  prestataireService: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_changementStatuts_updatedBy_prestataireService | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_changementStatuts {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_changementStatuts_createdBy | null;
  updatedBy: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_changementStatuts_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_facturesTva {
  __typename: "GedFactureTva";
  idDocument: string;
  idTva: string;
  montantHt: number;
  montantTva: number;
  montantTtc: number;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_typeAvoirAssociations {
  __typename: "GedDocumentAvoirAssociation";
  idDocument: string;
  type: string;
  correspondant: string;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_commande {
  __typename: "PRTCommande";
  id: string;
  nbrRef: number | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_createdBy {
  __typename: "User";
  id: string;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_dernierChangementStatut_createdBy | null;
  updatedBy: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer {
  __typename: "GedDocument";
  id: string;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  createdAt: any;
  updatedAt: any;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  nombreJoursPreavis: number | null;
  isRenouvellementTacite: boolean | null;
  isMultipleTva: boolean | null;
  facturesTva: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_facturesTva[] | null;
  typeAvoirAssociations: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_typeAvoirAssociations[] | null;
  commande: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_commande | null;
  idCommandes: string[] | null;
  type: string | null;
  createdBy: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_createdBy;
  sousCategorie: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_sousCategorie | null;
  fichier: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_fichier;
  verificateur: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_verificateur | null;
  redacteur: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_redacteur | null;
  categorie: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_categorie | null;
  dernierChangementStatut: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer_dernierChangementStatut | null;
}

export interface UPDATE_DOCUMENT_STATUS_updateStatutGedDocument {
  __typename: "GedDocument";
  id: string;
  type: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_origineAssocie | null;
  factureTotalHt: number | null;
  factureTotalTtc: number | null;
  factureTva: number | null;
  factureDate: any | null;
  factureDateReglement: any | null;
  idReglementMode: string | null;
  numeroFacture: string | null;
  isGenererCommande: boolean | null;
  numeroCommande: number | null;
  nombreJoursPreavis: number | null;
  isRenouvellementTacite: boolean | null;
  isMultipleTva: boolean | null;
  facturesTva: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_facturesTva[] | null;
  typeAvoirAssociations: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_typeAvoirAssociations[] | null;
  commande: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_commande | null;
  idCommandes: string[] | null;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  favoris: boolean;
  createdAt: any;
  updatedAt: any;
  isOcr: boolean | null;
  createdBy: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_createdBy;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  sousCategorie: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_sousCategorie | null;
  fichier: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_fichier;
  verificateur: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_verificateur | null;
  redacteur: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_redacteur | null;
  categorie: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_categorie | null;
  dernierChangementStatut: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_dernierChangementStatut | null;
  changementStatuts: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_changementStatuts[] | null;
  documentARemplacer: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument_documentARemplacer | null;
}

export interface UPDATE_DOCUMENT_STATUS {
  updateStatutGedDocument: UPDATE_DOCUMENT_STATUS_updateStatutGedDocument;
}

export interface UPDATE_DOCUMENT_STATUSVariables {
  input: GedDocumentChangementStatutInput;
}
