/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, GedDocumentFilter, GedDocumentSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_DOCUMENTS_CATEGORIE
// ====================================================

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_createdBy {
  __typename: "User";
  id: string;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_dernierChangementStatut_createdBy | null;
  updatedBy: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_createdBy {
  __typename: "User";
  id: string;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_sousCategorie {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_verificateur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_redacteur {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_categorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_dernierChangementStatut_createdBy {
  __typename: "User";
  id: string;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_dernierChangementStatut_updatedBy {
  __typename: "User";
  id: string;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_dernierChangementStatut {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
  idGroupement: string;
  createdBy: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_dernierChangementStatut_createdBy | null;
  updatedBy: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_dernierChangementStatut_updatedBy | null;
  createdAt: any;
  updatedAt: any;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer {
  __typename: "GedDocument";
  id: string;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  createdAt: any;
  updatedAt: any;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  createdBy: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_createdBy;
  sousCategorie: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_sousCategorie | null;
  fichier: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_fichier;
  verificateur: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_verificateur | null;
  redacteur: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_redacteur | null;
  categorie: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_categorie | null;
  dernierChangementStatut: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer_dernierChangementStatut | null;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes {
  __typename: "GedDocument";
  id: string;
  description: string;
  nomenclature: string | null;
  numeroVersion: string | null;
  motCle1: string | null;
  motCle2: string | null;
  motCle3: string | null;
  dateHeureParution: any | null;
  dateHeureDebutValidite: any | null;
  dateHeureFinValidite: any | null;
  idDocumentARemplacer: string | null;
  idUserRedacteur: string | null;
  idUserVerificateur: string | null;
  idFichier: string;
  idSousCategorie: string | null;
  idPharmacie: string;
  idGroupement: string;
  favoris: boolean;
  createdAt: any;
  updatedAt: any;
  createdBy: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_createdBy;
  nombreConsultations: number;
  nombreTelechargements: number;
  nombreCommentaires: number;
  nombreReactions: number;
  sousCategorie: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_sousCategorie | null;
  fichier: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_fichier;
  verificateur: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_verificateur | null;
  redacteur: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_redacteur | null;
  categorie: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_categorie | null;
  dernierChangementStatut: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_dernierChangementStatut | null;
  documentARemplacer: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes_documentARemplacer | null;
}

export interface GET_DOCUMENTS_CATEGORIE_gedDocuments {
  __typename: "GedDocumentConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_DOCUMENTS_CATEGORIE_gedDocuments_nodes[];
}

export interface GET_DOCUMENTS_CATEGORIE {
  gedDocuments: GET_DOCUMENTS_CATEGORIE_gedDocuments;
}

export interface GET_DOCUMENTS_CATEGORIEVariables {
  paging?: OffsetPaging | null;
  filter?: GedDocumentFilter | null;
  sorting?: GedDocumentSort[] | null;
  idUser?: string | null;
}
