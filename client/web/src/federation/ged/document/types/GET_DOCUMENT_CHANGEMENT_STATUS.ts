/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, GedDocumentChangementStatutFilter, GedDocumentChangementStatutSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_DOCUMENT_CHANGEMENT_STATUS
// ====================================================

export interface GET_DOCUMENT_CHANGEMENT_STATUS_gedDocumentChangementStatuts_nodes {
  __typename: "GedDocumentChangementStatut";
  id: string;
  idDocument: string;
  status: string;
  commentaire: string;
}

export interface GET_DOCUMENT_CHANGEMENT_STATUS_gedDocumentChangementStatuts {
  __typename: "GedDocumentChangementStatutConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_DOCUMENT_CHANGEMENT_STATUS_gedDocumentChangementStatuts_nodes[];
}

export interface GET_DOCUMENT_CHANGEMENT_STATUS {
  gedDocumentChangementStatuts: GET_DOCUMENT_CHANGEMENT_STATUS_gedDocumentChangementStatuts;
}

export interface GET_DOCUMENT_CHANGEMENT_STATUSVariables {
  paging?: OffsetPaging | null;
  filter?: GedDocumentChangementStatutFilter | null;
  sorting?: GedDocumentChangementStatutSort[] | null;
}
