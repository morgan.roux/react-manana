import gql from 'graphql-tag';

export const FULL_DOCUMENT_CATEGORIE_INFO = gql`
  fragment DocumentCategorie on GedDocument {
    id
    type
    idOrigine
    idOrigineAssocie
    origineAssocie {
      ... on Laboratoire {
        id
        dataType
        nom
      }
      ... on PrestataireService {
        id
        dataType
        nom
      }
      ... on User {
        id
        dataType
        fullName
      }
    }
    factureTotalHt
    factureTotalTtc
    factureTva
    factureDate
    factureDateReglement
    idReglementMode
    numeroFacture
    isGenererCommande
    numeroCommande
    nombreJoursPreavis
    isRenouvellementTacite
    isMultipleTva
    facturesTva {
      idDocument
      idTva
      montantHt
      montantTva
      montantTtc
    }
    typeAvoirAssociations {
      idDocument
      type
      correspondant
    }
    commande {
      id
      nbrRef
    }
    idCommandes
    description
    nomenclature
    numeroVersion
    motCle1
    motCle2
    motCle3
    dateHeureParution
    dateHeureDebutValidite
    dateHeureFinValidite
    idDocumentARemplacer
    idUserRedacteur
    idUserVerificateur
    idFichier
    idSousCategorie
    idPharmacie
    idGroupement
    favoris
    createdAt
    updatedAt
    isOcr
    createdBy {
      id
      fullName
    }
    nombreConsultations
    nombreTelechargements
    nombreCommentaires
    nombreReactions
    sousCategorie {
      id
      libelle
      workflowValidation
      idPartenaireValidateur
    }
    fichier {
      id
      chemin
      nomOriginal
      type
      publicUrl
      presignedUrl
    }
    verificateur {
      id
      fullName
    }
    redacteur {
      id
      fullName
    }
    categorie {
      id
      libelle
      workflowValidation
      idPartenaireValidateur
    }
    dernierChangementStatut {
      id
      idDocument
      status
      commentaire
      idGroupement
      createdBy {
        id
        fullName
        prestataireService {
          id
          nom
        }
      }
      updatedBy {
        id
        fullName
        prestataireService {
          id
          nom
        }
      }
      createdAt
      updatedAt
    }
    changementStatuts {
      id
      idDocument
      status
      commentaire
      idGroupement
      createdBy {
        id
        fullName
        prestataireService {
          id
          nom
        }
      }
      updatedBy {
        id
        fullName
        prestataireService {
          id
          nom
        }
      }
      createdAt
      updatedAt
    }
    documentARemplacer {
      id
      description
      nomenclature
      numeroVersion
      motCle1
      motCle2
      motCle3
      dateHeureParution
      dateHeureDebutValidite
      dateHeureFinValidite
      idDocumentARemplacer
      idUserRedacteur
      idUserVerificateur
      idFichier
      idSousCategorie
      idPharmacie
      idGroupement
      createdAt
      updatedAt
      nombreConsultations
      nombreTelechargements
      nombreCommentaires
      nombreReactions
      idOrigine
      idOrigineAssocie
      factureTotalHt
      factureTotalTtc
      factureTva
      factureDate
      factureDateReglement
      idReglementMode
      numeroFacture
      isGenererCommande
      numeroCommande
      nombreJoursPreavis
      isRenouvellementTacite
      isMultipleTva
      facturesTva {
        idDocument
        idTva
        montantHt
        montantTva
        montantTtc
      }
      typeAvoirAssociations {
        idDocument
        type
        correspondant
      }
      commande {
        id
        nbrRef
      }
      idCommandes
      type
      createdBy {
        id
      }
      sousCategorie {
        id
        libelle
        workflowValidation
        idPartenaireValidateur
      }
      fichier {
        id
        chemin
        nomOriginal
        type
        publicUrl
      }
      verificateur {
        id
        fullName
      }
      redacteur {
        id
        fullName
      }
      categorie {
        id
        libelle
      }
      dernierChangementStatut {
        id
        idDocument
        status
        commentaire
        idGroupement
        createdBy {
          id
        }
        updatedBy {
          id
        }
        createdAt
        updatedAt
      }
    }
  }
`;

export const SEARCH_DOCUMENT_RESULT = gql`
  fragment SearchDocumentResult on GedSearchDocumentResult {
    total
    data {
      ...DocumentCategorie
    }
  }
  ${FULL_DOCUMENT_CATEGORIE_INFO}
`;
