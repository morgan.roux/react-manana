/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_MES_CATEGORIES
// ====================================================

export interface GET_MES_CATEGORIES_gedMesCategories_mesSousCategories_partenaireValidateur {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface GET_MES_CATEGORIES_gedMesCategories_mesSousCategories_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface GET_MES_CATEGORIES_gedMesCategories_mesSousCategories {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
  nombreDocuments: number;
  partenaireValidateur: GET_MES_CATEGORIES_gedMesCategories_mesSousCategories_partenaireValidateur | null;
  participants: GET_MES_CATEGORIES_gedMesCategories_mesSousCategories_participants[];
}

export interface GET_MES_CATEGORIES_gedMesCategories {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  base: boolean;
  idGroupement: string;
  idPharmacie: string | null;
  createdAt: any;
  updatedAt: any;
  type: string | null;
  mesSousCategories: GET_MES_CATEGORIES_gedMesCategories_mesSousCategories[];
}

export interface GET_MES_CATEGORIES {
  gedMesCategories: GET_MES_CATEGORIES_gedMesCategories[];
}
