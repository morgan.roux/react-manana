/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GedSousCategorieInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_SOUS_CATEGORIE
// ====================================================

export interface CREATE_SOUS_CATEGORIE_createOneGedSousCategorie {
  __typename: "GedSousCategorie";
  id: string;
}

export interface CREATE_SOUS_CATEGORIE {
  createOneGedSousCategorie: CREATE_SOUS_CATEGORIE_createOneGedSousCategorie;
}

export interface CREATE_SOUS_CATEGORIEVariables {
  input: GedSousCategorieInput;
}
