/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GedCategorieInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_CATEGORIE
// ====================================================

export interface CREATE_CATEGORIE_createOneGedCategorie_mesSousCategories_partenaireValidateur {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface CREATE_CATEGORIE_createOneGedCategorie_mesSousCategories_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface CREATE_CATEGORIE_createOneGedCategorie_mesSousCategories {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
  nombreDocuments: number;
  partenaireValidateur: CREATE_CATEGORIE_createOneGedCategorie_mesSousCategories_partenaireValidateur | null;
  participants: CREATE_CATEGORIE_createOneGedCategorie_mesSousCategories_participants[];
}

export interface CREATE_CATEGORIE_createOneGedCategorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  base: boolean;
  idGroupement: string;
  idPharmacie: string | null;
  createdAt: any;
  updatedAt: any;
  type: string | null;
  mesSousCategories: CREATE_CATEGORIE_createOneGedCategorie_mesSousCategories[];
}

export interface CREATE_CATEGORIE {
  createOneGedCategorie: CREATE_CATEGORIE_createOneGedCategorie;
}

export interface CREATE_CATEGORIEVariables {
  categoryInput: GedCategorieInput;
}
