/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GedCategorieInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_CATEGORIE
// ====================================================

export interface UPDATE_CATEGORIE_updateOneGedCategorie_mesSousCategories_partenaireValidateur {
  __typename: "PrestataireService";
  id: string;
  nom: string;
}

export interface UPDATE_CATEGORIE_updateOneGedCategorie_mesSousCategories_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
}

export interface UPDATE_CATEGORIE_updateOneGedCategorie_mesSousCategories {
  __typename: "GedSousCategorie";
  id: string;
  libelle: string;
  workflowValidation: string | null;
  idPartenaireValidateur: string | null;
  nombreDocuments: number;
  partenaireValidateur: UPDATE_CATEGORIE_updateOneGedCategorie_mesSousCategories_partenaireValidateur | null;
  participants: UPDATE_CATEGORIE_updateOneGedCategorie_mesSousCategories_participants[];
}

export interface UPDATE_CATEGORIE_updateOneGedCategorie {
  __typename: "GedCategorie";
  id: string;
  libelle: string;
  base: boolean;
  idGroupement: string;
  idPharmacie: string | null;
  createdAt: any;
  updatedAt: any;
  type: string | null;
  mesSousCategories: UPDATE_CATEGORIE_updateOneGedCategorie_mesSousCategories[];
}

export interface UPDATE_CATEGORIE {
  updateOneGedCategorie: UPDATE_CATEGORIE_updateOneGedCategorie;
}

export interface UPDATE_CATEGORIEVariables {
  categoryInput: GedCategorieInput;
  id: string;
}
