import gql from 'graphql-tag';
import { FULL_OPTION_PARTAGE_INFO, OPTION_PARTAGE_TYPE } from './fragment';

export const GET_OPTION_PARTAGES = gql`
    query GET_OPTION_PARTAGES($paging: OffsetPaging, $filter: OptionPartageFilter, $sorting: [OptionPartageSort!] ){
        optionPartages(paging: $paging, filter: $filter, sorting: $sorting){
            nodes{
            ...OptionPartageInfo
            }
        }
    }

    ${FULL_OPTION_PARTAGE_INFO}
`;

export const GET_OPTION_PARTAGE_TYPES = gql`
    query GET_OPTION_PARTAGE_TYPES($paging: OffsetPaging, $filter: OptionPartageTypeFilter, $sorting: [OptionPartageTypeSort!] ){
        optionPartageTypes(paging: $paging, filter: $filter, sorting: $sorting){
            nodes{
            ...OptionPartageTypeInfo
            }
        }
    }

    ${OPTION_PARTAGE_TYPE}
`;
