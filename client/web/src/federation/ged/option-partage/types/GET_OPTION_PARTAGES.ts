/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, OptionPartageFilter, OptionPartageSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_OPTION_PARTAGES
// ====================================================

export interface GET_OPTION_PARTAGES_optionPartages_nodes_type {
  __typename: "OptionPartageType";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_OPTION_PARTAGES_optionPartages_nodes {
  __typename: "OptionPartage";
  id: string;
  idParametre: string;
  idPharmacie: string;
  idType: string;
  type: GET_OPTION_PARTAGES_optionPartages_nodes_type | null;
}

export interface GET_OPTION_PARTAGES_optionPartages {
  __typename: "OptionPartageConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_OPTION_PARTAGES_optionPartages_nodes[];
}

export interface GET_OPTION_PARTAGES {
  optionPartages: GET_OPTION_PARTAGES_optionPartages;
}

export interface GET_OPTION_PARTAGESVariables {
  paging?: OffsetPaging | null;
  filter?: OptionPartageFilter | null;
  sorting?: OptionPartageSort[] | null;
}
