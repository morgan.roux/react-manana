/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: OptionPartageInfo
// ====================================================

export interface OptionPartageInfo_type {
  __typename: "OptionPartageType";
  id: string;
  code: string;
  libelle: string;
}

export interface OptionPartageInfo {
  __typename: "OptionPartage";
  id: string;
  idParametre: string;
  idPharmacie: string;
  idType: string;
  type: OptionPartageInfo_type | null;
}
