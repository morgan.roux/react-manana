/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OptionPartageInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_OPTION_PARTAGE
// ====================================================

export interface CREATE_OPTION_PARTAGE_createOneOptionPartage_type {
  __typename: "OptionPartageType";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_OPTION_PARTAGE_createOneOptionPartage {
  __typename: "OptionPartage";
  id: string;
  idParametre: string;
  idPharmacie: string;
  idType: string;
  type: CREATE_OPTION_PARTAGE_createOneOptionPartage_type | null;
}

export interface CREATE_OPTION_PARTAGE {
  createOneOptionPartage: CREATE_OPTION_PARTAGE_createOneOptionPartage;
}

export interface CREATE_OPTION_PARTAGEVariables {
  input: OptionPartageInput;
}
