/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: OptionPartageTypeInfo
// ====================================================

export interface OptionPartageTypeInfo {
  __typename: "OptionPartageType";
  id: string;
  code: string;
  libelle: string;
}
