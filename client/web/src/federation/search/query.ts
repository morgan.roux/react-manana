import gql from 'graphql-tag';
import { FULL_TRAITEMENT_AUTO_INFO } from './../auto/traitement-automatique/fragment';
import { FULL_USER_INFO } from './../iam/user/fragment';
import { FULL_SUIVI_APPEL } from '../basis/suiviAppel/fragment';
import { PARAMETRE_LABORATOIRE_INFO } from '../partenaire-service/laboratoire/fragment'
import { PRT_CONTACT_SEARCH_INFO } from '../partenaire-service/contact/fragment'

export const SEARCH = gql`
  query SEARCH(
    $index: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $include: [String]
    $exclude: [String]
  ) {
    search(
      index: $index
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
      include: $include
      exclude: $exclude
    ) {
      total
      data {
        ... on TATraitement {
          ...TraitementInfo
        }
        ... on User {
          ...UserInfo
        }
        ... on SuiviAppelType {
          ...suiviAppelInfo
        }
      }
    }
  }
  ${FULL_TRAITEMENT_AUTO_INFO}
  ${FULL_USER_INFO}
  ${FULL_SUIVI_APPEL}
`;


export const SEARCH_PARAMETRE_LABORATOIRES = gql`
  query SEARCH_PARAMETRE_LABORATOIRES(
    $index: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $include: [String]
    $exclude: [String]
  ) {
    search(
      index: $index
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
      include: $include
      exclude: $exclude
    ) {
      total
      data {
        ... on Laboratoire {
          ...LaboratoireInfo
        }
      }
    }
  }
  ${PARAMETRE_LABORATOIRE_INFO}
`;


export const SEARCH_PRT_CONTACT = gql`
  query SEARCH_PRT_CONTACT(
    $index: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $include: [String]
    $exclude: [String]
  ) {
    search(
      index: $index
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
      include: $include
      exclude: $exclude
    ) {
      total
      data {
        ... on PRTContact {
          ...PRTContactInfo
        }
      }
    }
  }
  ${PRT_CONTACT_SEARCH_INFO}
`;

