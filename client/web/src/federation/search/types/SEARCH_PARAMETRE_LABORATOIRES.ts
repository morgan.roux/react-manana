/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_PARAMETRE_LABORATOIRES
// ====================================================

export interface SEARCH_PARAMETRE_LABORATOIRES_search_data_User {
  __typename: "User" | "TATraitement" | "SuiviAppelType" | "PRTContact" | "TodoAction" | "TodoProjet" | "InformationLiaison";
}

export interface SEARCH_PARAMETRE_LABORATOIRES_search_data_Laboratoire_laboratoireRattachement {
  __typename: "Laboratoire";
  id: string;
  nom: string;
}

export interface SEARCH_PARAMETRE_LABORATOIRES_search_data_Laboratoire_laboratoireRattaches {
  __typename: "Laboratoire";
  id: string;
  nom: string;
}

export interface SEARCH_PARAMETRE_LABORATOIRES_search_data_Laboratoire_laboratoireSuite {
  __typename: "LaboratoireSuite";
  id: string;
  adresse: string;
  codePostal: string | null;
  telephone: string | null;
  ville: string | null;
  email: string | null;
}

export interface SEARCH_PARAMETRE_LABORATOIRES_search_data_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  nom: string;
  idLaboSuite: string | null;
  sortie: number;
  laboratoireRattachement: SEARCH_PARAMETRE_LABORATOIRES_search_data_Laboratoire_laboratoireRattachement | null;
  laboratoireRattaches: SEARCH_PARAMETRE_LABORATOIRES_search_data_Laboratoire_laboratoireRattaches[] | null;
  laboratoireSuite: SEARCH_PARAMETRE_LABORATOIRES_search_data_Laboratoire_laboratoireSuite | null;
  pharmacieInfos: any;
  createdAt: any;
  updatedAt: any;
}

export type SEARCH_PARAMETRE_LABORATOIRES_search_data = SEARCH_PARAMETRE_LABORATOIRES_search_data_User | SEARCH_PARAMETRE_LABORATOIRES_search_data_Laboratoire;

export interface SEARCH_PARAMETRE_LABORATOIRES_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_PARAMETRE_LABORATOIRES_search_data | null)[] | null;
}

export interface SEARCH_PARAMETRE_LABORATOIRES {
  search: SEARCH_PARAMETRE_LABORATOIRES_search | null;
}

export interface SEARCH_PARAMETRE_LABORATOIRESVariables {
  index?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
  include?: (string | null)[] | null;
  exclude?: (string | null)[] | null;
}
