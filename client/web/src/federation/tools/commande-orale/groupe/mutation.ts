import gql from 'graphql-tag';
import { MINIMAL_GROUPE_SOURCE_APPRO_INFO } from './fragment';

export const CREATE_GROUPE = gql`
  mutation CREATE_GROUPE($input: COGroupeInput!) {
    createOneCOGroupeSourceAppro(input: $input) {
      ...COGroupeInfo
    }
  }
  ${MINIMAL_GROUPE_SOURCE_APPRO_INFO}
`;

export const UPDATE_GROUPE = gql`
  mutation UPDATE_GROUPE($id: String!, $input: COGroupeInput!) {
    updateOneCOGroupeSourceAppro(id: $id, input: $input) {
      ...COGroupeInfo
    }
  }
  ${MINIMAL_GROUPE_SOURCE_APPRO_INFO}
`;

export const DELETE_ONE_GROUPE = gql`
  mutation DELETE_ONE_GROUPE($input: DeleteOneCOGroupeInput!) {
    deleteOneCOGroupe(input: $input) {
      id
    }
  }
`;
