/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneCOGroupeInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_ONE_GROUPE
// ====================================================

export interface DELETE_ONE_GROUPE_deleteOneCOGroupe {
  __typename: "COGroupeDeleteResponse";
  id: string | null;
}

export interface DELETE_ONE_GROUPE {
  deleteOneCOGroupe: DELETE_ONE_GROUPE_deleteOneCOGroupe;
}

export interface DELETE_ONE_GROUPEVariables {
  input: DeleteOneCOGroupeInput;
}
