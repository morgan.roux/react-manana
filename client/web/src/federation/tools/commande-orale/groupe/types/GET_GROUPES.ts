/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, COGroupeFilter, COGroupeSort } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_GROUPES
// ====================================================

export interface GET_GROUPES_cOGroupes_nodes_sourceAppros_attributs {
  __typename: "COSourceApproAttribut";
  id: string;
  identifiant: string;
  valeur: string;
}

export interface GET_GROUPES_cOGroupes_nodes_sourceAppros {
  __typename: "COSourceAppro";
  id: string;
  nom: string;
  ordre: number;
  tel: string;
  commentaire: string | null;
  idGroupement: string;
  attributs: GET_GROUPES_cOGroupes_nodes_sourceAppros_attributs[];
}

export interface GET_GROUPES_cOGroupes_nodes {
  __typename: "COGroupe";
  id: string;
  nom: string;
  sourceAppros: GET_GROUPES_cOGroupes_nodes_sourceAppros[];
}

export interface GET_GROUPES_cOGroupes {
  __typename: "COGroupeConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_GROUPES_cOGroupes_nodes[];
}

export interface GET_GROUPES {
  cOGroupes: GET_GROUPES_cOGroupes;
}

export interface GET_GROUPESVariables {
  paging?: OffsetPaging | null;
  filter?: COGroupeFilter | null;
  sorting?: COGroupeSort[] | null;
}
