/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_GROUPE
// ====================================================

export interface GET_GROUPE_cOGroupe_sourceAppros_attributs {
  __typename: "COSourceApproAttribut";
  id: string;
  identifiant: string;
  valeur: string;
}

export interface GET_GROUPE_cOGroupe_sourceAppros {
  __typename: "COSourceAppro";
  id: string;
  nom: string;
  ordre: number;
  tel: string;
  commentaire: string | null;
  idGroupement: string;
  attributs: GET_GROUPE_cOGroupe_sourceAppros_attributs[];
}

export interface GET_GROUPE_cOGroupe {
  __typename: "COGroupe";
  id: string;
  nom: string;
  sourceAppros: GET_GROUPE_cOGroupe_sourceAppros[];
}

export interface GET_GROUPE {
  cOGroupe: GET_GROUPE_cOGroupe | null;
}

export interface GET_GROUPEVariables {
  id: string;
}
