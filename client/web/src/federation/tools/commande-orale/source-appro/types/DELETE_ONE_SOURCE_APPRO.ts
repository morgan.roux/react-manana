/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { DeleteOneCOSourceApproInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_ONE_SOURCE_APPRO
// ====================================================

export interface DELETE_ONE_SOURCE_APPRO_deleteOneCOSourceAppro {
  __typename: "COSourceApproDeleteResponse";
  id: string | null;
}

export interface DELETE_ONE_SOURCE_APPRO {
  deleteOneCOSourceAppro: DELETE_ONE_SOURCE_APPRO_deleteOneCOSourceAppro;
}

export interface DELETE_ONE_SOURCE_APPROVariables {
  input: DeleteOneCOSourceApproInput;
}
