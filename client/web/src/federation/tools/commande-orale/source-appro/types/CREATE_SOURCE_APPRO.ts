/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { COSourceApproInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_SOURCE_APPRO
// ====================================================

export interface CREATE_SOURCE_APPRO_createOneCOSourceAppro_attributs {
  __typename: "COSourceApproAttribut";
  id: string;
  identifiant: string;
  valeur: string;
}

export interface CREATE_SOURCE_APPRO_createOneCOSourceAppro {
  __typename: "COSourceAppro";
  id: string;
  nom: string;
  ordre: number;
  tel: string;
  commentaire: string | null;
  idGroupeSourceAppro: string;
  idPharmacie: string;
  idGroupement: string;
  attributs: CREATE_SOURCE_APPRO_createOneCOSourceAppro_attributs[];
}

export interface CREATE_SOURCE_APPRO {
  createOneCOSourceAppro: CREATE_SOURCE_APPRO_createOneCOSourceAppro;
}

export interface CREATE_SOURCE_APPROVariables {
  input: COSourceApproInput;
}
