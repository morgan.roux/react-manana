/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { COSourceApproInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_SOURCE_APPRO
// ====================================================

export interface UPDATE_SOURCE_APPRO_updateOneCOSourceAppro_attributs {
  __typename: "COSourceApproAttribut";
  id: string;
  identifiant: string;
  valeur: string;
}

export interface UPDATE_SOURCE_APPRO_updateOneCOSourceAppro {
  __typename: "COSourceAppro";
  id: string;
  nom: string;
  ordre: number;
  tel: string;
  commentaire: string | null;
  idGroupeSourceAppro: string;
  idPharmacie: string;
  idGroupement: string;
  attributs: UPDATE_SOURCE_APPRO_updateOneCOSourceAppro_attributs[];
}

export interface UPDATE_SOURCE_APPRO {
  updateOneCOSourceAppro: UPDATE_SOURCE_APPRO_updateOneCOSourceAppro;
}

export interface UPDATE_SOURCE_APPROVariables {
  id: string;
  input: COSourceApproInput;
}
