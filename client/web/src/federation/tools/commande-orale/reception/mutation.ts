import gql from 'graphql-tag';
import { FULL_COMMANDE_ORALE_INFO } from '../commande-orale/fragment';

export const CREATE_RECEPTION = gql`
  mutation CREATE_RECEPTION($input: COReceptionInput!) {
    createOneCOReception(input: $input) {
      ...COCommandeOraleInfo
    }
  }
  ${FULL_COMMANDE_ORALE_INFO}
`;
