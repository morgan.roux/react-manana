/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: COCommandeOraleInfo
// ====================================================

export interface COCommandeOraleInfo {
  __typename: "COCommandeOrale";
  id: string;
  idUser: string;
  designation: string;
  quantite: number;
  forme: string;
  dateHeure: any;
  cloturee: boolean;
  commentaire: string | null;
}
