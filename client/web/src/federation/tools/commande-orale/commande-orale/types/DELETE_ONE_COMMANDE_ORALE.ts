/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_ONE_COMMANDE_ORALE
// ====================================================

export interface DELETE_ONE_COMMANDE_ORALE {
  deleteOneCOCommandeOrale: string;
}

export interface DELETE_ONE_COMMANDE_ORALEVariables {
  id: string;
}
