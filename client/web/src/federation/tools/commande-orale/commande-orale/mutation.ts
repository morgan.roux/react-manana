import gql from 'graphql-tag';
import { FULL_COMMANDE_ORALE_INFO } from './fragment';

export const CREATE_COMMANDE_ORALE = gql`
  mutation CREATE_COMMANDE_ORALE($input: COCommandeOraleInput!) {
    createOneCOCommandeOrale(input: $input) {
      ...COCommandeOraleInfo
    }
  }
  ${FULL_COMMANDE_ORALE_INFO}
`;

export const UPDATE_COMMANDE_ORALE = gql`
  mutation UPDATE_COMMANDE_ORALE($id: String!, $input: COCommandeOraleInput!) {
    updateOneCOCommandeOrale(id: $id, input: $input) {
      ...COCommandeOraleInfo
    }
  }
  ${FULL_COMMANDE_ORALE_INFO}
`;

export const DELETE_ONE_COMMANDE_ORALE = gql`
  mutation DELETE_ONE_COMMANDE_ORALE($id: String!) {
    deleteOneCOCommandeOrale(id: $id)
  }
`;

export const CLOTURER_COMMANDE = gql`
  mutation CLOTURER_COMMANDE($id: String!) {
    cloturerCommande(id: $id) {
      ...COCommandeOraleInfo
    }
  }
  ${FULL_COMMANDE_ORALE_INFO}
`;
