import gql from 'graphql-tag';
import { FULL_COMMANDE_ORALE_INFO } from './fragment';

export const GET_COMMANDES_ORALES = gql`
  query GET_COMMANDES_ORALES(
    $paging: OffsetPaging
    $filter: COCommandeOraleFilter
    $sorting: [COCommandeOraleSort!]
  ) {
    cOCommandeOrales(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...COCommandeOraleInfo
      }
    }
  }

  ${FULL_COMMANDE_ORALE_INFO}
`;

export const GET_ROW_COMMANDES_ORALES = gql`
  query GET_ROW_COMMANDES_ORALES($filter: COCommandeOraleAggregateFilter) {
    cOCommandeOraleAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;
