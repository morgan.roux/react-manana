import gql from 'graphql-tag';

export const FULL_COMMANDE_ORALE_INFO = gql`
  fragment COCommandeOraleInfo on COCommandeOrale {
    id
    designation
    quantite
    forme
    dateHeure
    cloturee
    commentaire
    idPassation
    passation {
      id
      dateHeure
      commentaire
      collaborateur {
        id
        userName
      }
      sourceAppro {
        id
        nom
        ordre
        tel
        commentaire
        attributs {
          identifiant
          valeur
        }
      }
    }
    idReception
    reception {
      id
      dateHeure
      commentaire
      collaborateur {
        id
        userName
      }
    }
    collaborateur {
      id
      userName
    }
  }
`;

export const MIN_COMMANDE_ORALE_INFO = gql`
  fragment COCommandeOraleInfo on COCommandeOrale {
    id
    idUser
    designation
    quantite
    forme
    dateHeure
    cloturee
    commentaire
  }
`;
