/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PilotageStatistiqueInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_PILOTAGE_STATISTIQUE
// ====================================================

export interface GET_PILOTAGE_STATISTIQUE_pfPilotageStatistique_evolutions_importances {
  __typename: "PFPilotageImportance";
  id: string;
  ordre: number;
  libelle: string;
  pilotageCouleur: string;
  pilotageOccurences: number;
}

export interface GET_PILOTAGE_STATISTIQUE_pfPilotageStatistique_evolutions {
  __typename: "PFPilotageEvolution";
  id: string;
  categorie: string;
  ordre: number;
  importances: GET_PILOTAGE_STATISTIQUE_pfPilotageStatistique_evolutions_importances[];
}

export interface GET_PILOTAGE_STATISTIQUE_pfPilotageStatistique_items_statuts {
  __typename: "PFPilotageStatut";
  id: string;
  code: string;
  libelle: string;
  pilotageCouleur: string;
  pilotageOccurences: number;
}

export interface GET_PILOTAGE_STATISTIQUE_pfPilotageStatistique_items {
  __typename: "PFPilotageItem";
  id: string;
  code: string;
  libelle: string;
  pilotagePercentage: number;
  pilotageCouleur: string;
  statuts: GET_PILOTAGE_STATISTIQUE_pfPilotageStatistique_items_statuts[];
}

export interface GET_PILOTAGE_STATISTIQUE_pfPilotageStatistique {
  __typename: "PFPilotageStatistique";
  evolutions: GET_PILOTAGE_STATISTIQUE_pfPilotageStatistique_evolutions[];
  items: GET_PILOTAGE_STATISTIQUE_pfPilotageStatistique_items[];
}

export interface GET_PILOTAGE_STATISTIQUE {
  pfPilotageStatistique: GET_PILOTAGE_STATISTIQUE_pfPilotageStatistique;
}

export interface GET_PILOTAGE_STATISTIQUEVariables {
  input: PilotageStatistiqueInput;
}
