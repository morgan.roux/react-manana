import gql from 'graphql-tag';

export const PILOTAGE_STATISTIQUE_INFO = gql`
  fragment PilotageStatistiqueInfo on PFPilotageStatistique {
    evolutions{
      id
      categorie
      ordre
      importances {
        id
        ordre
        libelle
        pilotageCouleur
        pilotageOccurences
      }
    }
    items{
      id
      code
      libelle
      pilotagePercentage
      pilotageCouleur
      statuts {
        id
        code
        libelle
        pilotageCouleur
        pilotageOccurences
      }
    }
  }
`;
