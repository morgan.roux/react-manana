/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: AppetenceInfo
// ====================================================

export interface AppetenceInfo_item {
  __typename: "Item";
  id: string;
  code: string;
  name: string;
}

export interface AppetenceInfo {
  __typename: "Appetence";
  id: string;
  idItem: string;
  ordre: number;
  createdAt: any;
  updatedAt: any;
  item: AppetenceInfo_item;
}
