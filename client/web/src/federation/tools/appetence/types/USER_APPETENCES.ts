/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: USER_APPETENCES
// ====================================================

export interface USER_APPETENCES_userAppetences_item {
  __typename: "Item";
  id: string;
  code: string;
  name: string;
}

export interface USER_APPETENCES_userAppetences {
  __typename: "Appetence";
  id: string;
  idItem: string;
  ordre: number;
  createdAt: any;
  updatedAt: any;
  item: USER_APPETENCES_userAppetences_item;
}

export interface USER_APPETENCES {
  userAppetences: USER_APPETENCES_userAppetences[];
}

export interface USER_APPETENCESVariables {
  idUser: string;
}
