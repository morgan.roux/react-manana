/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AppetenceInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: SAVE_ROLE_APPETENCES
// ====================================================

export interface SAVE_ROLE_APPETENCES_saveRoleAppetences_item {
  __typename: "Item";
  id: string;
  code: string;
  name: string;
}

export interface SAVE_ROLE_APPETENCES_saveRoleAppetences {
  __typename: "Appetence";
  id: string;
  idItem: string;
  ordre: number;
  createdAt: any;
  updatedAt: any;
  item: SAVE_ROLE_APPETENCES_saveRoleAppetences_item;
}

export interface SAVE_ROLE_APPETENCES {
  saveRoleAppetences: SAVE_ROLE_APPETENCES_saveRoleAppetences[];
}

export interface SAVE_ROLE_APPETENCESVariables {
  appetences?: AppetenceInput[] | null;
  idRole: string;
}
