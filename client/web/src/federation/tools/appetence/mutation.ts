import gql from 'graphql-tag';
import { FULL_APPETENCE_INFO } from './fragment';

export const DO_SAVE_ROLE_APPETENCES = gql`
  mutation SAVE_ROLE_APPETENCES($appetences: [AppetenceInput!], $idRole: String!) {
    saveRoleAppetences(appetences: $appetences, idRole: $idRole) {
      ...AppetenceInfo
    }
  }
  ${FULL_APPETENCE_INFO}
`;

export const DO_SAVE_USER_APPETENCES = gql`
  mutation SAVE_USER_APPETENCES($appetences: [AppetenceInput!], $idUser: String!) {
    saveUserAppetences(appetences: $appetences, idUser: $idUser) {
      ...AppetenceInfo
    }
  }
  ${FULL_APPETENCE_INFO}
`;
