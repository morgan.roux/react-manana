import gql from 'graphql-tag';

export const FULL_CAPTEUR_INFO = gql`
  fragment RTCapteurInfo on RTCapteur {
    id
    nom
    numeroSerie
    pointAcces{
      id
      nom
      numeroSerie
    }
  }
`;