/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, RTCapteurFilter, RTCapteurSort } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_CAPTEURS
// ====================================================

export interface GET_CAPTEURS_rTCapteurs_nodes_pointAcces {
  __typename: "RTPointAcces";
  id: string;
  nom: string;
  numeroSerie: string;
}

export interface GET_CAPTEURS_rTCapteurs_nodes {
  __typename: "RTCapteur";
  id: string;
  nom: string;
  numeroSerie: string;
  pointAcces: GET_CAPTEURS_rTCapteurs_nodes_pointAcces;
}

export interface GET_CAPTEURS_rTCapteurs {
  __typename: "RTCapteurConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_CAPTEURS_rTCapteurs_nodes[];
}

export interface GET_CAPTEURS {
  rTCapteurs: GET_CAPTEURS_rTCapteurs;
}

export interface GET_CAPTEURSVariables {
  paging?: OffsetPaging | null;
  filter?: RTCapteurFilter | null;
  sorting?: RTCapteurSort[] | null;
}
