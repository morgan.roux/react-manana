/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RTPointAccesInfo
// ====================================================

export interface RTPointAccesInfo_capteurs {
  __typename: "RTCapteur";
  id: string;
  nom: string;
  numeroSerie: string;
}

export interface RTPointAccesInfo {
  __typename: "RTPointAcces";
  id: string;
  nom: string;
  numeroSerie: string;
  capteurs: RTPointAccesInfo_capteurs[];
}
