/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RTFrigoInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_FRIGO
// ====================================================

export interface CREATE_FRIGO_createOneRTFrigo_traitements_type {
  __typename: "TATraitementType";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_FRIGO_createOneRTFrigo_traitements_participants_photo | null;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_fonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_tache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_derniereExecutionCloturee_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_derniereExecutionCloturee_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_derniereExecutionCloturee_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_derniereExecutionCloturee_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_derniereExecutionCloturee {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: CREATE_FRIGO_createOneRTFrigo_traitements_derniereExecutionCloturee_dernierChangementStatut | null;
  changementStatuts: CREATE_FRIGO_createOneRTFrigo_traitements_derniereExecutionCloturee_changementStatuts[];
  urgence: CREATE_FRIGO_createOneRTFrigo_traitements_derniereExecutionCloturee_urgence;
  importance: CREATE_FRIGO_createOneRTFrigo_traitements_derniereExecutionCloturee_importance;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_execution_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_execution_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_execution_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_execution_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements_execution {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: CREATE_FRIGO_createOneRTFrigo_traitements_execution_dernierChangementStatut | null;
  changementStatuts: CREATE_FRIGO_createOneRTFrigo_traitements_execution_changementStatuts[];
  urgence: CREATE_FRIGO_createOneRTFrigo_traitements_execution_urgence;
  importance: CREATE_FRIGO_createOneRTFrigo_traitements_execution_importance;
}

export interface CREATE_FRIGO_createOneRTFrigo_traitements {
  __typename: "TATraitement";
  id: string;
  dataType: string;
  description: string;
  termine: boolean;
  nombreTraitementsCloture: number;
  nombreTraitementsEnRetard: number;
  joursSemaine: number[] | null;
  dateDebut: any | null;
  isPrivate: boolean;
  type: CREATE_FRIGO_createOneRTFrigo_traitements_type;
  participants: CREATE_FRIGO_createOneRTFrigo_traitements_participants[];
  importance: CREATE_FRIGO_createOneRTFrigo_traitements_importance;
  fonction: CREATE_FRIGO_createOneRTFrigo_traitements_fonction | null;
  tache: CREATE_FRIGO_createOneRTFrigo_traitements_tache | null;
  repetition: number;
  repetitionUnite: string;
  recurrence: string;
  heure: string;
  dateHeureFin: any | null;
  nombreOccurencesFin: number | null;
  urgence: CREATE_FRIGO_createOneRTFrigo_traitements_urgence;
  derniereExecutionCloturee: CREATE_FRIGO_createOneRTFrigo_traitements_derniereExecutionCloturee | null;
  execution: CREATE_FRIGO_createOneRTFrigo_traitements_execution;
}

export interface CREATE_FRIGO_createOneRTFrigo_capteurs_pointAcces {
  __typename: "RTPointAcces";
  id: string;
  nom: string;
  numeroSerie: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_capteurs {
  __typename: "RTCapteur";
  id: string;
  nom: string;
  numeroSerie: string;
  pointAcces: CREATE_FRIGO_createOneRTFrigo_capteurs_pointAcces;
}

export interface CREATE_FRIGO_createOneRTFrigo_dernierReleve_releveur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_FRIGO_createOneRTFrigo_dernierReleve_releveur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_FRIGO_createOneRTFrigo_dernierReleve_releveur_photo | null;
}

export interface CREATE_FRIGO_createOneRTFrigo_dernierReleve_status {
  __typename: "RTStatus";
  id: string;
  code: string;
  couleur: string;
  libelle: string;
}

export interface CREATE_FRIGO_createOneRTFrigo_dernierReleve_frigo {
  __typename: "RTFrigo";
  id: string;
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  temporisation: number | null;
}

export interface CREATE_FRIGO_createOneRTFrigo_dernierReleve {
  __typename: "RTReleve";
  id: string;
  temperature: number | null;
  type: string;
  createdAt: any;
  releveur: CREATE_FRIGO_createOneRTFrigo_dernierReleve_releveur | null;
  status: CREATE_FRIGO_createOneRTFrigo_dernierReleve_status;
  frigo: CREATE_FRIGO_createOneRTFrigo_dernierReleve_frigo;
}

export interface CREATE_FRIGO_createOneRTFrigo {
  __typename: "RTFrigo";
  id: string;
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  idPharmacie: string;
  temporisation: number | null;
  typeReleve: string;
  traitements: CREATE_FRIGO_createOneRTFrigo_traitements[] | null;
  capteurs: CREATE_FRIGO_createOneRTFrigo_capteurs[] | null;
  dernierReleve: CREATE_FRIGO_createOneRTFrigo_dernierReleve | null;
}

export interface CREATE_FRIGO {
  createOneRTFrigo: CREATE_FRIGO_createOneRTFrigo;
}

export interface CREATE_FRIGOVariables {
  input: RTFrigoInput;
}
