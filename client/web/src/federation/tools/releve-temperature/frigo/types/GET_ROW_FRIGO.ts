/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RTFrigoAggregateFilter } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ROW_FRIGO
// ====================================================

export interface GET_ROW_FRIGO_rTFrigoAggregate_count {
  __typename: "RTFrigoCountAggregate";
  id: number | null;
}

export interface GET_ROW_FRIGO_rTFrigoAggregate {
  __typename: "RTFrigoAggregateResponse";
  count: GET_ROW_FRIGO_rTFrigoAggregate_count | null;
}

export interface GET_ROW_FRIGO {
  rTFrigoAggregate: GET_ROW_FRIGO_rTFrigoAggregate[];
}

export interface GET_ROW_FRIGOVariables {
  filter?: RTFrigoAggregateFilter | null;
}
