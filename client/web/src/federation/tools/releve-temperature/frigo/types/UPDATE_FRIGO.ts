/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RTFrigoInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_FRIGO
// ====================================================

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_type {
  __typename: "TATraitementType";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_FRIGO_updateOneRTFrigo_traitements_participants_photo | null;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_fonction {
  __typename: "DQMTFonction";
  id: string;
  ordre: number;
  libelle: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_tache {
  __typename: "DQMTTache";
  id: string;
  ordre: number;
  libelle: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_derniereExecutionCloturee_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_derniereExecutionCloturee_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_derniereExecutionCloturee_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_derniereExecutionCloturee_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_derniereExecutionCloturee {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: UPDATE_FRIGO_updateOneRTFrigo_traitements_derniereExecutionCloturee_dernierChangementStatut | null;
  changementStatuts: UPDATE_FRIGO_updateOneRTFrigo_traitements_derniereExecutionCloturee_changementStatuts[];
  urgence: UPDATE_FRIGO_updateOneRTFrigo_traitements_derniereExecutionCloturee_urgence;
  importance: UPDATE_FRIGO_updateOneRTFrigo_traitements_derniereExecutionCloturee_importance;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_execution_dernierChangementStatut {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_execution_changementStatuts {
  __typename: "TATraitementExecutionChangementStatut";
  id: string;
  status: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_execution_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_execution_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements_execution {
  __typename: "TATraitementExecution";
  id: string;
  prochainement: any | null;
  dateEcheance: any;
  dernierChangementStatut: UPDATE_FRIGO_updateOneRTFrigo_traitements_execution_dernierChangementStatut | null;
  changementStatuts: UPDATE_FRIGO_updateOneRTFrigo_traitements_execution_changementStatuts[];
  urgence: UPDATE_FRIGO_updateOneRTFrigo_traitements_execution_urgence;
  importance: UPDATE_FRIGO_updateOneRTFrigo_traitements_execution_importance;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_traitements {
  __typename: "TATraitement";
  id: string;
  dataType: string;
  description: string;
  termine: boolean;
  nombreTraitementsCloture: number;
  nombreTraitementsEnRetard: number;
  joursSemaine: number[] | null;
  dateDebut: any | null;
  isPrivate: boolean;
  type: UPDATE_FRIGO_updateOneRTFrigo_traitements_type;
  participants: UPDATE_FRIGO_updateOneRTFrigo_traitements_participants[];
  importance: UPDATE_FRIGO_updateOneRTFrigo_traitements_importance;
  fonction: UPDATE_FRIGO_updateOneRTFrigo_traitements_fonction | null;
  tache: UPDATE_FRIGO_updateOneRTFrigo_traitements_tache | null;
  repetition: number;
  repetitionUnite: string;
  recurrence: string;
  heure: string;
  dateHeureFin: any | null;
  nombreOccurencesFin: number | null;
  urgence: UPDATE_FRIGO_updateOneRTFrigo_traitements_urgence;
  derniereExecutionCloturee: UPDATE_FRIGO_updateOneRTFrigo_traitements_derniereExecutionCloturee | null;
  execution: UPDATE_FRIGO_updateOneRTFrigo_traitements_execution;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_capteurs_pointAcces {
  __typename: "RTPointAcces";
  id: string;
  nom: string;
  numeroSerie: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_capteurs {
  __typename: "RTCapteur";
  id: string;
  nom: string;
  numeroSerie: string;
  pointAcces: UPDATE_FRIGO_updateOneRTFrigo_capteurs_pointAcces;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_dernierReleve_releveur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_dernierReleve_releveur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_FRIGO_updateOneRTFrigo_dernierReleve_releveur_photo | null;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_dernierReleve_status {
  __typename: "RTStatus";
  id: string;
  code: string;
  couleur: string;
  libelle: string;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_dernierReleve_frigo {
  __typename: "RTFrigo";
  id: string;
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  temporisation: number | null;
}

export interface UPDATE_FRIGO_updateOneRTFrigo_dernierReleve {
  __typename: "RTReleve";
  id: string;
  temperature: number | null;
  type: string;
  createdAt: any;
  releveur: UPDATE_FRIGO_updateOneRTFrigo_dernierReleve_releveur | null;
  status: UPDATE_FRIGO_updateOneRTFrigo_dernierReleve_status;
  frigo: UPDATE_FRIGO_updateOneRTFrigo_dernierReleve_frigo;
}

export interface UPDATE_FRIGO_updateOneRTFrigo {
  __typename: "RTFrigo";
  id: string;
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  idPharmacie: string;
  temporisation: number | null;
  typeReleve: string;
  traitements: UPDATE_FRIGO_updateOneRTFrigo_traitements[] | null;
  capteurs: UPDATE_FRIGO_updateOneRTFrigo_capteurs[] | null;
  dernierReleve: UPDATE_FRIGO_updateOneRTFrigo_dernierReleve | null;
}

export interface UPDATE_FRIGO {
  updateOneRTFrigo: UPDATE_FRIGO_updateOneRTFrigo;
}

export interface UPDATE_FRIGOVariables {
  id: string;
  input: RTFrigoInput;
}
