import gql from 'graphql-tag';
import { MINIMAL_FRIGO_INFO } from './fragment';

export const GET_FRIGOS = gql`
  query GET_FRIGOS(
    $paging: OffsetPaging
    $filter: RTFrigoFilter
    $sorting: [RTFrigoSort!]
  ) {
    rTFrigos(paging: $paging, filter: $filter, sorting: $sorting) {
      nodes{
      ...RTFrigoInfo
      }
    }
  }

  ${MINIMAL_FRIGO_INFO}
`;

export const GET_ROW_FRIGO = gql`
  query GET_ROW_FRIGO($filter: RTFrigoAggregateFilter) {
    rTFrigoAggregate(filter: $filter) {
      count {
        id
      }
    }
  }
`;
