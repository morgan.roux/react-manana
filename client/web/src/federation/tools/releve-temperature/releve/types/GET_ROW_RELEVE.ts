/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RTReleveAggregateFilter } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ROW_RELEVE
// ====================================================

export interface GET_ROW_RELEVE_rTReleveAggregate_count {
  __typename: "RTReleveCountAggregate";
  id: number | null;
}

export interface GET_ROW_RELEVE_rTReleveAggregate {
  __typename: "RTReleveAggregateResponse";
  count: GET_ROW_RELEVE_rTReleveAggregate_count | null;
}

export interface GET_ROW_RELEVE {
  rTReleveAggregate: GET_ROW_RELEVE_rTReleveAggregate[];
}

export interface GET_ROW_RELEVEVariables {
  filter?: RTReleveAggregateFilter | null;
}
