/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RTStatus
// ====================================================

export interface RTStatus {
  __typename: "RTStatus";
  id: string;
  code: string;
  couleur: string;
  libelle: string;
  createdAt: any;
  updatedAt: any;
}
