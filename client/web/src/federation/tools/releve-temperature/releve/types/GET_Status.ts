/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, RTStatusFilter, RTStatusSort } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_Status
// ====================================================

export interface GET_Status_rTStatuses_nodes {
  __typename: "RTStatus";
  id: string;
  code: string;
  couleur: string;
  libelle: string;
  createdAt: any;
  updatedAt: any;
}

export interface GET_Status_rTStatuses {
  __typename: "RTStatusConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_Status_rTStatuses_nodes[];
}

export interface GET_Status {
  rTStatuses: GET_Status_rTStatuses;
}

export interface GET_StatusVariables {
  paging?: OffsetPaging | null;
  filter?: RTStatusFilter | null;
  sorting?: RTStatusSort[] | null;
}
