/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RTReleveInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_RELEVE
// ====================================================

export interface CREATE_RELEVE_createOneRTReleve_releveur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface CREATE_RELEVE_createOneRTReleve_releveur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: CREATE_RELEVE_createOneRTReleve_releveur_photo | null;
}

export interface CREATE_RELEVE_createOneRTReleve_status {
  __typename: "RTStatus";
  id: string;
  code: string;
  couleur: string;
  libelle: string;
}

export interface CREATE_RELEVE_createOneRTReleve_frigo {
  __typename: "RTFrigo";
  id: string;
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  temporisation: number | null;
}

export interface CREATE_RELEVE_createOneRTReleve {
  __typename: "RTReleve";
  id: string;
  temperature: number | null;
  type: string;
  createdAt: any;
  releveur: CREATE_RELEVE_createOneRTReleve_releveur | null;
  status: CREATE_RELEVE_createOneRTReleve_status;
  frigo: CREATE_RELEVE_createOneRTReleve_frigo;
}

export interface CREATE_RELEVE {
  createOneRTReleve: CREATE_RELEVE_createOneRTReleve;
}

export interface CREATE_RELEVEVariables {
  input: RTReleveInput;
}
