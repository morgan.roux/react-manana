/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, RTReleveFilter, RTReleveSort } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_RELEVES
// ====================================================

export interface GET_RELEVES_rTReleves_nodes_releveur_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: string | null;
  publicUrl: string | null;
}

export interface GET_RELEVES_rTReleves_nodes_releveur {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_RELEVES_rTReleves_nodes_releveur_photo | null;
}

export interface GET_RELEVES_rTReleves_nodes_status {
  __typename: "RTStatus";
  id: string;
  code: string;
  couleur: string;
  libelle: string;
}

export interface GET_RELEVES_rTReleves_nodes_frigo {
  __typename: "RTFrigo";
  id: string;
  nom: string;
  temperatureBasse: number;
  temperatureHaute: number;
  temporisation: number | null;
}

export interface GET_RELEVES_rTReleves_nodes {
  __typename: "RTReleve";
  id: string;
  temperature: number | null;
  type: string;
  createdAt: any;
  releveur: GET_RELEVES_rTReleves_nodes_releveur | null;
  status: GET_RELEVES_rTReleves_nodes_status;
  frigo: GET_RELEVES_rTReleves_nodes_frigo;
}

export interface GET_RELEVES_rTReleves {
  __typename: "RTReleveConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_RELEVES_rTReleves_nodes[];
}

export interface GET_RELEVES {
  rTReleves: GET_RELEVES_rTReleves;
}

export interface GET_RELEVESVariables {
  paging?: OffsetPaging | null;
  filter?: RTReleveFilter | null;
  sorting?: RTReleveSort[] | null;
}
