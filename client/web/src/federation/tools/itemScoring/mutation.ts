import gql from 'graphql-tag';
import { FULL_ITEM_SCORING, ITEM_SCORING_PERSONNALISATION } from './fragment';

export const UPDATE_ONE_SCORING = gql`
    mutation UPDATE_ONE_SCORING($input: UpdateOneItemScoringInput!) {
        updateOneItemScoring(input: $input) {
            ...ItemScoring
        }
    }
    ${FULL_ITEM_SCORING}
`;

export const UPDATE_MANY_ITEM_SCORING = gql`
    mutation UPDATE_MANY_ITEM_SCORING($input: UpdateManyItemScoringsInput!) {
        updateManyItemScorings(input: $input) {
            updatedCount
        }
    }
`;

export const CREATE_ONE_ITEM_SCORING = gql`
    mutation CREATE_ONE_ITEM_SCORING($input: CreateOneItemScoringInput!) {
        createOneItemScoring(input: $input) {
            ...ItemScoring
        }
    }
    ${FULL_ITEM_SCORING}
`;

export const CREATE_MANY_ITEM_SCORING = gql`
    mutation CREATE_MANY_ITEM_SCORING($input: CreateManyItemScoringsInput!) {
        createManyItemScorings(input: $input) {
            ...ItemScoring
        }
    }
    ${FULL_ITEM_SCORING}
`;


export const CREATE_ONE_ITEM_SCORING_PERSONNALISATION = gql`
    mutation CREATE_ONE_ITEM_SCORING_PERSONNALISATION($input: ItemScoringPersonnalisationInput!) {
        createOneItemScoringPersonnalisation(input: $input) {
            ...ItemScoringPersonnalisationInfo
        }
    }
    ${ITEM_SCORING_PERSONNALISATION}
`;

export const UPDATE_ONE_ITEM_SCORING_PERSONNALISATION = gql`
    mutation UPDATE_ONE_ITEM_SCORING_PERSONNALISATION($input: ItemScoringPersonnalisationInput!,$id:String!) {
        updateOneItemScoringPersonnalisation(input: $input,id:$id) {
            ...ItemScoringPersonnalisationInfo
        }
    }
    ${ITEM_SCORING_PERSONNALISATION}
`;

