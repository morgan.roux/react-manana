/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateOneItemScoringInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_ONE_SCORING
// ====================================================

export interface UPDATE_ONE_SCORING_updateOneItemScoring_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface UPDATE_ONE_SCORING_updateOneItemScoring_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface UPDATE_ONE_SCORING_updateOneItemScoring_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface UPDATE_ONE_SCORING_updateOneItemScoring_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: UPDATE_ONE_SCORING_updateOneItemScoring_participants_photo | null;
}

export interface UPDATE_ONE_SCORING_updateOneItemScoring_item {
  __typename: "Item";
  id: string;
  code: string;
  codeItem: string;
  name: string;
}

export interface UPDATE_ONE_SCORING_updateOneItemScoring {
  __typename: "ItemScoring";
  id: string;
  idItem: string | null;
  idItemAssocie: string | null;
  idUser: string | null;
  description: string | null;
  dateEcheance: any | null;
  idUrgence: string | null;
  statut: string | null;
  nomItem: string | null;
  ordreStatut: number | null;
  urgence: UPDATE_ONE_SCORING_updateOneItemScoring_urgence;
  importance: UPDATE_ONE_SCORING_updateOneItemScoring_importance;
  participants: UPDATE_ONE_SCORING_updateOneItemScoring_participants[];
  idImportance: string | null;
  nombreJoursRetard: number;
  score: number;
  item: UPDATE_ONE_SCORING_updateOneItemScoring_item;
}

export interface UPDATE_ONE_SCORING {
  updateOneItemScoring: UPDATE_ONE_SCORING_updateOneItemScoring;
}

export interface UPDATE_ONE_SCORINGVariables {
  input: UpdateOneItemScoringInput;
}
