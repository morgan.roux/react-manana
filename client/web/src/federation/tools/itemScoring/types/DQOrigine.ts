/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: DQOrigine
// ====================================================

export interface DQOrigine {
  __typename: "Origine";
  id: string;
  libelle: string;
}
