/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: Participant
// ====================================================

export interface Participant_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface Participant {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: Participant_photo | null;
}
