/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: DQFicheAmelioration
// ====================================================

export interface DQFicheAmelioration_origine {
  __typename: "Origine";
  id: string;
  libelle: string;
}

export interface DQFicheAmelioration_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  type: string;
}

export interface DQFicheAmelioration_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface DQFicheAmelioration_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: DQFicheAmelioration_participants_photo | null;
}

export interface DQFicheAmelioration_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface DQFicheAmelioration_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface DQFicheAmelioration {
  __typename: "DQFicheAmelioration";
  id: string;
  dataType: string;
  description: string;
  origine: DQFicheAmelioration_origine;
  statut: DQFicheAmelioration_statut;
  participants: DQFicheAmelioration_participants[];
  urgence: DQFicheAmelioration_urgence;
  importance: DQFicheAmelioration_importance;
}
