/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: UPDATE_ITEM_SCORING_FOR_ITEM
// ====================================================

export interface UPDATE_ITEM_SCORING_FOR_ITEM {
  updateItemScoringForItem: number;
}

export interface UPDATE_ITEM_SCORING_FOR_ITEMVariables {
  codeItem: string;
  idItemAssocie: string;
}
