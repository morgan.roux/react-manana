/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ItemScoringPersonnalisationInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_ONE_ITEM_SCORING_PERSONNALISATION
// ====================================================

export interface CREATE_ONE_ITEM_SCORING_PERSONNALISATION_createOneItemScoringPersonnalisation {
  __typename: "ItemScoringPersonnalisation";
  id: string;
  type: string;
  idTypeAssocie: string;
  ordre: number;
  idPharmacie: string;
}

export interface CREATE_ONE_ITEM_SCORING_PERSONNALISATION {
  createOneItemScoringPersonnalisation: CREATE_ONE_ITEM_SCORING_PERSONNALISATION_createOneItemScoringPersonnalisation;
}

export interface CREATE_ONE_ITEM_SCORING_PERSONNALISATIONVariables {
  input: ItemScoringPersonnalisationInput;
}
