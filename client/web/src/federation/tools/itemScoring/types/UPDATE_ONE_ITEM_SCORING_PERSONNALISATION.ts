/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ItemScoringPersonnalisationInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_ONE_ITEM_SCORING_PERSONNALISATION
// ====================================================

export interface UPDATE_ONE_ITEM_SCORING_PERSONNALISATION_updateOneItemScoringPersonnalisation {
  __typename: "ItemScoringPersonnalisation";
  id: string;
  type: string;
  idTypeAssocie: string;
  ordre: number;
  idPharmacie: string;
}

export interface UPDATE_ONE_ITEM_SCORING_PERSONNALISATION {
  updateOneItemScoringPersonnalisation: UPDATE_ONE_ITEM_SCORING_PERSONNALISATION_updateOneItemScoringPersonnalisation;
}

export interface UPDATE_ONE_ITEM_SCORING_PERSONNALISATIONVariables {
  input: ItemScoringPersonnalisationInput;
  id: string;
}
