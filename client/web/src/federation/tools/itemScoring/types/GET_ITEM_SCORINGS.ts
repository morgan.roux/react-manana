/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OffsetPaging, ItemScoringFilter, ItemScoringSort } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL query operation: GET_ITEM_SCORINGS
// ====================================================

export interface GET_ITEM_SCORINGS_itemScorings_nodes_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_ITEM_SCORINGS_itemScorings_nodes_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_ITEM_SCORINGS_itemScorings_nodes_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface GET_ITEM_SCORINGS_itemScorings_nodes_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: GET_ITEM_SCORINGS_itemScorings_nodes_participants_photo | null;
}

export interface GET_ITEM_SCORINGS_itemScorings_nodes_item {
  __typename: "Item";
  id: string;
  code: string;
  codeItem: string;
  name: string;
}

export interface GET_ITEM_SCORINGS_itemScorings_nodes {
  __typename: "ItemScoring";
  id: string;
  idItem: string | null;
  idItemAssocie: string | null;
  idUser: string | null;
  description: string | null;
  dateEcheance: any | null;
  idUrgence: string | null;
  statut: string | null;
  nomItem: string | null;
  ordreStatut: number | null;
  urgence: GET_ITEM_SCORINGS_itemScorings_nodes_urgence;
  importance: GET_ITEM_SCORINGS_itemScorings_nodes_importance;
  participants: GET_ITEM_SCORINGS_itemScorings_nodes_participants[];
  idImportance: string | null;
  nombreJoursRetard: number;
  score: number;
  item: GET_ITEM_SCORINGS_itemScorings_nodes_item;
}

export interface GET_ITEM_SCORINGS_itemScorings {
  __typename: "ItemScoringConnection";
  /**
   * Array of nodes.
   */
  nodes: GET_ITEM_SCORINGS_itemScorings_nodes[];
}

export interface GET_ITEM_SCORINGS {
  itemScorings: GET_ITEM_SCORINGS_itemScorings;
}

export interface GET_ITEM_SCORINGSVariables {
  paging?: OffsetPaging | null;
  filter?: ItemScoringFilter | null;
  sorting?: ItemScoringSort[] | null;
}
