/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ITEM_STATISTIQUES
// ====================================================

export interface ITEM_STATISTIQUES_itemStatistiques_item {
  __typename: "ItemType";
  id: string;
  code: string;
  codeItem: string;
  name: string;
}

export interface ITEM_STATISTIQUES_itemStatistiques {
  __typename: "ItemStatistique";
  item: ITEM_STATISTIQUES_itemStatistiques_item;
  occurence: number;
  percentage: number;
}

export interface ITEM_STATISTIQUES {
  itemStatistiques: ITEM_STATISTIQUES_itemStatistiques[] | null;
}

export interface ITEM_STATISTIQUESVariables {
  idUser: string;
}
