/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: DQFicheIncident
// ====================================================

export interface DQFicheIncident_origine {
  __typename: "Origine";
  id: string;
  libelle: string;
}

export interface DQFicheIncident_statut {
  __typename: "DQStatut";
  id: string;
  libelle: string;
  type: string;
}

export interface DQFicheIncident_participants_photo {
  __typename: "Fichier";
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface DQFicheIncident_participants {
  __typename: "User";
  id: string;
  fullName: string | null;
  photo: DQFicheIncident_participants_photo | null;
}

export interface DQFicheIncident_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface DQFicheIncident_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface DQFicheIncident {
  __typename: "DQFicheIncident";
  id: string;
  dataType: string;
  description: string;
  origine: DQFicheIncident_origine;
  statut: DQFicheIncident_statut;
  participants: DQFicheIncident_participants[];
  urgence: DQFicheIncident_urgence;
  importance: DQFicheIncident_importance;
}
