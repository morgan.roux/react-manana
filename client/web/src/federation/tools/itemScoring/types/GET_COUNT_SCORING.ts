/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_COUNT_SCORING
// ====================================================

export interface GET_COUNT_SCORING_itemScoringsCount_urgence {
  __typename: "Urgence";
  id: string;
  code: string;
  libelle: string;
  couleur: string;
}

export interface GET_COUNT_SCORING_itemScoringsCount_importance {
  __typename: "Importance";
  id: string;
  ordre: number;
  libelle: string;
  couleur: string;
}

export interface GET_COUNT_SCORING_itemScoringsCount {
  __typename: "ItemScoringCount";
  urgence: GET_COUNT_SCORING_itemScoringsCount_urgence;
  importance: GET_COUNT_SCORING_itemScoringsCount_importance;
  occurence: number;
}

export interface GET_COUNT_SCORING {
  itemScoringsCount: GET_COUNT_SCORING_itemScoringsCount[] | null;
}

export interface GET_COUNT_SCORINGVariables {
  dateDebut?: any | null;
  dateFin?: any | null;
  dateEcheance?: any | null;
  idItems?: string[] | null;
}
