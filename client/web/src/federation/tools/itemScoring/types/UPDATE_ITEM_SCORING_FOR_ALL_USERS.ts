/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: UPDATE_ITEM_SCORING_FOR_ALL_USERS
// ====================================================

export interface UPDATE_ITEM_SCORING_FOR_ALL_USERS {
  updateItemScoringForAllUsers: number;
}
