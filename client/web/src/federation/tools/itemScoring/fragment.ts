import gql from 'graphql-tag';

export const DQ_ORIGINE = gql`
  fragment DQOrigine on Origine {
    id
    libelle
  }
`;

export const DQ_STATUT = gql`
  fragment DQStatut on DQStatut {
    id
    libelle
    type
  }
`;

export const PARTICIPANT = gql`
  fragment Participant on User {
    id
    fullName
    photo {
      id
      chemin
      nomOriginal
      publicUrl
    }
  }
`;

export const URGENCE = gql`
  fragment Urgence on Urgence {
    id
    code
    libelle
    couleur
  }
`;

export const IMPORTANCE = gql`
  fragment Importance on Importance {
    id
    ordre
    libelle
    couleur
  }
`;

export const DQ_ACTION_OPERATIONELLE = gql`
  fragment DQActionOperationnelle on DQActionOperationnelle {
    id
    dataType
    description
    origine {
      ...DQOrigine
    }
    statut {
      ...DQStatut
    }
    participants {
      ...Participant
    }
    urgence {
      ...Urgence
    }
    importance {
      ...Importance
    }
  }
  ${DQ_ORIGINE}
  ${DQ_STATUT}
  ${PARTICIPANT}
  ${URGENCE}
  ${IMPORTANCE}
`;

export const DQ_FICHE_AMELIORATION = gql`
  fragment DQFicheAmelioration on DQFicheAmelioration {
    id
    dataType
    description
    origine {
      ...DQOrigine
    }
    statut {
      ...DQStatut
    }
    participants {
      ...Participant
    }
    urgence {
      ...Urgence
    }
    importance {
      ...Importance
    }
  }
  ${DQ_ORIGINE}
  ${DQ_STATUT}
  ${PARTICIPANT}
  ${URGENCE}
  ${IMPORTANCE}
`;

export const DQ_FICHE_INCIDENT = gql`
  fragment DQFicheIncident on DQFicheIncident {
    id
    dataType
    description
    origine {
      ...DQOrigine
    }
    statut {
      ...DQStatut
    }
    participants {
      ...Participant
    }
    urgence {
      ...Urgence
    }
    importance {
      ...Importance
    }
  }
  ${DQ_ORIGINE}
  ${DQ_STATUT}
  ${PARTICIPANT}
  ${URGENCE}
  ${IMPORTANCE}
`;

export const FULL_ITEM_SCORING = gql`
  fragment ItemScoring on ItemScoring {
    id
    idItem
    idItemAssocie
    idUser
    description
    dateEcheance
    idUrgence
    statut
    nomItem
    ordreStatut
    urgence {
      ...Urgence
    }
    importance {
      ...Importance
    }
    participants {
      ...Participant
    }
    idImportance
    nombreJoursRetard
    score
    item {
      id
      code
      codeItem
      name
    }
  }
  ${URGENCE}
  ${IMPORTANCE}
  ${PARTICIPANT}
`;

export const ITEM_SCORING_COUNT = gql`
  fragment ItemScoringCount on ItemScoringCount {
    urgence {
      ...Urgence
    }
    importance {
      ...Importance
    }
    occurence
  }
  ${URGENCE}
  ${IMPORTANCE}
`;

export const ITEM_SCORING_PERSONNALISATION = gql`
  fragment ItemScoringPersonnalisationInfo on ItemScoringPersonnalisation {
    id
    type
    idTypeAssocie
    ordre
    idPharmacie
  }
`;