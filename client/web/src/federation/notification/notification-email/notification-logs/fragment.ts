import gql from 'graphql-tag';

export const NOTIFICATION_LOGS_INFO = gql`
  fragment NotificationLogsInfo on NotificationLogs {
    id
    titre
    message
    type
    expediteur
    createdAt
    updatedAt
  }
`;
