import gql from 'graphql-tag';
import { NOTIFICATION_LOGS_INFO } from './fragment';

export const NOTIFICATION_LOGS = gql`
  mutation NOTIFICATION_LOGS($input: NotificationLogsInput!) {
    createOneNotificationLogs(input: $input) {
      ...NotificationLogsInfo
    }
  }

  ${NOTIFICATION_LOGS_INFO}
`;
