/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: NotificationLogsInfo
// ====================================================

export interface NotificationLogsInfo {
  __typename: "NotificationLogs";
  id: string;
  titre: string;
  message: string;
  type: string;
  expediteur: string | null;
  createdAt: any;
  updatedAt: any;
}
