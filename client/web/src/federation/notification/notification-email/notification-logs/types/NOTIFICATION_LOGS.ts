/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { NotificationLogsInput } from "./../../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: NOTIFICATION_LOGS
// ====================================================

export interface NOTIFICATION_LOGS_createOneNotificationLogs {
  __typename: "NotificationLogs";
  id: string;
  titre: string;
  message: string;
  type: string;
  expediteur: string | null;
  createdAt: any;
  updatedAt: any;
}

export interface NOTIFICATION_LOGS {
  createOneNotificationLogs: NOTIFICATION_LOGS_createOneNotificationLogs;
}

export interface NOTIFICATION_LOGSVariables {
  input: NotificationLogsInput;
}
