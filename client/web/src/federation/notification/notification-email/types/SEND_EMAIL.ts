/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { EmailInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: SEND_EMAIL
// ====================================================

export interface SEND_EMAIL_sendEmail {
  __typename: "SendEmailResult";
  code: string | null;
  message: string | null;
}

export interface SEND_EMAIL {
  sendEmail: SEND_EMAIL_sendEmail | null;
}

export interface SEND_EMAILVariables {
  input?: EmailInput | null;
}
