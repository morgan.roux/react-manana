/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GedDocumentEmailInput } from "./../../../../types/federation-global-types";

// ====================================================
// GraphQL mutation operation: SEND_GED_DOCUMENT_BY_EMAIL
// ====================================================

export interface SEND_GED_DOCUMENT_BY_EMAIL_sendGEDDocumentByEmail {
  __typename: "SendEmailResult";
  code: string | null;
  message: string | null;
}

export interface SEND_GED_DOCUMENT_BY_EMAIL {
  sendGEDDocumentByEmail: SEND_GED_DOCUMENT_BY_EMAIL_sendGEDDocumentByEmail | null;
}

export interface SEND_GED_DOCUMENT_BY_EMAILVariables {
  input?: GedDocumentEmailInput | null;
}
