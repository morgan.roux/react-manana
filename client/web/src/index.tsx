import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { addLocaleData, IntlProvider } from 'react-intl';
import { ApolloProvider } from '@apollo/react-hooks';
import createClient from './apolloClient';
import en from 'react-intl/locale-data/en';
import fr from 'react-intl/locale-data/fr';
import mg from 'react-intl/locale-data/mg';
import messages from './locales/messages';
import { getAccessToken } from './services/LocalStorage';
import { ContentContextProvider } from './AppContext';

const supportedLanguages = ['en-US', 'fr-FR', 'mg-MG'];
const token = getAccessToken() || '';

let locale: string =
  (navigator.languages && navigator.languages[0]) || navigator.language || 'en-US';

if (!supportedLanguages.find(l => l === locale)) {
  locale = 'fr-FR';
}

if (localStorage.getItem('language')) {
  locale = localStorage.getItem('language') || '';
}

addLocaleData([...en, ...fr, ...mg]);

const client = createClient(token);

ReactDOM.render(
  <div style={{ height: '100vh' }}>
    <ApolloProvider client={client}>
      <IntlProvider locale={locale} messages={(messages as any)[locale]}>
        <ContentContextProvider>
          <App />
        </ContentContextProvider>
      </IntlProvider>
    </ApolloProvider>
  </div>,
  document.getElementById('root'),
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
