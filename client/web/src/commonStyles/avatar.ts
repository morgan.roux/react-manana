import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const avatarStyle = makeStyles((theme: Theme) =>
  createStyles({
    avatarContainer: {
      borderRadius: '50%',
      position: 'relative',
      overflow: 'hidden',
      background: '#ffffff',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
    avatarImage: {
      objectFit: 'cover',
      width: '100%',
    },
    avatarGroupement: {
      height: 50,
      width: 200,
      borderRadius: 5,
    },
  }),
);
