/*
 * IMPORTANT: All the data keys shoud be similar to the defined in Graphql Schema
 */

import { getGroupement } from '../services/LocalStorage';

interface PharmacieSearchProps {
  data: any;
  skip: number;
  take: number;
  idPharmacie: string;
  sortBy?: any;
}

const boolNumericFields = ['sortie']; // 0 or 1
const codeFields = ['cip', 'codeEnseigne', 'numIvrylab', 'cp']; // All the fields containing CODE (string fields)
const boolFields = ['actived'];

export const pharmaciesSearch = (props: PharmacieSearchProps) => {
  const { data, skip, take, idPharmacie, sortBy } = props;

  let query = {};

  if (data) {
    const fieldKeys = Object.keys(data);
    const must: any[] = [];

    // All the possible conditions are not yet here
    fieldKeys.forEach(key => {
      if (data[key] !== null && data[key] !== undefined) {
        let q = `${data[key]}`.trim();

        // A space mean OR operator in elasticsearch so we must escapes the spaces in the queries for desired results
        q = q
          .split(' ')
          .filter(str => str.trim().length > 0)
          .join('\\ ');

        let default_field = key;
        if (key === 'departement') {
          default_field = 'departement.nom';
          q = q.toUpperCase();
        }

        if (boolNumericFields.includes(key)) q = data[key];
        else if (codeFields.includes(key)) q = `${q}*`;
        else q = `*${q}*`;

        if (key === 'adresse1') {
          must.push({
            query_string: {
              query: q,
              fields: ['adresse1', 'adresse2'],
            },
          });
        } else if (key === 'tele1') {
          must.push({
            query_string: {
              query: q,
              fields: ['tele1', 'tele2'],
            },
          });
        } else if (key === 'titulaire') {
          const words = `${data[key]}`
            .trim()
            .split(' ')
            .filter(str => str.trim().length > 0);

          if (words.length > 0) {
            must.push({
              query_string: {
                query: words.map(word => `*${word}*`).join(' AND '),
                fields: ['titulaires.nom', 'titulaires.prenom'],
              },
            });
          }
        } else if (boolFields.includes(key)) {
          const value = data[key];
          must.push({
            match: {
              [key]: {
                query: value,
              },
            },
          });
        } else {
          must.push({
            query_string: {
              query: q,
              default_field,
            },
          });
        }
      }
    });

    // Only Pharmacies in the current groupement
    if (getGroupement() && getGroupement().id) {
      must.push({
        match: {
          idGroupement: {
            query: getGroupement().id,
          },
        },
      });
    }

    console.log(must);

    query = {
      query: {
        bool: { must },
      },
    };
  }

  return {
    variables: {
      type: ['pharmacie'],
      skip,
      take,
      idPharmacie,
      query,
      sortBy,
    },
  };
};
