import { API_URL, APP_WS_URL } from './config';
import { split } from 'apollo-link';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import introspectionQueryResultData from './graphql-schema.json';

import resolvers from './apollo/resolvers';
import typeDefs from './apollo/typeDefs';
import { getGroupement, getPharmacie, getAccessToken,getIp } from './services/LocalStorage';

const createClient = (token: string) => {
  const httpLink = createHttpLink({
    uri: `${API_URL}/graphql`,
  });
  const authToken = token || (getAccessToken() || '');

  // Create a WebSocket link:
  const wsLink = new WebSocketLink({
    uri: APP_WS_URL || 'ws://localhost:4000',
    options: {
      reconnect: true,
      connectionParams: {
        authToken,
      },
    },
  });

  const authLink = setContext((_, { headers }) => {
    // get the authentication token from local storage if it exists
    // const token = localStorage.getItem('token');
    // return the headers to the context so httpLink can read them
    const groupement = getGroupement();
    const pharmacie = getPharmacie();
    const localAccessToken = getAccessToken();
    const ip = getIp()
    const headerToken = token || localAccessToken;

    return {
      headers: {
        ...headers,
        authorization: headerToken ? `Bearer ${headerToken}` : '',
        idGroupement: groupement ? groupement.id : '',
        idPharmacie: pharmacie ? pharmacie.id : '',
        ip: ip || '' 
      },
    };
  });

  // using the ability to split links, you can send data to each link
  // depending on what kind of operation is being sent
  const link = split(
    // split based on operation type
    ({ query }) => {
      const definition = getMainDefinition(query);
      return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
    },
    wsLink,
    authLink.concat(httpLink),
  );

  const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData: introspectionQueryResultData as any ,
  });

  return new ApolloClient({
    link,
    cache: new InMemoryCache({ fragmentMatcher }),
    typeDefs,
    resolvers,
  });
};

export default createClient;
