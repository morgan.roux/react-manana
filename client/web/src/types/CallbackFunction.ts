type CallbackFunction<T = undefined> = (value: T) => void;

export default CallbackFunction;
