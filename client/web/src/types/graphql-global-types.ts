/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum ActionActiviteType {
  ASSIGN_USER = "ASSIGN_USER",
  CHANGE_DATE_DEBUT = "CHANGE_DATE_DEBUT",
  CHANGE_DATE_FIN = "CHANGE_DATE_FIN",
  CHANGE_STATUS = "CHANGE_STATUS",
  COMMENT = "COMMENT",
  EDIT = "EDIT",
  UNASSIGN_USER = "UNASSIGN_USER",
}

export enum ActionStatus {
  ACTIVE = "ACTIVE",
  CLOSED = "CLOSED",
  DONE = "DONE",
  EN_COURS = "EN_COURS",
}

export enum BusinessIndicateur {
  CA = "CA",
  CA_MOYEN = "CA_MOYEN",
  NB_COMMANDE = "NB_COMMANDE",
  NB_LIGNE = "NB_LIGNE",
  NB_PHARMACIE = "NB_PHARMACIE",
  NB_PRODUIT = "NB_PRODUIT",
  REMISE = "REMISE",
  TOTAL_REMISE = "TOTAL_REMISE",
}

export enum ConnexionStatut {
  AUTHORIZED = "AUTHORIZED",
  BLOCKED = "BLOCKED",
  TO_CONFIRM = "TO_CONFIRM",
}

export enum FeedbackIndicateur {
  ADORE = "ADORE",
  AIME = "AIME",
  CIBLE = "CIBLE",
  COMMENTAIRE = "COMMENTAIRE",
  CONTENT = "CONTENT",
  DETESTE = "DETESTE",
  LU = "LU",
  MECONTENT = "MECONTENT",
  PARTAGE = "PARTAGE",
  RECOMMANDATION = "RECOMMANDATION",
}

export enum GroupeAmisStatus {
  ACTIVER = "ACTIVER",
  BLOQUER = "BLOQUER",
}

export enum GroupeAmisType {
  DEPARTEMENT = "DEPARTEMENT",
  PERSONNALISE = "PERSONNALISE",
  PHARMACIE = "PHARMACIE",
  REGION = "REGION",
}

export enum IdeeOuBonnePratiqueOrigine {
  FOURNISSEUR = "FOURNISSEUR",
  GROUPEMENT = "GROUPEMENT",
  GROUPE_AMIS = "GROUPE_AMIS",
  INTERNE = "INTERNE",
  PATIENT = "PATIENT",
  PRESTATAIRE = "PRESTATAIRE",
}

export enum IdeeOuBonnePratiqueStatus {
  EN_COURS = "EN_COURS",
  NOUVELLE = "NOUVELLE",
  REJETEE = "REJETEE",
  VALIDEE = "VALIDEE",
}

export enum LdapType {
  OPENLDAP = "OPENLDAP",
  WINDOWS_LDAP = "WINDOWS_LDAP",
}

export enum MarcheStatus {
  ACTIF = "ACTIF",
  CLOTURE = "CLOTURE",
  NON_ACTIF = "NON_ACTIF",
}

export enum MarcheType {
  ARTICLE = "ARTICLE",
  LABORATOIRE = "LABORATOIRE",
}

export enum MessagerieCategory {
  GROUPEMENT = "GROUPEMENT",
  LABO_PARTENAIRE = "LABO_PARTENAIRE",
  MY_REGION = "MY_REGION",
  PARTENAIRE_SERVICE = "PARTENAIRE_SERVICE",
  PHARMACIES = "PHARMACIES",
}

export enum MessagerieType {
  E = "E",
  R = "R",
}

export enum NotificationOrderByInput {
  content_ASC = "content_ASC",
  content_DESC = "content_DESC",
  dateCreation_ASC = "dateCreation_ASC",
  dateCreation_DESC = "dateCreation_DESC",
  dateModification_ASC = "dateModification_ASC",
  dateModification_DESC = "dateModification_DESC",
  id_ASC = "id_ASC",
  id_DESC = "id_DESC",
  seen_ASC = "seen_ASC",
  seen_DESC = "seen_DESC",
  type_ASC = "type_ASC",
  type_DESC = "type_DESC",
}

export enum OrderByFeedback {
  adore = "adore",
  aime = "aime",
  cibles = "cibles",
  cip = "cip",
  commentaires = "commentaires",
  content = "content",
  cp = "cp",
  dateDebut = "dateDebut",
  dateFin = "dateFin",
  deteste = "deteste",
  libelle = "libelle",
  lu = "lu",
  mecontent = "mecontent",
  nom = "nom",
  ville = "ville",
}

export enum OrederByBusiness {
  caMoyenParPharmacie = "caMoyenParPharmacie",
  caTotal = "caTotal",
  canal = "canal",
  cip = "cip",
  cp = "cp",
  dateDebut = "dateDebut",
  dateFin = "dateFin",
  globaliteRemise = "globaliteRemise",
  libelle = "libelle",
  nbCommandes = "nbCommandes",
  nbLignes = "nbLignes",
  nbPharmacies = "nbPharmacies",
  nbProduits = "nbProduits",
  nom = "nom",
  totalRemise = "totalRemise",
  ville = "ville",
}

export enum OriginePublicite {
  EXTERNE = "EXTERNE",
  INTERNE = "INTERNE",
}

export enum OrigineType {
  FOURNISSEUR = "FOURNISSEUR",
  GROUPEMENT = "GROUPEMENT",
  GROUPE_AMIS = "GROUPE_AMIS",
  INTERNE = "INTERNE",
  PATIENT = "PATIENT",
  PRESTATAIRE = "PRESTATAIRE",
}

export enum ParamCategory {
  GROUPEMENT = "GROUPEMENT",
  GROUPEMENT_SUPERADMIN = "GROUPEMENT_SUPERADMIN",
  PHARMACIE = "PHARMACIE",
  PLATEFORME = "PLATEFORME",
  USER = "USER",
}

export enum PartageType {
  PARTAGE = "PARTAGE",
  RECOMMANDATION = "RECOMMANDATION",
}

export enum PilotageBusiness {
  GLOBAL = "GLOBAL",
  OPERATION = "OPERATION",
  PHARMACIE = "PHARMACIE",
}

export enum PilotageFeedback {
  GLOBAL = "GLOBAL",
  ITEM = "ITEM",
  PHARMACIE = "PHARMACIE",
}

export enum PromotioStatus {
  ACTIF = "ACTIF",
  CLOTURE = "CLOTURE",
  NON_ACTIF = "NON_ACTIF",
}

export enum PromotionType {
  AUTRE_PROMOTION = "AUTRE_PROMOTION",
  BONNE_AFFAIRE = "BONNE_AFFAIRE",
  LIQUIDATION = "LIQUIDATION",
  OFFRE_DU_MOIS = "OFFRE_DU_MOIS",
}

export enum RgpdHistoriqueInfoPlusType {
  ACCUEIL = "ACCUEIL",
  AUTORISATION = "AUTORISATION",
  PARTENAIRE = "PARTENAIRE",
}

export enum Sendingtype {
  GET = "GET",
  POST = "POST",
}

export enum Sexe {
  F = "F",
  M = "M",
}

export enum SsoType {
  CRYPTAGE_AES256 = "CRYPTAGE_AES256",
  CRYPTAGE_MD5 = "CRYPTAGE_MD5",
  OTHER_CRYPTO = "OTHER_CRYPTO",
  SAML = "SAML",
  TOKEN_AUTHENTIFICATION = "TOKEN_AUTHENTIFICATION",
  TOKEN_FTP_AUTHENTIFICATION = "TOKEN_FTP_AUTHENTIFICATION",
}

export enum StatusTicket {
  CLOTURE = "CLOTURE",
  EN_CHARGE = "EN_CHARGE",
  EN_COURS = "EN_COURS",
  NOUVELLE = "NOUVELLE",
}

export enum StatutPartenaire {
  ACTIVER = "ACTIVER",
  BLOQUER = "BLOQUER",
}

export enum TicketAppel {
  ENTRANT = "ENTRANT",
  SORTANT = "SORTANT",
}

export enum TicketType {
  APPEL = "APPEL",
  RECLAMATION = "RECLAMATION",
}

export enum TicketVisibilite {
  OUVERT = "OUVERT",
  PRIVE = "PRIVE",
}

export enum TypeAffectation {
  DEPARTEMENT = "DEPARTEMENT",
  REMPLACEMENT_ARRET_SANS = "REMPLACEMENT_ARRET_SANS",
  REMPLACEMENT_ATTRIBUTION = "REMPLACEMENT_ATTRIBUTION",
}

export enum TypeEspace {
  BANNIERE = "BANNIERE",
  PUBLICITE = "PUBLICITE",
}

export enum TypeFichier {
  AVATAR = "AVATAR",
  EXCEL = "EXCEL",
  LOGO = "LOGO",
  PDF = "PDF",
  PHOTO = "PHOTO",
}

export enum TypeMotif {
  E = "E",
  S = "S",
}

export enum TypePartenaire {
  NEGOCIATION_EN_COURS = "NEGOCIATION_EN_COURS",
  NON_PARTENAIRE = "NON_PARTENAIRE",
  NOUVEAU_REFERENCEMENT = "NOUVEAU_REFERENCEMENT",
  PARTENARIAT_ACTIF = "PARTENARIAT_ACTIF",
}

export enum TypePresidentCible {
  PHARMACIE = "PHARMACIE",
  PRESIDENT = "PRESIDENT",
}

export enum TypePrestataire {
  A = "A",
  D = "D",
  E = "E",
  M = "M",
}

export enum TypeProject {
  GROUPEMENT = "GROUPEMENT",
  PERSONNEL = "PERSONNEL",
  PROFESSIONNEL = "PROFESSIONNEL",
}

export enum TypeRole {
  GROUPEMENT = "GROUPEMENT",
  LABO = "LABO",
  PHARMACIE = "PHARMACIE",
  REGION = "REGION",
  SERVICE = "SERVICE",
  STATUT = "STATUT",
  SU = "SU",
  TITULAIRE = "TITULAIRE",
}

export enum TypeService {
  GROUPEMENT = "GROUPEMENT",
  LIBRE = "LIBRE",
  PARTENAIRE = "PARTENAIRE",
}

export enum UserStatus {
  ACTIVATED = "ACTIVATED",
  ACTIVATION_REQUIRED = "ACTIVATION_REQUIRED",
  ARCHIVED = "ARCHIVED",
  BLOCKED = "BLOCKED",
  RESETED = "RESETED",
}

export interface ActionCommentaireInput {
  id?: string | null;
  content: string;
}

export interface ActionInput {
  id?: string | null;
  description: string;
  ordre?: number | null;
  idProject?: string | null;
  idsEtiquettes?: string[] | null;
  idImportance?: string | null;
  idOrigine?: string | null;
  idActionType?: string | null;
  codeItem?: string | null;
  idItemAssocie?: string | null;
  idSection?: string | null;
  status?: ActionStatus | null;
  dateDebut?: any | null;
  dateFin?: any | null;
  commentaire?: ActionCommentaireInput | null;
  isInInbox?: boolean | null;
  isInInboxTeam?: boolean | null;
  isPrivate?: boolean | null;
  isRemoved?: boolean | null;
  idActionParent?: string | null;
  codeMaj?: string | null;
  idsUsersDestinations?: string[] | null;
}

export interface AddEtiquetteParticipantsInput {
  idEtiquette: string;
  idsParticipants: string[];
}

export interface AddParticipantsInput {
  idProject: string;
  idsParticipants: string[];
}

export interface AesInput {
  key?: string | null;
  requestUrl?: string | null;
  hexadecimal?: boolean | null;
}

export interface AffectationInput {
  id?: string | null;
  typeAffectation: TypeAffectation;
  idFonction: string;
  idsDepartements: string[];
  idAffecte: string;
  idRemplacent?: string | null;
  dateDebut?: any | null;
  dateFin?: any | null;
}

export interface AideInput {
  id?: string | null;
  title: string;
  description: string;
}

export interface ApplicationRoleInput {
  idrole: string;
  idGroupement: string;
  idApplications: string;
}

export interface ArticleCommande {
  id: string;
  quantite: number;
}

export interface AssignOrUnAssignActionInput {
  idAction: string;
  idUserDestination: string;
}

export interface CommentInput {
  id?: string | null;
  codeItem: string;
  idItemAssocie: string;
  content: string;
  fichiers?: FichierInput[] | null;
}

export interface ContactInput {
  id?: string | null;
  adresse1?: string | null;
  adresse2?: string | null;
  cp?: string | null;
  ville?: string | null;
  pays?: string | null;
  telProf?: string | null;
  faxProf?: string | null;
  faxPerso?: string | null;
  telMobProf?: string | null;
  telPerso?: string | null;
  telMobPerso?: string | null;
  mailProf?: string | null;
  mailPerso?: string | null;
  siteProf?: string | null;
  sitePerso?: string | null;
  whatsAppMobProf?: string | null;
  whatsappMobPerso?: string | null;
  compteSkypeProf?: string | null;
  compteSkypePerso?: string | null;
  urlLinkedinProf?: string | null;
  urlLinkedinPerso?: string | null;
  urlTwitterProf?: string | null;
  urlTwitterPerso?: string | null;
  urlFacebookProf?: string | null;
  urlFacebookPerso?: string | null;
  urlMessenger?: string | null;
  urlYoutube?: string | null;
}

export interface CouleurInput {
  id?: string | null;
  code: string;
  libelle: string;
  isRemoved?: boolean | null;
}

export interface CountTodosInput {
  idUser: string;
  typesProjects: TypeProject[];
  taskStatus?: ActionStatus | null;
  myTask?: boolean | null;
  taskOfOthers?: boolean | null;
  unassignedTask?: boolean | null;
  withDate?: boolean | null;
  withoutDate?: boolean | null;
  useMatriceResponsabilite?: boolean | null;
}

export interface ExternalMappingInput {
  userID: string;
  md5Id: string;
  idExternaluser?: string | null;
  idClient?: string | null;
  password?: string | null;
}

export interface FichierInput {
  id?: string | null;
  idAvatar?: string | null;
  chemin: string;
  nomOriginal: string;
  type: TypeFichier;
}

export interface GroupeAmisDetailInput {
  id?: string | null;
  idGroupeAmis: string;
  idPharmacie: string;
  confirmedInvitation?: boolean | null;
  isRemoved?: boolean | null;
}

export interface GroupeAmisInput {
  id?: string | null;
  nom: string;
  description?: string | null;
  status: GroupeAmisStatus;
  type: GroupeAmisType;
  predefine?: boolean | null;
  codeMaj?: string | null;
  isRemoved?: boolean | null;
  idPharmacie?: string | null;
  idDepartement?: string | null;
  idRegion?: string | null;
}

export interface GroupeClienRemisetInput {
  codeGroupeClient: number;
  idRemise?: string | null;
  remiseDetails?: (RemiseDetailInput | null)[] | null;
}

export interface IdeeOuBonnePratiqueInput {
  id?: string | null;
  origine: IdeeOuBonnePratiqueOrigine;
  status: IdeeOuBonnePratiqueStatus;
  idClassification?: string | null;
  title: string;
  idAuteur: string;
  concurent?: string | null;
  idFournisseur?: string | null;
  idPrestataire?: string | null;
  idGroupeAmis?: string | null;
  idService?: string | null;
  beneficiaires_cles?: string | null;
  contexte?: string | null;
  objectifs?: string | null;
  resultats?: string | null;
  facteursClesDeSucces?: string | null;
  contraintes?: string | null;
  codeMaj?: string | null;
  isRemoved?: boolean | null;
  fichiers?: FichierInput[] | null;
}

export interface IdeeOuBonnePratiqueLuInput {
  id?: string | null;
  idIdeeOuBonnePratique: string;
  idUser: string;
  isRemoved?: boolean | null;
}

export interface InformationLiaisonFilter {
  idPharmacie: string;
  idsCollegueConcernee?: string[] | null;
  idsDeclarant?: string[] | null;
  dateDebut?: any | null;
  dateFin?: any | null;
}

export interface InformationLiaisonInput {
  id?: string | null;
  idType: string;
  idItem?: string | null;
  idItemAssocie?: string | null;
  idOrigine?: string | null;
  idOrigineAssocie?: string | null;
  statut: string;
  titre: string;
  description: string;
  idDeclarant: string;
  idColleguesConcernees?: string[] | null;
  idFicheIncident?: string | null;
  idFicheAmelioration?: string | null;
  idFicheReclamation?: string | null;
  idTodoAction?: string | null;
  codeMaj?: string | null;
  isRemoved?: boolean | null;
  bloquant?: boolean | null;
  fichiers?: FichierInput[] | null;
  idFonction?: string | null;
  idTache?: string | null;
  idImportance: string;
  idUrgence: string;
  dateEcheance?: any | null;
}

export interface LaboratoirePartenaireInput {
  id?: string | null;
  debutPartenaire?: any | null;
  finPartenaire?: any | null;
  typePartenaire?: TypePartenaire | null;
  idLaboratoire: string;
  statutPartenaire?: StatutPartenaire | null;
  adresse?: string | null;
  codePostal?: string | null;
  ville?: string | null;
  Pays?: string | null;
}

export interface LaboratoireRepresentantInput {
  id?: string | null;
  idLaboratoire: string;
  type?: string | null;
  photo?: FichierInput | null;
  civilite?: string | null;
  nom: string;
  prenom?: string | null;
  sexe?: string | null;
  fonction?: string | null;
  afficherComme?: string | null;
  contact?: ContactInput | null;
}

export interface LaboratoireRessourceInput {
  id?: string | null;
  idLaboratoire?: string | null;
  typeRessource?: string | null;
  idItem?: string | null;
  code: string;
  libelle: string;
  dateChargement?: any | null;
  fichier?: FichierInput | null;
  url?: string | null;
}

export interface LaboratoireSuiteInput {
  adresse?: string | null;
  codePostal?: string | null;
  telephone?: string | null;
  ville?: string | null;
  telecopie?: string | null;
  email?: string | null;
  webSiteUrl?: string | null;
}

export interface MappingVariableInput {
  nom?: string | null;
  value?: string | null;
}

export interface MarcheLaboratoireInput {
  idLaboratoire: string;
  obligatoire: boolean;
}

export interface Md5Input {
  beginGet?: number | null;
  endGet?: number | null;
  hexadecimal?: boolean | null;
  requestUrl?: string | null;
  haveExternalUserMapping?: boolean | null;
}

export interface MessagerieAttachmentInput {
  chemin: string;
  nomOriginal: string;
}

export interface MessagerieInput {
  id?: string | null;
  recepteursIds: string[];
  objet: string;
  message: string;
  typeMessagerie?: MessagerieType | null;
  themeId?: string | null;
  sourceId?: string | null;
  attachments?: MessagerieAttachmentInput[] | null;
}

export interface OperationArticleInput {
  idCanalArticle?: string | null;
  quantite?: number | null;
}

export interface OtherInput {
  function?: string | null;
  requestUrl?: string | null;
}

export interface ParameterInput {
  id: string;
  value?: string | null;
}

export interface PartageInput {
  id?: string | null;
  type: PartageType;
  toutesPharmacies?: boolean | null;
  toutesGroupesAmis?: boolean | null;
  codeItem: string;
  idItemAssocie: string;
  idUserPartageant?: string | null;
  idFonctionCible?: string | null;
  idsGroupesAmis?: string[] | null;
  idsPharmacies?: string[] | null;
  dateHeurePartage: any;
  codeMaj?: string | null;
  isRemoved?: boolean | null;
}

export interface PartenaireRepresentantAffectationInput {
  id?: string | null;
  typeAffectation: TypeAffectation;
  fonction?: string | null;
  idsDepartements: string[];
  idAffecte: string;
  idRemplacent?: string | null;
  dateDebut?: any | null;
  dateFin?: any | null;
}

export interface PartenaireRepresentantInput {
  id?: string | null;
  photo?: FichierInput | null;
  civilite?: string | null;
  nom: string;
  prenom?: string | null;
  sexe?: string | null;
  fonction?: string | null;
  idPartenaire: string;
  idPartenaireType?: string | null;
  afficherComme?: string | null;
  contact?: ContactInput | null;
}

export interface PartenaireServiceInput {
  id?: string | null;
  nom: string;
  dateDebut: any;
  dateFin: any;
  idTypeService?: string | null;
  typePartenaire?: TypePartenaire | null;
  statutPartenaire?: StatutPartenaire | null;
  adresse1?: string | null;
  codePostal?: string | null;
  ville?: string | null;
  servicePartenaires?: ServicePartenaireInput[] | null;
}

export interface PharmacieAchatInput {
  dateDebut?: string | null;
  dateFin?: string | null;
  identifiantAchatCanal?: string | null;
  commentaire?: string | null;
  idCanal?: string | null;
}

export interface PharmacieCapInput {
  cap?: boolean | null;
  dateDebut?: string | null;
  dateFin?: string | null;
  identifiantCap?: string | null;
  commentaire?: string | null;
}

export interface PharmacieComptaInput {
  siret?: string | null;
  codeERP?: string | null;
  ape?: string | null;
  tvaIntra?: string | null;
  rcs?: string | null;
  contactCompta?: string | null;
  chequeGrp?: boolean | null;
  valeurChequeGrp?: number | null;
  commentaireChequeGrp?: string | null;
  droitAccesGrp?: boolean | null;
  valeurChequeAccesGrp?: number | null;
  commentaireChequeAccesGrp?: string | null;
  structureJuridique?: string | null;
  denominationSociale?: string | null;
  telCompta?: string | null;
  modifStatut?: boolean | null;
  raisonModif?: string | null;
  dateModifStatut?: string | null;
  nomBanque?: string | null;
  banqueGuichet?: string | null;
  banqueCompte?: string | null;
  banqueRib?: string | null;
  banqueCle?: string | null;
  dateMajRib?: string | null;
  iban?: string | null;
  swift?: string | null;
  dateMajIban?: string | null;
}

/**
 * pharmacie vente
 */
export interface PharmacieConceptInput {
  idFournisseurMobilier?: string | null;
  dateInstallConcept?: string | null;
  avecConcept?: boolean | null;
  idConcept?: string | null;
  avecFacade?: boolean | null;
  dateInstallFacade?: string | null;
  idEnseigniste?: string | null;
  conformiteFacade?: boolean | null;
  idFacade?: string | null;
  surfaceTotale?: number | null;
  surfaceVente?: number | null;
  SurfaceVitrine?: number | null;
  volumeLeaflet?: number | null;
  nbreLineaire?: number | null;
  nbreVitrine?: number | null;
  otcLibAcces?: boolean | null;
  idLeaflet?: string | null;
  commentaire?: string | null;
}

export interface PharmacieDigitaleInput {
  id?: string | null;
  idServicePharmacie?: string | null;
  idPrestataire?: string | null;
  flagService?: boolean | null;
  dateInstallation?: string | null;
  url?: string | null;
}

export interface PharmacieEntreeSortieInput {
  idContrat?: string | null;
  idConcurent?: string | null;
  idConcurentAncien?: string | null;
  dateEntree?: string | null;
  idMotifEntree?: string | null;
  commentaireEntree?: string | null;
  sortieFuture?: number | null;
  dateSortieFuture?: string | null;
  idMotifSortieFuture?: string | null;
  commentaireSortieFuture?: string | null;
  sortie?: number | null;
  dateSortie?: string | null;
  idMotifSortie?: string | null;
  commentaireSortie?: string | null;
  dateSignature?: string | null;
}

export interface PharmacieInformatiqueInput {
  idLogiciel?: string | null;
  numVersion?: string | null;
  dateLogiciel?: string | null;
  nbrePoste?: number | null;
  nbreComptoir?: number | null;
  nbreBackOffice?: number | null;
  nbreBureau?: number | null;
  commentaire?: string | null;
  idAutomate1?: string | null;
  dateInstallation1?: string | null;
  idAutomate2?: string | null;
  dateInstallation2?: string | null;
  idAutomate3?: string | null;
  dateInstallation3?: string | null;
}

export interface PharmacieRolesInput {
  roles?: string[] | null;
  pharmacies?: string[] | null;
}

export interface PharmacieSatisfactionInput {
  dateSaisie?: string | null;
  commentaire?: string | null;
  idSmyley?: string | null;
}

export interface PharmacieSegmentationInput {
  idTypologie?: string | null;
  idTrancheCA?: string | null;
  idQualite?: string | null;
  idContrat?: string | null;
  dateSignature?: string | null;
}

export interface PharmacieStatCAInput {
  exercice?: number | null;
  dateDebut?: string | null;
  dateFin?: string | null;
  caTTC?: number | null;
  caHt?: number | null;
  caTVA1?: number | null;
  tauxTVA1?: number | null;
  caTVA2?: number | null;
  tauxTVA2?: number | null;
  caTVA3?: number | null;
  tauxTVA3?: number | null;
  caTVA4?: number | null;
  tauxTVA4?: number | null;
  caTVA5?: number | null;
  tauxTVA5?: number | null;
}

export interface PpersonnelInput {
  id?: string | null;
  idPharmacie: string;
  idGroupement: string;
  civilite: string;
  nom?: string | null;
  prenom?: string | null;
  telBureau?: string | null;
  telMobile?: string | null;
  mail?: string | null;
  estAmbassadrice?: boolean | null;
  sortie?: number | null;
  commentaire?: string | null;
}

export interface PresidentRegionsInput {
  type?: TypePresidentCible | null;
  presidents?: string[] | null;
}

export interface ProduitCanalInput {
  id?: string | null;
  qteStock?: number | null;
  qteMin?: number | null;
  stv?: number | null;
  unitePetitCond?: number | null;
  prixFab?: number | null;
  prixPhv?: number | null;
  datePeremption?: any | null;
  dateRetrait?: any | null;
  dateCreation?: any | null;
  dateModification?: any | null;
  idCanalSousGamme?: string | null;
  codeSousGammeCatalogue?: number | null;
  codeCommandeCanal?: string | null;
  isRemoved: boolean;
}

export interface ProduitInput {
  id?: string | null;
}

export interface ProjectInput {
  id?: string | null;
  ordre?: number | null;
  name: string;
  isArchived?: boolean | null;
  isShared?: boolean | null;
  isFavoris?: boolean | null;
  isRemoved?: boolean | null;
  typeProject: TypeProject;
  idUser: string;
  idParent?: string | null;
  idPharmacie: string;
  idCouleur?: string | null;
}

export interface RemiseDetailInput {
  quantiteMin?: number | null;
  quantiteMax?: number | null;
  pourcentageRemise?: number | null;
  remiseSupplementaire?: number | null;
}

export interface RemoveEtiquetteParticipantInput {
  idEtiquette: string;
  idParticipant: string;
}

export interface RemoveParticipantInput {
  idProject: string;
  idParticipant: string;
}

export interface RessourceInput {
  id?: string | null;
  idPartenaireService?: string | null;
  typeRessource?: string | null;
  idItem?: string | null;
  code: string;
  libelle: string;
  dateChargement?: any | null;
  fichier?: FichierInput | null;
  url?: string | null;
}

export interface RgpdAccueilInput {
  id?: string | null;
  title: string;
  description: string;
}

export interface RgpdAccueilPlusInput {
  id?: string | null;
  title: string;
  description: string;
  order?: number | null;
}

export interface RgpdAutorisationInput {
  id?: string | null;
  title: string;
  description: string;
  order?: number | null;
}

export interface RgpdHistoriqueInfoPlusInput {
  id?: string | null;
  type: RgpdHistoriqueInfoPlusType;
  idHistorique?: string | null;
  idItemAssocie: string;
  accepted: boolean;
}

export interface RgpdHistoriqueInput {
  id?: string | null;
  accept_all?: boolean | null;
  refuse_all?: boolean | null;
  idUser: string;
  historiquesInfosPlus?: RgpdHistoriqueInfoPlusInput[] | null;
}

export interface RgpdInput {
  id?: string | null;
  politiqueConfidentialite?: string | null;
  conditionUtilisation?: string | null;
  informationsCookies?: string | null;
}

export interface RgpdPartenaireInput {
  id?: string | null;
  title: string;
  description: string;
  order?: number | null;
}

export interface SamlInput {
  idpX509Certificate?: string | null;
  spX509Certificate?: string | null;
  consumerUrl?: string | null;
}

export interface ServicePartenaireInput {
  id?: string | null;
  nom: string;
  dateDemarrage: any;
  nbCollaboQualifie?: number | null;
  commentaire?: string | null;
  produit?: ProduitInput | null;
}

export interface TakeChargeInformationLiaisonInput {
  id: string;
  date: any;
  description: string;
  files?: FichierInput[] | null;
}

export interface TicketCibleInput {
  id?: string | null;
  idTicket?: string | null;
  idCurrentUser: string;
  idUserCible?: string | null;
  idRoleCible?: string | null;
  idServiceCible?: string | null;
  idPharmacieCible?: string | null;
  idLaboratoireCible?: string | null;
  idPrestataireServiceCible?: string | null;
  dateHeureAffectation?: any | null;
  commentaire?: string | null;
}

export interface TicketInput {
  id?: string | null;
  origine: string;
  idOrganisation: string;
  nomInterlocuteur?: string | null;
  telephoneInterlocuteur?: string | null;
  dateHeureSaisie?: any | null;
  commentaire?: string | null;
  idSmyley?: string | null;
  visibilite: TicketVisibilite;
  typeTicket: TicketType;
  typeAppel?: TicketAppel | null;
  declarant?: string | null;
  idMotif?: string | null;
  fichiers?: FichierInput[] | null;
  idsUserConcernees?: (string | null)[] | null;
  priority?: number | null;
  status?: StatusTicket | null;
}

export interface TodoEtiquetteInput {
  id?: string | null;
  ordre?: number | null;
  nom: string;
  idCouleur?: string | null;
  codeMaj?: string | null;
  isRemoved?: boolean | null;
  isFavoris?: boolean | null;
}

export interface TodoSectionInput {
  id?: string | null;
  ordre?: number | null;
  libelle: string;
  isInInbox?: boolean | null;
  isInInboxTeam?: boolean | null;
  idProject?: string | null;
  idUser: string;
  isRemoved?: boolean | null;
  isArchived?: boolean | null;
}

export interface TokenFtpInput {
  requestUrl?: string | null;
  ftpFilePath?: string | null;
  ftpFileName?: string | null;
}

export interface TokenInput {
  reqTokenUrl?: string | null;
  reqUserUrl?: string | null;
}

export interface UpdateActionDateDebutFinInput {
  idAction: string;
  dateDebut: any;
  dateFin: any;
}

export interface UpdateActionStatusInput {
  idAction: string;
  status: ActionStatus;
}

export interface UpdateMappingVariableInput {
  id: string;
  nom?: string | null;
  value?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
