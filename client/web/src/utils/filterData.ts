export const filterBySeen = (data: any[], statusFilter: string) => {
  switch (statusFilter) {
    case 'read':
      data = data.filter((item: any) => item && item.seen === true);
      return data;
    case 'unread':
      data = data.filter((item: any) => item && item.seen === false);
      return data;
    default:
      return data;
  }
};
