import { isInteger } from 'lodash';

export const capitalize = (text: string): string => {
  const texLower = text.toLowerCase();
  return texLower.charAt(0).toUpperCase() + texLower.slice(1);
};

export const makeElementsUnique = (elements: any, uniqueKey: any): any => {
  if (!elements || !uniqueKey) {
    return elements;
  }

  return [...(new Set(elements.map((elt: any) => elt[uniqueKey])) as any)].map(elementKey =>
    elements.find((elt: any) => elt[uniqueKey] === elementKey),
  );
};

export const strippedString = (text: string) => {

  const tmp = document.createElement("DIV");
  tmp.innerHTML = text;
  return tmp.textContent || tmp.innerText || "";

  // return text.replace(/(<([^>]+)>)/gi, '');
};

/**
 *
 * @param str {String}
 */
export const stringToAvatar = (str: string): string => {
  return str
    .split(' ')
    .map((item: any) => item.slice(0, 1))
    .join('')
    .toUpperCase()
    .slice(0, 2);
};

/**
 *
 * @param num {number}
 */
export const roundNumber = (num: number): number => {
  return isInteger(num) ? num : parseFloat(parseFloat(num.toString()).toFixed(2));
};

/**
 *
 * @param str {string}
 * @param length {number}
 */
export const trimmedString = (str: string, length: number): string => {
  return str.length > length ? `${str.substring(0, length)}...` : str;
};

export const getMonthNumber = (month?: string): number => {
  switch (month) {
    case 'Janvier':
      return 1;
    case 'Février':
      return 2;
    case 'Mars':
      return 3;
    case 'Avril':
      return 4;
    case 'Mai':
      return 5;
    case 'Juin':
      return 6;
    case 'Juillet':
      return 7;
    case 'Août':
      return 8;
    case 'Septembre':
      return 9;
    case 'Octobre':
      return 10;
    case 'Novembre':
      return 11;
    default:
      return 12;
  }
};

export const getMonthName = (month?: number): string => {
  switch (month) {
    case 1:
      return 'Janvier';
    case 2:
      return 'Février';
    case 3:
      return 'Mars';
    case 4:
      return 'Avril';
    case 5:
      return 'Mai';
    case 6:
      return 'Juin';
    case 7:
      return 'Juillet';
    case 8:
      return 'Août';
    case 9:
      return 'Septembre';
    case 10:
      return 'Octobre';
    case 11:
      return 'Novembre';
    default:
      return 'Décembre';
  }
};

export const getMonths = () => [
  'Janvier',
  'Février',
  'Mars',
  'Avril',
  'Mai',
  'Juin',
  'Juillet',
  'Août',
  'Septembre',
  'Octobre',
  'Novembre',
  'Décembre',
];

export const isInvalidArray = (array: any[]): boolean => {
  return array.some((el: any) => {
    return el === null || el === undefined;
  });
};

export const formatIBAN = (iban: string): string => {
  const parts: string[] = iban.split('.');
  const firstPart: string = parts[0]
    .toString()
    .replace(/[^\dA-Z]/g, '')
    .replace(/(.{4})/g, '$1 ')
    .trim();
  return firstPart + (parts.length > 1 ? '.' + parts[1] : '');
};

export const years = (startYear: number): number[] => {
  const currentYear = new Date().getFullYear();
  const years: number[] = [];
  while (startYear <= currentYear) {
    years.push(startYear++);
  }
  return years;
};

export const normalizeString = (str: string) => {
  return str
    .normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .replace(/[^a-zA-Z]/g, '');
};

export const formatBytes = (bytes: number, decimals?: number) => {
  if (bytes === 0) return '0 Bytes';
  const k = 1024;
  const dm = decimals || 2;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  const i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
};

export const isMobile = () => {
  return window.innerWidth < 960;
};

export const isMd = () => {
  return window.innerWidth >= 960 && window.innerWidth < 1280;
};

export const isLg = () => {
  return window.innerWidth >= 1280 && window.innerWidth < 1920;
};

export const isSm = () => {
  return window.innerWidth >= 600 && window.innerWidth < 960;
};

