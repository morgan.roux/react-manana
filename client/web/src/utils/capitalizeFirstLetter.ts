export const capitalizeFirstLetter = (text: any) => {
  return typeof text === 'number'
    ? text
    : text && text.length > 0
    ? text[0].toUpperCase() + text.slice(1).toLowerCase()
    : '';
};
