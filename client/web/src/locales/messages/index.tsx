import en_US from './en-US';
import fr_FR from './fr-FR';
import mg_MG from './mg-MG';

export default {
  'en-US': en_US,
  'fr-FR': fr_FR,
  'mg-MG': mg_MG,
};
