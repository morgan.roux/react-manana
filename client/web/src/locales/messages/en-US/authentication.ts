export default {
  'auth.login.title': 'Log in',
  'auth.login.labels.email': 'Email Address',
  'auth.login.labels.password': 'Password',
  'auth.login.labels.loading': 'Loading...',
  'auth.login.labels.login': 'Log in',
  'auth.login.labels.signup': 'Sign up',

  'auth.signup.title': 'Sign Up',
  'auth.signup.labels.email': 'Email Address',
  'auth.signup.labels.password': 'Password',
  'auth.signup.labels.password.confirm': 'Re-enter your password',
  'auth.signup.labels.loading': 'Loading...',
  'auth.signup.labels.signup': 'Sign up',
  'auth.signup.message.waitConfirm':
    "Veuillez confirmer votre adresse email pour pouvoir utiliser l'application. Vous pouvez fermer cet onglet.",
  'auth.signup.confirmation.processing': 'Processing...',
  'auth.signup.confirmation.expired': 'This token is expired.',
  'auth.signup.confirmation.networkError': 'Network Error. Please verify your Internet connection.',
  'auth.signup.confirmation.confirmed':
    'Your email address has been confirmed, you can sign in now.',
};
