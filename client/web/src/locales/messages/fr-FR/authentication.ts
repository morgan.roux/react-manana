export default {
  'auth.login': 'Connexion',
  'auth.login.labels.login': 'Se connecter',
  'auth.login.labels.loading': 'Chargement...',
  'auth.login.labels.email': 'Email',
  'auth.login.labels.loginLabel': 'Login',
  'auth.login.labels.password': 'Mot de passe',
};
