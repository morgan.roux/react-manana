import Authentication from './authentication';
import Home from './home';

export default {
  ...Authentication,
  ...Home,
};
