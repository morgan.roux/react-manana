/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GroupeAmises
// ====================================================

export interface GroupeAmises_groupeAmises {
  __typename: "GroupeAmis";
  id: string;
  nom: string;
  nbPharmacie: number;
  nbMember: number;
  nbPartage: number;
  nbRecommandation: number;
  dateCreation: any;
}

export interface GroupeAmises {
  groupeAmises: GroupeAmises_groupeAmises[];
}

export interface GroupeAmisesVariables {
  isRemoved?: boolean | null;
}
