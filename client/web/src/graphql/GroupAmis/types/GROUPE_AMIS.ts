/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GroupeAmisType, GroupeAmisStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: GROUPE_AMIS
// ====================================================

export interface GROUPE_AMIS_groupeAmis_pharmacieMembres_users {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface GROUPE_AMIS_groupeAmis_pharmacieMembres {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  users: (GROUPE_AMIS_groupeAmis_pharmacieMembres_users | null)[] | null;
}

export interface GROUPE_AMIS_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
  description: string | null;
  typeGroupeAmis: GroupeAmisType;
  nbPharmacie: number;
  groupStatus: GroupeAmisStatus;
  nbMember: number;
  nbPartage: number;
  nbRecommandation: number;
  dateCreation: any | null;
  pharmacieMembres: GROUPE_AMIS_groupeAmis_pharmacieMembres[] | null;
}

export interface GROUPE_AMIS {
  groupeAmis: GROUPE_AMIS_groupeAmis | null;
}

export interface GROUPE_AMISVariables {
  id: string;
}
