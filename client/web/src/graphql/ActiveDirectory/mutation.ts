import gql from 'graphql-tag';
import { ACTIVE_DIRECTORY_INFO } from './fragment';

export const DO_CREATE_ACTIVE_DIRECTORY = gql`
  mutation CREATE_ACTIVE_DIRECTORY(
    $idGroupement: ID!
    $url: String
    $user: String
    $password: String
    $base: String
    $ldapType: LdapType
  ) {
    createActiveDirectoryCredential(
      idGroupement: $idGroupement
      url: $url
      user: $user
      password: $password
      base: $base
      ldapType: $ldapType
    ) {
      ...ActiveDirectoryInfo
    }
  }
  ${ACTIVE_DIRECTORY_INFO}
`;

export const DO_UPDATE_ACTIVE_DIRECTORY = gql`
  mutation UPDATE_ACTIVE_DIRECTORY(
    $id: ID!
    $url: String
    $user: String
    $password: String
    $base: String
    $ldapType: LdapType
  ) {
    updateActiveDirectoryCredential(
      id: $id
      url: $url
      user: $user
      password: $password
      base: $base
      ldapType: $ldapType
    ) {
      ...ActiveDirectoryInfo
    }
  }
  ${ACTIVE_DIRECTORY_INFO}
`;
