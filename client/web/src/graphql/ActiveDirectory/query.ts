import gql from 'graphql-tag';
import { ACTIVE_DIRECTORY_INFO } from './fragment';

export const GET_ACTIVE_DIRECTORY_GROUPEMENT = gql`
  query ACTIVE_DIRECTORY_GROUPEMENT($idgroupement: ID!) {
    activedirectoryGroupement(idgroupement: $idgroupement) {
        ...ActiveDirectoryInfo
    }
  }
  ${ACTIVE_DIRECTORY_INFO}
`;
