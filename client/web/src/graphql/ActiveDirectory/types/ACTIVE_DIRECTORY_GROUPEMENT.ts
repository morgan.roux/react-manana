/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { LdapType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: ACTIVE_DIRECTORY_GROUPEMENT
// ====================================================

export interface ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement_groupement_defaultPharmacie_departement_region | null;
}

export interface ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement_groupement_defaultPharmacie_departement | null;
}

export interface ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement_groupement_groupementLogo_fichier | null;
}

export interface ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement_groupement_defaultPharmacie | null;
  groupementLogo: ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement_groupement_groupementLogo | null;
}

export interface ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement {
  __typename: "ActiveDirectoryCredential";
  id: string;
  url: string;
  user: string | null;
  password: string | null;
  base: string | null;
  ldapType: LdapType | null;
  groupement: ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement_groupement | null;
}

export interface ACTIVE_DIRECTORY_GROUPEMENT {
  activedirectoryGroupement: ACTIVE_DIRECTORY_GROUPEMENT_activedirectoryGroupement | null;
}

export interface ACTIVE_DIRECTORY_GROUPEMENTVariables {
  idgroupement: string;
}
