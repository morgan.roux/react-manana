import gql from 'graphql-tag';

import { GROUPEMENT_INFO } from '../Groupement/fragement';

export const ACTIVE_DIRECTORY_INFO = gql`
  fragment ActiveDirectoryInfo on ActiveDirectoryCredential {
    id
    url
    user
    password
    base
    ldapType
    groupement {
      ...GroupementInfo
    }
  }
  ${GROUPEMENT_INFO}
`;
