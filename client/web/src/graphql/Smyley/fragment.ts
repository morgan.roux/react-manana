import { USER_INFO_FRAGEMENT } from './../User/fragment';
import { GROUPEMENT_INFO } from './../Groupement/fragement';
import gql from 'graphql-tag';

export const SMYLEY_INFO_FRAGEMENT = gql`
  fragment SmyleyInfo on Smyley {
    id
    nom
    note
    photo
    groupement {
      ...GroupementInfo
    }
  }
  ${GROUPEMENT_INFO}
`;

export const USER_SMYLEY_INFO_FRAGMENT = gql`
  fragment UserSmyleyInfo on UserSmyley {
    id
    item {
      id
    }
    idSource
    user {
      ...UserInfo
    }
    smyley {
      ...SmyleyInfo
    }
    isRemoved
    dateCreation
    dateModification
  }
  ${USER_INFO_FRAGEMENT}
  ${SMYLEY_INFO_FRAGEMENT}
`;

export const USER_SMYLEY_RESULT_INFO_FRAGEMENT = gql`
  fragment UserSmyleyResultInfo on UserSmyleyResult {
    total
    data {
      ...UserSmyleyInfo
    }
  }
  ${USER_SMYLEY_INFO_FRAGMENT}
`;
