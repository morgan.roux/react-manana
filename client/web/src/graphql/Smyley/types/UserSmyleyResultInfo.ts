/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: UserSmyleyResultInfo
// ====================================================

export interface UserSmyleyResultInfo_data_item {
  __typename: "Item";
  id: string;
}

export interface UserSmyleyResultInfo_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UserSmyleyResultInfo_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface UserSmyleyResultInfo_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: UserSmyleyResultInfo_data_user_userPhoto_fichier | null;
}

export interface UserSmyleyResultInfo_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface UserSmyleyResultInfo_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: UserSmyleyResultInfo_data_user_role | null;
  userPhoto: UserSmyleyResultInfo_data_user_userPhoto | null;
  pharmacie: UserSmyleyResultInfo_data_user_pharmacie | null;
}

export interface UserSmyleyResultInfo_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface UserSmyleyResultInfo_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: UserSmyleyResultInfo_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface UserSmyleyResultInfo_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: UserSmyleyResultInfo_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface UserSmyleyResultInfo_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface UserSmyleyResultInfo_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: UserSmyleyResultInfo_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface UserSmyleyResultInfo_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: UserSmyleyResultInfo_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: UserSmyleyResultInfo_data_smyley_groupement_groupementLogo | null;
}

export interface UserSmyleyResultInfo_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: UserSmyleyResultInfo_data_smyley_groupement | null;
}

export interface UserSmyleyResultInfo_data {
  __typename: "UserSmyley";
  id: string;
  item: UserSmyleyResultInfo_data_item | null;
  idSource: string | null;
  user: UserSmyleyResultInfo_data_user | null;
  smyley: UserSmyleyResultInfo_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface UserSmyleyResultInfo {
  __typename: "UserSmyleyResult";
  total: number;
  data: (UserSmyleyResultInfo_data | null)[] | null;
}
