import gql from 'graphql-tag';
import { SMYLEY_INFO_FRAGEMENT, USER_SMYLEY_RESULT_INFO_FRAGEMENT } from './fragment';

export const GET_SMYLEYS = gql`
  query SMYLEYS($idGroupement: ID!) {
    smyleys(idGroupement: $idGroupement) {
      ...SmyleyInfo
    }
  }
  ${SMYLEY_INFO_FRAGEMENT}
`;

export const GET_SMYLEYS_WITH_MIN_INFO = gql`
  query SMYLEYS_WITH_MIN_INFO($idGroupement: ID!) {
    smyleys(idGroupement: $idGroupement) {
      id
      nom
      photo
    }
  }
`;

export const GET_USER_SMYLEYS = gql`
  query USER_SMYLEYS($codeItem: String!, $idSource: String!) {
    userSmyleys(codeItem: $codeItem, idSource: $idSource) {
      ...UserSmyleyResultInfo
    }
  }
  ${USER_SMYLEY_RESULT_INFO_FRAGEMENT}
`;
