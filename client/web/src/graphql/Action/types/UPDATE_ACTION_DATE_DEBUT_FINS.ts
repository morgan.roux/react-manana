/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UpdateActionDateDebutFinInput, ActionStatus, TypeProject } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_ACTION_DATE_DEBUT_FINS
// ====================================================

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_origine {
  __typename: "TodoActionOrigine";
  id: string;
  code: string;
  libelle: string;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_section {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_actionParent {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  nbComment: number;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_subActions_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_subActions {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_subActions_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbComment: number;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_project {
  __typename: "Project";
  id: string;
  name: string | null;
  typeProject: TypeProject | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_userCreation_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_userCreation_role | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_assignedUsers_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_assignedUsers_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_assignedUsers_userPhoto_fichier | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_assignedUsers {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  userPhoto: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_assignedUsers_userPhoto | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_etiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_etiquettes {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  nom: string;
  isRemoved: boolean | null;
  couleur: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_etiquettes_couleur | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  _priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  idItemAssocie: string | null;
  nbComment: number;
  origine: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_origine | null;
  section: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_section | null;
  actionParent: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_actionParent | null;
  subActions: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_subActions[] | null;
  item: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_item | null;
  project: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_project | null;
  userCreation: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_userCreation | null;
  assignedUsers: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_assignedUsers[] | null;
  etiquettes: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins_etiquettes[] | null;
}

export interface UPDATE_ACTION_DATE_DEBUT_FINS {
  updateActionDateDebutFins: UPDATE_ACTION_DATE_DEBUT_FINS_updateActionDateDebutFins[];
}

export interface UPDATE_ACTION_DATE_DEBUT_FINSVariables {
  inputs: UpdateActionDateDebutFinInput[];
}
