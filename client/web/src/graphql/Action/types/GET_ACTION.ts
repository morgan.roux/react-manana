/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionStatus, TypeProject } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: GET_ACTION
// ====================================================

export interface GET_ACTION_action_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface GET_ACTION_action_origine {
  __typename: "TodoActionOrigine";
  id: string;
  code: string;
  libelle: string;
}

export interface GET_ACTION_action_section {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
}

export interface GET_ACTION_action_actionParent {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  nbComment: number;
}

export interface GET_ACTION_action_subActions_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface GET_ACTION_action_subActions {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: GET_ACTION_action_subActions_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbComment: number;
}

export interface GET_ACTION_action_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
}

export interface GET_ACTION_action_project {
  __typename: "Project";
  id: string;
  name: string | null;
  typeProject: TypeProject | null;
}

export interface GET_ACTION_action_userCreation_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_ACTION_action_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: GET_ACTION_action_userCreation_role | null;
}

export interface GET_ACTION_action_assignedUsers_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface GET_ACTION_action_assignedUsers_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: GET_ACTION_action_assignedUsers_userPhoto_fichier | null;
}

export interface GET_ACTION_action_assignedUsers {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  userPhoto: GET_ACTION_action_assignedUsers_userPhoto | null;
}

export interface GET_ACTION_action_etiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface GET_ACTION_action_etiquettes {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  nom: string;
  isRemoved: boolean | null;
  couleur: GET_ACTION_action_etiquettes_couleur | null;
}

export interface GET_ACTION_action {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  _priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: GET_ACTION_action_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  idItemAssocie: string | null;
  nbComment: number;
  origine: GET_ACTION_action_origine | null;
  section: GET_ACTION_action_section | null;
  actionParent: GET_ACTION_action_actionParent | null;
  subActions: GET_ACTION_action_subActions[] | null;
  item: GET_ACTION_action_item | null;
  project: GET_ACTION_action_project | null;
  userCreation: GET_ACTION_action_userCreation | null;
  assignedUsers: GET_ACTION_action_assignedUsers[] | null;
  etiquettes: GET_ACTION_action_etiquettes[] | null;
}

export interface GET_ACTION {
  action: GET_ACTION_action | null;
}

export interface GET_ACTIONVariables {
  id: string;
}
