/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionStatus, TypeProject } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_ACTION
// ====================================================

export interface SEARCH_ACTION_search_data_TodoActionType {
  __typename: "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_ACTION_search_data_Action_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface SEARCH_ACTION_search_data_Action_origine {
  __typename: "TodoActionOrigine";
  id: string;
  code: string;
  libelle: string;
}

export interface SEARCH_ACTION_search_data_Action_section {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
}

export interface SEARCH_ACTION_search_data_Action_actionParent {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  nbComment: number;
}

export interface SEARCH_ACTION_search_data_Action_subActions_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface SEARCH_ACTION_search_data_Action_subActions {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: SEARCH_ACTION_search_data_Action_subActions_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbComment: number;
}

export interface SEARCH_ACTION_search_data_Action_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
}

export interface SEARCH_ACTION_search_data_Action_project {
  __typename: "Project";
  id: string;
  name: string | null;
  typeProject: TypeProject | null;
}

export interface SEARCH_ACTION_search_data_Action_userCreation_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_ACTION_search_data_Action_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: SEARCH_ACTION_search_data_Action_userCreation_role | null;
}

export interface SEARCH_ACTION_search_data_Action_assignedUsers_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_ACTION_search_data_Action_assignedUsers_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_ACTION_search_data_Action_assignedUsers_userPhoto_fichier | null;
}

export interface SEARCH_ACTION_search_data_Action_assignedUsers {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  userPhoto: SEARCH_ACTION_search_data_Action_assignedUsers_userPhoto | null;
}

export interface SEARCH_ACTION_search_data_Action_etiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface SEARCH_ACTION_search_data_Action_etiquettes {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  nom: string;
  isRemoved: boolean | null;
  couleur: SEARCH_ACTION_search_data_Action_etiquettes_couleur | null;
}

export interface SEARCH_ACTION_search_data_Action {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  _priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: SEARCH_ACTION_search_data_Action_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  idItemAssocie: string | null;
  nbComment: number;
  origine: SEARCH_ACTION_search_data_Action_origine | null;
  section: SEARCH_ACTION_search_data_Action_section | null;
  actionParent: SEARCH_ACTION_search_data_Action_actionParent | null;
  subActions: SEARCH_ACTION_search_data_Action_subActions[] | null;
  item: SEARCH_ACTION_search_data_Action_item | null;
  project: SEARCH_ACTION_search_data_Action_project | null;
  userCreation: SEARCH_ACTION_search_data_Action_userCreation | null;
  assignedUsers: SEARCH_ACTION_search_data_Action_assignedUsers[] | null;
  etiquettes: SEARCH_ACTION_search_data_Action_etiquettes[] | null;
}

export type SEARCH_ACTION_search_data = SEARCH_ACTION_search_data_TodoActionType | SEARCH_ACTION_search_data_Action;

export interface SEARCH_ACTION_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_ACTION_search_data | null)[] | null;
}

export interface SEARCH_ACTION {
  search: SEARCH_ACTION_search | null;
}

export interface SEARCH_ACTIONVariables {
  query?: any | null;
  type?: (string | null)[] | null;
  filterBy?: any | null;
  sortBy?: any | null;
  take?: number | null;
  skip?: number | null;
}
