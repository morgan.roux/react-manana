/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionStatus, TypeProject } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: SubActionInfo
// ====================================================

export interface SubActionInfo_firstComment {
  __typename: "Comment";
  id: string;
  content: string;
}

export interface SubActionInfo_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface SubActionInfo_origine {
  __typename: "TodoActionOrigine";
  id: string;
  code: string;
  libelle: string;
}

export interface SubActionInfo_section {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
}

export interface SubActionInfo_actionParent {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  nbComment: number;
  isPrivate: boolean | null;
}

export interface SubActionInfo_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
}

export interface SubActionInfo_project_participants {
  __typename: "User";
  id: string;
}

export interface SubActionInfo_project {
  __typename: "Project";
  id: string;
  name: string | null;
  typeProject: TypeProject | null;
  participants: SubActionInfo_project_participants[] | null;
}

export interface SubActionInfo_userCreation_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SubActionInfo_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  idGroupement: string | null;
  role: SubActionInfo_userCreation_role | null;
}

export interface SubActionInfo_assignedUsers {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface SubActionInfo_etiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface SubActionInfo_etiquettes {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  nom: string;
  isRemoved: boolean | null;
  couleur: SubActionInfo_etiquettes_couleur | null;
}

export interface SubActionInfo_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
}

export interface SubActionInfo {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  codeMaj: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isPrivate: boolean | null;
  isRemoved: boolean | null;
  firstComment: SubActionInfo_firstComment | null;
  actionType: SubActionInfo_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  idItemAssocie: string | null;
  nbComment: number;
  origine: SubActionInfo_origine | null;
  section: SubActionInfo_section | null;
  actionParent: SubActionInfo_actionParent | null;
  item: SubActionInfo_item | null;
  project: SubActionInfo_project | null;
  userCreation: SubActionInfo_userCreation | null;
  assignedUsers: SubActionInfo_assignedUsers[] | null;
  etiquettes: SubActionInfo_etiquettes[] | null;
  importance: SubActionInfo_importance | null;
}
