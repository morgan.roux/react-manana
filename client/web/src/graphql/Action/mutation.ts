import gql from 'graphql-tag';
import { ACTION_INFO_FRAGEMENT } from './fragment';

export const DO_CREATE_ACTION = gql`
  mutation CREATE_ACTION($input: ActionInput!) {
    createUpdateAction(input: $input) {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;

export const DO_ASSIGN_ACTION_TO_USER = gql`
  mutation ASSIGN_ACTION_TO_USER($input: AssignOrUnAssignActionInput!) {
    assignActionToUser(input: $input) {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;

export const DO_UPDATE_MANY_ACTIONS = gql`
  mutation UPDATE_MANY_ACTIONS($inputs: [ActionInput!]!) {
    updateManyActions(inputs: $inputs) {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;

export const DO_SOFT_DELETE_ACTIONS = gql`
  mutation SOFT_DELETE_ACTIONS($ids: [ID!]!) {
    softDeleteActions(ids: $ids) {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;

export const DO_UPDATE_ACTION_STATUS = gql`
  mutation UPDATE_ACTION_STATUS($input: UpdateActionStatusInput!) {
    updateActionStatus(input: $input) {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;

export const DO_UPDATE_ACTION_DATE_DEBUT_FIN = gql`
  mutation UPDATE_ACTION_DATE_DEBUT_FIN($input: UpdateActionDateDebutFinInput!) {
    updateActionDateDebutFin(input: $input) {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;

export const DO_UPDATE_ACTION_DATE_DEBUT_FINS = gql`
  mutation UPDATE_ACTION_DATE_DEBUT_FINS($inputs: [UpdateActionDateDebutFinInput!]!) {
    updateActionDateDebutFins(inputs: $inputs) {
      ...ActionInfo
    }
  }
  ${ACTION_INFO_FRAGEMENT}
`;
