/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: AUTOMATES_WITH_MINIM_INFO
// ====================================================

export interface AUTOMATES_WITH_MINIM_INFO_automates {
  __typename: "Automate";
  id: string;
  modeleAutomate: string | null;
}

export interface AUTOMATES_WITH_MINIM_INFO {
  automates: (AUTOMATES_WITH_MINIM_INFO_automates | null)[] | null;
}
