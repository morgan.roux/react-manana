/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePrestataire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: AutomateInfo
// ====================================================

export interface AutomateInfo_prestataire {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface AutomateInfo {
  __typename: "Automate";
  id: string;
  modeleAutomate: string | null;
  dateCommercialisation: any | null;
  prestataire: AutomateInfo_prestataire | null;
}
