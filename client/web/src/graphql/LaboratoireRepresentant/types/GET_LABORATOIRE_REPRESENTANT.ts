/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePartenaire, StatutPartenaire, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: GET_LABORATOIRE_REPRESENTANT
// ====================================================

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_contact {
  __typename: "Contact";
  id: string;
  adresse1: string | null;
  ville: string | null;
  mailProf: string | null;
  siteProf: string | null;
  mailPerso: string | null;
  telProf: string | null;
  telPerso: string | null;
  cp: string | null;
  urlLinkedinProf: string | null;
  urlFacebookProf: string | null;
  whatsAppMobProf: string | null;
  urlMessenger: string | null;
  urlYoutube: string | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_laboratoire_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  ville: string | null;
  adresse: string | null;
  codePostal: string | null;
  telephone: string | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_laboratoire_actualites_fichierPresentations {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_laboratoire_actualites {
  __typename: "Actualite";
  id: string;
  libelle: string | null;
  dateCreation: any | null;
  description: string | null;
  fichierPresentations: (GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_laboratoire_actualites_fichierPresentations | null)[] | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_laboratoire_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_laboratoire_photo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_laboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  laboSuite: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_laboratoire_laboSuite | null;
  actualites: (GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_laboratoire_actualites | null)[] | null;
  laboratoirePartenaire: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_laboratoire_laboratoirePartenaire | null;
  photo: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_laboratoire_photo | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userCreation_userPhoto_fichier | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userCreation_role | null;
  userPhoto: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userCreation_userPhoto | null;
  pharmacie: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userCreation_pharmacie | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userModification_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userModification_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userModification_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userModification_userPhoto_fichier | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userModification_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userModification {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userModification_role | null;
  userPhoto: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userModification_userPhoto | null;
  pharmacie: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userModification_pharmacie | null;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_photo {
  __typename: "Fichier";
  id: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  nomOriginal: string;
  chemin: string;
}

export interface GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant {
  __typename: "LaboratoireRepresentant";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  sexe: string | null;
  fonction: string | null;
  commentaire: string | null;
  afficherComme: string | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  contact: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_contact | null;
  laboratoire: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_laboratoire | null;
  userCreation: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userCreation | null;
  userModification: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_userModification | null;
  photo: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant_photo | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface GET_LABORATOIRE_REPRESENTANT {
  laboratoireRepresentant: GET_LABORATOIRE_REPRESENTANT_laboratoireRepresentant | null;
}

export interface GET_LABORATOIRE_REPRESENTANTVariables {
  id: string;
}
