/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePartenaire, StatutPartenaire, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_LABO_REPRESENTANT
// ====================================================

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_contact {
  __typename: "Contact";
  id: string;
  adresse1: string | null;
  ville: string | null;
  mailProf: string | null;
  siteProf: string | null;
  mailPerso: string | null;
  telProf: string | null;
  telPerso: string | null;
  cp: string | null;
  urlLinkedinProf: string | null;
  urlFacebookProf: string | null;
  whatsAppMobProf: string | null;
  urlMessenger: string | null;
  urlYoutube: string | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_laboratoire_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  ville: string | null;
  adresse: string | null;
  codePostal: string | null;
  telephone: string | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_laboratoire_actualites_fichierPresentations {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_laboratoire_actualites {
  __typename: "Actualite";
  id: string;
  libelle: string | null;
  dateCreation: any | null;
  description: string | null;
  fichierPresentations: (DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_laboratoire_actualites_fichierPresentations | null)[] | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_laboratoire_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_laboratoire_photo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_laboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  laboSuite: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_laboratoire_laboSuite | null;
  actualites: (DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_laboratoire_actualites | null)[] | null;
  laboratoirePartenaire: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_laboratoire_laboratoirePartenaire | null;
  photo: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_laboratoire_photo | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userCreation_userPhoto_fichier | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userCreation_role | null;
  userPhoto: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userCreation_userPhoto | null;
  pharmacie: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userCreation_pharmacie | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userModification_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userModification_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userModification_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userModification_userPhoto_fichier | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userModification_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userModification {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userModification_role | null;
  userPhoto: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userModification_userPhoto | null;
  pharmacie: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userModification_pharmacie | null;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_photo {
  __typename: "Fichier";
  id: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  nomOriginal: string;
  chemin: string;
}

export interface DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants {
  __typename: "LaboratoireRepresentant";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  sexe: string | null;
  fonction: string | null;
  commentaire: string | null;
  afficherComme: string | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  contact: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_contact | null;
  laboratoire: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_laboratoire | null;
  userCreation: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userCreation | null;
  userModification: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_userModification | null;
  photo: DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants_photo | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_LABO_REPRESENTANT {
  softDeleteLaboratoireRepresentants: (DELETE_LABO_REPRESENTANT_softDeleteLaboratoireRepresentants | null)[] | null;
}

export interface DELETE_LABO_REPRESENTANTVariables {
  ids: string[];
}
