import gql from 'graphql-tag';
import { LABORATOIRE_REPRESENTANT_INFO } from './fragments';

export const DO_GET_LABORATOIRE_REPRESENTANT = gql`
  query GET_LABORATOIRE_REPRESENTANT($id: ID!) {
    laboratoireRepresentant(id: $id) {
      ...LaboratoireRepresentantInfo
    }
  }
  ${LABORATOIRE_REPRESENTANT_INFO}
`;

export const DO_SEARCH_LABO_REPRESENTANT = gql`
  query SEARCH_LABO_REPRESENTANT(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on LaboratoireRepresentant {
          ...LaboratoireRepresentantInfo
        }
      }
    }
  }
  ${LABORATOIRE_REPRESENTANT_INFO}
`;
