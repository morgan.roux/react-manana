import gql from 'graphql-tag';
import { LABORATOIRE_INFO_FRAGMENT } from '../Laboratoire/fragment';
import { USER_INFO_FRAGEMENT } from '../User/fragment';

export const LABORATOIRE_REPRESENTANT_INFO = gql`
  fragment LaboratoireRepresentantInfo on LaboratoireRepresentant {
    type
    id
    type
    civilite
    nom
    prenom
    sexe
    fonction
    commentaire
    afficherComme
    codeMaj
    isRemoved
    contact {
      id
      adresse1
      ville
      mailProf
      siteProf
      mailPerso
      telProf
      telPerso
      cp
      urlLinkedinProf
      urlFacebookProf
      whatsAppMobProf
      urlMessenger
      urlYoutube
    }
    laboratoire {
      ...LaboratoireInfo
    }
    userCreation {
      ...UserInfo
    }
    userModification {
      ...UserInfo
    }
    photo {
      id
      type
      urlPresigned
      publicUrl
      nomOriginal
      chemin
    }
    dateCreation
    dateModification
  }
  ${USER_INFO_FRAGEMENT}
  ${LABORATOIRE_INFO_FRAGMENT}
`;
