/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL subscription operation: NOTIFICATIONS_SUBSCRIPTION
// ====================================================

export interface NOTIFICATIONS_SUBSCRIPTION_notificationAdded_from {
  __typename: "User";
  id: string;
  userName: string | null;
  type: string;
}

export interface NOTIFICATIONS_SUBSCRIPTION_notificationAdded_to {
  __typename: "User";
  id: string;
  userName: string | null;
  type: string;
}

export interface NOTIFICATIONS_SUBSCRIPTION_notificationAdded {
  __typename: "Notification";
  id: string;
  type: string | null;
  targetId: string | null;
  targetName: string | null;
  message: string | null;
  seen: boolean | null;
  typeActualite: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  from: NOTIFICATIONS_SUBSCRIPTION_notificationAdded_from | null;
  to: NOTIFICATIONS_SUBSCRIPTION_notificationAdded_to | null;
}

export interface NOTIFICATIONS_SUBSCRIPTION {
  notificationAdded: NOTIFICATIONS_SUBSCRIPTION_notificationAdded | null;
}
