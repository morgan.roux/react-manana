/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: MARK_NOTIFICATION_AS_SEEN
// ====================================================

export interface MARK_NOTIFICATION_AS_SEEN_markNotificationAsSeen_from {
  __typename: "User";
  id: string;
  userName: string | null;
  type: string;
}

export interface MARK_NOTIFICATION_AS_SEEN_markNotificationAsSeen_to {
  __typename: "User";
  id: string;
  userName: string | null;
  type: string;
}

export interface MARK_NOTIFICATION_AS_SEEN_markNotificationAsSeen {
  __typename: "Notification";
  id: string;
  type: string | null;
  targetId: string | null;
  targetName: string | null;
  message: string | null;
  seen: boolean | null;
  typeActualite: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  from: MARK_NOTIFICATION_AS_SEEN_markNotificationAsSeen_from | null;
  to: MARK_NOTIFICATION_AS_SEEN_markNotificationAsSeen_to | null;
}

export interface MARK_NOTIFICATION_AS_SEEN {
  markNotificationAsSeen: MARK_NOTIFICATION_AS_SEEN_markNotificationAsSeen | null;
}

export interface MARK_NOTIFICATION_AS_SEENVariables {
  id: string;
  seen?: boolean | null;
}
