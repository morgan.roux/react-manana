/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: NotificationInfo
// ====================================================

export interface NotificationInfo_from {
  __typename: "User";
  id: string;
  userName: string | null;
  type: string;
}

export interface NotificationInfo_to {
  __typename: "User";
  id: string;
  userName: string | null;
  type: string;
}

export interface NotificationInfo {
  __typename: "Notification";
  id: string;
  type: string | null;
  targetId: string | null;
  targetName: string | null;
  message: string | null;
  seen: boolean | null;
  typeActualite: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  from: NotificationInfo_from | null;
  to: NotificationInfo_to | null;
}
