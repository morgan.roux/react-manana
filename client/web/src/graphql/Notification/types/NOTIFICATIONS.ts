/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { NotificationOrderByInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: NOTIFICATIONS
// ====================================================

export interface NOTIFICATIONS_notifications_data_from {
  __typename: "User";
  id: string;
  userName: string | null;
  type: string;
}

export interface NOTIFICATIONS_notifications_data_to {
  __typename: "User";
  id: string;
  userName: string | null;
  type: string;
}

export interface NOTIFICATIONS_notifications_data {
  __typename: "Notification";
  id: string;
  type: string | null;
  targetId: string | null;
  targetName: string | null;
  message: string | null;
  seen: boolean | null;
  typeActualite: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  from: NOTIFICATIONS_notifications_data_from | null;
  to: NOTIFICATIONS_notifications_data_to | null;
}

export interface NOTIFICATIONS_notifications {
  __typename: "NotificationResult";
  total: number;
  notSeen: number;
  data: (NOTIFICATIONS_notifications_data | null)[] | null;
}

export interface NOTIFICATIONS {
  notifications: NOTIFICATIONS_notifications | null;
}

export interface NOTIFICATIONSVariables {
  first?: number | null;
  skip?: number | null;
  orderBy?: NotificationOrderByInput | null;
  dateFilterStart?: string | null;
  dateFilterEnd?: string | null;
}
