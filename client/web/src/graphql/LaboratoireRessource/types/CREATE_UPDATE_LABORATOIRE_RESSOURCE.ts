/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { LaboratoireRessourceInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_LABORATOIRE_RESSOURCE
// ====================================================

export interface CREATE_UPDATE_LABORATOIRE_RESSOURCE_createUpdateLaboratoireRessource_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
}

export interface CREATE_UPDATE_LABORATOIRE_RESSOURCE_createUpdateLaboratoireRessource {
  __typename: "LaboratoireRessource";
  id: string;
  typeRessource: string | null;
  item: CREATE_UPDATE_LABORATOIRE_RESSOURCE_createUpdateLaboratoireRessource_item | null;
  code: string | null;
  libelle: string | null;
  dateChargement: any | null;
  url: string | null;
}

export interface CREATE_UPDATE_LABORATOIRE_RESSOURCE {
  createUpdateLaboratoireRessource: CREATE_UPDATE_LABORATOIRE_RESSOURCE_createUpdateLaboratoireRessource | null;
}

export interface CREATE_UPDATE_LABORATOIRE_RESSOURCEVariables {
  input: LaboratoireRessourceInput;
}
