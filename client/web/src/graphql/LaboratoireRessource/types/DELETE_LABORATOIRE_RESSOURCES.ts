/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_LABORATOIRE_RESSOURCES
// ====================================================

export interface DELETE_LABORATOIRE_RESSOURCES_softDeleteLaboratoireRessources {
  __typename: "LaboratoireRessource";
  id: string;
}

export interface DELETE_LABORATOIRE_RESSOURCES {
  softDeleteLaboratoireRessources: (DELETE_LABORATOIRE_RESSOURCES_softDeleteLaboratoireRessources | null)[] | null;
}

export interface DELETE_LABORATOIRE_RESSOURCESVariables {
  ids: string[];
}
