import gql from 'graphql-tag';

export const DO_CREATE_UPDATE_LABORATOIRE_RESSOURCE = gql`
  mutation CREATE_UPDATE_LABORATOIRE_RESSOURCE($input: LaboratoireRessourceInput!) {
    createUpdateLaboratoireRessource(input: $input) {
      id
      typeRessource
      item {
        id
        code
        name
        codeItem
      }
      code
      libelle
      dateChargement
      url
    }
  }
`;

export const DO_DELETE_LABORATOIRE_RESSOURCES = gql`
  mutation DELETE_LABORATOIRE_RESSOURCES($ids: [ID!]!) {
    softDeleteLaboratoireRessources(ids: $ids) {
      id
    }
  }
`;
