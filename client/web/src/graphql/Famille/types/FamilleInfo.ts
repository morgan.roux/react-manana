/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FamilleInfo
// ====================================================

export interface FamilleInfo_parent {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  countCanalArticles: number | null;
  libelleFamille: string | null;
}

export interface FamilleInfo {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  libelleFamille: string | null;
  countCanalArticles: number | null;
  parent: FamilleInfo_parent | null;
}
