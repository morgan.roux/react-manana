/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TrFamilleInfo
// ====================================================

export interface TrFamilleInfo {
  __typename: "TrFamille";
  id: string;
  idFamille: number;
  codeFamille: string;
  nomFamille: string | null;
  majFamille: string | null;
}
