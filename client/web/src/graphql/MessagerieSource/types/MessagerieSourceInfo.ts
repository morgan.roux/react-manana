/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: MessagerieSourceInfo
// ====================================================

export interface MessagerieSourceInfo {
  __typename: "MessagerieSource";
  id: string;
  nom: string | null;
}
