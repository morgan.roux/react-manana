import gql from 'graphql-tag';

export const MESSAGERIE_SOURCE_INFO_FRAGMENT = gql`
  fragment MessagerieSourceInfo on MessagerieSource {
    id
    nom
  }
`;
