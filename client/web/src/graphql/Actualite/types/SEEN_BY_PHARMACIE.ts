/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePresidentCible, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: SEEN_BY_PHARMACIE
// ====================================================

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_item_fichier | null;
  parent: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_origine {
  __typename: "ActualiteOrigine";
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_actualiteCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  type: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_actualiteCible {
  __typename: "ActualiteCible";
  id: string | null;
  globalite: boolean | null;
  fichierCible: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_actualiteCible_fichierCible | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_servicesCible {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_comments {
  __typename: "CommentResult";
  total: number;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_user_role | null;
  userPhoto: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_user_userPhoto | null;
  pharmacie: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_user_pharmacie | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley_groupement | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_item | null;
  idSource: string | null;
  user: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_user | null;
  smyley: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys_data | null)[] | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_pharmaciesCible_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_pharmaciesCible_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_pharmaciesCible {
  __typename: "Pharmacie";
  type: string;
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_pharmaciesCible_departement | null;
  titulaires: (SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_pharmaciesCible_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_pharmaciesRolesCible {
  __typename: "Role";
  id: string;
  code: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_presidentsCibles_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_presidentsCibles {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_presidentsCibles_pharmacies | null)[] | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_partenairesCible {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  idGroupement: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_laboratoiresCible {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_operation {
  __typename: "Operation";
  id: string;
  commandePassee: boolean | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_action_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_action {
  __typename: "Action";
  id: string;
  description: string;
  priority: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  project: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_action_project | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface SEEN_BY_PHARMACIE_actualiteSeenByPharmacie {
  __typename: "Actualite";
  type: string;
  id: string;
  idGroupement: string | null;
  idTache: string | null;
  idFonction: string | null;
  dateEcheance: any | null;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  description: string | null;
  actionAuto: boolean | null;
  niveauPriorite: number | null;
  typePresidentCible: TypePresidentCible | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbSeenByPharmacie: number | null;
  nbPartage: number;
  seen: boolean | null;
  item: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_item | null;
  origine: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_origine | null;
  laboratoire: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_laboratoire | null;
  fichierPresentations: (SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_fichierPresentations | null)[] | null;
  actualiteCible: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_actualiteCible | null;
  servicesCible: (SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_servicesCible | null)[] | null;
  comments: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_comments | null;
  userSmyleys: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_userSmyleys | null;
  pharmaciesCible: (SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_pharmaciesCible | null)[] | null;
  pharmaciesRolesCible: (SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_pharmaciesRolesCible | null)[] | null;
  presidentsCibles: (SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_presidentsCibles | null)[] | null;
  partenairesCible: (SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_partenairesCible | null)[] | null;
  laboratoiresCible: (SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_laboratoiresCible | null)[] | null;
  operation: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_operation | null;
  action: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_action | null;
  importance: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_importance | null;
  urgence: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie_urgence | null;
}

export interface SEEN_BY_PHARMACIE {
  actualiteSeenByPharmacie: SEEN_BY_PHARMACIE_actualiteSeenByPharmacie | null;
}

export interface SEEN_BY_PHARMACIEVariables {
  id: string;
  idPharmacieUser: string;
  userId?: string | null;
}
