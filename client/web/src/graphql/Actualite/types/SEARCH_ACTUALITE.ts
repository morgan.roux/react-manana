/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePresidentCible, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_ACTUALITE
// ====================================================

export interface SEARCH_ACTUALITE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_ACTUALITE_search_data_Actualite_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: SEARCH_ACTUALITE_search_data_Actualite_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: SEARCH_ACTUALITE_search_data_Actualite_item_fichier | null;
  parent: SEARCH_ACTUALITE_search_data_Actualite_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_origine {
  __typename: "ActualiteOrigine";
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_actualiteCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  type: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_actualiteCible {
  __typename: "ActualiteCible";
  id: string | null;
  globalite: boolean | null;
  fichierCible: SEARCH_ACTUALITE_search_data_Actualite_actualiteCible_fichierCible | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_servicesCible {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_comments {
  __typename: "CommentResult";
  total: number;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_user_role | null;
  userPhoto: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_user_userPhoto | null;
  pharmacie: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_user_pharmacie | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley_groupement | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_item | null;
  idSource: string | null;
  user: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_user | null;
  smyley: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (SEARCH_ACTUALITE_search_data_Actualite_userSmyleys_data | null)[] | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_pharmaciesCible_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_pharmaciesCible_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_pharmaciesCible {
  __typename: "Pharmacie";
  type: string;
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: SEARCH_ACTUALITE_search_data_Actualite_pharmaciesCible_departement | null;
  titulaires: (SEARCH_ACTUALITE_search_data_Actualite_pharmaciesCible_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_pharmaciesRolesCible {
  __typename: "Role";
  id: string;
  code: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_presidentsCibles_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_presidentsCibles {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (SEARCH_ACTUALITE_search_data_Actualite_presidentsCibles_pharmacies | null)[] | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_partenairesCible {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  idGroupement: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_laboratoiresCible {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_operation {
  __typename: "Operation";
  id: string;
  commandePassee: boolean | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_action_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_action {
  __typename: "Action";
  id: string;
  description: string;
  priority: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  project: SEARCH_ACTUALITE_search_data_Actualite_action_project | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface SEARCH_ACTUALITE_search_data_Actualite {
  __typename: "Actualite";
  type: string;
  id: string;
  idGroupement: string | null;
  idTache: string | null;
  idFonction: string | null;
  dateEcheance: any | null;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  description: string | null;
  actionAuto: boolean | null;
  niveauPriorite: number | null;
  typePresidentCible: TypePresidentCible | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbSeenByPharmacie: number | null;
  nbPartage: number;
  seen: boolean | null;
  item: SEARCH_ACTUALITE_search_data_Actualite_item | null;
  origine: SEARCH_ACTUALITE_search_data_Actualite_origine | null;
  laboratoire: SEARCH_ACTUALITE_search_data_Actualite_laboratoire | null;
  fichierPresentations: (SEARCH_ACTUALITE_search_data_Actualite_fichierPresentations | null)[] | null;
  actualiteCible: SEARCH_ACTUALITE_search_data_Actualite_actualiteCible | null;
  servicesCible: (SEARCH_ACTUALITE_search_data_Actualite_servicesCible | null)[] | null;
  comments: SEARCH_ACTUALITE_search_data_Actualite_comments | null;
  userSmyleys: SEARCH_ACTUALITE_search_data_Actualite_userSmyleys | null;
  pharmaciesCible: (SEARCH_ACTUALITE_search_data_Actualite_pharmaciesCible | null)[] | null;
  pharmaciesRolesCible: (SEARCH_ACTUALITE_search_data_Actualite_pharmaciesRolesCible | null)[] | null;
  presidentsCibles: (SEARCH_ACTUALITE_search_data_Actualite_presidentsCibles | null)[] | null;
  partenairesCible: (SEARCH_ACTUALITE_search_data_Actualite_partenairesCible | null)[] | null;
  laboratoiresCible: (SEARCH_ACTUALITE_search_data_Actualite_laboratoiresCible | null)[] | null;
  operation: SEARCH_ACTUALITE_search_data_Actualite_operation | null;
  action: SEARCH_ACTUALITE_search_data_Actualite_action | null;
  importance: SEARCH_ACTUALITE_search_data_Actualite_importance | null;
  urgence: SEARCH_ACTUALITE_search_data_Actualite_urgence | null;
}

export type SEARCH_ACTUALITE_search_data = SEARCH_ACTUALITE_search_data_Action | SEARCH_ACTUALITE_search_data_Actualite;

export interface SEARCH_ACTUALITE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_ACTUALITE_search_data | null)[] | null;
}

export interface SEARCH_ACTUALITE {
  search: SEARCH_ACTUALITE_search | null;
}

export interface SEARCH_ACTUALITEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  idPharmacieUser?: string | null;
  userId?: string | null;
}
