/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePresidentCible, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: ACTUALITE
// ====================================================

export interface ACTUALITE_actualite_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ACTUALITE_actualite_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ACTUALITE_actualite_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: ACTUALITE_actualite_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ACTUALITE_actualite_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: ACTUALITE_actualite_item_fichier | null;
  parent: ACTUALITE_actualite_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ACTUALITE_actualite_origine {
  __typename: "ActualiteOrigine";
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ACTUALITE_actualite_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface ACTUALITE_actualite_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ACTUALITE_actualite_actualiteCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  type: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface ACTUALITE_actualite_actualiteCible {
  __typename: "ActualiteCible";
  id: string | null;
  globalite: boolean | null;
  fichierCible: ACTUALITE_actualite_actualiteCible_fichierCible | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ACTUALITE_actualite_servicesCible {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
}

export interface ACTUALITE_actualite_comments {
  __typename: "CommentResult";
  total: number;
}

export interface ACTUALITE_actualite_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface ACTUALITE_actualite_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ACTUALITE_actualite_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface ACTUALITE_actualite_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: ACTUALITE_actualite_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface ACTUALITE_actualite_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface ACTUALITE_actualite_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: ACTUALITE_actualite_userSmyleys_data_user_role | null;
  userPhoto: ACTUALITE_actualite_userSmyleys_data_user_userPhoto | null;
  pharmacie: ACTUALITE_actualite_userSmyleys_data_user_pharmacie | null;
}

export interface ACTUALITE_actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface ACTUALITE_actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: ACTUALITE_actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface ACTUALITE_actualite_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: ACTUALITE_actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface ACTUALITE_actualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface ACTUALITE_actualite_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: ACTUALITE_actualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface ACTUALITE_actualite_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: ACTUALITE_actualite_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: ACTUALITE_actualite_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface ACTUALITE_actualite_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: ACTUALITE_actualite_userSmyleys_data_smyley_groupement | null;
}

export interface ACTUALITE_actualite_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: ACTUALITE_actualite_userSmyleys_data_item | null;
  idSource: string | null;
  user: ACTUALITE_actualite_userSmyleys_data_user | null;
  smyley: ACTUALITE_actualite_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ACTUALITE_actualite_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (ACTUALITE_actualite_userSmyleys_data | null)[] | null;
}

export interface ACTUALITE_actualite_pharmaciesCible_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface ACTUALITE_actualite_pharmaciesCible_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface ACTUALITE_actualite_pharmaciesCible {
  __typename: "Pharmacie";
  type: string;
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: ACTUALITE_actualite_pharmaciesCible_departement | null;
  titulaires: (ACTUALITE_actualite_pharmaciesCible_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
}

export interface ACTUALITE_actualite_pharmaciesRolesCible {
  __typename: "Role";
  id: string;
  code: string | null;
}

export interface ACTUALITE_actualite_presidentsCibles_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface ACTUALITE_actualite_presidentsCibles {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (ACTUALITE_actualite_presidentsCibles_pharmacies | null)[] | null;
}

export interface ACTUALITE_actualite_partenairesCible {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  idGroupement: string | null;
}

export interface ACTUALITE_actualite_laboratoiresCible {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface ACTUALITE_actualite_operation {
  __typename: "Operation";
  id: string;
  commandePassee: boolean | null;
}

export interface ACTUALITE_actualite_action_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface ACTUALITE_actualite_action {
  __typename: "Action";
  id: string;
  description: string;
  priority: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  project: ACTUALITE_actualite_action_project | null;
}

export interface ACTUALITE_actualite_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface ACTUALITE_actualite_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface ACTUALITE_actualite {
  __typename: "Actualite";
  type: string;
  id: string;
  idGroupement: string | null;
  idTache: string | null;
  idFonction: string | null;
  dateEcheance: any | null;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  description: string | null;
  actionAuto: boolean | null;
  niveauPriorite: number | null;
  typePresidentCible: TypePresidentCible | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbSeenByPharmacie: number | null;
  nbPartage: number;
  seen: boolean | null;
  item: ACTUALITE_actualite_item | null;
  origine: ACTUALITE_actualite_origine | null;
  laboratoire: ACTUALITE_actualite_laboratoire | null;
  fichierPresentations: (ACTUALITE_actualite_fichierPresentations | null)[] | null;
  actualiteCible: ACTUALITE_actualite_actualiteCible | null;
  servicesCible: (ACTUALITE_actualite_servicesCible | null)[] | null;
  comments: ACTUALITE_actualite_comments | null;
  userSmyleys: ACTUALITE_actualite_userSmyleys | null;
  pharmaciesCible: (ACTUALITE_actualite_pharmaciesCible | null)[] | null;
  pharmaciesRolesCible: (ACTUALITE_actualite_pharmaciesRolesCible | null)[] | null;
  presidentsCibles: (ACTUALITE_actualite_presidentsCibles | null)[] | null;
  partenairesCible: (ACTUALITE_actualite_partenairesCible | null)[] | null;
  laboratoiresCible: (ACTUALITE_actualite_laboratoiresCible | null)[] | null;
  operation: ACTUALITE_actualite_operation | null;
  action: ACTUALITE_actualite_action | null;
  importance: ACTUALITE_actualite_importance | null;
  urgence: ACTUALITE_actualite_urgence | null;
}

export interface ACTUALITE {
  actualite: ACTUALITE_actualite | null;
}

export interface ACTUALITEVariables {
  id: string;
  idPharmacieUser?: string | null;
  userId?: string | null;
}
