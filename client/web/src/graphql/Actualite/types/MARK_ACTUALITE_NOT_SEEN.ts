/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePresidentCible, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: MARK_ACTUALITE_NOT_SEEN
// ====================================================

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_item_fichier | null;
  parent: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_origine {
  __typename: "ActualiteOrigine";
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_actualiteCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  type: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_actualiteCible {
  __typename: "ActualiteCible";
  id: string | null;
  globalite: boolean | null;
  fichierCible: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_actualiteCible_fichierCible | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_servicesCible {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_comments {
  __typename: "CommentResult";
  total: number;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_user_role | null;
  userPhoto: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_user_userPhoto | null;
  pharmacie: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_user_pharmacie | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley_groupement | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_item | null;
  idSource: string | null;
  user: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_user | null;
  smyley: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys_data | null)[] | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_pharmaciesCible_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_pharmaciesCible_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_pharmaciesCible {
  __typename: "Pharmacie";
  type: string;
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_pharmaciesCible_departement | null;
  titulaires: (MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_pharmaciesCible_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_pharmaciesRolesCible {
  __typename: "Role";
  id: string;
  code: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_presidentsCibles_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_presidentsCibles {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_presidentsCibles_pharmacies | null)[] | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_partenairesCible {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  idGroupement: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_laboratoiresCible {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_operation {
  __typename: "Operation";
  id: string;
  commandePassee: boolean | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_action_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_action {
  __typename: "Action";
  id: string;
  description: string;
  priority: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  project: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_action_project | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite {
  __typename: "Actualite";
  type: string;
  id: string;
  idGroupement: string | null;
  idTache: string | null;
  idFonction: string | null;
  dateEcheance: any | null;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  description: string | null;
  actionAuto: boolean | null;
  niveauPriorite: number | null;
  typePresidentCible: TypePresidentCible | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbSeenByPharmacie: number | null;
  nbPartage: number;
  seen: boolean | null;
  item: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_item | null;
  origine: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_origine | null;
  laboratoire: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_laboratoire | null;
  fichierPresentations: (MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_fichierPresentations | null)[] | null;
  actualiteCible: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_actualiteCible | null;
  servicesCible: (MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_servicesCible | null)[] | null;
  comments: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_comments | null;
  userSmyleys: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_userSmyleys | null;
  pharmaciesCible: (MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_pharmaciesCible | null)[] | null;
  pharmaciesRolesCible: (MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_pharmaciesRolesCible | null)[] | null;
  presidentsCibles: (MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_presidentsCibles | null)[] | null;
  partenairesCible: (MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_partenairesCible | null)[] | null;
  laboratoiresCible: (MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_laboratoiresCible | null)[] | null;
  operation: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_operation | null;
  action: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_action | null;
  importance: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_importance | null;
  urgence: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite_urgence | null;
}

export interface MARK_ACTUALITE_NOT_SEEN {
  markAsNotSeenActualite: MARK_ACTUALITE_NOT_SEEN_markAsNotSeenActualite | null;
}

export interface MARK_ACTUALITE_NOT_SEENVariables {
  id: string;
  idPharmacieUser?: string | null;
  userId?: string | null;
}
