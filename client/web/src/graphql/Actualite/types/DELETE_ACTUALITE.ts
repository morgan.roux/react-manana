/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePresidentCible, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_ACTUALITE
// ====================================================

export interface DELETE_ACTUALITE_deleteActualite_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_ACTUALITE_deleteActualite_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_ACTUALITE_deleteActualite_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: DELETE_ACTUALITE_deleteActualite_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_ACTUALITE_deleteActualite_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: DELETE_ACTUALITE_deleteActualite_item_fichier | null;
  parent: DELETE_ACTUALITE_deleteActualite_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_ACTUALITE_deleteActualite_origine {
  __typename: "ActualiteOrigine";
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_ACTUALITE_deleteActualite_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_ACTUALITE_deleteActualite_actualiteCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  type: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite_actualiteCible {
  __typename: "ActualiteCible";
  id: string | null;
  globalite: boolean | null;
  fichierCible: DELETE_ACTUALITE_deleteActualite_actualiteCible_fichierCible | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_ACTUALITE_deleteActualite_servicesCible {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
}

export interface DELETE_ACTUALITE_deleteActualite_comments {
  __typename: "CommentResult";
  total: number;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_user_role | null;
  userPhoto: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_user_userPhoto | null;
  pharmacie: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_user_pharmacie | null;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley_groupement | null;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_item | null;
  idSource: string | null;
  user: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_user | null;
  smyley: DELETE_ACTUALITE_deleteActualite_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_ACTUALITE_deleteActualite_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (DELETE_ACTUALITE_deleteActualite_userSmyleys_data | null)[] | null;
}

export interface DELETE_ACTUALITE_deleteActualite_pharmaciesCible_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite_pharmaciesCible_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite_pharmaciesCible {
  __typename: "Pharmacie";
  type: string;
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: DELETE_ACTUALITE_deleteActualite_pharmaciesCible_departement | null;
  titulaires: (DELETE_ACTUALITE_deleteActualite_pharmaciesCible_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
}

export interface DELETE_ACTUALITE_deleteActualite_pharmaciesRolesCible {
  __typename: "Role";
  id: string;
  code: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite_presidentsCibles_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface DELETE_ACTUALITE_deleteActualite_presidentsCibles {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (DELETE_ACTUALITE_deleteActualite_presidentsCibles_pharmacies | null)[] | null;
}

export interface DELETE_ACTUALITE_deleteActualite_partenairesCible {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  idGroupement: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite_laboratoiresCible {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface DELETE_ACTUALITE_deleteActualite_operation {
  __typename: "Operation";
  id: string;
  commandePassee: boolean | null;
}

export interface DELETE_ACTUALITE_deleteActualite_action_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite_action {
  __typename: "Action";
  id: string;
  description: string;
  priority: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  project: DELETE_ACTUALITE_deleteActualite_action_project | null;
}

export interface DELETE_ACTUALITE_deleteActualite_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface DELETE_ACTUALITE_deleteActualite {
  __typename: "Actualite";
  type: string;
  id: string;
  idGroupement: string | null;
  idTache: string | null;
  idFonction: string | null;
  dateEcheance: any | null;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  description: string | null;
  actionAuto: boolean | null;
  niveauPriorite: number | null;
  typePresidentCible: TypePresidentCible | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbSeenByPharmacie: number | null;
  nbPartage: number;
  seen: boolean | null;
  item: DELETE_ACTUALITE_deleteActualite_item | null;
  origine: DELETE_ACTUALITE_deleteActualite_origine | null;
  laboratoire: DELETE_ACTUALITE_deleteActualite_laboratoire | null;
  fichierPresentations: (DELETE_ACTUALITE_deleteActualite_fichierPresentations | null)[] | null;
  actualiteCible: DELETE_ACTUALITE_deleteActualite_actualiteCible | null;
  servicesCible: (DELETE_ACTUALITE_deleteActualite_servicesCible | null)[] | null;
  comments: DELETE_ACTUALITE_deleteActualite_comments | null;
  userSmyleys: DELETE_ACTUALITE_deleteActualite_userSmyleys | null;
  pharmaciesCible: (DELETE_ACTUALITE_deleteActualite_pharmaciesCible | null)[] | null;
  pharmaciesRolesCible: (DELETE_ACTUALITE_deleteActualite_pharmaciesRolesCible | null)[] | null;
  presidentsCibles: (DELETE_ACTUALITE_deleteActualite_presidentsCibles | null)[] | null;
  partenairesCible: (DELETE_ACTUALITE_deleteActualite_partenairesCible | null)[] | null;
  laboratoiresCible: (DELETE_ACTUALITE_deleteActualite_laboratoiresCible | null)[] | null;
  operation: DELETE_ACTUALITE_deleteActualite_operation | null;
  action: DELETE_ACTUALITE_deleteActualite_action | null;
  importance: DELETE_ACTUALITE_deleteActualite_importance | null;
  urgence: DELETE_ACTUALITE_deleteActualite_urgence | null;
}

export interface DELETE_ACTUALITE {
  deleteActualite: DELETE_ACTUALITE_deleteActualite | null;
}

export interface DELETE_ACTUALITEVariables {
  id: string;
  idPharmacieUser?: string | null;
  userId?: string | null;
}
