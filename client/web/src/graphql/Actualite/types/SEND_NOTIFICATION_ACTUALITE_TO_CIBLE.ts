/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SEND_NOTIFICATION_ACTUALITE_TO_CIBLE
// ====================================================

export interface SEND_NOTIFICATION_ACTUALITE_TO_CIBLE {
  sendNotificationActualiteToCible: boolean | null;
}

export interface SEND_NOTIFICATION_ACTUALITE_TO_CIBLEVariables {
  id: string;
  isUpdate?: boolean | null;
}
