/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ACTUALITE_ORIGINES
// ====================================================

export interface ACTUALITE_ORIGINES_actualiteOrigines {
  __typename: "ActualiteOrigine";
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ACTUALITE_ORIGINES {
  actualiteOrigines: (ACTUALITE_ORIGINES_actualiteOrigines | null)[] | null;
}
