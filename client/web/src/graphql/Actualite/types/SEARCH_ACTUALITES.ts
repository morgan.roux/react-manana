/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_ACTUALITES
// ====================================================

export interface SEARCH_ACTUALITES_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_ACTUALITES_search_data_Actualite_origine {
  __typename: "ActualiteOrigine";
  id: string;
  libelle: string | null;
}

export interface SEARCH_ACTUALITES_search_data_Actualite_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface SEARCH_ACTUALITES_search_data_Actualite_fichierPresentations {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SEARCH_ACTUALITES_search_data_Actualite {
  __typename: "Actualite";
  id: string;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  description: string | null;
  niveauPriorite: number | null;
  nbSeenByPharmacie: number | null;
  seen: boolean | null;
  origine: SEARCH_ACTUALITES_search_data_Actualite_origine | null;
  laboratoire: SEARCH_ACTUALITES_search_data_Actualite_laboratoire | null;
  fichierPresentations: (SEARCH_ACTUALITES_search_data_Actualite_fichierPresentations | null)[] | null;
}

export type SEARCH_ACTUALITES_search_data = SEARCH_ACTUALITES_search_data_Action | SEARCH_ACTUALITES_search_data_Actualite;

export interface SEARCH_ACTUALITES_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_ACTUALITES_search_data | null)[] | null;
}

export interface SEARCH_ACTUALITES {
  search: SEARCH_ACTUALITES_search | null;
}

export interface SEARCH_ACTUALITESVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
  idPharmacieUser?: string | null;
  userId?: string | null;
}
