/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PharmacieRolesInput, PresidentRegionsInput, FichierInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: ACTUALITE_ALL_USERS_CIBLE
// ====================================================

export interface ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPhoto_fichier | null;
}

export interface ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPersonnel_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPersonnel {
  __typename: "Personnel";
  id: string;
  nom: string | null;
  prenom: string | null;
  service: ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPersonnel_service | null;
}

export interface ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPpersonnel_pharmacie {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
}

export interface ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPpersonnel {
  __typename: "Ppersonnel";
  id: string;
  nom: string | null;
  prenom: string | null;
  estAmbassadrice: boolean | null;
  pharmacie: ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPpersonnel_pharmacie | null;
}

export interface ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userLaboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPartenaire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
}

export interface ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userTitulaire_titulaire {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
}

export interface ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userTitulaire {
  __typename: "UserTitulaire";
  id: string;
  isPresident: boolean | null;
  titulaire: ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userTitulaire_titulaire | null;
}

export interface ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible {
  __typename: "User";
  id: string;
  userPhoto: ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPhoto | null;
  userName: string | null;
  userPersonnel: ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPersonnel | null;
  userPpersonnel: ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPpersonnel | null;
  userLaboratoire: ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userLaboratoire | null;
  userPartenaire: ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userPartenaire | null;
  userTitulaire: ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible_userTitulaire | null;
}

export interface ACTUALITE_ALL_USERS_CIBLE {
  actualiteAllUsersCible: (ACTUALITE_ALL_USERS_CIBLE_actualiteAllUsersCible | null)[] | null;
}

export interface ACTUALITE_ALL_USERS_CIBLEVariables {
  codeServices?: (string | null)[] | null;
  idPartenaires?: (string | null)[] | null;
  idLaboratoires?: (string | null)[] | null;
  pharmacieRoles?: PharmacieRolesInput | null;
  presidentRegions?: PresidentRegionsInput | null;
  fichierCible?: FichierInput | null;
}
