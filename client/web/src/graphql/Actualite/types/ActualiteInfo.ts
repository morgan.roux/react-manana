/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePresidentCible, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: ActualiteInfo
// ====================================================

export interface ActualiteInfo_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ActualiteInfo_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ActualiteInfo_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: ActualiteInfo_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ActualiteInfo_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: ActualiteInfo_item_fichier | null;
  parent: ActualiteInfo_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ActualiteInfo_origine {
  __typename: "ActualiteOrigine";
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ActualiteInfo_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface ActualiteInfo_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ActualiteInfo_actualiteCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  type: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface ActualiteInfo_actualiteCible {
  __typename: "ActualiteCible";
  id: string | null;
  globalite: boolean | null;
  fichierCible: ActualiteInfo_actualiteCible_fichierCible | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ActualiteInfo_servicesCible {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
}

export interface ActualiteInfo_comments {
  __typename: "CommentResult";
  total: number;
}

export interface ActualiteInfo_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface ActualiteInfo_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ActualiteInfo_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface ActualiteInfo_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: ActualiteInfo_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface ActualiteInfo_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface ActualiteInfo_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: ActualiteInfo_userSmyleys_data_user_role | null;
  userPhoto: ActualiteInfo_userSmyleys_data_user_userPhoto | null;
  pharmacie: ActualiteInfo_userSmyleys_data_user_pharmacie | null;
}

export interface ActualiteInfo_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface ActualiteInfo_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: ActualiteInfo_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface ActualiteInfo_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: ActualiteInfo_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface ActualiteInfo_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface ActualiteInfo_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: ActualiteInfo_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface ActualiteInfo_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: ActualiteInfo_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: ActualiteInfo_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface ActualiteInfo_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: ActualiteInfo_userSmyleys_data_smyley_groupement | null;
}

export interface ActualiteInfo_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: ActualiteInfo_userSmyleys_data_item | null;
  idSource: string | null;
  user: ActualiteInfo_userSmyleys_data_user | null;
  smyley: ActualiteInfo_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ActualiteInfo_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (ActualiteInfo_userSmyleys_data | null)[] | null;
}

export interface ActualiteInfo_pharmaciesCible_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface ActualiteInfo_pharmaciesCible_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface ActualiteInfo_pharmaciesCible {
  __typename: "Pharmacie";
  type: string;
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: ActualiteInfo_pharmaciesCible_departement | null;
  titulaires: (ActualiteInfo_pharmaciesCible_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
}

export interface ActualiteInfo_pharmaciesRolesCible {
  __typename: "Role";
  id: string;
  code: string | null;
}

export interface ActualiteInfo_presidentsCibles_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface ActualiteInfo_presidentsCibles {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (ActualiteInfo_presidentsCibles_pharmacies | null)[] | null;
}

export interface ActualiteInfo_partenairesCible {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  idGroupement: string | null;
}

export interface ActualiteInfo_laboratoiresCible {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface ActualiteInfo_operation {
  __typename: "Operation";
  id: string;
  commandePassee: boolean | null;
}

export interface ActualiteInfo_action_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface ActualiteInfo_action {
  __typename: "Action";
  id: string;
  description: string;
  priority: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  project: ActualiteInfo_action_project | null;
}

export interface ActualiteInfo_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface ActualiteInfo_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface ActualiteInfo {
  __typename: "Actualite";
  type: string;
  id: string;
  idGroupement: string | null;
  idTache: string | null;
  idFonction: string | null;
  dateEcheance: any | null;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  description: string | null;
  actionAuto: boolean | null;
  niveauPriorite: number | null;
  typePresidentCible: TypePresidentCible | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbSeenByPharmacie: number | null;
  nbPartage: number;
  seen: boolean | null;
  item: ActualiteInfo_item | null;
  origine: ActualiteInfo_origine | null;
  laboratoire: ActualiteInfo_laboratoire | null;
  fichierPresentations: (ActualiteInfo_fichierPresentations | null)[] | null;
  actualiteCible: ActualiteInfo_actualiteCible | null;
  servicesCible: (ActualiteInfo_servicesCible | null)[] | null;
  comments: ActualiteInfo_comments | null;
  userSmyleys: ActualiteInfo_userSmyleys | null;
  pharmaciesCible: (ActualiteInfo_pharmaciesCible | null)[] | null;
  pharmaciesRolesCible: (ActualiteInfo_pharmaciesRolesCible | null)[] | null;
  presidentsCibles: (ActualiteInfo_presidentsCibles | null)[] | null;
  partenairesCible: (ActualiteInfo_partenairesCible | null)[] | null;
  laboratoiresCible: (ActualiteInfo_laboratoiresCible | null)[] | null;
  operation: ActualiteInfo_operation | null;
  action: ActualiteInfo_action | null;
  importance: ActualiteInfo_importance | null;
  urgence: ActualiteInfo_urgence | null;
}
