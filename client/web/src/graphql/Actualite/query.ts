import gql from 'graphql-tag';
import { ACTUALITE_ORIGINE_INFO_FRAGEMENT, ACTUALITE_INFO_FRAGEMENT } from './fragment';

export const GET_ACTUALITE_ORIGINES = gql`
  query ACTUALITE_ORIGINES {
    actualiteOrigines {
      ...ActualiteOrigineInfo
    }
  }
  ${ACTUALITE_ORIGINE_INFO_FRAGEMENT}
`;

export const GET_ACTUALITE_ORIGINES_BY_USER_ROLE = gql`
  query ACTUALITE_ORIGINES_BY_USER_ROLE($codeRole: String!) {
    actualiteOriginesByRole(codeRole: $codeRole) {
      ...ActualiteOrigineInfo
    }
  }
  ${ACTUALITE_ORIGINE_INFO_FRAGEMENT}
`;

export const GET_ACTUALITE = gql`
  query ACTUALITE($id: ID!, $idPharmacieUser: ID, $userId: String) {
    actualite(id: $id) {
      ...ActualiteInfo
    }
  }
  ${ACTUALITE_INFO_FRAGEMENT}
`;

export const GET_COUNT_ACTUALITES_CIBLES = gql`
  query COUNT_ACTUALITES_CIBLES {
    countActualitesCibles {
      code
      name
      total
    }
  }
`;

export const GET_ACTUALITE_ALL_USERS_CIBLE = gql`
  query ACTUALITE_ALL_USERS_CIBLE(
    $codeServices: [String]
    $idPartenaires: [String]
    $idLaboratoires: [ID]
    $pharmacieRoles: PharmacieRolesInput
    $presidentRegions: PresidentRegionsInput
    $fichierCible: FichierInput
  ) {
    actualiteAllUsersCible(
      codeServices: $codeServices
      idPartenaires: $idPartenaires
      idLaboratoires: $idLaboratoires
      pharmacieRoles: $pharmacieRoles
      presidentRegions: $presidentRegions
      fichierCible: $fichierCible
    ) {
      id
      userPhoto {
        id
        fichier {
          id
          publicUrl
        }
      }
      userName
      userPersonnel {
        id
        nom
        prenom
        service {
          id
          nom
        }
      }
      userPpersonnel {
        id
        nom
        prenom
        estAmbassadrice
        pharmacie {
          id
          cip
        }
      }
      userLaboratoire {
        id
        nomLabo
      }
      userPartenaire {
        id
        nom
      }
      userTitulaire {
        id
        isPresident
        titulaire {
          id
          nom
          prenom
        }
      }
    }
  }
`;

export const DO_SEARCH_ACTUALITES = gql`
  query SEARCH_ACTUALITES(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $idPharmacieUser: ID
    $userId: String
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Actualite {
          id
          libelle
          dateDebut
          dateFin
          description
          niveauPriorite
          nbSeenByPharmacie
          seen(idPharmacie: $idPharmacieUser, userId: $userId)
          origine {
            id
            libelle
          }
          laboratoire {
            id
            nomLabo
          }
          fichierPresentations {
            id
            nomOriginal
            publicUrl
          }
        }
      }
    }
  }
`;

export const GET_SEARCH_CUSTOM_CONTENT_ACTUALITE = gql`
  query SEARCH_CUSTOM_CONTENT_ACTUALITE($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Actualite {
          id
          libelle
          description
        }
      }
    }
  }
`;

export const GET_SEARCH_ACTUALITE = gql`
  query SEARCH_ACTUALITE(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $idPharmacieUser: ID
    $userId: String
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Actualite {
          ...ActualiteInfo
        }
      }
    }
  }
  ${ACTUALITE_INFO_FRAGEMENT}
`;
