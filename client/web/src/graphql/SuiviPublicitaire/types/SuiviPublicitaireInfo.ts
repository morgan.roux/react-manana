/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SuiviPublicitaireInfo
// ====================================================

export interface SuiviPublicitaireInfo_publicite {
  __typename: "Publicite";
  id: string;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
}

export interface SuiviPublicitaireInfo_commande {
  __typename: "Commande";
  id: string;
}

export interface SuiviPublicitaireInfo {
  __typename: "SuiviPublicitaire";
  id: string;
  publicite: SuiviPublicitaireInfo_publicite | null;
  commande: SuiviPublicitaireInfo_commande | null;
  nbView: number | null;
  nbClick: number | null;
  nbAccesOp: number | null;
  nbCommande: number | null;
}
