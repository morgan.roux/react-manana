/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: incrementPubliciteOperationClickCount
// ====================================================

export interface incrementPubliciteOperationClickCount_incrementPubliciteOperationClickCount_publicite {
  __typename: "Publicite";
  id: string;
}

export interface incrementPubliciteOperationClickCount_incrementPubliciteOperationClickCount {
  __typename: "SuiviPublicitaire";
  id: string;
  publicite: incrementPubliciteOperationClickCount_incrementPubliciteOperationClickCount_publicite | null;
  nbView: number | null;
  nbClick: number | null;
  nbAccesOp: number | null;
  nbCommande: number | null;
}

export interface incrementPubliciteOperationClickCount {
  incrementPubliciteOperationClickCount: incrementPubliciteOperationClickCount_incrementPubliciteOperationClickCount | null;
}

export interface incrementPubliciteOperationClickCountVariables {
  idPublicite: string;
}
