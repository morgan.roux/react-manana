import gql from 'graphql-tag';

export const DO_INCREMENT_PUBLICITE_VIEW_COUNT = gql`
  mutation incrementPubliciteViewCount($idPublicite: ID!) {
    incrementPubliciteViewCount(idPublicite: $idPublicite) {
      id
      publicite {
        id
      }
      nbView
      nbClick
      nbAccesOp
      nbCommande
    }
  }
`;

export const DO_INCREMENT_PUBLICITE_CLICK_COUNT = gql`
  mutation incrementPubliciteClickCount($idPublicite: ID!) {
    incrementPubliciteClickCount(idPublicite: $idPublicite) {
      id
      publicite {
        id
      }
      nbView
      nbClick
      nbAccesOp
      nbCommande
    }
  }
`;

export const DO_INCREMENT_PUBLICITE_OPERATION_CLICK_COUNT = gql`
  mutation incrementPubliciteOperationClickCount($idPublicite: ID!) {
    incrementPubliciteOperationClickCount(idPublicite: $idPublicite) {
      id
      publicite {
        id
      }
      nbView
      nbClick
      nbAccesOp
      nbCommande
    }
  }
`;
