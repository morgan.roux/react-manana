import gql from 'graphql-tag';
import { COMMANDE_CANAL_INFO_FRAGMENT } from '../CommandeCanal/fragment';

export const PHARMACIE_ACHAT_INFO_FRAGMENT = gql`
  fragment PharmacieAchatInfo on PharmacieAchat {
    id
    dateDebut
    dateFin
    identifiantAchatCanal
    commentaire
    canal {
      ...CommandeCanalInfo
    }
  }
  ${COMMANDE_CANAL_INFO_FRAGMENT}
`;
