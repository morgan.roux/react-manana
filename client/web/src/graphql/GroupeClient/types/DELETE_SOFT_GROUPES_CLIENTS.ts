/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_SOFT_GROUPES_CLIENTS
// ====================================================

export interface DELETE_SOFT_GROUPES_CLIENTS_deleteSoftGroupeClients_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface DELETE_SOFT_GROUPES_CLIENTS_deleteSoftGroupeClients {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
  nomGroupe: string | null;
  nomCommercial: string | null;
  dateValiditeDebut: any | null;
  dateValiditeFin: any | null;
  pharmacies: (DELETE_SOFT_GROUPES_CLIENTS_deleteSoftGroupeClients_pharmacies | null)[] | null;
}

export interface DELETE_SOFT_GROUPES_CLIENTS {
  deleteSoftGroupeClients: (DELETE_SOFT_GROUPES_CLIENTS_deleteSoftGroupeClients | null)[] | null;
}

export interface DELETE_SOFT_GROUPES_CLIENTSVariables {
  ids: string[];
}
