/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_SOFT_GROUPE_CLIENT
// ====================================================

export interface DELETE_SOFT_GROUPE_CLIENT_deleteSoftGroupeClient_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface DELETE_SOFT_GROUPE_CLIENT_deleteSoftGroupeClient {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
  nomGroupe: string | null;
  nomCommercial: string | null;
  dateValiditeDebut: any | null;
  dateValiditeFin: any | null;
  pharmacies: (DELETE_SOFT_GROUPE_CLIENT_deleteSoftGroupeClient_pharmacies | null)[] | null;
}

export interface DELETE_SOFT_GROUPE_CLIENT {
  deleteSoftGroupeClient: DELETE_SOFT_GROUPE_CLIENT_deleteSoftGroupeClient | null;
}

export interface DELETE_SOFT_GROUPE_CLIENTVariables {
  id: string;
}
