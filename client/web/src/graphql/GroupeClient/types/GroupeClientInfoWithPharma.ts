/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: GroupeClientInfoWithPharma
// ====================================================

export interface GroupeClientInfoWithPharma_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface GroupeClientInfoWithPharma {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
  nomGroupe: string | null;
  nomCommercial: string | null;
  dateValiditeDebut: any | null;
  dateValiditeFin: any | null;
  pharmacies: (GroupeClientInfoWithPharma_pharmacies | null)[] | null;
}
