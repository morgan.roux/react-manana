/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT_search_data_GroupeClient_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT_search_data_GroupeClient {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
  nomGroupe: string | null;
  nomCommercial: string | null;
  dateValiditeDebut: any | null;
  dateValiditeFin: any | null;
  pharmacies: (SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT_search_data_GroupeClient_pharmacies | null)[] | null;
}

export type SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT_search_data = SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT_search_data_Action | SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT_search_data_GroupeClient;

export interface SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT {
  search: SEARCH_CUSTOM_CONTENT_GROUPE_CLIENT_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_GROUPE_CLIENTVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
