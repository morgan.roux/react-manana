/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePartenaire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: LIST_LABORATOIRE_PARTENAIRE
// ====================================================

export interface LIST_LABORATOIRE_PARTENAIRE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_laboSuite {
  __typename: "LaboratoireSuite";
  adresse: string | null;
  codePostal: string | null;
  ville: string | null;
}

export interface LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_actualites_partenairesCible {
  __typename: "Partenaire";
  nom: string | null;
}

export interface LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_actualites {
  __typename: "Actualite";
  dateCreation: any | null;
  dateFin: any | null;
  partenairesCible: (LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_actualites_partenairesCible | null)[] | null;
}

export interface LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_user_userPartenaire_partenaireServicePartenaire {
  __typename: "PartenaireServicePartenaire";
  dateDebutPartenaire: any;
  dateFinPartenaire: any;
  typePartenaire: TypePartenaire | null;
}

export interface LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_user_userPartenaire {
  __typename: "Partenaire";
  partenaireServicePartenaire: LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_user_userPartenaire_partenaireServicePartenaire | null;
}

export interface LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_user {
  __typename: "User";
  userPartenaire: LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_user_userPartenaire | null;
}

export interface LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  laboSuite: LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_laboSuite | null;
  actualites: (LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_actualites | null)[] | null;
  user: LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire_user | null;
}

export type LIST_LABORATOIRE_PARTENAIRE_search_data = LIST_LABORATOIRE_PARTENAIRE_search_data_Action | LIST_LABORATOIRE_PARTENAIRE_search_data_Laboratoire;

export interface LIST_LABORATOIRE_PARTENAIRE_search {
  __typename: "SearchResult";
  total: number;
  data: (LIST_LABORATOIRE_PARTENAIRE_search_data | null)[] | null;
}

export interface LIST_LABORATOIRE_PARTENAIRE {
  search: LIST_LABORATOIRE_PARTENAIRE_search | null;
}

export interface LIST_LABORATOIRE_PARTENAIREVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
