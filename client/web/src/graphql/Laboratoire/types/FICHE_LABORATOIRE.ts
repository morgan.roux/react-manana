/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FICHE_LABORATOIRE
// ====================================================

export interface FICHE_LABORATOIRE_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface FICHE_LABORATOIRE {
  laboratoire: FICHE_LABORATOIRE_laboratoire | null;
}

export interface FICHE_LABORATOIREVariables {
  id: string;
}
