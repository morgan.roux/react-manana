/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePartenaire, StatutPartenaire, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: LABORATOIRE
// ====================================================

export interface LABORATOIRE_laboratoire_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  ville: string | null;
  adresse: string | null;
  codePostal: string | null;
  telephone: string | null;
}

export interface LABORATOIRE_laboratoire_actualites_fichierPresentations {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface LABORATOIRE_laboratoire_actualites {
  __typename: "Actualite";
  id: string;
  libelle: string | null;
  dateCreation: any | null;
  description: string | null;
  fichierPresentations: (LABORATOIRE_laboratoire_actualites_fichierPresentations | null)[] | null;
}

export interface LABORATOIRE_laboratoire_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface LABORATOIRE_laboratoire_photo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface LABORATOIRE_laboratoire_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface LABORATOIRE_laboratoire_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: LABORATOIRE_laboratoire_user_userPhoto_fichier | null;
}

export interface LABORATOIRE_laboratoire_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  codeTraitements: (string | null)[] | null;
  userPhoto: LABORATOIRE_laboratoire_user_userPhoto | null;
}

export interface LABORATOIRE_laboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  laboSuite: LABORATOIRE_laboratoire_laboSuite | null;
  actualites: (LABORATOIRE_laboratoire_actualites | null)[] | null;
  laboratoirePartenaire: LABORATOIRE_laboratoire_laboratoirePartenaire | null;
  photo: LABORATOIRE_laboratoire_photo | null;
  user: LABORATOIRE_laboratoire_user | null;
}

export interface LABORATOIRE {
  laboratoire: LABORATOIRE_laboratoire | null;
}

export interface LABORATOIREVariables {
  id: string;
}
