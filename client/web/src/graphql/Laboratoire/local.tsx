import gql from 'graphql-tag';
export const GET_LABORATOIRES = gql`
  {
    laboratoires {
      id
      nomLabo
      actualites {
        libelle
        dateCreation
        description
        fichierPresentations {
          publicUrl
        }
      }
    }
  }
`;

export const GET_CHECKEDS_LABORATOIRE = gql`
  {
    checkedsLaboratoire @client {
      id
      nomLabo
    }
  }
`;
