import gql from 'graphql-tag';

export const SET_SORTIE_LABORATOIRE = gql`
  mutation setLaboratoireSortie($id: ID!, $sortie: Int) {
    updateLaboratoireSortie(id: $id, sortie: $sortie) {
      id
      nomLabo
      sortie
      actualites {
        libelle
        dateCreation
        fichierPresentations {
          publicUrl
        }
        description
      }
    }
  }
`;

export const DO_CREATE_UPDATE_LABORATOIRE = gql`
  mutation CREATE_UPDATE_LABORATOIRE($id: ID!, $nomLabo: String!, $suite: LaboratoireSuiteInput) {
    createUpdateLaboratoire(id: $id, nomLabo: $nomLabo, suite: $suite) {
      id
      nomLabo
      sortie
      laboSuite {
        id
        adresse
        telephone
        ville
        codePostal
        webSiteUrl
      }
    }
  }
`;

export const DO_CREATE_UPDATE_PHOTO_LABORATOIRE = gql`
  mutation CREATE_UPDATE_PHOTO_LABORATOIRE($id: ID!, $photo: FichierInput) {
    updatePhotoLaboratoire(id: $id, photo: $photo) {
      id
      nomLabo
      sortie
      laboSuite {
        id
        adresse
        telephone
        ville
        codePostal
        webSiteUrl
      }
    }
  }
`;
