import gql from 'graphql-tag';
import { FICHIER_FRAGMENT } from '../Fichier/fragment';

export const OPERATION_ARTICLE_CIBLE_INFO_FRAGEMENT = gql`
  fragment OperationArticleCibleInfo on OperationArticleCible {
    id
    dateCreation
    dateModification
    fichierCible {
      ...FichierInfo
    }
  }
  ${FICHIER_FRAGMENT}
`;
