/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: OperationArticleCibleInfo
// ====================================================

export interface OperationArticleCibleInfo_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OperationArticleCibleInfo {
  __typename: "OperationArticleCible";
  id: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichierCible: OperationArticleCibleInfo_fichierCible | null;
}
