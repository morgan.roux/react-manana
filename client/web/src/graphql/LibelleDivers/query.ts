import gql from 'graphql-tag';

export const GET_LIBELLE_DIVERSES = gql`
  query LIBELLE_DIVERSES($code: Int!) {
    libelleDiverses(code: $code) {
      id
      codeInfo
      code
      libelle
    }
  }
`;
