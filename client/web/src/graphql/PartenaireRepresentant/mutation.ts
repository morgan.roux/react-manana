import gql from 'graphql-tag';

export const DO_CREATE_UPDATE_PARTENAIRE_REPRESENTANT = gql`
  mutation CREATE_UPDATE_PARTENAIRE_REPRESENTANT($input: PartenaireRepresentantInput!) {
    createUpdatePartenaireRepresentant(input: $input) {
      id
      photo {
        id
        urlPresigned
        publicUrl
      }
      fonction
      nom
      prenom
      contact {
        id
        adresse1
        ville
        telMobProf
        telPerso
        telMobPerso
        mailProf
        mailPerso
        adresse1
        adresse2
      }
    }
  }
`;

export const DO_DELETE_PARTENAIRE_REPRESENTANT = gql`
  mutation DELETE_PARTENAIRE_REPRESENTANT($ids: [ID!]!) {
    softDeletePartenaireRepresentants(ids: $ids) {
      id
    }
  }
`;
