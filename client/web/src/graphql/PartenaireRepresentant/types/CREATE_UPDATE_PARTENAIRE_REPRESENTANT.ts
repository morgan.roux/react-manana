/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PartenaireRepresentantInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_PARTENAIRE_REPRESENTANT
// ====================================================

export interface CREATE_UPDATE_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentant_photo {
  __typename: "Fichier";
  id: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface CREATE_UPDATE_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentant_contact {
  __typename: "Contact";
  id: string;
  adresse1: string | null;
  ville: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  adresse2: string | null;
}

export interface CREATE_UPDATE_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentant {
  __typename: "PartenaireRepresentant";
  id: string;
  photo: CREATE_UPDATE_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentant_photo | null;
  fonction: string | null;
  nom: string;
  prenom: string | null;
  contact: CREATE_UPDATE_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentant_contact | null;
}

export interface CREATE_UPDATE_PARTENAIRE_REPRESENTANT {
  createUpdatePartenaireRepresentant: CREATE_UPDATE_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentant | null;
}

export interface CREATE_UPDATE_PARTENAIRE_REPRESENTANTVariables {
  input: PartenaireRepresentantInput;
}
