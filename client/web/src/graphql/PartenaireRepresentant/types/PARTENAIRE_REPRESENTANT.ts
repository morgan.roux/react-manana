/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sexe } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PARTENAIRE_REPRESENTANT
// ====================================================

export interface PARTENAIRE_REPRESENTANT_partenaireRepresentant_contact {
  __typename: "Contact";
  id: string;
  adresse1: string | null;
  ville: string | null;
  mailProf: string | null;
  siteProf: string | null;
  mailPerso: string | null;
  telProf: string | null;
  telPerso: string | null;
  cp: string | null;
  urlLinkedinProf: string | null;
  urlFacebookProf: string | null;
  whatsAppMobProf: string | null;
  urlMessenger: string | null;
  urlYoutube: string | null;
}

export interface PARTENAIRE_REPRESENTANT_partenaireRepresentant_partenaireType {
  __typename: "PartenaireType";
  id: string;
  libelle: string;
  codeMaj: string | null;
  isRemoved: boolean | null;
}

export interface PARTENAIRE_REPRESENTANT_partenaireRepresentant_partenaire_partenaireServiceSuite_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
}

export interface PARTENAIRE_REPRESENTANT_partenaireRepresentant_partenaire_partenaireServiceSuite {
  __typename: "PartenaireServiceSuite";
  id: string;
  groupement: PARTENAIRE_REPRESENTANT_partenaireRepresentant_partenaire_partenaireServiceSuite_groupement | null;
}

export interface PARTENAIRE_REPRESENTANT_partenaireRepresentant_partenaire {
  __typename: "Partenaire";
  id: string;
  partenaireServiceSuite: PARTENAIRE_REPRESENTANT_partenaireRepresentant_partenaire_partenaireServiceSuite | null;
}

export interface PARTENAIRE_REPRESENTANT_partenaireRepresentant_photo_avatar_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface PARTENAIRE_REPRESENTANT_partenaireRepresentant_photo_avatar {
  __typename: "Avatar";
  id: string;
  description: string | null;
  codeSexe: Sexe | null;
  fichier: PARTENAIRE_REPRESENTANT_partenaireRepresentant_photo_avatar_fichier | null;
}

export interface PARTENAIRE_REPRESENTANT_partenaireRepresentant_photo {
  __typename: "Fichier";
  id: string;
  avatar: PARTENAIRE_REPRESENTANT_partenaireRepresentant_photo_avatar | null;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface PARTENAIRE_REPRESENTANT_partenaireRepresentant {
  __typename: "PartenaireRepresentant";
  id: string;
  civilite: string | null;
  nom: string;
  prenom: string | null;
  sexe: string | null;
  fonction: string | null;
  afficherComme: string | null;
  contact: PARTENAIRE_REPRESENTANT_partenaireRepresentant_contact | null;
  partenaireType: PARTENAIRE_REPRESENTANT_partenaireRepresentant_partenaireType | null;
  partenaire: PARTENAIRE_REPRESENTANT_partenaireRepresentant_partenaire | null;
  photo: PARTENAIRE_REPRESENTANT_partenaireRepresentant_photo | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
}

export interface PARTENAIRE_REPRESENTANT {
  partenaireRepresentant: PARTENAIRE_REPRESENTANT_partenaireRepresentant | null;
}

export interface PARTENAIRE_REPRESENTANTVariables {
  id: string;
}
