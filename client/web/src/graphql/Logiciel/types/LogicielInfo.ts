/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: LogicielInfo
// ====================================================

export interface LogicielInfo_ssii {
  __typename: "SSII";
  id: string;
  nom: string;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  mail: string | null;
  web: string | null;
  commentaire: string | null;
}

export interface LogicielInfo {
  __typename: "Logiciel";
  id: string;
  nom: string | null;
  os: string | null;
  osVersion: string | null;
  commentaire: string | null;
  ssii: LogicielInfo_ssii | null;
}
