/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: LOGICIELS_WITH_MINIM_INFO
// ====================================================

export interface LOGICIELS_WITH_MINIM_INFO_logiciels_ssii {
  __typename: "SSII";
  id: string;
  nom: string;
}

export interface LOGICIELS_WITH_MINIM_INFO_logiciels {
  __typename: "Logiciel";
  id: string;
  nom: string | null;
  ssii: LOGICIELS_WITH_MINIM_INFO_logiciels_ssii | null;
}

export interface LOGICIELS_WITH_MINIM_INFO {
  logiciels: (LOGICIELS_WITH_MINIM_INFO_logiciels | null)[] | null;
}
