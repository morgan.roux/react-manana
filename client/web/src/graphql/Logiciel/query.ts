import gql from 'graphql-tag';

export const GET_LOGICIELS_WITH_MINIM_INFO = gql`
  query LOGICIELS_WITH_MINIM_INFO {
    logiciels {
      id
      nom
      ssii {
        id
        nom
      }
    }
  }
`;
