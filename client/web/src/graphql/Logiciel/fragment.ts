import gql from 'graphql-tag';
import { SSII_INFO_FRAGMENT } from '../SSII/fragment';

export const LOGICIEL_INFO_FRAGMENT = gql`
  fragment LogicielInfo on Logiciel {
    id
    nom
    os
    osVersion
    commentaire
    ssii {
      ...SSIIInfo
    }
  }
  ${SSII_INFO_FRAGMENT}
`;
