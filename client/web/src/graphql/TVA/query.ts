import gql from 'graphql-tag';

export const GET_TVAS = gql`
  query TVAS {
    tvas {
      id
      codeTva
      tauxTva
      designation
    }
  }
`;
