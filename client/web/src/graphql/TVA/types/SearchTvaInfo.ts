/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SearchTvaInfo
// ====================================================

export interface SearchTvaInfo {
  __typename: "TVA";
  type: string;
  id: string;
  codeTva: number | null;
  tauxTva: number | null;
}
