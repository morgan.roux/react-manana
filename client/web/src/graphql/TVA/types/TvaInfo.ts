/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TvaInfo
// ====================================================

export interface TvaInfo {
  __typename: "TVA";
  type: string;
  id: string;
  codeTva: number | null;
  tauxTva: number | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}
