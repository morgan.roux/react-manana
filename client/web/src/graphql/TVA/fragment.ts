import gql from 'graphql-tag';

export const TVA_INFO = gql`
  fragment TvaInfo on TVA {
    type
    id
    codeTva
    tauxTva
    codeMaj
    dateCreation
    dateModification
  }
`;

export const SEARCH_TVA_INFO = gql`
  fragment SearchTvaInfo on TVA {
    type
    id
    codeTva
    tauxTva
  }
`;
