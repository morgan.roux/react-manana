import gql from 'graphql-tag';

export const CANAL_GAMME_INFO = gql`
  fragment CanalGammeInfo on CanalGamme {
    id
    codeGamme
    libelle
    estDefaut
  }
`;

export const SEARCH_CANAL_GAMME_INFO = gql`
  fragment SearchCanalGammeInfo on CanalGamme {
    type
    id
    codeGamme
    libelle
    countCanalArticles
    sousGamme {
      id
      codeSousGamme
      libelle
      countCanalArticles
    }
  }
`;
