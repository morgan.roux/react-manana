/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: CanalGammeInfo
// ====================================================

export interface CanalGammeInfo {
  __typename: "CanalGamme";
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
}
