/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GAMME_COMMERCIAL
// ====================================================

export interface GAMME_COMMERCIAL_canalGamme_sousGamme {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
  countCanalArticles: number | null;
}

export interface GAMME_COMMERCIAL_canalGamme {
  __typename: "CanalGamme";
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  countCanalArticles: number | null;
  sousGamme: (GAMME_COMMERCIAL_canalGamme_sousGamme | null)[] | null;
}

export interface GAMME_COMMERCIAL {
  canalGamme: GAMME_COMMERCIAL_canalGamme | null;
}

export interface GAMME_COMMERCIALVariables {
  id: string;
}
