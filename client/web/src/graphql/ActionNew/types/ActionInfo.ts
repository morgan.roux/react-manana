/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionStatus, TypeProject } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: ActionInfo
// ====================================================

export interface ActionInfo_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface ActionInfo_origine {
  __typename: "TodoActionOrigine";
  id: string;
  code: string;
  libelle: string;
}

export interface ActionInfo_section {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
}

export interface ActionInfo_actionParent {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  nbComment: number;
}

export interface ActionInfo_subActions_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface ActionInfo_subActions {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: ActionInfo_subActions_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbComment: number;
}

export interface ActionInfo_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
}

export interface ActionInfo_project {
  __typename: "Project";
  id: string;
  name: string | null;
  typeProject: TypeProject | null;
}

export interface ActionInfo_userCreation_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ActionInfo_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: ActionInfo_userCreation_role | null;
}

export interface ActionInfo_assignedUsers_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface ActionInfo_assignedUsers_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: ActionInfo_assignedUsers_userPhoto_fichier | null;
}

export interface ActionInfo_assignedUsers {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  userPhoto: ActionInfo_assignedUsers_userPhoto | null;
}

export interface ActionInfo_etiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface ActionInfo_etiquettes {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  nom: string;
  isRemoved: boolean | null;
  couleur: ActionInfo_etiquettes_couleur | null;
}

export interface ActionInfo {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  _priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: ActionInfo_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  idItemAssocie: string | null;
  nbComment: number;
  origine: ActionInfo_origine | null;
  section: ActionInfo_section | null;
  actionParent: ActionInfo_actionParent | null;
  subActions: ActionInfo_subActions[] | null;
  item: ActionInfo_item | null;
  project: ActionInfo_project | null;
  userCreation: ActionInfo_userCreation | null;
  assignedUsers: ActionInfo_assignedUsers[] | null;
  etiquettes: ActionInfo_etiquettes[] | null;
}
