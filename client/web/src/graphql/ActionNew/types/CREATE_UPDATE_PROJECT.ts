/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ProjectInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_PROJECT
// ====================================================

export interface CREATE_UPDATE_PROJECT_createUpdateProject {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface CREATE_UPDATE_PROJECT {
  createUpdateProject: CREATE_UPDATE_PROJECT_createUpdateProject;
}

export interface CREATE_UPDATE_PROJECTVariables {
  input: ProjectInput;
}
