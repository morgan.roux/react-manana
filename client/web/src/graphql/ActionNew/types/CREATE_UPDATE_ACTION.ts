/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_ACTION
// ====================================================

export interface CREATE_UPDATE_ACTION_createUpdateAction {
  __typename: "Action";
  id: string;
  type: string;
}

export interface CREATE_UPDATE_ACTION {
  createUpdateAction: CREATE_UPDATE_ACTION_createUpdateAction;
}

export interface CREATE_UPDATE_ACTIONVariables {
  input: ActionInput;
}
