import gql from 'graphql-tag';

import {
  INDICATEUR_STATISTIQUE_BUSINESS_INFO_FRAGMENT,
  INDICATEUR_STATISTIQUE_FEEDBACK_INFO_FRAGMENT,
  PHARMACIE_STATISTIQUE_BUSINESS_INFO_FRAGMENT,
  PHARMACIE_STATISTIQUE_FEEDBACK_INFO_FRAGMENT,
} from './fragment';

export const GET_INDICATEUR_STATISTIQUE_BUSINESS = gql`
  query INDICATEUR_STATISTIQUE_BUSINESS(
    $pilotage: PilotageBusiness!
    $idPharmacies: [ID]
    $idOperations: [ID]
    $idPharmaciesSelected: [ID]
  ) {
    indicateurStatistiqueBusiness(
      pilotage: $pilotage
      idOperations: $idOperations
      idPharmacies: $idPharmacies
      idPharmaciesSelected: $idPharmaciesSelected
    ) {
      ...IndicateurStatistiqueBusinessInfo
    }
  }
  ${INDICATEUR_STATISTIQUE_BUSINESS_INFO_FRAGMENT}
`;

export const GET_INDICATEUR_STATISTIQUE_FEEDBACK = gql`
  query INDICATEUR_STATISTIQUE_FEEDBACK(
    $codeItem: String!
    $pilotage: PilotageFeedback!
    $idItemAssocies: [ID]
    $idPharmacies: [ID]
  ) {
    indicateurStatistiqueFeedback(
      codeItem: $codeItem
      pilotage: $pilotage
      idItemAssocies: $idItemAssocies
      idPharmacies: $idPharmacies
    ) {
      ...IndicateurStatistiqueFeedbackInfo
    }
  }
  ${INDICATEUR_STATISTIQUE_FEEDBACK_INFO_FRAGMENT}
`;

export const GET_PHARMACIE_OPERATION_STATISTIQUE_BUSINESS = gql`
  query PHARMACIE_OPERATION_STATISTIQUE_BUSINESS(
    $pilotage: PilotageBusiness!
    $idPharmacies: [ID]
    $idOperations: [ID]
    $take: Int
    $skip: Int
    $orderBy: OrederByBusiness
    $isAsc: Boolean
  ) {
    pharmacieOperationStatistiqueBusiness(
      pilotage: $pilotage
      idOperations: $idOperations
      idPharmacies: $idPharmacies
      take: $take
      skip: $skip
      orderBy: $orderBy
      isAsc: $isAsc
    ) {
      ...PharmacieStatistiqueBusinessResultInfo
    }
  }
  ${PHARMACIE_STATISTIQUE_BUSINESS_INFO_FRAGMENT}
`;

export const GET_PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK = gql`
  query PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK(
    $codeItem: String!
    $pilotage: PilotageFeedback!
    $idItemAssocies: [ID]
    $idPharmacies: [ID]
    $take: Int
    $skip: Int
    $orderBy: OrderByFeedback
    $isAsc: Boolean
  ) {
    pharmacieItemAssocieStatistiqueFeedback(
      codeItem: $codeItem
      pilotage: $pilotage
      idItemAssocies: $idItemAssocies
      idPharmacies: $idPharmacies
      take: $take
      skip: $skip
      orderBy: $orderBy
      isAsc: $isAsc
    ) {
      ...PharmacieStatistiqueFeedbackResultInfo
    }
  }
  ${PHARMACIE_STATISTIQUE_FEEDBACK_INFO_FRAGMENT}
`;

export const GET_STATISTIQUE_BUSINESS = gql`
  query STATISTIQUE_BUSINESS(
    $pilotage: PilotageBusiness!
    $indicateur: BusinessIndicateur!
    $idPharmacies: [ID]
    $idOperations: [ID]
    $idPharmaciesSelected: [ID]
  ) {
    statistiqueBusiness(
      pilotage: $pilotage
      indicateur: $indicateur
      idOperations: $idOperations
      idPharmacies: $idPharmacies
      idPharmaciesSelected: $idPharmaciesSelected
    ) {
      objectif
      value
    }
  }
`;

export const GET_STATISTIQUE_FEEDBACK = gql`
  query STATISTIQUE_FEEDBACK(
    $codeItem: String!
    $pilotage: PilotageFeedback!
    $indicateur: FeedbackIndicateur!
    $idItemAssocies: [ID]
    $idPharmacies: [ID]
  ) {
    statistiqueFeedback(
      codeItem: $codeItem
      pilotage: $pilotage
      indicateur: $indicateur
      idItemAssocies: $idItemAssocies
      idPharmacies: $idPharmacies
    ) {
      objectif
      value
      nbPharmacieSmeyley
    }
  }
`;

export const GET_BUSINESS_ACHIEVED_IN_YEAR = gql`
  query BUSINESS_ACHIEVED_IN_YEAR(
    $pilotage: PilotageBusiness!
    $indicateur: BusinessIndicateur!
    $year: Int
    $idOperations: [ID]
    $idPharmacies: [ID]
    $idPharmaciesSelected: [ID]
  ) {
    businessAchievedInYear(
      pilotage: $pilotage
      indicateur: $indicateur
      year: $year
      idOperations: $idOperations
      idPharmacies: $idPharmacies
      idPharmaciesSelected: $idPharmaciesSelected
    ) {
      key
      value
    }
  }
`;

export const GET_FEEDBACK_ACHIEVED_IN_YEAR = gql`
  query FEEDBACK_ACHIEVED_IN_YEAR(
    $codeItem: String!
    $pilotage: PilotageFeedback!
    $indicateur: FeedbackIndicateur!
    $year: Int
    $idItemAssocies: [ID]
    $idPharmacies: [ID]
  ) {
    feedbackAchievedInYear(
      codeItem: $codeItem
      pilotage: $pilotage
      indicateur: $indicateur
      year: $year
      idItemAssocies: $idItemAssocies
      idPharmacies: $idPharmacies
    ) {
      key
      value
    }
  }
`;

export const GET_PILOTAGE_COMMENTS = gql`
  query PILOTAGE_COMMENTS(
    $codeItem: String!
    $idSource: ID!
    $idsPharmacies: [ID!]!
    $take: Int
    $skip: Int
  ) {
    pilotageComments(
      codeItem: $codeItem
      idSource: $idSource
      idsPharmacies: $idsPharmacies
      take: $take
      skip: $skip
    ) {
      total
      data {
        id
        content
        dateCreation
        dateModification
        user {
          id
          userName
          userPhoto {
            id
            fichier {
              id
              publicUrl
            }
          }
          role {
            id
            nom
          }
          pharmacie {
            id
            nom
          }
        }
      }
    }
  }
`;
