import gql from 'graphql-tag';

export const INDICATEUR_STATISTIQUE_BUSINESS_INFO_FRAGMENT = gql`
  fragment IndicateurStatistiqueBusinessInfo on IndicateurStatistiqueBusiness {
    caTotal
    nbCommandes
    nbPharmacies
    nbProduits
    nbLignes
    caMoyenParPharmacie
    totalRemise
    globaliteRemise
    operations {
      id
      libelle
      dateDebut
      dateFin
      commandeCanal {
        id
        libelle
      }
    }
    pharmacies {
      id
      nom
      cp
      cip
      ville
    }
  }
`;

export const INDICATEUR_STATISTIQUE_FEEDBACK_INFO_FRAGMENT = gql`
  fragment IndicateurStatistiqueFeedbackInfo on IndicateurStatistiqueFeedback {
    lu
    adore
    content
    aime
    mecontent
    deteste
    commentaires
    partages
    recommandations
    cibles
    pharmacies {
      id
      nom
      cp
      cip
      ville
    }
    itemAssocies {
      ... on Operation {
        type
        id
        libelle
        dateDebut
        dateFin
        commandeCanal {
          id
          libelle
        }
      }
      ... on Partenaire {
        type
        id
        nom
      }
      ... on Laboratoire {
        type
        id
        nomLabo
        sortie
      }
      ... on Project {
        type
        id
        name
        isRemoved
        dateCreation
        dateModification
      }
      ... on ProduitCanal {
        type
        id
      }
      ... on Actualite {
        type
        id
        libelle
        dateDebut
        dateFin
        item {
          id
          code
          name
          codeItem
        }
      }
      ... on Commande {
        type
        id
        dateCommande
        commandeType {
          id
          code
          libelle
        }
        codeReference
        owner {
          id
          email
          login
          userName
        }
      }
    }
  }
`;

export const PHARMACIE_STATISTIQUE_BUSINESS_INFO_FRAGMENT = gql`
  fragment PharmacieStatistiqueBusinessResultInfo on ResultStatistiqueBusiness {
    total
    data {
      id
      caTotal
      nbCommandes
      nbPharmacies
      nbProduits
      nbLignes
      caMoyenParPharmacie
      totalRemise
      globaliteRemise
      operation {
        id
        libelle
        dateDebut
        dateFin
        commandeCanal {
          id
          libelle
        }
      }
      pharmacie {
        id
        nom
        cp
        cip
        ville
      }
    }
  }
`;

export const PHARMACIE_STATISTIQUE_FEEDBACK_INFO_FRAGMENT = gql`
  fragment PharmacieStatistiqueFeedbackResultInfo on ResultStatistiqueFeedback {
    total
    data {
      id
      lu
      adore
      content
      aime
      mecontent
      deteste
      commentaires
      partages
      recommandations
      cibles
      pharmacie {
        id
        nom
        cp
        cip
        ville
      }
      itemAssocie {
        ... on Operation {
          type
          id
          libelle
          dateDebut
          dateFin
          commandeCanal {
            id
            libelle
          }
        }
        ... on Partenaire {
          type
          id
          nom
        }
        ... on Laboratoire {
          type
          id
          nomLabo
          sortie
        }
        ... on Project {
          type
          id
          name
          isRemoved
          dateCreation
          dateModification
        }
        ... on ProduitCanal {
          type
          id
        }
        ... on Actualite {
          type
          id
          libelle
          dateDebut
          dateFin
          item {
            id
            code
            name
            codeItem
          }
        }
        ... on Commande {
          type
          id
          dateCommande
          commandeType {
            id
            code
            libelle
          }
          codeReference
          owner {
            id
            email
            login
            userName
          }
        }
      }
    }
  }
`;
