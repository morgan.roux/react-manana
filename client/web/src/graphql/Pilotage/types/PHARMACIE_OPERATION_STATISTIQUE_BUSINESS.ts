/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PilotageBusiness, OrederByBusiness } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PHARMACIE_OPERATION_STATISTIQUE_BUSINESS
// ====================================================

export interface PHARMACIE_OPERATION_STATISTIQUE_BUSINESS_pharmacieOperationStatistiqueBusiness_data_operation_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  libelle: string | null;
}

export interface PHARMACIE_OPERATION_STATISTIQUE_BUSINESS_pharmacieOperationStatistiqueBusiness_data_operation {
  __typename: "Operation";
  id: string;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  commandeCanal: PHARMACIE_OPERATION_STATISTIQUE_BUSINESS_pharmacieOperationStatistiqueBusiness_data_operation_commandeCanal | null;
}

export interface PHARMACIE_OPERATION_STATISTIQUE_BUSINESS_pharmacieOperationStatistiqueBusiness_data_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  cp: string | null;
  cip: string | null;
  ville: string | null;
}

export interface PHARMACIE_OPERATION_STATISTIQUE_BUSINESS_pharmacieOperationStatistiqueBusiness_data {
  __typename: "PharmacieStatistiqueBusiness";
  id: string;
  caTotal: number | null;
  nbCommandes: number | null;
  nbPharmacies: number | null;
  nbProduits: number | null;
  nbLignes: number | null;
  caMoyenParPharmacie: number | null;
  totalRemise: number | null;
  globaliteRemise: number | null;
  operation: PHARMACIE_OPERATION_STATISTIQUE_BUSINESS_pharmacieOperationStatistiqueBusiness_data_operation | null;
  pharmacie: PHARMACIE_OPERATION_STATISTIQUE_BUSINESS_pharmacieOperationStatistiqueBusiness_data_pharmacie | null;
}

export interface PHARMACIE_OPERATION_STATISTIQUE_BUSINESS_pharmacieOperationStatistiqueBusiness {
  __typename: "ResultStatistiqueBusiness";
  total: number;
  data: (PHARMACIE_OPERATION_STATISTIQUE_BUSINESS_pharmacieOperationStatistiqueBusiness_data | null)[] | null;
}

export interface PHARMACIE_OPERATION_STATISTIQUE_BUSINESS {
  pharmacieOperationStatistiqueBusiness: PHARMACIE_OPERATION_STATISTIQUE_BUSINESS_pharmacieOperationStatistiqueBusiness | null;
}

export interface PHARMACIE_OPERATION_STATISTIQUE_BUSINESSVariables {
  pilotage: PilotageBusiness;
  idPharmacies?: (string | null)[] | null;
  idOperations?: (string | null)[] | null;
  take?: number | null;
  skip?: number | null;
  orderBy?: OrederByBusiness | null;
  isAsc?: boolean | null;
}
