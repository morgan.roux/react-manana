/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PilotageBusiness, BusinessIndicateur } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: BUSINESS_ACHIEVED_IN_YEAR
// ====================================================

export interface BUSINESS_ACHIEVED_IN_YEAR_businessAchievedInYear {
  __typename: "ResultPayload";
  key: string | null;
  value: string | null;
}

export interface BUSINESS_ACHIEVED_IN_YEAR {
  businessAchievedInYear: (BUSINESS_ACHIEVED_IN_YEAR_businessAchievedInYear | null)[] | null;
}

export interface BUSINESS_ACHIEVED_IN_YEARVariables {
  pilotage: PilotageBusiness;
  indicateur: BusinessIndicateur;
  year?: number | null;
  idOperations?: (string | null)[] | null;
  idPharmacies?: (string | null)[] | null;
  idPharmaciesSelected?: (string | null)[] | null;
}
