/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PILOTAGE_COMMENTS
// ====================================================

export interface PILOTAGE_COMMENTS_pilotageComments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface PILOTAGE_COMMENTS_pilotageComments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: PILOTAGE_COMMENTS_pilotageComments_data_user_userPhoto_fichier | null;
}

export interface PILOTAGE_COMMENTS_pilotageComments_data_user_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface PILOTAGE_COMMENTS_pilotageComments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface PILOTAGE_COMMENTS_pilotageComments_data_user {
  __typename: "User";
  id: string;
  userName: string | null;
  userPhoto: PILOTAGE_COMMENTS_pilotageComments_data_user_userPhoto | null;
  role: PILOTAGE_COMMENTS_pilotageComments_data_user_role | null;
  pharmacie: PILOTAGE_COMMENTS_pilotageComments_data_user_pharmacie | null;
}

export interface PILOTAGE_COMMENTS_pilotageComments_data {
  __typename: "Comment";
  id: string;
  content: string;
  dateCreation: any | null;
  dateModification: any | null;
  user: PILOTAGE_COMMENTS_pilotageComments_data_user;
}

export interface PILOTAGE_COMMENTS_pilotageComments {
  __typename: "CommentResult";
  total: number;
  data: PILOTAGE_COMMENTS_pilotageComments_data[];
}

export interface PILOTAGE_COMMENTS {
  pilotageComments: PILOTAGE_COMMENTS_pilotageComments | null;
}

export interface PILOTAGE_COMMENTSVariables {
  codeItem: string;
  idSource: string;
  idsPharmacies: string[];
  take?: number | null;
  skip?: number | null;
}
