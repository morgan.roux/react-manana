/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PharmacieStatistiqueBusinessResultInfo
// ====================================================

export interface PharmacieStatistiqueBusinessResultInfo_data_operation_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  libelle: string | null;
}

export interface PharmacieStatistiqueBusinessResultInfo_data_operation {
  __typename: "Operation";
  id: string;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  commandeCanal: PharmacieStatistiqueBusinessResultInfo_data_operation_commandeCanal | null;
}

export interface PharmacieStatistiqueBusinessResultInfo_data_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  cp: string | null;
  cip: string | null;
  ville: string | null;
}

export interface PharmacieStatistiqueBusinessResultInfo_data {
  __typename: "PharmacieStatistiqueBusiness";
  id: string;
  caTotal: number | null;
  nbCommandes: number | null;
  nbPharmacies: number | null;
  nbProduits: number | null;
  nbLignes: number | null;
  caMoyenParPharmacie: number | null;
  totalRemise: number | null;
  globaliteRemise: number | null;
  operation: PharmacieStatistiqueBusinessResultInfo_data_operation | null;
  pharmacie: PharmacieStatistiqueBusinessResultInfo_data_pharmacie | null;
}

export interface PharmacieStatistiqueBusinessResultInfo {
  __typename: "ResultStatistiqueBusiness";
  total: number;
  data: (PharmacieStatistiqueBusinessResultInfo_data | null)[] | null;
}
