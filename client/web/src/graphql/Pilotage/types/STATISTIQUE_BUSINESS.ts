/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PilotageBusiness, BusinessIndicateur } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: STATISTIQUE_BUSINESS
// ====================================================

export interface STATISTIQUE_BUSINESS_statistiqueBusiness {
  __typename: "StatistiquePayload";
  objectif: string | null;
  value: string | null;
}

export interface STATISTIQUE_BUSINESS {
  statistiqueBusiness: STATISTIQUE_BUSINESS_statistiqueBusiness | null;
}

export interface STATISTIQUE_BUSINESSVariables {
  pilotage: PilotageBusiness;
  indicateur: BusinessIndicateur;
  idPharmacies?: (string | null)[] | null;
  idOperations?: (string | null)[] | null;
  idPharmaciesSelected?: (string | null)[] | null;
}
