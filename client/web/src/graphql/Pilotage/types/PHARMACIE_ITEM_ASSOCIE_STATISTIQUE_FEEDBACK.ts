/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PilotageFeedback, OrderByFeedback } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK
// ====================================================

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  cp: string | null;
  cip: string | null;
  ville: string | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "CanalGamme" | "GammeCatalogue" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Operation_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  libelle: string | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Operation {
  __typename: "Operation";
  type: string;
  id: string;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  commandeCanal: PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Operation_commandeCanal | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Laboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Project {
  __typename: "Project";
  type: string;
  id: string;
  name: string | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_ProduitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Actualite_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Actualite {
  __typename: "Actualite";
  type: string;
  id: string;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  item: PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Actualite_item | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Commande_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Commande_owner {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Commande {
  __typename: "Commande";
  type: string;
  id: string;
  dateCommande: any | null;
  commandeType: PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Commande_commandeType | null;
  codeReference: string | null;
  owner: PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Commande_owner | null;
}

export type PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie = PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Action | PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Operation | PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Partenaire | PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Laboratoire | PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Project | PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_ProduitCanal | PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Actualite | PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie_Commande;

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data {
  __typename: "PharmacieStatistiqueFeedback";
  id: string;
  lu: number | null;
  adore: number | null;
  content: number | null;
  aime: number | null;
  mecontent: number | null;
  deteste: number | null;
  commentaires: number | null;
  partages: number | null;
  recommandations: number | null;
  cibles: number | null;
  pharmacie: PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_pharmacie | null;
  itemAssocie: PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data_itemAssocie | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback {
  __typename: "ResultStatistiqueFeedback";
  total: number;
  data: (PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback_data | null)[] | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK {
  pharmacieItemAssocieStatistiqueFeedback: PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACK_pharmacieItemAssocieStatistiqueFeedback | null;
}

export interface PHARMACIE_ITEM_ASSOCIE_STATISTIQUE_FEEDBACKVariables {
  codeItem: string;
  pilotage: PilotageFeedback;
  idItemAssocies?: (string | null)[] | null;
  idPharmacies?: (string | null)[] | null;
  take?: number | null;
  skip?: number | null;
  orderBy?: OrderByFeedback | null;
  isAsc?: boolean | null;
}
