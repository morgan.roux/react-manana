/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: IndicateurStatistiqueFeedbackInfo
// ====================================================

export interface IndicateurStatistiqueFeedbackInfo_pharmacies {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  cp: string | null;
  cip: string | null;
  ville: string | null;
}

export interface IndicateurStatistiqueFeedbackInfo_itemAssocies_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "CanalGamme" | "GammeCatalogue" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface IndicateurStatistiqueFeedbackInfo_itemAssocies_Operation_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  libelle: string | null;
}

export interface IndicateurStatistiqueFeedbackInfo_itemAssocies_Operation {
  __typename: "Operation";
  type: string;
  id: string;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  commandeCanal: IndicateurStatistiqueFeedbackInfo_itemAssocies_Operation_commandeCanal | null;
}

export interface IndicateurStatistiqueFeedbackInfo_itemAssocies_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
}

export interface IndicateurStatistiqueFeedbackInfo_itemAssocies_Laboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface IndicateurStatistiqueFeedbackInfo_itemAssocies_Project {
  __typename: "Project";
  type: string;
  id: string;
  name: string | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface IndicateurStatistiqueFeedbackInfo_itemAssocies_ProduitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
}

export interface IndicateurStatistiqueFeedbackInfo_itemAssocies_Actualite_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
}

export interface IndicateurStatistiqueFeedbackInfo_itemAssocies_Actualite {
  __typename: "Actualite";
  type: string;
  id: string;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  item: IndicateurStatistiqueFeedbackInfo_itemAssocies_Actualite_item | null;
}

export interface IndicateurStatistiqueFeedbackInfo_itemAssocies_Commande_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface IndicateurStatistiqueFeedbackInfo_itemAssocies_Commande_owner {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface IndicateurStatistiqueFeedbackInfo_itemAssocies_Commande {
  __typename: "Commande";
  type: string;
  id: string;
  dateCommande: any | null;
  commandeType: IndicateurStatistiqueFeedbackInfo_itemAssocies_Commande_commandeType | null;
  codeReference: string | null;
  owner: IndicateurStatistiqueFeedbackInfo_itemAssocies_Commande_owner | null;
}

export type IndicateurStatistiqueFeedbackInfo_itemAssocies = IndicateurStatistiqueFeedbackInfo_itemAssocies_Action | IndicateurStatistiqueFeedbackInfo_itemAssocies_Operation | IndicateurStatistiqueFeedbackInfo_itemAssocies_Partenaire | IndicateurStatistiqueFeedbackInfo_itemAssocies_Laboratoire | IndicateurStatistiqueFeedbackInfo_itemAssocies_Project | IndicateurStatistiqueFeedbackInfo_itemAssocies_ProduitCanal | IndicateurStatistiqueFeedbackInfo_itemAssocies_Actualite | IndicateurStatistiqueFeedbackInfo_itemAssocies_Commande;

export interface IndicateurStatistiqueFeedbackInfo {
  __typename: "IndicateurStatistiqueFeedback";
  lu: number | null;
  adore: number | null;
  content: number | null;
  aime: number | null;
  mecontent: number | null;
  deteste: number | null;
  commentaires: number | null;
  partages: number | null;
  recommandations: number | null;
  cibles: number | null;
  pharmacies: (IndicateurStatistiqueFeedbackInfo_pharmacies | null)[] | null;
  itemAssocies: (IndicateurStatistiqueFeedbackInfo_itemAssocies | null)[] | null;
}
