/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PilotageFeedback, FeedbackIndicateur } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: FEEDBACK_ACHIEVED_IN_YEAR
// ====================================================

export interface FEEDBACK_ACHIEVED_IN_YEAR_feedbackAchievedInYear {
  __typename: "ResultPayload";
  key: string | null;
  value: string | null;
}

export interface FEEDBACK_ACHIEVED_IN_YEAR {
  feedbackAchievedInYear: (FEEDBACK_ACHIEVED_IN_YEAR_feedbackAchievedInYear | null)[] | null;
}

export interface FEEDBACK_ACHIEVED_IN_YEARVariables {
  codeItem: string;
  pilotage: PilotageFeedback;
  indicateur: FeedbackIndicateur;
  year?: number | null;
  idItemAssocies?: (string | null)[] | null;
  idPharmacies?: (string | null)[] | null;
}
