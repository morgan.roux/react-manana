import gql from 'graphql-tag';

export const GET_PILOTAGE_CIBLES = gql`
  {
    pilotageCibles @client {
      pilotageType
      item {
        type
        id
        code
        codeItem
        parent {
          id
          code
        }
      }
      items {
        id
      }
      pharmacies {
        id
      }
    }
  }
`;

export const GET_PILOTAGE_OC_SOURCE = gql`
  {
    pilotageOcSource @client {
      id
      libelle
      niveauPriorite
      description
      dateDebut
      dateFin
      dateCreation
      dateModification
      CATotal
      caMoyenPharmacie
      item {
        id
        name
      }
      commandeCanal {
        id
        libelle
      }
      laboratoire {
        id
        nomLabo
      }
    }
  }
`;

export const GET_PILOTAGE_ACTU_SOURCE = gql`
  {
    pilotageActuSource @client {
      id
      libelle
      niveauPriorite
      description
      dateDebut
      dateFin
      dateCreation
      dateModification
      item {
        id
        name
      }
      laboratoire {
        id
        nomLabo
      }
      origine {
        id
        libelle
      }
    }
  }
`;
