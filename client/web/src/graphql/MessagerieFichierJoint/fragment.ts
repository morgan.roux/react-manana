import gql from 'graphql-tag';

export const MESSAGERIE_FICHIER_JOINT_INFO_FRAGMENT = gql`
  fragment MessagerieFichierJointInfo on MessagerieFichierJoint {
    id
    fichier {
      chemin
      nomOriginal
      publicUrl
    }
  }
`;
