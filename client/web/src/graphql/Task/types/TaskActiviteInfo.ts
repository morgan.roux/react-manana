/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionActiviteType, ActionStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: TaskActiviteInfo
// ====================================================

export interface TaskActiviteInfo_action {
  __typename: "Action";
  id: string;
  description: string;
}

export interface TaskActiviteInfo_userSource_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  publicUrl: string | null;
}

export interface TaskActiviteInfo_userSource_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: TaskActiviteInfo_userSource_userPhoto_fichier | null;
}

export interface TaskActiviteInfo_userSource {
  __typename: "User";
  id: string;
  userName: string | null;
  userPhoto: TaskActiviteInfo_userSource_userPhoto | null;
}

export interface TaskActiviteInfo_userDestination {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface TaskActiviteInfo {
  __typename: "ActionActivite";
  id: string;
  activiteType: ActionActiviteType;
  action: TaskActiviteInfo_action;
  userSource: TaskActiviteInfo_userSource;
  userDestination: TaskActiviteInfo_userDestination | null;
  oldStatus: ActionStatus | null;
  newStatus: ActionStatus | null;
  oldDateDebut: any | null;
  newDateDebut: any | null;
  oldDateFin: any | null;
  newDateFin: any | null;
  dateCreation: any;
  dateModification: any;
}
