/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionActiviteType, ActionStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: TASK_ACTIVITES
// ====================================================

export interface TASK_ACTIVITES_actionActivites_action {
  __typename: "Action";
  id: string;
  description: string;
}

export interface TASK_ACTIVITES_actionActivites_userSource_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  publicUrl: string | null;
}

export interface TASK_ACTIVITES_actionActivites_userSource_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: TASK_ACTIVITES_actionActivites_userSource_userPhoto_fichier | null;
}

export interface TASK_ACTIVITES_actionActivites_userSource {
  __typename: "User";
  id: string;
  userName: string | null;
  userPhoto: TASK_ACTIVITES_actionActivites_userSource_userPhoto | null;
}

export interface TASK_ACTIVITES_actionActivites_userDestination {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface TASK_ACTIVITES_actionActivites {
  __typename: "ActionActivite";
  id: string;
  activiteType: ActionActiviteType;
  action: TASK_ACTIVITES_actionActivites_action;
  userSource: TASK_ACTIVITES_actionActivites_userSource;
  userDestination: TASK_ACTIVITES_actionActivites_userDestination | null;
  oldStatus: ActionStatus | null;
  newStatus: ActionStatus | null;
  oldDateDebut: any | null;
  newDateDebut: any | null;
  oldDateFin: any | null;
  newDateFin: any | null;
  dateCreation: any;
  dateModification: any;
}

export interface TASK_ACTIVITES {
  actionActivites: TASK_ACTIVITES_actionActivites[];
}

export interface TASK_ACTIVITESVariables {
  idTask: string;
}
