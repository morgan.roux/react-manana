import gql from 'graphql-tag';
import { TASK_ACTIVITE_INFO_FRAGMENT } from './fragment';

export const GET_TASK_ACTIVITES = gql`
  query TASK_ACTIVITES($idTask: ID!) {
    actionActivites(idAction: $idTask) {
      ...TaskActiviteInfo
    }
  }
  ${TASK_ACTIVITE_INFO_FRAGMENT}
`;
