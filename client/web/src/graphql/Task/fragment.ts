import gql from 'graphql-tag';

export const TASK_ACTIVITE_INFO_FRAGMENT = gql`
  fragment TaskActiviteInfo on ActionActivite {
    id
    activiteType
    action {
      id
      description
    }
    userSource {
      id
      userName
      userPhoto {
        id
        fichier {
          id
          chemin
          publicUrl
        }
      }
    }
    userDestination {
      id
      userName
    }
    oldStatus
    newStatus
    oldDateDebut
    newDateDebut
    oldDateFin
    newDateFin
    dateCreation
    dateModification
  }
`;
