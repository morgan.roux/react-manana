import gql from 'graphql-tag';
import { MIN_CONTACT_INFO_FRAGMENT } from '../Contact/fragment';

export const GET_PERSONNEL_PHARMACY = gql`
  query PERSONNEL_PHARMACY($id: ID!) {
    ppersonnel(id: $id) {
      type
      id
      civilite
      nom
      prenom
      estAmbassadrice
      commentaire
      sortie
      dateSortie
      idGroupement
      idPharmacie
      pharmacie {
        id
        cip
        nom
        titulaires {
          id
          nom
          prenom
          users {
            id
            userName
            email
            login
          }
        }
      }
      user {
        id
        email
        login
        status
        jourNaissance
        moisNaissance
        anneeNaissance
        codeTraitements
        userPhoto {
          id
          fichier {
            id
            chemin
            nomOriginal
            publicUrl
          }
        }
      }
      role {
        id
        code
        nom
      }
      contact {
        ...MinContactInfo
      }
    }
  }
  ${MIN_CONTACT_INFO_FRAGMENT}
`;

export const GET_SEARCH_CUSTOM_CONTENT_PPHARMACIE = gql`
  query SEARCH_CUSTOM_CONTENT_PPHARMACIE($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Ppersonnel {
          id
          civilite
          fullName
          sortie
          pharmacie {
            id
            nom
            titulaires {
              id
              fullName
              users {
                id
                email
                login
              }
            }
          }
          estAmbassadrice
          user {
            id
            anneeNaissance
            status
            email
            login
            lastLoginDate
            role {
              id
              code
              nom
            }
            userPhoto {
              id
              fichier {
                id
                chemin
                nomOriginal
                publicUrl
              }
            }
          }
        }
      }
    }
  }
`;
