/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: SearchPersonnelPharmacieInfo
// ====================================================

export interface SearchPersonnelPharmacieInfo_pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface SearchPersonnelPharmacieInfo_pharmacie {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
  titulaires: (SearchPersonnelPharmacieInfo_pharmacie_titulaires | null)[] | null;
}

export interface SearchPersonnelPharmacieInfo__user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
}

export interface SearchPersonnelPharmacieInfo_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SearchPersonnelPharmacieInfo {
  __typename: "Ppersonnel";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estAmbassadrice: boolean | null;
  commentaire: string | null;
  sortie: number | null;
  dateSortie: any | null;
  idGroupement: string | null;
  idPharmacie: string | null;
  pharmacie: SearchPersonnelPharmacieInfo_pharmacie | null;
  _user: SearchPersonnelPharmacieInfo__user | null;
  role: SearchPersonnelPharmacieInfo_role | null;
}
