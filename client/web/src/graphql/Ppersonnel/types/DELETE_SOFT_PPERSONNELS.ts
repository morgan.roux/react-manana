/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_SOFT_PPERSONNELS
// ====================================================

export interface DELETE_SOFT_PPERSONNELS_deleteSoftPpersonnels_pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface DELETE_SOFT_PPERSONNELS_deleteSoftPpersonnels_pharmacie {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
  titulaires: (DELETE_SOFT_PPERSONNELS_deleteSoftPpersonnels_pharmacie_titulaires | null)[] | null;
}

export interface DELETE_SOFT_PPERSONNELS_deleteSoftPpersonnels__user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
}

export interface DELETE_SOFT_PPERSONNELS_deleteSoftPpersonnels_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DELETE_SOFT_PPERSONNELS_deleteSoftPpersonnels {
  __typename: "Ppersonnel";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estAmbassadrice: boolean | null;
  commentaire: string | null;
  sortie: number | null;
  dateSortie: any | null;
  idGroupement: string | null;
  idPharmacie: string | null;
  pharmacie: DELETE_SOFT_PPERSONNELS_deleteSoftPpersonnels_pharmacie | null;
  _user: DELETE_SOFT_PPERSONNELS_deleteSoftPpersonnels__user | null;
  role: DELETE_SOFT_PPERSONNELS_deleteSoftPpersonnels_role | null;
}

export interface DELETE_SOFT_PPERSONNELS {
  deleteSoftPpersonnels: (DELETE_SOFT_PPERSONNELS_deleteSoftPpersonnels | null)[] | null;
}

export interface DELETE_SOFT_PPERSONNELSVariables {
  ids: string[];
}
