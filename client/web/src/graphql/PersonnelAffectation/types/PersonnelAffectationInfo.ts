/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PersonnelAffectationInfo
// ====================================================

export interface PersonnelAffectationInfo_personnelFonction {
  __typename: "PersonnelFonction";
  id: string;
  nom: string;
  code: string;
}

export interface PersonnelAffectationInfo_departement {
  __typename: "Departement";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PersonnelAffectationInfo_personnel {
  __typename: "Personnel";
  id: string;
  fullName: string | null;
}

export interface PersonnelAffectationInfo_personnelDemandeAffectation_personnelRemplacent {
  __typename: "Personnel";
  id: string;
  fullName: string | null;
}

export interface PersonnelAffectationInfo_personnelDemandeAffectation {
  __typename: "PersonnelDemandeAffectation";
  id: string;
  personnelRemplacent: PersonnelAffectationInfo_personnelDemandeAffectation_personnelRemplacent | null;
}

export interface PersonnelAffectationInfo {
  __typename: "PersonnelAffectation";
  id: string;
  personnelFonction: PersonnelAffectationInfo_personnelFonction | null;
  departement: PersonnelAffectationInfo_departement | null;
  personnel: PersonnelAffectationInfo_personnel | null;
  personnelDemandeAffectation: PersonnelAffectationInfo_personnelDemandeAffectation | null;
  dateDebut: any | null;
  dateFin: any | null;
}
