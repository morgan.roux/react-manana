/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_PersonnelAffectation_personnelFonction {
  __typename: "PersonnelFonction";
  id: string;
  nom: string;
  code: string;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_PersonnelAffectation_departement {
  __typename: "Departement";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_PersonnelAffectation_personnel {
  __typename: "Personnel";
  id: string;
  fullName: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_PersonnelAffectation_personnelDemandeAffectation_personnelRemplacent {
  __typename: "Personnel";
  id: string;
  fullName: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_PersonnelAffectation_personnelDemandeAffectation {
  __typename: "PersonnelDemandeAffectation";
  id: string;
  personnelRemplacent: SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_PersonnelAffectation_personnelDemandeAffectation_personnelRemplacent | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_PersonnelAffectation {
  __typename: "PersonnelAffectation";
  id: string;
  personnelFonction: SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_PersonnelAffectation_personnelFonction | null;
  departement: SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_PersonnelAffectation_departement | null;
  personnel: SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_PersonnelAffectation_personnel | null;
  personnelDemandeAffectation: SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_PersonnelAffectation_personnelDemandeAffectation | null;
  dateDebut: any | null;
  dateFin: any | null;
}

export type SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data = SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_Action | SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data_PersonnelAffectation;

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION {
  search: SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATION_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_PERSONNEL_AFFECTATIONVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
