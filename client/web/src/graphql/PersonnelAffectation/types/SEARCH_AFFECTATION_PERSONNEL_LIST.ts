/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_AFFECTATION_PERSONNEL_LIST
// ====================================================

export interface SEARCH_AFFECTATION_PERSONNEL_LIST_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_AFFECTATION_PERSONNEL_LIST_search_data_Personnel {
  __typename: "Personnel";
  id: string;
  fullName: string | null;
}

export type SEARCH_AFFECTATION_PERSONNEL_LIST_search_data = SEARCH_AFFECTATION_PERSONNEL_LIST_search_data_Action | SEARCH_AFFECTATION_PERSONNEL_LIST_search_data_Personnel;

export interface SEARCH_AFFECTATION_PERSONNEL_LIST_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_AFFECTATION_PERSONNEL_LIST_search_data | null)[] | null;
}

export interface SEARCH_AFFECTATION_PERSONNEL_LIST {
  search: SEARCH_AFFECTATION_PERSONNEL_LIST_search | null;
}

export interface SEARCH_AFFECTATION_PERSONNEL_LISTVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
