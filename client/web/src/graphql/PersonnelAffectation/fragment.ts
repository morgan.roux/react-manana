import gql from 'graphql-tag';
import { PERSONNEL_FONCTION_INFO_FRAGMENT } from '../PersonnelFonction/fragment';
import { DEPARTEMENT_INFO_FRAGMENT } from '../Departement/fragment';

export const PERSONNEL_AFFECTATION_INFO_FRAGMENT = gql`
  fragment PersonnelAffectationInfo on PersonnelAffectation {
    id
    personnelFonction {
      ...PersonnelFonctionInfo
    }
    departement {
      ...DepartementInfo
    }
    personnel {
      id
      fullName
    }
    personnelDemandeAffectation {
      id
      personnelRemplacent {
        id
        fullName
      }
    }
    dateDebut
    dateFin
  }
  ${PERSONNEL_FONCTION_INFO_FRAGMENT}
  ${DEPARTEMENT_INFO_FRAGMENT}
`;
