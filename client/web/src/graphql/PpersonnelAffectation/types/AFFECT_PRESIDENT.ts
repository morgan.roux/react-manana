/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AffectationInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: AFFECT_PRESIDENT
// ====================================================

export interface AFFECT_PRESIDENT_createUpdateTitulaireAffectation_titulaireFonction {
  __typename: "TitulaireFonction";
  id: string;
  nom: string;
  code: string;
}

export interface AFFECT_PRESIDENT_createUpdateTitulaireAffectation_departement {
  __typename: "Departement";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface AFFECT_PRESIDENT_createUpdateTitulaireAffectation_titulaire {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
}

export interface AFFECT_PRESIDENT_createUpdateTitulaireAffectation_titulaireDemandeAffectation_titulaireRemplacent {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
}

export interface AFFECT_PRESIDENT_createUpdateTitulaireAffectation_titulaireDemandeAffectation {
  __typename: "TitulaireDemandeAffectation";
  id: string;
  titulaireRemplacent: AFFECT_PRESIDENT_createUpdateTitulaireAffectation_titulaireDemandeAffectation_titulaireRemplacent | null;
}

export interface AFFECT_PRESIDENT_createUpdateTitulaireAffectation {
  __typename: "TitulaireAffectation";
  id: string;
  titulaireFonction: AFFECT_PRESIDENT_createUpdateTitulaireAffectation_titulaireFonction | null;
  departement: AFFECT_PRESIDENT_createUpdateTitulaireAffectation_departement | null;
  titulaire: AFFECT_PRESIDENT_createUpdateTitulaireAffectation_titulaire | null;
  titulaireDemandeAffectation: AFFECT_PRESIDENT_createUpdateTitulaireAffectation_titulaireDemandeAffectation | null;
  dateDebut: any | null;
  dateFin: any | null;
}

export interface AFFECT_PRESIDENT {
  createUpdateTitulaireAffectation: (AFFECT_PRESIDENT_createUpdateTitulaireAffectation | null)[] | null;
}

export interface AFFECT_PRESIDENTVariables {
  inputs?: AffectationInput | null;
}
