/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PresidentAffectationInfo
// ====================================================

export interface PresidentAffectationInfo_titulaireFonction {
  __typename: "TitulaireFonction";
  id: string;
  nom: string;
  code: string;
}

export interface PresidentAffectationInfo_departement {
  __typename: "Departement";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PresidentAffectationInfo_titulaire {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
}

export interface PresidentAffectationInfo_titulaireDemandeAffectation_titulaireRemplacent {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
}

export interface PresidentAffectationInfo_titulaireDemandeAffectation {
  __typename: "TitulaireDemandeAffectation";
  id: string;
  titulaireRemplacent: PresidentAffectationInfo_titulaireDemandeAffectation_titulaireRemplacent | null;
}

export interface PresidentAffectationInfo {
  __typename: "TitulaireAffectation";
  id: string;
  titulaireFonction: PresidentAffectationInfo_titulaireFonction | null;
  departement: PresidentAffectationInfo_departement | null;
  titulaire: PresidentAffectationInfo_titulaire | null;
  titulaireDemandeAffectation: PresidentAffectationInfo_titulaireDemandeAffectation | null;
  dateDebut: any | null;
  dateFin: any | null;
}
