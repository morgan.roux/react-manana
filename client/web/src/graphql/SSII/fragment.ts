import gql from 'graphql-tag';

export const SSII_INFO_FRAGMENT = gql`
  fragment SSIIInfo on SSII {
    id
    nom
    adresse1
    adresse2
    cp
    ville
    telephone1
    telephone2
    fax
    mail
    web
    commentaire
  }
`;
