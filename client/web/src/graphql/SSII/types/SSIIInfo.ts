/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SSIIInfo
// ====================================================

export interface SSIIInfo {
  __typename: "SSII";
  id: string;
  nom: string;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  mail: string | null;
  web: string | null;
  commentaire: string | null;
}
