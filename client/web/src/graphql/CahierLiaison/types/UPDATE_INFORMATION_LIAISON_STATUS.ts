/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_INFORMATION_LIAISON_STATUS
// ====================================================

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_item {
  __typename: "Item";
  id: string;
  name: string | null;
  code: string | null;
  codeItem: string | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  type: string;
  nomLabo: string | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie_Service {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie_GroupeAmis {
  __typename: "GroupeAmis";
  type: string;
  id: string;
  nom: string | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie_User {
  __typename: "User";
  type: string;
  id: string;
  userName: string | null;
}

export type UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie = UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie_Action | UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie_Laboratoire | UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie_Service | UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie_Partenaire | UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie_GroupeAmis | UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie_User;

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_declarant_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_declarant_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_declarant_userPhoto_fichier | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_declarant {
  __typename: "User";
  id: string;
  userName: string | null;
  userPhoto: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_declarant_userPhoto | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_informationLiaison {
  __typename: "InformationLiaison";
  id: string;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_userConcernee_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_userConcernee_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_userConcernee_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_userConcernee_userPhoto_fichier | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_userConcernee_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_userConcernee {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_userConcernee_role | null;
  userPhoto: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_userConcernee_userPhoto | null;
  pharmacie: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_userConcernee_pharmacie | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_prisEnCharge_fichiersJoints_fichier;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_prisEnCharge_fichiersJoints[] | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees {
  __typename: "InformationLiaisonUserConcernee";
  id: string;
  statut: string;
  dateCreation: any;
  dateStatutModification: any | null;
  dateModification: any;
  informationLiaison: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_informationLiaison;
  userConcernee: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_userConcernee;
  prisEnCharge: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees_prisEnCharge | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_ficheReclamation {
  __typename: "Ticket";
  id: string;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_todoAction {
  __typename: "Action";
  id: string;
  description: string;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_groupement {
  __typename: "Groupement";
  id: string;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_userCreation {
  __typename: "User";
  id: string;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_userModification {
  __typename: "User";
  id: string;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_prisEnCharge_fichiersJoints_fichier;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_prisEnCharge_fichiersJoints[] | null;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_fichiersJoints {
  __typename: "InformationLiaisonFichierJoint";
  id: string;
  fichier: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_fichiersJoints_fichier;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut {
  __typename: "InformationLiaison";
  id: string;
  type: string;
  idItem: string | null;
  item: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_item | null;
  idItemAssocie: string | null;
  idType: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_origineAssocie | null;
  statut: string;
  priority: string | null;
  bloquant: boolean | null;
  titre: string;
  description: string;
  dateCreation: any;
  dateModification: any;
  nbCollegue: number | null;
  nbComment: number | null;
  nbLue: number | null;
  declarant: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_declarant | null;
  idFonction: string | null;
  idTache: string | null;
  importance: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_importance | null;
  urgence: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_urgence | null;
  colleguesConcernees: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_colleguesConcernees[] | null;
  idFicheIncident: string | null;
  idFicheAmelioration: string | null;
  ficheReclamation: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_ficheReclamation | null;
  todoAction: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_todoAction | null;
  groupement: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_userCreation | null;
  userModification: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_userModification | null;
  prisEnCharge: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_prisEnCharge | null;
  fichiersJoints: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_fichiersJoints[] | null;
  userSmyleys: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut_userSmyleys | null;
  nbPartage: number;
  nbRecommandation: number;
  isShared: boolean;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS {
  updateInformationLiaisonStatut: UPDATE_INFORMATION_LIAISON_STATUS_updateInformationLiaisonStatut;
}

export interface UPDATE_INFORMATION_LIAISON_STATUSVariables {
  id: string;
  statut: string;
}
