/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: InformationLiaisonInfo
// ====================================================

export interface InformationLiaisonInfo_item {
  __typename: "Item";
  id: string;
  name: string | null;
  code: string | null;
  codeItem: string | null;
}

export interface InformationLiaisonInfo_origineAssocie_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface InformationLiaisonInfo_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  type: string;
  nomLabo: string | null;
}

export interface InformationLiaisonInfo_origineAssocie_Service {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface InformationLiaisonInfo_origineAssocie_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
}

export interface InformationLiaisonInfo_origineAssocie_GroupeAmis {
  __typename: "GroupeAmis";
  type: string;
  id: string;
  nom: string | null;
}

export interface InformationLiaisonInfo_origineAssocie_User {
  __typename: "User";
  type: string;
  id: string;
  userName: string | null;
}

export type InformationLiaisonInfo_origineAssocie = InformationLiaisonInfo_origineAssocie_Action | InformationLiaisonInfo_origineAssocie_Laboratoire | InformationLiaisonInfo_origineAssocie_Service | InformationLiaisonInfo_origineAssocie_Partenaire | InformationLiaisonInfo_origineAssocie_GroupeAmis | InformationLiaisonInfo_origineAssocie_User;

export interface InformationLiaisonInfo_declarant_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface InformationLiaisonInfo_declarant_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: InformationLiaisonInfo_declarant_userPhoto_fichier | null;
}

export interface InformationLiaisonInfo_declarant {
  __typename: "User";
  id: string;
  userName: string | null;
  userPhoto: InformationLiaisonInfo_declarant_userPhoto | null;
}

export interface InformationLiaisonInfo_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface InformationLiaisonInfo_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface InformationLiaisonInfo_colleguesConcernees_informationLiaison {
  __typename: "InformationLiaison";
  id: string;
}

export interface InformationLiaisonInfo_colleguesConcernees_userConcernee_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface InformationLiaisonInfo_colleguesConcernees_userConcernee_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface InformationLiaisonInfo_colleguesConcernees_userConcernee_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: InformationLiaisonInfo_colleguesConcernees_userConcernee_userPhoto_fichier | null;
}

export interface InformationLiaisonInfo_colleguesConcernees_userConcernee_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface InformationLiaisonInfo_colleguesConcernees_userConcernee {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: InformationLiaisonInfo_colleguesConcernees_userConcernee_role | null;
  userPhoto: InformationLiaisonInfo_colleguesConcernees_userConcernee_userPhoto | null;
  pharmacie: InformationLiaisonInfo_colleguesConcernees_userConcernee_pharmacie | null;
}

export interface InformationLiaisonInfo_colleguesConcernees_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string | null;
}

export interface InformationLiaisonInfo_colleguesConcernees_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: InformationLiaisonInfo_colleguesConcernees_prisEnCharge_fichiersJoints_fichier;
}

export interface InformationLiaisonInfo_colleguesConcernees_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: InformationLiaisonInfo_colleguesConcernees_prisEnCharge_fichiersJoints[] | null;
}

export interface InformationLiaisonInfo_colleguesConcernees {
  __typename: "InformationLiaisonUserConcernee";
  id: string;
  statut: string;
  dateCreation: any;
  dateStatutModification: any | null;
  dateModification: any;
  informationLiaison: InformationLiaisonInfo_colleguesConcernees_informationLiaison;
  userConcernee: InformationLiaisonInfo_colleguesConcernees_userConcernee;
  prisEnCharge: InformationLiaisonInfo_colleguesConcernees_prisEnCharge | null;
}

export interface InformationLiaisonInfo_ficheReclamation {
  __typename: "Ticket";
  id: string;
}

export interface InformationLiaisonInfo_todoAction {
  __typename: "Action";
  id: string;
  description: string;
}

export interface InformationLiaisonInfo_groupement {
  __typename: "Groupement";
  id: string;
}

export interface InformationLiaisonInfo_userCreation {
  __typename: "User";
  id: string;
}

export interface InformationLiaisonInfo_userModification {
  __typename: "User";
  id: string;
}

export interface InformationLiaisonInfo_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface InformationLiaisonInfo_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: InformationLiaisonInfo_prisEnCharge_fichiersJoints_fichier;
}

export interface InformationLiaisonInfo_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: InformationLiaisonInfo_prisEnCharge_fichiersJoints[] | null;
}

export interface InformationLiaisonInfo_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface InformationLiaisonInfo_fichiersJoints {
  __typename: "InformationLiaisonFichierJoint";
  id: string;
  fichier: InformationLiaisonInfo_fichiersJoints_fichier;
}

export interface InformationLiaisonInfo_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
}

export interface InformationLiaisonInfo {
  __typename: "InformationLiaison";
  id: string;
  type: string;
  idItem: string | null;
  item: InformationLiaisonInfo_item | null;
  idItemAssocie: string | null;
  idType: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: InformationLiaisonInfo_origineAssocie | null;
  statut: string;
  priority: string | null;
  bloquant: boolean | null;
  titre: string;
  description: string;
  dateCreation: any;
  dateModification: any;
  nbCollegue: number | null;
  nbComment: number | null;
  nbLue: number | null;
  declarant: InformationLiaisonInfo_declarant | null;
  idFonction: string | null;
  idTache: string | null;
  importance: InformationLiaisonInfo_importance | null;
  urgence: InformationLiaisonInfo_urgence | null;
  colleguesConcernees: InformationLiaisonInfo_colleguesConcernees[] | null;
  idFicheIncident: string | null;
  idFicheAmelioration: string | null;
  ficheReclamation: InformationLiaisonInfo_ficheReclamation | null;
  todoAction: InformationLiaisonInfo_todoAction | null;
  groupement: InformationLiaisonInfo_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: InformationLiaisonInfo_userCreation | null;
  userModification: InformationLiaisonInfo_userModification | null;
  prisEnCharge: InformationLiaisonInfo_prisEnCharge | null;
  fichiersJoints: InformationLiaisonInfo_fichiersJoints[] | null;
  userSmyleys: InformationLiaisonInfo_userSmyleys | null;
  nbPartage: number;
  nbRecommandation: number;
  isShared: boolean;
}
