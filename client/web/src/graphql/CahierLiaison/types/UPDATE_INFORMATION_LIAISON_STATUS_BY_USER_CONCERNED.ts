/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED
// ====================================================

export interface UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED_updateInformationLiaisonStatutByUserConcerned {
  __typename: "InformationLiaisonUserConcernee";
  id: string;
  statut: string;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED {
  updateInformationLiaisonStatutByUserConcerned: UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNED_updateInformationLiaisonStatutByUserConcerned;
}

export interface UPDATE_INFORMATION_LIAISON_STATUS_BY_USER_CONCERNEDVariables {
  id: string;
  statut: string;
}
