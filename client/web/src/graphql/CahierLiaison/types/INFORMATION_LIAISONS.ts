/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: INFORMATION_LIAISONS
// ====================================================

export interface INFORMATION_LIAISONS_informationLiaisons_item {
  __typename: "Item";
  id: string;
  name: string | null;
  code: string | null;
  codeItem: string | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_origineAssocie_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface INFORMATION_LIAISONS_informationLiaisons_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  type: string;
  nomLabo: string | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_origineAssocie_Service {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_origineAssocie_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_origineAssocie_GroupeAmis {
  __typename: "GroupeAmis";
  type: string;
  id: string;
  nom: string | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_origineAssocie_User {
  __typename: "User";
  type: string;
  id: string;
  userName: string | null;
}

export type INFORMATION_LIAISONS_informationLiaisons_origineAssocie = INFORMATION_LIAISONS_informationLiaisons_origineAssocie_Action | INFORMATION_LIAISONS_informationLiaisons_origineAssocie_Laboratoire | INFORMATION_LIAISONS_informationLiaisons_origineAssocie_Service | INFORMATION_LIAISONS_informationLiaisons_origineAssocie_Partenaire | INFORMATION_LIAISONS_informationLiaisons_origineAssocie_GroupeAmis | INFORMATION_LIAISONS_informationLiaisons_origineAssocie_User;

export interface INFORMATION_LIAISONS_informationLiaisons_declarant_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_declarant_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: INFORMATION_LIAISONS_informationLiaisons_declarant_userPhoto_fichier | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_declarant {
  __typename: "User";
  id: string;
  userName: string | null;
  userPhoto: INFORMATION_LIAISONS_informationLiaisons_declarant_userPhoto | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_informationLiaison {
  __typename: "InformationLiaison";
  id: string;
}

export interface INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_userConcernee_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_userConcernee_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_userConcernee_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_userConcernee_userPhoto_fichier | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_userConcernee_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_userConcernee {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_userConcernee_role | null;
  userPhoto: INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_userConcernee_userPhoto | null;
  pharmacie: INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_userConcernee_pharmacie | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_prisEnCharge_fichiersJoints_fichier;
}

export interface INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_prisEnCharge_fichiersJoints[] | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees {
  __typename: "InformationLiaisonUserConcernee";
  id: string;
  statut: string;
  dateCreation: any;
  dateStatutModification: any | null;
  dateModification: any;
  informationLiaison: INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_informationLiaison;
  userConcernee: INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_userConcernee;
  prisEnCharge: INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees_prisEnCharge | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_ficheReclamation {
  __typename: "Ticket";
  id: string;
}

export interface INFORMATION_LIAISONS_informationLiaisons_todoAction {
  __typename: "Action";
  id: string;
  description: string;
}

export interface INFORMATION_LIAISONS_informationLiaisons_groupement {
  __typename: "Groupement";
  id: string;
}

export interface INFORMATION_LIAISONS_informationLiaisons_userCreation {
  __typename: "User";
  id: string;
}

export interface INFORMATION_LIAISONS_informationLiaisons_userModification {
  __typename: "User";
  id: string;
}

export interface INFORMATION_LIAISONS_informationLiaisons_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface INFORMATION_LIAISONS_informationLiaisons_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: INFORMATION_LIAISONS_informationLiaisons_prisEnCharge_fichiersJoints_fichier;
}

export interface INFORMATION_LIAISONS_informationLiaisons_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: INFORMATION_LIAISONS_informationLiaisons_prisEnCharge_fichiersJoints[] | null;
}

export interface INFORMATION_LIAISONS_informationLiaisons_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface INFORMATION_LIAISONS_informationLiaisons_fichiersJoints {
  __typename: "InformationLiaisonFichierJoint";
  id: string;
  fichier: INFORMATION_LIAISONS_informationLiaisons_fichiersJoints_fichier;
}

export interface INFORMATION_LIAISONS_informationLiaisons_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
}

export interface INFORMATION_LIAISONS_informationLiaisons {
  __typename: "InformationLiaison";
  id: string;
  type: string;
  idItem: string | null;
  item: INFORMATION_LIAISONS_informationLiaisons_item | null;
  idItemAssocie: string | null;
  idType: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: INFORMATION_LIAISONS_informationLiaisons_origineAssocie | null;
  statut: string;
  priority: string | null;
  bloquant: boolean | null;
  titre: string;
  description: string;
  dateCreation: any;
  dateModification: any;
  nbCollegue: number | null;
  nbComment: number | null;
  nbLue: number | null;
  declarant: INFORMATION_LIAISONS_informationLiaisons_declarant | null;
  idFonction: string | null;
  idTache: string | null;
  importance: INFORMATION_LIAISONS_informationLiaisons_importance | null;
  urgence: INFORMATION_LIAISONS_informationLiaisons_urgence | null;
  colleguesConcernees: INFORMATION_LIAISONS_informationLiaisons_colleguesConcernees[] | null;
  idFicheIncident: string | null;
  idFicheAmelioration: string | null;
  ficheReclamation: INFORMATION_LIAISONS_informationLiaisons_ficheReclamation | null;
  todoAction: INFORMATION_LIAISONS_informationLiaisons_todoAction | null;
  groupement: INFORMATION_LIAISONS_informationLiaisons_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: INFORMATION_LIAISONS_informationLiaisons_userCreation | null;
  userModification: INFORMATION_LIAISONS_informationLiaisons_userModification | null;
  prisEnCharge: INFORMATION_LIAISONS_informationLiaisons_prisEnCharge | null;
  fichiersJoints: INFORMATION_LIAISONS_informationLiaisons_fichiersJoints[] | null;
  userSmyleys: INFORMATION_LIAISONS_informationLiaisons_userSmyleys | null;
  nbPartage: number;
  nbRecommandation: number;
  isShared: boolean;
}

export interface INFORMATION_LIAISONS {
  informationLiaisons: INFORMATION_LIAISONS_informationLiaisons[];
}

export interface INFORMATION_LIAISONSVariables {
  isRemoved?: boolean | null;
}
