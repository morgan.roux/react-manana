/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_SOFT_INFORMATION_LIAISONS
// ====================================================

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_item {
  __typename: "Item";
  id: string;
  name: string | null;
  code: string | null;
  codeItem: string | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  type: string;
  nomLabo: string | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie_Service {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie_GroupeAmis {
  __typename: "GroupeAmis";
  type: string;
  id: string;
  nom: string | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie_User {
  __typename: "User";
  type: string;
  id: string;
  userName: string | null;
}

export type DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie = DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie_Action | DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie_Laboratoire | DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie_Service | DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie_Partenaire | DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie_GroupeAmis | DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie_User;

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_declarant_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_declarant_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_declarant_userPhoto_fichier | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_declarant {
  __typename: "User";
  id: string;
  userName: string | null;
  userPhoto: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_declarant_userPhoto | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_informationLiaison {
  __typename: "InformationLiaison";
  id: string;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_userConcernee_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_userConcernee_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_userConcernee_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_userConcernee_userPhoto_fichier | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_userConcernee_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_userConcernee {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_userConcernee_role | null;
  userPhoto: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_userConcernee_userPhoto | null;
  pharmacie: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_userConcernee_pharmacie | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  publicUrl: string | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_prisEnCharge_fichiersJoints_fichier;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_prisEnCharge_fichiersJoints[] | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees {
  __typename: "InformationLiaisonUserConcernee";
  id: string;
  statut: string;
  dateCreation: any;
  dateStatutModification: any | null;
  dateModification: any;
  informationLiaison: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_informationLiaison;
  userConcernee: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_userConcernee;
  prisEnCharge: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees_prisEnCharge | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_ficheReclamation {
  __typename: "Ticket";
  id: string;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_todoAction {
  __typename: "Action";
  id: string;
  description: string;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_groupement {
  __typename: "Groupement";
  id: string;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_userCreation {
  __typename: "User";
  id: string;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_userModification {
  __typename: "User";
  id: string;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_prisEnCharge_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_prisEnCharge_fichiersJoints {
  __typename: "InformationLiaisonPrisChargeFichierJoint";
  id: string;
  fichier: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_prisEnCharge_fichiersJoints_fichier;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_prisEnCharge {
  __typename: "InformationLiaisonPrisCharge";
  id: string;
  date: any;
  description: string;
  fichiersJoints: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_prisEnCharge_fichiersJoints[] | null;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_fichiersJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_fichiersJoints {
  __typename: "InformationLiaisonFichierJoint";
  id: string;
  fichier: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_fichiersJoints_fichier;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons {
  __typename: "InformationLiaison";
  id: string;
  type: string;
  idItem: string | null;
  item: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_item | null;
  idItemAssocie: string | null;
  idType: string | null;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_origineAssocie | null;
  statut: string;
  priority: string | null;
  bloquant: boolean | null;
  titre: string;
  description: string;
  dateCreation: any;
  dateModification: any;
  nbCollegue: number | null;
  nbComment: number | null;
  nbLue: number | null;
  declarant: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_declarant | null;
  idFonction: string | null;
  idTache: string | null;
  importance: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_importance | null;
  urgence: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_urgence | null;
  colleguesConcernees: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_colleguesConcernees[] | null;
  idFicheIncident: string | null;
  idFicheAmelioration: string | null;
  ficheReclamation: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_ficheReclamation | null;
  todoAction: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_todoAction | null;
  groupement: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_userCreation | null;
  userModification: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_userModification | null;
  prisEnCharge: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_prisEnCharge | null;
  fichiersJoints: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_fichiersJoints[] | null;
  userSmyleys: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons_userSmyleys | null;
  nbPartage: number;
  nbRecommandation: number;
  isShared: boolean;
}

export interface DELETE_SOFT_INFORMATION_LIAISONS {
  softDeleteInformationLiaisons: DELETE_SOFT_INFORMATION_LIAISONS_softDeleteInformationLiaisons[];
}

export interface DELETE_SOFT_INFORMATION_LIAISONSVariables {
  ids: string[];
}
