import gql from 'graphql-tag';
import { INFORMATION_LIAISON_INFO_FRAGEMENT } from './fragment';

export const GET_INFORMATION_LIAISONS = gql`
  query INFORMATION_LIAISONS($isRemoved: Boolean) {
    informationLiaisons(isRemoved: $isRemoved) {
      ...InformationLiaisonInfo
    }
  }
  ${INFORMATION_LIAISON_INFO_FRAGEMENT}
`;

export const GET_INFORMATION_LIAISON = gql`
  query INFORMATION_LIAISON($id: ID!) {
    informationLiaison(id: $id) {
      ...InformationLiaisonInfo
    }
  }
  ${INFORMATION_LIAISON_INFO_FRAGEMENT}
`;

export const GET_SEARCH_CUSTOM_INFORMATION_LIAISON = gql`
  query SEARCH_CUSTOM_CONTENT_INFORMATION_LIAISON(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on InformationLiaison {
          ...InformationLiaisonInfo
        }
      }
    }
  }
  ${INFORMATION_LIAISON_INFO_FRAGEMENT}
`;
