/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_PARTENAIRES
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Partenaire_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Partenaire_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Partenaire_user_userPhoto_fichier | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Partenaire_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  codeTraitements: (string | null)[] | null;
  userPhoto: SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Partenaire_user_userPhoto | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Partenaire_role {
  __typename: "Role";
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Partenaire_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Partenaire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
  commentaire: string | null;
  idGroupement: string | null;
  user: SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Partenaire_user | null;
  role: SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Partenaire_role | null;
  contact: SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Partenaire_contact | null;
}

export type SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data = SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Action | SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data_Partenaire;

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRES_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_PARTENAIRES_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRES {
  search: SEARCH_CUSTOM_CONTENT_PARTENAIRES_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRESVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
