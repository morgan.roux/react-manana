/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: SearchPartenaireInfo
// ====================================================

export interface SearchPartenaireInfo__user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
}

export interface SearchPartenaireInfo_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SearchPartenaireInfo {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  commentaire: string | null;
  idGroupement: string | null;
  _user: SearchPartenaireInfo__user | null;
  role: SearchPartenaireInfo_role | null;
}
