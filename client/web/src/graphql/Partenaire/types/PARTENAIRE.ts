/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PARTENAIRE
// ====================================================

export interface PARTENAIRE_partenaire_partenaireServiceSuite {
  __typename: "PartenaireServiceSuite";
  id: string;
  adresse1: string | null;
  adresse2: string | null;
}

export interface PARTENAIRE_partenaire_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface PARTENAIRE_partenaire_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: PARTENAIRE_partenaire_user_userPhoto_fichier | null;
}

export interface PARTENAIRE_partenaire_user {
  __typename: "User";
  id: string;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  codeTraitements: (string | null)[] | null;
  userPhoto: PARTENAIRE_partenaire_user_userPhoto | null;
}

export interface PARTENAIRE_partenaire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
  partenaireServiceSuite: PARTENAIRE_partenaire_partenaireServiceSuite | null;
  user: PARTENAIRE_partenaire_user | null;
}

export interface PARTENAIRE {
  partenaire: PARTENAIRE_partenaire | null;
}

export interface PARTENAIREVariables {
  id: string;
}
