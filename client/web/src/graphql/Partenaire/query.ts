import gql from 'graphql-tag';

export const GET_PARTENAIRE = gql`
  query PARTENAIRE($id: ID!) {
    partenaire(id: $id) {
      id
      nom
      partenaireServiceSuite {
        id
        adresse1
        adresse2
      }
      user {
        id
        jourNaissance
        moisNaissance
        anneeNaissance
        email
        login
        status
        codeTraitements
        userPhoto {
          id
          fichier {
            id
            chemin
            nomOriginal
            publicUrl
          }
        }
      }
    }
  }
`;

export const GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE = gql`
  query SEARCH_CUSTOM_CONTENT_PARTENAIRES($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Partenaire {
          id
          nom
          commentaire
          idGroupement
          user {
            id
            email
            login
            status
            codeTraitements
            userPhoto {
              id
              fichier {
                id
                chemin
                nomOriginal
                publicUrl
              }
            }
          }
          role {
            code
            nom
          }
          contact {
            id
            cp
            ville
            pays
            adresse1
            adresse2
            faxProf
            faxPerso
            telProf
            telMobProf
            telPerso
            telMobPerso
            mailProf
            mailPerso
            siteProf
            sitePerso
          }
        }
      }
    }
  }
`;
