import gql from 'graphql-tag';

export const DO_CREATE_UPDATE_TICKET = gql`
  mutation CREATE_UPDATE_TICKET($input: TicketInput!) {
    createUpdateTicket(input: $input) {
      id
      commentaire
      visibilite
      typeTicket
    }
  }
`;
export const DO_UPDATE_TICKET_STATUT = gql`
  mutation UPDATE_TICKET_STATUT($id: ID!, $status: StatusTicket!) {
    updateTicketStatut(id: $id, status: $status) {
      id
      statusTicket
    }
  }
`;

export const DO_UPDATE_TICKET_CIBLE = gql`
  mutation UPDATE_TICKET_CIBLE($input: TicketCibleInput!) {
    updateTicketCible(input: $input) {
      id
      dateHeureAffectation
      commentaire
    }
  }
`;
