/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TicketVisibilite, TicketAppel, OrigineType, StatusTicket } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_TICKET
// ====================================================

export interface SEARCH_TICKET_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_TICKET_search_data_Ticket_declarant_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_TICKET_search_data_Ticket_declarant_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_TICKET_search_data_Ticket_declarant {
  __typename: "User";
  id: string;
  userName: string | null;
  role: SEARCH_TICKET_search_data_Ticket_declarant_role | null;
  pharmacie: SEARCH_TICKET_search_data_Ticket_declarant_pharmacie | null;
}

export interface SEARCH_TICKET_search_data_Ticket_usersConcernees_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_TICKET_search_data_Ticket_usersConcernees_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_TICKET_search_data_Ticket_usersConcernees {
  __typename: "User";
  id: string;
  userName: string | null;
  role: SEARCH_TICKET_search_data_Ticket_usersConcernees_role | null;
  pharmacie: SEARCH_TICKET_search_data_Ticket_usersConcernees_pharmacie | null;
}

export interface SEARCH_TICKET_search_data_Ticket_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  photo: string;
}

export interface SEARCH_TICKET_search_data_Ticket_motif {
  __typename: "TicketMotif";
  id: string;
  nom: string;
}

export interface SEARCH_TICKET_search_data_Ticket_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_TICKET_search_data_Ticket_userCreation_userPersonnel_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface SEARCH_TICKET_search_data_Ticket_userCreation_userPersonnel {
  __typename: "Personnel";
  id: string;
  service: SEARCH_TICKET_search_data_Ticket_userCreation_userPersonnel_service | null;
}

export interface SEARCH_TICKET_search_data_Ticket_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface SEARCH_TICKET_search_data_Ticket_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  pharmacie: SEARCH_TICKET_search_data_Ticket_userCreation_pharmacie | null;
  userPersonnel: SEARCH_TICKET_search_data_Ticket_userCreation_userPersonnel | null;
  role: SEARCH_TICKET_search_data_Ticket_userCreation_role | null;
}

export interface SEARCH_TICKET_search_data_Ticket_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface SEARCH_TICKET_search_data_Ticket {
  __typename: "Ticket";
  type: string;
  id: string;
  idOrganisation: string | null;
  nomOrganisation: string | null;
  dateHeureSaisie: any | null;
  commentaire: string | null;
  visibilite: TicketVisibilite | null;
  priority: number | null;
  typeAppel: TicketAppel | null;
  declarant: SEARCH_TICKET_search_data_Ticket_declarant | null;
  usersConcernees: (SEARCH_TICKET_search_data_Ticket_usersConcernees | null)[] | null;
  smyley: SEARCH_TICKET_search_data_Ticket_smyley | null;
  motif: SEARCH_TICKET_search_data_Ticket_motif | null;
  userCreation: SEARCH_TICKET_search_data_Ticket_userCreation | null;
  fichiers: (SEARCH_TICKET_search_data_Ticket_fichiers | null)[] | null;
  origineType: OrigineType | null;
  statusTicket: StatusTicket | null;
}

export type SEARCH_TICKET_search_data = SEARCH_TICKET_search_data_Action | SEARCH_TICKET_search_data_Ticket;

export interface SEARCH_TICKET_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_TICKET_search_data | null)[] | null;
}

export interface SEARCH_TICKET {
  search: SEARCH_TICKET_search | null;
}

export interface SEARCH_TICKETVariables {
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
