import gql from 'graphql-tag';

export const TICKET_MOTIF_INFO_FRAGMENT = gql`
  fragment TicketMotifInfo on TicketMotif {
    id
    nom
  }
`;

export const TICKET_INFO_FRAGMENT = gql`
  fragment TicketInfo on Ticket {
    type
    id
    idOrganisation
    nomOrganisation
    dateHeureSaisie
    commentaire
    type
    visibilite
    priority
    typeAppel
    declarant {
      id
      userName
      role {
        id
        code
        nom
      }
      pharmacie {
        id
        nom
      }
    }
    usersConcernees {
      id
      userName
      role {
        id
        code
        nom
      }
      pharmacie {
        id
        nom
      }
    }
    smyley {
      id
      nom
      photo
    }
    motif {
      ...TicketMotifInfo
    }
    userCreation {
      id
      userName
      pharmacie {
        id
        nom
      }
      userPersonnel {
        id
        service {
          id
          nom
        }
      }
      role {
        id
        nom
      }
    }
    fichiers {
      id
      chemin
      nomOriginal
      type
      urlPresigned
      publicUrl
    }
    origineType
    statusTicket
  }
  ${TICKET_MOTIF_INFO_FRAGMENT}
`;
