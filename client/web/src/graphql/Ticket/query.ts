import gql from 'graphql-tag';
import { TICKET_INFO_FRAGMENT } from './fragment';

export const GET_SEARCH_CUSTOM_CONTENT_TICKET = gql`
  query SEARCH_CUSTOM_CONTENT_TICKET($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Ticket {
          ...TicketInfo
        }
      }
    }
  }
  ${TICKET_INFO_FRAGMENT}
`;

export const GET_TICKET = gql`
  query TICKET($id: ID!) {
    ticket(id: $id) {
      ...TicketInfo
    }
  }
  ${TICKET_INFO_FRAGMENT}
`;

export const GET_SEARCH_TICKET_CHANGEMENT_CIBLE = gql`
  query SEARCH_TICKET_CHANGEMENT_CIBLE(
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(
      type: "ticketchangementcible"
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on TicketChangementCible {
          id
          dateHeureAffectation
          commentaire
          userCible {
            id
            userName
            pharmacie {
              id
              nom
            }
            userPersonnel {
              id
              service {
                id
                nom
              }
            }
            role {
              id
              nom
            }
          }
        }
      }
    }
  }
`;

export const GET_SEARCH_TICKET = gql`
  query SEARCH_TICKET($query: JSON, $skip: Int, $take: Int, $filterBy: JSON, $sortBy: JSON) {
    search(
      type: "ticket"
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Ticket {
          ...TicketInfo
        }
      }
    }
  }
  ${TICKET_INFO_FRAGMENT}
`;
