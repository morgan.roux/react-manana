/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePrestataire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: PharmacieConceptInfo
// ====================================================

export interface PharmacieConceptInfo_concept {
  __typename: "Concept";
  id: string;
  typeConcept: string | null;
  modele: string | null;
}

export interface PharmacieConceptInfo_fournisseurMobilier {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieConceptInfo_enseigniste {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieConceptInfo_facade {
  __typename: "Facade";
  id: string;
  modele: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieConceptInfo_leaflet {
  __typename: "Leaflet";
  id: string;
  modele: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieConceptInfo {
  __typename: "PharmacieConcept";
  id: string;
  dateInstallConcept: string | null;
  avecConcept: boolean | null;
  avecFacade: boolean | null;
  dateInstallFacade: string | null;
  conformiteFacade: boolean | null;
  surfaceTotale: number | null;
  surfaceVente: number | null;
  SurfaceVitrine: number | null;
  volumeLeaflet: number | null;
  nbreLineaire: number | null;
  nbreVitrine: number | null;
  otcLibAcces: boolean | null;
  commentaire: string | null;
  concept: PharmacieConceptInfo_concept | null;
  fournisseurMobilier: PharmacieConceptInfo_fournisseurMobilier | null;
  enseigniste: PharmacieConceptInfo_enseigniste | null;
  facade: PharmacieConceptInfo_facade | null;
  leaflet: PharmacieConceptInfo_leaflet | null;
}
