import { DO_CREATE_COMMENT } from './mutation';
import { GET_COMMENTS } from './query';

export { DO_CREATE_COMMENT, GET_COMMENTS };
