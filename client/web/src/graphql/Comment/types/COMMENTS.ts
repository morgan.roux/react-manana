/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: COMMENTS
// ====================================================

export interface COMMENTS_comments_data_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface COMMENTS_comments_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface COMMENTS_comments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface COMMENTS_comments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: COMMENTS_comments_data_user_userPhoto_fichier | null;
}

export interface COMMENTS_comments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface COMMENTS_comments_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: COMMENTS_comments_data_user_role | null;
  userPhoto: COMMENTS_comments_data_user_userPhoto | null;
  pharmacie: COMMENTS_comments_data_user_pharmacie | null;
}

export interface COMMENTS_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: COMMENTS_comments_data_fichiers[] | null;
  user: COMMENTS_comments_data_user;
}

export interface COMMENTS_comments {
  __typename: "CommentResult";
  total: number;
  data: COMMENTS_comments_data[];
}

export interface COMMENTS {
  comments: COMMENTS_comments | null;
}

export interface COMMENTSVariables {
  codeItem: string;
  idItemAssocie: string;
  take?: number | null;
  skip?: number | null;
}
