/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeService, TypePrestataire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: PharmacieDigitaleInfo
// ====================================================

export interface PharmacieDigitaleInfo_servicePharmacie {
  __typename: "ServicePharmacie";
  id: string | null;
  nom: string | null;
  typeService: TypeService | null;
}

export interface PharmacieDigitaleInfo_pharmaciePestataire {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieDigitaleInfo {
  __typename: "PharmacieDigitale";
  id: string;
  flagService: boolean | null;
  dateInstallation: any | null;
  url: string | null;
  servicePharmacie: PharmacieDigitaleInfo_servicePharmacie | null;
  pharmaciePestataire: PharmacieDigitaleInfo_pharmaciePestataire | null;
}
