/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: QualiteInfo
// ====================================================

export interface QualiteInfo {
  __typename: "Qualite";
  id: string;
  libelle: string | null;
}
