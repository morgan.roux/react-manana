/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { InformationLiaisonFilter } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: COUNT_INFORMATION_LIAISONS
// ====================================================

export interface COUNT_INFORMATION_LIAISONS_countInformationLiaisons_statuts {
  __typename: "StatutCount";
  statut: string | null;
  count: number;
}

export interface COUNT_INFORMATION_LIAISONS_countInformationLiaisons_urgences {
  __typename: "UrgenceCount";
  id: string;
  count: number;
}

export interface COUNT_INFORMATION_LIAISONS_countInformationLiaisons_importances {
  __typename: "ImportanceCount";
  id: string;
  count: number;
}

export interface COUNT_INFORMATION_LIAISONS_countInformationLiaisons {
  __typename: "InformationLiaisonCount";
  statuts: (COUNT_INFORMATION_LIAISONS_countInformationLiaisons_statuts | null)[] | null;
  urgences: (COUNT_INFORMATION_LIAISONS_countInformationLiaisons_urgences | null)[] | null;
  importances: (COUNT_INFORMATION_LIAISONS_countInformationLiaisons_importances | null)[] | null;
}

export interface COUNT_INFORMATION_LIAISONS {
  countInformationLiaisons: COUNT_INFORMATION_LIAISONS_countInformationLiaisons | null;
}

export interface COUNT_INFORMATION_LIAISONSVariables {
  input: InformationLiaisonFilter;
}
