/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_INFORMATION_LIAISON
// ====================================================

export interface SEARCH_INFORMATION_LIAISON_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  type: string;
  nomLabo: string | null;
}

export interface SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Service {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
}

export interface SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_GroupeAmis {
  __typename: "GroupeAmis";
  type: string;
  id: string;
  nom: string | null;
}

export interface SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_User {
  __typename: "User";
  type: string;
  id: string;
  userName: string | null;
}

export type SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie = SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Action | SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Laboratoire | SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Service | SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_Partenaire | SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_GroupeAmis | SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie_User;

export interface SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_informationLiaison {
  __typename: "InformationLiaison";
  id: string;
}

export interface SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_userConcernee {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees {
  __typename: "InformationLiaisonUserConcernee";
  id: string;
  statut: string;
  informationLiaison: SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_informationLiaison;
  userConcernee: SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees_userConcernee;
}

export interface SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison {
  __typename: "InformationLiaison";
  id: string;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_origineAssocie | null;
  idType: string | null;
  idItem: string | null;
  idItemAssocie: string | null;
  titre: string;
  description: string;
  bloquant: boolean | null;
  statut: string;
  idTache: string | null;
  idFonction: string | null;
  nbComment: number | null;
  nbCollegue: number | null;
  nbLue: number | null;
  coupleStatutDeclarant: string | null;
  colleguesConcernees: SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison_colleguesConcernees[] | null;
}

export type SEARCH_INFORMATION_LIAISON_search_data = SEARCH_INFORMATION_LIAISON_search_data_Action | SEARCH_INFORMATION_LIAISON_search_data_InformationLiaison;

export interface SEARCH_INFORMATION_LIAISON_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_INFORMATION_LIAISON_search_data | null)[] | null;
}

export interface SEARCH_INFORMATION_LIAISON {
  search: SEARCH_INFORMATION_LIAISON_search | null;
}

export interface SEARCH_INFORMATION_LIAISONVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  filterBy?: any | null;
  sortBy?: any | null;
  skip?: number | null;
  take?: number | null;
}
