import gql from 'graphql-tag';

export const DO_AFFECT_PARTENAIRE_REPRESENTANT = gql`
  mutation AFFECT_PARTENAIRE_REPRESENTANT($input: PartenaireRepresentantAffectationInput) {
    createUpdatePartenaireRepresentantAffectation(input: $input) {
      id
      codeDepartement
      dateDebut
      dateFin
      typeAffectation
      departement {
        id
        nom
        code
      }
      partenaireRepresentant {
        id
        nom
        prenom
        fonction
      }
      partenaireRepresentantDemandeAffectation {
        id
        remplacent {
          id
          nom
          prenom
        }
      }
    }
  }
`;
