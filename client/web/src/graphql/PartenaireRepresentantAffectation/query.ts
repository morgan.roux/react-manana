import gql from 'graphql-tag';

export const GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION = gql`
  query SEARCH_CUSTOM_CONTENT_PARTENAIRE_REPRESENTANT_AFFECTATION(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on PartenaireRepresentantAffectation {
          id
          codeDepartement
          dateDebut
          dateFin
          typeAffectation
          departement {
            id
            nom
            code
          }
          partenaireRepresentant {
            id
            nom
            prenom
            fonction
          }
          partenaireRepresentantDemandeAffectation {
            id
            remplacent {
              id
              nom
              prenom
            }
          }
        }
      }
    }
  }
`;
