/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PartenaireRepresentantAffectationInput, TypeAffectation } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: AFFECT_PARTENAIRE_REPRESENTANT
// ====================================================

export interface AFFECT_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentantAffectation_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  code: string | null;
}

export interface AFFECT_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentantAffectation_partenaireRepresentant {
  __typename: "PartenaireRepresentant";
  id: string;
  nom: string;
  prenom: string | null;
  fonction: string | null;
}

export interface AFFECT_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentantAffectation_partenaireRepresentantDemandeAffectation_remplacent {
  __typename: "PartenaireRepresentant";
  id: string;
  nom: string;
  prenom: string | null;
}

export interface AFFECT_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentantAffectation_partenaireRepresentantDemandeAffectation {
  __typename: "PartenaireRepresentantDemandeAffectation";
  id: string;
  remplacent: AFFECT_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentantAffectation_partenaireRepresentantDemandeAffectation_remplacent | null;
}

export interface AFFECT_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentantAffectation {
  __typename: "PartenaireRepresentantAffectation";
  id: string;
  codeDepartement: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  typeAffectation: TypeAffectation;
  departement: AFFECT_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentantAffectation_departement | null;
  partenaireRepresentant: AFFECT_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentantAffectation_partenaireRepresentant | null;
  partenaireRepresentantDemandeAffectation: AFFECT_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentantAffectation_partenaireRepresentantDemandeAffectation | null;
}

export interface AFFECT_PARTENAIRE_REPRESENTANT {
  createUpdatePartenaireRepresentantAffectation: (AFFECT_PARTENAIRE_REPRESENTANT_createUpdatePartenaireRepresentantAffectation | null)[] | null;
}

export interface AFFECT_PARTENAIRE_REPRESENTANTVariables {
  input?: PartenaireRepresentantAffectationInput | null;
}
