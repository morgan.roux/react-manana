/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PharmacieCapInfo
// ====================================================

export interface PharmacieCapInfo {
  __typename: "PharmacieCap";
  id: string;
  cap: boolean | null;
  dateDebut: any | null;
  dateFin: any | null;
  identifiantCap: string | null;
  commentaire: string | null;
}
