import gql from 'graphql-tag';

export const PHARMACIE_CAP_INFO_FRAGMENT = gql`
  fragment PharmacieCapInfo on PharmacieCap {
    id
    cap
    dateDebut
    dateFin
    identifiantCap
    commentaire
  }
`;
