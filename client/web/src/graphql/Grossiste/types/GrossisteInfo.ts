/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: GrossisteInfo
// ====================================================

export interface GrossisteInfo {
  __typename: "Grossiste";
  id: string;
  nom: string | null;
}
