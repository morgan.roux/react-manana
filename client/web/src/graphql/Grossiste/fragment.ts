import gql from 'graphql-tag';

export const GROSSISTE_INFO_FRAGMENT = gql`
  fragment GrossisteInfo on Grossiste {
    id
    nom
  }
`;
