/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: couleurs
// ====================================================

export interface couleurs_couleurs {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface couleurs {
  couleurs: couleurs_couleurs[];
}
