/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: softDeleteCanalArticles
// ====================================================

export interface softDeleteCanalArticles_softDeleteCanalArticles {
  __typename: "ProduitCanal";
  id: string;
}

export interface softDeleteCanalArticles {
  softDeleteCanalArticles: (softDeleteCanalArticles_softDeleteCanalArticles | null)[] | null;
}

export interface softDeleteCanalArticlesVariables {
  ids: string[];
}
