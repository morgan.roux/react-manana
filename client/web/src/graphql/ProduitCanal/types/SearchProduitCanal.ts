/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: SearchProduitCanal
// ====================================================

export interface SearchProduitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface SearchProduitCanal_comments_data_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface SearchProduitCanal_comments_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SearchProduitCanal_comments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SearchProduitCanal_comments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SearchProduitCanal_comments_data_user_userPhoto_fichier | null;
}

export interface SearchProduitCanal_comments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SearchProduitCanal_comments_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SearchProduitCanal_comments_data_user_role | null;
  userPhoto: SearchProduitCanal_comments_data_user_userPhoto | null;
  pharmacie: SearchProduitCanal_comments_data_user_pharmacie | null;
}

export interface SearchProduitCanal_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: SearchProduitCanal_comments_data_fichiers[] | null;
  user: SearchProduitCanal_comments_data_user;
}

export interface SearchProduitCanal_comments {
  __typename: "CommentResult";
  total: number;
  data: SearchProduitCanal_comments_data[];
}

export interface SearchProduitCanal_produit_famille {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  libelleFamille: string | null;
}

export interface SearchProduitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface SearchProduitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface SearchProduitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: SearchProduitCanal_produit_produitTechReg_laboExploitant | null;
}

export interface SearchProduitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  famille: SearchProduitCanal_produit_famille | null;
  produitCode: SearchProduitCanal_produit_produitCode | null;
  produitTechReg: SearchProduitCanal_produit_produitTechReg | null;
}

export interface SearchProduitCanal_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface SearchProduitCanal_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SearchProduitCanal_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SearchProduitCanal_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SearchProduitCanal_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface SearchProduitCanal_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SearchProduitCanal_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SearchProduitCanal_userSmyleys_data_user_role | null;
  userPhoto: SearchProduitCanal_userSmyleys_data_user_userPhoto | null;
  pharmacie: SearchProduitCanal_userSmyleys_data_user_pharmacie | null;
}

export interface SearchProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface SearchProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: SearchProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface SearchProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: SearchProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface SearchProduitCanal_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface SearchProduitCanal_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: SearchProduitCanal_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface SearchProduitCanal_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: SearchProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: SearchProduitCanal_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface SearchProduitCanal_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: SearchProduitCanal_userSmyleys_data_smyley_groupement | null;
}

export interface SearchProduitCanal_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: SearchProduitCanal_userSmyleys_data_item | null;
  idSource: string | null;
  user: SearchProduitCanal_userSmyleys_data_user | null;
  smyley: SearchProduitCanal_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SearchProduitCanal_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (SearchProduitCanal_userSmyleys_data | null)[] | null;
}

export interface SearchProduitCanal_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface SearchProduitCanal_inOtherPanier {
  __typename: "PanierLigne";
  id: string;
}

export interface SearchProduitCanal_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
}

export interface SearchProduitCanal_operations {
  __typename: "Operation";
  id: string;
}

export interface SearchProduitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
  qteStock: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  prixPhv: number | null;
  dateCreation: any | null;
  isActive: boolean | null;
  qteTotalRemisePanachees: number | null;
  inMyPanier: boolean | null;
  remises: (SearchProduitCanal_remises | null)[] | null;
  comments: SearchProduitCanal_comments | null;
  produit: SearchProduitCanal_produit | null;
  userSmyleys: SearchProduitCanal_userSmyleys | null;
  commandeCanal: SearchProduitCanal_commandeCanal | null;
  inOtherPanier: SearchProduitCanal_inOtherPanier | null;
  articleSamePanachees: (SearchProduitCanal_articleSamePanachees | null)[] | null;
  operations: (SearchProduitCanal_operations | null)[] | null;
}
