/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_PRODUIT
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  nomOriginal: string;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_produit_produitPhoto_fichier | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_produit_produitTechReg_laboExploitant | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  produitPhoto: SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_produit_produitPhoto | null;
  produitCode: SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_produit_produitCode | null;
  produitTechReg: SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_produit_produitTechReg | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal {
  __typename: "ProduitCanal";
  id: string;
  qteStock: number | null;
  stv: number | null;
  prixPhv: number | null;
  produit: SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_produit | null;
  articleSamePanachees: (SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_articleSamePanachees | null)[] | null;
  commandeCanal: SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_commandeCanal | null;
  remises: (SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal_remises | null)[] | null;
}

export type SEARCH_CUSTOM_CONTENT_PRODUIT_search_data = SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_Action | SEARCH_CUSTOM_CONTENT_PRODUIT_search_data_ProduitCanal;

export interface SEARCH_CUSTOM_CONTENT_PRODUIT_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_PRODUIT_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUIT {
  search: SEARCH_CUSTOM_CONTENT_PRODUIT_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_PRODUITVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  idPharmacie?: string | null;
  idRemiseOperation?: string | null;
}
