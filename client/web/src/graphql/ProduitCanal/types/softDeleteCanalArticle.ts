/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: softDeleteCanalArticle
// ====================================================

export interface softDeleteCanalArticle_softDeleteCanalArticle {
  __typename: "ProduitCanal";
  id: string;
}

export interface softDeleteCanalArticle {
  softDeleteCanalArticle: softDeleteCanalArticle_softDeleteCanalArticle | null;
}

export interface softDeleteCanalArticleVariables {
  id: string;
}
