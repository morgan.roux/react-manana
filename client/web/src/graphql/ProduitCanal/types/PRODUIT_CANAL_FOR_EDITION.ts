/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PRODUIT_CANAL_FOR_EDITION
// ====================================================

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_sousGammeCommercial {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_sousGammeCatalogue {
  __typename: "SousGammeCatalogue";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  libelle: string | null;
  code: string | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitPhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_famille {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  libelleFamille: string | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitTechReg_acte {
  __typename: "Acte";
  id: string;
  codeActe: number | null;
  libelleActe: string | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitTechReg_liste {
  __typename: "Liste";
  id: string;
  codeListe: number | null;
  libelle: string | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitTechReg_libelleStockage {
  __typename: "LibelleDivers";
  id: string;
  codeInfo: number | null;
  code: number | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitTechReg_tauxss {
  __typename: "TauxSS";
  id: string;
  codeTaux: number | null;
  tauxSS: number | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  isTipsLpp: number | null;
  isHomeo: number | null;
  laboExploitant: PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitTechReg_laboExploitant | null;
  acte: PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitTechReg_acte | null;
  liste: PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitTechReg_liste | null;
  libelleStockage: PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitTechReg_libelleStockage | null;
  tauxss: PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitTechReg_tauxss | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal_produit {
  __typename: "Produit";
  id: string;
  produitPhoto: PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitPhoto | null;
  libelle: string | null;
  libelle2: string | null;
  isReferentGenerique: number | null;
  supprimer: any | null;
  surveillanceRenforcee: number | null;
  produitCode: PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitCode | null;
  famille: PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_famille | null;
  produitTechReg: PRODUIT_CANAL_FOR_EDITION_produitCanal_produit_produitTechReg | null;
}

export interface PRODUIT_CANAL_FOR_EDITION_produitCanal {
  __typename: "ProduitCanal";
  id: string;
  qteStock: number | null;
  qteMin: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  uniteSupCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  prixTtc: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  dateRetrait: any | null;
  isActive: boolean | null;
  sousGammeCommercial: PRODUIT_CANAL_FOR_EDITION_produitCanal_sousGammeCommercial | null;
  sousGammeCatalogue: PRODUIT_CANAL_FOR_EDITION_produitCanal_sousGammeCatalogue | null;
  commandeCanal: PRODUIT_CANAL_FOR_EDITION_produitCanal_commandeCanal | null;
  produit: PRODUIT_CANAL_FOR_EDITION_produitCanal_produit | null;
}

export interface PRODUIT_CANAL_FOR_EDITION {
  produitCanal: PRODUIT_CANAL_FOR_EDITION_produitCanal | null;
}

export interface PRODUIT_CANAL_FOR_EDITIONVariables {
  id: string;
}
