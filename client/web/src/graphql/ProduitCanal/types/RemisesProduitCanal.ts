/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RemisesProduitCanal
// ====================================================

export interface RemisesProduitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface RemisesProduitCanal {
  __typename: "ProduitCanal";
  remises: (RemisesProduitCanal_remises | null)[] | null;
}
