/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_PLAN_MARKETING_PRODUIT_CANALS
// ====================================================

export interface SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal_produit_produitPhoto_fichier | null;
}

export interface SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal_produit_famille {
  __typename: "Famille";
  id: string;
  libelleFamille: string | null;
}

export interface SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
}

export interface SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  produitPhoto: SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal_produit_produitPhoto | null;
  famille: SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal_produit_famille | null;
  produitCode: SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal_produit_produitCode | null;
}

export interface SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal {
  __typename: "ProduitCanal";
  id: string;
  produit: SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal_produit | null;
}

export type SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data = SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_Action | SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data_ProduitCanal;

export interface SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search_data | null)[] | null;
}

export interface SEARCH_PLAN_MARKETING_PRODUIT_CANALS {
  search: SEARCH_PLAN_MARKETING_PRODUIT_CANALS_search | null;
}

export interface SEARCH_PLAN_MARKETING_PRODUIT_CANALSVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
