/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ProduitCanalOperationInfo
// ====================================================

export interface ProduitCanalOperationInfo_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
}

export interface ProduitCanalOperationInfo_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  nomOriginal: string;
}

export interface ProduitCanalOperationInfo_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: ProduitCanalOperationInfo_produit_produitPhoto_fichier | null;
}

export interface ProduitCanalOperationInfo_produit {
  __typename: "Produit";
  id: string;
  produitPhoto: ProduitCanalOperationInfo_produit_produitPhoto | null;
}

export interface ProduitCanalOperationInfo_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
}

export interface ProduitCanalOperationInfo_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface ProduitCanalOperationInfo {
  __typename: "ProduitCanal";
  id: string;
  qteStock: number | null;
  stv: number | null;
  prixPhv: number | null;
  commandeCanal: ProduitCanalOperationInfo_commandeCanal | null;
  produit: ProduitCanalOperationInfo_produit | null;
  articleSamePanachees: (ProduitCanalOperationInfo_articleSamePanachees | null)[] | null;
  remises: (ProduitCanalOperationInfo_remises | null)[] | null;
}
