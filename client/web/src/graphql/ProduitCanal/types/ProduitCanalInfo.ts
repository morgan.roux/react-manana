/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: ProduitCanalInfo
// ====================================================

export interface ProduitCanalInfo_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ProduitCanalInfo_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: ProduitCanalInfo_produit_produitPhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ProduitCanalInfo_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface ProduitCanalInfo_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface ProduitCanalInfo_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: ProduitCanalInfo_produit_produitTechReg_laboExploitant | null;
}

export interface ProduitCanalInfo_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitPhoto: ProduitCanalInfo_produit_produitPhoto | null;
  produitCode: ProduitCanalInfo_produit_produitCode | null;
  produitTechReg: ProduitCanalInfo_produit_produitTechReg | null;
}

export interface ProduitCanalInfo_inOtherPanier {
  __typename: "PanierLigne";
  id: string;
}

export interface ProduitCanalInfo_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface ProduitCanalInfo_comments_data_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface ProduitCanalInfo_comments_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ProduitCanalInfo_comments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface ProduitCanalInfo_comments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: ProduitCanalInfo_comments_data_user_userPhoto_fichier | null;
}

export interface ProduitCanalInfo_comments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface ProduitCanalInfo_comments_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: ProduitCanalInfo_comments_data_user_role | null;
  userPhoto: ProduitCanalInfo_comments_data_user_userPhoto | null;
  pharmacie: ProduitCanalInfo_comments_data_user_pharmacie | null;
}

export interface ProduitCanalInfo_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: ProduitCanalInfo_comments_data_fichiers[] | null;
  user: ProduitCanalInfo_comments_data_user;
}

export interface ProduitCanalInfo_comments {
  __typename: "CommentResult";
  total: number;
  data: ProduitCanalInfo_comments_data[];
}

export interface ProduitCanalInfo {
  __typename: "ProduitCanal";
  type: string;
  id: string;
  produit: ProduitCanalInfo_produit | null;
  qteStock: number | null;
  qteMin: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  uniteSupCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  prixTtc: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  dateRetrait: any | null;
  isActive: boolean | null;
  qteTotalRemisePanachees: number | null;
  inMyPanier: boolean | null;
  inOtherPanier: ProduitCanalInfo_inOtherPanier | null;
  remises: (ProduitCanalInfo_remises | null)[] | null;
  comments: ProduitCanalInfo_comments | null;
}
