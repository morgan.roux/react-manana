import gql from 'graphql-tag';
import { PRODUIT_CANAL_INFO_FRAGMENT, REMISES_PRODUIT_CANAL } from './fragment';
import { USER_SMYLEY_INFO_FRAGMENT } from '../Smyley/fragment';
import { COMMENT_RESULT_INFO_FRAGEMENT } from '../Comment/fragment';
import { COMMANDE_CANAL_INFO_FRAGMENT } from '../CommandeCanal/fragment';
import { PRODUIT_MIN_INFO } from '../Produit/fragment';

export const GET_PRODUIT_CANAL = gql`
  query PRODUIT_CANAL($id: ID!, $idPharmacie: ID, $skip: Int, $take: Int, $idRemiseOperation: ID) {
    produitCanal(id: $id) {
      type
      id
      qteStock
      qteMin
      stv
      unitePetitCond
      uniteSupCond
      prixFab
      prixPhv
      prixTtc
      dateCreation
      dateModification
      dateRetrait
      isActive
      ...RemisesProduitCanal
      qteTotalRemisePanachees(idPharmacie: $idPharmacie)
      inMyPanier(idPharmacie: $idPharmacie)
      inOtherPanier(idPharmacie: $idPharmacie) {
        id
      }
      articleSamePanachees {
        id
      }
      operations {
        id
      }
      userSmyleys {
        total
        data {
          ...UserSmyleyInfo
        }
      }
      comments(take: $take, skip: $skip) {
        ...CommentResultInfo
      }
      commandeCanal {
        ...CommandeCanalInfo
      }
      produit {
        ...ProduitMinInfo
      }
    }
  }
  ${USER_SMYLEY_INFO_FRAGMENT}
  ${REMISES_PRODUIT_CANAL}
  ${COMMENT_RESULT_INFO_FRAGEMENT}
  ${COMMANDE_CANAL_INFO_FRAGMENT}
  ${PRODUIT_MIN_INFO}
`;

export const GET_PRODUIT_CANAL_FOR_EDITION = gql`
  query PRODUIT_CANAL_FOR_EDITION($id: ID!) {
    produitCanal(id: $id) {
      id
      qteStock
      qteMin
      stv
      unitePetitCond
      uniteSupCond
      prixFab
      prixPhv
      prixTtc
      dateCreation
      dateModification
      dateRetrait
      isActive
      sousGammeCommercial {
        id
        codeSousGamme
        libelle
        estDefaut
      }
      sousGammeCatalogue {
        id
        codeSousGamme
        libelle
        numOrdre
        estDefaut
      }
      commandeCanal {
        id
        libelle
        code
      }
      produit {
        id
        produitPhoto {
          ...ProduitPhotoInfo
        }
        libelle
        libelle2
        isReferentGenerique
        supprimer
        surveillanceRenforcee
        produitCode {
          id
          code
          typeCode
          referent
        }
        famille {
          id
          codeFamille
          libelleFamille
        }
        produitTechReg {
          id
          isTipsLpp
          isHomeo
          laboExploitant {
            id
            nomLabo
            sortie
          }
          acte {
            id
            codeActe
            libelleActe
          }
          liste {
            id
            codeListe
            libelle
          }
          libelleStockage {
            id
            codeInfo
            code
          }
          tauxss {
            id
            codeTaux
            tauxSS
          }
        }
      }
    }
  }
`;

export const GET_SEARCH_PRODUIT_CANAL = gql`
  query SEARCH_PRODUIT_CANAL(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $idPharmacie: ID
    $idRemiseOperation: ID
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on ProduitCanal {
          id
          unitePetitCond
          produit {
            id
            libelle
            qteStockPharmacie(idPharmacie: $idPharmacie)
            produitPhoto {
              id
              fichier {
                id
                publicUrl

                nomOriginal
              }
            }
            famille {
              id
              codeFamille
              libelleFamille
            }
            produitCode {
              id
              code
              typeCode
              referent
            }
            produitTechReg {
              id
              laboExploitant {
                id
                nomLabo
                sortie
              }
            }
          }
          qteStock
          stv
          prixPhv
          articleSamePanachees(idPharmacie: $idPharmacie) {
            id
          }
          commandeCanal {
            id
            code
          }
          remises(idPharmacie: $idPharmacie, idRemiseOperation: $idRemiseOperation) {
            id
            quantiteMin
            quantiteMax
            pourcentageRemise
            nombreUg
          }
          comments(take: $take, skip: $skip) {
            total
          }
          userSmyleys {
            total
          }
        }
      }
    }
  }
`;

export const GET_SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE = gql`
  query SEARCH_CUSTOM_CONTENT_PRODUIT_MARCHE(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on ProduitCanal {
          id
          produit {
            id
            libelle
            famille {
              id
              codeFamille
              libelleFamille
            }
            produitCode {
              id
              code
              typeCode
              referent
            }
            produitTechReg {
              id
              laboExploitant {
                id
                nomLabo
                sortie
              }
              tva {
                id
                codeTva
                tauxTva
              }
            }
          }
          sousGammeCommercial {
            id
            codeSousGamme
            libelle
          }
        }
      }
    }
  }
`;

export const GET_SEARCH_CUSTOM_CONTENT_PRODUIT = gql`
  query SEARCH_CUSTOM_CONTENT_PRODUIT(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $idPharmacie: ID
    $idRemiseOperation: ID
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on ProduitCanal {
          id
          qteStock
          stv
          prixPhv
          unitePetitCond
          produit {
            id
            libelle
            produitPhoto {
              id
              fichier {
                id
                publicUrl
                nomOriginal
              }
            }
            produitCode {
              id
              code
              typeCode
              referent
            }
            produitTechReg {
              id
              laboExploitant {
                id
                nomLabo
                sortie
              }
            }
          }
          articleSamePanachees(idPharmacie: $idPharmacie) {
            id
          }
          commandeCanal {
            id
            code
          }
          remises(idPharmacie: $idPharmacie, idRemiseOperation: $idRemiseOperation) {
            id
            quantiteMin
            quantiteMax
            pourcentageRemise
            nombreUg
          }
        }
      }
    }
  }
`;

export const GET_SEARCH_GESTION_PRODUIT_CANAL = gql`
  query SEARCH_GESTION_PRODUIT_CANAL($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on ProduitCanal {
          id
          qteStock
          stv
          prixPhv
          commandeCanal {
            id
            code
          }
          produit {
            id
            libelle
            produitPhoto {
              id
              fichier {
                id
                publicUrl
                nomOriginal
              }
            }
            produitCode {
              id
              code
              typeCode
              referent
            }
            produitTechReg {
              id
              laboExploitant {
                id
                nomLabo
                sortie
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_MINIMAL_SEARCH_PRODUIT_CANAL = gql`
  query MINIMAL_SEARCH_PRODUIT_CANAL($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on ProduitCanal {
          id
        }
      }
    }
  }
`;

export const GET_SEARCH_PLAN_MARKETING_PRODUIT_CANALS = gql`
  query SEARCH_PLAN_MARKETING_PRODUIT_CANALS(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on ProduitCanal {
          id
          produit {
            id
            libelle
            produitPhoto {
              id
              fichier {
                id
                publicUrl
              }
            }
            famille {
              id
              libelleFamille
            }
            produitCode {
              id
              code
            }
          }
        }
      }
    }
  }
`;
