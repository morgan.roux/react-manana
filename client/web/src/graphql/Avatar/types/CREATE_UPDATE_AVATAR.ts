/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sexe, FichierInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_AVATAR
// ====================================================

export interface CREATE_UPDATE_AVATAR_createUpdateAvatar_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface CREATE_UPDATE_AVATAR_createUpdateAvatar_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_AVATAR_createUpdateAvatar_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  role: CREATE_UPDATE_AVATAR_createUpdateAvatar_userCreation_role | null;
}

export interface CREATE_UPDATE_AVATAR_createUpdateAvatar {
  __typename: "Avatar";
  id: string;
  description: string | null;
  codeSexe: Sexe | null;
  dateCreation: any | null;
  dateModification: any | null;
  fichier: CREATE_UPDATE_AVATAR_createUpdateAvatar_fichier | null;
  userCreation: CREATE_UPDATE_AVATAR_createUpdateAvatar_userCreation | null;
}

export interface CREATE_UPDATE_AVATAR {
  createUpdateAvatar: CREATE_UPDATE_AVATAR_createUpdateAvatar | null;
}

export interface CREATE_UPDATE_AVATARVariables {
  id?: string | null;
  description?: string | null;
  codeSexe?: Sexe | null;
  fichier?: FichierInput | null;
}
