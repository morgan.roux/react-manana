/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: AvatarMinInfo
// ====================================================

export interface AvatarMinInfo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface AvatarMinInfo {
  __typename: "Avatar";
  id: string;
  description: string | null;
  fichier: AvatarMinInfo_fichier | null;
}
