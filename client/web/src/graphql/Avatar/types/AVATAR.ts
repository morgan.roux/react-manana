/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sexe } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: AVATAR
// ====================================================

export interface AVATAR_avatar_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface AVATAR_avatar_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface AVATAR_avatar_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  role: AVATAR_avatar_userCreation_role | null;
}

export interface AVATAR_avatar {
  __typename: "Avatar";
  id: string;
  description: string | null;
  codeSexe: Sexe | null;
  dateCreation: any | null;
  dateModification: any | null;
  fichier: AVATAR_avatar_fichier | null;
  userCreation: AVATAR_avatar_userCreation | null;
}

export interface AVATAR {
  avatar: AVATAR_avatar | null;
}

export interface AVATARVariables {
  id: string;
}
