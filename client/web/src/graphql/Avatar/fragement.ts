import gql from 'graphql-tag';

export const AVATAR_INFO_FRAGEMENT = gql`
  fragment AvatarInfo on Avatar {
    id
    description
    codeSexe
    dateCreation
    dateModification
    fichier {
      id
      chemin
      nomOriginal
      publicUrl
    }
    userCreation {
      id
      userName
      role {
        id
        nom
      }
    }
  }
`;

export const AVATAR_MIN_INFO_FRAGEMENT = gql`
  fragment AvatarMinInfo on Avatar {
    id
    description
    fichier {
      id
      chemin
      nomOriginal
      publicUrl
    }
  }
`;
