/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PersonnelFonctionInfo
// ====================================================

export interface PersonnelFonctionInfo {
  __typename: "PersonnelFonction";
  id: string;
  nom: string;
  code: string;
}
