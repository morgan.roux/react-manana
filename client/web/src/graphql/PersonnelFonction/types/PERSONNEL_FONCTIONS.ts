/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PERSONNEL_FONCTIONS
// ====================================================

export interface PERSONNEL_FONCTIONS_personnelFonctions_data {
  __typename: "PersonnelFonction";
  id: string;
  nom: string;
  code: string;
}

export interface PERSONNEL_FONCTIONS_personnelFonctions {
  __typename: "PersonnelFonctionResult";
  total: number;
  data: (PERSONNEL_FONCTIONS_personnelFonctions_data | null)[] | null;
}

export interface PERSONNEL_FONCTIONS {
  personnelFonctions: PERSONNEL_FONCTIONS_personnelFonctions | null;
}

export interface PERSONNEL_FONCTIONSVariables {
  take?: number | null;
  skip?: number | null;
}
