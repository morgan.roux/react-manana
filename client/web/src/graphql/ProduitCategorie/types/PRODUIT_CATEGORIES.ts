/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PRODUIT_CATEGORIES
// ====================================================

export interface PRODUIT_CATEGORIES_produitCategories {
  __typename: "ProduitCategorie";
  id: string;
  resipIdCategorie: number | null;
  libelle: string | null;
}

export interface PRODUIT_CATEGORIES {
  produitCategories: (PRODUIT_CATEGORIES_produitCategories | null)[] | null;
}
