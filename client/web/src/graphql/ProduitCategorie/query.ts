import gql from 'graphql-tag';

export const GET_PRODUIT_CATEGORIES = gql`
  query PRODUIT_CATEGORIES {
    produitCategories {
      id
      resipIdCategorie
      libelle
    }
  }
`;
