/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { LaboratoirePartenaireInput, StatutPartenaire, TypePartenaire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_LABORATOIRE_PARTENAIRE
// ====================================================

export interface CREATE_UPDATE_LABORATOIRE_PARTENAIRE_createUpdateLaboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  statutPartenaire: StatutPartenaire | null;
  typePartenaire: TypePartenaire | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_UPDATE_LABORATOIRE_PARTENAIRE {
  createUpdateLaboratoirePartenaire: CREATE_UPDATE_LABORATOIRE_PARTENAIRE_createUpdateLaboratoirePartenaire | null;
}

export interface CREATE_UPDATE_LABORATOIRE_PARTENAIREVariables {
  input: LaboratoirePartenaireInput;
}
