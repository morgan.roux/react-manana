/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { StatutPartenaire, TypePartenaire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: GET_LABO_PARTENAIRE
// ====================================================

export interface GET_LABO_PARTENAIRE_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  statutPartenaire: StatutPartenaire | null;
  typePartenaire: TypePartenaire | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface GET_LABO_PARTENAIRE {
  laboratoirePartenaire: GET_LABO_PARTENAIRE_laboratoirePartenaire | null;
}

export interface GET_LABO_PARTENAIREVariables {
  id: string;
}
