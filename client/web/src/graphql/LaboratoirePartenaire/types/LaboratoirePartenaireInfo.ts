/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { StatutPartenaire, TypePartenaire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: LaboratoirePartenaireInfo
// ====================================================

export interface LaboratoirePartenaireInfo {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  statutPartenaire: StatutPartenaire | null;
  typePartenaire: TypePartenaire | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}
