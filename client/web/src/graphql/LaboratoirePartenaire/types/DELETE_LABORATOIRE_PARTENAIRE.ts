/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { StatutPartenaire, TypePartenaire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_LABORATOIRE_PARTENAIRE
// ====================================================

export interface DELETE_LABORATOIRE_PARTENAIRE_softDeleteLaboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  statutPartenaire: StatutPartenaire | null;
  typePartenaire: TypePartenaire | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface DELETE_LABORATOIRE_PARTENAIRE {
  softDeleteLaboratoirePartenaire: DELETE_LABORATOIRE_PARTENAIRE_softDeleteLaboratoirePartenaire[];
}

export interface DELETE_LABORATOIRE_PARTENAIREVariables {
  ids: string[];
}
