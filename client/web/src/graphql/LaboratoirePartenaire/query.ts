import gql from 'graphql-tag';
import { LABORATOIRE_PARTENAIRE_INFO_FRAGMENT } from './fragments';

export const DO_GET_LABO_PARTENAIRE = gql`
  query GET_LABO_PARTENAIRE($id: ID!) {
    laboratoirePartenaire(id: $id) {
      ...LaboratoirePartenaireInfo
    }
  }
  ${LABORATOIRE_PARTENAIRE_INFO_FRAGMENT}
`;
