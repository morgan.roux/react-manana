import gql from 'graphql-tag';

export const LABORATOIRE_PARTENAIRE_INFO_FRAGMENT = gql`
  fragment LaboratoirePartenaireInfo on LaboratoirePartenaire {
    id
    debutPartenaire
    finPartenaire
    statutPartenaire
    typePartenaire
    codeMaj
    dateCreation
    dateModification
  }
`;
