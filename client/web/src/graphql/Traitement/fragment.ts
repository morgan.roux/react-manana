import gql from 'graphql-tag';

export const TRAITEMENT_FRAGMENT = gql`
  fragment TraitementInfo on Traitement {
    type
    id
    nom
    code
  }
`;
