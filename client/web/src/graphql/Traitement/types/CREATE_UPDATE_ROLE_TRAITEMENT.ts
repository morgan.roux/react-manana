/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_ROLE_TRAITEMENT
// ====================================================

export interface CREATE_UPDATE_ROLE_TRAITEMENT_createUpdateRoleTraitement_roleTraitements {
  __typename: "RoleTraitement";
  codeTraitement: string | null;
}

export interface CREATE_UPDATE_ROLE_TRAITEMENT_createUpdateRoleTraitement {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  roleTraitements: (CREATE_UPDATE_ROLE_TRAITEMENT_createUpdateRoleTraitement_roleTraitements | null)[] | null;
}

export interface CREATE_UPDATE_ROLE_TRAITEMENT {
  /**
   * traitement
   */
  createUpdateRoleTraitement: CREATE_UPDATE_ROLE_TRAITEMENT_createUpdateRoleTraitement | null;
}

export interface CREATE_UPDATE_ROLE_TRAITEMENTVariables {
  codeRole: string;
  codeTraitements?: (string | null)[] | null;
}
