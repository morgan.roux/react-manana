/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RegionInfo
// ====================================================

export interface RegionInfo {
  __typename: "Region";
  id: string;
  nom: string | null;
}
