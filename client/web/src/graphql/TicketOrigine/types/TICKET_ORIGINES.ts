/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TICKET_ORIGINES
// ====================================================

export interface TICKET_ORIGINES_ticketOrigines {
  __typename: "TicketOrigine";
  id: string;
  nom: string;
}

export interface TICKET_ORIGINES {
  ticketOrigines: (TICKET_ORIGINES_ticketOrigines | null)[] | null;
}
