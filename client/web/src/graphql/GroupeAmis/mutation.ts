import gql from 'graphql-tag';
import { GROUP_AMIS_FRAGMENT } from '../GroupAmis/fragment';

export const DO_CREATE_UPDATE_GROUPE_AMIS = gql`
  mutation CREATE_UPDATE_GROUPE_AMIS($input: GroupeAmisInput!) {
    createUpdateGroupeAmis(input: $input) {
      ...GroupAmisInfo
    }
  }
  ${GROUP_AMIS_FRAGMENT}
`;

export const DO_UPDATE_STATUS_GROUP = gql`
  mutation UPDATE_STATUS_GROUPE($id: ID!, $status: GroupeAmisStatus!) {
    updateGroupeAmisStatus(id: $id, status: $status) {
      ...GroupAmisInfo
    }
  }
  ${GROUP_AMIS_FRAGMENT}
`;
