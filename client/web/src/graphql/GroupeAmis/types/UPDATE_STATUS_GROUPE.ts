/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GroupeAmisStatus, GroupeAmisType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_STATUS_GROUPE
// ====================================================

export interface UPDATE_STATUS_GROUPE_updateGroupeAmisStatus {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
  description: string | null;
  typeGroupeAmis: GroupeAmisType;
  nbPharmacie: number;
  groupStatus: GroupeAmisStatus;
  nbMember: number;
  nbPartage: number;
  nbRecommandation: number;
  dateCreation: any | null;
}

export interface UPDATE_STATUS_GROUPE {
  updateGroupeAmisStatus: UPDATE_STATUS_GROUPE_updateGroupeAmisStatus;
}

export interface UPDATE_STATUS_GROUPEVariables {
  id: string;
  status: GroupeAmisStatus;
}
