/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { GroupeAmisType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_GROUPE_AMIS
// ====================================================

export interface SEARCH_GROUPE_AMIS_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_GROUPE_AMIS_search_data_GroupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
  nbPharmacie: number;
  dateCreation: any | null;
  isRemoved: boolean | null;
  typeGroupeAmis: GroupeAmisType;
}

export type SEARCH_GROUPE_AMIS_search_data = SEARCH_GROUPE_AMIS_search_data_Action | SEARCH_GROUPE_AMIS_search_data_GroupeAmis;

export interface SEARCH_GROUPE_AMIS_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_GROUPE_AMIS_search_data | null)[] | null;
}

export interface SEARCH_GROUPE_AMIS {
  search: SEARCH_GROUPE_AMIS_search | null;
}

export interface SEARCH_GROUPE_AMISVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
