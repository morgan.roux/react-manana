import gql from 'graphql-tag';

export const DO_SEARCH_GROUPE_AMIS = gql`
  query SEARCH_GROUPE_AMIS(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on GroupeAmis {
          id
          nom
          nbPharmacie
          dateCreation
          isRemoved
          typeGroupeAmis
        }
      }
    }
  }
`;
