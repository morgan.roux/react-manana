import gql from 'graphql-tag';

export const DO_REFRESH_INDEXES = gql`
  query REFRESH_INDEXES($withMapping: Boolean) {
    refreshIndexes(withMapping: $withMapping)
  }
`;

export const DO_REINDEX = gql`
  query REINDEX($routingKey: String) {
    reindex(routingKey: $routingKey)
  }
`;
