/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: REINDEX
// ====================================================

export interface REINDEX {
  reindex: boolean | null;
}

export interface REINDEXVariables {
  routingKey?: string | null;
}
