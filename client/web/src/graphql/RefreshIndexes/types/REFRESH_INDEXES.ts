/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: REFRESH_INDEXES
// ====================================================

export interface REFRESH_INDEXES {
  refreshIndexes: boolean | null;
}

export interface REFRESH_INDEXESVariables {
  withMapping?: boolean | null;
}
