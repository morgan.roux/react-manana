import gql from 'graphql-tag';

export const GROUPEMENT_LOGO = gql`
  fragment GroupementLogo on Groupement {
    groupementLogo {
      id
      fichier {
        id
        chemin
      }
    }
  }
`;

export const GROUPEMENT_INFO = gql`
  fragment GroupementInfo on Groupement {
    id
    isReference
    nom
    adresse1
    adresse2
    cp
    ville
    pays
    telBureau
    telMobile
    mail
    site
    commentaire
    nomResponsable
    prenomResponsable
    dateSortie
    sortie
    defaultPharmacie {
      id
      nom
      ville
      cip
      adresse1
      type
      pharmacieType
      departement {
        id
        nom
        region {
          id
          nom
        }
      }
    }
    ...GroupementLogo
  }
  ${GROUPEMENT_LOGO}
`;

export const SEARCH_GROUPEMENT_INFO = gql`
  fragment SearchGroupementInfo on Groupement {
    type
    id
    nom
    adresse1
    adresse2
    cp
    ville
    pays
    telBureau
    telMobile
    mail
    site
    commentaire
    nomResponsable
    prenomResponsable
    dateSortie
    sortie
    dateCreation
    dateModification
  }
  ${GROUPEMENT_INFO}
`;
