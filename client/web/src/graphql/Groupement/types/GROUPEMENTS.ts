/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GROUPEMENTS
// ====================================================

export interface GROUPEMENTS_groupements_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  publicUrl: string | null;
}

export interface GROUPEMENTS_groupements_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: GROUPEMENTS_groupements_groupementLogo_fichier | null;
}

export interface GROUPEMENTS_groupements {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  groupementLogo: GROUPEMENTS_groupements_groupementLogo | null;
}

export interface GROUPEMENTS {
  groupements: (GROUPEMENTS_groupements | null)[] | null;
}
