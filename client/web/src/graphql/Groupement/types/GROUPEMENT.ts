/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GROUPEMENT
// ====================================================

export interface GROUPEMENT_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface GROUPEMENT_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: GROUPEMENT_groupement_defaultPharmacie_departement_region | null;
}

export interface GROUPEMENT_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: GROUPEMENT_groupement_defaultPharmacie_departement | null;
}

export interface GROUPEMENT_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface GROUPEMENT_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: GROUPEMENT_groupement_groupementLogo_fichier | null;
}

export interface GROUPEMENT_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: GROUPEMENT_groupement_defaultPharmacie | null;
  groupementLogo: GROUPEMENT_groupement_groupementLogo | null;
}

export interface GROUPEMENT {
  groupement: GROUPEMENT_groupement | null;
}

export interface GROUPEMENTVariables {
  id: string;
}
