/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_GROUPEMENT
// ====================================================

export interface UPDATE_GROUPEMENT_updateGroupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface UPDATE_GROUPEMENT_updateGroupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: UPDATE_GROUPEMENT_updateGroupement_groupementLogo_fichier | null;
}

export interface UPDATE_GROUPEMENT_updateGroupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  groupementLogo: UPDATE_GROUPEMENT_updateGroupement_groupementLogo | null;
}

export interface UPDATE_GROUPEMENT {
  updateGroupement: UPDATE_GROUPEMENT_updateGroupement | null;
}

export interface UPDATE_GROUPEMENTVariables {
  id: string;
  nom: string;
  adresse1: string;
  adresse2?: string | null;
  cp: string;
  ville: string;
  pays?: string | null;
  telBureau: string;
  telMobile?: string | null;
  mail: string;
  site?: string | null;
  commentaire?: string | null;
  nomResponsable: string;
  prenomResponsable: string;
  dateSortie?: any | null;
  sortie?: number | null;
  avatar?: FichierInput | null;
  isReference?: boolean | null;
}
