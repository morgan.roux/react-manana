/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: GroupementInfo
// ====================================================

export interface GroupementInfo_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface GroupementInfo_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: GroupementInfo_defaultPharmacie_departement_region | null;
}

export interface GroupementInfo_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: GroupementInfo_defaultPharmacie_departement | null;
}

export interface GroupementInfo_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface GroupementInfo_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: GroupementInfo_groupementLogo_fichier | null;
}

export interface GroupementInfo {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: GroupementInfo_defaultPharmacie | null;
  groupementLogo: GroupementInfo_groupementLogo | null;
}
