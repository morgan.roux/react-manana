/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_GROUPEMENT
// ====================================================

export interface DELETE_GROUPEMENT_deleteGroupement {
  __typename: "Groupement";
  id: string;
}

export interface DELETE_GROUPEMENT {
  deleteGroupement: DELETE_GROUPEMENT_deleteGroupement | null;
}

export interface DELETE_GROUPEMENTVariables {
  id: string;
}
