/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CREATE_GROUPEMENT_LOGO
// ====================================================

export interface CREATE_GROUPEMENT_LOGO_createGroupementLogo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
}

export interface CREATE_GROUPEMENT_LOGO {
  createGroupementLogo: CREATE_GROUPEMENT_LOGO_createGroupementLogo | null;
}

export interface CREATE_GROUPEMENT_LOGOVariables {
  id: string;
  chemin: string;
  nomOriginal: string;
}
