/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GROUPEMENT_WITHOUT_PHARMACIES
// ====================================================

export interface GROUPEMENT_WITHOUT_PHARMACIES_groupement_pharmacies_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface GROUPEMENT_WITHOUT_PHARMACIES_groupement_pharmacies_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: GROUPEMENT_WITHOUT_PHARMACIES_groupement_pharmacies_departement_region | null;
}

export interface GROUPEMENT_WITHOUT_PHARMACIES_groupement_pharmacies {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: GROUPEMENT_WITHOUT_PHARMACIES_groupement_pharmacies_departement | null;
}

export interface GROUPEMENT_WITHOUT_PHARMACIES_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface GROUPEMENT_WITHOUT_PHARMACIES_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: GROUPEMENT_WITHOUT_PHARMACIES_groupement_defaultPharmacie_departement_region | null;
}

export interface GROUPEMENT_WITHOUT_PHARMACIES_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: GROUPEMENT_WITHOUT_PHARMACIES_groupement_defaultPharmacie_departement | null;
}

export interface GROUPEMENT_WITHOUT_PHARMACIES_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface GROUPEMENT_WITHOUT_PHARMACIES_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: GROUPEMENT_WITHOUT_PHARMACIES_groupement_groupementLogo_fichier | null;
}

export interface GROUPEMENT_WITHOUT_PHARMACIES_groupement {
  __typename: "Groupement";
  id: string;
  pharmacies: (GROUPEMENT_WITHOUT_PHARMACIES_groupement_pharmacies | null)[] | null;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: GROUPEMENT_WITHOUT_PHARMACIES_groupement_defaultPharmacie | null;
  groupementLogo: GROUPEMENT_WITHOUT_PHARMACIES_groupement_groupementLogo | null;
}

export interface GROUPEMENT_WITHOUT_PHARMACIES {
  groupement: GROUPEMENT_WITHOUT_PHARMACIES_groupement | null;
}

export interface GROUPEMENT_WITHOUT_PHARMACIESVariables {
  id: string;
}
