/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_GROUPEMENT
// ====================================================

export interface CREATE_GROUPEMENT_createGroupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface CREATE_GROUPEMENT_createGroupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: CREATE_GROUPEMENT_createGroupement_groupementLogo_fichier | null;
}

export interface CREATE_GROUPEMENT_createGroupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  groupementLogo: CREATE_GROUPEMENT_createGroupement_groupementLogo | null;
}

export interface CREATE_GROUPEMENT {
  createGroupement: CREATE_GROUPEMENT_createGroupement | null;
}

export interface CREATE_GROUPEMENTVariables {
  nom: string;
  adresse1: string;
  adresse2?: string | null;
  cp: string;
  ville: string;
  pays?: string | null;
  telBureau: string;
  telMobile?: string | null;
  mail: string;
  site?: string | null;
  commentaire?: string | null;
  nomResponsable: string;
  prenomResponsable: string;
  dateSortie?: any | null;
  sortie?: number | null;
  avatar?: FichierInput | null;
}
