/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_GROUPEMENT_LOGO
// ====================================================

export interface DELETE_GROUPEMENT_LOGO_deleteGroupementLogo {
  __typename: "Fichier";
  id: string;
}

export interface DELETE_GROUPEMENT_LOGO {
  deleteGroupementLogo: DELETE_GROUPEMENT_LOGO_deleteGroupementLogo | null;
}

export interface DELETE_GROUPEMENT_LOGOVariables {
  chemin: string;
}
