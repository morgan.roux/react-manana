import gql from 'graphql-tag';

export const TODO_SECTION_INFO_FRAGMENT = gql`
  fragment TodoSectionInfo on TodoSection {
    id
    ordre
    libelle
    isInInbox
    isInInboxTeam
    nbAction
    project {
      id
    }
    user {
      id
    }
    isArchived
    userCreation {
      id
    }
  }
`;
