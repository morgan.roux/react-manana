/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TodoSectionInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_TODO_SECTION
// ====================================================

export interface CREATE_UPDATE_TODO_SECTION_createUpdateTodoSection_project {
  __typename: "Project";
  id: string;
}

export interface CREATE_UPDATE_TODO_SECTION_createUpdateTodoSection_user {
  __typename: "User";
  id: string;
}

export interface CREATE_UPDATE_TODO_SECTION_createUpdateTodoSection_userCreation {
  __typename: "User";
  id: string;
}

export interface CREATE_UPDATE_TODO_SECTION_createUpdateTodoSection {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  nbAction: number;
  project: CREATE_UPDATE_TODO_SECTION_createUpdateTodoSection_project | null;
  user: CREATE_UPDATE_TODO_SECTION_createUpdateTodoSection_user;
  isArchived: boolean | null;
  userCreation: CREATE_UPDATE_TODO_SECTION_createUpdateTodoSection_userCreation | null;
}

export interface CREATE_UPDATE_TODO_SECTION {
  createUpdateTodoSection: CREATE_UPDATE_TODO_SECTION_createUpdateTodoSection;
}

export interface CREATE_UPDATE_TODO_SECTIONVariables {
  input: TodoSectionInput;
}
