/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TodoSectionInfo
// ====================================================

export interface TodoSectionInfo_project {
  __typename: "Project";
  id: string;
}

export interface TodoSectionInfo_user {
  __typename: "User";
  id: string;
}

export interface TodoSectionInfo_userCreation {
  __typename: "User";
  id: string;
}

export interface TodoSectionInfo {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  nbAction: number;
  project: TodoSectionInfo_project | null;
  user: TodoSectionInfo_user;
  isArchived: boolean | null;
  userCreation: TodoSectionInfo_userCreation | null;
}
