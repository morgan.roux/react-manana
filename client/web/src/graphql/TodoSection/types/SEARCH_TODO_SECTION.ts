/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_TODO_SECTION
// ====================================================

export interface SEARCH_TODO_SECTION_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_TODO_SECTION_search_data_TodoSection_project {
  __typename: "Project";
  id: string;
}

export interface SEARCH_TODO_SECTION_search_data_TodoSection_user {
  __typename: "User";
  id: string;
}

export interface SEARCH_TODO_SECTION_search_data_TodoSection_userCreation {
  __typename: "User";
  id: string;
}

export interface SEARCH_TODO_SECTION_search_data_TodoSection {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  nbAction: number;
  project: SEARCH_TODO_SECTION_search_data_TodoSection_project | null;
  user: SEARCH_TODO_SECTION_search_data_TodoSection_user;
  isArchived: boolean | null;
  userCreation: SEARCH_TODO_SECTION_search_data_TodoSection_userCreation | null;
}

export type SEARCH_TODO_SECTION_search_data = SEARCH_TODO_SECTION_search_data_Action | SEARCH_TODO_SECTION_search_data_TodoSection;

export interface SEARCH_TODO_SECTION_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_TODO_SECTION_search_data | null)[] | null;
}

export interface SEARCH_TODO_SECTION {
  search: SEARCH_TODO_SECTION_search | null;
}

export interface SEARCH_TODO_SECTIONVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  filterBy?: any | null;
  sortBy?: any | null;
  skip?: number | null;
  take?: number | null;
}
