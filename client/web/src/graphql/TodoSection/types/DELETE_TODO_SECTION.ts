/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_TODO_SECTION
// ====================================================

export interface DELETE_TODO_SECTION_softDeleteTodoSections_project {
  __typename: "Project";
  id: string;
}

export interface DELETE_TODO_SECTION_softDeleteTodoSections_user {
  __typename: "User";
  id: string;
}

export interface DELETE_TODO_SECTION_softDeleteTodoSections_userCreation {
  __typename: "User";
  id: string;
}

export interface DELETE_TODO_SECTION_softDeleteTodoSections {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  nbAction: number;
  project: DELETE_TODO_SECTION_softDeleteTodoSections_project | null;
  user: DELETE_TODO_SECTION_softDeleteTodoSections_user;
  isArchived: boolean | null;
  userCreation: DELETE_TODO_SECTION_softDeleteTodoSections_userCreation | null;
}

export interface DELETE_TODO_SECTION {
  softDeleteTodoSections: DELETE_TODO_SECTION_softDeleteTodoSections[];
}

export interface DELETE_TODO_SECTIONVariables {
  ids: string[];
}
