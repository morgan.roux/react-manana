/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TYPOLOGIES
// ====================================================

export interface TYPOLOGIES_typologies {
  __typename: "Typologie";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface TYPOLOGIES {
  typologies: (TYPOLOGIES_typologies | null)[] | null;
}
