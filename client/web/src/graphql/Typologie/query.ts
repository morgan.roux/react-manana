import gql from 'graphql-tag';
import { TYPOLOGIE_INFO_FRAGMENT } from './fragment';

export const GET_TYPOLOGIES = gql`
  query TYPOLOGIES {
    typologies {
      ...TypologieInfo
    }
  }
  ${TYPOLOGIE_INFO_FRAGMENT}
`;
