/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_AFFECTATION_TITULAIRE_LIST
// ====================================================

export interface SEARCH_AFFECTATION_TITULAIRE_LIST_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_AFFECTATION_TITULAIRE_LIST_search_data_Titulaire {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
}

export type SEARCH_AFFECTATION_TITULAIRE_LIST_search_data = SEARCH_AFFECTATION_TITULAIRE_LIST_search_data_Action | SEARCH_AFFECTATION_TITULAIRE_LIST_search_data_Titulaire;

export interface SEARCH_AFFECTATION_TITULAIRE_LIST_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_AFFECTATION_TITULAIRE_LIST_search_data | null)[] | null;
}

export interface SEARCH_AFFECTATION_TITULAIRE_LIST {
  search: SEARCH_AFFECTATION_TITULAIRE_LIST_search | null;
}

export interface SEARCH_AFFECTATION_TITULAIRE_LISTVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
