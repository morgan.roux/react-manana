/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_TitulaireAffectation_titulaireFonction {
  __typename: "TitulaireFonction";
  id: string;
  nom: string;
  code: string;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_TitulaireAffectation_departement {
  __typename: "Departement";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_TitulaireAffectation_titulaire {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_TitulaireAffectation_titulaireDemandeAffectation_titulaireRemplacent {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_TitulaireAffectation_titulaireDemandeAffectation {
  __typename: "TitulaireDemandeAffectation";
  id: string;
  titulaireRemplacent: SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_TitulaireAffectation_titulaireDemandeAffectation_titulaireRemplacent | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_TitulaireAffectation {
  __typename: "TitulaireAffectation";
  id: string;
  titulaireFonction: SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_TitulaireAffectation_titulaireFonction | null;
  departement: SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_TitulaireAffectation_departement | null;
  titulaire: SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_TitulaireAffectation_titulaire | null;
  titulaireDemandeAffectation: SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_TitulaireAffectation_titulaireDemandeAffectation | null;
  dateDebut: any | null;
  dateFin: any | null;
}

export type SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data = SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_Action | SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data_TitulaireAffectation;

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION {
  search: SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATION_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_AFFECTATIONVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
