import gql from 'graphql-tag';
import { TITULAIRE_FONCTION_INFO_FRAGMENT } from '../TitulaireFonction/fragment';
import { DEPARTEMENT_INFO_FRAGMENT } from '../Departement/fragment';

export const TITULAIRE_AFFECTATION_INFO_FRAGMENT = gql`
  fragment TitulaireAffectationInfo on TitulaireAffectation {
    id
    titulaireFonction {
      ...TitulaireFonctionInfo
    }
    departement {
      ...DepartementInfo
    }
    titulaire {
      id
      fullName
    }
    titulaireDemandeAffectation {
      id
      titulaireRemplacent {
        id
        fullName
      }
    }
    dateDebut
    dateFin
  }
  ${TITULAIRE_FONCTION_INFO_FRAGMENT}
  ${DEPARTEMENT_INFO_FRAGMENT}
`;
