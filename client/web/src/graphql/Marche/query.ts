import gql from 'graphql-tag';
import { MARCHE_INFO_FRAGMENT_FOR_EDIT, MARCHE_INFO_FRAGMENT } from './fragment';

export const GET_MARCHE = gql`
  query MARCHE($id: ID!) {
    marche(id: $id) {
      ...MarcheInfoForEdit
    }
  }
  ${MARCHE_INFO_FRAGMENT_FOR_EDIT}
`;

export const GET_SEARCH_CUSTOM_CONTENT_MARCHE = gql`
  query SEARCH_CUSTOM_CONTENT_MARCHE($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Marche {
          ...MarcheInfo
        }
      }
    }
  }
  ${MARCHE_INFO_FRAGMENT}
`;

export const GET_MARCHE_PRODUIT_CANAL = gql`
  query MARCHE_PRODUIT_CANAL($id: ID!) {
    marche(id: $id) {
      id
      nom
      dateDebut
      dateFin
      marcheType
      avecPalier
      status
      nbAdhesion
      nbPalier
      dateCreation
      dateModification
      commandeCanal {
        id
        code
        libelle
      }
      canalArticles {
        id
        produit {
          id
          libelle
          libelle2
          produitCode {
            id
            code
            typeCode
            referent
          }
        }
      }
    }
  }
`;
