/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MarcheType, MarcheStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_MARCHE
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_MARCHE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_CUSTOM_CONTENT_MARCHE_search_data_Marche_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_MARCHE_search_data_Marche_userCreated {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_MARCHE_search_data_Marche {
  __typename: "Marche";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  marcheType: MarcheType | null;
  avecPalier: boolean | null;
  status: MarcheStatus | null;
  nbAdhesion: number | null;
  nbPalier: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: SEARCH_CUSTOM_CONTENT_MARCHE_search_data_Marche_commandeCanal | null;
  userCreated: SEARCH_CUSTOM_CONTENT_MARCHE_search_data_Marche_userCreated | null;
}

export type SEARCH_CUSTOM_CONTENT_MARCHE_search_data = SEARCH_CUSTOM_CONTENT_MARCHE_search_data_Action | SEARCH_CUSTOM_CONTENT_MARCHE_search_data_Marche;

export interface SEARCH_CUSTOM_CONTENT_MARCHE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_MARCHE_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_MARCHE {
  search: SEARCH_CUSTOM_CONTENT_MARCHE_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_MARCHEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
