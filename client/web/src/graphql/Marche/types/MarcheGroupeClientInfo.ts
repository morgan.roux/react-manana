/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: MarcheGroupeClientInfo
// ====================================================

export interface MarcheGroupeClientInfo_groupeClient {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
  nomGroupe: string | null;
  nomCommercial: string | null;
  dateValiditeDebut: any | null;
  dateValiditeFin: any | null;
}

export interface MarcheGroupeClientInfo {
  __typename: "MarcheGroupeClient";
  id: string;
  dateEngagement: any | null;
  commentaire: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  groupeClient: MarcheGroupeClientInfo_groupeClient | null;
}
