/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MarcheType, MarcheStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: MarcheInfoForEdit
// ====================================================

export interface MarcheInfoForEdit_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface MarcheInfoForEdit_canalArticles_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface MarcheInfoForEdit_canalArticles_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitCode: MarcheInfoForEdit_canalArticles_produit_produitCode | null;
}

export interface MarcheInfoForEdit_canalArticles {
  __typename: "ProduitCanal";
  id: string;
  produit: MarcheInfoForEdit_canalArticles_produit | null;
}

export interface MarcheInfoForEdit_laboratoires_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface MarcheInfoForEdit_laboratoires {
  __typename: "MarcheLaboratoire";
  id: string;
  numOrdre: number | null;
  obligatoire: boolean | null;
  laboratoire: MarcheInfoForEdit_laboratoires_laboratoire | null;
}

export interface MarcheInfoForEdit_groupeClients_groupeClient {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
}

export interface MarcheInfoForEdit_groupeClients_remise_remiseDetails {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  remiseSupplementaire: number | null;
}

export interface MarcheInfoForEdit_groupeClients_remise {
  __typename: "Remise";
  id: string;
  remiseDetails: (MarcheInfoForEdit_groupeClients_remise_remiseDetails | null)[] | null;
}

export interface MarcheInfoForEdit_groupeClients {
  __typename: "MarcheGroupeClient";
  id: string;
  groupeClient: MarcheInfoForEdit_groupeClients_groupeClient | null;
  remise: MarcheInfoForEdit_groupeClients_remise | null;
}

export interface MarcheInfoForEdit {
  __typename: "Marche";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  marcheType: MarcheType | null;
  avecPalier: boolean | null;
  status: MarcheStatus | null;
  nbAdhesion: number | null;
  nbPalier: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: MarcheInfoForEdit_commandeCanal | null;
  canalArticles: (MarcheInfoForEdit_canalArticles | null)[] | null;
  laboratoires: (MarcheInfoForEdit_laboratoires | null)[] | null;
  groupeClients: (MarcheInfoForEdit_groupeClients | null)[] | null;
}
