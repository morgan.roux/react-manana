/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MarcheType, MarcheStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_MARCHE
// ====================================================

export interface DELETE_MARCHE_softDeleteMarche_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface DELETE_MARCHE_softDeleteMarche_userCreated {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface DELETE_MARCHE_softDeleteMarche {
  __typename: "Marche";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  marcheType: MarcheType | null;
  avecPalier: boolean | null;
  status: MarcheStatus | null;
  nbAdhesion: number | null;
  nbPalier: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: DELETE_MARCHE_softDeleteMarche_commandeCanal | null;
  userCreated: DELETE_MARCHE_softDeleteMarche_userCreated | null;
}

export interface DELETE_MARCHE {
  softDeleteMarche: DELETE_MARCHE_softDeleteMarche | null;
}

export interface DELETE_MARCHEVariables {
  id: string;
}
