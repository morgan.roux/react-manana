/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MarcheType, MarcheStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: MarcheInfo
// ====================================================

export interface MarcheInfo_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface MarcheInfo_userCreated {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface MarcheInfo {
  __typename: "Marche";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  marcheType: MarcheType | null;
  avecPalier: boolean | null;
  status: MarcheStatus | null;
  nbAdhesion: number | null;
  nbPalier: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: MarcheInfo_commandeCanal | null;
  userCreated: MarcheInfo_userCreated | null;
}
