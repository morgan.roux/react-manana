/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MarcheType, MarcheStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: MARCHE
// ====================================================

export interface MARCHE_marche_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface MARCHE_marche_canalArticles_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface MARCHE_marche_canalArticles_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitCode: MARCHE_marche_canalArticles_produit_produitCode | null;
}

export interface MARCHE_marche_canalArticles {
  __typename: "ProduitCanal";
  id: string;
  produit: MARCHE_marche_canalArticles_produit | null;
}

export interface MARCHE_marche_laboratoires_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface MARCHE_marche_laboratoires {
  __typename: "MarcheLaboratoire";
  id: string;
  numOrdre: number | null;
  obligatoire: boolean | null;
  laboratoire: MARCHE_marche_laboratoires_laboratoire | null;
}

export interface MARCHE_marche_groupeClients_groupeClient {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
}

export interface MARCHE_marche_groupeClients_remise_remiseDetails {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  remiseSupplementaire: number | null;
}

export interface MARCHE_marche_groupeClients_remise {
  __typename: "Remise";
  id: string;
  remiseDetails: (MARCHE_marche_groupeClients_remise_remiseDetails | null)[] | null;
}

export interface MARCHE_marche_groupeClients {
  __typename: "MarcheGroupeClient";
  id: string;
  groupeClient: MARCHE_marche_groupeClients_groupeClient | null;
  remise: MARCHE_marche_groupeClients_remise | null;
}

export interface MARCHE_marche {
  __typename: "Marche";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  marcheType: MarcheType | null;
  avecPalier: boolean | null;
  status: MarcheStatus | null;
  nbAdhesion: number | null;
  nbPalier: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: MARCHE_marche_commandeCanal | null;
  canalArticles: (MARCHE_marche_canalArticles | null)[] | null;
  laboratoires: (MARCHE_marche_laboratoires | null)[] | null;
  groupeClients: (MARCHE_marche_groupeClients | null)[] | null;
}

export interface MARCHE {
  marche: MARCHE_marche | null;
}

export interface MARCHEVariables {
  id: string;
}
