import gql from 'graphql-tag';

export const DO_CREATE_UPDATE_RESSOURCE = gql`
  mutation CREATE_UPDATE_RESSOURCE($input: RessourceInput!) {
    createUpdateRessource(input: $input) {
      id
      typeRessource
      item {
        id
        code
        name
        codeItem
      }
      code
      libelle
      dateChargement
      url
    }
  }
`;

export const DO_DELETE_RESSOURCE = gql`
  mutation DELETE_RESSOURCE($id: ID!) {
    softDeleteRessource(id: $id) {
      id
    }
  }
`;

export const DO_DELETE_RESSOURCES = gql`
  mutation DELETE_RESSOURCES($ids: [ID!]!) {
    softDeleteRessources(ids: $ids) {
      id
    }
  }
`;
