/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_RESSOURCES
// ====================================================

export interface DELETE_RESSOURCES_softDeleteRessources {
  __typename: "Ressource";
  id: string;
}

export interface DELETE_RESSOURCES {
  softDeleteRessources: (DELETE_RESSOURCES_softDeleteRessources | null)[] | null;
}

export interface DELETE_RESSOURCESVariables {
  ids: string[];
}
