/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RessourceInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_RESSOURCE
// ====================================================

export interface CREATE_UPDATE_RESSOURCE_createUpdateRessource_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
}

export interface CREATE_UPDATE_RESSOURCE_createUpdateRessource {
  __typename: "Ressource";
  id: string;
  typeRessource: string | null;
  item: CREATE_UPDATE_RESSOURCE_createUpdateRessource_item | null;
  code: string | null;
  libelle: string | null;
  dateChargement: any | null;
  url: string | null;
}

export interface CREATE_UPDATE_RESSOURCE {
  createUpdateRessource: CREATE_UPDATE_RESSOURCE_createUpdateRessource | null;
}

export interface CREATE_UPDATE_RESSOURCEVariables {
  input: RessourceInput;
}
