/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_RESSOURCE
// ====================================================

export interface DELETE_RESSOURCE_softDeleteRessource {
  __typename: "Ressource";
  id: string;
}

export interface DELETE_RESSOURCE {
  softDeleteRessource: DELETE_RESSOURCE_softDeleteRessource | null;
}

export interface DELETE_RESSOURCEVariables {
  id: string;
}
