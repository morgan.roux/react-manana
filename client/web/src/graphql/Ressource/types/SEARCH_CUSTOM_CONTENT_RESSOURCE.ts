/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_RESSOURCE
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_RESSOURCE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_CUSTOM_CONTENT_RESSOURCE_search_data_Ressource_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_RESSOURCE_search_data_Ressource {
  __typename: "Ressource";
  id: string;
  typeRessource: string | null;
  item: SEARCH_CUSTOM_CONTENT_RESSOURCE_search_data_Ressource_item | null;
  code: string | null;
  libelle: string | null;
  dateChargement: any | null;
  url: string | null;
}

export type SEARCH_CUSTOM_CONTENT_RESSOURCE_search_data = SEARCH_CUSTOM_CONTENT_RESSOURCE_search_data_Action | SEARCH_CUSTOM_CONTENT_RESSOURCE_search_data_Ressource;

export interface SEARCH_CUSTOM_CONTENT_RESSOURCE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_RESSOURCE_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_RESSOURCE {
  search: SEARCH_CUSTOM_CONTENT_RESSOURCE_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_RESSOURCEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
