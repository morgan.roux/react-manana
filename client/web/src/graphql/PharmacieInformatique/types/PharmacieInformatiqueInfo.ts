/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePrestataire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: PharmacieInformatiqueInfo
// ====================================================

export interface PharmacieInformatiqueInfo_logiciel_ssii {
  __typename: "SSII";
  id: string;
  nom: string;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  mail: string | null;
  web: string | null;
  commentaire: string | null;
}

export interface PharmacieInformatiqueInfo_logiciel {
  __typename: "Logiciel";
  id: string;
  nom: string | null;
  os: string | null;
  osVersion: string | null;
  commentaire: string | null;
  ssii: PharmacieInformatiqueInfo_logiciel_ssii | null;
}

export interface PharmacieInformatiqueInfo_automate1_prestataire {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInformatiqueInfo_automate1 {
  __typename: "Automate";
  id: string;
  modeleAutomate: string | null;
  dateCommercialisation: any | null;
  prestataire: PharmacieInformatiqueInfo_automate1_prestataire | null;
}

export interface PharmacieInformatiqueInfo_automate2_prestataire {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInformatiqueInfo_automate2 {
  __typename: "Automate";
  id: string;
  modeleAutomate: string | null;
  dateCommercialisation: any | null;
  prestataire: PharmacieInformatiqueInfo_automate2_prestataire | null;
}

export interface PharmacieInformatiqueInfo_automate3_prestataire {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInformatiqueInfo_automate3 {
  __typename: "Automate";
  id: string;
  modeleAutomate: string | null;
  dateCommercialisation: any | null;
  prestataire: PharmacieInformatiqueInfo_automate3_prestataire | null;
}

export interface PharmacieInformatiqueInfo {
  __typename: "PharmacieInformatique";
  id: string;
  numVersion: string | null;
  dateLogiciel: string | null;
  nbrePoste: number | null;
  nbreComptoir: number | null;
  nbreBackOffice: number | null;
  nbreBureau: number | null;
  commentaire: string | null;
  logiciel: PharmacieInformatiqueInfo_logiciel | null;
  automate1: PharmacieInformatiqueInfo_automate1 | null;
  dateInstallation1: any | null;
  automate2: PharmacieInformatiqueInfo_automate2 | null;
  dateInstallation2: any | null;
  automate3: PharmacieInformatiqueInfo_automate3 | null;
  dateInstallation3: any | null;
}
