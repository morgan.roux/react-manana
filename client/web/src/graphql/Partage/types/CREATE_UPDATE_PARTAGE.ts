/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PartageInput, PartageType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_PARTAGE
// ====================================================

export interface CREATE_UPDATE_PARTAGE_createUpdatePartage_item {
  __typename: "Item";
  code: string | null;
}

export interface CREATE_UPDATE_PARTAGE_createUpdatePartage_userPartageant {
  __typename: "User";
  userName: string | null;
}

export interface CREATE_UPDATE_PARTAGE_createUpdatePartage {
  __typename: "Partage";
  id: string;
  typePartage: PartageType;
  item: CREATE_UPDATE_PARTAGE_createUpdatePartage_item | null;
  userPartageant: CREATE_UPDATE_PARTAGE_createUpdatePartage_userPartageant | null;
}

export interface CREATE_UPDATE_PARTAGE {
  createUpdatePartage: CREATE_UPDATE_PARTAGE_createUpdatePartage;
}

export interface CREATE_UPDATE_PARTAGEVariables {
  input: PartageInput;
}
