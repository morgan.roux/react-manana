/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sexe } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: GROUPEMENT_AVATARS
// ====================================================

export interface GROUPEMENT_AVATARS_groupementAvatars_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
}

export interface GROUPEMENT_AVATARS_groupementAvatars {
  __typename: "Avatar";
  id: string;
  idGroupement: string | null;
  codeSexe: Sexe | null;
  description: string | null;
  fichier: GROUPEMENT_AVATARS_groupementAvatars_fichier | null;
}

export interface GROUPEMENT_AVATARS {
  groupementAvatars: (GROUPEMENT_AVATARS_groupementAvatars | null)[] | null;
}
