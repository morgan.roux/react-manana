import gql from 'graphql-tag';
import { HELLOID_GROUP, HELLOID_GROUP_INFO } from './fragment';

export const GET_HELLOID_GROUPS = gql`
  query HELLOIDGROUPS($idgroupement: ID!) {
    helloidGroups(idgroupement: $idgroupement) {
        ...HelloidGroupInfo
    }
  }
  ${HELLOID_GROUP}
`;

export const GET_ALL_HELLOID_GROUPS = gql`
  query ALL_HELLOID_GROUPS($idgroupement: ID!) {
    allHelloidGroupsInfo(idgroupement: $idgroupement) {
        ...HelloidGroupInfoInfo
    }
  }
  ${HELLOID_GROUP_INFO}
`;
