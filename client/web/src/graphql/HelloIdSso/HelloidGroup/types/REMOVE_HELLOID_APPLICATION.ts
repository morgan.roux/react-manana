/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: REMOVE_HELLOID_APPLICATION
// ====================================================

export interface REMOVE_HELLOID_APPLICATION_removeApplicationToHelloidGroup_application {
  __typename: "Application";
  applicationGUID: string;
  name: string | null;
  type: string | null;
  url: string | null;
  icon: string | null;
  needConfiguration: boolean | null;
  options: number | null;
  settingOptions: number | null;
  isNew: boolean | null;
  lastTimeUsed: any | null;
  nrofTimesUsed: number | null;
  helloIdurl: string | null;
  iconlink: string | null;
}

export interface REMOVE_HELLOID_APPLICATION_removeApplicationToHelloidGroup {
  __typename: "HelloidGroup";
  name: string | null;
  groupGuid: string | null;
  managedByUserGuid: string | null;
  immutableId: string | null;
  isEnabled: boolean | null;
  isDefault: boolean | null;
  isQrEnabled: boolean | null;
  isDeleted: boolean | null;
  source: string | null;
  application: (REMOVE_HELLOID_APPLICATION_removeApplicationToHelloidGroup_application | null)[] | null;
}

export interface REMOVE_HELLOID_APPLICATION {
  removeApplicationToHelloidGroup: REMOVE_HELLOID_APPLICATION_removeApplicationToHelloidGroup | null;
}

export interface REMOVE_HELLOID_APPLICATIONVariables {
  idGroupement: string;
  groupGuid?: string | null;
  applicationsGuid?: string[] | null;
}
