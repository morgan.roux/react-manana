/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: HelloidGroupInfoInfo
// ====================================================

export interface HelloidGroupInfoInfo {
  __typename: "HelloidGroupInfo";
  name: string | null;
  groupGuid: string | null;
  managedByUserGuid: string | null;
  immutableId: string | null;
  isEnabled: boolean | null;
  isDefault: boolean | null;
  isQrEnabled: boolean | null;
  isDeleted: boolean | null;
  source: string | null;
}
