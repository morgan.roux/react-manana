/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_HELLOID_GROUP
// ====================================================

export interface DELETE_HELLOID_GROUP_deleteHelloIdGroupInfo {
  __typename: "HelloidGroupInfo";
  name: string | null;
  groupGuid: string | null;
  managedByUserGuid: string | null;
  immutableId: string | null;
  isEnabled: boolean | null;
  isDefault: boolean | null;
  isQrEnabled: boolean | null;
  isDeleted: boolean | null;
  source: string | null;
}

export interface DELETE_HELLOID_GROUP {
  deleteHelloIdGroupInfo: (DELETE_HELLOID_GROUP_deleteHelloIdGroupInfo | null)[] | null;
}

export interface DELETE_HELLOID_GROUPVariables {
  idgroupement: string;
  groupGuid: string;
}
