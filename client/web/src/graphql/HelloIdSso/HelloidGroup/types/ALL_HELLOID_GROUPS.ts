/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ALL_HELLOID_GROUPS
// ====================================================

export interface ALL_HELLOID_GROUPS_allHelloidGroupsInfo {
  __typename: "HelloidGroupInfo";
  name: string | null;
  groupGuid: string | null;
  managedByUserGuid: string | null;
  immutableId: string | null;
  isEnabled: boolean | null;
  isDefault: boolean | null;
  isQrEnabled: boolean | null;
  isDeleted: boolean | null;
  source: string | null;
}

export interface ALL_HELLOID_GROUPS {
  allHelloidGroupsInfo: (ALL_HELLOID_GROUPS_allHelloidGroupsInfo | null)[] | null;
}

export interface ALL_HELLOID_GROUPSVariables {
  idgroupement: string;
}
