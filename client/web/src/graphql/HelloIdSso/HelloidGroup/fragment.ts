import gql from 'graphql-tag';
import { HELLOID_APPLICATION_INFO } from '../../HelloIdApplication/fragment';

export const HELLOID_GROUP = gql`
  fragment HelloidGroupInfo on HelloidGroup {
    name
    groupGuid
    managedByUserGuid
    immutableId
    isEnabled
    isDefault
    isQrEnabled
    isDeleted
    source
    application{
        ...HelloidApplication
    }
  }
  ${HELLOID_APPLICATION_INFO}
`;

export const HELLOID_GROUP_INFO = gql`
  fragment HelloidGroupInfoInfo on HelloidGroupInfo {
    name
    groupGuid
    managedByUserGuid
    immutableId
    isEnabled
    isDefault
    isQrEnabled
    isDeleted
    source
  }
`;