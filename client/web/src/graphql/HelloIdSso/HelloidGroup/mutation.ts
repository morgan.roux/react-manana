import gql from 'graphql-tag';
import { HELLOID_GROUP, HELLOID_GROUP_INFO } from './fragment';

export const DO_ADD_HELLOID_APPLICATION = gql`
  mutation ADD_HELLOID_APPLICATION(
    $idGroupement: ID!, 
    $groupGuid: String, 
    $applicationsGuid: [String!], 
    ) {
      addApplicationToHelloidGroup(
        idGroupement: $idGroupement, 
        groupGuid: $groupGuid, 
        applicationsGuid: $applicationsGuid, 
        ) {
          ...HelloidGroupInfo
    }
  }
  ${HELLOID_GROUP}
`;

export const DO_REMOVE_HELLOID_APPLICATION = gql`
  mutation REMOVE_HELLOID_APPLICATION(
    $idGroupement: ID!, 
    $groupGuid: String, 
    $applicationsGuid: [String!], 
    ) {
      removeApplicationToHelloidGroup(
        idGroupement: $idGroupement, 
        groupGuid: $groupGuid, 
        applicationsGuid: $applicationsGuid, 
        ) {
          ...HelloidGroupInfo
    }
  }
  ${HELLOID_GROUP}
`;


export const DO_CREATE_HELLOID_GROUP = gql`
  mutation CREATE_HELLOID_GROUP(
    $idgroupement: ID!, 
    $Name: String, 
    $IsEnabled: Boolean, 
    $IsDefault: Boolean, 
    $IsQrEnabled: Boolean,
    $ApplicationGUIDs: [String]
    ) {
      createHelloIdGroup(
        idgroupement: $idgroupement, 
        Name: $Name, 
        IsEnabled: $IsEnabled, 
        IsDefault: $IsDefault, 
        IsQrEnabled: $IsQrEnabled,
        ApplicationGUIDs: $ApplicationGUIDs
      ){
        ...HelloidGroupInfo
    }
  }
  ${HELLOID_GROUP}
`;

export const DO_DELETE_HELLOID_GROUP = gql`
  mutation DELETE_HELLOID_GROUP(
    $idgroupement: ID!, 
    $groupGuid: String! 
    ) {
      deleteHelloIdGroupInfo(
        idgroupement: $idgroupement, 
        groupGuid: $groupGuid, 
      ){
        ...HelloidGroupInfoInfo
    }
  }
  ${HELLOID_GROUP_INFO}
`;