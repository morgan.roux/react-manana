/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: HelloIdSsoInfo
// ====================================================

export interface HelloIdSsoInfo_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface HelloIdSsoInfo_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: HelloIdSsoInfo_groupement_defaultPharmacie_departement_region | null;
}

export interface HelloIdSsoInfo_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: HelloIdSsoInfo_groupement_defaultPharmacie_departement | null;
}

export interface HelloIdSsoInfo_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface HelloIdSsoInfo_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: HelloIdSsoInfo_groupement_groupementLogo_fichier | null;
}

export interface HelloIdSsoInfo_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: HelloIdSsoInfo_groupement_defaultPharmacie | null;
  groupementLogo: HelloIdSsoInfo_groupement_groupementLogo | null;
}

export interface HelloIdSsoInfo {
  __typename: "HelloIdSso";
  id: string;
  groupement: HelloIdSsoInfo_groupement | null;
  helloIdUrl: string | null;
  helloIDConsumerUrl: string | null;
  x509Certificate: string | null;
  apiKey: string | null;
  apiPass: string | null;
}
