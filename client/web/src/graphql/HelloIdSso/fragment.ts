import gql from 'graphql-tag';

import { GROUPEMENT_INFO } from '../Groupement/fragement';

export const HELLOIDSSO_FRAGMENT = gql`
  fragment HelloIdSsoInfo on HelloIdSso {
    id
    groupement{
        ...GroupementInfo
    }
    helloIdUrl
    helloIDConsumerUrl
    x509Certificate
    apiKey
    apiPass
  }
  ${GROUPEMENT_INFO}
`;