import gql from 'graphql-tag';
import { COMMANDE_CANAL_INFO_FRAGMENT } from './fragment';

export const GET_COMMANDCANALS = gql`
  query commandeCanals {
    commandeCanals {
      ...CommandeCanalInfo
    }
  }
  ${COMMANDE_CANAL_INFO_FRAGMENT}
`;

export const GET_MINIM_COMMANDE_CANALS = gql`
  query MINIM_COMMANDE_CANALS {
    commandeCanals {
      id
      libelle
      code
    }
  }
`;
