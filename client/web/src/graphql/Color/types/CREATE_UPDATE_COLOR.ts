/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CouleurInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_COLOR
// ====================================================

export interface CREATE_UPDATE_COLOR_createUpdateCouleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  dateCreation: any;
  dateModification: any;
}

export interface CREATE_UPDATE_COLOR {
  createUpdateCouleur: CREATE_UPDATE_COLOR_createUpdateCouleur;
}

export interface CREATE_UPDATE_COLORVariables {
  input: CouleurInput;
}
