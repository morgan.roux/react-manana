/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ColorInfo
// ====================================================

export interface ColorInfo {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  dateCreation: any;
  dateModification: any;
}
