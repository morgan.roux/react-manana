/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: COLOR
// ====================================================

export interface COLOR_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  dateCreation: any;
  dateModification: any;
}

export interface COLOR {
  couleur: COLOR_couleur | null;
}

export interface COLORVariables {
  id: string;
}
