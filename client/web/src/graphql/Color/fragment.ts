import gql from 'graphql-tag';

export const COLOR_INFO = gql`
  fragment ColorInfo on Couleur {
    id
    code
    libelle
    dateCreation
    dateModification
  }
`;
