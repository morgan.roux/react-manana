import gql from 'graphql-tag';
import { COLOR_INFO } from './fragment';

export const DO_CREATE_UPDATE_COLOR = gql`
  mutation CREATE_UPDATE_COLOR($input: CouleurInput!) {
    createUpdateCouleur(input: $input) {
      ...ColorInfo
    }
  }
  ${COLOR_INFO}
`;

export const DO_SOFT_DELETE_COULEURS = gql`
  mutation SOFT_DELETE_COULEURS($ids: [ID!]!) {
    softDeleteCouleurs(ids: $ids) {
      ...ColorInfo
    }
  }
  ${COLOR_INFO}
`;
