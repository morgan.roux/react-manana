import gql from 'graphql-tag';

export const TODO_ETIQUETTE_INFO_FRAGMENT = gql`
  fragment TodoEtiquetteInfo on TodoEtiquette {
    id
    _name: nom
    ordre
    isInFavoris
    isRemoved
    isShared
    nbAction(actionStatus: $actionStatus)
    activeActions
    couleur {
      id
      code
    }
    participants {
      id
      email
      login
      userName
    }
  }
`;
