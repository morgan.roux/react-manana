/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ETIQUETTE
// ====================================================

export interface ETIQUETTE_todoEtiquette_couleur {
  __typename: "Couleur";
  code: string;
  id: string;
  libelle: string;
}

export interface ETIQUETTE_todoEtiquette {
  __typename: "TodoEtiquette";
  id: string;
  nom: string;
  nbAction: number;
  activeActions: boolean | null;
  isRemoved: boolean | null;
  couleur: ETIQUETTE_todoEtiquette_couleur | null;
}

export interface ETIQUETTE {
  todoEtiquette: ETIQUETTE_todoEtiquette | null;
}

export interface ETIQUETTEVariables {
  id: string;
}
