/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AddEtiquetteParticipantsInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: ADD_PARTICIPANTS_TO_ETIQUETTE
// ====================================================

export interface ADD_PARTICIPANTS_TO_ETIQUETTE_addParticipantsToEtiquette_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
}

export interface ADD_PARTICIPANTS_TO_ETIQUETTE_addParticipantsToEtiquette_participants {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface ADD_PARTICIPANTS_TO_ETIQUETTE_addParticipantsToEtiquette {
  __typename: "TodoEtiquette";
  id: string;
  _name: string;
  ordre: number | null;
  isInFavoris: boolean | null;
  isRemoved: boolean | null;
  isShared: boolean;
  nbAction: number;
  activeActions: boolean | null;
  couleur: ADD_PARTICIPANTS_TO_ETIQUETTE_addParticipantsToEtiquette_couleur | null;
  participants: ADD_PARTICIPANTS_TO_ETIQUETTE_addParticipantsToEtiquette_participants[] | null;
}

export interface ADD_PARTICIPANTS_TO_ETIQUETTE {
  addParticipantsToEtiquette: ADD_PARTICIPANTS_TO_ETIQUETTE_addParticipantsToEtiquette;
}

export interface ADD_PARTICIPANTS_TO_ETIQUETTEVariables {
  input: AddEtiquetteParticipantsInput;
  actionStatus?: string | null;
}
