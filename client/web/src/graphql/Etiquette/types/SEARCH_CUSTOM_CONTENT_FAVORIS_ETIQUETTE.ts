/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Groupement" | "Pharmacie" | "Contact" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "Ticket" | "TicketOrigine" | "TicketMotif" | "TicketStatut" | "TicketChangementCible" | "TicketChangementStatut" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE_search_data_TodoEtiquette_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
}

export interface SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE_search_data_TodoEtiquette_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE_search_data_TodoEtiquette {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  couleur: SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE_search_data_TodoEtiquette_couleur | null;
  isInFavoris: boolean | null;
  isRemoved: boolean | null;
  userCreation: SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE_search_data_TodoEtiquette_userCreation | null;
}

export type SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE_search_data = SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE_search_data_Action | SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE_search_data_TodoEtiquette;

export interface SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE {
  search: SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTE_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_FAVORIS_ETIQUETTEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
