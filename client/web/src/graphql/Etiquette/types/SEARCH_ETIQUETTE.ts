/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_ETIQUETTE
// ====================================================

export interface SEARCH_ETIQUETTE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_ETIQUETTE_search_data_TodoEtiquette_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
}

export interface SEARCH_ETIQUETTE_search_data_TodoEtiquette_participants {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface SEARCH_ETIQUETTE_search_data_TodoEtiquette {
  __typename: "TodoEtiquette";
  id: string;
  _name: string;
  ordre: number | null;
  isInFavoris: boolean | null;
  isRemoved: boolean | null;
  isShared: boolean;
  nbAction: number;
  activeActions: boolean | null;
  couleur: SEARCH_ETIQUETTE_search_data_TodoEtiquette_couleur | null;
  participants: SEARCH_ETIQUETTE_search_data_TodoEtiquette_participants[] | null;
}

export type SEARCH_ETIQUETTE_search_data = SEARCH_ETIQUETTE_search_data_Action | SEARCH_ETIQUETTE_search_data_TodoEtiquette;

export interface SEARCH_ETIQUETTE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_ETIQUETTE_search_data | null)[] | null;
}

export interface SEARCH_ETIQUETTE {
  search: SEARCH_ETIQUETTE_search | null;
}

export interface SEARCH_ETIQUETTEVariables {
  query?: any | null;
  type?: (string | null)[] | null;
  sortBy?: any | null;
  actionStatus?: string | null;
}
