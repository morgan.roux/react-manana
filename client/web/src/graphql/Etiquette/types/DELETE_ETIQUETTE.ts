/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DELETE_ETIQUETTE
// ====================================================

export interface DELETE_ETIQUETTE_softDeleteTodoEtiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
}

export interface DELETE_ETIQUETTE_softDeleteTodoEtiquettes_participants {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface DELETE_ETIQUETTE_softDeleteTodoEtiquettes {
  __typename: "TodoEtiquette";
  id: string;
  _name: string;
  ordre: number | null;
  isInFavoris: boolean | null;
  isRemoved: boolean | null;
  isShared: boolean;
  nbAction: number;
  activeActions: boolean | null;
  couleur: DELETE_ETIQUETTE_softDeleteTodoEtiquettes_couleur | null;
  participants: DELETE_ETIQUETTE_softDeleteTodoEtiquettes_participants[] | null;
}

export interface DELETE_ETIQUETTE {
  softDeleteTodoEtiquettes: DELETE_ETIQUETTE_softDeleteTodoEtiquettes[];
}

export interface DELETE_ETIQUETTEVariables {
  ids: string[];
  actionStatus?: string | null;
}
