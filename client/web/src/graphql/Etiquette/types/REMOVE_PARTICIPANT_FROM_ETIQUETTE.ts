/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RemoveEtiquetteParticipantInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: REMOVE_PARTICIPANT_FROM_ETIQUETTE
// ====================================================

export interface REMOVE_PARTICIPANT_FROM_ETIQUETTE_removeParticipantFromEtiquette_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
}

export interface REMOVE_PARTICIPANT_FROM_ETIQUETTE_removeParticipantFromEtiquette_participants {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface REMOVE_PARTICIPANT_FROM_ETIQUETTE_removeParticipantFromEtiquette {
  __typename: "TodoEtiquette";
  id: string;
  _name: string;
  ordre: number | null;
  isInFavoris: boolean | null;
  isRemoved: boolean | null;
  isShared: boolean;
  nbAction: number;
  activeActions: boolean | null;
  couleur: REMOVE_PARTICIPANT_FROM_ETIQUETTE_removeParticipantFromEtiquette_couleur | null;
  participants: REMOVE_PARTICIPANT_FROM_ETIQUETTE_removeParticipantFromEtiquette_participants[] | null;
}

export interface REMOVE_PARTICIPANT_FROM_ETIQUETTE {
  removeParticipantFromEtiquette: REMOVE_PARTICIPANT_FROM_ETIQUETTE_removeParticipantFromEtiquette;
}

export interface REMOVE_PARTICIPANT_FROM_ETIQUETTEVariables {
  input: RemoveEtiquetteParticipantInput;
  actionStatus?: string | null;
}
