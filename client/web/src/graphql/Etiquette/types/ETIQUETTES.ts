/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ETIQUETTES
// ====================================================

export interface ETIQUETTES_todoEtiquettes_couleur {
  __typename: "Couleur";
  code: string;
  id: string;
  libelle: string;
}

export interface ETIQUETTES_todoEtiquettes {
  __typename: "TodoEtiquette";
  id: string;
  nom: string;
  nbAction: number;
  activeActions: boolean | null;
  isRemoved: boolean | null;
  couleur: ETIQUETTES_todoEtiquettes_couleur | null;
}

export interface ETIQUETTES {
  todoEtiquettes: ETIQUETTES_todoEtiquettes[];
}

export interface ETIQUETTESVariables {
  isRemoved?: boolean | null;
}
