/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionStatus, TypeProject, TicketVisibilite, TicketAppel, OrigineType, StatusTicket } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_SHARED_INFO
// ====================================================

export interface SEARCH_SHARED_INFO_search_data_TodoActionType {
  __typename: "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaisonUserConcernee" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_SHARED_INFO_search_data_Action_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface SEARCH_SHARED_INFO_search_data_Action_origine {
  __typename: "TodoActionOrigine";
  id: string;
  code: string;
  libelle: string;
}

export interface SEARCH_SHARED_INFO_search_data_Action_section {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
}

export interface SEARCH_SHARED_INFO_search_data_Action_actionParent {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  nbComment: number;
}

export interface SEARCH_SHARED_INFO_search_data_Action_subActions_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface SEARCH_SHARED_INFO_search_data_Action_subActions {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: SEARCH_SHARED_INFO_search_data_Action_subActions_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbComment: number;
}

export interface SEARCH_SHARED_INFO_search_data_Action_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_Action_project {
  __typename: "Project";
  id: string;
  name: string | null;
  typeProject: TypeProject | null;
}

export interface SEARCH_SHARED_INFO_search_data_Action_userCreation_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_Action_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: SEARCH_SHARED_INFO_search_data_Action_userCreation_role | null;
}

export interface SEARCH_SHARED_INFO_search_data_Action_assignedUsers_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_Action_assignedUsers_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_SHARED_INFO_search_data_Action_assignedUsers_userPhoto_fichier | null;
}

export interface SEARCH_SHARED_INFO_search_data_Action_assignedUsers {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  userPhoto: SEARCH_SHARED_INFO_search_data_Action_assignedUsers_userPhoto | null;
}

export interface SEARCH_SHARED_INFO_search_data_Action_etiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface SEARCH_SHARED_INFO_search_data_Action_etiquettes {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  nom: string;
  isRemoved: boolean | null;
  couleur: SEARCH_SHARED_INFO_search_data_Action_etiquettes_couleur | null;
}

export interface SEARCH_SHARED_INFO_search_data_Action {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  _priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: SEARCH_SHARED_INFO_search_data_Action_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  idItemAssocie: string | null;
  nbComment: number;
  origine: SEARCH_SHARED_INFO_search_data_Action_origine | null;
  section: SEARCH_SHARED_INFO_search_data_Action_section | null;
  actionParent: SEARCH_SHARED_INFO_search_data_Action_actionParent | null;
  subActions: SEARCH_SHARED_INFO_search_data_Action_subActions[] | null;
  item: SEARCH_SHARED_INFO_search_data_Action_item | null;
  project: SEARCH_SHARED_INFO_search_data_Action_project | null;
  userCreation: SEARCH_SHARED_INFO_search_data_Action_userCreation | null;
  assignedUsers: SEARCH_SHARED_INFO_search_data_Action_assignedUsers[] | null;
  etiquettes: SEARCH_SHARED_INFO_search_data_Action_etiquettes[] | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_declarant_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_declarant_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_declarant {
  __typename: "User";
  id: string;
  userName: string | null;
  role: SEARCH_SHARED_INFO_search_data_Ticket_declarant_role | null;
  pharmacie: SEARCH_SHARED_INFO_search_data_Ticket_declarant_pharmacie | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_usersConcernees_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_usersConcernees_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_usersConcernees {
  __typename: "User";
  id: string;
  userName: string | null;
  role: SEARCH_SHARED_INFO_search_data_Ticket_usersConcernees_role | null;
  pharmacie: SEARCH_SHARED_INFO_search_data_Ticket_usersConcernees_pharmacie | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  photo: string;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_motif {
  __typename: "TicketMotif";
  id: string;
  nom: string;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_userCreation_userPersonnel_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_userCreation_userPersonnel {
  __typename: "Personnel";
  id: string;
  service: SEARCH_SHARED_INFO_search_data_Ticket_userCreation_userPersonnel_service | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  pharmacie: SEARCH_SHARED_INFO_search_data_Ticket_userCreation_pharmacie | null;
  userPersonnel: SEARCH_SHARED_INFO_search_data_Ticket_userCreation_userPersonnel | null;
  role: SEARCH_SHARED_INFO_search_data_Ticket_userCreation_role | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_Ticket {
  __typename: "Ticket";
  type: string;
  id: string;
  idOrganisation: string | null;
  nomOrganisation: string | null;
  dateHeureSaisie: any | null;
  commentaire: string | null;
  visibilite: TicketVisibilite | null;
  priority: number | null;
  typeAppel: TicketAppel | null;
  declarant: SEARCH_SHARED_INFO_search_data_Ticket_declarant | null;
  usersConcernees: (SEARCH_SHARED_INFO_search_data_Ticket_usersConcernees | null)[] | null;
  smyley: SEARCH_SHARED_INFO_search_data_Ticket_smyley | null;
  motif: SEARCH_SHARED_INFO_search_data_Ticket_motif | null;
  userCreation: SEARCH_SHARED_INFO_search_data_Ticket_userCreation | null;
  fichiers: (SEARCH_SHARED_INFO_search_data_Ticket_fichiers | null)[] | null;
  origineType: OrigineType | null;
  statusTicket: StatusTicket | null;
}

export interface SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie_Laboratoire {
  __typename: "Laboratoire";
  id: string;
  type: string;
  nomLabo: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie_Service {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie_GroupeAmis {
  __typename: "GroupeAmis";
  type: string;
  id: string;
  nom: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie_User {
  __typename: "User";
  type: string;
  id: string;
  userName: string | null;
}

export type SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie = SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie_Action | SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie_Laboratoire | SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie_Service | SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie_Partenaire | SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie_GroupeAmis | SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie_User;

export interface SEARCH_SHARED_INFO_search_data_InformationLiaison_declarant {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_InformationLiaison_ficheReclamation {
  __typename: "Ticket";
  id: string;
  commentaire: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_InformationLiaison_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface SEARCH_SHARED_INFO_search_data_InformationLiaison {
  __typename: "InformationLiaison";
  id: string;
  idOrigine: string | null;
  idOrigineAssocie: string | null;
  origineAssocie: SEARCH_SHARED_INFO_search_data_InformationLiaison_origineAssocie | null;
  _statusInformation: string;
  _priorityInfo: string | null;
  _descriptionInfo: string;
  _nbComment: number | null;
  nbCollegue: number | null;
  nbLue: number | null;
  declarant: SEARCH_SHARED_INFO_search_data_InformationLiaison_declarant | null;
  ficheReclamation: SEARCH_SHARED_INFO_search_data_InformationLiaison_ficheReclamation | null;
  userCreation: SEARCH_SHARED_INFO_search_data_InformationLiaison_userCreation | null;
  _dateCreationInfo: any;
}

export type SEARCH_SHARED_INFO_search_data = SEARCH_SHARED_INFO_search_data_TodoActionType | SEARCH_SHARED_INFO_search_data_Action | SEARCH_SHARED_INFO_search_data_Ticket | SEARCH_SHARED_INFO_search_data_InformationLiaison;

export interface SEARCH_SHARED_INFO_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_SHARED_INFO_search_data | null)[] | null;
}

export interface SEARCH_SHARED_INFO {
  search: SEARCH_SHARED_INFO_search | null;
}

export interface SEARCH_SHARED_INFOVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  filterBy?: any | null;
  sortBy?: any | null;
  skip?: number | null;
  take?: number | null;
}
