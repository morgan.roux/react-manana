/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePartenaire, StatutPartenaire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_PARTENAIRE
// ====================================================

export interface SEARCH_PARTENAIRE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_PARTENAIRE_search_data_Partenaire_partenaireServiceSuite {
  __typename: "PartenaireServiceSuite";
  id: string;
  adresse1: string | null;
  codePostal: string | null;
  telephone: string | null;
  ville: string | null;
  email: string | null;
  dateCreation: any | null;
}

export interface SEARCH_PARTENAIRE_search_data_Partenaire_partenaireServicePartenaire {
  __typename: "PartenaireServicePartenaire";
  id: string;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
  dateFinPartenaire: any;
  dateDebutPartenaire: any;
}

export interface SEARCH_PARTENAIRE_search_data_Partenaire_services {
  __typename: "ServicePartenaire";
  id: string;
  type: string;
  nom: string;
}

export interface SEARCH_PARTENAIRE_search_data_Partenaire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
  partenaireServiceSuite: SEARCH_PARTENAIRE_search_data_Partenaire_partenaireServiceSuite | null;
  partenaireServicePartenaire: SEARCH_PARTENAIRE_search_data_Partenaire_partenaireServicePartenaire | null;
  services: (SEARCH_PARTENAIRE_search_data_Partenaire_services | null)[] | null;
}

export type SEARCH_PARTENAIRE_search_data = SEARCH_PARTENAIRE_search_data_Action | SEARCH_PARTENAIRE_search_data_Partenaire;

export interface SEARCH_PARTENAIRE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_PARTENAIRE_search_data | null)[] | null;
}

export interface SEARCH_PARTENAIRE {
  search: SEARCH_PARTENAIRE_search | null;
}

export interface SEARCH_PARTENAIREVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  filterBy?: any | null;
  sortBy?: any | null;
  skip?: number | null;
  take?: number | null;
}
