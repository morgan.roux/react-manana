/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SHARED_COMMENTS
// ====================================================

export interface SHARED_COMMENTS_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SHARED_COMMENTS_search_data_Comment_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface SHARED_COMMENTS_search_data_Comment_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SHARED_COMMENTS_search_data_Comment_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SHARED_COMMENTS_search_data_Comment_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SHARED_COMMENTS_search_data_Comment_user_userPhoto_fichier | null;
}

export interface SHARED_COMMENTS_search_data_Comment_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SHARED_COMMENTS_search_data_Comment_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SHARED_COMMENTS_search_data_Comment_user_role | null;
  userPhoto: SHARED_COMMENTS_search_data_Comment_user_userPhoto | null;
  pharmacie: SHARED_COMMENTS_search_data_Comment_user_pharmacie | null;
}

export interface SHARED_COMMENTS_search_data_Comment {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: SHARED_COMMENTS_search_data_Comment_fichiers[] | null;
  user: SHARED_COMMENTS_search_data_Comment_user;
}

export type SHARED_COMMENTS_search_data = SHARED_COMMENTS_search_data_Action | SHARED_COMMENTS_search_data_Comment;

export interface SHARED_COMMENTS_search {
  __typename: "SearchResult";
  total: number;
  data: (SHARED_COMMENTS_search_data | null)[] | null;
}

export interface SHARED_COMMENTS {
  search: SHARED_COMMENTS_search | null;
}

export interface SHARED_COMMENTSVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  filterBy?: any | null;
  sortBy?: any | null;
  skip?: number | null;
  take?: number | null;
}
