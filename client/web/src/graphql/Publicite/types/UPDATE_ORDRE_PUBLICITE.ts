/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeEspace, OriginePublicite } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_ORDRE_PUBLICITE
// ====================================================

export interface UPDATE_ORDRE_PUBLICITE_updateOrdrePublicite_item {
  __typename: "Item";
  id: string;
  codeItem: string | null;
  name: string | null;
}

export interface UPDATE_ORDRE_PUBLICITE_updateOrdrePublicite_image {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  chemin: string;
}

export interface UPDATE_ORDRE_PUBLICITE_updateOrdrePublicite {
  __typename: "Publicite";
  type: string;
  id: string;
  libelle: string | null;
  description: string | null;
  typeEspace: TypeEspace | null;
  origineType: OriginePublicite | null;
  idItemSource: string | null;
  url: string | null;
  ordre: number | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  item: UPDATE_ORDRE_PUBLICITE_updateOrdrePublicite_item | null;
  image: UPDATE_ORDRE_PUBLICITE_updateOrdrePublicite_image | null;
}

export interface UPDATE_ORDRE_PUBLICITE {
  updateOrdrePublicite: UPDATE_ORDRE_PUBLICITE_updateOrdrePublicite | null;
}

export interface UPDATE_ORDRE_PUBLICITEVariables {
  id: string;
  ordre?: number | null;
}
