/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeEspace, OriginePublicite, FichierInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_PUBLICITE
// ====================================================

export interface CREATE_UPDATE_PUBLICITE_createUpdatePublicite_item {
  __typename: "Item";
  id: string;
  codeItem: string | null;
  name: string | null;
}

export interface CREATE_UPDATE_PUBLICITE_createUpdatePublicite_image {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  chemin: string;
}

export interface CREATE_UPDATE_PUBLICITE_createUpdatePublicite {
  __typename: "Publicite";
  type: string;
  id: string;
  libelle: string | null;
  description: string | null;
  typeEspace: TypeEspace | null;
  origineType: OriginePublicite | null;
  idItemSource: string | null;
  url: string | null;
  ordre: number | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  item: CREATE_UPDATE_PUBLICITE_createUpdatePublicite_item | null;
  image: CREATE_UPDATE_PUBLICITE_createUpdatePublicite_image | null;
}

export interface CREATE_UPDATE_PUBLICITE {
  /**
   * ESPACE PUBLICITAIRE
   */
  createUpdatePublicite: CREATE_UPDATE_PUBLICITE_createUpdatePublicite | null;
}

export interface CREATE_UPDATE_PUBLICITEVariables {
  id?: string | null;
  libelle?: string | null;
  description?: string | null;
  typeEspace: TypeEspace;
  origine: OriginePublicite;
  url?: string | null;
  ordre?: number | null;
  codeItem?: string | null;
  idItemSource?: string | null;
  image?: FichierInput | null;
  dateDebut?: string | null;
  dateFin?: string | null;
}
