/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeEspace, OriginePublicite } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: GET_PUBLICITE
// ====================================================

export interface GET_PUBLICITE_publicite_item {
  __typename: "Item";
  id: string;
  codeItem: string | null;
  name: string | null;
  code: string | null;
}

export interface GET_PUBLICITE_publicite_image {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  chemin: string;
}

export interface GET_PUBLICITE_publicite {
  __typename: "Publicite";
  type: string;
  id: string;
  libelle: string | null;
  description: string | null;
  typeEspace: TypeEspace | null;
  origineType: OriginePublicite | null;
  idItemSource: string | null;
  url: string | null;
  ordre: number | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  item: GET_PUBLICITE_publicite_item | null;
  image: GET_PUBLICITE_publicite_image | null;
}

export interface GET_PUBLICITE {
  publicite: GET_PUBLICITE_publicite | null;
}

export interface GET_PUBLICITEVariables {
  id: string;
}
