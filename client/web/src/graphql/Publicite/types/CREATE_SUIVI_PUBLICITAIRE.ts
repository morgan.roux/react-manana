/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeEspace, OriginePublicite } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_SUIVI_PUBLICITAIRE
// ====================================================

export interface CREATE_SUIVI_PUBLICITAIRE_createSuiviPublicitaire_user {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface CREATE_SUIVI_PUBLICITAIRE_createSuiviPublicitaire_publicite_item {
  __typename: "Item";
  id: string;
  codeItem: string | null;
  name: string | null;
}

export interface CREATE_SUIVI_PUBLICITAIRE_createSuiviPublicitaire_publicite_image {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  chemin: string;
}

export interface CREATE_SUIVI_PUBLICITAIRE_createSuiviPublicitaire_publicite {
  __typename: "Publicite";
  type: string;
  id: string;
  libelle: string | null;
  description: string | null;
  typeEspace: TypeEspace | null;
  origineType: OriginePublicite | null;
  idItemSource: string | null;
  url: string | null;
  ordre: number | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  item: CREATE_SUIVI_PUBLICITAIRE_createSuiviPublicitaire_publicite_item | null;
  image: CREATE_SUIVI_PUBLICITAIRE_createSuiviPublicitaire_publicite_image | null;
}

export interface CREATE_SUIVI_PUBLICITAIRE_createSuiviPublicitaire {
  __typename: "SuiviPublicitaire";
  id: string;
  dateClick: any | null;
  user: CREATE_SUIVI_PUBLICITAIRE_createSuiviPublicitaire_user | null;
  publicite: CREATE_SUIVI_PUBLICITAIRE_createSuiviPublicitaire_publicite | null;
}

export interface CREATE_SUIVI_PUBLICITAIRE {
  createSuiviPublicitaire: CREATE_SUIVI_PUBLICITAIRE_createSuiviPublicitaire | null;
}

export interface CREATE_SUIVI_PUBLICITAIREVariables {
  idPublicite: string;
}
