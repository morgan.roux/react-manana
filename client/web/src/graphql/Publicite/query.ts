import gql from 'graphql-tag';
import { PUBLICITE_INFO_FRAGMENT } from './fragement';

export const DO_GET_PUBLICITE = gql`
  query GET_PUBLICITE($id: ID!) {
    publicite(id: $id) {
      ...PubliciteInfo
      item {
        id
        code
        name
      }
      idItemSource
    }
  }
  ${PUBLICITE_INFO_FRAGMENT}
`;
