import gql from 'graphql-tag';
import { CANAL_GAMME_INFO } from '../CanalGamme/fragment';

export const CANAL_SOUS_GAMME_INFO = gql`
  fragment CanalSousGammeInfo on CanalSousGamme {
    id
    codeSousGamme
    libelle
    estDefaut
    gammeCommercial {
      ...CanalGammeInfo
    }
    countCanalArticles
  }
  ${CANAL_GAMME_INFO}
`;

export const CANAL_SOUS_GAMME_PRODUIT_CANAL_FRAGMENT = gql`
  fragment CanalSousGammeProduitCanal on ProduitCanal {
    sousGammeCommercial {
      id
      codeSousGamme
      libelle
      estDefaut
      countCanalArticles
    }
  }
`;
