/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: CanalSousGammeProduitCanal
// ====================================================

export interface CanalSousGammeProduitCanal_sousGammeCommercial {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
  countCanalArticles: number | null;
}

export interface CanalSousGammeProduitCanal {
  __typename: "ProduitCanal";
  sousGammeCommercial: CanalSousGammeProduitCanal_sousGammeCommercial | null;
}
