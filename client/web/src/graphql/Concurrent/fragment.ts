import gql from 'graphql-tag';

export const CONCURRENT_MINIM_INFO_FRAGMENT = gql`
  fragment ConcurrentMinimInfo on Concurrent {
    id
    nom
  }
`;

export const CONCURRENT_INFO_FRAGMENT = gql`
  fragment ConcurrentInfo on Concurrent {
    id
    nom
    adresse1
    adresse2
    cp
    ville
    telephone1
    telephone2
    fax
    commentaire
    codeMaj
    dateCreation
    dateModification
  }
`;
