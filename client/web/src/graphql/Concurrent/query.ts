import gql from 'graphql-tag';
import { CONCURRENT_MINIM_INFO_FRAGMENT } from './fragment';

export const GET_CONCURRENTS_MINIM = gql`
  query CONCURRENTS_MINIM {
    concurrents {
      ...ConcurrentMinimInfo
    }
  }
  ${CONCURRENT_MINIM_INFO_FRAGMENT}
`;
