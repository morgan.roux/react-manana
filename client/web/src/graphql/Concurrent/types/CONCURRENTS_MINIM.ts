/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CONCURRENTS_MINIM
// ====================================================

export interface CONCURRENTS_MINIM_concurrents {
  __typename: "Concurrent";
  id: string;
  nom: string | null;
}

export interface CONCURRENTS_MINIM {
  concurrents: (CONCURRENTS_MINIM_concurrents | null)[] | null;
}
