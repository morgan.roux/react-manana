/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ConcurrentMinimInfo
// ====================================================

export interface ConcurrentMinimInfo {
  __typename: "Concurrent";
  id: string;
  nom: string | null;
}
