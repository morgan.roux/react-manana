import gql from 'graphql-tag';
import { MESSAGERIE_THEME_INFO_FRAGMENT } from '../MessagerieTheme/fragment';
import { MESSAGERIE_SOURCE_INFO_FRAGMENT } from '../MessagerieSource/fragment';
import { MESSAGERIE_HISTO_INFO_FRAGMENT } from '../MessagerieHisto/fragment';
import { MESSAGERIE_FICHIER_JOINT_INFO_FRAGMENT } from '../MessagerieFichierJoint/fragment';

export const MESSAGERIE_INFO_FRAGMENT = gql`
  fragment MessagerieInfo on Messagerie {
    id
    typeFilter
    typeMessagerie
    objet
    message
    lu
    isRemoved
    dateHeureMessagerie
    dateCreation
    dateModification
    messagerieTheme {
      ...MessagerieThemeInfo
    }
    messagerieSource {
      ...MessagerieSourceInfo
    }
    userEmetteur {
      id
      email
      login
      userName
    }
    recepteurs {
      ...MessagerieHistoInfo
    }
    attachments {
      ...MessagerieFichierJointInfo
    }
  }
  ${MESSAGERIE_THEME_INFO_FRAGMENT}
  ${MESSAGERIE_SOURCE_INFO_FRAGMENT}
  ${MESSAGERIE_HISTO_INFO_FRAGMENT}
  ${MESSAGERIE_FICHIER_JOINT_INFO_FRAGMENT}
`;

export const MESSAGERIE_RESULT_INFO_FRAGMENT = gql`
  fragment MessagerieResultInfo on MessagerieResult {
    total
    data {
      ...MessagerieInfo
    }
  }
  ${MESSAGERIE_INFO_FRAGMENT}
`;
