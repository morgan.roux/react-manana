import gql from 'graphql-tag';
import { MESSAGERIE_INFO_FRAGMENT } from './fragment';

export const GET_SEND_MESSAGE_SUBSCRIPTION = gql`
  subscription SEND_MESSAGE_SUBSCRIPTION {
    messagerieEnvoyee {
      ...MessagerieInfo
    }
  }
  ${MESSAGERIE_INFO_FRAGMENT}
`;

export const GET_MESSAGE_LU_SUBSCRIPTION = gql`
  subscription MESSAGE_LU_SUBSCRIPTION {
    messagerieLu {
      ...MessagerieInfo
    }
  }
  ${MESSAGERIE_INFO_FRAGMENT}
`;

export const GET_MESSAGE_SUPPRIMEE_SUBSCRIPTION = gql`
  subscription MESSAGE_SUPPRIMEE_SUBSCRIPTION {
    messagerieSupprimee {
      ...MessagerieInfo
    }
  }
  ${MESSAGERIE_INFO_FRAGMENT}
`;
