/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL subscription operation: SEND_MESSAGE_SUBSCRIPTION
// ====================================================

export interface SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_messagerieTheme {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}

export interface SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_messagerieSource {
  __typename: "MessagerieSource";
  id: string;
  nom: string | null;
}

export interface SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_userEmetteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_recepteurs_userRecepteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_recepteurs_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_recepteurs {
  __typename: "MessagerieHisto";
  id: string;
  dateHeureLecture: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  userRecepteur: SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_recepteurs_userRecepteur | null;
  userCreation: SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_recepteurs_userCreation | null;
}

export interface SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_attachments_fichier {
  __typename: "Fichier";
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_attachments {
  __typename: "MessagerieFichierJoint";
  id: string;
  fichier: SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_attachments_fichier | null;
}

export interface SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee {
  __typename: "Messagerie";
  id: string;
  typeFilter: string | null;
  typeMessagerie: MessagerieType | null;
  objet: string | null;
  message: string | null;
  lu: boolean | null;
  isRemoved: boolean | null;
  dateHeureMessagerie: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  messagerieTheme: SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_messagerieTheme | null;
  messagerieSource: SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_messagerieSource | null;
  userEmetteur: SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_userEmetteur | null;
  recepteurs: (SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_recepteurs | null)[] | null;
  attachments: (SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee_attachments | null)[] | null;
}

export interface SEND_MESSAGE_SUBSCRIPTION {
  messagerieEnvoyee: SEND_MESSAGE_SUBSCRIPTION_messagerieEnvoyee | null;
}
