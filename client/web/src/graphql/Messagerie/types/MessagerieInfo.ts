/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: MessagerieInfo
// ====================================================

export interface MessagerieInfo_messagerieTheme {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}

export interface MessagerieInfo_messagerieSource {
  __typename: "MessagerieSource";
  id: string;
  nom: string | null;
}

export interface MessagerieInfo_userEmetteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MessagerieInfo_recepteurs_userRecepteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MessagerieInfo_recepteurs_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MessagerieInfo_recepteurs {
  __typename: "MessagerieHisto";
  id: string;
  dateHeureLecture: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  userRecepteur: MessagerieInfo_recepteurs_userRecepteur | null;
  userCreation: MessagerieInfo_recepteurs_userCreation | null;
}

export interface MessagerieInfo_attachments_fichier {
  __typename: "Fichier";
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface MessagerieInfo_attachments {
  __typename: "MessagerieFichierJoint";
  id: string;
  fichier: MessagerieInfo_attachments_fichier | null;
}

export interface MessagerieInfo {
  __typename: "Messagerie";
  id: string;
  typeFilter: string | null;
  typeMessagerie: MessagerieType | null;
  objet: string | null;
  message: string | null;
  lu: boolean | null;
  isRemoved: boolean | null;
  dateHeureMessagerie: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  messagerieTheme: MessagerieInfo_messagerieTheme | null;
  messagerieSource: MessagerieInfo_messagerieSource | null;
  userEmetteur: MessagerieInfo_userEmetteur | null;
  recepteurs: (MessagerieInfo_recepteurs | null)[] | null;
  attachments: (MessagerieInfo_attachments | null)[] | null;
}
