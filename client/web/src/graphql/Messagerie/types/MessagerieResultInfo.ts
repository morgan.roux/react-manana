/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: MessagerieResultInfo
// ====================================================

export interface MessagerieResultInfo_data_messagerieTheme {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}

export interface MessagerieResultInfo_data_messagerieSource {
  __typename: "MessagerieSource";
  id: string;
  nom: string | null;
}

export interface MessagerieResultInfo_data_userEmetteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MessagerieResultInfo_data_recepteurs_userRecepteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MessagerieResultInfo_data_recepteurs_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MessagerieResultInfo_data_recepteurs {
  __typename: "MessagerieHisto";
  id: string;
  dateHeureLecture: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  userRecepteur: MessagerieResultInfo_data_recepteurs_userRecepteur | null;
  userCreation: MessagerieResultInfo_data_recepteurs_userCreation | null;
}

export interface MessagerieResultInfo_data_attachments_fichier {
  __typename: "Fichier";
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface MessagerieResultInfo_data_attachments {
  __typename: "MessagerieFichierJoint";
  id: string;
  fichier: MessagerieResultInfo_data_attachments_fichier | null;
}

export interface MessagerieResultInfo_data {
  __typename: "Messagerie";
  id: string;
  typeFilter: string | null;
  typeMessagerie: MessagerieType | null;
  objet: string | null;
  message: string | null;
  lu: boolean | null;
  isRemoved: boolean | null;
  dateHeureMessagerie: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  messagerieTheme: MessagerieResultInfo_data_messagerieTheme | null;
  messagerieSource: MessagerieResultInfo_data_messagerieSource | null;
  userEmetteur: MessagerieResultInfo_data_userEmetteur | null;
  recepteurs: (MessagerieResultInfo_data_recepteurs | null)[] | null;
  attachments: (MessagerieResultInfo_data_attachments | null)[] | null;
}

export interface MessagerieResultInfo {
  __typename: "MessagerieResult";
  total: number;
  data: (MessagerieResultInfo_data | null)[] | null;
}
