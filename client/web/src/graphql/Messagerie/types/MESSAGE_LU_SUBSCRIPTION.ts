/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL subscription operation: MESSAGE_LU_SUBSCRIPTION
// ====================================================

export interface MESSAGE_LU_SUBSCRIPTION_messagerieLu_messagerieTheme {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}

export interface MESSAGE_LU_SUBSCRIPTION_messagerieLu_messagerieSource {
  __typename: "MessagerieSource";
  id: string;
  nom: string | null;
}

export interface MESSAGE_LU_SUBSCRIPTION_messagerieLu_userEmetteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MESSAGE_LU_SUBSCRIPTION_messagerieLu_recepteurs_userRecepteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MESSAGE_LU_SUBSCRIPTION_messagerieLu_recepteurs_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MESSAGE_LU_SUBSCRIPTION_messagerieLu_recepteurs {
  __typename: "MessagerieHisto";
  id: string;
  dateHeureLecture: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  userRecepteur: MESSAGE_LU_SUBSCRIPTION_messagerieLu_recepteurs_userRecepteur | null;
  userCreation: MESSAGE_LU_SUBSCRIPTION_messagerieLu_recepteurs_userCreation | null;
}

export interface MESSAGE_LU_SUBSCRIPTION_messagerieLu_attachments_fichier {
  __typename: "Fichier";
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface MESSAGE_LU_SUBSCRIPTION_messagerieLu_attachments {
  __typename: "MessagerieFichierJoint";
  id: string;
  fichier: MESSAGE_LU_SUBSCRIPTION_messagerieLu_attachments_fichier | null;
}

export interface MESSAGE_LU_SUBSCRIPTION_messagerieLu {
  __typename: "Messagerie";
  id: string;
  typeFilter: string | null;
  typeMessagerie: MessagerieType | null;
  objet: string | null;
  message: string | null;
  lu: boolean | null;
  isRemoved: boolean | null;
  dateHeureMessagerie: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  messagerieTheme: MESSAGE_LU_SUBSCRIPTION_messagerieLu_messagerieTheme | null;
  messagerieSource: MESSAGE_LU_SUBSCRIPTION_messagerieLu_messagerieSource | null;
  userEmetteur: MESSAGE_LU_SUBSCRIPTION_messagerieLu_userEmetteur | null;
  recepteurs: (MESSAGE_LU_SUBSCRIPTION_messagerieLu_recepteurs | null)[] | null;
  attachments: (MESSAGE_LU_SUBSCRIPTION_messagerieLu_attachments | null)[] | null;
}

export interface MESSAGE_LU_SUBSCRIPTION {
  messagerieLu: MESSAGE_LU_SUBSCRIPTION_messagerieLu | null;
}
