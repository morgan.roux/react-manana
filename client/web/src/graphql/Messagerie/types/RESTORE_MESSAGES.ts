/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: RESTORE_MESSAGES
// ====================================================

export interface RESTORE_MESSAGES_restoreMessages_messagerieTheme {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}

export interface RESTORE_MESSAGES_restoreMessages_messagerieSource {
  __typename: "MessagerieSource";
  id: string;
  nom: string | null;
}

export interface RESTORE_MESSAGES_restoreMessages_userEmetteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface RESTORE_MESSAGES_restoreMessages_recepteurs_userRecepteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface RESTORE_MESSAGES_restoreMessages_recepteurs_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface RESTORE_MESSAGES_restoreMessages_recepteurs {
  __typename: "MessagerieHisto";
  id: string;
  dateHeureLecture: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  userRecepteur: RESTORE_MESSAGES_restoreMessages_recepteurs_userRecepteur | null;
  userCreation: RESTORE_MESSAGES_restoreMessages_recepteurs_userCreation | null;
}

export interface RESTORE_MESSAGES_restoreMessages_attachments_fichier {
  __typename: "Fichier";
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface RESTORE_MESSAGES_restoreMessages_attachments {
  __typename: "MessagerieFichierJoint";
  id: string;
  fichier: RESTORE_MESSAGES_restoreMessages_attachments_fichier | null;
}

export interface RESTORE_MESSAGES_restoreMessages {
  __typename: "Messagerie";
  id: string;
  typeFilter: string | null;
  typeMessagerie: MessagerieType | null;
  objet: string | null;
  message: string | null;
  lu: boolean | null;
  isRemoved: boolean | null;
  dateHeureMessagerie: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  messagerieTheme: RESTORE_MESSAGES_restoreMessages_messagerieTheme | null;
  messagerieSource: RESTORE_MESSAGES_restoreMessages_messagerieSource | null;
  userEmetteur: RESTORE_MESSAGES_restoreMessages_userEmetteur | null;
  recepteurs: (RESTORE_MESSAGES_restoreMessages_recepteurs | null)[] | null;
  attachments: (RESTORE_MESSAGES_restoreMessages_attachments | null)[] | null;
}

export interface RESTORE_MESSAGES {
  restoreMessages: (RESTORE_MESSAGES_restoreMessages | null)[] | null;
}

export interface RESTORE_MESSAGESVariables {
  ids: string[];
}
