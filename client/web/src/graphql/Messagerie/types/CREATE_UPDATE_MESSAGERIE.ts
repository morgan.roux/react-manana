/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieInput, MessagerieType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_MESSAGERIE
// ====================================================

export interface CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_messagerieTheme {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_messagerieSource {
  __typename: "MessagerieSource";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_userEmetteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_recepteurs_userRecepteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_recepteurs_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_recepteurs {
  __typename: "MessagerieHisto";
  id: string;
  dateHeureLecture: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  userRecepteur: CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_recepteurs_userRecepteur | null;
  userCreation: CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_recepteurs_userCreation | null;
}

export interface CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_attachments_fichier {
  __typename: "Fichier";
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_attachments {
  __typename: "MessagerieFichierJoint";
  id: string;
  fichier: CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_attachments_fichier | null;
}

export interface CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie {
  __typename: "Messagerie";
  id: string;
  typeFilter: string | null;
  typeMessagerie: MessagerieType | null;
  objet: string | null;
  message: string | null;
  lu: boolean | null;
  isRemoved: boolean | null;
  dateHeureMessagerie: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  messagerieTheme: CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_messagerieTheme | null;
  messagerieSource: CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_messagerieSource | null;
  userEmetteur: CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_userEmetteur | null;
  recepteurs: (CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_recepteurs | null)[] | null;
  attachments: (CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie_attachments | null)[] | null;
}

export interface CREATE_UPDATE_MESSAGERIE {
  /**
   * Messagerie
   */
  createUpdateMessagerie: CREATE_UPDATE_MESSAGERIE_createUpdateMessagerie | null;
}

export interface CREATE_UPDATE_MESSAGERIEVariables {
  inputs?: MessagerieInput | null;
}
