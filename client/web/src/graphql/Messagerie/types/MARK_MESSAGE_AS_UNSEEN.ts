/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: MARK_MESSAGE_AS_UNSEEN
// ====================================================

export interface MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_messagerieTheme {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}

export interface MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_messagerieSource {
  __typename: "MessagerieSource";
  id: string;
  nom: string | null;
}

export interface MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_userEmetteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_recepteurs_userRecepteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_recepteurs_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_recepteurs {
  __typename: "MessagerieHisto";
  id: string;
  dateHeureLecture: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  userRecepteur: MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_recepteurs_userRecepteur | null;
  userCreation: MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_recepteurs_userCreation | null;
}

export interface MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_attachments_fichier {
  __typename: "Fichier";
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_attachments {
  __typename: "MessagerieFichierJoint";
  id: string;
  fichier: MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_attachments_fichier | null;
}

export interface MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen {
  __typename: "Messagerie";
  id: string;
  typeFilter: string | null;
  typeMessagerie: MessagerieType | null;
  objet: string | null;
  message: string | null;
  lu: boolean | null;
  isRemoved: boolean | null;
  dateHeureMessagerie: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  messagerieTheme: MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_messagerieTheme | null;
  messagerieSource: MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_messagerieSource | null;
  userEmetteur: MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_userEmetteur | null;
  recepteurs: (MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_recepteurs | null)[] | null;
  attachments: (MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen_attachments | null)[] | null;
}

export interface MARK_MESSAGE_AS_UNSEEN {
  markMessageAsUnseen: MARK_MESSAGE_AS_UNSEEN_markMessageAsUnseen | null;
}

export interface MARK_MESSAGE_AS_UNSEENVariables {
  idMessageHisto: string;
}
