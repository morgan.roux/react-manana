/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { MessagerieType, MessagerieCategory } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: MESSAGERIES
// ====================================================

export interface MESSAGERIES_messageries_data_messagerieTheme {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}

export interface MESSAGERIES_messageries_data_messagerieSource {
  __typename: "MessagerieSource";
  id: string;
  nom: string | null;
}

export interface MESSAGERIES_messageries_data_userEmetteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MESSAGERIES_messageries_data_recepteurs_userRecepteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MESSAGERIES_messageries_data_recepteurs_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
}

export interface MESSAGERIES_messageries_data_recepteurs {
  __typename: "MessagerieHisto";
  id: string;
  dateHeureLecture: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  userRecepteur: MESSAGERIES_messageries_data_recepteurs_userRecepteur | null;
  userCreation: MESSAGERIES_messageries_data_recepteurs_userCreation | null;
}

export interface MESSAGERIES_messageries_data_attachments_fichier {
  __typename: "Fichier";
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface MESSAGERIES_messageries_data_attachments {
  __typename: "MessagerieFichierJoint";
  id: string;
  fichier: MESSAGERIES_messageries_data_attachments_fichier | null;
}

export interface MESSAGERIES_messageries_data {
  __typename: "Messagerie";
  id: string;
  typeFilter: string | null;
  typeMessagerie: MessagerieType | null;
  objet: string | null;
  message: string | null;
  lu: boolean | null;
  isRemoved: boolean | null;
  dateHeureMessagerie: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  messagerieTheme: MESSAGERIES_messageries_data_messagerieTheme | null;
  messagerieSource: MESSAGERIES_messageries_data_messagerieSource | null;
  userEmetteur: MESSAGERIES_messageries_data_userEmetteur | null;
  recepteurs: (MESSAGERIES_messageries_data_recepteurs | null)[] | null;
  attachments: (MESSAGERIES_messageries_data_attachments | null)[] | null;
}

export interface MESSAGERIES_messageries {
  __typename: "MessagerieResult";
  total: number;
  data: (MESSAGERIES_messageries_data | null)[] | null;
}

export interface MESSAGERIES {
  messageries: MESSAGERIES_messageries | null;
}

export interface MESSAGERIESVariables {
  take?: number | null;
  skip?: number | null;
  typeMessagerie?: MessagerieType | null;
  category?: MessagerieCategory | null;
  year?: number | null;
  isRemoved?: boolean | null;
  lu?: boolean | null;
  orderBy?: string | null;
  typeFilter?: string | null;
}
