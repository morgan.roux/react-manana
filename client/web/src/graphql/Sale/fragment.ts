import gql from 'graphql-tag';

export const SALE_INFO_FRAGEMENT = gql`
  fragment SaleInfo on Sale {
    date
    value
  }
`;
