/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ALL_HELLOID_APPLICATION
// ====================================================

export interface ALL_HELLOID_APPLICATION_helloidApplications {
  __typename: "Application";
  applicationGUID: string;
  name: string | null;
  type: string | null;
  url: string | null;
  icon: string | null;
  needConfiguration: boolean | null;
  options: number | null;
  settingOptions: number | null;
  isNew: boolean | null;
  lastTimeUsed: any | null;
  nrofTimesUsed: number | null;
  helloIdurl: string | null;
  iconlink: string | null;
}

export interface ALL_HELLOID_APPLICATION {
  helloidApplications: (ALL_HELLOID_APPLICATION_helloidApplications | null)[] | null;
}

export interface ALL_HELLOID_APPLICATIONVariables {
  idgroupement: string;
}
