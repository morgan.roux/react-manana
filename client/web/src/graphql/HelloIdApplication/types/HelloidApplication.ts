/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: HelloidApplication
// ====================================================

export interface HelloidApplication {
  __typename: "Application";
  applicationGUID: string;
  name: string | null;
  type: string | null;
  url: string | null;
  icon: string | null;
  needConfiguration: boolean | null;
  options: number | null;
  settingOptions: number | null;
  isNew: boolean | null;
  lastTimeUsed: any | null;
  nrofTimesUsed: number | null;
  helloIdurl: string | null;
  iconlink: string | null;
}
