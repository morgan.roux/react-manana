import gql from 'graphql-tag';

export const HELLOID_APPLICATION_INFO = gql`
  fragment HelloidApplication on Application {
    applicationGUID
    name
    type
    url
    icon
    needConfiguration
    options
    settingOptions
    isNew
    lastTimeUsed
    nrofTimesUsed
    helloIdurl
    iconlink
  }
`;