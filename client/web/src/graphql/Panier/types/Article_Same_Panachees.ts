/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: Article_Same_Panachees
// ====================================================

export interface Article_Same_Panachees_articleSamePanachees_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface Article_Same_Panachees_articleSamePanachees_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface Article_Same_Panachees_articleSamePanachees_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: Article_Same_Panachees_articleSamePanachees_produit_produitTechReg_laboExploitant | null;
}

export interface Article_Same_Panachees_articleSamePanachees_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  produitCode: Article_Same_Panachees_articleSamePanachees_produit_produitCode | null;
  produitTechReg: Article_Same_Panachees_articleSamePanachees_produit_produitTechReg | null;
}

export interface Article_Same_Panachees_articleSamePanachees_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface Article_Same_Panachees_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
  produit: Article_Same_Panachees_articleSamePanachees_produit | null;
  prixPhv: number | null;
  qteStock: number | null;
  remises: (Article_Same_Panachees_articleSamePanachees_remises | null)[] | null;
}

export interface Article_Same_Panachees {
  __typename: "ProduitCanal";
  articleSamePanachees: (Article_Same_Panachees_articleSamePanachees | null)[] | null;
}
