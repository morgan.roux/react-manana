/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PanierPharmacie_Panier
// ====================================================

export interface PanierPharmacie_Panier_pharmacie {
  __typename: "Pharmacie";
  type: string;
  id: string;
  nom: string | null;
  cip: string | null;
  adresse1: string | null;
}

export interface PanierPharmacie_Panier {
  __typename: "Panier";
  pharmacie: PanierPharmacie_Panier_pharmacie | null;
}
