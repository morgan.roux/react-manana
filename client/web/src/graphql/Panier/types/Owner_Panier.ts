/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: Owner_Panier
// ====================================================

export interface Owner_Panier_owner {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface Owner_Panier {
  __typename: "Panier";
  owner: Owner_Panier_owner | null;
}
