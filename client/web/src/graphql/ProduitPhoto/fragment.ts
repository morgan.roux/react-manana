import gql from 'graphql-tag';
import { FICHIER_FRAGMENT } from '../Fichier/fragment';

export const PRODUIT_PHOTO_INFO = gql`
  fragment ProduitPhotoInfo on ProduitPhoto {
    id
    fichier {
      ...FichierInfo
    }
    dateCreation
    dateModification
  }
  ${FICHIER_FRAGMENT}
`;
