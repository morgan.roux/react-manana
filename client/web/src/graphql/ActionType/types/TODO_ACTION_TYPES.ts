/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TODO_ACTION_TYPES
// ====================================================

export interface TODO_ACTION_TYPES_todoActionTypes {
  __typename: "TodoActionType";
  libelle: string;
  id: string;
}

export interface TODO_ACTION_TYPES {
  todoActionTypes: TODO_ACTION_TYPES_todoActionTypes[];
}

export interface TODO_ACTION_TYPESVariables {
  isRemoved?: boolean | null;
}
