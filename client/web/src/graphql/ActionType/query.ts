import gql from 'graphql-tag';

export const GET_TODO_ACTION_TYPES = gql`
  query TODO_ACTION_TYPES($isRemoved: Boolean) {
    todoActionTypes(isRemoved: $isRemoved) {
      libelle
      id
    }
  }
`;
