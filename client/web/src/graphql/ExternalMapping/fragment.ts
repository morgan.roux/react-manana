import gql from 'graphql-tag';
import { USER_INFO_FRAGEMENT } from '../User/fragment';


export const EXTERNAL_MAPPING_INFO = gql`
  fragment ExternalMappingInfo on ExternalMapping {
    id
    idExternaluser
    idClient
    password
    user {
    ...UserInfo
    }
  }
  ${USER_INFO_FRAGEMENT}
`;