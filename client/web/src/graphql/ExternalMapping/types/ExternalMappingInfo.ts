/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: ExternalMappingInfo
// ====================================================

export interface ExternalMappingInfo_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ExternalMappingInfo_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface ExternalMappingInfo_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: ExternalMappingInfo_user_userPhoto_fichier | null;
}

export interface ExternalMappingInfo_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface ExternalMappingInfo_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: ExternalMappingInfo_user_role | null;
  userPhoto: ExternalMappingInfo_user_userPhoto | null;
  pharmacie: ExternalMappingInfo_user_pharmacie | null;
}

export interface ExternalMappingInfo {
  __typename: "ExternalMapping";
  id: string;
  idExternaluser: string | null;
  idClient: string | null;
  password: string | null;
  user: ExternalMappingInfo_user | null;
}
