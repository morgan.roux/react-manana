/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ExternalMappingInput, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_EXTERNAL_MAPPING
// ====================================================

export interface UPDATE_EXTERNAL_MAPPING_updateExternalMapping_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UPDATE_EXTERNAL_MAPPING_updateExternalMapping_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface UPDATE_EXTERNAL_MAPPING_updateExternalMapping_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: UPDATE_EXTERNAL_MAPPING_updateExternalMapping_user_userPhoto_fichier | null;
}

export interface UPDATE_EXTERNAL_MAPPING_updateExternalMapping_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface UPDATE_EXTERNAL_MAPPING_updateExternalMapping_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: UPDATE_EXTERNAL_MAPPING_updateExternalMapping_user_role | null;
  userPhoto: UPDATE_EXTERNAL_MAPPING_updateExternalMapping_user_userPhoto | null;
  pharmacie: UPDATE_EXTERNAL_MAPPING_updateExternalMapping_user_pharmacie | null;
}

export interface UPDATE_EXTERNAL_MAPPING_updateExternalMapping {
  __typename: "ExternalMapping";
  id: string;
  idExternaluser: string | null;
  idClient: string | null;
  password: string | null;
  user: UPDATE_EXTERNAL_MAPPING_updateExternalMapping_user | null;
}

export interface UPDATE_EXTERNAL_MAPPING {
  updateExternalMapping: UPDATE_EXTERNAL_MAPPING_updateExternalMapping | null;
}

export interface UPDATE_EXTERNAL_MAPPINGVariables {
  id: string;
  extenalMappingInput?: ExternalMappingInput | null;
}
