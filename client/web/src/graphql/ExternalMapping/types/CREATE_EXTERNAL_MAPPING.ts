/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ExternalMappingInput, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_EXTERNAL_MAPPING
// ====================================================

export interface CREATE_EXTERNAL_MAPPING_createExternalMapping_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_EXTERNAL_MAPPING_createExternalMapping_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_EXTERNAL_MAPPING_createExternalMapping_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_EXTERNAL_MAPPING_createExternalMapping_user_userPhoto_fichier | null;
}

export interface CREATE_EXTERNAL_MAPPING_createExternalMapping_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CREATE_EXTERNAL_MAPPING_createExternalMapping_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CREATE_EXTERNAL_MAPPING_createExternalMapping_user_role | null;
  userPhoto: CREATE_EXTERNAL_MAPPING_createExternalMapping_user_userPhoto | null;
  pharmacie: CREATE_EXTERNAL_MAPPING_createExternalMapping_user_pharmacie | null;
}

export interface CREATE_EXTERNAL_MAPPING_createExternalMapping {
  __typename: "ExternalMapping";
  id: string;
  idExternaluser: string | null;
  idClient: string | null;
  password: string | null;
  user: CREATE_EXTERNAL_MAPPING_createExternalMapping_user | null;
}

export interface CREATE_EXTERNAL_MAPPING {
  createExternalMapping: CREATE_EXTERNAL_MAPPING_createExternalMapping | null;
}

export interface CREATE_EXTERNAL_MAPPINGVariables {
  extenalMappingInput?: ExternalMappingInput | null;
}
