import gql from 'graphql-tag';

export const GET_CHECKEDS_COMMANDE = gql`
  {
    checkedsCommande @client {
      id
      codeReference
      operation {
        id
        libelle
      }
      commandeType {
        id
        libelle
      }
      commandeStatut {
        id
        libelle
      }
      owner {
        id
        userName
      }
      prixNetTotalHT
      dateCreation
    }
  }
`;
