/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_COMMANDE
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_COMMANDE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_CUSTOM_CONTENT_COMMANDE_search_data_Commande_operation {
  __typename: "Operation";
  id: string;
  libelle: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_COMMANDE_search_data_Commande_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  libelle: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_COMMANDE_search_data_Commande_commandeStatut {
  __typename: "StatutTypePayload";
  id: string;
  libelle: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_COMMANDE_search_data_Commande_owner {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_COMMANDE_search_data_Commande {
  __typename: "Commande";
  id: string;
  codeReference: string | null;
  operation: SEARCH_CUSTOM_CONTENT_COMMANDE_search_data_Commande_operation | null;
  commandeType: SEARCH_CUSTOM_CONTENT_COMMANDE_search_data_Commande_commandeType | null;
  commandeStatut: SEARCH_CUSTOM_CONTENT_COMMANDE_search_data_Commande_commandeStatut | null;
  owner: SEARCH_CUSTOM_CONTENT_COMMANDE_search_data_Commande_owner | null;
  prixNetTotalHT: number | null;
  dateCreation: any | null;
}

export type SEARCH_CUSTOM_CONTENT_COMMANDE_search_data = SEARCH_CUSTOM_CONTENT_COMMANDE_search_data_Action | SEARCH_CUSTOM_CONTENT_COMMANDE_search_data_Commande;

export interface SEARCH_CUSTOM_CONTENT_COMMANDE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_COMMANDE_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_COMMANDE {
  search: SEARCH_CUSTOM_CONTENT_COMMANDE_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_COMMANDEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
