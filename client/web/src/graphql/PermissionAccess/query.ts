import gql from 'graphql-tag';
import { PERMISSION_ACCESS_INFO_FRAGMENT } from './fragment';

export const DO_SEARCH_PERMISSIONS_ACCESS = gql`
  query SEARCH_PERMISSIONS_ACCESS(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Traitement {
          ...PermissionAccessInfo
        }
      }
    }
  }
  ${PERMISSION_ACCESS_INFO_FRAGMENT}
`;
