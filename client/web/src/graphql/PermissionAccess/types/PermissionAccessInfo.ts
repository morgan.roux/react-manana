/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PermissionAccessInfo
// ====================================================

export interface PermissionAccessInfo {
  __typename: "Traitement";
  id: string;
  nom: string | null;
  code: string | null;
}
