import gql from 'graphql-tag';
import { COUNT_TODO_INFO_FRAGMENT, PROJECT_INFO_FRAGMENT } from './fragement';

export const GET_PROJECT = gql`
  query PROJECT($id: ID!, $actionStatus: String) {
    project(id: $id) {
      ...ProjectInfo
    }
  }
  ${PROJECT_INFO_FRAGMENT}
`;

export const GET_PROJECTS = gql`
  query PROJECTS($isRemoved: Boolean, $actionStatus: String) {
    projects(isRemoved: $isRemoved) {
      ...ProjectInfo
    }
  }
  ${PROJECT_INFO_FRAGMENT}
`;

export const GET_PROJECTS_BY_USER_WITH_MIN_INFO = gql`
  query PROJECTS_BY_USER_WITH_MIN_INFO($idUser: ID!, $isRemoved: Boolean) {
    projectsByUser(idUser: $idUser, isRemoved: $isRemoved) {
      id
      name
      projetParent {
        id
        name
      }
    }
  }
`;

export const DO_SEARCH_PROJECT = gql`
  query SEARCH_PROJECT(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $actionStatus: String
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Project {
          ...ProjectInfo
        }
      }
    }
  }
  ${PROJECT_INFO_FRAGMENT}
`;

export const GET_SEARCH_CUSTOM_CONTENT_PROJECT = gql`
  query SEARCH_CUSTOM_CONTENT_PROJECT(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $actionStatus: String
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Project {
          ...ProjectInfo
        }
      }
    }
  }
  ${PROJECT_INFO_FRAGMENT}
`;

export const GET_COUNT_TODOS = gql`
  query COUNT_TODOS($input: CountTodosInput!) {
    countTodos(input: $input) {
      ...CountTodoInfo
    }
  }
  ${COUNT_TODO_INFO_FRAGMENT}
`;
