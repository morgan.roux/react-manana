import gql from 'graphql-tag';
import { USER_INFO_FRAGEMENT } from '../User/fragment';

export const PROJECT_INFO_FRAGMENT = gql`
  fragment ProjectInfo on Project {
    type
    id
    _name: name
    typeProject
    isArchived
    isShared
    activeActions
    isRemoved
    isInFavoris
    idFonction
    idTache
    ordre
    nbAction(actionStatus: $actionStatus)
    projetParent {
      id
      _name: name
      typeProject
      isArchived
      isShared
      isRemoved
      dateCreation
      dateModification
    }
    couleur {
      id
      code
      libelle
    }
    groupement {
      id
      nom
    }
    user {
      ...UserInfo
    }
    subProjects {
      id
      _name: name
      typeProject
      isArchived
      isShared
      isRemoved
      isInFavoris
      ordre
      nbAction
      couleur {
        id
        code
        libelle
      }
      projetParent {
        id
        _name: name
      }
      participants {
        id
      }
      user{
        id
      }
    }
    dateCreation
    dateModification
    participants {
      id
    }
  }
  ${USER_INFO_FRAGEMENT}
`;

export const COUNT_TODO_INFO_FRAGMENT = gql`
  fragment CountTodoInfo on CountTodoResult {
    project {
      personnel
      groupement
      professionnel
    }
    action {
      myActions
      actionsOfOthers
      notAssignedActions
      priorityOne
      priorityTwo
      priorityThree
      priorityFour
      withDateEcheance
      withoutDateEcheance
      inbox
      inboxTeam
      today
      todayTeam
      nextSeventDays
      nextSeventDaysTeam
      all
      taskActive
      taskDone
    }
  }
`;
