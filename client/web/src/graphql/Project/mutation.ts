import gql from 'graphql-tag';
import { PROJECT_INFO_FRAGMENT } from '../Project/fragement';

export const DO_CREATE_PROJECT = gql`
  mutation CREATE_PROJECT($input: ProjectInput!, $actionStatus: String) {
    createUpdateProject(input: $input) {
      ...ProjectInfo
    }
  }
  ${PROJECT_INFO_FRAGMENT}
`;

export const DO_DELETE_PROJECT = gql`
  mutation DELETE_PROJECTS($ids: [ID!]!, $actionStatus: String) {
    softDeleteProjects(ids: $ids) {
      ...ProjectInfo
    }
  }
  ${PROJECT_INFO_FRAGMENT}
`;
