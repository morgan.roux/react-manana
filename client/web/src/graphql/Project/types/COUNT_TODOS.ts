/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { CountTodosInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: COUNT_TODOS
// ====================================================

export interface COUNT_TODOS_countTodos_project {
  __typename: "CountProjectResult";
  personnel: number;
  groupement: number;
  professionnel: number;
}

export interface COUNT_TODOS_countTodos_action {
  __typename: "CountActionResult";
  myActions: number;
  actionsOfOthers: number;
  notAssignedActions: number;
  priorityOne: number;
  priorityTwo: number;
  priorityThree: number;
  priorityFour: number;
  withDateEcheance: number;
  withoutDateEcheance: number;
  inbox: number;
  inboxTeam: number;
  today: number;
  todayTeam: number;
  nextSeventDays: number;
  nextSeventDaysTeam: number | null;
  all: number | null;
  taskActive: number | null;
  taskDone: number | null;
}

export interface COUNT_TODOS_countTodos {
  __typename: "CountTodoResult";
  project: COUNT_TODOS_countTodos_project;
  action: COUNT_TODOS_countTodos_action;
}

export interface COUNT_TODOS {
  countTodos: COUNT_TODOS_countTodos;
}

export interface COUNT_TODOSVariables {
  input: CountTodosInput;
}
