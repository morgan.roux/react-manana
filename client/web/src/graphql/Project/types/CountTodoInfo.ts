/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: CountTodoInfo
// ====================================================

export interface CountTodoInfo_project {
  __typename: "CountProjectResult";
  personnel: number;
  groupement: number;
  professionnel: number;
}

export interface CountTodoInfo_action {
  __typename: "CountActionResult";
  myActions: number;
  actionsOfOthers: number;
  notAssignedActions: number;
  priorityOne: number;
  priorityTwo: number;
  priorityThree: number;
  priorityFour: number;
  withDateEcheance: number;
  withoutDateEcheance: number;
  inbox: number;
  inboxTeam: number;
  today: number;
  todayTeam: number;
  nextSeventDays: number;
  nextSeventDaysTeam: number | null;
  all: number | null;
  taskActive: number | null;
  taskDone: number | null;
}

export interface CountTodoInfo {
  __typename: "CountTodoResult";
  project: CountTodoInfo_project;
  action: CountTodoInfo_action;
}
