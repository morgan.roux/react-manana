/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ProjectInput, TypeProject, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PROJECT
// ====================================================

export interface CREATE_PROJECT_createUpdateProject_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_PROJECT_createUpdateProject_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_PROJECT_createUpdateProject_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
}

export interface CREATE_PROJECT_createUpdateProject_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_PROJECT_createUpdateProject_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CREATE_PROJECT_createUpdateProject_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CREATE_PROJECT_createUpdateProject_user_userPhoto_fichier | null;
}

export interface CREATE_PROJECT_createUpdateProject_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CREATE_PROJECT_createUpdateProject_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CREATE_PROJECT_createUpdateProject_user_role | null;
  userPhoto: CREATE_PROJECT_createUpdateProject_user_userPhoto | null;
  pharmacie: CREATE_PROJECT_createUpdateProject_user_pharmacie | null;
}

export interface CREATE_PROJECT_createUpdateProject_subProjects_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface CREATE_PROJECT_createUpdateProject_subProjects_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
}

export interface CREATE_PROJECT_createUpdateProject_subProjects_participants {
  __typename: "User";
  id: string;
}

export interface CREATE_PROJECT_createUpdateProject_subProjects_user {
  __typename: "User";
  id: string;
}

export interface CREATE_PROJECT_createUpdateProject_subProjects {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  ordre: number | null;
  nbAction: number;
  couleur: CREATE_PROJECT_createUpdateProject_subProjects_couleur | null;
  projetParent: CREATE_PROJECT_createUpdateProject_subProjects_projetParent | null;
  participants: CREATE_PROJECT_createUpdateProject_subProjects_participants[] | null;
  user: CREATE_PROJECT_createUpdateProject_subProjects_user;
}

export interface CREATE_PROJECT_createUpdateProject_participants {
  __typename: "User";
  id: string;
}

export interface CREATE_PROJECT_createUpdateProject {
  __typename: "Project";
  type: string;
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  activeActions: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  idFonction: string | null;
  idTache: string | null;
  ordre: number | null;
  nbAction: number;
  projetParent: CREATE_PROJECT_createUpdateProject_projetParent | null;
  couleur: CREATE_PROJECT_createUpdateProject_couleur | null;
  groupement: CREATE_PROJECT_createUpdateProject_groupement | null;
  user: CREATE_PROJECT_createUpdateProject_user;
  subProjects: CREATE_PROJECT_createUpdateProject_subProjects[] | null;
  dateCreation: any | null;
  dateModification: any | null;
  participants: CREATE_PROJECT_createUpdateProject_participants[] | null;
}

export interface CREATE_PROJECT {
  createUpdateProject: CREATE_PROJECT_createUpdateProject;
}

export interface CREATE_PROJECTVariables {
  input: ProjectInput;
  actionStatus?: string | null;
}
