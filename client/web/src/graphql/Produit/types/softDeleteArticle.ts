/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: softDeleteArticle
// ====================================================

export interface softDeleteArticle_softDeleteArticle {
  __typename: "Article";
  id: string;
}

export interface softDeleteArticle {
  softDeleteArticle: softDeleteArticle_softDeleteArticle | null;
}

export interface softDeleteArticleVariables {
  idArticle: string;
}
