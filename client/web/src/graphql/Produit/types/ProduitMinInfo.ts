/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ProduitMinInfo
// ====================================================

export interface ProduitMinInfo_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ProduitMinInfo_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: ProduitMinInfo_produitPhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ProduitMinInfo_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface ProduitMinInfo_famille {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  libelleFamille: string | null;
}

export interface ProduitMinInfo_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface ProduitMinInfo_produitTechReg_acte {
  __typename: "Acte";
  id: string;
  codeActe: number | null;
  libelleActe: string | null;
}

export interface ProduitMinInfo_produitTechReg_liste {
  __typename: "Liste";
  id: string;
  codeListe: number | null;
  libelle: string | null;
}

export interface ProduitMinInfo_produitTechReg_libelleStockage {
  __typename: "LibelleDivers";
  id: string;
  codeInfo: number | null;
  code: number | null;
}

export interface ProduitMinInfo_produitTechReg_tauxss {
  __typename: "TauxSS";
  id: string;
  codeTaux: number | null;
  tauxSS: number | null;
}

export interface ProduitMinInfo_produitTechReg_tva {
  __typename: "TVA";
  id: string;
  codeTva: number | null;
  tauxTva: number | null;
}

export interface ProduitMinInfo_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  isTipsLpp: number | null;
  isHomeo: number | null;
  laboExploitant: ProduitMinInfo_produitTechReg_laboExploitant | null;
  acte: ProduitMinInfo_produitTechReg_acte | null;
  liste: ProduitMinInfo_produitTechReg_liste | null;
  libelleStockage: ProduitMinInfo_produitTechReg_libelleStockage | null;
  tauxss: ProduitMinInfo_produitTechReg_tauxss | null;
  tva: ProduitMinInfo_produitTechReg_tva | null;
}

export interface ProduitMinInfo {
  __typename: "Produit";
  id: string;
  produitPhoto: ProduitMinInfo_produitPhoto | null;
  libelle: string | null;
  libelle2: string | null;
  supprimer: any | null;
  surveillanceRenforcee: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  produitCode: ProduitMinInfo_produitCode | null;
  famille: ProduitMinInfo_famille | null;
  produitTechReg: ProduitMinInfo_produitTechReg | null;
}
