/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ProduitInfo
// ====================================================

export interface ProduitInfo_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ProduitInfo_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: ProduitInfo_produitPhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ProduitInfo_categorie {
  __typename: "ProduitCategorie";
  id: string;
  resipIdCategorie: number | null;
  libelle: string | null;
}

export interface ProduitInfo_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface ProduitInfo_famille {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  libelleFamille: string | null;
}

export interface ProduitInfo_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface ProduitInfo_produitTechReg_laboDistirbuteur {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface ProduitInfo_produitTechReg_laboTitulaire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface ProduitInfo_produitTechReg_acte {
  __typename: "Acte";
  id: string;
  codeActe: number | null;
  libelleActe: string | null;
}

export interface ProduitInfo_produitTechReg_liste {
  __typename: "Liste";
  id: string;
  codeListe: number | null;
  libelle: string | null;
}

export interface ProduitInfo_produitTechReg_libelleStockage {
  __typename: "LibelleDivers";
  id: string;
  codeInfo: number | null;
  code: number | null;
}

export interface ProduitInfo_produitTechReg_tauxss {
  __typename: "TauxSS";
  id: string;
  codeTaux: number | null;
  tauxSS: number | null;
}

export interface ProduitInfo_produitTechReg_tva {
  __typename: "TVA";
  id: string;
  codeTva: number | null;
  tauxTva: number | null;
}

export interface ProduitInfo_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  isTipsLpp: number | null;
  isHomeo: number | null;
  prixAchat: number | null;
  prixAchatHT: number | null;
  prixVenteTTC: number | null;
  laboExploitant: ProduitInfo_produitTechReg_laboExploitant | null;
  laboDistirbuteur: ProduitInfo_produitTechReg_laboDistirbuteur | null;
  laboTitulaire: ProduitInfo_produitTechReg_laboTitulaire | null;
  acte: ProduitInfo_produitTechReg_acte | null;
  liste: ProduitInfo_produitTechReg_liste | null;
  libelleStockage: ProduitInfo_produitTechReg_libelleStockage | null;
  tauxss: ProduitInfo_produitTechReg_tauxss | null;
  tva: ProduitInfo_produitTechReg_tva | null;
}

export interface ProduitInfo_canauxArticle_sousGammeCommercial_gammeCommercial {
  __typename: "CanalGamme";
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
}

export interface ProduitInfo_canauxArticle_sousGammeCommercial {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
  gammeCommercial: ProduitInfo_canauxArticle_sousGammeCommercial_gammeCommercial | null;
  countCanalArticles: number | null;
}

export interface ProduitInfo_canauxArticle_sousGammeCatalogue_gammeCatalogue {
  __typename: "GammeCatalogue";
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
}

export interface ProduitInfo_canauxArticle_sousGammeCatalogue {
  __typename: "SousGammeCatalogue";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
  gammeCatalogue: ProduitInfo_canauxArticle_sousGammeCatalogue_gammeCatalogue | null;
}

export interface ProduitInfo_canauxArticle_commandeCanal {
  __typename: "CommandeCanal";
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
  countCanalArticles: number | null;
}

export interface ProduitInfo_canauxArticle {
  __typename: "ProduitCanal";
  id: string;
  qteStock: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  datePeremption: any | null;
  dateRetrait: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  sousGammeCommercial: ProduitInfo_canauxArticle_sousGammeCommercial | null;
  sousGammeCatalogue: ProduitInfo_canauxArticle_sousGammeCatalogue | null;
  commandeCanal: ProduitInfo_canauxArticle_commandeCanal | null;
  isRemoved: boolean | null;
}

export interface ProduitInfo {
  __typename: "Produit";
  id: string;
  produitPhoto: ProduitInfo_produitPhoto | null;
  libelle: string | null;
  libelle2: string | null;
  isReferentGenerique: number | null;
  supprimer: any | null;
  surveillanceRenforcee: number | null;
  categorie: ProduitInfo_categorie | null;
  produitCode: ProduitInfo_produitCode | null;
  famille: ProduitInfo_famille | null;
  produitTechReg: ProduitInfo_produitTechReg | null;
  canauxArticle: (ProduitInfo_canauxArticle | null)[] | null;
}
