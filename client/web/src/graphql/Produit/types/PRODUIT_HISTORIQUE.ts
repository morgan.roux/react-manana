/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PRODUIT_HISTORIQUE
// ====================================================

export interface PRODUIT_HISTORIQUE_produitHistorique_data {
  __typename: "GraphDatePayload";
  order: number | null;
  date: string | null;
  value: number | null;
}

export interface PRODUIT_HISTORIQUE_produitHistorique {
  __typename: "ResultProduitHistorique";
  year: number | null;
  dateHistorique: any | null;
  data: (PRODUIT_HISTORIQUE_produitHistorique_data | null)[] | null;
}

export interface PRODUIT_HISTORIQUE {
  produitHistorique: PRODUIT_HISTORIQUE_produitHistorique | null;
}

export interface PRODUIT_HISTORIQUEVariables {
  idProduit: string;
  idPharmacie: string;
  year?: number | null;
}
