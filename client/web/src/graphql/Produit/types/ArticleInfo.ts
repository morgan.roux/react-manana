/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypeFichier } from '../../../types/graphql-global-types';

// ====================================================
// GraphQL fragment: ArticleInfo
// ====================================================

export interface ArticleInfo_articlePhoto_fichier {
  __typename: 'Fichier';
  id: string;
  chemin: string | null;
  nomOriginal: string | null;
  type: TypeFichier | null;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ArticleInfo_articlePhoto {
  __typename: 'ArticlePhoto';
  id: string;
  fichier: ArticleInfo_articlePhoto_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ArticleInfo_tva {
  __typename: 'TrTVA';
  type: string;
  id: string;
  codeTva: string | null;
  valTva: number | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ArticleInfo_tableau {
  __typename: 'TrTableau';
  type: string;
  id: string;
  codeTableau: string | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ArticleInfo_marge {
  __typename: 'TrMarge';
  type: string;
  id: string;
  codeMarge: string | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ArticleInfo_forme {
  __typename: 'TrForme';
  type: string;
  id: string;
  codeForme: string | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ArticleInfo_stockage {
  __typename: 'TrStockage';
  type: string;
  id: string;
  codeStockage: string | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ArticleInfo_rbtArticle {
  __typename: 'RbtArticle';
  type: string;
  id: string;
  codeRbt: string | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ArticleInfo_acte {
  __typename: 'TrActe';
  type: string;
  id: string;
  codeActe: number | null;
  libelle: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface ArticleInfo_laboratoire_actualites_fichierPresentations {
  __typename: 'Fichier';
  id: string;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface ArticleInfo_laboratoire_actualites {
  __typename: 'Actualite';
  id: string;
  libelle: string | null;
  dateCreation: any | null;
  description: string | null;
  fichierPresentations: (ArticleInfo_laboratoire_actualites_fichierPresentations | null)[] | null;
}

export interface ArticleInfo_laboratoire {
  __typename: 'Laboratoire';
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  codeGroupLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  actualites: (ArticleInfo_laboratoire_actualites | null)[] | null;
}

export interface ArticleInfo_famille {
  __typename: 'TrFamille';
  id: string;
  idFamille: number;
  codeFamille: string;
  nomFamille: string | null;
  majFamille: string | null;
}

export interface ArticleInfo_canauxArticle_laboratoire_actualites_fichierPresentations {
  __typename: 'Fichier';
  id: string;
  nomOriginal: string | null;
  publicUrl: string | null;
}

export interface ArticleInfo_canauxArticle_laboratoire_actualites {
  __typename: 'Actualite';
  id: string;
  libelle: string | null;
  dateCreation: any | null;
  description: string | null;
  fichierPresentations:
    | (ArticleInfo_canauxArticle_laboratoire_actualites_fichierPresentations | null)[]
    | null;
}

export interface ArticleInfo_canauxArticle_laboratoire {
  __typename: 'Laboratoire';
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  codeGroupLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  actualites: (ArticleInfo_canauxArticle_laboratoire_actualites | null)[] | null;
}

export interface ArticleInfo_canauxArticle_sousGammeCommercial_gammeCommercial {
  __typename: 'CanalGamme';
  id: string;
  idGamme: number | null;
  codeGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
}

export interface ArticleInfo_canauxArticle_sousGammeCommercial {
  __typename: 'CanalSousGamme';
  id: string;
  idSousGamme: number | null;
  codeSousGamme: number | null;
  libelle: string | null;
  estDefaut: number | null;
  gammeCommercial: ArticleInfo_canauxArticle_sousGammeCommercial_gammeCommercial | null;
  countCanalArticles: number | null;
}

export interface ArticleInfo_canauxArticle_sousGammeCatalogue_gammeCatalogue {
  __typename: 'GammeCatalogue';
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
}

export interface ArticleInfo_canauxArticle_sousGammeCatalogue {
  __typename: 'SousGammeCatalogue';
  id: string;
  idSousGamme: number | null;
  codeSousGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
  gammeCatalogue: ArticleInfo_canauxArticle_sousGammeCatalogue_gammeCatalogue | null;
}

export interface ArticleInfo_canauxArticle_commandeCanal {
  __typename: 'CommandeCanal';
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
  countCanalArticles: number | null;
}

export interface ArticleInfo_canauxArticle {
  __typename: 'CanalArticle';
  id: string;
  qteStock: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  prixFab: number | null;
  prixPhv: number | null;
  laboratoire: ArticleInfo_canauxArticle_laboratoire | null;
  datePeremption: any | null;
  dateRetrait: any | null;
  dateCreation: any | null;
  dateModification: any | null;
  sousGammeCommercial: ArticleInfo_canauxArticle_sousGammeCommercial | null;
  sousGammeCatalogue: ArticleInfo_canauxArticle_sousGammeCatalogue | null;
  commandeCanal: ArticleInfo_canauxArticle_commandeCanal | null;
  isRemoved: boolean | null;
}

export interface ArticleInfo {
  __typename: 'Article';
  id: string;
  idArticle: number | null;
  articlePhoto: ArticleInfo_articlePhoto | null;
  nomArticle: string | null;
  typeCodeReferent: string | null;
  codeReference: string | null;
  pxAchatArticle: number | null;
  pxVenteArticle: number | null;
  tva: ArticleInfo_tva | null;
  tableau: ArticleInfo_tableau | null;
  marge: ArticleInfo_marge | null;
  forme: ArticleInfo_forme | null;
  stockage: ArticleInfo_stockage | null;
  rbtArticle: ArticleInfo_rbtArticle | null;
  acte: ArticleInfo_acte | null;
  laboratoire: ArticleInfo_laboratoire | null;
  famille: ArticleInfo_famille | null;
  dateSuppression: any | null;
  hospitalArticle: number | null;
  generiqArticle: number | null;
  carteFidelite: number | null;
  canauxArticle: (ArticleInfo_canauxArticle | null)[] | null;
}
