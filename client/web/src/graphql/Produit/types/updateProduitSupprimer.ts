/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: updateProduitSupprimer
// ====================================================

export interface updateProduitSupprimer_updateProduitSupprimer {
  __typename: "Produit";
  id: string;
  supprimer: any | null;
}

export interface updateProduitSupprimer {
  updateProduitSupprimer: updateProduitSupprimer_updateProduitSupprimer | null;
}

export interface updateProduitSupprimerVariables {
  id: string;
  date: string;
}
