import gql from 'graphql-tag';
import { PRODUIT_INFO } from './fragment';

export const GET_ARTICLE = gql`
  query PRODUIT($id: ID!) {
    produit(id: $id) {
      ...ProduitInfo
    }
  }
  ${PRODUIT_INFO}
`;

export const GET_SEARCH_GESTION_PRODUIT = gql`
  query SEARCH_GESTION_PRODUIT($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Produit {
          id
          libelle
          supprimer
          produitPhoto {
            id
            fichier {
              id
              publicUrl
              nomOriginal
            }
          }
          famille {
            id
            codeFamille
            libelleFamille
          }
          produitCode {
            id
            code
            typeCode
            referent
          }
          produitTechReg {
            id
            laboExploitant {
              id
              nomLabo
              sortie
            }
          }
        }
      }
    }
  }
`;

export const GET_SEARCH_GESTION_PRODUIT_CANAL = gql`
  query SEARCH_GESTION_PRODUIT_CANAL($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on ProduitCanal {
          id
          stv
          prixPhv
          unitePetitCond
          produit {
            id
            libelle
            supprimer
            produitPhoto {
              id
              fichier {
                id
                publicUrl
                nomOriginal
              }
            }
            famille {
              id
              codeFamille
              libelleFamille
            }
            produitCode {
              id
              code
              typeCode
              referent
            }
            produitTechReg {
              id
              laboExploitant {
                id
                nomLabo
                sortie
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_GESTION_PRODUIT_CANAL = gql`
  query GESTION_PRODUIT_CANAL($id: ID!) {
    produitCanal(id: $id) {
      id
      stv
      prixPhv
      unitePetitCond
      produit {
        id
        libelle
        supprimer
        produitPhoto {
          id
          fichier {
            id
            publicUrl
            nomOriginal
          }
        }
        famille {
          id
          codeFamille
          libelleFamille
        }
        produitCode {
          id
          code
          typeCode
          referent
        }
        produitTechReg {
          id
          laboExploitant {
            id
            nomLabo
            sortie
          }
        }
      }
    }
  }
`;

export const GET_PRODUIT_TYPE_CODES = gql`
  query PRODUIT_TYPE_CODES {
    produitTypeCodes {
      id
      resipIdTypeCode
      libelle
    }
  }
`;

export const GET_PRODUIT_HISTORIQUE = gql`
  query PRODUIT_HISTORIQUE($idProduit: ID!, $idPharmacie: ID!, $year: Int) {
    produitHistorique(idProduit: $idProduit, idPharmacie: $idPharmacie, year: $year) {
      year
      dateHistorique
      data {
        order
        date
        value
      }
    }
  }
`;
