import gql from 'graphql-tag';
import { PRODUIT_PHOTO_INFO } from '../ProduitPhoto/fragment';
import { TVA_INFO } from '../TVA/fragment';
import { ACTE_INFO } from '../Acte/fragment';
import { FAMILLE_INFO } from '../Famille/fragment';
import { CANAL_SOUS_GAMME_INFO } from '../CanalSousGamme/fragment';
import { SOUS_GAMME_CATALOGUE_INFO } from '../SousGammeCatalogue/fragment';
import { COMMANDE_CANAL_INFO_FRAGMENT } from '../CommandeCanal/fragment';
import { USER_SMYLEY_INFO_FRAGMENT } from '../Smyley/fragment';

export const PRODUIT_INFO = gql`
  fragment ProduitInfo on Produit {
    id
    produitPhoto {
      ...ProduitPhotoInfo
    }
    libelle
    libelle2
    isReferentGenerique
    supprimer
    surveillanceRenforcee
    categorie {
      id
      resipIdCategorie
      libelle
    }
    produitCode {
      id
      code
      typeCode
      referent
    }
    famille {
      id
      codeFamille
      libelleFamille
    }
    produitTechReg {
      id
      isTipsLpp
      isHomeo
      prixAchat
      prixAchatHT
      prixVenteTTC
      laboExploitant {
        id
        nomLabo
        sortie
      }
      laboDistirbuteur {
        id
        nomLabo
        sortie
      }
      laboTitulaire {
        id
        nomLabo
        sortie
      }
      acte {
        id
        codeActe
        libelleActe
      }
      liste {
        id
        codeListe
        libelle
      }
      libelleStockage {
        id
        codeInfo
        code
      }
      tauxss {
        id
        codeTaux
        tauxSS
      }
      tva {
        id
        codeTva
        tauxTva
      }
    }
    canauxArticle {
      id
      qteStock
      stv
      unitePetitCond
      prixFab
      prixPhv
      datePeremption
      dateRetrait
      dateCreation
      dateModification
      sousGammeCommercial {
        ...CanalSousGammeInfo
      }
      sousGammeCatalogue {
        ...SousGammeCatalogueInfo
      }
      commandeCanal {
        ...CommandeCanalInfo
      }
      isRemoved
    }
  }
  ${PRODUIT_PHOTO_INFO}
  ${CANAL_SOUS_GAMME_INFO}
  ${SOUS_GAMME_CATALOGUE_INFO}
  ${COMMANDE_CANAL_INFO_FRAGMENT}
`;

export const PRODUIT_MIN_INFO = gql`
  fragment ProduitMinInfo on Produit {
    id
    produitPhoto {
      ...ProduitPhotoInfo
    }
    id
    libelle
    libelle2
    supprimer
    surveillanceRenforcee
    dateCreation
    dateModification
    produitCode {
      id
      code
      typeCode
      referent
    }
    famille {
      id
      codeFamille
      libelleFamille
    }
    produitTechReg {
      id
      isTipsLpp
      isHomeo
      laboExploitant {
        id
        nomLabo
        sortie
      }
      acte {
        id
        codeActe
        libelleActe
      }
      liste {
        id
        codeListe
        libelle
      }
      libelleStockage {
        id
        codeInfo
        code
      }
      tauxss {
        id
        codeTaux
        tauxSS
      }
      tva {
        id
        codeTva
        tauxTva
      }
    }
  }
  ${PRODUIT_PHOTO_INFO}
  ${COMMANDE_CANAL_INFO_FRAGMENT}
`;
