/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PromotionType, PromotioStatus, GroupeClienRemisetInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_PROMOTION
// ====================================================

export interface CREATE_UPDATE_PROMOTION_createUpdatePromotion_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface CREATE_UPDATE_PROMOTION_createUpdatePromotion_userCreated {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface CREATE_UPDATE_PROMOTION_createUpdatePromotion_canalArticles_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface CREATE_UPDATE_PROMOTION_createUpdatePromotion_canalArticles_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitCode: CREATE_UPDATE_PROMOTION_createUpdatePromotion_canalArticles_produit_produitCode | null;
}

export interface CREATE_UPDATE_PROMOTION_createUpdatePromotion_canalArticles {
  __typename: "ProduitCanal";
  id: string;
  produit: CREATE_UPDATE_PROMOTION_createUpdatePromotion_canalArticles_produit | null;
}

export interface CREATE_UPDATE_PROMOTION_createUpdatePromotion_groupeClients_groupeClient {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
}

export interface CREATE_UPDATE_PROMOTION_createUpdatePromotion_groupeClients_remise_remiseDetails {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  remiseSupplementaire: number | null;
}

export interface CREATE_UPDATE_PROMOTION_createUpdatePromotion_groupeClients_remise {
  __typename: "Remise";
  id: string;
  remiseDetails: (CREATE_UPDATE_PROMOTION_createUpdatePromotion_groupeClients_remise_remiseDetails | null)[] | null;
}

export interface CREATE_UPDATE_PROMOTION_createUpdatePromotion_groupeClients {
  __typename: "PromotionGroupeClient";
  id: string;
  groupeClient: CREATE_UPDATE_PROMOTION_createUpdatePromotion_groupeClients_groupeClient | null;
  remise: CREATE_UPDATE_PROMOTION_createUpdatePromotion_groupeClients_remise | null;
}

export interface CREATE_UPDATE_PROMOTION_createUpdatePromotion {
  __typename: "Promotion";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  promotionType: PromotionType | null;
  status: PromotioStatus | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: CREATE_UPDATE_PROMOTION_createUpdatePromotion_commandeCanal | null;
  userCreated: CREATE_UPDATE_PROMOTION_createUpdatePromotion_userCreated | null;
  canalArticles: (CREATE_UPDATE_PROMOTION_createUpdatePromotion_canalArticles | null)[] | null;
  groupeClients: (CREATE_UPDATE_PROMOTION_createUpdatePromotion_groupeClients | null)[] | null;
}

export interface CREATE_UPDATE_PROMOTION {
  /**
   * promotion
   */
  createUpdatePromotion: CREATE_UPDATE_PROMOTION_createUpdatePromotion | null;
}

export interface CREATE_UPDATE_PROMOTIONVariables {
  id?: string | null;
  nom: string;
  dateDebut: string;
  dateFin: string;
  promotionType: PromotionType;
  status: PromotioStatus;
  codeCanal: string;
  idsCanalArticle?: (string | null)[] | null;
  groupeClients: GroupeClienRemisetInput[];
}
