/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PromotionType, PromotioStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PROMOTION
// ====================================================

export interface PROMOTION_promotion_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface PROMOTION_promotion_userCreated {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface PROMOTION_promotion_canalArticles_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface PROMOTION_promotion_canalArticles_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitCode: PROMOTION_promotion_canalArticles_produit_produitCode | null;
}

export interface PROMOTION_promotion_canalArticles {
  __typename: "ProduitCanal";
  id: string;
  produit: PROMOTION_promotion_canalArticles_produit | null;
}

export interface PROMOTION_promotion_groupeClients_groupeClient {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
}

export interface PROMOTION_promotion_groupeClients_remise_remiseDetails {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  remiseSupplementaire: number | null;
}

export interface PROMOTION_promotion_groupeClients_remise {
  __typename: "Remise";
  id: string;
  remiseDetails: (PROMOTION_promotion_groupeClients_remise_remiseDetails | null)[] | null;
}

export interface PROMOTION_promotion_groupeClients {
  __typename: "PromotionGroupeClient";
  id: string;
  groupeClient: PROMOTION_promotion_groupeClients_groupeClient | null;
  remise: PROMOTION_promotion_groupeClients_remise | null;
}

export interface PROMOTION_promotion {
  __typename: "Promotion";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  promotionType: PromotionType | null;
  status: PromotioStatus | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: PROMOTION_promotion_commandeCanal | null;
  userCreated: PROMOTION_promotion_userCreated | null;
  canalArticles: (PROMOTION_promotion_canalArticles | null)[] | null;
  groupeClients: (PROMOTION_promotion_groupeClients | null)[] | null;
}

export interface PROMOTION {
  promotion: PROMOTION_promotion | null;
}

export interface PROMOTIONVariables {
  id: string;
}
