/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PromotionType, PromotioStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_PROMOTION
// ====================================================

export interface DELETE_PROMOTION_softDeletePromotion_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface DELETE_PROMOTION_softDeletePromotion_userCreated {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface DELETE_PROMOTION_softDeletePromotion {
  __typename: "Promotion";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  promotionType: PromotionType | null;
  promoStatus: PromotioStatus | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: DELETE_PROMOTION_softDeletePromotion_commandeCanal | null;
  userCreated: DELETE_PROMOTION_softDeletePromotion_userCreated | null;
}

export interface DELETE_PROMOTION {
  softDeletePromotion: DELETE_PROMOTION_softDeletePromotion | null;
}

export interface DELETE_PROMOTIONVariables {
  id: string;
}
