/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PromotionType, PromotioStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PROMOTION_PRODUIT_CANAL
// ====================================================

export interface PROMOTION_PRODUIT_CANAL_promotion_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface PROMOTION_PRODUIT_CANAL_promotion_userCreated {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface PROMOTION_PRODUIT_CANAL_promotion_canalArticles_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface PROMOTION_PRODUIT_CANAL_promotion_canalArticles_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
  produitCode: PROMOTION_PRODUIT_CANAL_promotion_canalArticles_produit_produitCode | null;
}

export interface PROMOTION_PRODUIT_CANAL_promotion_canalArticles {
  __typename: "ProduitCanal";
  id: string;
  produit: PROMOTION_PRODUIT_CANAL_promotion_canalArticles_produit | null;
}

export interface PROMOTION_PRODUIT_CANAL_promotion {
  __typename: "Promotion";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  promotionType: PromotionType | null;
  status: PromotioStatus | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: PROMOTION_PRODUIT_CANAL_promotion_commandeCanal | null;
  userCreated: PROMOTION_PRODUIT_CANAL_promotion_userCreated | null;
  canalArticles: (PROMOTION_PRODUIT_CANAL_promotion_canalArticles | null)[] | null;
}

export interface PROMOTION_PRODUIT_CANAL {
  promotion: PROMOTION_PRODUIT_CANAL_promotion | null;
}

export interface PROMOTION_PRODUIT_CANALVariables {
  id: string;
}
