/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PromotionType, PromotioStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: PromotionInfo
// ====================================================

export interface PromotionInfo_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface PromotionInfo_userCreated {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface PromotionInfo {
  __typename: "Promotion";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  promotionType: PromotionType | null;
  promoStatus: PromotioStatus | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: PromotionInfo_commandeCanal | null;
  userCreated: PromotionInfo_userCreated | null;
}
