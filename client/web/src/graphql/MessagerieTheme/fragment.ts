import gql from 'graphql-tag';

export const MESSAGERIE_THEME_INFO_FRAGMENT = gql`
  fragment MessagerieThemeInfo on MessagerieTheme {
    id
    nom
  }
`;
