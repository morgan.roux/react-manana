/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MESSAGERIE_THEMES
// ====================================================

export interface MESSAGERIE_THEMES_messagerieThemes {
  __typename: "MessagerieTheme";
  id: string;
  nom: string | null;
}

export interface MESSAGERIE_THEMES {
  messagerieThemes: (MESSAGERIE_THEMES_messagerieThemes | null)[] | null;
}
