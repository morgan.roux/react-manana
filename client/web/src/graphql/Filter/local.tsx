import gql from 'graphql-tag';

export const GET_STATUS_FILTER = gql`
  {
    statusFilter @client
  }
`;

export const GET_FILTER_DATE = gql`
  {
    dateFilter @client {
      startDate
      endDate
    }
  }
`;
