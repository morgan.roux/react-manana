/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: LEAVE_GROUP
// ====================================================

export interface LEAVE_GROUP_leaveGroupeAmis {
  __typename: "GroupeAmisDetail";
  id: string;
}

export interface LEAVE_GROUP {
  leaveGroupeAmis: LEAVE_GROUP_leaveGroupeAmis[] | null;
}

export interface LEAVE_GROUPVariables {
  id: string;
  idsPharmacies: string[];
}
