/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PartageType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PRODUIT_CANAL_PARTAGE
// ====================================================

export interface PRODUIT_CANAL_PARTAGE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_promotions_userCreated {
  __typename: "User";
  userName: string | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_promotions {
  __typename: "Promotion";
  userCreated: PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_promotions_userCreated | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_userSmyleys_data_user {
  __typename: "User";
  userName: string | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_userSmyleys_data {
  __typename: "UserSmyley";
  user: PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_userSmyleys_data_user | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_userSmyleys {
  __typename: "UserSmyleyResult";
  data: (PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_userSmyleys_data | null)[] | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_produit_service {
  __typename: "Service";
  nom: string | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_produit {
  __typename: "Produit";
  libelle: string | null;
  service: PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_produit_service | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_pharmacieRemisePaliers {
  __typename: "Pharmacie";
  nom: string | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_pharmacieRemisePanachees_titulaires {
  __typename: "Titulaire";
  fullName: string | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_pharmacieRemisePanachees_users_role {
  __typename: "Role";
  nom: string | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_pharmacieRemisePanachees_users {
  __typename: "User";
  role: PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_pharmacieRemisePanachees_users_role | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_pharmacieRemisePanachees {
  __typename: "Pharmacie";
  nom: string | null;
  titulaires: (PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_pharmacieRemisePanachees_titulaires | null)[] | null;
  users: (PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_pharmacieRemisePanachees_users | null)[] | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_partage_groupeAmisCibles_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_partage_groupeAmisCibles {
  __typename: "PartageGroupeAmisCible";
  groupeAmis: PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_partage_groupeAmisCibles_groupeAmis;
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_partage {
  __typename: "Partage";
  typePartage: PartageType;
  groupeAmisCibles: PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_partage_groupeAmisCibles[];
}

export interface PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal {
  __typename: "ProduitCanal";
  isShared: boolean;
  isActive: boolean | null;
  isRemoved: boolean | null;
  id: string;
  type: string;
  promotions: (PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_promotions | null)[] | null;
  userSmyleys: PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_userSmyleys | null;
  produit: PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_produit | null;
  pharmacieRemisePaliers: (PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_pharmacieRemisePaliers | null)[] | null;
  pharmacieRemisePanachees: (PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_pharmacieRemisePanachees | null)[] | null;
  partage: PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal_partage | null;
}

export type PRODUIT_CANAL_PARTAGE_search_data = PRODUIT_CANAL_PARTAGE_search_data_Action | PRODUIT_CANAL_PARTAGE_search_data_ProduitCanal;

export interface PRODUIT_CANAL_PARTAGE_search {
  __typename: "SearchResult";
  total: number;
  data: (PRODUIT_CANAL_PARTAGE_search_data | null)[] | null;
}

export interface PRODUIT_CANAL_PARTAGE {
  search: PRODUIT_CANAL_PARTAGE_search | null;
}

export interface PRODUIT_CANAL_PARTAGEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
}
