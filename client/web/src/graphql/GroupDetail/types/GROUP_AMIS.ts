/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GROUP_AMIS
// ====================================================

export interface GROUP_AMIS_groupeAmis {
  __typename: "GroupeAmis";
  nom: string | null;
  id: string;
}

export interface GROUP_AMIS {
  groupeAmis: GROUP_AMIS_groupeAmis | null;
}

export interface GROUP_AMISVariables {
  id: string;
}
