import gql from 'graphql-tag';

export const DO_LEAVE_GROUP = gql`
  mutation LEAVE_GROUP($id: ID!, $idsPharmacies: [ID!]!) {
    leaveGroupeAmis(id: $id, idsPharmacies: $idsPharmacies) {
      id
    }
  }
`;
