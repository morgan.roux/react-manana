import gql from 'graphql-tag';

export const GROUP_AMIS_DETAIL_FRAGMENT = gql`
  fragment GroupAmisDetailInfo on GroupeAmisDetail {
    id
    dateCreation
    pharmacie {
      id
      nom
      cip
      cp
      ville
      departement {
        id
        nom
      }
      titulaires {
        id
        fullName
      }
    }
    groupeAmis {
      id
      nbMember
      nom
    }
  }
`;
