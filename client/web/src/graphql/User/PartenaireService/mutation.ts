import gql from 'graphql-tag';

export const DO_CREATE_USER_PARTENAIRE_SERVICE = gql`
  mutation CREATE_USER_PARTENAIRE_SERVICE(
    $idGroupement: ID!
    $id: ID!
    $email: String
    $login: String!
    $userPhoto: FichierInput
    $codeTraitements: [String]
    $day: Int
    $month: Int
    $year: Int
    $idPharmacie: ID!
  ) {
    createUserPartenaireService(
      idGroupement: $idGroupement
      id: $id
      email: $email
      login: $login
      userPhoto: $userPhoto
      codeTraitements: $codeTraitements
      day: $day
      month: $month
      year: $year
      idPharmacie: $idPharmacie
    ) {
      type
      id
      nom
      commentaire
      idGroupement
      user {
        id
        email
        login
        status
      }
      role {
        code
        nom
      }
    }
  }
`;
