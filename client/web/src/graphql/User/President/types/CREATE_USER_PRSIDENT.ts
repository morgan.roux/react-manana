/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_USER_PRSIDENT
// ====================================================

export interface CREATE_USER_PRSIDENT_createUserPresidentRegion_users {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
}

export interface CREATE_USER_PRSIDENT_createUserPresidentRegion_pharmacieUser {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
}

export interface CREATE_USER_PRSIDENT_createUserPresidentRegion {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  users: (CREATE_USER_PRSIDENT_createUserPresidentRegion_users | null)[] | null;
  pharmacieUser: CREATE_USER_PRSIDENT_createUserPresidentRegion_pharmacieUser | null;
}

export interface CREATE_USER_PRSIDENT {
  createUserPresidentRegion: CREATE_USER_PRSIDENT_createUserPresidentRegion | null;
}

export interface CREATE_USER_PRSIDENTVariables {
  idGroupement: string;
  id: string;
  idPharmacie: string;
  email?: string | null;
  login: string;
  userId?: string | null;
  day?: number | null;
  month?: number | null;
  year?: number | null;
  userPhoto?: FichierInput | null;
  codeTraitements?: (string | null)[] | null;
}
