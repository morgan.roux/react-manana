/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput } from "./../../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_USER_PARTENAIRE_LABORATOIRE
// ====================================================

export interface CREATE_USER_PARTENAIRE_LABORATOIRE_createUserPartenaireLaboratoire_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
}

export interface CREATE_USER_PARTENAIRE_LABORATOIRE_createUserPartenaireLaboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  user: CREATE_USER_PARTENAIRE_LABORATOIRE_createUserPartenaireLaboratoire_user | null;
}

export interface CREATE_USER_PARTENAIRE_LABORATOIRE {
  createUserPartenaireLaboratoire: CREATE_USER_PARTENAIRE_LABORATOIRE_createUserPartenaireLaboratoire | null;
}

export interface CREATE_USER_PARTENAIRE_LABORATOIREVariables {
  idGroupement: string;
  id: string;
  email?: string | null;
  login: string;
  userId?: string | null;
  userPhoto?: FichierInput | null;
  codeTraitements?: (string | null)[] | null;
}
