/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_TOTAL_USER_IDS
// ====================================================

export interface GET_TOTAL_USER_IDS_search {
  __typename: "SearchResult";
  total: number;
}

export interface GET_TOTAL_USER_IDS {
  search: GET_TOTAL_USER_IDS_search | null;
}

export interface GET_TOTAL_USER_IDSVariables {
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
