/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_USERS
// ====================================================

export interface SEARCH_USERS_search_data_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_USERS_search_data_User_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface SEARCH_USERS_search_data_User_pharmacie {
  __typename: "Pharmacie";
  id: string;
}

export interface SEARCH_USERS_search_data_User_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  urlPresigned: string | null;
  publicUrl: string | null;
}

export interface SEARCH_USERS_search_data_User_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_USERS_search_data_User_userPhoto_fichier | null;
}

export interface SEARCH_USERS_search_data_User_userPersonnel_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface SEARCH_USERS_search_data_User_userPersonnel_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
}

export interface SEARCH_USERS_search_data_User_userPersonnel {
  __typename: "Personnel";
  id: string;
  service: SEARCH_USERS_search_data_User_userPersonnel_service | null;
  contact: SEARCH_USERS_search_data_User_userPersonnel_contact | null;
}

export interface SEARCH_USERS_search_data_User_userPpersonnel_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
}

export interface SEARCH_USERS_search_data_User_userPpersonnel {
  __typename: "Ppersonnel";
  id: string;
  contact: SEARCH_USERS_search_data_User_userPpersonnel_contact | null;
}

export interface SEARCH_USERS_search_data_User_userPartenaire_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
}

export interface SEARCH_USERS_search_data_User_userPartenaire {
  __typename: "Partenaire";
  id: string;
  contact: SEARCH_USERS_search_data_User_userPartenaire_contact | null;
}

export interface SEARCH_USERS_search_data_User_userTitulaire_titulaire_contact {
  __typename: "Contact";
  cp: string | null;
}

export interface SEARCH_USERS_search_data_User_userTitulaire_titulaire {
  __typename: "Titulaire";
  id: string;
  contact: SEARCH_USERS_search_data_User_userTitulaire_titulaire_contact | null;
}

export interface SEARCH_USERS_search_data_User_userTitulaire {
  __typename: "UserTitulaire";
  id: string;
  titulaire: SEARCH_USERS_search_data_User_userTitulaire_titulaire | null;
}

export interface SEARCH_USERS_search_data_User {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  role: SEARCH_USERS_search_data_User_role | null;
  nbAppel: number | null;
  nbReclamation: number | null;
  pharmacie: SEARCH_USERS_search_data_User_pharmacie | null;
  userPhoto: SEARCH_USERS_search_data_User_userPhoto | null;
  userPersonnel: SEARCH_USERS_search_data_User_userPersonnel | null;
  userPpersonnel: SEARCH_USERS_search_data_User_userPpersonnel | null;
  userPartenaire: SEARCH_USERS_search_data_User_userPartenaire | null;
  userTitulaire: SEARCH_USERS_search_data_User_userTitulaire | null;
}

export type SEARCH_USERS_search_data = SEARCH_USERS_search_data_Action | SEARCH_USERS_search_data_User;

export interface SEARCH_USERS_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_USERS_search_data | null)[] | null;
}

export interface SEARCH_USERS {
  search: SEARCH_USERS_search | null;
}

export interface SEARCH_USERSVariables {
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
