/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: USER
// ====================================================

export interface USER_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface USER_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface USER_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: USER_user_userPhoto_fichier | null;
}

export interface USER_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface USER_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: USER_user_role | null;
  userPhoto: USER_user_userPhoto | null;
  pharmacie: USER_user_pharmacie | null;
}

export interface USER {
  user: USER_user | null;
}

export interface USERVariables {
  id: string;
}
