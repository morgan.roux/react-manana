/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_USER_IDS
// ====================================================

export interface GET_USER_IDS_search_data_Action {
  __typename: "Action" | "TodoActionType" | "Groupement" | "Pharmacie" | "Contact" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmis" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "Ticket" | "TicketOrigine" | "TicketMotif" | "TicketStatut" | "TicketChangementCible" | "Partage" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementStatut" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement";
}

export interface GET_USER_IDS_search_data_User {
  __typename: "User";
  id: string;
}

export type GET_USER_IDS_search_data = GET_USER_IDS_search_data_Action | GET_USER_IDS_search_data_User;

export interface GET_USER_IDS_search {
  __typename: "SearchResult";
  total: number;
  data: (GET_USER_IDS_search_data | null)[] | null;
}

export interface GET_USER_IDS {
  search: GET_USER_IDS_search | null;
}

export interface GET_USER_IDSVariables {
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
