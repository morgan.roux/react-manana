/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus, TypeRole } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_MIN_USERS
// ====================================================

export interface SEARCH_MIN_USERS_search_data_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_MIN_USERS_search_data_User_role {
  __typename: "Role";
  id: string;
  nom: string | null;
  typeRole: TypeRole | null;
  code: string | null;
}

export interface SEARCH_MIN_USERS_search_data_User_contact {
  __typename: "Contact";
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
}

export interface SEARCH_MIN_USERS_search_data_User {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  role: SEARCH_MIN_USERS_search_data_User_role | null;
  phoneNumber: string | null;
  contact: SEARCH_MIN_USERS_search_data_User_contact | null;
}

export type SEARCH_MIN_USERS_search_data = SEARCH_MIN_USERS_search_data_Action | SEARCH_MIN_USERS_search_data_User;

export interface SEARCH_MIN_USERS_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_MIN_USERS_search_data | null)[] | null;
}

export interface SEARCH_MIN_USERS {
  search: SEARCH_MIN_USERS_search | null;
}

export interface SEARCH_MIN_USERSVariables {
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
