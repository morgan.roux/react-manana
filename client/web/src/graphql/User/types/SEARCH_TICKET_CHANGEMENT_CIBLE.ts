/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_TICKET_CHANGEMENT_CIBLE
// ====================================================

export interface SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_ActiviteUser {
  __typename: "ActiviteUser" | "User" | "Groupement" | "Pharmacie" | "Contact" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "Project" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "PartenaireServicePartenaire" | "Ppersonnel" | "Commande" | "CommandeLigne" | "CommandeType" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "Ticket" | "TicketOrigine" | "TicketMotif" | "TicketStatut" | "TicketChangementStatut" | "Traitement";
}

export interface SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_TicketChangementCible_userCible_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_TicketChangementCible_userCible_userPersonnel_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_TicketChangementCible_userCible_userPersonnel {
  __typename: "Personnel";
  id: string;
  service: SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_TicketChangementCible_userCible_userPersonnel_service | null;
}

export interface SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_TicketChangementCible_userCible_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_TicketChangementCible_userCible {
  __typename: "User";
  id: string;
  userName: string | null;
  pharmacie: SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_TicketChangementCible_userCible_pharmacie | null;
  userPersonnel: SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_TicketChangementCible_userCible_userPersonnel | null;
  role: SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_TicketChangementCible_userCible_role | null;
}

export interface SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_TicketChangementCible {
  __typename: "TicketChangementCible";
  id: string;
  userCible: SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_TicketChangementCible_userCible | null;
}

export type SEARCH_TICKET_CHANGEMENT_CIBLE_search_data = SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_ActiviteUser | SEARCH_TICKET_CHANGEMENT_CIBLE_search_data_TicketChangementCible;

export interface SEARCH_TICKET_CHANGEMENT_CIBLE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_TICKET_CHANGEMENT_CIBLE_search_data | null)[] | null;
}

export interface SEARCH_TICKET_CHANGEMENT_CIBLE {
  search: SEARCH_TICKET_CHANGEMENT_CIBLE_search | null;
}

export interface SEARCH_TICKET_CHANGEMENT_CIBLEVariables {
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
