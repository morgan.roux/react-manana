import gql from 'graphql-tag';

export const DO_CREATE_USER_TITULAIRE = gql`
  mutation CREATE_USER_TITULAIRE(
    $idGroupement: ID!
    $id: ID!
    $email: String
    $login: String!
    $idPharmacie: ID!
    $userId: ID
    $day: Int
    $month: Int
    $year: Int
    $userPhoto: FichierInput
    $codeTraitements: [String]
  ) {
    createUserTitulaire(
      idGroupement: $idGroupement
      id: $id
      email: $email
      login: $login
      idPharmacie: $idPharmacie
      userId: $userId
      day: $day
      month: $month
      year: $year
      userPhoto: $userPhoto
      codeTraitements: $codeTraitements
    ) {
      type
      id
      nom
      prenom
      estPresidentRegion
      idGroupement
      users {
        id
        email
        login
      }
      pharmacieUser {
        id
        cip
      }
    }
  }
`;
