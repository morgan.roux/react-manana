import gql from 'graphql-tag';

export const SERVICE_INFO_FRAGEMENT = gql`
  fragment ServiceInfo on Service {
    type
    id
    code
    nom
    countUsers(idGroupement: $idGroupement)
    dateCreation
    dateModification
  }
`;
