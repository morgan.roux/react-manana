/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: DEPARTEMENTS
// ====================================================

export interface DEPARTEMENTS_departements {
  __typename: "Departement";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface DEPARTEMENTS {
  departements: (DEPARTEMENTS_departements | null)[] | null;
}
