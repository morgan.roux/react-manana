import gql from 'graphql-tag';
import { DEPARTEMENT_INFO_FRAGMENT } from './fragment';

export const GET_DEPARTEMENTS = gql`
  query DEPARTEMENTS {
    departements {
      ...DepartementInfo
    }
  }
  ${DEPARTEMENT_INFO_FRAGMENT}
`;
