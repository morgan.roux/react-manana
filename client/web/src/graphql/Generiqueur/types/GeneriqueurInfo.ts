/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: GeneriqueurInfo
// ====================================================

export interface GeneriqueurInfo {
  __typename: "Generiqueur";
  id: string;
  nom: string | null;
}
