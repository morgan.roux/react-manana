/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: StatutTypePayloadInfo
// ====================================================

export interface StatutTypePayloadInfo {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}
