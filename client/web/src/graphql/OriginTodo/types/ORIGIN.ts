/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ORIGIN
// ====================================================

export interface ORIGIN_todoActionOrigines {
  __typename: "TodoActionOrigine";
  libelle: string;
  id: string;
}

export interface ORIGIN {
  todoActionOrigines: ORIGIN_todoActionOrigines[];
}
