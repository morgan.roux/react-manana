import gql from 'graphql-tag';

export const GET_TODO_ACTION_ORIGINES = gql`
  query TODO_ACTION_ORIGINES($isRemoved: Boolean) {
    todoActionOrigines(isRemoved: $isRemoved) {
      libelle
      id
      code
    }
  }
`;
