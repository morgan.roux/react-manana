/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: TITULAIRE_PHARMACIES
// ====================================================

export interface TITULAIRE_PHARMACIES_titulairePharmacies_users {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface TITULAIRE_PHARMACIES_titulairePharmacies {
  __typename: "Pharmacie";
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  nbEmploye: number | null;
  nbAssistant: number | null;
  nbAutreTravailleur: number | null;
  commentaire: string | null;
  nbJourOuvre: number | null;
  latitude: string | null;
  longitude: string | null;
  users: (TITULAIRE_PHARMACIES_titulairePharmacies_users | null)[] | null;
}

export interface TITULAIRE_PHARMACIES {
  titulairePharmacies: (TITULAIRE_PHARMACIES_titulairePharmacies | null)[] | null;
}

export interface TITULAIRE_PHARMACIESVariables {
  id: string;
}
