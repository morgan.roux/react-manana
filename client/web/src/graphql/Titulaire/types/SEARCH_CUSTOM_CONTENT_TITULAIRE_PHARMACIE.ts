/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_pharmacies_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_pharmacies {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  cip: string | null;
  departement: SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_pharmacies_departement | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_users_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_users_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_users_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_users_userPhoto_fichier | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_users {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  dateCreation: any | null;
  dateModification: any | null;
  anneeNaissance: number | null;
  lastLoginDate: any | null;
  pharmacie: SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_users_pharmacie | null;
  userPhoto: SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_users_userPhoto | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_pharmacieUser {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  civilite: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  pharmacies: (SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_pharmacies | null)[] | null;
  users: (SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_users | null)[] | null;
  pharmacieUser: SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_pharmacieUser | null;
  contact: SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire_contact | null;
}

export type SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data = SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Action | SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data_Titulaire;

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE {
  search: SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
