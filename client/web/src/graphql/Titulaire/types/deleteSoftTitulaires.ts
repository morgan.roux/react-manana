/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: deleteSoftTitulaires
// ====================================================

export interface deleteSoftTitulaires_deleteSoftTitulaires {
  __typename: "Titulaire";
  id: string;
}

export interface deleteSoftTitulaires {
  deleteSoftTitulaires: (deleteSoftTitulaires_deleteSoftTitulaires | null)[] | null;
}

export interface deleteSoftTitulairesVariables {
  ids: string[];
}
