/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TitulaireInfo
// ====================================================

export interface TitulaireInfo {
  __typename: "Titulaire";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
}
