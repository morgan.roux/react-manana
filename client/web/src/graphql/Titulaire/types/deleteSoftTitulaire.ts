/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: deleteSoftTitulaire
// ====================================================

export interface deleteSoftTitulaire_deleteSoftTitulaire {
  __typename: "Titulaire";
  id: string;
}

export interface deleteSoftTitulaire {
  deleteSoftTitulaire: deleteSoftTitulaire_deleteSoftTitulaire | null;
}

export interface deleteSoftTitulaireVariables {
  id: string;
}
