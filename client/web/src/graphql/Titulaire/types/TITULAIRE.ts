/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: TITULAIRE
// ====================================================

export interface TITULAIRE_titulaire_users_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface TITULAIRE_titulaire_users_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: TITULAIRE_titulaire_users_userPhoto_fichier | null;
}

export interface TITULAIRE_titulaire_users {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
  codeTraitements: (string | null)[] | null;
  userPhoto: TITULAIRE_titulaire_users_userPhoto | null;
}

export interface TITULAIRE_titulaire_pharmacieUser {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
}

export interface TITULAIRE_titulaire_pharmacies {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface TITULAIRE_titulaire_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
}

export interface TITULAIRE_titulaire {
  __typename: "Titulaire";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  users: (TITULAIRE_titulaire_users | null)[] | null;
  pharmacieUser: TITULAIRE_titulaire_pharmacieUser | null;
  pharmacies: (TITULAIRE_titulaire_pharmacies | null)[] | null;
  contact: TITULAIRE_titulaire_contact | null;
}

export interface TITULAIRE {
  titulaire: TITULAIRE_titulaire | null;
}

export interface TITULAIREVariables {
  id: string;
}
