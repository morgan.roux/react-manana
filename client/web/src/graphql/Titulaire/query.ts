import gql from 'graphql-tag';
import { MIN_CONTACT_INFO_FRAGMENT } from '../Contact/fragment';

export const GET_TITULAIRE = gql`
  query TITULAIRE($id: ID!) {
    titulaire(id: $id) {
      type
      id
      civilite
      nom
      prenom
      estPresidentRegion
      idGroupement
      dateCreation
      dateModification
      users {
        id
        email
        login
        status
        jourNaissance
        moisNaissance
        anneeNaissance
        codeTraitements
        userPhoto {
          id
          fichier {
            id
            chemin
            nomOriginal
            publicUrl
          }
        }
      }
      pharmacieUser {
        id
        cip
        nom
      }
      pharmacies {
        id
        nom
      }
      contact {
        ...MinContactInfo
      }
    }
  }
  ${MIN_CONTACT_INFO_FRAGMENT}
`;

export const GET_PHARMACIES_BY_TITULAIRE = gql`
  query TITULAIRE_PHARMACIES($id: ID!) {
    titulairePharmacies(id: $id) {
      id
      sortie
      cip
      numFiness
      nom
      adresse1
      adresse2
      cp
      ville
      nbEmploye
      nbAssistant
      nbAutreTravailleur
      commentaire
      nbJourOuvre
      latitude
      longitude
      users {
        id
        userName
        email
        login
        status
      }
    }
  }
`;

export const GET_SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE = gql`
  query SEARCH_CUSTOM_CONTENT_TITULAIRE_PHARMACIE(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
  ) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Titulaire {
          id
          nom
          prenom
          civilite
          fullName
          estPresidentRegion
          idGroupement
          civilite
          dateCreation
          dateModification
          pharmacies {
            id
            nom
            cip
            pharmacieType
            departement {
              id
              nom
            }
          }
          users {
            id
            userName
            email
            login
            status
            dateCreation
            dateModification
            anneeNaissance
            lastLoginDate
            pharmacie {
              id
              nom
            }
            userPhoto {
              id
              fichier {
                id
                chemin
                nomOriginal
                publicUrl
              }
            }
          }
          pharmacieUser {
            id
            cip
            nom
          }
          contact {
            ...MinContactInfo
          }
        }
      }
    }
  }
  ${MIN_CONTACT_INFO_FRAGMENT}
`;
