import gql from 'graphql-tag';

export const SEARCH_TITULAIRE_FRAGMENT = gql`
  fragment SearchTitulaireInfo on Titulaire {
    type
    id
    nom
    prenom
    fullName
    estPresidentRegion
    idGroupement
    pharmacies {
      id
      nom
      cip
      pharmacieType
      departement {
        id
        nom
      }
    }
    users {
      id
      userName
      email
      login
      status
      jourNaissance
      moisNaissance
      anneeNaissance
      pharmacie {
        id
        nom
      }
      userPhoto {
        id
        fichier {
          id
          nomOriginal
          publicUrl
          urlPresigned
        }
      }
    }
    pharmacieUser {
      id
      cip
      nom
    }
  }
`;
