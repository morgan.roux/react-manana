/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CONTRATS
// ====================================================

export interface CONTRATS_contrats {
  __typename: "Contrat";
  id: string;
  nom: string | null;
}

export interface CONTRATS {
  contrats: (CONTRATS_contrats | null)[] | null;
}
