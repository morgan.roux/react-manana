import gql from 'graphql-tag';

export const CONTRAT_INFO_FRAGMENT = gql`
  fragment ContratInfo on Contrat {
    id
    nom
    numeroVersion
    codeMaj
    dateCreation
    dateModification
  }
`;
