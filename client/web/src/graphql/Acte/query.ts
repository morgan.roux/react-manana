import gql from 'graphql-tag';

export const GET_ACTES = gql`
  query ACTES {
    actes {
      id
      codeActe
      libelleActe
    }
  }
`;
