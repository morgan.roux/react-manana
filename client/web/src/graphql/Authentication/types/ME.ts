/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: ME
// ====================================================

export interface ME_me_role {
  __typename: "Role";
  code: string | null;
  nom: string | null;
}

export interface ME_me_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface ME_me_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: ME_me_groupement_groupementLogo_fichier | null;
}

export interface ME_me_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
  groupementLogo: ME_me_groupement_groupementLogo | null;
}

export interface ME_me_pharmaciePartenaires_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface ME_me_pharmaciePartenaires_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: ME_me_pharmaciePartenaires_departement_region | null;
}

export interface ME_me_pharmaciePartenaires {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: ME_me_pharmaciePartenaires_departement | null;
}

export interface ME_me_pharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface ME_me_pharmacie_departement {
  __typename: "Departement";
  id: string;
  region: ME_me_pharmacie_departement_region | null;
}

export interface ME_me_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  departement: ME_me_pharmacie_departement | null;
}

export interface ME_me_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface ME_me_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: ME_me_userPhoto_fichier | null;
}

export interface ME_me_userPersonnel_service {
  __typename: "Service";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ME_me_userPersonnel {
  __typename: "Personnel";
  id: string;
  nom: string | null;
  prenom: string | null;
  service: ME_me_userPersonnel_service | null;
}

export interface ME_me_userPpersonnel_role {
  __typename: "Role";
  code: string | null;
}

export interface ME_me_userPpersonnel_pharmacie {
  __typename: "Pharmacie";
  id: string;
}

export interface ME_me_userPpersonnel {
  __typename: "Ppersonnel";
  id: string;
  nom: string | null;
  prenom: string | null;
  role: ME_me_userPpersonnel_role | null;
  estAmbassadrice: boolean | null;
  pharmacie: ME_me_userPpersonnel_pharmacie | null;
}

export interface ME_me_userLaboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface ME_me_userPartenaire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
}

export interface ME_me_userTitulaire_titulaire_pharmacies {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface ME_me_userTitulaire_titulaire_pharmacieUser {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface ME_me_userTitulaire_titulaire {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  pharmacies: (ME_me_userTitulaire_titulaire_pharmacies | null)[] | null;
  pharmacieUser: ME_me_userTitulaire_titulaire_pharmacieUser | null;
}

export interface ME_me_userTitulaire {
  __typename: "UserTitulaire";
  id: string;
  isPresident: boolean | null;
  titulaire: ME_me_userTitulaire_titulaire | null;
}

export interface ME_me {
  __typename: "User";
  type: string;
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  emailConfirmed: boolean | null;
  phoneNumber: string | null;
  phoneNumberConfirmed: boolean | null;
  twoFactorEnabled: boolean | null;
  lockoutEndDateUtc: any | null;
  lockoutEnabled: boolean | null;
  accessFailedCount: number | null;
  userName: string | null;
  lastLoginDate: any | null;
  lastPasswordChangedDate: any | null;
  isLockedOut: boolean | null;
  isLockedOutPermanent: boolean | null;
  isObligationChangePassword: boolean | null;
  accessFailedCountBeforeLockoutPermanent: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  theme: string | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
  codeTraitements: (string | null)[] | null;
  role: ME_me_role | null;
  groupement: ME_me_groupement | null;
  pharmaciePartenaires: (ME_me_pharmaciePartenaires | null)[] | null;
  pharmacie: ME_me_pharmacie | null;
  userPhoto: ME_me_userPhoto | null;
  userPersonnel: ME_me_userPersonnel | null;
  userPpersonnel: ME_me_userPpersonnel | null;
  userLaboratoire: ME_me_userLaboratoire | null;
  userPartenaire: ME_me_userPartenaire | null;
  userTitulaire: ME_me_userTitulaire | null;
}

export interface ME {
  me: ME_me | null;
}
