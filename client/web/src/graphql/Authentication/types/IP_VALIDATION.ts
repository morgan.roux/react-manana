/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ConnexionStatut } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: IP_VALIDATION
// ====================================================

export interface IP_VALIDATION_validateUserIp {
  __typename: "CheckIpPayload";
  accessToken: string | null;
}

export interface IP_VALIDATION {
  validateUserIp: IP_VALIDATION_validateUserIp | null;
}

export interface IP_VALIDATIONVariables {
  token?: string | null;
  status?: ConnexionStatut | null;
}
