/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: SIGNIN
// ====================================================

export interface SIGNIN_login {
  __typename: "LoginPayload";
  accessToken: string | null;
}

export interface SIGNIN {
  login: SIGNIN_login | null;
}

export interface SIGNINVariables {
  login: string;
  password: string;
  ip?: string | null;
}
