/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: FORGOT_PASSWORD
// ====================================================

export interface FORGOT_PASSWORD {
  forgotPassword: boolean | null;
}

export interface FORGOT_PASSWORDVariables {
  login: string;
}
