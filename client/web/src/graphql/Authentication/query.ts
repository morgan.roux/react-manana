import gql from 'graphql-tag';

export const GET_ME = gql`
  query ME {
    me {
      type
      id
      email
      login
      status
      emailConfirmed
      phoneNumber
      phoneNumberConfirmed
      twoFactorEnabled
      lockoutEndDateUtc
      lockoutEnabled
      accessFailedCount
      userName
      lastLoginDate
      lastPasswordChangedDate
      isLockedOut
      isLockedOutPermanent
      isObligationChangePassword
      accessFailedCountBeforeLockoutPermanent
      dateCreation
      dateModification
      theme
      jourNaissance
      moisNaissance
      anneeNaissance
      codeTraitements
      role {
        code
        nom
      }
      groupement {
        id
        nom
        groupementLogo {
          id
          fichier {
            id
            chemin
          }
        }
      }
      pharmaciePartenaires {
        id
        nom
        ville
        cip
        adresse1
        type
        pharmacieType
        departement {
          id
          nom
          code
          region {
            id
            nom
          }
        }
      }
      pharmacie {
        id
        nom
        type
        pharmacieType
        departement {
          id
          region {
            id
            nom
          }
        }
      }
      userPhoto {
        id
        fichier {
          id
          chemin
          nomOriginal
          publicUrl
        }
      }
      userPersonnel {
        id
        nom
        prenom
        service {
          id
          code
          nom
        }
      }
      userPpersonnel {
        id
        nom
        prenom
        id
        role {
          code
        }
        estAmbassadrice
        pharmacie {
          id
        }
      }
      userLaboratoire {
        id
        nomLabo
      }
      userPartenaire {
        id
        nom
      }
      userTitulaire {
        id
        isPresident
        titulaire {
          id
          nom
          prenom
          pharmacies {
            id
            nom
          }
          pharmacieUser {
            id
            nom
          }
        }
      }
    }
  }
`;

export const GET_IP_VALIDATION = gql`
  query IP_VALIDATION($token: String, $status: ConnexionStatut) {
    validateUserIp(token: $token, status: $status) {
      accessToken
    }
  }
`;
