/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CREATE_GET_PRESIGNED_URL
// ====================================================

export interface CREATE_GET_PRESIGNED_URL_createGetPresignedUrls {
  __typename: "PresignedUrl";
  filePath: string;
  presignedUrl: string;
}

export interface CREATE_GET_PRESIGNED_URL {
  createGetPresignedUrls: (CREATE_GET_PRESIGNED_URL_createGetPresignedUrls | null)[] | null;
}

export interface CREATE_GET_PRESIGNED_URLVariables {
  filePaths?: (string | null)[] | null;
}
