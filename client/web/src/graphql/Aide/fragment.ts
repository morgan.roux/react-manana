import gql from 'graphql-tag';

export const AIDE_INFO = gql`
  fragment AideInfo on Aide {
    id
    title
    description
  }
`;
