/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AideInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_AIDE
// ====================================================

export interface CREATE_UPDATE_AIDE_createUpdateAide {
  __typename: "Aide";
  id: string;
  title: string;
  description: string;
}

export interface CREATE_UPDATE_AIDE {
  createUpdateAide: CREATE_UPDATE_AIDE_createUpdateAide;
}

export interface CREATE_UPDATE_AIDEVariables {
  input: AideInput;
}
