import gql from 'graphql-tag';
import { AIDE_INFO } from './fragment';

export const DO_CREATE_UPDATE_AIDE = gql`
  mutation CREATE_UPDATE_AIDE($input: AideInput!) {
    createUpdateAide(input: $input) {
      ...AideInfo
    }
  }
  ${AIDE_INFO}
`;
