/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: SearchGammeCatalogueInfo
// ====================================================

export interface SearchGammeCatalogueInfo_sousGammes {
  __typename: "SousGammeCatalogue";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
}

export interface SearchGammeCatalogueInfo {
  __typename: "GammeCatalogue";
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
  sousGammes: (SearchGammeCatalogueInfo_sousGammes | null)[] | null;
}
