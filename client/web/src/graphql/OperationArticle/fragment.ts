import gql from 'graphql-tag';
import { PRODUIT_CANAL_OPERATION_INFO_FRAGMENT } from '../ProduitCanal/fragment';

export const OPERATION_ARTICLE_INFO_FRAGEMENT = gql`
  fragment OperationArticleInfo on OperationArticleResult {
    total
    data {
      id
      produitCanal {
        ...ProduitCanalOperationInfo
      }
      quantite
    }
  }
  ${PRODUIT_CANAL_OPERATION_INFO_FRAGMENT}
`;
