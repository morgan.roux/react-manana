/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus, TypePartenaire, StatutPartenaire, TypePresidentCible, TypeProject, MarcheType, MarcheStatus, PromotionType, PromotioStatus, Sexe, GroupeAmisType, GroupeAmisStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: CUSTOM_CONTENT_SEARCH
// ====================================================

export interface CUSTOM_CONTENT_SEARCH_search_data_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Comment" | "Item" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Role" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "GammeCatalogue" | "Partage" | "PartenaireServicePartenaire" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "Aide" | "CommandeLigne" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Personnel__user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Personnel__user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CUSTOM_CONTENT_SEARCH_search_data_Personnel__user_userPhoto_fichier | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Personnel__user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userPhoto: CUSTOM_CONTENT_SEARCH_search_data_Personnel__user_userPhoto | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Personnel_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Personnel_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Personnel {
  __typename: "Personnel";
  type: string;
  id: string;
  sortie: number | null;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  commentaire: string | null;
  dateSortie: any | null;
  idGroupement: string | null;
  _user: CUSTOM_CONTENT_SEARCH_search_data_Personnel__user | null;
  role: CUSTOM_CONTENT_SEARCH_search_data_Personnel_role | null;
  service: CUSTOM_CONTENT_SEARCH_search_data_Personnel_service | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Ppersonnel_pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Ppersonnel_pharmacie {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
  titulaires: (CUSTOM_CONTENT_SEARCH_search_data_Ppersonnel_pharmacie_titulaires | null)[] | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Ppersonnel__user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Ppersonnel_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Ppersonnel {
  __typename: "Ppersonnel";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estAmbassadrice: boolean | null;
  commentaire: string | null;
  sortie: number | null;
  dateSortie: any | null;
  idGroupement: string | null;
  idPharmacie: string | null;
  pharmacie: CUSTOM_CONTENT_SEARCH_search_data_Ppersonnel_pharmacie | null;
  _user: CUSTOM_CONTENT_SEARCH_search_data_Ppersonnel__user | null;
  role: CUSTOM_CONTENT_SEARCH_search_data_Ppersonnel_role | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Partenaire__user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Partenaire_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  commentaire: string | null;
  idGroupement: string | null;
  _user: CUSTOM_CONTENT_SEARCH_search_data_Partenaire__user | null;
  role: CUSTOM_CONTENT_SEARCH_search_data_Partenaire_role | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Titulaire_pharmacies_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Titulaire_pharmacies {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  cip: string | null;
  departement: CUSTOM_CONTENT_SEARCH_search_data_Titulaire_pharmacies_departement | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Titulaire_users_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Titulaire_users_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Titulaire_users_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CUSTOM_CONTENT_SEARCH_search_data_Titulaire_users_userPhoto_fichier | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Titulaire_users {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
  pharmacie: CUSTOM_CONTENT_SEARCH_search_data_Titulaire_users_pharmacie | null;
  userPhoto: CUSTOM_CONTENT_SEARCH_search_data_Titulaire_users_userPhoto | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Titulaire_pharmacieUser {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Titulaire {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (CUSTOM_CONTENT_SEARCH_search_data_Titulaire_pharmacies | null)[] | null;
  users: (CUSTOM_CONTENT_SEARCH_search_data_Titulaire_users | null)[] | null;
  pharmacieUser: CUSTOM_CONTENT_SEARCH_search_data_Titulaire_pharmacieUser | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Groupement {
  __typename: "Groupement";
  type: string;
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Laboratoire_actualites_fichierPresentations {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Laboratoire_actualites {
  __typename: "Actualite";
  id: string;
  libelle: string | null;
  description: string | null;
  fichierPresentations: (CUSTOM_CONTENT_SEARCH_search_data_Laboratoire_actualites_fichierPresentations | null)[] | null;
  dateCreation: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Laboratoire__user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Laboratoire_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Laboratoire_photo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Laboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  countCanalArticles: number | null;
  actualites: (CUSTOM_CONTENT_SEARCH_search_data_Laboratoire_actualites | null)[] | null;
  _user: CUSTOM_CONTENT_SEARCH_search_data_Laboratoire__user | null;
  laboratoirePartenaire: CUSTOM_CONTENT_SEARCH_search_data_Laboratoire_laboratoirePartenaire | null;
  photo: CUSTOM_CONTENT_SEARCH_search_data_Laboratoire_photo | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Pharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Pharmacie_users {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Pharmacie {
  __typename: "Pharmacie";
  type: string;
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: CUSTOM_CONTENT_SEARCH_search_data_Pharmacie_departement | null;
  titulaires: (CUSTOM_CONTENT_SEARCH_search_data_Pharmacie_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
  users: (CUSTOM_CONTENT_SEARCH_search_data_Pharmacie_users | null)[] | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Famille_parent {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  countCanalArticles: number | null;
  libelleFamille: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Famille {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  libelleFamille: string | null;
  countCanalArticles: number | null;
  parent: CUSTOM_CONTENT_SEARCH_search_data_Famille_parent | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Commande {
  __typename: "Commande";
  id: string;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_CanalGamme_sousGamme {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  countCanalArticles: number | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_CanalGamme {
  __typename: "CanalGamme";
  type: string;
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  countCanalArticles: number | null;
  sousGamme: (CUSTOM_CONTENT_SEARCH_search_data_CanalGamme_sousGamme | null)[] | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: CUSTOM_CONTENT_SEARCH_search_data_Actualite_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: CUSTOM_CONTENT_SEARCH_search_data_Actualite_item_fichier | null;
  parent: CUSTOM_CONTENT_SEARCH_search_data_Actualite_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_origine {
  __typename: "ActualiteOrigine";
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_actualiteCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  type: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_actualiteCible {
  __typename: "ActualiteCible";
  id: string | null;
  globalite: boolean | null;
  fichierCible: CUSTOM_CONTENT_SEARCH_search_data_Actualite_actualiteCible_fichierCible | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_servicesCible {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_comments {
  __typename: "CommentResult";
  total: number;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_user_role | null;
  userPhoto: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_user_userPhoto | null;
  pharmacie: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_user_pharmacie | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_item | null;
  idSource: string | null;
  user: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_user | null;
  smyley: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys_data | null)[] | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_pharmaciesCible_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_pharmaciesCible_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_pharmaciesCible {
  __typename: "Pharmacie";
  type: string;
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: CUSTOM_CONTENT_SEARCH_search_data_Actualite_pharmaciesCible_departement | null;
  titulaires: (CUSTOM_CONTENT_SEARCH_search_data_Actualite_pharmaciesCible_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_pharmaciesRolesCible {
  __typename: "Role";
  id: string;
  code: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_presidentsCibles_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_presidentsCibles {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (CUSTOM_CONTENT_SEARCH_search_data_Actualite_presidentsCibles_pharmacies | null)[] | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_partenairesCible {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  idGroupement: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_laboratoiresCible {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_operation {
  __typename: "Operation";
  id: string;
  commandePassee: boolean | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_action_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_action {
  __typename: "Action";
  id: string;
  description: string;
  priority: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  project: CUSTOM_CONTENT_SEARCH_search_data_Actualite_action_project | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Actualite {
  __typename: "Actualite";
  type: string;
  id: string;
  idGroupement: string | null;
  idTache: string | null;
  idFonction: string | null;
  dateEcheance: any | null;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  description: string | null;
  actionAuto: boolean | null;
  niveauPriorite: number | null;
  typePresidentCible: TypePresidentCible | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbSeenByPharmacie: number | null;
  nbPartage: number;
  seen: boolean | null;
  item: CUSTOM_CONTENT_SEARCH_search_data_Actualite_item | null;
  origine: CUSTOM_CONTENT_SEARCH_search_data_Actualite_origine | null;
  laboratoire: CUSTOM_CONTENT_SEARCH_search_data_Actualite_laboratoire | null;
  fichierPresentations: (CUSTOM_CONTENT_SEARCH_search_data_Actualite_fichierPresentations | null)[] | null;
  actualiteCible: CUSTOM_CONTENT_SEARCH_search_data_Actualite_actualiteCible | null;
  servicesCible: (CUSTOM_CONTENT_SEARCH_search_data_Actualite_servicesCible | null)[] | null;
  comments: CUSTOM_CONTENT_SEARCH_search_data_Actualite_comments | null;
  userSmyleys: CUSTOM_CONTENT_SEARCH_search_data_Actualite_userSmyleys | null;
  pharmaciesCible: (CUSTOM_CONTENT_SEARCH_search_data_Actualite_pharmaciesCible | null)[] | null;
  pharmaciesRolesCible: (CUSTOM_CONTENT_SEARCH_search_data_Actualite_pharmaciesRolesCible | null)[] | null;
  presidentsCibles: (CUSTOM_CONTENT_SEARCH_search_data_Actualite_presidentsCibles | null)[] | null;
  partenairesCible: (CUSTOM_CONTENT_SEARCH_search_data_Actualite_partenairesCible | null)[] | null;
  laboratoiresCible: (CUSTOM_CONTENT_SEARCH_search_data_Actualite_laboratoiresCible | null)[] | null;
  operation: CUSTOM_CONTENT_SEARCH_search_data_Actualite_operation | null;
  action: CUSTOM_CONTENT_SEARCH_search_data_Actualite_action | null;
  importance: CUSTOM_CONTENT_SEARCH_search_data_Actualite_importance | null;
  urgence: CUSTOM_CONTENT_SEARCH_search_data_Actualite_urgence | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CUSTOM_CONTENT_SEARCH_search_data_Project_user_userPhoto_fichier | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CUSTOM_CONTENT_SEARCH_search_data_Project_user_role | null;
  userPhoto: CUSTOM_CONTENT_SEARCH_search_data_Project_user_userPhoto | null;
  pharmacie: CUSTOM_CONTENT_SEARCH_search_data_Project_user_pharmacie | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_subProjects_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_subProjects_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_subProjects_participants {
  __typename: "User";
  id: string;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_subProjects_user {
  __typename: "User";
  id: string;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_subProjects {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  ordre: number | null;
  nbAction: number;
  couleur: CUSTOM_CONTENT_SEARCH_search_data_Project_subProjects_couleur | null;
  projetParent: CUSTOM_CONTENT_SEARCH_search_data_Project_subProjects_projetParent | null;
  participants: CUSTOM_CONTENT_SEARCH_search_data_Project_subProjects_participants[] | null;
  user: CUSTOM_CONTENT_SEARCH_search_data_Project_subProjects_user;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project_participants {
  __typename: "User";
  id: string;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Project {
  __typename: "Project";
  type: string;
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  activeActions: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  idFonction: string | null;
  idTache: string | null;
  ordre: number | null;
  nbAction: number;
  projetParent: CUSTOM_CONTENT_SEARCH_search_data_Project_projetParent | null;
  couleur: CUSTOM_CONTENT_SEARCH_search_data_Project_couleur | null;
  groupement: CUSTOM_CONTENT_SEARCH_search_data_Project_groupement | null;
  user: CUSTOM_CONTENT_SEARCH_search_data_Project_user;
  subProjects: CUSTOM_CONTENT_SEARCH_search_data_Project_subProjects[] | null;
  dateCreation: any | null;
  dateModification: any | null;
  participants: CUSTOM_CONTENT_SEARCH_search_data_Project_participants[] | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_CommandeType {
  __typename: "CommandeType";
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_CommandeCanal {
  __typename: "CommandeCanal";
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
  countCanalArticles: number | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ActualiteOrigine {
  __typename: "ActualiteOrigine";
  type: string;
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  countActualites: number | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_User_contact {
  __typename: "Contact";
  telPerso: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_User {
  __typename: "User";
  type: string;
  id: string;
  userName: string | null;
  contact: CUSTOM_CONTENT_SEARCH_search_data_User_contact | null;
  phoneNumber: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Service {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ActiviteUser_idUser_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ActiviteUser_idUser {
  __typename: "User";
  id: string;
  userName: string | null;
  role: CUSTOM_CONTENT_SEARCH_search_data_ActiviteUser_idUser_role | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ActiviteUser_idActiviteType {
  __typename: "ActiviteType";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ActiviteUser {
  __typename: "ActiviteUser";
  id: string;
  type: string;
  dateCreation: any | null;
  idUser: CUSTOM_CONTENT_SEARCH_search_data_ActiviteUser_idUser | null;
  idActiviteType: CUSTOM_CONTENT_SEARCH_search_data_ActiviteUser_idActiviteType | null;
  log: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_TVA {
  __typename: "TVA";
  type: string;
  id: string;
  codeTva: number | null;
  tauxTva: number | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_GroupeClient {
  __typename: "GroupeClient";
  id: string;
  codeGroupe: number | null;
  nomGroupe: string | null;
  nomCommercial: string | null;
  dateValiditeDebut: any | null;
  dateValiditeFin: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Marche_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Marche_userCreated {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Marche {
  __typename: "Marche";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  marcheType: MarcheType | null;
  avecPalier: boolean | null;
  status: MarcheStatus | null;
  nbAdhesion: number | null;
  nbPalier: number | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: CUSTOM_CONTENT_SEARCH_search_data_Marche_commandeCanal | null;
  userCreated: CUSTOM_CONTENT_SEARCH_search_data_Marche_userCreated | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Promotion_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Promotion_userCreated {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Promotion {
  __typename: "Promotion";
  type: string;
  id: string;
  nom: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  promotionType: PromotionType | null;
  promoStatus: PromotioStatus | null;
  dateCreation: any | null;
  dateModification: any | null;
  commandeCanal: CUSTOM_CONTENT_SEARCH_search_data_Promotion_commandeCanal | null;
  userCreated: CUSTOM_CONTENT_SEARCH_search_data_Promotion_userCreated | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Avatar_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Avatar_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Avatar_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  role: CUSTOM_CONTENT_SEARCH_search_data_Avatar_userCreation_role | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Avatar {
  __typename: "Avatar";
  id: string;
  description: string | null;
  codeSexe: Sexe | null;
  dateCreation: any | null;
  dateModification: any | null;
  fichier: CUSTOM_CONTENT_SEARCH_search_data_Avatar_fichier | null;
  userCreation: CUSTOM_CONTENT_SEARCH_search_data_Avatar_userCreation | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_GroupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
  description: string | null;
  typeGroupeAmis: GroupeAmisType;
  nbPharmacie: number;
  groupStatus: GroupeAmisStatus;
  nbMember: number;
  nbPartage: number;
  nbRecommandation: number;
  dateCreation: any | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Operation_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Operation_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_Operation {
  __typename: "Operation";
  id: string;
  accordCommercial: boolean | null;
  commandePassee: boolean | null;
  description: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  libelle: string | null;
  laboratoire: CUSTOM_CONTENT_SEARCH_search_data_Operation_laboratoire | null;
  item: CUSTOM_CONTENT_SEARCH_search_data_Operation_item | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  nomOriginal: string;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_produitPhoto_fichier | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_famille {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  libelleFamille: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_produitTechReg_tva {
  __typename: "TVA";
  id: string;
  codeTva: number | null;
  tauxTva: number | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_produitTechReg_laboExploitant | null;
  tva: CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_produitTechReg_tva | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  produitPhoto: CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_produitPhoto | null;
  produitCode: CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_produitCode | null;
  famille: CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_famille | null;
  produitTechReg: CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit_produitTechReg | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_sousGammeCommercial {
  __typename: "CanalSousGamme";
  id: string;
  libelle: string | null;
}

export interface CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
  qteStock: number | null;
  stv: number | null;
  prixPhv: number | null;
  produit: CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_produit | null;
  remises: (CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_remises | null)[] | null;
  sousGammeCommercial: CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal_sousGammeCommercial | null;
}

export type CUSTOM_CONTENT_SEARCH_search_data = CUSTOM_CONTENT_SEARCH_search_data_Action | CUSTOM_CONTENT_SEARCH_search_data_Personnel | CUSTOM_CONTENT_SEARCH_search_data_Ppersonnel | CUSTOM_CONTENT_SEARCH_search_data_Partenaire | CUSTOM_CONTENT_SEARCH_search_data_Titulaire | CUSTOM_CONTENT_SEARCH_search_data_Groupement | CUSTOM_CONTENT_SEARCH_search_data_Laboratoire | CUSTOM_CONTENT_SEARCH_search_data_Pharmacie | CUSTOM_CONTENT_SEARCH_search_data_Famille | CUSTOM_CONTENT_SEARCH_search_data_Commande | CUSTOM_CONTENT_SEARCH_search_data_CanalGamme | CUSTOM_CONTENT_SEARCH_search_data_Actualite | CUSTOM_CONTENT_SEARCH_search_data_Project | CUSTOM_CONTENT_SEARCH_search_data_CommandeType | CUSTOM_CONTENT_SEARCH_search_data_CommandeCanal | CUSTOM_CONTENT_SEARCH_search_data_ActualiteOrigine | CUSTOM_CONTENT_SEARCH_search_data_User | CUSTOM_CONTENT_SEARCH_search_data_Service | CUSTOM_CONTENT_SEARCH_search_data_ActiviteUser | CUSTOM_CONTENT_SEARCH_search_data_TVA | CUSTOM_CONTENT_SEARCH_search_data_GroupeClient | CUSTOM_CONTENT_SEARCH_search_data_Marche | CUSTOM_CONTENT_SEARCH_search_data_Promotion | CUSTOM_CONTENT_SEARCH_search_data_Avatar | CUSTOM_CONTENT_SEARCH_search_data_GroupeAmis | CUSTOM_CONTENT_SEARCH_search_data_Operation | CUSTOM_CONTENT_SEARCH_search_data_ProduitCanal;

export interface CUSTOM_CONTENT_SEARCH_search {
  __typename: "SearchResult";
  total: number;
  data: (CUSTOM_CONTENT_SEARCH_search_data | null)[] | null;
}

export interface CUSTOM_CONTENT_SEARCH {
  search: CUSTOM_CONTENT_SEARCH_search | null;
}

export interface CUSTOM_CONTENT_SEARCHVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
  idPharmacie?: string | null;
  searchActualiteParams?: string | null;
  userId?: string | null;
  idPharmacieUser?: string | null;
  idGroupement?: string | null;
  actionStatus?: string | null;
  idRemiseOperation?: string | null;
}
