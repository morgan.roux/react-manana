/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MINIMAL_SEARCH
// ====================================================

export interface MINIMAL_SEARCH_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Comment" | "Item" | "Avatar" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface MINIMAL_SEARCH_search_data_ProduitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  libelle2: string | null;
}

export interface MINIMAL_SEARCH_search_data_ProduitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
  produit: MINIMAL_SEARCH_search_data_ProduitCanal_produit | null;
}

export interface MINIMAL_SEARCH_search_data_Service {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
}

export interface MINIMAL_SEARCH_search_data_Pharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface MINIMAL_SEARCH_search_data_Pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface MINIMAL_SEARCH_search_data_Pharmacie {
  __typename: "Pharmacie";
  type: string;
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: MINIMAL_SEARCH_search_data_Pharmacie_departement | null;
  titulaires: (MINIMAL_SEARCH_search_data_Pharmacie_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
}

export interface MINIMAL_SEARCH_search_data_Titulaire_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface MINIMAL_SEARCH_search_data_Titulaire {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (MINIMAL_SEARCH_search_data_Titulaire_pharmacies | null)[] | null;
}

export interface MINIMAL_SEARCH_search_data_Laboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface MINIMAL_SEARCH_search_data_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  idGroupement: string | null;
}

export type MINIMAL_SEARCH_search_data = MINIMAL_SEARCH_search_data_Action | MINIMAL_SEARCH_search_data_ProduitCanal | MINIMAL_SEARCH_search_data_Service | MINIMAL_SEARCH_search_data_Pharmacie | MINIMAL_SEARCH_search_data_Titulaire | MINIMAL_SEARCH_search_data_Laboratoire | MINIMAL_SEARCH_search_data_Partenaire;

export interface MINIMAL_SEARCH_search {
  __typename: "SearchResult";
  total: number;
  data: (MINIMAL_SEARCH_search_data | null)[] | null;
}

export interface MINIMAL_SEARCH {
  search: MINIMAL_SEARCH_search | null;
}

export interface MINIMAL_SEARCHVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
  idGroupement?: string | null;
}
