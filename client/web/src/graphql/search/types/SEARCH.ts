/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus, TypePartenaire, StatutPartenaire, TypePresidentCible, TypeProject, TypeEspace, OriginePublicite, IdeeOuBonnePratiqueOrigine, IdeeOuBonnePratiqueStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH
// ====================================================

export interface SEARCH_search_data_Action {
  __typename: "Action" | "TodoActionType" | "Contact" | "Comment" | "Avatar" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Liste" | "LibelleDivers" | "TauxSS" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "Aide" | "CommandeLigne" | "GroupeAmisDetail" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_search_data_GammeCatalogue_sousGammes {
  __typename: "SousGammeCatalogue";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
}

export interface SEARCH_search_data_GammeCatalogue {
  __typename: "GammeCatalogue";
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  numOrdre: number | null;
  estDefaut: number | null;
  sousGammes: (SEARCH_search_data_GammeCatalogue_sousGammes | null)[] | null;
}

export interface SEARCH_search_data_Acte {
  __typename: "Acte";
  type: string;
  id: string;
  codeActe: number | null;
  libelleActe: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_TVA {
  __typename: "TVA";
  type: string;
  id: string;
  codeTva: number | null;
  tauxTva: number | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Personnel__user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface SEARCH_search_data_Personnel__user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_search_data_Personnel__user_userPhoto_fichier | null;
}

export interface SEARCH_search_data_Personnel__user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userPhoto: SEARCH_search_data_Personnel__user_userPhoto | null;
}

export interface SEARCH_search_data_Personnel_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_Personnel_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_Personnel {
  __typename: "Personnel";
  type: string;
  id: string;
  sortie: number | null;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  commentaire: string | null;
  dateSortie: any | null;
  idGroupement: string | null;
  _user: SEARCH_search_data_Personnel__user | null;
  role: SEARCH_search_data_Personnel_role | null;
  service: SEARCH_search_data_Personnel_service | null;
}

export interface SEARCH_search_data_Ppersonnel_pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface SEARCH_search_data_Ppersonnel_pharmacie {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
  titulaires: (SEARCH_search_data_Ppersonnel_pharmacie_titulaires | null)[] | null;
}

export interface SEARCH_search_data_Ppersonnel__user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
}

export interface SEARCH_search_data_Ppersonnel_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_Ppersonnel {
  __typename: "Ppersonnel";
  type: string;
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estAmbassadrice: boolean | null;
  commentaire: string | null;
  sortie: number | null;
  dateSortie: any | null;
  idGroupement: string | null;
  idPharmacie: string | null;
  pharmacie: SEARCH_search_data_Ppersonnel_pharmacie | null;
  _user: SEARCH_search_data_Ppersonnel__user | null;
  role: SEARCH_search_data_Ppersonnel_role | null;
}

export interface SEARCH_search_data_Partenaire__user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
}

export interface SEARCH_search_data_Partenaire_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_Partenaire {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  commentaire: string | null;
  idGroupement: string | null;
  _user: SEARCH_search_data_Partenaire__user | null;
  role: SEARCH_search_data_Partenaire_role | null;
}

export interface SEARCH_search_data_Titulaire_pharmacies_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_Titulaire_pharmacies {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  cip: string | null;
  departement: SEARCH_search_data_Titulaire_pharmacies_departement | null;
}

export interface SEARCH_search_data_Titulaire_users_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_Titulaire_users_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface SEARCH_search_data_Titulaire_users_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_search_data_Titulaire_users_userPhoto_fichier | null;
}

export interface SEARCH_search_data_Titulaire_users {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
  pharmacie: SEARCH_search_data_Titulaire_users_pharmacie | null;
  userPhoto: SEARCH_search_data_Titulaire_users_userPhoto | null;
}

export interface SEARCH_search_data_Titulaire_pharmacieUser {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_Titulaire {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (SEARCH_search_data_Titulaire_pharmacies | null)[] | null;
  users: (SEARCH_search_data_Titulaire_users | null)[] | null;
  pharmacieUser: SEARCH_search_data_Titulaire_pharmacieUser | null;
}

export interface SEARCH_search_data_Groupement {
  __typename: "Groupement";
  type: string;
  id: string;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Laboratoire_laboratoirePartenaire {
  __typename: "LaboratoirePartenaire";
  id: string;
  debutPartenaire: any | null;
  finPartenaire: any | null;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface SEARCH_search_data_Laboratoire_photo {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SEARCH_search_data_Laboratoire {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  countCanalArticles: number | null;
  sortie: number | null;
  laboratoirePartenaire: SEARCH_search_data_Laboratoire_laboratoirePartenaire | null;
  photo: SEARCH_search_data_Laboratoire_photo | null;
}

export interface SEARCH_search_data_Pharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_Pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface SEARCH_search_data_Pharmacie_users {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface SEARCH_search_data_Pharmacie {
  __typename: "Pharmacie";
  type: string;
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: SEARCH_search_data_Pharmacie_departement | null;
  titulaires: (SEARCH_search_data_Pharmacie_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
  users: (SEARCH_search_data_Pharmacie_users | null)[] | null;
}

export interface SEARCH_search_data_ProduitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface SEARCH_search_data_ProduitCanal_comments_data_fichiers {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface SEARCH_search_data_ProduitCanal_comments_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_ProduitCanal_comments_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_search_data_ProduitCanal_comments_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_search_data_ProduitCanal_comments_data_user_userPhoto_fichier | null;
}

export interface SEARCH_search_data_ProduitCanal_comments_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_ProduitCanal_comments_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_search_data_ProduitCanal_comments_data_user_role | null;
  userPhoto: SEARCH_search_data_ProduitCanal_comments_data_user_userPhoto | null;
  pharmacie: SEARCH_search_data_ProduitCanal_comments_data_user_pharmacie | null;
}

export interface SEARCH_search_data_ProduitCanal_comments_data {
  __typename: "Comment";
  id: string;
  content: string;
  idItemAssocie: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichiers: SEARCH_search_data_ProduitCanal_comments_data_fichiers[] | null;
  user: SEARCH_search_data_ProduitCanal_comments_data_user;
}

export interface SEARCH_search_data_ProduitCanal_comments {
  __typename: "CommentResult";
  total: number;
  data: SEARCH_search_data_ProduitCanal_comments_data[];
}

export interface SEARCH_search_data_ProduitCanal_produit_famille {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  libelleFamille: string | null;
}

export interface SEARCH_search_data_ProduitCanal_produit_produitCode {
  __typename: "ProduitCode";
  id: string;
  code: string | null;
  typeCode: number | null;
  referent: number | null;
}

export interface SEARCH_search_data_ProduitCanal_produit_produitTechReg_laboExploitant {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface SEARCH_search_data_ProduitCanal_produit_produitTechReg {
  __typename: "ProduitTechReg";
  id: string;
  laboExploitant: SEARCH_search_data_ProduitCanal_produit_produitTechReg_laboExploitant | null;
}

export interface SEARCH_search_data_ProduitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
  famille: SEARCH_search_data_ProduitCanal_produit_famille | null;
  produitCode: SEARCH_search_data_ProduitCanal_produit_produitCode | null;
  produitTechReg: SEARCH_search_data_ProduitCanal_produit_produitTechReg | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_search_data_ProduitCanal_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_search_data_ProduitCanal_userSmyleys_data_user_role | null;
  userPhoto: SEARCH_search_data_ProduitCanal_userSmyleys_data_user_userPhoto | null;
  pharmacie: SEARCH_search_data_ProduitCanal_userSmyleys_data_user_pharmacie | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley_groupement | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: SEARCH_search_data_ProduitCanal_userSmyleys_data_item | null;
  idSource: string | null;
  user: SEARCH_search_data_ProduitCanal_userSmyleys_data_user | null;
  smyley: SEARCH_search_data_ProduitCanal_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_ProduitCanal_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (SEARCH_search_data_ProduitCanal_userSmyleys_data | null)[] | null;
}

export interface SEARCH_search_data_ProduitCanal_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface SEARCH_search_data_ProduitCanal_inOtherPanier {
  __typename: "PanierLigne";
  id: string;
}

export interface SEARCH_search_data_ProduitCanal_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
}

export interface SEARCH_search_data_ProduitCanal_operations {
  __typename: "Operation";
  id: string;
}

export interface SEARCH_search_data_ProduitCanal {
  __typename: "ProduitCanal";
  type: string;
  id: string;
  qteStock: number | null;
  stv: number | null;
  unitePetitCond: number | null;
  prixPhv: number | null;
  dateCreation: any | null;
  isActive: boolean | null;
  qteTotalRemisePanachees: number | null;
  inMyPanier: boolean | null;
  remises: (SEARCH_search_data_ProduitCanal_remises | null)[] | null;
  comments: SEARCH_search_data_ProduitCanal_comments | null;
  produit: SEARCH_search_data_ProduitCanal_produit | null;
  userSmyleys: SEARCH_search_data_ProduitCanal_userSmyleys | null;
  commandeCanal: SEARCH_search_data_ProduitCanal_commandeCanal | null;
  inOtherPanier: SEARCH_search_data_ProduitCanal_inOtherPanier | null;
  articleSamePanachees: (SEARCH_search_data_ProduitCanal_articleSamePanachees | null)[] | null;
  operations: (SEARCH_search_data_ProduitCanal_operations | null)[] | null;
}

export interface SEARCH_search_data_Famille_parent {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  countCanalArticles: number | null;
  libelleFamille: string | null;
}

export interface SEARCH_search_data_Famille {
  __typename: "Famille";
  id: string;
  codeFamille: string | null;
  libelleFamille: string | null;
  countCanalArticles: number | null;
  parent: SEARCH_search_data_Famille_parent | null;
}

export interface SEARCH_search_data_Commande {
  __typename: "Commande";
  id: string;
}

export interface SEARCH_search_data_Operation_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
}

export interface SEARCH_search_data_Operation_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface SEARCH_search_data_Operation_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  libelle: string | null;
}

export interface SEARCH_search_data_Operation_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface SEARCH_search_data_Operation_pharmacieVisitors {
  __typename: "Pharmacie";
  id: string;
}

export interface SEARCH_search_data_Operation_pharmacieCible {
  __typename: "Pharmacie";
  id: string;
}

export interface SEARCH_search_data_Operation_operationPharmacie_pharmaciedetails_pharmacie {
  __typename: "Pharmacie";
  id: string;
}

export interface SEARCH_search_data_Operation_operationPharmacie_pharmaciedetails {
  __typename: "OperationPharmacieDetail";
  id: string;
  pharmacie: SEARCH_search_data_Operation_operationPharmacie_pharmaciedetails_pharmacie | null;
}

export interface SEARCH_search_data_Operation_operationPharmacie {
  __typename: "OperationPharmacie";
  id: string;
  globalite: boolean | null;
  pharmaciedetails: (SEARCH_search_data_Operation_operationPharmacie_pharmaciedetails | null)[] | null;
}

export interface SEARCH_search_data_Operation_operationArticles {
  __typename: "OperationArticleResult";
  total: number;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_search_data_Operation_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_search_data_Operation_userSmyleys_data_user_role | null;
  userPhoto: SEARCH_search_data_Operation_userSmyleys_data_user_userPhoto | null;
  pharmacie: SEARCH_search_data_Operation_userSmyleys_data_user_pharmacie | null;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface SEARCH_search_data_Operation_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: SEARCH_search_data_Operation_userSmyleys_data_smyley_groupement | null;
}

export interface SEARCH_search_data_Operation_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: SEARCH_search_data_Operation_userSmyleys_data_item | null;
  idSource: string | null;
  user: SEARCH_search_data_Operation_userSmyleys_data_user | null;
  smyley: SEARCH_search_data_Operation_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Operation_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (SEARCH_search_data_Operation_userSmyleys_data | null)[] | null;
}

export interface SEARCH_search_data_Operation_comments {
  __typename: "CommentResult";
  total: number;
}

export interface SEARCH_search_data_Operation_item {
  __typename: "Item";
  id: string;
  code: string | null;
  codeItem: string | null;
  name: string | null;
}

export interface SEARCH_search_data_Operation_marche_remise {
  __typename: "Remise";
  id: string;
}

export interface SEARCH_search_data_Operation_marche {
  __typename: "Marche";
  id: string;
  nom: string | null;
  remise: SEARCH_search_data_Operation_marche_remise | null;
}

export interface SEARCH_search_data_Operation_promotion_remise {
  __typename: "Remise";
  id: string;
}

export interface SEARCH_search_data_Operation_promotion {
  __typename: "Promotion";
  id: string;
  nom: string | null;
  remise: SEARCH_search_data_Operation_promotion_remise | null;
}

export interface SEARCH_search_data_Operation {
  __typename: "Operation";
  type: string;
  id: string;
  commandePassee: boolean | null;
  isRemoved: boolean | null;
  seen: boolean | null;
  caMoyenPharmacie: number | null;
  nbPharmacieCommande: number | null;
  nbMoyenLigne: number | null;
  commandeType: SEARCH_search_data_Operation_commandeType | null;
  fichierPresentations: (SEARCH_search_data_Operation_fichierPresentations | null)[] | null;
  commandeCanal: SEARCH_search_data_Operation_commandeCanal | null;
  laboratoire: SEARCH_search_data_Operation_laboratoire | null;
  pharmacieVisitors: (SEARCH_search_data_Operation_pharmacieVisitors | null)[] | null;
  idGroupement: string | null;
  pharmacieCible: (SEARCH_search_data_Operation_pharmacieCible | null)[] | null;
  operationPharmacie: SEARCH_search_data_Operation_operationPharmacie | null;
  operationArticles: SEARCH_search_data_Operation_operationArticles | null;
  libelle: string | null;
  niveauPriorite: number | null;
  description: string | null;
  dateDebut: any | null;
  dateModification: any | null;
  dateFin: any | null;
  dateCreation: any | null;
  nombrePharmaciesConsultes: number | null;
  nombrePharmaciesCommandees: number | null;
  nombreProduitsCommandes: number | null;
  nombreTotalTypeProduitsCommandes: number | null;
  nombreProduitsMoyenParPharmacie: number | null;
  qteProduitsPharmacieCommandePassee: number | null;
  qteTotalProduitsCommandePassee: number | null;
  nombreCommandePassee: number | null;
  totalCommandePassee: number | null;
  CATotal: number | null;
  CAMoyenParPharmacie: number | null;
  userSmyleys: SEARCH_search_data_Operation_userSmyleys | null;
  comments: SEARCH_search_data_Operation_comments | null;
  item: SEARCH_search_data_Operation_item | null;
  marche: SEARCH_search_data_Operation_marche | null;
  promotion: SEARCH_search_data_Operation_promotion | null;
}

export interface SEARCH_search_data_CanalGamme_sousGamme {
  __typename: "CanalSousGamme";
  id: string;
  codeSousGamme: number | null;
  libelle: string | null;
  countCanalArticles: number | null;
}

export interface SEARCH_search_data_CanalGamme {
  __typename: "CanalGamme";
  type: string;
  id: string;
  codeGamme: number | null;
  libelle: string | null;
  countCanalArticles: number | null;
  sousGamme: (SEARCH_search_data_CanalGamme_sousGamme | null)[] | null;
}

export interface SEARCH_search_data_Actualite_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Actualite_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Actualite_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: SEARCH_search_data_Actualite_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Actualite_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: SEARCH_search_data_Actualite_item_fichier | null;
  parent: SEARCH_search_data_Actualite_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Actualite_origine {
  __typename: "ActualiteOrigine";
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Actualite_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface SEARCH_search_data_Actualite_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Actualite_actualiteCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  type: string;
  nomOriginal: string;
  publicUrl: string | null;
  urlPresigned: string | null;
}

export interface SEARCH_search_data_Actualite_actualiteCible {
  __typename: "ActualiteCible";
  id: string | null;
  globalite: boolean | null;
  fichierCible: SEARCH_search_data_Actualite_actualiteCible_fichierCible | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Actualite_servicesCible {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
}

export interface SEARCH_search_data_Actualite_comments {
  __typename: "CommentResult";
  total: number;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_search_data_Actualite_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_search_data_Actualite_userSmyleys_data_user_role | null;
  userPhoto: SEARCH_search_data_Actualite_userSmyleys_data_user_userPhoto | null;
  pharmacie: SEARCH_search_data_Actualite_userSmyleys_data_user_pharmacie | null;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: SEARCH_search_data_Actualite_userSmyleys_data_smyley_groupement | null;
}

export interface SEARCH_search_data_Actualite_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: SEARCH_search_data_Actualite_userSmyleys_data_item | null;
  idSource: string | null;
  user: SEARCH_search_data_Actualite_userSmyleys_data_user | null;
  smyley: SEARCH_search_data_Actualite_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Actualite_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (SEARCH_search_data_Actualite_userSmyleys_data | null)[] | null;
}

export interface SEARCH_search_data_Actualite_pharmaciesCible_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_Actualite_pharmaciesCible_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
}

export interface SEARCH_search_data_Actualite_pharmaciesCible {
  __typename: "Pharmacie";
  type: string;
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: SEARCH_search_data_Actualite_pharmaciesCible_departement | null;
  titulaires: (SEARCH_search_data_Actualite_pharmaciesCible_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
}

export interface SEARCH_search_data_Actualite_pharmaciesRolesCible {
  __typename: "Role";
  id: string;
  code: string | null;
}

export interface SEARCH_search_data_Actualite_presidentsCibles_pharmacies {
  __typename: "Pharmacie";
  id: string;
}

export interface SEARCH_search_data_Actualite_presidentsCibles {
  __typename: "Titulaire";
  type: string;
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  estPresidentRegion: boolean | null;
  idGroupement: string | null;
  pharmacies: (SEARCH_search_data_Actualite_presidentsCibles_pharmacies | null)[] | null;
}

export interface SEARCH_search_data_Actualite_partenairesCible {
  __typename: "Partenaire";
  type: string;
  id: string;
  nom: string | null;
  idGroupement: string | null;
}

export interface SEARCH_search_data_Actualite_laboratoiresCible {
  __typename: "Laboratoire";
  type: string;
  id: string;
  nomLabo: string | null;
  sortie: number | null;
}

export interface SEARCH_search_data_Actualite_operation {
  __typename: "Operation";
  id: string;
  commandePassee: boolean | null;
}

export interface SEARCH_search_data_Actualite_action_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface SEARCH_search_data_Actualite_action {
  __typename: "Action";
  id: string;
  description: string;
  priority: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  project: SEARCH_search_data_Actualite_action_project | null;
}

export interface SEARCH_search_data_Actualite_importance {
  __typename: "Importance";
  id: string;
  ordre: number | null;
  libelle: string | null;
  couleur: string | null;
}

export interface SEARCH_search_data_Actualite_urgence {
  __typename: "Urgence";
  id: string;
  code: string | null;
  libelle: string | null;
  couleur: string | null;
}

export interface SEARCH_search_data_Actualite {
  __typename: "Actualite";
  type: string;
  id: string;
  idGroupement: string | null;
  idTache: string | null;
  idFonction: string | null;
  dateEcheance: any | null;
  libelle: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  description: string | null;
  actionAuto: boolean | null;
  niveauPriorite: number | null;
  typePresidentCible: TypePresidentCible | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbSeenByPharmacie: number | null;
  nbPartage: number;
  seen: boolean | null;
  item: SEARCH_search_data_Actualite_item | null;
  origine: SEARCH_search_data_Actualite_origine | null;
  laboratoire: SEARCH_search_data_Actualite_laboratoire | null;
  fichierPresentations: (SEARCH_search_data_Actualite_fichierPresentations | null)[] | null;
  actualiteCible: SEARCH_search_data_Actualite_actualiteCible | null;
  servicesCible: (SEARCH_search_data_Actualite_servicesCible | null)[] | null;
  comments: SEARCH_search_data_Actualite_comments | null;
  userSmyleys: SEARCH_search_data_Actualite_userSmyleys | null;
  pharmaciesCible: (SEARCH_search_data_Actualite_pharmaciesCible | null)[] | null;
  pharmaciesRolesCible: (SEARCH_search_data_Actualite_pharmaciesRolesCible | null)[] | null;
  presidentsCibles: (SEARCH_search_data_Actualite_presidentsCibles | null)[] | null;
  partenairesCible: (SEARCH_search_data_Actualite_partenairesCible | null)[] | null;
  laboratoiresCible: (SEARCH_search_data_Actualite_laboratoiresCible | null)[] | null;
  operation: SEARCH_search_data_Actualite_operation | null;
  action: SEARCH_search_data_Actualite_action | null;
  importance: SEARCH_search_data_Actualite_importance | null;
  urgence: SEARCH_search_data_Actualite_urgence | null;
}

export interface SEARCH_search_data_Project_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Project_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface SEARCH_search_data_Project_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_Project_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_Project_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_search_data_Project_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_search_data_Project_user_userPhoto_fichier | null;
}

export interface SEARCH_search_data_Project_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_Project_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_search_data_Project_user_role | null;
  userPhoto: SEARCH_search_data_Project_user_userPhoto | null;
  pharmacie: SEARCH_search_data_Project_user_pharmacie | null;
}

export interface SEARCH_search_data_Project_subProjects_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
}

export interface SEARCH_search_data_Project_subProjects_projetParent {
  __typename: "Project";
  id: string;
  _name: string | null;
}

export interface SEARCH_search_data_Project_subProjects_participants {
  __typename: "User";
  id: string;
}

export interface SEARCH_search_data_Project_subProjects_user {
  __typename: "User";
  id: string;
}

export interface SEARCH_search_data_Project_subProjects {
  __typename: "Project";
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  ordre: number | null;
  nbAction: number;
  couleur: SEARCH_search_data_Project_subProjects_couleur | null;
  projetParent: SEARCH_search_data_Project_subProjects_projetParent | null;
  participants: SEARCH_search_data_Project_subProjects_participants[] | null;
  user: SEARCH_search_data_Project_subProjects_user;
}

export interface SEARCH_search_data_Project_participants {
  __typename: "User";
  id: string;
}

export interface SEARCH_search_data_Project {
  __typename: "Project";
  type: string;
  id: string;
  _name: string | null;
  typeProject: TypeProject | null;
  isArchived: boolean | null;
  isShared: boolean | null;
  activeActions: boolean | null;
  isRemoved: boolean | null;
  isInFavoris: boolean | null;
  idFonction: string | null;
  idTache: string | null;
  ordre: number | null;
  nbAction: number;
  projetParent: SEARCH_search_data_Project_projetParent | null;
  couleur: SEARCH_search_data_Project_couleur | null;
  groupement: SEARCH_search_data_Project_groupement | null;
  user: SEARCH_search_data_Project_user;
  subProjects: SEARCH_search_data_Project_subProjects[] | null;
  dateCreation: any | null;
  dateModification: any | null;
  participants: SEARCH_search_data_Project_participants[] | null;
}

export interface SEARCH_search_data_CommandeType {
  __typename: "CommandeType";
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
}

export interface SEARCH_search_data_CommandeCanal {
  __typename: "CommandeCanal";
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
  countCanalArticles: number | null;
}

export interface SEARCH_search_data_ActualiteOrigine {
  __typename: "ActualiteOrigine";
  type: string;
  id: string;
  code: string | null;
  libelle: string | null;
  ordre: number | null;
  countActualites: number | null;
}

export interface SEARCH_search_data_User_contact {
  __typename: "Contact";
  telPerso: string | null;
}

export interface SEARCH_search_data_User {
  __typename: "User";
  type: string;
  id: string;
  userName: string | null;
  contact: SEARCH_search_data_User_contact | null;
  phoneNumber: string | null;
}

export interface SEARCH_search_data_Service {
  __typename: "Service";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
  countUsers: number | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_ActiviteUser_idUser_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_ActiviteUser_idUser {
  __typename: "User";
  id: string;
  userName: string | null;
  role: SEARCH_search_data_ActiviteUser_idUser_role | null;
}

export interface SEARCH_search_data_ActiviteUser_idActiviteType {
  __typename: "ActiviteType";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_ActiviteUser {
  __typename: "ActiviteUser";
  id: string;
  type: string;
  dateCreation: any | null;
  idUser: SEARCH_search_data_ActiviteUser_idUser | null;
  idActiviteType: SEARCH_search_data_ActiviteUser_idActiviteType | null;
  log: string | null;
}

export interface SEARCH_search_data_Publicite_item {
  __typename: "Item";
  id: string;
  codeItem: string | null;
  name: string | null;
}

export interface SEARCH_search_data_Publicite_image {
  __typename: "Fichier";
  id: string;
  nomOriginal: string;
  publicUrl: string | null;
  chemin: string;
}

export interface SEARCH_search_data_Publicite {
  __typename: "Publicite";
  type: string;
  id: string;
  libelle: string | null;
  description: string | null;
  typeEspace: TypeEspace | null;
  origineType: OriginePublicite | null;
  idItemSource: string | null;
  url: string | null;
  ordre: number | null;
  dateDebut: any | null;
  dateFin: any | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  item: SEARCH_search_data_Publicite_item | null;
  image: SEARCH_search_data_Publicite_image | null;
}

export interface SEARCH_search_data_Item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: SEARCH_search_data_Item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_Item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: SEARCH_search_data_Item_fichier | null;
  parent: SEARCH_search_data_Item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_classification_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_classification_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_classification_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_search_data_IdeeOuBonnePratique_classification_userCreation_userPhoto_fichier | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_classification_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_classification_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_search_data_IdeeOuBonnePratique_classification_userCreation_role | null;
  userPhoto: SEARCH_search_data_IdeeOuBonnePratique_classification_userCreation_userPhoto | null;
  pharmacie: SEARCH_search_data_IdeeOuBonnePratique_classification_userCreation_pharmacie | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_classification {
  __typename: "IdeeOuBonnePratiqueClassification";
  id: string;
  nom: string;
  userCreation: SEARCH_search_data_IdeeOuBonnePratique_classification_userCreation | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_auteur_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_auteur_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_auteur_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_search_data_IdeeOuBonnePratique_auteur_userPhoto_fichier | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_auteur_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_auteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_search_data_IdeeOuBonnePratique_auteur_role | null;
  userPhoto: SEARCH_search_data_IdeeOuBonnePratique_auteur_userPhoto | null;
  pharmacie: SEARCH_search_data_IdeeOuBonnePratique_auteur_pharmacie | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_fournisseur_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_fournisseur {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  laboSuite: SEARCH_search_data_IdeeOuBonnePratique_fournisseur_laboSuite | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_prestataire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
  commentaire: string | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: SEARCH_search_data_IdeeOuBonnePratique_groupement_defaultPharmacie_departement_region | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: SEARCH_search_data_IdeeOuBonnePratique_groupement_defaultPharmacie_departement | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: SEARCH_search_data_IdeeOuBonnePratique_groupement_groupementLogo_fichier | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: SEARCH_search_data_IdeeOuBonnePratique_groupement_defaultPharmacie | null;
  groupementLogo: SEARCH_search_data_IdeeOuBonnePratique_groupement_groupementLogo | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueChangeStatus {
  __typename: "IdeeOuBonnePratiqueChangeStatus";
  id: string;
  commentaire: string | null;
  isRemoved: boolean | null;
  status: IdeeOuBonnePratiqueStatus;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueFichierJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueFichierJoints {
  __typename: "IdeeOuBonnePratiqueFichierJoint";
  id: string;
  fichier: SEARCH_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueFichierJoints_fichier;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueLus {
  __typename: "IdeeOuBonnePratiqueLu";
  id: string;
}

export interface SEARCH_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueSmyleys {
  __typename: "UserSmyley";
  id: string;
}

export interface SEARCH_search_data_IdeeOuBonnePratique {
  __typename: "IdeeOuBonnePratique";
  id: string;
  _origine: IdeeOuBonnePratiqueOrigine | null;
  status: IdeeOuBonnePratiqueStatus;
  nbLu: number;
  lu: boolean;
  nbSmyley: number;
  nbComment: number;
  nbPartage: number;
  classification: SEARCH_search_data_IdeeOuBonnePratique_classification | null;
  title: string;
  auteur: SEARCH_search_data_IdeeOuBonnePratique_auteur;
  concurent: string | null;
  fournisseur: SEARCH_search_data_IdeeOuBonnePratique_fournisseur | null;
  prestataire: SEARCH_search_data_IdeeOuBonnePratique_prestataire | null;
  groupeAmis: SEARCH_search_data_IdeeOuBonnePratique_groupeAmis | null;
  service: SEARCH_search_data_IdeeOuBonnePratique_service | null;
  beneficiaires_cles: string | null;
  contexte: string | null;
  objectifs: string | null;
  resultats: string | null;
  facteursClesDeSucces: string | null;
  contraintes: string | null;
  groupement: SEARCH_search_data_IdeeOuBonnePratique_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: SEARCH_search_data_IdeeOuBonnePratique_userCreation | null;
  dateCreation: any | null;
  dateModification: any | null;
  ideeOuBonnePratiqueChangeStatus: (SEARCH_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueChangeStatus | null)[];
  ideeOuBonnePratiqueFichierJoints: SEARCH_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueFichierJoints[];
  ideeOuBonnePratiqueLus: SEARCH_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueLus[];
  ideeOuBonnePratiqueSmyleys: SEARCH_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueSmyleys[];
}

export interface SEARCH_search_data_Role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export type SEARCH_search_data = SEARCH_search_data_Action | SEARCH_search_data_GammeCatalogue | SEARCH_search_data_Acte | SEARCH_search_data_TVA | SEARCH_search_data_Personnel | SEARCH_search_data_Ppersonnel | SEARCH_search_data_Partenaire | SEARCH_search_data_Titulaire | SEARCH_search_data_Groupement | SEARCH_search_data_Laboratoire | SEARCH_search_data_Pharmacie | SEARCH_search_data_ProduitCanal | SEARCH_search_data_Famille | SEARCH_search_data_Commande | SEARCH_search_data_Operation | SEARCH_search_data_CanalGamme | SEARCH_search_data_Actualite | SEARCH_search_data_Project | SEARCH_search_data_CommandeType | SEARCH_search_data_CommandeCanal | SEARCH_search_data_ActualiteOrigine | SEARCH_search_data_User | SEARCH_search_data_Service | SEARCH_search_data_ActiviteUser | SEARCH_search_data_Publicite | SEARCH_search_data_Item | SEARCH_search_data_IdeeOuBonnePratique | SEARCH_search_data_Role;

export interface SEARCH_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_search_data | null)[] | null;
}

export interface SEARCH {
  search: SEARCH_search | null;
}

export interface SEARCHVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
  idPharmacie?: string | null;
  searchActualiteParams?: string | null;
  userId?: string | null;
  idPharmacieUser?: string | null;
  idGroupement?: string | null;
  actionStatus?: string | null;
  idRemiseOperation?: string | null;
}
