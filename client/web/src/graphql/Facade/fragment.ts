import gql from 'graphql-tag';

export const FACADE_INFO_FRAGMENT = gql`
  fragment FacadeInfo on Facade {
    id
    modele
    codeMaj
    dateCreation
    dateModification
  }
`;
