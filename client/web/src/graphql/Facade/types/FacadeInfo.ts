/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FacadeInfo
// ====================================================

export interface FacadeInfo {
  __typename: "Facade";
  id: string;
  modele: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}
