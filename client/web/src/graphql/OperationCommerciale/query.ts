import gql from 'graphql-tag';
import {
  OPERATION_COMMERCIALE_INFO_FRAGEMENT,
  OPERATION_COMMERCIALE_PRESENTATION_FRAGEMENT,
} from './fragment';
export const GET_OPERATION_COMMERCIALE = gql`
  query OPERATION_COMMERCIALE(
    $id: ID!
    $idPharmacie: ID!
    $userId: String
    $idPharmacieUser: ID
    $idRemiseOperation: ID
  ) {
    operation(id: $id) {
      ...OperationCommercialeInfo
    }
  }
  ${OPERATION_COMMERCIALE_INFO_FRAGEMENT}
`;

export const GET_OPERATION_PRESENTATION = gql`
  query OPERATION_PRESENTATION($id: ID!, $userId: String, $idPharmacieUser: ID) {
    operation(id: $id) {
      ...OperationCommercialePresentation
    }
  }
  ${OPERATION_COMMERCIALE_PRESENTATION_FRAGEMENT}
`;

export const GET_OPERATION_ARTICLES = gql`
  query OPERATION_ARTICLES($id: ID!, $idPharmacie: ID!, $idRemiseOperation: ID) {
    operation(id: $id) {
      operationArticlesWithQte {
        id
        quantite
        produitCanal {
          id
          prixPhv
          qteStock
          remises(idPharmacie: $idPharmacie, idRemiseOperation: $idRemiseOperation) {
            id
            quantiteMin
            quantiteMax
            pourcentageRemise
            nombreUg
          }
        }
      }
    }
  }
`;

export const GET_OPERATION_SEARCH = gql`
  query OPERATION_SEARCH(
    $type: [String]
    $query: JSON
    $skip: Int
    $take: Int
    $filterBy: JSON
    $sortBy: JSON
    $idPharmacie: ID
    $userId: String
    $idPharmacieUser: ID
    $idRemiseOperation: ID
  ) {
    search(
      type: $type
      query: $query
      skip: $skip
      take: $take
      filterBy: $filterBy
      sortBy: $sortBy
    ) {
      total
      data {
        ... on Operation {
          ...OperationCommercialeInfo
        }
      }
    }
  }
  ${OPERATION_COMMERCIALE_INFO_FRAGEMENT}
`;

export const GET_SEARCH_CUSTOM_CONTENT_OPERATION = gql`
  query SEARCH_CUSTOM_CONTENT_OPERATION($type: [String], $query: JSON, $skip: Int, $take: Int) {
    search(type: $type, query: $query, skip: $skip, take: $take) {
      total
      data {
        ... on Operation {
          id
          description
          dateDebut
          dateFin
          libelle
          commandeType {
            id
            libelle
          }
          commandeCanal {
            id
            libelle
          }
          laboratoire {
            id
            nomLabo
          }
          CAMoyenParPharmacie
          CATotal
        }
      }
    }
  }
`;
