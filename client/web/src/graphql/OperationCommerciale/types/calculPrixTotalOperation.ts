/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ArticleCommande } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: calculPrixTotalOperation
// ====================================================

export interface calculPrixTotalOperation_calculPrixTotalOperation {
  __typename: "PrixTotalOperation";
  qteTotal: number | null;
  prixBaseTotal: number | null;
  prixNetTotal: number | null;
  uniteGratuite: number | null;
}

export interface calculPrixTotalOperation {
  calculPrixTotalOperation: calculPrixTotalOperation_calculPrixTotalOperation | null;
}

export interface calculPrixTotalOperationVariables {
  articles?: (ArticleCommande | null)[] | null;
  idPharmacie: string;
  idOperation: string;
}
