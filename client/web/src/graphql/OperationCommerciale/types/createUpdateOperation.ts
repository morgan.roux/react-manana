/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput, OperationArticleInput, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: createUpdateOperation
// ====================================================

export interface createUpdateOperation_createUpdateOperation_pharmacieVisitors {
  __typename: "Pharmacie";
  id: string;
}

export interface createUpdateOperation_createUpdateOperation_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface createUpdateOperation_createUpdateOperation_pharmacieCible {
  __typename: "Pharmacie";
  id: string;
}

export interface createUpdateOperation_createUpdateOperation_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface createUpdateOperation_createUpdateOperation_item_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface createUpdateOperation_createUpdateOperation_item_parent_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface createUpdateOperation_createUpdateOperation_item_parent {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: createUpdateOperation_createUpdateOperation_item_parent_fichier | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface createUpdateOperation_createUpdateOperation_item {
  __typename: "Item";
  type: string;
  id: string;
  code: string | null;
  name: string | null;
  codeItem: string | null;
  fichier: createUpdateOperation_createUpdateOperation_item_fichier | null;
  parent: createUpdateOperation_createUpdateOperation_item_parent | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface createUpdateOperation_createUpdateOperation_actions_data_project {
  __typename: "Project";
  id: string;
  name: string | null;
}

export interface createUpdateOperation_createUpdateOperation_actions_data {
  __typename: "Action";
  id: string;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  project: createUpdateOperation_createUpdateOperation_actions_data_project | null;
}

export interface createUpdateOperation_createUpdateOperation_actions {
  __typename: "ActionResult";
  total: number;
  data: createUpdateOperation_createUpdateOperation_actions_data[];
}

export interface createUpdateOperation_createUpdateOperation_operationPharmacie_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface createUpdateOperation_createUpdateOperation_operationPharmacie_pharmaciedetails_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface createUpdateOperation_createUpdateOperation_operationPharmacie_pharmaciedetails {
  __typename: "OperationPharmacieDetail";
  id: string;
  pharmacie: createUpdateOperation_createUpdateOperation_operationPharmacie_pharmaciedetails_pharmacie | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface createUpdateOperation_createUpdateOperation_operationPharmacie {
  __typename: "OperationPharmacie";
  id: string;
  globalite: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  fichierCible: createUpdateOperation_createUpdateOperation_operationPharmacie_fichierCible | null;
  pharmaciedetails: (createUpdateOperation_createUpdateOperation_operationPharmacie_pharmaciedetails | null)[] | null;
}

export interface createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal_commandeCanal {
  __typename: "CommandeCanal";
  id: string;
  code: string | null;
}

export interface createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal_produit_produitPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  nomOriginal: string;
}

export interface createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal_produit_produitPhoto {
  __typename: "ProduitPhoto";
  id: string;
  fichier: createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal_produit_produitPhoto_fichier | null;
}

export interface createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal_produit {
  __typename: "Produit";
  id: string;
  produitPhoto: createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal_produit_produitPhoto | null;
}

export interface createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal_articleSamePanachees {
  __typename: "ProduitCanal";
  id: string;
}

export interface createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal {
  __typename: "ProduitCanal";
  id: string;
  qteStock: number | null;
  stv: number | null;
  prixPhv: number | null;
  commandeCanal: createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal_commandeCanal | null;
  produit: createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal_produit | null;
  articleSamePanachees: (createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal_articleSamePanachees | null)[] | null;
  remises: (createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal_remises | null)[] | null;
}

export interface createUpdateOperation_createUpdateOperation_operationArticles_data {
  __typename: "OperationArticle";
  id: string;
  produitCanal: createUpdateOperation_createUpdateOperation_operationArticles_data_produitCanal | null;
  quantite: number | null;
}

export interface createUpdateOperation_createUpdateOperation_operationArticles {
  __typename: "OperationArticleResult";
  total: number;
  data: (createUpdateOperation_createUpdateOperation_operationArticles_data | null)[] | null;
}

export interface createUpdateOperation_createUpdateOperation_operationArticleCible_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface createUpdateOperation_createUpdateOperation_operationArticleCible {
  __typename: "OperationArticleCible";
  id: string;
  dateCreation: any | null;
  dateModification: any | null;
  fichierCible: createUpdateOperation_createUpdateOperation_operationArticleCible_fichierCible | null;
}

export interface createUpdateOperation_createUpdateOperation_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface createUpdateOperation_createUpdateOperation_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: createUpdateOperation_createUpdateOperation_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: createUpdateOperation_createUpdateOperation_userSmyleys_data_user_role | null;
  userPhoto: createUpdateOperation_createUpdateOperation_userSmyleys_data_user_userPhoto | null;
  pharmacie: createUpdateOperation_createUpdateOperation_userSmyleys_data_user_pharmacie | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley_groupement | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: createUpdateOperation_createUpdateOperation_userSmyleys_data_item | null;
  idSource: string | null;
  user: createUpdateOperation_createUpdateOperation_userSmyleys_data_user | null;
  smyley: createUpdateOperation_createUpdateOperation_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface createUpdateOperation_createUpdateOperation_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (createUpdateOperation_createUpdateOperation_userSmyleys_data | null)[] | null;
}

export interface createUpdateOperation_createUpdateOperation_comments {
  __typename: "CommentResult";
  total: number;
}

export interface createUpdateOperation_createUpdateOperation_marche_remise {
  __typename: "Remise";
  id: string;
}

export interface createUpdateOperation_createUpdateOperation_marche {
  __typename: "Marche";
  id: string;
  nom: string | null;
  remise: createUpdateOperation_createUpdateOperation_marche_remise | null;
}

export interface createUpdateOperation_createUpdateOperation_promotion_remise {
  __typename: "Remise";
  id: string;
}

export interface createUpdateOperation_createUpdateOperation_promotion {
  __typename: "Promotion";
  id: string;
  nom: string | null;
  remise: createUpdateOperation_createUpdateOperation_promotion_remise | null;
}

export interface createUpdateOperation_createUpdateOperation {
  __typename: "Operation";
  id: string;
  accordCommercial: boolean | null;
  commandePassee: boolean | null;
  isRemoved: boolean | null;
  seen: boolean | null;
  pharmacieVisitors: (createUpdateOperation_createUpdateOperation_pharmacieVisitors | null)[] | null;
  laboratoire: createUpdateOperation_createUpdateOperation_laboratoire | null;
  idGroupement: string | null;
  pharmacieCible: (createUpdateOperation_createUpdateOperation_pharmacieCible | null)[] | null;
  fichierPresentations: (createUpdateOperation_createUpdateOperation_fichierPresentations | null)[] | null;
  libelle: string | null;
  niveauPriorite: number | null;
  description: string | null;
  dateDebut: any | null;
  dateModification: any | null;
  dateFin: any | null;
  dateCreation: any | null;
  nombrePharmaciesConsultes: number | null;
  nombrePharmaciesCommandees: number | null;
  nombreProduitsCommandes: number | null;
  nombreTotalTypeProduitsCommandes: number | null;
  CAMoyenParPharmacie: number | null;
  CATotal: number | null;
  nombreProduitsMoyenParPharmacie: number | null;
  qteProduitsPharmacieCommandePassee: number | null;
  qteTotalProduitsCommandePassee: number | null;
  nombreCommandePassee: number | null;
  totalCommandePassee: number | null;
  caMoyenPharmacie: number | null;
  nbPharmacieCommande: number | null;
  nbMoyenLigne: number | null;
  item: createUpdateOperation_createUpdateOperation_item | null;
  actions: createUpdateOperation_createUpdateOperation_actions | null;
  operationPharmacie: createUpdateOperation_createUpdateOperation_operationPharmacie | null;
  operationArticles: createUpdateOperation_createUpdateOperation_operationArticles | null;
  operationArticleCible: createUpdateOperation_createUpdateOperation_operationArticleCible | null;
  commandeType: createUpdateOperation_createUpdateOperation_commandeType | null;
  commandeCanal: createUpdateOperation_createUpdateOperation_commandeCanal | null;
  userSmyleys: createUpdateOperation_createUpdateOperation_userSmyleys | null;
  comments: createUpdateOperation_createUpdateOperation_comments | null;
  marche: createUpdateOperation_createUpdateOperation_marche | null;
  promotion: createUpdateOperation_createUpdateOperation_promotion | null;
}

export interface createUpdateOperation {
  createUpdateOperation: createUpdateOperation_createUpdateOperation | null;
}

export interface createUpdateOperationVariables {
  idOperation?: string | null;
  codeItem: string;
  libelle: string;
  description: string;
  niveauPriorite: number;
  dateDebut: any;
  dateFin: any;
  idLaboratoire?: string | null;
  fichierPresentations: (FichierInput | null)[];
  globalite?: boolean | null;
  typeCommande?: string | null;
  idsPharmacie?: (string | null)[] | null;
  articles?: (OperationArticleInput | null)[] | null;
  fichierPharmacie?: FichierInput | null;
  fichierProduit?: FichierInput | null;
  idCanalCommande: string;
  accordCommercial: boolean;
  idPharmacie?: string | null;
  userId?: string | null;
  idPharmacieUser?: string | null;
  idProject?: string | null;
  actionPriorite?: number | null;
  actionDescription?: string | null;
  actionDueDate?: string | null;
  caMoyenPharmacie?: number | null;
  nbPharmacieCommande?: number | null;
  nbMoyenLigne?: number | null;
  idMarche?: string | null;
  idPromotion?: string | null;
  idRemiseOperation?: string | null;
}
