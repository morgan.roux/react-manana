/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: OPERATION_ARTICLES
// ====================================================

export interface OPERATION_ARTICLES_operation_operationArticlesWithQte_produitCanal_remises {
  __typename: "RemiseDetail";
  id: string;
  quantiteMin: number | null;
  quantiteMax: number | null;
  pourcentageRemise: number | null;
  nombreUg: number | null;
}

export interface OPERATION_ARTICLES_operation_operationArticlesWithQte_produitCanal {
  __typename: "ProduitCanal";
  id: string;
  prixPhv: number | null;
  qteStock: number | null;
  remises: (OPERATION_ARTICLES_operation_operationArticlesWithQte_produitCanal_remises | null)[] | null;
}

export interface OPERATION_ARTICLES_operation_operationArticlesWithQte {
  __typename: "OperationArticle";
  id: string;
  quantite: number | null;
  produitCanal: OPERATION_ARTICLES_operation_operationArticlesWithQte_produitCanal | null;
}

export interface OPERATION_ARTICLES_operation {
  __typename: "Operation";
  operationArticlesWithQte: (OPERATION_ARTICLES_operation_operationArticlesWithQte | null)[] | null;
}

export interface OPERATION_ARTICLES {
  operation: OPERATION_ARTICLES_operation | null;
}

export interface OPERATION_ARTICLESVariables {
  id: string;
  idPharmacie: string;
  idRemiseOperation?: string | null;
}
