/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: markAsSeenOperation
// ====================================================

export interface markAsSeenOperation_markAsSeenOperation {
  __typename: "Operation";
  id: string;
  seen: boolean | null;
}

export interface markAsSeenOperation {
  markAsSeenOperation: markAsSeenOperation_markAsSeenOperation | null;
}

export interface markAsSeenOperationVariables {
  id: string;
  userId?: string | null;
  idPharmacie?: string | null;
}
