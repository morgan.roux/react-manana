/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_OPERATION
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_OPERATION_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_CUSTOM_CONTENT_OPERATION_search_data_Operation_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  libelle: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_OPERATION_search_data_Operation_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  libelle: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_OPERATION_search_data_Operation_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_OPERATION_search_data_Operation {
  __typename: "Operation";
  id: string;
  description: string | null;
  dateDebut: any | null;
  dateFin: any | null;
  libelle: string | null;
  commandeType: SEARCH_CUSTOM_CONTENT_OPERATION_search_data_Operation_commandeType | null;
  commandeCanal: SEARCH_CUSTOM_CONTENT_OPERATION_search_data_Operation_commandeCanal | null;
  laboratoire: SEARCH_CUSTOM_CONTENT_OPERATION_search_data_Operation_laboratoire | null;
  CAMoyenParPharmacie: number | null;
  CATotal: number | null;
}

export type SEARCH_CUSTOM_CONTENT_OPERATION_search_data = SEARCH_CUSTOM_CONTENT_OPERATION_search_data_Action | SEARCH_CUSTOM_CONTENT_OPERATION_search_data_Operation;

export interface SEARCH_CUSTOM_CONTENT_OPERATION_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_OPERATION_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_OPERATION {
  search: SEARCH_CUSTOM_CONTENT_OPERATION_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_OPERATIONVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
