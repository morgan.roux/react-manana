/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: OPERATION_PRESENTATION
// ====================================================

export interface OPERATION_PRESENTATION_operation_pharmacieVisitors {
  __typename: "Pharmacie";
  id: string;
}

export interface OPERATION_PRESENTATION_operation_laboratoire {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
}

export interface OPERATION_PRESENTATION_operation_pharmacieCible {
  __typename: "Pharmacie";
  id: string;
}

export interface OPERATION_PRESENTATION_operation_fichierPresentations {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_PRESENTATION_operation_actions {
  __typename: "ActionResult";
  total: number;
}

export interface OPERATION_PRESENTATION_operation_operationArticles {
  __typename: "OperationArticleResult";
  total: number;
}

export interface OPERATION_PRESENTATION_operation_operationPharmacie_fichierCible {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_PRESENTATION_operation_operationPharmacie_pharmaciedetails_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface OPERATION_PRESENTATION_operation_operationPharmacie_pharmaciedetails {
  __typename: "OperationPharmacieDetail";
  id: string;
  pharmacie: OPERATION_PRESENTATION_operation_operationPharmacie_pharmaciedetails_pharmacie | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_PRESENTATION_operation_operationPharmacie {
  __typename: "OperationPharmacie";
  id: string;
  globalite: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
  fichierCible: OPERATION_PRESENTATION_operation_operationPharmacie_fichierCible | null;
  pharmaciedetails: (OPERATION_PRESENTATION_operation_operationPharmacie_pharmaciedetails | null)[] | null;
}

export interface OPERATION_PRESENTATION_operation_commandeType {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_PRESENTATION_operation_commandeCanal {
  __typename: "StatutTypePayload";
  id: string;
  code: string | null;
  libelle: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_item {
  __typename: "Item";
  id: string;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: OPERATION_PRESENTATION_operation_userSmyleys_data_user_userPhoto_fichier | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: OPERATION_PRESENTATION_operation_userSmyleys_data_user_role | null;
  userPhoto: OPERATION_PRESENTATION_operation_userSmyleys_data_user_userPhoto | null;
  pharmacie: OPERATION_PRESENTATION_operation_userSmyleys_data_user_pharmacie | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: OPERATION_PRESENTATION_operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement_region | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_smyley_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: OPERATION_PRESENTATION_operation_userSmyleys_data_smyley_groupement_defaultPharmacie_departement | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_smyley_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_smyley_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: OPERATION_PRESENTATION_operation_userSmyleys_data_smyley_groupement_groupementLogo_fichier | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_smyley_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: OPERATION_PRESENTATION_operation_userSmyleys_data_smyley_groupement_defaultPharmacie | null;
  groupementLogo: OPERATION_PRESENTATION_operation_userSmyleys_data_smyley_groupement_groupementLogo | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data_smyley {
  __typename: "Smyley";
  id: string;
  nom: string;
  note: number | null;
  photo: string;
  groupement: OPERATION_PRESENTATION_operation_userSmyleys_data_smyley_groupement | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys_data {
  __typename: "UserSmyley";
  id: string;
  item: OPERATION_PRESENTATION_operation_userSmyleys_data_item | null;
  idSource: string | null;
  user: OPERATION_PRESENTATION_operation_userSmyleys_data_user | null;
  smyley: OPERATION_PRESENTATION_operation_userSmyleys_data_smyley | null;
  isRemoved: boolean | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface OPERATION_PRESENTATION_operation_userSmyleys {
  __typename: "UserSmyleyResult";
  total: number;
  data: (OPERATION_PRESENTATION_operation_userSmyleys_data | null)[] | null;
}

export interface OPERATION_PRESENTATION_operation_comments {
  __typename: "CommentResult";
  total: number;
}

export interface OPERATION_PRESENTATION_operation {
  __typename: "Operation";
  id: string;
  accordCommercial: boolean | null;
  commandePassee: boolean | null;
  isRemoved: boolean | null;
  seen: boolean | null;
  pharmacieVisitors: (OPERATION_PRESENTATION_operation_pharmacieVisitors | null)[] | null;
  laboratoire: OPERATION_PRESENTATION_operation_laboratoire | null;
  idGroupement: string | null;
  pharmacieCible: (OPERATION_PRESENTATION_operation_pharmacieCible | null)[] | null;
  fichierPresentations: (OPERATION_PRESENTATION_operation_fichierPresentations | null)[] | null;
  libelle: string | null;
  niveauPriorite: number | null;
  description: string | null;
  dateDebut: any | null;
  dateModification: any | null;
  dateFin: any | null;
  dateCreation: any | null;
  nombrePharmaciesConsultes: number | null;
  nombrePharmaciesCommandees: number | null;
  nombreProduitsCommandes: number | null;
  nombreTotalTypeProduitsCommandes: number | null;
  CAMoyenParPharmacie: number | null;
  CATotal: number | null;
  nombreProduitsMoyenParPharmacie: number | null;
  qteProduitsPharmacieCommandePassee: number | null;
  qteTotalProduitsCommandePassee: number | null;
  nombreCommandePassee: number | null;
  totalCommandePassee: number | null;
  actions: OPERATION_PRESENTATION_operation_actions | null;
  operationArticles: OPERATION_PRESENTATION_operation_operationArticles | null;
  operationPharmacie: OPERATION_PRESENTATION_operation_operationPharmacie | null;
  commandeType: OPERATION_PRESENTATION_operation_commandeType | null;
  commandeCanal: OPERATION_PRESENTATION_operation_commandeCanal | null;
  userSmyleys: OPERATION_PRESENTATION_operation_userSmyleys | null;
  comments: OPERATION_PRESENTATION_operation_comments | null;
}

export interface OPERATION_PRESENTATION {
  operation: OPERATION_PRESENTATION_operation | null;
}

export interface OPERATION_PRESENTATIONVariables {
  id: string;
  userId?: string | null;
  idPharmacieUser?: string | null;
}
