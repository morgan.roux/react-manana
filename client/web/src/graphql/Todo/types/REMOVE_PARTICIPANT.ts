/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RemoveParticipantInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: REMOVE_PARTICIPANT
// ====================================================

export interface REMOVE_PARTICIPANT_removeParticipantFromProject_participants_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface REMOVE_PARTICIPANT_removeParticipantFromProject_participants_pharmacie {
  __typename: "Pharmacie";
  id: string;
}

export interface REMOVE_PARTICIPANT_removeParticipantFromProject_participants {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: REMOVE_PARTICIPANT_removeParticipantFromProject_participants_role | null;
  pharmacie: REMOVE_PARTICIPANT_removeParticipantFromProject_participants_pharmacie | null;
}

export interface REMOVE_PARTICIPANT_removeParticipantFromProject_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface REMOVE_PARTICIPANT_removeParticipantFromProject_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  role: REMOVE_PARTICIPANT_removeParticipantFromProject_userCreation_role | null;
}

export interface REMOVE_PARTICIPANT_removeParticipantFromProject {
  __typename: "Project";
  id: string;
  participants: REMOVE_PARTICIPANT_removeParticipantFromProject_participants[] | null;
  userCreation: REMOVE_PARTICIPANT_removeParticipantFromProject_userCreation | null;
}

export interface REMOVE_PARTICIPANT {
  removeParticipantFromProject: REMOVE_PARTICIPANT_removeParticipantFromProject;
}

export interface REMOVE_PARTICIPANTVariables {
  input: RemoveParticipantInput;
}
