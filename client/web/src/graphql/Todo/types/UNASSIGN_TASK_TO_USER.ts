/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AssignOrUnAssignActionInput, ActionStatus, TypeProject } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UNASSIGN_TASK_TO_USER
// ====================================================

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_origine {
  __typename: "TodoActionOrigine";
  id: string;
  code: string;
  libelle: string;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_section {
  __typename: "TodoSection";
  id: string;
  ordre: number | null;
  libelle: string;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_actionParent {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  nbComment: number;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_subActions_actionType {
  __typename: "TodoActionType";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_subActions {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: UNASSIGN_TASK_TO_USER_unAssignUserInAction_subActions_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  nbComment: number;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_item {
  __typename: "Item";
  id: string;
  code: string | null;
  name: string | null;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_project {
  __typename: "Project";
  id: string;
  name: string | null;
  typeProject: TypeProject | null;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_userCreation_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: UNASSIGN_TASK_TO_USER_unAssignUserInAction_userCreation_role | null;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_assignedUsers_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_assignedUsers_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: UNASSIGN_TASK_TO_USER_unAssignUserInAction_assignedUsers_userPhoto_fichier | null;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_assignedUsers {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  userPhoto: UNASSIGN_TASK_TO_USER_unAssignUserInAction_assignedUsers_userPhoto | null;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_etiquettes_couleur {
  __typename: "Couleur";
  id: string;
  code: string;
  libelle: string;
  isRemoved: boolean | null;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction_etiquettes {
  __typename: "TodoEtiquette";
  id: string;
  ordre: number | null;
  nom: string;
  isRemoved: boolean | null;
  couleur: UNASSIGN_TASK_TO_USER_unAssignUserInAction_etiquettes_couleur | null;
}

export interface UNASSIGN_TASK_TO_USER_unAssignUserInAction {
  __typename: "Action";
  id: string;
  ordre: number | null;
  description: string;
  dateDebut: any | null;
  dateFin: any | null;
  _priority: string | null;
  status: ActionStatus | null;
  isInInbox: boolean | null;
  isInInboxTeam: boolean | null;
  isRemoved: boolean | null;
  actionType: UNASSIGN_TASK_TO_USER_unAssignUserInAction_actionType | null;
  dateCreation: any | null;
  dateModification: any | null;
  idItemAssocie: string | null;
  nbComment: number;
  origine: UNASSIGN_TASK_TO_USER_unAssignUserInAction_origine | null;
  section: UNASSIGN_TASK_TO_USER_unAssignUserInAction_section | null;
  actionParent: UNASSIGN_TASK_TO_USER_unAssignUserInAction_actionParent | null;
  subActions: UNASSIGN_TASK_TO_USER_unAssignUserInAction_subActions[] | null;
  item: UNASSIGN_TASK_TO_USER_unAssignUserInAction_item | null;
  project: UNASSIGN_TASK_TO_USER_unAssignUserInAction_project | null;
  userCreation: UNASSIGN_TASK_TO_USER_unAssignUserInAction_userCreation | null;
  assignedUsers: UNASSIGN_TASK_TO_USER_unAssignUserInAction_assignedUsers[] | null;
  etiquettes: UNASSIGN_TASK_TO_USER_unAssignUserInAction_etiquettes[] | null;
}

export interface UNASSIGN_TASK_TO_USER {
  unAssignUserInAction: UNASSIGN_TASK_TO_USER_unAssignUserInAction;
}

export interface UNASSIGN_TASK_TO_USERVariables {
  input: AssignOrUnAssignActionInput;
}
