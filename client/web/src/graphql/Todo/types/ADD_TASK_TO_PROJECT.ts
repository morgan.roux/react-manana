/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ActionInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: ADD_TASK_TO_PROJECT
// ====================================================

export interface ADD_TASK_TO_PROJECT_createUpdateAction {
  __typename: "Action";
  id: string;
}

export interface ADD_TASK_TO_PROJECT {
  createUpdateAction: ADD_TASK_TO_PROJECT_createUpdateAction;
}

export interface ADD_TASK_TO_PROJECTVariables {
  input: ActionInput;
}
