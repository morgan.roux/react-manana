/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { AddParticipantsInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: ADD_PARTICIPANT
// ====================================================

export interface ADD_PARTICIPANT_addParticipantsToProject_participants_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface ADD_PARTICIPANT_addParticipantsToProject_participants_pharmacie {
  __typename: "Pharmacie";
  id: string;
}

export interface ADD_PARTICIPANT_addParticipantsToProject_participants {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  userName: string | null;
  role: ADD_PARTICIPANT_addParticipantsToProject_participants_role | null;
  pharmacie: ADD_PARTICIPANT_addParticipantsToProject_participants_pharmacie | null;
}

export interface ADD_PARTICIPANT_addParticipantsToProject_userCreation_role {
  __typename: "Role";
  id: string;
  nom: string | null;
}

export interface ADD_PARTICIPANT_addParticipantsToProject_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  role: ADD_PARTICIPANT_addParticipantsToProject_userCreation_role | null;
}

export interface ADD_PARTICIPANT_addParticipantsToProject {
  __typename: "Project";
  id: string;
  participants: ADD_PARTICIPANT_addParticipantsToProject_participants[] | null;
  userCreation: ADD_PARTICIPANT_addParticipantsToProject_userCreation | null;
}

export interface ADD_PARTICIPANT {
  addParticipantsToProject: ADD_PARTICIPANT_addParticipantsToProject;
}

export interface ADD_PARTICIPANTVariables {
  input: AddParticipantsInput;
}
