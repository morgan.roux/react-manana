import gql from 'graphql-tag';

export const GET_MINIMAL_SEARCH_ID = gql`
  query MINIMAL_SEARCH_ID($type: [String], $query: JSON) {
    search(type: $type, query: $query) {
      total
      data {
        ... on ProduitCanal {
          id
        }
        ... on Titulaire {
          id
        }
        ... on Personnel {
          id
        }
        ... on Pharmacie {
          id
        }
        ... on GroupeClient {
          id
        }
        ... on TitulaireAffectation {
          id
        }
        ... on PersonnelAffectation {
          id
        }
      }
    }
  }
`;

export const GET_MINIMAL_SEARCH_LABEL = gql`
  query MINIMAL_SEARCH_LABEL($type: [String], $query: JSON) {
    search(type: $type, query: $query) {
      total
      data {
        ... on ProduitCanal {
          id
          produit {
            id
            libelle
          }
        }
        ... on Produit {
          id
          libelle
        }
        ... on Titulaire {
          id
        }
        ... on Personnel {
          id
        }
        ... on Pharmacie {
          id
          nom
        }
      }
    }
  }
`;
