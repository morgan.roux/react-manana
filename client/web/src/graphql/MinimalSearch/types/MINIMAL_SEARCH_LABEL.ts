/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MINIMAL_SEARCH_LABEL
// ====================================================

export interface MINIMAL_SEARCH_LABEL_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Comment" | "Item" | "Avatar" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface MINIMAL_SEARCH_LABEL_search_data_ProduitCanal_produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
}

export interface MINIMAL_SEARCH_LABEL_search_data_ProduitCanal {
  __typename: "ProduitCanal";
  id: string;
  produit: MINIMAL_SEARCH_LABEL_search_data_ProduitCanal_produit | null;
}

export interface MINIMAL_SEARCH_LABEL_search_data_Produit {
  __typename: "Produit";
  id: string;
  libelle: string | null;
}

export interface MINIMAL_SEARCH_LABEL_search_data_Titulaire {
  __typename: "Titulaire";
  id: string;
}

export interface MINIMAL_SEARCH_LABEL_search_data_Personnel {
  __typename: "Personnel";
  id: string;
}

export interface MINIMAL_SEARCH_LABEL_search_data_Pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export type MINIMAL_SEARCH_LABEL_search_data = MINIMAL_SEARCH_LABEL_search_data_Action | MINIMAL_SEARCH_LABEL_search_data_ProduitCanal | MINIMAL_SEARCH_LABEL_search_data_Produit | MINIMAL_SEARCH_LABEL_search_data_Titulaire | MINIMAL_SEARCH_LABEL_search_data_Personnel | MINIMAL_SEARCH_LABEL_search_data_Pharmacie;

export interface MINIMAL_SEARCH_LABEL_search {
  __typename: "SearchResult";
  total: number;
  data: (MINIMAL_SEARCH_LABEL_search_data | null)[] | null;
}

export interface MINIMAL_SEARCH_LABEL {
  search: MINIMAL_SEARCH_LABEL_search | null;
}

export interface MINIMAL_SEARCH_LABELVariables {
  type?: (string | null)[] | null;
  query?: any | null;
}
