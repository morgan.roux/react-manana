/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MINIMAL_SEARCH_CANAL_ARTICLE
// ====================================================

export interface MINIMAL_SEARCH_CANAL_ARTICLE_search_data_User {
  __typename: "User" | "Groupement" | "Pharmacie" | "Item" | "Avatar" | "Titulaire" | "Role" | "Personnel" | "Service" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "Project" | "Partenaire" | "Operation" | "TrFamille" | "TrTVA" | "TrTableau" | "TrMarge" | "TrForme" | "TrStockage" | "RbtArticle" | "TrActe" | "CanalGamme" | "GammeCatalogue" | "CommandeCanal" | "Marche" | "GroupeClient" | "Promotion" | "Ppersonnel" | "Commande" | "CommandeLigne" | "CommandeType" | "ActiviteUser" | "Publicite" | "SuiviPublicitaire" | "Traitement";
}

export interface MINIMAL_SEARCH_CANAL_ARTICLE_search_data_CanalArticle {
  __typename: "CanalArticle";
  id: string;
}

export type MINIMAL_SEARCH_CANAL_ARTICLE_search_data = MINIMAL_SEARCH_CANAL_ARTICLE_search_data_User | MINIMAL_SEARCH_CANAL_ARTICLE_search_data_CanalArticle;

export interface MINIMAL_SEARCH_CANAL_ARTICLE_search {
  __typename: "SearchResult";
  total: number;
  data: (MINIMAL_SEARCH_CANAL_ARTICLE_search_data | null)[] | null;
}

export interface MINIMAL_SEARCH_CANAL_ARTICLE {
  search: MINIMAL_SEARCH_CANAL_ARTICLE_search | null;
}

export interface MINIMAL_SEARCH_CANAL_ARTICLEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
}
