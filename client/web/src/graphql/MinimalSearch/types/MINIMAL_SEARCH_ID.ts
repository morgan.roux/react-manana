/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MINIMAL_SEARCH_ID
// ====================================================

export interface MINIMAL_SEARCH_ID_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Comment" | "Item" | "Avatar" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface MINIMAL_SEARCH_ID_search_data_ProduitCanal {
  __typename: "ProduitCanal";
  id: string;
}

export interface MINIMAL_SEARCH_ID_search_data_Titulaire {
  __typename: "Titulaire";
  id: string;
}

export interface MINIMAL_SEARCH_ID_search_data_Personnel {
  __typename: "Personnel";
  id: string;
}

export interface MINIMAL_SEARCH_ID_search_data_Pharmacie {
  __typename: "Pharmacie";
  id: string;
}

export interface MINIMAL_SEARCH_ID_search_data_GroupeClient {
  __typename: "GroupeClient";
  id: string;
}

export interface MINIMAL_SEARCH_ID_search_data_TitulaireAffectation {
  __typename: "TitulaireAffectation";
  id: string;
}

export interface MINIMAL_SEARCH_ID_search_data_PersonnelAffectation {
  __typename: "PersonnelAffectation";
  id: string;
}

export type MINIMAL_SEARCH_ID_search_data = MINIMAL_SEARCH_ID_search_data_Action | MINIMAL_SEARCH_ID_search_data_ProduitCanal | MINIMAL_SEARCH_ID_search_data_Titulaire | MINIMAL_SEARCH_ID_search_data_Personnel | MINIMAL_SEARCH_ID_search_data_Pharmacie | MINIMAL_SEARCH_ID_search_data_GroupeClient | MINIMAL_SEARCH_ID_search_data_TitulaireAffectation | MINIMAL_SEARCH_ID_search_data_PersonnelAffectation;

export interface MINIMAL_SEARCH_ID_search {
  __typename: "SearchResult";
  total: number;
  data: (MINIMAL_SEARCH_ID_search_data | null)[] | null;
}

export interface MINIMAL_SEARCH_ID {
  search: MINIMAL_SEARCH_ID_search | null;
}

export interface MINIMAL_SEARCH_IDVariables {
  type?: (string | null)[] | null;
  query?: any | null;
}
