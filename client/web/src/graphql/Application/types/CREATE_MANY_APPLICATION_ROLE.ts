/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ApplicationRoleInput, SsoType } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_MANY_APPLICATION_ROLE
// ====================================================

export interface CREATE_MANY_APPLICATION_ROLE_createManyApplicationRole_role {
  __typename: "Role";
  id: string;
  nom: string | null;
  code: string | null;
}

export interface CREATE_MANY_APPLICATION_ROLE_createManyApplicationRole_applications_icon {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_MANY_APPLICATION_ROLE_createManyApplicationRole_applications {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
  icon: CREATE_MANY_APPLICATION_ROLE_createManyApplicationRole_applications_icon | null;
  ssoType: SsoType;
  ssoTypeid: string | null;
}

export interface CREATE_MANY_APPLICATION_ROLE_createManyApplicationRole {
  __typename: "ApplicationsRole";
  id: string;
  role: CREATE_MANY_APPLICATION_ROLE_createManyApplicationRole_role | null;
  applications: CREATE_MANY_APPLICATION_ROLE_createManyApplicationRole_applications | null;
}

export interface CREATE_MANY_APPLICATION_ROLE {
  createManyApplicationRole: (CREATE_MANY_APPLICATION_ROLE_createManyApplicationRole | null)[] | null;
}

export interface CREATE_MANY_APPLICATION_ROLEVariables {
  applicationsRoleInput?: (ApplicationRoleInput | null)[] | null;
}
