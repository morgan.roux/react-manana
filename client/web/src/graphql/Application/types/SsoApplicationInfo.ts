/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SsoType, Sendingtype } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: SsoApplicationInfo
// ====================================================

export interface SsoApplicationInfo_icon {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SsoApplicationInfo_mappings {
  __typename: "MappingVariable";
  id: string;
  nom: string | null;
  value: string | null;
}

export interface SsoApplicationInfo_samlLoginDetail {
  __typename: "SamlLogin";
  id: string;
  idpX509Certificate: string | null;
  spX509Certificate: string | null;
  consumerUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface SsoApplicationInfo_cryptoAesDetail {
  __typename: "CryptoAes256";
  id: string;
  key: string | null;
  requestUrl: string | null;
  hexadecimal: boolean | null;
  Sendingtype: Sendingtype | null;
}

export interface SsoApplicationInfo_cryptoMd5Detail {
  __typename: "CryptoMd5";
  id: string;
  beginGet: number | null;
  endGet: number | null;
  hexadecimal: boolean | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  haveExternalUserMapping: boolean | null;
}

export interface SsoApplicationInfo_cryptoOtherDetail {
  __typename: "CryptoOther";
  id: string;
  function: string | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface SsoApplicationInfo_tokenAuthentificationDetail {
  __typename: "TokenAuthentification";
  id: string;
  reqTokenUrl: string | null;
  reqUserUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface SsoApplicationInfo_tokenFtpAuthentificationDetail_ftpid {
  __typename: "FtpConnect";
  id: string;
  host: string;
  port: number | null;
  user: string | null;
  password: string | null;
}

export interface SsoApplicationInfo_tokenFtpAuthentificationDetail {
  __typename: "TokenFtpAuthentification";
  id: string;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  ftpFilePath: string | null;
  ftpFileName: string | null;
  ftpid: SsoApplicationInfo_tokenFtpAuthentificationDetail_ftpid | null;
}

export interface SsoApplicationInfo {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
  icon: SsoApplicationInfo_icon | null;
  ssoType: SsoType;
  ssoTypeid: string | null;
  mappings: (SsoApplicationInfo_mappings | null)[] | null;
  samlLoginDetail: SsoApplicationInfo_samlLoginDetail | null;
  cryptoAesDetail: SsoApplicationInfo_cryptoAesDetail | null;
  cryptoMd5Detail: SsoApplicationInfo_cryptoMd5Detail | null;
  cryptoOtherDetail: SsoApplicationInfo_cryptoOtherDetail | null;
  tokenAuthentificationDetail: SsoApplicationInfo_tokenAuthentificationDetail | null;
  tokenFtpAuthentificationDetail: SsoApplicationInfo_tokenFtpAuthentificationDetail | null;
}
