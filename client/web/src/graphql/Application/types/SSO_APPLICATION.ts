/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SsoType, Sendingtype } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SSO_APPLICATION
// ====================================================

export interface SSO_APPLICATION_ssoApplication_icon {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface SSO_APPLICATION_ssoApplication_mappings {
  __typename: "MappingVariable";
  id: string;
  nom: string | null;
  value: string | null;
}

export interface SSO_APPLICATION_ssoApplication_samlLoginDetail {
  __typename: "SamlLogin";
  id: string;
  idpX509Certificate: string | null;
  spX509Certificate: string | null;
  consumerUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface SSO_APPLICATION_ssoApplication_cryptoAesDetail {
  __typename: "CryptoAes256";
  id: string;
  key: string | null;
  requestUrl: string | null;
  hexadecimal: boolean | null;
  Sendingtype: Sendingtype | null;
}

export interface SSO_APPLICATION_ssoApplication_cryptoMd5Detail {
  __typename: "CryptoMd5";
  id: string;
  beginGet: number | null;
  endGet: number | null;
  hexadecimal: boolean | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  haveExternalUserMapping: boolean | null;
}

export interface SSO_APPLICATION_ssoApplication_cryptoOtherDetail {
  __typename: "CryptoOther";
  id: string;
  function: string | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface SSO_APPLICATION_ssoApplication_tokenAuthentificationDetail {
  __typename: "TokenAuthentification";
  id: string;
  reqTokenUrl: string | null;
  reqUserUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface SSO_APPLICATION_ssoApplication_tokenFtpAuthentificationDetail_ftpid {
  __typename: "FtpConnect";
  id: string;
  host: string;
  port: number | null;
  user: string | null;
  password: string | null;
}

export interface SSO_APPLICATION_ssoApplication_tokenFtpAuthentificationDetail {
  __typename: "TokenFtpAuthentification";
  id: string;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  ftpFilePath: string | null;
  ftpFileName: string | null;
  ftpid: SSO_APPLICATION_ssoApplication_tokenFtpAuthentificationDetail_ftpid | null;
}

export interface SSO_APPLICATION_ssoApplication {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
  icon: SSO_APPLICATION_ssoApplication_icon | null;
  ssoType: SsoType;
  ssoTypeid: string | null;
  mappings: (SSO_APPLICATION_ssoApplication_mappings | null)[] | null;
  samlLoginDetail: SSO_APPLICATION_ssoApplication_samlLoginDetail | null;
  cryptoAesDetail: SSO_APPLICATION_ssoApplication_cryptoAesDetail | null;
  cryptoMd5Detail: SSO_APPLICATION_ssoApplication_cryptoMd5Detail | null;
  cryptoOtherDetail: SSO_APPLICATION_ssoApplication_cryptoOtherDetail | null;
  tokenAuthentificationDetail: SSO_APPLICATION_ssoApplication_tokenAuthentificationDetail | null;
  tokenFtpAuthentificationDetail: SSO_APPLICATION_ssoApplication_tokenFtpAuthentificationDetail | null;
}

export interface SSO_APPLICATION {
  ssoApplication: SSO_APPLICATION_ssoApplication | null;
}

export interface SSO_APPLICATIONVariables {
  id: string;
}
