/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput, SsoType, MappingVariableInput, Sendingtype, SamlInput, AesInput, Md5Input, OtherInput, TokenInput, TokenFtpInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_SSO_APPLICATION
// ====================================================

export interface CREATE_SSO_APPLICATION_createSsoApplication_icon {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_SSO_APPLICATION_createSsoApplication_mappings {
  __typename: "MappingVariable";
  id: string;
  nom: string | null;
  value: string | null;
}

export interface CREATE_SSO_APPLICATION_createSsoApplication_samlLoginDetail {
  __typename: "SamlLogin";
  id: string;
  idpX509Certificate: string | null;
  spX509Certificate: string | null;
  consumerUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface CREATE_SSO_APPLICATION_createSsoApplication_cryptoAesDetail {
  __typename: "CryptoAes256";
  id: string;
  key: string | null;
  requestUrl: string | null;
  hexadecimal: boolean | null;
  Sendingtype: Sendingtype | null;
}

export interface CREATE_SSO_APPLICATION_createSsoApplication_cryptoMd5Detail {
  __typename: "CryptoMd5";
  id: string;
  beginGet: number | null;
  endGet: number | null;
  hexadecimal: boolean | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  haveExternalUserMapping: boolean | null;
}

export interface CREATE_SSO_APPLICATION_createSsoApplication_cryptoOtherDetail {
  __typename: "CryptoOther";
  id: string;
  function: string | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface CREATE_SSO_APPLICATION_createSsoApplication_tokenAuthentificationDetail {
  __typename: "TokenAuthentification";
  id: string;
  reqTokenUrl: string | null;
  reqUserUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface CREATE_SSO_APPLICATION_createSsoApplication_tokenFtpAuthentificationDetail_ftpid {
  __typename: "FtpConnect";
  id: string;
  host: string;
  port: number | null;
  user: string | null;
  password: string | null;
}

export interface CREATE_SSO_APPLICATION_createSsoApplication_tokenFtpAuthentificationDetail {
  __typename: "TokenFtpAuthentification";
  id: string;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  ftpFilePath: string | null;
  ftpFileName: string | null;
  ftpid: CREATE_SSO_APPLICATION_createSsoApplication_tokenFtpAuthentificationDetail_ftpid | null;
}

export interface CREATE_SSO_APPLICATION_createSsoApplication {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
  icon: CREATE_SSO_APPLICATION_createSsoApplication_icon | null;
  ssoType: SsoType;
  ssoTypeid: string | null;
  mappings: (CREATE_SSO_APPLICATION_createSsoApplication_mappings | null)[] | null;
  samlLoginDetail: CREATE_SSO_APPLICATION_createSsoApplication_samlLoginDetail | null;
  cryptoAesDetail: CREATE_SSO_APPLICATION_createSsoApplication_cryptoAesDetail | null;
  cryptoMd5Detail: CREATE_SSO_APPLICATION_createSsoApplication_cryptoMd5Detail | null;
  cryptoOtherDetail: CREATE_SSO_APPLICATION_createSsoApplication_cryptoOtherDetail | null;
  tokenAuthentificationDetail: CREATE_SSO_APPLICATION_createSsoApplication_tokenAuthentificationDetail | null;
  tokenFtpAuthentificationDetail: CREATE_SSO_APPLICATION_createSsoApplication_tokenFtpAuthentificationDetail | null;
}

export interface CREATE_SSO_APPLICATION {
  createSsoApplication: CREATE_SSO_APPLICATION_createSsoApplication | null;
}

export interface CREATE_SSO_APPLICATIONVariables {
  nom?: string | null;
  url: string;
  icon?: FichierInput | null;
  ssoType: SsoType;
  mappings?: (MappingVariableInput | null)[] | null;
  idGroupement?: string | null;
  sendingType?: Sendingtype | null;
  saml?: SamlInput | null;
  aes?: AesInput | null;
  md5?: Md5Input | null;
  other?: OtherInput | null;
  token?: TokenInput | null;
  toknFtp?: TokenFtpInput | null;
}
