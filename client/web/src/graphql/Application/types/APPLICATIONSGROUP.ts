/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { SsoType, Sendingtype } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: APPLICATIONSGROUP
// ====================================================

export interface APPLICATIONSGROUP_applicationsGroup_groupement {
  __typename: "Groupement";
  id: string;
  nom: string | null;
}

export interface APPLICATIONSGROUP_applicationsGroup_applications_icon {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface APPLICATIONSGROUP_applicationsGroup_applications_mappings {
  __typename: "MappingVariable";
  id: string;
  nom: string | null;
  value: string | null;
}

export interface APPLICATIONSGROUP_applicationsGroup_applications_samlLoginDetail {
  __typename: "SamlLogin";
  id: string;
  idpX509Certificate: string | null;
  spX509Certificate: string | null;
  consumerUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface APPLICATIONSGROUP_applicationsGroup_applications_cryptoAesDetail {
  __typename: "CryptoAes256";
  id: string;
  key: string | null;
  requestUrl: string | null;
  hexadecimal: boolean | null;
  Sendingtype: Sendingtype | null;
}

export interface APPLICATIONSGROUP_applicationsGroup_applications_cryptoMd5Detail {
  __typename: "CryptoMd5";
  id: string;
  beginGet: number | null;
  endGet: number | null;
  hexadecimal: boolean | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  haveExternalUserMapping: boolean | null;
}

export interface APPLICATIONSGROUP_applicationsGroup_applications_cryptoOtherDetail {
  __typename: "CryptoOther";
  id: string;
  function: string | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface APPLICATIONSGROUP_applicationsGroup_applications_tokenAuthentificationDetail {
  __typename: "TokenAuthentification";
  id: string;
  reqTokenUrl: string | null;
  reqUserUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface APPLICATIONSGROUP_applicationsGroup_applications_tokenFtpAuthentificationDetail_ftpid {
  __typename: "FtpConnect";
  id: string;
  host: string;
  port: number | null;
  user: string | null;
  password: string | null;
}

export interface APPLICATIONSGROUP_applicationsGroup_applications_tokenFtpAuthentificationDetail {
  __typename: "TokenFtpAuthentification";
  id: string;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  ftpFilePath: string | null;
  ftpFileName: string | null;
  ftpid: APPLICATIONSGROUP_applicationsGroup_applications_tokenFtpAuthentificationDetail_ftpid | null;
}

export interface APPLICATIONSGROUP_applicationsGroup_applications {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
  icon: APPLICATIONSGROUP_applicationsGroup_applications_icon | null;
  ssoType: SsoType;
  ssoTypeid: string | null;
  mappings: (APPLICATIONSGROUP_applicationsGroup_applications_mappings | null)[] | null;
  samlLoginDetail: APPLICATIONSGROUP_applicationsGroup_applications_samlLoginDetail | null;
  cryptoAesDetail: APPLICATIONSGROUP_applicationsGroup_applications_cryptoAesDetail | null;
  cryptoMd5Detail: APPLICATIONSGROUP_applicationsGroup_applications_cryptoMd5Detail | null;
  cryptoOtherDetail: APPLICATIONSGROUP_applicationsGroup_applications_cryptoOtherDetail | null;
  tokenAuthentificationDetail: APPLICATIONSGROUP_applicationsGroup_applications_tokenAuthentificationDetail | null;
  tokenFtpAuthentificationDetail: APPLICATIONSGROUP_applicationsGroup_applications_tokenFtpAuthentificationDetail | null;
}

export interface APPLICATIONSGROUP_applicationsGroup {
  __typename: "ApplicationsGroup";
  id: string;
  groupement: APPLICATIONSGROUP_applicationsGroup_groupement;
  applications: APPLICATIONSGROUP_applicationsGroup_applications | null;
}

export interface APPLICATIONSGROUP {
  applicationsGroup: (APPLICATIONSGROUP_applicationsGroup | null)[] | null;
}

export interface APPLICATIONSGROUPVariables {
  idgroupement: string;
}
