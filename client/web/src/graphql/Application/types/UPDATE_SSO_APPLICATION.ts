/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { FichierInput, SsoType, UpdateMappingVariableInput, Sendingtype, SamlInput, AesInput, Md5Input, OtherInput, TokenInput, TokenFtpInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_SSO_APPLICATION
// ====================================================

export interface UPDATE_SSO_APPLICATION_updateSsoApplication_icon {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  type: string;
  urlPresigned: string | null;
  publicUrl: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface UPDATE_SSO_APPLICATION_updateSsoApplication_mappings {
  __typename: "MappingVariable";
  id: string;
  nom: string | null;
  value: string | null;
}

export interface UPDATE_SSO_APPLICATION_updateSsoApplication_samlLoginDetail {
  __typename: "SamlLogin";
  id: string;
  idpX509Certificate: string | null;
  spX509Certificate: string | null;
  consumerUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface UPDATE_SSO_APPLICATION_updateSsoApplication_cryptoAesDetail {
  __typename: "CryptoAes256";
  id: string;
  key: string | null;
  requestUrl: string | null;
  hexadecimal: boolean | null;
  Sendingtype: Sendingtype | null;
}

export interface UPDATE_SSO_APPLICATION_updateSsoApplication_cryptoMd5Detail {
  __typename: "CryptoMd5";
  id: string;
  beginGet: number | null;
  endGet: number | null;
  hexadecimal: boolean | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  haveExternalUserMapping: boolean | null;
}

export interface UPDATE_SSO_APPLICATION_updateSsoApplication_cryptoOtherDetail {
  __typename: "CryptoOther";
  id: string;
  function: string | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface UPDATE_SSO_APPLICATION_updateSsoApplication_tokenAuthentificationDetail {
  __typename: "TokenAuthentification";
  id: string;
  reqTokenUrl: string | null;
  reqUserUrl: string | null;
  Sendingtype: Sendingtype | null;
}

export interface UPDATE_SSO_APPLICATION_updateSsoApplication_tokenFtpAuthentificationDetail_ftpid {
  __typename: "FtpConnect";
  id: string;
  host: string;
  port: number | null;
  user: string | null;
  password: string | null;
}

export interface UPDATE_SSO_APPLICATION_updateSsoApplication_tokenFtpAuthentificationDetail {
  __typename: "TokenFtpAuthentification";
  id: string;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  ftpFilePath: string | null;
  ftpFileName: string | null;
  ftpid: UPDATE_SSO_APPLICATION_updateSsoApplication_tokenFtpAuthentificationDetail_ftpid | null;
}

export interface UPDATE_SSO_APPLICATION_updateSsoApplication {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
  icon: UPDATE_SSO_APPLICATION_updateSsoApplication_icon | null;
  ssoType: SsoType;
  ssoTypeid: string | null;
  mappings: (UPDATE_SSO_APPLICATION_updateSsoApplication_mappings | null)[] | null;
  samlLoginDetail: UPDATE_SSO_APPLICATION_updateSsoApplication_samlLoginDetail | null;
  cryptoAesDetail: UPDATE_SSO_APPLICATION_updateSsoApplication_cryptoAesDetail | null;
  cryptoMd5Detail: UPDATE_SSO_APPLICATION_updateSsoApplication_cryptoMd5Detail | null;
  cryptoOtherDetail: UPDATE_SSO_APPLICATION_updateSsoApplication_cryptoOtherDetail | null;
  tokenAuthentificationDetail: UPDATE_SSO_APPLICATION_updateSsoApplication_tokenAuthentificationDetail | null;
  tokenFtpAuthentificationDetail: UPDATE_SSO_APPLICATION_updateSsoApplication_tokenFtpAuthentificationDetail | null;
}

export interface UPDATE_SSO_APPLICATION {
  updateSsoApplication: UPDATE_SSO_APPLICATION_updateSsoApplication | null;
}

export interface UPDATE_SSO_APPLICATIONVariables {
  id: string;
  nom?: string | null;
  url: string;
  icon?: FichierInput | null;
  ssoType: SsoType;
  mappings?: (UpdateMappingVariableInput | null)[] | null;
  idGroupement?: string | null;
  sendingType?: Sendingtype | null;
  saml?: SamlInput | null;
  aes?: AesInput | null;
  md5?: Md5Input | null;
  other?: OtherInput | null;
  token?: TokenInput | null;
  toknFtp?: TokenFtpInput | null;
}
