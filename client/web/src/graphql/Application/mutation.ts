import gql from 'graphql-tag';
import { SSO_APPLICATION_INFO, APPLICATION_ROLE_INFO } from './fragment';

export const DO_CREATE_SSO_APPLICATION = gql`
  mutation CREATE_SSO_APPLICATION(
    $nom: String, 
    $url: String!
    $icon: FichierInput
    $ssoType: SsoType!
    $mappings: [MappingVariableInput]
    $idGroupement: String
    $sendingType: Sendingtype
    $saml: SamlInput
    $aes: AesInput
    $md5: Md5Input
    $other: OtherInput
    $token: TokenInput
    $toknFtp: TokenFtpInput
    ) 
    {
      createSsoApplication(
        nom: $nom, 
        url: $url, 
        icon: $icon,
        ssoType: $ssoType,
        mappings: $mappings,
        idGroupement: $idGroupement,
        sendingType: $sendingType,
        saml: $saml,
        aes: $aes,
        md5: $md5,
        other: $other,
        token: $token,
        toknFtp: $toknFtp,
      ) {
      ...SsoApplicationInfo
    }
  }
  ${SSO_APPLICATION_INFO}
`;


export const DO_UPDATE_SSO_APPLICATION = gql`
  mutation UPDATE_SSO_APPLICATION(
    $id: ID!
    $nom: String, 
    $url: String!
    $icon: FichierInput
    $ssoType: SsoType!
    $mappings: [UpdateMappingVariableInput]
    $idGroupement: String
    $sendingType: Sendingtype
    $saml: SamlInput
    $aes: AesInput
    $md5: Md5Input
    $other: OtherInput
    $token: TokenInput
    $toknFtp: TokenFtpInput
    ) 
    {
      updateSsoApplication(
        id: $id, 
        nom: $nom, 
        url: $url, 
        icon: $icon,
        ssoType: $ssoType,
        mappings: $mappings,
        idGroupement: $idGroupement,
        sendingType: $sendingType,
        saml: $saml,
        aes: $aes,
        md5: $md5,
        other: $other,
        token: $token,
        toknFtp: $toknFtp,
      ) {
      ...SsoApplicationInfo
    }
  }
  ${SSO_APPLICATION_INFO}
`;
export const DO_DELETE_SSO_APPLICATION = gql`
  mutation DELETE_SSO_APPLICATION($id: ID!) 
    {
      deleteSsoApplication( id: $id) {
      ...SsoApplicationInfo
    }
  }
  ${SSO_APPLICATION_INFO}
`;
export const DO_CREATE_APPLICATION_ROLE = gql`
  mutation CREATE_APPLICATION_ROLE( $applicationRoleInput: ApplicationRoleInput ){
    createApplicationRole( applicationRoleInput: $applicationRoleInput){
      ...ApplicationRoleInfo
    }
  }
  ${APPLICATION_ROLE_INFO}
`;

export const DO_CREATE_MANY_APPLICATION_ROLE = gql`
  mutation CREATE_MANY_APPLICATION_ROLE(
    $applicationsRoleInput: [ApplicationRoleInput]
  ) 
  {
    createManyApplicationRole(applicationsRoleInput: $applicationsRoleInput){
      ...ApplicationRoleInfo
    }
  }
  ${APPLICATION_ROLE_INFO}
`;

export const DO_DELETE_APPLICATION_ROLE = gql`
  mutation DELETE_APPLICATION_ROLE(
    $idrole: ID!
    $idGroupement: ID!
    $idApplications: ID!
  ) 
  {
    deleteApplicationRole(
      idrole: $idrole,
      idGroupement: $idGroupement,
      idApplications: $idApplications,
    ){
      ...ApplicationRoleInfo
    }
  }
  ${APPLICATION_ROLE_INFO}
`;
