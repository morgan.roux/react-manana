/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: CREATE_FTP
// ====================================================

export interface CREATE_FTP_createFtpConnect_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface CREATE_FTP_createFtpConnect_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: CREATE_FTP_createFtpConnect_groupement_defaultPharmacie_departement_region | null;
}

export interface CREATE_FTP_createFtpConnect_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: CREATE_FTP_createFtpConnect_groupement_defaultPharmacie_departement | null;
}

export interface CREATE_FTP_createFtpConnect_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface CREATE_FTP_createFtpConnect_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: CREATE_FTP_createFtpConnect_groupement_groupementLogo_fichier | null;
}

export interface CREATE_FTP_createFtpConnect_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: CREATE_FTP_createFtpConnect_groupement_defaultPharmacie | null;
  groupementLogo: CREATE_FTP_createFtpConnect_groupement_groupementLogo | null;
}

export interface CREATE_FTP_createFtpConnect {
  __typename: "FtpConnect";
  id: string;
  host: string;
  port: number | null;
  user: string | null;
  password: string | null;
  groupement: CREATE_FTP_createFtpConnect_groupement | null;
}

export interface CREATE_FTP {
  createFtpConnect: CREATE_FTP_createFtpConnect | null;
}

export interface CREATE_FTPVariables {
  idGroupement: string;
  host?: string | null;
  port?: number | null;
  user?: string | null;
  password?: string | null;
}
