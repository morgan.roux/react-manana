import gql from 'graphql-tag';

import { GROUPEMENT_INFO } from '../Groupement/fragement';

export const FTP_INFO = gql`
  fragment FtpInfo on FtpConnect {
    id
    host
    port
    user
    password
    groupement{
        ...GroupementInfo
    }
  }
  ${GROUPEMENT_INFO}
`;