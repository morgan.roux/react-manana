import gql from 'graphql-tag';

export const GET_LISTES = gql`
  query LISTES {
    listes {
      id
      codeListe
      libelle
    }
  }
`;
