/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: LISTES
// ====================================================

export interface LISTES_listes {
  __typename: "Liste";
  id: string;
  codeListe: number | null;
  libelle: string | null;
}

export interface LISTES {
  listes: (LISTES_listes | null)[] | null;
}
