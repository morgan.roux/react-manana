/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: QUIT_GROUPE_FRIENDS
// ====================================================

export interface QUIT_GROUPE_FRIENDS_leaveGroupeAmis_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
}

export interface QUIT_GROUPE_FRIENDS_leaveGroupeAmis_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface QUIT_GROUPE_FRIENDS_leaveGroupeAmis {
  __typename: "GroupeAmisDetail";
  id: string;
  type: string;
  groupeAmis: QUIT_GROUPE_FRIENDS_leaveGroupeAmis_groupeAmis;
  pharmacie: QUIT_GROUPE_FRIENDS_leaveGroupeAmis_pharmacie;
}

export interface QUIT_GROUPE_FRIENDS {
  leaveGroupeAmis: QUIT_GROUPE_FRIENDS_leaveGroupeAmis[] | null;
}

export interface QUIT_GROUPE_FRIENDSVariables {
  id: string;
  idsPharmacies: string[];
}
