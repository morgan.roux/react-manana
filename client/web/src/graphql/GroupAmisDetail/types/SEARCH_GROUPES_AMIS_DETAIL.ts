/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: SEARCH_GROUPES_AMIS_DETAIL
// ====================================================

export interface SEARCH_GROUPES_AMIS_DETAIL_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_GROUPES_AMIS_DETAIL_search_data_GroupeAmisDetail_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
}

export interface SEARCH_GROUPES_AMIS_DETAIL_search_data_GroupeAmisDetail_pharmacie_users {
  __typename: "User";
  id: string;
}

export interface SEARCH_GROUPES_AMIS_DETAIL_search_data_GroupeAmisDetail_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  users: (SEARCH_GROUPES_AMIS_DETAIL_search_data_GroupeAmisDetail_pharmacie_users | null)[] | null;
}

export interface SEARCH_GROUPES_AMIS_DETAIL_search_data_GroupeAmisDetail {
  __typename: "GroupeAmisDetail";
  id: string;
  groupeAmis: SEARCH_GROUPES_AMIS_DETAIL_search_data_GroupeAmisDetail_groupeAmis;
  pharmacie: SEARCH_GROUPES_AMIS_DETAIL_search_data_GroupeAmisDetail_pharmacie;
}

export type SEARCH_GROUPES_AMIS_DETAIL_search_data = SEARCH_GROUPES_AMIS_DETAIL_search_data_Action | SEARCH_GROUPES_AMIS_DETAIL_search_data_GroupeAmisDetail;

export interface SEARCH_GROUPES_AMIS_DETAIL_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_GROUPES_AMIS_DETAIL_search_data | null)[] | null;
}

export interface SEARCH_GROUPES_AMIS_DETAIL {
  search: SEARCH_GROUPES_AMIS_DETAIL_search | null;
}

export interface SEARCH_GROUPES_AMIS_DETAILVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  filterBy?: any | null;
  sortBy?: any | null;
  skip?: number | null;
  take?: number | null;
}
