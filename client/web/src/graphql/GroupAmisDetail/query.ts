import gql from 'graphql-tag';

export const GET_GROUPE_AMIS_DETAILS = gql`
  query GroupeAmisDetails {
    groupeAmisDetails {
      id
    }
  }
`;

export const DO_SEARCH_GROUPES_AMIS_DETAIL = gql`
  query SEARCH_GROUPES_AMIS_DETAIL(
    $type: [String]
    $query: JSON
    $filterBy: JSON
    $sortBy: JSON
    $skip: Int
    $take: Int
  ) {
    search(
      type: $type
      query: $query
      filterBy: $filterBy
      sortBy: $sortBy
      skip: $skip
      take: $take
    ) {
      total
      data {
        ... on GroupeAmisDetail {
          id
          groupeAmis {
            id
          }
          pharmacie {
            id
            nom
            users {
              id
            }
          }
        }
      }
    }
  }
`;

export default DO_SEARCH_GROUPES_AMIS_DETAIL;
