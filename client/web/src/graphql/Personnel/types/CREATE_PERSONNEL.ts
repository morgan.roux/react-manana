/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sexe, ContactInput, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_PERSONNEL
// ====================================================

export interface CREATE_PERSONNEL_createUpdatePersonnel_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface CREATE_PERSONNEL_createUpdatePersonnel_respHierarch {
  __typename: "Personnel";
  id: string;
}

export interface CREATE_PERSONNEL_createUpdatePersonnel_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CREATE_PERSONNEL_createUpdatePersonnel__user {
  __typename: "User";
  id: string;
  status: UserStatus | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface CREATE_PERSONNEL_createUpdatePersonnel_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
}

export interface CREATE_PERSONNEL_createUpdatePersonnel {
  __typename: "Personnel";
  type: string;
  id: string;
  service: CREATE_PERSONNEL_createUpdatePersonnel_service | null;
  respHierarch: CREATE_PERSONNEL_createUpdatePersonnel_respHierarch | null;
  sortie: number | null;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  sexe: Sexe | null;
  fullName: string | null;
  commentaire: string | null;
  dateSortie: any | null;
  idGroupement: string | null;
  role: CREATE_PERSONNEL_createUpdatePersonnel_role | null;
  _user: CREATE_PERSONNEL_createUpdatePersonnel__user | null;
  contact: CREATE_PERSONNEL_createUpdatePersonnel_contact | null;
}

export interface CREATE_PERSONNEL {
  createUpdatePersonnel: CREATE_PERSONNEL_createUpdatePersonnel | null;
}

export interface CREATE_PERSONNELVariables {
  id?: string | null;
  codeService: string;
  respHierarch?: string | null;
  sortie?: number | null;
  dateSortie?: any | null;
  civilite?: string | null;
  nom?: string | null;
  prenom?: string | null;
  sexe?: Sexe | null;
  contact?: ContactInput | null;
  commentaire?: string | null;
  idGroupement?: string | null;
}
