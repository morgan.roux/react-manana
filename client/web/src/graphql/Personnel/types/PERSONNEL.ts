/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sexe, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PERSONNEL
// ====================================================

export interface PERSONNEL_personnel_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
  nomOriginal: string;
  publicUrl: string | null;
}

export interface PERSONNEL_personnel_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: PERSONNEL_personnel_user_userPhoto_fichier | null;
}

export interface PERSONNEL_personnel_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
  codeTraitements: (string | null)[] | null;
  userPhoto: PERSONNEL_personnel_user_userPhoto | null;
}

export interface PERSONNEL_personnel_role {
  __typename: "Role";
  code: string | null;
  nom: string | null;
}

export interface PERSONNEL_personnel_service {
  __typename: "Service";
  id: string;
  code: string | null;
}

export interface PERSONNEL_personnel_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
}

export interface PERSONNEL_personnel {
  __typename: "Personnel";
  type: string;
  id: string;
  sortie: number | null;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  sexe: Sexe | null;
  commentaire: string | null;
  dateSortie: any | null;
  idGroupement: string | null;
  user: PERSONNEL_personnel_user | null;
  role: PERSONNEL_personnel_role | null;
  service: PERSONNEL_personnel_service | null;
  contact: PERSONNEL_personnel_contact | null;
}

export interface PERSONNEL {
  personnel: PERSONNEL_personnel | null;
}

export interface PERSONNELVariables {
  id: string;
}
