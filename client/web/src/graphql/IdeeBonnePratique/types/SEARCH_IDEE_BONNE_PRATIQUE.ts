/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { IdeeOuBonnePratiqueOrigine, IdeeOuBonnePratiqueStatus, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_IDEE_BONNE_PRATIQUE
// ====================================================

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_classification_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_classification_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_classification_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_classification_userCreation_userPhoto_fichier | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_classification_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_classification_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_classification_userCreation_role | null;
  userPhoto: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_classification_userCreation_userPhoto | null;
  pharmacie: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_classification_userCreation_pharmacie | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_classification {
  __typename: "IdeeOuBonnePratiqueClassification";
  id: string;
  nom: string;
  userCreation: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_classification_userCreation | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_auteur_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_auteur_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_auteur_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_auteur_userPhoto_fichier | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_auteur_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_auteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_auteur_role | null;
  userPhoto: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_auteur_userPhoto | null;
  pharmacie: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_auteur_pharmacie | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_fournisseur_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  nom: string | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_fournisseur {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  laboSuite: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_fournisseur_laboSuite | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_prestataire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
  commentaire: string | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupement_defaultPharmacie_departement_region | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupement_defaultPharmacie_departement | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupement_groupementLogo_fichier | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupement_defaultPharmacie | null;
  groupementLogo: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupement_groupementLogo | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueChangeStatus {
  __typename: "IdeeOuBonnePratiqueChangeStatus";
  id: string;
  commentaire: string | null;
  isRemoved: boolean | null;
  status: IdeeOuBonnePratiqueStatus;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueFichierJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueFichierJoints {
  __typename: "IdeeOuBonnePratiqueFichierJoint";
  id: string;
  fichier: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueFichierJoints_fichier;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueLus {
  __typename: "IdeeOuBonnePratiqueLu";
  id: string;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueSmyleys {
  __typename: "UserSmyley";
  id: string;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique {
  __typename: "IdeeOuBonnePratique";
  id: string;
  _origine: IdeeOuBonnePratiqueOrigine | null;
  status: IdeeOuBonnePratiqueStatus;
  nbLu: number;
  lu: boolean;
  nbSmyley: number;
  nbComment: number;
  nbPartage: number;
  classification: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_classification | null;
  title: string;
  auteur: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_auteur;
  concurent: string | null;
  fournisseur: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_fournisseur | null;
  prestataire: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_prestataire | null;
  groupeAmis: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupeAmis | null;
  service: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_service | null;
  beneficiaires_cles: string | null;
  contexte: string | null;
  objectifs: string | null;
  resultats: string | null;
  facteursClesDeSucces: string | null;
  contraintes: string | null;
  groupement: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_userCreation | null;
  dateCreation: any | null;
  dateModification: any | null;
  ideeOuBonnePratiqueChangeStatus: (SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueChangeStatus | null)[];
  ideeOuBonnePratiqueFichierJoints: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueFichierJoints[];
  ideeOuBonnePratiqueLus: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueLus[];
  ideeOuBonnePratiqueSmyleys: SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique_ideeOuBonnePratiqueSmyleys[];
}

export type SEARCH_IDEE_BONNE_PRATIQUE_search_data = SEARCH_IDEE_BONNE_PRATIQUE_search_data_Action | SEARCH_IDEE_BONNE_PRATIQUE_search_data_IdeeOuBonnePratique;

export interface SEARCH_IDEE_BONNE_PRATIQUE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_IDEE_BONNE_PRATIQUE_search_data | null)[] | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUE {
  search: SEARCH_IDEE_BONNE_PRATIQUE_search | null;
}

export interface SEARCH_IDEE_BONNE_PRATIQUEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
