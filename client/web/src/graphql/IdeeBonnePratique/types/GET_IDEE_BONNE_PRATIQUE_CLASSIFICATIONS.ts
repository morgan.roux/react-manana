/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS
// ====================================================

export interface GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS_ideeOuBonnePratiqueClassifications_parent {
  __typename: "IdeeOuBonnePratiqueClassification";
  id: string;
  nom: string;
}

export interface GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS_ideeOuBonnePratiqueClassifications_childs {
  __typename: "IdeeOuBonnePratiqueClassification";
  id: string;
  nom: string;
}

export interface GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS_ideeOuBonnePratiqueClassifications {
  __typename: "IdeeOuBonnePratiqueClassification";
  id: string;
  nom: string;
  parent: GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS_ideeOuBonnePratiqueClassifications_parent | null;
  childs: GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS_ideeOuBonnePratiqueClassifications_childs[] | null;
}

export interface GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS {
  ideeOuBonnePratiqueClassifications: GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONS_ideeOuBonnePratiqueClassifications[];
}

export interface GET_IDEE_BONNE_PRATIQUE_CLASSIFICATIONSVariables {
  isRemoved?: boolean | null;
}
