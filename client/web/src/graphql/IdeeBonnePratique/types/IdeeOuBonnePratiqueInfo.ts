/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { IdeeOuBonnePratiqueOrigine, IdeeOuBonnePratiqueStatus, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: IdeeOuBonnePratiqueInfo
// ====================================================

export interface IdeeOuBonnePratiqueInfo_classification_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface IdeeOuBonnePratiqueInfo_classification_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface IdeeOuBonnePratiqueInfo_classification_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: IdeeOuBonnePratiqueInfo_classification_userCreation_userPhoto_fichier | null;
}

export interface IdeeOuBonnePratiqueInfo_classification_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface IdeeOuBonnePratiqueInfo_classification_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: IdeeOuBonnePratiqueInfo_classification_userCreation_role | null;
  userPhoto: IdeeOuBonnePratiqueInfo_classification_userCreation_userPhoto | null;
  pharmacie: IdeeOuBonnePratiqueInfo_classification_userCreation_pharmacie | null;
}

export interface IdeeOuBonnePratiqueInfo_classification {
  __typename: "IdeeOuBonnePratiqueClassification";
  id: string;
  nom: string;
  userCreation: IdeeOuBonnePratiqueInfo_classification_userCreation | null;
}

export interface IdeeOuBonnePratiqueInfo_auteur_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface IdeeOuBonnePratiqueInfo_auteur_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface IdeeOuBonnePratiqueInfo_auteur_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: IdeeOuBonnePratiqueInfo_auteur_userPhoto_fichier | null;
}

export interface IdeeOuBonnePratiqueInfo_auteur_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface IdeeOuBonnePratiqueInfo_auteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: IdeeOuBonnePratiqueInfo_auteur_role | null;
  userPhoto: IdeeOuBonnePratiqueInfo_auteur_userPhoto | null;
  pharmacie: IdeeOuBonnePratiqueInfo_auteur_pharmacie | null;
}

export interface IdeeOuBonnePratiqueInfo_fournisseur_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  nom: string | null;
}

export interface IdeeOuBonnePratiqueInfo_fournisseur {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  laboSuite: IdeeOuBonnePratiqueInfo_fournisseur_laboSuite | null;
}

export interface IdeeOuBonnePratiqueInfo_prestataire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
  commentaire: string | null;
}

export interface IdeeOuBonnePratiqueInfo_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
}

export interface IdeeOuBonnePratiqueInfo_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface IdeeOuBonnePratiqueInfo_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface IdeeOuBonnePratiqueInfo_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: IdeeOuBonnePratiqueInfo_groupement_defaultPharmacie_departement_region | null;
}

export interface IdeeOuBonnePratiqueInfo_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: IdeeOuBonnePratiqueInfo_groupement_defaultPharmacie_departement | null;
}

export interface IdeeOuBonnePratiqueInfo_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface IdeeOuBonnePratiqueInfo_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: IdeeOuBonnePratiqueInfo_groupement_groupementLogo_fichier | null;
}

export interface IdeeOuBonnePratiqueInfo_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: IdeeOuBonnePratiqueInfo_groupement_defaultPharmacie | null;
  groupementLogo: IdeeOuBonnePratiqueInfo_groupement_groupementLogo | null;
}

export interface IdeeOuBonnePratiqueInfo_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface IdeeOuBonnePratiqueInfo_ideeOuBonnePratiqueChangeStatus {
  __typename: "IdeeOuBonnePratiqueChangeStatus";
  id: string;
  commentaire: string | null;
  isRemoved: boolean | null;
  status: IdeeOuBonnePratiqueStatus;
}

export interface IdeeOuBonnePratiqueInfo_ideeOuBonnePratiqueFichierJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface IdeeOuBonnePratiqueInfo_ideeOuBonnePratiqueFichierJoints {
  __typename: "IdeeOuBonnePratiqueFichierJoint";
  id: string;
  fichier: IdeeOuBonnePratiqueInfo_ideeOuBonnePratiqueFichierJoints_fichier;
}

export interface IdeeOuBonnePratiqueInfo_ideeOuBonnePratiqueLus {
  __typename: "IdeeOuBonnePratiqueLu";
  id: string;
}

export interface IdeeOuBonnePratiqueInfo_ideeOuBonnePratiqueSmyleys {
  __typename: "UserSmyley";
  id: string;
}

export interface IdeeOuBonnePratiqueInfo {
  __typename: "IdeeOuBonnePratique";
  id: string;
  _origine: IdeeOuBonnePratiqueOrigine | null;
  status: IdeeOuBonnePratiqueStatus;
  nbLu: number;
  lu: boolean;
  nbSmyley: number;
  nbComment: number;
  nbPartage: number;
  classification: IdeeOuBonnePratiqueInfo_classification | null;
  title: string;
  auteur: IdeeOuBonnePratiqueInfo_auteur;
  concurent: string | null;
  fournisseur: IdeeOuBonnePratiqueInfo_fournisseur | null;
  prestataire: IdeeOuBonnePratiqueInfo_prestataire | null;
  groupeAmis: IdeeOuBonnePratiqueInfo_groupeAmis | null;
  service: IdeeOuBonnePratiqueInfo_service | null;
  beneficiaires_cles: string | null;
  contexte: string | null;
  objectifs: string | null;
  resultats: string | null;
  facteursClesDeSucces: string | null;
  contraintes: string | null;
  groupement: IdeeOuBonnePratiqueInfo_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: IdeeOuBonnePratiqueInfo_userCreation | null;
  dateCreation: any | null;
  dateModification: any | null;
  ideeOuBonnePratiqueChangeStatus: (IdeeOuBonnePratiqueInfo_ideeOuBonnePratiqueChangeStatus | null)[];
  ideeOuBonnePratiqueFichierJoints: IdeeOuBonnePratiqueInfo_ideeOuBonnePratiqueFichierJoints[];
  ideeOuBonnePratiqueLus: IdeeOuBonnePratiqueInfo_ideeOuBonnePratiqueLus[];
  ideeOuBonnePratiqueSmyleys: IdeeOuBonnePratiqueInfo_ideeOuBonnePratiqueSmyleys[];
}
