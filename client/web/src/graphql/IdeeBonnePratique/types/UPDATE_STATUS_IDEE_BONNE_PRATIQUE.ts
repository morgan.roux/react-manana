/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { IdeeOuBonnePratiqueStatus, IdeeOuBonnePratiqueOrigine, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_STATUS_IDEE_BONNE_PRATIQUE
// ====================================================

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_classification_userCreation_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_classification_userCreation_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_classification_userCreation_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_classification_userCreation_userPhoto_fichier | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_classification_userCreation_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_classification_userCreation {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_classification_userCreation_role | null;
  userPhoto: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_classification_userCreation_userPhoto | null;
  pharmacie: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_classification_userCreation_pharmacie | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_classification {
  __typename: "IdeeOuBonnePratiqueClassification";
  id: string;
  nom: string;
  userCreation: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_classification_userCreation | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_auteur_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_auteur_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_auteur_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_auteur_userPhoto_fichier | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_auteur_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_auteur {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_auteur_role | null;
  userPhoto: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_auteur_userPhoto | null;
  pharmacie: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_auteur_pharmacie | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_fournisseur_laboSuite {
  __typename: "LaboratoireSuite";
  id: string;
  nom: string | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_fournisseur {
  __typename: "Laboratoire";
  id: string;
  nomLabo: string | null;
  sortie: number | null;
  laboSuite: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_fournisseur_laboSuite | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_prestataire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
  commentaire: string | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupeAmis {
  __typename: "GroupeAmis";
  id: string;
  nom: string | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_service {
  __typename: "Service";
  id: string;
  nom: string | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupement_defaultPharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupement_defaultPharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupement_defaultPharmacie_departement_region | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupement_defaultPharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
  ville: string | null;
  cip: string | null;
  adresse1: string | null;
  type: string;
  departement: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupement_defaultPharmacie_departement | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupement_groupementLogo_fichier {
  __typename: "Fichier";
  id: string;
  chemin: string;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupement_groupementLogo {
  __typename: "GroupementLogo";
  id: string;
  fichier: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupement_groupementLogo_fichier | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupement {
  __typename: "Groupement";
  id: string;
  isReference: boolean | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  telBureau: string | null;
  telMobile: string | null;
  mail: string | null;
  site: string | null;
  commentaire: string | null;
  nomResponsable: string | null;
  prenomResponsable: string | null;
  dateSortie: any | null;
  sortie: number | null;
  defaultPharmacie: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupement_defaultPharmacie | null;
  groupementLogo: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupement_groupementLogo | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_userCreation {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_ideeOuBonnePratiqueChangeStatus {
  __typename: "IdeeOuBonnePratiqueChangeStatus";
  id: string;
  commentaire: string | null;
  isRemoved: boolean | null;
  status: IdeeOuBonnePratiqueStatus;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_ideeOuBonnePratiqueFichierJoints_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
  chemin: string;
  nomOriginal: string;
  type: string;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_ideeOuBonnePratiqueFichierJoints {
  __typename: "IdeeOuBonnePratiqueFichierJoint";
  id: string;
  fichier: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_ideeOuBonnePratiqueFichierJoints_fichier;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_ideeOuBonnePratiqueLus {
  __typename: "IdeeOuBonnePratiqueLu";
  id: string;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_ideeOuBonnePratiqueSmyleys {
  __typename: "UserSmyley";
  id: string;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus {
  __typename: "IdeeOuBonnePratique";
  id: string;
  _origine: IdeeOuBonnePratiqueOrigine | null;
  status: IdeeOuBonnePratiqueStatus;
  nbLu: number;
  lu: boolean;
  nbSmyley: number;
  nbComment: number;
  nbPartage: number;
  classification: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_classification | null;
  title: string;
  auteur: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_auteur;
  concurent: string | null;
  fournisseur: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_fournisseur | null;
  prestataire: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_prestataire | null;
  groupeAmis: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupeAmis | null;
  service: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_service | null;
  beneficiaires_cles: string | null;
  contexte: string | null;
  objectifs: string | null;
  resultats: string | null;
  facteursClesDeSucces: string | null;
  contraintes: string | null;
  groupement: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_groupement | null;
  codeMaj: string | null;
  isRemoved: boolean | null;
  userCreation: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_userCreation | null;
  dateCreation: any | null;
  dateModification: any | null;
  ideeOuBonnePratiqueChangeStatus: (UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_ideeOuBonnePratiqueChangeStatus | null)[];
  ideeOuBonnePratiqueFichierJoints: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_ideeOuBonnePratiqueFichierJoints[];
  ideeOuBonnePratiqueLus: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_ideeOuBonnePratiqueLus[];
  ideeOuBonnePratiqueSmyleys: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus_ideeOuBonnePratiqueSmyleys[];
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUE {
  updateIdeeOuBonnePratiqueStatus: UPDATE_STATUS_IDEE_BONNE_PRATIQUE_updateIdeeOuBonnePratiqueStatus | null;
}

export interface UPDATE_STATUS_IDEE_BONNE_PRATIQUEVariables {
  id: string;
  status: IdeeOuBonnePratiqueStatus;
  commentaire?: string | null;
}
