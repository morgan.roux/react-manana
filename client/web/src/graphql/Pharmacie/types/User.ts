/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: User
// ====================================================

export interface User_pharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface User_pharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: User_pharmacie_departement_region | null;
}

export interface User_pharmacie {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
  adresse1: string | null;
  type: string;
  ville: string | null;
  departement: User_pharmacie_departement | null;
}

export interface User {
  __typename: "User";
  id: string;
  pharmacie: User_pharmacie | null;
}
