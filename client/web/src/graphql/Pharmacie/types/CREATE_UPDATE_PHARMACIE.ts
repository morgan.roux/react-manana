/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ContactInput, PharmacieEntreeSortieInput, PharmacieSatisfactionInput, PharmacieSegmentationInput, PharmacieComptaInput, PharmacieInformatiqueInput, PharmacieAchatInput, PharmacieCapInput, PharmacieConceptInput, PharmacieStatCAInput, PharmacieDigitaleInput, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_PHARMACIE
// ====================================================

export interface CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  sortie: number | null;
}

export interface CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_users {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_segmentation_trancheCA {
  __typename: "TrancheCA";
  id: string;
  libelle: string | null;
}

export interface CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_segmentation_contrat {
  __typename: "Contrat";
  id: string;
  nom: string | null;
}

export interface CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_segmentation {
  __typename: "PharmacieSegmentation";
  id: string;
  trancheCA: CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_segmentation_trancheCA | null;
  contrat: CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_segmentation_contrat | null;
}

export interface CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_achat_canal {
  __typename: "CommandeCanal";
  id: string;
  libelle: string | null;
}

export interface CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_achat {
  __typename: "PharmacieAchat";
  id: string;
  canal: CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_achat_canal | null;
}

export interface CREATE_UPDATE_PHARMACIE_createUpdatePharmacie {
  __typename: "Pharmacie";
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_departement | null;
  titulaires: (CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
  users: (CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_users | null)[] | null;
  segmentation: CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_segmentation | null;
  achat: CREATE_UPDATE_PHARMACIE_createUpdatePharmacie_achat | null;
  nbReclamation: number | null;
  nbAppel: number | null;
}

export interface CREATE_UPDATE_PHARMACIE {
  createUpdatePharmacie: CREATE_UPDATE_PHARMACIE_createUpdatePharmacie | null;
}

export interface CREATE_UPDATE_PHARMACIEVariables {
  id?: string | null;
  cip: string;
  nom: string;
  adresse1?: string | null;
  adresse2?: string | null;
  cp?: string | null;
  ville?: string | null;
  pays?: string | null;
  longitude?: string | null;
  latitude?: string | null;
  numFiness?: string | null;
  sortie?: number | null;
  sortieFuture?: number | null;
  nbEmploye?: number | null;
  nbAssistant?: number | null;
  nbAutreTravailleur?: number | null;
  uga?: string | null;
  commentaire?: string | null;
  idGrossistes?: (string | null)[] | null;
  idGeneriqueurs?: (string | null)[] | null;
  contact?: ContactInput | null;
  entreeSortie?: PharmacieEntreeSortieInput | null;
  satisfaction?: PharmacieSatisfactionInput | null;
  segmentation?: PharmacieSegmentationInput | null;
  compta?: PharmacieComptaInput | null;
  informatique?: PharmacieInformatiqueInput | null;
  achat?: PharmacieAchatInput | null;
  cap?: PharmacieCapInput | null;
  concept?: PharmacieConceptInput | null;
  chiffreAffaire?: PharmacieStatCAInput | null;
  digitales?: (PharmacieDigitaleInput | null)[] | null;
}
