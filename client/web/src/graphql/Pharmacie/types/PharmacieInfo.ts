/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePrestataire, TypeService } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: PharmacieInfo
// ====================================================

export interface PharmacieInfo_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface PharmacieInfo_departement {
  __typename: "Departement";
  id: string;
  code: string | null;
  nom: string | null;
  region: PharmacieInfo_departement_region | null;
}

export interface PharmacieInfo_titulaires {
  __typename: "Titulaire";
  id: string;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
}

export interface PharmacieInfo_presidentRegion_titulaire {
  __typename: "Titulaire";
  id: string;
  fullName: string | null;
  nom: string | null;
  prenom: string | null;
}

export interface PharmacieInfo_presidentRegion_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface PharmacieInfo_presidentRegion_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: PharmacieInfo_presidentRegion_departement_region | null;
}

export interface PharmacieInfo_presidentRegion {
  __typename: "TitulaireAffectation";
  id: string;
  titulaire: PharmacieInfo_presidentRegion_titulaire | null;
  departement: PharmacieInfo_presidentRegion_departement | null;
}

export interface PharmacieInfo_contact {
  __typename: "Contact";
  id: string;
  cp: string | null;
  ville: string | null;
  pays: string | null;
  adresse1: string | null;
  adresse2: string | null;
  faxProf: string | null;
  faxPerso: string | null;
  telProf: string | null;
  telMobProf: string | null;
  telPerso: string | null;
  telMobPerso: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  siteProf: string | null;
  sitePerso: string | null;
  whatsAppMobProf: string | null;
  whatsappMobPerso: string | null;
  compteSkypeProf: string | null;
  compteSkypePerso: string | null;
  urlLinkedinProf: string | null;
  urlLinkedinPerso: string | null;
  urlTwitterProf: string | null;
  urlTwitterPerso: string | null;
  urlFacebookProf: string | null;
  urlFacebookPerso: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInfo_entreeSortie_pharmacieMotifEntree {
  __typename: "Motif";
  id: string;
  libelle: string | null;
}

export interface PharmacieInfo_entreeSortie_pharmacieMotifSortie {
  __typename: "Motif";
  id: string;
  libelle: string | null;
}

export interface PharmacieInfo_entreeSortie_pharmacieMotifSortieFuture {
  __typename: "Motif";
  id: string;
  libelle: string | null;
}

export interface PharmacieInfo_entreeSortie_contrat {
  __typename: "Contrat";
  id: string;
  nom: string | null;
  dateCreation: any | null;
}

export interface PharmacieInfo_entreeSortie_concurrent {
  __typename: "Concurrent";
  id: string;
  nom: string | null;
}

export interface PharmacieInfo_entreeSortie_concurrentAncien {
  __typename: "Concurrent";
  id: string;
  nom: string | null;
}

export interface PharmacieInfo_entreeSortie {
  __typename: "PharmacieEntreeSortie";
  id: string;
  dateEntree: any | null;
  dateSortie: any | null;
  commentaireEntree: string | null;
  commentaireSortie: string | null;
  dateSortieFuture: any | null;
  commentaireSortieFuture: string | null;
  pharmacieMotifEntree: PharmacieInfo_entreeSortie_pharmacieMotifEntree | null;
  pharmacieMotifSortie: PharmacieInfo_entreeSortie_pharmacieMotifSortie | null;
  pharmacieMotifSortieFuture: PharmacieInfo_entreeSortie_pharmacieMotifSortieFuture | null;
  contrat: PharmacieInfo_entreeSortie_contrat | null;
  concurrent: PharmacieInfo_entreeSortie_concurrent | null;
  concurrentAncien: PharmacieInfo_entreeSortie_concurrentAncien | null;
}

export interface PharmacieInfo_grossistes {
  __typename: "Grossiste";
  id: string;
  nom: string | null;
}

export interface PharmacieInfo_generiqueurs {
  __typename: "Generiqueur";
  id: string;
  nom: string | null;
}

export interface PharmacieInfo_satisfaction_smyley {
  __typename: "Smyley";
  id: string;
  photo: string;
  note: number | null;
  nom: string;
}

export interface PharmacieInfo_satisfaction_user {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface PharmacieInfo_satisfaction {
  __typename: "PharmacieSatisfaction";
  id: string;
  dateSaisie: any | null;
  commentaire: string | null;
  smyley: PharmacieInfo_satisfaction_smyley | null;
  user: PharmacieInfo_satisfaction_user | null;
}

export interface PharmacieInfo_segmentation_typologie {
  __typename: "Typologie";
  id: string;
  code: string | null;
  libelle: string | null;
}

export interface PharmacieInfo_segmentation_trancheCA {
  __typename: "TrancheCA";
  id: string;
  libelle: string | null;
  valMax: number | null;
  valMin: number | null;
}

export interface PharmacieInfo_segmentation_qualite {
  __typename: "Qualite";
  id: string;
  libelle: string | null;
}

export interface PharmacieInfo_segmentation_contrat {
  __typename: "Contrat";
  id: string;
  nom: string | null;
  numeroVersion: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInfo_segmentation {
  __typename: "PharmacieSegmentation";
  id: string;
  dateSignature: any | null;
  typologie: PharmacieInfo_segmentation_typologie | null;
  trancheCA: PharmacieInfo_segmentation_trancheCA | null;
  qualite: PharmacieInfo_segmentation_qualite | null;
  contrat: PharmacieInfo_segmentation_contrat | null;
}

export interface PharmacieInfo_compta {
  __typename: "PharmacieCompta";
  id: string;
  siret: string | null;
  codeERP: string | null;
  ape: string | null;
  tvaIntra: string | null;
  rcs: string | null;
  contactCompta: string | null;
  chequeGrp: boolean | null;
  valeurChequeGrp: number | null;
  commentaireChequeGrp: string | null;
  droitAccesGrp: boolean | null;
  valeurChequeAccesGrp: number | null;
  commentaireChequeAccesGrp: string | null;
  structureJuridique: string | null;
  denominationSociale: string | null;
  telCompta: string | null;
  modifStatut: boolean | null;
  raisonModif: string | null;
  dateModifStatut: any | null;
  nomBanque: string | null;
  banqueGuichet: string | null;
  banqueCompte: string | null;
  banqueRib: string | null;
  banqueCle: string | null;
  dateMajRib: any | null;
  iban: string | null;
  swift: string | null;
  dateMajIban: any | null;
}

export interface PharmacieInfo_informatique_logiciel_ssii {
  __typename: "SSII";
  id: string;
  nom: string;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  mail: string | null;
  web: string | null;
  commentaire: string | null;
}

export interface PharmacieInfo_informatique_logiciel {
  __typename: "Logiciel";
  id: string;
  nom: string | null;
  os: string | null;
  osVersion: string | null;
  commentaire: string | null;
  ssii: PharmacieInfo_informatique_logiciel_ssii | null;
}

export interface PharmacieInfo_informatique_automate1_prestataire {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInfo_informatique_automate1 {
  __typename: "Automate";
  id: string;
  modeleAutomate: string | null;
  dateCommercialisation: any | null;
  prestataire: PharmacieInfo_informatique_automate1_prestataire | null;
}

export interface PharmacieInfo_informatique_automate2_prestataire {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInfo_informatique_automate2 {
  __typename: "Automate";
  id: string;
  modeleAutomate: string | null;
  dateCommercialisation: any | null;
  prestataire: PharmacieInfo_informatique_automate2_prestataire | null;
}

export interface PharmacieInfo_informatique_automate3_prestataire {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInfo_informatique_automate3 {
  __typename: "Automate";
  id: string;
  modeleAutomate: string | null;
  dateCommercialisation: any | null;
  prestataire: PharmacieInfo_informatique_automate3_prestataire | null;
}

export interface PharmacieInfo_informatique {
  __typename: "PharmacieInformatique";
  id: string;
  numVersion: string | null;
  dateLogiciel: string | null;
  nbrePoste: number | null;
  nbreComptoir: number | null;
  nbreBackOffice: number | null;
  nbreBureau: number | null;
  commentaire: string | null;
  logiciel: PharmacieInfo_informatique_logiciel | null;
  automate1: PharmacieInfo_informatique_automate1 | null;
  dateInstallation1: any | null;
  automate2: PharmacieInfo_informatique_automate2 | null;
  dateInstallation2: any | null;
  automate3: PharmacieInfo_informatique_automate3 | null;
  dateInstallation3: any | null;
}

export interface PharmacieInfo_achat_canal {
  __typename: "CommandeCanal";
  type: string;
  id: string;
  libelle: string | null;
  code: string | null;
  countOperationsCommercials: number | null;
  countCanalArticles: number | null;
}

export interface PharmacieInfo_achat {
  __typename: "PharmacieAchat";
  id: string;
  dateDebut: any | null;
  dateFin: any | null;
  identifiantAchatCanal: string | null;
  commentaire: string | null;
  canal: PharmacieInfo_achat_canal | null;
}

export interface PharmacieInfo_cap {
  __typename: "PharmacieCap";
  id: string;
  cap: boolean | null;
  dateDebut: any | null;
  dateFin: any | null;
  identifiantCap: string | null;
  commentaire: string | null;
}

export interface PharmacieInfo_concept_concept {
  __typename: "Concept";
  id: string;
  typeConcept: string | null;
  modele: string | null;
}

export interface PharmacieInfo_concept_fournisseurMobilier {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInfo_concept_enseigniste {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInfo_concept_facade {
  __typename: "Facade";
  id: string;
  modele: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInfo_concept_leaflet {
  __typename: "Leaflet";
  id: string;
  modele: string | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInfo_concept {
  __typename: "PharmacieConcept";
  id: string;
  dateInstallConcept: string | null;
  avecConcept: boolean | null;
  avecFacade: boolean | null;
  dateInstallFacade: string | null;
  conformiteFacade: boolean | null;
  surfaceTotale: number | null;
  surfaceVente: number | null;
  SurfaceVitrine: number | null;
  volumeLeaflet: number | null;
  nbreLineaire: number | null;
  nbreVitrine: number | null;
  otcLibAcces: boolean | null;
  commentaire: string | null;
  concept: PharmacieInfo_concept_concept | null;
  fournisseurMobilier: PharmacieInfo_concept_fournisseurMobilier | null;
  enseigniste: PharmacieInfo_concept_enseigniste | null;
  facade: PharmacieInfo_concept_facade | null;
  leaflet: PharmacieInfo_concept_leaflet | null;
}

export interface PharmacieInfo_statCA {
  __typename: "PharmacieStatCA";
  id: string;
  exercice: number | null;
  dateDebut: any | null;
  dateFin: any | null;
  caTTC: number | null;
  caHt: number | null;
  caTVA1: number | null;
  tauxTVA1: number | null;
  caTVA2: number | null;
  tauxTVA2: number | null;
  caTVA3: number | null;
  tauxTVA3: number | null;
  caTVA4: number | null;
  tauxTVA4: number | null;
  caTVA5: number | null;
  tauxTVA5: number | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInfo_digitales_servicePharmacie {
  __typename: "ServicePharmacie";
  id: string | null;
  nom: string | null;
  typeService: TypeService | null;
}

export interface PharmacieInfo_digitales_pharmaciePestataire {
  __typename: "PrestatairePharmacie";
  id: string;
  typePrestataire: TypePrestataire | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  telephone1: string | null;
  telephone2: string | null;
  fax: string | null;
  commentaire: string | null;
  partenaire: boolean | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}

export interface PharmacieInfo_digitales {
  __typename: "PharmacieDigitale";
  id: string;
  flagService: boolean | null;
  dateInstallation: any | null;
  url: string | null;
  servicePharmacie: PharmacieInfo_digitales_servicePharmacie | null;
  pharmaciePestataire: PharmacieInfo_digitales_pharmaciePestataire | null;
}

export interface PharmacieInfo {
  __typename: "Pharmacie";
  cip: string | null;
  nom: string | null;
  adresse1: string | null;
  adresse2: string | null;
  id: string;
  type: string;
  ville: string | null;
  cp: string | null;
  pays: string | null;
  uga: string | null;
  nbEmploye: number | null;
  nbAssistant: number | null;
  nbAutreTravailleur: number | null;
  sortie: number | null;
  numFiness: string | null;
  nbJourOuvre: number | null;
  latitude: string | null;
  longitude: string | null;
  commentaire: string | null;
  departement: PharmacieInfo_departement | null;
  titulaires: (PharmacieInfo_titulaires | null)[] | null;
  presidentRegion: PharmacieInfo_presidentRegion | null;
  contact: PharmacieInfo_contact | null;
  entreeSortie: PharmacieInfo_entreeSortie | null;
  grossistes: (PharmacieInfo_grossistes | null)[] | null;
  generiqueurs: (PharmacieInfo_generiqueurs | null)[] | null;
  satisfaction: PharmacieInfo_satisfaction | null;
  segmentation: PharmacieInfo_segmentation | null;
  compta: PharmacieInfo_compta | null;
  informatique: PharmacieInfo_informatique | null;
  achat: PharmacieInfo_achat | null;
  cap: PharmacieInfo_cap | null;
  concept: PharmacieInfo_concept | null;
  statCA: PharmacieInfo_statCA | null;
  digitales: (PharmacieInfo_digitales | null)[] | null;
}
