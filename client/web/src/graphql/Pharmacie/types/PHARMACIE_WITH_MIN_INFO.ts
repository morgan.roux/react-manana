/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: PHARMACIE_WITH_MIN_INFO
// ====================================================

export interface PHARMACIE_WITH_MIN_INFO_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface PHARMACIE_WITH_MIN_INFO {
  pharmacie: PHARMACIE_WITH_MIN_INFO_pharmacie | null;
}

export interface PHARMACIE_WITH_MIN_INFOVariables {
  id: string;
}
