/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_PHARMACIE
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "Partenaire" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  sortie: number | null;
}

export interface SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_users {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_segmentation_trancheCA {
  __typename: "TrancheCA";
  id: string;
  libelle: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_segmentation_contrat {
  __typename: "Contrat";
  id: string;
  nom: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_segmentation {
  __typename: "PharmacieSegmentation";
  id: string;
  trancheCA: SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_segmentation_trancheCA | null;
  contrat: SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_segmentation_contrat | null;
}

export interface SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_achat_canal {
  __typename: "CommandeCanal";
  id: string;
  libelle: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_achat {
  __typename: "PharmacieAchat";
  id: string;
  canal: SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_achat_canal | null;
}

export interface SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie {
  __typename: "Pharmacie";
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_departement | null;
  titulaires: (SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
  users: (SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_users | null)[] | null;
  segmentation: SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_segmentation | null;
  achat: SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie_achat | null;
  nbReclamation: number | null;
  nbAppel: number | null;
}

export type SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data = SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Action | SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data_Pharmacie;

export interface SEARCH_CUSTOM_CONTENT_PHARMACIE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_PHARMACIE_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PHARMACIE {
  search: SEARCH_CUSTOM_CONTENT_PHARMACIE_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_PHARMACIEVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
}
