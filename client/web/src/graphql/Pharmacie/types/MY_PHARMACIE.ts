/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: MY_PHARMACIE
// ====================================================

export interface MY_PHARMACIE_me_pharmacie_departement_region {
  __typename: "Region";
  id: string;
  nom: string | null;
}

export interface MY_PHARMACIE_me_pharmacie_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
  region: MY_PHARMACIE_me_pharmacie_departement_region | null;
}

export interface MY_PHARMACIE_me_pharmacie {
  __typename: "Pharmacie";
  id: string;
  cip: string | null;
  nom: string | null;
  adresse1: string | null;
  type: string;
  ville: string | null;
  departement: MY_PHARMACIE_me_pharmacie_departement | null;
}

export interface MY_PHARMACIE_me {
  __typename: "User";
  id: string;
  pharmacie: MY_PHARMACIE_me_pharmacie | null;
}

export interface MY_PHARMACIE {
  me: MY_PHARMACIE_me | null;
}
