/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: DELETE_SOFT_PHARMACIES
// ====================================================

export interface DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_departement {
  __typename: "Departement";
  id: string;
  nom: string | null;
}

export interface DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_titulaires {
  __typename: "Titulaire";
  id: string;
  nom: string | null;
  prenom: string | null;
  fullName: string | null;
  sortie: number | null;
}

export interface DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_users {
  __typename: "User";
  id: string;
  userName: string | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_segmentation_trancheCA {
  __typename: "TrancheCA";
  id: string;
  libelle: string | null;
}

export interface DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_segmentation_contrat {
  __typename: "Contrat";
  id: string;
  nom: string | null;
}

export interface DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_segmentation {
  __typename: "PharmacieSegmentation";
  id: string;
  trancheCA: DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_segmentation_trancheCA | null;
  contrat: DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_segmentation_contrat | null;
}

export interface DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_achat_canal {
  __typename: "CommandeCanal";
  id: string;
  libelle: string | null;
}

export interface DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_achat {
  __typename: "PharmacieAchat";
  id: string;
  canal: DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_achat_canal | null;
}

export interface DELETE_SOFT_PHARMACIES_deleteSoftPharmacies {
  __typename: "Pharmacie";
  id: string;
  sortie: number | null;
  cip: string | null;
  numFiness: string | null;
  nom: string | null;
  departement: DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_departement | null;
  titulaires: (DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_titulaires | null)[] | null;
  adresse1: string | null;
  adresse2: string | null;
  cp: string | null;
  ville: string | null;
  idGroupement: string | null;
  actived: boolean | null;
  users: (DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_users | null)[] | null;
  segmentation: DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_segmentation | null;
  achat: DELETE_SOFT_PHARMACIES_deleteSoftPharmacies_achat | null;
  nbReclamation: number | null;
  nbAppel: number | null;
}

export interface DELETE_SOFT_PHARMACIES {
  deleteSoftPharmacies: (DELETE_SOFT_PHARMACIES_deleteSoftPharmacies | null)[] | null;
}

export interface DELETE_SOFT_PHARMACIESVariables {
  ids: string[];
}
