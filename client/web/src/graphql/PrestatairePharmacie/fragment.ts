import gql from 'graphql-tag';

export const PRESTATAIRE_PHARMACIE_INFO_FRAGMENT = gql`
  fragment PrestatairePharmacieInfo on PrestatairePharmacie {
    id
    typePrestataire
    nom
    adresse1
    adresse2
    cp
    ville
    telephone1
    telephone2
    fax
    commentaire
    partenaire
    codeMaj
    dateCreation
    dateModification
  }
`;
