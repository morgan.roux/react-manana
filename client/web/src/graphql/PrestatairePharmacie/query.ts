import gql from 'graphql-tag';

export const GET_MINIM_PRESTATAIRE_PHARMACIES = gql`
  query MINIM_PRESTATAIRE_PHARMACIES {
    prestatairePharmacies {
      id
      nom
    }
  }
`;
