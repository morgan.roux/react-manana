/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RGPD_AUTORISATIONS
// ====================================================

export interface RGPD_AUTORISATIONS_rgpdAutorisations {
  __typename: "RgpdAutorisation";
  id: string;
  title: string;
  description: string;
  order: number | null;
}

export interface RGPD_AUTORISATIONS {
  rgpdAutorisations: RGPD_AUTORISATIONS_rgpdAutorisations[];
}

export interface RGPD_AUTORISATIONSVariables {
  idGroupement?: string | null;
}
