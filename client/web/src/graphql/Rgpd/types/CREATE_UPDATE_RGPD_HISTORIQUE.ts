/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RgpdHistoriqueInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_RGPD_HISTORIQUE
// ====================================================

export interface CREATE_UPDATE_RGPD_HISTORIQUE_createUpdateRgpdHistorique_user {
  __typename: "User";
  id: string;
  userName: string | null;
}

export interface CREATE_UPDATE_RGPD_HISTORIQUE_createUpdateRgpdHistorique {
  __typename: "RgpdHistorique";
  id: string;
  accept_all: boolean | null;
  refuse_all: boolean | null;
  user: CREATE_UPDATE_RGPD_HISTORIQUE_createUpdateRgpdHistorique_user;
}

export interface CREATE_UPDATE_RGPD_HISTORIQUE {
  createUpdateRgpdHistorique: CREATE_UPDATE_RGPD_HISTORIQUE_createUpdateRgpdHistorique;
}

export interface CREATE_UPDATE_RGPD_HISTORIQUEVariables {
  input: RgpdHistoriqueInput;
}
