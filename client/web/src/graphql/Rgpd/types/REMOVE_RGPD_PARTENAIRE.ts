/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: REMOVE_RGPD_PARTENAIRE
// ====================================================

export interface REMOVE_RGPD_PARTENAIRE_deleteRgpdPartenaire {
  __typename: "RgpdPartenaire";
  id: string;
  title: string;
  description: string;
  order: number | null;
}

export interface REMOVE_RGPD_PARTENAIRE {
  deleteRgpdPartenaire: REMOVE_RGPD_PARTENAIRE_deleteRgpdPartenaire | null;
}

export interface REMOVE_RGPD_PARTENAIREVariables {
  id: string;
}
