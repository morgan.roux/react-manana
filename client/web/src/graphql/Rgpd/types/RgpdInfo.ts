/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RgpdInfo
// ====================================================

export interface RgpdInfo {
  __typename: "Rgpd";
  id: string;
  politiqueConfidentialite: string | null;
  conditionUtilisation: string | null;
  informationsCookies: string | null;
}
