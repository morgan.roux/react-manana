/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RgpdAccueilPlusInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_RGP_ACCEUIL_PLUSES
// ====================================================

export interface CREATE_UPDATE_RGP_ACCEUIL_PLUSES_createUpdateRgpdAccueilPlus {
  __typename: "RgpdAccueilPlus";
  id: string;
  title: string;
  description: string;
  order: number | null;
}

export interface CREATE_UPDATE_RGP_ACCEUIL_PLUSES {
  createUpdateRgpdAccueilPlus: CREATE_UPDATE_RGP_ACCEUIL_PLUSES_createUpdateRgpdAccueilPlus;
}

export interface CREATE_UPDATE_RGP_ACCEUIL_PLUSESVariables {
  input: RgpdAccueilPlusInput;
}
