/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RGPD_ACCUEIL
// ====================================================

export interface RGPD_ACCUEIL_rgpdAccueil {
  __typename: "RgpdAccueil";
  id: string;
  title: string;
  description: string;
}

export interface RGPD_ACCUEIL {
  rgpdAccueil: RGPD_ACCUEIL_rgpdAccueil | null;
}

export interface RGPD_ACCUEILVariables {
  id: string;
}
