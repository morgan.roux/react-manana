/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RgpdAccueilInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_RGP_ACCEUIL
// ====================================================

export interface CREATE_UPDATE_RGP_ACCEUIL_createUpdateRgpdAccueil {
  __typename: "RgpdAccueil";
  id: string;
  title: string;
  description: string;
}

export interface CREATE_UPDATE_RGP_ACCEUIL {
  createUpdateRgpdAccueil: CREATE_UPDATE_RGP_ACCEUIL_createUpdateRgpdAccueil;
}

export interface CREATE_UPDATE_RGP_ACCEUILVariables {
  input: RgpdAccueilInput;
}
