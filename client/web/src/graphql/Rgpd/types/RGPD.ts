/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RGPD
// ====================================================

export interface RGPD_rgpd {
  __typename: "Rgpd";
  id: string;
  politiqueConfidentialite: string | null;
  conditionUtilisation: string | null;
  informationsCookies: string | null;
}

export interface RGPD {
  rgpd: RGPD_rgpd | null;
}

export interface RGPDVariables {
  id: string;
}
