/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RgpdPartenaireInfo
// ====================================================

export interface RgpdPartenaireInfo {
  __typename: "RgpdPartenaire";
  id: string;
  title: string;
  description: string;
  order: number | null;
}
