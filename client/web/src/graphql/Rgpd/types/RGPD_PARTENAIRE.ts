/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RGPD_PARTENAIRE
// ====================================================

export interface RGPD_PARTENAIRE_rgpdPartenaire {
  __typename: "RgpdPartenaire";
  id: string;
  title: string;
  description: string;
  order: number | null;
}

export interface RGPD_PARTENAIRE {
  rgpdPartenaire: RGPD_PARTENAIRE_rgpdPartenaire | null;
}

export interface RGPD_PARTENAIREVariables {
  id: string;
}
