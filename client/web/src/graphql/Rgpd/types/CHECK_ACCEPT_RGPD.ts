/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: CHECK_ACCEPT_RGPD
// ====================================================

export interface CHECK_ACCEPT_RGPD {
  checkAcceptRgpd: boolean;
}

export interface CHECK_ACCEPT_RGPDVariables {
  idUser: string;
}
