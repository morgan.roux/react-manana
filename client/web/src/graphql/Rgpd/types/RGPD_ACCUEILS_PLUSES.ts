/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: RGPD_ACCUEILS_PLUSES
// ====================================================

export interface RGPD_ACCUEILS_PLUSES_rgpdAccueilPluses {
  __typename: "RgpdAccueilPlus";
  id: string;
  title: string;
  description: string;
  order: number | null;
}

export interface RGPD_ACCUEILS_PLUSES {
  rgpdAccueilPluses: RGPD_ACCUEILS_PLUSES_rgpdAccueilPluses[];
}

export interface RGPD_ACCUEILS_PLUSESVariables {
  idGroupement?: string | null;
}
