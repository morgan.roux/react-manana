/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: RgpdAutorisationInfo
// ====================================================

export interface RgpdAutorisationInfo {
  __typename: "RgpdAutorisation";
  id: string;
  title: string;
  description: string;
  order: number | null;
}
