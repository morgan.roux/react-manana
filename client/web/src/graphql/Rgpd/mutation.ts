import gql from 'graphql-tag';
import { RGPD_ACCUEIL_INFO_FRAGEMENT, RGPD_ACCUEIL_PLUS_INFO_FRAGEMENT, RGPD_HISTORIQUE_INFO_FRAGEMENT, RGPD_INFO_FRAGEMENT, RGPD_PARTENAIRE_INFO_FRAGEMENT } from './fragment';



export const DO_CREATE_UPDATE_RGPD_HISTORIQUE = gql`
  mutation CREATE_UPDATE_RGPD_HISTORIQUE($input: RgpdHistoriqueInput!) {
    createUpdateRgpdHistorique(input: $input) {
      ...RgpdHistoriqueInfo
    }
  }
  ${RGPD_HISTORIQUE_INFO_FRAGEMENT}
`;
export const DO_CREATE_UPDATE_RGPD_PARTENAIRE = gql`
  mutation CREATE_UPDATE_RGPD_PARTENAIRE ($input: RgpdPartenaireInput!) {
    createUpdateRgpdPartenaire(input:$input) {
      ...RgpdPartenaireInfo
       
    }
  }
  ${RGPD_PARTENAIRE_INFO_FRAGEMENT}
`;
export const DO_REMOVE_RGPD_PARTENAIRE = gql`
  mutation REMOVE_RGPD_PARTENAIRE ($id: ID!) {
    deleteRgpdPartenaire(id:$id) {
      ...RgpdPartenaireInfo     
    }
  }
  ${RGPD_PARTENAIRE_INFO_FRAGEMENT}
`;
export const DO_CREATE_UPDATE_RGP_ACCEUIL = gql`
  mutation CREATE_UPDATE_RGP_ACCEUIL ($input: RgpdAccueilInput!) {
    createUpdateRgpdAccueil(input:$input) {
      ...RgpdAccueilInfo     
    }
  }
  ${RGPD_ACCUEIL_INFO_FRAGEMENT}
`;
export const DO_CREATE_UPDATE_RGP_ACCEUIL_PLUSES = gql`
  mutation CREATE_UPDATE_RGP_ACCEUIL_PLUSES ($input: RgpdAccueilPlusInput!) {
    createUpdateRgpdAccueilPlus(input:$input) {
      ...RgpdAccueilPlusInfo     
    }
  }
  ${RGPD_ACCUEIL_PLUS_INFO_FRAGEMENT}
`;

export const DO_CREATE_UPDATE_RGPD = gql`
  mutation CREATE_UPDATE_RGPD($input: RgpdInput!) {
    createUpdateRgpd(input: $input) {
      ...RgpdInfo
    }
  }
  ${RGPD_INFO_FRAGEMENT}
`;
