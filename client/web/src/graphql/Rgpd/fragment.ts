import gql from 'graphql-tag';

export const RGPD_INFO_FRAGEMENT = gql`
  fragment RgpdInfo on Rgpd {
    id
    politiqueConfidentialite
    conditionUtilisation
    informationsCookies
  }
`;

export const RGPD_ACCUEIL_INFO_FRAGEMENT = gql`
  fragment RgpdAccueilInfo on RgpdAccueil {
    id
    title
    description
  }
`;

export const RGPD_ACCUEIL_PLUS_INFO_FRAGEMENT = gql`
  fragment RgpdAccueilPlusInfo on RgpdAccueilPlus {
    id
    title
    description
    order
  }
`;

export const RGPD_AUTORISATION_INFO_FRAGEMENT = gql`
  fragment RgpdAutorisationInfo on RgpdAutorisation {
    id
    title
    description
    order
  }
`;

export const RGPD_PARTENAIRE_INFO_FRAGEMENT = gql`
  fragment RgpdPartenaireInfo on RgpdPartenaire {
    id
    title
    description
    order
  }
`;

export const RGPD_HISTORIQUE_INFO_FRAGEMENT = gql`
  fragment RgpdHistoriqueInfo on RgpdHistorique {
    id
    accept_all
    refuse_all
    user {
      id
      userName
    }
  }
`;

export const RGPD_HISTORIQUE_INFO_PLUS_INFO_FRAGEMENT = gql`
  fragment RgpdHistoriqueInfoPlusInfo on RgpdHistoriqueInfoPlus {
    id
    type
    accepted
    idItemAssocie
    historique {
      id
    }
  }
`;
