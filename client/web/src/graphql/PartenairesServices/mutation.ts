import gql from 'graphql-tag';

export const DO_CREATE_UPDATE_PARTENAIRE_SERVICE = gql`
  mutation CREATE_UPDATE_PARTENAIRE_SERVICE($input: PartenaireServiceInput!) {
    createUpdatePartenaireService(input: $input) {
      id
      nom
      partenaireServiceSuite {
        id
        adresse1
        codePostal
        ville
      }
      partenaireServicePartenaire {
        id
        dateDebutPartenaire
        dateFinPartenaire
        typePartenaire
        statutPartenaire
      }
    }
  }
`;

export const DO_DELETE_PARTENAIRE_SERVICES = gql`
  mutation DELETE_SOFT_PARTENAIRE_SERVICE($ids: [ID!]!) {
    softDeletePartenaireServices(ids: $ids) {
      id
    }
  }
`;
