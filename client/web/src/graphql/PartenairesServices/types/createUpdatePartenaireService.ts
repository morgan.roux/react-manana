/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PartenaireServiceInput } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: createUpdatePartenaireService
// ====================================================

export interface createUpdatePartenaireService_createPartenaire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
}

export interface createUpdatePartenaireService {
  createPartenaire: createUpdatePartenaireService_createPartenaire | null;
}

export interface createUpdatePartenaireServiceVariables {
  input: PartenaireServiceInput;
}
