/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { StatutPartenaire, TypePartenaire, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: SEARCH_CUSTOM_CONTENT_PARTENAIRE
// ====================================================

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data_Action {
  __typename: "Action" | "TodoActionType" | "User" | "Contact" | "Groupement" | "Pharmacie" | "Comment" | "Item" | "Avatar" | "Titulaire" | "TitulaireAffectation" | "TitulaireDemandeAffectation" | "TitulaireFonction" | "Service" | "CommandeCanal" | "Role" | "Personnel" | "Actualite" | "ActualiteOrigine" | "Laboratoire" | "LaboratoireRessource" | "LaboratoireRepresentant" | "PartenaireServiceSuite" | "ServicePartenaire" | "ServiceType" | "Produit" | "Famille" | "TVA" | "Acte" | "Liste" | "LibelleDivers" | "TauxSS" | "ProduitCanal" | "CanalGamme" | "GammeCatalogue" | "Operation" | "Marche" | "GroupeClient" | "Promotion" | "Partage" | "GroupeAmis" | "PartenaireServicePartenaire" | "Ppersonnel" | "Project" | "Couleur" | "TodoActionOrigine" | "TodoSection" | "TodoEtiquette" | "ActionActivite" | "ActiviteUser" | "Aide" | "Commande" | "CommandeLigne" | "CommandeType" | "GroupeAmisDetail" | "IdeeOuBonnePratique" | "InformationLiaison" | "InformationLiaisonUserConcernee" | "Ticket" | "TicketMotif" | "PartenaireRepresentant" | "PartenaireType" | "PartenaireRepresentantAffectation" | "PartenaireRepresentantDemandeAffectation" | "PersonnelAffectation" | "PersonnelFonction" | "PersonnelDemandeAffectation" | "Publicite" | "SuiviPublicitaire" | "Ressource" | "TicketChangementCible" | "TicketStatut" | "TicketChangementStatut" | "TicketOrigine" | "TodoActionEtiquette" | "TodoEtiquetteFavoris" | "TodoProjetFavoris" | "Traitement" | "ProduitStock";
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data_Partenaire_partenaireServiceSuite {
  __typename: "PartenaireServiceSuite";
  id: string;
  adresse1: string | null;
  telephone: string | null;
  codePostal: string | null;
  ville: string | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data_Partenaire_partenaireServicePartenaire {
  __typename: "PartenaireServicePartenaire";
  id: string;
  dateDebutPartenaire: any;
  dateFinPartenaire: any;
  statutPartenaire: StatutPartenaire | null;
  typePartenaire: TypePartenaire | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data_Partenaire_services {
  __typename: "ServicePartenaire";
  id: string;
  nom: string;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data_Partenaire_user {
  __typename: "User";
  id: string;
  userName: string | null;
  jourNaissance: number | null;
  moisNaissance: number | null;
  anneeNaissance: number | null;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data_Partenaire {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
  partenaireServiceSuite: SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data_Partenaire_partenaireServiceSuite | null;
  partenaireServicePartenaire: SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data_Partenaire_partenaireServicePartenaire | null;
  services: (SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data_Partenaire_services | null)[] | null;
  user: SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data_Partenaire_user | null;
}

export type SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data = SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data_Action | SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data_Partenaire;

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE_search {
  __typename: "SearchResult";
  total: number;
  data: (SEARCH_CUSTOM_CONTENT_PARTENAIRE_search_data | null)[] | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIRE {
  search: SEARCH_CUSTOM_CONTENT_PARTENAIRE_search | null;
}

export interface SEARCH_CUSTOM_CONTENT_PARTENAIREVariables {
  type?: (string | null)[] | null;
  query?: any | null;
  skip?: number | null;
  take?: number | null;
  filterBy?: any | null;
  sortBy?: any | null;
}
