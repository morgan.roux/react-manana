/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { TypePartenaire, StatutPartenaire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PARTENAIRE_SERVICE
// ====================================================

export interface PARTENAIRE_SERVICE_partenaire_partenaireServiceSuite {
  __typename: "PartenaireServiceSuite";
  id: string;
  adresse1: string | null;
  telephone: string | null;
  ville: string | null;
  codePostal: string | null;
}

export interface PARTENAIRE_SERVICE_partenaire_services {
  __typename: "ServicePartenaire";
  id: string;
  nom: string;
  dateDemarrage: any | null;
  nbCollaboQualifie: number | null;
  commentaire: string | null;
}

export interface PARTENAIRE_SERVICE_partenaire_partenaireServicePartenaire {
  __typename: "PartenaireServicePartenaire";
  id: string;
  dateDebutPartenaire: any;
  dateFinPartenaire: any;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface PARTENAIRE_SERVICE_partenaire {
  __typename: "Partenaire";
  id: string;
  type: string;
  nom: string | null;
  partenaireServiceSuite: PARTENAIRE_SERVICE_partenaire_partenaireServiceSuite | null;
  services: (PARTENAIRE_SERVICE_partenaire_services | null)[] | null;
  partenaireServicePartenaire: PARTENAIRE_SERVICE_partenaire_partenaireServicePartenaire | null;
}

export interface PARTENAIRE_SERVICE {
  partenaire: PARTENAIRE_SERVICE_partenaire | null;
}

export interface PARTENAIRE_SERVICEVariables {
  id: string;
}
