/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { PartenaireServiceInput, TypePartenaire, StatutPartenaire } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: CREATE_UPDATE_PARTENAIRE_SERVICE
// ====================================================

export interface CREATE_UPDATE_PARTENAIRE_SERVICE_createUpdatePartenaireService_partenaireServiceSuite {
  __typename: "PartenaireServiceSuite";
  id: string;
  adresse1: string | null;
  codePostal: string | null;
  ville: string | null;
}

export interface CREATE_UPDATE_PARTENAIRE_SERVICE_createUpdatePartenaireService_partenaireServicePartenaire {
  __typename: "PartenaireServicePartenaire";
  id: string;
  dateDebutPartenaire: any;
  dateFinPartenaire: any;
  typePartenaire: TypePartenaire | null;
  statutPartenaire: StatutPartenaire | null;
}

export interface CREATE_UPDATE_PARTENAIRE_SERVICE_createUpdatePartenaireService {
  __typename: "Partenaire";
  id: string;
  nom: string | null;
  partenaireServiceSuite: CREATE_UPDATE_PARTENAIRE_SERVICE_createUpdatePartenaireService_partenaireServiceSuite | null;
  partenaireServicePartenaire: CREATE_UPDATE_PARTENAIRE_SERVICE_createUpdatePartenaireService_partenaireServicePartenaire | null;
}

export interface CREATE_UPDATE_PARTENAIRE_SERVICE {
  createUpdatePartenaireService: CREATE_UPDATE_PARTENAIRE_SERVICE_createUpdatePartenaireService | null;
}

export interface CREATE_UPDATE_PARTENAIRE_SERVICEVariables {
  input: PartenaireServiceInput;
}
