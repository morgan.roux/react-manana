import gql from 'graphql-tag';
import { TICKET_STATUT_INFO_FRAGMENT } from './fragment';
export const GET_TICKET_STATUTS = gql`
  query TICKET_STATUTS {
    ticketStatuts {
      ...TicketStatutInfo
    }
  }
  ${TICKET_STATUT_INFO_FRAGMENT}
`;
