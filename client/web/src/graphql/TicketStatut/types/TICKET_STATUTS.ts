/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TICKET_STATUTS
// ====================================================

export interface TICKET_STATUTS_ticketStatuts {
  __typename: "TicketStatut";
  id: string;
  code: string;
  nom: string;
}

export interface TICKET_STATUTS {
  ticketStatuts: (TICKET_STATUTS_ticketStatuts | null)[] | null;
}
