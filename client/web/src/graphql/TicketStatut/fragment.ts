import gql from 'graphql-tag';
export const TICKET_STATUT_INFO_FRAGMENT = gql`
  fragment TicketStatutInfo on TicketStatut {
    id
    code
    nom
  }
`;
