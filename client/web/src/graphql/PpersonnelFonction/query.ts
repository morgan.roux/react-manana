import gql from 'graphql-tag';
import { PRESIDENT_FONCTION_INFO_FRAGMENT } from './fragment';

export const GET_PRESIDENT_FONCTIONS = gql`
  query PRESIDENT_FONCTIONS($take: Int, $skip: Int) {
    titulaireFonctions(take: $take, skip: $skip) {
      total
      data {
        ...PresidentFonctionInfo
      }
    }
  }
  ${PRESIDENT_FONCTION_INFO_FRAGMENT}
`;
