/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TrancheCAInfo
// ====================================================

export interface TrancheCAInfo {
  __typename: "TrancheCA";
  id: string;
  libelle: string | null;
  valMax: number | null;
  valMin: number | null;
}
