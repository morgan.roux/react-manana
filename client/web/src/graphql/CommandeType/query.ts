import gql from 'graphql-tag';

export const GET_COMMANDETYPES = gql`
  query commandeTypes {
    commandeTypes {
      id
      libelle
      code
      countOperationsCommercials
    }
  }
`;
