/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sendingtype, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL fragment: CryptoMd5Info
// ====================================================

export interface CryptoMd5Info_externalUserMappings_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface CryptoMd5Info_externalUserMappings_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface CryptoMd5Info_externalUserMappings_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: CryptoMd5Info_externalUserMappings_user_userPhoto_fichier | null;
}

export interface CryptoMd5Info_externalUserMappings_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface CryptoMd5Info_externalUserMappings_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: CryptoMd5Info_externalUserMappings_user_role | null;
  userPhoto: CryptoMd5Info_externalUserMappings_user_userPhoto | null;
  pharmacie: CryptoMd5Info_externalUserMappings_user_pharmacie | null;
}

export interface CryptoMd5Info_externalUserMappings {
  __typename: "ExternalMapping";
  id: string;
  idExternaluser: string | null;
  idClient: string | null;
  password: string | null;
  user: CryptoMd5Info_externalUserMappings_user | null;
}

export interface CryptoMd5Info_SsoApplication {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
}

export interface CryptoMd5Info {
  __typename: "CryptoMd5";
  id: string;
  beginGet: number | null;
  endGet: number | null;
  hexadecimal: boolean | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  haveExternalUserMapping: boolean | null;
  externalUserMappings: (CryptoMd5Info_externalUserMappings | null)[] | null;
  SsoApplication: CryptoMd5Info_SsoApplication | null;
}
