/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { Sendingtype, UserStatus } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: MANY_CRYPTO_MD5
// ====================================================

export interface MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings_user_role {
  __typename: "Role";
  type: string;
  id: string;
  code: string | null;
  nom: string | null;
}

export interface MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings_user_userPhoto_fichier {
  __typename: "Fichier";
  id: string;
  publicUrl: string | null;
}

export interface MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings_user_userPhoto {
  __typename: "UserPhoto";
  id: string;
  fichier: MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings_user_userPhoto_fichier | null;
}

export interface MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings_user_pharmacie {
  __typename: "Pharmacie";
  id: string;
  nom: string | null;
}

export interface MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings_user {
  __typename: "User";
  id: string;
  email: string | null;
  login: string | null;
  status: UserStatus | null;
  userName: string | null;
  type: string;
  theme: string | null;
  nbReclamation: number | null;
  nbAppel: number | null;
  role: MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings_user_role | null;
  userPhoto: MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings_user_userPhoto | null;
  pharmacie: MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings_user_pharmacie | null;
}

export interface MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings {
  __typename: "ExternalMapping";
  id: string;
  idExternaluser: string | null;
  idClient: string | null;
  password: string | null;
  user: MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings_user | null;
}

export interface MANY_CRYPTO_MD5_cryptoMd5s_SsoApplication {
  __typename: "SsoApplication";
  id: string;
  nom: string;
  url: string;
}

export interface MANY_CRYPTO_MD5_cryptoMd5s {
  __typename: "CryptoMd5";
  id: string;
  beginGet: number | null;
  endGet: number | null;
  hexadecimal: boolean | null;
  requestUrl: string | null;
  Sendingtype: Sendingtype | null;
  haveExternalUserMapping: boolean | null;
  externalUserMappings: (MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings | null)[] | null;
  SsoApplication: MANY_CRYPTO_MD5_cryptoMd5s_SsoApplication | null;
}

export interface MANY_CRYPTO_MD5 {
  cryptoMd5s: (MANY_CRYPTO_MD5_cryptoMd5s | null)[] | null;
}

export interface MANY_CRYPTO_MD5Variables {
  idgroupement: string;
}
