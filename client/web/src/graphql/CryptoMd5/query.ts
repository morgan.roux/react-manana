import gql from 'graphql-tag';
import { CRYPTO_MD5_INFO } from './fragment';

export const GET_MANY_CRYPTO_MD5 = gql`
  query MANY_CRYPTO_MD5($idgroupement: ID!){
    cryptoMd5s(idgroupement: $idgroupement) {
        ...CryptoMd5Info
    }
  }
  ${CRYPTO_MD5_INFO}
`;