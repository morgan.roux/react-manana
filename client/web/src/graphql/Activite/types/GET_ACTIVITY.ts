/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: GET_ACTIVITY
// ====================================================

export interface GET_ACTIVITY_userActiviteUser_idUser_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_ACTIVITY_userActiviteUser_idUser {
  __typename: "User";
  id: string;
  userName: string | null;
  role: GET_ACTIVITY_userActiviteUser_idUser_role | null;
}

export interface GET_ACTIVITY_userActiviteUser_idActiviteType {
  __typename: "ActiviteType";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface GET_ACTIVITY_userActiviteUser {
  __typename: "ActiviteUser";
  id: string;
  type: string;
  dateCreation: any | null;
  idUser: GET_ACTIVITY_userActiviteUser_idUser | null;
  idActiviteType: GET_ACTIVITY_userActiviteUser_idActiviteType | null;
  log: string | null;
}

export interface GET_ACTIVITY {
  userActiviteUser: (GET_ACTIVITY_userActiviteUser | null)[] | null;
}

export interface GET_ACTIVITYVariables {
  idUser: string;
}
