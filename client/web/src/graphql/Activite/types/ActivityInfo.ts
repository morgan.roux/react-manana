/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ActivityInfo
// ====================================================

export interface ActivityInfo_idUser_role {
  __typename: "Role";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ActivityInfo_idUser {
  __typename: "User";
  id: string;
  userName: string | null;
  role: ActivityInfo_idUser_role | null;
}

export interface ActivityInfo_idActiviteType {
  __typename: "ActiviteType";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface ActivityInfo {
  __typename: "ActiviteUser";
  id: string;
  type: string;
  dateCreation: any | null;
  idUser: ActivityInfo_idUser | null;
  idActiviteType: ActivityInfo_idActiviteType | null;
  log: string | null;
}
