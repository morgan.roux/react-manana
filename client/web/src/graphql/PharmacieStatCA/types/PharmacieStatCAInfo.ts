/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: PharmacieStatCAInfo
// ====================================================

export interface PharmacieStatCAInfo {
  __typename: "PharmacieStatCA";
  id: string;
  exercice: number | null;
  dateDebut: any | null;
  dateFin: any | null;
  caTTC: number | null;
  caHt: number | null;
  caTVA1: number | null;
  tauxTVA1: number | null;
  caTVA2: number | null;
  tauxTVA2: number | null;
  caTVA3: number | null;
  tauxTVA3: number | null;
  caTVA4: number | null;
  tauxTVA4: number | null;
  caTVA5: number | null;
  tauxTVA5: number | null;
  codeMaj: string | null;
  dateCreation: any | null;
  dateModification: any | null;
}
