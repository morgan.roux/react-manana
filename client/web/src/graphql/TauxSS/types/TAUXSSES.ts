/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: TAUXSSES
// ====================================================

export interface TAUXSSES_tauxsses {
  __typename: "TauxSS";
  id: string;
  codeTaux: number | null;
  tauxSS: number | null;
}

export interface TAUXSSES {
  tauxsses: (TAUXSSES_tauxsses | null)[] | null;
}
