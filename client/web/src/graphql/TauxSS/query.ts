import gql from 'graphql-tag';

export const GET_TAUXSSES = gql`
  query TAUXSSES {
    tauxsses {
      id
      codeTaux
      tauxSS
    }
  }
`;
