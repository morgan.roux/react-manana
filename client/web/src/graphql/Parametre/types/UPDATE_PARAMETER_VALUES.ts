/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ParameterInput, ParamCategory } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL mutation operation: UPDATE_PARAMETER_VALUES
// ====================================================

export interface UPDATE_PARAMETER_VALUES_updateParameterValues_options {
  __typename: "ParameterOption";
  label: string | null;
  value: string | null;
}

export interface UPDATE_PARAMETER_VALUES_updateParameterValues_parameterGroupe {
  __typename: "ParameterGroupe";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UPDATE_PARAMETER_VALUES_updateParameterValues_parameterType {
  __typename: "ParameterType";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface UPDATE_PARAMETER_VALUES_updateParameterValues_value {
  __typename: "ParameterValue";
  id: string;
  value: string | null;
}

export interface UPDATE_PARAMETER_VALUES_updateParameterValues {
  __typename: "Parameter";
  id: string;
  code: string | null;
  name: string | null;
  defaultValue: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  category: ParamCategory | null;
  options: (UPDATE_PARAMETER_VALUES_updateParameterValues_options | null)[] | null;
  parameterGroupe: UPDATE_PARAMETER_VALUES_updateParameterValues_parameterGroupe | null;
  parameterType: UPDATE_PARAMETER_VALUES_updateParameterValues_parameterType | null;
  value: UPDATE_PARAMETER_VALUES_updateParameterValues_value | null;
}

export interface UPDATE_PARAMETER_VALUES {
  updateParameterValues: (UPDATE_PARAMETER_VALUES_updateParameterValues | null)[] | null;
}

export interface UPDATE_PARAMETER_VALUESVariables {
  parameters: ParameterInput[];
}
