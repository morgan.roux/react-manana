/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { ParamCategory } from "./../../../types/graphql-global-types";

// ====================================================
// GraphQL query operation: PARAMETERS_BY_CATEGORIES
// ====================================================

export interface PARAMETERS_BY_CATEGORIES_parametersByCategories_options {
  __typename: "ParameterOption";
  label: string | null;
  value: string | null;
}

export interface PARAMETERS_BY_CATEGORIES_parametersByCategories_parameterGroupe {
  __typename: "ParameterGroupe";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PARAMETERS_BY_CATEGORIES_parametersByCategories_parameterType {
  __typename: "ParameterType";
  id: string;
  code: string | null;
  nom: string | null;
}

export interface PARAMETERS_BY_CATEGORIES_parametersByCategories_value {
  __typename: "ParameterValue";
  id: string;
  value: string | null;
}

export interface PARAMETERS_BY_CATEGORIES_parametersByCategories {
  __typename: "Parameter";
  id: string;
  code: string | null;
  name: string | null;
  defaultValue: string | null;
  dateCreation: any | null;
  dateModification: any | null;
  category: ParamCategory | null;
  options: (PARAMETERS_BY_CATEGORIES_parametersByCategories_options | null)[] | null;
  parameterGroupe: PARAMETERS_BY_CATEGORIES_parametersByCategories_parameterGroupe | null;
  parameterType: PARAMETERS_BY_CATEGORIES_parametersByCategories_parameterType | null;
  value: PARAMETERS_BY_CATEGORIES_parametersByCategories_value | null;
}

export interface PARAMETERS_BY_CATEGORIES {
  parametersByCategories: (PARAMETERS_BY_CATEGORIES_parametersByCategories | null)[] | null;
}

export interface PARAMETERS_BY_CATEGORIESVariables {
  categories: ParamCategory[];
}
