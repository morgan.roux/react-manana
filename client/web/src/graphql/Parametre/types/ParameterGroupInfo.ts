/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ParameterGroupInfo
// ====================================================

export interface ParameterGroupInfo {
  __typename: "ParameterGroupe";
  id: string;
  code: string | null;
  nom: string | null;
}
