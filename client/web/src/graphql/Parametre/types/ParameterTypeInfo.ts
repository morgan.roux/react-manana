/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ParameterTypeInfo
// ====================================================

export interface ParameterTypeInfo {
  __typename: "ParameterType";
  id: string;
  code: string | null;
  nom: string | null;
}
