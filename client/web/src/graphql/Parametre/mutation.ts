import gql from 'graphql-tag';
import { PARAMETER_INFO_FRAGEMENT } from './fragment';

interface ParameterInput {
  id: string;
  value: string;
}

export const DO_UPDATE_PARAMETER_VALUES = gql`
  mutation UPDATE_PARAMETER_VALUES($parameters: [ParameterInput!]!) {
    updateParameterValues(parameters: $parameters) {
      ...ParameterInfo
    }
  }
  ${PARAMETER_INFO_FRAGEMENT}
`;
