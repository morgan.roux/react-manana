import gql from 'graphql-tag';

export const PARAMETER_VALUE_INFO_FRAGEMENT = gql`
  fragment ParameterValueInfo on ParameterValue {
    id
    value
  }
`;

export const PARAMETER_GROUP_INFO_FRAGEMENT = gql`
  fragment ParameterGroupInfo on ParameterGroupe {
    id
    code
    nom
  }
`;

export const PARAMETER_TYPE_INFO_FRAGEMENT = gql`
  fragment ParameterTypeInfo on ParameterType {
    id
    code
    nom
  }
`;

export const PARAMETER_INFO_FRAGEMENT = gql`
  fragment ParameterInfo on Parameter {
    id
    code
    name
    defaultValue
    dateCreation
    dateModification
    category
    options {
      label
      value
    }
    parameterGroupe {
      ...ParameterGroupInfo
    }
    parameterType {
      ...ParameterTypeInfo
    }
    value {
      ...ParameterValueInfo
    }
  }
  ${PARAMETER_GROUP_INFO_FRAGEMENT}
  ${PARAMETER_TYPE_INFO_FRAGEMENT}
  ${PARAMETER_VALUE_INFO_FRAGEMENT}
`;
