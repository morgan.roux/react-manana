export interface Projet {
  id: string;
  name: string;
  archived: boolean;
  typeProject: string;
  children: Projet[] | null;
  projectActions: any;
}
