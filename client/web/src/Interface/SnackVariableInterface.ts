export default interface SnackVariableInterface {
  message?: string;
  type?: 'ERROR' | 'SUCCESS' | 'WARNING' | 'INFO';
  isOpen: boolean;
}
