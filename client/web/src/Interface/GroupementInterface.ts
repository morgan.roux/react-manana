export default interface IGroupement {
  id?: string | null;
  nom: string;
  adresse1: string;
  adresse2?: string | null;
  cp: string;
  ville: string;
  pays?: string | null;
  telBureau: string;
  telMobile?: string | null;
  mail: string;
  site?: string | null;
  commentaire?: string | null;
  nomResponsable: string;
  prenomResponsable: string;
  dateSortie?: any | null;
  sortie?: number | null;
  isReference?: boolean;
}
