import { 
    SsoType, 
    MappingVariableInput, 
    Sendingtype,
    SamlInput,
    AesInput,
    Md5Input,
    OtherInput,
    TokenInput,
    TokenFtpInput,
    UpdateMappingVariableInput,
    FichierInput
 } from '../types/graphql-global-types'
export interface ISsoApplicationInput {
    id?: string;
    nom?: string;
    url?: string;
    icon?: FichierInput;
    ssoType?: SsoType;
    mappings?: (MappingVariableInput | null)[] | (UpdateMappingVariableInput | null)[];
    idGroupement?: string;
    sendingType?: Sendingtype;
    saml?: SamlInput ;
    aes?: AesInput;
    md5?: Md5Input;
    other?: OtherInput;
    token?: TokenInput;
    toknFtp?: TokenFtpInput;
  }
  
  // export interface ISsoApplicationUpdateInput {
  //   id?: string;
  //   nom?: string;
  //   url?: string;
  //   iconUrl?: string;
  //   ssoType?: SsoType;
  //   mappings?: (MappingVariableInput | null)[];
  //   idGroupement?: string;
  //   sendingType?: Sendingtype;
  //   saml?: SamlInput ;
  //   aes?: AesInput;
  //   md5?: Md5Input;
  //   other?: OtherInput;
  //   token?: TokenInput;
  //   toknFtp?: TokenFtpInput;
  // }
  