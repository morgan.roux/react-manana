export interface DATA {
  id: number;
  nom: string;
  nbArticle: number;
  sousDatas?: DATA[];
}

export interface FILTER_PARAMETER {
  parameter: {
    idFamilles: number[];
    idLaboratoires: number[];
    idGammeCommercials: number[];
    laboratoires: any[];
    familles: any[];
    gammes: any[];
    searchText: string;
  };
}
