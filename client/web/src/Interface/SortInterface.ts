interface SortInterface {
  sort: {
    sortItem: {
      label: string;
      name: string;
      direction: string;
      active?: boolean;
      __typename: string;
    };
  };
}

export default SortInterface;
