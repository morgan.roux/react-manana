import {LABORATOIRE_laboratoire} from '../graphql/Laboratoire/types/LABORATOIRE';

export default interface Laboratoires {
    laboratoire : LABORATOIRE_laboratoire;
    onClick : (laboratoire :LABORATOIRE_laboratoire ) => void;
}