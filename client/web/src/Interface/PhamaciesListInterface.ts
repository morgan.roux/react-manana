import { PharmacieMinimInfo } from '../graphql/Pharmacie/types/PharmacieMinimInfo';

export default interface IPharmaciesListInterface {
  pharmacies: Array<PharmacieMinimInfo | null> | null;
}
