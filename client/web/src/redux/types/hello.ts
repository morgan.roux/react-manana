export const SET_MSG = 'SET_MSG';

export interface HelloState {
  msg: string;
}

export interface SetMsgAction {
  type: typeof SET_MSG;
  payload: string;
}

export type HelloActionTypes = SetMsgAction;

// like
// export type ChatActionTypes = SendMessageAction | DeleteMessageAction
// https://redux.js.org/recipes/usage-with-typescript
