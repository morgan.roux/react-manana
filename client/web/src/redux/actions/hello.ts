import { HelloActionTypes, SET_MSG } from '../types/hello';

export function setMsg(newMsg: string): HelloActionTypes {
  return {
    type: SET_MSG,
    payload: newMsg,
  };
}
