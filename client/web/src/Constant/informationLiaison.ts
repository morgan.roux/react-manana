
export const INFO_STATUS = {
  NON_LUES: 'Non lues',
  LUES: 'Lues',
  CLOTURE: 'Clôturée',
  EN_CHARGE: 'Pris en charge',
};
