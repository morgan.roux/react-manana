import moment from 'moment';

export const START_DAY_OF_CURRENT_MONTH = moment()
  .startOf('month')
  .format('YYYY-MM-DD');
export const END_DAY_OF_CURRENT_MONTH = moment()
  .endOf('month')
  .format('YYYY-MM-DD');

export const TODAY = moment().format('YYYY-MM-DD');
export const MINYEAR = 1920;
