import Actualites from '../assets/img/service_illustration/actualites.png';
import CatalogueProduit from '../assets/img/service_illustration/mon_catalogue_produit.png';
import DemarcheProduit from '../assets/img/service_illustration/demarche_qualite2.png';
import EspaceDocumentaire from '../assets/img/service_illustration/ged.png';
import Evenement from '../assets/img/service_illustration/evenements.png';
import Formation from '../assets/img/service_illustration/formations.png';
import SuiviCommande from '../assets/img/service_illustration/suivi_de_commandes.png';
import Laboratoire from '../assets/img/service_illustration/laboratoire.png';
import Newsletter from '../assets/img/service_illustration/newsletter.png';
import AchatGroupe from '../assets/img/service_illustration/achat_groupe.png';
import CahierLiaison from '../assets/img/service_illustration/cahier_liaison.png';
import Todo from '../assets/img/service_illustration/todo.png';
import GroupePharmacies from '../assets/img/service_illustration/groupe_pharmacies.png';
import GestionFeedback from '../assets/img/service_illustration/gestion_feedback.png';
import Messagerie from '../assets/img/service_illustration/messagerie.png';
import PartageIdees from '../assets/img/service_illustration/partage_idee_bonne_pratique.png';
import EspaceFacture from '../assets/img/service_illustration/espace_facture.png';

import OperationCommerciale from '../assets/img/service_illustration/operations_commerciales.png';
import PrestatairePartenaire from '../assets/img/service_illustration/prestataires_partenaires.png';
import ServicePharmacie from '../assets/img/service_illustration/services_aux_pharmacies.png';
import ReleveTemperatures from '../assets/img/service_illustration/Releve_temperature.png';
import { isMobile } from '../utils/Helpers';

export const SERVICE_ADMIN = 'ADMIN';
export const SERVICE_MARKETING = 'MARKET';
export const SERVICE_COMMUNICATION = 'COMM';

export const SERVICE_SALES = 'SALES';
export const SERVICE_QUALITE = 'QUALI';
export const SERVICE_LOGISTIQUE = 'LOGI';

export const SERVICE_FORMATION = 'FORMA';
export const SERVICE_AUTRE = 'AUTRE';

export const SERVICES_LIST = [
  {
    id: 1,
    groupe: 1,
    title: 'Actualités',
    url: '/actualites',
    src: Actualites,
    activationParameters: '0301',
    code: 'ACTUALITE',
  },
  {
    id: 2,
    groupe: 1,
    title: 'Opérations commerciales',
    url: '/operations-commerciales',
    src: OperationCommerciale,
    activationParameters: '0302',
  },
  {
    id: 3,
    groupe: 1,
    title: 'Catalogues des produits',
    url: '/catalogue-produits/card',
    src: CatalogueProduit,
    activationParameters: '0303',
  },
  {
    id: 4,
    groupe: 1,
    title: 'Suivi de commandes',
    url: '/suivi-commandes',
    src: SuiviCommande,
    activationParameters: '0304',
  },
  {
    id: 5,
    groupe: 2,
    title: 'Laboratoires',
    url: '/laboratoires',
    src: Laboratoire,
    activationParameters: '0305',
  },
  {
    id: 6,
    groupe: 2,
    title: 'Prestataires partenaires',
    url: '/prestataires-partenaires',
    src: PrestatairePartenaire,
    activationParameters: '0306',
  },
  {
    id: 7,
    groupe: 2,
    title: 'Services aux pharmacies',
    url: '/services-aux-pharmacies',
    src: ServicePharmacie,
    activationParameters: '0307',
  },
  {
    id: 8,
    groupe: 3,
    title: 'Evènements',
    url: '/evenements',
    src: Evenement,
    activationParameters: '0308',
  },
  {
    id: 9,
    groupe: 3,
    title: 'Formations',
    url: '/formations',
    src: Formation,
    activationParameters: '0309',
  },
  {
    id: 10,
    groupe: 3,
    title: 'Newsletters',
    url: '/newsletters',
    src: Newsletter,
    activationParameters: '0310',
  },

  {
    id: 11,
    groupe: 4,
    title: 'Achat Groupé',
    url: '/achat-groupe',
    src: AchatGroupe,
    activationParameters: '0311',
  },

  {
    id: 12,
    groupe: 4,
    title: 'Groupe de Pharmacies',
    url: '/groupe-pharmacies',
    src: GroupePharmacies,
    activationParameters: '0316',
  },

  {
    id: 13,
    groupe: 4,
    title: 'To-Do',
    url: '/todo/aujourdhui',
    src: Todo,
    activationParameters: '0207',
    statsKey: 'actionTodo',
  },

  {
    id: 14,
    groupe: 4,
    title: 'Messagerie',
    url: '/messagerie/inbox',
    src: Messagerie,
    activationParameters: '0317',
  },

  {
    id: 15,
    groupe: 4,
    title: 'Cahier de Liaison',
    url: '/intelligence-collective/cahier-de-liaison/recue',
    src: CahierLiaison,
    activationParameters: '0312',
    statsKey: 'liaison',
  },

  {
    id: 16,
    groupe: 3,
    title: 'Démarche qualité',
    url: isMobile() ? '/demarche-qualite/outils' : '/demarche-qualite',
    src: DemarcheProduit,
    activationParameters: '0313',
  },

  {
    id: 17,
    groupe: 3,
    title: 'Espace facturation',
    url: '/espace-facture',
    src: EspaceFacture,
    activationParameters: '0314',
  },

  {
    id: 10,
    groupe: 3,
    title: 'Espace documentaire',
    url: '/espace-documentaire',
    src: EspaceDocumentaire,
    activationParameters: '0314',
  },

  {
    id: 16,
    groupe: 4,
    title: 'Gestion du feedback',
    url: '/intelligence-collective/partage-des-informatons/choix-items',
    src: GestionFeedback,
    activationParameters: '0315',
  },

  {
    id: 20,
    groupe: 4,
    title: 'Partage des idées',
    url: '/partage-des-idees-ou-bonnes-pratiques',
    src: PartageIdees,
    activationParameters: '0206',
  },

  {
    id: 21,
    groupe: 4,
    title: 'Relevé des températures',
    url: '/releve-des-temperatures',
    src: ReleveTemperatures,
    activationParameters: '0825',
  },
];
