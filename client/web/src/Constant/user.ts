import { Sexe } from '../types/graphql-global-types';

export const CIVILITE_LIST = [
  { id: 'M.', value: 'Monsieur' },
  { id: 'Mme', value: 'Madame' },
  { id: 'Mlle', value: 'Mademoiselle' },
];

export const SEXE_LIST = [{ id: Sexe.M, value: 'Homme' }, { id: Sexe.F, value: 'Femme' }];
