import React, { FC, useState } from 'react';
import { ThemeProvider as MuiThemeProvider, Theme, createMuiTheme } from '@material-ui/core/styles';
// import useLocaleStorage from './../hooks/useLocaleStorage';
import ThemeContext from './ThemeContext';
import { availableThemes, ThemeName } from './config';
// import { CODE_THEME } from '../Constant/parameter';
// import { useValueParameter } from '../utils/getValueParameter';
import { useQuery } from '@apollo/react-hooks';
import { GET_ME } from '../graphql/Authentication/query';
import { ME } from '../graphql/Authentication/types/ME';
// import useMediaQuery from '@material-ui/core/useMediaQuery';

const ThemeProvider: FC<any> = (props: any) => {
  const { children } = props;
  const [themeName, changeTheme] = useState<ThemeName>('onyx'); // useLocaleStorage<ThemeName>('theme', 'onyx');
  // const userTheme = useValueParameter(CODE_THEME);
  const me = useQuery<ME>(GET_ME);
  const userTheme: any = me && me.data && me.data.me && me.data.me.theme;

  // Change theme
  React.useLayoutEffect(() => {
    if (userTheme) {
      changeTheme(userTheme);
    }
  }, [userTheme]);

  const theme: Theme = React.useMemo(() => {
    const nextTheme = createMuiTheme(availableThemes[themeName]);
    return nextTheme;
  }, [themeName]);

  React.useLayoutEffect(() => {
    // Expose the theme as a global variable so people can play with it.
    if ((process as any).browser) {
      (window as any).theme = theme;
    }
  }, [theme]);

  return (
    <MuiThemeProvider theme={theme}>
      <ThemeContext.Provider value={[themeName, changeTheme]}> {children} </ThemeContext.Provider>
    </MuiThemeProvider>
  );
};

export default ThemeProvider;
