import * as createTypography from '@material-ui/core/styles/createTypography';
declare module '@material-ui/core/styles/createTypography' {
  interface TypographyOptions {
    small?: TypographyOptions;
    medium?: TypographyOptions;
    big?: TypographyOptions;
  }
  interface Typography {
    small: Typography;
    medium: Typography;
    big: Typography;
  }
}

declare module '@material-ui/core/styles/createBreakpoints' {
  interface BreakpointOverrides {
    xxs: true;
    xs: true;
    sm: true;
    md: true;
    lg: true;
    xl: true;
  }
}
