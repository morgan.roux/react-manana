import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
  createStyles({
    copyright: {
      fontSize: 11,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      textAlign: 'center',
      '& p': {
        marginBottom: 0,
      },
    },
  }),
);

export default useStyles;
