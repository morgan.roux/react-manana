import {  useQuery } from '@apollo/react-hooks';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { GET_GED_CATEGORIES_ANNEE_MOIS } from '../../../federation/ged/document/query'
import { GET_GED_CATEGORIES_ANNEE_MOIS as GET_GED_CATEGORIES_ANNEE_MOIS_TYPE, GET_GED_CATEGORIES_ANNEE_MOISVariables } from '../../../federation/ged/document/types/GET_GED_CATEGORIES_ANNEE_MOIS'
import { QueryResult } from 'react-apollo';


const useGedCategoriesAnneeMois = (
    variables : GET_GED_CATEGORIES_ANNEE_MOISVariables,
    skip: boolean = false
): QueryResult<GET_GED_CATEGORIES_ANNEE_MOIS_TYPE, GET_GED_CATEGORIES_ANNEE_MOISVariables> => {

    return useQuery<GET_GED_CATEGORIES_ANNEE_MOIS_TYPE, GET_GED_CATEGORIES_ANNEE_MOISVariables>(GET_GED_CATEGORIES_ANNEE_MOIS,{
        client:FEDERATION_CLIENT,
        variables,
        skip
    })
};

export default useGedCategoriesAnneeMois;
