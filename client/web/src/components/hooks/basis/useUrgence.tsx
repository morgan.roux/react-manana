import { useQuery } from '@apollo/react-hooks';
import { QueryResult } from 'react-apollo';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { GET_URGENCES } from './../../../federation/basis/query'
import { GET_URGENCES as GET_URGENCES_TYPE, GET_URGENCESVariables } from './../../../federation/basis/types/GET_URGENCES'

const useImportance = (
): QueryResult<GET_URGENCES_TYPE, GET_URGENCESVariables> => {
    return useQuery<GET_URGENCES_TYPE, GET_URGENCESVariables>(GET_URGENCES, {
        client: FEDERATION_CLIENT
    });
    ;
};

export default useImportance;
