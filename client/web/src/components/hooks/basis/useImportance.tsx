import { useQuery } from '@apollo/react-hooks';
import { QueryResult } from 'react-apollo';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { GET_IMPORTANCES } from './../../../federation/basis/query'
import { IMPORTANCES as GET_IMPORTANCES_TYPE, IMPORTANCESVariables } from './../../../federation/basis/types/IMPORTANCES'

const useImportance = (
): QueryResult<GET_IMPORTANCES_TYPE, IMPORTANCESVariables> => {

    return useQuery<GET_IMPORTANCES_TYPE, IMPORTANCESVariables>(GET_IMPORTANCES, {
        client: FEDERATION_CLIENT
    });
    ;
};

export default useImportance;
