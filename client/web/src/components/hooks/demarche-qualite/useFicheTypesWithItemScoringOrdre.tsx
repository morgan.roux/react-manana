import { useQuery } from '@apollo/react-hooks';
import { QueryResult } from 'react-apollo';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE } from '../../../federation/demarche-qualite/fiche-type/query'
import { GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE as GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE_TYPE, GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDREVariables } from '../../../federation/demarche-qualite/fiche-type/types/GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE'

const useFicheTypesWithItemScoringOrdre = (
    idPharmacie?: string
): QueryResult<GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE_TYPE, GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDREVariables> => {

    return useQuery<GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE_TYPE, GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDREVariables>(GET_FICHE_TYPES_WITH_ITEM_SCORING_ORDRE, {
        client: FEDERATION_CLIENT,
        variables: {
            idPharmacie
        }
    });
    ;
};

export default useFicheTypesWithItemScoringOrdre;
