import { MutationTuple, useMutation } from '@apollo/react-hooks';
import { FEDERATION_CLIENT } from '../../Dashboard/DemarcheQualite/apolloClientFederation';
import { CREATE_ONE_ITEM_SCORING_PERSONNALISATION } from '../../../federation/tools/itemScoring/mutation'
import { CREATE_ONE_ITEM_SCORING_PERSONNALISATION as CREATE_ONE_ITEM_SCORING_PERSONNALISATION_TYPE, CREATE_ONE_ITEM_SCORING_PERSONNALISATIONVariables } from '../../../federation/tools/itemScoring/types/CREATE_ONE_ITEM_SCORING_PERSONNALISATION'


const useCreateOneItemScoringPersonnalisation = (
): MutationTuple<CREATE_ONE_ITEM_SCORING_PERSONNALISATION_TYPE, CREATE_ONE_ITEM_SCORING_PERSONNALISATIONVariables> => {

    return useMutation<CREATE_ONE_ITEM_SCORING_PERSONNALISATION_TYPE, CREATE_ONE_ITEM_SCORING_PERSONNALISATIONVariables>(CREATE_ONE_ITEM_SCORING_PERSONNALISATION, {
        client: FEDERATION_CLIENT
    });
    ;
};

export default useCreateOneItemScoringPersonnalisation;
