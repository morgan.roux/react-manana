import React, { FC, useState, ChangeEvent } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import TableHead from '@material-ui/core/TableHead';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import Checkbox from '@material-ui/core/Checkbox';
import { useApolloClient } from '@apollo/react-hooks';
import { Theme, lighten } from '@material-ui/core/styles';
import { differenceBy, intersectionBy, uniqBy } from 'lodash';
import { Column } from '../../../../../Dashboard/Content/Interface';
import { SortBy } from '../../Users';
import _ from 'lodash';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  root: {
    background: lighten(theme.palette.primary.main, 0.1),
    '& .MuiTableSortLabel-root.MuiTableSortLabel-active, & .MuiTableSortLabel-icon': {
      color: `${theme.palette.common.white} !important`,
    },
    '& .MuiTableCell-root': {
      padding: '0px 16px',
    },
    '& .MuiTableCell-head': {
      lineHeight: '1rem',
      height: 40,
    },
  },
  loader: {
    color: theme.palette.common.white,
    marginLeft: 12,
  },
  label: {
    color: theme.palette.common.white,
    fontSize: 12,
    '&:hover': {
      color: theme.palette.common.white,
      opacity: '0.6',
    },
  },
});

interface TableHeadProps {
  columns: Column[] | undefined;
  sortBy: SortBy;
  setSortBy: (value: any) => void;
  allUsers?: any[];
  userIdsSelected?: string[];
  setUserIdsSelected?: (value: any) => void;
  setUsersSelected?: (value: any) => void;
  usersSelected?: any[];
}

const EnhancedTableHead: FC<TableHeadProps & WithStyles> = ({
  classes,
  columns,
  sortBy,
  setSortBy,
  allUsers,
  userIdsSelected,
  setUserIdsSelected,
  setUsersSelected,
  usersSelected,
}) => {
  const handleNewSorting = (newColumn: any) => {
    setSortBy({
      column: newColumn,
      order: sortBy.order === 'desc' ? 'asc' : 'desc',
    });
  };
  const idsSelected = userIdsSelected && userIdsSelected.length ? userIdsSelected : [];
  const allUsersId = allUsers && allUsers.length ? allUsers.map(user => user.id) : [];
  const idsAll = allUsersId && allUsersId.length ? allUsersId : [];

  const uSelected = usersSelected && usersSelected.length ? usersSelected : [];
  const users = allUsers && allUsers.length ? allUsers : [];
  const isCheckedAll = _.difference(allUsersId, idsSelected).length === 0;

  const handleClick = () => {
    if (isCheckedAll) {
      if (setUserIdsSelected)
        setUserIdsSelected(uSelected.filter(user => user && !idsAll.includes(user.id)));
      if (setUsersSelected)
        setUsersSelected([
          ...uSelected,
          ...users.filter(user => user && !idsSelected.includes(user.id)),
        ]);
    } else if (!isCheckedAll) {
      if (setUserIdsSelected)
        setUserIdsSelected([...idsSelected, ...idsAll.filter(id => !idsSelected.includes(id))]);
      if (setUsersSelected)
        setUsersSelected([
          ...uSelected,
          ...users.filter(user => user && !idsSelected.includes(user.id)),
        ]);
    }
  };

  return (
    <TableHead className={classes.root}>
      <TableRow>
        <TableCell style={{ padding: '0px 4px' }}>
          <Checkbox
            indeterminate={false}
            inputProps={{ 'aria-label': 'Select all desserts' }}
            style={{ color: '#ffffff' }}
            checked={isCheckedAll}
            onClick={handleClick}
          />
        </TableCell>
        {columns
          ? (columns || []).map((column, index) => (
              <TableCell
                key={`column-${index}`}
                align={column.centered ? 'center' : 'left'}
                sortDirection={sortBy.column === column.name ? sortBy.order : false}
              >
                <TableSortLabel
                  active={
                    sortBy.column === column.name ? (column.name === '' ? false : true) : false
                  }
                  hideSortIcon={column.name === '' ? true : false}
                  direction={sortBy && sortBy.column === column.name ? sortBy.order : undefined}
                  onClick={() => (column.name ? handleNewSorting(column.name) : null)}
                  className={classes.label}
                >
                  {column.label}
                </TableSortLabel>
              </TableCell>
            ))
          : null}
      </TableRow>
    </TableHead>
  );
};

export default withStyles(styles)(EnhancedTableHead);
