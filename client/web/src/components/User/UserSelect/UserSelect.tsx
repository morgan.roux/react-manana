import React, { useRef, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { useStyles } from './styles';
import { Filter } from './Filter';
import { Users } from './Users';
import { columnsFilter } from './columns';
import { getGroupement, getUser } from '../../../services/LocalStorage';
import { ME_me } from '../../../graphql/Authentication/types/ME';
import { GET_CURRENT_PHARMACIE } from '../../../graphql/Pharmacie/local';
import ICurrentPharmacieInterface from '../../../Interface/CurrentPharmacieInterface';
import { useLazyQuery, useQuery } from '@apollo/react-hooks';
import { SEARCH_USERSVariables } from '../../../graphql/User/types/SEARCH_USERS';
import _ from 'lodash';
import { updateQuerySearch } from './query';
import { hiddenFilter } from '../../Main/Content/Messagerie/SideBar/filter';
import {
  PARAMETERS_GROUPES_CATEGORIES,
  PARAMETERS_GROUPES_CATEGORIESVariables,
} from '../../../graphql/Parametre/types/PARAMETERS_GROUPES_CATEGORIES';
import { GET_PARAMETERS_GROUPES_CATEGORIES } from '../../../graphql/Parametre/query';
import { ParamCategory } from '../../../types/graphql-global-types';
import { Box, Checkbox, CssBaseline, FormControlLabel, Hidden } from '@material-ui/core';
import { DO_GET_SEARCH_USER_IDS } from '../../../graphql/User/query';
import { GET_SEARCH_USER_IDSVariables } from '../../../graphql/User/types/GET_SEARCH_USER_IDS';
import { Add } from '@material-ui/icons';
interface MainContentProps {
  data?: any[];
  setFiltersSelected?: (value: any) => void;
  filtersSelected?: string[];
  setUserIdsSelected?: (value: any) => void;
  userIdsSelected?: string[];
  setUsersSelected?: (value: any) => void;
  usersSelected?: any[];
  disableAllFilter?: boolean;
  activeAllContact?: boolean;
  clientContact?: boolean;
}

const UserSelect: React.FC<RouteComponentProps & MainContentProps> = ({
  data,
  filtersSelected,
  setFiltersSelected,
  setUserIdsSelected,
  userIdsSelected,
  setUsersSelected,
  usersSelected,
  disableAllFilter,
  activeAllContact,
  clientContact,
}) => {
  const classes = useStyles({});

  const [selected, setSelected] = useState<string[]>(
    filtersSelected && filtersSelected.length ? filtersSelected : ['ALL'],
  );
  const [textSearch, setTextSearch] = useState<string>('');

  const groupement = getGroupement();
  const idGroupement = (groupement && groupement.id) || '';
  const currentUser: ME_me = getUser();
  const defaultMustNot = [{ term: { _id: currentUser && currentUser.id } }];
  const defaultShould = [{ term: { idGroupement } }];
  const [must, setMust] = React.useState<any[]>([]);
  const [mustNot, setMustNot] = React.useState<any[]>(defaultMustNot);
  const [should, setShould] = React.useState<any[]>(defaultShould);
  const [filterColumns, setFilterColumns] = React.useState<any[]>(columnsFilter());
  const [checkedContactClient, setCheckedContactClient] = React.useState<boolean>(false);
  const [openAddClient, setOpenAddClient] = React.useState<boolean>(false);

  const handleOpenAddCLient = () => {
    setOpenAddClient(prev => !prev);
  };

  const myPharmacie = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);

  const [getParameters, { data: parameters }] = useLazyQuery<
    PARAMETERS_GROUPES_CATEGORIES,
    PARAMETERS_GROUPES_CATEGORIESVariables
  >(GET_PARAMETERS_GROUPES_CATEGORIES, {
    variables: {
      categories: [ParamCategory.GROUPEMENT, ParamCategory.USER],
      groupes: ['MESSAGERIE'],
    },
  });

  const [totalUsers, results] = useLazyQuery<any, GET_SEARCH_USER_IDSVariables>(
    DO_GET_SEARCH_USER_IDS,
    {
      fetchPolicy: 'cache-and-network',
    },
  );

  const sUsersVar: SEARCH_USERSVariables = {
    query: {
      bool: {
        must_not: mustNot,
        should: should,
        must: [
          ...must,
          {
            term: {
              idGroupement,
            },
          },
        ],
        minimum_should_match: 1,
      },
    },
  };

  const queryString = textSearch
    ? [
        {
          query_string: {
            query:
              textSearch.startsWith('*') || textSearch.endsWith('*')
                ? textSearch
                : `*${textSearch}*`,
            analyze_wildcard: true,
          },
        },
      ]
    : [];

  React.useEffect(() => {
    getParameters();
  }, []);

  React.useEffect(() => {
    totalUsers({
      variables: {
        query: {
          query: {
            bool: {
              must_not: mustNot,
              should: should,
              must: [
                ...must.filter(term => !term.query_string),
                {
                  term: {
                    idGroupement,
                  },
                },
              ],
              minimum_should_match: 1,
            },
          },
        },
      },
    });
  }, [mustNot, should, must]);

  React.useEffect(() => {
    if (selected && selected.length) {
      updateQuerySearch({
        currentUser,
        defaultMustNot,
        myPharmacie,
        queryString,
        selected,
        setFiltersSelected,
        setMust,
        setMustNot,
      });
    }
  }, [selected, textSearch]);

  const disableFilter = (code: string) => {
    return parameters &&
      parameters.parametersGroupesCategories &&
      parameters.parametersGroupesCategories.length
      ? hiddenFilter(
          (currentUser.userTitulaire && currentUser.userTitulaire.id) ||
            (currentUser.userPpersonnel && currentUser.userPpersonnel.id)
            ? true
            : false,
          code,
          parameters && (parameters.parametersGroupesCategories as any),
        )
      : true;
  };

  React.useEffect(() => {
    if (
      parameters &&
      parameters.parametersGroupesCategories &&
      parameters.parametersGroupesCategories.length &&
      currentUser
    ) {
      setFilterColumns(
        parameters &&
          parameters.parametersGroupesCategories &&
          parameters.parametersGroupesCategories.length &&
          currentUser
          ? columnsFilter(disableFilter, disableAllFilter, activeAllContact)
          : columnsFilter(),
      );
    }
  }, [parameters]);

  const handleChecked = (code: string) => {
    console.log('handleChecked : ', code);

    if (code === 'PHARMACIE') {
      setSelected(prev =>
        prev.includes(code)
          ? [
              ...prev.filter(
                data =>
                  !['MY_PHARMACIE', 'MY_REGION', 'OTHER_PHARMACIE', 'PHARMACIE'].includes(data),
              ),
              'ALL',
            ]
          : ['MY_PHARMACIE', 'MY_REGION', 'OTHER_PHARMACIE', code],
      );
    }

    if (['MY_PHARMACIE', 'MY_REGION', 'OTHER_PHARMACIE'].includes(code)) {
      setSelected(prev =>
        prev.every(data => code === data || data === 'PHARMACIE') ? prev : ['PHARMACIE', code],
      );
    }

    setSelected(prev =>
      code === 'ALL' && prev.includes(code)
        ? prev
        : prev.includes(code)
        ? [...prev.filter(data => code !== data), 'ALL']
        : [code],
    );
  };

  const handleAddContactClient = () => {
    setOpenAddClient(true);
  };

  const handleChangeContactClientCheckbox = (__e: any, checked: boolean) => {
    setCheckedContactClient(checked);
    setSelected([]);
    if (!checked) {
      setSelected(['ALL']);
    } else {
      setSelected(['CLT']);
    }
  };

  const handleAddClientFormUserComp = () => {
    handleOpenAddCLient();
  };

  return (
    <Box className={classes.espace}>
      <CssBaseline />
      <Hidden mdDown={true} implementation="css">
        <Box className={classes.largeur}>
          <Filter filterColumns={filterColumns} selected={selected} handleChecked={handleChecked} />
          {clientContact && (
            <Box width="100%" marginTop={5}>
              <h3>Autres contacts</h3>
              <Box className={classes.contactUserFilerContainer}>
                <Box>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={checkedContactClient}
                        onChange={handleChangeContactClientCheckbox}
                        color="primary"
                      />
                    }
                    label="Clients"
                  />
                </Box>
                {checkedContactClient && (
                  <Box marginRight={1} onClick={handleAddContactClient}>
                    <Add />
                  </Box>
                )}
              </Box>
            </Box>
          )}
        </Box>
      </Hidden>
      <Box className={classes.mainTableUser}>
        <Users
          onAddClient={handleAddClientFormUserComp}
          modalAddClient={{
            open: openAddClient,
            setOpen: handleOpenAddCLient,
          }}
          query={sUsersVar}
          textSearch={textSearch}
          setTextSearch={setTextSearch}
          userIdsSelected={userIdsSelected}
          setUserIdsSelected={setUserIdsSelected}
          usersSelected={usersSelected}
          setUsersSelected={setUsersSelected}
          handleChecked={handleChecked}
          allUsers={
            results &&
            results.data &&
            results.data.search &&
            results.data.search.data &&
            results.data.search.data.length &&
            results.data.search.data.map((user: any) => user && user.id).filter(item => item).length
              ? results.data.search.data.map((user: any) => user)
              : []
          }
        />
      </Box>
    </Box>
  );
};

export default withRouter(UserSelect);
