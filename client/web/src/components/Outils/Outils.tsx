import React, { ReactNode, FC } from 'react';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import useStyles from './styles';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CommandeOraleIcon from '@material-ui/icons/ShoppingBasket';
import SuiviAppelIcon from '@material-ui/icons/ContactPhone';
import TraitementAutomatiqueIcon from '@material-ui/icons/SettingsApplications';
import IconButton from '@material-ui/core/IconButton';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Draggable from 'react-draggable';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';

import { RouteComponentProps, withRouter } from 'react-router';
import { useValueParameterByView } from './../../utils/getValueParameter';
import { isMobile } from './../../utils/Helpers';
import { Dashboard } from '@material-ui/icons';

interface OutilDigital {
  icon: ReactNode;
  name: string;
  path: string;
  view: boolean;
}

interface DraggableData {
  node: HTMLElement;
  // lastX + deltaX === x
  x: number;
  y: number;
  deltaX: number;
  deltaY: number;
  lastX: number;
  lastY: number;
}

export interface ResponseMessage {
  type: string;
  message: string;
}

const RecipeReviewCard: FC<RouteComponentProps> = ({ history, location: { pathname } }) => {
  const classes = useStyles({});
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const handleDrag = (e: Event, data: DraggableData) => {
    if (data.x >= 20) {
      console.log('stop');
    }
  };

  const enableCommandeOrale = useValueParameterByView('0821', '0721', isMobile());
  const enableMesTraitementsAutomatique = useValueParameterByView('0822', '0722', isMobile());
  const enableSuiviAppel = useValueParameterByView('0824', '0723', isMobile());
  const enableDashboard = useValueParameterByView('0210', '0745', isMobile());

  const MES_OUTILS: OutilDigital[] = [
    {
      icon: <CommandeOraleIcon />,
      name: 'La gestion des commandes orales',
      path: '/outils-digitaux/commandes-orales',
      view: enableCommandeOrale,
    },
    {
      icon: <TraitementAutomatiqueIcon />,
      name: 'Mes Tâches Récurrentes',
      path: '/outils-digitaux/traitements-automatiques',
      view: enableMesTraitementsAutomatique,
    },
    {
      icon: <SuiviAppelIcon />,
      name: 'Suivi des appels',
      path: '/outils-digitaux/suivi-appels',
      view: enableSuiviAppel,
    },
    {
      icon: <Dashboard />,
      name: "Mon Dashboard d'Activité",
      path: '/dashboard',
      view: enableDashboard,
    },
  ].filter(outil => outil.view);

  if (MES_OUTILS.length <= 0 || MES_OUTILS.some(({ path }) => path === pathname)) {
    return null;
  }

  return (
    <div>
      <Draggable
        axis="x"
        handle="#draggable-content"
        onDrag={(e: any, data: any) => {
          return handleDrag(e, data);
        }}
      >
        <Card className={classes.card}>
          <CardHeader
            id="draggable-content"
            title={<div className={classes.title}>Autres Outils</div>}
            className={classes.header}
            action={
              <CardActions disableSpacing={true}>
                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expanded,
                  })}
                  onClick={handleExpandClick}
                  aria-expanded={expanded}
                  size="small"
                >
                  <ExpandMoreIcon className={classes.expandIcon} />
                </IconButton>
              </CardActions>
            }
          />
          <Collapse in={expanded} timeout="auto">
            <CardContent className={classes.body}>
              <Typography variant="body2" color="textSecondary" component="div">
                <List component="nav" className={classes.root}>
                  {MES_OUTILS.map((outil, index) => {
                    return (
                      <ListItem
                        button
                        key={`list_${index}`}
                        className={classes.cursor}
                        onClick={() => history.push(outil.path)}
                      >
                        <Avatar>{outil.icon}</Avatar>
                        <ListItemText className={classes.appname} primary={outil.name} />
                      </ListItem>
                    );
                  })}
                </List>
              </Typography>
            </CardContent>
          </Collapse>
        </Card>
      </Draggable>
    </div>
  );
};

export default withRouter(RecipeReviewCard);
