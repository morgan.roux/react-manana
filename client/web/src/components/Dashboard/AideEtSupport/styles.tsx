import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    pilotageRoot: {
      width: '100%',
      maxWidth: '1400px',
      margin: '0 auto',
      height: 'calc(100vh - 86px)',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '@media (max-width: 768px)': {
        overflowY: 'auto',
        paddingTop: 86,
      },
    },
    title: {
      color: '#616161',
      fontWeight: 'bold',
      fontSize: 25,
    },
    subTitle: {
      marginTop: 15,
      color: '#9E9E9E',
      fontWeight: 'normal',
      fontSize: 14,
    },
    choiceContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginTop: 50,
      width: '100%',
      padding: '0 24px',
      '@media (max-width: 768px)': {
        justifyContent: 'center',
        flexWrap: 'wrap',
      },
    },
    choice: {
      border: '1px solid #9E9E9E',
      borderRadius: 16,
      width: '28%',
      display: 'flex',
      margin: '14px',
      flexDirection: 'column',
      alignItems: 'center',
      cursor: 'pointer',
      '&:hover': {
        opacity: 0.9,
      },

      '@media (max-width: 1300px)': {
        width: '31%',
        padding: '0 20px',
      },
      '@media (max-width: 1100px)': {
        '& img': {
          maxWidth: 188,
          maxHeight: 184,
        },
        '& p': {
          fontSize: 16,
        },
      },
      '@media (max-width: 768px)': {
        margin: '12px 10',
        width: '32%',
        minWidth: 195,
        '& img': {
          maxWidth: 144,
          maxHeight: 140,
        },
        '& p': {
          fontSize: 14,
        },
      },
    },
    choiceImg: {
      width: '100%',
      maxWidth: '254px',
      height: '100%',
      maxHeight: '251px',
      padding: '12px 0',
    },
    choiceText: {
      fontSize: 20,
      fontWeight: 'normal',
      color: theme.palette.common.black,
      maxWidth: 250,
      textAlign: 'center',
      padding: '0 0 14px',
      '& span': {
        color: theme.palette.secondary.main,
        fontWeight: 'bold',
      },
    },
    centerTextVertival: {
      minHeight: 58,
      lineHeight: '58px',
      '@media (max-width: 1100px)': {
        minHeight: 62,
        lineHeight: '44px',
      },
      '@media (max-width: 768px)': {
        minHeight: 42,
        lineHeight: '42px',
      },
    },
  }),
);

export default useStyles;
