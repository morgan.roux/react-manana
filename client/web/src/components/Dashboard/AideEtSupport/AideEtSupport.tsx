import React, { FC } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import AideImage from '../../../assets/img/aide.png';
import AppelImage from '../../../assets/img/suivi_appel.png';
import ReclamationImage from '../../../assets/img/suivi_reclamation.png';
import { AIDE_SUPPORT_URL } from '../../../Constant/url';
import ChoicePage, { ChoicePageItem } from '../../Common/ChoicePage/ChoicePage';

interface IAideSupportprops {
  match: {
    params: {
      ticketType: string | undefined;
      type: string | undefined;
    };
  };
}

const AideSupport: FC<RouteComponentProps & IAideSupportprops> = ({ history: { push } }) => {
  const handleGotoAppel = () => push(`/db/aide-support/suivi-appel`);

  const handleGoToReclamation = () => push(`/db/aide-support/reclamation/pharmacie`);

  const handleGotoAide = () => push(`/db/${AIDE_SUPPORT_URL}/aides`);

  const title = 'Differents type de support';
  const items: ChoicePageItem[] = [
    { text: 'Suivi des Appels', img: AppelImage, onClick: handleGotoAppel },
    { text: 'Suivi des réclamations', img: ReclamationImage, onClick: handleGoToReclamation },
    { text: 'Aide', img: AideImage, onClick: handleGotoAide },
  ];

  return <ChoicePage title={title} items={items} />;
};

export default AideSupport;
