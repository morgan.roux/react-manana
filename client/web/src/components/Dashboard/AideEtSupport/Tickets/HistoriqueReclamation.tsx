import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { Typography } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { GET_PHARMACIE_WITH_MIN_INFO } from '../../../../graphql/Pharmacie';
import {
  PHARMACIE_WITH_MIN_INFO,
  PHARMACIE_WITH_MIN_INFOVariables,
} from '../../../../graphql/Pharmacie/types/PHARMACIE_WITH_MIN_INFO';
import { GET_USER_WITH_MIN_INFO } from '../../../../graphql/User';
import {
  USER_WITH_MIN_INFO,
  USER_WITH_MIN_INFOVariables,
} from '../../../../graphql/User/types/USER_WITH_MIN_INFO';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import CustomButton from '../../../Common/CustomButton';
import SubToolbar from '../../../Common/newCustomContent/SubToolbar';
import useStyles from './styles';
import TicketTable from './TicketTable';

interface IhistoriqueReclamationProps {
  onClickAddBtn: () => void;
}

const HistoriqueReclamation: React.FC<IhistoriqueReclamationProps & RouteComponentProps> = ({
  onClickAddBtn,
  history: { push },
  match: { params },
  location: { pathname },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();

  const { idType } = params as any;

  console.log('params :>> ', params);
  const isOnPharmacie = pathname.includes('/reclamation/pharmacie/');
  const isOnCollabo = pathname.includes('/reclamation/collaborateur/');
  const isOnAppelPharma = pathname.includes('/appel/pharmacie/');
  const isOnAppelCollabo = pathname.includes('/appel/collaborateur');
  const isOnReclamation = pathname.includes('reclamation');
  const onClickBack = () => {
    if (isOnPharmacie) {
      push(`/db/aide-support/reclamation/pharmacie/`);
    }
    if (isOnCollabo) {
      push(`/db/aide-support/reclamation/collaborateur/`);
    }
    if (isOnAppelCollabo) {
      push(`/db/aide-support/appel/collaborateur/`);
    }
    if (isOnAppelPharma) {
      push(`/db/aide-support/appel/pharmacie/`);
    }
  };

  const { data: pharmaData, loading: pharmaLoading } = useQuery<
    PHARMACIE_WITH_MIN_INFO,
    PHARMACIE_WITH_MIN_INFOVariables
  >(GET_PHARMACIE_WITH_MIN_INFO, {
    variables: { id: idType },
    skip: isOnCollabo,
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const { data: userData, loading: userLoading } = useQuery<
    USER_WITH_MIN_INFO,
    USER_WITH_MIN_INFOVariables
  >(GET_USER_WITH_MIN_INFO, {
    variables: { id: idType },
    skip: isOnPharmacie,
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const pharma = pharmaData && pharmaData.pharmacie;
  const user = userData && userData.user;

  const title = isOnPharmacie ? pharma && pharma.nom : isOnCollabo ? user && user.userName : '';

  return (
    <div className={classes.root}>
      {(pharmaLoading || userLoading) && <Backdrop />}
      <SubToolbar
        title={isOnReclamation ? 'Historique des réclamations' : 'Historique des appels'}
        dark={true}
        withBackBtn={true}
        onClickBack={onClickBack}
        backBtnIcon={<ArrowBack />}
      >
        {isOnReclamation ? (
          <CustomButton onClick={onClickAddBtn} color="secondary">
            ouvrir une réclamation
          </CustomButton>
        ) : (
          <CustomButton onClick={onClickAddBtn} color="secondary">
            ajouter un appel
          </CustomButton>
        )}
      </SubToolbar>
      <div className={classes.globalTabsContainer}>
        <Typography className={classes.title}>{title}</Typography>
      </div>
      <div className={classes.contentContainer}>
        <TicketTable />
      </div>
    </div>
  );
};

export default withRouter(HistoriqueReclamation);
