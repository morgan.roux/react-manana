export const searchUsersFilterBy = (
  destination: string,
  idLaboratoireCible: string | null | undefined,
  idPharmacieCible: string | null | undefined,
  idPrestataireServiceCible: string | null | undefined,
  idServiceCible: string | null | undefined,
): any[] => {
  switch (destination) {
    case 'groupement':
      if (idServiceCible) {
        return [{ term: { 'userPersonnel.service.id': idServiceCible } }];
      } else {
        return [];
      }
    case 'president':
      if (idPharmacieCible) {
        return [{ term: { 'pharmacie.id': idPharmacieCible } }];
      } else {
        return [];
      }
    case 'laboratoire':
      if (idLaboratoireCible) {
        return [{ term: { 'userLaboratoire.id': idLaboratoireCible } }];
      } else {
        return [];
      }
    case 'partenaire':
      if (idPrestataireServiceCible) {
        return [{ term: { 'userPartenaire.id': idPrestataireServiceCible } }];
      } else {
        return [];
      }
    default:
      return [];
  }
};
