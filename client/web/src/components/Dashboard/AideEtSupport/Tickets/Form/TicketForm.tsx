import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import {
  debounce,
  Divider,
  FormControlLabel,
  Radio,
  TextField,
} from '@material-ui/core';
import { AxiosResponse } from 'axios';
import classnames from 'classnames';
import { uniqBy } from 'lodash';
import React, {
  ChangeEvent,
  Dispatch,
  FC,
  MouseEvent,
  SetStateAction,
  useRef,
  useState,
} from 'react';
import { ME_me } from '../../../../../graphql/Authentication/types/ME';
import { DO_SEARCH_GROUPES_AMIS_WITH_MINIM_INFO } from '../../../../../graphql/GroupAmis';
import {
  SEARCH_GROUPES_AMIS_WITH_MINIM_INFO,
  SEARCH_GROUPES_AMIS_WITH_MINIM_INFOVariables,
} from '../../../../../graphql/GroupAmis/types/SEARCH_GROUPES_AMIS_WITH_MINIM_INFO';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../../graphql/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { GET_SMYLEYS_WITH_MIN_INFO } from '../../../../../graphql/Smyley';
import {
  SMYLEYS_WITH_MIN_INFO,
  SMYLEYS_WITH_MIN_INFOVariables,
  SMYLEYS_WITH_MIN_INFO_smyleys,
} from '../../../../../graphql/Smyley/types/SMYLEYS_WITH_MIN_INFO';
import { DO_CREATE_UPDATE_TICKET } from '../../../../../graphql/Ticket';
import {
  CREATE_UPDATE_TICKET,
  CREATE_UPDATE_TICKETVariables,
} from '../../../../../graphql/Ticket/types/CREATE_UPDATE_TICKET';
import { GET_TICKET_MOTIFS } from '../../../../../graphql/TicketMotif';
import { TICKET_MOTIFS } from '../../../../../graphql/TicketMotif/types/TICKET_MOTIFS';
import { getGroupement, getUser } from '../../../../../services/LocalStorage';
import { uploadToS3 } from '../../../../../services/S3';
import {
  FichierInput,
  TicketAppel,
  TicketType,
  TicketVisibilite,
} from '../../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../../utils/filenameFormater';
import { AWS_HOST } from '../../../../../utils/s3urls';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import Backdrop from '../../../../Common/Backdrop';
import CustomButton from '../../../../Common/CustomButton';
import { CustomModal } from '../../../../Common/CustomModal';
import CustomSelect from '../../../../Common/CustomSelect';
import CustomTextarea from '../../../../Common/CustomTextarea';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import Autocomplete from '../../../../Main/Content/Messagerie/SideBar/FormMessage/Autocomplete';
import useCommonStyles from '../../../../Main/Content/Messagerie/SideBar/FormMessage/styles';
import { TypeFichier } from '../../../../Main/Content/OperationCommerciale/CreateOC/Resume/useResume';
import AssignTaskUserModal from '../../../../Common/CustomSelectUser';
import useStyles from './styles';
import useTicketForm from './useTicketForm';
import Dropzone from '../../../../Common/Dropzone';
import {
  SEARCH_LABORATOIRE,
  SEARCH_LABORATOIREVariables,
} from '../../../../../graphql/Laboratoire/types/SEARCH_LABORATOIRE';
import { DO_SEARCH_LABORATOIRE, GET_LABORATOIRE } from '../../../../../graphql/Laboratoire/query';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import {
  SEARCH_CUSTOM_CONTENT_PARTENAIRE,
  SEARCH_CUSTOM_CONTENT_PARTENAIREVariables,
} from '../../../../../graphql/PartenairesServices/types/SEARCH_CUSTOM_CONTENT_PARTENAIRE';
import { GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE } from '../../../../../graphql/PartenairesServices/query';
import { MINIM_SERVICES } from '../../../../../graphql/Service/types/MINIM_SERVICES';
import { GET_MINIM_SERVICES } from '../../../../../graphql/Service';
import { initialState } from './useTicketForm';
import {
  LABORATOIRE,
  LABORATOIREVariables,
} from '../../../../../graphql/Laboratoire/types/LABORATOIRE';

interface InitialState {
  concernedColleaguesIds: any[];
  teamAll: boolean;
  serviceConcerned: string;
  concernedColleagueName: string;
}

interface TicketFormProps {
  open: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
  refetch: any;
  isAppelEntrant?: boolean;
  type?: any;
  idLaboratoire?: string;
}

const TicketForm: FC<TicketFormProps> = ({
  open: openModal,
  setOpen,
  refetch,
  type,
  isAppelEntrant,
  idLaboratoire,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();

  const client = useApolloClient();
  const grp = getGroupement();
  const idGrp = (grp && grp.id) || '';

  const currentUser: ME_me = getUser();
  const currentUserId = (currentUser && currentUser.id) || '';
  const currentUserName = (currentUser && currentUser.userName) || '';

  const { values, setValues, handleChange } = useTicketForm();

  const { selectedFiles, ...othersValues } = values;
  const {
    idOrganisation,
    commentaire,
    dateHeureSaisie,
    fichiers,
    priority,
    idMotif,
    origine,
    typeTicket,
    visibilite,
    nomInterlocuteur,
    telephoneInterlocuteur,
  } = othersValues;

  const origineList = [
    {
      id: 'INTERNE',
      nom: 'Interne',
    },
    {
      id: 'PATIENT',
      nom: 'Patient',
    },
    {
      id: 'GROUPE_AMIS',
      nom: "Groupe d'amis",
    },
    {
      id: 'GROUPEMENT',
      nom: 'Groupement',
    },
    {
      id: 'FOURNISSEUR',
      nom: 'Laboratoire',
    },
    {
      id: 'PRESTATAIRE',
      nom: 'Prestataire',
    },
  ];

  const priorityList = [
    { id: 1, nom: 'Normal' },
    { id: 2, nom: 'Important' },
    { id: 3, nom: 'Bloquant' },
  ];

  const [loading, setLoading] = useState(false);
  const [selectedDeclarant, setSelectedDeclarant] = useState<any[]>([currentUser]);
  const [openDeclarantModal, setOpenDeclarantModal] = useState<boolean>(false);
  const [declarantName, setDeclarantName] = useState<string>('Moi');
  const [openConcernedParticipantModal, setOpenConcernedParticipantModal] = useState<boolean>(
    false,
  );
  const [selectedCollegues, setSelectedCollegues] = useState<any[]>([]);
  const initialStateValue: InitialState = {
    concernedColleaguesIds: [],
    teamAll: true,
    serviceConcerned: '',
    concernedColleagueName: '',
  };

  const [state, setState] = useState(initialStateValue);
  const [isCheckedTeam, setIsCheckedTeam] = useState<boolean>(false);
  const [fichiersJoints, setFichiersJoints] = useState<File[]>([]);
  const [organisation, setOrganisation] = useState<any>(null);

  const laboType = ['laboratoire'];
  const partenaireType = ['partenaire'];
  const laboSortBy = [{ nomLabo: { order: 'asc' } }];
  const laboFilterBy = [{ term: { sortie: 0 } }];

  const groupeAmisType = ['groupeamis'];
  const groupeAmisFilterBy = [{ term: { isRemoved: false } }, { term: { 'groupement.id': idGrp } }];

  let uploadResult: AxiosResponse<any> | null = null;
  let files: FichierInput[] = [];

  const [currentSmyley, setCurrentSmyley] = useState<
    SMYLEYS_WITH_MIN_INFO_smyleys | null | undefined
  >(null);

  const [opacity, setOpacity] = useState<number>(0);

  const { data } = useQuery<LABORATOIRE, LABORATOIREVariables>(GET_LABORATOIRE, {
    variables: {
      id: idLaboratoire || '',
    },
    skip: !idLaboratoire,
    fetchPolicy: 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  console.log('datadatadatadatadata', data);

  React.useEffect(() => {
    if (data && data.laboratoire) {
      setOrganisation(data.laboratoire);
    }
  }, [data]);

  // Get groupe amis list
  const [searchGroupeAmis, { data: groupeAmisData, loading: groupeAmisLoading }] = useLazyQuery<
    SEARCH_GROUPES_AMIS_WITH_MINIM_INFO,
    SEARCH_GROUPES_AMIS_WITH_MINIM_INFOVariables
  >(DO_SEARCH_GROUPES_AMIS_WITH_MINIM_INFO, {
    fetchPolicy: 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  const { data: dataService } = useQuery<MINIM_SERVICES>(GET_MINIM_SERVICES);

  // Get motif list
  const { data: motifData } = useQuery<TICKET_MOTIFS>(GET_TICKET_MOTIFS, {
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const motifList = (motifData && motifData.ticketMotifs) || [];

  const [searchLaboratoires, searchLaboratoiresResult] = useLazyQuery<
    SEARCH_LABORATOIRE,
    SEARCH_LABORATOIREVariables
  >(DO_SEARCH_LABORATOIRE, {
    fetchPolicy: 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const [searchPartenaires, searchPartenairesResult] = useLazyQuery<
    SEARCH_CUSTOM_CONTENT_PARTENAIRE,
    SEARCH_CUSTOM_CONTENT_PARTENAIREVariables
  >(GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE, {
    fetchPolicy: 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  // Get smyleys list
  const { data: smyleyData } = useQuery<SMYLEYS_WITH_MIN_INFO, SMYLEYS_WITH_MIN_INFOVariables>(
    GET_SMYLEYS_WITH_MIN_INFO,
    {
      variables: { idGroupement: idGrp },
      onError: error => {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
      },
    },
  );

  // TODO : Form smylesy
  const smyleyList = (smyleyData && smyleyData.smyleys) || [];
  const like = smyleyList.find(i => i && i.nom === "J'aime");

  const [doCreateUpdateTicket, { loading: ticketLoading }] = useMutation<
    CREATE_UPDATE_TICKET,
    CREATE_UPDATE_TICKETVariables
  >(DO_CREATE_UPDATE_TICKET, {
    onCompleted: data => {
      if (data && data.createUpdateTicket) {
        setLoading(false);
        setOpen(false);
        setValues(initialState);
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message:
            type === 'RECLAMATION'
              ? 'Reclamation ajouté avec succès'
              : isAppelEntrant
              ? 'Appel entrant ajouté avec succès'
              : 'Appel sortant ajouté avec succès',
        });
        if (refetch) refetch();
      }
    },
    onError: error => {
      setLoading(false);
      console.log('error :>> ', error);
      if (error.graphQLErrors && error.graphQLErrors.length)
        error.graphQLErrors.map(err => {
          displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
        });
    },
  });

  const [doPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (data && data.createPutPresignedUrls && data.createPutPresignedUrls.length > 0) {
        Promise.all(
          data.createPutPresignedUrls.map(async (presigned, index) => {
            if (presigned && selectedFiles.length > 0) {
              await Promise.all(
                selectedFiles.map(async file => {
                  if (presigned.filePath.includes(formatFilenameWithoutDate(file))) {
                    const type: TypeFichier = file.type.includes('pdf')
                      ? TypeFichier.PDF
                      : file.type.includes('xlsx')
                      ? TypeFichier.EXCEL
                      : TypeFichier.PHOTO;
                    const newFile: FichierInput = {
                      type,
                      nomOriginal: file.name,
                      chemin: presigned.filePath,
                    };
                    setValues(prevState => {
                      const fichiers = prevState.fichiers
                        ? [...prevState.fichiers, newFile]
                        : [newFile];
                      files = uniqBy(fichiers, 'chemin');
                      const nextState = { ...prevState, fichiers: files };
                      return nextState;
                    });

                    await uploadToS3(selectedFiles[index], presigned.presignedUrl)
                      .then(result => {
                        if (result && result.status === 200) {
                          uploadResult = result;
                        }
                      })
                      .catch(error => {
                        setLoading(false);
                        console.log('error :>> ', error);
                        displaySnackBar(client, {
                          type: 'ERROR',
                          message: "Erreur lors de l'envoye de(s) fichier(s)",
                          isOpen: true,
                        });
                      });
                  }
                }),
              );
            }
          }),
        ).then(async () => {
          if (uploadResult && uploadResult.status === 200) {
            await doCreateUpdateTicket({
              variables: {
                input: {
                  ...othersValues,
                  idSmyley: (currentSmyley && currentSmyley.id) || '',
                },
              },
            });
          }
        });
      }
    },
    onError: error => {
      console.log('error :>> ', error);
      setLoading(false);
      displaySnackBar(client, {
        type: 'ERROR',
        message: "Erreur lors de l'envoye de(s) fichier(s)",
        isOpen: true,
      });
    },
  });

  React.useEffect(() => {
    if (idLaboratoire) {
      setValues(prev => ({ ...prev, idOrganisation: idLaboratoire, origine: 'FOURNISSEUR' }));
    }
  }, [idLaboratoire]);

  React.useEffect(() => {
    if (type === 'APPEL') {
      setValues(prevState => ({
        ...prevState,
        typeAppel: isAppelEntrant ? TicketAppel.ENTRANT : TicketAppel.SORTANT,
      }));
    }
  }, [type, isAppelEntrant]);

  React.useEffect(() => {
    setValues(prevState => ({
      ...prevState,
      selectedFiles: fichiersJoints,
    }));
  }, [fichiersJoints]);

  React.useEffect(() => {
    const declarant = selectedDeclarant[0];
    if (declarant) {
      setDeclarantName(declarant.userName);
      setValues(prevState => ({ ...prevState, declarant: declarant.id }));
    }
  }, [selectedDeclarant]);

  React.useEffect(() => {
    searchGroupeAmis({
      variables: {
        type: groupeAmisType,
        take: 10,
        filterBy: groupeAmisFilterBy,
      },
    });
  }, []);

  React.useEffect(() => {
    if (type) setValues(prevState => ({ ...prevState, typeTicket: type }));
  }, [type]);

  React.useEffect(() => {
    if (organisation && organisation.id) {
      setValues(prev => ({ ...prev, idOrganisation: organisation.id }));
    } else {
      setValues(prev => ({ ...prev, idOrganisation: '' }));
    }
  }, [organisation]);

  React.useEffect(() => {
    setValues(prevState => ({
      ...prevState,
      idsUserConcernees:
        selectedCollegues && selectedCollegues.length
          ? selectedCollegues.map(selected => selected.id)
          : [],
    }));
  }, [selectedCollegues]);

  // isCheckedTeam, selectedCollegues
  React.useEffect(() => {
    if (isCheckedTeam) {
      setState(prevState => ({ ...prevState, concernedColleagueName: 'all' }));
      setState(prevState => ({ ...prevState, teamAll: true }));
      setState(prevState => ({
        ...prevState,
        concernedColleaguesIds: selectedCollegues
          .filter(sc => sc.id !== currentUserId)
          .map(i => i.id),
      }));
    }

    if (selectedCollegues.length > 0 && !isCheckedTeam) {
      setState(prevState => ({ ...prevState, teamAll: false }));
      setState(prevState => ({
        ...prevState,
        concernedColleagueName: selectedCollegues.map(i => i.userName).join(', '),
      }));
      setState(prevState => ({
        ...prevState,
        concernedColleaguesIds: selectedCollegues.map(i => i.id),
      }));
    }
  }, [isCheckedTeam, selectedCollegues]);

  const debouncedSearch = useRef(
    debounce((keyword: string, dataType: string) => {
      switch (dataType) {
        case 'FOURNISSEUR':
          searchLaboratoires({
            variables: {
              type: laboType,
              sortBy: laboSortBy,
              filterBy:
                keyword.length > 0
                  ? [...laboFilterBy, { wildcard: { nomLabo: `*${keyword}*` } }]
                  : laboFilterBy,
              take: keyword.length > 0 ? null : 10,
            },
          });
          break;
        case 'PRESTATAIRE':
          if (keyword.length > 0)
            searchPartenaires({
              variables: {
                type: partenaireType,
                filterBy: [{ wildcard: { nom: `*${keyword}*` } }],
                take: keyword.length > 0 ? null : 10,
              },
            });

          break;
        case 'GROUPE_AMIS':
          searchGroupeAmis({
            variables: {
              type: groupeAmisType,
              filterBy:
                keyword.length > 0
                  ? [...groupeAmisFilterBy, { wildcard: { nom: `*${keyword}*` } }]
                  : groupeAmisFilterBy,
              take: keyword.length > 0 ? null : 10,
            },
          });
          break;
      }
    }, 1000),
  );

  const onClickCancel = () => {
    setOpen(false);
  };

  const onClickAdd = () => {
    setLoading(true);
    if (!disabledBtnAdd()) {
      if (selectedFiles.length > 0) {
        const filePaths: string[] = [];
        selectedFiles.map((file: File) => {
          filePaths.push(`tickets/${idGrp}/${formatFilenameWithoutDate(file)}`);
        });
        doPresignedUrl({ variables: { filePaths } });
      } else {
        // Add ticket
        doCreateUpdateTicket({
          variables: {
            input: {
              ...othersValues,
              idSmyley: (currentSmyley && currentSmyley.id) || '',
            },
          },
        });
      }
    }
  };

  const disabledBtnAdd = (): boolean => {
    if (typeTicket && origine && declarantName) {
      return false;
    }
    return true;
  };

  const onClickSmyley = (smyley: SMYLEYS_WITH_MIN_INFO_smyleys) => (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    setCurrentSmyley(smyley);
    setValues(prevState => ({ ...prevState, idSmyley: (smyley && smyley.id) || '' }));
  };

  const showSmyleys = () => {
    setOpacity(1);
  };

  const hideSmyleys = () => {
    setOpacity(0);
  };

  const handleChangeText = (name: string) => (e: ChangeEvent<any>, value: string) => {
    debouncedSearch.current(value, origine);
  };

  const handleOpenDeclarant = event => {
    event.stopPropagation();
    setOpenDeclarantModal(true);
  };

  const handleOpenConcernedParticipant = event => {
    event.stopPropagation();
    setOpenConcernedParticipantModal(true);
  };

  const handleChangeVisibilty = (event: ChangeEvent<any>) => {
    event.stopPropagation();
    setValues(prevState => ({ ...prevState, idsUserConcernees: [] }));
    setState(prevState => ({ ...prevState, concernedColleagueName: '' }));
    handleChange(event);
  };

  const handleChangeOrganisationAutocomplete = (name: string) => (
    e: React.ChangeEvent<any>,
    value: any,
  ) => {
    setOrganisation(value);
  };

  const handleChangeAutocomplete = (name: string) => (e: React.ChangeEvent<any>, value: any) => {};

  const handleChangeOrigine = (event: ChangeEvent<any>) => {
    event.stopPropagation();
    const { value } = event.target;
    switch (value) {
      case 'FOURNISSEUR':
        searchLaboratoires({
          variables: {
            type: laboType,
            sortBy: laboSortBy,
            filterBy: laboFilterBy,
            take: 10,
          },
        });
        break;
      case 'PRESTATAIRE': {
        searchPartenaires({
          variables: {
            type: partenaireType,
            take: 10,
          },
        });
      }
    }
    setValues(prevState => ({ ...prevState, idOrganisation: '' }));
    handleChange(event);
  };

  let options: any[] = [];

  let optionLabel = 'nom';

  switch (origine) {
    case 'GROUPE_AMIS':
      options = (groupeAmisData && groupeAmisData.search && groupeAmisData.search.data) || [];
      break;
    case 'GROUPEMENT':
      options = (dataService && dataService.services) || [];
      break;

    case 'FOURNISSEUR':
      options =
        (searchLaboratoiresResult &&
          searchLaboratoiresResult.data &&
          searchLaboratoiresResult.data.search &&
          (searchLaboratoiresResult.data.search.data as any)) ||
        [];
      optionLabel = 'nomLabo';
      break;

    case 'PRESTATAIRE':
      options =
        (searchPartenairesResult &&
          searchPartenairesResult.data &&
          searchPartenairesResult.data.search &&
          (searchPartenairesResult.data.search.data as any)) ||
        [];
      break;
  }

  return (
    <CustomModal
      open={openModal}
      setOpen={setOpen}
      title={
        type === TicketType.APPEL
          ? isAppelEntrant
            ? "Ajout d'un appel entrant"
            : "Ajout d'un appel sortant"
          : "Ajout d'une reclamation"
      }
      withBtnsActions={false}
      closeIcon={true}
      headerWithBgColor={true}
      maxWidth="md"
      fullWidth={true}
      className={classes.modal}
    >
      {(loading || presignedLoading || ticketLoading) && <Backdrop value="Ajout en cours..." />}
      <div className={classes.ticketFormRoot}>
        <div className={classes.formRow}>
          {type === TicketType.RECLAMATION && (
            <CustomSelect
              listId="id"
              index="nom"
              list={motifList}
              name="idMotif"
              label="Motif"
              value={idMotif}
              onChange={handleChange}
            />
          )}
        </div>
        <div className={classnames(classes.formRow)}>
          <CustomSelect
            listId="id"
            index="nom"
            list={origineList}
            name="origine"
            label="Origine"
            value={origine}
            onChange={handleChangeOrigine}
            required={true}
            disabled={idLaboratoire ? true : false}
          />
        </div>
        <div className={classnames(classes.formRow)}>
          <Autocomplete
            value={organisation || ''}
            name="organisation"
            multiple={false}
            handleChange={handleChangeOrganisationAutocomplete}
            handleInputChange={handleChangeText}
            options={options}
            optionLabel={optionLabel}
            label="Nom organisation"
            loading={groupeAmisLoading}
            variant="outlined"
            style={{ width: '100%' }}
            className={classes.customAutomcomplete}
            disabled={idLaboratoire ? true : false}
          />
        </div>
        <div className={classnames(classes.formRow, classes.inputContainer)}>
          <TextField
            variant="outlined"
            label="Nom Interlocuteur"
            InputLabelProps={{ shrink: true }}
            fullWidth
            name="nomInterlocuteur"
            value={nomInterlocuteur}
            onChange={handleChange}
          />
          <TextField
            variant="outlined"
            label="Telephone Interlocuteur"
            InputLabelProps={{ shrink: true }}
            fullWidth
            value={telephoneInterlocuteur}
            name="telephoneInterlocuteur"
            onChange={handleChange}
          />
        </div>
        <div className={classnames(classes.formRow, classes.inputContainer)}>
          <Autocomplete
            value={''}
            name="tache"
            multiple={false}
            handleChange={handleChangeAutocomplete}
            handleInputChange={handleChangeText}
            options={[]}
            optionLabel={''}
            label="Quelle tâche"
            variant="outlined"
            style={{ width: '100%' }}
            className={classes.customAutomcomplete}
          />
          <Autocomplete
            value={''}
            name="fonction"
            multiple={false}
            handleChange={handleChangeAutocomplete}
            handleInputChange={handleChangeText}
            options={[]}
            optionLabel={''}
            label="Fonction"
            variant="outlined"
            style={{ width: '100%' }}
            className={classes.customAutomcomplete}
          />
        </div>
        <div className={classes.formRow}>
          <CustomSelect
            label="Priorité"
            list={priorityList}
            listId="id"
            index="nom"
            name="priority"
            value={priority}
            onChange={handleChange}
            required={true}
          />
        </div>
        <div className={classes.formRow}>
          <CustomTextarea
            label="Détails"
            name="commentaire"
            value={commentaire}
            onChangeTextarea={handleChange}
            rows={3}
          />
        </div>
        <div className={classes.formRow}>
          <div className={classes.emojisContainer} onMouseLeave={hideSmyleys}>
            <img
              className={classes.activeEmoji}
              onClick={onClickSmyley(like as any)}
              src={`${AWS_HOST}/${(currentSmyley && currentSmyley.photo) ||
                (like && like.photo) ||
                ''}`}
              alt={(currentSmyley && currentSmyley.nom) || (like && like.nom) || ''}
              onMouseOver={showSmyleys}
            />
            <div className={classes.emojisList} style={{ opacity }}>
              {smyleyList.map(s => (
                <div className={classes.imgContainer}>
                  <img
                    src={`${AWS_HOST}/${(s && s.photo) || ''}`}
                    alt={(s && s.nom) || ''}
                    onClick={onClickSmyley(s as any)}
                    title={(s && s.nom) || ''}
                  />
                </div>
              ))}
            </div>
          </div>
        </div>
        <div className={classes.formRow}>
          <AssignTaskUserModal
            openModal={openDeclarantModal}
            setOpenModal={setOpenDeclarantModal}
            withNotAssigned={false}
            selected={selectedDeclarant}
            setSelected={setSelectedDeclarant}
            title="Déclarant"
            searchPlaceholder="Rechercher..."
            assignTeamText="Toute l'équipe"
            withAssignTeam={false}
            singleSelect={true}
          />
          <CustomFormTextField
            name="declarant"
            label="Déclarant"
            value={
              currentUserName !== '' && declarantName === currentUserName ? 'Moi' : declarantName
            }
            onClick={handleOpenDeclarant}
            required={true}
          />
        </div>
        <div className={classes.formRow}>
          <FormControlLabel
            control={
              <Radio
                onChange={handleChangeVisibilty}
                checked={visibilite === TicketVisibilite.PRIVE}
                value={TicketVisibilite.PRIVE}
                name="visibilite"
              />
            }
            label="Privé"
          />
          <FormControlLabel
            control={
              <Radio
                onChange={handleChangeVisibilty}
                checked={visibilite === TicketVisibilite.OUVERT}
                value={TicketVisibilite.OUVERT}
                name="visibilite"
              />
            }
            label="Ouvert"
          />
        </div>
        <div className={classes.formRow}>
          <AssignTaskUserModal
            openModal={openConcernedParticipantModal}
            setOpenModal={setOpenConcernedParticipantModal}
            withNotAssigned={false}
            selected={selectedCollegues}
            setSelected={setSelectedCollegues}
            title="Collègue(s) concerné(s)"
            searchPlaceholder="Rechercher..."
            assignTeamText="Toute l'équipe"
            withAssignTeam={true}
            singleSelect={visibilite === TicketVisibilite.PRIVE ? true : false}
            isCheckedTeam={isCheckedTeam}
            setIsCheckedTeam={setIsCheckedTeam}
          />
          <CustomFormTextField
            name="concernedParticipant"
            label="Collègue(s) concerné(s)"
            value={
              state.concernedColleagueName === 'all' || isCheckedTeam
                ? "Toute l'équipe"
                : state.concernedColleagueName
            }
            onClick={handleOpenConcernedParticipant}
          />
        </div>
        <div className={classes.formRow}>
          <Dropzone
            contentText="Glissez et déposez vos documents"
            selectedFiles={fichiersJoints}
            setSelectedFiles={setFichiersJoints}
            multiple={true}
            // acceptFiles="application/pdf"
          />
        </div>
        <Divider orientation="horizontal" />
        <div className={classes.btnsContainer}>
          <CustomButton color="default" onClick={onClickCancel}>
            Annuler
          </CustomButton>
          <CustomButton color="secondary" onClick={onClickAdd} disabled={disabledBtnAdd()}>
            Ajouter
          </CustomButton>
        </div>
      </div>
    </CustomModal>
  );
};

export default TicketForm;
