import {
  Box,
  Checkbox,
  FormControlLabel,
  IconButton,
  Typography,
} from '@material-ui/core';
import { ArrowBack, Visibility } from '@material-ui/icons';
import React, { FC, Fragment, useContext, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { ContentContext, ContentStateInterface } from '../../../../AppContext';
import { AIDE_SUPPORT_URL } from '../../../../Constant/url';
import CustomButton from '../../../Common/CustomButton';
import SearchInput from '../../../Common/newCustomContent/SearchInput';
import SubToolbar from '../../../Common/newCustomContent/SubToolbar';
import WithSearch from '../../../Common/newWithSearch/withSearch';
import TicketForm from './Form/TicketForm';
import useStyles from './styles';
import TicketDetails from './TicketDetails';
import CustomContent from '../../../Common/newCustomContent';
import { Column } from '../../Content/Interface';
import moment from 'moment';
import { AWS_HOST } from '../../../../utils/s3urls';
import { GET_SEARCH_CUSTOM_CONTENT_TICKET } from '../../../../graphql/Ticket';
import { getGroupement, getUser } from '../../../../services/LocalStorage';
import { TicketType } from '../../../../types/graphql-global-types';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { CustomModal } from '../../../Common/CustomModal';
import Status from './TicketStatus';
import { isMobile } from '../../../../utils/Helpers';
import CustomSelectUser from '../../../Common/CustomSelectUser';

interface TicketsProps {
  match: {
    params: {
      ticketType: string | undefined;
      type: string | undefined;
      idType: string | undefined;
      idTicket: string | undefined;
    };
  };
}

const MobileCallpage = CustomSelectUser;

const Tickets: FC<TicketsProps & RouteComponentProps> = ({
  location: { pathname },
  match: { params },
  history: { push },
}) => {
  const {
    content: { variables, operationName, refetch },
  } = useContext<ContentStateInterface>(ContentContext);

  const classes = useStyles({});

  const [isAppelEntrant, setIsAppelEntrant] = useState<boolean>(true);
  const [ticket, setTicket] = useState<any>(null);
  const [entrant, setEntrant] = useState<boolean>(true);
  const [sortant, setSortant] = useState<boolean>(false);

  const isOnAppel: boolean = pathname.includes('appel');

  const [operationNameState, setOperationName] = useState<any>();
  const [variablesState, setVariables] = useState<any>();

  const [openForm, setOpenForm] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);

  const groupement = getGroupement();

  const currentUser: ME_me = getUser();
  const currentUserName = (currentUser && currentUser.userName) || '';

  const [must, setMust] = useState<any[]>([]);

  const defaultMust = [
    { term: { 'groupement.id': (groupement && groupement.id) || '' } },
    { term: { typeTicket: isOnAppel ? 'APPEL' : 'RECLAMATION' } },
  ];

  useEffect(() => {
    if (operationName && !operationNameState) {
      setOperationName(operationName);
      setVariables(variables);
    }
  }, [operationName]);

  const onClickBack = () => push(`/db/${AIDE_SUPPORT_URL}`);

  const onClickAdd = (isEntrant: boolean) => () => {
    setIsAppelEntrant(isEntrant);
    setOpenForm(true);
  };

  const getOrigine = (origine: string) => {
    switch (origine) {
      case 'INTERNE':
        return 'Interne';
      case 'PATIENT':
        return 'Patient';
      case 'GROUPE_AMIS':
        return `Groupe d'amis`;
      case 'GROUPEMENT':
        return 'Groupement';
      case 'FOURNISSEUR':
        return 'Laboratoire';
      case 'PRESTATAIRE':
        return 'Prestataire';

      default:
        return '';
    }
  };

  const getPriority = (priority: number) => {
    switch (priority) {
      case 1:
        return 'Normal';
      case 2:
        return 'Important';
      case 3:
        return `Bloquant`;

      default:
        return '';
    }
  };

  const viewDetail = (value: any) => () => {
    setTicket(value);
    setOpenModal(true);
  };

  const columnTypeAppel = isOnAppel
    ? [
        {
          name: 'typeAppel',
          label: 'Type',
          renderer: (value: any) => {
            return value.typeAppel === 'ENTRANT' ? 'Entrant' : 'Sortant';
          },
        },
      ]
    : [];

  const ticketColumns: Column[] = [
    ...columnTypeAppel,
    {
      name: 'dateHeureSaisie',
      label: 'Date de saisie',
      renderer: (value: any) => {
        return value.dateHeureSaisie ? moment(value.dateHeureSaisie).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'dateHeureSaisie',
      label: 'Heure de saisie',
      renderer: (value: any) => {
        return value.dateHeureSaisie ? moment(value.dateHeureSaisie).format('HH:mm') : '-';
      },
    },
    {
      name: 'origineType',
      label: 'Origine',
      renderer: (value: any) => {
        return value.origineType ? getOrigine(value.origineType) : '-';
      },
    },
    {
      name: 'priority',
      label: 'Priorité',
      renderer: (value: any) => {
        return value.priority ? getPriority(value.priority) : '-';
      },
    },
    {
      name: 'declarant.userName',
      label: 'Declarant',
      renderer: (value: any) => {
        return (
          (value.declarant && value.declarant.userName
            ? value.declarant.userName === currentUserName
              ? 'Moi'
              : value.declarant.userName
            : '-') || '-'
        );
      },
    },
    {
      name: '',
      label: 'Concerné(e)',
      renderer: (value: any) => {
        const concernees =
          value.usersConcernees && value.usersConcernees.length
            ? value.usersConcernees.map(user => user.userName).join(', ')
            : '';
        return concernees;
      },
    },
    {
      name: 'smyley.nom',
      label: 'Smyley',
      renderer: (value: any) => {
        if (value.smyley && value.smyley.photo) {
          return (
            <img
              src={`${AWS_HOST}/${value.smyley.photo}`}
              alt={value.smyley.nom}
              title={value.smyley.nom}
            />
          );
        } else {
          return '-';
        }
      },
    },
    {
      name: 'statusTicket',
      label: 'Statut',
      renderer: (value: any) => {
        return <Status id={value && value.id} currentStatus={value && value.statusTicket} />;
      },
    },
    {
      name: '',
      label: '-',
      renderer: (value: any) => {
        return (
          <IconButton onClick={viewDetail(value)}>
            <Visibility />
          </IconButton>
        );
      },
    },
  ];
  const childrenProps: any = {
    isSelectable: false,
    paginationCentered: true,
    columns: ticketColumns,
  };

  const optionalShould = [
    {
      term: { 'userCreation.id': currentUser.id },
    },
    { term: { 'declarant.id': currentUser.id } },
    { term: { 'usersConcernees.id': currentUser.id } },
  ];
  const sortBy = [{ priority: { order: 'desc' } }, { dateHeureSaisie: { order: 'desc' } }];

  const handleChange = (event: React.ChangeEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    const { name, value } = event.target;

    console.log('name', { name });
    if (name === 'entrant') setEntrant(!entrant);
    if (name === 'sortant') setSortant(!sortant);
  };

  useEffect(() => {
    /*  { term: { typeTicket: isOnAppel ? 'APPEL' : 'RECLAMATION' } }, */
    if ((entrant === true && sortant === true) || (entrant === false && sortant === false)) {
      setMust(defaultMust);
    } else if (isOnAppel) {
      if (entrant === true) {
        setMust([...defaultMust, { term: { typeAppel: 'ENTRANT' } }]);
      }
      if (sortant === true) {
        setMust([...defaultMust, { term: { typeAppel: 'SORTANT' } }]);
      }
    }
  }, [entrant, sortant]);

  const hackModal = () => {};

  return (
    <>
      {isMobile() && isOnAppel ? (
        <>
          <MobileCallpage
            searchPlaceholder="Rechercher par nom ou téléphone"
            page
            useInCall
            openModal={false}
            setOpenModal={hackModal}
          />
        </>
      ) : (
        // web
        <div className={classes.root}>
          <SubToolbar
            title={isOnAppel ? 'Suivi des appels' : 'Suivi des réclamations'}
            dark={true}
            withBackBtn={true}
            onClickBack={onClickBack}
            backBtnIcon={<ArrowBack />}
          >
            {isOnAppel ? (
              <>
                <CustomButton onClick={onClickAdd(true)} color="secondary">
                  ajouter un appel entrant
                </CustomButton>
                <CustomButton onClick={onClickAdd(false)} color="secondary">
                  ajouter un appel sortant
                </CustomButton>
              </>
            ) : (
              <CustomButton onClick={onClickAdd(false)} color="secondary">
                ouvrir une réclamation
              </CustomButton>
            )}
          </SubToolbar>
          <Fragment>
            <div className={classes.globalTabsContainer}>
              <Box display="flex" flexDirection="column">
                {isOnAppel ? (
                  <Typography className={classes.title}>
                    {(entrant === true && sortant === true) ||
                    (entrant === false && sortant === false)
                      ? 'Liste des appels'
                      : entrant === true
                      ? 'Liste des appels entrant'
                      : 'Liste des appels sortant'}
                  </Typography>
                ) : (
                  <Typography className={classes.title}>Liste des réclamations</Typography>
                )}
              </Box>
            </div>
            <div className={classes.contentContainer}>
              <Fragment>
                <div className={classes.searchInputBox}>
                  <SearchInput searchPlaceholder="Rechercher" />
                  {isOnAppel && (
                    <div style={{ padding: '5px 20px' }}>
                      <FormControlLabel
                        control={<Checkbox tabIndex={-1} checked={entrant} disableRipple={true} />}
                        label="Entrant"
                        name={'entrant'}
                        onChange={handleChange}
                      />
                      <FormControlLabel
                        control={<Checkbox tabIndex={-1} checked={sortant} disableRipple={true} />}
                        label="Sortant"
                        name={'sortant'}
                        onChange={handleChange}
                      />
                    </div>
                  )}
                </div>
                <WithSearch
                  type="ticket"
                  props={childrenProps}
                  WrappedComponent={CustomContent}
                  searchQuery={GET_SEARCH_CUSTOM_CONTENT_TICKET}
                  optionalMust={
                    must && must.length
                      ? must
                      : isOnAppel
                      ? [...defaultMust, { term: { typeAppel: 'ENTRANT' } }]
                      : defaultMust
                  }
                  optionalShould={optionalShould}
                  sortByProps={sortBy}
                  fetchPolicy={'network-only'}
                />
              </Fragment>
            </div>
          </Fragment>
          <TicketForm
            open={openForm}
            setOpen={setOpenForm}
            refetch={refetch}
            type={isOnAppel ? TicketType.APPEL : TicketType.RECLAMATION}
            isAppelEntrant={isAppelEntrant}
          />
          <CustomModal
            open={openModal}
            setOpen={setOpenModal}
            title={
              isOnAppel
                ? isAppelEntrant
                  ? "Details d'un appel entrant"
                  : "Details d'un appel sortant"
                : 'Details Réclamation'
            }
            withBtnsActions={false}
            closeIcon={true}
            headerWithBgColor={true}
            fullWidth={true}
          >
            <TicketDetails ticket={ticket} isOnAppel={isOnAppel} />
          </CustomModal>
        </div>
      )}
    </>
  );
};

export default withRouter(Tickets);
