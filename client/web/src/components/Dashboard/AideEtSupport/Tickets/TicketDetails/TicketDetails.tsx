import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { Box, Divider, IconButton, Paper, Typography } from '@material-ui/core';
import { ArrowBack, Close, Edit, PictureAsPdf } from '@material-ui/icons';
import moment from 'moment';
import React, { ChangeEvent, FC, useRef, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { AIDE_SUPPORT_URL } from '../../../../../Constant/url';
import { GET_TICKET } from '../../../../../graphql/Ticket/query';
import { TICKET, TICKETVariables } from '../../../../../graphql/Ticket/types/TICKET';
import { GET_TICKET_STATUTS } from '../../../../../graphql/TicketStatut/query';
import { formatBytes } from '../../../../../utils/Helpers';
import CustomButton from '../../../../Common/CustomButton';
import CustomSelect from '../../../../Common/CustomSelect';
import CustomTextarea from '../../../../Common/CustomTextarea';
import SubToolbar from '../../../../Common/newCustomContent/SubToolbar';
import { Loader } from '../../../Content/Loader';
import useStyles from './styles';
import useCommonStyles from '../../../../Main/Content/Messagerie/SideBar/FormMessage/styles';
import { CustomModal } from '../../../../Common/CustomModal';
import { DO_SEARCH_USERS } from '../../../../../graphql/User';
import {
  SEARCH_USERS,
  SEARCH_USERSVariables,
} from '../../../../../graphql/User/types/SEARCH_USERS';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { debounce } from 'lodash';
import Autocomplete from '../../../../Main/Content/Messagerie/SideBar/FormMessage/Autocomplete';
import { CustomDatePicker, CustomTimePicker } from '../../../../Common/CustomDateTimePicker';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import {
  DO_UPDATE_TICKET_CIBLE,
  DO_UPDATE_TICKET_STATUT,
} from '../../../../../graphql/Ticket/mutation';
import {
  UPDATE_TICKET_STATUT,
  UPDATE_TICKET_STATUTVariables,
} from '../../../../../graphql/Ticket/types/UPDATE_TICKET_STATUT';
import { getGroupement, getUser } from '../../../../../services/LocalStorage';
import {
  UPDATE_TICKET_CIBLE,
  UPDATE_TICKET_CIBLEVariables,
} from '../../../../../graphql/Ticket/types/UPDATE_TICKET_CIBLE';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { DO_CREATE_UPDATE_TICKET } from '../../../../../graphql/Ticket/mutation';
import {
  CREATE_UPDATE_TICKET,
  CREATE_UPDATE_TICKETVariables,
} from '../../../../../graphql/Ticket/types/CREATE_UPDATE_TICKET';
import { GET_SEARCH_TICKET_CHANGEMENT_CIBLE } from '../../../../../graphql/Ticket/query';
import { AWS_HOST } from '../../../../../utils/s3urls';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
import Status from '../TicketStatus';
interface TicketDetailsProps {
  ticket: any;
  isOnAppel: boolean;
}

const TicketDetails: FC<TicketDetailsProps & RouteComponentProps> = ({
  location: { pathname },
  ticket,
  isOnAppel,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();
  const [openReaffectationModal, setOpenReaffectaionModal] = useState<boolean>(false);
  const client = useApolloClient();
  const isOnReclamamation: boolean = !isOnAppel;

  const InfoTitle: FC<{ label: string }> = ({ label }) => {
    return <Typography className={classes.infoTitle}>{label}</Typography>;
  };

  const InfoRow: FC<{ label: string; value: string }> = ({ label, value }) => {
    return (
      <div className={classes.infoRowContainer}>
        <Typography className={classes.infoRowLabel}>{label} : </Typography>
        <Typography className={classes.infoRowValue}>{value}</Typography>
      </div>
    );
  };

  const DividerComponent: FC<{
    marginTop?: number;
    marginBottom?: number;
    marginLeft?: number;
    marginRight?: number;
  }> = ({ marginTop, marginBottom, marginLeft, marginRight }) => {
    return (
      <div
        style={{
          marginTop,
          marginBottom,
          marginLeft: marginLeft ? marginLeft : 0,
          marginRight: marginRight ? marginRight : 0,
        }}
      >
        <Divider />
      </div>
    );
  };

  const [values, setValues] = useState<any>({
    userDestination: {},
    date: null,
  });

  const { userDestination, date } = values;

  const user = getUser();

  const groupement = getGroupement();

  const [ticketstate, setTicketstate] = useState<any>('APPEL');

  const [doUpdateTicketStatut, { loading: updateLoading, data: updateData }] = useMutation<
    UPDATE_TICKET_STATUT,
    UPDATE_TICKET_STATUTVariables
  >(DO_UPDATE_TICKET_STATUT);

  const [doTransformTicketType, { loading: ticketLoading }] = useMutation<
    CREATE_UPDATE_TICKET,
    CREATE_UPDATE_TICKETVariables
  >(DO_CREATE_UPDATE_TICKET, {
    variables: {
      input: {
        id: '',
        visibilite: '' as any,
        typeTicket: ticketstate,
        origine: '',
        idOrganisation: '',
        idMotif: '',
      },
    },
    onCompleted: data => {
      if (data && data.createUpdateTicket) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: "L'appel a été transformé en reclamation",
        });
      }
    },
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const openFile = (url: string) => {
    window.open(url, '_blank');
  };

  const getOrigine = (origine: string) => {
    switch (origine) {
      case 'INTERNE':
        return 'Interne';
      case 'PATIENT':
        return 'Patient';
      case 'GROUPE_AMIS':
        return `Groupe d'amis`;
      case 'GROUPEMENT':
        return 'Groupement';
      case 'FOURNISSEUR':
        return 'Laboratoire';
      case 'PRESTATAIRE':
        return 'Prestataire';

      default:
        return '';
    }
  };

  return (
    <div className={classes.root} id="pageToConvert">
      <div className={classes.rootContainer}>
        <div>
          <Paper elevation={3} className={classes.infoContainer}>
            <InfoTitle
              label={
                isOnReclamamation
                  ? 'Informations sur la réclamation'
                  : ticket && ticket.typeAppel === 'ENTRANT'
                  ? 'Appel Entrant'
                  : 'Appel Sortant'
              }
            />
            {isOnReclamamation && <InfoRow label="Motif" value={''} />}
            <InfoRow
              label="Date de saisie"
              value={
                ticket &&
                ticket.dateHeureSaisie &&
                moment(ticket.dateHeureSaisie).format('DD/MM/YYYY')
              }
            />
            <InfoRow
              label="Heure de saisie"
              value={
                ticket && ticket.dateHeureSaisie && moment(ticket.dateHeureSaisie).format('HH:mm')
              }
            />
            <InfoRow label="Origine" value={getOrigine(ticket && ticket.origineType)} />
            <InfoRow
              label="Type"
              value={ticket && ticket.visibilite === 'PRIVE' ? 'Privé' : 'Ouvert'}
            />
            <InfoRow label="Nom Organisation" value={ticket && ticket.nomOrganisation} />
            {/* <InfoRow label="Statut" value={ticket && ticket.statusTicket} /> */}
            <div className={classes.infoRowContainer}>
              <Typography className={classes.infoRowLabel}>Statut : </Typography>
              <Typography className={classes.infoRowValue}>
                <Status id={ticket && ticket.id} currentStatus={ticket && ticket.statusTicket} />
              </Typography>
            </div>
            <DividerComponent marginTop={30} marginBottom={24} />

            <Typography className={classes.infoRowLabel}>Commentaires : </Typography>
            <div className={classes.infoCommentsContainer}>
              {ticket && ticket.commentaire}
              {ticket && ticket.smyley && ticket.smyley.photo && (
                <Paper elevation={3} className={classes.smyleyContainer}>
                  <img src={`${AWS_HOST}/${ticket.smyley.photo}`} />
                </Paper>
              )}
            </div>
            <Typography className={classes.infoRowLabel}>Pièces Jointes : </Typography>
            <Box className={commonClasses.filesContainer}>
              {ticket && ticket.fichiers && ticket.fichiers.length
                ? ticket.fichiers.map((file, index) => (
                    <Box
                      key={`${file.nomOriginal}_${index}`}
                      className={commonClasses.fileItem}
                      onClick={() => openFile(file.publicUrl)}
                    >
                      <PictureAsPdf />
                      <Box className={commonClasses.filenameContainer}>
                        <Typography className={classes.nomFile}>{file.nomOriginal}</Typography>
                        {file.size && <Typography>{formatBytes(file.size, 1)}</Typography>}
                      </Box>
                    </Box>
                  ))
                : ''}
            </Box>
          </Paper>
        </div>
        <div>
          <Paper elevation={3} className={classes.infoContainer}>
            <InfoTitle label="Déclarant" />
            <InfoRow
              label="Nom et prénom"
              value={ticket && ticket.declarant && ticket.declarant.userName}
            />
            <InfoRow
              label="Pharmacie"
              value={
                (ticket &&
                  ticket.declarant &&
                  ticket.declarant.pharmacie &&
                  ticket.declarant.pharmacie.nom) ||
                '-'
              }
            />
            <InfoRow
              label="Rôle"
              value={
                ticket && ticket.declarant && ticket.declarant.role && ticket.declarant.role.nom
              }
            />

            {ticket && ticket.usersConcernees && (
              <>
                <DividerComponent marginTop={30} marginBottom={24} />
                <InfoTitle label="Collaborateur concernées" />
                {ticket.usersConcernees.map(user => (
                  <>
                    <InfoRow label="Nom et prénom" value={user.userName} />
                    <InfoRow
                      label="Pharmacie"
                      value={(user.pharmacie && user.pharmacie.nom) || '-'}
                    />
                    <InfoRow label="Rôle" value={user.role && user.role.nom} />
                    <DividerComponent
                      marginTop={30}
                      marginRight={75}
                      marginLeft={75}
                      marginBottom={24}
                    />
                  </>
                ))}
              </>
            )}
          </Paper>
        </div>
      </div>
    </div>
  );
};
export default withRouter(TicketDetails);
