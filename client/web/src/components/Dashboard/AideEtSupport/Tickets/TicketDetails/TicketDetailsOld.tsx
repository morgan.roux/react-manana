import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { Box, Divider, IconButton, Paper, Typography } from '@material-ui/core';
import { ArrowBack, Close, Edit, PictureAsPdf } from '@material-ui/icons';
import moment from 'moment';
import React, { ChangeEvent, FC, useRef, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { AIDE_SUPPORT_URL } from '../../../../../Constant/url';
import { GET_TICKET } from '../../../../../graphql/Ticket/query';
import { TICKET, TICKETVariables } from '../../../../../graphql/Ticket/types/TICKET';
import { GET_TICKET_STATUTS } from '../../../../../graphql/TicketStatut/query';
import { formatBytes } from '../../../../../utils/Helpers';
import CustomButton from '../../../../Common/CustomButton';
import CustomSelect from '../../../../Common/CustomSelect';
import CustomTextarea from '../../../../Common/CustomTextarea';
import SubToolbar from '../../../../Common/newCustomContent/SubToolbar';
import { Loader } from '../../../Content/Loader';
import useStyles from './styles';
import useCommonStyles from '../../../../Main/Content/Messagerie/SideBar/FormMessage/styles';
import { CustomModal } from '../../../../Common/CustomModal';
import { DO_SEARCH_USERS } from '../../../../../graphql/User';
import {
  SEARCH_USERS,
  SEARCH_USERSVariables,
} from '../../../../../graphql/User/types/SEARCH_USERS';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { debounce } from 'lodash';
import Autocomplete from '../../../../Main/Content/Messagerie/SideBar/FormMessage/Autocomplete';
import { CustomDatePicker, CustomTimePicker } from '../../../../Common/CustomDateTimePicker';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import {
  DO_UPDATE_TICKET_CIBLE,
  DO_UPDATE_TICKET_STATUT,
} from '../../../../../graphql/Ticket/mutation';
import {
  UPDATE_TICKET_STATUT,
  UPDATE_TICKET_STATUTVariables,
} from '../../../../../graphql/Ticket/types/UPDATE_TICKET_STATUT';
import { getGroupement, getUser } from '../../../../../services/LocalStorage';
import {
  UPDATE_TICKET_CIBLE,
  UPDATE_TICKET_CIBLEVariables,
} from '../../../../../graphql/Ticket/types/UPDATE_TICKET_CIBLE';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { DO_CREATE_UPDATE_TICKET } from '../../../../../graphql/Ticket/mutation';
import {
  CREATE_UPDATE_TICKET,
  CREATE_UPDATE_TICKETVariables,
} from '../../../../../graphql/Ticket/types/CREATE_UPDATE_TICKET';
import { GET_SEARCH_TICKET_CHANGEMENT_CIBLE } from '../../../../../graphql/Ticket/query';
import { AWS_HOST } from '../../../../../utils/s3urls';
import html2canvas from 'html2canvas';
import jsPDF from 'jspdf';
interface TicketDetailsProps {
  match: {
    params: {
      ticketType: string | undefined;
      type: string | undefined;
      idType: string | undefined;
      idTicket: string | undefined;
    };
  };
  ticket: any;
  isOnAppel: boolean;
}

const TicketDetails: FC<TicketDetailsProps & RouteComponentProps> = ({
  location: { pathname },
  match: {
    params: { ticketType, type, idTicket, idType },
  },
  history: { push },
  ticket,
}) => {
  const classes = useStyles({});
  const commonClasses = useCommonStyles();
  const [openReaffectationModal, setOpenReaffectaionModal] = useState<boolean>(false);
  const client = useApolloClient();
  const isOnAppel: boolean = pathname.includes('appel');
  const isOnReclamamation: boolean = pathname.includes('reclamation');
  const ticketStatutsQuery = useQuery(GET_TICKET_STATUTS);
  const ticketStatutsResult =
    (ticketStatutsQuery && ticketStatutsQuery.data && ticketStatutsQuery.data.ticketStatuts) || [];

  const ticketQuery = useQuery<TICKET, TICKETVariables>(GET_TICKET, {
    variables: { id: idTicket || '' },
  });

  const ticketResult = ticketQuery && ticketQuery.data && ticketQuery.data.ticket;
  console.log('ticketQuery', ticketResult);
  const onClickBack = () => {
    push(`/db/${AIDE_SUPPORT_URL}/${ticketType}/${type}/${idType}/list`);
  };

  const onTicketStatutChange = () => {};

  // Get users
  const [searchUsers, { data: userData, loading: userLoading }] = useLazyQuery<
    SEARCH_USERS,
    SEARCH_USERSVariables
  >(DO_SEARCH_USERS, {
    variables: {},
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const debouncedSearch = useRef(
    debounce((text: string) => {
      searchUsers({
        variables: {
          query: {
            query: {
              exists: { field: 'userPersonnel' },
            },
          },
        },
      });
    }, 1000),
  );

  const onChangeUserTxt = (name: string) => (e: ChangeEvent<any>, value: string) => {
    debouncedSearch.current(value);
  };

  const InfoTitle: FC<{ label: string }> = ({ label }) => {
    return <Typography className={classes.infoTitle}>{label}</Typography>;
  };

  const InfoRow: FC<{ label: string; value: string }> = ({ label, value }) => {
    return (
      <div className={classes.infoRowContainer}>
        <Typography className={classes.infoRowLabel}>{label} : </Typography>
        <Typography className={classes.infoRowValue}>{value}</Typography>
      </div>
    );
  };

  const ReclamationComponent: FC<{ data: any }> = ({ data }) => {
    const userCible = data && data.userCible;
    return (
      <Paper elevation={3} className={classes.reclamationRoot}>
        {isOnReclamamation ? (
          <Typography className={classes.infoRowValue}>Réclamation consulté par</Typography>
        ) : (
          <Typography className={classes.infoRowValue}>Appel consulté par</Typography>
        )}
        <DividerComponent marginTop={16} marginBottom={24} />
        <div className={classes.rowContainer}>
          <div>
            <InfoRow label="Nom et prénom" value={userCible && userCible.userName} />
            <InfoRow
              label="Date"
              value={
                data &&
                data.dateHeureAffectation &&
                moment(data.dateHeureAffectation).format('DD/MM/YYYY')
              }
            />
            <InfoRow
              label="Heure"
              value={
                data &&
                data.dateHeureAffectation &&
                moment(data.dateHeureAffectation).format('HH:mm')
              }
            />
          </div>
          <div className={classes.statusSelect}>
            <CustomSelect
              label="Statut"
              list={ticketStatutsResult}
              listId="id"
              index="value"
              name="nom"
              value={null}
              onChange={onTicketStatutChange}
              required={false}
              disabled={true}
            />
          </div>
        </div>
        {/* <CustomTextarea
          className={classes.suiviDescription}
          label="Description"
          value={data && data.description}
          onChangeTextarea={() => {}}
          disabled={true}
        /> */}
      </Paper>
    );
  };

  const DividerComponent: FC<{ marginTop?: number; marginBottom?: number }> = ({
    marginTop,
    marginBottom,
  }) => {
    return (
      <div style={{ marginTop, marginBottom }}>
        <Divider />
      </div>
    );
  };

  const ticketMotif = ticketResult && ticketResult.motif;
  const ticketUserCreation = ticketResult && ticketResult.userCreation;
  const ticketUserCreationRole =
    (ticketUserCreation && ticketUserCreation.role && ticketUserCreation.role.nom) || '';
  const ticketUserCreationService =
    (ticketUserCreation &&
      ticketUserCreation.userPersonnel &&
      ticketUserCreation.userPersonnel.service &&
      ticketUserCreation.userPersonnel.service.nom) ||
    '';
  const ticketUserCreationPharmacie =
    (ticketUserCreation && ticketUserCreation.pharmacie && ticketUserCreation.pharmacie.nom) || '';
  const ticketFiles: any[] = (ticketResult && ticketResult.fichiers) || [];

  const ticketUserCibleCreation = null;
  const ticketUserCibleCreationRole = '';
  const ticketUserCibleCreationService = '';
  const ticketUserCibleCreationPharmacie =
    (ticketUserCreation && ticketUserCreation.pharmacie && ticketUserCreation.pharmacie.nom) || '';

  const isLoading =
    (ticketStatutsQuery && ticketStatutsQuery.loading) || (ticketQuery && ticketQuery.loading);

  const [values, setValues] = useState<any>({
    userDestination: {},
    date: null,
  });

  const { userDestination, date } = values;

  console.log('values : ', values);

  const user = getUser();

  const groupement = getGroupement();

  const handleUpdateTicketCible = () => {
    doUpdateTicketCible();
  };

  const getTicketChangementCiblesQuery = useQuery(GET_SEARCH_TICKET_CHANGEMENT_CIBLE, {
    variables: {
      query: {
        query: {
          bool: {
            must: [
              { term: { 'groupement.id': groupement && groupement.id } },
              { term: { 'ticket.id': idTicket } },
            ],
          },
        },
      },
    },
  });

  const getTicketChangementCiblesResult =
    (getTicketChangementCiblesQuery &&
      getTicketChangementCiblesQuery.data &&
      getTicketChangementCiblesQuery.data.search &&
      getTicketChangementCiblesQuery.data.search.data) ||
    [];

  const [doUpdateTicketStatut, { loading: updateLoading, data: updateData }] = useMutation<
    UPDATE_TICKET_STATUT,
    UPDATE_TICKET_STATUTVariables
  >(DO_UPDATE_TICKET_STATUT);

  const [doUpdateTicketCible, { loading: updateCibleLoading, data: updateCibleData }] = useMutation<
    UPDATE_TICKET_CIBLE,
    UPDATE_TICKET_CIBLEVariables
  >(DO_UPDATE_TICKET_CIBLE, {
    variables: {
      input: {
        idCurrentUser: (user && user.id) || '',
        idTicket,
        dateHeureAffectation: date,
        idUserCible: (userDestination && userDestination.id) || '',
        idServiceCible:
          userDestination &&
          userDestination.userPersonnel &&
          userDestination.userPersonnel.service &&
          userDestination.userPersonnel.service.id,
      },
    },
    update: (cache, { data, errors }) => {
      if (data && data.updateTicketCible) {
        const req: any = cache.readQuery({
          query: GET_SEARCH_TICKET_CHANGEMENT_CIBLE,
          variables: {
            query: {
              query: {
                bool: {
                  must: [
                    { term: { 'groupement.id': groupement && groupement.id } },
                    { term: { 'ticket.id': idTicket } },
                  ],
                },
              },
            },
          },
        });
        cache.writeQuery({
          query: GET_SEARCH_TICKET_CHANGEMENT_CIBLE,
          data: {
            search: {
              ...req.search,
              ...{
                total: req.search.total + 1,
                data: [...req.search.data, data.updateTicketCible],
              },
            },
          },
          variables: {
            query: {
              query: {
                bool: {
                  must: [
                    { term: { 'groupement.id': groupement && groupement.id } },
                    { term: { 'ticket.id': idTicket } },
                  ],
                },
              },
            },
          },
        });
      }
    },
    onCompleted: data => {
      if (data && data.updateTicketCible) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: "Réaffectaion d'un réclamation réussie",
          isOpen: true,
        });
      }

      setOpenReaffectaionModal(false);
    },
    onError: error => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: error.message,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [ticketstate, setTicketstate] = useState<any>('APPEL');
  const handleChangeTicketType = async () => {
    setTicketstate('RECLAMATION');
    doTransformTicketType();
    await pathname.replace('appel', 'reclamation');
  };
  console.log('ticketState', ticketstate);
  const [doTransformTicketType, { loading: ticketLoading }] = useMutation<
    CREATE_UPDATE_TICKET,
    CREATE_UPDATE_TICKETVariables
  >(DO_CREATE_UPDATE_TICKET, {
    variables: {
      input: {
        id: idTicket,
        visibilite: (ticketResult && ticketResult.visibilite) as any,
        typeTicket: ticketstate,
        origine: '',
        idOrganisation: '',
        idMotif: (ticketResult && ticketResult.motif && ticketResult.motif.id) || '',
      },
    },
    onCompleted: data => {
      if (data && data.createUpdateTicket) {
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: "L'appel a été transformé en reclamation",
        });
      }
    },
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const handleChangeDate = (name: string) => (date: any) => {
    if (date && name) {
      setValues(prevState => ({ ...prevState, [name]: date }));
    }
  };

  const handleChangeAutocomplete = (name: string) => (e: React.ChangeEvent<any>, value: any) => {
    setValues(prevState => ({ ...prevState, [name]: value }));
  };

  const handleOpenReaffectationModal = () => {
    setOpenReaffectaionModal(!openReaffectationModal);
  };

  const openFile = (url: string) => {
    window.open(url, '_blank');
  };

  const exportToPdf = () => {
    const input = document.getElementById('pageToConvert');
    if (input) {
      html2canvas(input).then(canvas => {
        const imgData = canvas.toDataURL('image/png');
        const pdf = new jsPDF();
        pdf.addImage(imgData, 'PNG', 0, 0, 0, 0);
        pdf.save('download.pdf');
      });
    }
  };

  if (isLoading) {
    return <Loader />;
  }

  return (
    <div className={classes.root} id="pageToConvert">
      <SubToolbar
        title={isOnReclamamation ? 'Détail de réclamation' : "Détail d'un appel"}
        dark={true}
        withBackBtn={true}
        onClickBack={onClickBack}
        backBtnIcon={<ArrowBack />}
      />
      <div className={classes.rootContainer}>
        <div>
          <Paper elevation={3} className={classes.infoContainer}>
            <InfoTitle
              label={isOnReclamamation ? 'Informations sur la réclamation' : "Saisi d'un appel"}
            />
            {isOnReclamamation && (
              <InfoRow label="Motif" value={(ticketMotif && ticketMotif.nom) || ''} />
            )}
            {isOnAppel && <InfoRow label="Nom de l'operateur" value={'-'} />}
            <InfoRow
              label="Date de saisie"
              value={
                ticketResult &&
                ticketResult.dateHeureSaisie &&
                moment(ticketResult.dateHeureSaisie).format('DD/MM/YYYY')
              }
            />
            <InfoRow
              label="Heure de saisie"
              value={
                ticketResult &&
                ticketResult.dateHeureSaisie &&
                moment(ticketResult.dateHeureSaisie).format('HH:mm')
              }
            />
            <InfoRow label="Type de l'appel" value={(ticketResult && ticketResult.type) || ''} />
            <DividerComponent marginTop={24} marginBottom={16} />
            <InfoTitle label="Réclamation ouvert par" />
            <InfoRow
              label="Nom et prénom"
              value={(ticketUserCreation && ticketUserCreation.userName) || ''}
            />
            <InfoRow label="Pharmacie" value={ticketUserCreationPharmacie || '-'} />
            <InfoRow label="Service" value={ticketUserCreationService || '-'} />
            <InfoRow label="Rôle" value={ticketUserCreationRole} />
            <InfoRow label="Code postal" value="-" />
            <InfoRow label="Ville" value="-" />
            <DividerComponent marginTop={30} marginBottom={24} />
            <Typography className={classes.infoRowLabel}>Commentaires : </Typography>
            <div className={classes.infoCommentsContainer}>
              {ticketResult && ticketResult.commentaire}
              <Paper elevation={3} className={classes.smyleyContainer}>
                <img
                  src={
                    (ticketResult &&
                      ticketResult.smyley &&
                      ticketResult.smyley.photo &&
                      `${AWS_HOST}/${ticketResult.smyley.photo}`) ||
                    ''
                  }
                />
              </Paper>
            </div>
            <Typography className={classes.infoRowLabel}>Pièces Jointes : </Typography>
            <Box className={commonClasses.filesContainer}>
              {ticketFiles.map((file, index) => (
                <Box
                  key={`${file.nomOriginal}_${index}`}
                  className={commonClasses.fileItem}
                  onClick={() => openFile(file.publicUrl)}
                >
                  <PictureAsPdf />
                  <Box className={commonClasses.filenameContainer}>
                    <Typography className={classes.nomFile}>{file.nomOriginal}</Typography>
                    {file.size && <Typography>{formatBytes(file.size, 1)}</Typography>}
                  </Box>
                </Box>
              ))}
            </Box>
          </Paper>
          <Paper elevation={3} className={classes.infoContainer}>
            <InfoTitle label={isOnAppel ? 'Appel  associé à' : 'Réclamation associé à'} />
            <InfoRow label="Nom et prénom" value={'-'} />
            <InfoRow label="Pharmacie" value={ticketUserCibleCreationPharmacie || '-'} />
            <InfoRow label="Service" value={ticketUserCibleCreationService || '-'} />
            <InfoRow label="Rôle" value={ticketUserCibleCreationRole || '-'} />
            <InfoRow label="Code postal" value="-" />
            <InfoRow label="Ville" value="-" />
            <div className={classes.editIconContainer}>
              <IconButton
                color="secondary"
                classes={{ root: classes.editIcon }}
                onClick={handleOpenReaffectationModal}
              >
                <Edit />
              </IconButton>
            </div>
          </Paper>
        </div>

        <div className={classes.suiviContainer}>
          {isOnReclamamation ? (
            <Typography className={classes.suiviTitle}>Suivie de réclamation</Typography>
          ) : (
            <Typography className={classes.suiviTitle}>Suivie des appels</Typography>
          )}
          {isOnAppel && (
            <CustomButton
              className={classes.transformButton}
              onClick={handleChangeTicketType}
              color="secondary"
            >
              TRANSFORMER EN RECLAMATION
            </CustomButton>
          )}

          <CustomButton
            className={classes.exportBtn}
            startIcon={<PictureAsPdf />}
            onClick={exportToPdf}
          >
            EXPORTER EN PDF
          </CustomButton>

          {getTicketChangementCiblesResult.map(item => (
            <ReclamationComponent data={item} />
          ))}
          <CustomModal
            open={openReaffectationModal}
            setOpen={setOpenReaffectaionModal}
            closeIcon={true}
            withBtnsActions={false}
            title={
              isOnReclamamation ? 'Réaffectation d’une réclamation' : "Réaffectation d'un appel"
            }
            headerWithBgColor={true}
            centerBtns={true}
          >
            <div className={classes.modalRoot}>
              <div className={classes.autocomplete}>
                <Autocomplete
                  variant="outlined"
                  label={isOnAppel ? 'Appel réaffecté à' : 'Réclamation réaffectée à'}
                  value={userDestination}
                  name="userDestination"
                  handleChange={handleChangeAutocomplete}
                  // tslint:disable-next-line: jsx-no-lambda
                  handleInputChange={() => onChangeUserTxt('userDestination')}
                  options={(userData && userData.search && userData.search.data) || []}
                  optionLabel="userName"
                  loading={userLoading}
                />
              </div>
              <div>
                <CustomFormTextField
                  label="Service"
                  value={
                    userDestination &&
                    userDestination.userPersonnel &&
                    userDestination.userPersonnel.service &&
                    userDestination.userPersonnel.service.nom
                  }
                  disabled={true}
                />
              </div>

              <div className={classes.dateTimeContainer}>
                <CustomDatePicker
                  label="Date"
                  placeholder="Date "
                  onChange={handleChangeDate('date')}
                  name="date"
                  value={date || null}
                  // className={classes.dateInput}
                  disablePast={false}
                />
                <CustomTimePicker
                  label="Heure début"
                  placeholder="Heure début"
                  onChange={handleChangeDate('date')}
                  name="date"
                  value={date}
                  // className={classes.dateInput}
                />
              </div>
              <div className={classes.modalButtonContainer}>
                <CustomButton onClick={handleOpenReaffectationModal}>ANNULER</CustomButton>
                <CustomButton onClick={handleUpdateTicketCible} color="secondary">
                  Ajouter
                </CustomButton>
              </div>
            </div>
          </CustomModal>
        </div>
      </div>
    </div>
  );
};
export default withRouter(TicketDetails);
