import { IconButton } from '@material-ui/core';
import { Visibility } from '@material-ui/icons';
import moment from 'moment';
import React, { Fragment } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { AIDE_SUPPORT_URL } from '../../../../Constant/url';
import { GET_SEARCH_CUSTOM_CONTENT_TICKET } from '../../../../graphql/Ticket';
import { getGroupement } from '../../../../services/LocalStorage';
import { AWS_HOST } from '../../../../utils/s3urls';
import CustomContent from '../../../Common/newCustomContent';
import SearchInput from '../../../Common/newCustomContent/SearchInput';
import WithSearch from '../../../Common/newWithSearch/withSearch';
import { Column } from '../../Content/Interface';
import useStyles from './styles';

const TicketTable: React.FC<RouteComponentProps> = ({
  history: { push },
  location: { pathname },
  match: { params },
}) => {
  const groupement = getGroupement();
  const classes = useStyles({});

  const { idType } = params as any;

  const isOnPharmacie: boolean = pathname.includes('pharmacie');
  const isOnCollabotateur: boolean = pathname.includes('collaborateur');
  const isOnAppel: boolean = pathname.includes('appel');
  const viewDetail = (row: any) => () => {
    if (isOnPharmacie) {
      isOnAppel
        ? push(`/db/${AIDE_SUPPORT_URL}/appel/pharmacie/${idType}/list/${row.id}`)
        : push(`/db/${AIDE_SUPPORT_URL}/reclamation/pharmacie/${idType}/list/${row.id}`);
    }
    if (isOnCollabotateur) {
      isOnAppel
        ? push(`/db/${AIDE_SUPPORT_URL}/appel/collaborateur/${idType}/list/${row.id}`)
        : push(`/db/${AIDE_SUPPORT_URL}/reclamation/collaborateur/${idType}/list/${row.id}`);
    }
  };

  const ticketColumns: Column[] = [
    {
      name: 'dateHeureSaisie',
      label: 'Date de saisie',
      renderer: (value: any) => {
        return value.dateHeureSaisie ? moment(value.dateHeureSaisie).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'dateHeureSaisie',
      label: 'Heure de saisie',
      renderer: (value: any) => {
        return value.dateHeureSaisie ? moment(value.dateHeureSaisie).format('HH:mm') : '-';
      },
    },
    {
      name: 'origine.nom',
      label: 'Origine',
      renderer: (value: any) => {
        return (value.origine && value.origine.nom) || '-';
      },
    },
    isOnAppel
      ? {
          name: 'motif.nom',
          label: "Motif d'appel",
          renderer: (value: any) => {
            return (value.motif && value.motif.nom) || '-';
          },
        }
      : {
          name: 'motif.nom',
          label: 'Motif',
          renderer: (value: any) => {
            return (value.motif && value.motif.nom) || '-';
          },
        },
    {
      name: 'cible.serviceCible.nom',
      label: 'Concerné(e)',
      renderer: (value: any) => {
        return (value.cible && value.cible.serviceCible && value.cible.serviceCible.nom) || '-';
      },
    },
    {
      name: 'cible.userCible.userName',
      label: 'Correspondant',
      renderer: (value: any) => {
        return (value.cible && value.cible.userCible && value.cible.userCible.userName) || '-';
      },
    },
    {
      name: 'commentaire',
      label: 'Commentaire',
      renderer: (value: any) => {
        return value.commentaire || '-';
      },
    },
    {
      name: 'smyley.photo',
      label: 'Smyley',
      renderer: (value: any) => {
        if (value.smyley && value.smyley.photo) {
          return (
            <img
              src={`${AWS_HOST}/${value.smyley.photo}`}
              alt={value.smyley.nom}
              title={value.smyley.nom}
            />
          );
        } else {
          return '-';
        }
      },
    },
    {
      name: 'statut',
      label: 'Statut',
      renderer: (value: any) => {
        return (value.statut && value.statut.nom) || '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <IconButton onClick={viewDetail(value)}>
            <Visibility />
          </IconButton>
        );
      },
    },
  ];

  const must = [{ term: { 'groupement.id': (groupement && groupement.id) || '' } }];

  const childrenProps: any = {
    isSelectable: false,
    paginationCentered: true,
    columns: ticketColumns,
  };

  return (
    <Fragment>
      <div className={classes.searchInputBox}>
        <SearchInput searchPlaceholder="Rechercher" />
      </div>
      <WithSearch
        type="ticket"
        props={childrenProps}
        WrappedComponent={CustomContent}
        searchQuery={GET_SEARCH_CUSTOM_CONTENT_TICKET}
        //optionalMust={[...must, ...othersMust]}
        optionalMust={must}
      />
    </Fragment>
  );
};

export default withRouter(TicketTable);
