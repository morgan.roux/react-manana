import { Call } from '@material-ui/icons';
import React, { FC, useState } from 'react';
import { SuiviAppleMobile } from '../SuiviAppels/SuiviAppels';

const CarnetContact: FC = () => {
  const [openDrawer, setOpenDrawer] = useState<boolean>(false);

  const goBack = () => {};

  const handleNext = () => {};

  const handleItemClick = () => {};

  const handleChangeSearch = (value: string): void => {};

  return (
    <SuiviAppleMobile
      banner={{
        title: 'Liste des appels reçu',
        goBack: goBack,
      }}
      search={{
        onSearch: handleChangeSearch,
        placeholder: 'Rechercher par nom ou téléphone',
      }}
      drawer={{
        title: 'Détails appels',
        onClose: () => setOpenDrawer(false),
        onOpen: () => setOpenDrawer(true),
        open: openDrawer,
        loading: false,
        data: [],
      }}
      listPerson={{
        onNext: handleNext,
        onClickItem: handleItemClick,
        data: [],
        loading: false,
        actionsRenderer: (data: any) => {
          return <Call />;
        },
      }}
    />
  );
};

export default CarnetContact;
