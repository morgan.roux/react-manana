import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { Box } from '@material-ui/core';
import React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { useAideForm } from '.';
import { AIDE_SUPPORT_URL } from '../../../../../Constant/url';
import { DO_CREATE_UPDATE_AIDE } from '../../../../../graphql/Aide';
import {
  CREATE_UPDATE_AIDE,
  CREATE_UPDATE_AIDEVariables,
} from '../../../../../graphql/Aide/types/CREATE_UPDATE_AIDE';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import Backdrop from '../../../../Common/Backdrop';
import CustomButton from '../../../../Common/CustomButton';
import CustomTextarea from '../../../../Common/CustomTextarea';
import { CustomFormTextField } from '../../../../Common/CustomTextField';

export interface AideFormProps {
  refetch: any;
}

const AideForm: React.FC<AideFormProps & RouteComponentProps> = ({
  history: { push },
  refetch,
}) => {
  const client = useApolloClient();
  const { values, handleChange } = useAideForm();
  const { title, description } = values;

  const onClickCancel = () => push(`/db/${AIDE_SUPPORT_URL}/aides`);

  const onClickAdd = () => {
    if (title && description) {
      doCreateUpdate({ variables: { input: { ...values, id: null } } });
    }
  };

  const [doCreateUpdate, { loading }] = useMutation<
    CREATE_UPDATE_AIDE,
    CREATE_UPDATE_AIDEVariables
  >(DO_CREATE_UPDATE_AIDE, {
    onCompleted: data => {
      if (data && data.createUpdateAide) {
        if (refetch) refetch();
        displaySnackBar(client, {
          isOpen: true,
          type: 'SUCCESS',
          message: 'Aide créée avec succès',
        });
        push(`/db/${AIDE_SUPPORT_URL}/aides`);
      }
    },
    onError: error => {
      console.log('error :>> ', error);
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  return (
    <Box display="flex" flexDirection="column" maxWidth="800px" width="100%" marginTop="50px">
      {loading && <Backdrop value="Création en cours..." />}
      <CustomFormTextField
        name="title"
        value={title}
        placeholder="Titre"
        onChange={handleChange}
        style={{ marginBottom: 30 }}
      />
      <CustomTextarea
        label=""
        name="description"
        value={description}
        placeholder="Description"
        onChangeTextarea={handleChange}
        rowsMin={5}
      />
      <Box display="flex" flexDirection="row" alignItems="center" marginTop="30px" width="100%">
        <CustomButton color="default" onClick={onClickCancel}>
          Annuler
        </CustomButton>
        <CustomButton color="secondary" style={{ marginLeft: 30 }} onClick={onClickAdd}>
          Ajouter
        </CustomButton>
      </Box>
    </Box>
  );
};

export default withRouter(AideForm);
