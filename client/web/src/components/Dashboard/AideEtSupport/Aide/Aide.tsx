import { useLazyQuery, useQuery } from '@apollo/react-hooks';
import {
  Collapse,
  Divider,
  InputAdornment,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
} from '@material-ui/core';
import { ExpandLess, ExpandMore, Message, Search } from '@material-ui/icons';
import classnames from 'classnames';
import { debounce } from 'lodash';
import React, { Fragment } from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { AIDE_SUPPORT_URL } from '../../../../Constant/url';
import { DO_SEARCH_AIDES } from '../../../../graphql/Aide';
import { SEARCH_AIDES, SEARCH_AIDESVariables } from '../../../../graphql/Aide/types/SEARCH_AIDES';
import Backdrop from '../../../Common/Backdrop';
import CustomButton from '../../../Common/CustomButton';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import AideForm from './AideForm';
import useStyles from './styles';

export interface IData {
  id: string;
  title: string;
  description: string;
  open: boolean;
}

const Aide: React.FC<RouteComponentProps> = ({ location: { pathname }, history: { push } }) => {
  const classes = useStyles({});

  const CREATE_URL = `/db/aide/${AIDE_SUPPORT_URL}/aide/create`;
  const isOnCreate: boolean = pathname === CREATE_URL;

  const handleClick = (dataIndex: number) => () => {
    setData(prevState => {
      if (prevState[dataIndex]) {
        prevState[dataIndex].open = !prevState[dataIndex].open;
      }
      return [...prevState];
    });
  };

  const goToAdd = () => push(CREATE_URL);

  const [search, { data: sData, loading, refetch }] = useLazyQuery<
    SEARCH_AIDES,
    SEARCH_AIDESVariables
  >(DO_SEARCH_AIDES, { fetchPolicy: 'cache-and-network' });

  const title = isOnCreate ? "Ajout de l'aide" : 'Vous avez une question ?';

  const [data, setData] = React.useState<IData[]>([]);
  const [searchText, setSearchText] = React.useState<string>('');

  // TODO : Fix error
  const debouncedSearch = React.useRef(
    debounce((text: string) => {
      search({
        variables: {
          query: {
            query_string: {
              query: text.startsWith('*') || text.endsWith('*') ? text : `*${text}*`,
              analyze_wildcard: true,
            },
          },
        },
      });
    }, 1000),
  );

  // query: {
  //           bool: {
  //             should: [
  //               { wildcard: { title: `*${text}*` } },
  //               { wildcard: { description: `*${text}*` } },
  //             ],
  //             minimum_should_match: 1,
  //           },
  //         },

  const onChangeSearchTxt = (e: React.ChangeEvent<any>) => {
    if (e.target) {
      setSearchText(e.target.value);
      debouncedSearch.current(e.target.value);
    }
  };

  // Set data
  React.useEffect(() => {
    if (sData && sData.search && sData.search.data) {
      const newData: IData[] = sData.search.data.map((d: any) => {
        const newD: IData = {
          id: (d && d.id) || '',
          title: (d && d.title) || '',
          description: (d && d.description) || '',
          open: false,
        };
        return newD;
      });
      setData(newData);
    }
  }, [sData]);

  // Launch query
  React.useEffect(() => {
    search();
  }, []);

  return (
    <div className={classes.aideRoot}>
      {loading && <Backdrop />}
      <div className={classes.head}>
        <Typography className={classes.headTitle}>{title}</Typography>
      </div>
      {isOnCreate ? (
        <AideForm refetch={refetch} />
      ) : (
        <div className={classes.content}>
          <Typography className={classnames(classes.headTitle, classes.contentTitle)}>
            Posez votre question ici :
          </Typography>
          <div className={classes.searchInput}>
            <CustomFormTextField
              variant="outlined"
              placeholder="Exemple : comment ajouter des contacts"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <Search />
                  </InputAdornment>
                ),
              }}
              value={searchText}
              onChange={onChangeSearchTxt}
            />
            <CustomButton color="secondary" onClick={goToAdd}>
              Ecrire au responsable
            </CustomButton>
          </div>
          <div className={classnames(classes.head, classes.aidesTitle)}>
            <Typography className={classnames(classes.headTitle, classes.contentTitle)}>
              Questions les plus fréquentes :
            </Typography>
          </div>
          <List
            component="nav"
            aria-labelledby="nested-list-subheader"
            className={classes.listRoot}
          >
            {data.map((d, index) => {
              return (
                <Fragment key={index}>
                  <ListItem
                    button={true}
                    onClick={handleClick(index)}
                    className={classes.aideTitle}
                  >
                    <ListItemIcon>
                      <Message />
                    </ListItemIcon>
                    <ListItemText primary={d.title} />
                    {d.open ? <ExpandLess /> : <ExpandMore />}
                  </ListItem>
                  <Collapse in={d.open} timeout="auto" unmountOnExit={true}>
                    <Divider orientation="horizontal" />
                    <div className={classes.descriptionContainer}>
                      <Typography className={classes.description}>{d.description}</Typography>
                    </div>
                  </Collapse>
                </Fragment>
              );
            })}
          </List>
        </div>
      )}
    </div>
  );
};

export default withRouter(Aide);
