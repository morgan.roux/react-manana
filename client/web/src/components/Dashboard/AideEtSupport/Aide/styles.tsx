import { Theme, createStyles, makeStyles, lighten } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    aideRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      height: 'auto',
      minHeight: 'fit-content',
      '& *': {
        letterSpacing: 0,
        fontFamily: 'Roboto',
      },
    },
    head: {
      width: '100%',
      height: 70,
      backgroundColor: lighten(theme.palette.primary.main, 0.1),
      color: theme.palette.common.white,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    headTitle: {
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 20,
    },
    contentTitle: { fontFamily: 'Roboto', marginBottom: 22 },
    searchInput: {
      marginBottom: 33,
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      '& > div': {
        marginBottom: 0,
      },
      '& > button': {
        marginLeft: 30,
        minWidth: 'fit-content',
        height: 48,
      },
    },
    content: {
      width: '75%',
      margin: '65px 275px',
    },
    aidesTitle: {
      backgroundColor: lighten(theme.palette.primary.main, 0.2),
      height: 72,
      justifyContent: 'flex-start',
      padding: 25,
      '& > p': {
        marginBottom: 0,
      },
    },
    listRoot: {},
    nested: {
      paddingLeft: theme.spacing(4),
    },
    aideTitle: {
      height: 108,
      '& .MuiTypography-root': {
        fontSize: 16,
        fontWeight: 500,
      },
      '& .MuiSvgIcon-root': {
        width: 46,
        height: 46,
      },
    },
    descriptionContainer: {},
    description: {
      fontSize: 16,
      fontWeight: 'normal',
      margin: '35px 70px',
    },
  }),
);

export default useStyles;
