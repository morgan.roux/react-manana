import SwiperCore, { Pagination, A11y } from 'swiper';
import {
  Avatar,
  Box,
  Menu,
  MenuItem,
  Select,
  SwipeableDrawer,
  Typography,
} from '@material-ui/core';
import { ArrowBack, Call, CallReceived, Delete, Edit, MoreHorizRounded } from '@material-ui/icons';
import React, { FC, ReactElement, ReactNode, useCallback, useEffect, useState } from 'react';
import { isMobile, stringToAvatar } from '../../../../utils/Helpers';
import { ListPerson } from '../../../Common/ListPerson';
import { SearchInput } from '../../Content/SearchInput';
import useStyles from './styles';
import {
  Column,
  ConfirmDeleteDialog,
  CustomButton,
  ErrorPage,
  Table,
  TableChange,
} from '@app/ui-kit';
import { Add } from '@material-ui/icons';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.min.css';
import { Data, ModalFormAddCall } from './ModalFormAddCall';
import {
  CREATE_SUIVI_APPEL,
  UPDATE_SUIVI_APPEL,
  DELETE_SUIVI_APPEL,
  CHANGE_STATUS_SUIVI_APPEL,
} from '../../../../federation/basis/suiviAppel/mutation';
import {
  CREATE_SUIVI_APPEL as CREATE_SUIVI_APPEL_TYPE,
  CREATE_SUIVI_APPELVariables,
} from '../../../../federation/basis/suiviAppel/types/CREATE_SUIVI_APPEL';
import {
  CHANGE_STATUS_SUIVI_APPEL as CHANGE_STATUS_SUIVI_APPEL_TYPE,
  CHANGE_STATUS_SUIVI_APPELVariables,
} from '../../../../federation/basis/suiviAppel/types/CHANGE_STATUS_SUIVI_APPEL';
import {
  DELETE_SUIVI_APPEL as DELETE_SUIVI_APPEL_TYPE,
  DELETE_SUIVI_APPELVariables,
} from '../../../../federation/basis/suiviAppel/types/DELETE_SUIVI_APPEL';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { FEDERATION_CLIENT } from '../../DemarcheQualite/apolloClientFederation';
import { uploadToS3 } from '../../../../services/S3';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import {
  UPDATE_SUIVI_APPEL as UPDATE_SUIVI_APPEL_TYPE,
  UPDATE_SUIVI_APPELVariables,
} from '../../../../federation/basis/suiviAppel/types/UPDATE_SUIVI_APPEL';
import { SEARCH_search_data_SuiviAppelType } from '../../../../federation/search/types/SEARCH';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../graphql/S3/mutation';
import { FichierInput } from '../../../../types/graphql-global-types';
import { formatFilename } from '../../../Common/Dropzone/Dropzone';
import { getUser, getPharmacie } from '../../../../services/LocalStorage';
import { useSearch } from '../../../Common/useSearch';
import moment from 'moment';
import { AvatarGroup } from '@material-ui/lab';
import { GET_SUIVI_APPELS_BY_INTERLOCUTEUR } from '../../../../federation/basis/suiviAppel/query';
import {
  GET_SUIVI_APPELS_BY_INTERLOCUTEUR as GET_SUIVI_APPELS_BY_INTERLOCUTEUR_TYPE,
  GET_SUIVI_APPELS_BY_INTERLOCUTEURVariables,
} from '../../../../federation/basis/suiviAppel/types/GET_SUIVI_APPELS_BY_INTERLOCUTEUR';
import 'swiper/components/pagination/pagination.min.css';
import { getOrigin } from '../../../User/UserSelect/columns';

SwiperCore.use([Pagination, A11y]);

const DetailComp: FC<any> = props => {
  const styles = useStyles();
  return (
    <>
      {Object.entries(props).map(([key, value], i) => (
        <div key={i} className={styles.detailItem}>
          <div className={styles.key}>{key}</div>
          <div>{value as any}</div>
        </div>
      ))}
    </>
  );
};
interface SelectCompProps {
  id: string;
  value: string;
  handleChange: (value: string, id: string) => void;
}

const SelectComp: FC<SelectCompProps> = ({ value, handleChange, id }) => {
  const [val, setVal] = useState<number>(1);
  const handleChangeValue = (event: any) => {
    setVal(event.target.value);
    const valeur = event.target.value;
    handleChange(
      valeur === 1 ? 'Nouvelle' : valeur === 2 ? 'En cours' : valeur === 3 ? 'Clôturée' : '',
      id,
    );
  };

  useEffect(() => {
    setVal(value === 'Nouvelle' ? 1 : value === 'En cours' ? 2 : value === 'Clôturée' ? 3 : 0);
  }, [value]);

  return (
    <>
      <Select
        style={{ minWidth: 120 }}
        variant="standard"
        value={val}
        onChange={handleChangeValue}
        displayEmpty
      >
        <MenuItem value={1}>Nouvelle</MenuItem>
        <MenuItem value={2}>En cours</MenuItem>
        <MenuItem value={3}>Clôturée</MenuItem>
      </Select>
    </>
  );
};

interface MenuELprops {
  data: any;
  onEditClick?: (data: any) => void;
  onDeleteClick?: (id: string) => void;
}

const MenuEl: FC<MenuELprops> = ({ data, onDeleteClick, onEditClick }) => {
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);

  const openMenu = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
  };

  const onClose = () => {
    setAnchorEl(null);
  };

  const handleEditClick = useCallback(() => {
    if (onEditClick) onEditClick(data);
    onClose();
  }, [onEditClick, data]);

  const handleDeleteClick = useCallback(() => {
    if (onDeleteClick && data.id) onDeleteClick(data.id);
    onClose();
  }, [onDeleteClick, data.id]);

  return (
    <>
      <span onClick={openMenu}>
        <MoreHorizRounded />
      </span>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={onClose}
      >
        <MenuItem onClick={handleEditClick}>
          <Edit />
          &nbsp;
          <Typography variant="inherit">Modifier</Typography>
        </MenuItem>
        <MenuItem onClick={handleDeleteClick}>
          <Delete />
          &nbsp;
          <Typography variant="inherit">Suprimer</Typography>
        </MenuItem>
      </Menu>
    </>
  );
};

export interface SuiviAppleMobileProps {
  search: {
    onSearch: (value: string) => void;
    placeholder: string;
  };
  drawer: {
    open: boolean;
    onClose: () => void;
    onOpen: () => void;
    initSlide?: number;
    loading: boolean;
    data: any[];
    title: string;
  };
  listPerson: {
    loading: boolean;
    onNext: () => void;
    onClickItem: (data: any) => void;
    data: any[];
    actionsRenderer: (arg: any) => ReactNode;
    onScroll?: () => void;
  };
  fab?: {
    variant: 'round' | 'extended';
    color?: 'inherit' | 'primary' | 'secondary' | 'default';
    icon: ReactElement;
    onClick: () => void;
  };
  banner: {
    goBack: () => void;
    other?: {
      icon: ReactElement;
      onClick: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    };
    title: string;
  };
}

export const SuiviAppleMobile: FC<SuiviAppleMobileProps> = ({
  search,
  drawer,
  listPerson,
  fab,
  banner,
}) => {
  const styles = useStyles();

  return (
    <>
      <div className={styles.bannerMobile}>
        <div className={styles.goBack} onClick={banner.goBack}>
          <ArrowBack />
        </div>
        <div className={styles.title}>{banner.title}</div>
        <div onClick={banner.other?.onClick}>{banner.other?.icon}</div>
      </div>
      <div className={styles.rootMobile}>
        <div className={styles.searchInputContainer}>
          <SearchInput onChange={search.onSearch} placeholder={search.placeholder} />
        </div>
        <SwipeableDrawer
          open={drawer.open}
          onOpen={drawer.onOpen}
          onClose={drawer.onClose}
          anchor="bottom"
          classes={{
            paperAnchorBottom: styles.drawerStyles,
          }}
        >
          <div className={styles.closeDrawer} onClick={drawer.onClose} />
          <div className={styles.detailContainer}>
            <h2 style={{ marginLeft: 8 }}>{drawer.title}</h2>
            {drawer.loading ? (
              <Box marginTop={20} display="flex" width="100%" justifyContent="center">
                <h2>Chargement en cours...</h2>
              </Box>
            ) : (
              <>
                <Swiper
                  pagination={{ type: 'fraction', el: '.page' }}
                  spaceBetween={16}
                  slidesPerView={1}
                  className={styles.swiperWrapper}
                  initialSlide={drawer.initSlide || 0}
                >
                  {drawer.data.map((item, i) => (
                    <SwiperSlide key={i} className={styles.detail}>
                      <div className={styles.detailContent}>
                        <DetailComp {...item} />
                      </div>
                    </SwiperSlide>
                  ))}
                </Swiper>
                <Box
                  marginTop={1}
                  display="flex"
                  width="100%"
                  alignItems="center"
                  justifyContent="center"
                >
                  <div className={`page ${styles.page}`}></div>
                </Box>
              </>
            )}
          </div>
        </SwipeableDrawer>
        <ListPerson
          onscroll={listPerson.onScroll}
          onNext={listPerson.onNext}
          onClickItem={listPerson.onClickItem}
          listes={listPerson.data}
          actionsRenderer={listPerson.actionsRenderer}
          fab={
            fab
              ? {
                  color: fab?.color,
                  variant: fab?.variant || 'round',
                  icon: fab?.icon as any,
                  onClick: fab?.onClick as any,
                }
              : undefined
          }
          loading={listPerson.loading || false}
        />
      </div>
    </>
  );
};

const columns: Column[] = [
  { name: 'fullName', label: 'Nom et Prenom' },
  { name: 'fonction', label: 'Fonction' },
  { name: 'DateHeure', label: 'Date et Heure' },
  { name: 'details', label: 'Détails' },
  { name: 'origin', label: 'Origine' },
  { name: 'urgence', label: 'Urgence' },
  { name: 'importance', label: 'Importance' },
  { name: 'concernee', label: 'Concernées' },
  { name: 'status', label: 'status' },
  { name: 'show', label: '' },
  { name: 'action', label: '' },
];

const SuiviAppels: FC = () => {
  const styles = useStyles();
  const [openDrawer, setOpenDrawer] = useState<boolean>(false);
  const [openModalAddForm, setOpenModalAddForm] = useState<boolean>(false);
  const [modeModalForm, setModeModalForm] = useState<'ajout' | 'modification'>('ajout');
  const [skip, setSkip] = useState<number>(0);
  const [take, setTake] = useState<number>(12);
  const [searchText, setSearchText] = useState<string | undefined>();
  const [openConfigDialog, setOpenConfigDialog] = useState<boolean>(false);
  const [idToDel, setIdToDel] = useState<string | undefined>();
  const [valueToEdit, setValueToEdit] = useState<Data | undefined>();
  const [dataListPerson, setDataListPerson] = useState<any[]>([]);
  const [dataDrawer, setDataDrawer] = useState<any[]>([]);
  const [merge, setMerge] = useState<boolean>(true);

  const [doCreatePutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    client: FEDERATION_CLIENT,
  });

  const [search, searchResult] = useSearch<SEARCH_search_data_SuiviAppelType>();

  const [createSuiviAppel, creationSuiviAppel] = useMutation<
    CREATE_SUIVI_APPEL_TYPE,
    CREATE_SUIVI_APPELVariables
  >(CREATE_SUIVI_APPEL, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      window.location.reload();
    },
  });

  const [changeStatus, changingStatus] = useMutation<
    CHANGE_STATUS_SUIVI_APPEL_TYPE,
    CHANGE_STATUS_SUIVI_APPELVariables
  >(CHANGE_STATUS_SUIVI_APPEL, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      searchResult.refetch();
    },
  });

  const [deleteOneSuiviAppel, deletingSuiviAppel] = useMutation<
    DELETE_SUIVI_APPEL_TYPE,
    DELETE_SUIVI_APPELVariables
  >(DELETE_SUIVI_APPEL, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      searchResult.refetch();
    },
  });

  const [updateSuiviAppel, updatetinSuiviAppel] = useMutation<
    UPDATE_SUIVI_APPEL_TYPE,
    UPDATE_SUIVI_APPELVariables
  >(UPDATE_SUIVI_APPEL, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      searchResult.refetch();
      handleOpenModal();
    },
  });

  const [loadSuiviAppelByInterlocuteur, loadingSuiviAppleByInterlocuteur] = useLazyQuery<
    GET_SUIVI_APPELS_BY_INTERLOCUTEUR_TYPE,
    GET_SUIVI_APPELS_BY_INTERLOCUTEURVariables
  >(GET_SUIVI_APPELS_BY_INTERLOCUTEUR, {
    client: FEDERATION_CLIENT,
    onCompleted: data => {
      setDataDrawer(
        data.getSuivisAppelsByIdInterlocuteur.map(
          row =>
            ({
              'Nom de Prenom': row.interlocuteur.fullName,
              fonction: row.interlocuteur.role?.nom,
              DateHeure: moment(row.createdAt).format('DD/MM/YYYY HH:mm'),
              Origine: getOrigin(row.interlocuteur.role?.code || ''),
              urgence: (
                <div style={{ color: row.urgence?.couleur }}>
                  {row.urgence?.code}&nbsp;{row.urgence?.libelle}
                </div>
              ),
              importance: row.importance.libelle,
              'concerné(es)': (
                <AvatarGroup max={5}>
                  {(row.concernes || []).map(user => {
                    return user.photo && user.photo.publicUrl ? (
                      <Avatar
                        className={styles.avatar}
                        src={user.photo.publicUrl}
                        alt={user.fullName || ''}
                      />
                    ) : (
                      <Avatar className={styles.avatar}>
                        {stringToAvatar(user.fullName || '')}
                      </Avatar>
                    );
                  })}
                </AvatarGroup>
              ),
              status: (
                <SelectComp
                  id={row.id}
                  value={row.status || ''}
                  handleChange={handleChangeStatus}
                />
              ),
            } as any),
        ),
      );
    },
  });
  const handleOpenModal = () => {
    setOpenModalAddForm(prev => !prev);
  };

  const handleClickItem = (data: any) => {
    loadSuiviAppelByInterlocuteur({
      variables: {
        idUserInterlocuteur: data.idUserInterlocuteur,
      },
    });
    setOpenDrawer(true);
  };

  const handleClickAddCall = () => {
    setValueToEdit(undefined);
    handleOpenModal();
    setMerge(false);
  };

  const handleSave = (data: Data) => {
    if (data.fichiers && data.fichiers.length > 0) {
      const filePaths = data.fichiers.map(fichier => formatFilename(fichier, ``));
      doCreatePutPresignedUrl({
        variables: {
          filePaths,
        },
      }).then(async response => {
        if (response.data?.createPutPresignedUrls) {
          try {
            const fichiers = await Promise.all(
              response.data.createPutPresignedUrls.map((item, index) => {
                if (item?.presignedUrl) {
                  return data.fichiers
                    ? uploadToS3(data.fichiers[index], item?.presignedUrl).then(uploadResult => {
                        if (uploadResult.status === 200 && uploadResult.statusText === 'OK') {
                          return {
                            chemin: item.filePath,
                            nomOriginal: data.fichiers ? data.fichiers[index].name : '',
                            type: data.fichiers ? data.fichiers[index].type : '',
                          };
                        }
                      })
                    : null;
                }
              }),
            );
            saveSuiviAppel(data, fichiers as FichierInput[]);
          } catch (error) {
            alert(error.message);
          }
        } else {
          alert('erreur 404 !!!');
        }
      });
    } else {
      saveSuiviAppel(data, undefined);
    }
  };

  const saveSuiviAppel = (data: Data, fichiers: FichierInput[] | undefined) => {
    if (data.id) {
      updateSuiviAppel({
        variables: {
          id: data.id,
          input: {
            idImportance: data.idImportance || '',
            idFonction: data.idFonction,
            idTache: data.idTache,
            idUrgence: data.idUrgence || '',
            idUserDeclarant: getUser().id,
            idUserInterlocuteur: data.idUserInterlocuteur || '',
            idUserParticipants: data.concernesIds,
            commentaire: data.commentaire,
            fichiers,
          },
        },
      });
    } else {
      if (data.idImportance && data.idUrgence && data.idUserInterlocuteur) {
        createSuiviAppel({
          variables: {
            input: {
              idImportance: data.idImportance,
              idFonction: data.idFonction,
              idTache: data.idTache,
              idUrgence: data.idUrgence,
              idUserDeclarant: getUser().id,
              idUserInterlocuteur: data.idUserInterlocuteur,
              idUserParticipants: data.concernesIds,
              commentaire: data.commentaire,
              fichiers,
            },
          },
        });
      }
    }
  };

  const handleChangeTable = (change: TableChange) => {
    setSkip(change.skip);
    setTake(change.take);
    handleSearch(change.skip, change.take, searchText);
  };

  const handleSearch = (skip: number, take: number, searchText: string | undefined) => {
    console.log('search DATA');
    search({
      variables: {
        skip: skip,
        take: take,
        query: {
          query: {
            bool: {
              must: [
                { term: { idPharmacie: getPharmacie().id } },
                {
                  query_string: {
                    query: `*${searchText || ''}*`,
                    analyze_wildcard: true,
                    fields: [],
                  },
                },
              ],
            },
          },
          sort: [{ createdAt: { order: 'desc' } }],
        },

        index: ['suiviappeltype'],
      },
    });
  };

  const handleChangeSearch = (value: string) => {
    handleSearch(0, 12, value);
    setSearchText(value);
    setMerge(false);
  };

  const handleChangeStatus = (value: string, id: string) => {
    changeStatus({
      variables: {
        id,
        status: value,
      },
    });
    setMerge(false);
  };

  const handleDeleteMenuClick = (id: string) => {
    setOpenConfigDialog(true);
    setIdToDel(id);
  };

  const deleteSuiviAppel = (id: string) => {
    deleteOneSuiviAppel({
      variables: {
        id,
      },
    });
    setOpenConfigDialog(false);
  };

  const handleEditClick = (data: SEARCH_search_data_SuiviAppelType) => {
    setModeModalForm('modification');
    setOpenModalAddForm(true);
    setValueToEdit({
      id: data.id,
      idImportance: data.idImportance,
      idTache: data.idTache,
      commentaire: data.commentaire || undefined,
      idUserInterlocuteur: data.idUserInterlocuteur,
      idFonction: data.idFonction,
      idUrgence: data.idUrgence,
      concernes:
        data.idTache === '' && data.idFonction === '' ? data.concernes || undefined : undefined,
      interlocuteur: data.interlocuteur,
    });
  };

  const handleOneNext = () => {
    handleSearch(skip + take, take, searchText);
    setSkip(skip + take);
    setMerge(true);
  };

  useEffect(() => {
    if (isMobile()) handleSearch(skip, take, searchText);
  }, []);

  useEffect(() => {
    if (isMobile()) {
      if (searchResult.data) {
        if (merge) {
          setDataListPerson(
            dataListPerson.concat(
              (searchResult.data || []).map(row => ({
                id: row.id,
                nom: row.interlocuteur.fullName,
                autres: [`Il y a ${moment(row.createdAt).fromNow(true)}`],
                src: row.interlocuteur.photo?.publicUrl,
                idUserInterlocuteur: row.idUserInterlocuteur,
                interlocuteur: row.interlocuteur,
              })),
            ),
          );
        } else {
          setDataListPerson(
            (searchResult.data || []).map(row => ({
              id: row.id,
              nom: row.interlocuteur.fullName,
              autres: [`Il y a ${moment(row.createdAt).fromNow(true)}`],
              src: row.interlocuteur.photo?.publicUrl,
              idUserInterlocuteur: row.idUserInterlocuteur,
              interlocuteur: row.interlocuteur,
            })),
          );
        }
      }
    }
  }, [searchResult.data]);

  useEffect(() => {
    setSkip(0);
    setTake(12);
    if (isMobile()) {
      if (searchText && searchText !== '') {
        setDataListPerson([]);
        setMerge(false);
      } else {
        handleSearch(0, 12, '');
      }
    }
  }, [searchText]);

  const goBack = () => {};

  return (
    <>
      {isMobile() ? (
        <SuiviAppleMobile
          banner={{
            title: 'Liste des appels reçus',
            goBack: goBack,
          }}
          search={{
            onSearch: handleChangeSearch,
            placeholder: 'Rechercher par nom ou téléphone',
          }}
          drawer={{
            title: 'Détails appels',
            onClose: () => setOpenDrawer(false),
            onOpen: () => setOpenDrawer(true),
            open: openDrawer,
            loading: loadingSuiviAppleByInterlocuteur.loading,
            data: dataDrawer,
          }}
          listPerson={{
            onNext: handleOneNext,
            onClickItem: handleClickItem,
            data: dataListPerson,
            loading: searchResult.loading || false,
            actionsRenderer: (data: any) => {
              return data.interlocuteur.phoneNumber ? (
                <a
                  style={{ textDecoration: 'none', color: '#000000' }}
                  href={`tel:${data.interlocuteur.phoneNumber}`}
                >
                  <Call />
                </a>
              ) : (
                <Call />
              );
            },
          }}
          fab={{
            onClick: handleClickAddCall,
            variant: 'round',
            color: 'secondary',
            icon: <Add />,
          }}
        />
      ) : (
        <div className={styles.rootWeb}>
          <div className={styles.banner}>
            <h2>Suivi des apples reçus</h2>
          </div>
          <div className={styles.tableContainer}>
            <div className={styles.headerTable}>
              <div className={styles.titleTab}>
                <CallReceived />
                <h2>&nbsp;Liste des appels reçus</h2>
              </div>
              <div className={styles.actionHeaderTable}>
                <div className={styles.actionHeaderTableSearch}>
                  <SearchInput onChange={handleChangeSearch} placeholder="Recherche" />
                </div>
                <div className={styles.actionHeaderTableAdd}>
                  <CustomButton onClick={handleClickAddCall} color="secondary" startIcon={<Add />}>
                    AJOUTER UN APPEL
                  </CustomButton>
                </div>
              </div>
            </div>
            <Table
              loading={searchResult.loading}
              error={searchResult.error}
              columns={columns}
              onRunSearch={handleChangeTable}
              data={(searchResult.data || []).map(row => ({
                fullName: row.interlocuteur?.fullName,
                fonction: row.interlocuteur.role?.nom,
                DateHeure: moment(row.createdAt).format('DD/MM/YYYY HH:mm'),
                origin: getOrigin(row.interlocuteur.role?.code || ''),
                details: <div dangerouslySetInnerHTML={{ __html: row.commentaire || '-' }} />,
                urgence: (
                  <div style={{ color: row.urgence?.couleur }}>
                    {row.urgence?.code}&nbsp;{row.urgence?.libelle}
                  </div>
                ),
                importance: row.importance?.libelle,
                concernee: (
                  <AvatarGroup max={10}>
                    {(row.concernes || []).map(user => {
                      return user.photo && user.photo.publicUrl ? (
                        <Avatar src={user.photo.publicUrl} alt={user.fullName || ''} />
                      ) : (
                        <Avatar style={{ width: 30, height: 30, fontSize: 12 }}>
                          {stringToAvatar(user.fullName || '')}
                        </Avatar>
                      );
                    })}
                  </AvatarGroup>
                ),
                status: (
                  <SelectComp
                    id={row.id}
                    value={row.status || ''}
                    handleChange={handleChangeStatus}
                  />
                ),
                show: '-',
                action: (
                  <MenuEl
                    data={row}
                    onDeleteClick={handleDeleteMenuClick}
                    onEditClick={handleEditClick}
                  />
                ),
              }))}
              search={false}
              rowsTotal={searchResult.total || 0}
            />
            {searchResult.error && <ErrorPage />}
          </div>
          <ConfirmDeleteDialog
            title="Suppression"
            content="Etes-vous sûre de vouloir supprimer cet appel ?"
            open={openConfigDialog}
            setOpen={setOpenConfigDialog}
            onClickConfirm={() => (idToDel ? deleteSuiviAppel(idToDel) : null)}
          />
        </div>
      )}
      <ModalFormAddCall
        onSave={handleSave}
        open={openModalAddForm}
        onOpen={handleOpenModal}
        mode={modeModalForm}
        value={valueToEdit}
      />
    </>
  );
};

export default SuiviAppels;
