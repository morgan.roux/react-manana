import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { differenceBy } from 'lodash';
import { Dispatch, SetStateAction, useContext } from 'react';
import { ContentContext, ContentStateInterface } from '../../../../AppContext';
import { DO_SOFT_DELETE_COULEURS } from '../../../../graphql/Color';
import {
  SOFT_DELETE_COULEURS,
  SOFT_DELETE_COULEURSVariables,
} from '../../../../graphql/Color/types/SOFT_DELETE_COULEURS';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

export const useDeleteColor = (
  selected: any[],
  setSelected: Dispatch<SetStateAction<any[]>>,
  deleteRow: any,
  setDeleteRow: Dispatch<SetStateAction<any>>,
) => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();
  console.log('deleteRow :>> ', deleteRow);

  const selectedIds = (selected.length > 0 && selected.map(i => i.id)) || [];

  const [deleteColors, { loading, data }] = useMutation<
    SOFT_DELETE_COULEURS,
    SOFT_DELETE_COULEURSVariables
  >(DO_SOFT_DELETE_COULEURS, {
    variables: { ids: selectedIds },
    update: (cache, { data }) => {
      if (data && data.softDeleteCouleurs) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.softDeleteCouleurs;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables,
            });
            // client.writeData({
            //   data: { checkedsGroupeClient: differenceBy(selected, result, 'id') },
            // });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    onCompleted: data => {
      if (data && data.softDeleteCouleurs) {
        displaySnackBar(client, { isOpen: true, type: 'SUCCESS', message: 'Suppression réussie' });
        setSelected([]);
        setDeleteRow(null);
      }
    },
  });

  return { deleteColors, loading, data };
};
