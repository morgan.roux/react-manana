import React, { useState, ReactNode, Dispatch, SetStateAction, FC, useEffect } from 'react';
import useStyles from './styles';
import {
  Stepper,
  StepLabel,
  Typography,
  Box,
  Step,
  RadioGroup,
  FormControlLabel,
  Radio,
  capitalize,
  Grid,
  FormLabel,
} from '@material-ui/core';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import Dropzone from '../../../Common/Dropzone';
import CustomTextarea from '../../../Common/CustomTextarea';
import { PubInterface } from './useFormPub';
import { CustomDatePicker, CustomTimePicker } from '../../../Common/CustomDateTimePicker';
import moment from 'moment';
import { OriginePublicite, TypeEspace, TypeFichier } from '../../../../types/graphql-global-types';
import { PUB_EMPLACEMENT_LIST, PUB_BASE_URL } from '../../../../Constant/publicite';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import {
  GET_PUBLICITE,
  GET_PUBLICITEVariables,
} from '../../../../graphql/Publicite/types/GET_PUBLICITE';
import { useLazyQuery, useApolloClient, useQuery } from '@apollo/react-hooks';
import { DO_GET_PUBLICITE } from '../../../../graphql/Publicite';
import Backdrop from '../../../Common/Backdrop';
import { last } from 'lodash';
import { AWS_HOST } from '../../../../utils/s3urls';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import CustomTree from '../../../Common/CustomTree';
import CustomSelect from '../../../Common/CustomSelect';
import classnames from 'classnames';
import { isUrlValid } from '../../../../utils/Validator';

export interface FormValidation {
  stepOne: boolean;
  stepTwo: boolean;
}

export interface FormPubProps {
  activeStepId: number;
  setActiveStepId: Dispatch<SetStateAction<number>>;
  steps: StepInterface[];
  formIsValid: FormValidation;
  setFormIsValid: Dispatch<SetStateAction<FormValidation>>;
}

export interface FormPubStepProps {
  values: PubInterface;
  setValues: Dispatch<SetStateAction<PubInterface>>;
  selectedFiles?: File[];
  setSelectedFiles?: Dispatch<SetStateAction<File[] | undefined>>;
  handleChange: (e: React.ChangeEvent<any>) => void;
  handleChangeDate: (name: string) => (date: any) => void;
}

export interface StepInterface {
  id: number;
  label: string;
  content: ReactNode;
}

export const StepOne: FC<FormPubStepProps> = ({
  values,
  setValues,
  selectedFiles,
  setSelectedFiles,
  handleChange,
}) => {
  const classes = useStyles({});

  const { libelle, description, url, image, item, codeItem, idItemSource, origine } = values;
  const ORIGINE_INT = OriginePublicite.INTERNE;
  const ORIGINE_EXT = OriginePublicite.EXTERNE;
  const [origineType, setOrigineType] = useState<string>(origine || ORIGINE_EXT);
  const [imageUrl, setImageUrl] = useState<string | undefined>(undefined);

  const initOrigineTypeFields = () => {
    if (url !== null) {
      setValues(prevState => ({ ...prevState, url: null }));
    }
    if (codeItem !== null) {
      setValues(prevState => ({ ...prevState, codeItem: null }));
    }
    if (idItemSource !== null) {
      setValues(prevState => ({ ...prevState, idItemSource: null }));
    }
    if (item !== undefined) {
      setValues((prevState: any) => ({ ...prevState, item: undefined }));
    }
  };

  const handleChangeOrigineType = event => {
    setOrigineType(event.target.value);
    setValues(prevState => ({ ...prevState, origine: event.target.value }));
    // initOrigineTypeFields();
  };

  const setItem = (item: any) => {
    setValues(prevState => ({ ...prevState, item }));
  };

  /**
   * Update origineType
   */
  useEffect(() => {
    if (origine !== origineType) {
      setOrigineType(origine);
    }
  }, [origine]);

  // TODO: Mutation delete
  const deletePubPhoto = () => {
    console.log(' Delete Pub Photo :>> ');
  };

  /**
   * Set image url
   */
  useEffect(() => {
    if (image && image.chemin) {
      setImageUrl(`${AWS_HOST}/${image.chemin}`);
    }

    if (selectedFiles && selectedFiles.length > 0) {
      const file = last(selectedFiles);
      if (file) {
        const url = URL.createObjectURL(file);
        setImageUrl(url);
      }
    }
  }, [image, selectedFiles]);

  return (
    <div className={classes.formContainer}>
      <Typography className={classes.formSubTitle}>Image de la pubilcité</Typography>
      <div className={classes.formPubDropzone}>
        <Dropzone
          contentText="Glissez et déposez ici votre photo"
          selectedFiles={selectedFiles}
          setSelectedFiles={setSelectedFiles}
          multiple={false}
          acceptFiles="image/*"
          withFileIcon={false}
          where="inPubForm"
          withImagePreview={true}
          withImagePreviewCustomized={true}
          onClickDelete={deletePubPhoto}
          fileAlreadySetUrl={imageUrl}
        />
      </div>
      <Typography className={classes.formSubTitle}>Description de la publicité</Typography>
      <CustomFormTextField
        label="Libellé"
        name="libelle"
        value={libelle}
        onChange={handleChange}
        required={true}
      />
      <CustomTextarea
        label="Description"
        name="description"
        value={description as any}
        onChangeTextarea={handleChange}
        rows={3}
        required={true}
        className={classes.formPubTextArea}
      />
      <Typography className={classes.formSubTitle}>Origines</Typography>
      <RadioGroup
        aria-label="origineType"
        name="origineType"
        value={origineType}
        onChange={handleChangeOrigineType}
        className={classes.formPubRadioGroup}
      >
        <FormControlLabel
          value={ORIGINE_INT}
          control={<Radio />}
          label={capitalize(ORIGINE_INT.toLowerCase())}
        />
        <FormControlLabel
          value={ORIGINE_EXT}
          control={<Radio />}
          label={capitalize(ORIGINE_EXT.toLowerCase())}
        />
      </RadioGroup>
      {origineType === ORIGINE_INT && (
        <>
          <Box marginBottom="25px !important">
            <fieldset className={classes.fieldset}>
              <legend>
                <FormLabel>Choix des items</FormLabel>
              </legend>
              <CustomTree
                type="item"
                nodeKey="codeItem"
                nodeLabel="name"
                nodeParent="parent"
                multiple={false}
                setChecked={setItem}
                checked={item}
              />
            </fieldset>
          </Box>
        </>
      )}
      {origineType === ORIGINE_EXT && (
        <CustomFormTextField
          label="URL"
          name="url"
          value={url}
          onChange={handleChange}
          required={true}
        />
      )}
    </div>
  );
};

export const StepTwo: FC<FormPubStepProps> = ({ values, handleChange, handleChangeDate }) => {
  const classes = useStyles({});
  const { dateDebut, dateFin, ordre } = values;

  console.log('dateDebut :>> ', dateDebut);

  return (
    <div className={classes.formContainer}>
      <Typography className={classes.formSubTitle}>Emplacement</Typography>
      <div className={classes.formPubEmplacement}>
        <Typography>Ordre</Typography>
        <CustomSelect
          label=""
          list={PUB_EMPLACEMENT_LIST}
          listId="id"
          index="value"
          name="ordre"
          value={ordre}
          onChange={handleChange}
          shrink={false}
        />
      </div>
      <Typography className={classes.formSubTitle}>Période d'affichage</Typography>
      <Box>
        <Grid container={true} spacing={5}>
          <Grid item={true} xs={6}>
            <CustomDatePicker
              label="Date début"
              placeholder="Date début"
              onChange={handleChangeDate('dateDebut')}
              name="dateDebut"
              value={dateDebut}
              className={classes.dateInput}
              disablePast={false}
            />
            <CustomDatePicker
              label="Date fin"
              placeholder="Date fin"
              onChange={handleChangeDate('dateFin')}
              name="dateFin"
              value={dateFin}
              className={classes.dateInput}
              minDate={dateDebut}
              disablePast={false}
            />
          </Grid>
          <Grid item={true} xs={6}>
            <CustomTimePicker
              label="Heure début"
              placeholder="Heure début"
              onChange={handleChangeDate('dateDebut')}
              name="dateDebut"
              value={dateDebut}
              className={classes.dateInput}
            />
            <CustomTimePicker
              label="Heure fin"
              placeholder="Heure fin"
              onChange={handleChangeDate('dateFin')}
              name="dateFin"
              value={dateFin}
              className={classes.dateInput}
            />
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};

const FormPub: FC<FormPubProps & FormPubStepProps & RouteComponentProps> = ({
  steps,
  activeStepId,
  setActiveStepId,
  values,
  setValues,
  formIsValid,
  setFormIsValid,
  selectedFiles,
  match: { params },
  location: { pathname },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const activeStep = steps && steps.find((step: StepInterface) => step.id === activeStepId);
  const stepsLength = steps.length;
  const isOnLastStep: boolean = activeStepId >= stepsLength - 1;
  const isOnFirstStep: boolean = activeStepId === 0;
  const { pubId } = params as any;
  const EDIT_URL = `${PUB_BASE_URL}/edit/${pubId}`;

  const { image, libelle, description, ordre, dateDebut, dateFin, item, url } = values;

  const startDate = moment(dateDebut);
  const endDate = moment(dateFin);

  const IMAGE_EXIST =
    (image && image.chemin && image.nomOriginal) || (selectedFiles && selectedFiles.length > 0);

  /**
   * TODO: Update form validation (fields...)
   * Check form validation
   */

  useEffect(() => {
    if (isOnFirstStep) {
      if (
        libelle &&
        description &&
        IMAGE_EXIST &&
        ((values.origine === OriginePublicite.EXTERNE && url && isUrlValid(url)) ||
          (values.origine === OriginePublicite.INTERNE && item))
      ) {
        setFormIsValid(prevState => ({ ...prevState, stepOne: true }));
      } else {
        setFormIsValid(prevState => ({ ...prevState, stepOne: false }));
      }
    }

    if (isOnLastStep) {
      if (ordre && startDate.isValid() && endDate.isValid()) {
        setFormIsValid(prevState => ({ ...prevState, stepTwo: true }));
      } else {
        setFormIsValid(prevState => ({ ...prevState, stepTwo: false }));
      }
    }
  }, [values, isOnLastStep, isOnFirstStep, IMAGE_EXIST]);

  /**
   * Set complete stepper on last step if all inputs'is completed
   */
  useEffect(() => {
    if (isOnLastStep && formIsValid.stepTwo && activeStepId === stepsLength - 1) {
      setActiveStepId(prevState => prevState + 1);
    } else if (isOnLastStep && activeStepId === stepsLength && !formIsValid.stepTwo) {
      setActiveStepId(prevState => prevState - 1);
    }
  }, [isOnLastStep, formIsValid, activeStepId, stepsLength]);

  // EDIT
  /**
   * Get pub query
   */
  const [doGetPub, { loading }] = useLazyQuery<GET_PUBLICITE, GET_PUBLICITEVariables>(
    DO_GET_PUBLICITE,
    {
      fetchPolicy: 'network-only',
      onError: errors => {
        errors.graphQLErrors.map(err => {
          const snackBarData: SnackVariableInterface = {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        });
      },
      onCompleted: data => {
        if (data && data.publicite) {
          const pub = data.publicite;
          // Set formsInputs for edit
          setValues({
            id: pub.id,
            libelle: pub.libelle,
            description: pub.description,
            typeEspace: pub.typeEspace || TypeEspace.PUBLICITE,
            origine: pub.origineType as any,
            url: pub.url,
            ordre: pub.ordre,
            codeItem: pub.item && pub.item.code,
            idItemSource: pub.idItemSource,
            image: {
              chemin: pub.image && pub.image && (pub.image.chemin as any),
              nomOriginal: pub.image && pub.image && (pub.image.nomOriginal as any),
              type: TypeFichier.PHOTO,
            },
            dateDebut: pub.dateDebut,
            dateFin: pub.dateFin,
            item: pub.item,
            items: [{ id: pub.idItemSource }],
          });
        }
      },
    },
  );

  /**
   * Get publicite for edit
   */
  useEffect(() => {
    if (pathname === EDIT_URL && pubId) {
      // Get pub
      doGetPub({ variables: { id: pubId } });
    }
  }, [pathname, pubId]);

  const noMaxWidth: boolean = item && activeStepId === 1;

  return (
    <div className={classes.formPubRootContainer}>
      <div
        className={
          noMaxWidth
            ? classnames(classes.formPubRoot, classes.formPubRootNoMaxWidth)
            : classes.formPubRoot
        }
      >
        {loading && <Backdrop />}
        <Stepper activeStep={activeStepId} alternativeLabel={true}>
          {steps &&
            steps.map((step: StepInterface) => (
              <Step key={`form_pub_step_label${step.label}`}>
                <StepLabel>{step.label}</StepLabel>
              </Step>
            ))}
        </Stepper>
        <Box width="100%" marginTop="10px">
          {activeStep && activeStep.content}
          {!activeStep && activeStepId === stepsLength && formIsValid && (
            <>{steps && steps[stepsLength - 1].content}</>
          )}
        </Box>
      </div>
    </div>
  );
};

export default withRouter(FormPub);
