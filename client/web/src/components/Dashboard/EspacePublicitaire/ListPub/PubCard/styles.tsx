import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      minHeight: 305,
      marginTop: '15px',
      padding: '10px 10px',
      '& .MuiCardHeader-root': {
        padding: '0 0 3px',
      },
    },
    media: {
      height: 118,
      borderRadius: 6,
      border: '1px solid #E0E0E0',
    },
    cardTitle: {
      '& .MuiCardHeader-title': {
        fontSize: 14,
        fontWeight: 'bold',
        color: '#616161',
      },
      '& .MuiIconButton-root': {
        padding: 0,
      },
      '& .MuiCardHeader-action': {
        marginTop: 0,
        marginRight: 0,
      },
    },
    cardContent: {
      padding: '16px 0 0!important',
    },
    libelle: {
      fontWeight: 'bold',
      fontSize: 16,
      color: theme.palette.common.black,
      marginBottom: 15,
    },
    infosTitles: {
      fontWeight: 'normal',
      fontSize: 12,
      color: '#616161',
      marginBottom: 8,
    },
    infosValues: {
      fontWeight: 'bold',
      fontSize: 14,
      color: '#616161',
    },
    dateHeureContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      width: '100%',
    },
    dateContainer: {
      display: 'flex',
      flexDirection: 'column',
      '& p:nth-last-child(1)': {
        marginBottom: '0px !important',
      },
    },
    menu: {
      '& *': {
        color: theme.palette.common.black,
      },
      '& .MuiListItemIcon-root': {
        minWidth: '35px !important',
      },
      '& .MuiMenuItem-root': {
        paddingTop: '15px !important',
      },
    },
  }),
);

export default useStyles;
