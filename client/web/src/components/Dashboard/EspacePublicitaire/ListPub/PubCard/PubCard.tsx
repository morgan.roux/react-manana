import React, { FC, useState } from 'react';
import {
  Card,
  CardHeader,
  IconButton,
  CardMedia,
  CardContent,
  Typography,
  Menu,
  MenuItem,
  ListItemIcon,
} from '@material-ui/core';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import useStyles from './styles';
import { PubliciteInfo } from '../../../../../graphql/Publicite/types/PubliciteInfo';
import { capitalize } from 'lodash';
import moment from 'moment';
import { Edit, Delete } from '@material-ui/icons';
import { ConfirmDeleteDialog } from '../../../../Common/ConfirmDialog';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import {
  DELETE_PUBLICITE,
  DELETE_PUBLICITEVariables,
} from '../../../../../graphql/Publicite/types/DELETE_PUBLICITE';
import { DO_DELETE_PUBLICITE } from '../../../../../graphql/Publicite';
import Backdrop from '../../../../Common/Backdrop';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { SEARCH, SEARCHVariables } from '../../../../../graphql/search/types/SEARCH';
import { SEARCH as SEARCH_QUERY } from '../../../../../graphql/search/query';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { AppAuthorization } from '../../../../../services/authorization';
import { getUser } from '../../../../../services/LocalStorage';

interface PubCardProps {
  pub: PubliciteInfo;
  queryVariables: any;
}

const PubCard: FC<PubCardProps & RouteComponentProps> = ({
  pub,
  queryVariables,
  history: { push },
}) => {
  const client = useApolloClient();
  const classes = useStyles({});
  const pubImage = (pub.image && pub.image.publicUrl) || '';
  const pubOrigine = capitalize(pub.origineType || '');

  const user = getUser();
  const auth = new AppAuthorization(user);

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [openConfirmDelete, setOpenConfirmDelete] = useState(false);

  const handleClickMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const editPub = () => {
    handleClose();
    push(`/db/espace-publicitaire/edit/${pub.id}`);
  };

  const handleClickDelete = () => {
    handleClose();
    setOpenConfirmDelete(true);
  };

  const [doDeletePub, { loading }] = useMutation<DELETE_PUBLICITE, DELETE_PUBLICITEVariables>(
    DO_DELETE_PUBLICITE,
    {
      variables: { id: pub.id },
      onCompleted: data => {
        if (data && data.deletePublicite) {
          const snackBarData: SnackVariableInterface = {
            type: 'SUCCESS',
            message: 'Publicité supprimée avec succès',
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        }
      },
      update: (cache, { data }) => {
        if (data && data.deletePublicite) {
          const actu = data.deletePublicite;
          const req = cache.readQuery<SEARCH, SEARCHVariables>({
            query: SEARCH_QUERY,
            variables: queryVariables,
          });
          if (req && req.search && req.search.data) {
            cache.writeQuery({
              query: SEARCH_QUERY,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: req.search.data.filter((item: any) => {
                      if (item && item.id !== actu.id) {
                        return item;
                      }
                    }),
                  },
                },
              },
              variables: queryVariables,
            });
          }
        }
      },
      onError: errors => {
        errors.graphQLErrors.map(err => {
          const snackBarData: SnackVariableInterface = {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        });
      },
    },
  );

  const deletePub = () => {
    setOpenConfirmDelete(false);
    doDeletePub();
  };

  const menuItems = [
    {
      label: 'Modifier',
      onClick: editPub,
      icon: <Edit />,
      disabled: !auth.isAuthorizedToEditPub(),
    },
    {
      label: 'Supprimer',
      onClick: handleClickDelete,
      icon: <Delete />,
      disabled: !auth.isAuthorizedToDeletePub(),
    },
  ];

  const CardMenu = () => {
    return (
      <Menu
        id="pub-card-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        getContentAnchorEl={null}
        className={classes.menu}
      >
        {menuItems.map((item, index) => (
          <MenuItem
            onClick={item.onClick}
            key={`card_pub_menu_item_${index}`}
            disabled={item.disabled}
          >
            <ListItemIcon>{item.icon}</ListItemIcon>
            {item.label}
          </MenuItem>
        ))}
      </Menu>
    );
  };

  const ConfirmDeleteContent = () => {
    return (
      <>
        Êtes-vous sûre de vouloir supprimer la publicité : <strong>{pub.libelle}</strong> ?
      </>
    );
  };

  return (
    <>
      <Card className={classes.root}>
        <CardHeader
          className={classes.cardTitle}
          title={pubOrigine}
          action={
            <IconButton aria-label="more-options" size="medium" onClick={handleClickMenu}>
              <MoreHorizIcon />
            </IconButton>
          }
        />
        <CardMedia className={classes.media} image={pubImage} title={pub.libelle || '-'} />
        <CardContent className={classes.cardContent}>
          <Typography className={classes.libelle}>{pub.libelle}</Typography>
          <Typography className={classes.infosTitles}>
            Ordre d'emplacement : <span className={classes.infosValues}>{pub.ordre || '-'}</span>
          </Typography>
          <div className={classes.dateHeureContainer}>
            <div className={classes.dateContainer}>
              <Typography className={classes.infosTitles}>
                Date Début :{' '}
                <span className={classes.infosValues}>
                  {moment(pub.dateDebut).format('DD MMM YYYY') || '-'}
                </span>
              </Typography>
              <Typography className={classes.infosTitles}>
                Date Fin :{' '}
                <span className={classes.infosValues}>
                  {moment(pub.dateFin).format('DD MMM YYYY') || '-'}
                </span>
              </Typography>
            </div>
            <div className={classes.dateContainer}>
              <Typography className={classes.infosTitles}>
                Heure Début :{' '}
                <span className={classes.infosValues}>
                  {moment(pub.dateDebut).format('HH:mm') || '-'}
                </span>
              </Typography>
              <Typography className={classes.infosTitles}>
                Heure Fin :{' '}
                <span className={classes.infosValues}>
                  {moment(pub.dateFin).format('HH:mm') || '-'}
                </span>
              </Typography>
            </div>
          </div>
        </CardContent>
      </Card>
      <CardMenu />
      <ConfirmDeleteDialog
        open={openConfirmDelete}
        setOpen={setOpenConfirmDelete}
        onClickConfirm={deletePub}
        content={<ConfirmDeleteContent />}
      />
      {loading && <Backdrop value="Suppression en cours..." />}
    </>
  );
};

export default withRouter(PubCard);
