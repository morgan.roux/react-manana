import React, { useState, useEffect, useCallback } from 'react';
import useStyles from './styles';
import LeftList from './LeftList';
import MainList from './MainList';
import { useLazyQuery } from '@apollo/react-hooks';
import { SEARCH, SEARCHVariables } from '../../../../graphql/search/types/SEARCH';
import { SEARCH as SEARCH_QUERY } from '../../../../graphql/search/query';
import { Loader } from '../../Content/Loader';
import Backdrop from '../../../Common/Backdrop';
import { getGroupement } from '../../../../services/LocalStorage';

const ListPub = () => {
  const classes = useStyles({});
  const groupement = getGroupement();
  const [pubList, setpubList] = useState<any[]>([]);

  const [queryState, setqueryState] = useState<any>({
    skip: 0,
    take: 12,
    query: '',
    sortBy: [],
    filterBy: [{ term: { isRemoved: false } }, { term: { 'groupement.id': groupement.id } }],
  });

  const { skip, take, query, sortBy, filterBy } = queryState;
  const queryVariables = {
    type: ['publicite'],
    skip,
    take,
    query,
    sortBy,
    filterBy,
  };

  const [search, { loading, error, data, fetchMore }] = useLazyQuery<SEARCH, SEARCHVariables>(
    SEARCH_QUERY,
    {
      variables: queryVariables,
      fetchPolicy: 'network-only',
    },
  );

  /**
   * Lanch query
   */
  useEffect(() => {
    search();
  }, [queryState]);

  /**
   * Set data
   */
  useEffect(() => {
    if (data && data.search && data.search.data) {
      setpubList(data.search.data);
    }
  }, [data]);

  // const pubList: any[] = (data && data.search && data.search.data) || [];
  const pubTotal: number = (data && data.search && data.search.total) || 0;

  const fetchMorePub = () => {
    fetchMore({
      variables: {
        ...queryVariables,
        skip: pubList.length || 0,
      },
      updateQuery: (prev: any, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev;
        return {
          search: {
            ...prev.search,
            data:
              prev &&
              prev.search &&
              prev.search.data &&
              fetchMoreResult.search &&
              fetchMoreResult.search.data
                ? [...prev.search.data, ...fetchMoreResult.search.data]
                : [],
          },
        };
      },
    });
  };

  const handleClickFirstPage = useCallback(() => {
    // setqueryState(prevState => ({ ...prevState, skip: 0, take: 12 }));
  }, []);

  const handleClickLastPage = useCallback(() => {
    if (pubTotal > 12) {
      const newSkip = pubTotal - 12;
      setqueryState(prevState => ({ ...prevState, skip: newSkip, take: 12 }));
    }
  }, []);

  const handleClickPrevPage = useCallback(() => {
    if (pubList.length > 12) {
      const newSkip = pubList.length - 12;
      setqueryState(prevState => ({ ...prevState, skip: newSkip, take: 12 }));
    }
  }, []);

  const handleClickNextPage = useCallback(() => {
    if (pubList.length < pubTotal) {
      const newSkip = pubList.length + 12;
      setqueryState(prevState => ({ ...prevState, skip: newSkip, take: 12 }));
    }
  }, []);

  if (loading && !data && pubList.length === 0) return <Loader />;

  return (
    <div className={classes.listPubRoot}>
      {loading && <Backdrop />}
      <div className={classes.listPubContainer}>
        <LeftList
          list={pubList}
          queryVariables={queryVariables}
          fetchMorePub={fetchMorePub}
          pubTotal={pubTotal}
        />
        <MainList
          list={pubList}
          queryVariables={queryVariables}
          onClickFirstPage={handleClickFirstPage}
          onClickLastPage={handleClickLastPage}
          onClickPrevPage={handleClickPrevPage}
          onClickNextPage={handleClickNextPage}
        />
      </div>
    </div>
  );
};

export default ListPub;
