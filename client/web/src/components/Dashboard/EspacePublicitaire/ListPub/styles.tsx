import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    listPubRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    listPubContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
    },
  }),
);

export default useStyles;
