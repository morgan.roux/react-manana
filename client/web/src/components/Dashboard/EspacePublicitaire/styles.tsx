import { Theme, createStyles, makeStyles, lighten } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    espacePubRoot: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      minHeight: 'fit-content',
      position: 'relative',
      height: 'auto',
    },
    head: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: 70,
      background: lighten(theme.palette.primary.main, 0.1),
      opacity: 1,
      color: theme.palette.common.white,
      padding: '0px 30px',
      position: 'sticky',
      top: 0,
      zIndex: 1,
    },
    headTitle: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 20,
    },
    headBtnsContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      '& > button:not(:nth-last-child(1))': {
        marginRight: 15,
      },
    },
    mutationSuccessContainer: {
      width: '100%',
      minHeight: 'calc(100vh - 156px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& p': {
        textAlign: 'center',
      },
    },
    searchInputBox: {
      padding: '20px 0px 0px 20px',
      display: 'flex',
    },
  }),
);

export default useStyles;
