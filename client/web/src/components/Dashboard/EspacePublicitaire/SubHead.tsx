import { IconButton, Typography } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import React, { FC, ReactNode } from 'react';
import useStyles from './styles';

interface SubHeadProps {
  title: string;
  children: ReactNode;
  showBtnBack?: boolean;
  onClickBack?: () => void;
}

const SubHead: FC<SubHeadProps> = ({ title, children, showBtnBack, onClickBack }) => {
  const classes = useStyles({});
  return (
    <div className={classes.head}>
      {showBtnBack && (
        <IconButton color="inherit" onClick={onClickBack}>
          <ArrowBack />
        </IconButton>
      )}
      <Typography className={classes.headTitle}>{title}</Typography>
      <div className={classes.headBtnsContainer}>{children}</div>
    </div>
  );
};

export default SubHead;
