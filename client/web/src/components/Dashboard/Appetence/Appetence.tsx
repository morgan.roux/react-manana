import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { Box } from '@material-ui/core';
import React, { FC, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { DO_SAVE_ROLE_APPETENCES } from '../../../federation/tools/appetence/mutation';
import { GET_ROLE_APPETENCES } from '../../../federation/tools/appetence/query';
import { GET_ROLE } from '../../../graphql/Role/query';
import { ROLE, ROLEVariables } from '../../../graphql/Role/types/ROLE';
import { capitalizeFirstLetter } from '../../../utils/capitalizeFirstLetter';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import AppetenceList from '../../Common/AppetenceList';
import Backdrop from '../../Common/Backdrop';
import { FEDERATION_CLIENT } from '../DemarcheQualite/apolloClientFederation';
import { useStyles } from './styles';
import SubHeader from './SubHeader';

const Appetence: FC<RouteComponentProps> = ({ match: { params }, history: { push } }) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const { idRole } = params as any;
  const [itemList, setItemList] = useState<any[]>([]);

  const { data, loading } = useQuery<any, any>(GET_ROLE_APPETENCES, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
    variables: {
      idRole,
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  const { data: dataRole, loading: loadingRole } = useQuery<ROLE, ROLEVariables>(GET_ROLE, {
    variables: { id: idRole },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });

  const roleName =
    (dataRole &&
      dataRole.role &&
      dataRole.role.nom &&
      (capitalizeFirstLetter(dataRole.role.nom) as string)) ||
    '';

  const [doSaveRoleAppetences, { loading: mutationLoading }] = useMutation(
    DO_SAVE_ROLE_APPETENCES,
    {
      client: FEDERATION_CLIENT,
      variables: {
        idRole,
        appetences: itemList
          .filter(item => item.ordre)
          .map(item => ({ idItem: item.id, ordre: item.ordre })),
      },
      onCompleted: data => {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: `Scores enregistrés`,
          isOpen: true,
        });
        push('/db/roles');
      },
      onError: error => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: error.message,
          isOpen: true,
        });
      },
    },
  );

  const handleSave = () => {
    doSaveRoleAppetences();
  };

  const handleOrderChange = (id: string, event: any) => {
    const { value } = event.target;

    setItemList(prevState =>
      prevState.map((item: any, index) =>
        item.id === id ? { ...item, ordre: parseInt(value) } : item,
      ),
    );
  };

  return (
    <Box>
      {(loading || mutationLoading) && (
        <Backdrop value={`${mutationLoading ? 'Enregistrement' : 'Chargement'} en cours...`} />
      )}
      <SubHeader {...{ handleSave, roleName }} />
      <Box className={classes.container}>
        <Box className={classes.content}>
          <AppetenceList
            handleOrderChange={handleOrderChange}
            itemList={itemList.filter(e => e.active)}
            setItemList={setItemList}
            loading={loading}
            appetences={data?.roleAppetences}
            withAddBtn={false}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default withRouter(Appetence);
