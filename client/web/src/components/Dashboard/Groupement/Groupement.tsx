import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';
import style from './style';
import Form from '../../Groupement/Form';
import GroupementInterface from '../../../Interface/GroupementInterface';
import SnackVariableInterface from '../../../Interface/SnackVariableInterface';
import { getGroupement, setGroupement } from './../../../services/LocalStorage';
import { DO_UPDATE_GROUPEMENT } from '../../../graphql/Groupement/mutation';
import {
  UPDATE_GROUPEMENT,
  UPDATE_GROUPEMENTVariables,
} from '../../../graphql/Groupement/types/UPDATE_GROUPEMENT';
import { GROUPEMENT, GROUPEMENTVariables } from '../../../graphql/Groupement/types/GROUPEMENT';
import { GET_GROUPEMENT } from '../../../graphql/Groupement/query';
import { useMutation, useQuery, useApolloClient, useLazyQuery } from '@apollo/react-hooks';
import { Loader } from '../../Dashboard/Content/Loader';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import { AxiosResponse } from 'axios';
import moment from 'moment';
import { TypeFichier } from '../../../types/graphql-global-types';
import { formatFilename } from '../../Common/Dropzone/Dropzone';
import { AWS_HOST } from '../../../utils/s3urls';

// Image identifier prefix (for preventing filename conflicts)
export const IDENTIFIER_PREFIX = moment().format('YYYYMMDDhmmss');
export const DIRECTORY = 'avatars';

const Groupement: FC<RouteComponentProps<any, any, any> & WithStyles> = ({ history, classes }) => {
  const [myGroupement, setMyGroupement] = useState<any>();
  const [datas, setDatas] = useState<GroupementInterface | null>(null);
  const [startUpload, setStartUpload] = useState<boolean>(false);
  const [fileUploadResult, setFileUploadResult] = useState<AxiosResponse<any> | null>(null);
  const [files, setFiles] = useState<File[]>([]);
  const [fileAlreadySet, setFileAlreadySet] = useState<string | undefined>();

  const groupement = getGroupement();
  const client = useApolloClient();

  const { data, loading } = useQuery<GROUPEMENT, GROUPEMENTVariables>(GET_GROUPEMENT, {
    variables: {
      id: (groupement && groupement.id) || '',
    },
  });

  const [updateGroupement] = useMutation<UPDATE_GROUPEMENT, UPDATE_GROUPEMENTVariables>(
    DO_UPDATE_GROUPEMENT,
    {
      onCompleted: async data => {
        if (data && data.updateGroupement) {
          setGroupement({
            id: data.updateGroupement.id,
            nom: data.updateGroupement.nom,
          });
          const snackBarData: SnackVariableInterface = {
            type: 'SUCCESS',
            message: `Modification Groupement avec succès`,
            isOpen: true,
          };

          displaySnackBar(client, snackBarData);

          window.location.reload();
        }
      },
      onError: errors => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: `Erreur lors de la modification du groupement`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      },
    },
  );

  const submitUpdate = (incommingDatas: GroupementInterface) => {
    // Start the file upload
    setStartUpload(true);
    setDatas(incommingDatas);
  };

  useEffect(() => {
    if (
      fileAlreadySet === undefined &&
      files.length > 0 &&
      fileUploadResult &&
      fileUploadResult.status === 200 &&
      fileUploadResult.statusText === 'OK' &&
      datas
    ) {
      updateGroupement({
        variables: {
          ...datas,
          avatar: {
            chemin: formatFilename(files[0], DIRECTORY, IDENTIFIER_PREFIX),
            nomOriginal: files[0].name,
            type: TypeFichier.PHOTO,
          },
          id: groupement.id,
        },
      });
    } else if (files.length === 0 && fileAlreadySet && datas) {
      updateGroupement({
        variables: {
          ...datas,
          id: groupement.id,
        },
      });
    } else if (files.length === 0 && fileAlreadySet === undefined && datas) {
      updateGroupement({
        variables: {
          ...datas,
          avatar: null,
          id: groupement.id,
        },
      });
    }
  }, [files, fileUploadResult, datas]);

  useEffect(() => {
    if (data && data.groupement) {
      if (
        data.groupement.groupementLogo &&
        data.groupement.groupementLogo.fichier &&
        data.groupement.groupementLogo.fichier.chemin
      ) {
        setFileAlreadySet(`${AWS_HOST}/${data.groupement.groupementLogo.fichier.chemin}`);
      }

      const myGroupe = { ...data.groupement, pharmacies: [] };
      setMyGroupement(myGroupe);
    }
  }, [data]);

  if (loading) return <Loader />;

  return myGroupement ? (
    <>
      <div className={classes.content}>
        <Form
          defaultData={myGroupement}
          action={submitUpdate}
          fileIdentifierPrefix={IDENTIFIER_PREFIX}
          setFilesOut={setFiles}
          setFileUploadResult={setFileUploadResult}
          startUpload={startUpload}
          setStartUpload={setStartUpload}
          withImagePreview={true}
          uploadDirectory={DIRECTORY}
          fileAleadySetUrl={fileAlreadySet}
          setFileAlreadySet={setFileAlreadySet}
        />
      </div>
    </>
  ) : (
    <></>
  );
};

export default withStyles(style)(withRouter(Groupement));
