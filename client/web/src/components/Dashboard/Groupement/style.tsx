import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles';

const style = (theme: Theme): Record<string, CSSProperties> => ({
  container: {
    display: 'flex',
    position: 'relative',
    '@media (max-height: 440px)': {
      height: '100%',
      padding: 24,
    },
  },
  content: {
    margin: 'auto',
    paddingTop: 50,
    paddingBottom: 50,
    maxWidth: 720,
    width: '100%',
    paddingLeft: 24,
    paddingRight: 24,
  },
  marginBottom: {
    marginBottom: 25,
  },
  title: {
    marginBottom: 35,
    textAlign: 'center',
    font: 'Bold 24px/29px Montserrat',
    color: '#1D1D1D',
    opacity: 1,
  },
  marginRight: {
    marginRight: 10,
  },
  grey: {
    color: 'grey',
  },
  flex: {
    display: 'flex',
    flexDirection: 'row',
  },
  w100: {
    width: '100%',
  },
});

export default style;
