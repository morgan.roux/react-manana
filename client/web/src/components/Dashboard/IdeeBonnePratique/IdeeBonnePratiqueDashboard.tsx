import React from 'react';
import { DO_SEARCH_IDEE_BONNE_PRATIQUE } from '../../../graphql/IdeeBonnePratique/query';
import WithSearch from '../../Common/newWithSearch/withSearch';
import IdeeBonnePratique from './IdeeBonnePratique';

const IdeeBonnePratiqueDashboard = () => {
  // const groupement = getGroupement();
  const must = [{ term: { isRemoved: false } }];
  return (
    <WithSearch
      type="ideeoubonnepratique"
      WrappedComponent={IdeeBonnePratique}
      searchQuery={DO_SEARCH_IDEE_BONNE_PRATIQUE}
      optionalMust={must}
    />
  );
};

export default IdeeBonnePratiqueDashboard;
