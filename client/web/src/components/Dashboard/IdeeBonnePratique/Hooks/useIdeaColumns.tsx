import { Box, MenuItem, Select } from '@material-ui/core';
import moment from 'moment';
import React, { Dispatch, SetStateAction, useState, useMemo, Fragment } from 'react';
import { COLORS_URL, PARTAGE_IDEE_URL } from '../../../../Constant/url';
import { IdeeOuBonnePratiqueStatus } from '../../../../types/graphql-global-types';
import TableActionColumn from '../../TableActionColumn';
import { useDeleteIdea } from './useDeleteIdea';
import { useUpdateStatus } from './useUpdateStatus';

export const useIdeaColumns = (setOpenDeleteDialog: Dispatch<SetStateAction<boolean>>) => {
  const [selected, setSelected] = useState<any[]>([]);
  const [deleteRow, setDeleteRow] = useState<any>(null);

  const { deleteIdea, loading } = useDeleteIdea(
    deleteRow ? [deleteRow] : selected,
    setSelected,
    deleteRow,
    setDeleteRow,
  );

  const { updateStatus, loading: updateStatusLoading } = useUpdateStatus();
  useMemo(() => {
    if (selected && selected.length > 0) {
      if (deleteRow !== null) setDeleteRow(null);
    }
  }, [selected]);

  const onClickDelete = (row: any) => {
    setDeleteRow(row);
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (selected.length > 0) {
      deleteIdea();
    }

    if (deleteRow && deleteRow.id) {
      deleteIdea();
    }
  };

  const handleSelectchange = (event: any, row: any): void => {
    updateStatus({
      variables: {
        id: row && row.id,
        status: event?.target?.value ?? '',
      },
    });
  };
  const columns = [
    {
      label: 'Origine',
      name: '_origine',
    },
    {
      label: 'Date de création',
      name: ' _idee_creation',
      renderer: (row: any) => {
        return row._idee_creation ? moment(row._idee_creation).format('DD/MM/YYYY') : '-';
      },
    },
    {
      label: 'Classification',
      name: 'classification.nom',
      renderer: (row: any) => {
        return row?.classification?.nom ?? '-';
      },
    },
    {
      label: 'Titre',
      name: 'title',
    },
    {
      label: 'Auteur',
      name: 'auteur',
      renderer: (row: any) => {
        return row?.auteur?.userName ?? '-';
      },
    },
    {
      label: 'Statut',
      name: 'status',
      renderer: (row: any) => {
        return (
          <Select value={row.status} onChange={e => handleSelectchange(e, row)}>
            <MenuItem value={IdeeOuBonnePratiqueStatus.NOUVELLE}> Nouvelle</MenuItem>
            <MenuItem value={IdeeOuBonnePratiqueStatus.EN_COURS}> En cours</MenuItem>
            <MenuItem value={IdeeOuBonnePratiqueStatus.VALIDEE}> Validée</MenuItem>
            <MenuItem value={IdeeOuBonnePratiqueStatus.REJETEE}> Rejetée</MenuItem>
          </Select>
        );
      },
    },
    {
      name: '',
      label: '',
      renderer: (row: any) => {
        return (
          <TableActionColumn
            row={row}
            baseUrl={PARTAGE_IDEE_URL}
            setOpenDeleteDialog={setOpenDeleteDialog}
            handleClickDelete={onClickDelete}
            showResendEmail={false}
          />
        );
      },
    },
  ];

  return {
    columns,
    deleteRow,
    onClickConfirmDelete,
    loading,
    selected,
    setSelected,
    updateStatusLoading,
  };
};
