import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { useState } from 'react';
import { DO_UPDATE_STATUS_IDEE_BONNE_PRATIQUE } from '../../../../graphql/IdeeBonnePratique/mutation';
import {
  UPDATE_STATUS_IDEE_BONNE_PRATIQUE,
  UPDATE_STATUS_IDEE_BONNE_PRATIQUEVariables,
} from '../../../../graphql/IdeeBonnePratique/types/UPDATE_STATUS_IDEE_BONNE_PRATIQUE';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

export const useUpdateStatus = () => {
  const client = useApolloClient();

  const [updateStatus, { loading, data }] = useMutation<
    UPDATE_STATUS_IDEE_BONNE_PRATIQUE,
    UPDATE_STATUS_IDEE_BONNE_PRATIQUEVariables
  >(DO_UPDATE_STATUS_IDEE_BONNE_PRATIQUE, {
    onCompleted: data => {
      if (data && data.updateIdeeOuBonnePratiqueStatus) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Mise à jour du statut réussie',
          isOpen: true,
        });
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  return { updateStatus, loading, data };
};
