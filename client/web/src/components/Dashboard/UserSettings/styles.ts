import { makeStyles, createStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() =>
  createStyles({
    head: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: 70,
      background:
        'transparent linear-gradient(259deg, #002F58 0%, #002444 100%) 0% 0% no-repeat padding-box',
      opacity: 1,
      color: '#FFFFFF',
      padding: '0px 50px',
    },
    headText: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 16,
    },
    conteneur: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginBottom: 35,
      marginTop: 30,
    },
    titre: {
      '& > span': {
        fontWeight: 'bold',
      },
    },
    data: {
      textAlign: 'right',
    },
    userInfo: {
      display: 'flex',
      justifyContent: 'flex-end',
      minWidth: '40%',
      '& > ul': {
        width: '100%',
        padding: 0,
        '& > li:nth-child(1)': {
          paddingTop: 0,
        },
      },
    },
  }),
);
