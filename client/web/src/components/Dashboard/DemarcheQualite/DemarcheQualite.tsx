import React, { FC, useState, useEffect, ChangeEvent } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import useStyles from './style';
import { Box, Divider, IconButton } from '@material-ui/core';
import { CardTheme, CardThemeList, ITheme } from '@app/ui-kit';
import DetailsTheme from './Theme/Theme';
import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import {
  GET_THEMES as GET_THEMES_TYPE,
  GET_THEMES_dqThemes
} from '../../../federation/demarche-qualite/theme/types/GET_THEMES';
import { GET_THEME, GET_THEMES } from '../../../federation/demarche-qualite/theme/query';
import { Add, Delete, Edit } from '@material-ui/icons';
import { TwoColumnsParameters, Backdrop, ConfirmDeleteDialog, ErrorPage } from '@app/ui-kit';
import ThemeAdd from './ThemeAdd/ThemeAdd';
import {
  CREATE_THEME as CREATE_THEME_TYPE,
  CREATE_THEMEVariables,
} from '../../../federation/demarche-qualite/theme/types/CREATE_THEME';
import {
  CREATE_THEME,
  DELETE_THEME,
  UPDATE_THEME,
} from '../../../federation/demarche-qualite/theme/mutation';
import { FEDERATION_CLIENT } from './apolloClientFederation';
import {
  GET_THEMEVariables,
  GET_THEME as GET_THEME_TYPE,
} from '../../../federation/demarche-qualite/theme/types/GET_THEME';
import {
  DELETE_THEMEVariables,
  DELETE_THEME as DELETE_THEME_TYPE,
} from '../../../federation/demarche-qualite/theme/types/DELETE_THEME';
import NoItemContentImage from '../../Common/NoItemContentImage';
import {
  UPDATE_THEMEVariables,
  UPDATE_THEME as UPDATE_THEME_TYPE,
} from '../../../federation/demarche-qualite/theme/types/UPDATE_THEME';
import { displaySnackBar } from '../../../utils/snackBarUtils';

const DemarcheQualite: FC<RouteComponentProps> = ({
  history: { push },
  location: { pathname },
  match: { params },
}) => {
  const classes = useStyles({});

  const [item, setItem] = useState<any>();
  const [show, setShow] = useState<boolean>(false);
  const [showDeleteButton, setShowDeleteButton] = useState<boolean>(false);
  const [themeToDeleteId, setThemeToDeleteId] = useState<string>('');
  const [mode, setMode] = useState<'creation' | 'modification'>('creation');
  const [numeroOrdre, setNumeroOrdre] = useState<number | undefined>();
  const [nom, setNom] = useState<string>('');
  const [currentId, setCurrentId] = useState<string>('');

  const client = useApolloClient();

  const handleNumeroOrdreChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNumeroOrdre(parseInt((event.target as HTMLInputElement).value, 10));
  };

  const handleNomChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNom((event.target as HTMLInputElement).value);
  };

  const { loading, error, data } = useQuery<GET_THEMES_TYPE, GET_THEMES_dqThemes>(GET_THEMES, {
    client: FEDERATION_CLIENT,
  });

  const [loadTheme, { called, loading: load, data: themeData, error: errorQuery }] = useLazyQuery<
    GET_THEME_TYPE,
    GET_THEMEVariables
  >(GET_THEME, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-first', // TODO : Use cache
  });

  const [addTheme, creationTheme] = useMutation<CREATE_THEME_TYPE, CREATE_THEMEVariables>(
    CREATE_THEME,
    {
      onError: () => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: `Une s'est produite lors de l'ajout du nouveau thème`,
          isOpen: true,
        });
      },
      onCompleted: () => {
        setShow(false);
        reInit();
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Le thème a été ajouté avec succès !',
          isOpen: true,
        });
      },
      update: (cache, creationResult) => {
        if (creationResult?.data?.createDQTheme) {
          const previousList = cache.readQuery<GET_THEMES_TYPE, GET_THEMES_dqThemes>({
            query: GET_THEMES,
          })?.dqThemes;

          cache.writeQuery<GET_THEMES_TYPE, GET_THEMES_dqThemes>({
            query: GET_THEMES,
            data: {
              dqThemes: [(creationResult.data.createDQTheme as any), ...(previousList || [])],
            },
          });
        }
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [updateTheme, modificationTheme] = useMutation<UPDATE_THEME_TYPE, UPDATE_THEMEVariables>(
    UPDATE_THEME,
    {
      onError: () => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: `Une s'est produite lors de la mise à jour du thème`,
          isOpen: true,
        });
      },
      onCompleted: () => {
        setShow(false);
        reInit();
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Le thème a été modifié avec succès !',
          isOpen: true,
        });
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [deleteTheme] = useMutation<DELETE_THEME_TYPE, DELETE_THEMEVariables>(DELETE_THEME, {
    onError: () => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: `Une s'est produite lors de la suppression du thème`,
        isOpen: true,
      });
    },
    onCompleted: deleteResult => {
      setShowDeleteButton(false);
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: 'Le thème a été supprimé avec succès !',
        isOpen: true,
      });

      const selectedThemeId = (params as any).id;

      if (selectedThemeId && selectedThemeId === deleteResult.deleteDQTheme?.id) {
        push('/db/demarche_qualite/');
      }
    },
    update: (cache, deletionResult) => {
      if (
        deletionResult.data &&
        deletionResult.data.deleteDQTheme &&
        deletionResult.data.deleteDQTheme.id
      ) {
        const previousList = cache.readQuery<GET_THEMES_TYPE, GET_THEMES_dqThemes>({
          query: GET_THEMES,
        })?.dqThemes;

        const deleteId = deletionResult.data.deleteDQTheme.id;

        cache.writeData<GET_THEMES_TYPE>({
          data: {
            dqThemes: (previousList || []).filter(item => item?.id !== deleteId),
          },
        });

        // If currentTheme is the selected theme, then back to home

        if (deleteId === currentId) {
          push('db/demarche_qualite');
        }
      }
    },
    client: FEDERATION_CLIENT,
  });

  const handleIdChange = (item: ITheme) => {
    loadTheme({
      variables: {
        id: item.id,
      },
    });
  };

  useEffect(() => {
    if (called) {
      if (themeData && themeData.dqTheme) {
        setItem(themeData.dqTheme);
        push({
          pathname: `/db/demarche_qualite/${themeData.dqTheme.id}`,
          state: { theme: themeData.dqTheme },
        });
      }
    }
  }, [themeData]);

  const handleClick = (id: string): void => {
    setThemeToDeleteId(id);
    setShowDeleteButton(!showDeleteButton);
  };

  const reInit = () => {
    setNumeroOrdre(undefined);
    setNom('');
  };

  const handleChangeShow = (mode: 'creation' | 'modification', element: any = null): void => {
    setShow(!show);
    setMode(mode);
    if (element) {
      setCurrentId(element.id);
      setNumeroOrdre(element.ordre);
      setNom(element.nom);
    }
  };

  // Submit ajout theme
  const handleSubmit = (): void => {
    if (mode === 'creation') {
      addTheme({
        variables: {
          themeInput: {
            ordre: numeroOrdre,
            nom: nom,
          },
        },
      });
    } else {
      updateTheme({
        variables: {
          id: currentId,
          themeInput: {
            ordre: numeroOrdre,
            nom: nom,
          },
        },
      });
    }
  };

  const handleDelete = (): void => {
    deleteTheme({
      variables: {
        id: themeToDeleteId,
      },
    });
  };

  if (loading || load)
    return <Backdrop value="Récuperation de données..." open={loading || load} />;

  if (error) {
    return <ErrorPage />;
  }

  return (
    <TwoColumnsParameters
      leftHeaderContent={
        <div>
          <div className={classes.headerContent}>
            <div>Liste des thèmes</div>
            <IconButton onClick={handleChangeShow.bind(null, 'creation')}>
              <Add color="primary" />
            </IconButton>
          </div>
          <ThemeAdd
            saving={mode === 'modification' ? modificationTheme.loading : creationTheme.loading}
            title={mode == 'creation' ? 'Ajout de thème' : 'Modification de thème'}
            show={show}
            setShow={setShow}
            onSubmit={handleSubmit}
            value1={numeroOrdre}
            value2={nom}
            setValue1={handleNumeroOrdreChange}
            setValue2={handleNomChange}
          />
        </div>
      }
      rightContent={<DetailsTheme />}
    >
      {!loading &&
        !error &&
        (data && data.dqThemes && data.dqThemes.length > 0 ? (
          <Box>
            <CardThemeList>
              {data.dqThemes
                .sort((a, b) => (a?.ordre || 0) - (b?.ordre || 0))
                .map((item: any, index: number) => {
                  const pathName = pathname.split('/')[3];
                  const result: boolean = (pathName && item && pathName === item.id) === true;

                  return (
                    <>
                      <CardTheme
                        key={item.id}
                        listItemFields={{
                          url: '/db/demarche_qualite/',
                        }}
                        title={`${item.ordre}-${item.nom}`}
                        setCurrentId={setCurrentId}
                        littleComment={`Nbre de sous-thèmes : ${item.nombreSousThemes}`}
                        active={result}
                        onClick={handleIdChange.bind(null, item)}
                        moreOptions={[
                          {
                            menuItemLabel: {
                              title: 'Modifier',
                              icon: <Edit />,
                            },
                            onClick: handleChangeShow.bind(null, 'modification', item),
                          },
                          {
                            menuItemLabel: {
                              title: 'Supprimer',
                              icon: <Delete />,
                            },
                            onClick: handleClick.bind(null, item.id),
                          },
                        ]}
                      />
                    </>
                  );
                })}
            </CardThemeList>
            <ConfirmDeleteDialog
              key="delete-theme"
              open={showDeleteButton}
              setOpen={setShowDeleteButton}
              onClickConfirm={handleDelete}
            />
          </Box>
        ) : (
          <NoItemContentImage
            title="Aucun thème à afficher"
            subtitle="Ajouter-en un en cliquant sur le bouton d'en haut"
          />
        ))}
    </TwoColumnsParameters>
  );
};

export default withRouter(DemarcheQualite);
