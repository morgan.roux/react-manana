import React, { FC } from 'react';
import { ApolloProvider } from 'react-apollo';
import { FEDERATION_CLIENT } from '../apolloClientFederation';
import ChecklistForm from './ChecklistForm';

// FIXME : Temporary solution
const WithApolloFederationClient: FC<{}> = ({}) => (
  <ApolloProvider client={FEDERATION_CLIENT}>
    <ChecklistForm />
  </ApolloProvider>
);

export default WithApolloFederationClient;
