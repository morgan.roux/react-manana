import React, { FC } from 'react';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { ChecklistPage, Checklist } from '@app/ui-kit';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import {
  GET_CHECKLISTS as GET_CHECKLISTS_TYPE,
  GET_CHECKLISTSVariables,
} from './../../../../federation/demarche-qualite/checklist/types/GET_CHECKLISTS';
import {
  DELETE_CHECKLIST as DELETE_CHECKLIST_TYPE,
  DELETE_CHECKLISTVariables,
} from './../../../../federation/demarche-qualite/checklist/types/DELETE_CHECKLIST';
import { GET_CHECKLISTS } from './../../../../federation/demarche-qualite/checklist/query';
import { DELETE_CHECKLIST } from './../../../../federation/demarche-qualite/checklist/mutation';
import { FEDERATION_CLIENT } from '../apolloClientFederation';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { AppAuthorization } from '../../../../services/authorization';
import { getUser, getPharmacie } from '../../../../services/LocalStorage';

const ChecklistManagement: FC<RouteComponentProps> = ({ history }) => {
  const client = useApolloClient();

  const user = getUser();
  const auth = new AppAuthorization(user);

  const variables = !auth.isSupAdminOrIsGpmAdmin
    ? { idPharmacie: getPharmacie().id, onlyChecklistsPharmacie: true }
    : undefined;

  const { loading, error, data } = useQuery<GET_CHECKLISTS_TYPE, GET_CHECKLISTSVariables>(
    GET_CHECKLISTS,
    { client: FEDERATION_CLIENT, variables },
  );

  const [deleteChecklist, deletionChecklist] = useMutation<
    DELETE_CHECKLIST_TYPE,
    DELETE_CHECKLISTVariables
  >(DELETE_CHECKLIST, {
    client: FEDERATION_CLIENT,
    onError: () => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: `Une erreur s'est produite lors de la suppréssion du checklist`,
        isOpen: true,
      });
    },

    onCompleted: () => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: 'Le checklist a été supprimé avec succès !',
        isOpen: true,
      });
    },
    update: (cache, deletionResult) => {
      if (deletionResult?.data?.deleteDQChecklist?.id) {
        const previousList = cache.readQuery<GET_CHECKLISTS_TYPE, GET_CHECKLISTSVariables>({
          query: GET_CHECKLISTS,
          variables,
        })?.dqChecklists;

        const deletedId = deletionResult.data.deleteDQChecklist.id;

        cache.writeQuery<GET_CHECKLISTS_TYPE, GET_CHECKLISTSVariables>({
          query: GET_CHECKLISTS,
          variables,
          data: {
            dqChecklists: (previousList || []).filter((item) => item?.id !== deletedId),
          },
        });
      }
    },
  });

  const handleAddChecklist = () => {
    history.push(`/db/check-list-qualite/new`);
  };
  const handleEditChecklist = (checklistToEdit: Checklist) => {
    history.push(`/db/check-list-qualite/${checklistToEdit.id}`);
  };
  const handleDeleteChecklist = (checklistToDelete: Checklist) => {
    if (checklistToDelete.id) {
      deleteChecklist({
        variables: {
          id: checklistToDelete.id,
        },
      });
    }
  };

  return (
    <ChecklistPage
      onRequestClose={() => console.log('Close')}
      loading={loading || deletionChecklist.loading}
      error={error}
      data={(data?.dqChecklists || []) as any}
      onRequestAdd={handleAddChecklist}
      onRequestEdit={handleEditChecklist}
      onRequestDelete={handleDeleteChecklist}
    />
  );
};

export default withRouter(ChecklistManagement);
