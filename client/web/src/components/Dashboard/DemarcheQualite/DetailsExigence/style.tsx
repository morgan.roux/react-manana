import { createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      padding: 24,
      alignSelf: 'center',
    },
    btn: {
      textTransform: 'none',
      marginRight: 8,
    },

    textContent: {
      textAlign: 'center',
      fontSize: 20,
      height: 80,
    },

    content: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      maxWidth: 795,
    },
  }),
);

export default useStyles;
