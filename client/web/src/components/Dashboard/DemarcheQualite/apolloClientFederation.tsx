import { GATEWAY_FEDERATION_URL } from '../../../config';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import introspectionQueryResultData from '../../../federation-schema.json';
import {
  getGroupement,
  getPharmacie,
  getAccessToken,
  getIp,
  getUser,
} from '../../../services/LocalStorage';

// TODO : Error handling, https://www.apollographql.com/docs/react/data/error-handling/
const createClient = () => {
  const httpLink = createHttpLink({
    uri: `${GATEWAY_FEDERATION_URL}/graphql`,
  });

  const authLink = setContext((_, { headers }) => {
    const user = getUser();
    const groupement = getGroupement();
    const pharmacie = getPharmacie();
    const prestataire = user?.userPartenaire?.id;
    const headerToken = getAccessToken();
    const ip = getIp();

    return {
      headers: {
        ...headers,
        authorization: headerToken ? `Bearer ${headerToken}` : '',
        groupement: groupement ? groupement.id : '',
        pharmacie: pharmacie ? pharmacie.id : '',
        prestataire:prestataire??'',
        ip: ip || '',
      },
    };
  });

  const fragmentMatcher = new IntrospectionFragmentMatcher({
    introspectionQueryResultData: introspectionQueryResultData as any,
  });

  return new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache({ fragmentMatcher }),
  });
};

export const FEDERATION_CLIENT: ApolloClient<any> = createClient();

export default createClient;
