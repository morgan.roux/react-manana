import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => createStyles({
    formsContent : {
      paddingTop:20,
      width:'100%',
      display : 'flex',
      flexDirection :'column',
      justifyContent : 'center'
    },

}));

export default useStyles;
