import React, { FC, useState, ChangeEvent, MouseEvent } from 'react';
import { CustomModal, CustomFormTextField } from '@app/ui-kit';
import useStyles from './style';
import { Box } from '@material-ui/core';

interface MatriceTacheFormProps {
  title: string;
  show: boolean;
  saving: boolean;
  ordre?: number;
  libelle?: string;
  setOrdre: (value: number) => void;
  setLibelle: (value: string) => void;
  setShow: (open: boolean) => void;
  onSubmit: (event: MouseEvent) => void;
}

const MatriceForm: FC<MatriceTacheFormProps> = ({
  title,
  show,
  setOrdre,
  setLibelle,
  saving,
  ordre,
  libelle,
  setShow,
  onSubmit,
}) => {
  const classes = useStyles({});

  return (
    <CustomModal
      title={title}
      open={show}
      setOpen={setShow}
      headerWithBgColor={true}
      fullWidth={true}
      maxWidth="sm"
      closeIcon={true}
      withBtnsActions={true}
      disabledButton={!ordre || !libelle || saving}
      onClickConfirm={onSubmit}
      centerBtns={true}
    >
      <Box className={classes.formsContent}>
        <Box>
          <CustomFormTextField
            type="number"
            placeholder="Numéro d'ordre"
            value={ordre}
            onChange={(event) =>
              setOrdre(event.target.value ? parseInt(event.target.value, 10) : 0)
            }
            label="Numéro d'ordre"
          />
        </Box>
        <Box>
          <CustomFormTextField
            placeholder="Nom"
            value={libelle}
            onChange={(event) => setLibelle(event.target.value)}
            label="Nom"
          />
        </Box>
      </Box>
    </CustomModal>
  );
};

export default MatriceForm;
