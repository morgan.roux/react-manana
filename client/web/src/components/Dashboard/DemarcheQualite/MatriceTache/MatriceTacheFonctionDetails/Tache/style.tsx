import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => createStyles({
    root: { 
         display : 'flex',
         flexDirection : 'column'
    },

    header : {
        padding : 10,
        display : 'flex',
        flexDirection : 'row',
        justifyContent : 'space-between',
        marginBottom : 20
    },

    content : {
        display : 'flex',
        flexDirection : 'column',
        padding : 5,
        marginTop : 8
    },

    title : {
        display : 'block'
    },

    span : {
        color : '#E34168'
    },

    spanBold : {
        fontWeight : 'bold'
    },

    btn : {
        textTransform: 'none',
        marginRight: 8,
    },
    exigenceContent : {
        padding : 15
    },

    modalContent : {
        display : 'flex',
        flexDirection : 'column'
    },

    listItem : {
        marginBottom : 10
    },

    nombreExigences : {
        color : '#9E9E9E',
        marginLeft:50,
        marginBottom:10 
    },
}));

export default useStyles;
