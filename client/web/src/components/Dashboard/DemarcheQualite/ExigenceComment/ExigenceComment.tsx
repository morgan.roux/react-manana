import { useApolloClient, useQuery } from '@apollo/react-hooks';
import React, { FC } from 'react';
import { GET_COMMENTS } from '../../../../graphql/Comment';
import { COMMENTS, COMMENTSVariables } from '../../../../graphql/Comment/types/COMMENTS';
import { GET_SMYLEYS } from '../../../../graphql/Smyley';
import { SMYLEYS, SMYLEYSVariables } from '../../../../graphql/Smyley/types/SMYLEYS';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { getGroupement } from '../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { Comment } from '../../../Comment';
import CommentList from '../../../Comment/CommentList';
import SmallLoading from '../../../../components/Loading/SmallLoading';
import usesStyles from './styles';

interface ExigenceCommentProps {
  exigenceId: string;
}

const ExigenceComment: FC<ExigenceCommentProps> = ({ exigenceId }) => {
  const classes = usesStyles();
  const client = useApolloClient();

  const getComments = useQuery<COMMENTS, COMMENTSVariables>(GET_COMMENTS, {
    variables: {
      codeItem: 'EXIGENCE',
      idItemAssocie: exigenceId,
      take: 10,
      skip: 0,
    },
    fetchPolicy: 'cache-and-network',
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const groupement = getGroupement();
  // Get smyleys by groupement
  const getSmyleys = useQuery<SMYLEYS, SMYLEYSVariables>(GET_SMYLEYS, {
    variables: {
      idGroupement: groupement && groupement.id,
    },
    onError: (errors) => {
      errors.graphQLErrors.map((err) => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const nbComment =
    (getComments &&
      getComments.data &&
      getComments.data.comments &&
      getComments.data.comments.total) ||
    0;

  const fetchMoreComments = () => {
    if (getComments.data && getComments.data.comments && getComments.data.comments.data) {
      const { fetchMore } = getComments;
      fetchMore({
        variables: {
          codeItem: 'EXIGENCE',
          idItemAssocie: exigenceId,
          skip: getComments.data.comments.data.length,
          take: 10,
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          if (!fetchMoreResult) return prev;
          return {
            ...prev,
            data: [
              ...((fetchMoreResult && fetchMoreResult.comments && fetchMoreResult.comments.data) ||
                []),
              ...((prev && prev.comments && prev.comments.data) || []),
            ],
          };
        },
      });
    }
  };

  return (
    <div className={classes.root}>
      {getComments.loading && !getComments.data && <SmallLoading />}
      {getComments.data && getComments.data.comments && (
        <CommentList
          key={`comment_list_${exigenceId}`}
          comments={getComments.data.comments}
          loading={getComments.loading && !getComments.data}
          fetchMoreComments={fetchMoreComments}
        />
      )}
      <Comment codeItem="EXIGENCE" idSource={exigenceId} />
    </div>
  );
};

export default ExigenceComment;
