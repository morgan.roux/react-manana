import React, { ChangeEvent, FC, useState, MouseEvent } from 'react';
import { useApolloClient, useLazyQuery, useMutation } from '@apollo/react-hooks';
import { Box } from '@material-ui/core';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import useStyles from './style';
import { Theme, ConfirmDeleteDialog, IExigence, Loader, CustomFullScreenModal } from '@app/ui-kit';
import Exigences from './Exigences/Exigences';
import DetailsExigence from './../../DetailsExigence/DetailsExigence';
import {
  GET_FULL_SOUS_THEMEVariables,
  GET_FULL_SOUS_THEME as GET_FULL_SOUS_THEME_TYPE,
} from '../../../../../federation/demarche-qualite/theme/types/GET_FULL_SOUS_THEME';
import {
  GET_FULL_SOUS_THEME,
  GET_THEME,
} from '../../../../../federation/demarche-qualite/theme/query';
import { FEDERATION_CLIENT } from '../../apolloClientFederation';
import { Add, Delete, Edit } from '@material-ui/icons';
import ThemeAdd from '../../ThemeAdd/ThemeAdd';
import {
  UPDATE_SOUS_THEME as UPDATE_SOUS_THEME_TYPE,
  UPDATE_SOUS_THEMEVariables,
} from '../../../../../federation/demarche-qualite/theme/types/UPDATE_SOUS_THEME';
import {
  DELETE_SOUS_THEME,
  UPDATE_SOUS_THEME,
  DELETE_EXIGENCE,
} from '../../../../../federation/demarche-qualite/theme/mutation';
import {
  deleteDQSousTheme,
  deleteDQSousThemeVariables,
} from '../../../../../federation/demarche-qualite/theme/types/deleteDQSousTheme';
import {
  GET_THEMEVariables,
  GET_THEME as GET_THEME_TYPE,
} from '../../../../../federation/demarche-qualite/theme/types/GET_THEME';
import {
  DELETE_EXIGENCEVariables,
  DELETE_EXIGENCE as DELETE_EXIGENCE_TYPE,
} from '../../../../../federation/demarche-qualite/theme/types/DELETE_EXIGENCE';

import { displaySnackBar } from '../../../../../utils/snackBarUtils';

interface SousThemeComponentProps {
  sousThemes: any[];
  themeId: string;
}

const SousThemeComponent: FC<SousThemeComponentProps & RouteComponentProps> = ({
  sousThemes,
  themeId,
  history,
}) => {
  const classes = useStyles({});
  const [show, setShow] = useState<boolean>(false);
  const [showDeleteButton, setShowDeleteButton] = useState<boolean>(false);
  const [current, setCurrent] = useState<any>();
  const [numeroOrdre, setNumeroOrdre] = useState<number | undefined>();
  const [nom, setNom] = useState<string>('');
  const [currentId, setCurrentId] = useState<string>('');

  const [openDeleteExigenceDialog, setOpenDeleteExigenceDialog] = useState<boolean>(false);
  const [exigenceToDelete, setExigenceToDelete] = useState<IExigence | null>(null);

  const [openDetailExigenceDialog, setOpenDetailExigenceDialog] = useState<boolean>(false);
  const [exigenceToShowDetail, setExigenceToShowDetail] = useState<IExigence | null>(null);
  const [
    exigenceToShowDetailSousTheme,
    setExigenceToShowDetailSousTheme,
  ] = useState<IExigence | null>(null);

  const reInit = () => {
    setNumeroOrdre(undefined);
    setNom('');
  };

  const client = useApolloClient();

  const handleNumeroOrdreChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNumeroOrdre(parseInt((event.target as HTMLInputElement).value, 10));
  };

  const handleNomChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setNom((event.target as HTMLInputElement).value);
  };

  const [loadSousTheme, loadingSousTheme] = useLazyQuery<
    GET_FULL_SOUS_THEME_TYPE,
    GET_FULL_SOUS_THEMEVariables
  >(GET_FULL_SOUS_THEME, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'network-only', // TODO : Use cache
  });

  const [deleteSousTheme, deletionSousTheme] = useMutation<
    deleteDQSousTheme,
    deleteDQSousThemeVariables
  >(DELETE_SOUS_THEME, {
    onError: () => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: `Une erreur s'est produite lors de la suppression du sous-thème.`,
        isOpen: true,
      });
    },

    onCompleted: () => {
      setShowDeleteButton(false);
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: 'Le sous-thème a été supprimé avec succès !',
        isOpen: true,
      });
    },
    update: (cache, deletionResult) => {
      if (
        deletionResult.data &&
        deletionResult.data.deleteDQSousTheme &&
        deletionResult.data.deleteDQSousTheme.id
      ) {
        const previousTheme = cache.readQuery<GET_THEME_TYPE, GET_THEMEVariables>({
          query: GET_THEME,
          variables: {
            id: themeId,
          },
        })?.dqTheme;

        const deleteId = deletionResult.data.deleteDQSousTheme.id;

        const deletedSousTheme = {
          ...previousTheme,
          nombreSousThemes: ((previousTheme as any)?.sousThemes?.length || 0) - 1,
          sousThemes: sousThemes.filter(item => item.id !== deleteId),
        };

        cache.writeData<GET_THEME_TYPE>({
          data: {
            dqTheme: deletedSousTheme,
          } as any,
        });
      }
    },

    client: FEDERATION_CLIENT,
  });

  const [updateSousTheme, modificationSousTheme] = useMutation<
    UPDATE_SOUS_THEME_TYPE,
    UPDATE_SOUS_THEMEVariables
  >(UPDATE_SOUS_THEME, {
    onError: () => {
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: `Une erreur s'est produite lors de la modification du sous-thème.`,
        isOpen: true,
      });
    },
    onCompleted: () => {
      setShow(false);
      reInit();
      displaySnackBar(client, {
        type: 'SUCCESS',
        message: 'Le sous-thème a été modifié avec succès !',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteExigence, deletionExigence] = useMutation<
    DELETE_EXIGENCE_TYPE,
    DELETE_EXIGENCEVariables
  >(DELETE_EXIGENCE, {
    client: FEDERATION_CLIENT,
    onError: () => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: `Une s'est produite lors de la suppression de l'exigence`,
        isOpen: true,
      });
    },
    onCompleted: () => {
      setExigenceToDelete(null);
      setOpenDeleteExigenceDialog(false);

      displaySnackBar(client, {
        type: 'SUCCESS',
        message: "L'exigence a été supprimée avec succès !",
        isOpen: true,
      });
    },
    update: (cache, deletionExigenceResult) => {
      const deletedExigenceId = deletionExigenceResult.data?.deleteDQExigence?.id;
      if (deletedExigenceId) {
        const previousTheme = cache.readQuery<GET_THEME_TYPE, GET_THEMEVariables>({
          query: GET_THEME,
          variables: {
            id: themeId,
          },
        })?.dqTheme;

        const updatedTheme = {
          ...previousTheme,
          sousThemes: ((previousTheme as any).sousThemes || []).map((sousTheme: any) => {
            const foundExigenceInSousTheme = (sousTheme.exigences || []).some(
              ({ id }) => id === deletedExigenceId,
            );

            return {
              ...sousTheme,
              nombreExigences: foundExigenceInSousTheme
                ? (sousTheme.nombreExigences || 0) - 1
                : sousTheme.nombreExigences,
              exigences: foundExigenceInSousTheme
                ? (sousTheme.exigences || []).filter(({ id }) => id !== deletedExigenceId)
                : sousTheme.exigences,
            };
          }),
        };

        cache.writeQuery<GET_THEME_TYPE, GET_THEMEVariables>({
          query: GET_THEME,
          variables: {
            id: themeId,
          },
          data: {
            dqTheme: updatedTheme as any,
          },
        });
      }
    },
  });

  const handleLoadSousTheme = (id: string, event: MouseEvent, expended: boolean): void => {
    if (expended) {
      loadSousTheme({
        variables: {
          id,
        },
      });
    }
  };

  const handleChange = (element: any): void => {
    setCurrent(element);
    setShowDeleteButton(!showDeleteButton);
  };

  const handleAddExigence = (sousTheme: any) => {
    history.push(`/db/demarche_qualite/exigence/${themeId}/${sousTheme.id}/new`);
  };

  const handleUpdate = (sousTheme: any) => {
    setCurrent(sousTheme);
    setCurrentId(sousTheme.id); // A supprimer peut-être ?
    setNumeroOrdre(sousTheme.ordre);
    setNom(sousTheme.nom);
    setShow(!show);
  };

  const handleRequestEditExigence = (sousTheme: any, exigence: IExigence) => {
    history.push(`/db/demarche_qualite/exigence/${themeId}/${sousTheme.id}/${exigence.id}`);
  };

  const handleRequestDeleteExigence = (exigence: IExigence) => {
    setExigenceToDelete(exigence);
    setOpenDeleteExigenceDialog(true);
  };

  const handleRequestDetailExigence = (sousTheme: any, exigence: IExigence) => {
    setExigenceToShowDetail(exigence);
    setExigenceToShowDetailSousTheme(sousTheme);
    setOpenDetailExigenceDialog(true);
  };

  const handleOutilClick = (typologie: string, id: string) => {
    if (typologie === 'checklist') {
      history.push(`/demarche-qualite/outils/checklist`);
    } else {
      history.push(`/demarche-qualite/outils/${typologie}/${id}`);
    }
  };

  const doUpdate = (): void => {
    updateSousTheme({
      variables: {
        id: currentId,
        sousThemeInput: {
          ordre: numeroOrdre,
          nom: nom,
        },
      },
    });
  };

  const doDelete = (): void => {
    deleteSousTheme({
      variables: {
        id: current.id,
      },
    });
  };

  const doDeleteExigence = (): void => {
    if (exigenceToDelete?.id) {
      deleteExigence({
        variables: {
          id: exigenceToDelete.id,
        },
      });
    }
  };

  return (
    <Box className={classes.content}>
      {sousThemes &&
        sousThemes
          .sort((a, b) => a.ordre - b.ordre)
          .map((sousTheme: any) => (
            <Theme
              key={sousTheme.id}
              theme={sousTheme}
              summarySubContent={
                <Box
                  className={classes.nombreExigences}
                >{`Nombre d'exigence : ${sousTheme.nombreExigences}`}</Box>
              }
              checkList={true}
              onClick={handleLoadSousTheme.bind(null, sousTheme.id)}
              textStyle={{ fontWeight: 'bold', color: '#000' }}
              moreOptions={[
                {
                  menuItemLabel: {
                    title: 'Ajouter exigence',
                    icon: <Add />,
                  },
                  onClick: handleAddExigence.bind(null, sousTheme),
                },
                {
                  menuItemLabel: {
                    title: 'Modifier sous-thème',
                    icon: <Edit />,
                  },
                  onClick: handleUpdate.bind(null, sousTheme),
                },
                {
                  menuItemLabel: {
                    title: 'Supprimer sous-thème',
                    icon: <Delete />,
                  },
                  onClick: handleChange.bind(null, sousTheme),
                },
              ]}
            >
              <Exigences
                onRequestAdd={handleAddExigence.bind(null, sousTheme)}
                onRequestDelete={handleRequestDeleteExigence}
                onRequestEdit={handleRequestEditExigence.bind(null, sousTheme)}
                onRequestDetail={handleRequestDetailExigence.bind(null, sousTheme)}
                error={loadingSousTheme.error}
                loading={loadingSousTheme.loading}
                data={(loadingSousTheme?.data?.dqSousTheme?.exigences as any) || []}
              />
            </Theme>
          ))}
      <ThemeAdd
        title={'Modification de sous-thème'}
        saving={modificationSousTheme.loading}
        show={show}
        setShow={setShow}
        value1={numeroOrdre}
        value2={nom}
        setValue1={handleNumeroOrdreChange}
        setValue2={handleNomChange}
        onSubmit={doUpdate}
      />
      <ConfirmDeleteDialog
        open={openDeleteExigenceDialog}
        setOpen={setOpenDeleteExigenceDialog}
        onClickConfirm={doDeleteExigence}
        isLoading={deletionExigence.loading}
      />

      <ConfirmDeleteDialog
        open={showDeleteButton}
        setOpen={setShowDeleteButton}
        onClickConfirm={doDelete}
        isLoading={deletionSousTheme.loading}
      />
      {/* TODO: Change*/}
      <CustomFullScreenModal
        title="Détails d'exigence"
        actionButton="Modifier"
        onClickConfirm={() => {
          if (exigenceToShowDetail) {
            handleRequestEditExigence(exigenceToShowDetailSousTheme, exigenceToShowDetail);
          }
        }}
        open={openDetailExigenceDialog}
        setOpen={setOpenDetailExigenceDialog}
      >
        {exigenceToShowDetail && exigenceToShowDetailSousTheme ? (
          <DetailsExigence
            onRequestEdit={() =>
              handleRequestEditExigence(exigenceToShowDetailSousTheme, exigenceToShowDetail)
            }
            onRequestGoBack={() => setOpenDetailExigenceDialog(false)}
            onOutilClick={handleOutilClick}
            exigence={exigenceToShowDetail}
            sousTheme={exigenceToShowDetailSousTheme as any}
          />
        ) : (
          <Loader />
        )}
      </CustomFullScreenModal>
    </Box>
  );
};

export default withRouter(SousThemeComponent);
