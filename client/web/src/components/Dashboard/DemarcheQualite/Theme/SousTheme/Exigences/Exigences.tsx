import React, { FC } from 'react';
import useStyles from './style';
import { IconButton, Box } from '@material-ui/core';
import { Add, Edit, Delete, Visibility } from '@material-ui/icons';
import { IExigence, Loader, ErrorPage, CardThemeList, Exigence } from '@app/ui-kit';

interface ExigenceProps {
  data?: IExigence[];
  loading?: boolean;
  error?: Error;
  onRequestAdd: () => void;
  onRequestEdit: (exigence: IExigence) => void;
  onRequestDelete: (exigence: IExigence) => void;
  onRequestDetail: (exigence: IExigence) => void;
}

const Exigences: FC<ExigenceProps> = ({
  data,
  loading,
  error,
  onRequestAdd,
  onRequestDelete,
  onRequestDetail,
  onRequestEdit,
}) => {
  const classes = useStyles({});

  if (loading) {
    return <Loader />;
  }
  if (error) {
    return <ErrorPage />;
  }

  return (
    <Box style={{ marginLeft: 15 }}>
      <Box className={classes.exigenceContent}>
        <Box>
          <IconButton color="secondary" onClick={() => onRequestAdd()}>
            <Add />
            Ajouter une exigence
          </IconButton>
        </Box>
        <CardThemeList>
          {(data || []).map((item: any) => {
            const affiches = item.outils?.affiches || [];
            const enregistrements = item.outils?.enregistrements || [];
            const memos = item.outils?.memos || [];
            const procedures = item.outils?.procedures || [];
            const documents = item.outils?.documents || [];
            const checklists = item.outils?.checklists || [];

            const nombreOutils =
              affiches.length +
              enregistrements.length +
              memos.length +
              procedures.length +
              checklists.length;

            const nombreDocuments = documents.length;
            return (
              <Exigence
                exigence={{
                  ...item,
                  nombreOutils,
                  nombreDocuments,
                }}
                moreOptions={[
                  {
                    menuItemLabel: {
                      title: 'Consulter exigence',
                      icon: <Visibility />,
                    },
                    onClick: () => onRequestDetail(item),
                  },

                  {
                    menuItemLabel: {
                      title: 'Modifier exigence',
                      icon: <Edit />,
                    },
                    onClick: () => onRequestEdit(item),
                  },
                  {
                    menuItemLabel: {
                      title: 'Supprimer exigence',
                      icon: <Delete />,
                    },
                    onClick: () => onRequestDelete(item),
                  },
                ]}
              />
            );
          })}
        </CardThemeList>
      </Box>
    </Box>
  );
};

export default Exigences;
