import React, { FC, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { Grid, Box } from '@material-ui/core';
import useStyles from './styles';
import Form from './FtpForm';
import { getGroupement } from '../../../services/LocalStorage';
import IFtp from '../../../Interface/FtpInterface';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { GET_FTP_GROUPEMENT } from '../../../graphql/Ftp/query';
import { FTP_GROUPEMENT, FTP_GROUPEMENTVariables } from '../../../graphql/Ftp/types/FTP_GROUPEMENT';
import { DO_CREATE_FTP, DO_UPDATE_FTP } from '../../../graphql/Ftp/mutation';
import { CREATE_FTP, CREATE_FTPVariables } from '../../../graphql/Ftp/types/CREATE_FTP';
import { UPDATE_FTP, UPDATE_FTPVariables } from '../../../graphql/Ftp/types/UPDATE_FTP';
import { Loader } from '../Content/Loader';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../Interface/SnackVariableInterface';

const Ftp: FC = () => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [myFtp, setmyFtp] = useState<IFtp | null>();
  const [update, setUpdate] = useState(true);

  const groupement = getGroupement();

  const { data: ftpData, loading } = useQuery<FTP_GROUPEMENT, FTP_GROUPEMENTVariables>(
    GET_FTP_GROUPEMENT,
    {
      fetchPolicy: 'cache-and-network',
      variables: {
        idgroupement: (groupement && groupement.id) || '',
      },
      onError: errors => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: `Erreur lors de la récupération du détail de l'FTP`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      },
    },
  );

  const [createFtp] = useMutation<CREATE_FTP, CREATE_FTPVariables>(DO_CREATE_FTP, {
    onCompleted: async data => {
      if (data && data.createFtpConnect) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Création du FTP`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);

        setmyFtp({
          id: data.createFtpConnect.id,
          host: data.createFtpConnect.host ? data.createFtpConnect.host : '',
          port: data.createFtpConnect.port ? data.createFtpConnect.port : 0,
          user: data.createFtpConnect.user ? data.createFtpConnect.user : '',
          password: data.createFtpConnect.password ? data.createFtpConnect.password : '',
          idGroupement: groupement && groupement.id,
        });
        window.location.reload();
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de la création du FTP`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [updateFtp] = useMutation<UPDATE_FTP, UPDATE_FTPVariables>(DO_UPDATE_FTP, {
    onCompleted: data => {
      if (data && data.updateFtpConnect) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Modification du FTP avec succès`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        window.location.reload();
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de la modification du FTP`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const submitUpdate = (incommingDatas: any) => {
    const datas: IFtp = { ...incommingDatas, port: parseInt(incommingDatas.port) };
    if (update) {
      updateFtp({ variables: { ...datas } });
    } else {
      createFtp({ variables: { ...datas, idGroupement: groupement && groupement.id } });
      setUpdate(true);
    }
  };

  useEffect(() => {
    if (ftpData && ftpData.ftpGroupement) {
      setUpdate(true);
      const myFtp = ftpData.ftpGroupement;
      setmyFtp({
        id: myFtp.id,
        host: myFtp.host ? myFtp.host : '',
        port: myFtp.port ? myFtp.port : 0,
        user: myFtp.user ? myFtp.user : '',
        password: myFtp.password ? myFtp.password : '',
        idGroupement: groupement && groupement.id,
      });
    } else {
      setUpdate(false);
    }
  }, [ftpData]);

  if (loading) return <Loader />;

  return (
    <Box display="flex" justifyContent="center" className={classes.contentBottom}>
      {myFtp && (
        <Grid item={true} xs={6} className={classes.spacingLeft}>
          <Form defaultData={myFtp} action={submitUpdate} />
        </Grid>
      )}
      {!myFtp && (
        <Grid item={true} xs={6} className={classes.spacingLeft}>
          <Form defaultData={undefined} action={submitUpdate} />
        </Grid>
      )}
    </Box>
  );
};
export default withRouter(Ftp);
