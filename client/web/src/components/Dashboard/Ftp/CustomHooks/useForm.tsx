import { useState, ChangeEvent } from 'react';
import IFtp from '../../../../Interface/FtpInterface';


const useForm = (
  callback: (data: IFtp) => void,
  defaultState?: IFtp,
) => {
  const initialState: IFtp = defaultState || {
    id: "",
    idGroupement : "",
    host : "",
    port : 0,
    user : "",
    password : "",
  };

  const [values, setValues] = useState<IFtp>(initialState);

  const handleSubmit = (e: any) => {
    if (e) e.preventDefault();
    callback(values);
  };

  const handleChange = (e: ChangeEvent<any>) => {
    const { name, value } = e.target;
    e.persist();
    setValues(prevState => ({ ...prevState, [name]: value }));
  };

  return {
    handleChange,
    handleSubmit,
    values,
  };
};

export default useForm;
