import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => createStyles({
    root: { flexWrap: 'nowrap' },
    spacingLeft: {
        marginTop : 20,
        marginLeft: 20,
        overflowY: 'auto'
      },
    contentBottom: {
      [theme.breakpoints.down('sm')]: {},
      '@media (max-width: 959.95px)': {
        marginTop: 0,
      },
      margin: 'auto'
    },
}));

export default useStyles;
