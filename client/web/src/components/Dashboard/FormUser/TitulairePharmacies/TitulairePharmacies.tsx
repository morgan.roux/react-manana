import React, { FC, useState, useEffect } from 'react';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { getGroupement } from '../../../../services/LocalStorage';
import {
  CREATE_USER_TITULAIRE,
  CREATE_USER_TITULAIREVariables,
} from '../../../../graphql/User/Titulaire/types/CREATE_USER_TITULAIRE';
import { DO_CREATE_USER_TITULAIRE } from '../../../../graphql/User/Titulaire/mutation';
import { TITULAIRE, TITULAIREVariables } from '../../../../graphql/Titulaire/types/TITULAIRE';
import {
  TITULAIRE_PHARMACIES,
  TITULAIRE_PHARMACIESVariables,
} from '../../../../graphql/Titulaire/types/TITULAIRE_PHARMACIES';
import { GET_TITULAIRE, GET_PHARMACIES_BY_TITULAIRE } from '../../../../graphql/Titulaire/query';
import { Formulaire } from '../Formulaire';
import { IRoles } from '../FormUser';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import InfoFormUser from '../InfoFormUser';
import { getMonthNumber, getMonthName, isInvalidArray } from '../../../../utils/Helpers';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../graphql/S3';
import { uploadToS3 } from '../../../../services/S3';
import { TypeFichier, FichierInput } from '../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../utils/filenameFormater';
import Backdrop from '../../../Common/Backdrop';
import {
  UPDATE_MANY_TACHE_RESPONSABLES as UPDATE_MANY_TACHE_RESPONSABLES_TYPE,
  UPDATE_MANY_TACHE_RESPONSABLESVariables,
} from './../../../../federation/demarche-qualite/matrice-tache/types/UPDATE_MANY_TACHE_RESPONSABLES';
import {
  GENERATE_ONE_COLLABORATEUR_FONCTION as GENERATE_ONE_COLLABORATEUR_FONCTION_TYPE,
  GENERATE_ONE_COLLABORATEUR_FONCTIONVariables,
} from './../../../../federation/demarche-qualite/matrice-tache/types/GENERATE_ONE_COLLABORATEUR_FONCTION';
import {
  UPDATE_MANY_TACHE_RESPONSABLES,
  GENERATE_ONE_COLLABORATEUR_FONCTION,
} from './../../../../federation/demarche-qualite/matrice-tache/mutation';
import { TacheResponsableInfo } from './../../../../federation/demarche-qualite/matrice-tache/types/TacheResponsableInfo';
import { FEDERATION_CLIENT } from '../../DemarcheQualite/apolloClientFederation';

interface TitulairePharmaciesProps {
  id: string;
  userId: string;
  goTo: () => any;
}

export interface IFragementPharmacie {
  id: string;
  nom: string;
  mail: string;
  adresse: string;
  tel: string;
  users: IUsers[];
}

export interface IUsers {
  id: string;
  userName: string;
  email: string;
  status: string;
}

const initUser = {
  id: '',
  userName: '',
  email: '',
  status: '',
};

const initPharmacie = {
  id: '',
  nom: '',
  mail: '',
  adresse: '',
  tel: '',
  users: [initUser],
};

const TitulairePharmacies: FC<TitulairePharmaciesProps> = ({ id, userId, goTo }) => {
  const [nom, setNom] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [code, setCode] = useState<string>('');
  const [roles, setRoles] = useState<IRoles[]>([{ code: '', nom: '' }]);
  const [pharmacies, setPharmacies] = useState<IFragementPharmacie[]>([initPharmacie]);
  const [pharmacie, setPharmacie] = useState<IFragementPharmacie>(initPharmacie);
  const [login, setLogin] = useState<string>('');

  /** birth date */
  const [days, setDays] = useState<string>('');
  const [month, setMonth] = useState<string>('');
  const [year, setYear] = useState<string>('');

  /** Photo */
  const [photo, setPhoto] = useState<File | any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const [permissionsAccess, setPermissionsAccess] = useState<any[]>([]);
  const permissionsAccessCodes = permissionsAccess.map((i: any) => i && i.code);

  const [matriceTacheResponsables, setMatriceTacheResponsables] = useState<TacheResponsableInfo[]>(
    [],
  );

  const client = useApolloClient();
  const groupement = getGroupement();

  const [updateManyTacheResponsables] = useMutation<
    UPDATE_MANY_TACHE_RESPONSABLES_TYPE,
    UPDATE_MANY_TACHE_RESPONSABLESVariables
  >(UPDATE_MANY_TACHE_RESPONSABLES, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      displaySucessMessage();
    },
    onError: () => {
      setLoading(false);
      displaySnackBar(client, {
        type: 'ERROR',
        message: 'Erreur lors de la modification de la matrice des fonctions de base',
        isOpen: true,
      });
    },
  });

  const [generateCollaborateurProjetPerso] = useMutation<
    GENERATE_ONE_COLLABORATEUR_FONCTION_TYPE,
    GENERATE_ONE_COLLABORATEUR_FONCTIONVariables
  >(GENERATE_ONE_COLLABORATEUR_FONCTION, {
    client: FEDERATION_CLIENT,
    onError: () => {
      setLoading(false);
      displaySnackBar(client, {
        type: 'ERROR',
        message: 'Erreur lors de la modification de la création du projet perso du collaborateur',
        isOpen: true,
      });
    },
  });

  const displaySucessMessage = () => {
    setLoading(false);
    const snackBarData: SnackVariableInterface = {
      type: 'SUCCESS',
      message: `Mail de définition de mot de passe envoyé à l'utilisateur`,
      isOpen: true,
    };
    displaySnackBar(client, snackBarData);
    goTo();
  };

  const upsertMatriceTaches = (idUser: string, idPharmacie: string) => {
    updateManyTacheResponsables({
      variables: {
        idUser,
        idPharmacie,
        taches: matriceTacheResponsables.map(({ idFonction, idTache, idType }) => ({ idFonction, idTache, idType })),
      },
    });

    generateCollaborateurProjetPerso({
      variables: {
        idPharmacie,
        idUser,
      },
    });
  };

  const { loading: getLoading } = useQuery<TITULAIRE, TITULAIREVariables>(GET_TITULAIRE, {
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: (data) => {
      if (data && data.titulaire) {
        const nom = data.titulaire.nom ? data.titulaire.nom.trim() : '';
        const prenom = data.titulaire.prenom ? data.titulaire.prenom.trim() : '';
        setEmail('');
        /* if (data.titulaire.mail) setEmail(data.titulaire.mail.trim()); */
        setNom(`${prenom} ${nom}`);

        if (userId) {
          const titUser =
            data.titulaire.users && data.titulaire.users.find((u) => u && u.id === userId);
          if (titUser && titUser.email) {
            setEmail(titUser.email);
          }

          const defaultLogin = titUser ? titUser.login || titUser.email : '';
          setLogin(defaultLogin || '');

          if (data.titulaire.pharmacieUser && data.titulaire.pharmacieUser.id) {
            setCode(data.titulaire.pharmacieUser.id);
          }

          if (titUser && titUser.anneeNaissance) {
            setYear(titUser.anneeNaissance.toString());
          }

          if (titUser && titUser.moisNaissance) {
            setMonth(getMonthName(titUser.moisNaissance));
          }

          if (titUser && titUser.jourNaissance) {
            setDays(titUser.jourNaissance.toString());
          }

          /**
           * Set photo on edit
           */
          const newPhoto =
            data.titulaire && titUser && titUser.userPhoto && titUser.userPhoto.fichier;
          if (newPhoto) {
            setPhoto(newPhoto);
          }

          /**
           * Set selected permissionsAccess
           */
          const codeTraitements = data.titulaire && titUser && titUser.codeTraitements;
          if (codeTraitements && codeTraitements.length > 0) {
            const newUserTraitements: any[] = codeTraitements.map((i) => {
              if (i) return { code: i };
            });
            setPermissionsAccess(newUserTraitements);
          }
        }
      }
    },
  });

  const { loading: getPharmacieLoading } = useQuery<
    TITULAIRE_PHARMACIES,
    TITULAIRE_PHARMACIESVariables
  >(GET_PHARMACIES_BY_TITULAIRE, {
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: (data) => {
      if (data && data.titulairePharmacies && data.titulairePharmacies) {
        if (data.titulairePharmacies[0] !== null) {
          const pharmacies = [initPharmacie];
          setRoles(
            data.titulairePharmacies.map((pharmacie: any) => {
              const id = pharmacie.id ? pharmacie.id : '';
              const cip = pharmacie.cip ? pharmacie.cip : '';
              const nom = pharmacie.nom ? pharmacie.nom : '';
              const mail = pharmacie.mail ? pharmacie.mail : '';
              const adresse = pharmacie.adresse1
                ? pharmacie.adresse1
                : pharmacie.adresse2
                  ? pharmacie.adresse2
                  : '';
              const users = pharmacie.users ? pharmacie.users : [];
              const tel = pharmacie.tele1
                ? pharmacie.tele1
                : pharmacie.tele2
                  ? pharmacie.tele2
                  : '';
              pharmacies.push({ id, nom, mail, adresse, tel, users });
              return { code: id, nom: `${cip} - ${nom}` };
            }),
          );
          setPharmacies(pharmacies);
        }
      }
    },
  });

  /**
   * Mutation createPutPresignedUrl
   */
  const [doCreatePutPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async (data) => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        photo &&
        photo.size
      ) {
        const presignedPhoto = data.createPutPresignedUrls[0];
        if (presignedPhoto && presignedPhoto.filePath && presignedPhoto.presignedUrl) {
          setLoading(true);
          await uploadToS3(photo, presignedPhoto.presignedUrl)
            .then((result) => {
              if (result && result.status === 200) {
                createUserTitulaire({
                  variables: {
                    idGroupement: groupement.id,
                    id,
                    email,
                    login,
                    idPharmacie: code,
                    userId,
                    day: Number(days),
                    month: getMonthNumber(month),
                    year: Number(year),
                    userPhoto: {
                      chemin: presignedPhoto.filePath,
                      nomOriginal: photo.name,
                      type: TypeFichier.PHOTO,
                    },
                    codeTraitements: !isInvalidArray(permissionsAccessCodes)
                      ? permissionsAccessCodes
                      : null,
                  },
                });
              }
            })
            .catch((error) => {
              console.error(error);
              setLoading(false);
              displaySnackBar(client, {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              });
            });
        }
      }
    },
    onError: (errors) => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      });
    },
  });

  const [createUserTitulaire, { loading: createUserLoading }] = useMutation<
    CREATE_USER_TITULAIRE,
    CREATE_USER_TITULAIREVariables
  >(DO_CREATE_USER_TITULAIRE, {
    onCompleted: (data) => {
      if (data && data.createUserTitulaire && data.createUserTitulaire.pharmacieUser?.id) {
        const user = (data.createUserTitulaire?.users || []).find((user) => user?.login === login);
        if (user) {
          upsertMatriceTaches(user.id, data.createUserTitulaire.pharmacieUser.id);
        } else {
          displaySucessMessage();
        }
      }
    },
    onError: (errors) => {
      setLoading(false);
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map((error) => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'MAIL_NOT_SENT':
              errorMessage = "L'email n'est pas envoyé à l'utilisatuer";
              break;
            case 'LOGIN_ALREADY_EXIST':
              errorMessage = 'Ce login est déjà utilisé par un autre utilisateur';
              break;
            case 'ACCOUNT_ALREADY_CREATED':
              errorMessage = 'Le titulaire a déjà un compte créer sur cette pharmacie';
              break;
          }
        }
      });

      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errorMessage,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  useEffect(() => {
    pharmacies.map((pharmacie) => {
      if (pharmacie.id === code) setPharmacie(pharmacie);
    });
  }, [pharmacies]);

  useEffect(() => {
    if (roles && roles.length === 1) {
      setCode(roles[0].code);
    }
  }, [roles]);

  const handleChange = (name: string, value: string) => {
    switch (name) {
      case 'email':
        setEmail(value);
        break;
      case 'login':
        setLogin(value);
        break;
      case 'days':
        setDays(value);
        break;
      case 'month':
        setMonth(value);
        break;
      case 'year':
        setYear(value);
        break;
      default:
        setCode(value);
    }
  };

  const submit = () => {
    setLoading(true);
    if (photo && photo.size) {
      // Presigned file
      const filePathsTab: string[] = [];
      filePathsTab.push(
        `users/photos/${groupement && groupement.id}/${formatFilenameWithoutDate(photo)}`,
      );
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
      return;
    }

    let userPhoto: FichierInput | null = null;
    if (photo && photo.id) {
      userPhoto = {
        chemin: photo.chemin,
        nomOriginal: photo.nomOriginal,
        type: TypeFichier.AVATAR,
        idAvatar: photo && photo.idAvatar,
      };
    }
    createUserTitulaire({
      variables: {
        idGroupement: groupement.id,
        id,
        email,
        login,
        idPharmacie: code,
        userId,
        day: Number(days),
        month: getMonthNumber(month),
        year: Number(year),
        userPhoto,
        codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
      },
    });
  };

  return (
    <>
      {(createUserLoading || presignedLoading || loading) && <Backdrop />}
      <Formulaire
        matriceTacheResponsables={matriceTacheResponsables}
        onChangeMatriceTacheResponsables={setMatriceTacheResponsables}
        idPharmacie={code}
        user={{ nom, email, login }}
        values={{ code, roles, year, month, days }}
        label="CIP"
        userId={userId}
        doCreateOrUpdate={submit}
        handleChange={handleChange}
        goTo={goTo}
        photo={photo}
        setPhoto={setPhoto}
        permissionsAccess={permissionsAccess}
        setPermissionsAccess={setPermissionsAccess}
        loading={getLoading || getPharmacieLoading}
      >
        {pharmacie && pharmacie.id && (
          <div>
            <InfoFormUser pharmacie={pharmacie} />
          </div>
        )}
      </Formulaire>
    </>
  );
};

export default TitulairePharmacies;
