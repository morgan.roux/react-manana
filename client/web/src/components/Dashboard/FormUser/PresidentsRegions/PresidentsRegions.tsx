import React, { FC, useState, useEffect } from 'react';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { getGroupement } from '../../../../services/LocalStorage';
import {
  CREATE_USER_PRSIDENT,
  CREATE_USER_PRSIDENTVariables,
} from '../../../../graphql/User/President/types/CREATE_USER_PRSIDENT';
import { DO_CREATE_USER_PRSIDENT } from '../../../../graphql/User/President/mutation';
import { TITULAIRE, TITULAIREVariables } from '../../../../graphql/Titulaire/types/TITULAIRE';
import { GET_TITULAIRE, GET_PHARMACIES_BY_TITULAIRE } from '../../../../graphql/Titulaire/query';
import { Formulaire } from '../Formulaire';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { IRoles } from '../FormUser';
import { IFragementPharmacie } from '../TitulairePharmacies/TitulairePharmacies';
import {
  TITULAIRE_PHARMACIES,
  TITULAIRE_PHARMACIESVariables,
} from '../../../../graphql/Titulaire/types/TITULAIRE_PHARMACIES';
import InfoFormUser from '../InfoFormUser';
import { getMonthNumber, getMonthName, isInvalidArray } from '../../../../utils/Helpers';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../graphql/S3';
import { uploadToS3 } from '../../../../services/S3';
import { TypeFichier, FichierInput } from '../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../utils/filenameFormater';
import Backdrop from '../../../Common/Backdrop';

interface PresidentsRegionsProps {
  id: string;
  userId: string;
  goTo: () => any;
}

const initUser = {
  id: '',
  userName: '',
  email: '',
  status: '',
};

const initPharmacie = {
  id: '',
  nom: '',
  mail: '',
  adresse: '',
  tel: '',
  users: [initUser],
};

const PresidentsRegions: FC<PresidentsRegionsProps> = ({ id, userId, goTo }) => {
  const [nom, setNom] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [code, setCode] = useState<string>('');
  const [roles, setRoles] = useState<IRoles[]>([{ code: '', nom: '' }]);
  const [pharmacies, setPharmacies] = useState<IFragementPharmacie[]>([initPharmacie]);
  const [pharmacie, setPharmacie] = useState<IFragementPharmacie>(initPharmacie);
  const [login, setLogin] = useState<string>('');

  /** birth date */
  const [days, setDays] = useState<string>('');
  const [month, setMonth] = useState<string>('');
  const [year, setYear] = useState<string>('');

  /** Photo */
  const [photo, setPhoto] = useState<File | any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const [permissionsAccess, setPermissionsAccess] = useState<any[]>([]);
  const permissionsAccessCodes = permissionsAccess.map((i: any) => i && i.code);

  const client = useApolloClient();
  const groupement = getGroupement();

  const { loading: getLoading } = useQuery<TITULAIRE, TITULAIREVariables>(GET_TITULAIRE, {
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: data => {
      if (data && data.titulaire) {
        const nom = data.titulaire.nom ? data.titulaire.nom.trim() : '';
        const prenom = data.titulaire.prenom ? data.titulaire.prenom.trim() : '';
        setEmail('');
        /* if (data.titulaire.mail) setEmail(data.titulaire.mail.trim()); */
        setNom(`${nom} ${prenom}`);

        if (userId) {
          const titUser =
            data.titulaire.users && data.titulaire.users.find(u => u && u.id === userId);
          if (titUser && titUser.email) {
            setEmail(titUser.email);
          }

          const defaultLogin = titUser ? titUser.login || titUser.email : ''
          setLogin(defaultLogin || '');

          if (data.titulaire.pharmacieUser && data.titulaire.pharmacieUser.id) {
            setCode(data.titulaire.pharmacieUser.id);
          }

          if (titUser && titUser.anneeNaissance) {
            setYear(titUser.anneeNaissance.toString());
          }

          if (titUser && titUser.moisNaissance) {
            setMonth(getMonthName(titUser.moisNaissance));
          }

          if (titUser && titUser.jourNaissance) {
            setDays(titUser.jourNaissance.toString());
          }

          /**
           * Set photo on edit
           */
          const newPhoto =
            data.titulaire && titUser && titUser.userPhoto && titUser.userPhoto.fichier;
          if (newPhoto) {
            setPhoto(newPhoto);
          }

          /**
           * Set selected permissionsAccess
           */
          const codeTraitements = data.titulaire && titUser && titUser.codeTraitements;
          if (codeTraitements && codeTraitements.length > 0) {
            const newUserTraitements: any[] = codeTraitements.map(i => {
              if (i) return { code: i };
            });
            setPermissionsAccess(newUserTraitements);
          }
        }
      }
      if (
        data &&
        data.titulaire &&
        data.titulaire.users &&
        data.titulaire.users.length > 0 &&
        data.titulaire.users[0] &&
        data.titulaire.users[0].login
      ) {
        setLogin(data.titulaire.users[0].login);
      }
    },
  });

  /**
   * Mutation createPutPresignedUrl
   */
  const [doCreatePutPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        photo &&
        photo.size
      ) {
        const presignedPhoto = data.createPutPresignedUrls[0];
        if (presignedPhoto && presignedPhoto.filePath && presignedPhoto.presignedUrl) {
          setLoading(true);
          await uploadToS3(photo, presignedPhoto.presignedUrl)
            .then(result => {
              if (result && result.status === 200) {
                createUserPresidentRegion({
                  variables: {
                    idGroupement: groupement.id,
                    id,
                    email,
                    login,
                    userId,
                    idPharmacie: code,
                    day: Number(days),
                    month: getMonthNumber(month),
                    year: Number(year),
                    userPhoto: {
                      chemin: presignedPhoto.filePath,
                      nomOriginal: photo.name,
                      type: TypeFichier.PHOTO,
                    },
                    codeTraitements: !isInvalidArray(permissionsAccessCodes)
                      ? permissionsAccessCodes
                      : null,
                  },
                });
              }
            })
            .catch(error => {
              console.error(error);
              setLoading(false);
              displaySnackBar(client, {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              });
            });
        }
      }
    },
    onError: errors => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      });
    },
  });

  const [createUserPresidentRegion, { loading: createUserLoading }] = useMutation<
    CREATE_USER_PRSIDENT,
    CREATE_USER_PRSIDENTVariables
  >(DO_CREATE_USER_PRSIDENT, {
    onCompleted: data => {
      if (data && data.createUserPresidentRegion) {
        setLoading(false);
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Mail de définition de mot de passe envoyé à l'utilisateur`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        goTo();
      }
    },
    onError: errors => {
      setLoading(false);
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map(error => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'MAIL_NOT_SENT':
              errorMessage = "L'email n'est pas envoyé à l'utilisatuer";
              break;
            case 'LOGIN_ALREADY_EXIST':
              errorMessage = 'Cet email est déjà utilisé par un autre utilisateur';
              break;
            case 'ACCOUNT_ALREADY_CREATED':
              errorMessage = 'Le président de région a déjà un compte créer sur cette pharmacie';
              break;
          }
        }
      });

      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errorMessage,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const { loading: getPharmacieLoading } = useQuery<
    TITULAIRE_PHARMACIES,
    TITULAIRE_PHARMACIESVariables
  >(GET_PHARMACIES_BY_TITULAIRE, {
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: data => {
      if (data && data.titulairePharmacies && data.titulairePharmacies) {
        if (data.titulairePharmacies[0] !== null) {
          const pharmacies = [initPharmacie];
          setRoles(
            data.titulairePharmacies.map((pharmacie: any) => {
              const id = pharmacie.id ? pharmacie.id : '';
              const cip = pharmacie.cip ? pharmacie.cip : '';
              const nom = pharmacie.nom ? pharmacie.nom : '';
              const mail = pharmacie.mail ? pharmacie.mail : '';
              const adresse = pharmacie.adresse1
                ? pharmacie.adresse1
                : pharmacie.adresse2
                ? pharmacie.adresse2
                : '';
              const users = pharmacie.users ? pharmacie.users : [];
              const tel = pharmacie.tele1
                ? pharmacie.tele1
                : pharmacie.tele2
                ? pharmacie.tele2
                : '';
              pharmacies.push({ id, nom, mail, adresse, tel, users });
              return { code: id, nom: `${cip} - ${nom}` };
            }),
          );
          setPharmacies(pharmacies);
        }
      }
    },
  });

  useEffect(() => {
    pharmacies.map(pharmacie => {
      if (pharmacie.id === code) setPharmacie(pharmacie);
    });
  }, [pharmacies]);

  useEffect(() => {
    if (roles && roles.length === 1) {
      setCode(roles[0].code);
    }
  }, [roles]);

  const handleChange = (name: string, value: string) => {
    console.log('name value', name, value);

    switch (name) {
      case 'code':
        setCode(value);
        break;
      case 'email':
        setEmail(value);
        break;
      case 'days':
        setDays(value);
        break;
      case 'month':
        setMonth(value);
        break;
      case 'year':
        setYear(value);
        break;
      case 'login':
        setLogin(value);
        break;
    }
  };

  const submit = () => {
    setLoading(true);
    if (photo && photo.size) {
      // Presigned file
      const filePathsTab: string[] = [];
      filePathsTab.push(
        `users/photos/${groupement && groupement.id}/${formatFilenameWithoutDate(photo)}`,
      );
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
      return;
    }

    let userPhoto: FichierInput | null = null;
    if (photo && photo.id) {
      userPhoto = {
        chemin: photo.chemin,
        nomOriginal: photo.nomOriginal,
        type: TypeFichier.AVATAR,
        idAvatar: photo && photo.idAvatar,
      };
    }
    createUserPresidentRegion({
      variables: {
        idGroupement: groupement.id,
        id,
        email,
        login,
        userId,
        idPharmacie: code,
        day: Number(days),
        month: getMonthNumber(month),
        year: Number(year),
        userPhoto,
        codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
      },
    });
  };

  return (
    <>
      {(createUserLoading || presignedLoading || loading) && <Backdrop />}
      <Formulaire
        user={{ nom, email, login }}
        userId={userId}
        values={{ code, roles, days, month, year }}
        label="Pharmacie"
        doCreateOrUpdate={submit}
        handleChange={handleChange}
        goTo={goTo}
        photo={photo}
        setPhoto={setPhoto}
        permissionsAccess={permissionsAccess}
        setPermissionsAccess={setPermissionsAccess}
        loading={getLoading || getPharmacieLoading}
      >
        {pharmacie && pharmacie.id && (
          <div>
            <InfoFormUser pharmacie={pharmacie} />
          </div>
        )}
      </Formulaire>
    </>
  );
};

export default PresidentsRegions;
