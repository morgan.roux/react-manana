import React, { FC, useState, useEffect } from 'react';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { Theme } from '@material-ui/core/styles';
import withStyles, { CSSProperties } from '@material-ui/core/styles/withStyles';
import { getGroupement } from '../../../../services/LocalStorage';
import { Formulaire } from '../Formulaire';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import {
  PERSONNEL_PHARMACY,
  PERSONNEL_PHARMACYVariables,
} from '../../../../graphql/Ppersonnel/types/PERSONNEL_PHARMACY';
import { GET_PERSONNEL_PHARMACY } from '../../../../graphql/Ppersonnel/query';
import {
  CREATE_USER_COLLABORATEUR,
  CREATE_USER_COLLABORATEURVariables,
} from '../../../../graphql/User/Collaborateur/types/CREATE_USER_COLLABORATEUR';
import { DO_CREATE_USER_COLLABORATEUR } from '../../../../graphql/User/Collaborateur/mutation';
import { getMonthNumber, getMonthName, isInvalidArray } from '../../../../utils/Helpers';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../graphql/S3';
import { uploadToS3 } from '../../../../services/S3';
import { TypeFichier, FichierInput } from '../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../utils/filenameFormater';
import Backdrop from '../../../Common/Backdrop';
import { SEARCH_ROLES, SEARCH_ROLESVariables } from '../../../../graphql/Role/types/SEARCH_ROLES';
import { DO_SEARCH_ROLES } from '../../../../graphql/Role';
import {
  UPDATE_MANY_TACHE_RESPONSABLES as UPDATE_MANY_TACHE_RESPONSABLES_TYPE,
  UPDATE_MANY_TACHE_RESPONSABLESVariables,
} from './../../../../federation/demarche-qualite/matrice-tache/types/UPDATE_MANY_TACHE_RESPONSABLES';
import {
  GENERATE_ONE_COLLABORATEUR_FONCTION as GENERATE_ONE_COLLABORATEUR_FONCTION_TYPE,
  GENERATE_ONE_COLLABORATEUR_FONCTIONVariables,
} from './../../../../federation/demarche-qualite/matrice-tache/types/GENERATE_ONE_COLLABORATEUR_FONCTION';
import {
  UPDATE_MANY_TACHE_RESPONSABLES,
  GENERATE_ONE_COLLABORATEUR_FONCTION,
} from './../../../../federation/demarche-qualite/matrice-tache/mutation';
import { TacheResponsableInfo } from './../../../../federation/demarche-qualite/matrice-tache/types/TacheResponsableInfo';
import { FEDERATION_CLIENT } from '../../DemarcheQualite/apolloClientFederation';

const styles = (theme: Theme): Record<string, CSSProperties> => ({});

interface PROPS {
  id: string;
  userId: string;
  goTo: () => any;
}

// const initiRoles = [
//   {
//     code: COLLABORATEUR_PHARMACIE,
//     nom: 'Collaborateur Officine',
//   },
// ];

const PersonnelPharmacies: FC<PROPS> = ({ id, userId, goTo }) => {
  const [nom, setNom] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [emailLink, setEmailLink] = useState<string>('');
  const [idPharmacie, setIdPharmacie] = useState<string>('');
  const [code, setCode] = useState<string>('');
  const [roles, setRoles] = useState<any[]>([]);
  const [login, setLogin] = useState<string>('');

  /** birth date */
  const [days, setDays] = useState<string>('');
  const [month, setMonth] = useState<string>('');
  const [year, setYear] = useState<string>('');

  /** Photo */
  const [photo, setPhoto] = useState<File | any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const [permissionsAccess, setPermissionsAccess] = useState<any[]>([]);
  const permissionsAccessCodes = permissionsAccess.map((i: any) => i && i.code);

  const [matriceTacheResponsables, setMatriceTacheResponsables] = useState<TacheResponsableInfo[]>(
    [],
  );

  const client = useApolloClient();
  const groupement = getGroupement();

  /**
   * Search roles
   */
  const { data: dataRoles, loading: loadingRoles } = useQuery<SEARCH_ROLES, SEARCH_ROLESVariables>(
    DO_SEARCH_ROLES,
    {
      variables: {
        type: ['role'],
        filterBy: [{ term: { typeRole: 'PHARMACIE' } }],
        sortBy: [{ nom: { order: 'asc' } }],
      },
    },
  );

  const [updateManyTacheResponsables, modificationManyTacheResponsables] = useMutation<
    UPDATE_MANY_TACHE_RESPONSABLES_TYPE,
    UPDATE_MANY_TACHE_RESPONSABLESVariables
  >(UPDATE_MANY_TACHE_RESPONSABLES, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      displaySucessMessage();
    },
    onError: () => {
      setLoading(false);
      displaySnackBar(client, {
        type: 'ERROR',
        message: 'Erreur lors de la modification de la matrice des fonctions de base',
        isOpen: true,
      });
    },
  });

  const [generateCollaborateurProjetPerso] = useMutation<
    GENERATE_ONE_COLLABORATEUR_FONCTION_TYPE,
    GENERATE_ONE_COLLABORATEUR_FONCTIONVariables
  >(GENERATE_ONE_COLLABORATEUR_FONCTION, {
    client: FEDERATION_CLIENT,
    onError: () => {
      setLoading(false);
      displaySnackBar(client, {
        type: 'ERROR',
        message: 'Erreur lors de la modification de la création du projet perso du collaborateur',
        isOpen: true,
      });
    },
  });

  const upsertMatriceTaches = (idUser: string, idPharmacie: string) => {
    updateManyTacheResponsables({
      variables: {
        idUser,
        idPharmacie,
        taches: matriceTacheResponsables.map(({ idFonction, idTache, idType }) => ({ idFonction, idTache, idType })),
      },
    });

    generateCollaborateurProjetPerso({
      variables: {
        idPharmacie,
        idUser,
      },
    });
  };

  /**
   * Set init roles
   */
  useEffect(() => {
    if (
      dataRoles &&
      dataRoles.search &&
      dataRoles.search.data &&
      dataRoles.search.data.length > 0
    ) {
      setRoles(dataRoles.search.data);
    }
  }, [dataRoles]);

  const { loading: getLoading, data: personnelData } = useQuery<
    PERSONNEL_PHARMACY,
    PERSONNEL_PHARMACYVariables
  >(GET_PERSONNEL_PHARMACY, {
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: (data) => {
      if (data && data.ppersonnel) {
        const nom = data.ppersonnel.nom ? data.ppersonnel.nom : '';
        const prenom = data.ppersonnel.prenom ? data.ppersonnel.prenom : '';
        setNom(`${prenom} ${nom}`);
        if (data.ppersonnel.pharmacie && data.ppersonnel.pharmacie.id) {
          setIdPharmacie(data.ppersonnel.pharmacie.id);
        }
        const defaultEmail =
          userId && data.ppersonnel.user && data.ppersonnel.user.email
            ? data.ppersonnel.user.email
            : '';
        setEmail(defaultEmail);

        const defaultLogin =
          userId && data.ppersonnel.user && data.ppersonnel.user.login
            ? data.ppersonnel.user.login
            : defaultEmail;

        setLogin(defaultLogin);

        if (userId && data.ppersonnel.role && data.ppersonnel.role.code) {
          setCode(data.ppersonnel.role.code);
        }
        // setRoles(initiRoles);
        if (data.ppersonnel.user && data.ppersonnel.user.anneeNaissance) {
          setYear(data.ppersonnel.user.anneeNaissance.toString());
        }
        if (data.ppersonnel.user && data.ppersonnel.user.moisNaissance) {
          setMonth(getMonthName(data.ppersonnel.user.moisNaissance));
        }
        if (data.ppersonnel.user && data.ppersonnel.user.jourNaissance) {
          setDays(data.ppersonnel.user.jourNaissance.toString());
        }
        if (data.ppersonnel.user && data.ppersonnel.user.login) {
          setLogin(data.ppersonnel.user.login);
        }

        if (
          data.ppersonnel.pharmacie &&
          data.ppersonnel.pharmacie.titulaires &&
          data.ppersonnel.pharmacie.titulaires &&
          data.ppersonnel.pharmacie.titulaires.length > 0
        ) {
          const titulaire = data.ppersonnel.pharmacie.titulaires[0];
          const userTitulaire =
            titulaire && titulaire.users && titulaire.users.length > 0 && titulaire.users[0];
          if (userTitulaire && userTitulaire.email) {
            // setEmailLink(userTitulaire.email);
          }
        }

        /**
         * Set photo on edit
         */
        const newPhoto =
          data.ppersonnel &&
          data.ppersonnel.user &&
          data.ppersonnel.user.userPhoto &&
          data.ppersonnel.user.userPhoto.fichier;
        if (newPhoto) {
          setPhoto(newPhoto);
        }

        /**
         * Set selected permissionsAccess
         */
        const codeTraitements =
          data.ppersonnel && data.ppersonnel.user && data.ppersonnel.user.codeTraitements;
        if (codeTraitements && codeTraitements.length > 0) {
          const newUserTraitements: any[] = codeTraitements.map((i) => {
            if (i) return { code: i };
          });
          setPermissionsAccess(newUserTraitements);
        }
      }
    },
  });

  /**
   * Mutation createPutPresignedUrl
   */
  const [doCreatePutPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async (data) => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        photo &&
        photo.size
      ) {
        const presignedPhoto = data.createPutPresignedUrls[0];
        if (presignedPhoto && presignedPhoto.filePath && presignedPhoto.presignedUrl) {
          setLoading(true);
          await uploadToS3(photo, presignedPhoto.presignedUrl)
            .then((result) => {
              if (result && result.status === 200) {
                createUserCollaborateurOfficine({
                  variables: {
                    idGroupement: groupement.id,
                    id,
                    idPharmacie,
                    email,
                    login,
                    role: code,
                    userId,
                    day: Number(days),
                    month: getMonthNumber(month),
                    year: Number(year),
                    userPhoto: {
                      chemin: presignedPhoto.filePath,
                      nomOriginal: photo.name,
                      type: TypeFichier.PHOTO,
                    },
                    codeTraitements: !isInvalidArray(permissionsAccessCodes)
                      ? permissionsAccessCodes
                      : null,
                  },
                });
              }
            })
            .catch((error) => {
              console.error(error);
              setLoading(false);
              displaySnackBar(client, {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              });
            });
        }
      }
    },
    onError: (errors) => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      });
    },
  });

  const displaySucessMessage = () => {
    setLoading(false);
    const snackBarData: SnackVariableInterface = {
      type: 'SUCCESS',
      message: `Mail de définition de mot de passe envoyé à l'utilisateur`,
      isOpen: true,
    };
    displaySnackBar(client, snackBarData);
    goTo();
  };

  const [createUserCollaborateurOfficine, { loading: createUserLoading }] = useMutation<
    CREATE_USER_COLLABORATEUR,
    CREATE_USER_COLLABORATEURVariables
  >(DO_CREATE_USER_COLLABORATEUR, {
    onCompleted: (data) => {
      if (data && data.createUserCollaborateurOfficine) {
        if (
          data.createUserCollaborateurOfficine.user?.id &&
          data.createUserCollaborateurOfficine.idPharmacie
        ) {
          upsertMatriceTaches(
            data.createUserCollaborateurOfficine.user.id,
            data.createUserCollaborateurOfficine.idPharmacie,
          );
        } else {
          displaySucessMessage();
        }
      }
    },
    onError: (errors) => {
      setLoading(false);
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map((error) => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'MAIL_NOT_SENT':
              errorMessage = "L'email n'est pas envoyé à l'utilisatuer";
              break;
            case 'LOGIN_ALREADY_EXIST':
              errorMessage = 'Ce login est déjà utilisé par un autre utilisateur';
              break;
            case 'ACCOUNT_ALREADY_CREATED':
              errorMessage = 'Compte est déjà créé pour ce personnel';
              break;
          }
        }
      });

      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errorMessage,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleChange = (name: string, value: string) => {
    switch (name) {
      case 'email':
        setEmail(value);
        break;
      case 'login':
        setLogin(value);
        break;
      case 'days':
        setDays(value);
        break;
      case 'month':
        setMonth(value);
        break;
      case 'year':
        setYear(value);
        break;
      default:
        setCode(value);
    }
  };

  const submit = () => {
    setLoading(true);
    if (photo && photo.size) {
      // Presigned file
      const filePathsTab: string[] = [];
      filePathsTab.push(
        `users/photos/${groupement && groupement.id}/${formatFilenameWithoutDate(photo)}`,
      );
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
      return;
    }

    let userPhoto: FichierInput | null = null;
    if (photo && photo.id) {
      userPhoto = {
        chemin: photo.chemin,
        nomOriginal: photo.nomOriginal,
        type: TypeFichier.AVATAR,
        idAvatar: photo && photo.idAvatar,
      };
    }
    createUserCollaborateurOfficine({
      variables: {
        idGroupement: groupement.id,
        id,
        idPharmacie,
        email,
        login,
        role: code,
        userId,
        day: Number(days),
        month: getMonthNumber(month),
        year: Number(year),
        userPhoto,
        codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
      },
    });
  };

  return (
    <>
      {(createUserLoading ||
        presignedLoading ||
        loading ||
        loadingRoles ||
        modificationManyTacheResponsables.loading) && <Backdrop />}
      <Formulaire
        idPharmacie={idPharmacie}
        matriceTacheResponsables={matriceTacheResponsables}
        onChangeMatriceTacheResponsables={setMatriceTacheResponsables}
        user={{
          nom,
          email,
          login,
          emailLink,
          userLinks: (personnelData?.ppersonnel?.pharmacie?.titulaires || [])
            .filter((titulaire) => titulaire?.users)
            .reduce((links, titulaire) => {
              return [...(titulaire?.users || []), ...links] as any;
            }, []),
        }}
        userId={userId}
        values={{ code, roles, days, month, year }}
        label="Rôle"
        doCreateOrUpdate={submit}
        handleChange={handleChange}
        goTo={goTo}
        photo={photo}
        setPhoto={setPhoto}
        permissionsAccess={permissionsAccess}
        setPermissionsAccess={setPermissionsAccess}
        loading={getLoading}
      />
    </>
  );
};

export default withStyles(styles)(PersonnelPharmacies);
