import React, { FC, useEffect } from 'react';
import useStyles from '../styles';
import { Box } from '@material-ui/core';
import { useLazyQuery } from '@apollo/react-hooks';
import {
  GET_TACHE_RESPONSABLES as GET_TACHE_RESPONSABLES_TYPE,
  GET_TACHE_RESPONSABLESVariables,
} from './../../../../../federation/demarche-qualite/matrice-tache/types/GET_TACHE_RESPONSABLES';
import { GET_TACHE_RESPONSABLES } from './../../../../../federation/demarche-qualite/matrice-tache/query';
import { FEDERATION_CLIENT } from '../../../DemarcheQualite/apolloClientFederation';
import { ErrorPage, LoaderSmall } from '@app/ui-kit';

import MatriceTacheTree, { MatriceTacheTreeProps } from './MatriceTacheTree/MatriceTacheTree';
import { useNiveauMatriceFonctions } from '../../../DemarcheQualite/MatriceTache/hooks';

const FormulaireMatriceTache: FC<MatriceTacheTreeProps> = ({
  idUser,
  fullNameUser,
  idPharmacie,
  selected,
  onSelectionChange,
}) => {
  const classes = useStyles({});
  const niveauMatriceFonctions = useNiveauMatriceFonctions()

  const [loadTacheResponsables, loadingResponsables] = useLazyQuery<
    GET_TACHE_RESPONSABLES_TYPE,
    GET_TACHE_RESPONSABLESVariables
  >(GET_TACHE_RESPONSABLES, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
    onCompleted: (result) => {
      onSelectionChange(result.dQMTTacheResponsables.nodes);
    },
  });

  useEffect(() => {
    if (idUser) {
      loadTacheResponsables({
        variables: {
          filter: {
            idUser: {
              eq: idUser,
            },
          },
          paging: {
            offset: 0,
            limit: 200,
          },
        },
      });
    }
  }, [idUser]);

  const loading = loadingResponsables.called && loadingResponsables.loading;
  const error = loadingResponsables.called ? loadingResponsables.error : undefined;

  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Box className={classes.title}>{fullNameUser}</Box>
        {loading ? (
          <LoaderSmall />
        ) : error ? (
          <ErrorPage />
        ) : (
          <MatriceTacheTree
            niveauMatriceFonctions={niveauMatriceFonctions}
            idUser={idUser}
            selected={selected}
            onSelectionChange={onSelectionChange}
            idPharmacie={idPharmacie}
            fullNameUser={fullNameUser}
          />
        )}
      </Box>
    </Box>
  );
};

export default FormulaireMatriceTache;
