import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { Box } from '@material-ui/core';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { USER_APPETENCES, USER_APPETENCESVariables } from '../../../../../federation/tools/appetence/types/USER_APPETENCES';
import { GET_USER_APPETENCES } from '../../../../../federation/tools/appetence/query';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import AppetenceList from '../../../../Common/AppetenceList';
import { FEDERATION_CLIENT } from '../../../DemarcheQualite/apolloClientFederation';
import useStyles from '../styles';

interface FormulaireAppetenceProps {
  match: {
    params: {
      userId: string | undefined;
    };
  };
  fullNameUser: string
  setAppetences?: (appetences: any[]) => void;
}

const FormulaireAppetence: FC<FormulaireAppetenceProps & RouteComponentProps> = ({
  match: {
    params: { userId },
  },
  fullNameUser,
  setAppetences,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [itemList, setItemList] = useState<any[]>([]);

  const { data, loading } = useQuery<USER_APPETENCES, USER_APPETENCESVariables>(GET_USER_APPETENCES, {
    client: FEDERATION_CLIENT,
    fetchPolicy: 'cache-and-network',
    variables: {
      idUser: userId || '',
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  const handleOrderChange = (id: string, event: any) => {
    const { value } = event.target;

    setItemList(prevState =>
      prevState.map((item: any, index) =>
        item.id === id ? { ...item, ordre: parseInt(value) } : item,
      ),
    );
  };

  useEffect(() => {
    const appetences = itemList.filter(item => item.ordre);
    if (appetences.length > 0 && setAppetences) {
      setAppetences(appetences);
    }
  }, [itemList]);

  return (
    <div className={classes.content}>
      <Box className={classes.title}>{fullNameUser}</Box>
      <AppetenceList
        handleOrderChange={handleOrderChange}
        itemList={itemList}
        setItemList={setItemList}
        loading={loading}
        appetences={(data?.userAppetences || [])}
        withAddBtn={false}
        sorting={true}
      />
    </div>
  );
};

export default withRouter(FormulaireAppetence);
