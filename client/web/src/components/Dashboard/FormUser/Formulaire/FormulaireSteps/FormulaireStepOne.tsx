import React, { FC, ChangeEvent, useState, useCallback, useEffect, useMemo } from 'react';
import { isEmailValid, isValidYears, isValidDays } from '../../../../../utils/Validator';
import useStyles from '../styles';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import CustomSelect from '../../../../Common/CustomSelect';
import { getMonthNumber, getMonths } from '../../../../../utils/Helpers';
import Dropzone from '../../../../Common/Dropzone';
import { Typography, RadioGroup, FormControlLabel, Radio, Box } from '@material-ui/core';
import { AWS_HOST } from '../../../../../utils/s3urls';
import { last, truncate } from 'lodash';
import { CustomModal } from '../../../../Common/CustomModal';
import { CustomReactEasyCrop } from '../../../../Common/CustomReactEasyCrop';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { FormulaireProps } from '../Formulaire';
import ChoiceAvatarForm from '../../../../Common/ChoiceAvatarForm';

const FormulaireStepOne: FC<FormulaireProps & RouteComponentProps> = ({
  label,
  user,
  values,
  children,
  handleChange,
  photo,
  setPhoto,
  location: { pathname },
}) => {
  const classes = useStyles({});

  const [openResize, setOpenResize] = useState<boolean>(false);
  const [croppedImgUrl, setCroppedImgUrl] = useState<any>('');
  const [savedCroppedImage, setSavedCroppedImage] = useState<boolean>(false);

  const [imgType, setImgType] = useState<'AVATAR' | 'IMPORT'>('AVATAR');
  const [selectedAvatar, setSelectedAvatar] = useState<any>(null);
  const [avatarList, setAvatarList] = useState<any[]>([]);

  const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
  const [croppedFiles, setCroppedFiles] = useState<File[]>([]);
  const [imageUrl, setImageUrl] = useState<string | null | undefined>(undefined);

  const [userPhotoSrc, setUserPhotoSrc] = useState<any>(null);

  const isOnPersonnelPharmacie = pathname.includes('/personnel-pharmacies');
  const isOnTitulairePharmacie = pathname.includes('/titulaires-pharmacies');

  const handleChangeRadioGroup = (event: React.ChangeEvent<HTMLInputElement>) => {
    setImgType((event.target as HTMLInputElement).value as any);
    setSelectedFiles([]);
    setCroppedFiles([]);
    setPhoto(null);
    // setSelectedAvatar(null)
    // setCroppedImgUrl('')
    // setImageUrl(null)
    // setUserPhotoSrc(null)
  };

  const deleteAvatar = () => {
    console.log(' Delete Avatar Photo :>> ');
  };

  const handleClickResize = useCallback(() => {
    setOpenResize(true);
    // Reset cropped image
    setSavedCroppedImage(false);
    setCroppedImgUrl(null);
  }, []);

  const onClickAvatar = (avatar: any) => {
    if (avatar) {
      const photo = { ...avatar.fichier, idAvatar: avatar.id };
      if (photo) setPhoto(photo as any);
    }
  };

  /**
   * Set default selected avatar on edit
   */
  useMemo(() => {
    if (
      pathname.includes('/edit/') &&
      avatarList &&
      avatarList.length > 0 &&
      photo &&
      photo.chemin
    ) {
      const defaultAvatar: any = avatarList.find(
        (i: any) => i && i.fichier && i.fichier.chemin === photo.chemin,
      );
      if (defaultAvatar) {
        if (
          selectedAvatar &&
          selectedAvatar.fichier &&
          defaultAvatar.fichier &&
          selectedAvatar.fichier.chemin !== defaultAvatar.fichier.chemin
        ) {
          // console.log('defaultAvatar :>> ', defaultAvatar);
          setSelectedAvatar(defaultAvatar);
          return;
        }
        setSelectedAvatar(defaultAvatar);
      }
    }
  }, [avatarList, photo, pathname]);

  /**
   * Set set userPhotoSrc
   */
  useEffect(() => {
    const image = selectedFiles && selectedFiles.length > 0 && last(selectedFiles);
    const photo = imageUrl || (image && URL.createObjectURL(image));
    if (photo) {
      setUserPhotoSrc(photo);
    }
  }, [imageUrl, selectedFiles]);

  /**
   * Set user photo variables
   */
  useEffect(() => {
    let newPhoto: File | undefined;
    if (croppedFiles && croppedFiles.length > 0) {
      newPhoto = last(croppedFiles);
    } else if (selectedFiles && selectedFiles.length > 0) {
      newPhoto = last(selectedFiles);
    }
    if (newPhoto) {
      setPhoto(newPhoto);
    }
  }, [croppedFiles, selectedFiles]);

  /**
   * Reset preview
   */
  useEffect(() => {
    if (selectedFiles && selectedFiles.length > 0) {
      // Reset cropped image
      setCroppedImgUrl(null);
      // Open Resize
      handleClickResize();
    }
  }, [selectedFiles]);

  useEffect(() => {
    if (isOnTitulairePharmacie) {
      handleChange('email', user.login || '');
    } else if (isOnPersonnelPharmacie) {
      if ((user.userLinks || []).length === 1) {
        if (isEmailValid(user.login || '')) {
          handleChange('email', user.login || '');
        } else {
          handleChange(
            'email',
            user.emailLink || (user.userLinks ? user.userLinks[0]?.email || '' : ''),
          );
        }
      } else if (isEmailValid(user.login || '')) {
        handleChange('email', user.login || '');
      }
    }
  }, [user.login]);

  const saveResizedImage = () => {
    setOpenResize(false);
    setSavedCroppedImage(true);
  };

  /**
   * Set image url
   */
  useEffect(() => {
    if (photo && photo.chemin) {
      setImageUrl(`${AWS_HOST}/${photo.chemin}`);
    }

    if (selectedFiles && selectedFiles.length > 0) {
      const file = last(selectedFiles);
      if (file) {
        const url = URL.createObjectURL(file);
        setImageUrl(url);
      }
    }
  }, [photo, selectedFiles]);

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    handleChange(name, value);
  };

  const isInvalidLogin = (login: string) => {
    if (isOnTitulairePharmacie) {
      return !isEmailValid(login) ? true : false;
    } else {
      return false;
    }
  };

  return (
    <Box className={classes.container}>
      <Box className={classes.content}>
        <Box className={classes.title}>{user.nom}</Box>
        <Box className={classes.imgFormContainer}>
          <Typography className={classes.inputTitle}>Photo</Typography>
          <RadioGroup
            row={true}
            aria-label="imgType"
            name="imgType"
            value={imgType}
            onChange={handleChangeRadioGroup}
            className={classes.radioGroup}
          >
            <FormControlLabel
              value="AVATAR"
              control={<Radio color="primary" />}
              label="Sélectionner un avatar"
            />
            <Typography>ou</Typography>
            <FormControlLabel
              value="IMPORT"
              control={<Radio color="primary" />}
              label="Télécharger une photo"
            />
          </RadioGroup>
          {imgType === 'AVATAR' ? (
            <ChoiceAvatarForm
              selectedAvatar={selectedAvatar}
              setSelectedAvatar={setSelectedAvatar}
              onClickAvatar={onClickAvatar}
              avatarList={avatarList}
              setAvatarList={setAvatarList}
            />
          ) : (
            <Dropzone
              contentText="Glissez et déposez ici votre photo"
              selectedFiles={selectedFiles}
              setSelectedFiles={setSelectedFiles}
              multiple={false}
              acceptFiles="image/*"
              withFileIcon={false}
              where="inUserSettings"
              withImagePreview={true}
              withImagePreviewCustomized={true}
              onClickDelete={deleteAvatar}
              fileAlreadySetUrl={croppedImgUrl || (imageUrl as any)}
              onClickResize={handleClickResize}
            />
          )}
        </Box>
        <Typography className={classes.inputTitle}>Informations personnelles</Typography>
        {label && values && (
          <CustomSelect
            label={label}
            shrink={true}
            list={values.roles}
            name="code"
            listId="code"
            index="nom"
            value={values.code}
            variant="outlined"
            onChange={onChange}
            className={classes.marginBottom}
            required={true}
          />
        )}
        {isOnPersonnelPharmacie || isOnTitulairePharmacie ? (
          <>
            <Box className={classes.marginBottom}>
              <CustomFormTextField
                error={isInvalidLogin(user.login || '')}
                name="login"
                label="Login"
                value={user.login}
                onChange={onChange}
                required={true}
              />
            </Box>
            {user.userLinks && user.userLinks.length > 1 && !isEmailValid(user.login || '') ? (
              <Box className={classes.marginBottom}>
                <CustomSelect
                  label="Email de Lien"
                  shrink={true}
                  list={user.userLinks}
                  name="email"
                  listId="email"
                  index="email"
                  value={user.email}
                  variant="outlined"
                  onChange={onChange}
                  className={classes.marginBottom}
                  required={true}
                />
              </Box>
            ) : (
              <Box className={classes.marginBottom}>
                <CustomFormTextField
                  name="email"
                  label="Email de Lien"
                  value={user.email}
                  onChange={onChange}
                  disabled={true}
                />
              </Box>
            )}
          </>
        ) : (
          <Box className={classes.marginBottom}>
            <CustomFormTextField
              name="login"
              label="Login"
              value={user.login}
              onChange={onChange}
            />
          </Box>
        )}

        <Box className={classes.birthForm}>
          <CustomFormTextField
            error={!isValidDays(values?.days || '', getMonthNumber(values?.month), values?.year)}
            className={classes.days}
            name="days"
            label="Jour"
            value={values?.days}
            onChange={onChange}
            type="number"
          />
          <CustomSelect
            value={values?.month}
            name="month"
            label="Mois"
            list={getMonths()}
            onChange={onChange}
          />
          <CustomFormTextField
            error={!isValidYears(values?.year || '')}
            className={classes.year}
            name="year"
            label="Année"
            value={values?.year}
            onChange={onChange}
            required={true}
            type="number"
          />
        </Box>
        {children}
      </Box>

      {/* Modal crop image */}
      <CustomModal
        open={openResize}
        setOpen={setOpenResize}
        title="Redimensionnement"
        onClickConfirm={saveResizedImage}
        actionButton="Enregistrer"
        closeIcon={true}
        centerBtns={true}
      >
        {userPhotoSrc ? (
          <CustomReactEasyCrop
            src={userPhotoSrc}
            withZoom={true}
            croppedImage={croppedImgUrl}
            setCroppedImage={setCroppedImgUrl}
            setCropedFiles={setCroppedFiles}
            savedCroppedImage={savedCroppedImage}
          />
        ) : (
          <Box>Aucune Image</Box>
        )}
      </CustomModal>
    </Box>
  );
};

export default withRouter(FormulaireStepOne);
