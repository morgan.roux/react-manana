import { Theme, makeStyles, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      display: 'flex',
      position: 'relative',
      width: '100%',
      '@media (max-height: 440px)': {
        height: '100%',
        padding: 25,
      },
      '@media (max-width: 1135px)': {
        padding: 25,
      },
    },
    content: {
      margin: 'auto',
      maxWidth: 800,
      width: '100%',
      // paddingTop: 50,
      // paddingLeft: 24,
      // paddingRight: 24,
      '@media (max-width: 1135px)': {
        padding: 25,
      },
    },
    marginBottom: {
      marginBottom: 25,
    },
    title: {
      marginBottom: 35,
      textAlign: 'center',
      font: 'Bold 24px/29px Montserrat',
      color: '#1D1D1D',
      opacity: 1,
    },
    marginRight: {
      marginRight: 10,
    },
    grey: {
      color: 'grey',
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
    year: { marginLeft: 10 },
    days: { marginRight: 10 },
    birthForm: { display: 'flex', marginTop: -20 },
    radioGroup: {
      display: 'flex',
      alignItems: 'center',
      '& label, & p': {
        marginRight: 50,
      },
      marginBottom: 15,
    },
    inputTitle: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.palette.secondary.main,
      marginBottom: 15,
    },
    imgFormContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
      '& > div > div': {
        height: 300,
        border: '1px dashed #DCDCDC',
        alignItems: 'flex-start',
        '& img': {
          width: 225,
          height: 225,
          borderRadius: '50%',
        },
      },
    },
    avatarListContainer: {
      width: '100%',
      height: 330,
      border: '1px solid #DCDCDC',
      borderRadius: 12,
      padding: 25,
      marginBottom: 25,
    },
    avatarListFilterContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginBottom: 25,
      border: 'none !important',
      height: 'auto !important',
      '& > div': {
        margin: 0,
      },
      '& > div:nth-child(1)': {
        marginRight: 25,
      },
      '& > div:nth-child(2)': {
        width: 238,
      },
    },
    avatarListImgContainer: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-start',
      marginBottom: 5,
      border: 'none !important',
      height: 'auto !important',
      position: 'relative',
    },
    avatarContainer: {
      width: '110px !important',
      height: 'auto !important',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      '& img': {
        width: '60px !important',
        height: '60px !important',
        borderRadius: '50% !important',
        '&:hover': {
          border: `4px solid ${theme.palette.secondary.main}`,
          cursor: 'pointer',
        },
      },
      '& span': {
        textAlign: 'center',
        fontFamily: 'Montserrat',
        fontSize: 14,
        fontWeight: '600',
        marginTop: 5,
      },
    },
    textMsg: {
      fontWeight: 600,
    },
    selectedAvatar: {
      border: `4px solid ${theme.palette.secondary.main}`,
      cursor: 'pointer',
    },
  }),
);

export default useStyles;
