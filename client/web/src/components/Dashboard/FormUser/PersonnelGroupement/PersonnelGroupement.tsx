import React, { FC, useState, useEffect } from 'react';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { Theme } from '@material-ui/core/styles';
import withStyles, { CSSProperties } from '@material-ui/core/styles/withStyles';
import { getGroupement } from '../../../../services/LocalStorage';
import { GET_PERSONNEL } from '../../../../graphql/Personnel/query';
import { PERSONNEL, PERSONNELVariables } from '../../../../graphql/Personnel/types/PERSONNEL';
import {
  CREATE_USER_GROUPEMENT,
  CREATE_USER_GROUPEMENTVariables,
} from '../../../../graphql/User/Groupement/types/CREATE_USER_GROUPEMENT';
import { DO_CREATE_USER_GROUPEMENT } from '../../../../graphql/User/Groupement/mutation';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { Formulaire } from '../Formulaire';
import { getMonthNumber, getMonthName, isInvalidArray } from '../../../../utils/Helpers';
import { formatFilenameWithoutDate } from '../../../../utils/filenameFormater';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../graphql/S3';
import { uploadToS3 } from '../../../../services/S3';
import { TypeFichier, FichierInput } from '../../../../types/graphql-global-types';
import Backdrop from '../../../Common/Backdrop';
import { DO_SEARCH_ROLES } from '../../../../graphql/Role';
import { SEARCH_ROLESVariables, SEARCH_ROLES } from '../../../../graphql/Role/types/SEARCH_ROLES';

const styles = (theme: Theme): Record<string, CSSProperties> => ({});

interface PROPS {
  id: string;
  userId: string;
  goTo: () => any;
}

// const initRoles = [
//   {
//     code: ADMINISTRATEUR_GROUPEMENT,
//     nom: 'Admin groupement',
//   },
//   {
//     code: COLLABORATEUR_COMMERCIAL,
//     nom: 'Collaborateur commercial',
//   },
//   {
//     code: COLLABORATEUR_NON_COMMERCIAL,
//     nom: 'Collaborateur non commercial',
//   },
//   {
//     code: GROUPEMENT_AUTRE,
//     nom: 'Autre collaborateur',
//   },
// ];

const PersonnelGroupement: FC<PROPS> = ({ id, userId, goTo }) => {
  const [nom, setNom] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [code, setCode] = useState<string>('');
  const [roles, setRoles] = useState<any[]>([]);
  const [login, setLogin] = useState<string>('');

  /** birth date */
  const [days, setDays] = useState<string>('');
  const [month, setMonth] = useState<string>('');
  const [year, setYear] = useState<string>('');

  /** Photo */
  const [photo, setPhoto] = useState<File | any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const [permissionsAccess, setPermissionsAccess] = useState<any[]>([]);
  const permissionsAccessCodes = permissionsAccess.map((i: any) => i && i.code);

  const client = useApolloClient();
  const groupement = getGroupement();

  /**
   * Search roles
   */
  const { data: dataRoles, loading: loadingRoles } = useQuery<SEARCH_ROLES, SEARCH_ROLESVariables>(
    DO_SEARCH_ROLES,
    {
      variables: {
        type: ['role'],
        filterBy: [{ term: { typeRole: 'GROUPEMENT' } }],
        sortBy: [{ nom: { order: 'asc' } }],
      },
    },
  );

  /**
   * Set init roles
   */
  useEffect(() => {
    if (
      dataRoles &&
      dataRoles.search &&
      dataRoles.search.data &&
      dataRoles.search.data.length > 0
    ) {
      setRoles(dataRoles.search.data);
    }
  }, [dataRoles]);

  const { loading: getLoading } = useQuery<PERSONNEL, PERSONNELVariables>(GET_PERSONNEL, {
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: data => {
      if (data && data.personnel) {
        const nom = data.personnel.nom ? data.personnel.nom : '';
        const prenom = data.personnel.prenom ? data.personnel.prenom : '';
        setNom(`${nom} ${prenom}`);
        const defaultEmail =  userId && data.personnel.user && data.personnel.user.email? data.personnel.user.email :'';

        /* data && data.personnel && data.personnel.mailProf
            ? data.personnel.mailProf.trim()
            : data && data.personnel && data.personnel.mailPerso
            ? data.personnel.mailPerso.trim()
            : ''; */
          setEmail(defaultEmail);
          const defaultLogin =
          userId && data.personnel.user && data.personnel.user.login
            ? data.personnel.user.login
            : defaultEmail;
        setLogin(defaultLogin);

        


        if (userId && data.personnel.role && data.personnel.role.code) {
          setCode(data.personnel.role.code);
        }
        // setRoles(initRoles);
        if (userId && data.personnel.user && data.personnel.user.anneeNaissance) {
          setYear(data.personnel.user.anneeNaissance.toString());
        }
        if (userId && data.personnel.user && data.personnel.user.moisNaissance) {
          setMonth(getMonthName(data.personnel.user.moisNaissance));
        }
        if (userId && data.personnel.user && data.personnel.user.jourNaissance) {
          setDays(data.personnel.user.jourNaissance.toString());
        }

        /**
         * Set photo on edit
         */
        const newPhoto =
          data.personnel &&
          data.personnel.user &&
          data.personnel.user.userPhoto &&
          data.personnel.user.userPhoto.fichier;
        if (newPhoto) {
          setPhoto(newPhoto);
        }

        /**
         * Set selected permissionsAccess
         */
        const codeTraitements =
          data.personnel && data.personnel.user && data.personnel.user.codeTraitements;
        if (codeTraitements && codeTraitements.length > 0) {
          const newUserTraitements: any[] = codeTraitements.map(i => {
            if (i) return { code: i };
          });
          setPermissionsAccess(newUserTraitements);
        }
      }
      if (data && data.personnel && data.personnel.user && data.personnel.user.login) {
        setLogin(data.personnel.user.login);
      }
    },
  });

  /**
   * Mutation createPutPresignedUrl
   */
  const [doCreatePutPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        photo &&
        photo.size
      ) {
        const presignedPhoto = data.createPutPresignedUrls[0];
        if (presignedPhoto && presignedPhoto.filePath && presignedPhoto.presignedUrl) {
          setLoading(true);
          await uploadToS3(photo, presignedPhoto.presignedUrl)
            .then(result => {
              if (result && result.status === 200) {
                createUserGroupement({
                  variables: {
                    idGroupement: groupement.id,
                    id,
                    email,
                    login,
                    role: code,
                    userId,
                    day: Number(days),
                    month: getMonthNumber(month),
                    year: Number(year),
                    userPhoto: {
                      chemin: presignedPhoto.filePath,
                      nomOriginal: photo.name,
                      type: TypeFichier.PHOTO,
                    },
                    codeTraitements: !isInvalidArray(permissionsAccessCodes)
                      ? permissionsAccessCodes
                      : null,
                  },
                });
              }
            })
            .catch(error => {
              console.error(error);
              setLoading(false);
              displaySnackBar(client, {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              });
            });
        }
      }
    },
    onError: errors => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      });
    },
  });

  const [createUserGroupement, { loading: createUserLoading }] = useMutation<
    CREATE_USER_GROUPEMENT,
    CREATE_USER_GROUPEMENTVariables
  >(DO_CREATE_USER_GROUPEMENT, {
    onCompleted: data => {
      if (data && data.createUserGroupement) {
        setLoading(false);
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Mail de définition de mot de passe envoyé à l'utilisateur`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        goTo();
      }
    },
    onError: errors => {
      setLoading(false);
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map(error => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'MAIL_NOT_SENT':
              errorMessage = "L'email n'est pas envoyé à l'utilisatuer";
              break;
            case 'LOGIN_ALREADY_EXIST':
              errorMessage = 'Ce login est déjà utilisé par un autre utilisateur';
              break;
          }
        }
      });

      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errorMessage,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleChange = (name: string, value: string) => {
    switch (name) {
      case 'email':
        setEmail(value);
        break;
      case 'days':
        setDays(value);
        break;
      case 'month':
        setMonth(value);
        break;
      case 'year':
        setYear(value);
        break;
      case 'login':
        setLogin(value);
        break;
      default:
        setCode(value);
    }
  };

  const submit = () => {
    setLoading(true);
    if (photo && photo.size) {
      // Presigned file
      const filePathsTab: string[] = [];
      filePathsTab.push(
        `users/photos/${groupement && groupement.id}/${formatFilenameWithoutDate(photo)}`,
      );
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
      return;
    }

    let userPhoto: FichierInput | null = null;
    if (photo && photo.id) {
      userPhoto = {
        chemin: photo.chemin,
        nomOriginal: photo.nomOriginal,
        type: TypeFichier.AVATAR,
        idAvatar: photo && photo.idAvatar,
      };
    }
    createUserGroupement({
      variables: {
        idGroupement: groupement.id,
        id,
        email,
        login,
        role: code,
        userId,
        day: Number(days),
        month: getMonthNumber(month),
        year: Number(year),
        userPhoto,
        codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
      },
    });
  };

  return (
    <>
      {(createUserLoading || presignedLoading || loading || loadingRoles) && <Backdrop />}
      <Formulaire
        user={{ nom, email, login }}
        values={{ code, roles, days, month, year }}
        label="Rôle"
        userId={userId}
        doCreateOrUpdate={submit}
        handleChange={handleChange}
        goTo={goTo}
        photo={photo}
        setPhoto={setPhoto}
        permissionsAccess={permissionsAccess}
        setPermissionsAccess={setPermissionsAccess}
        loading={getLoading}
      />
    </>
  );
};

export default withStyles(styles)(PersonnelGroupement);
