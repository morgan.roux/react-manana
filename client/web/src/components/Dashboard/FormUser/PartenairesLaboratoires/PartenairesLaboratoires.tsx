import React, { FC, useState } from 'react';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { Theme } from '@material-ui/core/styles';
import withStyles, { CSSProperties } from '@material-ui/core/styles/withStyles';
import { getGroupement } from '../../../../services/LocalStorage';
import { Formulaire } from '../Formulaire';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import {
  LABORATOIRE,
  LABORATOIREVariables,
} from '../../../../graphql/Laboratoire/types/LABORATOIRE';
import { GET_LABORATOIRE } from '../../../../graphql/Laboratoire/query';
import {
  CREATE_USER_PARTENAIRE_LABORATOIRE,
  CREATE_USER_PARTENAIRE_LABORATOIREVariables,
} from '../../../../graphql/User/PatenaireLaboratoire/types/CREATE_USER_PARTENAIRE_LABORATOIRE';
import { DO_CREATE_USER_PARTENAIRE_LABORATOIRE } from '../../../../graphql/User/PatenaireLaboratoire/mutation';
import { uploadToS3 } from '../../../../services/S3';
import { TypeFichier, FichierInput } from '../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../utils/filenameFormater';
import Backdrop from '../../../Common/Backdrop';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../graphql/S3';
import { isInvalidArray } from '../../../../utils/Helpers';
import { PARTENAIRE_LABORATOIRE } from '../../../../Constant/roles';

const styles = (theme: Theme): Record<string, CSSProperties> => ({});

interface PROPS {
  id: string;
  userId: string;
  goTo: () => any;
}

const PartenairesLaboratoires: FC<PROPS> = ({ id, userId, goTo }) => {
  const [nom, setNom] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [login, setLogin] = useState<string>('');

  const client = useApolloClient();
  const groupement = getGroupement();

  /** Photo */
  const [photo, setPhoto] = useState<File | any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const [permissionsAccess, setPermissionsAccess] = useState<any[]>([]);
  const permissionsAccessCodes = permissionsAccess.map((i: any) => i && i.code);

  const { loading: getLoading } = useQuery<LABORATOIRE, LABORATOIREVariables>(GET_LABORATOIRE, {
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: (data) => {
      if (data && data.laboratoire && data.laboratoire.nomLabo) {
        setNom(data.laboratoire.nomLabo);

        const defaultEmail =
          userId && data.laboratoire.user && data.laboratoire.user.email
            ? data.laboratoire.user.email
            : '';
        setEmail(defaultEmail);

        const defaultLogin =
          userId && data.laboratoire.user && data.laboratoire.user.login
            ? data.laboratoire.user.login
            : defaultEmail;

        setLogin(defaultLogin);

        /**
         * Set photo on edit
         */
        const newPhoto =
          data.laboratoire &&
          data.laboratoire.user &&
          data.laboratoire.user.userPhoto &&
          data.laboratoire.user.userPhoto.fichier;
        if (newPhoto) {
          setPhoto(newPhoto);
        }

        /**
         * Set selected permissionsAccess
         */
        const codeTraitements =
          data.laboratoire && data.laboratoire.user && data.laboratoire.user.codeTraitements;
        if (codeTraitements && codeTraitements.length > 0) {
          const newUserTraitements: any[] = codeTraitements.map((i) => {
            if (i) return { code: i };
          });
          setPermissionsAccess(newUserTraitements);
        }
      }
      if (data && data.laboratoire && data.laboratoire.user && data.laboratoire.user.login) {
        setLogin(data.laboratoire.user.login);
      }
    },
  });

  /**
   * Mutation createPutPresignedUrl
   */
  const [doCreatePutPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async (data) => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        photo &&
        photo.size
      ) {
        const presignedPhoto = data.createPutPresignedUrls[0];
        if (presignedPhoto && presignedPhoto.filePath && presignedPhoto.presignedUrl) {
          setLoading(true);
          await uploadToS3(photo, presignedPhoto.presignedUrl)
            .then((result) => {
              if (result && result.status === 200) {
                createUserPartenaireLaboratoire({
                  variables: {
                    idGroupement: groupement.id,
                    id,
                    email,
                    login,
                    userId,
                    userPhoto: {
                      chemin: presignedPhoto.filePath,
                      nomOriginal: photo.name,
                      type: TypeFichier.PHOTO,
                    },
                    codeTraitements: !isInvalidArray(permissionsAccessCodes)
                      ? permissionsAccessCodes
                      : null,
                  },
                });
              }
            })
            .catch((error) => {
              console.error(error);
              setLoading(false);
              displaySnackBar(client, {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              });
            });
        }
      }
    },
    onError: (errors) => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      });
    },
  });

  const [createUserPartenaireLaboratoire, { loading: createUserLoading }] = useMutation<
    CREATE_USER_PARTENAIRE_LABORATOIRE,
    CREATE_USER_PARTENAIRE_LABORATOIREVariables
  >(DO_CREATE_USER_PARTENAIRE_LABORATOIRE, {
    onCompleted: (data) => {
      if (data && data.createUserPartenaireLaboratoire) {
        setLoading(false);
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Mail de définition de mot de passe envoyé à l'utilisateur`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        goTo();
      }
    },
    onError: (errors) => {
      setLoading(false);
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map((error) => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'MAIL_NOT_SENT':
              errorMessage = "L'email n'est pas envoyé à l'utilisatuer";
              break;
            case 'LOGIN_ALREADY_EXIST':
              errorMessage = 'Ce login est déjà utilisé par un autre utilisateur';
              break;
          }
        }
      });

      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errorMessage,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleChange = (name: string, value: string) => {
    if (name === 'email') setEmail(value);
    if (name === 'login') setLogin(value);
  };

  const submit = () => {
    setLoading(true);
    if (photo && photo.size) {
      // Presigned file
      const filePathsTab: string[] = [];
      filePathsTab.push(
        `users/photos/${groupement && groupement.id}/${formatFilenameWithoutDate(photo)}`,
      );
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
      return;
    }

    let userPhoto: FichierInput | null = null;
    if (photo && photo.id) {
      userPhoto = {
        chemin: photo.chemin,
        nomOriginal: photo.nomOriginal,
        type: TypeFichier.AVATAR,
        idAvatar: photo && photo.idAvatar,
      };
    }
    createUserPartenaireLaboratoire({
      variables: {
        idGroupement: groupement.id,
        id,
        email,
        login,
        userId,
        userPhoto,
        codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
      },
    });
  };

  return (
    <>
      {(createUserLoading || presignedLoading || loading) && <Backdrop />}
      <Formulaire
        user={{ nom, email, login }}
        userId={userId}
        userRole={PARTENAIRE_LABORATOIRE}
        doCreateOrUpdate={submit}
        handleChange={handleChange}
        goTo={goTo}
        photo={photo}
        setPhoto={setPhoto}
        permissionsAccess={permissionsAccess}
        setPermissionsAccess={setPermissionsAccess}
        loading={getLoading}
      />
    </>
  );
};

export default withStyles(styles)(PartenairesLaboratoires);
