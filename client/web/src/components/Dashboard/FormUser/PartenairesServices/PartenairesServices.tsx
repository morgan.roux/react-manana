import React, { FC, useState } from 'react';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { Theme } from '@material-ui/core/styles';
import withStyles, { CSSProperties } from '@material-ui/core/styles/withStyles';
import { getGroupement, getPharmacie } from '../../../../services/LocalStorage';
import { Formulaire } from '../Formulaire';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { PARTENAIRE, PARTENAIREVariables } from '../../../../graphql/Partenaire/types/PARTENAIRE';
import { GET_PARTENAIRE } from '../../../../graphql/Partenaire/query';
import {
  CREATE_USER_PARTENAIRE_SERVICE,
  CREATE_USER_PARTENAIRE_SERVICEVariables,
} from '../../../../graphql/User/PartenaireService/types/CREATE_USER_PARTENAIRE_SERVICE';
import { DO_CREATE_USER_PARTENAIRE_SERVICE } from '../../../../graphql/User/PartenaireService/mutation';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../graphql/S3';
import { uploadToS3 } from '../../../../services/S3';
import { TypeFichier, FichierInput } from '../../../../types/graphql-global-types';
import { formatFilenameWithoutDate } from '../../../../utils/filenameFormater';
import Backdrop from '../../../Common/Backdrop';
import { getMonthNumber, isInvalidArray } from '../../../../utils/Helpers';
import { PARTENAIRE_SERVICE } from '../../../../Constant/roles';
import { IRoles } from '../FormUser';

const styles = (theme: Theme): Record<string, CSSProperties> => ({});

interface PROPS {
  id: string;
  userId: string;
  goTo: () => any;
}

const PartenairesServices: FC<PROPS> = ({ id, userId, goTo }) => {
  const [nom, setNom] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [login, setLogin] = useState<string>('');
  const [days, setDays] = useState<string>('');
  const [month, setMonth] = useState<string>('');
  const [year, setYear] = useState<string>('');
  const [code, setCode] = useState<string>('');
  const [roles, setRoles] = useState<IRoles[]>([{ code: '', nom: '' }]);

  /** Photo */
  const [photo, setPhoto] = useState<File | any>(null);
  const [loading, setLoading] = useState<boolean>(false);

  const [permissionsAccess, setPermissionsAccess] = useState<any[]>([]);
  const permissionsAccessCodes = permissionsAccess.map((i: any) => i && i.code);

  const client = useApolloClient();
  const groupement = getGroupement();
  const pharmacie = getPharmacie();

  const { loading: getLoading } = useQuery<PARTENAIRE, PARTENAIREVariables>(GET_PARTENAIRE, {
    fetchPolicy: 'no-cache',
    variables: { id },
    onCompleted: data => {
      if (data && data.partenaire) {
        const nom = data.partenaire.nom ? data.partenaire.nom : '';
        setNom(nom);

        const defaultEmail =
          userId && data.partenaire.user && data.partenaire.user.email
            ? data.partenaire.user.email
            : '';
        setEmail(defaultEmail);

        const defaultLogin =
          userId && data.partenaire.user && data.partenaire.user.login
            ? data.partenaire.user.login
            : defaultEmail;

        setLogin(defaultLogin);

        /**
         * Set photo on edit
         */
        const newPhoto =
          data.partenaire &&
          data.partenaire.user &&
          data.partenaire.user.userPhoto &&
          data.partenaire.user.userPhoto.fichier;
        if (newPhoto) {
          setPhoto(newPhoto);
        }

        /**
         * Set selected permissionsAccess
         */
        const codeTraitements =
          data.partenaire && data.partenaire.user && data.partenaire.user.codeTraitements;
        if (codeTraitements && codeTraitements.length > 0) {
          const newUserTraitements: any[] = codeTraitements.map(i => {
            if (i) return { code: i };
          });
          setPermissionsAccess(newUserTraitements);
        }
      }
      if (data && data.partenaire && data.partenaire.user && data.partenaire.user.login) {
        setLogin(data.partenaire.user.login);
      }
    },
  });

  /**
   * Mutation createPutPresignedUrl
   */
  const [doCreatePutPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        photo &&
        photo.size
      ) {
        const presignedPhoto = data.createPutPresignedUrls[0];
        if (presignedPhoto && presignedPhoto.filePath && presignedPhoto.presignedUrl) {
          setLoading(true);
          await uploadToS3(photo, presignedPhoto.presignedUrl)
            .then(result => {
              if (result && result.status === 200) {
                createUserPartenaireService({
                  variables: {
                    idGroupement: groupement.id,
                    id,
                    email,
                    login,
                    userPhoto: {
                      chemin: presignedPhoto.filePath,
                      nomOriginal: photo.name,
                      type: TypeFichier.PHOTO,
                    },
                    codeTraitements: !isInvalidArray(permissionsAccessCodes)
                      ? permissionsAccessCodes
                      : null,
                    idPharmacie: pharmacie?.id,
                  },
                });
              }
            })
            .catch(error => {
              console.error(error);
              setLoading(false);
              displaySnackBar(client, {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              });
            });
        }
      }
    },
    onError: errors => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      });
    },
  });

  const [createUserPartenaireService, { loading: createUserLoading }] = useMutation<
    CREATE_USER_PARTENAIRE_SERVICE,
    CREATE_USER_PARTENAIRE_SERVICEVariables
  >(DO_CREATE_USER_PARTENAIRE_SERVICE, {
    onCompleted: data => {
      if (data && data.createUserPartenaireService) {
        setLoading(false);
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Mail de définition de mot de passe envoyé à l'utilisateur`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        goTo();
      }
    },
    onError: errors => {
      setLoading(false);
      let errorMessage: string = 'Erreur du serveur';
      errors.graphQLErrors.map(error => {
        if (error.extensions) {
          switch (error.extensions.code) {
            case 'MAIL_NOT_SENT':
              errorMessage = "L'email n'est pas envoyé à l'utilisatuer";
              break;
            case 'LOGIN_ALREADY_EXIST':
              errorMessage = 'Ce login est déjà utilisé par un autre utilisateur';
              break;
          }
        }
      });

      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: errorMessage,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleChange = (name: string, value: string) => {
    if (name === 'email') setEmail(value);
    else if (name === 'login') setLogin(value);
    else if (name === 'days') setDays(value);
    else if (name === 'month') setMonth(value);
    else if (name === 'year') setYear(value);
    else setCode(value);
  };

  const submit = () => {
    setLoading(true);
    if (photo && photo.size) {
      // Presigned file
      const filePathsTab: string[] = [];
      filePathsTab.push(
        `users/photos/${groupement && groupement.id}/${formatFilenameWithoutDate(photo)}`,
      );
      doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
      return;
    }

    let userPhoto: FichierInput | null = null;
    if (photo && photo.id) {
      userPhoto = {
        chemin: photo.chemin,
        nomOriginal: photo.nomOriginal,
        type: TypeFichier.AVATAR,
        idAvatar: photo && photo.idAvatar,
      };
    }
    createUserPartenaireService({
      variables: {
        idGroupement: groupement.id,
        id,
        email,
        login,
        userPhoto,
        day: Number(days),
        month: getMonthNumber(month),
        year: Number(year),
        codeTraitements: !isInvalidArray(permissionsAccessCodes) ? permissionsAccessCodes : null,
        idPharmacie: pharmacie?.id,
      },
    });
  };

  return (
    <>
      {(createUserLoading || presignedLoading || loading) && <Backdrop />}
      <Formulaire
        user={{ nom, email, login }}
        userId={userId}
        //values={{ code, roles, year, month, days }}
        userRole={PARTENAIRE_SERVICE}
        doCreateOrUpdate={submit}
        handleChange={handleChange}
        goTo={goTo}
        photo={photo}
        setPhoto={setPhoto}
        permissionsAccess={permissionsAccess}
        setPermissionsAccess={setPermissionsAccess}
        loading={getLoading}
      />
    </>
  );
};

export default withStyles(styles)(PartenairesServices);
