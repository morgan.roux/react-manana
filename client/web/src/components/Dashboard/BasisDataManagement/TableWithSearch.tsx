import React, { FC, useState, useEffect } from 'react';
import { useQuery, useApolloClient } from '@apollo/react-hooks';
import { SEARCH } from '../../../graphql/search/query';
import { SEARCH as SEARCH_Interface, SEARCHVariables } from '../../../graphql/search/types/SEARCH';

import TableDataManagement from './TableDataManagement';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import { ElementConfigItem } from './services/basis-service';

interface BasisDataManagementWithSearchProps {
  search: string;
  elementConfig: ElementConfigItem;
  elementId: any;
  onCreateNewElement: boolean;
  onForm: boolean;
  display: boolean;
  onElementToEditLoaded: (elementToEdit: any) => void;
  onDeleteBtnClicked: (id: string, modelName: string) => void;
}

const BasisDataManagementWithSearch: FC<BasisDataManagementWithSearchProps> = ({
  search,
  elementConfig,
  elementId,
  onCreateNewElement,
  onForm,
  display,
  onElementToEditLoaded,
  onDeleteBtnClicked,
}) => {
  const client = useApolloClient();

  const [take, setTake] = useState<number>(display ? 10 : 0);
  const [skip, setSkip] = useState<number>(0);

  const { data } = useQuery<SEARCH_Interface, SEARCHVariables>(SEARCH, {
    fetchPolicy: 'network-only',
    variables: {
      type: [elementConfig.config.index],
      query: search,
      take,
      skip,
    },
    onError: error => {
      if (display) {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          });
        });
      }
    },
  });

  const resultData = (data && data.search && data.search.data) || [];
  const total = (data && data.search && data.search.total) || 0;

  // TODO : Dont call search if elementId==='new'
  const fetchElementByIdResult = useQuery<SEARCH_Interface, SEARCHVariables>(SEARCH, {
    fetchPolicy: 'network-only',
    variables: {
      type: [elementConfig.config.index],
      query: elementId
        ? {
            query: {
              terms: {
                _id: [elementId],
              },
            },
          }
        : undefined,
      skip: 0,
      take: 1,
    },
    onError: error => {
      if (display && onForm && !onCreateNewElement) {
        error.graphQLErrors.map(err => {
          displaySnackBar(client, {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          });
        });
      }
    },
  });

  useEffect(() => {
    if (!onCreateNewElement) {
      const elementToEdit =
        fetchElementByIdResult.data &&
        fetchElementByIdResult.data.search &&
        fetchElementByIdResult.data.search.data &&
        fetchElementByIdResult.data.search.data[0];
      if (elementToEdit) {
        onElementToEditLoaded(elementToEdit);
      }
    }
  }, [elementId, fetchElementByIdResult]);

  return display ? (
    <TableDataManagement
      elementConfig={elementConfig}
      data={resultData}
      {...{ take, skip, setTake, setSkip, total }}
      onDeleteBtnCliked={onDeleteBtnClicked}
    />
  ) : null;
};

export default BasisDataManagementWithSearch;
