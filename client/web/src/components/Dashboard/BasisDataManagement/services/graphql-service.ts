import axios from 'axios';
import { API_URL } from './../../../../config';
import { ElementConfigItem, getHeaders } from './basis-service';

const fetchGraphQl = async ({ variables, query }: any): Promise<any> => {
  return axios({
    headers: getHeaders(),
    url: `${API_URL}/graphql`,
    method: 'post',
    data: {
      variables,
      query,
    },
  }).then(({ data }) => {
    return data.data;
  });
};

export const fetchAllElements = async (elementConfig: ElementConfigItem): Promise<any[]> => {
  if (!elementConfig.config || !elementConfig.config.query) {
    return [];
  }

  return fetchGraphQl({
    query: `
        query ${elementConfig.config.query} {
            ${elementConfig.config.query} {
               id ${Object.keys(elementConfig.fieldLabelsMapping).join(' ')}
            }
          }
        `,
  }).then((data) => data[elementConfig.config.query]);
};
