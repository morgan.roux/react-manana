import React, { FC } from 'react';
import ArrowBackspaceIcon from '@material-ui/icons/ArrowBack';
import CustomButton from '../../../Common/CustomButton';
import { IconButton, Box } from '@material-ui/core';
import { useStyles } from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';

interface FormHeaderProps {
  onSaveBtnClicked(): void;
  onBackBtnClicked(): void;
  title: string;
  saving: boolean;
}

const FormHeader: FC<RouteComponentProps & FormHeaderProps> = ({
  history: { push },
  onSaveBtnClicked,
  onBackBtnClicked,
  title,
  saving,
}) => {
  const classes = useStyles({});

  return (
    <Box className={classes.infoBar}>
      <Box>
        <IconButton className={classes.backIcon} onClick={onBackBtnClicked} disabled={saving}>
          <ArrowBackspaceIcon />
        </IconButton>
      </Box>
      <Box className={classes.title}>{title}</Box>
      <Box>
        <CustomButton
          className={classes.resetButton}
          color="default"
          onClick={onBackBtnClicked}
          disabled={saving}
        >
          annuler
        </CustomButton>
        <CustomButton
          className={classes.saveButton}
          color="secondary"
          onClick={onSaveBtnClicked}
          disabled={saving}
        >
          enregistrer
        </CustomButton>
      </Box>
    </Box>
  );
};

export default withRouter(FormHeader);
