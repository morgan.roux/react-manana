import React, { FC } from 'react';
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Box,
  IconButton,
} from '@material-ui/core';
import { Add } from '@material-ui/icons';

import { useStyles } from '../style';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import StyledTableCell from './StyledTableCell';
import StyledTableBodyCell from './StyledTableBodyCell';
import { capitalizeFirstLetter } from '../../../../utils/capitalizeFirstLetter';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

import CommonPagination from '../../../Common/CommonPagination';
import CustomButton from '../../../Common/CustomButton';
import { CustomModal } from '../../../Common/CustomModal';

import { ElementConfigItem } from './../services/basis-service';

interface TableProps {
  elementConfig: ElementConfigItem;
  data: any[]; // TODO : Set type
  take?: number;
  setTake?(take: number): void;
  skip?: number;
  setSkip?(skip: number): void;
  total?: number;
  onDeleteBtnCliked: (id: string, modelName: string) => void;
}

const TableDataManagement: FC<TableProps & RouteComponentProps> = (props) => {
  const {
    elementConfig,
    data,
    history: { push },
    take=12,
    skip=0,
    setTake,
    setSkip,
    total=0,
    onDeleteBtnCliked,
  } = props;
  const classes = useStyles({});

  const [elementToDelete, setElementToDelete] = React.useState<
    { modelName: string; id: any } | undefined
  >();

  const handleClickConfirm = () => {
    if (elementToDelete) {
      onDeleteBtnCliked(elementToDelete.id, elementToDelete.modelName);
    }

    setElementToDelete(undefined);
  };

  return (
    <Box className={classes.container}>
      {elementToDelete && (
        <CustomModal
          open={true}
          setOpen={() => setElementToDelete(undefined)}
          title="Suppression"
          onClickConfirm={handleClickConfirm}
          actionButton="Supprimer"
          closeIcon={true}
          centerBtns={true}
        >
          <div>Êtes-vous sûr de supprimer cet enregistrement ? </div>
        </CustomModal>
      )}

      <CustomButton
        style={{ marginTop: 10 }}
        color="secondary"
        startIcon={<Add />}
        onClick={() => push(`/db/basis/${elementConfig.modelName}/new`)}
      >
        Ajouter
      </CustomButton>
      <TableContainer className={classes.tableContainer} component={Box}>
        <Table stickyHeader className={classes.table} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              {Object.keys(elementConfig.fieldLabelsMapping).map((fieldName) => {
                return (
                  <StyledTableCell align="left">
                    {capitalizeFirstLetter(elementConfig.fieldLabelsMapping[fieldName])}
                  </StyledTableCell>
                );
              })}

              <StyledTableCell align="left"></StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data
              ? data.map((item: any, index) => {
                  return (
                    <TableRow key={index}>
                      {Object.keys(elementConfig.fieldLabelsMapping).map((fieldName) => {
                        return (
                          <StyledTableBodyCell align="left">{item[fieldName]}</StyledTableBodyCell>
                        );
                      })}
                      <TableCell align="right">
                        <IconButton
                          color="secondary"
                          onClick={() => push(`/db/basis/${elementConfig.modelName}/${item.id}`)}
                        >
                          <EditIcon />
                        </IconButton>

                        <IconButton
                          color="secondary"
                          onClick={() =>
                            setElementToDelete({ id: item.id, modelName: elementConfig.modelName })
                          }
                        >
                          <DeleteIcon />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  );
                })
              : null}
          </TableBody>
        </Table>
      </TableContainer>

      {setTake && setSkip ? (
        <CommonPagination
          {...{ take, skip, total, setTake, setSkip }}
          className={classes.pagination}
        />
      ) : null}
    </Box>
  );
};

export default withRouter(TableDataManagement);
