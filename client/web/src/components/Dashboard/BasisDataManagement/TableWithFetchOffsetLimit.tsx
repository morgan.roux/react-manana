import React, { FC, useState, useEffect } from 'react';

import TableDataManagement from './TableDataManagement';
import { ElementConfigItem } from './services/basis-service';
import { countElements, searchElements, getOneElement } from './services/basis-service';

interface BasisDataManagementWithSearchProps {
  elementConfig: ElementConfigItem;
  elementId: any;
  onCreateNewElement: boolean;
  display: boolean;
  onElementToEditLoaded: (elementToEdit: any) => void;
  onDeleteBtnClicked: (id: string, modelName: string) => void;
}

const BasisDataManagementWithSearch: FC<BasisDataManagementWithSearchProps> = ({
  elementConfig,
  elementId,
  onCreateNewElement,
  display,
  onElementToEditLoaded,
  onDeleteBtnClicked,
}) => {
  const [take, setTake] = useState<number>(display ? 10 : 0);
  const [skip, setSkip] = useState<number>(0);

  const [total, setTotal] = useState<number>(0);
  const [data, setData] = useState<any[]>([]);

  useEffect(() => {
    if (elementConfig && elementConfig.type==='FETCH_OFFSET_LIMIT') {
      countElements(elementConfig).then(setTotal);
    }
  }, [elementConfig]);

  useEffect(() => {
    if (elementConfig && elementConfig.type==='FETCH_OFFSET_LIMIT') {
      searchElements(elementConfig, skip, take)
      .then(({nodes})=>setData(nodes));
    }
  }, [take, skip]);

  useEffect(() => {
    if (!onCreateNewElement && elementConfig.type==='FETCH_OFFSET_LIMIT') {
      getOneElement(elementId, elementConfig).then((elementToEdit) => {
        if (elementToEdit) {
          onElementToEditLoaded(elementToEdit);
        }
      });
    }
  }, [elementId]);

  return display ? (
    <TableDataManagement
      elementConfig={elementConfig}
      data={data}
      {...{ take, skip, setTake, setSkip, total }}
      onDeleteBtnCliked={onDeleteBtnClicked}
    />
  ) : null;
};

export default BasisDataManagementWithSearch;
