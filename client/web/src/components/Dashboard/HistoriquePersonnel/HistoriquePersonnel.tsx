import React, { FC, useEffect, useState, ChangeEvent, useRef, useCallback } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import moment from 'moment';
import { CustomFullScreenModal } from '../../Common/CustomModal';
import useStyles from './styles';
import {
  Box,
  Divider,
  Typography,
  Avatar,
  FormControl,
  OutlinedInput,
  InputAdornment,
} from '@material-ui/core';
import CustomSelect from '../../Common/CustomSelect';
import { SEARCH as SEARCH_QUERY } from '../../../graphql/search/query';
import { useLazyQuery, useApolloClient } from '@apollo/react-hooks';
import { SEARCH, SEARCHVariables } from '../../../graphql/search/types/SEARCH';
import { LoaderSmall } from '../Content/Loader';
import { groupBy, values, debounce, isEqual, capitalize } from 'lodash';
import SearchIcon from '@material-ui/icons/Search';
import { CustomDatePicker } from '../../Common/CustomDateTimePicker';
import { GET_USER } from '../../../graphql/User/query';
import { USER, USERVariables } from '../../../graphql/User/types/USER';
import { stringToAvatar } from '../../../utils/Helpers';
import SnackVariableInterface from '../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../utils/snackBarUtils';

interface HistoriquePersonnelProps {
  open: boolean;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  userId: string;
}

const HistoriquePersonnel: FC<HistoriquePersonnelProps & RouteComponentProps> = ({
  open,
  setOpen,
  userId,
}) => {
  const classes = useStyles({});
  const [filter, setFilter] = useState<string>('');
  const [must, setMust] = useState<any[]>([]);
  const [searchText, setSearchText] = useState<string>('');
  const defaultSortBy = [{ dateCreation: { order: 'desc' } }];
  const defaultSearchVariables = {
    take: 25,
    query: {},
    filterBy: [],
    sortBy: defaultSortBy,
  };
  const [searchVariables, setSerachVariables] = useState<any>(defaultSearchVariables);

  const { take, query, sortBy, filterBy } = searchVariables;
  const [groupedData, setGroupedData] = useState<any[]>([]);
  const [startDate, setStartDate] = useState<any>(null);
  const [endDate, setEndDate] = useState<any>(null);
  const client = useApolloClient();

  const [search, { loading, error, data }] = useLazyQuery<SEARCH, SEARCHVariables>(SEARCH_QUERY, {
    fetchPolicy: 'network-only',
    variables: {
      type: ['activiteuser'],
      skip: 0,
      take,
      query,
      sortBy,
      filterBy,
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      });
    },
  });

  const [getUser, getUserResult] = useLazyQuery<USER, USERVariables>(GET_USER, {
    fetchPolicy: 'network-only',
  });
  const user = getUserResult.data && getUserResult.data.user;

  const debouncedSearch = useRef(
    debounce((text: string) => {
      if (text.length > 0) search();
    }, 1000),
  );

  /**
   * Grouped data by date
   */
  useEffect(() => {
    if (data && data.search && data.search.data && data.search.data.length > 0) {
      const array: any = data.search.data.filter((item: any) => item && item.dateCreation !== null);
      const dateCreation = (item: any) => moment(item.dateCreation).format('YYYY-MM-DD');
      let newData: any = groupBy(array, dateCreation);
      if (newData) {
        newData = values(newData);
        setGroupedData(newData);
      }
    } else {
      setGroupedData([]);
    }
  }, [data]);

  // Lanch search on open modal
  useEffect(() => {
    if (open && userId) {
      getUser({ variables: { id: userId } });
      search();
    }
    // Reset to default on close
    if (!open && searchVariables.query && searchVariables.query.length > 0) {
      setSerachVariables(defaultSearchVariables);
    }
  }, [userId, open]);

  /**
   * Update query if must change
   */
  useEffect(() => {
    if (must && must.length > 0) {
      const newQuery = { query: { bool: { must: [...must] } } };
      setSerachVariables(prevState => ({ ...prevState, query: newQuery }));
    } else {
      setSerachVariables(prevState => ({ ...prevState, query: {} }));
    }
  }, [must]);

  // Update filterBy
  useEffect(() => {
    if (userId) {
      const newTerm = { term: { 'idUser.id': userId } };
      updateFilterBy(newTerm);
    }
  }, [userId]);

  const emptyDateFilter = useCallback(() => {
    setStartDate(null);
    setEndDate(null);
  }, []);

  /**
   * Update filter
   */
  useEffect(() => {
    if (userId) {
      const newTerm = { term: { 'idUser.id': userId } };
      switch (filter) {
        case 'all':
          emptyDateFilter();
          setSerachVariables(prevState => ({ ...prevState, filterBy: [newTerm] }));
        case 'custom':
          setSerachVariables(prevState => ({ ...prevState, filterBy: [newTerm] }));
          break;
        case 'today':
          emptyDateFilter();
          const newFilterBy = [newTerm, { term: { dateCreation: 'now/d' } }];
          setSerachVariables(prevState => ({ ...prevState, filterBy: newFilterBy }));

          break;
        default:
          break;
      }
    }
  }, [filter, userId]);

  /**
   * Custom Filter with start date and end date (update must)
   */
  useEffect(() => {
    const textSearch = searchText.replace(/\s+/g, '\\ ').replace('/', '\\/');
    const queryString = {
      query_string: {
        query:
          textSearch.startsWith('*') || textSearch.endsWith('*') ? textSearch : `*${textSearch}*`,
        analyze_wildcard: true,
      },
    };

    const rangeStartDate = {
      range: {
        dateCreation: {
          gte: moment(startDate).format('YYYY-MM-DD'),
        },
      },
    };

    const rangeEndDate = {
      range: {
        dateCreation: {
          lte: moment(endDate).format('YYYY-MM-DD'),
        },
      },
    };

    const newMust: any[] = [queryString];

    if (startDate && !endDate) {
      setMust([...newMust, ...[rangeStartDate]]);
    } else if (!startDate && endDate) {
      setMust([...newMust, ...[rangeEndDate]]);
    } else if (startDate && endDate) {
      setMust([...newMust, ...[rangeStartDate], ...[rangeEndDate]]);
    } else {
      setMust(newMust);
    }
  }, [startDate, endDate, searchText]);

  const updateFilterBy = (newTerm: any) => {
    let newFilterBy: any[] = [];
    if (filterBy.length > 0) {
      filterBy.map(item => {
        if (item && !isEqual(item, newTerm)) {
          newFilterBy = [...filterBy, ...[newTerm]];
        }
      });
    } else {
      newFilterBy = [...filterBy, ...[newTerm]];
    }
    setSerachVariables(prevState => ({ ...prevState, filterBy: newFilterBy, take: null }));
  };

  const filters = [
    { id: 'all', libelle: 'Toutes les périodes' },
    { id: 'today', libelle: "Aujourd'hui" },
    { id: 'custom', libelle: 'Période personnalisée' },
  ];

  const handleChangeFilter = (e: ChangeEvent<any>) => {
    if (e && e.target && e.target.value) {
      setFilter(e.target.value);
    }
  };

  const handleChangeSearch = useCallback((e: ChangeEvent<any>) => {
    const value = e && e.target && e.target.value ? e.target.value : '';
    setSearchText(value);
  }, []);

  const handleStartDateChange = date => {
    setStartDate(date);
  };

  const handleEndDateChange = date => {
    setEndDate(date);
  };

  const userrole = capitalize((user && user.role && user.role.nom) || 'User Role');
  const momentCalendarFormats: moment.CalendarSpec = {
    sameDay: "[Aujourd'hui]",
    nextDay: '[Demain]',
    nextWeek: 'dddd',
    lastDay: '[Hier]',
    lastWeek: '[Dernier] dddd',
    sameElse: 'DD/MM/YYYY',
  };

  return (
    <CustomFullScreenModal
      title="Historique du personnel"
      open={open}
      setOpen={setOpen}
      closeIcon={true}
      withBtnsActions={false}
      headerWithBgColor={true}
      className={classes.historyRoot}
      fullScreen={true}
    >
      <Box display="flex" flexDirection="row" padding="25px">
        <Box className={classes.leftLayout}>
          <Box position="sticky" top="89px" width="100%">
            <div className={classes.historyUser}>
              {user &&
              user.userPhoto &&
              user.userPhoto.fichier &&
              user.userPhoto.fichier.publicUrl ? (
                <Avatar src={user.userPhoto.fichier.publicUrl} />
              ) : (
                <Avatar className={classes.avatarLarge}>
                  {stringToAvatar((user && user.userName) || 'U')}
                </Avatar>
              )}

              <div className={classes.historyUserInfo}>
                <Typography className={classes.historyUserName}>
                  {(user && user.userName) || 'User'}
                </Typography>
                <Typography className={classes.historyUserRole}>{userrole}</Typography>
              </div>
            </div>
            <div className={classes.historyFilter}>
              <Typography className={classes.historyFilterTitle}>Filtre de recherche</Typography>
              <div className={classes.historyFilterForm}>
                <CustomSelect
                  label="Période"
                  list={filters}
                  listId="id"
                  index="libelle"
                  name="filter"
                  value={filter}
                  onChange={handleChangeFilter}
                  required={false}
                />
                {filter === 'custom' && (
                  <>
                    <CustomDatePicker
                      label="Date début"
                      placeholder="Date début"
                      onChange={handleStartDateChange}
                      name="startDate"
                      value={startDate}
                    />
                    <CustomDatePicker
                      label="Date fin"
                      placeholder="Date fin"
                      onChange={handleEndDateChange}
                      name="endDate"
                      value={endDate}
                      minDate={startDate}
                      disabled={!startDate ? true : false}
                    />
                  </>
                )}
              </div>
            </div>
          </Box>
        </Box>
        <Divider className={classes.historyDivider} orientation="vertical" flexItem={true} />
        <Box
          className={classes.historyLayout}
          display="flex"
          alignItems="flex-start"
          justifyContent="flex-start"
          flexDirection="column"
        >
          <div className={classes.searchForm}>
            <FormControl
              fullWidth={true}
              variant="outlined"
              placeholder="Rechercher dans l'historique"
            >
              <OutlinedInput
                id="input-serach-history"
                value={searchText}
                onChange={handleChangeSearch}
                placeholder="Rechercher dans l'historique"
                endAdornment={
                  <InputAdornment position="end">
                    <SearchIcon />
                  </InputAdornment>
                }
              />
            </FormControl>
          </div>

          <div className={classes.historyContainer}>
            {(loading || getUserResult.loading) && (
              <Box marginBottom="25px">
                <LoaderSmall />
              </Box>
            )}
            {error && <Typography variant="h6">Error :)</Typography>}
            {groupedData && groupedData.length > 0 ? (
              <>
                {groupedData.map((tab: any, index: number) => {
                  if (tab && tab.length > 0) {
                    const group = tab[index];
                    return (
                      <>
                        <div key={`history_group_${index}`} className={classes.historyGroup}>
                          <Typography>
                            {moment(group.dateCreation).format('DD MMMM YYYY')}
                          </Typography>
                          <Typography>
                            {moment(group.dateCreation, 'YYYY-MM-DD')
                              .locale('fr')
                              .calendar(null, momentCalendarFormats)}
                          </Typography>
                        </div>
                        <div className={classes.historyList}>
                          {tab.map((item: any, index: number) => {
                            if (item) {
                              return (
                                <div
                                  key={`history_tab_item_${index}`}
                                  className={classes.historyListItem}
                                >
                                  <Typography className={classes.historyListItemHour}>
                                    {moment(item.dateCreation).format('HH:mm')}
                                  </Typography>
                                  <Typography className={classes.historyListItemText}>
                                    {item.idActiviteType && item.idActiviteType.nom}
                                  </Typography>
                                </div>
                              );
                            }
                          })}
                        </div>
                      </>
                    );
                  }
                })}
              </>
            ) : (
              <> {!loading && !error && <Typography variant="h6">Aucun résultat</Typography>}</>
            )}
          </div>
        </Box>
      </Box>
    </CustomFullScreenModal>
  );
};

export default withRouter(HistoriquePersonnel);
