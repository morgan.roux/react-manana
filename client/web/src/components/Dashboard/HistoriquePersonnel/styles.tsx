import { makeStyles, Theme } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) => ({
  historyRoot: {
    '& .MuiDialogContent-root': {
      padding: '0px 20px !important',
    },
    '& .MuiDialogContentText-root': {
      margin: '0px important',
    },
    '& *': {
      fontFamily: 'Roboto !important',
    },
    '& .MuiDialog-paperWidthSm': {
      maxWidth: 'none !important',
    },
    '& .MuiDialogTitle-root': {
      '& > h2': {
        fontSize: 30,
      },
    },
  },
  searchForm: {
    margin: '0px 0px 25px',
    width: 450,
  },
  leftLayout: {
    maxWidth: 300,
  },
  historyLayout: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
  },
  historyContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    minWidth: 500,
    '& > div': {
      padding: 0,
    },
  },
  historyUser: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  historyUserInfo: {
    marginLeft: 15,
  },
  historyUserName: {
    fontWeight: 'bold',
    fontSize: 18,
    color: theme.palette.common.black,
  },
  historyUserRole: {
    fontWeight: 'normal',
    fontSize: 14,
    color: '#9E9E9E',
  },
  historyFilter: {
    display: 'flex',
    flexDirection: 'column',
  },
  historyFilterTitle: {
    marginTop: 25,
    marginBottom: 25,
    color: '#616161',
    fontSize: 20,
  },
  historyFilterForm: {
    width: 300,
    '& > div': {
      marginBottom: '15px !important',
    },
  },
  avatarLarge: {
    maxWidth: 51,
    maxHeight: 51,
    borderRadius: '50%',
    border: '1px solid #e0e0e0',
    fontSize: '12px !important',
  },
  historyDivider: {
    margin: '-30px 20px -100vh 20px',
    backgroundColor: '#E0E0E0',
  },
  historyGroup: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    minHeight: 40,
    minWidth: 500,
    padding: '10px !important',
    borderRadius: 3,
    boxShadow: `0px 3px 2px ${theme.palette.primary.main}`,
    '& > p': {
      fontFamily: 'Roboto',
      fontSize: 18,
      fontWeight: 'bold',
    },
    '& > p:nth-child(1)': {
      textTransform: 'capitalize',
    },
  },
  historyList: {
    margin: '5px 0px',
    '& > div:nth-last-child(1)': {
      borderBottom: '0px !important',
    },
  },
  historyListItem: {
    display: 'flex',
    flexDirection: 'column',
    padding: ' 12px 10px !important',
    borderBottom: '1px solid #E0E0E0',
  },
  historyListItemHour: {
    color: '#616161',
    fontSize: 16,
    fontWeight: 'bold',
  },
  historyListItemText: {
    color: theme.palette.common.black,
    fontWeight: 'bold',
    fontSize: 16,
  },
}));

export default useStyles;
