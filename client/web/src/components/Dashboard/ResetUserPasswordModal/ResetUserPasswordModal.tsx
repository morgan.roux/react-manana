import React, { FC, ChangeEvent, useState, useCallback } from 'react';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import { CustomModal } from '../../Common/CustomModal';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import {
  RESET_USER_PASSWORD,
  RESET_USER_PASSWORDVariables,
} from '../../../graphql/User/types/RESET_USER_PASSWORD';
import { DO_RESET_USER_PASSWORD } from '../../../graphql/User/mutation';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import SnackVariableInterface from '../../../Interface/SnackVariableInterface';
import { useStyles } from './styles';
import { CustomFormTextField } from '../../Common/CustomTextField';

interface ResetUserPasswordInterface {
  userId: string;
  email: string;
  login: string;
  open: boolean;
  setOpen: (open: boolean) => void;
}

const ResetUserPasswordModal: FC<ResetUserPasswordInterface> = ({
  userId,
  open,
  setOpen,
  email,
  login,
}) => {
  const [password, setPassword] = useState<string>('');
  const [showPassword, setShowPassword] = useState<boolean>(false);

  const classes = useStyles({});
  const client = useApolloClient();

  const [resetUserPassword] = useMutation<RESET_USER_PASSWORD, RESET_USER_PASSWORDVariables>(
    DO_RESET_USER_PASSWORD,
    {
      onError: errors => {
        let errorMessage: string = 'Erreur du serveur';
        errors.graphQLErrors.map(error => {
          if (error.extensions) {
            switch (error.extensions.code) {
              case 'ERROR_RESER_PASSWORD_USER':
                errorMessage =
                  'Une erreur est survenue lors de la Réinitialisation du mot de passe';
                break;
            }
          }
        });

        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: errorMessage,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      },
      onCompleted: data => {
        if (data && data.resetUserPassword) {
          setOpen(false);
          setShowPassword(false);
          setPassword('');
          const snackBarData: SnackVariableInterface = {
            type: 'SUCCESS',
            message: 'Réinitialisation du mot de passe se termine avec succès',
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
        }
      },
    },
  );

  const toggleShowPassword = useCallback(() => {
    setShowPassword(prev => !prev);
  }, []);

  const handleMouseDownPassword = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    event.stopPropagation();
  };

  const handleValueChange = (e: ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setPassword(value);
  };

  const handleSubmit = () => {
    if (userId) {
      resetUserPassword({ variables: { id: userId, password } });
    }
  };

  return (
    <CustomModal
      open={open}
      actionButton="Réinitialiser"
      title="Réinitialisation du mot de passe"
      setOpen={setOpen}
      onClickConfirm={handleSubmit}
      disabledButton={password && userId ? false : true}
    >
      <div>
        {login && <CustomFormTextField type="text" label="Login" value={login} disabled={true} />}
        <CustomFormTextField type="text" label="Email de lien" value={email} disabled={true} />
        <CustomFormTextField
          label="Mot de passe"
          // type={showPassword ? 'text' : 'password'}
          name="password"
          value={password}
          onChange={handleValueChange}
          margin="dense"
          autoComplete="off"
          // InputProps={{
          //   endAdornment: (
          //     <InputAdornment position="end">
          //       <IconButton
          //         size="small"
          //         aria-label="toggle password visibility"
          //         onClick={toggleShowPassword}
          //         onMouseDown={handleMouseDownPassword}
          //       >
          //         {showPassword ? <Visibility /> : <VisibilityOff />}
          //       </IconButton>
          //     </InputAdornment>
          //   ),
          // }}
        />
      </div>
    </CustomModal>
  );
};

export default ResetUserPasswordModal;
