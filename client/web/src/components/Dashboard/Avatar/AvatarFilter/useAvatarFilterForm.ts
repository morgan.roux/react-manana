import { useState, ChangeEvent } from 'react';

export interface AvataFilterInterface {
  description: string;
  creator: string;
  sexe: 'ALL' | 'M' | 'F';
}

export const initialState: AvataFilterInterface = {
  description: '',
  creator: '',
  sexe: 'ALL',
};

const useAvatarFilterForm = (defaultState?: AvataFilterInterface) => {
  const initValues: AvataFilterInterface = defaultState || initialState;
  const [values, setValues] = useState<AvataFilterInterface>(initValues);

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  return {
    handleChange,
    values,
    setValues,
  };
};

export default useAvatarFilterForm;
