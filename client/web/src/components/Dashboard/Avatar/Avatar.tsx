import React, { FC, useState, useEffect, useMemo } from 'react';
import useStyles from './styles';
import AvatarHead from './AvatarHead';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import AvatarList from './AvatarList';
import AvatarForm from './AvatarForm';
import useAvatarForm from './AvatarForm/useAvatarForm';
import { last } from 'lodash';
import { useMutation, useApolloClient, useLazyQuery } from '@apollo/react-hooks';
import { DO_CREATE_UPDATE_AVATAR, GET_AVATAR } from '../../../graphql/Avatar';
import {
  CREATE_UPDATE_AVATARVariables,
  CREATE_UPDATE_AVATAR,
} from '../../../graphql/Avatar/types/CREATE_UPDATE_AVATAR';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../graphql/S3';
import { uploadToS3 } from '../../../services/S3';
import { TypeFichier } from '../../../types/graphql-global-types';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import Backdrop from '../../Common/Backdrop';
import { getGroupement } from '../../../services/LocalStorage';
import { formatFilenameWithoutDate } from '../../../utils/filenameFormater';
import { AVATAR, AVATARVariables } from '../../../graphql/Avatar/types/AVATAR';

const Avatar: FC<RouteComponentProps> = ({
  location: { pathname },
  match: { params },
  history: { push },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const groupement = getGroupement();

  const { avatarId } = params as any;
  const EDIT_URL = `/db/avatar/edit/${avatarId}`;

  const [selected, setSelected] = useState<any[]>([]);
  const [selectedFiles, setselectedFiles] = useState<File[]>([]);
  const [croppedFiles, setCroppedFiles] = useState<File[]>([]);
  const [imageUrl, setImageUrl] = useState<string | null | undefined>(undefined);
  const [deleteRow, setDeleteRow] = useState<any>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const [loading, setLoading] = useState<boolean>(false);

  const { handleChange, values, setValues } = useAvatarForm();

  const { id, nom, sexe, image } = values;

  const isOnList: boolean = pathname === '/db/avatar';
  const isOnCreate: boolean = pathname === '/db/avatar/create';
  const isOnEdit: boolean = pathname.startsWith('/db/avatar/edit');

  const avatarImage = selectedFiles && selectedFiles.length > 0 && last(selectedFiles);
  const avatarImageSrc = imageUrl || (avatarImage && URL.createObjectURL(avatarImage));

  // Img send to S3
  const avatarImageToS3 =
    croppedFiles && croppedFiles.length > 0 ? last(croppedFiles) : avatarImage;

  let loadingMsg = 'en cours...';
  loadingMsg = isOnCreate
    ? `Création ${loadingMsg}`
    : isOnEdit
    ? `Modification ${loadingMsg}`
    : `Chargement ${loadingMsg}`;

  const IMAGE_EXIST: boolean = image && image.chemin && image.nomOriginal ? true : false;

  const formIsValid = (): boolean => {
    if (nom && sexe && (selectedFiles.length > 0 || IMAGE_EXIST)) {
      return true;
    }
    return false;
  };

  // console.log('values ::::>> ', values);

  const onClickSaveAvatar = () => {
    if (formIsValid()) {
      setLoading(true);
      if (selectedFiles.length > 0) {
        // Presigned file
        const filePathsTab: string[] = [];
        selectedFiles.map((file: File) => {
          filePathsTab.push(
            `avatars/${groupement && groupement.id}/${formatFilenameWithoutDate(file)}`,
          );
        });
        doCreatePutPresignedUrl({ variables: { filePaths: filePathsTab } });
      }

      // EDIT WITHOUT NEW IMAGE
      if (isOnEdit && formIsValid() && IMAGE_EXIST && selectedFiles.length === 0) {
        // Execute createUpdateAvatar
        createUpdateAvatar({
          variables: {
            id,
            description: nom,
            codeSexe: sexe as any,
            fichier: {
              chemin: image && (image.chemin as any),
              nomOriginal: image && (image.nomOriginal as any),
              type: TypeFichier.AVATAR,
              idAvatar: image && image.idAvatar,
            },
          },
        });
      }
    }
  };

  /**
   * Mutation createPutPresignedUrl
   */
  const [doCreatePutPresignedUrl, { loading: presignedLoading }] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (
        data &&
        data.createPutPresignedUrls &&
        data.createPutPresignedUrls.length > 0 &&
        avatarImageToS3
      ) {
        const photo = data.createPutPresignedUrls[0];
        if (photo && photo.filePath && photo.presignedUrl) {
          setLoading(true);
          await uploadToS3(avatarImageToS3, photo.presignedUrl)
            .then(result => {
              if (result && result.status === 200) {
                createUpdateAvatar({
                  variables: {
                    id,
                    description: nom,
                    codeSexe: sexe as any,
                    fichier: {
                      chemin: photo.filePath,
                      nomOriginal: avatarImageToS3.name,
                      type: TypeFichier.AVATAR,
                      idAvatar: image && image.idAvatar,
                    },
                  },
                });
              }
            })
            .catch(error => {
              console.error(error);
              setLoading(false);
              displaySnackBar(client, {
                type: 'ERROR',
                message:
                  "Erreur lors de l'envoye de(s) fichier(s). Si vous utilisez AdBlocker, désactivez-le et réessayez.",
                isOpen: true,
              });
            });
        }
      }
    },
    onError: errors => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: errors.message,
        isOpen: true,
      });
      setLoading(false);
    },
  });

  /**
   * Mutation createUpdateAvatar
   */
  const [createUpdateAvatar, { loading: mutationLoading }] = useMutation<
    CREATE_UPDATE_AVATAR,
    CREATE_UPDATE_AVATARVariables
  >(DO_CREATE_UPDATE_AVATAR, {
    onCompleted: data => {
      if (data && data.createUpdateAvatar) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: `Avatar ${isOnCreate ? 'créé' : 'modifié'} avec succès`,
          isOpen: true,
        });
        push('/db/avatar');
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
      setLoading(false);
    },
  });

  // EDIT
  /**
   * Get avatar for edit
   */
  const [getAvatar, { loading: avatarLoading }] = useLazyQuery<AVATAR, AVATARVariables>(
    GET_AVATAR,
    {
      // fetchPolicy: 'network-only',
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          });
        });
        setLoading(false);
      },
      onCompleted: data => {
        if (data && data.avatar) {
          const avatar = data.avatar;
          // Set values for edit
          setValues({
            id: avatar.id,
            nom: avatar.description as any,
            sexe: avatar.codeSexe as any,
            image: {
              chemin: avatar.fichier && avatar.fichier && (avatar.fichier.chemin as any),
              nomOriginal: avatar.fichier && avatar.fichier && (avatar.fichier.nomOriginal as any),
              type: TypeFichier.AVATAR,
              idAvatar: image && image.idAvatar,
            },
          });
        }
      },
    },
  );

  /**
   * Get publicite for edit
   */
  useEffect(() => {
    if (pathname === EDIT_URL && avatarId) {
      // Get avatar
      getAvatar({ variables: { id: avatarId } });
    }
  }, [pathname, avatarId]);

  /**
   * Set deleteRow to null
   */
  useMemo(() => {
    if (deleteRow) {
      setDeleteRow(null);
    }
  }, [selected]);

  const isLoading = (): boolean => {
    if (loading || presignedLoading || mutationLoading || avatarLoading) {
      return true;
    }
    return false;
  };

  console.log('jjjjjjjjjj', croppedFiles);

  return (
    <div className={classes.avatarRoot}>
      {isLoading() && <Backdrop value={avatarLoading ? 'Chargement en cours...' : loadingMsg} />}
      <AvatarHead
        isOnList={isOnList}
        isOnCreate={isOnCreate}
        isOnEdit={isOnEdit}
        onClickSaveAvatar={onClickSaveAvatar}
        selected={selected}
        setSelected={setSelected}
        deleteRow={deleteRow}
        setDeleteRow={setDeleteRow}
        openDeleteDialog={openDeleteDialog}
        setOpenDeleteDialog={setOpenDeleteDialog}
      />
      {isOnList && (
        <AvatarList
          selected={selected}
          setSelected={setSelected}
          deleteRow={deleteRow}
          setDeleteRow={setDeleteRow}
          openDeleteDialog={openDeleteDialog}
          setOpenDeleteDialog={setOpenDeleteDialog}
        />
      )}
      {!isOnList && (
        <AvatarForm
          selectedFiles={selectedFiles}
          setSelectedFiles={setselectedFiles}
          handleChangeInput={handleChange}
          values={values}
          imageUrl={imageUrl}
          setImageUrl={setImageUrl}
          croppedFiles={croppedFiles}
          setCroppedFiles={setCroppedFiles}
          avatarImageSrc={avatarImageSrc as any}
        />
      )}
    </div>
  );
};

export default withRouter(Avatar);
