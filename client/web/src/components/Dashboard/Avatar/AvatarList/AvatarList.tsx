import React, {
  FC,
  Dispatch,
  SetStateAction,
  MouseEvent,
  useState,
  useCallback,
  useEffect,
} from 'react';
import useStyles from './styles';
import CustomTableWithSearch from '../../../Common/CustomTableWithSearch';
import { CustomTableWithSearchColumn } from '../../../Common/CustomTableWithSearch/interface';
import moment from 'moment';
import { Tooltip, Fade, IconButton } from '@material-ui/core';
import { Delete, Edit } from '@material-ui/icons';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Sexe } from '../../../../types/graphql-global-types';
import { startCase } from 'lodash';
import EmptyImgSrc from '../../../../assets/img/img_no_list_item2.png';
import { CustomModal } from '../../../Common/CustomModal';
import AvatarFilter from '../AvatarFilter';
import useAvatarFilterForm from '../AvatarFilter/useAvatarFilterForm';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import {
  DELETE_AVATARS,
  DELETE_AVATARSVariables,
} from '../../../../graphql/Avatar/types/DELETE_AVATARS';
import { DO_DELETE_AVATARS } from '../../../../graphql/Avatar';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../../Common/ConfirmDialog';
import { isInvalidArray } from '../../../../utils/Helpers';
import { getGroupement } from '../../../../services/LocalStorage';

interface AvatarListProps {
  selected: any[];
  deleteRow: any;
  openDeleteDialog: boolean;
  setSelected: Dispatch<SetStateAction<any[]>>;
  setDeleteRow: Dispatch<SetStateAction<any>>;
  setOpenDeleteDialog: Dispatch<SetStateAction<boolean>>;
}

const AvatarList: FC<AvatarListProps & RouteComponentProps> = ({
  selected,
  setSelected,
  setDeleteRow,
  deleteRow,
  openDeleteDialog,
  setOpenDeleteDialog,
  history: { push },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const groupement = getGroupement();

  const [filterBy, setFilterBy] = useState<any[]>([]);
  const [openFilterModal, setOpenFilterModal] = useState<boolean>(false);

  const { values, setValues, handleChange } = useAvatarFilterForm();
  const { description, creator, sexe } = values;

  const [data, setData] = useState<any[]>([]);

  const emptyTitle = 'Aucun Avatar';
  const emptySubTitle = "Vous n'avez pas encore d'avatar dans cette rubrique.";

  const ACTIONS_COLUMNS: CustomTableWithSearchColumn[] = [
    {
      key: '',
      label: '',
      numeric: false,
      disablePadding: false,
      sortable: false,
      renderer: (row: any) => {
        return (
          <>
            <Tooltip
              TransitionComponent={Fade}
              TransitionProps={{ timeout: 600 }}
              title="Supprimer"
            >
              <IconButton color="inherit" onClick={onClickDelete(row)}>
                <Delete />
              </IconButton>
            </Tooltip>
            <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Modifier">
              <IconButton color="secondary" onClick={goToEdit(row.id)}>
                <Edit />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  const DATES_COLUMNS: CustomTableWithSearchColumn[] = [
    {
      key: 'dateCreation',
      label: 'Date de création',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return moment(row.dateDebut).format('DD/MM/YYYY');
      },
    },
    {
      key: 'dateModification',
      label: 'Mise à jour',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return moment(row.dateFin).format('DD/MM/YYYY');
      },
    },
  ];

  const AVATAR_COLUMNS: CustomTableWithSearchColumn[] = [
    {
      key: '',
      label: 'Image',
      numeric: false,
      disablePadding: false,
      sortable: false,
      renderer: (row: any) => {
        if (row && row.fichier && row.fichier.publicUrl) {
          return (
            <img
              src={row.fichier.publicUrl}
              style={{ width: 50, height: 50, borderRadius: '50%' }}
            />
          );
        }
        return '-';
      },
    },
    {
      key: 'description',
      label: 'Désignation',
      numeric: false,
      disablePadding: false,
    },
    {
      key: 'codeSexe',
      label: 'Sexe',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return (row.codeSexe && row.codeSexe === Sexe.M ? 'Homme' : 'Femme') || '-';
      },
    },
    {
      key: 'userCreation.userName',
      label: 'Créateur',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return (row.userCreation && row.userCreation.userName) || '-';
      },
    },
    {
      key: 'userCreation.role.nom',
      label: 'Rôle',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return (
          (row.userCreation &&
            row.userCreation.role &&
            row.userCreation.role.nom &&
            startCase(row.userCreation.role.nom.toLowerCase())) ||
          '-'
        );
      },
    },
  ];

  const COLUMNS: CustomTableWithSearchColumn[] = [
    ...AVATAR_COLUMNS,
    ...DATES_COLUMNS,
    ...ACTIONS_COLUMNS,
  ];

  /**
   * Mutation delete avatars
   */
  const [deleteAvatars, { loading, data: deleteData }] = useMutation<
    DELETE_AVATARS,
    DELETE_AVATARSVariables
  >(DO_DELETE_AVATARS, {
    onCompleted: data => {
      if (data && data.deleteAvatars && data.deleteAvatars.length > 0) {
        setDeleteRow(null);
        const isInvalid: boolean = isInvalidArray(data.deleteAvatars);
        if (isInvalid) {
          displaySnackBar(client, {
            type: 'ERROR',
            message: 'Erreur lors de la suppression',
            isOpen: true,
          });
        } else {
          setSelected([]);
          displaySnackBar(client, {
            type: 'SUCCESS',
            message: 'Avatar(s) supprimé(s) avec succès',
            isOpen: true,
          });
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        const { extensions: ext } = err;
        let msg = err.message;
        if (ext && ext.code === 'NO_AVATAR_TO_DELETE') {
          msg = 'Aucune avatar a supprimé';
        }
        if (ext && ext.code === 'AVATAR_ALREADY_USED') {
          msg = 'Ces avatars sont déjà utilisés par des utilisateurs, impossible de les supprimés';
        }
        displaySnackBar(client, { type: 'ERROR', message: msg, isOpen: true });
      });
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
    },
  });

  const onClickDelete = (row: any) => (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    setOpenDeleteDialog(true);
    setDeleteRow(row);
    if (selected.length > 0) {
      setSelected([]);
    }
  };

  const goToEdit = (id: string) => () => {
    push(`/db/avatar/edit/${id}`);
  };

  const onClickBtnFilter = () => {
    setOpenFilterModal(true);
  };

  const onClickApplyBtn = () => {
    setOpenFilterModal(false);
    // Set filterBy
    const newFilterBy: any[] = [];
    if (description) {
      const newTerm = { wildcard: { description: `*${description}*` } };
      newFilterBy.push(newTerm);
    }
    if (creator) {
      const newTerm = { wildcard: { 'userCreation.userName': `*${creator}*` } };
      newFilterBy.push(newTerm);
    }

    if (sexe && sexe !== 'ALL') {
      const newTerm = { term: { codeSexe: sexe } };
      newFilterBy.push(newTerm);
    }
    setFilterBy(newFilterBy);
  };

  const onClickInitBtn = useCallback(() => {
    setOpenFilterModal(false);
    setValues(prevState => ({ ...prevState, description: '', creator: '', sexe: 'ALL' }));
    setFilterBy([]);
  }, []);

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (selected.length > 0) {
      const ids: string[] = selected.map(i => i.id);
      deleteAvatars({ variables: { ids } });
    }

    if (deleteRow && deleteRow.id) {
      deleteAvatars({ variables: { ids: [deleteRow.id] } });
    }
  };

  /**
   * Re-render component after delete
   */
  useEffect(() => {
    if (deleteData && deleteData.deleteAvatars) {
      onClickInitBtn();
      // setFilterBy([]);
    }
  }, [deleteData]);

  const DeleteDialogContent = () => {
    if (deleteRow && deleteRow.description) {
      return (
        <span>
          Êtes-vous sur de vouloir supprimer l'avatar
          <span style={{ fontWeight: 'bold' }}> {deleteRow.description}</span> ?
        </span>
      );
    }
    return <span>Êtes-vous sur de vouloir supprimer ces avatars ?</span>;
  };

  return (
    <div className={classes.avatarListRoot}>
      {loading && <Backdrop value="Suppression en cours..." />}

      <CustomTableWithSearch
        searchType="avatar"
        isSelectable={true}
        selected={selected}
        setSelected={setSelected}
        showToolbar={true}
        columns={COLUMNS}
        withEmptyContent={true}
        emptyTitle={emptyTitle}
        emptySubTitle={emptySubTitle}
        emptyImgSrc={EmptyImgSrc}
        withInputSearch={false}
        withFilterBtn={true}
        onClickBtnFilter={onClickBtnFilter}
        filterByFromPros={filterBy}
        dataFromProps={data}
        setDataFromProps={setData}
        optionalMust={[
          {
            term: {
              idGroupement: groupement.id,
            },
          },
        ]}
      />
      <CustomModal
        title="Filtres de recherche"
        open={openFilterModal}
        setOpen={setOpenFilterModal}
        closeIcon={true}
        headerWithBgColor={true}
        withBtnsActions={false}
        maxWidth="xl"
      >
        <AvatarFilter
          state={values}
          setState={setValues}
          handleChangeInput={handleChange}
          onClickApplyBtn={onClickApplyBtn}
          onClickInitBtn={onClickInitBtn}
        />
      </CustomModal>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        content={<DeleteDialogContent />}
        onClickConfirm={onClickConfirmDelete}
      />
    </div>
  );
};

export default withRouter(AvatarList);
