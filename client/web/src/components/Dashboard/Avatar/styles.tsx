import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatarRoot: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      position: 'relative',
      height: 'auto',
      minHeight: 'fit-content',
    },
    mutationSuccessContainer: {
      width: '100%',
      minHeight: 'calc(100vh - 156px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& p': {
        textAlign: 'center',
      },
    },
  }),
);

export default useStyles;
