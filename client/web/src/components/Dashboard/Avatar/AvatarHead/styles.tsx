import { Theme, createStyles, makeStyles, lighten } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatarHeadRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: 70,
      borderBottom: '1px solid #E0E0E0',
      padding: '0px 30px',
      position: 'sticky',
      top: 0,
      zIndex: 1,
      opacity: 1,
      backgroundColor: theme.palette.common.white,
    },
    avatarHeadRootWithBg: {
      background: lighten(theme.palette.primary.main, 0.1),
      color: theme.palette.common.white,
      border: 'none',
    },
    avatarHeadTitle: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      fontSize: 20,
    },
    avatarHeadBtnsContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      '& > button:not(:nth-last-child(1))': {
        marginRight: 15,
      },
    },
  }),
);

export default useStyles;
