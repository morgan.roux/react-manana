import React, {
  FC,
  ChangeEvent,
  Dispatch,
  SetStateAction,
  useEffect,
  useCallback,
  useState,
} from 'react';
import useStyles from './styles';
import { Typography } from '@material-ui/core';
import Dropzone from '../../../Common/Dropzone';
import CustomSelect from '../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import { AvataInterface } from './useAvatarForm';
import { AWS_HOST } from '../../../../utils/s3urls';
import { last } from 'lodash';
import { CustomModal } from '../../../Common/CustomModal';
import { CustomReactEasyCrop } from '../../../Common/CustomReactEasyCrop';
import { Sexe } from '../../../../types/graphql-global-types';

interface AvatarFormProps {
  selectedFiles: File[];
  croppedFiles: File[];
  values: AvataInterface;
  imageUrl: string | null | undefined;
  setImageUrl: Dispatch<SetStateAction<string | null | undefined>>;
  avatarImageSrc: string;
  setSelectedFiles: (files: File[]) => void;
  setCroppedFiles: (files: File[]) => void;
  handleChangeInput: (e: ChangeEvent<any>) => void;
}

const AvatarForm: FC<AvatarFormProps> = ({
  selectedFiles,
  setSelectedFiles,
  values,
  handleChangeInput,
  imageUrl,
  setImageUrl,
  // croppedFiles,
  setCroppedFiles,
  avatarImageSrc,
}) => {
  const classes = useStyles({});
  const [openResize, setOpenResize] = useState<boolean>(false);
  const [croppedImgUrl, setCroppedImgUrl] = useState<any>('');
  const [savedCroppedImage, setSavedCroppedImage] = useState<boolean>(false);

  // const avatarImage = selectedFiles && selectedFiles.length > 0 && last(selectedFiles);
  // const avatarImageSrc = imageUrl || (avatarImage && URL.createObjectURL(avatarImage));

  const AVATAR_SEXE_LIST = [
    { id: Sexe.M, value: 'Homme' },
    { id: Sexe.F, value: 'Femme' },
  ];

  const { nom, sexe, image } = values;

  const deleteAvatar = () => {
    console.log(' Delete Avatar Photo :>> ');
  };

  const handleClickResize = useCallback(() => {
    setOpenResize(true);
    // Reset cropped image
    setSavedCroppedImage(false);
    setCroppedImgUrl(null);
  }, []);

  /**
   * Reset preview
   */
  useEffect(() => {
    if (selectedFiles && selectedFiles.length > 0) {
      // Reset cropped image
      setCroppedImgUrl(null);
      // Open Resize
      handleClickResize();
    }
  }, [selectedFiles]);

  const saveResizedImage = () => {
    setOpenResize(false);
    setSavedCroppedImage(true);
  };

  /**
   * Set image url
   */
  useEffect(() => {
    if (image && image.chemin) {
      setImageUrl(`${AWS_HOST}/${image.chemin}`);
    }

    if (selectedFiles && selectedFiles.length > 0) {
      const file = last(selectedFiles);
      if (file) {
        const url = URL.createObjectURL(file);
        setImageUrl(url);
      }
    }
  }, [image, selectedFiles]);

  return (
    <div className={classes.avatarFormRoot}>
      <div className={classes.imgFormContainer}>
        <Typography className={classes.inputTitle}>Image de l'avatar</Typography>
        <Dropzone
          contentText="Glissez et déposez ici votre photo"
          selectedFiles={selectedFiles}
          setSelectedFiles={setSelectedFiles}
          multiple={false}
          acceptFiles="image/*"
          withFileIcon={false}
          where="inUserSettings"
          withImagePreview={true}
          withImagePreviewCustomized={true}
          onClickDelete={deleteAvatar}
          fileAlreadySetUrl={croppedImgUrl || (imageUrl as any)}
          onClickResize={handleClickResize}
        />
      </div>
      <div className={classes.descFormContainer}>
        <Typography className={classes.inputTitle}>Description</Typography>
        <div className={classes.descInputsContainer}>
          <div className={classes.formRow}>
            <Typography>Désignation</Typography>
            <CustomFormTextField
              name="nom"
              value={nom}
              onChange={handleChangeInput}
              placeholder="Nom de l'avatar"
            />
          </div>
          <div className={classes.formRow}>
            <Typography>Sexe</Typography>
            <CustomSelect
              label=""
              list={AVATAR_SEXE_LIST}
              listId="id"
              index="value"
              name="sexe"
              value={sexe}
              onChange={handleChangeInput}
              shrink={false}
              placeholder="Sexe de l'avatar"
              withPlaceholder={true}
            />
          </div>
        </div>
      </div>
      <CustomModal
        open={openResize}
        setOpen={setOpenResize}
        title="Redimensionnement"
        onClickConfirm={saveResizedImage}
        actionButton="Enregistrer"
        closeIcon={true}
        centerBtns={true}
      >
        {avatarImageSrc ? (
          <CustomReactEasyCrop
            src={avatarImageSrc}
            withZoom={true}
            croppedImage={croppedImgUrl}
            setCroppedImage={setCroppedImgUrl}
            setCropedFiles={setCroppedFiles}
            savedCroppedImage={savedCroppedImage}
          />
        ) : (
          <div>Aucune Image</div>
        )}
      </CustomModal>
    </div>
  );
};

export default AvatarForm;
