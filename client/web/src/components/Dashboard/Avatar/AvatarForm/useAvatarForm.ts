import { useState, ChangeEvent } from 'react';
import { FichierInput, Sexe } from '../../../../types/graphql-global-types';

export interface AvataInterface {
  id: string | null;
  image: FichierInput | null;
  nom: string;
  sexe: Sexe | string;
}

export const initialState: AvataInterface = {
  id: null,
  image: null,
  nom: '',
  sexe: '',
};

const useAvatarForm = (defaultState?: AvataInterface) => {
  const initValues: AvataInterface = defaultState || initialState;
  const [values, setValues] = useState<AvataInterface>(initValues);

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  return {
    handleChange,
    values,
    setValues,
  };
};

export default useAvatarForm;
