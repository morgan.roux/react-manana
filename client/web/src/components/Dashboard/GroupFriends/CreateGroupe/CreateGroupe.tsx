import { useApolloClient, useMutation } from '@apollo/react-hooks';
import {
  Box,
  Button,
  DialogContent,
  FormControl,
  FormControlLabel,
  MenuItem,
  Radio,
  RadioGroup,
  TextField,
} from '@material-ui/core';
import React, { FC, useState } from 'react';
import { DO_CREATE_UPDATE_GROUPE_AMIS } from '../../../../graphql/GroupeAmis/mutation';
import {
  CREATE_UPDATE_GROUPE_AMIS,
  CREATE_UPDATE_GROUPE_AMISVariables,
} from '../../../../graphql/GroupeAmis/types/CREATE_UPDATE_GROUPE_AMIS';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import styles from './styles';

interface CreateGroupeProps {
  handleCloseCreate: any;
  setAddedGroup: any;
  addedGroup: number;
}

const CreateGroupe: FC<CreateGroupeProps> = (props: CreateGroupeProps) => {
  const classes = styles();
  const { handleCloseCreate, setAddedGroup, addedGroup } = props;
  const [name, setName] = useState('');
  const [checked, setChecked] = useState('ACTIVER');
  const [type, setType] = useState('PHARMACIE');
  const client = useApolloClient();
  const [isDisabled, setIsDisabled] = useState(false);
  const [description, setDescription] = useState('');

  const [createUpdateGroup, { loading: loadingCreate, data }] = useMutation<
    CREATE_UPDATE_GROUPE_AMIS,
    CREATE_UPDATE_GROUPE_AMISVariables
  >(DO_CREATE_UPDATE_GROUPE_AMIS, {
    onCompleted: data => {
      setAddedGroup(addedGroup + 1);
      if (data && data.createUpdateGroupeAmis) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Groupe ajouté avec succès',
          isOpen: true,
        });
      }
      setIsDisabled(false);
      handleCloseCreate();
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: "Erreur lors de l'ajout", isOpen: true });
      });
      setIsDisabled(false);
      handleCloseCreate();
    },
  });
  const handleChangeType = (event: React.ChangeEvent<HTMLInputElement>) => {
    setType(event.target.value);
  };
  const handleChangeName = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };
  const handleChangeDescription = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDescription(event.target.value);
  };

  const handleAddGroup = () => {
    setIsDisabled(true);
    if (name.length === 0) {
      displaySnackBar(client, {
        type: 'ERROR',
        message: 'Veuillez renseigner le nom du groupe',
        isOpen: true,
      });
      return;
    }
    createUpdateGroup({
      variables: { input: { nom: name, status: checked as any, type: type as any, description } },
    });
  };

  return (
    <>
      <DialogContent className={classes.create}>
        {loadingCreate && <Backdrop value={'Ajout en cours...'} />}
        <TextField
          variant="outlined"
          label="Nom du Groupe"
          InputLabelProps={{ shrink: true }}
          fullWidth
          value={name}
          onChange={handleChangeName}
        />

        <TextField
          variant="outlined"
          label="Description"
          InputLabelProps={{ shrink: true }}
          fullWidth
          multiline
          value={description}
          onChange={handleChangeDescription}
          rows={4}
        />

        <TextField
          variant="outlined"
          label="Type"
          InputLabelProps={{ shrink: true }}
          select
          value={type}
          fullWidth
          onChange={handleChangeType}
        >
          <MenuItem button value="PHARMACIE">
            PHARMACIE
          </MenuItem>
          <MenuItem button value="DEPARTEMENT">
            DEPARTEMENT
          </MenuItem>
          <MenuItem button value="REGION">
            REGION
          </MenuItem>
          <MenuItem button value="PERSONNALISE">
            PERSONNALISE
          </MenuItem>
        </TextField>

        <FormControl component="fieldset">
          <RadioGroup
            row
            aria-label="active"
            name="active1"
            value={checked}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setChecked(event.target.value);
            }}
          >
            <FormControlLabel
              value="ACTIVER"
              control={<Radio className={classes.radio} />}
              label="Activer"
            />
            <FormControlLabel
              value="BLOQUER"
              control={<Radio className={classes.radio} />}
              label="Bloquer"
            />
          </RadioGroup>
        </FormControl>
        <Box textAlign="center">
          <Button
            className={classes.button}
            variant="contained"
            color="secondary"
            onClick={handleAddGroup}
            disabled={isDisabled}
          >
            Ajouter
          </Button>
        </Box>
      </DialogContent>
    </>
  );
};

export default CreateGroupe;
