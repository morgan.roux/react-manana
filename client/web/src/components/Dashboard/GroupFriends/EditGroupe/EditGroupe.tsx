import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import {
  Box,
  Button,
  DialogContent,
  FormControl,
  FormControlLabel,
  MenuItem,
  Radio,
  RadioGroup,
  TextField,
} from '@material-ui/core';
import React, { FC, useState } from 'react';
import { ME_me } from '../../../../graphql/Authentication/types/ME';
import { DO_CREATE_UPDATE_GROUPE_AMIS } from '../../../../graphql/GroupeAmis/mutation';
import {
  CREATE_UPDATE_GROUPE_AMIS,
  CREATE_UPDATE_GROUPE_AMISVariables,
} from '../../../../graphql/GroupeAmis/types/CREATE_UPDATE_GROUPE_AMIS';
import { GET_CURRENT_PHARMACIE } from '../../../../graphql/Pharmacie/local';
import ICurrentPharmacieInterface from '../../../../Interface/CurrentPharmacieInterface';
import { getGroupement, getUser } from '../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import styles from './styles';

interface CreateGroupeProps {
  row?: any;
  setRow?: any;
}

const EditGroupe: FC<CreateGroupeProps> = (props: CreateGroupeProps) => {
  const classes = styles();
  const { row, setRow } = props;
  const rowId = row && row.id ? row.id : '';
  const rowDisplay = {
    nom: row && row.nom ? row.nom : '',
    status: row && row.groupStatus ? row.groupStatus : 'ACTIVER',
    type: row && row.typeGroupeAmis ? row.typeGroupeAmis : 'PHARMACIE',
    description: row && row.description ? row.description : '',
  };
  const [name, setName] = useState(rowDisplay.nom);
  const [checked, setChecked] = useState(rowDisplay.status);
  const [type, setType] = useState(rowDisplay.type);
  const [description, setDescription] = useState(rowDisplay.description);
  const client = useApolloClient();
  const [isDisabled, setIsDisabled] = useState(false);

  /**
   * Get groupement
   */
  const groupement = getGroupement();
  const idGroupement = (groupement && groupement.id) || null;

  /**
   * Get user
   */
  const user: ME_me = getUser();
  const userId = (user && user.id) || null;

  /**
   * Get current pharmacie
   */
  const { data: dataPharma } = useQuery<ICurrentPharmacieInterface>(GET_CURRENT_PHARMACIE);
  const idPharmacie = (dataPharma && dataPharma.pharmacie && dataPharma.pharmacie.id) || null;

  const [createUpdateGroup, { loading: loadingCreate, data }] = useMutation<
    CREATE_UPDATE_GROUPE_AMIS,
    CREATE_UPDATE_GROUPE_AMISVariables
  >(DO_CREATE_UPDATE_GROUPE_AMIS, {
    onCompleted: data => {
      if (data && data.createUpdateGroupeAmis) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Groupe modifié avec succès',
          isOpen: true,
        });
      }
      setIsDisabled(false);
      setRow(null);
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: 'Erreur lors de la modification',
          isOpen: true,
        });
      });
      setIsDisabled(false);
      setRow(null);
    },
  });
  const handleChangeType = (event: React.ChangeEvent<HTMLInputElement>) => {
    setType(event.target.value);
  };
  const handleChangeName = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };
  const handleChangeDescription = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDescription(event.target.value);
  };

  const handleAddGroup = () => {
    setIsDisabled(true);
    if (name.length === 0) {
      displaySnackBar(client, {
        type: 'ERROR',
        message: 'Veuillez renseigner le nom du groupe',
        isOpen: true,
      });
      return;
    }
    createUpdateGroup({
      variables: {
        input: { id: rowId, nom: name, status: checked as any, type: type as any, description },
      },
    });
  };

  return (
    <>
      <DialogContent className={classes.create}>
        {loadingCreate && <Backdrop value={'Modification en cours...'} />}
        <TextField
          variant="outlined"
          label="Nom du Groupe"
          InputLabelProps={{ shrink: true }}
          fullWidth
          value={name}
          onChange={handleChangeName}
        />

        <TextField
          variant="outlined"
          label="Description"
          InputLabelProps={{ shrink: true }}
          fullWidth
          multiline
          value={description}
          onChange={handleChangeDescription}
          rows={4}
        />

        <TextField
          variant="outlined"
          label="Type"
          InputLabelProps={{ shrink: true }}
          select
          value={type}
          fullWidth
          onChange={handleChangeType}
        >
          <MenuItem button value="PHARMACIE">
            PHARMACIE
          </MenuItem>
          <MenuItem button value="DEPARTEMENT">
            DEPARTEMENT
          </MenuItem>
          <MenuItem button value="REGION">
            REGION
          </MenuItem>
          <MenuItem button value="PERSONNALISE">
            PERSONNALISE
          </MenuItem>
        </TextField>

        <FormControl component="fieldset">
          <RadioGroup
            row
            aria-label="active"
            name="active1"
            value={checked}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setChecked(event.target.value);
            }}
          >
            <FormControlLabel
              value="ACTIVER"
              control={<Radio className={classes.radio} />}
              label="Activer"
            />
            <FormControlLabel
              value="BLOQUER"
              control={<Radio className={classes.radio} />}
              label="Bloquer"
            />
          </RadioGroup>
        </FormControl>
        <Box textAlign="center">
          <Button
            className={classes.button}
            variant="contained"
            color="secondary"
            onClick={handleAddGroup}
            disabled={isDisabled}
          >
            Modifier
          </Button>
        </Box>
      </DialogContent>
    </>
  );
};

export default EditGroupe;
