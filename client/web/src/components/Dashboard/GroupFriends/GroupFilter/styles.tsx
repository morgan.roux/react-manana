import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatarFilterRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: '50px 200px',
    },
    avatarFilterTitle: {
      color: '#616161',
      fontFamily: 'Roboto',
      fontSize: 22,
      width: 280,
      textAlign: 'center',
      marginBottom: 35,
    },
    avatarFilterFormContainer: {
      width: 445,
      '& > div': {
        marginBottom: 40,
      },
    },
    avatarFilterBtnsContainer: {
      width: '100%',
      display: 'flex',
      justifyContent: 'space-between',
      margin: '0px !important',
      '& > button': {
        height: 50,
        width: 200,
      },
    },
  }),
);

export default useStyles;
