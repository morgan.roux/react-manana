import React, { FC, useState, ChangeEvent } from 'react';
import { withRouter } from 'react-router-dom';
import { Button } from '@material-ui/core';
import useStyles from './styles';
import { useApolloClient } from '@apollo/react-hooks';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import { getGroupement } from '../../../../services/LocalStorage';
import { APP_SSO_URL } from '../../../../config';
import axios from 'axios';

const MajHelloId: FC = () => {
  const classes = useStyles({});
  const client = useApolloClient();
  const groupement = getGroupement();

  const [loadingUpdate, setLoadingUpdate] = useState<boolean>(false);

  const crud = () => {
    const groupementInfo = {
      groupementId: (groupement && groupement.id) || '',
    };
    setLoadingUpdate(true);
    axios
      .post(`${APP_SSO_URL}/create-update-allusers-helloid`, groupementInfo)
      .then(() => {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Mise à jour terminéé.`,
          isOpen: true,
        };
        setLoadingUpdate(false);
        displaySnackBar(client, snackBarData);
      })
      .catch(() => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: `Il y a une erreur dans la mise à jour.`,
          isOpen: true,
        };
        setLoadingUpdate(false);
        displaySnackBar(client, snackBarData);
      });
  };

  const handleSubmit = (event: ChangeEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    crud();
  };

  return (
    <>
      {loadingUpdate && <Backdrop value="Mis à jour d'helloID en cours, veuillez patienter..." />}
      <div className={classes.root}>
        <h3>METTRE A JOUR HELLOID</h3>

        <Button
          className={classes.btn}
          variant="contained"
          color="secondary"
          onClick={handleSubmit}
          disabled={false}
        >
          Mettre à jour
        </Button>
      </div>
    </>
  );
};
export default withRouter(MajHelloId);
