import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    flexWrap: 'nowrap',
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    padding: '20px 50px',
    paddingTop: 200
  },
  leftOperation: {
    minWidth: 280,
    padding: 24,
    borderRight: '2px solid #E5E5E5',
    height: 'calc(100vh - 86px)',
    overflowY: 'auto'
  },
  btn: {
    width: 'fit-content',
  },
  input: {
    '& > div': {
      width: 460,
      minWidth: 300,
      marginTop: 16,
    },
  },
}));

export default useStyles;
