import React from 'react';
import useStyles from './styles';

const Marche = () => {
  const classes = useStyles({});
  return (
    <div className={classes.marcheRoot}>
      <h1>Marche</h1>
    </div>
  );
};

export default Marche;
