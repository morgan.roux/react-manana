import { StatusInterface, TypeOption } from './interface';
import { MarcheStatus, MarcheType, PromotionType } from '../../../types/graphql-global-types';

// MARCHE
export const MARCHE_BASE_URL = '/marche';
export const MARCHE_CREATE_BASE_URL = `${MARCHE_BASE_URL}/create`;
export const MARCHE_EDIT_BASE_URL = `${MARCHE_BASE_URL}/edit`;

export const MARCHE_CREATE_FIRST_STEP_URL = `${MARCHE_CREATE_BASE_URL}/base-infos`;
export const MARCHE_CREATE_SECOND_STEP_URL_ARTI = `${MARCHE_CREATE_BASE_URL}/articles`;
export const MARCHE_CREATE_SECOND_STEP_URL_LABO = `${MARCHE_CREATE_BASE_URL}/laboratoires`;
export const MARCHE_CREATE_THIRD_STEP_URL = `${MARCHE_CREATE_BASE_URL}/groups-client`;

export const MARCHE_EDIT_FIRST_STEP_URL = `${MARCHE_EDIT_BASE_URL}/base-infos`;
export const MARCHE_EDIT_SECOND_STEP_URL_ARTI = `${MARCHE_EDIT_BASE_URL}/articles`;
export const MARCHE_EDIT_SECOND_STEP_URL_LABO = `${MARCHE_EDIT_BASE_URL}/laboratoires`;
export const MARCHE_EDIT_THIRD_STEP_URL = `${MARCHE_EDIT_BASE_URL}/groups-client`;

export const FIRST_STEP_KEY_URL = '/base-infos';
export const SECOND_STEP_KEY_URL_ARTI = '/articles';
export const SECOND_STEP_KEY_URL_LABO = '/laboratoires';
export const THIRD_STEP_KEY_URL = '/groups-client';

// PROMO
export const PROMO_BASE_URL = '/promotion';
export const PROMO_CREATE_BASE_URL = `${PROMO_BASE_URL}/create`;
export const PROMO_EDIT_BASE_URL = `${PROMO_BASE_URL}/edit`;

export const PROMO_CREATE_FIRST_STEP_URL = `${PROMO_CREATE_BASE_URL}/base-infos`;
export const PROMO_CREATE_SECOND_STEP_URL_ARTI = `${PROMO_CREATE_BASE_URL}/articles`;
export const PROMO_CREATE_SECOND_STEP_URL_LABO = `${PROMO_CREATE_BASE_URL}/laboratoires`;
export const PROMO_CREATE_THIRD_STEP_URL = `${PROMO_CREATE_BASE_URL}/groups-client`;

export const PROMO_EDIT_FIRST_STEP_URL = `${PROMO_EDIT_BASE_URL}/base-infos`;
export const PROMO_EDIT_SECOND_STEP_URL_ARTI = `${PROMO_EDIT_BASE_URL}/articles`;
export const PROMO_EDIT_SECOND_STEP_URL_LABO = `${PROMO_EDIT_BASE_URL}/laboratoires`;
export const PROMO_EDIT_THIRD_STEP_URL = `${PROMO_EDIT_BASE_URL}/groups-client`;

export const MARCHE_TYPES: TypeOption[] = [
  { id: 1, code: MarcheType.ARTICLE, libelle: 'ARTICLE' },
  { id: 2, code: MarcheType.LABORATOIRE, libelle: 'LABORATOIRE' },
];

export const STATUS_LIST: StatusInterface[] = [
  { id: 1, code: MarcheStatus.NON_ACTIF, libelle: 'NON ACTIF' },
  { id: 2, code: MarcheStatus.ACTIF, libelle: 'ACTIF' },
  { id: 2, code: MarcheStatus.CLOTURE, libelle: 'CLOTURÉ' },
];

export const PROMOTION_TYPES: TypeOption[] = [
  { id: 1, code: PromotionType.OFFRE_DU_MOIS, libelle: 'OFFRE DU MOIS' },
  { id: 2, code: PromotionType.BONNE_AFFAIRE, libelle: 'BONNE AFFAIRE' },
  { id: 2, code: PromotionType.LIQUIDATION, libelle: 'LIQUIDATION' },
  { id: 2, code: PromotionType.AUTRE_PROMOTION, libelle: 'AUTRE PROMOTION' },
];
