import React, { Dispatch, SetStateAction, FC, ChangeEvent } from 'react';
import { Box } from '@material-ui/core';
import { SELECTION_OPTIONS } from '../../../../Common/CustomTableWithSearch/constant';
import CustomSelect from '../../../../Common/CustomSelect';

interface SelectionFilterProps {
  value: string;
  setValue: Dispatch<SetStateAction<string>>;
}

const SelectionFilter: FC<SelectionFilterProps> = ({ value, setValue }) => {
  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      setValue(e.target.value);
    }
  };

  return (
    <Box minWidth="250px">
      <CustomSelect
        label="Sélection"
        list={SELECTION_OPTIONS}
        listId="code"
        index="label"
        value={value}
        onChange={handleChange}
      />
    </Box>
  );
};

export default SelectionFilter;
