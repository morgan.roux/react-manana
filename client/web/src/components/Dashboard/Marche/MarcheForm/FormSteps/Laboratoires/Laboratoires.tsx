import React, { FC, useState, useMemo } from 'react';
import useStyles from '../styles';
import { MarcheStepProps, StepTableProps } from '../../../interface';
import BaseInfoResume from '../BaseInformations/BaseInfoResume';
import { CustomTableWithSearchColumn } from '../../../../../Common/CustomTableWithSearch/interface';
import CustomTableWithSearch from '../../../../../Common/CustomTableWithSearch';
import { Typography, Switch } from '@material-ui/core';
import { find, uniqBy } from 'lodash';
import { MarcheLaboratoireInput } from '../../../../../../types/graphql-global-types';
import WithSearch from '../../../../../Common/newWithSearch/withSearch';
import { GET_SEARCH_CUSTOM_CONTENT_LABORATOIRE } from '../../../../../../graphql/Laboratoire/query';
import CustomContent from '../../../../../Common/newCustomContent';
// import SelectionFilter from '../SelectionFilter';

const Laboratoires: FC<MarcheStepProps & StepTableProps> = ({
  state,
  setState,
  selected,
  setSelected,
  isOnEdit,
  // isOnMarche,
  // isOnPromo,
  // isOnCreate,
}) => {
  const classes = useStyles({});

  const [data, setData] = useState<any[]>([]);

  // const [selectionValue, setSelectionValue] = useState('all');

  const { laboratoires } = state;
  const [formattedLabo, setFormattedLabo] = useState<MarcheLaboratoireInput[]>(laboratoires || []);
  // const selectedIds = selected.map(item => item && item.id);
  // const idsLabs = laboratoires.map(item => item && item.idLaboratoire);

  const columns: any[] = [
    {
      name: 'nomLabo',
      label: 'Nom',
      numeric: false,
      disablePadding: false,
      renderer: (value: any) => {
        return (value && value.nomLabo) || '-';
      },
    },
    {
      name: 'laboSuite.adresse',
      label: 'Adresse',
      numeric: false,
      disablePadding: false,
      renderer: (value: any) => {
        return (value && value.laboSuite && value.laboSuite.adresse) || '-';
      },
    },
    {
      name: 'laboSuite.codePostal',
      label: 'Code Postal',
      numeric: false,
      disablePadding: false,
      renderer: (value: any) => {
        return (value && value.laboSuite && value.laboSuite.codePostal) || '-';
      },
    },
    {
      name: 'laboSuite.ville',
      label: 'Ville',
      numeric: false,
      disablePadding: false,
      renderer: (value: any) => {
        return (value && value.laboSuite && value.laboSuite.ville) || '-';
      },
    },
    {
      name: 'laboSuite.telephone',
      label: 'Téléphone',
      numeric: false,
      disablePadding: false,
      renderer: (value: any) => {
        return (value && value.laboSuite && value.laboSuite.telephone) || '-';
      },
    },
    {
      name: '',
      label: 'Obligatoire',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        const labo = find(formattedLabo, ['idLaboratoire', row.id]);
        const val = labo && labo.obligatoire;
        return val ? 'OUI' : 'NON';
      },
    },
    {
      name: '',
      label: '',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        const labo = find(formattedLabo, ['idLaboratoire', row.id]);
        const val = labo && labo.obligatoire;
        return (
          <Switch
            checked={val !== undefined ? val : false}
            value={val !== undefined ? val : false}
            onClick={handleSwitch(row)}
            inputProps={{ 'aria-label': `${row.nom}_switch` }}
            size="medium"
          />
        );
      },
    },
  ];

  console.log('formattedLabo : ', formattedLabo);

  /**
   * Set default formattedLabo
   */
  useMemo(() => {
    if (data && data.length > 0) {
      const labos: MarcheLaboratoireInput[] = [];
      let isReqired: boolean = false;
      data.map(d => {
        if (isOnEdit) {
          const lab = laboratoires.find(item => item && d && d.id === item.idLaboratoire);
          isReqired = lab ? lab.obligatoire : false;
        }
        const labo: MarcheLaboratoireInput = { idLaboratoire: d.id, obligatoire: isReqired };
        labos.push(labo);
      });

      const newFormattedLabo: MarcheLaboratoireInput[] = [...labos, ...formattedLabo];
      setFormattedLabo(uniqBy(newFormattedLabo, 'idLaboratoire'));
    }
  }, [data, isOnEdit]);

  const tableProps = {
    selected,
    setSelected,
    isSelectable: true,
    paginationCentered: true,
    columns,
  };

  /**
   * Set state
   */
  useMemo(() => {
    if (selected.length > 0) {
      const labos: MarcheLaboratoireInput[] = [];
      // formattedLabo.map(lab => {
      //   if (selected.some(item => lab && item && lab.idLaboratoire === item.id)) {
      //     labos.push(lab);
      //   }
      // });

      selected.map(s => {
        const formatted = formattedLabo.find(f => s && f && s.id === f.idLaboratoire);
        if (formatted) {
          labos.push(formatted);
        } else {
          labos.push({ idLaboratoire: s.id, obligatoire: false });
        }
      });

      console.log('labos : ', labos);
      const labs = laboratoires.filter(l => selected.some(s => l && s && l.idLaboratoire === s.id));

      const newLabos: MarcheLaboratoireInput[] = uniqBy([...labos, ...labs], 'idLaboratoire');
      setState(prevState => ({ ...prevState, laboratoires: newLabos }));
    } else {
      setState(prevState => ({ ...prevState, laboratoires: [] }));
    }
  }, [selected, formattedLabo]);

  const handleSwitch = (row: any) => () => {
    const labo = find(formattedLabo, ['idLaboratoire', row.id]);
    const val = (labo && labo.obligatoire) || false;
    const newLabo: MarcheLaboratoireInput = {
      idLaboratoire: row.id,
      obligatoire: val !== undefined ? !val : false,
    };

    const newLabos = formattedLabo.filter(item => item.idLaboratoire !== row.id);

    setFormattedLabo([...[newLabo], ...newLabos]);
  };

  return (
    <div className={classes.formStepRoot}>
      <Typography className={classes.tableTitle}>Sélection des laboratoires rattachés</Typography>
      <div className={classes.resumeAndSelection}>
        <BaseInfoResume data={state} />
        {/* <SelectionFilter value={selectionValue} setValue={setSelectionValue} /> */}
      </div>
      <div className={classes.tableSection}>
        <WithSearch
          type="laboratoire"
          props={tableProps}
          WrappedComponent={CustomContent}
          searchQuery={GET_SEARCH_CUSTOM_CONTENT_LABORATOIRE}
          optionalMust={[]}
        />
      </div>
    </div>
  );
};

export default Laboratoires;
