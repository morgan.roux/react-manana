import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formStepRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      // alignItems: 'center',
      justifyContent: 'center',
      padding: '0px 24px',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    tableTitle: {
      fontSize: 20,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      marginBottom: 15,
    },
    resumeAndSelection: {
      width: '100%',
      display: 'flex',
      '& > div:nth-child(2)': {
        marginLeft: 25,
      },
    },
    tableSection: {
      width: '100%',
      margin: '0px 0px 25px',
      '& > div > div': {
        padding: '24px 0px',
      },
    },
  }),
);

export default useStyles;
