import React, { ChangeEvent, Dispatch, SetStateAction, FC, useState, MouseEvent } from 'react';
import useStyles from './styles';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import CustomButton from '../../../../Common/CustomButton';
import { Tooltip, Fade, IconButton } from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import SimpleCustomTable, {
  SimpleCustomTableColumn,
} from '../../../../Common/SimpleCustomTable/SimpleCustomTable';
import { RemiseDetailInput } from '../../../../../types/graphql-global-types';
import { v4 as uuidv4 } from 'uuid';
import { DataInterface } from '../../interface';
import BaseInfoResume from '../FormSteps/BaseInformations/BaseInfoResume';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { useApolloClient } from '@apollo/react-hooks';

export interface PalierProps {
  codeGroupeClient: number;
  state: PalierState;
  setState: Dispatch<SetStateAction<PalierState>>;
  parentData: DataInterface;
}

export interface RemiseDetailInterface extends RemiseDetailInput {
  id: string | null;
}
export interface PalierState {
  [key: number]: { remiseDetails: RemiseDetailInterface[] };
}

const Palier: FC<PalierProps> = ({ state, setState, codeGroupeClient, parentData }) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);

  const defaultFormState: RemiseDetailInterface = {
    id: null,
    quantiteMin: null,
    quantiteMax: null,
    pourcentageRemise: null,
    remiseSupplementaire: null,
  };

  const [formState, setFormState] = useState<RemiseDetailInterface>(defaultFormState);

  const { quantiteMin, quantiteMax, pourcentageRemise, remiseSupplementaire } = formState;

  const columns: SimpleCustomTableColumn[] = [
    {
      key: 'quantiteMin',
      label: 'Quantité Minimale',
      numeric: true,
      disablePadding: false,
    },
    {
      key: 'quantiteMax',
      label: 'Quantité Maximale',
      numeric: true,
      disablePadding: false,
    },
    {
      key: 'pourcentageRemise',
      label: 'Remise sur Facture (%)',
      numeric: true,
      disablePadding: false,
    },
    {
      key: 'remiseSupplementaire',
      label: 'Remise Supplémentaire (%)',
      numeric: true,
      disablePadding: false,
    },
    {
      key: '',
      label: '',
      numeric: false,
      disablePadding: false,
      renderer: (row: any) => {
        return (
          <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Supprimer">
            <IconButton color="primary" onClick={onClickDelete(row)}>
              <Delete />
            </IconButton>
          </Tooltip>
        );
      },
    },
  ];

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setFormState(prevState => ({ ...prevState, [name]: value }));
    }
  };

  const onClickAdd = () => {
    if (quantiteMin && quantiteMax && pourcentageRemise && remiseSupplementaire) {
      if (parseFloat(quantiteMin.toString()) >= parseFloat(quantiteMax.toString())) {
        displaySnackBar(client, {
          type: 'ERROR',
          isOpen: true,
          message: 'La Quantité Minimale doit être inférieure à la Quantité Maximale',
        });
        return;
      }

      const remiseDetail: RemiseDetailInterface = {
        id: uuidv4(),
        quantiteMin,
        quantiteMax,
        pourcentageRemise,
        remiseSupplementaire,
      };

      if (state[codeGroupeClient]) {
        const prev = state[codeGroupeClient].remiseDetails;
        setState(prevState => ({
          ...prevState,
          [codeGroupeClient]: { remiseDetails: [...prev, ...[remiseDetail]] },
        }));
      } else {
        setState(prevState => ({
          ...prevState,
          [codeGroupeClient]: { remiseDetails: [remiseDetail] },
        }));
      }

      // Init form
      setFormState(defaultFormState);
    } else {
      displaySnackBar(client, {
        type: 'ERROR',
        isOpen: true,
        message: 'Veuillez remplir tous les champs',
      });
    }
  };

  console.log('formState ::-->> ', formState);

  const data = (state[codeGroupeClient] && state[codeGroupeClient].remiseDetails) || [];

  const onClickDelete = (row: any) => (event: MouseEvent<any>) => {
    event.preventDefault();
    event.stopPropagation();
    if (row) {
      const newRemiseDetails: RemiseDetailInterface[] = data.filter(
        item => item && item.id !== row.id,
      );
      // setState(prevState => ({ ...prevState, remiseDetails: newRemiseDetails }));
      setState(prevState => ({
        ...prevState,
        [codeGroupeClient]: { remiseDetails: newRemiseDetails },
      }));
    }
  };

  return (
    <div className={classes.palierRoot}>
      <BaseInfoResume data={parentData} />
      <div className={classes.palierForm}>
        <CustomFormTextField
          label="Quantité Minimale"
          name="quantiteMin"
          value={quantiteMin || ''}
          onChange={handleChange}
          type="number"
        />
        <CustomFormTextField
          label="Quantité Maximale"
          name="quantiteMax"
          value={quantiteMax || ''}
          onChange={handleChange}
          type="number"
        />
        <CustomFormTextField
          label="Remise sur Facture (%)"
          name="pourcentageRemise"
          value={pourcentageRemise || ''}
          onChange={handleChange}
          type="number"
        />
        <CustomFormTextField
          label="Remise Supplémentaire (%)"
          name="remiseSupplementaire"
          value={remiseSupplementaire || ''}
          onChange={handleChange}
          type="number"
        />
        <CustomButton color="primary" onClick={onClickAdd}>
          Ajouter
        </CustomButton>
      </div>
      <div className={classes.palierTable}>
        <SimpleCustomTable
          data={data}
          total={data.length}
          selectable={false}
          showToolbar={false}
          columns={columns}
          page={page}
          setPage={setPage}
          rowsPerPage={rowsPerPage}
          setRowsPerPage={setRowsPerPage}
        />
      </div>
    </div>
  );
};

export default Palier;
