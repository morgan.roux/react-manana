import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    marcheFormRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'flex-start',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    filterContainer: {
      borderRight: '1px solid rgba(0, 0, 0, 0.23)',
      minHeight: 'calc(100vh - 86px)',
    },
  }),
);

export default useStyles;
