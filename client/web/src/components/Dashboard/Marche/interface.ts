import { Dispatch, SetStateAction } from 'react';
import {
  MarcheStatus,
  MarcheType,
  MarcheLaboratoireInput,
  GroupeClienRemisetInput,
  PromotionType,
  PromotioStatus,
} from '../../../types/graphql-global-types';

export interface DataInterface {
  id: string | null;
  nom: string;
  dateDebut: any;
  dateFin: any;
  status: MarcheStatus | PromotioStatus;
  codeCanal: string;
  laboratoires: MarcheLaboratoireInput[];
  idsCanalArticle: string[];
  groupeClients: GroupeClienRemisetInput[];
}

export interface PromotionInterface extends DataInterface {
  promotionType: PromotionType;
}

export interface MarcheInterface extends DataInterface {
  avecPalier: boolean;
  marcheType: MarcheType;
}

export interface MarcheStepProps {
  state: MarcheInterface & PromotionInterface;
  setState: Dispatch<SetStateAction<MarcheInterface & PromotionInterface>>;
  isOnMarche: boolean;
  isOnPromo: boolean;
  isOnCreate: boolean;
  isOnEdit: boolean;
  canalList?: any[];
}

export interface StepTableProps {
  selected: any[];
  setSelected: Dispatch<SetStateAction<any[]>>;
  data: any[];
  setData: Dispatch<SetStateAction<any[]>>;
}

export interface TypeOption {
  id: number | string;
  code: MarcheType | PromotionType;
  libelle: string;
}

export interface StatusInterface {
  id: number | string;
  code: MarcheStatus | PromotionType;
  libelle: string;
}
