import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    marcheListRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'flex-start',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    filterContainer: {
      borderRight: '1px solid rgba(0, 0, 0, 0.23)',
      minHeight: 'calc(100vh - 86px)',
    },
    mainListPage: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    marcheListHead: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
      padding: 15,
      borderBottom: '1px solid rgba(0, 0, 0, 0.23)',
    },
    marcheListHeadTitle: {
      fontSize: 20,
      fontWeight: 'bold',
    },
    marcheListTable: {
      width: '100%',
      padding: 15,
    },
  }),
);

export default useStyles;
