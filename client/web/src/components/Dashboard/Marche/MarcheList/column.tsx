import moment from 'moment';

export const getColumns = (type: string) => {
  switch (type) {
    case 'date':
      return [
        {
          name: 'dateDebut',
          label: 'Date de début',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return moment(row.dateDebut).format('DD/MM/YYYY');
          },
        },
        {
          name: 'dateFin',
          label: 'Date de fin',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return moment(row.dateFin).format('DD/MM/YYYY');
          },
        },
        {
          name: 'dateModification',
          label: 'Mise à jour',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return moment(row.dateModification).format('DD/MM/YYYY');
          },
        },
      ];
    case 'marche':
      return [
        {
          name: 'nom',
          label: 'Nom',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return row.nom || '-';
          },
        },
        {
          name: 'userCreated.userName',
          label: 'Nom créateur',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return (row.userCreated && row.userCreated.userName) || '-';
          },
        },
        {
          name: 'commandeCanal.libelle',
          label: 'Canal',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return (row.commandeCanal && row.commandeCanal.libelle) || '-';
          },
        },
        {
          name: 'marcheType',
          label: 'Type',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return (row.marcheType && row.marcheType.replace(/[_]/g, ' ')) || '-';
          },
        },
        {
          name: 'avecPalier',
          label: 'Avec palier',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return row.avecPalier ? 'OUI' : row.avecPalier === false ? 'NON' : '-';
          },
        },
        {
          name: 'nbAdhesion',
          label: 'Nbre. Adhésion',
          numeric: true,
          disablePadding: false,
          renderer: (row: any) => {
            return row.nbAdhesion || '-';
          },
        },
        {
          name: 'nbPalier',
          label: 'Nbre. Paliers',
          numeric: true,
          disablePadding: false,
          renderer: (row: any) => {
            return row.nbPalier || '0';
          },
        },
        {
          name: 'status',
          label: 'Statut',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return (row.status && row.status.replace(/[-_]/g, ' ')) || '-';
          },
        },
      ];
    case 'promotion':
      return [
        {
          name: 'nom',
          label: 'Nom',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return row.nom || '-';
          },
        },
        {
          name: 'userCreated.userName',
          label: 'Nom créateur',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return (row.userCreated && row.userCreated.userName) || '-';
          },
        },
        {
          name: 'promotionType',
          label: 'Type',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return (row.promotionType && row.promotionType.replace(/[-_]/g, ' ')) || '-';
          },
        },
        {
          name: 'commandeCanal.libelle',
          label: 'Canal',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return (row.commandeCanal && row.commandeCanal.libelle) || '-';
          },
        },
        {
          name: 'promoStatus',
          label: 'Statut',
          numeric: false,
          disablePadding: false,
          renderer: (row: any) => {
            return (row.promoStatus && row.promoStatus.replace(/[-_]/g, ' ')) || '-';
          },
        },
      ];
    default:
      return [];
  }
};
