import React, { FC, useState, useEffect, Fragment } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
//import Stepper from './stepProduitt';
import InfoGenerale from './Steps/InfoGenerale';
import { useMutation, useApolloClient, useQuery } from '@apollo/react-hooks';
import {
  createUpdateArticle,
  createUpdateArticleVariables,
} from '../../../../graphql/Produit/types/createUpdateArticle';
import { CREATE_UPDATE_ARTICLE } from '../../../../graphql/Produit/mutation';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import {
  CREATE_PUT_PESIGNED_URL,
  CREATE_PUT_PESIGNED_URLVariables,
} from '../../../../graphql/S3/types/CREATE_PUT_PESIGNED_URL';
import { DO_CREATE_PUT_PESIGNED_URL } from '../../../../graphql/S3';
import { ProduitCanalInput, TypeFichier } from '../../../../types/graphql-global-types';
import { formatFilename } from '../../../../utils/filenameFormater';
import { uploadToS3 } from '../../../../services/S3';
import { Loader } from '../../Content/Loader';
import CanauxCommandes from './Steps/CanauxCommandes';
import { CanalArticleInterface } from './Interface/CanalArticleInterface';
import { GET_ARTICLE } from '../../../../graphql/Produit/query';
import { PRODUIT, PRODUITVariables } from '../../../../graphql/Produit/types/PRODUIT';
import Backdrop from '../../../Common/Backdrop';
import ConfirmDialog from '../../../Common/ConfirmDialog';
import { CustomFullScreenModal } from '@app/ui-kit';
import Stepper from '../../../Common/Stepper';

interface CreationProduitProps {
  match: { params: { id: string } };
  setOpenFormModal: (open: boolean) => void;
  openFormModal: boolean;
  refetch: any;
}

const CreationProduit: FC<CreationProduitProps & RouteComponentProps> = ({
  match: {
    params: { id },
  },
  setOpenFormModal,
  openFormModal,
  refetch,
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const [nextBtnDisabled, setNextBtnDisabled] = useState<boolean>(true);

  const title = id ? 'Modification de produit' : 'Ajout de produit';
  const initialValues = {
    isGenerique: 0,
    unitePetitCond: 0,
    pxVente: 0,
    pxAchat: 0,
    codeFamille: '',
    codeTauxss: 0,
    dateSuppression: null,
    fichierImage: null,
    selectedFiles: [],
  };
  const [values, setValues] = useState<any>(initialValues);

  const initValues = (article: any) => {
    return {
      idProduit: article.id,
      dateSuppression: article.supprimer,
      pxVente: article.produitTechReg?.prixVenteTTC,
      pxAchat: article.produitTechReg?.prixAchatHT,
      libelle: article.libelle,
      codeReference: article.produitCode?.code,
      typeCodeReferent: article.produitCode?.typeCode,
      codeTva: article.produitTechReg?.tva?.codeTva,
      codeLibelleStockage: article.produitTechReg?.libelleStockage?.codeInfo,
      acte: article.produitTechReg?.acte,
      codeActe: article.produitTechReg?.acte?.codeActe,
      codeListe: article.produitTechReg?.liste?.codeListe,
      codeTauxss: article.produitTechReg?.tauxss?.codeTaux,
      idCategorie: article.categorie?.resipIdCategorie,
      laboTitulaire: article.produitTechReg?.laboTitulaire,
      laboExploitant: article.produitTechReg?.laboExploitant,
      laboDistributaire: article.produitTechReg?.laboDistirbuteur,
      idLaboTitulaire: article.produitTechReg?.laboTitulaire?.id,
      idLaboExploitant: article.produitTechReg?.laboExploitant?.id,
      idLaboDistributaire: article.produitTechReg?.laboDistirbuteur?.id,
      famille: article.famille,
      codeFamille: article.famille?.codeFamille,
      fichierImage:
        (article.produitPhoto?.fichier && {
          type: TypeFichier.PHOTO,
          nomOriginal: article.produitPhoto.fichier.nomOriginal,
          chemin: article.produitPhoto.fichier.chemin,
        }) ||
        null,
      selectedFiles:
        (article.produitPhoto?.fichier && [
          {
            type: 'image/png',
            name: article.produitPhoto.fichier.nomOriginal,
            publicUrl: article.produitPhoto.fichier.publicUrl,
          },
        ]) ||
        [],
      produitCanaux:
        article.canauxArticle &&
        article.canauxArticle.map(canalArticle => ({
          ...canalArticle,
          codeCommandeCanal:
            canalArticle && canalArticle.commandeCanal && canalArticle.commandeCanal.code,
          gammeCommercial:
            canalArticle &&
            canalArticle.sousGammeCommercial &&
            canalArticle.sousGammeCommercial.gammeCommercial,
          idCanalSousGamme:
            canalArticle && canalArticle.sousGammeCommercial && canalArticle.sousGammeCommercial.id,
          gammeCatalogue:
            canalArticle &&
            canalArticle.sousGammeCatalogue &&
            canalArticle.sousGammeCatalogue.gammeCatalogue,
          codeSousGammeCatalogue:
            canalArticle &&
            canalArticle.sousGammeCatalogue &&
            canalArticle.sousGammeCatalogue.codeSousGamme,
        })),
    };
  };

  const loadingProduit = useQuery<PRODUIT, PRODUITVariables>(GET_ARTICLE, {
    variables: {
      id,
    },
    onCompleted: data => {
      if (data && data.produit) {
        const values = initValues(data.produit);
        setValues(values);
      }
    },
    skip: !id ? true : false,
    fetchPolicy: 'cache-and-network',
  });

  const getVarialbesValues = (allValues: any) => {
    let variables = allValues;
    delete variables.resetChange;
    delete variables.acte;
    delete variables.famille;
    delete variables.gammeCatalogue;
    delete variables.gammeCommercial;
    delete variables.laboratoire;
    delete variables.selectedFiles;
    delete variables.sousGammeCatalogue;
    delete variables.sousGammeCommercial;
    delete variables.activeCommandeCanal;
    delete variables.laboDistributaire;
    delete variables.laboExploitant;
    delete variables.laboTitulaire;
    const produitCanaux =
      variables &&
      variables.produitCanaux &&
      variables.produitCanaux.map((canalArticle: ProduitCanalInput & CanalArticleInterface) => {
        const variables: any = canalArticle;
        delete variables.laboratoire;
        delete variables.gammeCommercial;
        delete variables.sousGammeCommercial;
        delete variables.gammeCatalogue;
        delete variables.sousGammeCatalogue;
        delete variables.commandeCanal;
        delete variables.__typename;
        return variables;
      });

    variables = { ...variables, produitCanaux };
    return variables as createUpdateArticleVariables;
  };

  const [upsertProduit, upsertingProduit] = useMutation<
    createUpdateArticle,
    createUpdateArticleVariables
  >(CREATE_UPDATE_ARTICLE, {
    onCompleted: data => {
      if (data && data.createUpdateProduit) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Produit ${id ? 'modifié' : 'créée'} avec succès`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        refetch && refetch();
        setOpenFormModal(false);

        // // articleResult.refetch();
        // if (id) {
        //   openCloseModal(false);
        // }

        // setIsFinale(true);
        // if (isEdit) setIsEdit(false);
        // if (setIsAfterCreateUpdate) setIsAfterCreateUpdate(true);
      }
    },
    onError: errors => {
      displaySnackBar(client, {
        isOpen: true,
        type: 'ERROR',
        message: errors.message,
      });
    },
  });

  const sendFileToAWS = async (item: any, fileObject: any, typeFichier: TypeFichier) => {
    if (fileObject && fileObject.file && item && item.presignedUrl) {
      return uploadToS3(fileObject.file, item.presignedUrl).then(_ => ({
        chemin: item && item.filePath,
        nomOriginal: fileObject.file.name,
        type: typeFichier,
      }));
    }
  };

  const [createPutPresignedUrl, creatingPutPresignedUrl] = useMutation<
    CREATE_PUT_PESIGNED_URL,
    CREATE_PUT_PESIGNED_URLVariables
  >(DO_CREATE_PUT_PESIGNED_URL, {
    onCompleted: async data => {
      if (data && data.createPutPresignedUrls && data.createPutPresignedUrls.length > 0) {
        sendFileToAWS(
          data.createPutPresignedUrls[0],
          { file: values.fichierImage },
          TypeFichier.PHOTO,
        ).then(async fichierImage => {
          const variables = getVarialbesValues({ ...values, fichierImage: fichierImage });
          return upsertProduit({ variables });
        });
      }
    },
    onError: error => {
      error.graphQLErrors.map(err => {
        displaySnackBar(client, { isOpen: true, type: 'ERROR', message: err.message });
      });
    },
  });

  const createUpdateProduit = () => {
    const fichierImage = values.selectedFiles && values.selectedFiles[0];
    if (fichierImage && !fichierImage.publicUrl) {
      const filePath = formatFilename(fichierImage);
      createPutPresignedUrl({ variables: { filePaths: [filePath] } });
    } else {
      const variables = getVarialbesValues({ ...values });
      upsertProduit({ variables });
    }
  };

  const steps = [
    {
      title: 'Données économiques',
      content: (
        <InfoGenerale
          setNextBtnDisabled={setNextBtnDisabled}
          allValues={values}
          setAllValues={setValues}
        />
      ),
    },
    {
      title: 'Canaux de commandes',
      content: (
        <CanauxCommandes
          setNextBtnDisabled={setNextBtnDisabled}
          allValues={values}
          setAllValues={setValues}
          createUpdateProduit={createUpdateProduit}
        />
      ),
    },
  ];

  const finalStep = {
    buttonLabel: id ? 'MODIFIER PRODUIT' : 'AJOUTER PRODUIT',
    action: createUpdateProduit,
  };

  const handleBackBtnClick = () => {
    setOpenFormModal(false);
  };

  const isLoading = (): boolean => {
    return upsertingProduit.loading || creatingPutPresignedUrl.loading;
  };

  if (loadingProduit.loading) return <Loader />;

  return (
    <Fragment>
      {isLoading() && (
        <Backdrop value={`${id ? 'Modification' : 'Ajout'} de l'article en cours ...`} />
      )}
      <CustomFullScreenModal
        open={openFormModal}
        setOpen={setOpenFormModal}
        title={`Produit`}
        withBtnsActions={false}
      >
        {/* <Stepper
        title={title}
        steps={steps}
        backToHome={handleBackBtnClick}
        disableNextBtn={nextBtnDisabled}
        fullWidth={false}
        finalStep={finalStep}
        currentStep={currentStep}
        setCurrentStep={setCurrentStep}
        setResetChange={setResetChange}
        isFinale={isFinale}
        setIsFinale={setIsFinale}
        setAllValues={setAllValues}
      /> */}
        <Stepper
          title={title}
          steps={steps}
          backToHome={handleBackBtnClick}
          disableNextBtn={nextBtnDisabled}
          finalStep={finalStep}
        />
      </CustomFullScreenModal>
    </Fragment>
  );
};

export default withRouter(CreationProduit);
