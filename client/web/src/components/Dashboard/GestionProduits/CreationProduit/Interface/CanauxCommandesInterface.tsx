export interface CanauxCommandesInterface {
  qteStock: number;
  stv: number;
  unitePetitCond: number;
  idArticle?: string;
  pxAchatCanal: number;
  pxVentePharmacie: number;
  codeLaboratoire: string;
  laboratoire: any;
  datePeremption: any;
  dateCreation: any;
  dateModification: any;
  dateRetrait: any;
  gammeCommercial: any;
  sousGammeCommercial: any;
  idCanalSousGamme: string;
  gammeCatalogue: any;
  sousGammeCatalogue: any;
  codeGammeSousCatalogue: string;
  promotionType: string;
  promotionDebut: any;
  promotionFin: any;
  marcheNom: string;
  marcheDebut: any;
  marcheFin: any;
}
