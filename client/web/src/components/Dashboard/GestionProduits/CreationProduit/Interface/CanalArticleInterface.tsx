import { FichierInput } from '../../../../../types/graphql-global-types';

export interface CanalArticleInterface {
  laboratoire: any;
  gammeCommercial: any;
  sousGammeCommercial: any;
  gammeCatalogue: any;
  sousGammeCatalogue: any;
}
