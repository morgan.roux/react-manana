import React, { FC, useState, useEffect } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import { SEARCH } from '../../../../../../graphql/search/query';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import usePlateformeLogistique from './usePlateformeLogistique';
import CustomSelect from '../../../../../Common/CustomSelect';
import { CustomCheckbox } from '../../../../../Common/CustomCheckbox';
import {
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  Typography,
} from '@material-ui/core';
import CustomAutocomplete from '../../../../../Common/CustomAutocomplete';
import Validator from './Validator';
import { PlateformeLogistiqueInterface } from '../../Interface/PlateformeLogistiqueInterface';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
interface InfoGeneraleProps {
  setNextBtnDisabled: (disables: boolean) => void;
  allValues: any;
  setAllValues: (values: any) => void;
}

const InfoGenerale: FC<InfoGeneraleProps & RouteComponentProps> = ({
  history,
  setNextBtnDisabled,
  allValues,
  setAllValues,
}) => {
  const classes = useStyles({});
  const {
    handleChange,
    handleChangeDate,
    handleChangeAutocomplete,
    values,
  } = usePlateformeLogistique({
    ...allValues,
  } as PlateformeLogistiqueInterface);
  const {
    qteStock,
    stv,
    unitePetitCond,
    pxVenteArticle,
    pxAchatArticle,
    datePeremption,
    dateCreation,
    dateModification,
    gammeCommercial,
    sousGammeCommercial,
    gammeCatalogue,
    sousGammeCatalogue,
  } = values;

  const gammeCommercialResult = useQuery(SEARCH, { variables: { type: ['canalgamme'] } });
  const gammeCatalogueResult = useQuery(SEARCH, { variables: { type: ['gammecatalogue'] } });

  useEffect(() => {
    if (values) {
      setNextBtnDisabled(Validator(values));
      setAllValues(prevState => ({ ...prevState, ...values }));
    }
  }, [values]);

  return (
    <div className={classes.root}>
      <div className={classes.container}>
        <form>
          <Typography className={classes.subTitle}>Informations générales</Typography>
          <div className={classes.inlineContainer}>
            <CustomFormTextField
              label="Stock"
              placeholder="Stock"
              name="qteStock"
              value={qteStock}
              onChange={handleChange}
              fullWidth={true}
            />
            <CustomFormTextField
              label="Standard de vente"
              placeholder="Standard de vente"
              name="stv"
              value={stv}
              onChange={handleChange}
              fullWidth={true}
            />

            <CustomFormTextField
              label="Nbre de produit par carton"
              placeholder="Nbre de produit par carton"
              name="unitePetitCond"
              value={unitePetitCond}
              onChange={handleChange}
              fullWidth={true}
            />
          </div>
          <div className={classes.inlineContainer}>
            <FormControl fullWidth variant="outlined">
              <InputLabel htmlFor="outlined-pxAchatArticle">Prix achat PF</InputLabel>
              <OutlinedInput
                id="outlined-pxAchatArticle"
                value={pxAchatArticle}
                onChange={handleChange}
                name="pxAchatArticle"
                endAdornment={<InputAdornment position="end">€</InputAdornment>}
                labelWidth={110}
              />
            </FormControl>
            <FormControl fullWidth variant="outlined">
              <InputLabel htmlFor="outlined-pxVenteArticle">Prix vente pharmacien</InputLabel>
              <OutlinedInput
                id="outlined-pxVenteArticle"
                value={pxVenteArticle}
                onChange={handleChange}
                name="pxVenteArticle"
                endAdornment={<InputAdornment position="end">€</InputAdornment>}
                labelWidth={180}
              />
            </FormControl>
          </div>
          <div className={classes.inlineContainer}>
            <CustomFormTextField
              label="Laboratoire partenaire"
              placeholder="Laboratoire partenaire"
              fullWidth={true}
            />
            <CustomDatePicker
              label="Date de péremption"
              placeholder="Date de péremption"
              onChange={handleChangeDate('datePeremption')}
              name="datePeremption"
              value={datePeremption}
              format=""
              disablePast={false}
            />
          </div>
          <Typography className={classes.subTitle}>Dates</Typography>
          <div className={classes.inlineContainer}>
            <CustomDatePicker
              label="Création"
              placeholder="Création"
              onChange={handleChangeDate('dateCreation')}
              name="dateCreation"
              value={dateCreation}
              format=""
              disablePast={false}
            />
            <CustomDatePicker
              label="MAJ"
              placeholder="MAJ"
              onChange={handleChangeDate('dateModification')}
              name="dateModification"
              value={dateModification}
              format=""
              disablePast={false}
            />
          </div>
          <Typography className={classes.subTitle}>Gamme</Typography>
          <div className={classes.inlineContainer}>
            {/* <CustomAutocomplete
              id="gammeId"
              options={
                (gammeCommercialResult &&
                  gammeCommercialResult.data &&
                  gammeCommercialResult.data.search &&
                  gammeCommercialResult.data.search.data) ||
                []
              }
              optionLabelKey="libelle"
              value={gammeCommercial}
              onChange={(e, inputValue) =>
                handleChangeAutocomplete(
                  e,
                  inputValue,
                  'gammeCommercial',
                  'libelle',
                  'GammeCommercial',
                )
              }
              label="Gamme"
            />
            <CustomAutocomplete
              id="sousGammeId"
              options={(gammeCommercial && gammeCommercial.sousGamme) || []}
              optionLabelKey="libelle"
              value={sousGammeCommercial}
              onChange={(e, inputValue) =>
                handleChangeAutocomplete(
                  e,
                  inputValue,
                  'sousGammeCommercial',
                  'libelle',
                  'SousGammeCommercial',
                  'idCanalSousGamme',
                  'id',
                  true,
                )
              }
              label="Sous gamme"
              disabled={!gammeCommercial}
            /> */}
          </div>
          <Typography className={classes.subTitle}>Catalogue</Typography>
          <div className={classes.inlineContainer}>
            {/* <CustomAutocomplete
              id="catalogeId"
              options={
                (gammeCatalogueResult &&
                  gammeCatalogueResult.data &&
                  gammeCatalogueResult.data.search &&
                  gammeCatalogueResult.data.search.data) ||
                []
              }
              optionLabelKey="libelle"
              value={gammeCatalogue}
              onChange={(e, inputValue) =>
                handleChangeAutocomplete(
                  e,
                  inputValue,
                  'gammeCatalogue',
                  'libelle',
                  'GammeCatalogue',
                )
              }
              label="Gamme catalogue"
            />
            <CustomAutocomplete
              id="sousCatalogueId"
              options={(gammeCatalogue && gammeCatalogue.sousGammes) || []}
              optionLabelKey="libelle"
              value={sousGammeCatalogue}
              onChange={(e, inputValue) =>
                handleChangeAutocomplete(
                  e,
                  inputValue,
                  'sousGammeCatalogue',
                  'libelle',
                  'SousGammeCatalogue',
                  'codeSousGammeCatalogue',
                  'codeGammeSous',
                )
              }
              label="Sous gamme catalogue"
              disabled={!gammeCatalogue}
            /> */}
          </div>
        </form>
      </div>
    </div>
  );
};

export default withRouter(InfoGenerale);
