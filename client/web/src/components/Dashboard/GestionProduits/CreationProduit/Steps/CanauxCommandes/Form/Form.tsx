import React, { FC, useState, useEffect } from 'react';
import useStyles from './styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { useQuery } from '@apollo/react-hooks';
import {
  FormControl,
  InputLabel,
  OutlinedInput,
  InputAdornment,
  Typography,
  Box,
} from '@material-ui/core';
import useForm from './useForm';
import { CustomFormTextField, CustomTextField } from '../../../../../../Common/CustomTextField';
import { SEARCH } from '../../../../../../../graphql/search/query';
import CustomAutocomplete from '../../../../../../Common/CustomAutocomplete';
import { CustomDatePicker } from '../../../../../../Common/CustomDateTimePicker';
import { ProduitCanalInput } from '../../../../../../../types/graphql-global-types';
import { CanalArticleInterface } from '../../../Interface/CanalArticleInterface';
import { CustomCheckbox } from '../../../../../../Common/CustomCheckbox';
import ConfirmDialog from '../../../../../../Common/ConfirmDialog';
interface FormProps {
  tab: number;
  setNextBtnDisabled: (disables: boolean) => void;
  allValues: any;
  setAllValues: (values: any) => void;
  canalArticle: ProduitCanalInput & CanalArticleInterface;
  disabled?: boolean;
  createUpdateProduit: () => void;
}

const Form: FC<FormProps & RouteComponentProps> = ({
  history,
  setNextBtnDisabled,
  allValues,
  setAllValues,
  tab,
  canalArticle,
  disabled,
  createUpdateProduit,
}) => {
  const classes = useStyles({});
  const {
    handleChange,
    activeCanal,
    handleChangeAutocomplete,
    handleChangeDate,
    setValues,
    values,
  } = useForm(canalArticle, allValues);
  const [openModal, setOpenaModal] = useState<boolean>(false);
  const laboResult = useQuery(SEARCH, { variables: { type: ['laboratoire'] } });
  const gammeCommercialResult = useQuery(SEARCH, { variables: { type: ['canalgamme'] } });
  const gammeCatalogueResult = useQuery(SEARCH, { variables: { type: ['gammecatalogue'] } });
  const [desactive, setDesactive] = useState<boolean>(false);

  useEffect(() => {
    setAllValues(prevState => ({
      ...prevState,
      produitCanaux:
        prevState &&
        prevState.produitCanaux &&
        prevState.produitCanaux.map((canalArticle: CanalArticleInterface & ProduitCanalInput) => {
          if (
            canalArticle &&
            allValues &&
            allValues.activeCommandeCanal &&
            canalArticle.codeCommandeCanal === allValues.activeCommandeCanal.code
          ) {
            return canalArticle.gammeCommercial &&
              values.gammeCommercial &&
              canalArticle.gammeCommercial.id !== values.gammeCommercial.id
              ? { ...values, idCanalSousGamme: null, sousGammeCommercial: null }
              : canalArticle.gammeCatalogue &&
                values.gammeCatalogue &&
                canalArticle.gammeCatalogue.id !== values.gammeCatalogue.id
              ? { ...values, codeSousGammeCatalogue: null, sousGammeCatalogue: null }
              : values;
          } else {
            return canalArticle;
          }
        }),
    }));
  }, [values]);

  useEffect(() => {
    if (desactive) {
      createUpdateProduit();
      setDesactive(false);
    }
  }, [allValues]);

  /* useEffect(() => {
    if (allValues && allValues.produitCanaux && allValues.produitCanaux.length) {
      const canaux = allValues.produitCanaux.filter(
        produitCanal =>
          (produitCanal.id || !produitCanal.isRemoved) &&
          (!produitCanal.idCanalSousGamme || !produitCanal.codeSousGammeCatalogue),
      );

      if (canaux.length) return setNextBtnDisabled(true);
    }
    return setNextBtnDisabled(false);
  }, [allValues]); */

  const handleOpenCloseModal = (open: boolean) => () => {
    setOpenaModal(open);
  };

  const handleActiveCanal = (active: boolean) => () => {
    handleOpenCloseModal(!active);
    activeCanal(active);
    setValues(prevState => ({
      ...prevState,
      isRemoved: active,
    }));
    setDesactive(active);
  };

  return (
    <div className={classes.root}>
      <div className={classes.container}>
        <form>
          <CustomCheckbox
            label="Activer le canal"
            name="isRemoved"
            value={!canalArticle || (canalArticle && canalArticle.isRemoved) ? false : true}
            checked={!canalArticle || (canalArticle && canalArticle.isRemoved) ? false : true}
            onChange={
              !canalArticle || (canalArticle && canalArticle.isRemoved) ? handleChange : false
            }
            onClick={
              !canalArticle || (canalArticle && canalArticle.isRemoved)
                ? false
                : handleOpenCloseModal(true)
            }
            disabled={disabled}
          />
          {canalArticle && !canalArticle.isRemoved && (
            <div>
              <Typography className={classes.subTitle}>Informations générales</Typography>
              <div className={classes.articleInfo}>
                <Box display="flex" marginBottom="23px" alignItems="baseline">
                  <Typography className={classes.articleInfoLabel}>Libellé : </Typography>
                  <Typography className={classes.articleInfoValue}>
                    {allValues && allValues.libelle}
                  </Typography>
                </Box>
                <Box display="flex" alignItems="baseline">
                  <Typography className={classes.articleInfoLabel}>CIP : </Typography>
                  <Typography className={classes.articleInfoValue}>
                    {allValues && allValues.codeReference}
                  </Typography>
                </Box>
              </div>
              <div className={classes.inlineThreeContainer}>
                <CustomFormTextField
                  label="Stock"
                  name="qteStock"
                  placeholder="Stock"
                  value={(canalArticle && canalArticle.qteStock) || 0}
                  onChange={handleChange}
                  disabled={disabled}
                />
                <CustomFormTextField
                  label="Standard de vente"
                  name="stv"
                  placeholder="Standard de vente"
                  value={(canalArticle && canalArticle.stv) || 0}
                  onChange={handleChange}
                  disabled={disabled}
                />
                <CustomFormTextField
                  label="Nbre de produits par carton"
                  name="unitePetitCond"
                  placeholder="Nbre de produits par carton"
                  value={(canalArticle && canalArticle.unitePetitCond) || 0}
                  onChange={handleChange}
                  disabled={disabled}
                />
              </div>
              <div className={classes.inlineContainer}>
                <FormControl fullWidth variant="outlined">
                  <InputLabel htmlFor="outlined-prixFab">Prix achat canal</InputLabel>
                  <OutlinedInput
                    id="outlined-prixFab"
                    value={(canalArticle && canalArticle.prixFab) || 0}
                    onChange={handleChange}
                    name="prixFab"
                    endAdornment={<InputAdornment position="end">€</InputAdornment>}
                    labelWidth={110}
                    disabled={disabled}
                  />
                </FormControl>
                <FormControl fullWidth variant="outlined">
                  <InputLabel htmlFor="outlined-prixPhv">Prix vente pharmacie</InputLabel>
                  <OutlinedInput
                    id="outlined-prixPhv"
                    value={(canalArticle && canalArticle.prixPhv) || 0}
                    onChange={handleChange}
                    name="prixPhv"
                    endAdornment={<InputAdornment position="end">€</InputAdornment>}
                    labelWidth={180}
                    disabled={disabled}
                  />
                </FormControl>
              </div>
              <div className={classes.inlineContainer}>
                {/* <CustomAutocomplete
                  id="laboratoireId"
                  options={
                    (laboResult &&
                      laboResult.data &&
                      laboResult.data.search &&
                      laboResult.data.search.data) ||
                    []
                  }
                  optionLabelKey="nom"
                  inputValue={
                    (canalArticle && canalArticle.laboratoire && canalArticle.laboratoire.nomLabo) || ''
                  }
                  onAutocompleteChange={(e, inputValue) =>
                    handleChangeAutocomplete(e, inputValue, 'laboratoire', 'nom', 'Laboratoire')
                  }
                  label="Laboratoire partenaire"
                /> */}
                {/*  <CustomFormTextField
                  label="Laboratoire Partenaire"
                  placeholder="Laboratoire Partenaire"
                  disabled={true}
                  onChange={handleChange}
                /> */}
                <CustomDatePicker
                  label="Date de péremption"
                  placeholder="Date de péremption"
                  onChange={handleChangeDate('datePeremption')}
                  name="datePeremption"
                  value={(canalArticle && canalArticle.datePeremption) || null}
                  format=""
                  disablePast={false}
                  disabled={disabled}
                />
              </div>
              <Typography className={classes.subTitle}>Dates</Typography>
              <div className={classes.inlineThreeContainer}>
                <CustomDatePicker
                  label="Création"
                  placeholder="Création"
                  onChange={handleChangeDate('dateCreation')}
                  name="dateCreation"
                  value={(canalArticle && canalArticle.dateCreation) || null}
                  format=""
                  disablePast={false}
                  disabled={disabled}
                />
                <CustomDatePicker
                  label="MAJ"
                  placeholder="MAJ"
                  onChange={handleChangeDate('dateModification')}
                  name="dateModification"
                  value={(canalArticle && canalArticle.dateModification) || null}
                  format=""
                  disablePast={false}
                  disabled={disabled}
                />
                <CustomDatePicker
                  label="Retrait"
                  placeholder="Retrait"
                  onChange={handleChangeDate('dateRetrait')}
                  name="dateRetrait"
                  value={(canalArticle && canalArticle.dateRetrait) || null}
                  format=""
                  disablePast={false}
                  disabled={disabled}
                />
              </div>
              <Typography className={classes.subTitle}>Gamme</Typography>
              <div className={classes.inlineContainer}>
                {/* <CustomAutocomplete
                  id="gammeId"
                  options={
                    (gammeCommercialResult &&
                      gammeCommercialResult.data &&
                      gammeCommercialResult.data.search &&
                      canalArticle.codeCommandeCanal === 'PFL' &&
                      gammeCommercialResult.data.search.data) ||
                    []
                  }
                  optionLabelKey="libelle"
                  value={canalArticle.gammeCommercial}
                  onChange={(e, inputValue) =>
                    handleChangeAutocomplete(
                      e,
                      inputValue,
                      'gammeCommercial',
                      'libelle',
                      'GammeCommercial',
                    )
                  }
                  label="Gamme"
                  disabled={
                    !(
                      gammeCommercialResult &&
                      gammeCommercialResult.data &&
                      gammeCommercialResult.data.search &&
                      canalArticle.codeCommandeCanal === 'PFL' &&
                      gammeCommercialResult.data.search.data
                    ) || disabled
                  }
                  required={false}
                />
                <CustomAutocomplete
                  id="sousGammeId"
                  options={
                    (canalArticle &&
                      canalArticle.gammeCommercial &&
                      canalArticle.gammeCommercial.sousGamme) ||
                    []
                  }
                  optionLabelKey="libelle"
                  value={canalArticle.sousGammeCommercial}
                  onChange={(e, inputValue) =>
                    handleChangeAutocomplete(
                      e,
                      inputValue,
                      'sousGammeCommercial',
                      'libelle',
                      'SousGammeCommercial',
                      'idCanalSousGamme',
                      'id',
                      false,
                    )
                  }
                  label="Sous gamme"
                  disabled={
                    (canalArticle && !canalArticle.gammeCommercial) ||
                    (canalArticle &&
                      canalArticle.gammeCommercial &&
                      canalArticle.gammeCommercial.sousGamme &&
                      canalArticle.gammeCommercial.sousGamme.length === 0) ||
                    disabled
                  }
                  required={false}
                /> */}
              </div>
              <Typography className={classes.subTitle}>Catalogue</Typography>
              <div className={classes.inlineContainer}>
                {/* <CustomAutocomplete
                  id="catalogeId"
                  options={
                    (gammeCatalogueResult &&
                      gammeCatalogueResult.data &&
                      gammeCatalogueResult.data.search &&
                      gammeCatalogueResult.data.search.data) ||
                    []
                  }
                  optionLabelKey="libelle"
                  value={canalArticle.gammeCatalogue}
                  onChange={(e, inputValue) =>
                    handleChangeAutocomplete(
                      e,
                      inputValue,
                      'gammeCatalogue',
                      'libelle',
                      'GammeCatalogue',
                    )
                  }
                  label="Gamme catalogue"
                  required={false}
                  disabled={disabled}
                />
                <CustomAutocomplete
                  id="sousCatalogueId"
                  options={
                    (canalArticle &&
                      canalArticle.gammeCatalogue &&
                      canalArticle.gammeCatalogue.sousGammes) ||
                    []
                  }
                  optionLabelKey="libelle"
                  value={canalArticle.sousGammeCatalogue}
                  onChange={(e, inputValue) =>
                    handleChangeAutocomplete(
                      e,
                      inputValue,
                      'sousGammeCatalogue',
                      'libelle',
                      'SousGammeCatalogue',
                      'codeSousGammeCatalogue',
                      'codeSousGamme',
                    )
                  }
                  label="Sous gamme catalogue"
                  disabled={(canalArticle && !canalArticle.gammeCatalogue) || disabled}
                  required={false}
                /> */}
              </div>
              <Typography className={classes.subTitle}>Promotions</Typography>
              <div className={classes.inlineThreeContainer}>
                <CustomFormTextField
                  label="Type"
                  name="promotionType"
                  placeholder="Type"
                  value=""
                  onChange={handleChange}
                  disabled={true}
                />
                <CustomDatePicker
                  label="Date de début"
                  placeholder="Date de début"
                  onChange={handleChangeDate('promotionDebut')}
                  name="promotionDebut"
                  value={null}
                  format=""
                  disablePast={false}
                  disabled={true}
                />
                <CustomDatePicker
                  label="Date de fin"
                  placeholder="Date de fin"
                  onChange={handleChangeDate('promotionFin')}
                  name="promotionFin"
                  value={null}
                  format=""
                  disablePast={false}
                  disabled={true}
                />
              </div>
              <Typography className={classes.subTitle}>Marché</Typography>
              <div className={classes.inlineThreeContainer}>
                <CustomFormTextField
                  label="Nom"
                  name="marcheNom"
                  placeholder="Nom"
                  value=""
                  onChange={handleChange}
                  disabled={true}
                />
                <CustomDatePicker
                  label="Date de début"
                  placeholder="Date de début"
                  onChange={handleChangeDate('marcheDebut')}
                  name="marcheDebut"
                  value={null}
                  format=""
                  disablePast={false}
                  disabled={true}
                />
                <CustomDatePicker
                  label="Date de fin"
                  placeholder="Date de fin"
                  onChange={handleChangeDate('marcheFin')}
                  name="marcheFin"
                  value={null}
                  format=""
                  disablePast={false}
                  disabled={true}
                />
              </div>
            </div>
          )}
        </form>
      </div>
      <ConfirmDialog
        open={openModal}
        message={'Voulez-vous désactiver ce canal?'}
        handleClose={handleOpenCloseModal(false)}
        handleValidate={handleActiveCanal(true)}
      />
    </div>
  );
};

export default withRouter(Form);
