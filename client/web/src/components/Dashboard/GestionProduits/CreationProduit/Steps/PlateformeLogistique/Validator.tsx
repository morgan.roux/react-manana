import { PlateformeLogistiqueInterface } from '../../Interface/PlateformeLogistiqueInterface';
const Validator = (values: PlateformeLogistiqueInterface) => {
  switch (values.dateCreation) {
    case undefined: {
      return true;
    }
    case null: {
      return true;
    }
  }
  switch (values.dateModification) {
    case undefined: {
      return true;
    }
    case null: {
      return true;
    }
  }
  switch (values.idCanalSousGamme) {
    case undefined: {
      return true;
    }
    case null: {
      return true;
    }
  }
  switch (values.codeSousGammeCatalogue) {
    case undefined: {
      return true;
    }
    case null: {
      return true;
    }
  }

  return false;
};
export default Validator;
