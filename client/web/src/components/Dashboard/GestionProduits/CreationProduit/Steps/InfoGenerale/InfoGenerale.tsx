import { useLazyQuery, useQuery } from '@apollo/react-hooks';
import {
  Box,
  Collapse,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Typography,
} from '@material-ui/core';
import { ExpandLess, ExpandMore } from '@material-ui/icons';
import { last } from 'lodash';
import React, { FC, useEffect, useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { GET_ACTES } from '../../../../../../graphql/Acte/query';
import { GET_LIBELLE_DIVERSES } from '../../../../../../graphql/LibelleDivers/query';
import { GET_LISTES } from '../../../../../../graphql/Liste/query';
import { GET_PRODUIT_TYPE_CODES } from '../../../../../../graphql/Produit/query';
import { GET_PRODUIT_CATEGORIES } from '../../../../../../graphql/ProduitCategorie/query';
import { SEARCH } from '../../../../../../graphql/search/query';
import { GET_TAUXSSES } from '../../../../../../graphql/TauxSS/query';
import { GET_TVAS } from '../../../../../../graphql/TVA/query';
import CustomAutocomplete from '../../../../../Common/CustomAutocomplete';
import { CustomCheckbox } from '../../../../../Common/CustomCheckbox';
import { CustomDatePicker } from '../../../../../Common/CustomDateTimePicker';
import CustomSelect from '../../../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../../../Common/CustomTextField';
import Dropzone from '../../../../../Common/Dropzone';
import { InfoGeneraleInterface } from '../../Interface/InfoGeneraleInterface';
import {
  SEARCH as SEARCH_INTERFACE,
  SEARCHVariables,
} from '../../../../../../graphql/search/types/SEARCH';
import ChoixFamille from '../ChoixFamille';
import useStyles from './styles';
import useInfoGenerale from './useInfoGenerale';
import Validator from './Validator';
import TooltipComponent from '../../../../../Main/Content/Panier/Tooltip/Tooltip';

interface TooltipContentProps {
  codeFamille: string;
  familleResult?: any;
}

export const TooltipContent: FC<TooltipContentProps> = ({ codeFamille, familleResult }) => {
  const classes = useStyles({});
  const [familles, setFamilles] = useState<any>(null);

  const arborescenceCodeFamille = (codeFamille: string[]): string[] => {
    let letter: string = '';
    const famille: string[] = [];
    codeFamille.map((fam, index) => {
      letter += fam;
      famille[index] = letter;
    });
    return famille;
  };

  const arborescence = (codeFamille: string): string | null => {
    if (codeFamille && familles) {
      const objet = familles.find((data: any) => data.codeFamille === codeFamille);
      return (objet && objet.libelleFamille) || '';
    }

    return null;
  };

  useEffect(() => {
    if (
      familleResult &&
      familleResult.data &&
      familleResult.data.search &&
      familleResult.data.search
    ) {
      setFamilles(familleResult.data.search.data);
    }
  }, [familleResult]);

  const viewArborescences = codeFamille
    ? arborescenceCodeFamille(Array.from(codeFamille)).map((letter, index) => {
        return `${arborescence(letter)} ${index !== codeFamille.length - 1 ? ' / ' : ''}`;
      })
    : [];

  return (
    <div>
      {codeFamille && familles && familles.length ? (
        <>
          <div style={{ fontSize: '14px', fontWeight: 'bold' }}>
            {viewArborescences && viewArborescences.length ? viewArborescences : ''}
          </div>
        </>
      ) : (
        ''
      )}
    </div>
  );
};

interface InfoGeneraleProps {
  setNextBtnDisabled: (disables: boolean) => void;
  allValues: any;
  setAllValues: (values: any) => void;
  match: { params: { id: string | undefined } };
}

const InfoGenerale: FC<InfoGeneraleProps & RouteComponentProps> = ({
  setNextBtnDisabled,
  allValues,
  setAllValues,
  match: {
    params: { id },
  },
}) => {
  const classes = useStyles({});
  const {
    handleChange,
    handleChangeAutocomplete,
    handleFileSelection,
    handleFileDeletion,
    handleChangeDate,
    values,
    setValues,
  } = useInfoGenerale(allValues as InfoGeneraleInterface);
  const {
    libelle,
    codeReference,
    typeCodeReferent,
    isGenerique,
    codeTva,
    codeTauxss,
    codeListe,
    laboDistributaire,
    laboExploitant,
    laboTitulaire,
    acte,
    codeLibelleStockage,
    selectedFiles,
    fichierImage,
    pxVente,
    pxAchat,
    dateSuppression,
    idCategorie,
  } = values;
  const typeCodesResult = useQuery(GET_PRODUIT_TYPE_CODES);
  const tableauResult = useQuery(GET_LISTES);
  const tvaResult = useQuery(GET_TVAS);
  const actesResult = useQuery(GET_ACTES);
  const remboursementResult = useQuery(GET_TAUXSSES);
  const stockageResult = useQuery(GET_LIBELLE_DIVERSES, { variables: { code: 9 } });
  const [loadLaboratoires, loadingLaboratoire] = useLazyQuery(SEARCH);
  const produitCategorieResult = useQuery(GET_PRODUIT_CATEGORIES);

  console.log('+++++++++++++++++++++++++== values : ', values);

  const isOnEdit = allValues && allValues.idProduit;
  const [imageUrl, setImageUrl] = useState<string | undefined>(undefined);
  const [openFamille, setOpenFamille] = useState<boolean>(true);

  const familleResult = useQuery<SEARCH_INTERFACE, SEARCHVariables>(SEARCH, {
    variables: {
      type: ['famille'],
      skip: 0,
      take: 2000,
    },
    fetchPolicy: 'cache-first',
  });

  const handleSearchLabo = (searchText: string) => {
    const textSearch = searchText.replace(/\s+/g, '\\ ').replace('/', '\\/');
    loadLaboratoires({
      variables: {
        type: ['laboratoire'],
        query: {
          query: {
            bool: {
              must: [
                {
                  query_string: {
                    query:
                      textSearch.startsWith('*') || textSearch.endsWith('*')
                        ? textSearch
                        : `*${textSearch}*`,
                    analyze_wildcard: true,
                  },
                },
              ],
            },
          },
        },
      },
    });
  };

  useEffect(() => {
    if (!fichierImage || !selectedFiles.length) {
      setImageUrl(undefined);
    }
    if (fichierImage && fichierImage.chemin) {
      setImageUrl(`${fichierImage.chemin}`);
    }

    if (selectedFiles && selectedFiles.length > 0) {
      const file: any = last(selectedFiles);
      if (file && !file.publicUrl) {
        const url = URL.createObjectURL(file);
        setImageUrl(url);
      } else {
        setImageUrl(file.publicUrl);
      }
    }
  }, [fichierImage, selectedFiles]);

  useEffect(() => {
    if (values) {
      setAllValues(prevState => ({ ...prevState, ...values }));
    }
  }, [values]);

  useEffect(() => {
    if (allValues) {
      setNextBtnDisabled(Validator(allValues));
    }
  }, [allValues]);

  useEffect(() => {
    loadLaboratoires({
      variables: { type: ['laboratoire'], take: 10, skip: 0 },
    });
  }, []);

  return (
    <div className={classes.root}>
      <div className={classes.container}>
        <Typography className={classes.subTitle}>Photo du produit</Typography>
        <div className={classes.formPhotoDropzone}>
          <Dropzone
            contentText="Glissez et déposez ici votre photo"
            selectedFiles={selectedFiles}
            setSelectedFiles={handleFileSelection}
            multiple={false}
            acceptFiles="image/*"
            withFileIcon={false}
            where="inProduitForm"
            withImagePreview={true}
            withImagePreviewCustomized={true}
            onClickDelete={handleFileDeletion}
            fileAlreadySetUrl={imageUrl}
            disabled={!id}
          />
        </div>
        <Typography className={classes.subTitle}>Données économiques</Typography>
        <form>
          <CustomFormTextField
            label="Libellé"
            name="libelle"
            placeholder="Nom du produit"
            value={libelle}
            onChange={handleChange}
            fullWidth={true}
            required={true}
            disabled={!id}
          />
          <div className={classes.inlineCheckboxContainer}>
            <div className={classes.codeReferenceContainer}>
              <CustomFormTextField
                label="Code produit"
                name="codeReference"
                placeholder="Code du produit"
                value={codeReference}
                onChange={handleChange}
                required={true}
                disabled={!id}
              />
            </div>

            <div className={classes.typeCodeReferentContainer}>
              <CustomSelect
                label="Type de code"
                list={
                  (typeCodesResult &&
                    typeCodesResult.data &&
                    typeCodesResult.data.produitTypeCodes) ||
                  []
                }
                listId="resipIdTypeCode"
                index="libelle"
                name="typeCodeReferent"
                value={typeCodeReferent}
                onChange={handleChange}
                required={true}
                disabled={!id}
              />
            </div>
            <div className={classes.checkboxContainer}>
              <CustomCheckbox
                name=""
                label="Plateforme"
                value={
                  allValues &&
                  allValues.produitCanaux &&
                  allValues.produitCanaux.some(
                    canal =>
                      canal &&
                      canal.codeCommandeCanal &&
                      canal.codeCommandeCanal === 'PFL' &&
                      canal.isRemoved === false,
                  )
                    ? true
                    : false
                }
                checked={
                  allValues &&
                  allValues.produitCanaux &&
                  allValues.produitCanaux.some(
                    canal =>
                      canal &&
                      canal.codeCommandeCanal &&
                      canal.codeCommandeCanal === 'PFL' &&
                      canal.isRemoved === false,
                  )
                    ? true
                    : false
                }
                disabled={true}
                onChange={handleChange}
              />
              <CustomCheckbox
                name=""
                label="Direct"
                value={
                  allValues &&
                  allValues.produitCanaux &&
                  allValues.produitCanaux.some(
                    canal =>
                      canal &&
                      canal.codeCommandeCanal &&
                      canal.codeCommandeCanal === 'DIRECT' &&
                      canal.isRemoved === false,
                  )
                    ? true
                    : false
                }
                checked={
                  allValues &&
                  allValues.produitCanaux &&
                  allValues.produitCanaux.some(
                    canal =>
                      canal &&
                      canal.codeCommandeCanal &&
                      canal.codeCommandeCanal === 'DIRECT' &&
                      canal.isRemoved === false,
                  )
                    ? true
                    : false
                }
                disabled={true}
                onChange={handleChange}
              />

              {/* <CustomCheckbox
                name="carteFidelite"
                label="Carte de fidélité"
                value={false}
                checked={false}
                onChange={handleChange}
                disabled={true}
              /> */}
              <CustomCheckbox
                name=""
                label="Grossiste"
                value={
                  allValues &&
                  allValues.produitCanaux &&
                  allValues.produitCanaux.some(
                    canal =>
                      canal &&
                      canal.codeCommandeCanal &&
                      canal.codeCommandeCanal === 'GROSSISTE' &&
                      canal.isRemoved === false,
                  )
                    ? true
                    : false
                }
                checked={
                  allValues &&
                  allValues.produitCanaux &&
                  allValues.produitCanaux.some(
                    canal =>
                      canal &&
                      canal.codeCommandeCanal &&
                      canal.codeCommandeCanal === 'GROSSISTE' &&
                      canal.isRemoved === false,
                  )
                    ? true
                    : false
                }
                disabled={true}
                onChange={handleChange}
              />
              <CustomCheckbox
                name="isGenerique"
                label="Générique"
                value={isGenerique ? true : false}
                checked={isGenerique ? true : false}
                onChange={handleChange}
                disabled={!id}
              />
            </div>
          </div>
          <CustomSelect
            label="TVA"
            list={(tvaResult && tvaResult.data && tvaResult.data.tvas) || []}
            listId="codeTva"
            index="designation"
            name="codeTva"
            value={codeTva}
            onChange={handleChange}
            disabled={!id}
          />
          <div className={classes.inlineContainer}>
            <FormControl fullWidth={true} variant="outlined">
              <InputLabel htmlFor="outlined-pxAchat">Prix catalogue</InputLabel>
              <OutlinedInput
                id="outlined-pxAchat"
                value={pxAchat}
                onChange={handleChange}
                name="pxAchat"
                endAdornment={<InputAdornment position="end">€</InputAdornment>}
                labelWidth={110}
                disabled={!id}
              />
            </FormControl>
            <FormControl fullWidth={true} variant="outlined">
              <InputLabel htmlFor="outlined-pxVente">Prix de vente (conseillé)</InputLabel>
              <OutlinedInput
                id="outlined-pxVente"
                value={pxVente}
                onChange={handleChange}
                name="pxVente"
                endAdornment={<InputAdornment position="end">€</InputAdornment>}
                labelWidth={180}
                disabled={!id}
              />
            </FormControl>
          </div>
          <div className={classes.inlineContainer}>
            <CustomSelect
              label="Tableau"
              list={(tableauResult && tableauResult.data && tableauResult.data.listes) || []}
              listId="codeListe"
              index="libelle"
              name="codeListe"
              value={codeListe}
              onChange={handleChange}
              disabled={!id}
            />
            <CustomSelect
              label="Stockage"
              list={
                (stockageResult && stockageResult.data && stockageResult.data.libelleDiverses) || []
              }
              listId="codeInfo"
              index="libelle"
              name="codeLibelleStockage"
              value={codeLibelleStockage}
              onChange={handleChange}
              disabled={!id}
            />
          </div>
          <div className={classes.inlineContainer}>
            <CustomSelect
              label="Remboursement"
              list={
                (remboursementResult &&
                  remboursementResult.data &&
                  remboursementResult.data.tauxsses) ||
                []
              }
              listId="codeTaux"
              index="tauxSS"
              name="codeTauxss"
              value={codeTauxss}
              onChange={handleChange}
              disabled={!id}
            />

            {/* <CustomAutocomplete
              id="acteId"
              options={(actesResult && actesResult.data && actesResult.data.actes) || []}
              optionLabelKey="libelleActe"
              value={acte}
              onChange={(e, inputValue) =>
                handleChangeAutocomplete(
                  e,
                  inputValue,
                  'acte',
                  'libelleActe',
                  'Acte',
                  'codeActe',
                  'codeActe',
                )
              }
              label="Acte"
              required={false}
              disabled={!isEdit}
            /> */}
          </div>
          <CustomSelect
            label="Hospitalier"
            list={
              (produitCategorieResult &&
                produitCategorieResult.data &&
                produitCategorieResult.data.produitCategories) ||
              []
            }
            listId="resipIdCategorie"
            index="libelle"
            name="idCategorie"
            value={idCategorie}
            onChange={handleChange}
            disabled={!id}
          />
          <CustomDatePicker
            label="Date Suppression"
            placeholder="Date Suppression"
            onChange={handleChangeDate('dateSuppression')}
            name="dateSuppression"
            value={dateSuppression}
            format=""
            disablePast={false}
            disabled={!id}
          />
          <CustomAutocomplete
            id="laboratoireDistributeurId"
            options={loadingLaboratoire?.data?.search?.data || []}
            optionLabelKey="nomLabo"
            value={laboDistributaire}
            onAutocompleteChange={value =>
              setValues(prevState => ({
                ...prevState,
                laboDistributaire: value,
                idLaboDistributaire: value.id,
              }))
            }
            inputValue={laboDistributaire?.nomLabo || ''}
            search={handleSearchLabo}
            label="Laboratoire distributeur"
            required={false}
            disabled={!id}
          />
          <div className={classes.inlineContainer}>
            <CustomAutocomplete
              id="laboratoireTitulaireId"
              options={loadingLaboratoire?.data?.search?.data || []}
              optionLabelKey="nomLabo"
              value={laboTitulaire}
              onAutocompleteChange={value =>
                setValues(prevState => ({
                  ...prevState,
                  laboTitulaire: value,
                  idLaboTitulaire: value.id,
                }))
              }
              search={handleSearchLabo}
              label="Laboratoire titulaire"
              required={false}
              disabled={!id}
            />
            <CustomAutocomplete
              id="laboratoireExploitantId"
              options={loadingLaboratoire?.data?.search?.data || []}
              optionLabelKey="nomLabo"
              value={laboExploitant}
              onAutocompleteChange={value =>
                setValues(prevState => ({
                  ...prevState,
                  laboExploitant: value,
                  idLaboExploitant: value.id,
                }))
              }
              search={handleSearchLabo}
              label="Laboratoire exploitant"
              required={false}
              disabled={!id}
            />
          </div>
          <div className={classes.familleExpander}>
            <Typography style={{ marginRight: '10px' }}>Famille : </Typography>

            <TooltipContent
              codeFamille={allValues?.famille?.codeFamille}
              familleResult={familleResult}
            />
          </div>
          <div className={classes.familleExpander}>
            <Box borderTop="1px solid" width="100%" marginLeft="16px"></Box>
            {openFamille ? (
              <IconButton onClick={() => setOpenFamille(!openFamille)}>
                <ExpandLess />
              </IconButton>
            ) : (
              <IconButton onClick={() => setOpenFamille(!openFamille)}>
                <ExpandMore />
              </IconButton>
            )}
          </div>

          <Collapse in={openFamille}>
            <ChoixFamille
              setNextBtnDisabled={setNextBtnDisabled}
              allValues={allValues}
              setAllValues={setAllValues}
              disabled={!id}
            />
          </Collapse>
        </form>
      </div>
    </div>
  );
};

export default withRouter(InfoGenerale);
