import { useLazyQuery } from '@apollo/react-hooks';
// import React, {useState, useEffect, FC} from 'react';
// import { GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE } from '../../../../graphql/PartenairesServices/query';
// import { SEARCH_CUSTOM_CONTENT_PARTENAIRE, SEARCH_CUSTOM_CONTENT_PARTENAIREVariables } from '../../../../graphql/PartenairesServices/types/SEARCH_CUSTOM_CONTENT_PARTENAIRE';
// import CustomAutocomplete from '../../../Common/CustomAutocomplete';
// export interface PrestataireInterface {
//   id: string;
//   nom: string;
// }

// export interface SelectPartenaireProps {
//   valuePrestataire: PrestataireInterface;
//   setValuePrestaire: (valuePrestataire: PrestataireInterface) => void;
// }

// export const SelectPartenaire: FC<SelectPartenaireProps> = ({valuePrestataire, setValuePrestaire}) => {

//     const [valueAuto, setValueAuto] = useState<any>('');

//     // Get laboratoires partenaire list
//     const [searchPartenaires, searchPartenairesResult] = useLazyQuery<
//     SEARCH_CUSTOM_CONTENT_PARTENAIRE,
//     SEARCH_CUSTOM_CONTENT_PARTENAIREVariables
//     >(GET_SEARCH_CUSTOM_CONTENT_PARTENAIRE, {
//     fetchPolicy: 'cache-first',
//     onError: errors => {
//         errors.graphQLErrors.map(err => {
//         const snackBarData: SnackVariableInterface = {
//             type: 'ERROR',
//             message: err.message,
//             isOpen: true,
//         };
//         displaySnackBar(client, snackBarData);
//         });
//     },
//     });

//     return (
//       <CustomAutocomplete
//         id="idPartenaire"
//         value={valueAuto}
//         options={
//         (searchPartenairesResult?.data?.search?.data as any) || []
//         }
//         optionLabelKey="nom"
//         inputValue={valuePrestataire?.nom || ''}
//         onAutocompleteChange={handleChangePartenaireAutocomplete}
//         label="Prestataire"
//         required
//         disabled={false}
//         onChangeValue={handleChangeAuto}
//       />
//     )
// }