import React, { FC, useState } from 'react';
import NoItemContentImage from '../../Common/NoItemContentImage';
import {
  GET_CATEGORIES as GET_CATEGORIES_TYPE,
  GET_CATEGORIESVariables,
} from '../../../federation/ged/categorie/types/GET_CATEGORIES';
import { FEDERATION_CLIENT } from '../DemarcheQualite/apolloClientFederation';
import { CategoriePage, ICategorie, ISousCategorie } from '@app/ui-kit';
import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { GET_CATEGORIE, GET_CATEGORIES } from '../../../federation/ged/categorie/query';
import {
  GET_CATEGORIE as GET_CATEGORIE_TYPE,
  GET_CATEGORIEVariables,
} from '../../../federation/ged/categorie/types/GET_CATEGORIE';
import {
  CREATE_CATEGORIE,
  UPDATE_CATEGORIE,
  DELETE_CATEGORIE,
  CREATE_SOUS_CATEGORIE,
  UPDATE_SOUS_CATEGORIE,
  DELETE_SOUS_CATEGORIE,
} from '../../../federation/ged/categorie/mutation';
import {
  CREATE_CATEGORIE as CREATE_CATEGORIE_TYPE,
  CREATE_CATEGORIEVariables,
} from '../../../federation/ged/categorie/types/CREATE_CATEGORIE';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import {
  UPDATE_CATEGORIE as UPDATE_CATEGORIE_TYPE,
  UPDATE_CATEGORIEVariables,
} from '../../../federation/ged/categorie/types/UPDATE_CATEGORIE';
import {
  DELETE_CATEGORIE as DELETE_CATEGORIE_TYPE,
  DELETE_CATEGORIEVariables,
} from '../../../federation/ged/categorie/types/DELETE_CATEGORIE';
import {
  DELETE_SOUS_CATEGORIE as DELETE_SOUS_CATEGORIE_TYPE,
  DELETE_SOUS_CATEGORIEVariables,
} from '../../../federation/ged/categorie/types/DELETE_SOUS_CATEGORIE';
import {
  CREATE_SOUS_CATEGORIE as CREATE_SOUS_CATEGORIE_TYPE,
  CREATE_SOUS_CATEGORIEVariables,
} from '../../../federation/ged/categorie/types/CREATE_SOUS_CATEGORIE';
import {
  UPDATE_SOUS_CATEGORIE as UPDATE_SOUS_CATEGORIE_TYPE,
  UPDATE_SOUS_CATEGORIEVariables,
} from '../../../federation/ged/categorie/types/UPDATE_SOUS_CATEGORIE';
import { RouteComponentProps } from 'react-router';
import { getPharmacie } from '../../../services/LocalStorage';
import { SUPER_ADMINISTRATEUR } from '../../../Constant/roles';
import { GET_COLLABORATEURS as GET_COLLABORATEURS_TYPE } from '../../../federation/iam/user/types/GET_COLLABORATEURS';
import { GET_COLLABORATEURS } from '../../../federation/iam/user/query';
import UserInput from '../../Common/UserInput';
import PartenaireInput from '../../Common/PartenaireInput';

const workflows = [
  {
    code: 'INTERNE',
    libelle: 'Interne',
  },
  {
    code: 'UTILISATEUR',
    libelle: 'Utilisateur',
  },
  {
    code: 'PARTENAIRE_SERVICE',
    libelle: 'Prestataire Service',
  },
];

export interface PrestataireInterface {
  id: string;
  nom: string;
}

const Categorie: FC<RouteComponentProps & { user: any }> = ({ history: { push }, user }) => {
  const client = useApolloClient();
  const pharmacie = getPharmacie();

  const partenaireType = ['partenaire'];
  const codeParameter = '0831';

  const [openContact, setOpenContact] = useState<boolean>(false);
  const [usersSelected, setUsersSelected] = useState<any[]>([]);
  const [prestataire, setPrestataire] = useState<any>('');
  const [valueAuto, setValueAuto] = useState<any>('');

  const isSuperAdmin =
    user && user.role && user.role.code && user.role.code === SUPER_ADMINISTRATEUR;

  const { loading, error, data, refetch: refetchCategories } = useQuery<
    GET_CATEGORIES_TYPE,
    GET_CATEGORIESVariables
  >(GET_CATEGORIES, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
    variables: {
      filter: isSuperAdmin
        ? {
          or: [{ idPharmacie: { is: null } }, { idPharmacie: { eq: pharmacie.id } }],
        }
        : pharmacie
          ? {
            idPharmacie: {
              eq: pharmacie.id,
            },
          }
          : {},
    },
  });

  const [
    getCollaborateurs,
    { loading: getCollaborateursLoading, error: getCollaborateursError, data: collaborateurs },
  ] = useLazyQuery<GET_COLLABORATEURS_TYPE>(GET_COLLABORATEURS, {
    client: FEDERATION_CLIENT,
  });

  const [
    getCategorie,
    {
      loading: getCategorieLoading,
      error: getCategorieError,
      data: categorie,
      refetch: refetchCategorie,
    },
  ] = useLazyQuery<GET_CATEGORIE_TYPE, GET_CATEGORIEVariables>(GET_CATEGORIE, {
    fetchPolicy: 'cache-and-network',
    client: FEDERATION_CLIENT,
  });

  const [addCategorie] = useMutation<CREATE_CATEGORIE_TYPE, CREATE_CATEGORIEVariables>(
    CREATE_CATEGORIE,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'La catégorie a été ajoutée avec succès.',
          type: 'SUCCESS',
          isOpen: true,
        });

        // TODO : Update cache
        refetchCategories();
      },
      onError: () => {
        displaySnackBar(client, {
          message: "Des erreurs se sont survenues pendant l'enregistrement de la catégorie",
          type: 'ERROR',
          isOpen: true,
        });
      },
      update: (cache, creationData) => { },
      client: FEDERATION_CLIENT,
    },
  );

  const [editCategorie] = useMutation<UPDATE_CATEGORIE_TYPE, UPDATE_CATEGORIEVariables>(
    UPDATE_CATEGORIE,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'La catégorie a été modifiée avec succès !',
          isOpen: true,
          type: 'SUCCESS',
        });
      },
      onError: () => {
        displaySnackBar(client, {
          message: 'Des erreurs se sont survenues pendant la modification de la catégorie',
          isOpen: true,
          type: 'ERROR',
        });
      },
      client: FEDERATION_CLIENT,
    },
  );

  const [deleteCategorie] = useMutation<DELETE_CATEGORIE_TYPE, DELETE_CATEGORIEVariables>(
    DELETE_CATEGORIE,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          message: 'La catégorie a été supprimée avec succès !',
          type: 'SUCCESS',
          isOpen: true,
        });

        // TODO : Update cache
        refetchCategories();
      },
      onError: () => {
        displaySnackBar(client, {
          message: 'Des erreus se sont survenues pendant la suppression de la catégorie',
          type: 'ERROR',
          isOpen: true,
        });
      },
      update: (cache, deletionResult) => { },
      client: FEDERATION_CLIENT,
    },
  );

  const [createSousCategorie] = useMutation<
    CREATE_SOUS_CATEGORIE_TYPE,
    CREATE_SOUS_CATEGORIEVariables
  >(CREATE_SOUS_CATEGORIE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La sous-catégorie a été ajoutée avec succès!',
        type: 'SUCCESS',
        isOpen: true,
      });

      //TODO : Update cache
      refetchCategorie();
    },
    onError: () => {
      displaySnackBar(client, {
        message: "Des erreurs se sont survenues pendant l'ajout de la sous-catégorie",
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [updateSousCategorie] = useMutation<
    UPDATE_SOUS_CATEGORIE_TYPE,
    UPDATE_SOUS_CATEGORIEVariables
  >(UPDATE_SOUS_CATEGORIE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La sous-catégorie a été modifiée avec succès!',
        type: 'SUCCESS',
        isOpen: true,
      });

      //TODO : Update cache
      refetchCategorie();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreurs se sont survenues pendant la modification de la sous-catégorie',
        type: 'SUCCESS',
        isOpen: true,
      });
    },
    client: FEDERATION_CLIENT,
  });

  const [deleteSousCategorie] = useMutation<
    DELETE_SOUS_CATEGORIE_TYPE,
    DELETE_SOUS_CATEGORIEVariables
  >(DELETE_SOUS_CATEGORIE, {
    onCompleted: () => {
      displaySnackBar(client, {
        message: 'La sous-catégorie a été supprimée avec succès !',
        type: 'SUCCESS',
        isOpen: true,
      });

      // TODO : Update cache
      refetchCategorie();
    },
    onError: () => {
      displaySnackBar(client, {
        message: 'Des erreus se sont survenues pendant la suppression de la sous-catégorie',
        type: 'ERROR',
        isOpen: true,
      });
    },
    update: (cache, deletionResult) => { },
    client: FEDERATION_CLIENT,
  });


  const handleDelete = (categorie: ICategorie): void => {
    deleteCategorie({
      variables: {
        input: {
          id: categorie.id as any,
        },
      },
    });
  };

  const handleSaveCategorie = (categorie: ICategorie): void => {

    if (categorie?.id) {
      editCategorie({
        variables: {
          id: categorie?.id,
          categoryInput: {
            libelle: categorie?.libelle as any,
            type: categorie?.type as any,
            validation: categorie?.validation as any,
            idUserParticipants: categorie?.participants?.map(({ id }) => id),
            idPartenaireValidateur: categorie?.partenaireValidateur?.id || undefined,
            workflowValidation: categorie?.workflowValidation || undefined,
          },
        },
      });
    } else {
      addCategorie({
        variables: {
          categoryInput: {
            libelle: categorie?.libelle as any,
            type: categorie?.type as any,
            validation: categorie?.validation as any,
            idUserParticipants: categorie?.participants?.map(({ id }) => id),
            idPartenaireValidateur: categorie?.partenaireValidateur?.id || undefined,
            workflowValidation: categorie?.workflowValidation || undefined,
          },
        },
      });
    }
  };

  const handleSaveSousCategorie = (sousCategorie: ISousCategorie): void => {
    if (!sousCategorie.id) {
      createSousCategorie({
        variables: {
          input: {
            idCategorie: sousCategorie?.parent?.id as any,
            libelle: sousCategorie?.libelle as any,
            validation: sousCategorie?.validation as any,
            idUserParticipants: sousCategorie?.participants?.map(({ id }) => id),
            idPartenaireValidateur: sousCategorie?.partenaireValidateur?.id || undefined,
            workflowValidation: sousCategorie?.workflowValidation || undefined,
          },
        },
      });
    } else {
      updateSousCategorie({
        variables: {
          id: sousCategorie.id as any,
          input: {
            idCategorie: sousCategorie?.parent?.id as any,
            libelle: sousCategorie?.libelle as any,
            validation: sousCategorie?.validation as any,
            idUserParticipants: sousCategorie?.participants?.map(({ id }) => id),
            idPartenaireValidateur: sousCategorie?.partenaireValidateur?.id || undefined,
            workflowValidation: sousCategorie?.workflowValidation || undefined,
          },
        },
      });
    }
    setPrestataire('');
  };

  const handleDeleteSousCategorie = (sousCategorie: ISousCategorie) => {
    if (sousCategorie.id) {
      deleteSousCategorie({
        variables: {
          input: {
            id: sousCategorie.id,
          },
        },
      });
    }
  };

  const handleRequestShowDetail = (categorie: ICategorie): void => {
    getCategorie({
      variables: {
        id: categorie.id as any,
      },
    });
  };

  // console.log('input value', valuePrestataire);

  const PartageComponent = (
    <UserInput
      openModal={openContact}
      setOpenModal={setOpenContact}
      selected={usersSelected}
      setSelected={setUsersSelected}
      codeParameter={codeParameter}
    />
  );



  return (
    <CategoriePage
      currentIdPharmacie={pharmacie?.id}
      loading={loading}
      error={error as any}
      emptyListComponent={
        <NoItemContentImage
          title="Aucune catégorie trouvée"
          subtitle="Ajouter-en une en cliquant sur le bouton de création d'en haut"
        />
      }
      noCategorieSelected={
        <NoItemContentImage
          title="Affichage de catégorie"
          subtitle="Cliquer sur une catégorie de la liste de gauche pour voir la description dans cette partie"
        />
      }
      categories={data?.gedCategories.nodes as any}
      onRequestDelete={handleDelete}
      onRequestSave={handleSaveCategorie}
      onRequestSaveSousCategorie={handleSaveSousCategorie}
      onRequestDeleteSousCategorie={handleDeleteSousCategorie}
      onRequestShowDetail={handleRequestShowDetail}
      categorie={{
        loading: getCategorieLoading,
        error: getCategorieError as any,
        data: categorie?.gedCategorie as any,
      }}
      PartageComponent={PartageComponent}
      workflows={workflows}
      participants={usersSelected}
      setParticipants={setUsersSelected}
      partenaireValidateur={typeof prestataire === 'string' ? { id: prestataire } : prestataire}
      setPartenaireValidateur={setPrestataire}
      SelectPartenaireComponent={
        <PartenaireInput
          index="partenaire"
          label="Prestataire"
          value={prestataire}
          onChange={setPrestataire}
        />
      }
    />
  );
};

export default Categorie;
