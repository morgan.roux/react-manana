import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    actionModalContact: {
      width: '100%',
      display: 'flex',
      justifyContent: 'flex-end',
      marginTop: 16,
    },
  }),
);

export default useStyles;
