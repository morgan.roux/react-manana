import React, { FC, useState } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import useStyles from './style';
import { Grid, ListItem, Box, Button, Tooltip, Fade } from '@material-ui/core';
import AppsIcon from '@material-ui/icons/Apps';
import PeopleIcon from '@material-ui/icons/People';
import WidgetsIcon from '@material-ui/icons/Widgets';
import { IMenuItem } from '../Utilitaire/Utilitaire';
import { useValueParameterAsBoolean } from '../../../utils/getValueParameter';
import SyncAltIcon from '@material-ui/icons/SyncAlt';
import ArrowBack from '@material-ui/icons/ArrowBack';
import MenuItemComponent from '../Utilitaire/MenuItem';
import ParametreSsoRouter from './ParametreSsoRouter';
import { ME_me } from '../../../graphql/Authentication/types/ME';
import { getUser } from '../../../services/LocalStorage';
import { SUPER_ADMINISTRATEUR } from '../../../Constant/roles';

const ParametreSso: FC<RouteComponentProps> = ({ history, location: { pathname } }) => {
  const classes = useStyles({});
  const helloidParam = useValueParameterAsBoolean('0046');
  const activeDirectoryParam = useValueParameterAsBoolean('0049');
  const ftpParam = useValueParameterAsBoolean('0050');
  const applicationsParam = useValueParameterAsBoolean('0051');
  const currentUser: ME_me = getUser();
  const isSuperAdmin: boolean =
    currentUser && currentUser.role && currentUser.role.code === SUPER_ADMINISTRATEUR
      ? true
      : false;

  let pathName = pathname.split('/')[3];
  const isApplicationManagement: boolean = pathName === 'manage-applications';
  if (isApplicationManagement) {
    pathName = pathname.split('/')[4];
  }

  const items: IMenuItem[] = isApplicationManagement
    ? [
        {
          path: '/db/sso-parameter/manage-applications/applications',
          label: 'Applications',
          show: applicationsParam || helloidParam,
          icon: <AppsIcon className={classes.icon} />,
        },
        {
          path: '/db/sso-parameter/manage-applications/helloid-group',
          label: 'HelloID groupes',
          show: helloidParam,
          icon: <PeopleIcon className={classes.icon} />,
        },
        {
          path: '/db/sso-parameter/manage-applications/helloid-applications-role',
          label: 'Applications Helloid par rôles',
          show: helloidParam,
          icon: <WidgetsIcon className={classes.icon} />,
        },
        {
          path: '/db/sso-parameter/manage-applications/applications-role',
          label: 'Autres applications par rôles',
          show: applicationsParam,
          icon: <WidgetsIcon className={classes.icon} />,
        },
        {
          path: '/db/sso-parameter/manage-applications/external-mapping',
          label: 'Mapping externe des utilisateurs',
          show: applicationsParam,
          icon: <SyncAltIcon className={classes.icon} />,
        },
      ]
    : [
        {
          path: '/db/sso-parameter/active-directory',
          label: 'Paramètres Active Directory',
          show: activeDirectoryParam && isSuperAdmin,
        },
        {
          path: '/db/sso-parameter/ftp',
          label: 'Paramètres FTP',
          show: ftpParam,
        },
        {
          path: '/db/sso-parameter/helloid-parameter',
          label: 'Paramètres HelloID',
          show: helloidParam && isSuperAdmin,
        },
        {
          path: '/db/sso-parameter/manage-applications',
          label: 'Gestion des applications',
          show: applicationsParam || helloidParam,
        },
      ];

  const menuItems = items.filter(item => item.show);

  const handlePush = (link: string) => {
    history.push(link);
  };
  return (
    <Grid container={true} className={classes.root}>
      <Grid item={true} xs={3} className={classes.leftOperation}>
        {isApplicationManagement && (
          <Tooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Retour">
            <Button style={{ paddingLeft: 0 }} onClick={handlePush.bind(null, '/db/sso-parameter')}>
              <ArrowBack className={classes.icon} />
            </Button>
          </Tooltip>
        )}
        {menuItems.map((item, index) => {
          const itemPath = isApplicationManagement
            ? item.path.split('/')[4]
            : item.path.split('/')[3];
          const result: boolean =
            (pathName && itemPath && pathName === itemPath) === true || (index === 0 && !pathName);
          return (
            <ListItem
              button={true}
              onClick={handlePush.bind(null, item.path)}
              key={`item-${index}`}
            >
              <MenuItemComponent
                icon={item.icon}
                label={item.label}
                secondIcon={null}
                active={result}
              />
            </ListItem>
          );
        })}
      </Grid>
      <Grid item={true} xs={9}>
        <ParametreSsoRouter
          activeDirectoryParam={activeDirectoryParam}
          ftpParam={ftpParam}
          helloidParam={helloidParam}
          applicationsParam={applicationsParam}
          isSuperAdmin={isSuperAdmin}
        />
      </Grid>
    </Grid>
  );
};

export default withRouter(ParametreSso);
