import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { Column, Table, TableChange } from '@app/ui-kit';
import { IconButton } from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import moment from 'moment';
import React, { FC, useState } from 'react';
import { RouteComponentProps } from 'react-router';
import { REMISE_URL } from '../../../Constant/url';
import {
  CREATE_ONE_REMISE,
  DELETE_MANY_REMISE,
  DELETE_ONE_REMISE,
} from '../../../federation/partenaire-service/remise/mutation';
import { GET_REMISES } from '../../../federation/partenaire-service/remise/query';
import {
  CREATE_ONE_REMISE as CREATE_ONE_REMISE_TYPE,
  CREATE_ONE_REMISEVariables,
} from '../../../federation/partenaire-service/remise/types/CREATE_ONE_REMISE';
import {
  DELETE_MANY_REMISE as DELETE_MANY_REMISE_TYPE,
  DELETE_MANY_REMISEVariables,
} from '../../../federation/partenaire-service/remise/types/DELETE_MANY_REMISE';
import {
  DELETE_ONE_REMISE as DELETE_ONE_REMISE_TYPE,
  DELETE_ONE_REMISEVariables,
} from '../../../federation/partenaire-service/remise/types/DELETE_ONE_REMISE';
import SnackVariableInterface from '../../../Interface/SnackVariableInterface';
import { SortDirection } from '../../../types/federation-global-types';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import { ConfirmDeleteDialog } from '../../Common/ConfirmDialog';
import CustomButton from '../../Common/CustomButton';
import { CustomFullScreenModal } from '../../Common/CustomModal';
import SubToolbar from '../../Common/newCustomContent/SubToolbar';
import RemiseForm, { RemiseFormValues } from '../../Main/Content/Produits/RemiseForm';
import { FEDERATION_CLIENT } from '../DemarcheQualite/apolloClientFederation';
import useStyles from './styles';

interface RemisesProps {}

const Remises: FC<RemisesProps & RouteComponentProps> = ({
  location: { pathname },
  history: { push },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const isOnList = pathname === `/db/${REMISE_URL}`;
  const isOnCreate = pathname === `/db/${REMISE_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`/db/${REMISE_URL}/edit`);
  const [pagination, setPagination] = useState({ take: 10, skip: 0 });
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);

  const columns: Column[] = [
    {
      name: 'nom',
      label: 'Nom',
      renderer: (value: any) => {
        return value.nom ? value.nom : '-';
      },
    },
    {
      name: 'utilisateur',
      label: 'Utilisateur',
      renderer: (value: any) => {
        return value.utilisateur ? value.utilisateur : '-';
      },
    },
    {
      name: 'model',
      label: 'Model',
      renderer: (value: any) => {
        return value.model ? value.model : '-';
      },
    },
    {
      name: 'type',
      label: 'Type',
      renderer: (value: any) => {
        return value.type ? value.type : '-';
      },
    },
    {
      name: 'active',
      label: 'Active',
      renderer: (value: any) => {
        return value.active ? 'OUI' : 'NON';
      },
    },
    {
      name: 'dateDebut',
      label: 'Date Début',
      renderer: (value: any) => {
        return value.dateDebut ? moment(value.dateDebut).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: 'dateFin',
      label: 'Date Fin',
      renderer: (value: any) => {
        return value.dateFin ? moment(value.dateFin).format('DD/MM/YYYY') : '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <IconButton
            onClick={() => {
              setRowToDelete(value);
              setOpenDeleteDialog(true);
            }}
          >
            <Delete />
          </IconButton>
        );
      },
    },
  ];

  const [openForm, setOpenForm] = useState<boolean>(isOnCreate || isOnEdit);

  const [values, setValues] = useState<any>({});

  const [selected, setSelected] = useState<any>([]);

  const [showSelected, setShowSelected] = useState<boolean>(false);

  const [searchText, setSearchText] = useState('');

  const [rowToDelete, setRowToDelete] = useState<any>();

  const defaultValues: RemiseFormValues = {
    dateDebut: null,
    dateFin: null,
    nom: '',
    model: 'LIGNE',
    remiseDetails: [],
  };

  const loadingRemises = useQuery<any, any>(GET_REMISES, {
    client: FEDERATION_CLIENT,
    variables: {
      filter: searchText !== '' ? { nom: { iLike: searchText } } : undefined,
      sorting: [{ field: 'dateDebut', direction: SortDirection.DESC }],
      paging: { limit: pagination.take, offset: pagination.skip },
    },
  });

  const [createRemise, creationRemise] = useMutation<
    CREATE_ONE_REMISE_TYPE,
    CREATE_ONE_REMISEVariables
  >(CREATE_ONE_REMISE, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'La remise a été créée avec succès',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
      setValues(defaultValues);
      //refetch && refetch();
      setOpenForm(false);
    },
    onError: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: "Une erreur s'est produite lors de création de la remise",
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [deleteOneRemise, deletingOneRemise] = useMutation<
    DELETE_ONE_REMISE_TYPE,
    DELETE_ONE_REMISEVariables
  >(DELETE_ONE_REMISE, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'La remise a été supprimée avec succès',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
      setRowToDelete(null);
      setOpenDeleteDialog(false);
      loadingRemises.refetch();
    },
    onError: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: "Une erreur s'est produite lors de suppression de la remise",
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [deleteManyRemise, deletingManyRemise] = useMutation<
    DELETE_MANY_REMISE_TYPE,
    DELETE_MANY_REMISEVariables
  >(DELETE_MANY_REMISE, {
    client: FEDERATION_CLIENT,
    onCompleted: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'SUCCESS',
        message: 'Les remises ont été supprimées avec succès',
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
      setSelected([]);
      setOpenDeleteDialog(false);
      loadingRemises.refetch();
    },
    onError: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: "Une erreur s'est produite lors de suppression des remises",
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleCreateRemise = () => {
    createRemise({
      variables: {
        input: {
          idProduitCanals: [], //(selected || []).map(produitCanal => produitCanal.id),
          source: 'PERMANENT',
          ...values,
          remiseDetails: values.remiseDetails.map(remiseDetail => ({
            ...remiseDetail,
            id: undefined,
          })),
        },
      },
    });
  };

  const handleDelete = () => {
    rowToDelete && deleteOneRemise({ variables: { input: { id: rowToDelete.id } } });
    selected.length > 0 &&
      deleteManyRemise({
        variables: { input: { filter: { id: { in: selected.map(row => row.id) } } } },
      });
  };

  const handleRunSearch = (change: TableChange) => {
    setPagination({ skip: change.skip, take: change.take });
    setSearchText(change.searchText);
  };

  const goToList = () => push(`/db/${REMISE_URL}`);
  const goToAdd = () => push(`/db/${REMISE_URL}/create`);
  return (
    <div>
      <SubToolbar
        title={
          isOnCreate
            ? `Ajout d'une nouvelle remise`
            : isOnEdit
            ? `Modification d'une remise`
            : 'Liste des remises'
        }
        dark={!isOnList}
        withBackBtn={false}
        onClickBack={goToList}
      >
        {/* <CustomButton color="secondary" startIcon={<Add />} onClick={goToAdd}>
          Ajouter une remise
        </CustomButton> */}
        {selected.length > 0 && (
          <CustomButton
            color="secondary"
            startIcon={<Delete />}
            onClick={() => setOpenDeleteDialog(true)}
          >
            Supprimer la selection
          </CustomButton>
        )}
      </SubToolbar>
      <div style={{ padding: '32px 64px 64px 64px' }}>
        <Table
          data={loadingRemises.data?.remises?.nodes || []}
          columns={columns}
          selectable={true}
          rowsSelected={selected}
          rowsTotal={loadingRemises.data?.remises?.totalCount || 0}
          onRowsSelectionChange={setSelected}
          onRunSearch={handleRunSearch}
          loading={loadingRemises.loading}
          showSelectedRows={showSelected}
          onShowSelected={setShowSelected}
          onClearSelection={() => setSelected([])}
        />
      </div>

      <CustomFullScreenModal
        open={openForm}
        setOpen={setOpenForm}
        title="Remise"
        withBtnsActions={true}
        disabledButton={creationRemise.loading}
        actionButton="Ajouter"
        onClickConfirm={handleCreateRemise}
      >
        <RemiseForm values={values} onChange={setValues} />
      </CustomFullScreenModal>
      <ConfirmDeleteDialog
        open={openDeleteDialog}
        setOpen={setOpenDeleteDialog}
        onClickConfirm={handleDelete}
        isLoading={deletingOneRemise.loading || deletingManyRemise.loading}
      />
    </div>
  );
};

export default Remises;
