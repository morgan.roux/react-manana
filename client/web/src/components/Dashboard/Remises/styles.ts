import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    searchInputBox: {
      padding: '20px 0px 0px 20px',
      display: 'flex',
    },
  }),
);

export default useStyles;
