import React, { FC, ChangeEvent, useState } from 'react';
import { GroupeClientFormInterface } from './usePharmacieForm';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Stepper from '../../../Common/Stepper';
import { GROUPES_CLIENTS_URL } from '../../../../Constant/url';
import { Step } from '../../../Common/Stepper/Stepper';
import { GroupeClientFormStepOne, GroupeClientFormStepTwo } from './GroupeClientFormSteps';
import { useCheckedsPharmacie } from '../../TitulairesPharmacies/utils';

export interface InputInterface {
  label: string;
  placeholder: string;
  type: 'select' | 'date' | 'text';
  name: string;
  value: any;
  inputType?: string;
  selectOptions?: any[];
  required?: boolean;
}

export interface FormInputInterface {
  title: string;
  inputs: InputInterface[];
}

export interface GroupeClientFormProps {
  title: string;
  values: GroupeClientFormInterface;
  isOnCreate: boolean;
  isOnEdit: boolean;
  finalStepBtn: any;
  handleChangeInput: (e: ChangeEvent<any>) => void;
  handleChangeDate: (name: string) => (date: any) => void;
}

const GroupeClientForm: FC<GroupeClientFormProps & RouteComponentProps> = ({
  values,
  handleChangeInput,
  handleChangeDate,
  isOnCreate,
  isOnEdit,
  title,
  finalStepBtn,
  history: { push },
}) => {
  const [activeStep, setActiveStep] = useState<number | undefined>(0);
  const checkedsPharmacie = useCheckedsPharmacie();

  const steps: Step[] = [
    {
      title: 'Information',
      content: (
        <GroupeClientFormStepOne
          title={title}
          values={values}
          handleChangeInput={handleChangeInput}
          handleChangeDate={handleChangeDate}
          isOnCreate={isOnCreate}
          isOnEdit={isOnEdit}
          finalStepBtn={finalStepBtn}
        />
      ),
    },
    {
      title: 'Choix de pharmacie',
      content: (
        <GroupeClientFormStepTwo
          title={title}
          values={values}
          handleChangeInput={handleChangeInput}
          handleChangeDate={handleChangeDate}
          isOnCreate={isOnCreate}
          isOnEdit={isOnEdit}
          finalStepBtn={finalStepBtn}
        />
      ),
    },
  ];

  const { id, nom, dateValiditeDebut, dateValiditeFin, idPharmacies } = values;

  const disableNextButton = (): boolean => {
    if (
      (activeStep === 0 && (!nom || !dateValiditeDebut || !dateValiditeFin)) ||
      (dateValiditeDebut && dateValiditeFin && dateValiditeDebut > dateValiditeFin) ||
      (activeStep === 1 &&
        (idPharmacies.length <= 0 ||
          !checkedsPharmacie ||
          (checkedsPharmacie && checkedsPharmacie.length <= 0))) ||
      (isOnEdit && !id)
    ) {
      return true;
    }
    return false;
  };

  const goToList = () => push(`/db/${GROUPES_CLIENTS_URL}`);

  return (
    <Stepper
      title={title}
      steps={steps}
      backToHome={goToList}
      disableNextBtn={disableNextButton()}
      fullWidth={false}
      finalStep={finalStepBtn}
      activeStepFromProps={activeStep}
      setActiveStepFromProps={setActiveStep}
    />
  );
};

export default withRouter(GroupeClientForm);
