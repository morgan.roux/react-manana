import { useState, ChangeEvent } from 'react';

export interface GroupeClientFormInterface {
  id: string | null;
  nom: string;
  dateValiditeDebut: any;
  dateValiditeFin: any;
  idPharmacies: string[];
}

export const initialState: GroupeClientFormInterface = {
  id: '',
  nom: '',
  dateValiditeDebut: null,
  dateValiditeFin: null,
  idPharmacies: [],
};

const useGroupeClientForm = (defaultState?: GroupeClientFormInterface) => {
  const initValues: GroupeClientFormInterface = defaultState || initialState;
  const [values, setValues] = useState<GroupeClientFormInterface>(initValues);

  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      setValues(prevState => ({ ...prevState, [name]: value }));
    }
  };

  const handleChangeDate = (name: string) => (date: any) => {
    setValues(prevState => ({ ...prevState, [name]: date }));
  };

  return {
    handleChange,
    handleChangeDate,
    values,
    setValues,
  };
};

export default useGroupeClientForm;
