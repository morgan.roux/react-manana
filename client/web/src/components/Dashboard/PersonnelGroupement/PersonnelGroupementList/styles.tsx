import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    personnelGroupementListRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      position: 'relative',
      height: 'auto',
      minHeight: 'fit-content',
      padding: '25px 20px',
      '& .MuiTableHead-root th': {
        minWidth: 'auto',
        zIndex: 0,
      },
      '& *': {
        fontFamily: 'Roboto',
      },
    },
  }),
);

export default useStyles;
