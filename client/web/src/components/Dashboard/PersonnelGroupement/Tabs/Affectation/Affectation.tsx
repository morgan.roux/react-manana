import React from 'react';
import CustomContent from '../../../../Common/newCustomContent';
import { GET_CHECKEDS_PERSONNEL_GROUPEMENT } from '../../../../../graphql/Personnel/local';

import SubMenu from '../../SubMenu';
import { RouteComponentProps, withRouter } from 'react-router';
import { PERSONNEL_GROUPEMENT_URL } from '../../../../../Constant/url';
import { useCreatePersonnel, useButtonHeadAction } from '../../utils/utils';
import { GET_CHECKEDS_PERSONNEL_AFFECTATION } from '../../../../../graphql/PersonnelAffectation/local';

interface ListAffectationProps {
  listResult?: any;
  columns?: any;
  pathname: any;
  values: any;
  sexe: any;
  idGroupement: string;
  civilite: string | null | undefined;
  nom: string | null | undefined;
  codeService: string;
  openAffectationDialog;
  setOpenAffectationDialog;
  setPersonnelToAffect: (value: any) => void;
  personnelToAffect: any;
  affectPersonnel: any;
}

const ListAffectation: React.FC<ListAffectationProps & RouteComponentProps> = ({
  listResult,
  columns,
  pathname,
  values,
  sexe,
  idGroupement,
  civilite,
  nom,
  codeService,
  history: { push },
  openAffectationDialog,
  setOpenAffectationDialog,
  personnelToAffect,
  setPersonnelToAffect,
  affectPersonnel,
}) => {
  const isOnList =
    pathname === `/db/${PERSONNEL_GROUPEMENT_URL}` ||
    `/db/${PERSONNEL_GROUPEMENT_URL}/affectation` ||
    `/db/${PERSONNEL_GROUPEMENT_URL}/list`;
  const isOnCreate = pathname === `/db/${PERSONNEL_GROUPEMENT_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`/db/${PERSONNEL_GROUPEMENT_URL}/edit`);

  const [createPersonnel, mutationSuccess] = useCreatePersonnel({
    variables: { ...values, sexe: sexe as any, idGroupement },
  });
  const disabledSaveBtn = (): boolean => {
    if (!isOnList && (!civilite || !nom || !codeService)) {
      return true;
    }
    return false;
  };
  const [goToAddPersonnel, goBack] = useButtonHeadAction(push);
  return (
    <>
      <SubMenu
        isOnList={isOnList}
        isOnCreate={isOnCreate}
        isOnEdit={isOnEdit}
        createPersonnel={createPersonnel}
        disabledSaveBtn={disabledSaveBtn}
        goToAddPersonnel={goToAddPersonnel}
        goBack={goBack}
        openAffectationDialog={openAffectationDialog}
        setOpenAffectationDialog={setOpenAffectationDialog}
        setPersonnelToAffect={setPersonnelToAffect}
        personnelToAffect={personnelToAffect}
        affectPersonnel={affectPersonnel}
      />
      <CustomContent
        checkedItemsQuery={{
          name: 'checkedsPersonnelAffecation',
          type: 'personnelaffectation',
          query: GET_CHECKEDS_PERSONNEL_AFFECTATION,
        }}
        {...{ listResult, columns }}
        isSelectable={true}
      />
    </>
  );
};

export default withRouter(ListAffectation);
