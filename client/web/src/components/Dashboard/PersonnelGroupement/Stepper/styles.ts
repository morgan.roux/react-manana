import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    personnelGroupementFormRoot: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      marginTop: 50,
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
  }),
);

export default useStyles;
