import React, { FC, ChangeEvent, Dispatch, SetStateAction } from 'react';
import useStyles from './styles';
import { PersonnelGroupementInterface } from './usePersonnelGroupementForm';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { Typography } from '@material-ui/core';
import CustomSelect from '../../../Common/CustomSelect';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import { CIVILITE_LIST, SEXE_LIST } from '../../../../Constant/user';
import { useQuery } from '@apollo/react-hooks';
import { GET_MINIM_SERVICES } from '../../../../graphql/Service';
import { MINIM_SERVICES } from '../../../../graphql/Service/types/MINIM_SERVICES';
import { isEmailValid } from '../../../../utils/Validator';

export interface PersonnelGroupementFormProps {
  title: string;
  selectedFiles: File[];
  croppedFiles: File[];
  values: PersonnelGroupementInterface;
  imageUrl: string | null | undefined;
  setImageUrl: Dispatch<SetStateAction<string | null | undefined>>;
  avatarImageSrc: string;
  isOnCreate: boolean;
  isOnEdit: boolean;
  setSelectedFiles: (files: File[]) => void;
  setCroppedFiles: (files: File[]) => void;
  handleChangeInput: (e: ChangeEvent<any>) => void;
}

const PersonnelGroupementForm: FC<PersonnelGroupementFormProps & RouteComponentProps> = ({
  values,
  handleChangeInput,
}) => {
  const classes = useStyles({});
  const { civilite, nom, prenom, sexe, mailProf, telMobProf, codeService } = values;

  /**
   * Get service list
   */
  const { data: dataService, loading: loadingService } = useQuery<MINIM_SERVICES>(
    GET_MINIM_SERVICES,
  );

  return (
    <div className={classes.personnelGroupementFormRoot}>
      <div className={classes.infoPersoContainer}>
        <Typography className={classes.inputTitle}>Informations personnelles</Typography>
        <div className={classes.inputsContainer}>
          <div className={classes.formRow}>
            <Typography className={classes.inputLabel}>
              Civilité <span>*</span>
            </Typography>
            <CustomSelect
              label=""
              list={CIVILITE_LIST}
              listId="id"
              index="value"
              name="civilite"
              value={civilite}
              onChange={handleChangeInput}
              shrink={false}
              placeholder="Choisissez une civilité"
              withPlaceholder={true}
            />
          </div>

          <div className={classes.formRow}>
            <Typography>Prénom</Typography>
            <CustomFormTextField
              name="prenom"
              value={prenom}
              onChange={handleChangeInput}
              placeholder="Prénom de la personne"
            />
          </div>
          <div className={classes.formRow}>
            <Typography className={classes.inputLabel}>
              Nom <span>*</span>
            </Typography>
            <CustomFormTextField
              name="nom"
              value={nom}
              onChange={handleChangeInput}
              placeholder="Nom de la personne"
            />
          </div>
          <div className={classes.formRow}>
            <Typography>Sexe</Typography>
            <CustomSelect
              label=""
              list={SEXE_LIST}
              listId="id"
              index="value"
              name="sexe"
              value={sexe}
              onChange={handleChangeInput}
              shrink={false}
              placeholder="Sexe de la personne"
              withPlaceholder={true}
            />
          </div>
          <div className={classes.formRow}>
            <Typography>Email</Typography>
            <CustomFormTextField
              error={!isEmailValid(mailProf || '')}
              name="mailProf"
              value={mailProf}
              onChange={handleChangeInput}
              placeholder="Son email"
            />
          </div>
          <div className={classes.formRow}>
            <Typography>Téléphone</Typography>
            <CustomFormTextField
              name="telMobProf"
              value={telMobProf}
              onChange={handleChangeInput}
              placeholder="Son numéro"
            />
          </div>
        </div>
      </div>
      <div className={classes.infoPersoContainer}>
        <Typography className={classes.inputTitle}>Responsabilité</Typography>
        <div className={classes.inputsContainer}>
          <div className={classes.formRow}>
            <Typography className={classes.inputLabel}>
              Service <span>*</span>
            </Typography>
            <CustomSelect
              label=""
              list={(dataService && dataService.services) || []}
              listId="code"
              index="nom"
              name="codeService"
              value={codeService}
              onChange={handleChangeInput}
              shrink={false}
              placeholder="Service de la personne"
              withPlaceholder={true}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(PersonnelGroupementForm);
