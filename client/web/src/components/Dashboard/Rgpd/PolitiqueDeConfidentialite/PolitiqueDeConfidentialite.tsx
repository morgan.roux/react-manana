import { Box, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import ReactQuill from 'react-quill';
import { CREATE_UPDATE_RGPDVariables } from '../../../../graphql/Rgpd/types/CREATE_UPDATE_RGPD';
import CustomButton from '../../../Common/CustomButton';
import { useCreateUpdateRgpd, useGetRgpds } from '../hooks';
import useStyles from './styles';

const PolitiqueDeConfidentialite = () => {
  const { data: getData, loading: getLoading } = useGetRgpds();

  console.log('get', getData);
  const exist = getData && getData.rgpds && getData.rgpds[0];
  const [description, setDescription] = useState(exist ? exist.politiqueConfidentialite : '');
  const descriptionChange = (content: string) => {
    content && setDescription(content);
  };
  useEffect(() => {
    exist && setDescription(exist.politiqueConfidentialite);
  }, [getData]);

  const variables: CREATE_UPDATE_RGPDVariables = {
    input: {
      id: (exist && exist.id) || '',
      conditionUtilisation: (exist && exist.conditionUtilisation) || '',
      politiqueConfidentialite: description,
      informationsCookies: (exist && exist.informationsCookies) || '',
    },
  };

  const { createUpdateRgpd, loading, data } = useCreateUpdateRgpd(variables);
  const classes = useStyles({});

  const onClickSave = () => {
    createUpdateRgpd();
  };
  return (
    <Box className={classes.formRgpdAccueilRoot}>
      <Box className={classes.formContainer}>
        <Box display="flex" alignItems="flex-end">
          <Typography className={classes.title}>Politique de confidentialité</Typography>
        </Box>

        <ReactQuill
          theme="snow"
          className={classes.customReactQuill}
          onChange={descriptionChange}
          value={description || ''}
        />

        <CustomButton color="secondary" className={classes.btnSave} onClick={onClickSave}>
          ENREGISTRER
        </CustomButton>
      </Box>
    </Box>
  );
};
export default PolitiqueDeConfidentialite;
