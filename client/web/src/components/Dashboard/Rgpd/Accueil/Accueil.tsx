import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { Typography, Box } from '@material-ui/core';
import { values } from 'lodash';
import React, { Fragment, FC, useState } from 'react';
import ReactQuill from 'react-quill';
import {
  DO_CREATE_UPDATE_RGP_ACCEUIL,
  DO_CREATE_UPDATE_RGP_ACCEUIL_PLUSES,
} from '../../../../graphql/Rgpd/mutation';
import { GET_RGPD_ACCUEILS, GET_RGPD_ACCUEILS_PLUSES } from '../../../../graphql/Rgpd/query';
import {
  CREATE_UPDATE_RGP_ACCEUIL,
  CREATE_UPDATE_RGP_ACCEUILVariables,
} from '../../../../graphql/Rgpd/types/CREATE_UPDATE_RGP_ACCEUIL';
import {
  CREATE_UPDATE_RGP_ACCEUIL_PLUSES,
  CREATE_UPDATE_RGP_ACCEUIL_PLUSESVariables,
} from '../../../../graphql/Rgpd/types/CREATE_UPDATE_RGP_ACCEUIL_PLUSES';
import {
  RGPD_ACCUEILS,
  RGPD_ACCUEILSVariables,
} from '../../../../graphql/Rgpd/types/RGPD_ACCUEILS';
import {
  RGPD_ACCUEILS_PLUSES,
  RGPD_ACCUEILS_PLUSESVariables,
} from '../../../../graphql/Rgpd/types/RGPD_ACCUEILS_PLUSES';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop/Backdrop';
import CustomButton from '../../../Common/CustomButton/CustomButton';
import { CustomFormTextField } from '../../../Common/CustomTextField';
import { useGetRgpdAcceuil, useGetRgpdAcceuilPlus } from './hooks';
import useStyles from './styles';

interface Accueilprops {}
interface acceuilInitialStateProps {
  titre: string;
  description: string;
}
interface acceuilPlusInitialstateProps {
  titrePlus: string;
  descriptionPlus: string;
}
const Accueil: FC<Accueilprops> = ({}) => {
  // getting acceuil value from back
  const {
    RgpdAccueilData: acceuilData,
    RgpdAccueilLoading: acceuilLoading,
    id: idGroupement,
  } = useGetRgpdAcceuil();
  const resultID =
    acceuilData &&
    acceuilData.rgpdAccueils &&
    acceuilData.rgpdAccueils[0] &&
    acceuilData.rgpdAccueils[0].id;
  const result = acceuilData && acceuilData.rgpdAccueils && acceuilData.rgpdAccueils[0];
  console.log('idddd group====', idGroupement);

  // acceuil
  const [onCreate, setOnCreate] = useState<boolean>(true);
  React.useEffect(() => {
    if (acceuilData && acceuilData.rgpdAccueils && acceuilData.rgpdAccueils.length > 0) {
      setOnCreate(false);
    } else setOnCreate(true);
  }, [acceuilData]);

  // acceuil plus
  const [onCreatePlus, setOnCreatePlus] = useState<boolean>(true);
  const {
    RgpdAccueilPlusData: resultPlusData,
    RgpdAccueilPlusLoading: acceuilPlusLoading,
  } = useGetRgpdAcceuilPlus();

  const resultDataAcceuilPlus =
    resultPlusData && resultPlusData.rgpdAccueilPluses && resultPlusData.rgpdAccueilPluses[0];
  const resultPlusID =
    resultPlusData &&
    resultPlusData.rgpdAccueilPluses &&
    resultPlusData.rgpdAccueilPluses[0] &&
    resultPlusData.rgpdAccueilPluses[0].id;
  React.useEffect(() => {
    if (
      resultPlusData &&
      resultPlusData.rgpdAccueilPluses &&
      resultPlusData.rgpdAccueilPluses.length > 0
    ) {
      setOnCreatePlus(false);
    } else setOnCreatePlus(true);
  }, [resultPlusData]);

  const client = useApolloClient();

  // initial state for form change

  const acceuilInitialState = {
    titre: (result && result.title) || '',
    description: (result && result.description) || '',
  };

  // getting value from formsetAccueilValuePlus
  const [accueilValue, setAccueilValue] = useState<acceuilInitialStateProps>(acceuilInitialState);
  const [titrePlus, setTitrePlus] = useState<string>(
    (resultDataAcceuilPlus && resultDataAcceuilPlus.title) || '',
  );
  const [descriptionPlus, setDescriptionPlus] = useState<string>(
    (resultDataAcceuilPlus && resultDataAcceuilPlus.description) || '',
  );

  React.useEffect(() => {
    setAccueilValue(acceuilInitialState);
  }, [acceuilData, resultPlusData]);

  React.useEffect(() => {
    setTitrePlus((resultDataAcceuilPlus && resultDataAcceuilPlus.title) || '');
    setDescriptionPlus((resultDataAcceuilPlus && resultDataAcceuilPlus.description) || '');
  }, [resultDataAcceuilPlus, resultPlusData]);

  const descriptionAcceuilChange = (content: string) => {
    setAccueilValue({ ...accueilValue, description: content });
  };
  const descriptionAcceuilPlusChange = (content: string) => {
    setDescriptionPlus(content);
  };
  const formChange = event => {
    const { value, name } = event.target;
    setAccueilValue({ ...accueilValue, [name]: value });
  };
  const formChangePlus = event => {
    const { value, name } = event.target;
    setTitrePlus(value);
  };
  // mutation createUpdate texte accueil
  const [createUpdateRgpdAccueil, { loading: isLoading }] = useMutation<
    CREATE_UPDATE_RGP_ACCEUIL,
    CREATE_UPDATE_RGP_ACCEUILVariables
  >(DO_CREATE_UPDATE_RGP_ACCEUIL, {
    onCompleted: data => {
      if (data && data.createUpdateRgpdAccueil) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: onCreate
            ? 'Texte Accueil ajouté avec succès'
            : 'Texte Accueil modifié avec succès',
          isOpen: true,
        });
      }
      setOnCreate(false);
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
    update: (cache, { data }) => {
      if (cache && data) {
        const req: any = cache.readQuery<RGPD_ACCUEILS, RGPD_ACCUEILSVariables>({
          query: GET_RGPD_ACCUEILS,
          variables: { idGroupement },
        });
        if (req && onCreate) {
          const updated = data.createUpdateRgpdAccueil;
          cache.writeQuery<RGPD_ACCUEILS, RGPD_ACCUEILSVariables>({
            query: GET_RGPD_ACCUEILS,
            variables: { idGroupement },
            data: { rgpdAccueils: [updated] },
          });
        } else {
          const result = req.rgpdAccueils && req.rgpdAccueils[0];
          cache.writeQuery<RGPD_ACCUEILS, RGPD_ACCUEILSVariables>({
            query: GET_RGPD_ACCUEILS,
            variables: { idGroupement },
            data: {
              rgpdAccueils: [...result],
            },
          });
        }
      }
    },
  });

  // mutation createUpdate texte accueil plus
  const [createUpdateRgpdAccueilPlus, { loading: dataPlusInloading }] = useMutation<
    CREATE_UPDATE_RGP_ACCEUIL_PLUSES,
    CREATE_UPDATE_RGP_ACCEUIL_PLUSESVariables
  >(DO_CREATE_UPDATE_RGP_ACCEUIL_PLUSES, {
    onCompleted: data => {
      if (data && data.createUpdateRgpdAccueilPlus) {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: onCreatePlus
            ? 'Texte en savoir plus ajouté avec succès'
            : 'Texte en savoir plus modifié avec succès',
          isOpen: true,
        });
      }
      setOnCreatePlus(false);
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },

    update: (cache, { data }) => {
      if (cache && data) {
        const req: any = cache.readQuery<RGPD_ACCUEILS_PLUSES, RGPD_ACCUEILS_PLUSESVariables>({
          query: GET_RGPD_ACCUEILS_PLUSES,
          variables: { idGroupement },
        });
        if (req && onCreatePlus) {
          const updated = data.createUpdateRgpdAccueilPlus;
          cache.writeQuery<RGPD_ACCUEILS_PLUSES, RGPD_ACCUEILS_PLUSESVariables>({
            query: GET_RGPD_ACCUEILS_PLUSES,
            variables: { idGroupement },
            data: { rgpdAccueilPluses: [updated] },
          });
        } else {
          const result = req.rgpdAccueilPluses && req.rgpdAccueilPluses[0];

          cache.writeQuery<RGPD_ACCUEILS_PLUSES, RGPD_ACCUEILS_PLUSESVariables>({
            query: GET_RGPD_ACCUEILS_PLUSES,
            variables: { idGroupement },
            data: {
              rgpdAccueilPluses: [...result],
            },
          });
        }
      }
    },
  });
  // handle click texte acceuil
  const handleClickAcceuil = () => {
    if (onCreate) {
      createUpdateRgpdAccueil({
        variables: { input: { title: accueilValue.titre, description: accueilValue.description } },
      });
    } else {
      createUpdateRgpdAccueil({
        variables: {
          input: {
            id: resultID,
            title: accueilValue.titre,
            description: accueilValue.description,
          },
        },
      });
    }
  };

  // handle create texte acceuil plus
  const handleClickAcceuilPlus = () => {
    if (onCreatePlus) {
      createUpdateRgpdAccueilPlus({
        variables: {
          input: { title: titrePlus, description: descriptionPlus },
        },
      });
    } else {
      createUpdateRgpdAccueilPlus({
        variables: {
          input: {
            id: resultPlusID,
            title: titrePlus,
            description: descriptionPlus,
          },
        },
      });
    }
  };
  const classes = useStyles({});

  // test button disable
  const [clickDisable, setClickDisable] = useState<boolean>(true);
  React.useEffect(() => {
    if (
      accueilValue.titre !== '' &&
      accueilValue.description !== '' &&
      accueilValue.description !== '<p></p>'
    ) {
      setClickDisable(false);
    } else setClickDisable(true);
  }, [accueilValue]);
  const [clickDisablePlus, setClickDisablePlus] = useState<boolean>(true);
  React.useEffect(() => {
    if (titrePlus !== '' && descriptionPlus !== '') {
      setClickDisablePlus(false);
    } else setClickDisablePlus(true);
  }, [descriptionPlus, titrePlus]);

  return (
    <Box className={classes.formRgpdAccueilRoot}>
      <Box className={classes.formContainer}>
        {acceuilPlusLoading && <Backdrop value="Chargement en cours, veuillez patienter ..." />}
        {isLoading && dataPlusInloading && (
          <Backdrop value="Modification en cours, veuillez patienter ..." />
        )}
        <Fragment key={`rgpd_accueil_form_item_1`}>
          <Typography className={classes.inputTitle}>Texte accueil</Typography>
          <CustomFormTextField
            name="titre"
            label="Titre"
            required={true}
            value={accueilValue.titre}
            onChange={formChange}
          />
          <ReactQuill
            theme="snow"
            className={classes.customReactQuill}
            value={accueilValue.description}
            onChange={descriptionAcceuilChange}
          />
          <CustomButton onClick={handleClickAcceuil} disabled={clickDisable} color="secondary">
            {onCreate ? 'CREER UN TEXTE ACCUEIL' : 'MODIFIER UN TEXTE ACCUEIL'}
          </CustomButton>
        </Fragment>
      </Box>

      <Box className={classes.formContainer}>
        <Fragment key={`rgpd_accueil_form_item_2`}>
          <Typography className={classes.inputTitle}>Texte en savoir plus</Typography>
          <CustomFormTextField
            name="titrePlus"
            label="Titre"
            required={true}
            value={titrePlus}
            onChange={formChangePlus}
          />
          <ReactQuill
            theme="snow"
            className={classes.customReactQuill}
            value={descriptionPlus}
            onChange={descriptionAcceuilPlusChange}
          />
          <CustomButton
            onClick={handleClickAcceuilPlus}
            disabled={clickDisablePlus}
            color="secondary"
          >
            {onCreatePlus ? 'CREER UN TEXTE EN SAVOIR PLUS' : 'MODIFIER UN TEXTE EN SAVOIR PLUS'}
          </CustomButton>
        </Fragment>
      </Box>
    </Box>
  );
};
export default Accueil;
