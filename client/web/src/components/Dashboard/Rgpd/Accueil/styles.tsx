import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formRgpdAccueilRoot: {
      width: '100%',
      maxWidth: 700,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      padding: '36px 50px',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
    },
    formContainer: {
      width: '100%',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-start',
      justifyContent: 'center',
      marginBottom: 25,
      '& .MuiFormControl-root': {
        marginBottom: 24,
      },
    },
    inputTitle: {
      fontWeight: 'bold',
      fontSize: 20,
      color: theme.palette.primary.main,
      marginBottom: 15,
    },
    customReactQuill: {
      width: '100%',
      outline: 'none',
      marginBottom: 18,
      border: '1px solid rgba(0, 0, 0, 0.23)',
      borderRadius: 4,
      '& .ql-toolbar.ql-snow': {
        border: '0px',
        borderBottom: '1px solid rgba(0, 0, 0, 0.23)',
      },
      '& .ql-container': {
        minHeight: 200,
        border: '0px',
      },
      '&:hover': {
        borderColor: theme.palette.primary.main,
      },
      '&:focus': {
        outline: `auto ${theme.palette.primary.main}`,
      },
    },
  }),
);

export default useStyles;
