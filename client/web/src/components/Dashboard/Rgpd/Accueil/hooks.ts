import { useApolloClient, useLazyQuery, useMutation, useQuery } from '@apollo/react-hooks';
import { GET_RGPD_ACCUEILS, GET_RGPD_ACCUEILS_PLUSES } from '../../../../graphql/Rgpd';
import { RGPD_ACCUEILS, RGPD_ACCUEILSVariables } from '../../../../graphql/Rgpd/types/RGPD_ACCUEILS';
import { RGPD_ACCUEILS_PLUSES, RGPD_ACCUEILS_PLUSESVariables } from '../../../../graphql/Rgpd/types/RGPD_ACCUEILS_PLUSES';
import { getGroupement } from '../../../../services/LocalStorage';
import { displaySnackBar } from '../../../../utils/snackBarUtils';




// query to get accueil data
export const useGetRgpdAcceuil = (
) => {
    const client = useApolloClient();

    const groupement = getGroupement();
    const id = groupement.id


    const { loading: RgpdAccueilLoading, data: RgpdAccueilData } = useQuery<
        RGPD_ACCUEILS,
        RGPD_ACCUEILSVariables
    >(GET_RGPD_ACCUEILS, {
        variables: { idGroupement: groupement && groupement.id || "" },

        onError: errors => {
            errors.graphQLErrors.map(err => {
                displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
            });
        },
    });

    return { RgpdAccueilLoading, RgpdAccueilData, groupement, id };
};

// query to get accueil data plus
export const useGetRgpdAcceuilPlus = (
) => {
    const client = useApolloClient();

    const groupement = getGroupement()


    const { loading: RgpdAccueilPlusLoading, data: RgpdAccueilPlusData } = useQuery<
        RGPD_ACCUEILS_PLUSES,
        RGPD_ACCUEILS_PLUSESVariables
    >(GET_RGPD_ACCUEILS_PLUSES, {
        variables: { idGroupement: groupement && groupement.id || '' },

        onError: errors => {
            errors.graphQLErrors.map(err => {
                displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
            });
        },
    });

    return { RgpdAccueilPlusLoading, RgpdAccueilPlusData };
};

