import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { DO_CREATE_UPDATE_RGPD, GET_RGPDS } from '../../../graphql/Rgpd';
import { CREATE_UPDATE_RGPD, CREATE_UPDATE_RGPDVariables } from '../../../graphql/Rgpd/types/CREATE_UPDATE_RGPD';
import { RGPDS, RGPDSVariables } from '../../../graphql/Rgpd/types/RGPDS';
import { getGroupement } from '../../../services/LocalStorage';
import { displaySnackBar } from '../../../utils/snackBarUtils';

export const useCreateUpdateRgpd = (
  values: CREATE_UPDATE_RGPDVariables,
) => {
  const client = useApolloClient();

  const [createUpdateRgpd, { loading, data }] = useMutation<
    CREATE_UPDATE_RGPD,
    CREATE_UPDATE_RGPDVariables
  >(DO_CREATE_UPDATE_RGPD, {
    variables: { ...values },
    onCompleted: data => {
      if (data && data.createUpdateRgpd) {
        displaySnackBar(client, { type: 'SUCCESS', message: "Enregistrement effectué avec succès", isOpen: true });
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  return { createUpdateRgpd, loading, data };
};

export const useGetRgpds = (
) => {
  const client = useApolloClient();

  const groupement = getGroupement()
  const { loading, data } = useQuery<
    RGPDS,
    RGPDSVariables
  >(GET_RGPDS, {
    variables: { idGroupement: groupement && groupement.id || '' },

    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });

  return { loading, data };
};




