import { Box, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import ReactQuill from 'react-quill';
import { CREATE_UPDATE_RGPDVariables } from '../../../../graphql/Rgpd/types/CREATE_UPDATE_RGPD';
import CustomButton from '../../../Common/CustomButton';
import { useCreateUpdateRgpd, useGetRgpds } from '../hooks';
import useStyles from './styles';

const InformationsSurLesCookies = () => {
  const { data: getData, loading: getLoading } = useGetRgpds();
  const exist = getData && getData.rgpds && getData.rgpds[0];
  const [description, setDescription] = useState(exist ? exist.informationsCookies : '');
  const descriptionChange = (content: string) => {
    content && setDescription(content);
  };
  useEffect(() => {
    exist && setDescription(exist.informationsCookies);
  }, [getData]);
  const variables: CREATE_UPDATE_RGPDVariables = {
    input: {
      id: (exist && exist.id) || '',
      conditionUtilisation: (exist && exist.conditionUtilisation) || '',
      politiqueConfidentialite: (exist && exist.politiqueConfidentialite) || '',
      informationsCookies: description,
    },
  };

  const { createUpdateRgpd, loading, data } = useCreateUpdateRgpd(variables);

  const onClickSave = () => {
    createUpdateRgpd();
  };
  const classes = useStyles({});
  return (
    <Box className={classes.formRgpdAccueilRoot}>
      <Box className={classes.formContainer}>
        <Box display="flex" alignItems="flex-end">
          <Typography className={classes.title}>Informations sur les Cookies</Typography>
        </Box>

        <ReactQuill
          theme="snow"
          className={classes.customReactQuill}
          onChange={descriptionChange}
          value={description || ''}
        />

        <CustomButton color="secondary" className={classes.btnSave} onClick={onClickSave}>
          ENREGISTRER
        </CustomButton>
      </Box>
    </Box>
  );
};
export default InformationsSurLesCookies;
