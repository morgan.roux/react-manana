import { useApolloClient } from '@apollo/react-hooks';
import React, { FC } from 'react';
import useStyles from './styles';
import { Box, Paper, Typography } from '@material-ui/core';
import { useGetRgpds } from '../../hooks';

interface ConditionProps {
  Content?: any;
}

const Condition: FC<ConditionProps> = ({}) => {
  const { data: getData, loading: getLoading } = useGetRgpds();
  const exist =
    (getData && getData.rgpds && getData.rgpds[0] && getData.rgpds[0].conditionUtilisation) || '';
  const client = useApolloClient();
  const classes = useStyles({});
  return (
    <>
      <Paper elevation={20} className={classes.onPaper}>
        <Box dangerouslySetInnerHTML={{ __html: exist }}></Box>
      </Paper>
    </>
  );
};
export default Condition;
