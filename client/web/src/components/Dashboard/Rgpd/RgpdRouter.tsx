import React, { FC, useState, ChangeEvent } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import Accueil from './Accueil/Accueil';
import Autorisation from './Autorisation/Autorisation';
import Partenaires from './Partenaires/Partenaires';
import PolitiqueDeConfidentialite from './PolitiqueDeConfidentialite/PolitiqueDeConfidentialite';
import ConditionsDutilisations from './ConditionsDutilisations/ConditionsDutilisations';
import InformationsSurLesCookies from './InformationsSurLesCookies/InformationsSurLesCookies';

const RgpdRouter: FC = () => {
  return (
    <Switch>
      <Route
        path={['/db/rgpd', '/db/rgpd/accueil']}
        exact={true}
        component={Accueil}
        render={() => <Accueil />}
      />
      <Route path={['/db/rgpd', '/db/rgpd/autorisation']} exact={true} component={Autorisation} />
      <Route path={['/db/rgpd', '/db/rgpd/partenaires']} exact={true} component={Partenaires} />
      <Route
        path={['/db/rgpd', '/db/rgpd/politique-de-confidentialite']}
        exact={true}
        component={PolitiqueDeConfidentialite}
      />
      <Route
        path={['/db/rgpd', '/db/rgpd/conditions-d-utilisations']}
        exact={true}
        component={ConditionsDutilisations}
      />
      <Route
        path={['/db/rgpd', '/db/rgpd/informations-sur-les-cookies']}
        exact={true}
        component={InformationsSurLesCookies}
      />
    </Switch>
  );
};

export default withRouter(RgpdRouter);
