import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      letterSpacing: 0,
      opacity: 1,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
      font: 'normal normal bold 16px/19px Roboto',
    },
    titlePaper: {
      letterSpacing: 0,
      fontFamily: 'Montserrat',
      fontWeight: 'bold',
    },
    onPaper: {
      padding: 20,
      width: '48%',
      minHeight: 188,
      minWidth: 340,
      marginTop: theme.spacing(3),
      borderRadius: '6px',
      background: '#FFFFFF 0% 0% no-repeat padding-box',
      '&:nth-child(2n)': {
        marginLeft: theme.spacing(3),
      },
      '@media (max-width: 1385px)': {
        '&:nth-child(2n)': {
          marginLeft: theme.spacing(0),
        },
        width: '95%',
      },
    },
    icon: {
      display: 'flex',
      alignItems: 'center',
      fontFamily: 'Montserrat',
      fontSize: '14px',
      fontWeight: 'bold',
      color: theme.palette.secondary.main,
      marginTop: 12,
      cursor: 'pointer',
      width: 'fit-content',
    },
    contentText: {
      '& p': {
        color: '#9E9E9E',
        fontSize: '1rem',
      },
    },
  }),
);

export default useStyles;
