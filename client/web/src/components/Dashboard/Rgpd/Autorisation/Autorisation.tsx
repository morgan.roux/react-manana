import { IconButton, Typography } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import React, { FC, useState } from 'react';
import useStyles from './styles';
import Add from '@material-ui/icons/Add';
import { withRouter } from 'react-router';
import AutorisationModal from './AutorisationModal/AutorisationModal';
import { useApolloClient, useMutation, useQuery } from '@apollo/react-hooks';
import { GET_AUTHORIZATION_LIST } from '../../../../graphql/Authorization/query';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

import PaperItem from '../Common/PaperItem/PaperItem';
import Backdrop from '../../../Common/Backdrop/Backdrop';
import { AUTHORIZATION_LIST } from '../../../../graphql/Authorization/types/AUTHORIZATION_LIST';
import { GET_RGPD_AUTORISATIONS } from '../../../../graphql/Rgpd';
import { RGPD_AUTORISATIONS } from '../../../../graphql/Rgpd/types/RGPD_AUTORISATIONS';

interface AutorisationProps {}
const Autorisation: FC<AutorisationProps> = () => {
  const classes = useStyles({});
  const [openModal, setOpenModal] = useState(false);

  const client = useApolloClient();

  //authorizationList query
  const { data: authorizationData, loading: autorizationLoading } = useQuery<
    RGPD_AUTORISATIONS,
    any
  >(GET_RGPD_AUTORISATIONS, {
    onError: error => {
      displaySnackBar(client, {
        type: 'ERROR',
        message: error.message,
        isOpen: true,
      });
    },
  });
  const autorisations = authorizationData && authorizationData.rgpdAutorisations;

  const handleClick = event => {
    // openModal
    event.stopPropagation();
    setOpenModal(true);
  };

  return (
    <>
      <Box className={classes.root}>
        <Box display="flex" flexDirection="column" width="100%">
          <Box display="flex" alignItems="flex-end">
            <Typography className={classes.title}>Autorisation</Typography>
          </Box>
          <Typography className={classes.icon} onClick={handleClick}>
            <Add />
            Ajouter une autorisation
          </Typography>
        </Box>
        {autorizationLoading && <Backdrop value="Chargement en cours, veuillez patienter ..." />}
        <Box className={classes.contentPaperItem}>
          {autorisations &&
            autorisations.length > 0 &&
            autorisations.map(autorisation => (
              <PaperItem
                key={autorisation.id}
                title={autorisation.title}
                content={autorisation.description}
                item={autorisation}
                isOnAutorisation={true}
              />
            ))}
        </Box>
        <Box>
          <AutorisationModal
            setOpen={setOpenModal}
            open={openModal}
            title="Ajouter une autorisation"
            closeIcon={true}
            withHeaderColor={true}
            isOnCreate={true}
          />
        </Box>
      </Box>
    </>
  );
};
export default withRouter(Autorisation);
