import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { Fade, IconButton, ListItemIcon, Menu, MenuItem, Typography } from '@material-ui/core';
import { Delete, Edit, MoreHoriz } from '@material-ui/icons';
import AddIcon from '@material-ui/icons/Add';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import React, { useState } from 'react';
import { DO_REMOVE_AUTHORIZATION } from '../../../../../graphql/Authorization/mutation';
import { GET_AUTHORIZATION_LIST } from '../../../../../graphql/Authorization/query';
import { AUTHORIZATION_LIST } from '../../../../../graphql/Authorization/types/AUTHORIZATION_LIST';
import {
  REMOVE_AUTHORIZATION,
  REMOVE_AUTHORIZATIONVariables,
} from '../../../../../graphql/Authorization/types/REMOVE_AUTHORIZATION';
import { GET_RGPD_AUTORISATIONS } from '../../../../../graphql/Rgpd/query';
import { RGPD_AUTORISATIONS } from '../../../../../graphql/Rgpd/types/RGPD_AUTORISATIONS';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import Backdrop from '../../../../Common/Backdrop';
import { ConfirmDeleteDialog } from '../../../../Common/ConfirmDialog';
import { ActionButtonMenu } from '../../../../Main/Content/TodoNew/Common/ActionButton';
import { operationVariable } from '../../../../Main/Content/TodoNew/MainContent/MainContent';
import AutorisationModal from '../AutorisationModal/AutorisationModal';

interface RgpdActionButtonProps {
  rgpd?: any;
  refetch?: any;
  item: any;
}
const AutorisationActionButton: React.FC<RgpdActionButtonProps> = ({ item }) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = anchorEl ? true : false;
  const [openEditModal, setOpenEditModal] = useState<boolean>(false);
  const [openDeleteDialogRgpd, setOpenDeleteDialogRgpd] = useState<boolean>(false);
  const [operationName, setOperationName] = useState<string>('');

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    if (event) {
      event.stopPropagation();
      setAnchorEl(event.currentTarget);
    }
  };
  const client = useApolloClient();

  // delete autorisation
  const [deleteAutorisation] = useMutation<REMOVE_AUTHORIZATION, REMOVE_AUTHORIZATIONVariables>(
    DO_REMOVE_AUTHORIZATION,
    {
      onCompleted: () => {
        displaySnackBar(client, {
          type: 'SUCCESS',
          message: 'Autorisation supprimée avec succès',
          isOpen: true,
        });
        setOpenDeleteDialogRgpd(false);
      },
      onError: error => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: error.message,
          isOpen: true,
        });
      },
      update: (cache, { data }) => {
        if (cache && data) {
          const req: any = cache.readQuery({ query: GET_RGPD_AUTORISATIONS });
          if (req) {
            const idTofilter =
              data && data.deleteRgpdAutorisation && data.deleteRgpdAutorisation.id;
            const result = [...req.rgpdAutorisations];
            const resultFiltered = result.filter(item => idTofilter !== item.id);
            console.log('result deleted===>>', data);
            cache.writeQuery<RGPD_AUTORISATIONS, any>({
              query: GET_RGPD_AUTORISATIONS,
              data: {
                rgpdAutorisations: [...resultFiltered],
              },
            });
          }
        }
      },
    },
  );
  const onConfirmDelete = () => {
    deleteAutorisation({ variables: { id: item.id } });
  };
  const handleDeleteAutorisation = () => {
    setOpenDeleteDialogRgpd(true);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  // modification autorisation

  const handleOpenModalRgpdAction = () => {
    setOpenEditModal(true);
  };

  const rgpdlistMenuItems: ActionButtonMenu[] = [
    {
      label: 'Modifier',
      icon: <Edit />,
      onClick: handleOpenModalRgpdAction,
      disabled: false,
    },
    {
      label: 'Supprimer',
      icon: <Delete />,
      onClick: handleDeleteAutorisation,
      disabled: false,
    },
  ];

  return (
    <>
      <IconButton
        aria-controls="simple-menu"
        size="small"
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreHoriz />
      </IconButton>
      <Menu
        // tslint:disable-next-line: jsx-no-lambda
        onClick={event => {
          event.preventDefault();
          event.stopPropagation();
        }}
        id="fade-menu"
        anchorEl={anchorEl}
        keepMounted={true}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {rgpdlistMenuItems &&
          rgpdlistMenuItems.length > 0 &&
          rgpdlistMenuItems.map((i: ActionButtonMenu, index: number) => (
            <MenuItem
              // tslint:disable-next-line: jsx-no-lambda
              onClick={event => {
                event.preventDefault();
                event.stopPropagation();
                handleClose();
                i.onClick(event);
              }}
              key={`table_menu_item_${index}`}
              disabled={i.disabled}
            >
              <ListItemIcon>{i.icon}</ListItemIcon>
              <Typography variant="inherit">{i.label}</Typography>
            </MenuItem>
          ))}
      </Menu>
      <ConfirmDeleteDialog
        open={openDeleteDialogRgpd}
        onClickConfirm={onConfirmDelete}
        setOpen={setOpenDeleteDialogRgpd}
      />
      <AutorisationModal
        setOpen={setOpenEditModal}
        open={openEditModal}
        title="Modifier une autorisation"
        closeIcon={true}
        withHeaderColor={true}
        withButtonAction={true}
        item={item}
        isOnCreate={false}
      />
    </>
  );
};

export default AutorisationActionButton;
