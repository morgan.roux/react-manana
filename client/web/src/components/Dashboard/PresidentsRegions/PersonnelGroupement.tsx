import React, { FC, useState, useEffect, useMemo } from 'react';
import useStyles from './styles';
import PersonnelGroupementHead from './PersonnelGroupementHead';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import PersonnelGroupementList from './PersonnelGroupementList';
import PersonnelGroupementForm from './PersonnelGroupementForm';
import usePersonnelGroupement from './PersonnelGroupementForm/usePersonnelGroupementForm';
import { last } from 'lodash';
import { useMutation, useApolloClient, useLazyQuery } from '@apollo/react-hooks';
import { displaySnackBar } from '../../../utils/snackBarUtils';
import Backdrop from '../../Common/Backdrop';
import { getGroupement } from '../../../services/LocalStorage';
import { DO_CREATE_PERSONNEL, GET_PERSONNEL } from '../../../graphql/Personnel';
import { PERSONNEL, PERSONNELVariables } from '../../../graphql/Personnel/types/PERSONNEL';
import {
  CREATE_PERSONNELVariables,
  CREATE_PERSONNEL,
} from '../../../graphql/Personnel/types/CREATE_PERSONNEL';
import mutationSuccessImg from '../../../assets/img/mutation-success.png';
import NoItemContentImage from '../../Common/NoItemContentImage';
import CustomButton from '../../Common/CustomButton';
import { PERSONNEL_GROUPEMENT_URL } from '../../../Constant/url';

const PersonnelGroupement: FC<RouteComponentProps> = ({
  location: { pathname },
  match: { params },
  history: { push },
}) => {
  const classes = useStyles({});
  const client = useApolloClient();
  const groupement = getGroupement();
  const idGroupement = groupement && groupement.id;

  const { personnelId } = params as any;
  const EDIT_URL = `/db/${PERSONNEL_GROUPEMENT_URL}/edit/${personnelId}`;

  const [selected, setSelected] = useState<any[]>([]);
  const [selectedFiles, setselectedFiles] = useState<File[]>([]);
  const [croppedFiles, setCroppedFiles] = useState<File[]>([]);
  const [imageUrl, setImageUrl] = useState<string | null | undefined>(undefined);
  const [deleteRow, setDeleteRow] = useState<any>(null);
  const [openDeleteDialog, setOpenDeleteDialog] = useState<boolean>(false);
  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);

  const { handleChange, values, setValues } = usePersonnelGroupement();

  const { civilite, nom, sexe, codeService } = values;
  // const { civilite, nom, sexe, mailProf, telMobProf, codeService } = values;

  const isOnList: boolean = pathname === `/db/${PERSONNEL_GROUPEMENT_URL}`;
  const isOnCreate: boolean = pathname === `/db/${PERSONNEL_GROUPEMENT_URL}/create`;
  const isOnEdit: boolean = pathname.startsWith(`/db/${PERSONNEL_GROUPEMENT_URL}/edit`);

  const avatarImage = selectedFiles && selectedFiles.length > 0 && last(selectedFiles);
  const avatarImageSrc = imageUrl || (avatarImage && URL.createObjectURL(avatarImage));

  const title = isOnList
    ? 'Liste du personnel'
    : `${isOnCreate ? 'Ajout ' : 'Modification'} d'un personnel du groupement`;

  const loadingMsg = `${
    isOnCreate ? `Création` : isOnEdit ? `Modification` : `Chargement`
  } en cours...`;

  const mutationSuccessTitle = `${isOnCreate ? 'Ajout' : 'Modification'} du personnel réussi`;
  const mutationSuccessSubTitle = `Le nouvel utilisateur que vous venez ${
    isOnCreate ? "d'ajouter" : 'de modifier'
  } est désormais dans la liste du personnel du groupement.`;

  const formIsValid = (): boolean => {
    if (civilite && nom && sexe && codeService) {
      return true;
    }
    return false;
  };

  const onClickSaveAvatar = () => {
    if (formIsValid()) {
      createUpdatePersonnel({ variables: { ...values, sexe: sexe as any, idGroupement } });
    }
  };

  /**
   * Mutation createUpdateAvatar
   */
  const [createUpdatePersonnel, { loading: mutationLoading }] = useMutation<
    CREATE_PERSONNEL,
    CREATE_PERSONNELVariables
  >(DO_CREATE_PERSONNEL, {
    onCompleted: data => {
      if (data && data.createUpdatePersonnel) {
        setMutationSuccess(true);
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
      displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
    },
  });

  // EDIT
  /**
   * Get personnel for edit
   */
  const [getPersonnel, { loading: personnelLoading }] = useLazyQuery<PERSONNEL, PERSONNELVariables>(
    GET_PERSONNEL,
    {
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, {
            type: 'ERROR',
            message: err.message,
            isOpen: true,
          });
        });
      },
      onCompleted: data => {
        if (data && data.personnel) {
          const personnel = data.personnel;
          // Set values for edit
          if (personnel) {
            setValues({
              id: personnel.id,
              codeService: (personnel.service && personnel.service.code) || '',
              respHierarch: null,
              sortie: personnel.sortie as any,
              dateSortie: personnel.dateSortie,
              civilite: personnel.civilite,
              nom: personnel.nom,
              prenom: personnel.prenom,
              sexe: personnel.sexe,
              // adresse1: personnel.contact ? personnel.contact.adresse1 : '',
              // adresse2: personnel.contact ? personnel.contact.adresse2 : '',
              // telBureau: personnel.contact ? personnel.contact.telProf : '',
              // telDomicile: personnel.contact ? personnel.contact.telPerso : '',
              // telMobProf: personnel.contact ? personnel.contact.telMobProf : '',
              // telMobPerso: personnel.contact ? personnel.contact.telMobPerso : '',
              // faxBureau: personnel.contact ? personnel.contact.faxProf : '',
              // faxDomicile: personnel.contact ? personnel.contact.faxPerso : '',
              // cp: personnel.contact ? personnel.contact.cp : '',
              // ville: personnel.contact ? personnel.contact.ville : '',
              // mailProf: personnel.contact ? personnel.contact.mailProf : '',
              // mailPerso: personnel.contact ? personnel.contact.mailPerso : '',
              commentaire: personnel.commentaire,
              idGroupement: personnel.idGroupement,
            });
          }
        }
      },
    },
  );

  /**
   * Get personnel for edit
   */
  useEffect(() => {
    if (pathname === EDIT_URL && personnelId) {
      getPersonnel({ variables: { id: personnelId } });
    }
  }, [pathname, personnelId]);

  /**
   * Set deleteRow to null
   */
  useMemo(() => {
    if (deleteRow) {
      setDeleteRow(null);
    }
  }, [selected]);

  const goToList = () => {
    push(`/db/${PERSONNEL_GROUPEMENT_URL}`);
  };

  const disabledSaveBtn = (): boolean => {
    if (civilite && nom && codeService) {
      return false;
    }
    return true;
  };

  const isLoading = (): boolean => {
    if (mutationLoading || personnelLoading) {
      return true;
    }
    return false;
  };

  return (
    <div className={classes.personnelGroupementRoot}>
      {isLoading() && <Backdrop value={personnelLoading ? 'Chargement en cours...' : loadingMsg} />}
      {mutationSuccess ? (
        <div className={classes.mutationSuccessContainer}>
          <NoItemContentImage
            src={mutationSuccessImg}
            title={mutationSuccessTitle}
            subtitle={mutationSuccessSubTitle}
          >
            <CustomButton color="default" onClick={goToList}>
              Retour à la liste
            </CustomButton>
          </NoItemContentImage>
        </div>
      ) : (
        <>
          <PersonnelGroupementHead
            title={title}
            isOnList={isOnList}
            isOnCreate={isOnCreate}
            isOnEdit={isOnEdit}
            onClickSaveAvatar={onClickSaveAvatar}
            selected={selected}
            setSelected={setSelected}
            deleteRow={deleteRow}
            setDeleteRow={setDeleteRow}
            openDeleteDialog={openDeleteDialog}
            setOpenDeleteDialog={setOpenDeleteDialog}
            disabledSaveBtn={disabledSaveBtn()}
          />
          {isOnList && (
            <PersonnelGroupementList
              selected={selected}
              setSelected={setSelected}
              deleteRow={deleteRow}
              setDeleteRow={setDeleteRow}
              openDeleteDialog={openDeleteDialog}
              setOpenDeleteDialog={setOpenDeleteDialog}
            />
          )}
          {!isOnList && (
            <PersonnelGroupementForm
              title={title}
              selectedFiles={selectedFiles}
              setSelectedFiles={setselectedFiles}
              handleChangeInput={handleChange}
              values={values as any}
              imageUrl={imageUrl}
              setImageUrl={setImageUrl}
              croppedFiles={croppedFiles}
              setCroppedFiles={setCroppedFiles}
              avatarImageSrc={avatarImageSrc as any}
              isOnCreate={isOnCreate}
              isOnEdit={isOnEdit}
            />
          )}
        </>
      )}
    </div>
  );
};

export default withRouter(PersonnelGroupement);
