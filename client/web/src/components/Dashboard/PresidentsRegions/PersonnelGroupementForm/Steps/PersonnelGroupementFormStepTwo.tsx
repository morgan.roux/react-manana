import React, { FC, useState, useEffect, useCallback } from 'react';
import useStyles from '../styles';
import { Typography } from '@material-ui/core';
import { CustomFormTextField } from '../../../../Common/CustomTextField';
import CustomSelect from '../../../../Common/CustomSelect';
import Dropzone from '../../../../Common/Dropzone';
import { PersonnelGroupementFormProps } from '../PersonnelGroupementForm';
import { AWS_HOST } from '../../../../../utils/s3urls';
import { last } from 'lodash';
import { CustomCheckbox } from '../../../../Common/CustomCheckbox';
import classnames from 'classnames';

const PersonnelGroupementFormStepTwo: FC<PersonnelGroupementFormProps> = ({
  title,
  selectedFiles,
  setSelectedFiles,
  values,
  handleChangeInput,
  imageUrl,
  setImageUrl,
  // croppedFiles,
  setCroppedFiles,
  avatarImageSrc,
}) => {
  const classes = useStyles({});

  const permissionAccessList = [
    { label: 'Recherche des Pharmacies', name: 'searchPharma' },
    { label: 'Modification des Pharmacies', name: 'editPharma' },
    { label: 'Recherche des Titulaires', name: 'searchTit' },
  ];

  return (
    <div className={classes.infoPersoContainer}>
      <Typography className={classes.inputTitle}>Droits d'accès</Typography>
      <Typography className={classes.permissionAccessSubTite}>
        Choisissez les accès auxquels la personne aura droit
      </Typography>
      <div className={classes.inputsContainer}>
        <div className={classes.permissionAccessFormRow}>
          {permissionAccessList.map((i, index) => (
            <CustomCheckbox
              key={`permission_access_${index}`}
              label={i.label}
              name={i.name}
              // value={avecPalier}
              // checked={avecPalier}
              // onChange={handleChange}
              color="secondary"
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default PersonnelGroupementFormStepTwo;
