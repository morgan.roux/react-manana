import { useState, ChangeEvent } from 'react';
import { CREATE_PERSONNELVariables } from '../../../../graphql/Personnel/types/CREATE_PERSONNEL';

export interface PersonnelGroupementInterface {
  id: string | null;
  codeService: string;
  respHierarch: string | null;
  sortie: number;
  dateSortie: any;
  civilite: string | null;
  nom: string | null;
  prenom: string | null;
  sexe: string | null;
  adresse1: string | null;
  adresse2: string | null;
  telBureau: string | null;
  telDomicile: string | null;
  telMobProf: string | null;
  telMobPerso: string | null;
  faxBureau: string | null;
  faxDomicile: string | null;
  cp: string | null;
  ville: string | null;
  mailProf: string | null;
  mailPerso: string | null;
  commentaire: string | null;
  idGroupement: string | null;
}

export const initialState: CREATE_PERSONNELVariables = {
  id: null,
  codeService: '',
  respHierarch: null,
  sortie: 0,
  dateSortie: null,
  civilite: null,
  nom: null,
  prenom: null,
  sexe: null,
  commentaire: null,
  idGroupement: null,
  contact: {
    adresse1: null,
    adresse2: null,
    cp: null,
    faxPerso: null,
    faxProf: null,
    mailPerso: null,
    mailProf: null,
    telPerso: null,
    telProf: null,
    ville: null,
    compteSkypePerso: null,
    compteSkypeProf: null,
    pays: null,
    sitePerso: null,
    siteProf: null,
    telMobPerso: null,
    telMobProf: null,
    urlFacebookPerso: null,
    urlFacebookProf: null,
    urlLinkedinPerso: null,
    urlLinkedinProf: null,
    urlTwitterPerso: null,
    urlTwitterProf: null,
    whatsappMobPerso: null,
    whatsAppMobProf: null,
  },
};

const usePersonnelGroupementForm = (defaultState?: CREATE_PERSONNELVariables) => {
  const initValues: CREATE_PERSONNELVariables = defaultState || initialState;
  const [values, setValues] = useState<CREATE_PERSONNELVariables>(initValues);
  const currentContact = values.contact as any;
  const handleChange = (e: ChangeEvent<any>) => {
    if (e && e.target) {
      const { name, value } = e.target;
      const isInContact = name in currentContact;
      if (isInContact) {
        setValues(prevState => ({
          ...prevState,
          contact: { ...prevState.contact, [name]: value },
        }));
      } else {
        setValues(prevState => ({ ...prevState, [name]: value }));
      }
    }
  };

  return {
    handleChange,
    values,
    setValues,
  };
};

export default usePersonnelGroupementForm;
