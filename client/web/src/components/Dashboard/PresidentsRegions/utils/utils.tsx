import {
  QueryLazyOptions,
  useApolloClient,
  useLazyQuery,
  useMutation,
  useQuery,
} from '@apollo/react-hooks';
import { differenceBy } from 'lodash';
import moment from 'moment';
import React, { Fragment, useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { ContentContext, ContentStateInterface } from '../../../../AppContext';
import {
  PERSONNEL_GROUPEMENT_URL,
  PHARMACIE_URL,
  PRESIDENT_REGION_URL,
} from '../../../../Constant/url';
import {
  DO_CREATE_PERSONNEL,
  DO_DELETE_PERSONNELS,
  GET_PERSONNEL,
} from '../../../../graphql/Personnel';
import {
  CREATE_PERSONNEL,
  CREATE_PERSONNELVariables,
} from '../../../../graphql/Personnel/types/CREATE_PERSONNEL';
import {
  DELETE_PERSONNELS,
  DELETE_PERSONNELSVariables,
} from '../../../../graphql/Personnel/types/DELETE_PERSONNELS';
import {
  PERSONNEL,
  PERSONNELVariables,
  PERSONNEL_personnel,
} from '../../../../graphql/Personnel/types/PERSONNEL';
import { GET_CHECKEDS_PRESIDENT_REGION } from '../../../../graphql/Ppersonnel/local';
import { GET_CHECKEDS_PRESIDENT_AFFECTATION } from '../../../../graphql/PpersonnelAffectation/local';
import { DO_AFFECT_PRESIDENT } from '../../../../graphql/PpersonnelAffectation/mutation';
import {
  AFFECT_PRESIDENT,
  AFFECT_PRESIDENTVariables,
} from '../../../../graphql/PpersonnelAffectation/types/AFFECT_PRESIDENT';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { Column } from '../../../Common/newCustomContent/interfaces';
import UserStatus from '../../Content/UserStatus';
import TableActionColumn from '../../TableActionColumn';

/**
 *
 *  Create personnel hooks
 *
 */
export const useCreatePersonnel = (mutationVariables: any): [(variables?: any) => any, boolean] => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);

  const client = useApolloClient();

  const [mutationSuccess, setMutationSuccess] = useState<boolean>(false);

  const [createUpdatePersonnel] = useMutation<CREATE_PERSONNEL, CREATE_PERSONNELVariables>(
    DO_CREATE_PERSONNEL,
    {
      update: (cache, { data }) => {
        if (
          data &&
          data.createUpdatePersonnel &&
          mutationVariables &&
          mutationVariables.variables &&
          !mutationVariables.variables.id
        ) {
          if (variables && operationName) {
            const req: any = cache.readQuery({
              query: operationName,
              variables,
            });
            if (req && req.search && req.search.data) {
              cache.writeQuery({
                query: operationName,
                data: {
                  search: {
                    ...req.search,
                    ...{
                      total: req.search.total + 1,
                      data: [...req.search.data, data.createUpdatePersonnel],
                    },
                  },
                },

                variables,
              });
            }
          }
        }
      },
      onCompleted: data => {
        if (data && data.createUpdatePersonnel) {
          setMutationSuccess(true);
        }
      },
      onError: errors => {
        errors.graphQLErrors.map(err => {
          displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
        });
        displaySnackBar(client, { type: 'ERROR', message: errors.message, isOpen: true });
      },
    },
  );

  return [createUpdatePersonnel, mutationSuccess];
};

/**
 *
 *  Delete personnel hooks
 *
 */

export const useDeletePresident = () => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);
  const checkeds = useCheckedPresident();

  const client = useApolloClient();
  const [deletePersonnels, { loading, data: deleteData }] = useMutation<
    DELETE_PERSONNELS,
    DELETE_PERSONNELSVariables
  >(DO_DELETE_PERSONNELS, {
    update: (cache, { data }) => {
      if (data && data.deletePersonnels) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            const source = req.search.data;
            const result = data.deletePersonnels;
            const dif = differenceBy(source, result, 'id');
            cache.writeQuery({
              query: operationName,
              data: {
                search: {
                  ...req.search,
                  ...{
                    data: dif,
                  },
                },
              },
              variables,
            });
            client.writeData({
              data: { checkedsPersonnelGroupement: differenceBy(checkeds, result, 'id') },
            });
            displaySnackBar(client, {
              type: 'SUCCESS',
              message:
                result && result.length > 1
                  ? 'Utilisateurs supprimés avec succès'
                  : 'Utilisateur supprimé avec succès',
              isOpen: true,
            });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });
  return deletePersonnels;
};

export const useAffectTitulaire = () => {
  const {
    content: { variables, operationName },
  } = useContext<ContentStateInterface>(ContentContext);
  const checkeds = useCheckedPresidentAffectation();

  const client = useApolloClient();
  const [affectTitulaire, { loading, data: affectData }] = useMutation<
    AFFECT_PRESIDENT,
    AFFECT_PRESIDENTVariables
  >(DO_AFFECT_PRESIDENT, {
    update: (cache, { data }) => {
      if (data && data.createUpdateTitulaireAffectation) {
        if (variables && operationName) {
          const req: any = cache.readQuery({
            query: operationName,
            variables,
          });
          if (req && req.search && req.search.data) {
            // const source = req.search.data;
            const result = data.createUpdateTitulaireAffectation;
            // const dif = differenceBy(source, result, 'id');
            // cache.writeQuery({
            //   query: operationName,
            //   data: {
            //     search: {
            //       ...req.search,
            //       ...{
            //         data: dif,
            //       },
            //     },
            //   },
            //   variables: variables,
            // });
            // client.writeData({
            //   data: { checkedsPersonnelGroupement: differenceBy(checkeds, result, 'id') },
            // });
            displaySnackBar(client, {
              type: 'SUCCESS',
              message: 'Utilisateur affecté avec succès',
              isOpen: true,
            });
          }
        }
      }
    },
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, { type: 'ERROR', message: err.message, isOpen: true });
      });
    },
  });
  return affectTitulaire;
};

/**
 *
 *  list checked personnel groupement hooks
 *
 */

export const useCheckedPresident = () => {
  const checkedPresidentRegion = useQuery(GET_CHECKEDS_PRESIDENT_REGION);
  return (
    (checkedPresidentRegion &&
      checkedPresidentRegion.data &&
      checkedPresidentRegion.data.checkedsPresidentRegion) ||
    []
  );
};

export const useCheckedPresidentAffectation = () => {
  const checkedPresidentAffectation = useQuery(GET_CHECKEDS_PRESIDENT_AFFECTATION);
  return (
    (checkedPresidentAffectation &&
      checkedPresidentAffectation.data &&
      checkedPresidentAffectation.data.checkedsPresidentAffecation) ||
    []
  );
};

/**
 *
 *  subtoolbar button action hooks
 *
 */

export const useButtonHeadAction = (push: any) => {
  const goToAddPersonnel = () => push(`/db/${PERSONNEL_GROUPEMENT_URL}/create`);
  const goBack = () => push(`/db/${PERSONNEL_GROUPEMENT_URL}`);

  return [goToAddPersonnel, goBack];
};

/**
 *  get personnel hooks
 */

export const useGetPersonnel = (
  fetchPolicy?:
    | 'cache-first'
    | 'network-only'
    | 'cache-only'
    | 'no-cache'
    | 'standby'
    | 'cache-and-network',
): [
  (options?: QueryLazyOptions<PERSONNELVariables> | undefined) => void,
  PERSONNEL_personnel | null | undefined,
] => {
  const client = useApolloClient();
  const [getPersonnel, { data, loading: personnelLoading }] = useLazyQuery<
    PERSONNEL,
    PERSONNELVariables
  >(GET_PERSONNEL, {
    fetchPolicy: fetchPolicy || 'cache-first',
    onError: errors => {
      errors.graphQLErrors.map(err => {
        displaySnackBar(client, {
          type: 'ERROR',
          message: err.message,
          isOpen: true,
        });
      });
    },
  });
  const personnel = data && data.personnel;
  return [getPersonnel, personnel];
};

export const usePresidentRegionColumns = (
  setOpenDeleteDialog: any,
  push: any,
  classes: any,
): [Column[], any, () => void] => {
  const deletePresidents = useDeletePresident();
  const [deleteRow, setDeleteRow] = useState<any>();
  const checkedsPresidentRegion = useCheckedPresident();
  const client = useApolloClient();
  const pathname = '/db/presidents-regions';
  const onClickDelete = (row: any) => {
    setDeleteRow(row);
  };

  const onClickConfirmDelete = () => {
    setOpenDeleteDialog(false);
    if (checkedsPresidentRegion && checkedsPresidentRegion.length > 0) {
      const ids: string[] = checkedsPresidentRegion.map(i => i.id);
      deletePresidents({ variables: { ids } });
    }

    if (deleteRow && deleteRow.id) {
      deletePresidents({ variables: { ids: [deleteRow.id] } });
    }
  };

  const titulaireColumns = [
    {
      name: 'email',
      label: 'Email',
      renderer: (value: any) => {
        return value && value.contact && value.contact.mailProf ? value.contact.mailProf : '-';
      },
    },
    {
      name: 'civilite',
      label: 'Civilité',
      renderer: (value: any) => {
        return value && value.civilite ? value.civilite : '-';
      },
    },
    {
      name: 'nomGroupe',
      label: 'Nom du groupe',
      renderer: (value: any) => {
        return (value && value.fullName) || '-';
      },
    },
    {
      name: 'pharmacie',
      label: 'Pharmacie(s)',
      renderer: (value: any) => {
        const pharmacies: any[] = (value && value.pharmacies) || [];
        return (
          <div className={classes.pharmaciesLinkContainer}>
            {pharmacies.map((pharmacie: any, index: number) => {
              if (pharmacie) {
                const goToPharmacie = () => {
                  push(`/db/${PHARMACIE_URL}/fiche/${pharmacie.id}`);
                };
                return (
                  <Fragment key={`titulaire_pharma_${index}`}>
                    <Link color="inherit" to={`/db/${PHARMACIE_URL}/fiche/${pharmacie.id}`}>
                      {pharmacie.nom || '-'}
                    </Link>
                    <span> / </span>
                  </Fragment>
                );
              } else {
                return '-';
              }
            })}
          </div>
        );
      },
    },
    {
      name: 'departement.nom',
      label: 'Département',
      renderer: (value: any) => {
        return value && value.departement ? value.departement.nom : '-';
      },
    },
    {
      name: 'contact.cp',
      label: 'Code postal',
      renderer: (value: any) => {
        return value && value.contact ? value.contact.cp : '-';
      },
    },
    {
      name: 'contact.ville',
      label: 'Ville',
      renderer: (value: any) => {
        return value && value.contact ? value.contact.ville : '-';
      },
    },
    {
      name: 'user.status',
      label: 'Statut',
      renderer: (value: any) => {
        const valueUser =
          value.users &&
          value.pharmacies &&
          value.pharmacies.length === 1 &&
          value.users.length === 1
            ? value.users[0]
            : null;
        return valueUser ? <UserStatus user={valueUser} pathname={pathname} /> : '-';
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <TableActionColumn
            row={value}
            baseUrl={PRESIDENT_REGION_URL}
            setOpenDeleteDialog={setOpenDeleteDialog}
            handleClickDelete={onClickDelete}
          />
        );
      },
    },
  ];
  return [titulaireColumns, deleteRow, onClickConfirmDelete];
};

export const useAffectationColumns = (
  setOpenAffectationDialog: any,
): [Column[], any, (value: any) => any] => {
  const [presidentToAffect, setPresidentToAffect] = useState<any>({});
  // const deletePersonnels = useDeletePresident();
  // const checkedsPresidentAffectation = useCheckedPresidentAffectation();

  const onClickAffectation = (action: any, row: any) => {
    setOpenAffectationDialog(true);
    setPresidentToAffect({ ...row, action });
  };

  // const onClickConfirmDelete = () => {
  //   setOpenDeleteDialog(false);
  //   if (checkedsPersonnelGroupement && checkedsPersonnelGroupement.length > 0) {
  //     const ids: string[] = checkedsPersonnelGroupement.map(i => i.id);
  //     deletePersonnels({ variables: { ids } });
  //   }

  //   if (deleteRow && deleteRow.id) {
  //     deletePersonnels({ variables: { ids: [deleteRow.id] } });
  //   }
  // };

  const AffectationColumns = [
    {
      name: 'titulaireFonction.nom',
      label: 'Fonctions',
      renderer: (value: any) => {
        return (value && value.titulaireFonction && value.titulaireFonction.nom) || '-';
      },
    },
    {
      name: 'departement.nom',
      label: 'Nom du département',
      renderer: (value: any) => {
        return (
          `${value && value.departement && value.departement.nom} (${value &&
            value.departement &&
            value.departement.code})` || '-'
        );
      },
    },
    {
      name: 'titulaire.fullName',
      label: 'Affectation actuelle',
      renderer: (value: any) => {
        return (value && value.titulaire && value.titulaire.fullName) || '-';
      },
    },
    {
      name: 'titulaire.dateDebut',
      label: 'Date de début',
      renderer: (value: any) => {
        return '-';
      },
    },
    {
      name: 'titulaire.dateFin',
      label: 'Date de fin',
      renderer: (value: any) => {
        return '-';
      },
    },
    {
      name: 'titulaireDemandeAffectation.titulaireRemplacent.fullName',
      label: 'Remplacant',
      renderer: (value: any) => {
        return (
          (value &&
            value.titulaireDemandeAffectation &&
            value.titulaireDemandeAffectation.titulaireRemplacent &&
            value.titulaireDemandeAffectation.titulaireRemplacent.fullName) ||
          '-'
        );
      },
    },
    {
      name: 'dateDebut',
      label: 'Date de début',
      renderer: (value: any) => {
        return <>{value && value.dateDebut && moment(value.dateDebut).format('L')}</>;
      },
    },
    {
      name: 'dateFin',
      label: 'Date de fin',
      renderer: (value: any) => {
        return <>{value && value.dateDebut && moment(value.dateFin).format('L')}</>;
      },
    },
    {
      name: '',
      label: '',
      renderer: (value: any) => {
        return (
          <TableActionColumn
            row={value}
            baseUrl={`${PRESIDENT_REGION_URL}/affectation`}
            handleClickAffectation={onClickAffectation}
          />
        );
      },
    },
  ];

  return [AffectationColumns, presidentToAffect, setPresidentToAffect];
};
