import ButtonResendEmail from './ButtonResendEmail';
import MenuItemResendEmail from './MenuItemResendEmail';

export { MenuItemResendEmail };

export default ButtonResendEmail;
