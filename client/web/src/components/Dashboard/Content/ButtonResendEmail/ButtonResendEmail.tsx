import React, { FC } from 'react';
import { IconButton } from '@material-ui/core';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import Tooltip from '@material-ui/core/Tooltip';
import Fade from '@material-ui/core/Fade';
import SendIcon from '@material-ui/icons/Send';
import {
  RESEND_USER_EMAIL,
  RESEND_USER_EMAILVariables,
} from '../../../../graphql/User/types/RESEND_USER_EMAIL';
import { DO_RESEND_USER_EMAIL } from '../../../../graphql/User/mutation';
import { LoaderSmall } from '../Loader';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';

interface ResendProps {
  email: string;
  login: string;
}

const ButtonResendEmail: FC<ResendProps> = ({ email, login }) => {
  const client = useApolloClient();

  const [doResendMail, { loading }] = useMutation<RESEND_USER_EMAIL, RESEND_USER_EMAILVariables>(
    DO_RESEND_USER_EMAIL,
    {
      onError: (error) => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: error.message,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      },
      onCompleted: (_) => {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Mail de définition de mot de passe re-envoyé avec succès`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
      },
    },
  );

  const resendMail = () => {
    doResendMail({
      variables: { email, login },
    });
  };

  if (loading) return <LoaderSmall />;

  return (
    <>
      <Tooltip
        TransitionComponent={Fade}
        TransitionProps={{ timeout: 600 }}
        title="Ré-envoyer l'email de demande d'activation"
      >
        <IconButton
          style={{ textTransform: 'none' }}
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={resendMail}
          disabled={email && login ? false : true}
        >
          <SendIcon />
        </IconButton>
      </Tooltip>
    </>
  );
};

export default ButtonResendEmail;
