import React, { FC } from 'react';
import PropTypes from 'prop-types';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TablePagination from '@material-ui/core/TablePagination';
import { Column } from './../Interface';
import { TableHeader } from './TableHeader';
import { TableBody } from './TableBody';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  root: {
    padding: theme.spacing(3),
  },
  table: {},
  tableWrapper: {
    overflowX: 'auto' as 'auto',
    margin: '0',
    minHeight: 200,
    height: 'calc(100vh - 280px)',
    '& *': {
      fontWeight: 'bold',
    },
    '& thead *': {
      fontSize: 12,
    },
  },
  tablePagination: {
    color: theme.palette.text.primary,
    '@media (max-width: 600px)': {
      '& .MuiTablePagination-toolbar': {
        display: 'flex !important',
        flexWrap: 'wrap',
        top: 10,
        justifyContent: 'space-evenly',
      },
    },
    '& .MuiTablePagination-selectRoot': {
      marginLeft: 5,
      marginRight: 10,
    },
  },
});

const useStyles1 = makeStyles((theme: Theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
    '& .MuiTableSortLabel-root.MuiTableSortLabel-active, & .MuiTableSortLabel-icon': {
      color: `${theme.palette.common.white} !important`,
    },
  },
}));

function TablePaginationActions(props: any) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event: any) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event: any) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event: any) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event: any) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

interface TableProps {
  type: string;
  data: any;
  count: number;
  rowsPerPageOptions: number[];
  rowsPerPage: number;
  page: number;
  columns?: Column[];
  order: 'desc' | 'asc';
  columnSorted: string;
  toggleSort: (name: string) => any;
  onChangePage: (page: number) => void;
  onChangeRowsPerPage: (rowsPerPage: number) => void;
}

const Content: FC<TableProps & WithStyles> = ({
  classes,
  type,
  columns,
  data,
  count,
  rowsPerPage,
  rowsPerPageOptions,
  page,
  onChangePage,
  onChangeRowsPerPage,
  toggleSort,
  order,
  columnSorted,
}) => {
  return (
    <div className="container-table">
      <div className={classes.tableWrapper}>
        <Table className={classes.table}>
          <TableHeader
            columns={columns}
            toggleSort={toggleSort}
            order={order}
            columnSorted={columnSorted}
          />
          <TableBody columns={columns} data={data} />
        </Table>
      </div>
      <TablePagination
        className={classes.tablePagination}
        component="div"
        count={count}
        rowsPerPage={rowsPerPage}
        rowsPerPageOptions={rowsPerPageOptions}
        page={page}
        onChangePage={(e, newPage) => {
          onChangePage(newPage);
        }}
        onChangeRowsPerPage={e => {
          onChangeRowsPerPage(parseInt(e.target.value, 10));
        }}
        labelDisplayedRows={({ from, to, count }) =>
          `${from}-${to === -1 ? count : to} sur ${count}`
        }
        labelRowsPerPage="nombre par page :"
        ActionsComponent={TablePaginationActions}
      />
    </div>
  );
};

export default withStyles(styles)(Content);
