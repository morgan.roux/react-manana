import React, { FC } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles';
import { TableCell, TableHead, TableRow, TableSortLabel } from '@material-ui/core';
import { Column } from './../../Interface';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  col: {
    // color: theme.palette.text.secondary,
    cursor: 'pointer',
    padding: '12px 16px 12px 30px',
    borderBottom: 'inherit',
  },
});
interface TableHeaderProps {
  columns?: Column[];
  toggleSort: (name: string) => {};
  order: 'desc' | 'asc';
  columnSorted?: string;
}

const TableHeader: FC<TableHeaderProps & WithStyles> = ({
  classes,
  columns,
  toggleSort,
  order,
  columnSorted,
}) => {
  console.log(columnSorted);
  return (
    <TableHead>
      <TableRow>
        {(columns || []).map((column, index) => (
          <TableCell
            className={classes.col}
            key={`column-${index}`}
            align={'left'}
            sortDirection={columnSorted === column.name ? order : false}
          >
            {column.sortable === false || column.name === '' ? (
              column.label
            ) : (
              <TableSortLabel
                active={columnSorted === column.name}
                hideSortIcon={column.sortable}
                direction={columnSorted === column.name ? order : undefined}
                onClick={() => (column.name ? toggleSort(column.name) : null)}
              >
                {column.label}
              </TableSortLabel>
            )}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export default withStyles(styles)(TableHeader);
