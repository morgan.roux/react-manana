import React, { FC } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles';
import { TableCell, TableRow as MaterialTableRow } from '@material-ui/core';
import { Column } from './../../../Interface';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  col: {
    color: theme.palette.text.primary,
    padding: '1px 16px 1px 30px',
    borderBottom: 'inherit',
    fontSize: '0.75rem',
    fontWeight: 400,
  },
  tableRow: {
    height: 50,
    '&:nth-child(odd)': { backgroundColor: '#f9f9f9' },
  },
});
interface TableRowProps {
  columns?: Column[];
  key: any;
  value: any;
}

const TableRow: FC<TableRowProps & WithStyles> = ({ classes, columns, key, value }) => {
  return (
    <MaterialTableRow className={classes.tableRow} key={key}>
      {(columns || []).map((column, index) => (
        <TableCell key={index} className={classes.col}>
          {column.renderer ? column.renderer(value) : value[column.name] ? value[column.name] : '-'}
        </TableCell>
      ))}
    </MaterialTableRow>
  );
};

export default withStyles(styles)(TableRow);
