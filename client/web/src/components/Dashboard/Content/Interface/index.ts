export interface Column {
  name: string;
  label: string;
  sortable?: boolean;
  renderer?: (row: any) => any;
  editable?: boolean;
  centered?: boolean;
}
