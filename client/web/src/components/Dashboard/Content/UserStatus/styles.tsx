import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    btn: {
      textTransform: 'none',
      textAlign: 'left',
      '& .MuiButton-label': {
       // zIndex: -1,
      },
    },
    loaderContainer: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  }),
);

export default useStyles;
