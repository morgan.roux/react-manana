import React, { FC, useContext, useEffect, useState } from 'react';
import withStyles, { WithStyles, CSSProperties } from '@material-ui/core/styles/withStyles';
import { Theme } from '@material-ui/core/styles';
import { useLazyQuery } from '@apollo/react-hooks';
import { SEARCH } from '../../../graphql/search/query';
import { SearchInput } from './SearchInput';
import { Column } from './Interface';
import Table from '../../Common/Table';
import { Loader } from './Loader';
import { ContentContext, ContentStateInterface } from '../../../AppContext';

const styles = (theme: Theme): Record<string, CSSProperties> => ({
  root: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3),
    width: '100%',
  },
  searchBar: {
    display: 'flex',
    marginBottom: theme.spacing(5),
    '@media (max-width: 692px)': {
      flexWrap: 'wrap',
      justifyContent: 'center',
    },
  },
  search: {
    '@media (max-width: 580px)': {
      marginBottom: 15,
    },
  },
  alignCenter: {
    textAlign: 'center',
    margin: '40px 0px',
    fontSize: 20,
    fontWeight: 'bold',
  },
});

interface ContentProps {
  type: string;
  searchPlaceholder: string;
  filterBy?: any;
  sortBy?: any;
  columns?: Column[];
  buttons?: any;
  withAvatar?: boolean;
  avatarObjectName?: string;
  datas?: any[];
  localSort?: any;
  hideRightPagination?: boolean;
}

const ROWS_PER_PAGE_OPTIONS = [25, 50, 100, 250, 500, 1000, 2000];

const Content: FC<ContentProps & WithStyles> = ({
  classes,
  type,
  columns,
  searchPlaceholder,
  filterBy,
  sortBy,
  buttons,
  withAvatar,
  avatarObjectName,
  datas,
  localSort,
  hideRightPagination,
}) => {
  const {
    content: { page, rowsPerPage, query },
    setContent,
  } = useContext<ContentStateInterface>(ContentContext);
  const [order, setOrder] = useState<'desc' | 'asc'>('desc');
  const [columnSorted, setColumnSorted] = useState<string>('');
  const [sorted, setSorted] = useState<any>();

  const [loadSearch, { loading, error, data }] = useLazyQuery(SEARCH, {
    variables: {
      type: [type],
      skip: page * rowsPerPage,
      take: rowsPerPage,
      query,
      sortBy: sorted ? [sorted] : sortBy,
      filterBy,
      idPharmacie: '',
    },
  });

  const setRowsPerPage = (value: number) => {
    setContent(prevState => ({
      ...prevState,
      rowsPerPage: value,
      page: 0,
    }));
  };

  const setPage = (newPage: number) => {
    setContent(prevState => ({
      ...prevState,
      page: newPage,
    }));
  };

  const toggleSort = async (sortedBy: string) => {
    const ordered = order === 'desc' ? 'asc' : 'desc';
    setOrder(ordered);
    setSorted({ [sortedBy]: { order: ordered } });
    setColumnSorted(sortedBy);
  };

  useEffect(() => {
    // Reinitialiser la valeur de query si le type change
    setContent(prevState => ({
      ...prevState,
      query: '',
    }));
  }, [type]);

  useEffect(() => {
    if (sorted) {
      setColumnSorted(Object.keys(sorted)[0]);
    } else if (sortBy) {
      setColumnSorted(Object.keys(sortBy[0])[0]);
    }
    setContent(prevState => ({
      ...prevState,
      type,
      columns,
      searchPlaceholder,
      filterBy,
      sortBy: sorted ? [sorted] : sortBy,
      buttons,
    }));
    if (!datas) {
      loadSearch();
    }
  }, [type, columns, searchPlaceholder, filterBy, sortBy, sorted, buttons, datas]);

  useEffect(() => {
    if (localSort && localSort.data && localSort.data.sort && localSort.data.sort.sortItem) {
      console.log('changes : ', localSort.data.sort.sortItem);
      setOrder(localSort.data.sort.sortItem.direction as any);
    }
  }, [localSort]);

  return (
    <div className={classes.root}>
      {!datas && (
        <div className={classes.searchBar}>
          <div className={classes.search}>
            <SearchInput
              value={query}
              onChange={(value: string) => {
                setContent(prevState => ({
                  ...prevState,
                  page: 0,
                  query: value,
                }));
              }}
              placeholder={searchPlaceholder}
            />
          </div>
          {buttons}
        </div>
      )}

      {!datas && (loading || !data) ? (
        <Loader />
      ) : error ? (
        <div className={classes.alignCenter}>Erreur lors de chargement de la page</div>
      ) : (data && data.search && data.search.data && data.search.data.length > 0) || datas ? (
        <Table
          isSelectable={false}
          page={page}
          rowsPerPage={rowsPerPage}
          rowsPerPageOptions={ROWS_PER_PAGE_OPTIONS}
          onChangePage={setPage}
          onChangeRowsPerPage={setRowsPerPage}
          columns={columns}
          data={(data && data.search && data.search.data) || datas}
          count={(data && data.search && data.search.total) || (datas && datas.length) || 0}
          toggleSorting={toggleSort}
          order={order}
          columnSorted={columnSorted}
          densePadding={true}
          withAvatar={withAvatar}
          hideRightPagination={hideRightPagination}
          avatarObjectName={avatarObjectName}
        />
      ) : (
        <div className={classes.alignCenter}>Aucun résultat correspondant</div>
      )}
    </div>
  );
};

export default withStyles(styles)(Content);
