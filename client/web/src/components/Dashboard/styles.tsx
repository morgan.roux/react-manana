import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      padding: '24px',
    },
    alignRight: {
      display: 'flex',
      justifyContent: 'flex-end',
    },
    search: {
      margin: '80px 0',
      display: 'flex',
      '& > div:nth-child(1)': {
        marginRight: '10px',
      },
    },
    table: {
      margin: '20px 0',
    },
    pharmaciesLinkContainer: {
      '& > span:nth-last-child(1)': {
        display: 'none',
      },
    },
    searchInputBox: {
      padding: '20px 0px 0px 20px',
      display: 'flex',
    },
    childrenRoot: {
      display: 'flex',
      '& > button:not(:nth-last-child(1))': {
        marginRight: 15,
      },
    },
    container: {
      width: '100%',
    },
    mutationSuccessContainer: {
      width: '100%',
      minHeight: 'calc(100vh - 156px)',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      '& *': {
        fontFamily: 'Roboto',
        letterSpacing: 0,
      },
      '& img': {
        width: 350,
        height: 270,
      },

      '& p': {
        textAlign: 'center',
        maxWidth: 600,
      },
      '& > div > p:nth-child(2)': {
        fontSize: 30,
        fontWeight: 'bold',
      },
      '& > div > p:nth-child(3)': {
        fontSize: 16,
        fontWeight: 'normal',
      },
      '& button': {
        marginTop: 30,
      },
    },
  }),
);
