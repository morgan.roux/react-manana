import React, { FC, useState } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CustomFormTextField } from '../../../../../../../Common/CustomTextField';
import { TokenInput } from '../../../../../../../../types/graphql-global-types';
import { CustomCheckbox } from '../../../../../../../Common/CustomCheckbox';

export interface TokenFormProps {
  defaultData: TokenInput | undefined;
  handleChange: (param: any) => void;
}

const styles = () =>
  createStyles({
    textareaStyle: {
      marginBottom: 25,
      overflowY: "auto"
    },
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
  });

const TokenForm: FC<RouteComponentProps<any, any, any> & WithStyles & TokenFormProps> = ({ classes, handleChange, defaultData }) => {
  const reqTokenUrl = defaultData && defaultData.reqTokenUrl;
  const reqUserUrl = defaultData && defaultData.reqUserUrl;
  return (
    <>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="token.reqTokenUrl"
            label="Url de requête de token"
            onChange={handleChange}
            value={reqTokenUrl}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="token.reqUserUrl"
            label="Url de requête d'utilisateur de retour"
            onChange={handleChange}
            value={reqUserUrl}
            required={true}
          />
        </div>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(TokenForm));
