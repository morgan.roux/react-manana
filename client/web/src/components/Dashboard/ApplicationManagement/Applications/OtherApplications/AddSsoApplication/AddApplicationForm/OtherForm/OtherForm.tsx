import React, { FC, useState } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CustomFormTextField } from '../../../../../../../Common/CustomTextField';
import CustomTextarea from '../../../../../../../Common/CustomTextarea';
import { OtherInput } from '../../../../../../../../types/graphql-global-types';
import AceEditor from 'react-ace';

import 'ace-builds/src-noconflict/mode-javascript';
import 'ace-builds/src-noconflict/theme-monokai';

export interface OtherFormProps {
  defaultData: OtherInput | undefined;
  handleChange: (param: any) => void;
}

const styles = () =>
  createStyles({
    textareaStyle: {
      marginBottom: 25,
      overflowY: 'auto',
    },
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
  });

const OtherForm: FC<RouteComponentProps<any, any, any> & WithStyles & OtherFormProps> = ({
  classes,
  handleChange,
  defaultData,
}) => {
  const requestUrl = defaultData && defaultData.requestUrl;
  const customfunction = defaultData && defaultData.function;
  return (
    <>
      <div className={classes.marginBottom}>
        <CustomFormTextField
          name="other.requestUrl"
          label="Url de requête"
          onChange={handleChange}
          value={requestUrl}
          required={true}
        />
      </div>
      <div className={classes.marginBottom}>
        {/* <CustomTextarea
            name="other.function"
            label="Fonction de cryptage"
            onChangeTextarea={handleChange}
            value={customfunction || ''}
            required={true}
          /> */}

        <AceEditor
          placeholder="Custom javascript function!!"
          mode="javascript"
          theme="monokai"
          name="other.function"
          onChange={(value: string) => handleChange({ name: 'other.function', value })}
          fontSize={16}
          showPrintMargin={true}
          showGutter={true}
          highlightActiveLine={true}
          value={customfunction || ''}
          setOptions={{
            enableBasicAutocompletion: true,
            enableLiveAutocompletion: true,
            enableSnippets: false,
            showLineNumbers: true,
            tabSize: 2,
          }}
          style={{ height: 300, width: '100%' }}
        />
      </div>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(OtherForm));
