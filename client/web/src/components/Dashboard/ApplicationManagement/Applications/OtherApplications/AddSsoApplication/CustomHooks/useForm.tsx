import { useState, ChangeEvent } from 'react';
import { SsoType, Sendingtype, TypeFichier } from '../../../../../../../types/graphql-global-types';
import { ISsoApplicationInput } from '../../../../../../../Interface/SsoApplicationInputInterface';
import { DialogAction, DataInterface } from '../AddSsoApplication';

const useForm = (
  callback: (data: DataInterface) => void,
  isAdd: boolean,
  defaultState?: ISsoApplicationInput,
) => {
  const initialState: ISsoApplicationInput = defaultState || {
    nom: '',
    url: '',
    icon: {
      chemin: '',
      nomOriginal: '',
      type: TypeFichier.PHOTO,
    },
    ssoType: SsoType.SAML,
    mappings: [],
    sendingType: Sendingtype.GET,
    aes: {
      hexadecimal: false,
    },
    md5: {
      hexadecimal: false,
      haveExternalUserMapping: false,
    },
    other: {},
    saml: {},
    token: {},
    toknFtp: {},
  };

  const [values, setValues] = useState<ISsoApplicationInput>(initialState);

  const handleSubmit = (e: any) => {
    if (e) e.preventDefault();
    callback({
      dialogAction: isAdd ? DialogAction.addApplication : DialogAction.updateApplication,
      data: values,
    });
  };

  const handleChange = (e: ChangeEvent<any>) => {
    const { name, value, checked, type } = e.target ? e.target : e;
    if (name === 'ssoType') {
      switch (value) {
        case SsoType.SAML:
          setValues(prevState => ({
            ...prevState,
            aes: {
              hexadecimal: false,
            },
            md5: {
              hexadecimal: false,
              haveExternalUserMapping: false,
            },
            other: {},
            token: {},
            toknFtp: {},
          }));
          break;
        case SsoType.CRYPTAGE_AES256:
          setValues(prevState => ({
            ...prevState,
            saml: {},
            md5: {
              hexadecimal: false,
              haveExternalUserMapping: false,
            },
            other: {},
            token: {},
            toknFtp: {},
          }));
          break;
        case SsoType.CRYPTAGE_MD5:
          setValues(prevState => ({
            ...prevState,
            saml: {},
            aes: {},
            other: {},
            token: {},
            toknFtp: {},
          }));
          break;
        case SsoType.OTHER_CRYPTO:
          setValues(prevState => ({
            ...prevState,
            saml: {},
            aes: {
              hexadecimal: false,
            },
            md5: {},
            token: {},
            toknFtp: {},
          }));
          break;
        case SsoType.TOKEN_AUTHENTIFICATION:
          setValues(prevState => ({
            ...prevState,
            saml: {},
            aes: {
              hexadecimal: false,
            },
            md5: {
              hexadecimal: false,
              haveExternalUserMapping: false,
            },
            other: {},
            toknFtp: {},
          }));
          break;
        case SsoType.TOKEN_FTP_AUTHENTIFICATION:
          setValues(prevState => ({
            ...prevState,
            saml: {},
            aes: {
              hexadecimal: false,
            },
            md5: {
              hexadecimal: false,
              haveExternalUserMapping: false,
            },
            other: {},
            token: {},
          }));
          break;
        default:
          setValues(initialState);
          break;
      }
    }

    let valKey: string;
    let valValue: string;
    let valResult = {};
    const subVal = name.includes('.');
    const valueResult =
      type && type === 'checkbox' ? checked : type === 'number' ? parseInt(value) : value;
    if (subVal) {
      valKey = name.split('.')[0];
      valValue = name.split('.')[1];
      let prevValue;
      switch (valKey) {
        case 'saml':
          prevValue = values['saml'];
          break;
        case 'aes':
          prevValue = values['aes'];
          break;
        case 'md5':
          prevValue = values['md5'];
          break;
        case 'other':
          prevValue = values['other'];
          break;
        case 'token':
          prevValue = values['token'];
          break;
        case 'toknFtp':
          prevValue = values['toknFtp'];
          break;
        default:
          prevValue = {};
          break;
      }
      valResult = {
        [valKey]: {
          ...prevValue,
          [valValue]: valueResult,
        },
      };
    } else {
      valResult = {
        [name]: valueResult,
      };
    }
    e.persist && e.persist();
    setValues(prevState => ({ ...prevState, ...valResult }));
  };

  return {
    handleChange,
    handleSubmit,
    values,
  };
};

export default useForm;
