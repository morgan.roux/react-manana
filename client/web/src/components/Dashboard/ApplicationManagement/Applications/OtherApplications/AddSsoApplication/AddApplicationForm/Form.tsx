import React, { FC, useState, useEffect } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { SsoType, Sendingtype } from '../../../../../../../types/graphql-global-types';
import { CustomFormTextField } from '../../../../../../Common/CustomTextField';
import CustomSelect from '../../../../../../Common/CustomSelect';
import CustomButton from '../../../../../../Common/CustomButton';
import { useForm } from '../CustomHooks';
import { DialogAction, DataInterface } from '../AddSsoApplication';
import { ISsoApplicationInput } from '../../../../../../../Interface/SsoApplicationInputInterface';
import SamlForm from './SamlForm';
import Md5Form from './Md5Form';
import AesForm from './AesForm';
import Token from './TokenForm';
import TokenFtp from './TokenFtpForm';
import OtherForm from './OtherForm';
import Dropzone from '../../../../../../Common/Dropzone';
import { AxiosResponse } from 'axios';

interface FormProps {
  defaultData?: ISsoApplicationInput;
  isAdd: boolean;
  startUpload?: boolean;
  setStartUpload?: (state: boolean) => void;
  setFileUploadResult?: React.Dispatch<React.SetStateAction<AxiosResponse<any> | null>>;
  withImagePreview?: boolean;
  fileIdentifierPrefix?: string;
  setFilesOut?: React.Dispatch<React.SetStateAction<File[]>>;
  uploadDirectory?: string;
  fileAleadySetUrl?: string;
  setFileAlreadySet?: React.Dispatch<React.SetStateAction<string | undefined>>;
  action(data: DataInterface): void;
}

const styles = () =>
  createStyles({
    textareaStyle: {
      marginBottom: 25,
      overflowY: 'auto',
    },
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
  });

const Form: FC<RouteComponentProps<any, any, any> & WithStyles & FormProps> = ({
  classes,
  defaultData,
  action,
  isAdd,
  startUpload,
  setStartUpload,
  setFileUploadResult,
  withImagePreview,
  fileIdentifierPrefix,
  setFilesOut,
  uploadDirectory,
  fileAleadySetUrl,
  setFileAlreadySet,
}) => {
  const ssoTypeList = [
    { name: 'SAML', value: SsoType.SAML },
    { name: 'CRYPTAGE AES256', value: SsoType.CRYPTAGE_AES256 },
    { name: 'CRYPTAGE MD5', value: SsoType.CRYPTAGE_MD5 },
    { name: 'AUTRE CRYPTAGE', value: SsoType.OTHER_CRYPTO },
    { name: 'ECHANDE DE TOKEN ', value: SsoType.TOKEN_AUTHENTIFICATION },
    { name: 'TOKEN-FTP', value: SsoType.TOKEN_FTP_AUTHENTIFICATION },
  ];

  const sendingTypeList = [
    { name: 'GET', value: Sendingtype.GET },
    { name: 'POST', value: Sendingtype.POST },
  ];

  const { values, handleChange, handleSubmit } = useForm(action, isAdd, defaultData);
  const {
    nom,
    url,
    ssoType,
    mappings,
    sendingType,
    saml,
    aes,
    md5,
    token,
    toknFtp,
    other,
  } = values;

  const canSubmit = (): boolean => {
    return nom &&
      url &&
      mappings &&
      ssoType &&
      sendingType &&
      ((saml &&
        Object.keys(saml).length !== 0 &&
        saml.consumerUrl &&
        saml.idpX509Certificate &&
        saml.spX509Certificate) ||
        (aes && Object.keys(aes).length !== 0 && aes.key && aes.requestUrl) ||
        (md5 && Object.keys(md5).length !== 0 && md5.beginGet && md5.endGet && md5.requestUrl) ||
        (token && Object.keys(token).length !== 0 && token.reqTokenUrl && token.reqUserUrl) ||
        (toknFtp &&
          Object.keys(toknFtp).length !== 0 &&
          toknFtp.ftpFileName &&
          toknFtp.requestUrl) ||
        (other && Object.keys(other).length !== 0 && other.function && other.requestUrl))
      ? true
      : false;
  };

  return (
    <>
      <Dropzone
        setStartUpload={setStartUpload}
        startUpload={startUpload}
        setFileUploadResult={setFileUploadResult}
        withImagePreview={withImagePreview}
        identifierPrefix={fileIdentifierPrefix}
        setFilesOut={setFilesOut}
        uploadDirectory={uploadDirectory}
        fileAlreadySetUrl={fileAleadySetUrl}
        setFileAlreadySet={setFileAlreadySet}
      />
      <form
        onSubmit={action.bind(null, {
          dialogAction: isAdd ? DialogAction.addApplication : DialogAction.updateApplication,
        })}
      >
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="nom"
            label="Nom"
            onChange={handleChange}
            value={nom}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="url"
            label="Url"
            onChange={handleChange}
            value={url}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomSelect
            label="Type d'application"
            list={ssoTypeList}
            index="name"
            listId="value"
            name="ssoType"
            value={ssoType}
            onChange={handleChange}
            disabled={!isAdd}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomSelect
            label="Type d'envoi"
            list={sendingTypeList}
            index="name"
            listId="value"
            name="sendingType"
            value={sendingType}
            onChange={handleChange}
            disabled={false}
          />
        </div>
        {ssoType && ssoType === SsoType.SAML && (
          <SamlForm defaultData={saml} handleChange={handleChange} />
        )}
        {ssoType && ssoType === SsoType.CRYPTAGE_MD5 && (
          <Md5Form defaultData={md5} handleChange={handleChange} />
        )}
        {ssoType && ssoType === SsoType.CRYPTAGE_AES256 && (
          <AesForm defaultData={aes} handleChange={handleChange} />
        )}
        {ssoType && ssoType === SsoType.OTHER_CRYPTO && (
          <OtherForm defaultData={other} handleChange={handleChange} />
        )}
        {ssoType && ssoType === SsoType.TOKEN_AUTHENTIFICATION && (
          <Token defaultData={token} handleChange={handleChange} />
        )}
        {ssoType && ssoType === SsoType.TOKEN_FTP_AUTHENTIFICATION && (
          <TokenFtp defaultData={toknFtp} handleChange={handleChange} />
        )}
        <div className={`${classes.marginBottom} ${classes.flex}`}>
          <div className={classes.w100}>
            <CustomButton
              size="large"
              color="primary"
              fullWidth={true}
              disabled={!canSubmit()}
              onClick={handleSubmit}
            >
              ENREGISTRER
            </CustomButton>
          </div>
        </div>
      </form>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(Form));
