import React, { FC, useState, useEffect } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Grid, Button, Badge, IconButton } from '@material-ui/core';
import useStyles from './styles';
import ApplicationTemplate from '../ApplicationTemplate';
import CloseIcon from '@material-ui/icons/Close';
import { AWS_HOST } from '../../../../../utils/s3urls';
import { useApolloClient } from '@apollo/react-hooks';
import {
  APPLICATIONSGROUP,
  APPLICATIONSGROUPVariables,
} from '../../../../../graphql/Application/types/APPLICATIONSGROUP';
import { GET_APPLICATIONS_GROUP } from '../../../../../graphql/Application/query';
import { getGroupement } from '../../../../../services/LocalStorage';

export interface ApplicationsProps {
  title: string;
  applications: any[];
  groupGuid: string;
  badge?: boolean;
  badgeAction?: (param: any) => void;
  buttonAction?: (param: any) => void;
  editApplicationAction?: (param: any) => void;
  cursorStyle?: boolean;
  codeRole?: string;
}

const OtherApplications: FC<RouteComponentProps & ApplicationsProps> = ({
  applications,
  title,
  badge,
  badgeAction,
  groupGuid,
  buttonAction,
  editApplicationAction,
  cursorStyle,
  codeRole,
}) => {
  const classes = useStyles({});
  const [applicationState, setApplicationState] = useState<any[]>([]);
  const [hoverId, setHoverId] = useState<string>('');

  const setInitialState = () => {
    const initialState: any[] = [];
    if (applications) {
      applications.forEach(application => {
        if (application) {
          if (application.applications) {
            initialState.push({
              ...application.applications,
            });
          } else {
            initialState.push({
              ...application,
            });
          }
        }
      });
      setApplicationState(initialState);
    }
  };

  useEffect(() => {
    setInitialState();
  }, [applications, applications.length]);

  const handleMouseEnter = (applicationID: string) => {
    setHoverId(applicationID);
  };
  const handleMouseLeave = () => {
    setHoverId('');
  };

  return (
    <>
      <div className={classes.marginTop}>
        <div>
          <h3 className={classes.titleApps}> {title} </h3>
          <Button
            variant="contained"
            color="primary"
            className={classes.buttonAdd}
            onClick={() =>
              buttonAction &&
              buttonAction({
                openDialog: true,
                roleId: groupGuid,
                title,
                codeRole,
              })
            }
          >
            Ajouter une application
          </Button>
        </div>
        <div className={classes.root}>
          <Grid container={true} spacing={3}>
            {applicationState &&
              applicationState.map(application => {
                return (
                  application && (
                    <Grid item={true} xs={12} sm={12} lg={2} md={3} key={application.id}>
                      <div
                        onMouseEnter={handleMouseEnter.bind(null, application.id)}
                        onMouseLeave={handleMouseLeave}
                      >
                        {badge && badgeAction && (
                          <Badge
                            badgeContent={
                              hoverId === application.id && (
                                <IconButton
                                  size="small"
                                  aria-label="delete"
                                  onClick={badgeAction.bind(null, {
                                    applicationId: application.id,
                                    roleId: groupGuid,
                                    codeRole,
                                  })}
                                >
                                  <CloseIcon className={classes.buttonBadge} />
                                </IconButton>
                              )
                            }
                            // color="primary"
                          >
                            <ApplicationTemplate
                              classOther={cursorStyle ? classes.cursorStyle : ''}
                              icon={
                                application.icon && application.icon.chemin
                                  ? `${AWS_HOST}/${application.icon.chemin}`
                                  : null
                              }
                              applicationName={application.nom}
                              applicationAction={
                                editApplicationAction &&
                                editApplicationAction.bind(null, {
                                  openDialog: true,
                                  application,
                                })
                              }
                            />
                          </Badge>
                        )}
                      </div>
                    </Grid>
                  )
                );
              })}
          </Grid>
        </div>
      </div>
    </>
  );
};
export default withRouter(OtherApplications);
