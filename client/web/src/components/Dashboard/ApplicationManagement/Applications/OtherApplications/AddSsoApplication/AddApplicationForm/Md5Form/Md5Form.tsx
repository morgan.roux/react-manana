import React, { FC, useState } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CustomFormTextField } from '../../../../../../../Common/CustomTextField';
import { CustomCheckbox } from '../../../../../../../Common/CustomCheckbox';
import { Md5Input } from '../../../../../../../../types/graphql-global-types';

export interface Md5FormProps {
  defaultData: Md5Input | undefined;
  handleChange: (param: any) => void;
}

const styles = () =>
  createStyles({
    textareaStyle: {
      marginBottom: 25,
      overflowY: 'auto',
    },
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
  });

const Md5Form: FC<RouteComponentProps<any, any, any> & WithStyles & Md5FormProps> = ({
  classes,
  handleChange,
  defaultData,
}) => {
  const beginGet = defaultData && defaultData.beginGet;
  const endGet = defaultData && defaultData.endGet;
  const hexadecimal = (defaultData && defaultData.hexadecimal) || false;
  const requestUrl = defaultData && defaultData.requestUrl;
  const haveExternalUserMapping = (defaultData && defaultData.haveExternalUserMapping) || false;

  return (
    <>
      <div className={classes.marginBottom}>
        <CustomFormTextField
          name="md5.beginGet"
          type="number"
          label="Index du début de caractère à prendre"
          onChange={handleChange}
          value={beginGet}
          required={true}
        />
      </div>
      <div className={classes.marginBottom}>
        <CustomFormTextField
          name="md5.endGet"
          type="number"
          label="Index du fin de caractère à prendre"
          onChange={handleChange}
          value={endGet}
          required={true}
        />
      </div>
      <div className={classes.marginBottom}>
        <CustomFormTextField
          name="md5.requestUrl"
          label="Url du requête"
          onChange={handleChange}
          value={requestUrl}
          required={true}
        />
      </div>
      <div className={classes.marginBottom}>
        <CustomCheckbox
          name="md5.hexadecimal"
          label="Héxadecimal"
          value={hexadecimal}
          checked={hexadecimal}
          onChange={handleChange}
        />
        <CustomCheckbox
          name="md5.haveExternalUserMapping"
          label="Mapping externe"
          value={haveExternalUserMapping}
          checked={haveExternalUserMapping}
          onChange={handleChange}
        />
      </div>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(Md5Form));
