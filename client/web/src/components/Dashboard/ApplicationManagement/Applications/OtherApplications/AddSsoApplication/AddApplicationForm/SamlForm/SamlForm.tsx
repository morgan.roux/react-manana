import React, { FC, useState } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CustomFormTextField } from '../../../../../../../Common/CustomTextField';
import CustomTextarea from '../../../../../../../Common/CustomTextarea';
import { SamlInput } from '../../../../../../../../types/graphql-global-types';

export interface SamlFormProps {
  defaultData: SamlInput | undefined;
  handleChange: (param: any) => void;
}

const styles = () =>
  createStyles({
    textareaStyle: {
      marginBottom: 25,
      overflowY: 'auto',
    },
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
  });

const SamlForm: FC<RouteComponentProps<any, any, any> & WithStyles & SamlFormProps> = ({
  classes,
  handleChange,
  defaultData,
}) => {
  const idpX509Certificate = defaultData && defaultData.idpX509Certificate;
  const spX509Certificate = defaultData && defaultData.spX509Certificate;
  const consumerUrl = defaultData && defaultData.consumerUrl;

  return (
    <>
      <div className={classes.marginBottom}>
        <CustomFormTextField
          name="saml.consumerUrl"
          label="Consumer url"
          onChange={handleChange}
          required={true}
          value={consumerUrl}
        />
      </div>
      <div className={classes.marginBottom}>
        <CustomTextarea
          name="saml.idpX509Certificate"
          label="X509 identity provider"
          onChangeTextarea={handleChange}
          value={idpX509Certificate || ''}
          required={true}
        />
      </div>
      <div className={classes.marginBottom}>
        <CustomTextarea
          name="saml.spX509Certificate"
          label="X509 service provider"
          onChangeTextarea={handleChange}
          value={spX509Certificate || ''}
          required={true}
        />
      </div>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(SamlForm));
