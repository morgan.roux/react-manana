import React, { FC, useState } from 'react';
import { withStyles, createStyles, WithStyles } from '@material-ui/core/styles';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { CustomFormTextField } from '../../../../../../../Common/CustomTextField';
import { AesInput } from '../../../../../../../../types/graphql-global-types';
import { CustomCheckbox } from '../../../../../../../Common/CustomCheckbox';

export interface AesFormProps {
  defaultData: AesInput | undefined;
  handleChange: (param: any) => void;
}

const styles = () =>
  createStyles({
    textareaStyle: {
      marginBottom: 25,
      overflowY: "auto"
    },
    marginBottom: {
      marginBottom: 25,
    },
    marginRight: {
      marginRight: 10,
    },
    titleForm: {
      color: '#1D1D1D',
      marginTop: 0,
    },
    flex: {
      display: 'flex',
      flexDirection: 'row',
    },
    w100: {
      width: '100%',
    },
  });

const AesForm: FC<RouteComponentProps<any, any, any> & WithStyles & AesFormProps> = ({ classes, handleChange, defaultData }) => {
  const hexadecimal = defaultData && defaultData.hexadecimal || false;;
  const key = defaultData && defaultData.key;
  const requestUrl = defaultData && defaultData.requestUrl;

  return (
    <>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="aes.key"
            label="Clé de déchiffrement"
            onChange={handleChange}
            value={key}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomFormTextField
            name="aes.requestUrl"
            label="Url de requête"
            onChange={handleChange}
            value={requestUrl}
            required={true}
          />
        </div>
        <div className={classes.marginBottom}>
          <CustomCheckbox
            name="aes.hexadecimal"
            label="Héxadecimal"
            value={hexadecimal}
            checked={hexadecimal}
            onChange={handleChange}
          />
        </div>
    </>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(AesForm));
