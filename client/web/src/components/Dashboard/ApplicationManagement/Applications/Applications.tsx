import React, { FC, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { Box } from '@material-ui/core';
import HelloIdApplications from './HelloIdApplications';
import OtherApplications from './OtherApplications';
import {
  ALL_HELLOID_APPLICATION,
  ALL_HELLOID_APPLICATIONVariables,
} from '../../../../graphql/HelloIdApplication/types/ALL_HELLOID_APPLICATION';
import { GET_ALL_HELLOID_APPLICATIONS } from '../../../../graphql/HelloIdApplication/query';
import { HELLOIDSSO, HELLOIDSSOVariables } from '../../../../graphql/HelloIdSso/types/HELLOIDSSO';
import { GET_HELLOID_GROUPEMENT } from '../../../../graphql/HelloIdSso/query';
import { useQuery, useApolloClient, useMutation, useLazyQuery } from '@apollo/react-hooks';
import { getGroupement } from '../../../../services/LocalStorage';
import { Loader } from '../../Content/Loader';
import {
  APPLICATIONSGROUP,
  APPLICATIONSGROUPVariables,
} from '../../../../graphql/Application/types/APPLICATIONSGROUP';
import { GET_APPLICATIONS_GROUP } from '../../../../graphql/Application/query';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import {
  DELETE_SSO_APPLICATION,
  DELETE_SSO_APPLICATIONVariables,
} from '../../../../graphql/Application/types/DELETE_SSO_APPLICATION';
import { DO_DELETE_SSO_APPLICATION } from '../../../../graphql/Application/mutation';
import AddSsoApplication from './OtherApplications/AddSsoApplication';
import { SSO_APPLICATION_ssoApplication } from '../../../../graphql/Application/types/SSO_APPLICATION';
import { ISsoApplicationInput } from '../../../../Interface/SsoApplicationInputInterface';
import { SsoType, Sendingtype, TypeFichier } from '../../../../types/graphql-global-types';
import { useValueParameterAsBoolean } from '../../../../utils/getValueParameter';
import Backdrop from '../../../Common/Backdrop';

const Applications: FC = () => {
  const groupement = getGroupement();
  const client = useApolloClient();
  const [open, setOpen] = React.useState(false);
  const [isAdd, setIsAdd] = React.useState(false);
  const [applicationData, setApplicationData] = React.useState<ISsoApplicationInput>();
  const [startLoadingAdd, setStartLoadingAdd] = React.useState<boolean>(false);
  const [startLoadingUpdate, setStartLoadingUpdate] = React.useState<boolean>(false);

  const helloidParam = useValueParameterAsBoolean('0046');
  // const activeDirectoryParam = useValueParameterAsBoolean('0049');
  // const ftpParam = useValueParameterAsBoolean('0050');
  const applicationsParam = useValueParameterAsBoolean('0051');

  const [
    getHelloIdApplications,
    { data: helloidApplications, loading: helloidApplicationLoading },
  ] = useLazyQuery<ALL_HELLOID_APPLICATION, ALL_HELLOID_APPLICATIONVariables>(
    GET_ALL_HELLOID_APPLICATIONS,
    {
      // fetchPolicy: "cache-and-network",
      // variables: {
      //   idgroupement : (groupement && groupement.id) || '',
      // },
      onError: error => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: error.message,
          isOpen: true,
        };
        helloidParam && displaySnackBar(client, snackBarData);
      },
    },
  );

  useEffect(() => {
    helloidParam &&
      getHelloIdApplications({
        variables: {
          idgroupement: (groupement && groupement.id) || '',
        },
      });
  }, [helloidParam]);

  const { data: otherApplications, loading: otherLoading } = useQuery<
    APPLICATIONSGROUP,
    APPLICATIONSGROUPVariables
  >(GET_APPLICATIONS_GROUP, {
    fetchPolicy: 'cache-and-network',
    variables: {
      idgroupement: (groupement && groupement.id) || '',
    },
    onError: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de l'affichage des autres applications, veuillez recgetOtherApplicationsharger la page!!`,
        isOpen: true,
      };
      applicationsParam && displaySnackBar(client, snackBarData);
    },
  });

  const { data: helloidParameter } = useQuery<HELLOIDSSO, HELLOIDSSOVariables>(
    GET_HELLOID_GROUPEMENT,
    {
      fetchPolicy: 'cache-and-network',
      variables: {
        idgroupement: (groupement && groupement.id) || '',
      },
      onError: () => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: `Erreur lors de la récupération des paramètres HelloID, veuillez recharger la page!!`,
          isOpen: true,
        };
        helloidParam && displaySnackBar(client, snackBarData);
      },
    },
  );

  const [deleteSsoApplication, { loading: loadingDelete }] = useMutation<
    DELETE_SSO_APPLICATION,
    DELETE_SSO_APPLICATIONVariables
  >(DO_DELETE_SSO_APPLICATION, {
    update: (cache, { data }) => {
      const deleted = data && data.deleteSsoApplication;
      const applicationsOnCache = cache.readQuery<APPLICATIONSGROUP, APPLICATIONSGROUPVariables>({
        query: GET_APPLICATIONS_GROUP,
        variables: {
          idgroupement: (groupement && groupement.id) || '',
        },
      });

      const applicationsGroup = applicationsOnCache && applicationsOnCache.applicationsGroup;
      const newData =
        applicationsGroup &&
        applicationsGroup.filter(
          application =>
            (application && application.applications && application.applications.id) !==
            (deleted && deleted.id),
        );

      cache.writeQuery({
        query: GET_APPLICATIONS_GROUP,
        data: { applicationsGroup: newData },
        variables: {
          idgroupement: (groupement && groupement.id) || '',
        },
      });
    },
    onCompleted: async data => {
      if (data && data.deleteSsoApplication) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Application supprimée`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        // window.location.reload();
      }
    },
    onError: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur de suupréssion de l'application`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleGoToHelloId = () => {
    helloidParameter &&
      helloidParameter.helloIdSsoByGroupementId &&
      helloidParameter.helloIdSsoByGroupementId.helloIdUrl &&
      window.open(helloidParameter.helloIdSsoByGroupementId.helloIdUrl, '_blank');
  };

  const dispatch = (inputData: SSO_APPLICATION_ssoApplication) => {
    let result: ISsoApplicationInput = {
      id: inputData.id,
      url: inputData.url,
      nom: inputData.nom,
      icon:
        ((inputData.icon && {
          chemin: inputData.icon.chemin || '',
          nomOriginal: inputData.icon.nomOriginal || '',
          type: inputData.icon.type || TypeFichier.PHOTO,
        }) as any) || undefined,
      mappings: inputData.mappings || undefined,
      idGroupement: (groupement && groupement.id) || '',
      ssoType: inputData.ssoType,
    };
    switch (inputData.ssoType) {
      case SsoType.SAML:
        result = {
          ...result,
          sendingType:
            (inputData.samlLoginDetail && inputData.samlLoginDetail.Sendingtype) || Sendingtype.GET,
          saml: {
            idpX509Certificate:
              inputData.samlLoginDetail && inputData.samlLoginDetail.idpX509Certificate,
            spX509Certificate:
              inputData.samlLoginDetail && inputData.samlLoginDetail.spX509Certificate,
            consumerUrl: inputData.samlLoginDetail && inputData.samlLoginDetail.consumerUrl,
          },
        };
        break;
      case SsoType.CRYPTAGE_MD5:
        result = {
          ...result,
          sendingType:
            (inputData.cryptoMd5Detail && inputData.cryptoMd5Detail.Sendingtype) || undefined,
          md5: {
            beginGet: inputData.cryptoMd5Detail && inputData.cryptoMd5Detail.beginGet,
            endGet: inputData.cryptoMd5Detail && inputData.cryptoMd5Detail.endGet,
            hexadecimal: inputData.cryptoMd5Detail && inputData.cryptoMd5Detail.hexadecimal,
            requestUrl: inputData.cryptoMd5Detail && inputData.cryptoMd5Detail.requestUrl,
            haveExternalUserMapping:
              inputData.cryptoMd5Detail && inputData.cryptoMd5Detail.haveExternalUserMapping,
          },
        };
        break;
      case SsoType.OTHER_CRYPTO:
        result = {
          ...result,
          sendingType:
            (inputData.cryptoOtherDetail && inputData.cryptoOtherDetail.Sendingtype) || undefined,
          other: {
            function: inputData.cryptoOtherDetail && inputData.cryptoOtherDetail.function,
            requestUrl: inputData.cryptoOtherDetail && inputData.cryptoOtherDetail.requestUrl,
          },
        };
        break;
      case SsoType.CRYPTAGE_AES256:
        result = {
          ...result,
          sendingType:
            (inputData.cryptoAesDetail && inputData.cryptoAesDetail.Sendingtype) || undefined,
          aes: {
            key: inputData.cryptoAesDetail && inputData.cryptoAesDetail.key,
            requestUrl: inputData.cryptoAesDetail && inputData.cryptoAesDetail.requestUrl,
            hexadecimal: inputData.cryptoAesDetail && inputData.cryptoAesDetail.hexadecimal,
          },
        };
        break;
      case SsoType.TOKEN_AUTHENTIFICATION:
        result = {
          ...result,
          sendingType:
            (inputData.tokenAuthentificationDetail &&
              inputData.tokenAuthentificationDetail.Sendingtype) ||
            undefined,
          token: {
            reqTokenUrl:
              inputData.tokenAuthentificationDetail &&
              inputData.tokenAuthentificationDetail.reqTokenUrl,
            reqUserUrl:
              inputData.tokenAuthentificationDetail &&
              inputData.tokenAuthentificationDetail.reqUserUrl,
          },
        };
        break;
      case SsoType.TOKEN_FTP_AUTHENTIFICATION:
        result = {
          ...result,
          sendingType:
            (inputData.tokenFtpAuthentificationDetail &&
              inputData.tokenFtpAuthentificationDetail.Sendingtype) ||
            undefined,
          toknFtp: {
            requestUrl:
              inputData.tokenFtpAuthentificationDetail &&
              inputData.tokenFtpAuthentificationDetail.requestUrl,
            ftpFileName:
              inputData.tokenFtpAuthentificationDetail &&
              inputData.tokenFtpAuthentificationDetail.ftpFileName,
            ftpFilePath:
              inputData.tokenFtpAuthentificationDetail &&
              inputData.tokenFtpAuthentificationDetail.ftpFilePath,
          },
        };
        break;
      default:
        console.log('application type unknown');
        return null;
    }
    return result;
  };

  const handleAddApplication = (params: any) => {
    const { openDialog } = params;
    setApplicationData(undefined);
    setIsAdd(true);
    setOpen(openDialog);
  };

  const handleEditApplication = (params: any) => {
    const { application } = params;
    console.log('application to edit', application);
    setIsAdd(false);
    // getOneApplication({
    //   variables: { id: applicationId},
    // });
    // setOpenUpdate(true);
    const appDetail = application;
    const appToSet = dispatch(appDetail);
    appToSet && setApplicationData(appToSet);
    setOpen(true);
  };

  const handleRemoveApplication = (params: any) => {
    const { applicationId } = params;
    deleteSsoApplication({
      variables: {
        id: applicationId,
      },
    });
    setIsAdd(false);
    setOpen(false);
  };

  if (helloidApplicationLoading || otherLoading) return <Loader />;

  return (
    <>
      {loadingDelete && (
        <Backdrop value="Suppréssion de l'application en cours, veuillez patienter..." />
      )}
      {(startLoadingAdd || startLoadingUpdate) && (
        <Backdrop
          value={`${
            startLoadingAdd ? 'Ajout' : 'Modification'
          } de l'application en cours, veuillez patienter...`}
        />
      )}
      <AddSsoApplication
        open={open}
        setOpen={setOpen}
        defaultData={applicationData}
        isAdd={isAdd}
        setStartLoadingAdd={setStartLoadingAdd}
        setStartLoadingUpdate={setStartLoadingUpdate}
      />
      <Box marginTop="32px" marginLeft="32px" marginRight="32px">
        {helloidParam && helloidApplications && helloidApplications.helloidApplications && (
          <HelloIdApplications
            applications={helloidApplications.helloidApplications}
            buttonAction={handleGoToHelloId}
            title={'HELLOID'}
            groupGuid={''}
          />
        )}
        {applicationsParam && otherApplications && otherApplications.applicationsGroup && (
          <OtherApplications
            applications={otherApplications.applicationsGroup}
            title={'AUTRE'}
            buttonAction={handleAddApplication}
            groupGuid={''}
            badge={true}
            badgeAction={handleRemoveApplication}
            editApplicationAction={handleEditApplication}
            cursorStyle={true}
          />
        )}
      </Box>
    </>
  );
};
export default withRouter(Applications);
