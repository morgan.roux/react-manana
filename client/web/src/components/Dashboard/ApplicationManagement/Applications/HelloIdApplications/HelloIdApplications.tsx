import React, { FC, useState, useEffect } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { Grid, Button, Badge, IconButton } from '@material-ui/core';
import useStyles from './styles';
import ApplicationTemplate from '../ApplicationTemplate';
import CloseIcon from '@material-ui/icons/Close';

export interface ApplicationsProps {
  title: string;
  applications: any[];
  groupGuid: string;
  badge?: boolean;
  badgeAction?: (param: any) => void;
  buttonAction?: (param: any) => void;
  editApplicationAction?: (param: any) => void;
  cursorStyle?: boolean;
  codeRole?: string;
}

const HelloIdApplications: FC<RouteComponentProps & ApplicationsProps> = ({
  applications,
  buttonAction,
  title,
  groupGuid,
  badge,
  badgeAction,
}) => {
  const classes = useStyles({});
  const [applicationState, setApplicationState] = useState<any[]>([]);
  const [hoverId, setHoverId] = useState<string>('');

  useEffect(() => {
    const initialState: any[] = [];
    if (applications) {
      applications.forEach(application => {
        application &&
          initialState.push({
            ...application,
          });
      });
      setApplicationState(initialState);
    }
  }, [applications]);

  const handleMouseEnter = (applicationID: string) => {
    setHoverId(applicationID);
  };
  const handleMouseLeave = () => {
    setHoverId('');
  };

  return (
    <>
      <div>
        <div>
          <h3 className={classes.titleApps}> {title} </h3>
          <Button
            variant="contained"
            color="primary"
            className={classes.buttonAdd}
            onClick={() => buttonAction && buttonAction({ groupGuid, title })}
          >
            Ajouter une application
          </Button>
        </div>
        <div className={classes.root}>
          <Grid container={true} spacing={3}>
            {applicationState &&
              applicationState.map(application => {
                return (
                  <Grid item={true} xs={12} sm={12} lg={2} md={3} key={application.applicationGUID}>
                    <div
                      onMouseEnter={handleMouseEnter.bind(null, application.applicationGUID)}
                      onMouseLeave={handleMouseLeave}
                    >
                      {badge && badgeAction && (
                        <Badge
                          badgeContent={
                            hoverId === application.applicationGUID && (
                              <IconButton
                                size="small"
                                aria-label="delete"
                                onClick={() =>
                                  badgeAction({
                                    groupGuid,
                                    applicationGuid: application.applicationGUID,
                                  })
                                }
                              >
                                <CloseIcon className={classes.buttonBadge} />
                              </IconButton>
                            )
                          }
                          // color="secondary"
                        >
                          <ApplicationTemplate
                            icon={application.iconlink}
                            applicationName={application.name}
                          />
                        </Badge>
                      )}
                      {!badge && (
                        <ApplicationTemplate
                          icon={application.iconlink}
                          applicationName={application.name}
                        />
                      )}
                    </div>
                  </Grid>
                );
              })}
          </Grid>
        </div>
      </div>
    </>
  );
};
export default withRouter(HelloIdApplications);
