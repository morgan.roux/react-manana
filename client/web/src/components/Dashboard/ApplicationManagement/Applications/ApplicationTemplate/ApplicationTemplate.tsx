import React, { FC } from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import WebAssetIcon from '@material-ui/icons/WebAsset';
import useStyles from './styles';

interface ApplicationProps {
  icon: string | null | undefined;
  applicationName: string | null | undefined;
  applicationAction?: (param: any) => void;
  classOther?: string;
}

const ApplicationTemplate: FC<RouteComponentProps<any, any, any> & ApplicationProps> = ({
  icon,
  applicationName,
  applicationAction,
  classOther,
}) => {
  const classes = useStyles({});
  return (
    <>
      <div
        className={`${classes.paper} ${classOther}`}
        onClick={applicationAction && applicationAction.bind(null, {})}
      >
        {icon && <img src={icon} className={classes.icon} />}
        {!icon && <WebAssetIcon className={classes.icon} />}
        <p>{applicationName}</p>
      </div>
    </>
  );
};
export default withRouter(ApplicationTemplate);
