import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { flexWrap: 'nowrap' },
    leftOperation: {
      minWidth: 280,
      padding: 24,
      borderRight: '2px solid #E5E5E5',
      height: 'calc(100vh - 86px)',
      overflowY: 'auto',
    },
    floatRight: {
      position: 'absolute',
      bottom: theme.spacing(2),
      right: theme.spacing(2),
      color: '#fff',
      // background: 'linear-gradient(90deg, rgba(227,65,104,1) 14%, rgba(241,25,87,1) 100%)'
      backgroundColor: theme.palette.primary.light,
    },
  }),
);

export default useStyles;
