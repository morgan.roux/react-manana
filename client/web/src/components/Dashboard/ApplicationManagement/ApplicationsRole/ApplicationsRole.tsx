import React, { FC, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { Box, Fab } from '@material-ui/core';
import { useQuery, useLazyQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { getGroupement } from '../../../../services/LocalStorage';
import { Loader } from '../../Content/Loader';
import {
  GET_APPLICATIONS_ROLES,
  GET_APPLICATIONS_GROUP,
  GET_APPLICATIONS_ROLE,
} from '../../../../graphql/Application/query';
import {
  APPLICATIONSROLES,
  APPLICATIONSROLESVariables,
  APPLICATIONSROLES_applicationsRoles_applications_icon,
} from '../../../../graphql/Application/types/APPLICATIONSROLES';
import OthersApplications from '../Applications/OtherApplications';
import {
  DELETE_APPLICATION_ROLE,
  DELETE_APPLICATION_ROLEVariables,
} from '../../../../graphql/Application/types/DELETE_APPLICATION_ROLE';
import {
  DO_DELETE_APPLICATION_ROLE,
  DO_CREATE_APPLICATION_ROLE,
  DO_CREATE_MANY_APPLICATION_ROLE,
} from '../../../../graphql/Application/mutation';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import {
  APPLICATIONSGROUP,
  APPLICATIONSGROUPVariables,
} from '../../../../graphql/Application/types/APPLICATIONSGROUP';
import {
  CREATE_MANY_APPLICATION_ROLE,
  CREATE_MANY_APPLICATION_ROLEVariables,
} from '../../../../graphql/Application/types/CREATE_MANY_APPLICATION_ROLE';
import AddApplicationRole from './AddApplicationRole';
import { DialogAction } from './AddApplicationRole/AddApplicationRole';
import { ApplicationRoleInput } from '../../../../types/graphql-global-types';
import AddIcon from '@material-ui/icons/Add';
import useStyles from './styles';
import Tooltip from '@material-ui/core/Tooltip';
import AddRole from './AddRole';
import { USER_ROLES, USER_ROLES_roles } from '../../../../graphql/Role/types/USER_ROLES';
import { GET_USER_ROLES } from '../../../../graphql/Role/query';
import { AWS_HOST } from '../../../../utils/s3urls';
import {
  APPLICATIONSROLE,
  APPLICATIONSROLEVariables,
  APPLICATIONSROLE_applicationsRole,
} from '../../../../graphql/Application/types/APPLICATIONSROLE';
import Backdrop from '../../../Common/Backdrop';

export interface Application {
  id: string;
  nom: string;
  url: string;
  ssoType: string;
  ssoTypeid: string;
  icon: APPLICATIONSROLES_applicationsRoles_applications_icon | null;
}

export interface ApplicationsRoles {
  idrole: string;
  codeRole: string;
  nomRole: string;
  applications: Application[];
}

const Applications: FC = () => {
  const groupement = getGroupement();
  const client = useApolloClient();
  const classes = useStyles({});
  const [open, setOpen] = React.useState(false);
  const [openAddRole, setOpenAddRole] = React.useState(false);
  const [applicationsRoles, setapplicationsRoles] = useState<ApplicationsRoles[]>([]);
  const [applicationsRolesToShow, setApplicationsRolesToShow] = useState<ApplicationsRoles[]>([]);
  const [titleDialog, setTitleDialog] = React.useState('');
  const [roleID, setRoleID] = React.useState('');
  const [codeRoleState, setCodeRoleState] = React.useState('');
  const [rolesToShow, setRolesToShow] = React.useState<USER_ROLES_roles[]>([]);

  const { data: applicationsRole, loading: loadApplications } = useQuery<
    APPLICATIONSROLES,
    APPLICATIONSROLESVariables
  >(GET_APPLICATIONS_ROLES, {
    fetchPolicy: 'cache-and-network',
    variables: {
      idgroupement: (groupement && groupement.id) || '',
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur de récupération des applications`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const { data: rolesData, loading: loadingRole } = useQuery<USER_ROLES>(GET_USER_ROLES, {
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de la récupération des rôles.`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const { data: allApplications, loading: otherLoading } = useQuery<
    APPLICATIONSGROUP,
    APPLICATIONSGROUPVariables
  >(GET_APPLICATIONS_GROUP, {
    // fetchPolicy: "cache-and-network",
    variables: {
      idgroupement: (groupement && groupement.id) || '',
    },
    onError: error => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de l'affichage des autres applications, veuillez recharger la page!!`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [deleteApplicationRole] = useMutation<
    DELETE_APPLICATION_ROLE,
    DELETE_APPLICATION_ROLEVariables
  >(DO_DELETE_APPLICATION_ROLE, {
    onCompleted: async data => {
      if (data && data.deleteApplicationRole) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Application supprimée`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        // window.location.reload();
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur de suupréssion de l'application`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [addManyApplicationRole, { loading: loadingAddApplications }] = useMutation<
    CREATE_MANY_APPLICATION_ROLE,
    CREATE_MANY_APPLICATION_ROLEVariables
  >(DO_CREATE_MANY_APPLICATION_ROLE, {
    onCompleted: async data => {
      if (data && data.createManyApplicationRole) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Application ajoutée!`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        // setOpen(false);
        // setOpenAddRole(false);
        // window.location.reload();
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de l'ajout de l'application!`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
      setOpen(false);
      setOpenAddRole(false);
    },
  });

  useEffect(() => {
    if (applicationsRole) {
      const { applicationsRoles } = applicationsRole;
      const appsPerRole: ApplicationsRoles[] = [];
      applicationsRoles &&
        applicationsRoles.forEach(roleApps => {
          const roleCode = roleApps && roleApps.role && roleApps.role.code;
          const roleApplications = roleApps && roleApps.applications;
          const roleRole = roleApps && roleApps.role;
          const check = roleCode && appsPerRole.find(element => element.codeRole === roleCode);
          const appToAdd = {
            id: (roleApplications && roleApplications.id) || '',
            nom: (roleApplications && roleApplications.nom) || '',
            ssoType: (roleApplications && roleApplications.ssoType) || '',
            ssoTypeid: (roleApplications && roleApplications.ssoTypeid) || '',
            url: (roleApplications && roleApplications.url) || '',
            icon: roleApplications && roleApplications.icon,
          };
          if (check) {
            check.applications.push(appToAdd);
          } else {
            appsPerRole.push({
              idrole: (roleRole && roleRole.id) || '',
              codeRole: (roleRole && roleRole.code) || '',
              nomRole: (roleRole && roleRole.nom) || '',
              applications: [appToAdd],
            });
          }
        });

      const rolesToAdd: USER_ROLES_roles[] = [];
      rolesData &&
        rolesData.roles &&
        rolesData.roles.forEach(role => {
          const find =
            applicationsRoles &&
            applicationsRoles.find(
              appRole => (appRole && appRole.role && appRole.role.id) === (role && role.id),
            );
          if (!find && role) {
            rolesToAdd.push(role);
          }
        });
      setapplicationsRoles(appsPerRole);
      setRolesToShow(rolesToAdd);
    }
  }, [
    applicationsRole,
    rolesData,
    applicationsRole &&
      applicationsRole.applicationsRoles &&
      applicationsRole.applicationsRoles.length,
  ]);

  const addApplicationToRole = (params: any) => {
    // console.log("params ==========> ", params)
    const { roleId, codeRole, dialogAction, checkboxState } = params;
    if (dialogAction === DialogAction.addToRole) {
      setOpen(false);
      setOpenAddRole(false);
      const appsToAdd: ApplicationRoleInput[] = [];
      for (const key in checkboxState) {
        if (checkboxState[key]) {
          appsToAdd.push({
            idApplications: key,
            idGroupement: (groupement && groupement.id) || '',
            idrole: roleId,
          });
        }
      }
      if (appsToAdd && appsToAdd.length !== 0) {
        addManyApplicationRole({
          variables: {
            applicationsRoleInput: appsToAdd,
          },
          update: (cache, { data }) => {
            const created = data && data.createManyApplicationRole;
            const applicationsOnCache = cache.readQuery<
              APPLICATIONSROLES,
              APPLICATIONSROLESVariables
            >({
              query: GET_APPLICATIONS_ROLES,
              variables: {
                idgroupement: (groupement && groupement.id) || '',
              },
            });
            const applicationsRoles = applicationsOnCache && applicationsOnCache.applicationsRoles;
            if (applicationsRoles && created) {
              created.map(application => {
                application && applicationsRoles.push({ ...application });
              });

              cache.writeQuery({
                query: GET_APPLICATIONS_ROLES,
                data: { applicationsRoles },
                variables: {
                  idgroupement: (groupement && groupement.id) || '',
                },
              });
            }

            // application frame
            try {
              const applicationsFrameOnCache = cache.readQuery<
                APPLICATIONSROLE,
                APPLICATIONSROLEVariables
              >({
                query: GET_APPLICATIONS_ROLE,
                variables: {
                  idgroupement: (groupement && groupement.id) || '',
                  coderole: codeRole,
                },
              });

              if (
                applicationsFrameOnCache &&
                applicationsFrameOnCache.applicationsRole &&
                created
              ) {
                created.map(applicationRole => {
                  if (applicationRole) {
                    const appRole = applicationRole as APPLICATIONSROLE_applicationsRole;
                    applicationsFrameOnCache.applicationsRole &&
                      applicationsFrameOnCache.applicationsRole.push(appRole);
                  }
                });
                cache.writeQuery({
                  query: GET_APPLICATIONS_ROLE,
                  variables: {
                    idgroupement: (groupement && groupement.id) || '',
                    coderole: codeRole,
                  },
                  data: { applicationsRole: applicationsFrameOnCache.applicationsRole },
                });
              }
            } catch (error) {
              console.warn('query not yet in cache');
            }
          },
        });
      }
    } else {
      setOpen(false);
      setOpenAddRole(false);
    }
    // setOpen(false);
    // setOpenAddRole(false);
  };
  const openApplicationDialog = (params: any) => {
    const { openDialog, title, roleId, codeRole } = params;
    const appsToShow: any[] = [];
    const applicationsRoleFound = applicationsRoles.find(
      appRole => (appRole && appRole.idrole) === roleId,
    );
    allApplications &&
      allApplications.applicationsGroup &&
      allApplications.applicationsGroup.forEach(application => {
        const found =
          applicationsRoleFound &&
          applicationsRoleFound.applications.find(
            appRole =>
              (appRole && appRole.id) ===
              (application && application.applications && application.applications.id),
          );
        if (!found) {
          application &&
            application.applications &&
            appsToShow.push({
              applicationGUID: application.applications.id,
              name: application.applications.nom,
              iconlink:
                application.applications &&
                application.applications.icon &&
                application.applications.icon.chemin
                  ? `${AWS_HOST}/${application.applications.icon.chemin}`
                  : '',
            });
        }
      });
    setApplicationsRolesToShow(appsToShow);
    setOpen(openDialog);
    setTitleDialog(title);
    setRoleID(roleId);
    setCodeRoleState(codeRole);
  };

  const handleRemoveApplication = (params: any) => {
    const { applicationId, roleId, codeRole } = params;
    deleteApplicationRole({
      variables: {
        idApplications: applicationId,
        idGroupement: (groupement && groupement.id) || '',
        idrole: roleId,
      },
      update: (cache, { data }) => {
        const deleted = data && data.deleteApplicationRole;

        // application frame
        try {
          const applicationsFrameOnCache = cache.readQuery<
            APPLICATIONSROLE,
            APPLICATIONSROLEVariables
          >({
            query: GET_APPLICATIONS_ROLE,
            variables: {
              idgroupement: (groupement && groupement.id) || '',
              coderole: codeRole,
            },
          });

          if (applicationsFrameOnCache && applicationsFrameOnCache.applicationsRole && deleted) {
            applicationsFrameOnCache.applicationsRole = applicationsFrameOnCache.applicationsRole.filter(
              appRole => (appRole && appRole.id) !== (deleted && deleted.id),
            );

            cache.writeQuery({
              query: GET_APPLICATIONS_ROLE,
              variables: {
                idgroupement: (groupement && groupement.id) || '',
                coderole: codeRole,
              },
              data: { applicationsRole: applicationsFrameOnCache.applicationsRole },
            });
          }
        } catch (error) {
          console.warn('query not yet in cache');
        }
      },
    });
  };

  const handleAddNewRoleApplication = () => {
    setOpenAddRole(true);
  };

  if (loadApplications || otherLoading) return <Loader />;

  return (
    applicationsRoles && (
      <>
        {loadingAddApplications && (
          <Backdrop value="Ajout des application(s) en cours, veuillez patienter..." />
        )}
        <AddApplicationRole
          open={open}
          title={titleDialog}
          applicationList={applicationsRolesToShow}
          roleId={roleID}
          codeRole={codeRoleState}
          handleClose={addApplicationToRole}
        />
        <AddRole
          open={openAddRole}
          title={'AJOUTER UNE APLICATION POUR UN ROLE'}
          rolesList={rolesToShow}
          allApplications={allApplications && allApplications.applicationsGroup}
          handleClose={addApplicationToRole}
        />
        <Box marginTop="32px" marginLeft="32px" marginRight="32px">
          {applicationsRoles.map(roles => {
            return (
              roles &&
              roles.idrole && (
                <Box marginTop="20px" key={roles.idrole}>
                  <OthersApplications
                    applications={roles.applications}
                    buttonAction={openApplicationDialog}
                    title={roles.nomRole || ''}
                    groupGuid={roles.idrole}
                    badge={true}
                    badgeAction={handleRemoveApplication}
                    cursorStyle={false}
                    codeRole={roles.codeRole}
                  />
                </Box>
              )
            );
          })}
          <Tooltip
            color="primary"
            title="Ajouter un nouveau rôle"
            aria-label="Ajouter un nouveau rôle"
          >
            <Fab
              color="primary"
              aria-label="Ajouter un nouveau rôle"
              className={classes.floatRight}
              onClick={handleAddNewRoleApplication}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
        </Box>
      </>
    )
  );
};
export default withRouter(Applications);
