import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper, { PaperProps } from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import { Grid, InputAdornment } from '@material-ui/core';
import CustomSelect from '../../../../Common/CustomSelect';
import GroupIcon from '@material-ui/icons/Group';
import ApplicationDialogList from '../ApplicationDialogList';
import { APPLICATIONSGROUP_applicationsGroup } from '../../../../../graphql/Application/types/APPLICATIONSGROUP';

export interface DialogProps {
  open: boolean;
  title: string;
  rolesList?: any[];
  allApplications: Array<APPLICATIONSGROUP_applicationsGroup | null> | null | undefined;
  handleClose: (params: any) => void;
}
export enum DialogAction {
  close,
  addToRole,
}

function PaperComponent(props: PaperProps) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

export default function AddRole(props: DialogProps) {
  const { title, rolesList, allApplications, handleClose, open } = props;
  const [checkboxState, setCheckboxState] = React.useState<any[]>([]);
  const [roleSelected, setRoleSelected] = React.useState<string>('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRoleSelected(event.target.value);
  };

  const handleApplicationChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCheckboxState({ ...checkboxState, [event.target.name]: event.target.checked });
  };

  useEffect(() => {
    setCheckboxState([]);
    setRoleSelected('');
  }, [open]);

  const canSave = () => {
    for (const key in checkboxState) {
      if (roleSelected && checkboxState[key] == true) return true;
    }
    // console.log(checked);
    return false;
  };

  // if (loading) return <Loader />;

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
        maxWidth="md"
        keepMounted={false}
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          {title}
        </DialogTitle>
        <DialogContent dividers={true}>
          <form
            onSubmit={() =>
              handleClose({
                roleId: roleSelected,
                checkboxState,
                dialogAction: DialogAction.addToRole,
              })
            }
          >
            <Grid container={true} spacing={2}>
              {(!rolesList || (rolesList && rolesList.length) === 0) &&
                "Il n'y a pas d'autres roles à ajouter"}
              {(rolesList && rolesList.length) !== 0 && (
                <CustomSelect
                  list={rolesList}
                  listId="id"
                  index="nom"
                  variant="outlined"
                  disabled={false}
                  value={roleSelected}
                  onClick={handleChange}
                  startAdornment={
                    <InputAdornment position="start">
                      <GroupIcon />
                    </InputAdornment>
                  }
                />
              )}
              {allApplications && (rolesList && rolesList.length) !== 0 && (
                <Grid container={true} spacing={2}>
                  <ApplicationDialogList
                    applicationList={allApplications}
                    handleChangeCheck={handleApplicationChange}
                  />
                </Grid>
              )}
            </Grid>
          </form>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              handleClose(DialogAction.close);
            }}
            variant="contained"
          >
            Annuler
          </Button>
          <Button
            autoFocus={true}
            onClick={() => {
              handleClose({
                roleId: roleSelected,
                checkboxState,
                dialogAction: DialogAction.addToRole,
              });
            }}
            variant="contained"
            color="primary"
            disabled={!canSave()}
          >
            Ajouter
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
