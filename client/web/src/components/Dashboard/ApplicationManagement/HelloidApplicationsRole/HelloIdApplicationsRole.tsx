import React, { FC } from 'react';
import { withRouter } from 'react-router-dom';
import { Box } from '@material-ui/core';
import { useQuery, useLazyQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import { getGroupement, getUser } from '../../../../services/LocalStorage';
import { Loader } from '../../Content/Loader';
import { GET_HELLOID_GROUPS } from '../../../../graphql/HelloIdSso/HelloidGroup/query';
import {
  HELLOIDGROUPS,
  HELLOIDGROUPSVariables,
} from '../../../../graphql/HelloIdSso/HelloidGroup/types/HELLOIDGROUPS';
import HelloIdApplications from '../Applications/HelloIdApplications';
import AddApplicationRole from './AddApplicationRole/AddApplicationRole';
import {
  ALL_HELLOID_APPLICATION,
  ALL_HELLOID_APPLICATIONVariables,
} from '../../../../graphql/HelloIdApplication/types/ALL_HELLOID_APPLICATION';
import {
  GET_ALL_HELLOID_APPLICATIONS,
  GET_HELLOID_APPLICATIONS,
} from '../../../../graphql/HelloIdApplication/query';
import {
  REMOVE_HELLOID_APPLICATION,
  REMOVE_HELLOID_APPLICATIONVariables,
} from '../../../../graphql/HelloIdSso/HelloidGroup/types/REMOVE_HELLOID_APPLICATION';
import { DO_REMOVE_HELLOID_APPLICATION } from '../../../../graphql/HelloIdSso/HelloidGroup/mutation';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import Backdrop from '../../../Common/Backdrop';
import {
  HELLOID_APPLICATION,
  HELLOID_APPLICATIONVariables,
} from '../../../../graphql/HelloIdApplication/types/HELLOID_APPLICATION';

const Applications: FC = () => {
  const groupement = getGroupement();
  const client = useApolloClient();
  const [open, setOpen] = React.useState(false);
  const [applicationsToShow, setApplicationsToShow] = React.useState<any[]>([]);
  const [titleDialog, setTitleDialog] = React.useState('');
  const [groupGuid, setGroupGuid] = React.useState('');
  const [groupementId, setGroupementId] = React.useState('');
  const [adding, setAdding] = React.useState(false);
  const user = getUser();

  const { data, loading } = useQuery<HELLOIDGROUPS, HELLOIDGROUPSVariables>(GET_HELLOID_GROUPS, {
    // fetchPolicy: "cache-and-network",
    variables: {
      idgroupement: (groupement && groupement.id) || '',
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de la récupération des groupes HelloID`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const { data: allApps, loading: loadingAllApps } = useQuery<
    ALL_HELLOID_APPLICATION,
    ALL_HELLOID_APPLICATIONVariables
  >(GET_ALL_HELLOID_APPLICATIONS, {
    // fetchPolicy: "cache-and-network",
    variables: {
      idgroupement: (groupement && groupement.id) || '',
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur dla récupération des applications HelloID`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [removeToGroupHelloId, { loading: loadingRemove }] = useMutation<
    REMOVE_HELLOID_APPLICATION,
    REMOVE_HELLOID_APPLICATIONVariables
  >(DO_REMOVE_HELLOID_APPLICATION, {
    update: (cache, { data }) => {
      const deleted = data && data.removeApplicationToHelloidGroup;

      const applicationsOnCache = cache.readQuery<HELLOIDGROUPS, HELLOIDGROUPSVariables>({
        query: GET_HELLOID_GROUPS,
        variables: {
          idgroupement: (groupement && groupement.id) || '',
        },
      });

      const applicationsGroup =
        applicationsOnCache &&
        applicationsOnCache.helloidGroups &&
        applicationsOnCache.helloidGroups.slice();

      const found =
        applicationsGroup &&
        applicationsGroup.find(
          group => group && group.groupGuid === (deleted && deleted.groupGuid),
        );
      if (found && deleted) {
        found.application = deleted.application;
      }
      cache.writeQuery({
        query: GET_HELLOID_GROUPS,
        data: { helloidGroups: applicationsGroup },
        variables: {
          idgroupement: (groupement && groupement.id) || '',
        },
      });

      if (deleted) {
        const helloidApplicationsRole = cache.readQuery<
          HELLOID_APPLICATION,
          HELLOID_APPLICATIONVariables
        >({
          query: GET_HELLOID_APPLICATIONS,
          variables: {
            iduser: (user && user.id) || '',
            idGroupement: (groupement && groupement.id) || '',
          },
        });
        console.log(' deleted ', deleted);
        console.log(' helloidApplicationsRole ', helloidApplicationsRole);

        let userApplications = helloidApplicationsRole && helloidApplicationsRole.userApplications;

        userApplications = deleted.application;

        cache.writeQuery({
          query: GET_HELLOID_APPLICATIONS,
          data: { userApplications },
          variables: {
            iduser: (user && user.id) || '',
            idGroupement: (groupement && groupement.id) || '',
          },
        });
      }
    },
    onCompleted: async data => {
      if (data && data.removeApplicationToHelloidGroup) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Applications supprimée`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        // window.location.reload();
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur de suupréssion de l'applications`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleAddApplication = (params: any) => {
    const appsToShow: any[] = [];
    const { groupGuid, title } = params;
    const group =
      data &&
      data.helloidGroups &&
      data.helloidGroups.find(group => group && group.groupGuid === groupGuid);
    if (group) {
      allApps &&
        allApps.helloidApplications &&
        allApps.helloidApplications.forEach(application => {
          const getApplication =
            group.application &&
            group.application.find(
              app => (app && app.applicationGUID) === (application && application.applicationGUID),
            );
          if (!getApplication) {
            appsToShow.push(application);
          }
        });
    }
    setGroupGuid(groupGuid);
    setApplicationsToShow(appsToShow);
    setTitleDialog(title);
    setGroupementId(groupement.id);
    setOpen(true);
  };

  const handleRemoveApplication = (params: any) => {
    const { groupGuid, applicationGuid } = params;
    removeToGroupHelloId({
      variables: {
        idGroupement: groupement.id,
        groupGuid,
        applicationsGuid: [applicationGuid],
      },
    });
  };

  if (loading) return <Loader />;

  return (
    <>
      {loadingRemove && (
        <Backdrop value="Suppréssion de l'application en cours, veuillez patienter..." />
      )}
      {adding && <Backdrop value="Ajout des applications en cours, veuillez patienter..." />}
      <AddApplicationRole
        open={open}
        setOpen={setOpen}
        title={titleDialog}
        applicationList={applicationsToShow}
        groupGuid={groupGuid}
        groupement={groupementId}
        setAdding={setAdding}
      />
      <Box marginTop="32px" marginLeft="32px" marginRight="32px">
        {data &&
          data.helloidGroups &&
          data.helloidGroups.map(group => {
            return (
              group && (
                <Box marginTop="20px" key={group.groupGuid || ''}>
                  <HelloIdApplications
                    applications={group.application || []}
                    buttonAction={handleAddApplication}
                    title={group.name || ''}
                    groupGuid={group.groupGuid || ''}
                    badge={true}
                    badgeAction={handleRemoveApplication}
                  />
                </Box>
              )
            );
          })}
      </Box>
    </>
  );
};
export default withRouter(Applications);
