import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper, { PaperProps } from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import { Grid } from '@material-ui/core';
import useStyles from './styles';
import { useMutation, useApolloClient } from '@apollo/react-hooks';
import {
  ADD_HELLOID_APPLICATION,
  ADD_HELLOID_APPLICATIONVariables,
} from '../../../../../graphql/HelloIdSso/HelloidGroup/types/ADD_HELLOID_APPLICATION';
import { DO_ADD_HELLOID_APPLICATION } from '../../../../../graphql/HelloIdSso/HelloidGroup/mutation';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import ApplicationDialogList from '../ApplicationDialogList/ApplicationDialogList';
import { GET_HELLOID_GROUPS } from '../../../../../graphql/HelloIdSso/HelloidGroup/query';
import {
  HELLOIDGROUPS,
  HELLOIDGROUPSVariables,
} from '../../../../../graphql/HelloIdSso/HelloidGroup/types/HELLOIDGROUPS';
import { getUser } from '../../../../../services/LocalStorage';
import {
  HELLOID_APPLICATION,
  HELLOID_APPLICATIONVariables,
} from '../../../../../graphql/HelloIdApplication/types/HELLOID_APPLICATION';
import { GET_HELLOID_APPLICATIONS } from '../../../../../graphql/HelloIdApplication/query';

export interface DialogProps {
  open: boolean;
  title: string;
  applicationList?: any[];
  groupGuid: string;
  groupement: string;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  setAdding?: React.Dispatch<React.SetStateAction<boolean>>;
}
export enum DialogAction {
  close,
  addToRole,
}

function PaperComponent(props: PaperProps) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

export default function DraggableDialog(props: DialogProps) {
  const { open, setOpen, title, applicationList, groupement, groupGuid, setAdding } = props;
  const classes = useStyles({});
  const client = useApolloClient();
  const [checkboxState, setCheckboxState] = React.useState<any[]>([]);
  const user = getUser();

  useEffect(() => {
    setCheckboxState([]);
  }, [open]);

  const handleClose = (action: DialogAction) => {
    if (action === DialogAction.addToRole) {
      const appsToAdd: string[] = [];
      for (const key in checkboxState) {
        if (checkboxState[key]) {
          appsToAdd.push(key);
        }
      }
      if (appsToAdd && appsToAdd.length !== 0) {
        addToGroupHelloId({
          variables: {
            idGroupement: groupement,
            groupGuid,
            applicationsGuid: appsToAdd,
          },
        });
        setAdding && setAdding(true);
      }
    }
    setOpen(false);
  };

  const [addToGroupHelloId] = useMutation<
    ADD_HELLOID_APPLICATION,
    ADD_HELLOID_APPLICATIONVariables
  >(DO_ADD_HELLOID_APPLICATION, {
    update: (cache, { data }) => {
      const created = data && data.addApplicationToHelloidGroup;

      const applicationsOnCache = cache.readQuery<HELLOIDGROUPS, HELLOIDGROUPSVariables>({
        query: GET_HELLOID_GROUPS,
        variables: {
          idgroupement: groupement,
        },
      });

      const applicationsGroup =
        applicationsOnCache &&
        applicationsOnCache.helloidGroups &&
        applicationsOnCache.helloidGroups.slice();

      const found =
        applicationsGroup &&
        applicationsGroup.find(
          group => group && group.groupGuid === (created && created.groupGuid),
        );
      if (found && created) {
        found.application = created.application;
      }
      cache.writeQuery({
        query: GET_HELLOID_GROUPS,
        data: { helloidGroups: applicationsGroup },
        variables: {
          idgroupement: groupement,
        },
      });

      if (created) {
        const helloidApplicationsRole = cache.readQuery<
          HELLOID_APPLICATION,
          HELLOID_APPLICATIONVariables
        >({
          query: GET_HELLOID_APPLICATIONS,
          variables: {
            idGroupement: groupement,
            iduser: (user && user.id) || '',
          },
        });

        if (created && created.application) {
          created.application.map(application => {
            const found =
              helloidApplicationsRole &&
              helloidApplicationsRole.userApplications &&
              helloidApplicationsRole.userApplications.find(
                appRole =>
                  (appRole && appRole.applicationGUID) ===
                  (application && application.applicationGUID),
              );
            if (!found) {
              helloidApplicationsRole &&
                helloidApplicationsRole.userApplications &&
                helloidApplicationsRole &&
                helloidApplicationsRole.userApplications.push(application);
            }
          });
          cache.writeQuery({
            query: GET_HELLOID_APPLICATIONS,
            data: {
              userApplications: helloidApplicationsRole && helloidApplicationsRole.userApplications,
            },
            variables: {
              idGroupement: groupement,
              iduser: (user && user.id) || '',
            },
          });
        }
      }
    },
    onCompleted: async data => {
      if (data && data.addApplicationToHelloidGroup) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Applications Ajoutées`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        setAdding && setAdding(false);
        // window.location.reload();
      }
    },
    onError: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur d'ajout des applications`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCheckboxState({ ...checkboxState, [event.target.name]: event.target.checked });
  };

  const canSave = () => {
    for (const key in checkboxState) {
      if (checkboxState[key] == true) return true;
    }
    // console.log(checked);
    return false;
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
        maxWidth="md"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          Ajouter d'applications pour {title}
        </DialogTitle>
        <DialogContent dividers={true} className={classes.dialogContent}>
          <form onSubmit={handleClose.bind(null, DialogAction.addToRole)}>
            <Grid container={true} spacing={2}>
              <ApplicationDialogList
                applicationList={applicationList}
                handleChangeCheck={handleChange}
              />
            </Grid>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose.bind(null, DialogAction.close)} variant="contained">
            Annuler
          </Button>
          <Button
            autoFocus={true}
            onClick={handleClose.bind(null, DialogAction.addToRole)}
            variant="contained"
            color="primary"
            disabled={!canSave()}
          >
            Ajouter
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
