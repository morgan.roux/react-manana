import { Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { flexWrap: 'nowrap' },
    leftOperation: {
      minWidth: 280,
      padding: 24,
      borderRight: '2px solid #E5E5E5',
      height: 'calc(100vh - 86px)',
      overflowY: 'auto',
    },
  }),
);

export default useStyles;
