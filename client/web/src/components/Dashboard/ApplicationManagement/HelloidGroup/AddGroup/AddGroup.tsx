import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper, { PaperProps } from '@material-ui/core/Paper';
import Draggable from 'react-draggable';
import { Grid, InputAdornment } from '@material-ui/core';
import useStyles from './styles';
import { useQuery, useMutation, useApolloClient } from '@apollo/react-hooks';
import SnackVariableInterface from '../../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../../utils/snackBarUtils';
import { DO_CREATE_HELLOID_GROUP } from '../../../../../graphql/HelloIdSso/HelloidGroup/mutation';
import {
  CREATE_HELLOID_GROUPVariables,
  CREATE_HELLOID_GROUP,
} from '../../../../../graphql/HelloIdSso/HelloidGroup/types/CREATE_HELLOID_GROUP';
import CustomSelect from '../../../../Common/CustomSelect';
import GroupIcon from '@material-ui/icons/Group';
import {
  ALL_HELLOID_APPLICATION,
  ALL_HELLOID_APPLICATIONVariables,
} from '../../../../../graphql/HelloIdApplication/types/ALL_HELLOID_APPLICATION';
import { GET_ALL_HELLOID_APPLICATIONS } from '../../../../../graphql/HelloIdApplication/query';
import ApplicationDialogList from '../../HelloidApplicationsRole/ApplicationDialogList';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
  ALL_HELLOID_GROUPS,
  ALL_HELLOID_GROUPSVariables,
} from '../../../../../graphql/HelloIdSso/HelloidGroup/types/ALL_HELLOID_GROUPS';
import {
  GET_ALL_HELLOID_GROUPS,
  GET_HELLOID_GROUPS,
} from '../../../../../graphql/HelloIdSso/HelloidGroup/query';
import {
  HELLOIDGROUPS,
  HELLOIDGROUPSVariables,
} from '../../../../../graphql/HelloIdSso/HelloidGroup/types/HELLOIDGROUPS';

export interface DialogProps {
  open: boolean;
  title: string;
  rolesList?: any[];
  groupement: string;
  setOpen: React.Dispatch<React.SetStateAction<boolean>>;
  setAddLoading: React.Dispatch<React.SetStateAction<boolean>>;
}
export enum DialogAction {
  close,
  addToRole,
}

function PaperComponent(props: PaperProps) {
  return (
    <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
      <Paper {...props} />
    </Draggable>
  );
}

export default function DraggableDialog(props: DialogProps) {
  const { open, setOpen, title, rolesList, groupement, setAddLoading } = props;
  const classes = useStyles({});
  const client = useApolloClient();
  const [checkboxState, setCheckboxState] = React.useState<any[]>([]);
  const [roleSelected, setRoleSelected] = React.useState<string>('');

  const handleClose = (action: DialogAction) => {
    if (action === DialogAction.addToRole) {
      const appsToAdd: string[] = [];
      for (const key in checkboxState) {
        if (checkboxState[key]) {
          appsToAdd.push(key);
        }
      }
      createGroupHelloId({
        variables: {
          idgroupement: groupement,
          ApplicationGUIDs: appsToAdd,
          IsDefault: false,
          IsEnabled: true,
          IsQrEnabled: false,
          Name: roleSelected,
        },
      });
      setAddLoading(true);
    } else {
      setRoleSelected('');
      setCheckboxState([]);
    }
    setOpen(false);
  };

  const { data: allApplications, loading } = useQuery<
    ALL_HELLOID_APPLICATION,
    ALL_HELLOID_APPLICATIONVariables
  >(GET_ALL_HELLOID_APPLICATIONS, {
    // fetchPolicy: "cache-and-network",
    variables: {
      idgroupement: groupement,
    },
  });

  const [createGroupHelloId] = useMutation<CREATE_HELLOID_GROUP, CREATE_HELLOID_GROUPVariables>(
    DO_CREATE_HELLOID_GROUP,
    {
      update: (cache, { data }) => {
        const created = data && data.createHelloIdGroup;

        const groupsOnCache = cache.readQuery<ALL_HELLOID_GROUPS, ALL_HELLOID_GROUPSVariables>({
          query: GET_ALL_HELLOID_GROUPS,
          variables: {
            idgroupement: groupement,
          },
        });
        const allHelloidGroupsInfo = groupsOnCache && groupsOnCache.allHelloidGroupsInfo;

        if (allHelloidGroupsInfo && created) {
          allHelloidGroupsInfo.push({
            groupGuid: created.groupGuid,
            immutableId: created.immutableId,
            isDefault: created.isDefault,
            isDeleted: created.isDeleted,
            isEnabled: created.isEnabled,
            isQrEnabled: created.isQrEnabled,
            managedByUserGuid: created.managedByUserGuid,
            name: created.name,
            source: created.source,
            __typename: 'HelloidGroupInfo',
          });

          cache.writeQuery({
            query: GET_ALL_HELLOID_GROUPS,
            data: { allHelloidGroupsInfo },
            variables: {
              idgroupement: groupement,
            },
          });

          // update list application
          try {
            const groupOnCache = cache.readQuery<HELLOIDGROUPS, HELLOIDGROUPSVariables>({
              query: GET_HELLOID_GROUPS,
              variables: {
                idgroupement: groupement,
              },
            });

            if (groupOnCache && groupOnCache.helloidGroups) {
              groupOnCache.helloidGroups.push({ ...created });

              cache.writeQuery({
                query: GET_HELLOID_GROUPS,
                data: { helloidGroups: groupOnCache.helloidGroups },
                variables: {
                  idgroupement: groupement,
                },
              });
            }
          } catch (error) {
            console.warn('query not in cache yet');
          }
        }
      },
      onCompleted: async data => {
        if (data && data.createHelloIdGroup) {
          const snackBarData: SnackVariableInterface = {
            type: 'SUCCESS',
            message: `Création du groupement aves succès`,
            isOpen: true,
          };
          displaySnackBar(client, snackBarData);
          setRoleSelected('');
          setAddLoading(false);
          setOpen(false);
          // window.location.reload();
        }
      },
      onError: () => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: `Erreur lors de la création du groupement`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
        setAddLoading(false);
        setRoleSelected('');
      },
    },
  );

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRoleSelected(event.target.value);
  };

  const handleApplicationChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCheckboxState({ ...checkboxState, [event.target.name]: event.target.checked });
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={handleClose}
        PaperComponent={PaperComponent}
        aria-labelledby="draggable-dialog-title"
        maxWidth="md"
      >
        <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
          {title}
        </DialogTitle>
        <DialogContent dividers={true} className={classes.Dialog}>
          <form onSubmit={handleClose.bind(null, DialogAction.addToRole)}>
            <Grid container={true} spacing={2}>
              {!rolesList ||
                (rolesList && rolesList.length === 0 && "Il n'y a pas d'autres roles à ajouter")}
              {rolesList && (
                <CustomSelect
                  list={rolesList}
                  listId="nom"
                  index="nom"
                  variant="outlined"
                  disabled={false}
                  value={roleSelected}
                  onClick={handleChange}
                  startAdornment={
                    <InputAdornment position="start">
                      <GroupIcon />
                    </InputAdornment>
                  }
                />
              )}
              {loading && <CircularProgress color="secondary" className={classes.loader} />}
              {allApplications && allApplications.helloidApplications && (
                <Grid container={true} spacing={2}>
                  <ApplicationDialogList
                    applicationList={allApplications.helloidApplications}
                    handleChangeCheck={handleApplicationChange}
                  />
                </Grid>
              )}
            </Grid>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose.bind(null, DialogAction.close)} variant="contained">
            Annuler
          </Button>
          <Button
            autoFocus={true}
            onClick={handleClose.bind(null, DialogAction.addToRole)}
            variant="contained"
            color="primary"
            disabled={roleSelected ? false : true}
          >
            Ajouter
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
