import React, { FC, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { Box, Button } from '@material-ui/core';
import { useQuery, useApolloClient, useMutation } from '@apollo/react-hooks';
import { getGroupement } from '../../../../services/LocalStorage';
import { Loader } from '../../Content/Loader';
import {
  GET_ALL_HELLOID_GROUPS,
  GET_HELLOID_GROUPS,
} from '../../../../graphql/HelloIdSso/HelloidGroup/query';
import {
  ALL_HELLOID_GROUPS,
  ALL_HELLOID_GROUPSVariables,
} from '../../../../graphql/HelloIdSso/HelloidGroup/types/ALL_HELLOID_GROUPS';
import AddGroup from './AddGroup/AddGroup';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import { GET_USER_ROLES } from '../../../../graphql/Role/query';
import { USER_ROLES } from '../../../../graphql/Role/types/USER_ROLES';
import useStyles from './styles';
import GroupList from './GroupList';
import {
  DELETE_HELLOID_GROUP,
  DELETE_HELLOID_GROUPVariables,
} from '../../../../graphql/HelloIdSso/HelloidGroup/types/DELETE_HELLOID_GROUP';
import { DO_DELETE_HELLOID_GROUP } from '../../../../graphql/HelloIdSso/HelloidGroup/mutation';
import Backdrop from '../../../Common/Backdrop';
import {
  HELLOIDGROUPS,
  HELLOIDGROUPSVariables,
  HELLOIDGROUPS_helloidGroups,
} from '../../../../graphql/HelloIdSso/HelloidGroup/types/HELLOIDGROUPS';

export interface groupsData {
  name: string;
  groupGuid: string;
  managedByUserGuid: string;
  immutableId: string;
  isEnabled: boolean;
  isDefault: boolean;
  isQrEnabled: boolean;
  isDeleted: boolean;
  source: string;
}

const HelloIdGroup: FC = () => {
  const groupement = getGroupement();
  const client = useApolloClient();
  const classes = useStyles({});
  const [open, setOpen] = React.useState(false);
  const [rolesToShow, setRolesToShow] = React.useState<any[]>([]);
  const [groupList, setGroupList] = React.useState<groupsData[]>([]);
  const [addLoading, setAddLoading] = React.useState<boolean>(false);

  const { data: HelloidGroup, loading } = useQuery<ALL_HELLOID_GROUPS, ALL_HELLOID_GROUPSVariables>(
    GET_ALL_HELLOID_GROUPS,
    {
      // fetchPolicy: "cache-and-network",
      variables: {
        idgroupement: (groupement && groupement.id) || '',
      },
      onError: () => {
        const snackBarData: SnackVariableInterface = {
          type: 'ERROR',
          message: `Erreur lors de la récupération des groupes.`,
          isOpen: true,
        };
        displaySnackBar(client, snackBarData);
      },
    },
  );

  const [deleteHelloIdGroup, { loading: loadingRemove }] = useMutation<
    DELETE_HELLOID_GROUP,
    DELETE_HELLOID_GROUPVariables
  >(DO_DELETE_HELLOID_GROUP, {
    update: (cache, { data }) => {
      const allHelloidGroupsInfo = data && data.deleteHelloIdGroupInfo;

      if (allHelloidGroupsInfo) {
        cache.writeQuery({
          query: GET_ALL_HELLOID_GROUPS,
          data: { allHelloidGroupsInfo },
          variables: {
            idgroupement: (groupement && groupement.id) || '',
          },
        });

        //remove group to list
        try {
          const groupOnCache = cache.readQuery<HELLOIDGROUPS, HELLOIDGROUPSVariables>({
            query: GET_HELLOID_GROUPS,
            variables: {
              idgroupement: (groupement && groupement.id) || '',
            },
          });

          if (groupOnCache && groupOnCache.helloidGroups && allHelloidGroupsInfo) {
            let groupUpdated: (HELLOIDGROUPS_helloidGroups | null)[] = [];
            groupOnCache.helloidGroups.map(group => {
              const found = allHelloidGroupsInfo.find(
                deleted => (deleted && deleted.groupGuid) === (group && group.groupGuid),
              );
              if (found) {
                groupUpdated.push(group);
              }
            });

            cache.writeQuery({
              query: GET_HELLOID_GROUPS,
              data: { helloidGroups: groupUpdated },
              variables: {
                idgroupement: (groupement && groupement.id) || '',
              },
            });
          }
        } catch (error) {
          console.warn('query not in cache yet');
        }
      }
    },
    onCompleted: async data => {
      if (data && data.deleteHelloIdGroupInfo) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `HelloID group supprimé`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        setOpen(false);
        // window.location.reload();
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de la suppréssion du group!`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const { data: rolesData, loading: loadingRole } = useQuery<USER_ROLES>(GET_USER_ROLES, {
    onError: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de la récupération des rôles.`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });
  const handleAddGroup = () => {
    setOpen(true);
  };

  const handleAction = (data: any) => {
    const { groupGuid } = data;
    if (groupGuid) {
      deleteHelloIdGroup({
        variables: {
          groupGuid: groupGuid,
          idgroupement: (groupement && groupement.id) || '',
        },
      });
    }
  };

  useEffect(() => {
    const roles: any[] = [];
    if (HelloidGroup && rolesData && rolesData.roles) {
      rolesData.roles.forEach(role => {
        const find =
          HelloidGroup.allHelloidGroupsInfo &&
          HelloidGroup.allHelloidGroupsInfo.find(
            helloidGp =>
              (helloidGp && helloidGp.name && helloidGp.name.trim().toLocaleLowerCase()) ===
              (role && role.nom && role.nom.trim().toLocaleLowerCase()),
          );
        if (!find) {
          roles.push(role);
        }
      });
      setRolesToShow(roles);
      const grouplst: groupsData[] = [];
      HelloidGroup.allHelloidGroupsInfo &&
        HelloidGroup.allHelloidGroupsInfo.forEach(group => {
          group &&
            grouplst.push({
              groupGuid: group.groupGuid || '',
              immutableId: group.immutableId || '',
              isDefault: group.isDefault || false,
              isDeleted: group.isDeleted || false,
              isEnabled: group.isEnabled || false,
              isQrEnabled: group.isQrEnabled || false,
              managedByUserGuid: group.managedByUserGuid || '',
              name: group.name || '',
              source: group.source || '',
            });
        });
      setGroupList(grouplst);
    }
  }, [
    HelloidGroup,
    rolesData,
    HelloidGroup && HelloidGroup.allHelloidGroupsInfo && HelloidGroup.allHelloidGroupsInfo.length,
  ]);

  if (loading || loadingRole) return <Loader />;

  return (
    <>
      {loadingRemove && <Backdrop value="Suppréssion du groupe en cours, veuillez patienter..." />}
      {addLoading && <Backdrop value="Ajout du groupe en cours, veuillez patienter..." />}
      <AddGroup
        open={open}
        setOpen={setOpen}
        title={'CREER UN GROUPE DANS HELLOID'}
        rolesList={rolesToShow}
        groupement={groupement && groupement.id}
        setAddLoading={setAddLoading}
      />
      <Box marginTop="32px" marginLeft="32px" marginRight="32px">
        <h3 className={classes.titleApps}>HELLOID GROUPES</h3>
        <Button
          variant="contained"
          color="primary"
          className={classes.buttonAdd}
          onClick={handleAddGroup}
        >
          Ajouter un groupe
        </Button>
        {groupList && <GroupList rows={groupList} buttonAction={handleAction} />}
      </Box>
    </>
  );
};
export default withRouter(HelloIdGroup);
