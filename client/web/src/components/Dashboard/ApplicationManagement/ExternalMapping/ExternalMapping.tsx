import React, { FC, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { Box, Fab } from '@material-ui/core';
import { useQuery, useApolloClient, useMutation } from '@apollo/react-hooks';
import { getGroupement } from '../../../../services/LocalStorage';
import { Loader } from '../../Content/Loader';
import SnackVariableInterface from '../../../../Interface/SnackVariableInterface';
import { displaySnackBar } from '../../../../utils/snackBarUtils';
import AddIcon from '@material-ui/icons/Add';
import useStyles from './styles';
import Tooltip from '@material-ui/core/Tooltip';
import { GET_MANY_CRYPTO_MD5 } from '../../../../graphql/CryptoMd5/query';
import {
  MANY_CRYPTO_MD5,
  MANY_CRYPTO_MD5Variables,
  MANY_CRYPTO_MD5_cryptoMd5s,
  MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings,
} from '../../../../graphql/CryptoMd5/types/MANY_CRYPTO_MD5';
import ExternalMappingList from './ExternalMappingList';
import AddExternalMapping from './AddExternalMapping';
import { ExternalMappingInput } from '../../../../types/graphql-global-types';
import {
  DELETE_EXTERNAL_MAPPING,
  DELETE_EXTERNAL_MAPPINGVariables,
} from '../../../../graphql/ExternalMapping/types/DELETE_EXTERNAL_MAPPING';
import { DO_DELETE_EXTERNAL_MAPPING } from '../../../../graphql/ExternalMapping/mutation';
import Backdrop from '../../../Common/Backdrop';

export enum Action {
  delete,
  add,
  edit,
}

const ExternalMapping: FC = () => {
  const groupement = getGroupement();
  const client = useApolloClient();
  const classes = useStyles({});
  const [] = useState(false);
  const [cryptoMd5s, setCryptoMd5s] = useState<MANY_CRYPTO_MD5_cryptoMd5s[]>([]);
  const [isAdd, setIsAdd] = React.useState(false);
  const [isNew, setIsNew] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [md5ID, setMd5ID] = React.useState('');
  const [title, setTitle] = React.useState('');
  const [actualExternalMapping, setActualExternalMapping] = useState<
    (MANY_CRYPTO_MD5_cryptoMd5s_externalUserMappings | null)[]
  >([]);
  const [dataEdit, setDataEdit] = useState<ExternalMappingInput>();
  const [addLoading, setAddLoading] = React.useState<boolean>(false);

  const { data: allMd5, loading: allMd5Loading } = useQuery<
    MANY_CRYPTO_MD5,
    MANY_CRYPTO_MD5Variables
  >(GET_MANY_CRYPTO_MD5, {
    fetchPolicy: 'cache-and-network',
    variables: {
      idgroupement: (groupement && groupement.id) || '',
    },
    onError: () => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de l'affichage des mappings, veuillez recharger la page!!`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const [deleteUserMapping, { loading: deleteLoading }] = useMutation<
    DELETE_EXTERNAL_MAPPING,
    DELETE_EXTERNAL_MAPPINGVariables
  >(DO_DELETE_EXTERNAL_MAPPING, {
    onCompleted: async data => {
      if (data && data.deleteExternalMapping) {
        const snackBarData: SnackVariableInterface = {
          type: 'SUCCESS',
          message: `Mapping supprimé`,
          isOpen: true,
        };

        displaySnackBar(client, snackBarData);
        setOpen(false);
        // window.location.reload();
      }
    },
    onError: errors => {
      const snackBarData: SnackVariableInterface = {
        type: 'ERROR',
        message: `Erreur lors de la suppréssion du mapping!`,
        isOpen: true,
      };
      displaySnackBar(client, snackBarData);
    },
  });

  const handleAddNewApplicationMapping = () => {
    setActualExternalMapping([]);
    setTitle(`Ajouter un nouveau mapping`);
    setDataEdit(undefined);
    setIsAdd(true);
    setIsNew(true);
    setOpen(true);
  };

  const handleAction = (data: any) => {
    const {
      action,
      md5Id,
      externalMappings,
      mappingToEdit,
      mappingToDelete,
      ssoApplicationName,
    } = data;
    switch (action) {
      case Action.add:
        externalMappings && setActualExternalMapping(externalMappings);
        setTitle(`Ajouter un mapping pour ${ssoApplicationName}`);
        setDataEdit(undefined);
        setIsAdd(true);
        setMd5ID(md5Id);
        setOpen(true);
        setIsNew(false);
        break;
      case Action.edit:
        setActualExternalMapping([]);
        setDataEdit({
          ...mappingToEdit,
        });
        setTitle(`Modifier un mapping pour ${ssoApplicationName}`);
        setIsAdd(false);
        setMd5ID(md5Id);
        setOpen(true);
        setIsNew(false);
        break;
      case Action.delete:
        deleteUserMapping({
          variables: {
            id: mappingToDelete.id,
          },
          update: (cache, { data: dataDeleted }) => {
            const deleted = dataDeleted && dataDeleted.deleteExternalMapping;

            if (deleted) {
              const md5OnCache = cache.readQuery<MANY_CRYPTO_MD5, MANY_CRYPTO_MD5Variables>({
                query: GET_MANY_CRYPTO_MD5,
                variables: {
                  idgroupement: (groupement && groupement.id) || '',
                },
              });

              if (md5OnCache && md5OnCache.cryptoMd5s) {
                let found = md5OnCache.cryptoMd5s.find(md5 => (md5 && md5.id) === md5Id);
                if (found) {
                  found.externalUserMappings =
                    found.externalUserMappings &&
                    found.externalUserMappings.filter(
                      extrnalmapping => (extrnalmapping && extrnalmapping.id) !== deleted.id,
                    );
                  console.log('found ', found);
                  cache.writeQuery({
                    query: GET_MANY_CRYPTO_MD5,
                    data: { cryptoMd5s: md5OnCache.cryptoMd5s },
                    variables: {
                      idgroupement: (groupement && groupement.id) || '',
                    },
                  });
                }
              }
            }
          },
        });
        setIsAdd(false);
        setIsNew(false);
        setMd5ID(md5Id);
        break;
      default:
        console.log('Action unknown');
        break;
    }
  };

  useEffect(() => {
    console.log('useEffect change');

    let md5Apps: MANY_CRYPTO_MD5_cryptoMd5s[] = [];
    allMd5 &&
      allMd5.cryptoMd5s &&
      allMd5.cryptoMd5s.map(md5 => {
        md5 && md5Apps.push(md5);
      });
    setCryptoMd5s(md5Apps);
  }, [allMd5, allMd5 && allMd5.cryptoMd5s, addLoading]);

  if (allMd5Loading) return <Loader />;

  return (
    <>
      {deleteLoading && (
        <Backdrop value={`Suppréssion du mapping en cours, veuillez patienter...`} />
      )}
      {addLoading && <Backdrop value="Ajout du mapping en cours, veuillez patienter..." />}
      <AddExternalMapping
        isAdd={isAdd}
        open={open}
        setOpen={setOpen}
        md5Id={md5ID}
        allMappingList={actualExternalMapping}
        defaultData={dataEdit}
        title={title}
        isNew={isNew}
        appToShow={
          isNew
            ? cryptoMd5s
                .slice()
                .filter(data => data.externalUserMappings && data.externalUserMappings.length === 0)
            : undefined
        }
        setAddLoading={setAddLoading}
      />
      <Box marginTop="32px" marginLeft="32px" marginRight="32px">
        {cryptoMd5s
          .slice()
          .filter(data => data.externalUserMappings && data.externalUserMappings.length !== 0)
          .map(md5 => {
            return (
              <Box marginTop="20px" key={md5.id}>
                <ExternalMappingList
                  md5Application={md5}
                  buttonAction={handleAction}
                  added={addLoading}
                  deleted={deleteLoading}
                />
              </Box>
            );
          })}
        <Tooltip title="Ajouter un nouveau mapping" aria-label="Ajouter un nouveau mapping">
          <Fab
            color="primary"
            aria-label="Ajouter un nouveau mapping"
            className={classes.floatRight}
            onClick={handleAddNewApplicationMapping}
          >
            <AddIcon />
          </Fab>
        </Tooltip>
      </Box>
    </>
  );
};
export default withRouter(ExternalMapping);
